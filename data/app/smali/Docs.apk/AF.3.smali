.class public LAF;
.super LAw;
.source "Paragraph.java"


# instance fields
.field private a:LCq;

.field private final a:LDA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private a:LDI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDI",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LzI",
            "<",
            "LCl;",
            ">;>;"
        }
    .end annotation
.end field

.field private a:LzI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LzI",
            "<",
            "LCq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LDb;LAb;Lzd;Lxu;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "LAb;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3, p4}, LAw;-><init>(LDb;LAb;Lzd;Lxu;)V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, LAF;->a:LDI;

    .line 48
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, LAF;->a:Ljava/util/Map;

    .line 57
    new-instance v0, LAG;

    invoke-direct {v0, p0}, LAG;-><init>(LAF;)V

    iput-object v0, p0, LAF;->a:LDA;

    .line 80
    iget-object v0, p0, LAF;->a:LDA;

    invoke-virtual {v0, p0}, LDA;->a(Ljava/lang/Object;)V

    .line 81
    return-void
.end method

.method private a()V
    .registers 7

    .prologue
    .line 128
    iget-object v0, p0, LAF;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 129
    iget-object v1, p0, LAF;->a:Lzd;

    invoke-interface {v1, v0}, Lzd;->a(I)Lxc;

    move-result-object v1

    .line 130
    iget-object v2, p0, LAF;->a:Lzd;

    invoke-interface {v2, v0}, Lzd;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 131
    iget-object v3, p0, LAF;->a:Lzd;

    invoke-interface {v3, v0}, Lzd;->b(I)Lxg;

    move-result-object v0

    .line 132
    iget-object v3, p0, LAF;->a:Lxu;

    invoke-interface {v3}, Lxu;->a()Landroid/content/Context;

    move-result-object v3

    .line 133
    new-instance v4, LCq;

    iget-object v5, p0, LAF;->a:Lxu;

    invoke-direct {v4, v1, v5, v2, v0}, LCq;-><init>(Lxc;Lxu;Ljava/lang/String;Lxg;)V

    .line 135
    iget-object v0, p0, LAF;->a:LCq;

    invoke-virtual {v4, v0}, LCq;->a(LCt;)Z

    move-result v0

    if-nez v0, :cond_44

    .line 136
    iput-object v4, p0, LAF;->a:LCq;

    .line 137
    iget-object v0, p0, LAF;->a:LzI;

    if-eqz v0, :cond_3c

    .line 139
    iget-object v0, p0, LAF;->a:LzI;

    iget-object v1, p0, LAF;->a:LDA;

    invoke-interface {v0, v1}, LzI;->a(Landroid/text/Spannable;)V

    .line 141
    :cond_3c
    iget-object v0, p0, LAF;->a:LCq;

    invoke-virtual {v0}, LCq;->a()LzI;

    move-result-object v0

    iput-object v0, p0, LAF;->a:LzI;

    .line 144
    :cond_44
    iget-object v0, p0, LAF;->a:LDI;

    if-eqz v0, :cond_5a

    iget-object v0, p0, LAF;->a:LDI;

    invoke-virtual {v0}, LDI;->a()I

    move-result v0

    .line 146
    :goto_4e
    iget-object v1, p0, LAF;->a:LzI;

    iget-object v2, p0, LAF;->a:LDA;

    const/4 v4, 0x0

    invoke-interface {v1, v2, v4, v0, v3}, LzI;->a(Landroid/text/Spannable;IILandroid/content/Context;)V

    .line 148
    invoke-direct {p0}, LAF;->b()V

    .line 149
    return-void

    .line 144
    :cond_5a
    iget-object v0, p0, LAF;->a:LDA;

    invoke-virtual {v0}, LDA;->length()I

    move-result v0

    goto :goto_4e
.end method

.method private b()V
    .registers 11

    .prologue
    const/4 v2, 0x0

    .line 155
    iget-object v0, p0, LAF;->a:Lzd;

    iget-object v1, p0, LAF;->a:LDb;

    invoke-virtual {v1}, LDb;->d()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Lzd;->a(I)[Lvu;

    move-result-object v4

    .line 156
    iget-object v0, p0, LAF;->a:Lxu;

    invoke-interface {v0}, Lxu;->a()Landroid/content/Context;

    move-result-object v5

    .line 157
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 158
    array-length v3, v4

    move v0, v2

    :goto_1c
    if-ge v0, v3, :cond_2a

    aget-object v6, v4, v0

    .line 159
    invoke-interface {v6}, Lvu;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 164
    :cond_2a
    iget-object v0, p0, LAF;->a:LDI;

    if-nez v0, :cond_47

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_47

    .line 165
    new-instance v0, LDI;

    invoke-direct {v0}, LDI;-><init>()V

    iput-object v0, p0, LAF;->a:LDI;

    .line 166
    iget-object v0, p0, LAF;->a:LDI;

    invoke-virtual {v0, p0}, LDI;->a(Ljava/lang/Object;)V

    .line 167
    iget-object v0, p0, LAF;->a:LDA;

    iget-object v3, p0, LAF;->a:LDI;

    invoke-virtual {v0, v3}, LDA;->a(LDG;)V

    .line 171
    :cond_47
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 172
    iget-object v0, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_56
    :goto_56
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 173
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_56

    .line 174
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_56

    .line 178
    :cond_6c
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_70
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 179
    iget-object v1, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LzI;

    .line 180
    iget-object v6, p0, LAF;->a:LDI;

    invoke-interface {v1, v6}, LzI;->a(Landroid/text/Spannable;)V

    .line 181
    iget-object v1, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_70

    .line 185
    :cond_8f
    array-length v6, v4

    move v3, v2

    :goto_91
    if-ge v3, v6, :cond_d6

    aget-object v7, v4, v3

    .line 186
    invoke-interface {v7}, Lvu;->a()Ljava/lang/String;

    move-result-object v0

    .line 187
    new-instance v8, LCl;

    iget-object v1, p0, LAF;->a:Lxu;

    invoke-interface {v1}, Lxu;->a()LAS;

    move-result-object v1

    iget-object v9, p0, LAF;->a:Lxu;

    invoke-direct {v8, v1, v7, v9}, LCl;-><init>(LAS;Lvu;Lxu;)V

    .line 188
    iget-object v1, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzI;

    .line 189
    if-eqz v0, :cond_d4

    invoke-interface {v0}, LzI;->a()LCt;

    move-result-object v1

    check-cast v1, LCl;

    .line 190
    :goto_b6
    invoke-virtual {v8, v1}, LCl;->a(LCt;)Z

    move-result v1

    if-nez v1, :cond_d0

    .line 191
    if-eqz v0, :cond_c3

    .line 192
    iget-object v1, p0, LAF;->a:LDI;

    invoke-interface {v0, v1}, LzI;->a(Landroid/text/Spannable;)V

    .line 194
    :cond_c3
    invoke-virtual {v8}, LCl;->a()LzI;

    move-result-object v0

    .line 195
    iget-object v1, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v7}, Lvu;->a()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v1, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    :cond_d0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_91

    .line 189
    :cond_d4
    const/4 v1, 0x0

    goto :goto_b6

    .line 200
    :cond_d6
    iget-object v0, p0, LAF;->a:LDI;

    if-eqz v0, :cond_101

    iget-object v0, p0, LAF;->a:LDI;

    invoke-virtual {v0}, LDI;->length()I

    move-result v0

    iget-object v1, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    if-eq v0, v1, :cond_101

    .line 201
    iget-object v0, p0, LAF;->a:LDI;

    iget-object v1, p0, LAF;->a:LDI;

    invoke-virtual {v1}, LDI;->length()I

    move-result v1

    const-string v3, "*\n"

    iget-object v4, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    invoke-static {v3, v4}, LagE;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, LDI;->a(IILjava/lang/CharSequence;)LDI;

    .line 204
    :cond_101
    iget-object v0, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_10c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_123

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzI;

    .line 205
    iget-object v2, p0, LAF;->a:LDI;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v0, v2, v1, v4, v5}, LzI;->a(Landroid/text/Spannable;IILandroid/content/Context;)V

    .line 206
    add-int/lit8 v0, v1, 0x2

    move v1, v0

    goto :goto_10c

    .line 208
    :cond_123
    return-void
.end method


# virtual methods
.method protected a(I)I
    .registers 3
    .parameter

    .prologue
    .line 239
    iget-object v0, p0, LAF;->a:LDb;

    invoke-virtual {v0}, LDb;->f()I

    move-result v0

    if-ge p1, v0, :cond_f

    iget-object v0, p0, LAF;->a:LDA;

    invoke-virtual {v0}, LDA;->b()I

    move-result v0

    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, LAF;->a:LDA;

    invoke-virtual {v0}, LDA;->c()I

    move-result v0

    goto :goto_e
.end method

.method protected a(LAc;)LAx;
    .registers 5
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, LAF;->a:LDb;

    invoke-virtual {v0}, LDb;->a()LDd;

    move-result-object v0

    check-cast v0, LDb;

    .line 114
    invoke-interface {p1, v0}, LAc;->a(LDb;)LAx;

    move-result-object v0

    .line 115
    iget-object v1, p0, LAF;->a:LDI;

    if-eqz v1, :cond_1a

    .line 116
    iget-object v1, p0, LAF;->a:LDI;

    invoke-interface {v0}, LAx;->a()LDG;

    move-result-object v2

    invoke-virtual {v1, v2}, LDI;->b(LDG;)V

    .line 120
    :goto_19
    return-object v0

    .line 118
    :cond_1a
    iget-object v1, p0, LAF;->a:LDA;

    invoke-interface {v0}, LAx;->a()LDG;

    move-result-object v2

    invoke-virtual {v1, v2}, LDA;->a(LDG;)V

    goto :goto_19
.end method

.method public a()LDA;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, LAF;->a:LDA;

    return-object v0
.end method

.method public bridge synthetic a()LDG;
    .registers 2

    .prologue
    .line 39
    invoke-virtual {p0}, LAF;->a()LDA;

    move-result-object v0

    return-object v0
.end method

.method public a(IILjava/util/List;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, LAF;->a:LDA;

    add-int/lit8 v1, p1, -0x1

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {v0, v1, v2}, LDA;->a(II)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 230
    invoke-virtual {v0}, LDG;->a()Ljava/lang/Object;

    move-result-object v2

    if-eq v2, p0, :cond_e

    .line 231
    invoke-virtual {v0}, LDG;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    invoke-interface {v0, p1, p2, p3}, LAx;->a(IILjava/util/List;)V

    goto :goto_e

    .line 234
    :cond_2a
    return-void
.end method

.method public a(Lwj;Lza;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 91
    invoke-super {p0, p1, p2, p3}, LAw;->a(Lwj;Lza;I)V

    .line 93
    invoke-direct {p0}, LAF;->a()V

    .line 94
    return-void
.end method

.method public a(Lza;)V
    .registers 5
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, LAF;->a:LzI;

    if-eqz v0, :cond_b

    .line 99
    iget-object v0, p0, LAF;->a:LzI;

    iget-object v1, p0, LAF;->a:LDA;

    invoke-interface {v0, v1}, LzI;->a(Landroid/text/Spannable;)V

    .line 103
    :cond_b
    iget-object v0, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_15
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LzI;

    .line 104
    iget-object v2, p0, LAF;->a:LDI;

    invoke-interface {v0, v2}, LzI;->a(Landroid/text/Spannable;)V

    goto :goto_15

    .line 106
    :cond_27
    iget-object v0, p0, LAF;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 108
    invoke-super {p0, p1}, LAw;->a(Lza;)V

    .line 109
    return-void
.end method

.method public a(IILCh;)Z
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 212
    iget-object v0, p0, LAF;->a:LCq;

    invoke-virtual {p3, v0}, LCh;->a(LCq;)Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    .line 221
    :goto_a
    return v0

    .line 215
    :cond_b
    iget-object v0, p0, LAF;->a:LDA;

    invoke-virtual {v0, p1, p2}, LDA;->a(II)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 216
    invoke-virtual {v0}, LDG;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    .line 217
    if-eq v0, p0, :cond_15

    invoke-interface {v0, p1, p2, p3}, LAx;->a(IILCh;)Z

    move-result v0

    if-nez v0, :cond_15

    move v0, v1

    .line 218
    goto :goto_a

    .line 221
    :cond_31
    const/4 v0, 0x1

    goto :goto_a
.end method

.method protected b(I)I
    .registers 3
    .parameter

    .prologue
    .line 247
    iget-object v0, p0, LAF;->a:LDA;

    invoke-virtual {v0}, LDA;->b()I

    move-result v0

    if-ge p1, v0, :cond_f

    iget-object v0, p0, LAF;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    :goto_e
    return v0

    :cond_f
    iget-object v0, p0, LAF;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v0

    goto :goto_e
.end method
