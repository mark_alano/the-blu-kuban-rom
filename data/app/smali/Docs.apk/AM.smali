.class public abstract LAM;
.super LAC;
.source "UnitElement.java"


# direct methods
.method public constructor <init>(LDb;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1}, LAC;-><init>(LDb;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected a(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 60
    return-void
.end method

.method public a(IILjava/util/List;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, LAM;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    if-gt p1, v0, :cond_2c

    iget-object v0, p0, LAM;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    if-lt p2, v0, :cond_2c

    .line 81
    iget-object v0, p0, LAM;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v1

    .line 82
    iget-object v0, p0, LAM;->a:LDI;

    invoke-virtual {v0}, LDI;->c()I

    move-result v0

    if-lt p2, v0, :cond_2d

    iget-object v0, p0, LAM;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v0

    .line 83
    :goto_24
    new-instance v2, LKj;

    invoke-direct {v2, v1, v0}, LKj;-><init>(II)V

    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_2c
    return-void

    :cond_2d
    move v0, v1

    .line 82
    goto :goto_24
.end method

.method public a(Lwj;Lza;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3}, LAC;->a(Lwj;Lza;I)V

    .line 42
    invoke-virtual {p0}, LAM;->b()V

    .line 43
    return-void
.end method

.method public a(IILCh;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract b()V
.end method

.method protected b(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 65
    return-void
.end method

.method public c(I)I
    .registers 3
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, LAM;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    return v0
.end method

.method protected c(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 70
    return-void
.end method

.method public d(I)I
    .registers 3
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, LAM;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    return v0
.end method
