.class LAP;
.super Ljava/lang/Object;
.source "UnsupportedTableElement.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:LAO;


# direct methods
.method constructor <init>(LAO;)V
    .registers 2
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, LAP;->a:LAO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, LAP;->a:LAO;

    iget-object v0, v0, LAO;->a:Lxu;

    invoke-interface {v0}, Lxu;->a()LzP;

    move-result-object v0

    .line 44
    if-nez v0, :cond_12

    .line 45
    const-string v0, "UnsupportedTableElement"

    const-string v1, "WebViewLauncher is not defined."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :cond_11
    :goto_11
    return-void

    .line 48
    :cond_12
    iget-object v1, p0, LAP;->a:LAO;

    invoke-static {v1}, LAO;->a(LAO;)LxX;

    move-result-object v1

    iget-object v2, p0, LAP;->a:LAO;

    iget-object v2, v2, LAO;->a:Lzd;

    iget-object v3, p0, LAP;->a:LAO;

    iget-object v3, v3, LAO;->a:LDb;

    invoke-virtual {v3}, LDb;->c()I

    move-result v3

    iget-object v4, p0, LAP;->a:LAO;

    iget-object v4, v4, LAO;->a:LDb;

    invoke-virtual {v4}, LDb;->d()I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, LxX;->a(Lzd;II)Ljava/lang/String;

    move-result-object v1

    .line 50
    if-eqz v1, :cond_11

    .line 51
    invoke-interface {v0, v1}, LzP;->a(Ljava/lang/String;)V

    goto :goto_11
.end method
