.class public final LAZ;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LAS;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LBd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 32
    iput-object p1, p0, LAZ;->a:LYD;

    .line 33
    const-class v0, LAS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LAZ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LAZ;->a:LZb;

    .line 36
    const-class v0, LBd;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LAZ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LAZ;->b:LZb;

    .line 39
    return-void
.end method

.method static synthetic a(LAZ;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LAZ;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 68
    const-class v0, LBd;

    new-instance v1, LBa;

    invoke-direct {v1, p0}, LBa;-><init>(LAZ;)V

    invoke-virtual {p0, v0, v1}, LAZ;->a(Ljava/lang/Class;Laou;)V

    .line 76
    const-class v0, LAS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LAZ;->a:LZb;

    invoke-virtual {p0, v0, v1}, LAZ;->a(Laop;LZb;)V

    .line 77
    const-class v0, LBd;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LAZ;->b:LZb;

    invoke-virtual {p0, v0, v1}, LAZ;->a(Laop;LZb;)V

    .line 78
    iget-object v0, p0, LAZ;->a:LZb;

    new-instance v1, LBb;

    invoke-direct {v1, p0}, LBb;-><init>(LAZ;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 87
    iget-object v0, p0, LAZ;->b:LZb;

    new-instance v1, LBc;

    invoke-direct {v1, p0}, LBc;-><init>(LAZ;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 98
    return-void
.end method

.method public a(LBd;)V
    .registers 3
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, LAZ;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->g:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LAZ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZS;

    iput-object v0, p1, LBd;->a:LZS;

    .line 51
    iget-object v0, p0, LAZ;->a:LYD;

    iget-object v0, v0, LYD;->a:La;

    iget-object v0, v0, La;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LAZ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    iput-object v0, p1, LBd;->a:Landroid/app/Application;

    .line 57
    iget-object v0, p0, LAZ;->a:LYD;

    iget-object v0, v0, LYD;->a:LNl;

    iget-object v0, v0, LNl;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LAZ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNj;

    iput-object v0, p1, LBd;->a:LNj;

    .line 63
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 102
    return-void
.end method
