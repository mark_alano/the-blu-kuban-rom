.class public LAu;
.super Lvh;
.source "CellView.java"


# instance fields
.field private final a:LAF;

.field private final a:LCJ;

.field private final a:LDb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDb",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LDk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDk",
            "<",
            "LzU;",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LDp;

.field private final a:Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;

.field private final a:Lvw;

.field private final a:Lxa;

.field private final a:Lzd;

.field private final a:Lzi;

.field private final b:LDb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDb",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lvw;Lzd;Lxa;LAb;Lxu;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Lvh;-><init>()V

    .line 59
    iput-object p1, p0, LAu;->a:Lvw;

    .line 60
    iput-object p2, p0, LAu;->a:Lzd;

    .line 61
    iput-object p3, p0, LAu;->a:Lxa;

    .line 62
    new-instance v1, LDa;

    invoke-direct {v1}, LDa;-><init>()V

    .line 63
    invoke-virtual {v1}, LDa;->a()LDb;

    move-result-object v0

    invoke-virtual {v0}, LDb;->a()LDd;

    move-result-object v0

    check-cast v0, LDb;

    iput-object v0, p0, LAu;->a:LDb;

    .line 64
    invoke-virtual {v1}, LDa;->a()LDb;

    move-result-object v0

    invoke-virtual {v0}, LDb;->a()LDd;

    move-result-object v0

    check-cast v0, LDb;

    iput-object v0, p0, LAu;->b:LDb;

    .line 65
    new-instance v0, LAF;

    iget-object v1, p0, LAu;->b:LDb;

    invoke-direct {v0, v1, p4, p2, p5}, LAF;-><init>(LDb;LAb;Lzd;Lxu;)V

    iput-object v0, p0, LAu;->a:LAF;

    .line 67
    new-instance v0, LDk;

    invoke-direct {v0}, LDk;-><init>()V

    iput-object v0, p0, LAu;->a:LDk;

    .line 68
    iget-object v0, p0, LAu;->a:LDk;

    iget-object v1, p0, LAu;->a:LAF;

    invoke-virtual {v1}, LAF;->a()LDA;

    move-result-object v1

    invoke-virtual {v0, v1}, LDk;->a(LDG;)V

    .line 69
    iget-object v0, p0, LAu;->a:LDk;

    invoke-virtual {v0}, LDk;->a()LDp;

    move-result-object v0

    iput-object v0, p0, LAu;->a:LDp;

    .line 70
    new-instance v0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;

    invoke-interface {p5}, Lxu;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LAu;->a:LDk;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;-><init>(Landroid/content/Context;LDk;)V

    iput-object v0, p0, LAu;->a:Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;

    .line 72
    invoke-interface {p5}, Lxu;->a()LCA;

    move-result-object v0

    invoke-interface {v0}, LCA;->a()Ljava/lang/String;

    move-result-object v0

    .line 73
    new-instance v1, LCH;

    iget-object v2, p0, LAu;->a:Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;

    iget-object v3, p0, LAu;->a:LDk;

    invoke-direct {v1, v0, v2, v3}, LCH;-><init>(Ljava/lang/String;Landroid/view/View;LDG;)V

    iput-object v1, p0, LAu;->a:LCJ;

    .line 74
    invoke-interface {p5}, Lxu;->a()Lzs;

    move-result-object v1

    iget-object v2, p0, LAu;->a:LCJ;

    invoke-interface {v1, v0, v2}, Lzs;->a(Ljava/lang/String;LCJ;)V

    .line 75
    invoke-interface {p5}, Lxu;->a()Lzi;

    move-result-object v0

    iput-object v0, p0, LAu;->a:Lzi;

    .line 76
    return-void
.end method


# virtual methods
.method public a(I)D
    .registers 4
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, LAu;->a:Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->getMeasuredHeight()I

    move-result v0

    int-to-double v0, v0

    return-wide v0
.end method

.method public a()I
    .registers 2

    .prologue
    .line 90
    iget-object v0, p0, LAu;->b:LDb;

    invoke-virtual {v0}, LDb;->e()I

    move-result v0

    return v0
.end method

.method public a(IDD)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, LAu;->a:LCJ;

    invoke-interface {v0}, LCJ;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(IIDDLwL;Z)Lwl;
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-virtual/range {p0 .. p8}, LAu;->a(IIDDLwL;Z)Lwo;

    move-result-object v0

    return-object v0
.end method

.method public a(IIDDLwL;Z)Lwo;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, LAu;->a:Lxa;

    const-string v1, ""

    invoke-interface {v0, v1}, Lxa;->a(Ljava/lang/String;)Lwj;

    move-result-object v0

    .line 108
    new-instance v1, Lzb;

    invoke-direct {v1, v0}, Lzb;-><init>(Lwj;)V

    .line 113
    iget-object v2, p0, LAu;->a:LDb;

    iget-object v3, p0, LAu;->a:LDb;

    invoke-virtual {v3}, LDb;->a()I

    move-result v3

    sub-int v3, p1, v3

    invoke-virtual {v2, v3}, LDb;->a(I)V

    .line 115
    iget-object v2, p0, LAu;->a:LCJ;

    invoke-interface {v2}, LCJ;->a()LzU;

    move-result-object v2

    double-to-int v3, p3

    invoke-interface {v2, v3}, LzU;->setWidth(I)V

    .line 116
    iget-object v2, p0, LAu;->a:LCJ;

    invoke-interface {v2}, LCJ;->a()LzU;

    move-result-object v2

    double-to-int v3, p5

    invoke-interface {v2, v3}, LzU;->setHeight(I)V

    .line 118
    iget-object v2, p0, LAu;->a:LDp;

    invoke-virtual {v2}, LDp;->a()V

    .line 119
    iget-object v2, p0, LAu;->a:Lzd;

    invoke-interface {v2, p1}, Lzd;->a(I)I

    move-result v2

    .line 120
    iget-object v3, p0, LAu;->a:LAF;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v3, v0, v1, v2}, LAF;->a(Lwj;Lza;I)V

    .line 121
    iget-object v0, p0, LAu;->a:LDp;

    invoke-virtual {v0}, LDp;->b()V

    .line 123
    iget-object v0, p0, LAu;->a:Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->requestLayout()V

    .line 124
    double-to-int v0, p3

    const/high16 v1, 0x4000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 125
    double-to-int v1, p5

    const/high16 v2, -0x8000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 126
    iget-object v2, p0, LAu;->a:Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->measure(II)V

    .line 128
    iget-object v0, p0, LAu;->a:Lvw;

    new-instance v1, LAv;

    invoke-direct {v1, p0}, LAv;-><init>(LAu;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(Lvw;Lwm;)Lwo;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lxe;
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 134
    iget-object v0, p0, LAu;->a:Lzi;

    invoke-interface {v0, v1, v1, v1, v1}, Lzi;->a(IIII)Lxe;

    move-result-object v0

    .line 135
    if-nez v0, :cond_10

    .line 136
    const-string v1, "CellView"

    const-string v2, "Unable to create a rectangle"

    invoke-static {v1, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :cond_10
    return-object v0
.end method

.method public a(I)V
    .registers 2
    .parameter

    .prologue
    .line 96
    return-void
.end method
