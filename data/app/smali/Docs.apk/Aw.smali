.class public abstract LAw;
.super Ljava/lang/Object;
.source "ContainerElement.java"

# interfaces
.implements LAx;


# instance fields
.field private final a:LAb;

.field protected final a:LDb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDb",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field protected final a:Lxu;

.field protected final a:Lzd;


# direct methods
.method public constructor <init>(LDb;LAb;Lzd;Lxu;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "LAb;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, LAw;->a:LDb;

    .line 49
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0, p0}, LDb;->a(Ljava/lang/Object;)V

    .line 50
    iput-object p2, p0, LAw;->a:LAb;

    .line 51
    iput-object p3, p0, LAw;->a:Lzd;

    .line 52
    iput-object p4, p0, LAw;->a:Lxu;

    .line 53
    return-void
.end method

.method private a(LDb;Lwj;Lza;I)LDb;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "Lwj;",
            "Lza;",
            "I)",
            "LDb",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    invoke-static {p2}, Lzp;->a(Lwj;)Lzp;

    move-result-object v0

    .line 120
    invoke-virtual {p1}, LDb;->c()I

    move-result v1

    .line 121
    invoke-interface {p2}, Lwj;->b()I

    move-result v2

    sub-int/2addr v2, v1

    .line 122
    sget-object v3, Lzp;->d:Lzp;

    if-ne v0, v3, :cond_29

    if-nez v2, :cond_29

    invoke-interface {p2}, Lwj;->d()I

    move-result v3

    invoke-virtual {p1}, LDb;->e()I

    move-result v4

    if-lt v3, v4, :cond_29

    .line 125
    invoke-direct {p0, p1, p3}, LAw;->a(LDb;Lza;)V

    .line 127
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0, v1}, LDb;->a(I)LDd;

    move-result-object v0

    check-cast v0, LDb;

    .line 143
    :goto_28
    return-object v0

    .line 129
    :cond_29
    iget-object v1, p0, LAw;->a:LAb;

    invoke-virtual {p1}, LDb;->c()I

    move-result v3

    iget-object v4, p0, LAw;->a:Lzd;

    iget-object v5, p0, LAw;->a:Lxu;

    invoke-interface {v1, v3, p4, v4, v5}, LAb;->a(IILzd;Lxu;)LAc;

    move-result-object v1

    .line 131
    invoke-interface {v1}, LAc;->a()I

    move-result v3

    .line 132
    sget-object v4, Lzp;->c:Lzp;

    if-ne v0, v4, :cond_61

    invoke-interface {p2}, Lwj;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-lt v0, v3, :cond_61

    const/4 v0, 0x1

    .line 134
    :goto_48
    if-nez v2, :cond_63

    if-nez v0, :cond_58

    invoke-virtual {p1}, LDb;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    invoke-interface {v1, v0}, LAc;->a(LAx;)Z

    move-result v0

    if-nez v0, :cond_63

    .line 136
    :cond_58
    invoke-virtual {p0, p1, v1}, LAw;->a(LDb;LAc;)LAx;

    move-result-object v0

    .line 137
    invoke-interface {v0, p2, p3, v3}, LAx;->a(Lwj;Lza;I)V

    move-object v0, p1

    .line 138
    goto :goto_28

    .line 132
    :cond_61
    const/4 v0, 0x0

    goto :goto_48

    .line 141
    :cond_63
    invoke-virtual {p1}, LDb;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    .line 142
    invoke-interface {v0, p2, p3, v3}, LAx;->a(Lwj;Lza;I)V

    .line 143
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {p1}, LDb;->d()I

    move-result v1

    invoke-virtual {v0, v1}, LDb;->a(I)LDd;

    move-result-object v0

    check-cast v0, LDb;

    goto :goto_28
.end method

.method private a(LDb;Lza;)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "Lza;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149
    invoke-virtual {p1}, LDb;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    .line 150
    if-eqz v0, :cond_18

    .line 151
    invoke-interface {v0, p2}, LAx;->a(Lza;)V

    .line 158
    :goto_b
    invoke-interface {v0}, LAx;->a()LDG;

    move-result-object v0

    invoke-virtual {v0}, LDG;->c()V

    .line 159
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0, p1}, LDb;->a(LDd;)Z

    .line 160
    return-void

    .line 153
    :cond_18
    const-string v1, "ContainerElement"

    const-string v2, "Attempted to delete null child."

    invoke-static {v1, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b
.end method


# virtual methods
.method protected abstract a(I)I
.end method

.method protected a(LAc;)LAx;
    .registers 5
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0}, LDb;->a()LDd;

    move-result-object v0

    check-cast v0, LDb;

    .line 231
    invoke-interface {p1, v0}, LAc;->a(LDb;)LAx;

    move-result-object v0

    .line 232
    invoke-virtual {p0}, LAw;->a()LDA;

    move-result-object v1

    invoke-interface {v0}, LAx;->a()LDG;

    move-result-object v2

    invoke-virtual {v1, v2}, LDA;->a(LDG;)V

    .line 233
    return-object v0
.end method

.method protected a(LDb;LAc;)LAx;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "LAc;",
            ")",
            "LAx;"
        }
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p1}, LDb;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    .line 218
    iget-object v1, p0, LAw;->a:LDb;

    invoke-virtual {v1, p1}, LDb;->a(LDd;)LDd;

    move-result-object v1

    check-cast v1, LDb;

    .line 219
    invoke-interface {p2, v1}, LAc;->a(LDb;)LAx;

    move-result-object v1

    .line 220
    invoke-interface {v0}, LAx;->a()LDG;

    move-result-object v0

    invoke-interface {v1}, LAx;->a()LDG;

    move-result-object v2

    invoke-virtual {v0, v2}, LDG;->b(LDG;)V

    .line 221
    return-object v1
.end method

.method public abstract a()LDA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation
.end method

.method public bridge synthetic a()LDG;
    .registers 2

    .prologue
    .line 24
    invoke-virtual {p0}, LAw;->a()LDA;

    move-result-object v0

    return-object v0
.end method

.method public a(Lwj;Lza;I)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0}, LDb;->b()LDd;

    move-result-object v0

    check-cast v0, LDb;

    .line 70
    :goto_8
    if-eqz v0, :cond_12

    .line 71
    invoke-static {p1}, Lzp;->a(Lwj;)Lzp;

    move-result-object v1

    .line 72
    sget-object v2, Lzp;->a:Lzp;

    if-ne v1, v2, :cond_26

    .line 93
    :cond_12
    :goto_12
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0}, LDb;->g()I

    move-result v0

    if-le v0, p3, :cond_43

    .line 94
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0, p3}, LDb;->a(I)LDd;

    move-result-object v0

    check-cast v0, LDb;

    .line 95
    invoke-direct {p0, v0, p2}, LAw;->a(LDb;Lza;)V

    goto :goto_12

    .line 75
    :cond_26
    invoke-interface {p1}, Lwj;->b()I

    move-result v1

    .line 76
    if-ge v1, p3, :cond_12

    .line 80
    invoke-virtual {v0}, LDb;->d()I

    move-result v2

    if-le v1, v2, :cond_3e

    .line 82
    iget-object v0, p0, LAw;->a:LDb;

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LDb;->a(I)LDd;

    move-result-object v0

    check-cast v0, LDb;

    .line 83
    if-eqz v0, :cond_12

    .line 88
    :cond_3e
    invoke-direct {p0, v0, p1, p2, p3}, LAw;->a(LDb;Lwj;Lza;I)LDb;

    move-result-object v0

    goto :goto_8

    .line 99
    :cond_43
    :goto_43
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0}, LDb;->g()I

    move-result v0

    if-ge v0, p3, :cond_67

    .line 100
    iget-object v0, p0, LAw;->a:LAb;

    iget-object v1, p0, LAw;->a:LDb;

    invoke-virtual {v1}, LDb;->g()I

    move-result v1

    iget-object v2, p0, LAw;->a:Lzd;

    iget-object v3, p0, LAw;->a:Lxu;

    invoke-interface {v0, v1, p3, v2, v3}, LAb;->a(IILzd;Lxu;)LAc;

    move-result-object v0

    .line 102
    invoke-virtual {p0, v0}, LAw;->a(LAc;)LAx;

    move-result-object v1

    .line 103
    invoke-interface {v0}, LAc;->a()I

    move-result v0

    invoke-interface {v1, p1, p2, v0}, LAx;->a(Lwj;Lza;I)V

    goto :goto_43

    .line 106
    :cond_67
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0}, LDb;->e()I

    move-result v0

    if-lez v0, :cond_74

    const/4 v0, 0x1

    :goto_70
    invoke-static {v0}, Lagu;->b(Z)V

    .line 107
    return-void

    .line 106
    :cond_74
    const/4 v0, 0x0

    goto :goto_70
.end method

.method public a(Lza;)V
    .registers 3
    .parameter

    .prologue
    .line 168
    :goto_0
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0}, LDb;->b()LDd;

    move-result-object v0

    check-cast v0, LDb;

    .line 169
    if-nez v0, :cond_b

    .line 174
    return-void

    .line 172
    :cond_b
    invoke-direct {p0, v0, p1}, LAw;->a(LDb;Lza;)V

    goto :goto_0
.end method

.method protected abstract b(I)I
.end method

.method public c(I)I
    .registers 3
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, LAw;->a:LDb;

    invoke-virtual {v0, p1}, LDb;->a(I)LDd;

    move-result-object v0

    check-cast v0, LDb;

    .line 179
    if-eqz v0, :cond_15

    .line 180
    invoke-virtual {v0}, LDb;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    invoke-interface {v0, p1}, LAx;->c(I)I

    move-result v0

    .line 183
    :goto_14
    return v0

    :cond_15
    invoke-virtual {p0, p1}, LAw;->a(I)I

    move-result v0

    goto :goto_14
.end method

.method public d(I)I
    .registers 4
    .parameter

    .prologue
    .line 188
    invoke-virtual {p0}, LAw;->a()LDA;

    move-result-object v0

    invoke-virtual {v0}, LDA;->b()I

    move-result v0

    sub-int v0, p1, v0

    .line 189
    invoke-virtual {p0}, LAw;->a()LDA;

    move-result-object v1

    invoke-virtual {v1, v0}, LDA;->a(I)LDG;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_25

    invoke-virtual {v0}, LDG;->a()Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p0, :cond_25

    .line 191
    invoke-virtual {v0}, LDG;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LAx;

    invoke-interface {v0, p1}, LAx;->d(I)I

    move-result v0

    .line 193
    :goto_24
    return v0

    :cond_25
    invoke-virtual {p0, p1}, LAw;->b(I)I

    move-result v0

    goto :goto_24
.end method
