.class public LAy;
.super LAM;
.source "FootnoteNumber.java"


# instance fields
.field private a:LCj;

.field private final a:Lxu;

.field private a:LzI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LzI",
            "<",
            "LCj;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lzd;


# direct methods
.method public constructor <init>(LDb;Lzd;Lxu;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1}, LAM;-><init>(LDb;)V

    .line 39
    const-string v0, "FootnoteNumber"

    const-string v1, "Creating FootnoteNumber"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    iput-object p2, p0, LAy;->a:Lzd;

    .line 41
    iput-object p3, p0, LAy;->a:Lxu;

    .line 42
    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 54
    iget-object v0, p0, LAy;->a:Lzd;

    iget-object v1, p0, LAy;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    invoke-interface {v0, v1}, Lzd;->b(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a()V
    .registers 6

    .prologue
    .line 59
    iget-object v0, p0, LAy;->a:Lzd;

    iget-object v1, p0, LAy;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    invoke-interface {v0, v1}, Lzd;->a(I)Lxg;

    move-result-object v1

    .line 60
    const/4 v0, 0x0

    .line 61
    iget-object v2, p0, LAy;->a:Lxu;

    invoke-interface {v2}, Lxu;->a()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 62
    iget-object v0, p0, LAy;->a:Lzd;

    iget-object v2, p0, LAy;->a:LDb;

    invoke-virtual {v2}, LDb;->c()I

    move-result v2

    invoke-interface {v0, v2}, Lzd;->a(I)Lvo;

    move-result-object v0

    .line 64
    :cond_21
    new-instance v2, LCj;

    iget-object v3, p0, LAy;->a:Lxu;

    invoke-direct {v2, v1, v0, v3}, LCj;-><init>(Lxg;Lvo;Lxu;)V

    .line 66
    iget-object v0, p0, LAy;->a:LCj;

    invoke-virtual {v2, v0}, LCj;->a(LCt;)Z

    move-result v0

    if-nez v0, :cond_45

    .line 67
    iput-object v2, p0, LAy;->a:LCj;

    .line 68
    iget-object v0, p0, LAy;->a:LzI;

    if-eqz v0, :cond_3d

    .line 70
    iget-object v0, p0, LAy;->a:LzI;

    iget-object v1, p0, LAy;->a:LDI;

    invoke-interface {v0, v1}, LzI;->a(Landroid/text/Spannable;)V

    .line 72
    :cond_3d
    iget-object v0, p0, LAy;->a:LCj;

    invoke-virtual {v0}, LCj;->a()LzI;

    move-result-object v0

    iput-object v0, p0, LAy;->a:LzI;

    .line 74
    :cond_45
    iget-object v0, p0, LAy;->a:LDI;

    invoke-virtual {v0}, LDI;->length()I

    move-result v0

    .line 75
    iget-object v1, p0, LAy;->a:LzI;

    iget-object v2, p0, LAy;->a:LDI;

    const/4 v3, 0x0

    iget-object v4, p0, LAy;->a:Lxu;

    invoke-interface {v4}, Lxu;->a()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v1, v2, v3, v0, v4}, LzI;->a(Landroid/text/Spannable;IILandroid/content/Context;)V

    .line 76
    return-void
.end method

.method public a(IILjava/util/List;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, LAy;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    if-ge p1, v0, :cond_10

    iget-object v0, p0, LAy;->a:LDI;

    invoke-virtual {v0}, LDI;->b()I

    move-result v0

    if-lt p2, v0, :cond_20

    :cond_10
    iget-object v0, p0, LAy;->a:LDI;

    invoke-virtual {v0}, LDI;->c()I

    move-result v0

    if-lt p1, v0, :cond_21

    iget-object v0, p0, LAy;->a:LDI;

    invoke-virtual {v0}, LDI;->c()I

    move-result v0

    if-lt p2, v0, :cond_21

    .line 102
    :cond_20
    :goto_20
    return-void

    .line 93
    :cond_21
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, LAy;->a:LDI;

    invoke-virtual {v1}, LDI;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 94
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, LAy;->a:LDI;

    invoke-virtual {v2}, LDI;->c()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 95
    if-ge v0, v1, :cond_54

    .line 97
    new-instance v0, LKj;

    iget-object v1, p0, LAy;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    iget-object v2, p0, LAy;->a:LDb;

    invoke-virtual {v2}, LDb;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, LKj;-><init>(II)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_20

    .line 100
    :cond_54
    new-instance v0, LKj;

    iget-object v1, p0, LAy;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    iget-object v2, p0, LAy;->a:LDb;

    invoke-virtual {v2}, LDb;->c()I

    move-result v2

    invoke-direct {v0, v1, v2}, LKj;-><init>(II)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_20
.end method

.method public a(Lza;)V
    .registers 5
    .parameter

    .prologue
    .line 46
    invoke-super {p0, p1}, LAM;->a(Lza;)V

    .line 47
    const-string v0, "FootnoteNumber"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deleting FootnoteNumber at spacer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LAy;->a:LDI;

    invoke-virtual {v2}, LDI;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v0, p0, LAy;->a:LzI;

    iget-object v1, p0, LAy;->a:LDI;

    invoke-interface {v0, v1}, LzI;->a(Landroid/text/Spannable;)V

    .line 49
    iget-object v0, p0, LAy;->a:LDI;

    const/4 v1, 0x0

    iget-object v2, p0, LAy;->a:LDI;

    invoke-virtual {v2}, LDI;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LDI;->a(II)LDI;

    .line 50
    return-void
.end method

.method protected b()V
    .registers 5

    .prologue
    .line 81
    iget-object v0, p0, LAy;->a:LDI;

    const/4 v1, 0x0

    iget-object v2, p0, LAy;->a:LDI;

    invoke-virtual {v2}, LDI;->length()I

    move-result v2

    invoke-virtual {p0}, LAy;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LDI;->a(IILjava/lang/CharSequence;)LDI;

    .line 83
    invoke-virtual {p0}, LAy;->a()V

    .line 84
    return-void
.end method
