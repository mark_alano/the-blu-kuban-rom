.class public final enum LBG;
.super Ljava/lang/Enum;
.source "MenuHandler.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LBG;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LBG;

.field private static final synthetic a:[LBG;

.field public static final enum b:LBG;

.field public static final enum c:LBG;

.field public static final enum d:LBG;

.field public static final enum e:LBG;

.field public static final enum f:LBG;

.field public static final enum g:LBG;


# instance fields
.field private final a:I

.field private final a:LCi;

.field private final a:Ljava/lang/Object;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 14

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v2, 0x0

    const/4 v10, 0x0

    .line 99
    new-instance v0, LBG;

    const-string v1, "BOLD"

    sget-object v3, LCi;->a:LCi;

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget v5, LsD;->toolbar_bold_button:I

    sget v6, LsC;->action_bold:I

    invoke-direct/range {v0 .. v6}, LBG;-><init>(Ljava/lang/String;ILCi;Ljava/lang/Object;II)V

    sput-object v0, LBG;->a:LBG;

    .line 101
    new-instance v3, LBG;

    const-string v4, "ITALIC"

    sget-object v6, LCi;->b:LCi;

    sget-object v7, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget v8, LsD;->toolbar_italic_button:I

    sget v9, LsC;->action_italic:I

    move v5, v11

    invoke-direct/range {v3 .. v9}, LBG;-><init>(Ljava/lang/String;ILCi;Ljava/lang/Object;II)V

    sput-object v3, LBG;->b:LBG;

    .line 103
    new-instance v3, LBG;

    const-string v4, "UNDERLINE"

    sget-object v6, LCi;->c:LCi;

    sget-object v7, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    sget v8, LsD;->toolbar_underline_button:I

    sget v9, LsC;->action_underline:I

    move v5, v12

    invoke-direct/range {v3 .. v9}, LBG;-><init>(Ljava/lang/String;ILCi;Ljava/lang/Object;II)V

    sput-object v3, LBG;->c:LBG;

    .line 105
    new-instance v3, LBG;

    const-string v4, "BULLETED_LIST"

    sget-object v6, LCi;->f:LCi;

    sget-object v7, Lxr;->b:Lxr;

    sget v8, LsD;->toolbar_bulleted_list_button:I

    sget v9, LsC;->action_bulleted_list:I

    move v5, v13

    invoke-direct/range {v3 .. v9}, LBG;-><init>(Ljava/lang/String;ILCi;Ljava/lang/Object;II)V

    sput-object v3, LBG;->d:LBG;

    .line 107
    new-instance v3, LBG;

    const-string v4, "NUMBERED_LIST"

    const/4 v5, 0x4

    sget-object v6, LCi;->f:LCi;

    sget-object v7, Lxr;->c:Lxr;

    sget v8, LsD;->toolbar_numbered_list_button:I

    sget v9, LsC;->action_numbered_list:I

    invoke-direct/range {v3 .. v9}, LBG;-><init>(Ljava/lang/String;ILCi;Ljava/lang/Object;II)V

    sput-object v3, LBG;->e:LBG;

    .line 109
    new-instance v3, LBG;

    const-string v4, "COLOR_POPUP"

    const/4 v5, 0x5

    sget v8, LsD;->toolbar_color_button:I

    sget v9, LsC;->action_colour:I

    move-object v6, v10

    move-object v7, v10

    invoke-direct/range {v3 .. v9}, LBG;-><init>(Ljava/lang/String;ILCi;Ljava/lang/Object;II)V

    sput-object v3, LBG;->f:LBG;

    .line 110
    new-instance v3, LBG;

    const-string v4, "ALIGNMENT_POPUP"

    const/4 v5, 0x6

    sget v8, LsD;->toolbar_alignment_button:I

    sget v9, LsC;->action_text_align_left:I

    move-object v6, v10

    move-object v7, v10

    invoke-direct/range {v3 .. v9}, LBG;-><init>(Ljava/lang/String;ILCi;Ljava/lang/Object;II)V

    sput-object v3, LBG;->g:LBG;

    .line 98
    const/4 v0, 0x7

    new-array v0, v0, [LBG;

    sget-object v1, LBG;->a:LBG;

    aput-object v1, v0, v2

    sget-object v1, LBG;->b:LBG;

    aput-object v1, v0, v11

    sget-object v1, LBG;->c:LBG;

    aput-object v1, v0, v12

    sget-object v1, LBG;->d:LBG;

    aput-object v1, v0, v13

    const/4 v1, 0x4

    sget-object v2, LBG;->e:LBG;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LBG;->f:LBG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LBG;->g:LBG;

    aput-object v2, v0, v1

    sput-object v0, LBG;->a:[LBG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILCi;Ljava/lang/Object;II)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LCi;",
            "Ljava/lang/Object;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 122
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 123
    iput-object p3, p0, LBG;->a:LCi;

    .line 124
    iput-object p4, p0, LBG;->a:Ljava/lang/Object;

    .line 125
    iput p5, p0, LBG;->a:I

    .line 126
    iput p6, p0, LBG;->b:I

    .line 127
    return-void
.end method

.method public static synthetic a(LBG;)I
    .registers 2
    .parameter

    .prologue
    .line 98
    iget v0, p0, LBG;->a:I

    return v0
.end method

.method public static synthetic b(LBG;)I
    .registers 2
    .parameter

    .prologue
    .line 98
    iget v0, p0, LBG;->b:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)LBG;
    .registers 2
    .parameter

    .prologue
    .line 98
    const-class v0, LBG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LBG;

    return-object v0
.end method

.method public static values()[LBG;
    .registers 1

    .prologue
    .line 98
    sget-object v0, LBG;->a:[LBG;

    invoke-virtual {v0}, [LBG;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LBG;

    return-object v0
.end method


# virtual methods
.method public a(LCh;)Z
    .registers 4
    .parameter

    .prologue
    .line 130
    iget-object v0, p0, LBG;->a:LCi;

    invoke-virtual {p1, v0}, LCh;->a(LCi;)Ljava/lang/Object;

    move-result-object v0

    .line 131
    if-eqz v0, :cond_e

    iget-object v1, p0, LBG;->a:Ljava/lang/Object;

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method
