.class abstract enum LBR;
.super Ljava/lang/Enum;
.source "VerticalPositioning.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LBR;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LBR;

.field private static final synthetic a:[LBR;

.field public static final enum b:LBR;

.field public static final enum c:LBR;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, LBS;

    const-string v1, "ABOVE_FIRST_LINE"

    invoke-direct {v0, v1, v2}, LBS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBR;->a:LBR;

    .line 20
    new-instance v0, LBT;

    const-string v1, "BELOW_LAST_LINE"

    invoke-direct {v0, v1, v3}, LBT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBR;->b:LBR;

    .line 27
    new-instance v0, LBU;

    const-string v1, "EDITOR_CENTER"

    invoke-direct {v0, v1, v4}, LBU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LBR;->c:LBR;

    .line 12
    const/4 v0, 0x3

    new-array v0, v0, [LBR;

    sget-object v1, LBR;->a:LBR;

    aput-object v1, v0, v2

    sget-object v1, LBR;->b:LBR;

    aput-object v1, v0, v3

    sget-object v1, LBR;->c:LBR;

    aput-object v1, v0, v4

    sput-object v0, LBR;->a:[LBR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILBS;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, LBR;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LBR;
    .registers 2
    .parameter

    .prologue
    .line 12
    const-class v0, LBR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LBR;

    return-object v0
.end method

.method public static values()[LBR;
    .registers 1

    .prologue
    .line 12
    sget-object v0, LBR;->a:[LBR;

    invoke-virtual {v0}, [LBR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LBR;

    return-object v0
.end method


# virtual methods
.method abstract a(LBH;LKj;II)I
.end method
