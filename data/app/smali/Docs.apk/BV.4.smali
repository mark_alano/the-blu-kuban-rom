.class public LBV;
.super Landroid/text/style/CharacterStyle;
.source "DocosActiveSpan.java"


# instance fields
.field private final a:Ljava/lang/String;

.field private final a:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/text/style/CharacterStyle;-><init>()V

    .line 21
    iput-object p1, p0, LBV;->a:Ljava/lang/String;

    .line 22
    iput-boolean p2, p0, LBV;->a:Z

    .line 23
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 26
    iget-object v0, p0, LBV;->a:Ljava/lang/String;

    return-object v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 4
    .parameter

    .prologue
    .line 31
    iget v1, p1, Landroid/text/TextPaint;->bgColor:I

    iget-boolean v0, p0, LBV;->a:Z

    if-eqz v0, :cond_f

    const/high16 v0, -0x1

    :goto_8
    invoke-static {v1, v0}, LJN;->a(II)I

    move-result v0

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    .line 32
    return-void

    .line 31
    :cond_f
    const v0, -0xff0100

    goto :goto_8
.end method
