.class public LBZ;
.super LGH;
.source "KixTextFormatingSpan.java"


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final a:Lxu;


# direct methods
.method public constructor <init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;LGI;Lxu;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v0, p0

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LGH;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;LGI;)V

    .line 32
    iput-object p7, p0, LBZ;->a:Lxu;

    .line 33
    iput-object p1, p0, LBZ;->a:Ljava/lang/String;

    .line 34
    iput p2, p0, LBZ;->a:I

    .line 35
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 39
    iget v0, p0, LBZ;->a:I

    return v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 44
    iget-object v0, p0, LBZ;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .registers 3

    .prologue
    .line 49
    invoke-super {p0}, LGH;->b()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, LBZ;->a:Lxu;

    invoke-interface {v1}, Lxu;->a()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .registers 5
    .parameter

    .prologue
    .line 54
    invoke-super {p0, p1}, LGH;->updateMeasureState(Landroid/text/TextPaint;)V

    .line 56
    invoke-virtual {p0}, LBZ;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_f

    invoke-virtual {p0}, LBZ;->a()I

    move-result v0

    if-eqz v0, :cond_24

    .line 57
    :cond_f
    iget-object v0, p0, LBZ;->a:Lxu;

    invoke-interface {v0}, Lxu;->a()LxI;

    move-result-object v0

    invoke-virtual {p0}, LBZ;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LBZ;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, LxI;->a(Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 60
    :cond_24
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v0

    iget-object v1, p0, LBZ;->a:Lxu;

    invoke-interface {v1}, Lxu;->a()F

    move-result v1

    mul-float/2addr v0, v1

    .line 61
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 62
    return-void
.end method
