.class public LBd;
.super Ljava/lang/Object;
.source "ImageCache.java"

# interfaces
.implements LAY;


# static fields
.field private static final a:J


# instance fields
.field private a:I

.field a:LNj;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LZS;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:Landroid/app/Application;
    .annotation runtime Laon;
    .end annotation
.end field

.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LBg;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LBf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    .line 38
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    const-wide/16 v2, 0x4

    div-long/2addr v0, v2

    const-wide/32 v2, 0x1000000

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sput-wide v0, LBd;->a:J

    return-void
.end method

.method public constructor <init>()V
    .registers 5

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LBd;->a:Ljava/util/Map;

    .line 93
    const/4 v0, 0x0

    iput v0, p0, LBd;->a:I

    .line 96
    new-instance v0, LBe;

    const/16 v1, 0x10

    const/high16 v2, 0x3f40

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, LBe;-><init>(LBd;IFZ)V

    iput-object v0, p0, LBd;->b:Ljava/util/Map;

    .line 107
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LBd;->c:Ljava/util/Map;

    return-void
.end method

.method private a()I
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 259
    .line 260
    iget-object v0, p0, LBd;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 261
    if-eqz v0, :cond_3d

    .line 262
    array-length v0, v0

    add-int/2addr v0, v1

    :goto_1c
    move v1, v0

    goto :goto_c

    .line 266
    :cond_1e
    iget-object v0, p0, LBd;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_28
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBf;

    .line 267
    invoke-virtual {v0}, LBf;->a()I

    move-result v0

    add-int/2addr v2, v0

    goto :goto_28

    .line 269
    :cond_3a
    add-int v0, v1, v2

    return v0

    :cond_3d
    move v0, v1

    goto :goto_1c
.end method

.method static synthetic a(LBd;)I
    .registers 2
    .parameter

    .prologue
    .line 34
    iget v0, p0, LBd;->a:I

    return v0
.end method

.method static synthetic a()J
    .registers 2

    .prologue
    .line 34
    sget-wide v0, LBd;->a:J

    return-wide v0
.end method

.method private a(Ljava/lang/String;)LBg;
    .registers 4
    .parameter

    .prologue
    .line 321
    iget-object v0, p0, LBd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBg;

    .line 322
    if-nez v0, :cond_15

    .line 323
    new-instance v0, LBg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LBg;-><init>(LBd;LBe;)V

    .line 324
    iget-object v1, p0, LBd;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    :cond_15
    return-object v0
.end method

.method private a(Ljava/lang/String;II)Landroid/graphics/drawable/BitmapDrawable;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, LBd;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBf;

    .line 129
    if-eqz v0, :cond_28

    .line 130
    invoke-virtual {v0}, LBf;->a()Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    .line 131
    invoke-static {v0}, LBf;->a(LBf;)I

    move-result v2

    if-ne v2, p2, :cond_1c

    invoke-static {v0}, LBf;->b(LBf;)I

    move-result v0

    if-ne v0, p3, :cond_1c

    move-object v0, v1

    .line 139
    :goto_1b
    return-object v0

    .line 135
    :cond_1c
    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 136
    iget-object v0, p0, LBd;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :cond_28
    const/4 v0, 0x0

    goto :goto_1b
.end method

.method private a(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 183
    iget-object v0, p0, LBd;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 184
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 185
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBf;

    .line 186
    invoke-static {v0}, LBf;->c(LBf;)I

    move-result v2

    iget v3, p0, LBd;->a:I

    if-ge v2, v3, :cond_b

    invoke-static {v0}, LBf;->a(LBf;)Z

    move-result v2

    if-ne p1, v2, :cond_b

    .line 187
    invoke-static {v0}, LBf;->a(LBf;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 188
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_b

    .line 191
    :cond_34
    iget v0, p0, LBd;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LBd;->a:I

    .line 194
    iget-object v0, p0, LBd;->b:Ljava/util/Map;

    invoke-interface {v0, v4, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, LBd;->b:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    return-void
.end method

.method static synthetic b(LBd;)I
    .registers 2
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, LBd;->a()I

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;II)Landroid/graphics/drawable/BitmapDrawable;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 144
    iget-object v0, p0, LBd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBg;

    iget-boolean v5, v0, LBg;->a:Z

    .line 145
    iget-object v0, p0, LBd;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 146
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 147
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 149
    array-length v2, v0

    invoke-static {v0, v6, v2, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 150
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v2, v2

    int-to-float v3, p2

    div-float/2addr v2, v3

    .line 151
    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v3, v3

    int-to-float v4, p3

    div-float/2addr v3, v4

    .line 152
    iput-boolean v6, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 155
    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 156
    array-length v2, v0

    invoke-static {v0, v6, v2, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 157
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, LBd;->a:Landroid/app/Application;

    invoke-virtual {v1}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 158
    if-nez v0, :cond_6e

    .line 160
    const-string v0, "ImageCache"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Null image (bitmap) in url = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    iget-object v0, p0, LBd;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v0, p0, LBd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    const/4 v2, 0x0

    .line 168
    :goto_6d
    return-object v2

    .line 165
    :cond_6e
    iget-object v6, p0, LBd;->c:Ljava/util/Map;

    new-instance v0, LBf;

    move-object v1, p0

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, LBf;-><init>(LBd;Landroid/graphics/drawable/BitmapDrawable;IIZ)V

    invoke-interface {v6, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6d
.end method


# virtual methods
.method a(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 115
    invoke-direct {p0, p1, p2, p3}, LBd;->a(Ljava/lang/String;II)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_7

    .line 124
    :goto_6
    return-object v0

    .line 120
    :cond_7
    iget-object v0, p0, LBd;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 122
    invoke-direct {p0, p1, p2, p3}, LBd;->b(Ljava/lang/String;II)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    goto :goto_6

    .line 124
    :cond_14
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public a()V
    .registers 2

    .prologue
    .line 173
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LBd;->a(Z)V

    .line 174
    return-void
.end method

.method a(Ljava/lang/String;LBh;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 301
    invoke-direct {p0, p1}, LBd;->a(Ljava/lang/String;)LBg;

    move-result-object v0

    .line 302
    iget-object v0, v0, LBg;->a:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 303
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 277
    iget-object v0, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_5

    .line 298
    :cond_4
    :goto_4
    return-void

    .line 280
    :cond_5
    if-eqz p1, :cond_11

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2a

    .line 281
    :cond_11
    const-string v0, "ImageCache"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid image URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 284
    :cond_2a
    iget-object v0, p0, LBd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBg;

    .line 285
    if-nez v0, :cond_3c

    .line 286
    const-string v0, "ImageCache"

    const-string v1, "fetchData attempted for image which was never requested."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 290
    :cond_3c
    iget-boolean v1, v0, LBg;->b:Z

    if-nez v1, :cond_4

    iget v1, v0, LBg;->a:I

    if-nez v1, :cond_4

    .line 291
    const/4 v1, 0x1

    iput-boolean v1, v0, LBg;->b:Z

    .line 292
    iget v1, v0, LBg;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LBg;->a:I

    .line 293
    new-instance v0, LAX;

    invoke-direct {v0}, LAX;-><init>()V

    invoke-virtual {v0, p1}, LAX;->a(Ljava/lang/String;)LAX;

    move-result-object v0

    invoke-virtual {v0, p0}, LAX;->a(LBd;)LAX;

    move-result-object v0

    iget-object v1, p0, LBd;->a:LNj;

    invoke-virtual {v0, v1}, LAX;->a(LNj;)LAX;

    move-result-object v0

    invoke-virtual {v0, p2}, LAX;->b(Ljava/lang/String;)LAX;

    move-result-object v0

    iget-object v1, p0, LBd;->a:LZS;

    invoke-virtual {v0, v1}, LAX;->a(LZS;)LAX;

    move-result-object v0

    invoke-virtual {v0}, LAX;->a()LAV;

    move-result-object v0

    .line 296
    iget-object v1, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_4
.end method

.method a(Ljava/lang/String;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 306
    invoke-direct {p0, p1}, LBd;->a(Ljava/lang/String;)LBg;

    move-result-object v0

    .line 307
    iput-boolean p2, v0, LBg;->a:Z

    .line 308
    return-void
.end method

.method a(Ljava/lang/String;[B)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 231
    iget-object v0, p0, LBd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBg;

    .line 232
    if-nez v0, :cond_13

    .line 233
    const-string v0, "ImageCache"

    const-string v1, "Received image which was not requested."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :goto_12
    return-void

    .line 236
    :cond_13
    iput-boolean v2, v0, LBg;->b:Z

    .line 237
    if-eqz p2, :cond_1e

    .line 238
    iget-object v1, p0, LBd;->b:Ljava/util/Map;

    invoke-interface {v1, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iput v2, v0, LBg;->a:I

    .line 244
    :cond_1e
    iget-object v1, v0, LBg;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_24
    :goto_24
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_36

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LBh;

    .line 245
    if-eqz v1, :cond_24

    .line 246
    invoke-interface {v1, p1}, LBh;->a(Ljava/lang/String;)V

    goto :goto_24

    .line 249
    :cond_36
    iget-object v0, v0, LBg;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_12
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 227
    iget-object v0, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_e

    iget-object v0, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 311
    invoke-direct {p0, p1}, LBd;->a(Ljava/lang/String;)LBg;

    move-result-object v0

    .line 312
    iget-boolean v0, v0, LBg;->b:Z

    return v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 178
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LBd;->a(Z)V

    .line 179
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 204
    iget-object v0, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_c

    .line 205
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    .line 209
    :goto_b
    return-void

    .line 207
    :cond_c
    const-string v0, "ImageCache"

    const-string v1, "Background loader was already running."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b
.end method

.method public d()V
    .registers 2

    .prologue
    .line 217
    iget-object v0, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_c

    .line 218
    iget-object v0, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 219
    const/4 v0, 0x0

    iput-object v0, p0, LBd;->a:Ljava/util/concurrent/ExecutorService;

    .line 221
    :cond_c
    return-void
.end method
