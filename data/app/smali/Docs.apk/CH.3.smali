.class public LCH;
.super Ljava/lang/Object;
.source "NativeDummyRenderer.java"

# interfaces
.implements LCJ;


# instance fields
.field private final a:Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/view/View;LDG;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            "LDG",
            "<",
            "LzU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, LCH;->a:Ljava/lang/String;

    .line 29
    new-instance v0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;-><init>(Landroid/content/Context;LDG;)V

    iput-object v0, p0, LCH;->a:Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;

    .line 30
    iget-object v0, p0, LCH;->a:Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->addView(Landroid/view/View;)V

    .line 31
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, LCH;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()LzU;
    .registers 2

    .prologue
    .line 40
    iget-object v0, p0, LCH;->a:Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;

    return-object v0
.end method
