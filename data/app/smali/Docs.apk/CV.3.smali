.class LCV;
.super Ljava/lang/Object;
.source "XmlHttpRequestRelay.java"

# interfaces
.implements LKp;


# instance fields
.field final synthetic a:I

.field final synthetic a:LCQ;


# direct methods
.method constructor <init>(LCQ;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 694
    iput-object p1, p0, LCV;->a:LCQ;

    iput p2, p0, LCV;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 5

    .prologue
    .line 697
    iget-object v1, p0, LCV;->a:LCQ;

    monitor-enter v1

    .line 698
    :try_start_3
    iget-object v0, p0, LCV;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LCW;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 699
    iget v0, p0, LCV;->a:I

    invoke-static {v0}, LCQ;->a(I)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 700
    iget-object v0, p0, LCV;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LCW;

    move-result-object v0

    iget-object v2, p0, LCV;->a:LCQ;

    invoke-static {v2}, LCQ;->a(LCQ;)LCY;

    move-result-object v2

    invoke-virtual {v2}, LCY;->a()I

    move-result v2

    iget v3, p0, LCV;->a:I

    invoke-interface {v0, v2, v3}, LCW;->onRequestFailed(II)V

    .line 705
    :cond_28
    :goto_28
    iget-object v0, p0, LCV;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 706
    monitor-exit v1

    .line 707
    return-void

    .line 702
    :cond_33
    iget-object v0, p0, LCV;->a:LCQ;

    invoke-static {v0}, LCQ;->a(LCQ;)LCW;

    move-result-object v0

    iget-object v2, p0, LCV;->a:LCQ;

    invoke-static {v2}, LCQ;->a(LCQ;)LCY;

    move-result-object v2

    invoke-virtual {v2}, LCY;->a()I

    move-result v2

    invoke-interface {v0, v2}, LCW;->onRequestCompleted(I)V

    goto :goto_28

    .line 706
    :catchall_47
    move-exception v0

    monitor-exit v1
    :try_end_49
    .catchall {:try_start_3 .. :try_end_49} :catchall_47

    throw v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 711
    const/4 v0, 0x1

    return v0
.end method
