.class public final enum LCi;
.super Ljava/lang/Enum;
.source "AggregateStyle.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LCi;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LCi;

.field private static final synthetic a:[LCi;

.field public static final enum b:LCi;

.field public static final enum c:LCi;

.field public static final enum d:LCi;

.field public static final enum e:LCi;

.field public static final enum f:LCi;

.field public static final enum g:LCi;

.field public static final enum h:LCi;

.field public static final enum i:LCi;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, LCi;

    const-string v1, "BOLD"

    invoke-direct {v0, v1, v3}, LCi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCi;->a:LCi;

    .line 21
    new-instance v0, LCi;

    const-string v1, "ITALIC"

    invoke-direct {v0, v1, v4}, LCi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCi;->b:LCi;

    .line 22
    new-instance v0, LCi;

    const-string v1, "UNDERLINE"

    invoke-direct {v0, v1, v5}, LCi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCi;->c:LCi;

    .line 23
    new-instance v0, LCi;

    const-string v1, "BACKGROUND"

    invoke-direct {v0, v1, v6}, LCi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCi;->d:LCi;

    .line 24
    new-instance v0, LCi;

    const-string v1, "FOREGROUND"

    invoke-direct {v0, v1, v7}, LCi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCi;->e:LCi;

    .line 25
    new-instance v0, LCi;

    const-string v1, "BULLET_TYPE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LCi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCi;->f:LCi;

    .line 26
    new-instance v0, LCi;

    const-string v1, "ALIGNMENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LCi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCi;->g:LCi;

    .line 27
    new-instance v0, LCi;

    const-string v1, "FONT_FAMILY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LCi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCi;->h:LCi;

    .line 28
    new-instance v0, LCi;

    const-string v1, "FONT_SIZE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LCi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LCi;->i:LCi;

    .line 19
    const/16 v0, 0x9

    new-array v0, v0, [LCi;

    sget-object v1, LCi;->a:LCi;

    aput-object v1, v0, v3

    sget-object v1, LCi;->b:LCi;

    aput-object v1, v0, v4

    sget-object v1, LCi;->c:LCi;

    aput-object v1, v0, v5

    sget-object v1, LCi;->d:LCi;

    aput-object v1, v0, v6

    sget-object v1, LCi;->e:LCi;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LCi;->f:LCi;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LCi;->g:LCi;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LCi;->h:LCi;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LCi;->i:LCi;

    aput-object v2, v0, v1

    sput-object v0, LCi;->a:[LCi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LCi;
    .registers 2
    .parameter

    .prologue
    .line 19
    const-class v0, LCi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LCi;

    return-object v0
.end method

.method public static values()[LCi;
    .registers 1

    .prologue
    .line 19
    sget-object v0, LCi;->a:[LCi;

    invoke-virtual {v0}, [LCi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LCi;

    return-object v0
.end method
