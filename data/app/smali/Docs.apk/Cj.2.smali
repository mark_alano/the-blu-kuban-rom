.class public LCj;
.super Ljava/lang/Object;
.source "FootnoteNumberStyle.java"

# interfaces
.implements LCt;


# instance fields
.field private final a:LCw;

.field private final a:Ljava/lang/String;

.field private final a:Lxu;

.field private final a:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lxg;Lvo;Lxu;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, LCw;

    invoke-direct {v0, p1, p2, p3}, LCw;-><init>(Lxg;Lvo;Lxu;)V

    iput-object v0, p0, LCj;->a:LCw;

    .line 35
    if-eqz p2, :cond_1b

    invoke-interface {p2}, Lvo;->a()[Ljava/lang/String;

    move-result-object v0

    :goto_10
    iput-object v0, p0, LCj;->a:[Ljava/lang/String;

    .line 36
    invoke-interface {p1}, Lxg;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LCj;->a:Ljava/lang/String;

    .line 37
    iput-object p3, p0, LCj;->a:Lxu;

    .line 38
    return-void

    .line 35
    :cond_1b
    const/4 v0, 0x0

    goto :goto_10
.end method

.method static synthetic a(LCj;)LCw;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, LCj;->a:LCw;

    return-object v0
.end method


# virtual methods
.method public a()LzI;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LzI",
            "<",
            "LCj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, LCk;

    iget-object v1, p0, LCj;->a:Lxu;

    invoke-interface {v1}, Lxu;->a()Lth;

    move-result-object v1

    iget-object v2, p0, LCj;->a:[Ljava/lang/String;

    iget-object v3, p0, LCj;->a:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, LCk;-><init>(LCj;Lth;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(LCt;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 42
    instance-of v1, p1, LCj;

    if-eqz v1, :cond_13

    .line 43
    check-cast p1, LCj;

    .line 44
    iget-object v1, p0, LCj;->a:LCw;

    if-eqz v1, :cond_14

    iget-object v0, p0, LCj;->a:LCw;

    iget-object v1, p1, LCj;->a:LCw;

    invoke-virtual {v0, v1}, LCw;->a(LCt;)Z

    move-result v0

    .line 48
    :cond_13
    :goto_13
    return v0

    .line 44
    :cond_14
    iget-object v1, p1, LCj;->a:LCw;

    if-nez v1, :cond_13

    const/4 v0, 0x1

    goto :goto_13
.end method
