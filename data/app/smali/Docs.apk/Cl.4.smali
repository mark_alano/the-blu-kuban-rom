.class public LCl;
.super Ljava/lang/Object;
.source "ImageStyle.java"

# interfaces
.implements LCt;


# instance fields
.field private final a:F

.field private final a:LAS;

.field private final a:Ljava/lang/String;

.field private final a:Lxu;

.field private final b:F


# direct methods
.method public constructor <init>(LAS;Lvu;Lxu;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, LCl;->a:LAS;

    .line 42
    invoke-interface {p2}, Lvu;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LCl;->a:Ljava/lang/String;

    .line 43
    invoke-interface {p2}, Lvu;->a()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LCl;->a:F

    .line 44
    invoke-interface {p2}, Lvu;->b()D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, LCl;->b:F

    .line 45
    iput-object p3, p0, LCl;->a:Lxu;

    .line 46
    return-void
.end method

.method static synthetic a(LCl;)F
    .registers 2
    .parameter

    .prologue
    .line 24
    iget v0, p0, LCl;->a:F

    return v0
.end method

.method static synthetic a(LCl;)LAS;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, LCl;->a:LAS;

    return-object v0
.end method

.method static synthetic a(LCl;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, LCl;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LCl;)Lxu;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, LCl;->a:Lxu;

    return-object v0
.end method

.method static synthetic b(LCl;)F
    .registers 2
    .parameter

    .prologue
    .line 24
    iget v0, p0, LCl;->b:F

    return v0
.end method


# virtual methods
.method public a()LzI;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LzI",
            "<",
            "LCl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    new-instance v0, LCm;

    invoke-direct {v0, p0}, LCm;-><init>(LCl;)V

    return-object v0
.end method

.method public a(LCt;)Z
    .registers 3
    .parameter

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method
