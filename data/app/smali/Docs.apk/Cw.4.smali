.class public LCw;
.super Ljava/lang/Object;
.source "TextStyle.java"

# interfaces
.implements LCt;


# static fields
.field private static final a:[[I


# instance fields
.field private final a:I

.field private a:LGI;

.field private a:Landroid/content/res/ColorStateList;

.field private final a:Landroid/text/Editable;

.field private a:Ljava/lang/String;

.field private final a:Lxu;

.field private a:Z

.field private final a:[Ljava/lang/String;

.field private b:I

.field private b:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/String;

.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    new-array v0, v1, [[I

    new-array v1, v1, [I

    aput v2, v1, v2

    aput-object v1, v0, v2

    sput-object v0, LCw;->a:[[I

    return-void
.end method

.method public constructor <init>(Lxg;Lvo;Lxu;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 84
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LCw;-><init>(Lxg;Lvo;Lxu;ZLandroid/text/Editable;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Lxg;Lvo;Lxu;ZLandroid/text/Editable;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput v2, p0, LCw;->b:I

    .line 60
    iput-object v3, p0, LCw;->a:Ljava/lang/String;

    .line 61
    iput-object v3, p0, LCw;->b:Ljava/lang/String;

    .line 63
    iput-boolean v2, p0, LCw;->a:Z

    .line 64
    iput-boolean v2, p0, LCw;->b:Z

    .line 66
    const-string v0, "Arial"

    iput-object v0, p0, LCw;->c:Ljava/lang/String;

    .line 68
    iput-object v3, p0, LCw;->a:Landroid/content/res/ColorStateList;

    .line 99
    if-nez p5, :cond_95

    move v0, v1

    :goto_19
    xor-int/2addr v0, p4

    const-string v4, "The editable can be null if and only if isTextEnd is false."

    invoke-static {v0, v4}, Lagu;->a(ZLjava/lang/Object;)V

    .line 101
    iput-boolean p4, p0, LCw;->c:Z

    .line 102
    iput-object p5, p0, LCw;->a:Landroid/text/Editable;

    .line 103
    iput-object p3, p0, LCw;->a:Lxu;

    .line 104
    invoke-interface {p1}, Lxg;->a()Z

    move-result v0

    if-eqz v0, :cond_97

    :goto_2b
    invoke-interface {p1}, Lxg;->b()Z

    move-result v0

    if-eqz v0, :cond_32

    const/4 v2, 0x2

    :cond_32
    or-int v0, v1, v2

    iput v0, p0, LCw;->b:I

    .line 107
    invoke-interface {p1}, Lxg;->d()Z

    move-result v0

    iput-boolean v0, p0, LCw;->a:Z

    .line 108
    invoke-interface {p1}, Lxg;->c()Z

    move-result v0

    iput-boolean v0, p0, LCw;->b:Z

    .line 109
    invoke-interface {p1}, Lxg;->b()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 111
    packed-switch v0, :pswitch_data_e2

    .line 119
    sget-object v0, LGI;->a:LGI;

    iput-object v0, p0, LCw;->a:LGI;

    .line 122
    :goto_52
    invoke-interface {p1}, Lxg;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LCw;->c:Ljava/lang/String;

    .line 123
    invoke-interface {p1}, Lxg;->a()I

    move-result v0

    iput v0, p0, LCw;->a:I

    .line 125
    invoke-interface {p1}, Lxg;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LCw;->b:Ljava/lang/String;

    .line 126
    invoke-interface {p1}, Lxg;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LCw;->a:Ljava/lang/String;

    .line 128
    iget-object v0, p0, LCw;->b:Ljava/lang/String;

    if-eqz v0, :cond_73

    .line 130
    :try_start_6e
    iget-object v0, p0, LCw;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_73
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6e .. :try_end_73} :catch_a3

    .line 137
    :cond_73
    :goto_73
    iget-object v0, p0, LCw;->a:Ljava/lang/String;

    if-eqz v0, :cond_8c

    .line 139
    :try_start_77
    new-instance v0, Landroid/content/res/ColorStateList;

    sget-object v1, LCw;->a:[[I

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v4, 0x0

    iget-object v5, p0, LCw;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v5

    aput v5, v2, v4

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, LCw;->a:Landroid/content/res/ColorStateList;
    :try_end_8c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_77 .. :try_end_8c} :catch_c1

    .line 146
    :cond_8c
    :goto_8c
    if-eqz p2, :cond_df

    invoke-interface {p2}, Lvo;->a()[Ljava/lang/String;

    move-result-object v0

    :goto_92
    iput-object v0, p0, LCw;->a:[Ljava/lang/String;

    .line 147
    return-void

    :cond_95
    move v0, v2

    .line 99
    goto :goto_19

    :cond_97
    move v1, v2

    .line 104
    goto :goto_2b

    .line 113
    :pswitch_99
    sget-object v0, LGI;->b:LGI;

    iput-object v0, p0, LCw;->a:LGI;

    goto :goto_52

    .line 116
    :pswitch_9e
    sget-object v0, LGI;->c:LGI;

    iput-object v0, p0, LCw;->a:LGI;

    goto :goto_52

    .line 131
    :catch_a3
    move-exception v0

    .line 132
    const-string v1, "TextStyle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid background color "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, LCw;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 133
    iput-object v3, p0, LCw;->b:Ljava/lang/String;

    goto :goto_73

    .line 141
    :catch_c1
    move-exception v0

    .line 142
    const-string v1, "TextStyle"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid foreground color "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, LCw;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 143
    iput-object v3, p0, LCw;->a:Ljava/lang/String;

    goto :goto_8c

    :cond_df
    move-object v0, v3

    .line 146
    goto :goto_92

    .line 111
    nop

    :pswitch_data_e2
    .packed-switch 0x1
        :pswitch_99
        :pswitch_9e
    .end packed-switch
.end method

.method static synthetic a(LCw;)Z
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-boolean v0, p0, LCw;->a:Z

    return v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 217
    if-nez p0, :cond_8

    if-nez p1, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5

    :cond_8
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5
.end method

.method static synthetic b(LCw;)Z
    .registers 2
    .parameter

    .prologue
    .line 36
    iget-boolean v0, p0, LCw;->b:Z

    return v0
.end method


# virtual methods
.method public a()LGH;
    .registers 10

    .prologue
    const/4 v6, 0x0

    .line 196
    iget-boolean v0, p0, LCw;->c:Z

    if-eqz v0, :cond_19

    .line 197
    new-instance v0, LCa;

    iget-object v1, p0, LCw;->a:Landroid/text/Editable;

    iget-object v2, p0, LCw;->c:Ljava/lang/String;

    iget v3, p0, LCw;->b:I

    iget v4, p0, LCw;->a:I

    iget-object v5, p0, LCw;->a:Landroid/content/res/ColorStateList;

    iget-object v7, p0, LCw;->a:LGI;

    iget-object v8, p0, LCw;->a:Lxu;

    invoke-direct/range {v0 .. v8}, LCa;-><init>(Landroid/text/Editable;Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;LGI;Lxu;)V

    .line 200
    :goto_18
    return-object v0

    :cond_19
    new-instance v1, LBZ;

    iget-object v2, p0, LCw;->c:Ljava/lang/String;

    iget v3, p0, LCw;->b:I

    iget v4, p0, LCw;->a:I

    iget-object v5, p0, LCw;->a:Landroid/content/res/ColorStateList;

    iget-object v7, p0, LCw;->a:LGI;

    iget-object v8, p0, LCw;->a:Lxu;

    invoke-direct/range {v1 .. v8}, LBZ;-><init>(Ljava/lang/String;IILandroid/content/res/ColorStateList;Landroid/content/res/ColorStateList;LGI;Lxu;)V

    move-object v0, v1

    goto :goto_18
.end method

.method public a()Ljava/lang/Integer;
    .registers 2

    .prologue
    .line 245
    iget v0, p0, LCw;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, LCw;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/text/style/CharacterStyle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 207
    iget-boolean v1, p0, LCw;->a:Z

    if-eqz v1, :cond_11

    .line 208
    new-instance v1, Landroid/text/style/UnderlineSpan;

    invoke-direct {v1}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    :cond_11
    iget-boolean v1, p0, LCw;->b:Z

    if-eqz v1, :cond_1d

    .line 211
    new-instance v1, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v1}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    :cond_1d
    return-object v0
.end method

.method public a()LzI;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LzI",
            "<",
            "LCw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 171
    new-instance v0, LCx;

    iget-object v1, p0, LCw;->a:Lxu;

    invoke-interface {v1}, Lxu;->a()Lth;

    move-result-object v1

    iget-object v2, p0, LCw;->a:[Ljava/lang/String;

    iget-object v3, p0, LCw;->b:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, LCx;-><init>(LCw;Lth;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 221
    iget v0, p0, LCw;->b:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public a(LCt;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 151
    instance-of v1, p1, LCw;

    if-eqz v1, :cond_56

    .line 152
    check-cast p1, LCw;

    .line 153
    iget-boolean v1, p0, LCw;->c:Z

    if-nez v1, :cond_56

    iget-boolean v1, p1, LCw;->c:Z

    if-nez v1, :cond_56

    iget v1, p0, LCw;->b:I

    iget v2, p1, LCw;->b:I

    if-ne v1, v2, :cond_56

    iget-object v1, p0, LCw;->a:Ljava/lang/String;

    iget-object v2, p1, LCw;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LCw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    iget-object v1, p0, LCw;->b:Ljava/lang/String;

    iget-object v2, p1, LCw;->b:Ljava/lang/String;

    invoke-static {v1, v2}, LCw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    iget-boolean v1, p0, LCw;->a:Z

    iget-boolean v2, p1, LCw;->a:Z

    if-ne v1, v2, :cond_56

    iget-boolean v1, p0, LCw;->b:Z

    iget-boolean v2, p1, LCw;->b:Z

    if-ne v1, v2, :cond_56

    iget v1, p0, LCw;->a:I

    iget v2, p1, LCw;->a:I

    if-ne v1, v2, :cond_56

    iget-object v1, p0, LCw;->a:LGI;

    iget-object v2, p1, LCw;->a:LGI;

    if-ne v1, v2, :cond_56

    iget-object v1, p0, LCw;->c:Ljava/lang/String;

    iget-object v2, p1, LCw;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LCw;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    iget-object v1, p0, LCw;->a:[Ljava/lang/String;

    iget-object v2, p1, LCw;->a:[Ljava/lang/String;

    invoke-static {v1, v2}, Latx;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    const/4 v0, 0x1

    .line 166
    :cond_56
    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 237
    iget-object v0, p0, LCw;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 225
    iget v0, p0, LCw;->b:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 241
    iget-object v0, p0, LCw;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 229
    iget-boolean v0, p0, LCw;->a:Z

    return v0
.end method
