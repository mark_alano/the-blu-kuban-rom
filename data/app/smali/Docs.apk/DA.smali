.class public LDA;
.super LDG;
.source "SpannableGroup.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NodeOwner:",
        "Ljava/lang/Object;",
        "ChildOwner:",
        "Ljava/lang/Object;",
        ">",
        "LDG",
        "<TNodeOwner;>;",
        "Ljava/lang/Iterable",
        "<",
        "LDG",
        "<TChildOwner;>;>;"
    }
.end annotation


# instance fields
.field private final a:LJO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LJO",
            "<",
            "LDG",
            "<TChildOwner;>;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 25
    invoke-direct {p0}, LDG;-><init>()V

    .line 111
    new-instance v0, LJO;

    invoke-direct {v0}, LJO;-><init>()V

    iput-object v0, p0, LDA;->a:LJO;

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, LDA;->a:Z

    return-void
.end method

.method private a(II)LDE;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "LDE",
            "<",
            "LDG",
            "<TChildOwner;>;>;"
        }
    .end annotation

    .prologue
    .line 228
    if-gtz p1, :cond_e

    .line 229
    iget-object v0, p0, LDA;->a:LJO;

    invoke-virtual {v0}, LJO;->a()LJR;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p2}, LDA;->a(LKh;II)LDE;

    move-result-object v0

    .line 237
    :goto_d
    return-object v0

    .line 231
    :cond_e
    invoke-virtual {p0}, LDA;->d()I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 232
    invoke-virtual {p0, v2}, LDA;->a(I)Landroid/util/Pair;

    move-result-object v1

    .line 233
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LKh;

    .line 234
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 235
    sub-int v1, v2, v1

    .line 237
    invoke-direct {p0, v0, v1, p2}, LDA;->a(LKh;II)LDE;

    move-result-object v0

    goto :goto_d
.end method

.method static synthetic a(LDA;II)LDE;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, LDA;->a(II)LDE;

    move-result-object v0

    return-object v0
.end method

.method private a(LKh;II)LDE;
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKh",
            "<",
            "LDG",
            "<TChildOwner;>;>;II)",
            "LDE",
            "<",
            "LDG",
            "<TChildOwner;>;>;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v0, LDC;

    invoke-direct {v0, p0, p1, p2, p3}, LDC;-><init>(LDA;LKh;II)V

    return-object v0
.end method

.method static synthetic a(LDA;)LJO;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, LDA;->a:LJO;

    return-object v0
.end method

.method static synthetic a(LDA;)V
    .registers 1
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, LDA;->d()V

    return-void
.end method

.method static synthetic b(LDA;)V
    .registers 1
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, LDA;->e()V

    return-void
.end method

.method private d()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 344
    invoke-virtual {p0}, LDA;->a()Z

    move-result v0

    .line 345
    iput-boolean v2, p0, LDA;->a:Z

    .line 346
    invoke-virtual {p0}, LDA;->a()Z

    move-result v1

    if-eqz v1, :cond_16

    if-nez v0, :cond_16

    .line 347
    invoke-virtual {p0}, LDA;->a()LDz;

    move-result-object v0

    invoke-interface {v0, v2}, LDz;->a(Z)V

    .line 349
    :cond_16
    return-void
.end method

.method private e()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 352
    iget-boolean v0, p0, LDA;->a:Z

    if-eqz v0, :cond_1b

    .line 353
    invoke-virtual {p0}, LDA;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 354
    invoke-virtual {v0}, LDG;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 364
    :cond_1b
    :goto_1b
    return-void

    .line 358
    :cond_1c
    invoke-virtual {p0}, LDA;->a()Z

    move-result v0

    .line 359
    iput-boolean v2, p0, LDA;->a:Z

    .line 360
    invoke-virtual {p0}, LDA;->a()Z

    move-result v1

    if-nez v1, :cond_1b

    if-eqz v0, :cond_1b

    .line 361
    invoke-virtual {p0}, LDA;->a()LDz;

    move-result-object v0

    invoke-interface {v0, v2}, LDz;->a(Z)V

    goto :goto_1b
.end method


# virtual methods
.method public a(ILjava/lang/Object;)C
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 127
    invoke-virtual {p0, p1}, LDA;->a(I)Landroid/util/Pair;

    move-result-object v1

    .line 128
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LKh;

    invoke-interface {v0}, LKh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1, p2}, LDG;->a(ILjava/lang/Object;)C

    move-result v0

    return v0
.end method

.method public a(IILjava/lang/Class;Ljava/lang/Object;)I
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 187
    invoke-virtual {p0, p1, p2, p3}, LDA;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v0

    .line 188
    invoke-direct {p0, p1, v0}, LDA;->a(II)LDE;

    move-result-object v2

    move v1, v0

    .line 189
    :goto_9
    invoke-interface {v2}, LDE;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 190
    invoke-interface {v2}, LDE;->a()I

    move-result v3

    .line 191
    if-lt v3, v1, :cond_16

    .line 198
    :cond_15
    return v1

    .line 194
    :cond_16
    invoke-interface {v2}, LDE;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 195
    sub-int v4, p1, v3

    sub-int/2addr v1, v3

    invoke-virtual {v0, v4, v1, p3, p4}, LDG;->a(IILjava/lang/Class;Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v3

    move v1, v0

    .line 197
    goto :goto_9
.end method

.method public final a(I)LDG;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LDG",
            "<TChildOwner;>;"
        }
    .end annotation

    .prologue
    .line 302
    if-ltz p1, :cond_8

    invoke-virtual {p0}, LDA;->d()I

    move-result v0

    if-lt p1, v0, :cond_a

    .line 303
    :cond_8
    const/4 v0, 0x0

    .line 305
    :goto_9
    return-object v0

    :cond_a
    invoke-virtual {p0, p1}, LDA;->a(I)LKh;

    move-result-object v0

    invoke-interface {v0}, LKh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    goto :goto_9
.end method

.method protected a(I)LKh;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LKh",
            "<",
            "LDG",
            "<TChildOwner;>;>;"
        }
    .end annotation

    .prologue
    .line 282
    invoke-virtual {p0, p1}, LDA;->a(I)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LKh;

    return-object v0
.end method

.method protected a(I)Landroid/util/Pair;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "LKh",
            "<",
            "LDG",
            "<TChildOwner;>;>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 286
    iget-object v0, p0, LDA;->a:LJO;

    new-instance v1, LDD;

    invoke-direct {v1, p0}, LDD;-><init>(LDA;)V

    invoke-virtual {v0, p1, v1}, LJO;->a(ILKi;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    return-object v0
.end method

.method public a(II)Ljava/lang/Iterable;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/lang/Iterable",
            "<",
            "LDG",
            "<TChildOwner;>;>;"
        }
    .end annotation

    .prologue
    .line 208
    invoke-virtual {p0}, LDA;->b()I

    move-result v0

    .line 209
    sub-int v1, p1, v0

    sub-int v0, p2, v0

    invoke-virtual {p0, v1, v0}, LDA;->b(II)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .registers 3

    .prologue
    .line 324
    invoke-virtual {p0}, LDA;->b()V

    .line 325
    invoke-virtual {p0}, LDA;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 326
    invoke-virtual {v0}, LDG;->a()V

    goto :goto_7

    .line 328
    :cond_17
    return-void
.end method

.method public a(II[CILjava/lang/Object;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, LDA;->a(II)LDE;

    move-result-object v6

    .line 134
    :goto_4
    invoke-interface {v6}, LDE;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 135
    invoke-interface {v6}, LDE;->a()I

    move-result v2

    .line 136
    invoke-interface {v6}, LDE;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 137
    sub-int v1, p1, v2

    const/4 v3, 0x0

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 138
    add-int v3, v2, v1

    sub-int/2addr v3, p1

    .line 139
    sub-int v2, p2, v2

    invoke-virtual {v0}, LDG;->d()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 140
    add-int v4, p4, v3

    move-object v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LDG;->a(II[CILjava/lang/Object;)V

    goto :goto_4

    .line 142
    :cond_30
    return-void
.end method

.method public a(LDG;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDG",
            "<TChildOwner;>;)V"
        }
    .end annotation

    .prologue
    .line 119
    new-instance v0, LDF;

    iget-object v1, p0, LDA;->a:LJO;

    invoke-virtual {v1}, LJO;->b()LJR;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, LJR;->a(Ljava/lang/Object;I)LJR;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LDF;-><init>(LDA;LKh;)V

    .line 122
    invoke-virtual {p1, v0}, LDG;->a(LDz;)V

    .line 123
    return-void
.end method

.method public a(Ljava/util/List;IILjava/lang/Class;Ljava/lang/Object;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;II",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 149
    if-ne p2, p3, :cond_28

    add-int/lit8 v0, p2, -0x1

    move v1, v0

    .line 150
    :goto_5
    if-ne p2, p3, :cond_2a

    add-int/lit8 v0, p3, 0x1

    .line 151
    :goto_9
    invoke-direct {p0, v1, v0}, LDA;->a(II)LDE;

    move-result-object v6

    .line 153
    :goto_d
    invoke-interface {v6}, LDE;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 154
    invoke-interface {v6}, LDE;->a()I

    move-result v1

    .line 155
    invoke-interface {v6}, LDE;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 156
    sub-int v2, p2, v1

    sub-int v3, p3, v1

    move-object v1, p1

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LDG;->a(Ljava/util/List;IILjava/lang/Class;Ljava/lang/Object;)V

    goto :goto_d

    :cond_28
    move v1, p2

    .line 149
    goto :goto_5

    :cond_2a
    move v0, p3

    .line 150
    goto :goto_9

    .line 160
    :cond_2c
    invoke-virtual {p0, p1, p2, p3, p4}, LDA;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 161
    return-void
.end method

.method protected a()Z
    .registers 2

    .prologue
    .line 332
    invoke-virtual {p0}, LDA;->b()Z

    move-result v0

    return v0
.end method

.method public b(II)Ljava/lang/Iterable;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/lang/Iterable",
            "<",
            "LDG",
            "<TChildOwner;>;>;"
        }
    .end annotation

    .prologue
    .line 219
    new-instance v0, LDB;

    invoke-direct {v0, p0, p1, p2}, LDB;-><init>(LDA;II)V

    return-object v0
.end method

.method protected b()Z
    .registers 2

    .prologue
    .line 340
    iget-boolean v0, p0, LDA;->a:Z

    return v0
.end method

.method c(Ljava/util/List;II)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDs;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 165
    invoke-virtual {p0}, LDA;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 166
    invoke-virtual {p0, p1, p2, p3}, LDA;->b(Ljava/util/List;II)V

    .line 168
    :cond_9
    iget-boolean v0, p0, LDA;->a:Z

    if-eqz v0, :cond_36

    .line 172
    if-ne p2, p3, :cond_32

    add-int/lit8 v0, p2, -0x1

    move v1, v0

    .line 173
    :goto_12
    if-ne p2, p3, :cond_34

    add-int/lit8 v0, p3, 0x1

    .line 174
    :goto_16
    invoke-direct {p0, v1, v0}, LDA;->a(II)LDE;

    move-result-object v1

    .line 176
    :goto_1a
    invoke-interface {v1}, LDE;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 177
    invoke-interface {v1}, LDE;->a()I

    move-result v2

    .line 178
    invoke-interface {v1}, LDE;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDG;

    .line 179
    sub-int v3, p2, v2

    sub-int v2, p3, v2

    invoke-virtual {v0, p1, v3, v2}, LDG;->c(Ljava/util/List;II)V

    goto :goto_1a

    :cond_32
    move v1, p2

    .line 172
    goto :goto_12

    :cond_34
    move v0, p3

    .line 173
    goto :goto_16

    .line 182
    :cond_36
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "LDG",
            "<TChildOwner;>;>;"
        }
    .end annotation

    .prologue
    .line 319
    iget-object v0, p0, LDA;->a:LJO;

    invoke-virtual {v0}, LJO;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
