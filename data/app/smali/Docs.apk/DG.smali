.class public abstract LDG;
.super LDm;
.source "SpannableNode.java"

# interfaces
.implements Landroid/text/GetChars;
.implements Landroid/text/Spannable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NodeOwner:",
        "Ljava/lang/Object;",
        ">",
        "LDm",
        "<",
        "LDG",
        "<TNodeOwner;>;>;",
        "Landroid/text/GetChars;",
        "Landroid/text/Spannable;"
    }
.end annotation


# instance fields
.field private a:LDJ;

.field private final a:LDw;

.field private a:LDz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDz",
            "<TNodeOwner;>;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TNodeOwner;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, LDm;-><init>()V

    .line 32
    new-instance v0, LDw;

    invoke-direct {v0, p0}, LDw;-><init>(Landroid/text/Spannable;)V

    iput-object v0, p0, LDG;->a:LDw;

    .line 44
    iput-object v1, p0, LDG;->a:LDJ;

    .line 45
    iput-object v1, p0, LDG;->a:LDz;

    .line 46
    return-void
.end method

.method private a(Ljava/lang/Object;II)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 428
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 429
    invoke-direct {p0, v0, p2, p3}, LDG;->d(Ljava/util/List;II)V

    .line 430
    invoke-virtual {p0}, LDG;->b()I

    move-result v1

    .line 431
    add-int v2, v1, p2

    .line 432
    add-int/2addr v1, p3

    .line 433
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDs;

    .line 434
    invoke-interface {v0, p1, v2, v1}, LDs;->a(Ljava/lang/Object;II)V

    goto :goto_13

    .line 436
    :cond_23
    return-void
.end method

.method private a(Ljava/lang/Object;IIII)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 390
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 391
    invoke-static {p2, p4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 392
    invoke-static {p3, p5}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 393
    if-gt v2, v1, :cond_36

    .line 395
    invoke-direct {p0, v0, p2, p3}, LDG;->d(Ljava/util/List;II)V

    .line 396
    invoke-direct {p0, v0, p4, p5}, LDG;->d(Ljava/util/List;II)V

    .line 414
    :cond_15
    :goto_15
    invoke-virtual {p0}, LDG;->b()I

    move-result v1

    .line 415
    add-int v2, v1, p2

    .line 416
    add-int v3, v1, p3

    .line 417
    add-int v4, v1, p4

    .line 418
    add-int v5, v1, p5

    .line 419
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_25
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDs;

    move-object v1, p1

    .line 420
    invoke-interface/range {v0 .. v5}, LDs;->a(Ljava/lang/Object;IIII)V

    goto :goto_25

    .line 398
    :cond_36
    invoke-static {p2, p4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 399
    invoke-static {p3, p5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 400
    instance-of v5, p1, LDl;

    if-eqz v5, :cond_46

    .line 401
    invoke-direct {p0, v0, v3, v4}, LDG;->d(Ljava/util/List;II)V

    goto :goto_15

    .line 405
    :cond_46
    if-le v1, v3, :cond_4b

    .line 406
    invoke-direct {p0, v0, v3, v1}, LDG;->d(Ljava/util/List;II)V

    .line 408
    :cond_4b
    if-le v4, v2, :cond_15

    .line 409
    invoke-direct {p0, v0, v2, v4}, LDG;->d(Ljava/util/List;II)V

    goto :goto_15

    .line 422
    :cond_51
    return-void
.end method

.method private b(Ljava/lang/Object;II)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 443
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 444
    invoke-direct {p0, v0, p2, p3}, LDG;->d(Ljava/util/List;II)V

    .line 445
    invoke-virtual {p0}, LDG;->b()I

    move-result v1

    .line 446
    add-int v2, v1, p2

    .line 447
    add-int/2addr v1, p3

    .line 448
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDs;

    .line 449
    invoke-interface {v0, p1, v2, v1}, LDs;->b(Ljava/lang/Object;II)V

    goto :goto_13

    .line 451
    :cond_23
    return-void
.end method

.method private d(Ljava/util/List;II)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDs;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 381
    invoke-virtual {p0}, LDG;->a()LDz;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, LDz;->b(Ljava/util/List;II)V

    .line 382
    invoke-virtual {p0, p1, p2, p3}, LDG;->c(Ljava/util/List;II)V

    .line 383
    return-void
.end method


# virtual methods
.method public abstract a(ILjava/lang/Object;)C
.end method

.method public abstract a(IILjava/lang/Class;Ljava/lang/Object;)I
.end method

.method public a(Ljava/lang/Object;)I
    .registers 5
    .parameter

    .prologue
    .line 247
    invoke-virtual {p0}, LDG;->a()LDJ;

    move-result-object v0

    invoke-interface {v0}, LDJ;->a()LDy;

    move-result-object v0

    invoke-virtual {v0, p1}, LDy;->a(Ljava/lang/Object;)LDG;

    move-result-object v0

    .line 248
    if-eqz v0, :cond_3c

    .line 249
    invoke-virtual {v0}, LDG;->b()I

    move-result v1

    invoke-virtual {v0, p1}, LDG;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, LDG;->b()I

    move-result v2

    sub-int/2addr v1, v2

    .line 250
    invoke-virtual {v0}, LDG;->b()I

    move-result v2

    invoke-virtual {v0, p1}, LDG;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p0}, LDG;->b()I

    move-result v2

    sub-int/2addr v0, v2

    .line 251
    invoke-virtual {p0}, LDG;->length()I

    move-result v2

    if-gt v1, v2, :cond_3c

    if-ltz v0, :cond_3c

    .line 252
    const/4 v0, 0x0

    invoke-virtual {p0}, LDG;->length()I

    move-result v2

    invoke-static {v1, v0, v2}, LKf;->a(III)I

    move-result v0

    .line 255
    :goto_3b
    return v0

    :cond_3c
    const/4 v0, -0x1

    goto :goto_3b
.end method

.method protected a()LDJ;
    .registers 2

    .prologue
    .line 82
    iget-object v0, p0, LDG;->a:LDJ;

    if-nez v0, :cond_e

    .line 83
    invoke-virtual {p0}, LDG;->a()LDz;

    move-result-object v0

    invoke-interface {v0}, LDz;->a()LDJ;

    move-result-object v0

    iput-object v0, p0, LDG;->a:LDJ;

    .line 85
    :cond_e
    iget-object v0, p0, LDG;->a:LDJ;

    return-object v0
.end method

.method protected bridge synthetic a()LDo;
    .registers 2

    .prologue
    .line 28
    invoke-virtual {p0}, LDG;->a()LDz;

    move-result-object v0

    return-object v0
.end method

.method protected a()LDz;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDz",
            "<TNodeOwner;>;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, LDG;->a:LDz;

    if-nez v0, :cond_11

    .line 133
    new-instance v0, LDH;

    new-instance v1, LDK;

    invoke-direct {v1, p0}, LDK;-><init>(LDG;)V

    invoke-direct {v0, v1}, LDH;-><init>(LDJ;)V

    invoke-virtual {p0, v0}, LDG;->a(LDz;)V

    .line 135
    :cond_11
    iget-object v0, p0, LDG;->a:LDz;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TNodeOwner;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, LDG;->a:Ljava/lang/Object;

    return-object v0
.end method

.method protected abstract a()V
.end method

.method a(III)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 345
    iget-object v0, p0, LDG;->a:LDw;

    invoke-virtual {v0, p1, p2, p3}, LDw;->a(III)V

    .line 346
    return-void
.end method

.method protected a(IILjava/lang/CharSequence;IIZ)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 468
    sub-int v6, p2, p1

    .line 469
    sub-int v7, p5, p4

    .line 470
    invoke-virtual {p0}, LDG;->b()I

    move-result v8

    .line 473
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 474
    invoke-virtual {p0}, LDG;->a()LDz;

    move-result-object v0

    invoke-interface {v0, v9, p1, p2}, LDz;->a(Ljava/util/List;II)V

    .line 475
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDt;

    .line 476
    add-int v2, v8, p1

    invoke-interface {v0, v2, v6, v7}, LDt;->a(III)V

    goto :goto_18

    .line 480
    :cond_2a
    if-eqz p6, :cond_2f

    .line 481
    invoke-virtual {p0, p1, v6, v7}, LDG;->a(III)V

    .line 483
    :cond_2f
    invoke-virtual {p0}, LDG;->a()LDz;

    move-result-object v0

    invoke-interface {v0, p1, v6, v7}, LDz;->a(III)V

    .line 486
    invoke-virtual {p0}, LDG;->a()LDJ;

    move-result-object v0

    invoke-interface {v0}, LDJ;->a()Landroid/text/Editable;

    move-result-object v0

    add-int v1, v8, p1

    add-int v2, v8, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;II)Landroid/text/Editable;

    .line 488
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDt;

    .line 489
    add-int v2, v8, p1

    invoke-interface {v0, v2, v6, v7}, LDt;->b(III)V

    goto :goto_4c

    .line 492
    :cond_5e
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_62
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDt;

    .line 493
    invoke-interface {v0}, LDt;->a()V

    goto :goto_62

    .line 495
    :cond_72
    return-void
.end method

.method public abstract a(II[CILjava/lang/Object;)V
.end method

.method a(LDz;)V
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDz",
            "<TNodeOwner;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 100
    const-string v3, ""

    .line 102
    iget-object v0, p0, LDG;->a:LDz;

    if-eqz v0, :cond_2d

    .line 103
    invoke-virtual {p0}, LDG;->length()I

    move-result v0

    invoke-virtual {p0, v1, v0}, LDG;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    .line 106
    invoke-interface {v7}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const-string v3, ""

    move-object v0, p0

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-virtual/range {v0 .. v6}, LDG;->a(IILjava/lang/CharSequence;IIZ)V

    .line 109
    iget-object v0, p0, LDG;->a:LDz;

    invoke-interface {v0}, LDz;->a()V

    .line 111
    invoke-virtual {p0}, LDG;->a()Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 112
    iget-object v0, p0, LDG;->a:LDz;

    invoke-interface {v0, v1}, LDz;->a(Z)V

    :cond_2c
    move-object v3, v7

    .line 117
    :cond_2d
    iput-object p1, p0, LDG;->a:LDz;

    .line 118
    invoke-virtual {p0}, LDG;->a()V

    .line 120
    iget-object v0, p0, LDG;->a:LDz;

    if-eqz v0, :cond_4d

    .line 122
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v5

    move-object v0, p0

    move v2, v1

    move v4, v1

    move v6, v1

    invoke-virtual/range {v0 .. v6}, LDG;->a(IILjava/lang/CharSequence;IIZ)V

    .line 124
    invoke-virtual {p0}, LDG;->a()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 125
    iget-object v0, p0, LDG;->a:LDz;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LDz;->a(Z)V

    .line 128
    :cond_4d
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeOwner;)V"
        }
    .end annotation

    .prologue
    .line 165
    iput-object p1, p0, LDG;->a:Ljava/lang/Object;

    .line 166
    return-void
.end method

.method public a(Ljava/util/List;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDt;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 351
    return-void
.end method

.method public a(Ljava/util/List;IILjava/lang/Class;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;II",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 222
    iget-object v0, p0, LDG;->a:LDw;

    invoke-virtual {v0, p1, p2, p3, p4}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 223
    return-void
.end method

.method public abstract a(Ljava/util/List;IILjava/lang/Class;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;II",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation
.end method

.method abstract a()Z
.end method

.method public b(Ljava/lang/Object;)I
    .registers 5
    .parameter

    .prologue
    .line 264
    invoke-virtual {p0}, LDG;->a()LDJ;

    move-result-object v0

    invoke-interface {v0}, LDJ;->a()LDy;

    move-result-object v0

    invoke-virtual {v0, p1}, LDy;->a(Ljava/lang/Object;)LDG;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_3c

    .line 266
    invoke-virtual {v0}, LDG;->b()I

    move-result v1

    invoke-virtual {v0, p1}, LDG;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, LDG;->b()I

    move-result v2

    sub-int/2addr v1, v2

    .line 267
    invoke-virtual {v0}, LDG;->b()I

    move-result v2

    invoke-virtual {v0, p1}, LDG;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {p0}, LDG;->b()I

    move-result v2

    sub-int/2addr v0, v2

    .line 268
    invoke-virtual {p0}, LDG;->length()I

    move-result v2

    if-gt v1, v2, :cond_3c

    if-ltz v0, :cond_3c

    .line 269
    const/4 v1, 0x0

    invoke-virtual {p0}, LDG;->length()I

    move-result v2

    invoke-static {v0, v1, v2}, LKf;->a(III)I

    move-result v0

    .line 272
    :goto_3b
    return v0

    :cond_3c
    const/4 v0, -0x1

    goto :goto_3b
.end method

.method protected b()V
    .registers 5

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, LDG;->a:LDz;

    if-eqz v1, :cond_b

    .line 55
    iget-object v0, p0, LDG;->a:LDz;

    invoke-interface {v0}, LDz;->a()LDJ;

    move-result-object v0

    .line 57
    :cond_b
    iget-object v1, p0, LDG;->a:LDJ;

    if-ne v0, v1, :cond_10

    .line 74
    :goto_f
    return-void

    .line 61
    :cond_10
    iget-object v1, p0, LDG;->a:LDJ;

    if-eqz v1, :cond_2e

    .line 62
    iget-object v1, p0, LDG;->a:LDJ;

    invoke-interface {v1}, LDJ;->a()LDy;

    move-result-object v1

    .line 63
    iget-object v2, p0, LDG;->a:LDw;

    invoke-virtual {v2}, LDw;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_20
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 64
    invoke-virtual {v1, v3}, LDy;->a(Ljava/lang/Object;)V

    goto :goto_20

    .line 67
    :cond_2e
    if-eqz v0, :cond_48

    .line 68
    invoke-interface {v0}, LDJ;->a()LDy;

    move-result-object v1

    .line 69
    iget-object v2, p0, LDG;->a:LDw;

    invoke-virtual {v2}, LDw;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_48

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 70
    invoke-virtual {v1, v3, p0}, LDy;->a(Ljava/lang/Object;LDG;)V

    goto :goto_3a

    .line 73
    :cond_48
    iput-object v0, p0, LDG;->a:LDJ;

    goto :goto_f
.end method

.method public b(LDG;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDG",
            "<TNodeOwner;>;)V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-virtual {p0}, LDG;->a()LDz;

    move-result-object v0

    invoke-interface {v0, p1}, LDz;->a(LDG;)LDz;

    move-result-object v0

    invoke-virtual {p1, v0}, LDG;->a(LDz;)V

    .line 158
    return-void
.end method

.method public b(Ljava/util/List;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDs;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 356
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 143
    invoke-virtual {p0}, LDG;->length()I

    move-result v0

    if-lez v0, :cond_14

    .line 146
    new-instance v0, LDH;

    new-instance v1, LDK;

    invoke-direct {v1, p0}, LDK;-><init>(LDG;)V

    invoke-direct {v0, v1}, LDH;-><init>(LDJ;)V

    invoke-virtual {p0, v0}, LDG;->a(LDz;)V

    .line 151
    :goto_13
    return-void

    .line 149
    :cond_14
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LDG;->a(LDz;)V

    goto :goto_13
.end method

.method abstract c(Ljava/util/List;II)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDs;",
            ">;II)V"
        }
    .end annotation
.end method

.method public charAt(I)C
    .registers 4
    .parameter

    .prologue
    .line 290
    invoke-virtual {p0}, LDG;->a()LDJ;

    move-result-object v0

    invoke-interface {v0}, LDJ;->a()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {p0}, LDG;->b()I

    move-result v1

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    return v0
.end method

.method public getChars(II[CI)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 313
    const-string v0, "getChars"

    invoke-virtual {p0}, LDG;->length()I

    move-result v1

    invoke-static {v0, p1, p2, v1}, LDI;->a(Ljava/lang/String;III)V

    .line 314
    invoke-virtual {p0}, LDG;->b()I

    move-result v0

    .line 315
    invoke-virtual {p0}, LDG;->a()LDJ;

    move-result-object v1

    invoke-interface {v1}, LDJ;->a()Landroid/text/Editable;

    move-result-object v1

    add-int v2, v0, p1

    add-int/2addr v0, p2

    invoke-interface {v1, v2, v0, p3, p4}, Landroid/text/Editable;->getChars(II[CI)V

    .line 316
    return-void
.end method

.method public getSpanEnd(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 213
    iget-object v0, p0, LDG;->a:LDw;

    invoke-virtual {v0, p1}, LDw;->c(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getSpanFlags(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 203
    iget-object v0, p0, LDG;->a:LDw;

    invoke-virtual {v0, p1}, LDw;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getSpanStart(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 208
    iget-object v0, p0, LDG;->a:LDw;

    invoke-virtual {v0, p1}, LDw;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getSpans(IILjava/lang/Class;)[Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(II",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    .prologue
    .line 227
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 228
    invoke-virtual {p0, v1, p1, p2, p3}, LDG;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 230
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {p3, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    .line 231
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public length()I
    .registers 2

    .prologue
    .line 302
    invoke-virtual {p0}, LDG;->d()I

    move-result v0

    return v0
.end method

.method public nextSpanTransition(IILjava/lang/Class;)I
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 278
    iget-object v0, p0, LDG;->a:LDw;

    invoke-virtual {v0, p1, p2, p3}, LDw;->a(IILjava/lang/Class;)I

    move-result v0

    return v0
.end method

.method public removeSpan(Ljava/lang/Object;)V
    .registers 6
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, LDG;->a:LDw;

    invoke-virtual {v0, p1}, LDw;->b(Ljava/lang/Object;)I

    move-result v0

    .line 179
    if-ltz v0, :cond_22

    .line 180
    iget-object v1, p0, LDG;->a:LDw;

    invoke-virtual {v1, p1}, LDw;->c(Ljava/lang/Object;)I

    move-result v1

    .line 181
    invoke-virtual {p0}, LDG;->a()LDJ;

    move-result-object v2

    invoke-interface {v2}, LDJ;->a()LDy;

    move-result-object v2

    invoke-virtual {v2, p1}, LDy;->a(Ljava/lang/Object;)V

    .line 182
    iget-object v2, p0, LDG;->a:LDw;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, LDw;->a(Ljava/lang/Object;Z)V

    .line 183
    invoke-direct {p0, p1, v0, v1}, LDG;->b(Ljava/lang/Object;II)V

    .line 185
    :cond_22
    return-void
.end method

.method public setSpan(Ljava/lang/Object;III)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 189
    iget-object v0, p0, LDG;->a:LDw;

    invoke-virtual {v0, p1}, LDw;->b(Ljava/lang/Object;)I

    move-result v6

    .line 190
    if-ltz v6, :cond_22

    .line 191
    iget-object v0, p0, LDG;->a:LDw;

    invoke-virtual {v0, p1}, LDw;->c(Ljava/lang/Object;)I

    move-result v7

    .line 192
    iget-object v0, p0, LDG;->a:LDw;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, LDw;->a(Ljava/lang/Object;IIIZ)V

    move-object v0, p0

    move-object v1, p1

    move v2, v6

    move v3, v7

    move v4, p2

    move v5, p3

    .line 193
    invoke-direct/range {v0 .. v5}, LDG;->a(Ljava/lang/Object;IIII)V

    .line 199
    :goto_21
    return-void

    .line 195
    :cond_22
    iget-object v0, p0, LDG;->a:LDw;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, LDw;->a(Ljava/lang/Object;IIIZ)V

    .line 196
    invoke-virtual {p0}, LDG;->a()LDJ;

    move-result-object v0

    invoke-interface {v0}, LDJ;->a()LDy;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, LDy;->a(Ljava/lang/Object;LDG;)V

    .line 197
    invoke-direct {p0, p1, p2, p3}, LDG;->a(Ljava/lang/Object;II)V

    goto :goto_21
.end method

.method public subSequence(II)Ljava/lang/CharSequence;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 307
    invoke-virtual {p0}, LDG;->b()I

    move-result v0

    .line 308
    invoke-virtual {p0}, LDG;->a()LDJ;

    move-result-object v1

    invoke-interface {v1}, LDJ;->a()Landroid/text/Editable;

    move-result-object v1

    add-int v2, v0, p1

    add-int/2addr v0, p2

    invoke-interface {v1, v2, v0}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 327
    invoke-virtual {p0}, LDG;->length()I

    move-result v0

    .line 328
    new-array v1, v0, [C

    .line 329
    invoke-virtual {p0, v2, v0, v1, v2}, LDG;->getChars(II[CI)V

    .line 330
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method
