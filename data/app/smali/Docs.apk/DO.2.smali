.class public abstract LDO;
.super Ljava/lang/Object;
.source "BaseLayout.java"

# interfaces
.implements LEj;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# static fields
.field static final a:I

.field private static final a:Landroid/graphics/Rect;

.field private static final a:[Landroid/text/style/ParagraphStyle;

.field static final b:I


# instance fields
.field private a:F

.field private a:LFc;

.field protected final a:LFg;

.field private a:Landroid/text/Layout$Alignment;

.field a:Landroid/text/TextPaint;

.field private a:Ljava/lang/CharSequence;

.field private a:Z

.field private b:F

.field private b:Landroid/text/TextPaint;

.field private c:F

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    const/4 v1, -0x1

    .line 51
    const-class v0, Landroid/text/style/ParagraphStyle;

    invoke-static {v0}, LDN;->a(Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ParagraphStyle;

    sput-object v0, LDO;->a:[Landroid/text/style/ParagraphStyle;

    .line 56
    sput v1, LDO;->a:I

    .line 57
    sput v1, LDO;->b:I

    .line 1671
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LDO;->a:Landroid/graphics/Rect;

    return-void
.end method

.method protected constructor <init>(LFg;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1662
    const/high16 v0, 0x3f80

    iput v0, p0, LDO;->a:F

    .line 1668
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LDO;->a:Landroid/text/Layout$Alignment;

    .line 113
    if-gez p4, :cond_2d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Layout: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_2d
    iput-object p1, p0, LDO;->a:LFg;

    .line 121
    if-eqz p3, :cond_35

    .line 122
    iput v1, p3, Landroid/text/TextPaint;->bgColor:I

    .line 123
    iput v1, p3, Landroid/text/TextPaint;->baselineShift:I

    .line 126
    :cond_35
    iput-object p2, p0, LDO;->a:Ljava/lang/CharSequence;

    .line 127
    iput-object p3, p0, LDO;->b:Landroid/text/TextPaint;

    .line 128
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, LDO;->a:Landroid/text/TextPaint;

    .line 129
    iput p4, p0, LDO;->c:I

    .line 130
    iput-object p5, p0, LDO;->a:Landroid/text/Layout$Alignment;

    .line 131
    iput p6, p0, LDO;->b:F

    .line 132
    iput p7, p0, LDO;->c:F

    .line 133
    instance-of v0, p2, Landroid/text/Spanned;

    iput-boolean v0, p0, LDO;->a:Z

    .line 134
    return-void
.end method

.method private a(III)F
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 555
    invoke-virtual {p0, p1}, LDO;->b(I)S

    move-result v0

    .line 556
    if-nez v0, :cond_8

    .line 557
    const/4 v0, 0x0

    .line 560
    :goto_7
    return v0

    .line 559
    :cond_8
    invoke-virtual {p0, p1}, LDO;->a(I)S

    move-result v1

    sub-int v1, p2, v1

    sub-int/2addr v1, p3

    int-to-float v1, v1

    .line 560
    int-to-float v0, v0

    div-float v0, v1, v0

    goto :goto_7
.end method

.method private a(IZ)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 651
    invoke-virtual {p0, p1}, LDO;->g(I)I

    move-result v0

    .line 653
    invoke-direct {p0, p1, p2, v0}, LDO;->a(IZI)F

    move-result v0

    return v0
.end method

.method private a(IZI)F
    .registers 16
    .parameter
    .parameter
    .parameter

    .prologue
    .line 657
    invoke-virtual {p0, p3}, LDO;->n(I)I

    move-result v3

    .line 658
    invoke-virtual {p0, p3}, LDO;->h(I)I

    move-result v4

    .line 659
    invoke-virtual {p0, p3}, LDO;->o(I)I

    move-result v5

    .line 660
    invoke-virtual {p0, p3}, LDO;->c(I)Z

    move-result v7

    .line 661
    invoke-virtual {p0, p3}, LDO;->a(I)LEk;

    move-result-object v6

    .line 663
    const/4 v8, 0x0

    .line 664
    if-eqz v7, :cond_33

    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_33

    .line 667
    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    const-class v1, Landroid/text/style/TabStopSpan;

    invoke-static {v0, v3, v4, v1}, LDO;->a(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/TabStopSpan;

    .line 668
    array-length v1, v0

    if-lez v1, :cond_33

    .line 669
    new-instance v8, LDR;

    const/16 v1, 0x14

    invoke-direct {v8, v1, v0}, LDR;-><init>(I[Ljava/lang/Object;)V

    .line 673
    :cond_33
    invoke-virtual {p0, p3}, LDO;->l(I)I

    move-result v10

    .line 674
    invoke-virtual {p0, p3}, LDO;->m(I)I

    move-result v11

    .line 676
    invoke-static {}, LFb;->a()LFb;

    move-result-object v0

    .line 677
    iget v1, p0, LDO;->c:I

    invoke-direct {p0, p3, v1, v10}, LDO;->a(III)F

    move-result v9

    .line 678
    iget-object v1, p0, LDO;->b:Landroid/text/TextPaint;

    iget-object v2, p0, LDO;->a:Ljava/lang/CharSequence;

    invoke-virtual/range {v0 .. v9}, LFb;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILEk;ZLDR;F)V

    .line 680
    sub-int v1, p1, v3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, LFb;->a(IZLandroid/graphics/Paint$FontMetricsInt;)F

    move-result v1

    .line 681
    invoke-static {v0}, LFb;->a(LFb;)LFb;

    .line 683
    invoke-direct {p0, p3, v10, v11}, LDO;->a(III)I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v1

    return v0
.end method

.method static a(Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/CharSequence;II)F
    .registers 20
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1345
    invoke-static {}, LEp;->a()LEp;

    move-result-object v13

    .line 1346
    invoke-static {}, LFb;->a()LFb;

    move-result-object v14

    .line 1348
    :try_start_8
    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-virtual {v13, v0, v1, v2}, LEp;->a(Ljava/lang/CharSequence;II)V

    .line 1351
    iget-boolean v3, v13, LEp;->a:Z

    if-eqz v3, :cond_6b

    .line 1352
    sget-object v9, LDO;->a:LEk;

    .line 1353
    const/4 v8, 0x1

    .line 1358
    :goto_18
    iget-object v5, v13, LEp;->a:[C

    .line 1359
    iget v6, v13, LEp;->c:I

    .line 1360
    const/4 v10, 0x0

    .line 1361
    const/4 v4, 0x0

    .line 1362
    const/4 v3, 0x0

    :goto_1f
    if-ge v3, v6, :cond_89

    .line 1363
    aget-char v7, v5, v3

    const/16 v11, 0x9

    if-ne v7, v11, :cond_7c

    .line 1364
    const/4 v10, 0x1

    .line 1365
    move-object/from16 v0, p2

    instance-of v3, v0, Landroid/text/Spanned;

    if-eqz v3, :cond_89

    .line 1366
    move-object/from16 v0, p2

    check-cast v0, Landroid/text/Spanned;

    move-object v3, v0

    .line 1367
    const-class v5, Landroid/text/style/TabStopSpan;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-interface {v3, v0, v1, v5}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v5

    .line 1368
    const-class v6, Landroid/text/style/TabStopSpan;

    move/from16 v0, p3

    invoke-static {v3, v0, v5, v6}, LDO;->a(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/text/style/TabStopSpan;

    .line 1369
    array-length v5, v3

    if-lez v5, :cond_87

    .line 1370
    new-instance v4, LDR;

    const/16 v5, 0x14

    invoke-direct {v4, v5, v3}, LDR;-><init>(I[Ljava/lang/Object;)V

    move-object v3, v4

    :goto_52
    move-object v11, v3

    .line 1379
    :goto_53
    const/4 v12, 0x0

    move-object v3, v14

    move-object v4, p0

    move-object/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v3 .. v12}, LFb;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILEk;ZLDR;F)V

    .line 1380
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, LFb;->a(Landroid/graphics/Paint$FontMetricsInt;)F
    :try_end_63
    .catchall {:try_start_8 .. :try_end_63} :catchall_7f

    move-result v3

    .line 1382
    invoke-static {v14}, LFb;->a(LFb;)LFb;

    .line 1383
    invoke-static {v13}, LEp;->a(LEp;)LEp;

    return v3

    .line 1355
    :cond_6b
    :try_start_6b
    iget v3, v13, LEp;->b:I

    iget-object v4, v13, LEp;->a:[B

    const/4 v5, 0x0

    iget-object v6, v13, LEp;->a:[C

    const/4 v7, 0x0

    iget v8, v13, LEp;->c:I

    invoke-static/range {v3 .. v8}, LDM;->a(I[BI[CII)LEk;

    move-result-object v9

    .line 1356
    iget v8, v13, LEp;->b:I
    :try_end_7b
    .catchall {:try_start_6b .. :try_end_7b} :catchall_7f

    goto :goto_18

    .line 1362
    :cond_7c
    add-int/lit8 v3, v3, 0x1

    goto :goto_1f

    .line 1382
    :catchall_7f
    move-exception v3

    invoke-static {v14}, LFb;->a(LFb;)LFb;

    .line 1383
    invoke-static {v13}, LEp;->a(LEp;)LEp;

    throw v3

    :cond_87
    move-object v3, v4

    goto :goto_52

    :cond_89
    move-object v11, v4

    goto :goto_53
.end method

.method public static a(Ljava/lang/CharSequence;IILandroid/text/TextPaint;)F
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    const/4 v2, 0x0

    .line 74
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    .line 77
    :goto_6
    if-gt p1, p2, :cond_1d

    .line 78
    const/16 v0, 0xa

    invoke-static {p0, v0, p1, p2}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CII)I

    move-result v0

    .line 80
    if-gez v0, :cond_11

    move v0, p2

    .line 83
    :cond_11
    invoke-static {p3, v3, p0, p1, v0}, LDO;->a(Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/CharSequence;II)F

    move-result v1

    .line 85
    cmpl-float v4, v1, v2

    if-lez v4, :cond_1e

    .line 87
    :goto_19
    add-int/lit8 p1, v0, 0x1

    move v2, v1

    .line 77
    goto :goto_6

    .line 90
    :cond_1d
    return v2

    :cond_1e
    move v1, v2

    goto :goto_19
.end method

.method public static a(Ljava/lang/CharSequence;Landroid/text/TextPaint;)F
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-static {p0, v0, v1, p1}, LDO;->a(Ljava/lang/CharSequence;IILandroid/text/TextPaint;)F

    move-result v0

    return v0
.end method

.method private a(III)I
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    .line 455
    invoke-virtual {p0, p1}, LDO;->a(I)Landroid/text/Layout$Alignment;

    move-result-object v2

    .line 456
    invoke-virtual {p0, p1}, LDO;->o(I)I

    move-result v3

    .line 459
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    if-ne v2, v0, :cond_12

    .line 460
    if-ne v3, v7, :cond_10

    .line 488
    :goto_f
    return p2

    :cond_10
    move p2, p3

    .line 463
    goto :goto_f

    .line 466
    :cond_12
    const/4 v1, 0x0

    .line 467
    iget-boolean v0, p0, LDO;->a:Z

    if-eqz v0, :cond_5c

    invoke-virtual {p0, p1}, LDO;->c(I)Z

    move-result v0

    if-eqz v0, :cond_5c

    .line 468
    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    .line 469
    invoke-virtual {p0, p1}, LDO;->n(I)I

    move-result v4

    .line 470
    invoke-interface {v0}, Landroid/text/Spanned;->length()I

    move-result v5

    const-class v6, Landroid/text/style/TabStopSpan;

    invoke-interface {v0, v4, v5, v6}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v5

    .line 471
    const-class v6, Landroid/text/style/TabStopSpan;

    invoke-static {v0, v4, v5, v6}, LDO;->a(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/TabStopSpan;

    .line 472
    array-length v4, v0

    if-lez v4, :cond_5c

    .line 473
    new-instance v1, LDR;

    const/16 v4, 0x14

    invoke-direct {v1, v4, v0}, LDR;-><init>(I[Ljava/lang/Object;)V

    move-object v0, v1

    .line 476
    :goto_42
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LDO;->a(ILDR;Z)F

    move-result v0

    float-to-int v0, v0

    .line 477
    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    if-ne v2, v1, :cond_53

    .line 478
    if-ne v3, v7, :cond_51

    .line 479
    sub-int p2, p3, v0

    goto :goto_f

    .line 481
    :cond_51
    sub-int/2addr p2, v0

    goto :goto_f

    .line 484
    :cond_53
    and-int/lit8 v0, v0, -0x2

    .line 485
    add-int v1, p2, p3

    sub-int v0, v1, v0

    shr-int/lit8 p2, v0, 0x1

    goto :goto_f

    :cond_5c
    move-object v0, v1

    goto :goto_42
.end method

.method private a(IZ)I
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 991
    :cond_1
    invoke-direct {p0, p1, p2}, LDO;->b(IZ)I

    move-result p1

    .line 993
    iget-boolean v0, p0, LDO;->a:Z

    if-eqz v0, :cond_2e

    .line 994
    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    .line 995
    const-class v1, LGF;

    invoke-interface {v0, p1, p1, v1}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LGF;

    .line 996
    array-length v4, v1

    move v3, v2

    :goto_17
    if-ge v3, v4, :cond_2e

    aget-object v5, v1, v3

    .line 997
    invoke-interface {v0, v5}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    if-ge v6, p1, :cond_2b

    invoke-interface {v0, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    if-le v5, p1, :cond_2b

    .line 998
    const/4 v0, 0x1

    .line 1003
    :goto_28
    if-nez v0, :cond_1

    .line 1004
    return p1

    .line 996
    :cond_2b
    add-int/lit8 v3, v3, 0x1

    goto :goto_17

    :cond_2e
    move v0, v2

    goto :goto_28
.end method

.method private a(Landroid/text/Layout$Alignment;LDR;IIII)I
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 422
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    if-ne p1, v0, :cond_a

    .line 423
    if-ne p4, v2, :cond_8

    .line 441
    :goto_7
    return p5

    :cond_8
    move p5, p6

    .line 426
    goto :goto_7

    .line 429
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p0, p3, p2, v0}, LDO;->a(ILDR;Z)F

    move-result v0

    float-to-int v0, v0

    .line 430
    sget-object v1, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    if-ne p1, v1, :cond_1b

    .line 431
    if-ne p4, v2, :cond_19

    .line 432
    sub-int p5, p6, v0

    goto :goto_7

    .line 434
    :cond_19
    sub-int/2addr p5, v0

    goto :goto_7

    .line 437
    :cond_1b
    and-int/lit8 v0, v0, -0x2

    .line 438
    add-int v1, p6, p5

    sub-int v0, v1, v0

    shr-int/lit8 p5, v0, 0x1

    goto :goto_7
.end method

.method private a(IIIIILandroid/graphics/Path;)V
    .registers 17
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1193
    invoke-virtual {p0, p1}, LDO;->n(I)I

    move-result v8

    .line 1194
    invoke-virtual {p0, p1}, LDO;->h(I)I

    move-result v0

    .line 1195
    invoke-virtual {p0, p1}, LDO;->a(I)LEk;

    move-result-object v9

    .line 1197
    if-le v0, v8, :cond_66

    iget-object v1, p0, LDO;->a:Ljava/lang/CharSequence;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_66

    add-int/lit8 v0, v0, -0x1

    move v6, v0

    .line 1199
    :goto_1d
    const/4 v0, 0x0

    move v7, v0

    :goto_1f
    iget-object v0, v9, LEk;->a:[I

    array-length v0, v0

    if-ge v7, v0, :cond_65

    .line 1200
    iget-object v0, v9, LEk;->a:[I

    aget v0, v0, v7

    add-int v1, v8, v0

    .line 1201
    iget-object v0, v9, LEk;->a:[I

    add-int/lit8 v2, v7, 0x1

    aget v0, v0, v2

    const v2, 0x3ffffff

    and-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 1203
    if-le v0, v6, :cond_38

    move v0, v6

    .line 1205
    :cond_38
    if-gt p2, v0, :cond_61

    if-lt p3, v1, :cond_61

    .line 1206
    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1207
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1209
    if-eq v1, v0, :cond_61

    .line 1210
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, p1}, LDO;->a(IZI)F

    move-result v3

    .line 1211
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, p1}, LDO;->a(IZI)F

    move-result v0

    .line 1213
    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    int-to-float v2, p4

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    int-to-float v4, p5

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object/from16 v0, p6

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 1199
    :cond_61
    add-int/lit8 v0, v7, 0x2

    move v7, v0

    goto :goto_1f

    .line 1217
    :cond_65
    return-void

    :cond_66
    move v6, v0

    goto :goto_1d
.end method

.method private a(III[CI)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1522
    invoke-virtual {p0, p3}, LDO;->q(I)I

    move-result v3

    .line 1524
    if-nez v3, :cond_7

    .line 1546
    :cond_6
    return-void

    .line 1528
    :cond_7
    invoke-virtual {p0, p3}, LDO;->p(I)I

    move-result v2

    .line 1529
    invoke-virtual {p0, p3}, LDO;->n(I)I

    move-result v4

    move v1, v2

    .line 1531
    :goto_10
    add-int v0, v2, v3

    if-ge v1, v0, :cond_6

    .line 1534
    if-ne v1, v2, :cond_26

    .line 1535
    const/16 v0, 0x2026

    .line 1540
    :goto_18
    add-int v5, v1, v4

    .line 1542
    if-lt v5, p1, :cond_22

    if-ge v5, p2, :cond_22

    .line 1543
    add-int/2addr v5, p5

    sub-int/2addr v5, p1

    aput-char v0, p4, v5

    .line 1531
    :cond_22
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    .line 1537
    :cond_26
    const v0, 0xfeff

    goto :goto_18
.end method

.method static synthetic a(LDO;III[CI)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct/range {p0 .. p5}, LDO;->a(III[CI)V

    return-void
.end method

.method static a(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/text/Spanned;",
            "II",
            "Ljava/lang/Class",
            "<TT;>;)[TT;"
        }
    .end annotation

    .prologue
    .line 1514
    if-ne p1, p2, :cond_9

    if-lez p1, :cond_9

    .line 1515
    invoke-static {p3}, LDN;->a(Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    .line 1518
    :goto_8
    return-object v0

    :cond_9
    invoke-interface {p0, p1, p2, p3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    goto :goto_8
.end method

.method private b(IZ)F
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x0

    .line 757
    invoke-virtual {p0, p1}, LDO;->n(I)I

    move-result v3

    .line 758
    if-eqz p2, :cond_59

    invoke-virtual {p0, p1}, LDO;->h(I)I

    move-result v4

    .line 760
    :goto_b
    invoke-virtual {p0, p1}, LDO;->c(I)Z

    move-result v7

    .line 762
    if-eqz v7, :cond_5e

    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    instance-of v0, v0, Landroid/text/Spanned;

    if-eqz v0, :cond_5e

    .line 765
    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    const-class v1, Landroid/text/style/TabStopSpan;

    invoke-static {v0, v3, v4, v1}, LDO;->a(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/TabStopSpan;

    .line 766
    array-length v1, v0

    if-lez v1, :cond_5e

    .line 767
    new-instance v8, LDR;

    const/16 v1, 0x14

    invoke-direct {v8, v1, v0}, LDR;-><init>(I[Ljava/lang/Object;)V

    .line 770
    :goto_2d
    invoke-virtual {p0, p1}, LDO;->a(I)LEk;

    move-result-object v6

    .line 771
    invoke-virtual {p0, p1}, LDO;->o(I)I

    move-result v5

    .line 773
    invoke-static {}, LFb;->a()LFb;

    move-result-object v0

    .line 774
    const/4 v9, 0x0

    .line 775
    invoke-virtual {p0, p1}, LDO;->b(I)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 776
    invoke-virtual {p0, p1}, LDO;->f(I)I

    move-result v1

    .line 777
    iget v2, p0, LDO;->c:I

    invoke-direct {p0, p1, v2, v1}, LDO;->a(III)F

    move-result v9

    .line 779
    :cond_4a
    iget-object v1, p0, LDO;->b:Landroid/text/TextPaint;

    iget-object v2, p0, LDO;->a:Ljava/lang/CharSequence;

    invoke-virtual/range {v0 .. v9}, LFb;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILEk;ZLDR;F)V

    .line 781
    invoke-virtual {v0, v10}, LFb;->a(Landroid/graphics/Paint$FontMetricsInt;)F

    move-result v1

    .line 782
    invoke-static {v0}, LFb;->a(LFb;)LFb;

    .line 783
    return v1

    .line 758
    :cond_59
    invoke-virtual {p0, p1}, LDO;->i(I)I

    move-result v4

    goto :goto_b

    :cond_5e
    move-object v8, v10

    goto :goto_2d
.end method

.method private b(III)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 936
    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    .line 938
    invoke-virtual {p0}, LDO;->d()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_d

    .line 955
    :cond_a
    :goto_a
    return p3

    .line 942
    :cond_b
    add-int/lit8 p3, p3, -0x1

    :cond_d
    if-le p3, p2, :cond_a

    .line 943
    add-int/lit8 v1, p3, -0x1

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 945
    const/16 v2, 0xa

    if-ne v1, v2, :cond_1c

    .line 946
    add-int/lit8 p3, p3, -0x1

    goto :goto_a

    .line 949
    :cond_1c
    const/16 v2, 0x20

    if-eq v1, v2, :cond_b

    const/16 v2, 0x9

    if-eq v1, v2, :cond_b

    goto :goto_a
.end method

.method private b(IZ)I
    .registers 13
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 1008
    invoke-virtual {p0, p1}, LDO;->g(I)I

    move-result v2

    .line 1009
    invoke-virtual {p0, v2}, LDO;->n(I)I

    move-result v3

    .line 1010
    invoke-virtual {p0, v2}, LDO;->h(I)I

    move-result v4

    .line 1011
    invoke-virtual {p0, v2}, LDO;->o(I)I

    move-result v1

    .line 1014
    const/4 v5, -0x1

    if-ne v1, v5, :cond_69

    move v5, v0

    :goto_16
    if-ne p2, v5, :cond_6b

    move v5, v0

    .line 1016
    :goto_19
    if-eqz v5, :cond_6d

    .line 1017
    if-ne p1, v4, :cond_7a

    .line 1018
    invoke-virtual {p0}, LDO;->d()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v2, v5, :cond_68

    .line 1020
    add-int/lit8 v2, v2, 0x1

    move v8, v2

    move v2, v0

    .line 1036
    :goto_29
    if-eqz v2, :cond_78

    .line 1037
    invoke-virtual {p0, v8}, LDO;->n(I)I

    move-result v3

    .line 1038
    invoke-virtual {p0, v8}, LDO;->h(I)I

    move-result v4

    .line 1039
    invoke-virtual {p0, v8}, LDO;->o(I)I

    move-result v5

    .line 1040
    if-eq v5, v1, :cond_78

    .line 1044
    if-nez p2, :cond_76

    :goto_3b
    move p2, v0

    .line 1049
    :goto_3c
    invoke-virtual {p0, v8}, LDO;->a(I)LEk;

    move-result-object v6

    .line 1051
    invoke-static {}, LFb;->a()LFb;

    move-result-object v0

    .line 1053
    const/4 v9, 0x0

    .line 1054
    invoke-virtual {p0, v8}, LDO;->b(I)Z

    move-result v1

    if-eqz v1, :cond_55

    .line 1055
    invoke-virtual {p0, v8}, LDO;->f(I)I

    move-result v1

    .line 1056
    iget v2, p0, LDO;->c:I

    invoke-direct {p0, v8, v2, v1}, LDO;->a(III)F

    move-result v9

    .line 1058
    :cond_55
    iget-object v1, p0, LDO;->b:Landroid/text/TextPaint;

    iget-object v2, p0, LDO;->a:Ljava/lang/CharSequence;

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v9}, LFb;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILEk;ZLDR;F)V

    .line 1060
    sub-int v1, p1, v3

    invoke-virtual {v0, v1, p2}, LFb;->a(IZ)I

    move-result v1

    add-int p1, v3, v1

    .line 1061
    invoke-static {v0}, LFb;->a(LFb;)LFb;

    .line 1062
    :cond_68
    return p1

    :cond_69
    move v5, v7

    .line 1014
    goto :goto_16

    :cond_6b
    move v5, v7

    goto :goto_19

    .line 1026
    :cond_6d
    if-ne p1, v3, :cond_7a

    .line 1027
    if-lez v2, :cond_68

    .line 1029
    add-int/lit8 v2, v2, -0x1

    move v8, v2

    move v2, v0

    goto :goto_29

    :cond_76
    move v0, v7

    .line 1044
    goto :goto_3b

    :cond_78
    move v5, v1

    goto :goto_3c

    :cond_7a
    move v8, v2

    move v2, v7

    goto :goto_29
.end method

.method private d(I)Z
    .registers 14
    .parameter

    .prologue
    const v11, 0x3ffffff

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 590
    invoke-virtual {p0, p1}, LDO;->g(I)I

    move-result v7

    .line 591
    invoke-virtual {p0, v7}, LDO;->n(I)I

    move-result v8

    .line 592
    invoke-virtual {p0, v7}, LDO;->h(I)I

    move-result v5

    .line 593
    invoke-virtual {p0, v7}, LDO;->a(I)LEk;

    move-result-object v0

    iget-object v9, v0, LEk;->a:[I

    move v0, v1

    .line 596
    :goto_19
    array-length v4, v9

    if-ge v0, v4, :cond_7f

    .line 597
    aget v4, v9, v0

    add-int v6, v8, v4

    .line 598
    add-int/lit8 v4, v0, 0x1

    aget v4, v9, v4

    and-int/2addr v4, v11

    add-int/2addr v4, v6

    .line 599
    if-le v4, v5, :cond_29

    move v4, v5

    .line 602
    :cond_29
    if-lt p1, v6, :cond_4f

    if-ge p1, v4, :cond_4f

    .line 603
    if-le p1, v6, :cond_30

    .line 635
    :goto_2f
    return v1

    .line 607
    :cond_30
    add-int/lit8 v0, v0, 0x1

    aget v0, v9, v0

    ushr-int/lit8 v0, v0, 0x1a

    and-int/lit8 v0, v0, 0x3f

    .line 611
    :goto_38
    if-ne v0, v3, :cond_7d

    .line 613
    invoke-virtual {p0, v7}, LDO;->o(I)I

    move-result v0

    if-ne v0, v2, :cond_52

    move v0, v1

    :goto_41
    move v6, v0

    .line 618
    :goto_42
    if-ne p1, v8, :cond_56

    .line 619
    invoke-virtual {p0, v7}, LDO;->o(I)I

    move-result v0

    if-ne v0, v2, :cond_54

    move v0, v1

    .line 635
    :goto_4b
    if-ge v0, v6, :cond_79

    :goto_4d
    move v1, v2

    goto :goto_2f

    .line 596
    :cond_4f
    add-int/lit8 v0, v0, 0x2

    goto :goto_19

    :cond_52
    move v0, v2

    .line 613
    goto :goto_41

    :cond_54
    move v0, v2

    .line 619
    goto :goto_4b

    .line 621
    :cond_56
    add-int/lit8 v7, p1, -0x1

    move v0, v1

    .line 622
    :goto_59
    array-length v4, v9

    if-ge v0, v4, :cond_7b

    .line 623
    aget v4, v9, v0

    add-int v10, v8, v4

    .line 624
    add-int/lit8 v4, v0, 0x1

    aget v4, v9, v4

    and-int/2addr v4, v11

    add-int/2addr v4, v10

    .line 625
    if-le v4, v5, :cond_69

    move v4, v5

    .line 628
    :cond_69
    if-lt v7, v10, :cond_76

    if-ge v7, v4, :cond_76

    .line 629
    add-int/lit8 v0, v0, 0x1

    aget v0, v9, v0

    ushr-int/lit8 v0, v0, 0x1a

    and-int/lit8 v0, v0, 0x3f

    .line 630
    goto :goto_4b

    .line 622
    :cond_76
    add-int/lit8 v0, v0, 0x2

    goto :goto_59

    :cond_79
    move v2, v1

    .line 635
    goto :goto_4d

    :cond_7b
    move v0, v3

    goto :goto_4b

    :cond_7d
    move v6, v0

    goto :goto_42

    :cond_7f
    move v0, v3

    goto :goto_38
.end method

.method private r(I)I
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1068
    if-nez p1, :cond_4

    .line 1090
    :goto_3
    return v2

    .line 1070
    :cond_4
    iget-object v1, p0, LDO;->a:Ljava/lang/CharSequence;

    .line 1071
    invoke-interface {v1, p1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 1073
    const v3, 0xdc00

    if-lt v0, v3, :cond_26

    const v3, 0xdfff

    if-gt v0, v3, :cond_26

    .line 1074
    add-int/lit8 v0, p1, -0x1

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 1076
    const v3, 0xd800

    if-lt v0, v3, :cond_26

    const v3, 0xdbff

    if-gt v0, v3, :cond_26

    add-int/lit8 p1, p1, -0x1

    .line 1079
    :cond_26
    iget-boolean v0, p0, LDO;->a:Z

    if-eqz v0, :cond_54

    move-object v0, v1

    .line 1080
    check-cast v0, Landroid/text/Spanned;

    const-class v3, Landroid/text/style/ReplacementSpan;

    invoke-interface {v0, p1, p1, v3}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/ReplacementSpan;

    move v3, v2

    .line 1082
    :goto_36
    array-length v2, v0

    if-ge v3, v2, :cond_54

    move-object v2, v1

    .line 1083
    check-cast v2, Landroid/text/Spanned;

    aget-object v4, v0, v3

    invoke-interface {v2, v4}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    move-object v2, v1

    .line 1084
    check-cast v2, Landroid/text/Spanned;

    aget-object v5, v0, v3

    invoke-interface {v2, v5}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    .line 1086
    if-ge v4, p1, :cond_50

    if-le v2, p1, :cond_50

    move p1, v4

    .line 1082
    :cond_50
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_36

    :cond_54
    move v2, p1

    .line 1090
    goto :goto_3
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 156
    iget v0, p0, LDO;->a:F

    return v0
.end method

.method public a(I)F
    .registers 3
    .parameter

    .prologue
    .line 640
    invoke-direct {p0, p1}, LDO;->d(I)Z

    move-result v0

    .line 641
    invoke-direct {p0, p1, v0}, LDO;->a(IZ)F

    move-result v0

    return v0
.end method

.method protected a(ILDR;Z)F
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    .line 796
    invoke-virtual {p0, p1}, LDO;->n(I)I

    move-result v3

    .line 797
    if-eqz p3, :cond_3c

    invoke-virtual {p0, p1}, LDO;->h(I)I

    move-result v4

    .line 798
    :goto_a
    invoke-virtual {p0, p1}, LDO;->c(I)Z

    move-result v7

    .line 799
    invoke-virtual {p0, p1}, LDO;->a(I)LEk;

    move-result-object v6

    .line 800
    invoke-virtual {p0, p1}, LDO;->o(I)I

    move-result v5

    .line 802
    invoke-static {}, LFb;->a()LFb;

    move-result-object v0

    .line 803
    const/4 v9, 0x0

    .line 804
    invoke-virtual {p0, p1}, LDO;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 805
    invoke-virtual {p0, p1}, LDO;->f(I)I

    move-result v1

    .line 806
    iget v2, p0, LDO;->c:I

    invoke-direct {p0, p1, v2, v1}, LDO;->a(III)F

    move-result v9

    .line 808
    :cond_2b
    iget-object v1, p0, LDO;->b:Landroid/text/TextPaint;

    iget-object v2, p0, LDO;->a:Ljava/lang/CharSequence;

    move-object v8, p2

    invoke-virtual/range {v0 .. v9}, LFb;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILEk;ZLDR;F)V

    .line 810
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LFb;->a(Landroid/graphics/Paint$FontMetricsInt;)F

    move-result v1

    .line 811
    invoke-static {v0}, LFb;->a(LFb;)LFb;

    .line 812
    return v1

    .line 797
    :cond_3c
    invoke-virtual {p0, p1}, LDO;->i(I)I

    move-result v4

    goto :goto_a
.end method

.method public a()I
    .registers 2

    .prologue
    .line 503
    iget v0, p0, LDO;->c:I

    return v0
.end method

.method public a(I)I
    .registers 6
    .parameter

    .prologue
    .line 819
    invoke-virtual {p0}, LDO;->d()I

    move-result v1

    const/4 v0, -0x1

    move v2, v1

    .line 821
    :goto_6
    sub-int v1, v2, v0

    const/4 v3, 0x1

    if-le v1, v3, :cond_19

    .line 822
    add-int v1, v2, v0

    div-int/lit8 v1, v1, 0x2

    .line 824
    invoke-virtual {p0, v1}, LDO;->b(I)I

    move-result v3

    if-le v3, p1, :cond_17

    move v2, v1

    .line 825
    goto :goto_6

    :cond_17
    move v0, v1

    .line 827
    goto :goto_6

    .line 830
    :cond_19
    if-gez v0, :cond_1c

    .line 831
    const/4 v0, 0x0

    .line 833
    :cond_1c
    return v0
.end method

.method public a(IF)I
    .registers 17
    .parameter
    .parameter

    .prologue
    .line 857
    invoke-virtual {p0, p1}, LDO;->h(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 858
    invoke-virtual {p0, p1}, LDO;->n(I)I

    move-result v4

    .line 859
    invoke-virtual {p0, p1}, LDO;->a(I)LEk;

    move-result-object v11

    .line 861
    invoke-virtual {p0}, LDO;->d()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_18

    add-int/lit8 v0, v0, 0x1

    .line 864
    :cond_18
    invoke-virtual {p0, v4}, LDO;->a(I)F

    move-result v1

    sub-float v1, v1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 866
    const/4 v1, 0x0

    move v3, v4

    :goto_24
    iget-object v5, v11, LEk;->a:[I

    array-length v5, v5

    if-ge v1, v5, :cond_bb

    .line 867
    iget-object v5, v11, LEk;->a:[I

    aget v5, v5, v1

    add-int v9, v4, v5

    .line 868
    iget-object v5, v11, LEk;->a:[I

    add-int/lit8 v6, v1, 0x1

    aget v5, v5, v6

    const v6, 0x3ffffff

    and-int/2addr v5, v6

    add-int/2addr v5, v9

    .line 869
    iget-object v6, v11, LEk;->a:[I

    add-int/lit8 v7, v1, 0x1

    aget v6, v6, v7

    const/high16 v7, 0x400

    and-int/2addr v6, v7

    if-eqz v6, :cond_6e

    const/4 v6, -0x1

    move v10, v6

    .line 871
    :goto_47
    if-le v5, v0, :cond_4a

    move v5, v0

    .line 872
    :cond_4a
    add-int/lit8 v6, v5, -0x1

    add-int/lit8 v8, v6, 0x1

    add-int/lit8 v6, v9, 0x1

    add-int/lit8 v6, v6, -0x1

    .line 874
    :goto_52
    sub-int v7, v8, v6

    const/4 v12, 0x1

    if-le v7, v12, :cond_74

    .line 875
    add-int v7, v8, v6

    div-int/lit8 v7, v7, 0x2

    .line 876
    invoke-direct {p0, v7}, LDO;->r(I)I

    move-result v12

    .line 878
    invoke-virtual {p0, v12}, LDO;->a(I)F

    move-result v12

    int-to-float v13, v10

    mul-float/2addr v12, v13

    int-to-float v13, v10

    mul-float v13, v13, p2

    cmpl-float v12, v12, v13

    if-ltz v12, :cond_71

    :goto_6c
    move v8, v7

    .line 882
    goto :goto_52

    .line 869
    :cond_6e
    const/4 v6, 0x1

    move v10, v6

    goto :goto_47

    :cond_71
    move v6, v7

    move v7, v8

    .line 881
    goto :goto_6c

    .line 884
    :cond_74
    add-int/lit8 v7, v9, 0x1

    if-ge v6, v7, :cond_7a

    add-int/lit8 v6, v9, 0x1

    .line 886
    :cond_7a
    if-ge v6, v5, :cond_a7

    .line 887
    invoke-direct {p0, v6}, LDO;->r(I)I

    move-result v8

    .line 889
    invoke-virtual {p0, v8}, LDO;->a(I)F

    move-result v6

    sub-float v6, v6, p2

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 891
    iget-object v7, p0, LDO;->a:Ljava/lang/CharSequence;

    invoke-static {v7, v8}, Landroid/text/TextUtils;->getOffsetAfter(Ljava/lang/CharSequence;I)I

    move-result v7

    .line 892
    if-ge v7, v5, :cond_cc

    .line 893
    invoke-virtual {p0, v7}, LDO;->a(I)F

    move-result v5

    sub-float v5, v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 895
    cmpg-float v10, v5, v6

    if-gez v10, :cond_cc

    move v6, v7

    .line 901
    :goto_a1
    cmpg-float v7, v5, v2

    if-gez v7, :cond_a7

    move v2, v5

    move v3, v6

    .line 907
    :cond_a7
    invoke-virtual {p0, v9}, LDO;->a(I)F

    move-result v5

    sub-float v5, v5, p2

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 909
    cmpg-float v6, v5, v2

    if-gez v6, :cond_b7

    move v2, v5

    move v3, v9

    .line 866
    :cond_b7
    add-int/lit8 v1, v1, 0x2

    goto/16 :goto_24

    .line 915
    :cond_bb
    invoke-virtual {p0, v0}, LDO;->a(I)F

    move-result v1

    sub-float v1, v1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 917
    cmpg-float v1, v1, v2

    if-gez v1, :cond_ca

    .line 922
    :goto_c9
    return v0

    :cond_ca
    move v0, v3

    goto :goto_c9

    :cond_cc
    move v5, v6

    move v6, v8

    goto :goto_a1
.end method

.method public a(ILandroid/graphics/Rect;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 542
    if-eqz p2, :cond_17

    .line 543
    const/4 v0, 0x0

    iput v0, p2, Landroid/graphics/Rect;->left:I

    .line 544
    invoke-virtual {p0, p1}, LDO;->b(I)I

    move-result v0

    iput v0, p2, Landroid/graphics/Rect;->top:I

    .line 545
    iget v0, p0, LDO;->c:I

    iput v0, p2, Landroid/graphics/Rect;->right:I

    .line 546
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, LDO;->b(I)I

    move-result v0

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 548
    :cond_17
    invoke-virtual {p0, p1}, LDO;->e(I)I

    move-result v0

    return v0
.end method

.method public final a()Landroid/text/Layout$Alignment;
    .registers 2

    .prologue
    .line 527
    iget-object v0, p0, LDO;->a:Landroid/text/Layout$Alignment;

    return-object v0
.end method

.method public final a(I)Landroid/text/Layout$Alignment;
    .registers 7
    .parameter

    .prologue
    .line 1270
    iget-object v1, p0, LDO;->a:Landroid/text/Layout$Alignment;

    .line 1272
    iget-boolean v0, p0, LDO;->a:Z

    if-eqz v0, :cond_26

    .line 1273
    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    .line 1274
    invoke-virtual {p0, p1}, LDO;->n(I)I

    move-result v2

    invoke-virtual {p0, p1}, LDO;->h(I)I

    move-result v3

    const-class v4, Landroid/text/style/AlignmentSpan;

    invoke-static {v0, v2, v3, v4}, LDO;->a(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/AlignmentSpan;

    .line 1277
    array-length v2, v0

    .line 1278
    if-lez v2, :cond_26

    .line 1279
    add-int/lit8 v1, v2, -0x1

    aget-object v0, v0, v1

    invoke-interface {v0}, Landroid/text/style/AlignmentSpan;->getAlignment()Landroid/text/Layout$Alignment;

    move-result-object v0

    .line 1283
    :goto_25
    return-object v0

    :cond_26
    move-object v0, v1

    goto :goto_25
.end method

.method public final a()Landroid/text/TextPaint;
    .registers 2

    .prologue
    .line 498
    iget-object v0, p0, LDO;->b:Landroid/text/TextPaint;

    return-object v0
.end method

.method public a(I)Landroid/util/Pair;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1095
    invoke-virtual {p0, p1}, LDO;->g(I)I

    move-result v0

    .line 1096
    invoke-virtual {p0, v0}, LDO;->e(I)I

    move-result v2

    .line 1097
    iget-boolean v0, p0, LDO;->a:Z

    if-nez v0, :cond_2b

    .line 1098
    new-instance v0, Landroid/util/Pair;

    int-to-float v1, v2

    iget-object v3, p0, LDO;->b:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    add-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    int-to-float v2, v2

    iget-object v3, p0, LDO;->b:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1117
    :goto_2a
    return-object v0

    .line 1100
    :cond_2b
    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    .line 1102
    if-eqz p1, :cond_3d

    add-int/lit8 v3, p1, -0x1

    invoke-interface {v0, v3}, Landroid/text/Spanned;->charAt(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_3d

    .line 1103
    add-int/lit8 p1, p1, -0x1

    .line 1105
    :cond_3d
    add-int/lit8 v3, p1, 0x1

    const-class v4, Landroid/text/style/MetricAffectingSpan;

    invoke-interface {v0, p1, v3, v4}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/MetricAffectingSpan;

    .line 1106
    array-length v3, v0

    if-nez v3, :cond_68

    .line 1107
    new-instance v0, Landroid/util/Pair;

    int-to-float v1, v2

    iget-object v3, p0, LDO;->b:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    add-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    int-to-float v2, v2

    iget-object v3, p0, LDO;->b:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2a

    .line 1110
    :cond_68
    iget-object v3, p0, LDO;->a:Landroid/text/TextPaint;

    .line 1111
    iget-object v4, p0, LDO;->b:Landroid/text/TextPaint;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->set(Landroid/text/TextPaint;)V

    .line 1112
    iput v1, v3, Landroid/text/TextPaint;->baselineShift:I

    .line 1113
    array-length v4, v0

    :goto_72
    if-ge v1, v4, :cond_7c

    aget-object v5, v0, v1

    .line 1114
    invoke-virtual {v5, v3}, Landroid/text/style/MetricAffectingSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    .line 1113
    add-int/lit8 v1, v1, 0x1

    goto :goto_72

    .line 1116
    :cond_7c
    iget v0, v3, Landroid/text/TextPaint;->baselineShift:I

    add-int v1, v2, v0

    .line 1117
    new-instance v0, Landroid/util/Pair;

    int-to-float v2, v1

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    add-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    int-to-float v1, v1

    invoke-virtual {v3}, Landroid/text/TextPaint;->descent()F

    move-result v3

    add-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2a
.end method

.method public final a()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 493
    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 1657
    iget-object v0, p0, LDO;->a:LFc;

    if-eqz v0, :cond_9

    .line 1658
    iget-object v0, p0, LDO;->a:LFc;

    invoke-interface {v0}, LFc;->a()V

    .line 1660
    :cond_9
    return-void
.end method

.method public a(F)V
    .registers 2
    .parameter

    .prologue
    .line 161
    iput p1, p0, LDO;->a:F

    .line 162
    return-void
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 513
    iget v0, p0, LDO;->c:I

    if-ge p1, v0, :cond_c

    .line 514
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "attempted to reduce Layout width"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 517
    :cond_c
    iput p1, p0, LDO;->c:I

    .line 518
    return-void
.end method

.method public a(IILandroid/graphics/Path;)V
    .registers 20
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1221
    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Path;->reset()V

    .line 1223
    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_a

    .line 1266
    :goto_9
    return-void

    .line 1225
    :cond_a
    move/from16 v0, p2

    move/from16 v1, p1

    if-ge v0, v1, :cond_ec

    move/from16 v5, p1

    move/from16 v4, p2

    .line 1231
    :goto_14
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LDO;->g(I)I

    move-result v3

    .line 1232
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, LDO;->g(I)I

    move-result v14

    .line 1234
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->b(I)I

    move-result v6

    .line 1235
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LDO;->d(I)I

    move-result v7

    .line 1237
    if-ne v3, v14, :cond_36

    move-object/from16 v2, p0

    move-object/from16 v8, p3

    .line 1238
    invoke-direct/range {v2 .. v8}, LDO;->a(IIIIILandroid/graphics/Path;)V

    goto :goto_9

    .line 1240
    :cond_36
    move-object/from16 v0, p0

    iget v2, v0, LDO;->c:I

    int-to-float v15, v2

    .line 1242
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->h(I)I

    move-result v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->d(I)I

    move-result v12

    move-object/from16 v7, p0

    move v8, v3

    move v9, v4

    move v11, v6

    move-object/from16 v13, p3

    invoke-direct/range {v7 .. v13}, LDO;->a(IIIIILandroid/graphics/Path;)V

    .line 1244
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->o(I)I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_8e

    .line 1245
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->c(I)F

    move-result v7

    int-to-float v8, v6

    const/4 v9, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->d(I)I

    move-result v2

    int-to-float v10, v2

    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object/from16 v6, p3

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 1250
    :goto_70
    add-int/lit8 v2, v3, 0x1

    :goto_72
    if-ge v2, v14, :cond_a5

    .line 1251
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LDO;->b(I)I

    move-result v3

    .line 1252
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LDO;->d(I)I

    move-result v4

    .line 1253
    const/4 v7, 0x0

    int-to-float v8, v3

    int-to-float v10, v4

    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object/from16 v6, p3

    move v9, v15

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 1250
    add-int/lit8 v2, v2, 0x1

    goto :goto_72

    .line 1247
    :cond_8e
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->d(I)F

    move-result v7

    int-to-float v8, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->d(I)I

    move-result v2

    int-to-float v10, v2

    sget-object v11, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object/from16 v6, p3

    move v9, v15

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    goto :goto_70

    .line 1256
    :cond_a5
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LDO;->b(I)I

    move-result v6

    .line 1257
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LDO;->d(I)I

    move-result v7

    .line 1259
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LDO;->n(I)I

    move-result v4

    move-object/from16 v2, p0

    move v3, v14

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v8}, LDO;->a(IIIIILandroid/graphics/Path;)V

    .line 1261
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LDO;->o(I)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_da

    .line 1262
    int-to-float v4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LDO;->d(I)F

    move-result v5

    int-to-float v6, v7

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object/from16 v2, p3

    move v3, v15

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    goto/16 :goto_9

    .line 1264
    :cond_da
    const/4 v3, 0x0

    int-to-float v4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LDO;->c(I)F

    move-result v5

    int-to-float v6, v7

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    move-object/from16 v2, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    goto/16 :goto_9

    :cond_ec
    move/from16 v5, p2

    move/from16 v4, p1

    goto/16 :goto_14
.end method

.method public a(ILandroid/graphics/Path;Ljava/lang/CharSequence;)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/high16 v2, 0x3f00

    .line 1122
    invoke-virtual {p2}, Landroid/graphics/Path;->reset()V

    .line 1124
    invoke-virtual {p0, p1}, LDO;->a(I)Landroid/util/Pair;

    move-result-object v1

    .line 1126
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v5

    .line 1127
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v4

    .line 1129
    invoke-virtual {p0, p1}, LDO;->a(I)F

    move-result v0

    sub-float v1, v0, v2

    .line 1130
    invoke-virtual {p0, p1}, LDO;->a(I)Z

    move-result v0

    if-eqz v0, :cond_98

    invoke-virtual {p0, p1}, LDO;->b(I)F

    move-result v0

    sub-float/2addr v0, v2

    .line 1132
    :goto_2c
    invoke-static {p3, v9}, LGo;->a(Ljava/lang/CharSequence;I)I

    move-result v3

    const/16 v6, 0x800

    invoke-static {p3, v6}, LGo;->a(Ljava/lang/CharSequence;I)I

    move-result v6

    or-int/2addr v6, v3

    .line 1135
    invoke-static {p3, v10}, LGo;->a(Ljava/lang/CharSequence;I)I

    move-result v7

    .line 1136
    const/4 v3, 0x0

    .line 1138
    if-nez v6, :cond_40

    if-eqz v7, :cond_4a

    .line 1139
    :cond_40
    sub-int v3, v5, v4

    shr-int/lit8 v3, v3, 0x2

    .line 1141
    if-eqz v7, :cond_47

    add-int/2addr v4, v3

    .line 1142
    :cond_47
    if-eqz v6, :cond_4a

    sub-int/2addr v5, v3

    .line 1145
    :cond_4a
    cmpg-float v8, v1, v2

    if-gez v8, :cond_4f

    move v1, v2

    .line 1146
    :cond_4f
    cmpg-float v8, v0, v2

    if-gez v8, :cond_54

    move v0, v2

    .line 1148
    :cond_54
    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v8

    if-nez v8, :cond_9a

    .line 1149
    int-to-float v8, v4

    invoke-virtual {p2, v1, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1150
    int-to-float v8, v5

    invoke-virtual {p2, v1, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1159
    :goto_62
    if-ne v6, v10, :cond_b3

    .line 1160
    int-to-float v6, v5

    invoke-virtual {p2, v0, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1161
    int-to-float v6, v3

    sub-float v6, v0, v6

    add-int v8, v5, v3

    int-to-float v8, v8

    invoke-virtual {p2, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1162
    int-to-float v6, v5

    invoke-virtual {p2, v0, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1163
    int-to-float v6, v3

    add-float/2addr v0, v6

    add-int/2addr v5, v3

    int-to-float v5, v5

    invoke-virtual {p2, v0, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1175
    :cond_7c
    :goto_7c
    if-ne v7, v10, :cond_e2

    .line 1176
    int-to-float v0, v4

    invoke-virtual {p2, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1177
    int-to-float v0, v3

    sub-float v0, v1, v0

    sub-int v2, v4, v3

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1178
    int-to-float v0, v4

    invoke-virtual {p2, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1179
    int-to-float v0, v3

    add-float/2addr v0, v1

    sub-int v1, v4, v3

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1190
    :cond_97
    :goto_97
    return-void

    :cond_98
    move v0, v1

    .line 1130
    goto :goto_2c

    .line 1152
    :cond_9a
    int-to-float v8, v4

    invoke-virtual {p2, v1, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1153
    add-int v8, v4, v5

    shr-int/lit8 v8, v8, 0x1

    int-to-float v8, v8

    invoke-virtual {p2, v1, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1155
    add-int v8, v4, v5

    shr-int/lit8 v8, v8, 0x1

    int-to-float v8, v8

    invoke-virtual {p2, v0, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1156
    int-to-float v8, v5

    invoke-virtual {p2, v0, v8}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_62

    .line 1164
    :cond_b3
    if-ne v6, v9, :cond_7c

    .line 1165
    int-to-float v6, v5

    invoke-virtual {p2, v0, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1166
    int-to-float v6, v3

    sub-float v6, v0, v6

    add-int v8, v5, v3

    int-to-float v8, v8

    invoke-virtual {p2, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1168
    int-to-float v6, v3

    sub-float v6, v0, v6

    add-int v8, v5, v3

    int-to-float v8, v8

    sub-float/2addr v8, v2

    invoke-virtual {p2, v6, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1169
    int-to-float v6, v3

    add-float/2addr v6, v0

    add-int v8, v5, v3

    int-to-float v8, v8

    sub-float/2addr v8, v2

    invoke-virtual {p2, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1171
    int-to-float v6, v3

    add-float/2addr v6, v0

    add-int v8, v5, v3

    int-to-float v8, v8

    invoke-virtual {p2, v6, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1172
    int-to-float v5, v5

    invoke-virtual {p2, v0, v5}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_7c

    .line 1180
    :cond_e2
    if-ne v7, v9, :cond_97

    .line 1181
    int-to-float v0, v4

    invoke-virtual {p2, v1, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1182
    int-to-float v0, v3

    sub-float v0, v1, v0

    sub-int v5, v4, v3

    int-to-float v5, v5

    invoke-virtual {p2, v0, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1184
    int-to-float v0, v3

    sub-float v0, v1, v0

    sub-int v5, v4, v3

    int-to-float v5, v5

    add-float/2addr v5, v2

    invoke-virtual {p2, v0, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1185
    int-to-float v0, v3

    add-float/2addr v0, v1

    sub-int v5, v4, v3

    int-to-float v5, v5

    add-float/2addr v2, v5

    invoke-virtual {p2, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1187
    int-to-float v0, v3

    add-float/2addr v0, v1

    sub-int v2, v4, v3

    int-to-float v2, v2

    invoke-virtual {p2, v0, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1188
    int-to-float v0, v4

    invoke-virtual {p2, v1, v0}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_97
.end method

.method public a(LFc;)V
    .registers 2
    .parameter

    .prologue
    .line 1652
    iput-object p1, p0, LDO;->a:LFc;

    .line 1653
    return-void
.end method

.method public a(LFf;II)V
    .registers 52
    .parameter
    .parameter
    .parameter
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 202
    const/4 v4, 0x0

    .line 203
    invoke-virtual/range {p0 .. p0}, LDO;->d()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->b(I)I

    move-result v3

    .line 205
    move/from16 v0, p2

    if-le v0, v4, :cond_2f8

    .line 208
    :goto_f
    move/from16 v0, p3

    if-ge v0, v3, :cond_2f4

    .line 212
    :goto_13
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, LDO;->a(I)I

    move-result v37

    .line 213
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, LDO;->a(I)I

    move-result v42

    .line 215
    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v1}, LDO;->b(I)I

    move-result v9

    .line 216
    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v1}, LDO;->n(I)I

    move-result v8

    .line 218
    move-object/from16 v0, p0

    iget-object v5, v0, LDO;->b:Landroid/text/TextPaint;

    .line 219
    move-object/from16 v0, p0

    iget-object v11, v0, LDO;->a:Ljava/lang/CharSequence;

    .line 220
    move-object/from16 v0, p0

    iget v7, v0, LDO;->c:I

    .line 221
    move-object/from16 v0, p0

    iget-boolean v0, v0, LDO;->a:Z

    move/from16 v43, v0

    .line 223
    sget-object v6, LDO;->a:[Landroid/text/style/ParagraphStyle;

    .line 224
    const/4 v4, 0x0

    .line 225
    const/4 v3, 0x0

    .line 231
    if-eqz v43, :cond_2ea

    move-object v15, v11

    .line 232
    check-cast v15, Landroid/text/Spanned;

    .line 233
    move-object/from16 v0, p0

    move/from16 v1, v42

    invoke-virtual {v0, v1}, LDO;->h(I)I

    move-result v19

    move/from16 v14, v37

    move v3, v4

    move v12, v8

    move-object v4, v6

    move v8, v9

    .line 234
    :goto_5c
    move/from16 v0, v42

    if-gt v14, v0, :cond_b0

    .line 236
    add-int/lit8 v6, v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LDO;->n(I)I

    move-result v13

    .line 240
    add-int/lit8 v6, v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, LDO;->b(I)I

    move-result v10

    .line 242
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, LDO;->c(I)I

    move-result v6

    sub-int v9, v10, v6

    .line 244
    if-lt v12, v3, :cond_2e4

    .line 247
    const-class v3, LGB;

    move/from16 v0, v19

    invoke-interface {v15, v12, v0, v3}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v4

    .line 250
    const-class v3, LGB;

    invoke-static {v15, v12, v13, v3}, LDO;->a(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/text/style/ParagraphStyle;

    move/from16 v16, v4

    move-object/from16 v17, v3

    .line 253
    :goto_8e
    const/4 v3, 0x0

    move/from16 v18, v3

    :goto_91
    move-object/from16 v0, v17

    array-length v3, v0

    move/from16 v0, v18

    if-ge v0, v3, :cond_a7

    .line 254
    aget-object v3, v17, v18

    check-cast v3, LGB;

    .line 256
    const/4 v6, 0x0

    move-object/from16 v4, p1

    invoke-interface/range {v3 .. v14}, LGB;->a(LFf;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;III)V

    .line 253
    add-int/lit8 v3, v18, 0x1

    move/from16 v18, v3

    goto :goto_91

    .line 234
    :cond_a7
    add-int/lit8 v14, v14, 0x1

    move/from16 v3, v16

    move-object/from16 v4, v17

    move v12, v13

    move v8, v10

    goto :goto_5c

    .line 261
    :cond_b0
    const/4 v3, 0x0

    .line 262
    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v1}, LDO;->b(I)I

    move-result v7

    .line 263
    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-virtual {v0, v1}, LDO;->n(I)I

    move-result v6

    .line 264
    sget-object v4, LDO;->a:[Landroid/text/style/ParagraphStyle;

    move v8, v7

    move v7, v6

    move/from16 v6, v19

    .line 267
    :goto_c7
    move-object/from16 v0, p0

    iget-object v13, v0, LDO;->a:Landroid/text/Layout$Alignment;

    .line 268
    const/4 v14, 0x0

    .line 269
    const/4 v9, 0x0

    .line 271
    invoke-static {}, LFb;->a()LFb;

    move-result-object v44

    move/from16 v15, v37

    move/from16 v38, v7

    move/from16 v40, v8

    move-object v8, v4

    move v7, v9

    move v4, v3

    .line 276
    :goto_da
    move/from16 v0, v42

    if-gt v15, v0, :cond_2c9

    .line 279
    add-int/lit8 v3, v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->n(I)I

    move-result v39

    .line 280
    move-object/from16 v0, p0

    move/from16 v1, v38

    move/from16 v2, v39

    invoke-direct {v0, v15, v1, v2}, LDO;->b(III)I

    move-result v19

    .line 283
    add-int/lit8 v3, v15, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LDO;->b(I)I

    move-result v41

    .line 285
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, LDO;->c(I)I

    move-result v3

    sub-int v45, v41, v3

    .line 287
    const/16 v31, 0x0

    .line 288
    if-eqz v43, :cond_2e0

    move-object v3, v11

    .line 289
    check-cast v3, Landroid/text/Spanned;

    .line 290
    if-eqz v38, :cond_113

    add-int/lit8 v9, v38, -0x1

    invoke-interface {v11, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v9

    const/16 v10, 0xa

    if-ne v9, v10, :cond_205

    :cond_113
    const/16 v31, 0x1

    .line 302
    :goto_115
    move/from16 v0, v38

    if-lt v0, v4, :cond_2e0

    move/from16 v0, v37

    if-eq v15, v0, :cond_11f

    if-eqz v31, :cond_2e0

    .line 303
    :cond_11f
    const-class v4, Landroid/text/style/ParagraphStyle;

    move/from16 v0, v38

    invoke-interface {v3, v0, v6, v4}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v8

    .line 304
    const-class v4, Landroid/text/style/ParagraphStyle;

    move/from16 v0, v38

    invoke-static {v3, v0, v8, v4}, LDO;->a(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/text/style/ParagraphStyle;

    .line 306
    move-object/from16 v0, p0

    iget-object v4, v0, LDO;->a:Landroid/text/Layout$Alignment;

    .line 307
    array-length v7, v3

    add-int/lit8 v7, v7, -0x1

    :goto_138
    if-ltz v7, :cond_148

    .line 308
    aget-object v9, v3, v7

    instance-of v9, v9, Landroid/text/style/AlignmentSpan;

    if-eqz v9, :cond_209

    .line 309
    aget-object v4, v3, v7

    check-cast v4, Landroid/text/style/AlignmentSpan;

    invoke-interface {v4}, Landroid/text/style/AlignmentSpan;->getAlignment()Landroid/text/Layout$Alignment;

    move-result-object v4

    .line 314
    :cond_148
    const/4 v7, 0x0

    move-object v13, v4

    move-object v9, v3

    .line 320
    :goto_14b
    const/4 v3, 0x0

    .line 322
    move-object/from16 v0, p0

    iget-object v4, v0, LDO;->a:LFc;

    if-eqz v4, :cond_2da

    .line 324
    move-object/from16 v0, p0

    iget-object v3, v0, LDO;->a:LFc;

    invoke-interface {v3, v15}, LFc;->a(I)LEz;

    move-result-object v3

    .line 325
    if-nez v3, :cond_20d

    .line 326
    new-instance v3, LEz;

    invoke-direct {v3}, LEz;-><init>()V

    .line 327
    invoke-interface/range {p1 .. p1}, LFf;->a()LFe;

    move-result-object v4

    invoke-virtual {v3, v4}, LEz;->a(LFe;)V

    .line 328
    move-object/from16 v0, p0

    iget-object v4, v0, LDO;->a:LFc;

    invoke-interface {v4, v15, v3}, LFc;->a(ILEz;)V

    move-object/from16 v35, v3

    move-object/from16 v36, v3

    .line 336
    :goto_173
    if-eqz v36, :cond_27e

    .line 337
    move/from16 v0, v45

    int-to-float v3, v0

    move-object/from16 v0, v36

    invoke-interface {v0, v15, v3}, LFf;->a(IF)V

    .line 339
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, LDO;->o(I)I

    move-result v16

    .line 340
    const/16 v17, 0x0

    .line 341
    move-object/from16 v0, p0

    iget v0, v0, LDO;->c:I

    move/from16 v18, v0

    .line 343
    if-eqz v43, :cond_239

    move-object v3, v11

    .line 344
    check-cast v3, Landroid/text/Spanned;

    .line 347
    array-length v0, v9

    move/from16 v46, v0

    .line 348
    const/4 v4, 0x0

    move/from16 v34, v4

    move/from16 v23, v18

    move/from16 v10, v17

    :goto_19a
    move/from16 v0, v34

    move/from16 v1, v46

    if-ge v0, v1, :cond_235

    .line 349
    aget-object v4, v9, v34

    instance-of v4, v4, LGz;

    if-eqz v4, :cond_2d4

    .line 350
    aget-object v20, v9, v34

    check-cast v20, LGz;

    .line 352
    move-object/from16 v0, v20

    instance-of v4, v0, LGA;

    if-eqz v4, :cond_2d0

    move-object/from16 v4, v20

    .line 353
    check-cast v4, LGA;

    invoke-interface {v4}, LGA;->a()I

    move-result v4

    .line 354
    move-object/from16 v0, v20

    invoke-interface {v3, v0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, LDO;->g(I)I

    move-result v12

    .line 355
    add-int/2addr v4, v12

    if-ge v15, v4, :cond_214

    const/4 v4, 0x1

    .line 358
    :goto_1c8
    move-object/from16 v0, v20

    invoke-interface {v0, v4}, LGz;->a(Z)I

    move-result v4

    .line 360
    const/4 v12, -0x1

    move/from16 v0, v16

    if-ne v0, v12, :cond_216

    .line 362
    sub-int v18, v23, v4

    move/from16 v17, v10

    .line 369
    :goto_1d7
    move-object/from16 v0, v20

    instance-of v4, v0, LDL;

    if-eqz v4, :cond_21d

    move-object/from16 v12, p0

    .line 370
    invoke-direct/range {v12 .. v18}, LDO;->a(Landroid/text/Layout$Alignment;LDR;IIII)I

    move-result v33

    .line 371
    check-cast v20, LDL;

    move-object/from16 v21, v36

    move-object/from16 v22, v5

    move/from16 v24, v16

    move/from16 v25, v40

    move/from16 v26, v45

    move/from16 v27, v41

    move-object/from16 v28, v11

    move/from16 v29, v38

    move/from16 v30, v19

    move-object/from16 v32, p0

    invoke-interface/range {v20 .. v33}, LDL;->a(LFf;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLEj;I)V

    .line 348
    :goto_1fc
    add-int/lit8 v4, v34, 0x1

    move/from16 v34, v4

    move/from16 v23, v18

    move/from16 v10, v17

    goto :goto_19a

    .line 290
    :cond_205
    const/16 v31, 0x0

    goto/16 :goto_115

    .line 307
    :cond_209
    add-int/lit8 v7, v7, -0x1

    goto/16 :goto_138

    .line 332
    :cond_20d
    const/4 v4, 0x0

    move-object/from16 v35, v3

    move-object/from16 v36, v4

    goto/16 :goto_173

    .line 355
    :cond_214
    const/4 v4, 0x0

    goto :goto_1c8

    .line 365
    :cond_216
    add-int v17, v10, v4

    move/from16 v18, v23

    move/from16 v23, v10

    goto :goto_1d7

    :cond_21d
    move-object/from16 v21, v36

    move-object/from16 v22, v5

    move/from16 v24, v16

    move/from16 v25, v40

    move/from16 v26, v45

    move/from16 v27, v41

    move-object/from16 v28, v11

    move/from16 v29, v38

    move/from16 v30, v19

    move-object/from16 v32, p0

    .line 375
    invoke-interface/range {v20 .. v32}, LGz;->a(LFf;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLEj;)V

    goto :goto_1fc

    :cond_235
    move/from16 v18, v23

    move/from16 v17, v10

    .line 383
    :cond_239
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, LDO;->c(I)Z

    move-result v27

    .line 385
    if-eqz v27, :cond_2cd

    if-nez v7, :cond_2cd

    .line 386
    if-nez v14, :cond_295

    .line 387
    new-instance v14, LDR;

    const/16 v3, 0x14

    invoke-direct {v14, v3, v9}, LDR;-><init>(I[Ljava/lang/Object;)V

    .line 391
    :goto_24c
    const/4 v3, 0x1

    :goto_24d
    move-object/from16 v12, p0

    .line 394
    invoke-direct/range {v12 .. v18}, LDO;->a(Landroid/text/Layout$Alignment;LDR;IIII)I

    move-result v4

    .line 396
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, LDO;->a(I)LEk;

    move-result-object v26

    .line 397
    sget-object v7, LDO;->a:LEk;

    move-object/from16 v0, v26

    if-ne v0, v7, :cond_29b

    if-nez v43, :cond_29b

    if-nez v27, :cond_29b

    .line 399
    int-to-float v0, v4

    move/from16 v20, v0

    move/from16 v0, v45

    int-to-float v0, v0

    move/from16 v21, v0

    const/16 v23, 0x0

    move-object/from16 v16, v36

    move-object/from16 v17, v11

    move/from16 v18, v38

    move-object/from16 v22, v5

    invoke-interface/range {v16 .. v23}, LFf;->a(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)V

    .line 407
    :goto_278
    move-object/from16 v0, v36

    invoke-interface {v0, v15}, LFf;->a(I)V

    move v7, v3

    .line 411
    :cond_27e
    if-eqz v35, :cond_28b

    .line 412
    invoke-virtual/range {p0 .. p0}, LDO;->a()F

    move-result v3

    move-object/from16 v0, v35

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, LEz;->a(LFf;F)V

    .line 276
    :cond_28b
    add-int/lit8 v15, v15, 0x1

    move v4, v8

    move/from16 v38, v39

    move/from16 v40, v41

    move-object v8, v9

    goto/16 :goto_da

    .line 389
    :cond_295
    const/16 v3, 0x14

    invoke-virtual {v14, v3, v9}, LDR;->a(I[Ljava/lang/Object;)V

    goto :goto_24c

    .line 401
    :cond_29b
    move-object/from16 v0, p0

    iget v7, v0, LDO;->c:I

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v15, v7, v1}, LDO;->a(III)F

    move-result v29

    move-object/from16 v20, v44

    move-object/from16 v21, v5

    move-object/from16 v22, v11

    move/from16 v23, v38

    move/from16 v24, v19

    move/from16 v25, v16

    move-object/from16 v28, v14

    .line 402
    invoke-virtual/range {v20 .. v29}, LFb;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;IIILEk;ZLDR;F)V

    .line 404
    int-to-float v0, v4

    move/from16 v18, v0

    move-object/from16 v16, v44

    move-object/from16 v17, v36

    move/from16 v19, v40

    move/from16 v20, v45

    move/from16 v21, v41

    invoke-virtual/range {v16 .. v21}, LFb;->a(LFf;FIII)V

    goto :goto_278

    .line 416
    :cond_2c9
    invoke-static/range {v44 .. v44}, LFb;->a(LFb;)LFb;

    .line 417
    return-void

    :cond_2cd
    move v3, v7

    goto/16 :goto_24d

    :cond_2d0
    move/from16 v4, v31

    goto/16 :goto_1c8

    :cond_2d4
    move/from16 v18, v23

    move/from16 v17, v10

    goto/16 :goto_1fc

    :cond_2da
    move-object/from16 v35, v3

    move-object/from16 v36, p1

    goto/16 :goto_173

    :cond_2e0
    move-object v9, v8

    move v8, v4

    goto/16 :goto_14b

    :cond_2e4
    move/from16 v16, v3

    move-object/from16 v17, v4

    goto/16 :goto_8e

    :cond_2ea
    move v7, v8

    move v8, v9

    move-object/from16 v47, v6

    move v6, v3

    move v3, v4

    move-object/from16 v4, v47

    goto/16 :goto_c7

    :cond_2f4
    move/from16 p3, v3

    goto/16 :goto_13

    :cond_2f8
    move/from16 p2, v4

    goto/16 :goto_f
.end method

.method public a(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 166
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v1, v1, v0}, LDO;->a(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    .line 167
    return-void
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 173
    sget-object v1, LDO;->a:Landroid/graphics/Rect;

    monitor-enter v1

    .line 174
    :try_start_4
    sget-object v0, LDO;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->getClipBounds(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 175
    monitor-exit v1

    .line 197
    :cond_d
    :goto_d
    return-void

    .line 178
    :cond_e
    sget-object v0, LDO;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    .line 179
    sget-object v2, LDO;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 180
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_33

    .line 182
    iget-object v1, p0, LDO;->a:LFg;

    invoke-interface {v1, p1}, LFg;->a(Landroid/graphics/Canvas;)LFf;

    move-result-object v1

    invoke-virtual {p0, v1, v0, v2}, LDO;->a(LFf;II)V

    .line 186
    if-eqz p2, :cond_d

    .line 187
    if-eqz p4, :cond_28

    .line 188
    int-to-float v0, p4

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 191
    :cond_28
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 193
    if-eqz p4, :cond_d

    .line 194
    neg-int v0, p4

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_d

    .line 180
    :catchall_33
    move-exception v0

    :try_start_34
    monitor-exit v1
    :try_end_35
    .catchall {:try_start_34 .. :try_end_35} :catchall_33

    throw v0
.end method

.method public a(I)Z
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 565
    invoke-virtual {p0, p1}, LDO;->g(I)I

    move-result v0

    .line 566
    invoke-virtual {p0, v0}, LDO;->a(I)LEk;

    move-result-object v3

    .line 567
    sget-object v4, LDO;->a:LEk;

    if-eq v3, v4, :cond_12

    sget-object v4, LDO;->b:LEk;

    if-ne v3, v4, :cond_13

    .line 586
    :cond_12
    :goto_12
    return v1

    .line 571
    :cond_13
    iget-object v4, v3, LEk;->a:[I

    .line 572
    invoke-virtual {p0, v0}, LDO;->n(I)I

    move-result v5

    .line 573
    invoke-virtual {p0, v0}, LDO;->h(I)I

    move-result v3

    .line 574
    if-eq p1, v5, :cond_21

    if-ne p1, v3, :cond_3d

    .line 575
    :cond_21
    invoke-virtual {p0, v0}, LDO;->o(I)I

    move-result v0

    if-ne v0, v2, :cond_37

    move v3, v1

    .line 576
    :goto_28
    if-ne p1, v5, :cond_39

    move v0, v1

    .line 577
    :goto_2b
    add-int/lit8 v0, v0, 0x1

    aget v0, v4, v0

    ushr-int/lit8 v0, v0, 0x1a

    and-int/lit8 v0, v0, 0x3f

    if-eq v0, v3, :cond_12

    move v1, v2

    goto :goto_12

    :cond_37
    move v3, v2

    .line 575
    goto :goto_28

    .line 576
    :cond_39
    array-length v0, v4

    add-int/lit8 v0, v0, -0x2

    goto :goto_2b

    .line 580
    :cond_3d
    sub-int v3, p1, v5

    move v0, v1

    .line 581
    :goto_40
    array-length v5, v4

    if-ge v0, v5, :cond_12

    .line 582
    aget v5, v4, v0

    if-ne v3, v5, :cond_49

    move v1, v2

    .line 583
    goto :goto_12

    .line 581
    :cond_49
    add-int/lit8 v0, v0, 0x2

    goto :goto_40
.end method

.method public final b()F
    .registers 2

    .prologue
    .line 532
    iget v0, p0, LDO;->b:F

    return v0
.end method

.method public b(I)F
    .registers 3
    .parameter

    .prologue
    .line 646
    invoke-direct {p0, p1}, LDO;->d(I)Z

    move-result v0

    .line 647
    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_7
    invoke-direct {p0, p1, v0}, LDO;->a(IZ)F

    move-result v0

    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public b()I
    .registers 2

    .prologue
    .line 508
    iget v0, p0, LDO;->c:I

    return v0
.end method

.method public final c()F
    .registers 2

    .prologue
    .line 537
    iget v0, p0, LDO;->c:F

    return v0
.end method

.method public c(I)F
    .registers 7
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v0, 0x0

    .line 688
    invoke-virtual {p0, p1}, LDO;->o(I)I

    move-result v1

    .line 689
    invoke-virtual {p0, p1}, LDO;->a(I)Landroid/text/Layout$Alignment;

    move-result-object v2

    .line 691
    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    if-ne v2, v3, :cond_1b

    .line 692
    if-ne v1, v4, :cond_1a

    .line 693
    invoke-virtual {p0, p1}, LDO;->m(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, p1}, LDO;->f(I)F

    move-result v1

    sub-float/2addr v0, v1

    .line 706
    :cond_1a
    :goto_1a
    return v0

    .line 696
    :cond_1b
    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    if-ne v2, v3, :cond_2a

    .line 697
    if-eq v1, v4, :cond_1a

    .line 700
    iget v0, p0, LDO;->c:I

    int-to-float v0, v0

    invoke-virtual {p0, p1}, LDO;->f(I)F

    move-result v1

    sub-float/2addr v0, v1

    goto :goto_1a

    .line 702
    :cond_2a
    invoke-virtual {p0, p1}, LDO;->l(I)I

    move-result v0

    .line 703
    invoke-virtual {p0, p1}, LDO;->m(I)I

    move-result v1

    .line 704
    invoke-virtual {p0, p1}, LDO;->f(I)F

    move-result v2

    float-to-int v2, v2

    and-int/lit8 v2, v2, -0x2

    .line 706
    sub-int/2addr v1, v0

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    int-to-float v0, v0

    goto :goto_1a
.end method

.method public c()I
    .registers 2

    .prologue
    .line 522
    invoke-virtual {p0}, LDO;->d()I

    move-result v0

    invoke-virtual {p0, v0}, LDO;->b(I)I

    move-result v0

    return v0
.end method

.method public d(I)F
    .registers 6
    .parameter

    .prologue
    const/4 v3, -0x1

    .line 712
    invoke-virtual {p0, p1}, LDO;->o(I)I

    move-result v0

    .line 713
    invoke-virtual {p0, p1}, LDO;->a(I)Landroid/text/Layout$Alignment;

    move-result-object v1

    .line 715
    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    if-ne v1, v2, :cond_1e

    .line 716
    if-ne v0, v3, :cond_13

    .line 717
    iget v0, p0, LDO;->c:I

    int-to-float v0, v0

    .line 730
    :goto_12
    return v0

    .line 719
    :cond_13
    invoke-virtual {p0, p1}, LDO;->l(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0, p1}, LDO;->f(I)F

    move-result v1

    add-float/2addr v0, v1

    goto :goto_12

    .line 720
    :cond_1e
    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    if-ne v1, v2, :cond_2d

    .line 721
    if-ne v0, v3, :cond_29

    .line 722
    invoke-virtual {p0, p1}, LDO;->f(I)F

    move-result v0

    goto :goto_12

    .line 724
    :cond_29
    iget v0, p0, LDO;->c:I

    int-to-float v0, v0

    goto :goto_12

    .line 726
    :cond_2d
    invoke-virtual {p0, p1}, LDO;->l(I)I

    move-result v0

    .line 727
    invoke-virtual {p0, p1}, LDO;->m(I)I

    move-result v1

    .line 728
    invoke-virtual {p0, p1}, LDO;->f(I)F

    move-result v2

    float-to-int v2, v2

    and-int/lit8 v2, v2, -0x2

    .line 730
    sub-int v0, v1, v0

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    int-to-float v0, v0

    goto :goto_12
.end method

.method public final d(I)I
    .registers 3
    .parameter

    .prologue
    .line 960
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, LDO;->b(I)I

    move-result v0

    return v0
.end method

.method public e(I)F
    .registers 5
    .parameter

    .prologue
    .line 743
    invoke-virtual {p0, p1}, LDO;->f(I)I

    move-result v0

    int-to-float v1, v0

    .line 744
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LDO;->b(IZ)F

    move-result v0

    .line 745
    add-float/2addr v1, v0

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_11

    :goto_10
    return v0

    :cond_11
    neg-float v0, v0

    goto :goto_10
.end method

.method public final e(I)I
    .registers 4
    .parameter

    .prologue
    .line 966
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, LDO;->b(I)I

    move-result v0

    invoke-virtual {p0, p1}, LDO;->c(I)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public f(I)F
    .registers 5
    .parameter

    .prologue
    .line 736
    invoke-virtual {p0, p1}, LDO;->f(I)I

    move-result v0

    int-to-float v1, v0

    .line 737
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LDO;->b(IZ)F

    move-result v0

    .line 738
    add-float/2addr v1, v0

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_11

    :goto_10
    return v0

    :cond_11
    neg-float v0, v0

    goto :goto_10
.end method

.method public f(I)I
    .registers 12
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1308
    iget-boolean v0, p0, LDO;->a:Z

    if-nez v0, :cond_7

    .line 1338
    :cond_6
    :goto_6
    return v5

    .line 1311
    :cond_7
    iget-object v0, p0, LDO;->a:Ljava/lang/CharSequence;

    check-cast v0, Landroid/text/Spanned;

    .line 1313
    invoke-virtual {p0, p1}, LDO;->n(I)I

    move-result v2

    .line 1314
    invoke-virtual {p0, p1}, LDO;->h(I)I

    move-result v1

    .line 1315
    const-class v3, LGz;

    invoke-interface {v0, v2, v1, v3}, Landroid/text/Spanned;->nextSpanTransition(IILjava/lang/Class;)I

    move-result v1

    .line 1316
    const-class v3, LGz;

    invoke-static {v0, v2, v1, v3}, LDO;->a(Landroid/text/Spanned;IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LGz;

    .line 1318
    array-length v3, v1

    if-eqz v3, :cond_6

    .line 1324
    if-eqz v2, :cond_30

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Landroid/text/Spanned;->charAt(I)C

    move-result v2

    const/16 v3, 0xa

    if-ne v2, v3, :cond_5a

    :cond_30
    move v4, v6

    :goto_31
    move v7, v5

    move v8, v5

    .line 1326
    :goto_33
    array-length v2, v1

    if-ge v7, v2, :cond_5e

    .line 1327
    aget-object v3, v1, v7

    .line 1329
    instance-of v2, v3, LGA;

    if-eqz v2, :cond_60

    .line 1330
    invoke-interface {v0, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v2

    .line 1331
    invoke-virtual {p0, v2}, LDO;->g(I)I

    move-result v9

    move-object v2, v3

    .line 1332
    check-cast v2, LGA;

    invoke-interface {v2}, LGA;->a()I

    move-result v2

    .line 1333
    add-int/2addr v2, v9

    if-ge p1, v2, :cond_5c

    move v2, v6

    .line 1335
    :goto_4f
    invoke-interface {v3, v2}, LGz;->a(Z)I

    move-result v2

    add-int v3, v8, v2

    .line 1326
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    move v8, v3

    goto :goto_33

    :cond_5a
    move v4, v5

    .line 1324
    goto :goto_31

    :cond_5c
    move v2, v5

    .line 1333
    goto :goto_4f

    :cond_5e
    move v5, v8

    .line 1338
    goto :goto_6

    :cond_60
    move v2, v4

    goto :goto_4f
.end method

.method public g(I)I
    .registers 6
    .parameter

    .prologue
    .line 838
    invoke-virtual {p0}, LDO;->d()I

    move-result v1

    const/4 v0, -0x1

    move v2, v1

    .line 840
    :goto_6
    sub-int v1, v2, v0

    const/4 v3, 0x1

    if-le v1, v3, :cond_19

    .line 841
    add-int v1, v2, v0

    div-int/lit8 v1, v1, 0x2

    .line 843
    invoke-virtual {p0, v1}, LDO;->n(I)I

    move-result v3

    if-le v3, p1, :cond_17

    move v2, v1

    .line 844
    goto :goto_6

    :cond_17
    move v0, v1

    .line 846
    goto :goto_6

    .line 849
    :cond_19
    if-gez v0, :cond_1c

    .line 850
    const/4 v0, 0x0

    .line 852
    :cond_1c
    return v0
.end method

.method public final h(I)I
    .registers 3
    .parameter

    .prologue
    .line 927
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, LDO;->n(I)I

    move-result v0

    return v0
.end method

.method public i(I)I
    .registers 4
    .parameter

    .prologue
    .line 932
    invoke-virtual {p0, p1}, LDO;->n(I)I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v1}, LDO;->n(I)I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, LDO;->b(III)I

    move-result v0

    return v0
.end method

.method public j(I)I
    .registers 3
    .parameter

    .prologue
    .line 977
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LDO;->a(IZ)I

    move-result v0

    return v0
.end method

.method public k(I)I
    .registers 3
    .parameter

    .prologue
    .line 982
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LDO;->a(IZ)I

    move-result v0

    return v0
.end method

.method public final l(I)I
    .registers 5
    .parameter

    .prologue
    .line 1288
    const/4 v0, 0x0

    .line 1289
    invoke-virtual {p0, p1}, LDO;->o(I)I

    move-result v1

    .line 1290
    const/4 v2, -0x1

    if-eq v1, v2, :cond_c

    iget-boolean v1, p0, LDO;->a:Z

    if-nez v1, :cond_d

    .line 1293
    :cond_c
    :goto_c
    return v0

    :cond_d
    invoke-virtual {p0, p1}, LDO;->f(I)I

    move-result v0

    goto :goto_c
.end method

.method public final m(I)I
    .registers 5
    .parameter

    .prologue
    .line 1298
    iget v0, p0, LDO;->c:I

    .line 1299
    invoke-virtual {p0, p1}, LDO;->o(I)I

    move-result v1

    .line 1300
    const/4 v2, 0x1

    if-eq v1, v2, :cond_d

    iget-boolean v1, p0, LDO;->a:Z

    if-nez v1, :cond_e

    .line 1303
    :cond_d
    :goto_d
    return v0

    :cond_e
    invoke-virtual {p0, p1}, LDO;->f(I)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_d
.end method
