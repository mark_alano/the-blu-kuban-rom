.class public LDb;
.super LDd;
.source "ManualOffsetTreeNode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NodeOwner:",
        "Ljava/lang/Object;",
        ">",
        "LDd",
        "<TNodeOwner;",
        "LDb",
        "<TNodeOwner;>;>;"
    }
.end annotation


# direct methods
.method constructor <init>(LKh;LKh;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKh",
            "<TNodeOwner;>;",
            "LKh",
            "<TNodeOwner;>;)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, LDd;-><init>(LKh;LKh;)V

    .line 19
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, LDb;->a:LKh;

    invoke-interface {v0}, LKh;->b()I

    move-result v0

    return v0
.end method

.method protected a(LKh;LKh;)LDb;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKh",
            "<TNodeOwner;>;",
            "LKh",
            "<TNodeOwner;>;)",
            "LDb",
            "<TNodeOwner;>;"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v0, LDb;

    invoke-direct {v0, p1, p2}, LDb;-><init>(LKh;LKh;)V

    return-object v0
.end method

.method protected bridge synthetic a(LKh;LKh;)LDd;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-virtual {p0, p1, p2}, LDb;->a(LKh;LKh;)LDb;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TNodeOwner;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, LDb;->a:LKh;

    invoke-interface {v0}, LKh;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, LDb;->a:LKh;

    invoke-interface {v0, p1}, LKh;->a(I)V

    .line 53
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TNodeOwner;)V"
        }
    .end annotation

    .prologue
    .line 32
    iget-object v0, p0, LDb;->a:LKh;

    invoke-interface {v0, p1}, LKh;->a(Ljava/lang/Object;)V

    .line 33
    iget-object v0, p0, LDb;->b:LKh;

    invoke-interface {v0, p1}, LKh;->a(Ljava/lang/Object;)V

    .line 34
    return-void
.end method

.method public b()I
    .registers 2

    .prologue
    .line 78
    iget-object v0, p0, LDb;->b:LKh;

    invoke-interface {v0}, LKh;->b()I

    move-result v0

    return v0
.end method

.method public b(I)V
    .registers 3
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, LDb;->b:LKh;

    invoke-interface {v0, p1}, LKh;->a(I)V

    .line 63
    return-void
.end method
