.class public abstract LDj;
.super LDA;
.source "AbstractEditableGroup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NodeOwner:",
        "Ljava/lang/Object;",
        "ChildOwner:",
        "Ljava/lang/Object;",
        ">",
        "LDA",
        "<TNodeOwner;TChildOwner;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, LDA;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LDp;
.end method

.method a(III)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, LDA;->a(III)V

    .line 40
    invoke-virtual {p0}, LDj;->a()LDp;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LDp;->a(III)V

    .line 41
    return-void
.end method

.method public a(Ljava/util/List;II)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDt;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0}, LDj;->a()LDp;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LDp;->a(Ljava/util/List;II)V

    .line 30
    return-void
.end method

.method protected a()Z
    .registers 2

    .prologue
    .line 47
    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/util/List;II)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LDs;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, LDj;->a()LDp;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, LDp;->b(Ljava/util/List;II)V

    .line 35
    return-void
.end method
