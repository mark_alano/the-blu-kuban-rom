.class LDr;
.super Ljava/lang/Object;
.source "PresentedEditable.java"

# interfaces
.implements LDs;


# instance fields
.field final synthetic a:LDp;

.field final synthetic a:Landroid/text/SpanWatcher;


# direct methods
.method constructor <init>(LDp;Landroid/text/SpanWatcher;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 365
    iput-object p1, p0, LDr;->a:LDp;

    iput-object p2, p0, LDr;->a:Landroid/text/SpanWatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;II)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 368
    iget-object v0, p0, LDr;->a:LDp;

    invoke-static {v0}, LDp;->a(LDp;)LDG;

    move-result-object v0

    invoke-virtual {v0}, LDG;->b()I

    move-result v0

    .line 369
    iget-object v1, p0, LDr;->a:LDp;

    invoke-virtual {v1}, LDp;->length()I

    move-result v1

    .line 370
    sub-int v2, p2, v0

    invoke-static {v2, v3, v1}, LKf;->a(III)I

    move-result v2

    .line 371
    sub-int v0, p3, v0

    invoke-static {v0, v3, v1}, LKf;->a(III)I

    move-result v0

    .line 372
    iget-object v1, p0, LDr;->a:Landroid/text/SpanWatcher;

    iget-object v3, p0, LDr;->a:LDp;

    invoke-interface {v1, v3, p1, v2, v0}, Landroid/text/SpanWatcher;->onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V

    .line 373
    return-void
.end method

.method public a(Ljava/lang/Object;IIII)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 377
    iget-object v0, p0, LDr;->a:LDp;

    invoke-static {v0}, LDp;->a(LDp;)LDG;

    move-result-object v0

    invoke-virtual {v0}, LDG;->b()I

    move-result v0

    .line 378
    iget-object v1, p0, LDr;->a:LDp;

    invoke-virtual {v1}, LDp;->length()I

    move-result v1

    .line 379
    sub-int v2, p2, v0

    invoke-static {v2, v6, v1}, LKf;->a(III)I

    move-result v3

    .line 380
    sub-int v2, p3, v0

    invoke-static {v2, v6, v1}, LKf;->a(III)I

    move-result v4

    .line 381
    sub-int v2, p4, v0

    invoke-static {v2, v6, v1}, LKf;->a(III)I

    move-result v5

    .line 382
    sub-int v0, p5, v0

    invoke-static {v0, v6, v1}, LKf;->a(III)I

    move-result v6

    .line 383
    iget-object v0, p0, LDr;->a:Landroid/text/SpanWatcher;

    iget-object v1, p0, LDr;->a:LDp;

    move-object v2, p1

    invoke-interface/range {v0 .. v6}, Landroid/text/SpanWatcher;->onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V

    .line 384
    return-void
.end method

.method public b(Ljava/lang/Object;II)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 388
    iget-object v0, p0, LDr;->a:LDp;

    invoke-static {v0}, LDp;->a(LDp;)LDG;

    move-result-object v0

    invoke-virtual {v0}, LDG;->b()I

    move-result v0

    .line 389
    iget-object v1, p0, LDr;->a:LDp;

    invoke-virtual {v1}, LDp;->length()I

    move-result v1

    .line 390
    sub-int v2, p2, v0

    invoke-static {v2, v3, v1}, LKf;->a(III)I

    move-result v2

    .line 391
    sub-int v0, p3, v0

    invoke-static {v0, v3, v1}, LKf;->a(III)I

    move-result v0

    .line 392
    iget-object v1, p0, LDr;->a:Landroid/text/SpanWatcher;

    iget-object v3, p0, LDr;->a:LDp;

    invoke-interface {v1, v3, p1, v2, v0}, Landroid/text/SpanWatcher;->onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V

    .line 393
    return-void
.end method
