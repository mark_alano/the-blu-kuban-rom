.class public LDw;
.super Ljava/lang/Object;
.source "SpanArray.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/text/Spannable;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "LDx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/text/Spannable;)V
    .registers 3
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LDw;->a:Ljava/util/Map;

    .line 31
    iput-object p1, p0, LDw;->a:Landroid/text/Spannable;

    .line 32
    return-void
.end method

.method private a(Ljava/lang/Object;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 245
    const-class v1, Landroid/text/SpanWatcher;

    invoke-virtual {p0, v0, p2, p3, v1}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 246
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpanWatcher;

    .line 247
    if-eq v0, p1, :cond_e

    .line 248
    iget-object v2, p0, LDw;->a:Landroid/text/Spannable;

    invoke-interface {v0, v2, p1, p2, p3}, Landroid/text/SpanWatcher;->onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V

    goto :goto_e

    .line 251
    :cond_22
    return-void
.end method

.method private a(Ljava/lang/Object;IIII)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 228
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 231
    invoke-static {p2, p4}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p3, p5}, Ljava/lang/Math;->max(II)I

    move-result v2

    const-class v3, Landroid/text/SpanWatcher;

    invoke-virtual {p0, v0, v1, v2, v3}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 233
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_16
    :goto_16
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpanWatcher;

    .line 234
    if-eq v0, p1, :cond_16

    .line 235
    iget-object v1, p0, LDw;->a:Landroid/text/Spannable;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/text/SpanWatcher;->onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V

    goto :goto_16

    .line 238
    :cond_2f
    return-void
.end method

.method private static a(IIII)Z
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 122
    if-gt p0, p3, :cond_5

    if-ge p1, p2, :cond_6

    .line 134
    :cond_5
    :goto_5
    return v0

    .line 129
    :cond_6
    if-eq p0, p1, :cond_e

    if-eq p2, p3, :cond_e

    .line 130
    if-eq p0, p3, :cond_5

    if-eq p1, p2, :cond_5

    .line 134
    :cond_e
    const/4 v0, 0x1

    goto :goto_5
.end method

.method private b(Ljava/lang/Object;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 257
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 258
    const-class v1, Landroid/text/SpanWatcher;

    invoke-virtual {p0, v0, p2, p3, v1}, LDw;->a(Ljava/util/List;IILjava/lang/Class;)V

    .line 259
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_e
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/SpanWatcher;

    .line 260
    if-eq v0, p1, :cond_e

    .line 261
    iget-object v2, p0, LDw;->a:Landroid/text/Spannable;

    invoke-interface {v0, v2, p1, p2, p3}, Landroid/text/SpanWatcher;->onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V

    goto :goto_e

    .line 264
    :cond_22
    return-void
.end method


# virtual methods
.method public a(IILjava/lang/Class;)I
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 176
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    .line 177
    invoke-virtual {p3, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 180
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDx;

    .line 181
    invoke-virtual {v0}, LDx;->a()I

    move-result v1

    .line 182
    if-le v1, p1, :cond_2f

    if-ge v1, p2, :cond_2f

    move p2, v1

    .line 185
    :cond_2f
    invoke-virtual {v0}, LDx;->b()I

    move-result v0

    .line 186
    if-le v0, p1, :cond_a

    if-ge v0, p2, :cond_a

    move p2, v0

    .line 187
    goto :goto_a

    .line 190
    :cond_39
    return p2
.end method

.method public a(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDx;

    .line 86
    if-eqz v0, :cond_f

    .line 87
    invoke-virtual {v0}, LDx;->c()I

    move-result v0

    .line 89
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public a(III)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 211
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 212
    iget-object v0, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_f
    :goto_f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 213
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LDx;

    .line 214
    invoke-virtual {v1, p1, p2, p3}, LDx;->b(III)V

    .line 215
    invoke-virtual {v1}, LDx;->a()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 216
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 219
    :cond_32
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_36
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_45

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 220
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LDw;->a(Ljava/lang/Object;Z)V

    goto :goto_36

    .line 222
    :cond_45
    return-void
.end method

.method public a(Ljava/lang/Object;IIIZ)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDx;

    .line 64
    if-eqz v0, :cond_1f

    .line 65
    invoke-virtual {v0}, LDx;->a()I

    move-result v2

    .line 66
    invoke-virtual {v0}, LDx;->b()I

    move-result v3

    .line 67
    invoke-virtual {v0, p2, p3, p4}, LDx;->a(III)V

    .line 68
    if-eqz p5, :cond_1e

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    .line 69
    invoke-direct/range {v0 .. v5}, LDw;->a(Ljava/lang/Object;IIII)V

    .line 78
    :cond_1e
    :goto_1e
    return-void

    .line 72
    :cond_1f
    new-instance v0, LDx;

    invoke-direct {v0, p2, p3, p4}, LDx;-><init>(III)V

    .line 73
    iget-object v1, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    if-eqz p5, :cond_1e

    .line 75
    invoke-direct {p0, p1, p2, p3}, LDw;->a(Ljava/lang/Object;II)V

    goto :goto_1e
.end method

.method public a(Ljava/lang/Object;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDx;

    .line 43
    if-eqz v0, :cond_1c

    .line 44
    iget-object v1, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    if-eqz p2, :cond_1c

    .line 46
    invoke-virtual {v0}, LDx;->a()I

    move-result v1

    invoke-virtual {v0}, LDx;->b()I

    move-result v0

    invoke-direct {p0, p1, v1, v0}, LDw;->b(Ljava/lang/Object;II)V

    .line 49
    :cond_1c
    return-void
.end method

.method public a(Ljava/util/List;IILjava/lang/Class;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;II",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 149
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 150
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDx;

    .line 151
    invoke-virtual {p4, v2}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 155
    invoke-virtual {v0}, LDx;->a()I

    move-result v3

    invoke-virtual {v0}, LDx;->b()I

    move-result v0

    invoke-static {v3, v0, p2, p3}, LDw;->a(IIII)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 158
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 161
    :cond_38
    return-void
.end method

.method public b(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDx;

    .line 98
    if-eqz v0, :cond_f

    .line 99
    invoke-virtual {v0}, LDx;->a()I

    move-result v0

    .line 101
    :goto_e
    return v0

    :cond_f
    const/4 v0, -0x1

    goto :goto_e
.end method

.method public c(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LDx;

    .line 110
    if-eqz v0, :cond_f

    .line 111
    invoke-virtual {v0}, LDx;->b()I

    move-result v0

    .line 113
    :goto_e
    return v0

    :cond_f
    const/4 v0, -0x1

    goto :goto_e
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    iget-object v0, p0, LDw;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
