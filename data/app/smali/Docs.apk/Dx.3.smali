.class public LDx;
.super Ljava/lang/Object;
.source "SpanInfo.java"


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(III)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-virtual {p0, p1, p2, p3}, LDx;->a(III)V

    .line 24
    return-void
.end method

.method private static a(IZIII)I
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    if-gt p0, p2, :cond_6

    if-eqz p1, :cond_16

    if-ne p0, p2, :cond_16

    .line 73
    :cond_6
    add-int v0, p2, p3

    if-ge p0, v0, :cond_19

    .line 77
    add-int/lit8 v0, p2, 0x1

    if-gt p0, v0, :cond_14

    if-eqz p1, :cond_17

    add-int/lit8 v0, p2, 0x1

    if-ne p0, v0, :cond_17

    .line 78
    :cond_14
    add-int p0, p2, p4

    .line 87
    :cond_16
    :goto_16
    return p0

    :cond_17
    move p0, p2

    .line 80
    goto :goto_16

    .line 84
    :cond_19
    sub-int v0, p4, p3

    add-int/2addr p0, v0

    goto :goto_16
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 30
    iget v0, p0, LDx;->a:I

    return v0
.end method

.method public a(III)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    iput p1, p0, LDx;->a:I

    .line 61
    iput p2, p0, LDx;->b:I

    .line 62
    iput p3, p0, LDx;->c:I

    .line 63
    return-void
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 52
    iget v0, p0, LDx;->c:I

    and-int/lit8 v0, v0, 0x33

    .line 53
    const/16 v1, 0x21

    if-ne v0, v1, :cond_10

    iget v0, p0, LDx;->a:I

    iget v1, p0, LDx;->b:I

    if-lt v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public b()I
    .registers 2

    .prologue
    .line 37
    iget v0, p0, LDx;->b:I

    return v0
.end method

.method public b(III)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v5, 0x22

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 100
    iget v0, p0, LDx;->c:I

    and-int/lit8 v3, v0, 0x33

    .line 103
    const/16 v0, 0x21

    if-eq v3, v0, :cond_e

    if-ne v3, v5, :cond_27

    :cond_e
    move v0, v2

    .line 105
    :goto_f
    iget v4, p0, LDx;->a:I

    invoke-static {v4, v0, p1, p2, p3}, LDx;->a(IZIII)I

    move-result v0

    iput v0, p0, LDx;->a:I

    .line 107
    const/16 v0, 0x12

    if-eq v3, v0, :cond_1d

    if-ne v3, v5, :cond_1e

    :cond_1d
    move v1, v2

    .line 109
    :cond_1e
    iget v0, p0, LDx;->b:I

    invoke-static {v0, v1, p1, p2, p3}, LDx;->a(IZIII)I

    move-result v0

    iput v0, p0, LDx;->b:I

    .line 110
    return-void

    :cond_27
    move v0, v1

    .line 103
    goto :goto_f
.end method

.method public c()I
    .registers 2

    .prologue
    .line 44
    iget v0, p0, LDx;->c:I

    return v0
.end method
