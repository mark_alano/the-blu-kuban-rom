.class public LEa;
.super LDO;
.source "DynamicLayout.java"


# static fields
.field private static a:LEW;

.field private static a:Ljava/lang/Object;


# instance fields
.field private a:LEb;

.field private a:LEr;

.field private final a:LEs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LEs",
            "<",
            "LEk;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/text/TextUtils$TruncateAt;

.field private final a:Ljava/lang/CharSequence;

.field private final a:Z

.field private final b:LEr;

.field private final b:Ljava/lang/CharSequence;

.field private b:Z

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 498
    new-instance v0, LEW;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LEW;-><init>(Z)V

    sput-object v0, LEa;->a:LEW;

    .line 499
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LEa;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LFg;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v0 .. v9}, LEa;-><init>(LFg;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 54
    return-void
.end method

.method public constructor <init>(LFg;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V
    .registers 22
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v11}, LEa;-><init>(LFg;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    .line 69
    return-void
.end method

.method public constructor <init>(LFg;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 85
    if-nez p10, :cond_d3

    move-object v3, p3

    :goto_3
    move-object v1, p0

    move-object v2, p1

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v1 .. v8}, LDO;-><init>(LFg;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FF)V

    .line 93
    iput-object p2, p0, LEa;->a:Ljava/lang/CharSequence;

    .line 94
    iput-object p3, p0, LEa;->b:Ljava/lang/CharSequence;

    .line 96
    if-eqz p10, :cond_e5

    .line 97
    new-instance v1, LEr;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, LEr;-><init>(I)V

    iput-object v1, p0, LEa;->a:LEr;

    .line 98
    move/from16 v0, p11

    iput v0, p0, LEa;->c:I

    .line 99
    move-object/from16 v0, p10

    iput-object v0, p0, LEa;->a:Landroid/text/TextUtils$TruncateAt;

    .line 106
    :goto_25
    new-instance v1, LEs;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, LEs;-><init>(I)V

    iput-object v1, p0, LEa;->a:LEs;

    .line 108
    new-instance v1, LEr;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, LEr;-><init>(I)V

    iput-object v1, p0, LEa;->b:LEr;

    .line 110
    move/from16 v0, p9

    iput-boolean v0, p0, LEa;->a:Z

    .line 120
    if-eqz p10, :cond_4e

    .line 121
    invoke-virtual {p0}, LEa;->a()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, LDP;

    .line 123
    iput-object p0, v1, LDP;->a:LDO;

    .line 124
    move/from16 v0, p11

    iput v0, v1, LDP;->a:I

    .line 125
    move-object/from16 v0, p10

    iput-object v0, v1, LDP;->a:Landroid/text/TextUtils$TruncateAt;

    .line 126
    const/4 v1, 0x1

    iput-boolean v1, p0, LEa;->b:Z

    .line 135
    :cond_4e
    if-eqz p10, :cond_f4

    .line 136
    const/4 v1, 0x5

    new-array v1, v1, [I

    .line 137
    const/4 v2, 0x3

    const/high16 v3, -0x8000

    aput v3, v1, v2

    .line 142
    :goto_58
    const/4 v2, 0x1

    new-array v2, v2, [LEk;

    const/4 v3, 0x0

    sget-object v4, LEa;->a:LEk;

    aput-object v4, v2, v3

    .line 144
    invoke-virtual {p4}, Landroid/text/TextPaint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v3

    .line 145
    iget v4, v3, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 146
    iget v3, v3, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 148
    const/4 v5, 0x0

    const/4 v6, 0x0

    aput v6, v1, v5

    .line 149
    const/4 v5, 0x1

    const/4 v6, 0x0

    aput v6, v1, v5

    .line 150
    const/4 v5, 0x2

    aput v3, v1, v5

    .line 151
    iget-object v5, p0, LEa;->a:LEr;

    const/4 v6, 0x0

    invoke-virtual {v5, v6, v1}, LEr;->a(I[I)V

    .line 153
    const/4 v5, 0x1

    sub-int/2addr v3, v4

    aput v3, v1, v5

    .line 154
    iget-object v3, p0, LEa;->a:LEr;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v1}, LEr;->a(I[I)V

    .line 156
    iget-object v1, p0, LEa;->a:LEs;

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, LEs;->a(I[Ljava/lang/Object;)V

    .line 158
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 159
    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    .line 160
    const/4 v2, 0x1

    const/4 v3, 0x0

    aput v3, v1, v2

    .line 161
    iget-object v2, p0, LEa;->b:LEr;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, LEr;->a(I[I)V

    .line 165
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    invoke-direct {p0, p2, v1, v2, v3}, LEa;->a(Ljava/lang/CharSequence;III)V

    .line 167
    instance-of v1, p2, Landroid/text/Spannable;

    if-eqz v1, :cond_106

    .line 168
    iget-object v1, p0, LEa;->a:LEb;

    if-nez v1, :cond_b7

    .line 169
    new-instance v1, LEb;

    new-instance v2, LEc;

    invoke-direct {v2, p0}, LEc;-><init>(LEa;)V

    invoke-direct {v1, v2}, LEb;-><init>(LEQ;)V

    iput-object v1, p0, LEa;->a:LEb;

    :cond_b7
    move-object v1, p2

    .line 172
    check-cast v1, Landroid/text/Spannable;

    .line 173
    const/4 v2, 0x0

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v3

    const-class v4, LEb;

    invoke-interface {v1, v2, v3, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [LEb;

    .line 174
    const/4 v3, 0x0

    :goto_c8
    array-length v4, v2

    if-ge v3, v4, :cond_f9

    .line 175
    aget-object v4, v2, v3

    invoke-interface {v1, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 174
    add-int/lit8 v3, v3, 0x1

    goto :goto_c8

    .line 85
    :cond_d3
    instance-of v1, p3, Landroid/text/Spanned;

    if-eqz v1, :cond_de

    new-instance v3, LDQ;

    invoke-direct {v3, p3}, LDQ;-><init>(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_de
    new-instance v3, LDP;

    invoke-direct {v3, p3}, LDP;-><init>(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 101
    :cond_e5
    new-instance v1, LEr;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, LEr;-><init>(I)V

    iput-object v1, p0, LEa;->a:LEr;

    .line 102
    iput p5, p0, LEa;->c:I

    .line 103
    const/4 v1, 0x0

    iput-object v1, p0, LEa;->a:Landroid/text/TextUtils$TruncateAt;

    goto/16 :goto_25

    .line 139
    :cond_f4
    const/4 v1, 0x3

    new-array v1, v1, [I

    goto/16 :goto_58

    .line 177
    :cond_f9
    iget-object v2, p0, LEa;->a:LEb;

    const/4 v3, 0x0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const v5, 0x800012

    invoke-interface {v1, v2, v3, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 181
    :cond_106
    return-void
.end method

.method static synthetic a(LEa;Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, LEa;->a(Ljava/lang/CharSequence;III)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;III)V
    .registers 29
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 184
    move-object/from16 v0, p0

    iget-object v2, v0, LEa;->a:Ljava/lang/CharSequence;

    move-object/from16 v0, p1

    if-eq v0, v2, :cond_9

    .line 363
    :goto_8
    return-void

    .line 187
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, LEa;->b:Ljava/lang/CharSequence;

    .line 188
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v15

    .line 192
    const/16 v2, 0xa

    add-int/lit8 v4, p2, -0x1

    invoke-static {v3, v2, v4}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;CI)I

    move-result v2

    .line 193
    if-gez v2, :cond_7b

    .line 194
    const/4 v2, 0x0

    .line 199
    :goto_1c
    sub-int v2, p2, v2

    .line 200
    add-int v4, p3, v2

    .line 201
    add-int v7, p4, v2

    .line 202
    sub-int v6, p2, v2

    .line 207
    const/16 v2, 0xa

    add-int v5, v6, v7

    invoke-static {v3, v2, v5}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;CI)I

    move-result v2

    .line 208
    if-gez v2, :cond_7e

    move v2, v15

    .line 213
    :goto_2f
    add-int v5, v6, v7

    sub-int/2addr v2, v5

    .line 214
    add-int v5, v4, v2

    .line 215
    add-int v4, v7, v2

    .line 219
    instance-of v2, v3, Landroid/text/Spanned;

    if-eqz v2, :cond_83

    move-object v2, v3

    .line 220
    check-cast v2, Landroid/text/Spanned;

    .line 224
    :cond_3d
    const/4 v8, 0x0

    .line 226
    add-int v7, v6, v4

    const-class v9, Landroid/text/style/WrapTogetherSpan;

    invoke-interface {v2, v6, v7, v9}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v10

    .line 229
    const/4 v7, 0x0

    :goto_47
    array-length v9, v10

    if-ge v7, v9, :cond_81

    .line 230
    aget-object v9, v10, v7

    invoke-interface {v2, v9}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v9

    .line 231
    aget-object v11, v10, v7

    invoke-interface {v2, v11}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v11

    .line 233
    if-ge v9, v6, :cond_224

    .line 234
    const/4 v8, 0x1

    .line 236
    sub-int v9, v6, v9

    .line 237
    add-int/2addr v5, v9

    .line 238
    add-int/2addr v4, v9

    .line 239
    sub-int v9, v6, v9

    move v6, v4

    move/from16 v23, v8

    move v8, v5

    move/from16 v5, v23

    .line 242
    :goto_65
    add-int v4, v9, v6

    if-le v11, v4, :cond_70

    .line 243
    const/4 v5, 0x1

    .line 245
    add-int v4, v9, v6

    sub-int v4, v11, v4

    .line 246
    add-int/2addr v8, v4

    .line 247
    add-int/2addr v6, v4

    .line 229
    :cond_70
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v4, v6

    move v6, v9

    move/from16 v23, v8

    move v8, v5

    move/from16 v5, v23

    goto :goto_47

    .line 196
    :cond_7b
    add-int/lit8 v2, v2, 0x1

    goto :goto_1c

    .line 211
    :cond_7e
    add-int/lit8 v2, v2, 0x1

    goto :goto_2f

    .line 250
    :cond_81
    if-nez v8, :cond_3d

    :cond_83
    move/from16 v16, v4

    move/from16 v17, v5

    move v4, v6

    .line 255
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, LEa;->g(I)I

    move-result v20

    .line 256
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LEa;->b(I)I

    move-result v21

    .line 258
    add-int v2, v4, v17

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LEa;->g(I)I

    move-result v2

    .line 259
    add-int v5, v4, v16

    if-ne v5, v15, :cond_220

    .line 260
    invoke-virtual/range {p0 .. p0}, LEa;->d()I

    move-result v2

    move/from16 v18, v2

    .line 261
    :goto_a8
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LEa;->b(I)I

    move-result v22

    .line 262
    invoke-virtual/range {p0 .. p0}, LEa;->d()I

    move-result v2

    move/from16 v0, v18

    if-ne v0, v2, :cond_1fc

    const/4 v2, 0x1

    move/from16 v19, v2

    .line 268
    :goto_bb
    sget-object v5, LEa;->a:Ljava/lang/Object;

    monitor-enter v5

    .line 269
    :try_start_be
    sget-object v2, LEa;->a:LEW;

    .line 270
    const/4 v6, 0x0

    sput-object v6, LEa;->a:LEW;

    .line 271
    monitor-exit v5
    :try_end_c4
    .catchall {:try_start_be .. :try_end_c4} :catchall_201

    .line 273
    if-nez v2, :cond_cc

    .line 274
    new-instance v2, LEW;

    const/4 v5, 0x1

    invoke-direct {v2, v5}, LEW;-><init>(Z)V

    .line 276
    :cond_cc
    add-int v5, v4, v16

    invoke-virtual/range {p0 .. p0}, LEa;->a()Landroid/text/TextPaint;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, LEa;->a()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, LEa;->a()Landroid/text/Layout$Alignment;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, LEa;->b()F

    move-result v9

    invoke-virtual/range {p0 .. p0}, LEa;->c()F

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget v13, v0, LEa;->c:I

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, LEa;->a:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual/range {v2 .. v14}, LEW;->a(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZZFLandroid/text/TextUtils$TruncateAt;)V

    .line 280
    invoke-virtual {v2}, LEW;->d()I

    move-result v3

    .line 286
    add-int v5, v4, v16

    if-eq v5, v15, :cond_104

    add-int/lit8 v5, v3, -0x1

    invoke-virtual {v2, v5}, LEW;->n(I)I

    move-result v5

    add-int v4, v4, v16

    if-ne v5, v4, :cond_104

    .line 288
    add-int/lit8 v3, v3, -0x1

    .line 292
    :cond_104
    move-object/from16 v0, p0

    iget-object v4, v0, LEa;->a:LEr;

    sub-int v5, v18, v20

    move/from16 v0, v20

    invoke-virtual {v4, v0, v5}, LEr;->a(II)V

    .line 293
    move-object/from16 v0, p0

    iget-object v4, v0, LEa;->a:LEs;

    sub-int v5, v18, v20

    move/from16 v0, v20

    invoke-virtual {v4, v0, v5}, LEs;->a(II)V

    .line 294
    move-object/from16 v0, p0

    iget-object v4, v0, LEa;->b:LEr;

    sub-int v5, v18, v20

    move/from16 v0, v20

    invoke-virtual {v4, v0, v5}, LEr;->a(II)V

    .line 298
    invoke-virtual {v2, v3}, LEW;->b(I)I

    move-result v5

    .line 299
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 301
    move-object/from16 v0, p0

    iget-boolean v7, v0, LEa;->a:Z

    if-eqz v7, :cond_13c

    if-nez v20, :cond_13c

    .line 302
    invoke-virtual {v2}, LEW;->e()I

    move-result v4

    .line 303
    move-object/from16 v0, p0

    iput v4, v0, LEa;->d:I

    .line 304
    sub-int/2addr v5, v4

    .line 306
    :cond_13c
    move-object/from16 v0, p0

    iget-boolean v7, v0, LEa;->a:Z

    if-eqz v7, :cond_219

    if-eqz v19, :cond_219

    .line 307
    invoke-virtual {v2}, LEW;->f()I

    move-result v6

    .line 308
    move-object/from16 v0, p0

    iput v6, v0, LEa;->e:I

    .line 309
    add-int/2addr v5, v6

    move/from16 v23, v6

    move v6, v5

    move/from16 v5, v23

    .line 312
    :goto_152
    move-object/from16 v0, p0

    iget-object v7, v0, LEa;->a:LEr;

    const/4 v8, 0x0

    sub-int v9, v16, v17

    move/from16 v0, v20

    invoke-virtual {v7, v0, v8, v9}, LEr;->a(III)V

    .line 313
    move-object/from16 v0, p0

    iget-object v7, v0, LEa;->a:LEr;

    const/4 v8, 0x1

    sub-int v9, v21, v22

    add-int/2addr v6, v9

    move/from16 v0, v20

    invoke-virtual {v7, v0, v8, v6}, LEr;->a(III)V

    .line 319
    move-object/from16 v0, p0

    iget-boolean v6, v0, LEa;->b:Z

    if-eqz v6, :cond_204

    .line 320
    const/4 v6, 0x5

    new-array v6, v6, [I

    .line 321
    const/4 v7, 0x3

    const/high16 v8, -0x8000

    aput v8, v6, v7

    .line 326
    :goto_179
    const/4 v7, 0x1

    new-array v9, v7, [LEk;

    .line 327
    const/4 v7, 0x2

    new-array v10, v7, [I

    .line 329
    const/4 v7, 0x0

    move v8, v7

    :goto_181
    if-ge v8, v3, :cond_20e

    .line 330
    const/4 v11, 0x0

    invoke-virtual {v2, v8}, LEW;->n(I)I

    move-result v12

    invoke-virtual {v2, v8}, LEW;->o(I)I

    move-result v7

    const/4 v13, 0x1

    if-ne v7, v13, :cond_209

    const/4 v7, 0x0

    :goto_190
    or-int/2addr v12, v7

    invoke-virtual {v2, v8}, LEW;->c(I)Z

    move-result v7

    if-eqz v7, :cond_20c

    const/high16 v7, 0x2000

    :goto_199
    or-int/2addr v7, v12

    aput v7, v6, v11

    .line 335
    invoke-virtual {v2, v8}, LEW;->b(I)I

    move-result v7

    add-int v7, v7, v21

    .line 336
    if-lez v8, :cond_1a5

    .line 337
    sub-int/2addr v7, v4

    .line 338
    :cond_1a5
    const/4 v11, 0x1

    aput v7, v6, v11

    .line 340
    invoke-virtual {v2, v8}, LEW;->c(I)I

    move-result v7

    .line 341
    add-int/lit8 v11, v3, -0x1

    if-ne v8, v11, :cond_1b1

    .line 342
    add-int/2addr v7, v5

    .line 344
    :cond_1b1
    const/4 v11, 0x2

    aput v7, v6, v11

    .line 345
    const/4 v7, 0x0

    invoke-virtual {v2, v8}, LEW;->a(I)LEk;

    move-result-object v11

    aput-object v11, v9, v7

    .line 347
    move-object/from16 v0, p0

    iget-boolean v7, v0, LEa;->b:Z

    if-eqz v7, :cond_1cf

    .line 348
    const/4 v7, 0x3

    invoke-virtual {v2, v8}, LEW;->p(I)I

    move-result v11

    aput v11, v6, v7

    .line 349
    const/4 v7, 0x4

    invoke-virtual {v2, v8}, LEW;->q(I)I

    move-result v11

    aput v11, v6, v7

    .line 352
    :cond_1cf
    move-object/from16 v0, p0

    iget-object v7, v0, LEa;->a:LEr;

    add-int v11, v20, v8

    invoke-virtual {v7, v11, v6}, LEr;->a(I[I)V

    .line 353
    move-object/from16 v0, p0

    iget-object v7, v0, LEa;->a:LEs;

    add-int v11, v20, v8

    invoke-virtual {v7, v11, v9}, LEs;->a(I[Ljava/lang/Object;)V

    .line 355
    const/4 v7, 0x0

    invoke-virtual {v2, v8}, LEW;->a(I)S

    move-result v11

    aput v11, v10, v7

    .line 356
    const/4 v7, 0x1

    invoke-virtual {v2, v8}, LEW;->b(I)S

    move-result v11

    aput v11, v10, v7

    .line 357
    move-object/from16 v0, p0

    iget-object v7, v0, LEa;->b:LEr;

    add-int v11, v20, v8

    invoke-virtual {v7, v11, v10}, LEr;->a(I[I)V

    .line 329
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_181

    .line 262
    :cond_1fc
    const/4 v2, 0x0

    move/from16 v19, v2

    goto/16 :goto_bb

    .line 271
    :catchall_201
    move-exception v2

    :try_start_202
    monitor-exit v5
    :try_end_203
    .catchall {:try_start_202 .. :try_end_203} :catchall_201

    throw v2

    .line 323
    :cond_204
    const/4 v6, 0x3

    new-array v6, v6, [I

    goto/16 :goto_179

    .line 330
    :cond_209
    const/high16 v7, 0x4000

    goto :goto_190

    :cond_20c
    const/4 v7, 0x0

    goto :goto_199

    .line 360
    :cond_20e
    sget-object v3, LEa;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 361
    :try_start_211
    sput-object v2, LEa;->a:LEW;

    .line 362
    monitor-exit v3

    goto/16 :goto_8

    :catchall_216
    move-exception v2

    monitor-exit v3
    :try_end_218
    .catchall {:try_start_211 .. :try_end_218} :catchall_216

    throw v2

    :cond_219
    move/from16 v23, v6

    move v6, v5

    move/from16 v5, v23

    goto/16 :goto_152

    :cond_220
    move/from16 v18, v2

    goto/16 :goto_a8

    :cond_224
    move v9, v6

    move v6, v4

    move/from16 v23, v5

    move v5, v8

    move/from16 v8, v23

    goto/16 :goto_65
.end method


# virtual methods
.method public final a(I)LEk;
    .registers 4
    .parameter

    .prologue
    .line 413
    iget-object v0, p0, LEa;->a:LEs;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LEs;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LEk;

    return-object v0
.end method

.method public a(I)S
    .registers 4
    .parameter

    .prologue
    .line 372
    iget-object v0, p0, LEa;->b:LEr;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LEr;->a(II)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 428
    iget v0, p0, LEa;->c:I

    return v0
.end method

.method public b(I)I
    .registers 4
    .parameter

    .prologue
    .line 387
    iget-object v0, p0, LEa;->a:LEr;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LEr;->a(II)I

    move-result v0

    return v0
.end method

.method public b(I)S
    .registers 4
    .parameter

    .prologue
    .line 377
    iget-object v0, p0, LEa;->b:LEr;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, LEr;->a(II)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public b(I)Z
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 382
    iget-object v1, p0, LEa;->b:LEr;

    invoke-virtual {v1, p1, v0}, LEr;->a(II)I

    move-result v1

    if-lez v1, :cond_a

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public c(I)I
    .registers 4
    .parameter

    .prologue
    .line 392
    iget-object v0, p0, LEa;->a:LEr;

    const/4 v1, 0x2

    invoke-virtual {v0, p1, v1}, LEr;->a(II)I

    move-result v0

    return v0
.end method

.method public c(I)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 402
    iget-object v1, p0, LEa;->a:LEr;

    invoke-virtual {v1, p1, v0}, LEr;->a(II)I

    move-result v1

    const/high16 v2, 0x2000

    and-int/2addr v1, v2

    if-eqz v1, :cond_d

    const/4 v0, 0x1

    :cond_d
    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 367
    iget-object v0, p0, LEa;->a:LEr;

    invoke-virtual {v0}, LEr;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public n(I)I
    .registers 4
    .parameter

    .prologue
    .line 397
    iget-object v0, p0, LEa;->a:LEr;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LEr;->a(II)I

    move-result v0

    const v1, 0x1fffffff

    and-int/2addr v0, v1

    return v0
.end method

.method public o(I)I
    .registers 4
    .parameter

    .prologue
    .line 407
    iget-object v0, p0, LEa;->a:LEr;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LEr;->a(II)I

    move-result v0

    shr-int/lit8 v0, v0, 0x1e

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_c
    return v0

    :cond_d
    const/4 v0, -0x1

    goto :goto_c
.end method

.method public p(I)I
    .registers 4
    .parameter

    .prologue
    .line 468
    iget-object v0, p0, LEa;->a:Landroid/text/TextUtils$TruncateAt;

    if-nez v0, :cond_6

    .line 469
    const/4 v0, 0x0

    .line 472
    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, LEa;->a:LEr;

    const/4 v1, 0x3

    invoke-virtual {v0, p1, v1}, LEr;->a(II)I

    move-result v0

    goto :goto_5
.end method

.method public q(I)I
    .registers 4
    .parameter

    .prologue
    .line 477
    iget-object v0, p0, LEa;->a:Landroid/text/TextUtils$TruncateAt;

    if-nez v0, :cond_6

    .line 478
    const/4 v0, 0x0

    .line 481
    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, LEa;->a:LEr;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v1}, LEr;->a(II)I

    move-result v0

    goto :goto_5
.end method
