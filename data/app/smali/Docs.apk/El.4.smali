.class public LEl;
.super Ljava/lang/Object;
.source "LayoutAdapter.java"

# interfaces
.implements LEj;


# instance fields
.field private final a:LEj;


# direct methods
.method public constructor <init>(LEj;)V
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, LEl;->a:LEj;

    .line 25
    return-void
.end method


# virtual methods
.method public a(I)F
    .registers 3
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(I)F

    move-result v0

    return v0
.end method

.method public a()I
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0}, LEj;->a()I

    move-result v0

    return v0
.end method

.method public a(I)I
    .registers 3
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(I)I

    move-result v0

    return v0
.end method

.method public a(IF)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1, p2}, LEj;->a(IF)I

    move-result v0

    return v0
.end method

.method public a(ILandroid/graphics/Rect;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1, p2}, LEj;->a(ILandroid/graphics/Rect;)I

    move-result v0

    return v0
.end method

.method public a(I)LEk;
    .registers 3
    .parameter

    .prologue
    .line 240
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(I)LEk;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Landroid/text/Layout$Alignment;
    .registers 3
    .parameter

    .prologue
    .line 265
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(I)Landroid/text/Layout$Alignment;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Landroid/util/Pair;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(I)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 120
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0}, LEj;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public a(I)S
    .registers 3
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(I)S

    move-result v0

    return v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 290
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0}, LEj;->a()V

    .line 291
    return-void
.end method

.method public a(F)V
    .registers 3
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(F)V

    .line 35
    return-void
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(I)V

    .line 186
    return-void
.end method

.method public a(IILandroid/graphics/Path;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 260
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1, p2, p3}, LEj;->a(IILandroid/graphics/Path;)V

    .line 261
    return-void
.end method

.method public a(ILandroid/graphics/Path;Ljava/lang/CharSequence;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 255
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1, p2, p3}, LEj;->a(ILandroid/graphics/Path;Ljava/lang/CharSequence;)V

    .line 256
    return-void
.end method

.method public a(LFc;)V
    .registers 3
    .parameter

    .prologue
    .line 305
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->a(LFc;)V

    .line 306
    return-void
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1, p2, p3, p4}, LEj;->a(Landroid/graphics/Canvas;Landroid/graphics/Path;Landroid/graphics/Paint;I)V

    .line 45
    return-void
.end method

.method public b(I)F
    .registers 3
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->b(I)F

    move-result v0

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0}, LEj;->b()I

    move-result v0

    return v0
.end method

.method public b(I)I
    .registers 3
    .parameter

    .prologue
    .line 115
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->b(I)I

    move-result v0

    return v0
.end method

.method public b(I)S
    .registers 3
    .parameter

    .prologue
    .line 225
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->b(I)S

    move-result v0

    return v0
.end method

.method public b(I)Z
    .registers 3
    .parameter

    .prologue
    .line 230
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->b(I)Z

    move-result v0

    return v0
.end method

.method public c(I)F
    .registers 3
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->c(I)F

    move-result v0

    return v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 190
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0}, LEj;->c()I

    move-result v0

    return v0
.end method

.method public c(I)I
    .registers 3
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->c(I)I

    move-result v0

    return v0
.end method

.method public c(I)Z
    .registers 3
    .parameter

    .prologue
    .line 235
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->c(I)Z

    move-result v0

    return v0
.end method

.method public d(I)F
    .registers 3
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->d(I)F

    move-result v0

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 210
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0}, LEj;->d()I

    move-result v0

    return v0
.end method

.method public d(I)I
    .registers 3
    .parameter

    .prologue
    .line 155
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->d(I)I

    move-result v0

    return v0
.end method

.method public e(I)F
    .registers 3
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->e(I)F

    move-result v0

    return v0
.end method

.method public e(I)I
    .registers 3
    .parameter

    .prologue
    .line 160
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->e(I)I

    move-result v0

    return v0
.end method

.method public f(I)I
    .registers 3
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->f(I)I

    move-result v0

    return v0
.end method

.method public g(I)I
    .registers 3
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->g(I)I

    move-result v0

    return v0
.end method

.method public h(I)I
    .registers 3
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->h(I)I

    move-result v0

    return v0
.end method

.method public j(I)I
    .registers 3
    .parameter

    .prologue
    .line 170
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->j(I)I

    move-result v0

    return v0
.end method

.method public k(I)I
    .registers 3
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->k(I)I

    move-result v0

    return v0
.end method

.method public n(I)I
    .registers 3
    .parameter

    .prologue
    .line 215
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->n(I)I

    move-result v0

    return v0
.end method

.method public o(I)I
    .registers 3
    .parameter

    .prologue
    .line 220
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->o(I)I

    move-result v0

    return v0
.end method

.method public p(I)I
    .registers 3
    .parameter

    .prologue
    .line 285
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->p(I)I

    move-result v0

    return v0
.end method

.method public q(I)I
    .registers 3
    .parameter

    .prologue
    .line 295
    iget-object v0, p0, LEl;->a:LEj;

    invoke-interface {v0, p1}, LEj;->q(I)I

    move-result v0

    return v0
.end method
