.class public final LFB;
.super Landroid/os/Handler;
.source "TextView.java"


# instance fields
.field private a:B

.field public a:F

.field private a:I

.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/docs/editors/text/TextView;",
            ">;"
        }
    .end annotation
.end field

.field public b:F

.field private final c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 4
    .parameter

    .prologue
    .line 6045
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 6034
    const/4 v0, 0x0

    iput-byte v0, p0, LFB;->a:B

    .line 6046
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 6047
    const/high16 v1, 0x41f0

    mul-float/2addr v0, v1

    const/high16 v1, 0x4204

    div-float/2addr v0, v1

    iput v0, p0, LFB;->c:F

    .line 6048
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LFB;->a:Ljava/lang/ref/WeakReference;

    .line 6049
    return-void
.end method

.method private c()V
    .registers 2

    .prologue
    .line 6101
    const/4 v0, 0x0

    iput v0, p0, LFB;->b:F

    .line 6102
    iget-object v0, p0, LFB;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/text/TextView;

    .line 6103
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 6104
    :cond_10
    return-void
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 6133
    iget v0, p0, LFB;->f:F

    return v0
.end method

.method a()V
    .registers 5

    .prologue
    const/4 v3, 0x2

    .line 6073
    iget-byte v0, p0, LFB;->a:B

    if-eq v0, v3, :cond_6

    .line 6090
    :cond_5
    :goto_5
    return-void

    .line 6077
    :cond_6
    invoke-virtual {p0, v3}, LFB;->removeMessages(I)V

    .line 6079
    iget-object v0, p0, LFB;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/text/TextView;

    .line 6080
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->isFocused()Z

    move-result v1

    if-nez v1, :cond_1f

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 6081
    :cond_1f
    iget v1, p0, LFB;->b:F

    iget v2, p0, LFB;->c:F

    add-float/2addr v1, v2

    iput v1, p0, LFB;->b:F

    .line 6082
    iget v1, p0, LFB;->b:F

    iget v2, p0, LFB;->d:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3c

    .line 6083
    iget v1, p0, LFB;->d:F

    iput v1, p0, LFB;->b:F

    .line 6084
    const/4 v1, 0x3

    const-wide/16 v2, 0x4b0

    invoke-virtual {p0, v1, v2, v3}, LFB;->sendEmptyMessageDelayed(IJ)Z

    .line 6088
    :goto_38
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    goto :goto_5

    .line 6086
    :cond_3c
    const-wide/16 v1, 0x21

    invoke-virtual {p0, v3, v1, v2}, LFB;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_38
.end method

.method public a(I)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 6107
    if-nez p1, :cond_7

    .line 6108
    invoke-virtual {p0}, LFB;->b()V

    .line 6130
    :cond_6
    :goto_6
    return-void

    .line 6111
    :cond_7
    iput p1, p0, LFB;->a:I

    .line 6112
    iget-object v0, p0, LFB;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/text/TextView;

    .line 6113
    if-eqz v0, :cond_6

    iget-object v1, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v1, :cond_6

    .line 6114
    iput-byte v6, p0, LFB;->a:B

    .line 6115
    const/4 v1, 0x0

    iput v1, p0, LFB;->b:F

    .line 6116
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v2

    sub-int/2addr v1, v2

    .line 6119
    iget-object v2, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LEj;->e(I)F

    move-result v2

    .line 6120
    int-to-float v3, v1

    const/high16 v4, 0x4040

    div-float/2addr v3, v4

    .line 6121
    int-to-float v4, v1

    sub-float v4, v2, v4

    add-float/2addr v4, v3

    iput v4, p0, LFB;->e:F

    .line 6122
    iget v4, p0, LFB;->e:F

    int-to-float v5, v1

    add-float/2addr v4, v5

    iput v4, p0, LFB;->d:F

    .line 6123
    add-float/2addr v3, v2

    iput v3, p0, LFB;->f:F

    .line 6124
    int-to-float v1, v1

    const/high16 v3, 0x40c0

    div-float/2addr v1, v3

    add-float/2addr v1, v2

    iput v1, p0, LFB;->g:F

    .line 6125
    iget v1, p0, LFB;->e:F

    add-float/2addr v1, v2

    add-float/2addr v1, v2

    iput v1, p0, LFB;->a:F

    .line 6127
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->invalidate()V

    .line 6128
    const-wide/16 v0, 0x4b0

    invoke-virtual {p0, v6, v0, v1}, LFB;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_6
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 6137
    iget v0, p0, LFB;->b:F

    iget v1, p0, LFB;->g:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public b()V
    .registers 2

    .prologue
    .line 6093
    const/4 v0, 0x0

    iput-byte v0, p0, LFB;->a:B

    .line 6094
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LFB;->removeMessages(I)V

    .line 6095
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LFB;->removeMessages(I)V

    .line 6096
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LFB;->removeMessages(I)V

    .line 6097
    invoke-direct {p0}, LFB;->c()V

    .line 6098
    return-void
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 6141
    iget-byte v0, p0, LFB;->a:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_f

    iget v0, p0, LFB;->b:F

    iget v1, p0, LFB;->e:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_f

    const/4 v0, 0x1

    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 6145
    iget-byte v0, p0, LFB;->a:B

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public d()Z
    .registers 2

    .prologue
    .line 6149
    iget-byte v0, p0, LFB;->a:B

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x2

    .line 6053
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_26

    .line 6070
    :cond_6
    :goto_6
    return-void

    .line 6055
    :pswitch_7
    iput-byte v1, p0, LFB;->a:B

    .line 6056
    invoke-virtual {p0}, LFB;->a()V

    goto :goto_6

    .line 6059
    :pswitch_d
    invoke-virtual {p0}, LFB;->a()V

    goto :goto_6

    .line 6062
    :pswitch_11
    iget-byte v0, p0, LFB;->a:B

    if-ne v0, v1, :cond_6

    .line 6063
    iget v0, p0, LFB;->a:I

    if-ltz v0, :cond_1f

    .line 6064
    iget v0, p0, LFB;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LFB;->a:I

    .line 6066
    :cond_1f
    iget v0, p0, LFB;->a:I

    invoke-virtual {p0, v0}, LFB;->a(I)V

    goto :goto_6

    .line 6053
    nop

    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_7
        :pswitch_d
        :pswitch_11
    .end packed-switch
.end method
