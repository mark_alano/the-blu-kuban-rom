.class public LFH;
.super Ljava/lang/Object;
.source "TextView.java"

# interfaces
.implements LFr;


# instance fields
.field private a:I

.field private a:J

.field private a:LFG;

.field private a:LFI;

.field final synthetic a:Lcom/google/android/apps/docs/editors/text/TextView;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 4
    .parameter

    .prologue
    .line 8597
    iput-object p1, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8594
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LFH;->a:J

    .line 8598
    invoke-virtual {p0}, LFH;->c()V

    .line 8599
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .registers 7
    .parameter

    .prologue
    .line 8678
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    .line 8679
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_26

    .line 8680
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    float-to-int v2, v2

    .line 8681
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    float-to-int v3, v3

    .line 8682
    iget-object v4, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v4, v2, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v2

    .line 8683
    iget v3, p0, LFH;->a:I

    if-ge v2, v3, :cond_1d

    iput v2, p0, LFH;->a:I

    .line 8684
    :cond_1d
    iget v3, p0, LFH;->b:I

    if-le v2, v3, :cond_23

    iput v2, p0, LFH;->b:I

    .line 8679
    :cond_23
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 8686
    :cond_26
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 8689
    iget v0, p0, LFH;->a:I

    return v0
.end method

.method public a()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 8603
    iget-object v0, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->r()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 8616
    :goto_9
    return-void

    .line 8608
    :cond_a
    iget-object v0, p0, LFH;->a:LFI;

    if-nez v0, :cond_17

    new-instance v0, LFI;

    iget-object v1, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-direct {v0, v1, v2}, LFI;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;LFj;)V

    iput-object v0, p0, LFH;->a:LFI;

    .line 8609
    :cond_17
    iget-object v0, p0, LFH;->a:LFG;

    if-nez v0, :cond_24

    new-instance v0, LFG;

    iget-object v1, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-direct {v0, v1, v2}, LFG;-><init>(Lcom/google/android/apps/docs/editors/text/TextView;LFj;)V

    iput-object v0, p0, LFH;->a:LFG;

    .line 8611
    :cond_24
    iget-object v0, p0, LFH;->a:LFI;

    invoke-virtual {v0}, LFI;->b()V

    .line 8612
    iget-object v0, p0, LFH;->a:LFG;

    invoke-virtual {v0}, LFG;->b()V

    .line 8614
    iget-object v0, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Lcom/google/android/apps/docs/editors/text/TextView;)V

    .line 8615
    iget-object v0, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)LEX;

    move-result-object v0

    invoke-virtual {v0}, LEX;->b()V

    goto :goto_9
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 8705
    iget-object v0, p0, LFH;->a:LFI;

    if-eqz v0, :cond_e

    iget-object v0, p0, LFH;->a:LFI;

    invoke-virtual {v0}, LFI;->c()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    .line 8629
    iget-object v0, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->d(Lcom/google/android/apps/docs/editors/text/TextView;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->e(Lcom/google/android/apps/docs/editors/text/TextView;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 8630
    :cond_10
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_7e

    .line 8671
    :cond_17
    :goto_17
    :pswitch_17
    const/4 v0, 0x0

    return v0

    .line 8632
    :pswitch_19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 8633
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 8637
    iget-object v2, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v2

    iput v2, p0, LFH;->b:I

    iput v2, p0, LFH;->a:I

    .line 8640
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, LFH;->a:J

    sub-long/2addr v2, v4

    .line 8641
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v4

    .line 8642
    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_5c

    .line 8643
    iget v2, p0, LFH;->c:I

    sub-int v2, v0, v2

    .line 8644
    iget v3, p0, LFH;->d:I

    sub-int v3, v1, v3

    .line 8645
    mul-int/2addr v2, v2

    mul-int/2addr v3, v3

    add-int/2addr v2, v3

    .line 8646
    iget-object v3, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v3}, Lcom/google/android/apps/docs/editors/text/TextView;->j(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v3

    iget-object v4, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v4}, Lcom/google/android/apps/docs/editors/text/TextView;->j(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v4

    mul-int/2addr v3, v4

    if-ge v2, v3, :cond_5c

    .line 8647
    iget-object v2, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/text/TextView;->c()Z

    .line 8651
    :cond_5c
    iput v0, p0, LFH;->c:I

    .line 8652
    iput v1, p0, LFH;->d:I

    goto :goto_17

    .line 8660
    :pswitch_61
    iget-object v0, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 8662
    invoke-direct {p0, p1}, LFH;->a(Landroid/view/MotionEvent;)V

    goto :goto_17

    .line 8667
    :pswitch_77
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LFH;->a:J

    goto :goto_17

    .line 8630
    :pswitch_data_7e
    .packed-switch 0x0
        :pswitch_19
        :pswitch_77
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_61
        :pswitch_61
    .end packed-switch
.end method

.method public b()I
    .registers 2

    .prologue
    .line 8693
    iget v0, p0, LFH;->b:I

    return v0
.end method

.method public b()V
    .registers 2

    .prologue
    .line 8620
    iget-object v0, p0, LFH;->a:LFI;

    if-eqz v0, :cond_9

    iget-object v0, p0, LFH;->a:LFI;

    invoke-virtual {v0}, LFI;->d()V

    .line 8621
    :cond_9
    iget-object v0, p0, LFH;->a:LFG;

    if-eqz v0, :cond_12

    iget-object v0, p0, LFH;->a:LFG;

    invoke-virtual {v0}, LFG;->d()V

    .line 8622
    :cond_12
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 8697
    const/4 v0, -0x1

    iput v0, p0, LFH;->b:I

    iput v0, p0, LFH;->a:I

    .line 8698
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 8717
    iget-object v0, p0, LFH;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 8718
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 8720
    iget-object v0, p0, LFH;->a:LFI;

    if-eqz v0, :cond_12

    iget-object v0, p0, LFH;->a:LFI;

    invoke-virtual {v0}, LFI;->f()V

    .line 8721
    :cond_12
    iget-object v0, p0, LFH;->a:LFG;

    if-eqz v0, :cond_1b

    iget-object v0, p0, LFH;->a:LFG;

    invoke-virtual {v0}, LFG;->f()V

    .line 8722
    :cond_1b
    return-void
.end method

.method public onTouchModeChanged(Z)V
    .registers 2
    .parameter

    .prologue
    .line 8710
    if-nez p1, :cond_5

    .line 8711
    invoke-virtual {p0}, LFH;->b()V

    .line 8713
    :cond_5
    return-void
.end method
