.class LFL;
.super Ljava/lang/Object;
.source "AccessibilityHoverHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic a:LFJ;

.field final synthetic a:Landroid/view/accessibility/AccessibilityManager;

.field final synthetic a:Lcom/google/android/apps/docs/editors/text/TextView;

.field final synthetic b:I


# direct methods
.method constructor <init>(LFJ;Lcom/google/android/apps/docs/editors/text/TextView;IILandroid/view/accessibility/AccessibilityManager;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 226
    iput-object p1, p0, LFL;->a:LFJ;

    iput-object p2, p0, LFL;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iput p3, p0, LFL;->a:I

    iput p4, p0, LFL;->b:I

    iput-object p5, p0, LFL;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .prologue
    .line 229
    iget-object v0, p0, LFL;->a:LFJ;

    invoke-static {v0}, LFJ;->a(LFJ;)Ljava/lang/Runnable;

    move-result-object v0

    if-eq v0, p0, :cond_9

    .line 266
    :cond_8
    :goto_8
    return-void

    .line 232
    :cond_9
    iget-object v0, p0, LFL;->a:LFJ;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LFJ;->a(LFJ;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 234
    iget-object v0, p0, LFL;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a()LEj;

    move-result-object v1

    .line 235
    iget-object v0, p0, LFL;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget v2, p0, LFL;->a:I

    iget v3, p0, LFL;->b:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/docs/editors/text/TextView;->a(II)I

    move-result v0

    .line 236
    iget-object v2, p0, LFL;->a:LFJ;

    invoke-static {v2}, LFJ;->b(LFJ;)I

    move-result v2

    if-eq v0, v2, :cond_8

    .line 240
    invoke-interface {v1, v0}, LEj;->g(I)I

    move-result v2

    .line 241
    iget-object v3, p0, LFL;->a:LFJ;

    invoke-static {v3, v0}, LFJ;->d(LFJ;I)I

    .line 242
    invoke-interface {v1}, LEj;->a()Ljava/lang/CharSequence;

    move-result-object v3

    .line 243
    iget-object v4, p0, LFL;->a:LFJ;

    invoke-static {v4}, LFJ;->a(LFJ;)LGw;

    move-result-object v4

    invoke-virtual {v4, v3}, LGw;->a(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v4, p0, LFL;->a:LFJ;

    invoke-static {v4}, LFJ;->a(LFJ;)LGw;

    move-result-object v4

    invoke-virtual {v4, v0}, LGw;->a(I)I

    move-result v0

    .line 245
    const/4 v4, -0x1

    if-ne v0, v4, :cond_4b

    .line 246
    const/4 v0, 0x0

    .line 249
    :cond_4b
    iget-object v4, p0, LFL;->a:LFJ;

    invoke-static {v4}, LFJ;->c(LFJ;)I

    move-result v4

    if-eq v2, v4, :cond_84

    .line 250
    invoke-interface {v1, v2}, LEj;->n(I)I

    move-result v4

    .line 251
    invoke-interface {v1, v2}, LEj;->h(I)I

    move-result v1

    .line 252
    iget-object v5, p0, LFL;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-static {v5, v3, v4, v1}, LEZ;->a(Landroid/view/accessibility/AccessibilityManager;Ljava/lang/CharSequence;II)V

    .line 253
    iget-object v1, p0, LFL;->a:LFJ;

    invoke-static {v1, v2}, LFJ;->e(LFJ;I)I

    .line 254
    iget-object v1, p0, LFL;->a:LFJ;

    invoke-static {v1, v0}, LFJ;->f(LFJ;I)I

    .line 255
    iget-object v0, p0, LFL;->a:LFJ;

    invoke-static {v0, v4}, LFJ;->c(LFJ;I)I

    .line 257
    iget-object v0, p0, LFL;->a:LFJ;

    invoke-static {v0}, LFJ;->a(LFJ;)Landroid/os/Vibrator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 258
    iget-object v0, p0, LFL;->a:LFJ;

    invoke-static {v0}, LFJ;->a(LFJ;)Landroid/os/Vibrator;

    move-result-object v0

    const-wide/16 v1, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    goto :goto_8

    .line 261
    :cond_84
    iget-object v1, p0, LFL;->a:LFJ;

    invoke-static {v1}, LFJ;->d(LFJ;)I

    move-result v1

    if-eq v0, v1, :cond_8

    .line 262
    iget-object v1, p0, LFL;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v2, p0, LFL;->a:LFJ;

    invoke-static {v2}, LFJ;->a(LFJ;)LGw;

    move-result-object v2

    invoke-static {v1, v3, v2, v0}, LEZ;->a(Lcom/google/android/apps/docs/editors/text/TextView;Ljava/lang/CharSequence;LGw;I)V

    .line 263
    iget-object v1, p0, LFL;->a:LFJ;

    invoke-static {v1, v0}, LFJ;->f(LFJ;I)I

    .line 264
    iget-object v1, p0, LFL;->a:LFJ;

    invoke-static {v1, v0}, LFJ;->c(LFJ;I)I

    goto/16 :goto_8
.end method
