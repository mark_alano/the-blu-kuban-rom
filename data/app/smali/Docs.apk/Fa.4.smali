.class public LFa;
.super Ljava/lang/Object;
.source "TextDrawUtils.java"


# direct methods
.method public static a(Landroid/graphics/Path;F)Landroid/graphics/Path;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 86
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 87
    invoke-virtual {v0, p1, p1}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 88
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 89
    invoke-virtual {v1, p0}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 90
    invoke-virtual {v1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 91
    invoke-virtual {p0}, Landroid/graphics/Path;->getFillType()Landroid/graphics/Path$FillType;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 92
    return-object v1
.end method

.method public static a(Landroid/graphics/Path;FFF)Landroid/graphics/Path;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 72
    invoke-virtual {v0, p3, p3, p1, p2}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 74
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 75
    invoke-virtual {v1, p0}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 76
    invoke-virtual {v1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 77
    invoke-virtual {p0}, Landroid/graphics/Path;->getFillType()Landroid/graphics/Path$FillType;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 79
    return-object v1
.end method

.method public static a(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)Landroid/graphics/Path;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 31
    invoke-interface {p0, p1, p2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 32
    const/4 v2, 0x0

    sub-int v3, p2, p1

    move-object v0, p5

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextPath(Ljava/lang/String;IIFFLandroid/graphics/Path;)V

    .line 33
    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    .line 36
    invoke-virtual {p5}, Landroid/graphics/Paint;->isUnderlineText()Z

    move-result v0

    if-nez v0, :cond_25

    invoke-virtual {p5}, Landroid/graphics/Paint;->isStrikeThruText()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 37
    :cond_25
    invoke-virtual {p5, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v7

    move v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 39
    invoke-static/range {v2 .. v7}, LFa;->a(FFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;Landroid/graphics/Path;F)V

    .line 42
    :cond_30
    return-object v6
.end method

.method public static a([CIIFFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;)Landroid/graphics/Path;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    new-instance v6, Landroid/graphics/Path;

    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    move-object v0, p5

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 54
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Paint;->getTextPath([CIIFFLandroid/graphics/Path;)V

    .line 55
    invoke-virtual {v6}, Landroid/graphics/Path;->close()V

    .line 58
    invoke-virtual {p5}, Landroid/graphics/Paint;->isUnderlineText()Z

    move-result v0

    if-nez v0, :cond_1d

    invoke-virtual {p5}, Landroid/graphics/Paint;->isStrikeThruText()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 59
    :cond_1d
    invoke-virtual {p5, p0, p1, p2}, Landroid/graphics/Paint;->measureText([CII)F

    move-result v7

    move v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 61
    invoke-static/range {v2 .. v7}, LFa;->a(FFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;Landroid/graphics/Path;F)V

    .line 64
    :cond_28
    return-object v6
.end method

.method private static a(FFLandroid/graphics/Paint;Landroid/graphics/Paint$FontMetricsInt;Landroid/graphics/Path;F)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x41a0

    .line 102
    if-nez p3, :cond_8

    .line 103
    invoke-virtual {p2}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object p3

    .line 106
    :cond_8
    iget v0, p3, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iget v1, p3, Landroid/graphics/Paint$FontMetricsInt;->top:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    .line 108
    invoke-virtual {p2}, Landroid/graphics/Paint;->isUnderlineText()Z

    move-result v1

    if-eqz v1, :cond_33

    .line 109
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 110
    iget v2, p3, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v2, p1

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 111
    iget v2, v1, Landroid/graphics/RectF;->top:F

    div-float v3, v0, v4

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 112
    iput p0, v1, Landroid/graphics/RectF;->left:F

    .line 113
    add-float v2, p0, p5

    iput v2, v1, Landroid/graphics/RectF;->right:F

    .line 115
    sget-object v2, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {p4, v1, v2}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 118
    :cond_33
    invoke-virtual {p2}, Landroid/graphics/Paint;->isStrikeThruText()Z

    move-result v1

    if-eqz v1, :cond_5a

    .line 119
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    .line 120
    iget v2, p3, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iget v3, p3, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v2, p1

    iput v2, v1, Landroid/graphics/RectF;->top:F

    .line 121
    iget v2, v1, Landroid/graphics/RectF;->top:F

    div-float/2addr v0, v4

    add-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 122
    iput p0, v1, Landroid/graphics/RectF;->left:F

    .line 123
    add-float v0, p0, p5

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 125
    sget-object v0, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {p4, v1, v0}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 127
    :cond_5a
    return-void
.end method
