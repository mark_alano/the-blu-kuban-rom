.class LFi;
.super Ljava/lang/Object;
.source "TextScaler.java"


# instance fields
.field public final a:F

.field public final a:Landroid/graphics/Paint$FontMetricsInt;

.field public final b:F

.field public final c:F


# direct methods
.method public constructor <init>(LFh;)V
    .registers 4
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iget-object v0, p1, LFh;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    iput v0, p0, LFi;->a:F

    .line 33
    iget-object v0, p1, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    if-eqz v0, :cond_47

    .line 34
    new-instance v0, Landroid/graphics/Paint$FontMetricsInt;

    invoke-direct {v0}, Landroid/graphics/Paint$FontMetricsInt;-><init>()V

    iput-object v0, p0, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    .line 35
    iget-object v0, p0, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p1, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    .line 36
    iget-object v0, p0, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p1, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->bottom:I

    .line 37
    iget-object v0, p0, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p1, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    .line 38
    iget-object v0, p0, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p1, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    .line 39
    iget-object v0, p0, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget-object v1, p1, LFh;->a:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->top:I

    iput v1, v0, Landroid/graphics/Paint$FontMetricsInt;->top:I

    .line 44
    :goto_3e
    iget v0, p1, LFh;->a:F

    iput v0, p0, LFi;->b:F

    .line 45
    iget v0, p1, LFh;->b:F

    iput v0, p0, LFi;->c:F

    .line 46
    return-void

    .line 41
    :cond_47
    const/4 v0, 0x0

    iput-object v0, p0, LFi;->a:Landroid/graphics/Paint$FontMetricsInt;

    goto :goto_3e
.end method
