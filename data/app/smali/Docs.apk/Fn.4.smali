.class public LFn;
.super Landroid/os/Handler;
.source "TextView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/docs/editors/text/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 3
    .parameter

    .prologue
    .line 6859
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 6860
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LFn;->a:Ljava/lang/ref/WeakReference;

    .line 6861
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 6883
    iget-boolean v0, p0, LFn;->a:Z

    if-nez v0, :cond_a

    .line 6884
    invoke-virtual {p0, p0}, LFn;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6885
    const/4 v0, 0x1

    iput-boolean v0, p0, LFn;->a:Z

    .line 6887
    :cond_a
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 6890
    const/4 v0, 0x0

    iput-boolean v0, p0, LFn;->a:Z

    .line 6891
    return-void
.end method

.method public run()V
    .registers 5

    .prologue
    .line 6865
    iget-boolean v0, p0, LFn;->a:Z

    if-eqz v0, :cond_5

    .line 6880
    :cond_4
    :goto_4
    return-void

    .line 6869
    :cond_5
    invoke-virtual {p0, p0}, LFn;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 6871
    iget-object v0, p0, LFn;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/text/TextView;

    .line 6873
    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->c(Lcom/google/android/apps/docs/editors/text/TextView;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 6874
    iget-object v1, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-eqz v1, :cond_1f

    .line 6875
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->j()V

    .line 6878
    :cond_1f
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x1f4

    add-long/2addr v0, v2

    invoke-virtual {p0, p0, v0, v1}, LFn;->postAtTime(Ljava/lang/Runnable;J)Z

    goto :goto_4
.end method
