.class public final enum LFo;
.super Ljava/lang/Enum;
.source "TextView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LFo;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LFo;

.field private static final synthetic a:[LFo;

.field public static final enum b:LFo;

.field public static final enum c:LFo;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7001
    new-instance v0, LFo;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, LFo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LFo;->a:LFo;

    new-instance v0, LFo;

    const-string v1, "SPANNABLE"

    invoke-direct {v0, v1, v3}, LFo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LFo;->b:LFo;

    new-instance v0, LFo;

    const-string v1, "EDITABLE"

    invoke-direct {v0, v1, v4}, LFo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LFo;->c:LFo;

    .line 7000
    const/4 v0, 0x3

    new-array v0, v0, [LFo;

    sget-object v1, LFo;->a:LFo;

    aput-object v1, v0, v2

    sget-object v1, LFo;->b:LFo;

    aput-object v1, v0, v3

    sget-object v1, LFo;->c:LFo;

    aput-object v1, v0, v4

    sput-object v0, LFo;->a:[LFo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7000
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LFo;
    .registers 2
    .parameter

    .prologue
    .line 7000
    const-class v0, LFo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LFo;

    return-object v0
.end method

.method public static values()[LFo;
    .registers 1

    .prologue
    .line 7000
    sget-object v0, LFo;->a:[LFo;

    invoke-virtual {v0}, [LFo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LFo;

    return-object v0
.end method
