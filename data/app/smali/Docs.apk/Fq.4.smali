.class public LFq;
.super Ljava/lang/Object;
.source "TextView.java"


# instance fields
.field private a:I

.field private a:J

.field private final a:Landroid/graphics/Paint;

.field private final a:Landroid/graphics/Path;

.field final synthetic a:Lcom/google/android/apps/docs/editors/text/TextView;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 4
    .parameter

    .prologue
    .line 4890
    iput-object p1, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4884
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, LFq;->a:Landroid/graphics/Path;

    .line 4885
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LFq;->a:Landroid/graphics/Paint;

    .line 4891
    iget-object v0, p0, LFq;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 4892
    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 4969
    iget-object v0, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;LFq;)LFq;

    .line 4970
    return-void
.end method

.method public static synthetic a(LFq;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 4883
    invoke-direct {p0, p1}, LFq;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 11
    .parameter

    .prologue
    .line 4948
    iget-object v0, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    if-nez v0, :cond_7

    .line 4966
    :goto_6
    return-void

    .line 4950
    :cond_7
    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v7

    monitor-enter v7

    .line 4951
    :try_start_c
    iget-object v0, p0, LFq;->a:Landroid/graphics/Path;

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 4953
    iget-object v0, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v5

    .line 4954
    iget-object v0, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v0

    iget-object v1, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;Z)I

    move-result v1

    add-int v6, v0, v1

    .line 4956
    if-eqz p1, :cond_59

    .line 4957
    iget-object v0, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    const-wide/16 v1, 0x10

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    add-int/2addr v3, v5

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    add-int/2addr v4, v6

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v8

    iget v8, v8, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    add-int/2addr v5, v8

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v8

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v8

    add-int/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/docs/editors/text/TextView;->postInvalidateDelayed(JIIII)V

    .line 4965
    :goto_54
    monitor-exit v7

    goto :goto_6

    :catchall_56
    move-exception v0

    monitor-exit v7
    :try_end_58
    .catchall {:try_start_c .. :try_end_58} :catchall_56

    throw v0

    .line 4962
    :cond_59
    :try_start_59
    iget-object v0, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    invoke-static {}, Lcom/google/android/apps/docs/editors/text/TextView;->a()Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/docs/editors/text/TextView;->postInvalidate(IIII)V
    :try_end_7a
    .catchall {:try_start_59 .. :try_end_7a} :catchall_56

    goto :goto_54
.end method

.method private a()Z
    .registers 5

    .prologue
    .line 4923
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, LFq;->a:J

    sub-long/2addr v0, v2

    .line 4924
    const-wide/16 v2, 0x190

    cmp-long v2, v0, v2

    if-lez v2, :cond_f

    const/4 v0, 0x0

    .line 4930
    :goto_e
    return v0

    .line 4926
    :cond_f
    const/high16 v2, 0x3f80

    long-to-float v0, v0

    const/high16 v1, 0x43c8

    div-float/2addr v0, v1

    sub-float v0, v2, v0

    .line 4927
    iget-object v1, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v1

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    .line 4928
    iget-object v2, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v2}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v2

    const v3, 0xffffff

    and-int/2addr v2, v3

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    shl-int/lit8 v0, v0, 0x18

    add-int/2addr v0, v2

    .line 4929
    iget-object v1, p0, LFq;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 4930
    const/4 v0, 0x1

    goto :goto_e
.end method

.method private b()Z
    .registers 5

    .prologue
    .line 4934
    iget-object v0, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    .line 4935
    if-nez v0, :cond_8

    const/4 v0, 0x0

    .line 4944
    :goto_7
    return v0

    .line 4938
    :cond_8
    iget-object v0, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->b(Lcom/google/android/apps/docs/editors/text/TextView;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 4939
    iget v1, p0, LFq;->a:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 4940
    iget v2, p0, LFq;->b:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4942
    iget-object v2, p0, LFq;->a:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 4943
    iget-object v2, p0, LFq;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v2, v2, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    iget-object v3, p0, LFq;->a:Landroid/graphics/Path;

    invoke-interface {v2, v1, v0, v3}, LEj;->a(IILandroid/graphics/Path;)V

    .line 4944
    const/4 v0, 0x1

    goto :goto_7
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;I)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 4905
    invoke-direct {p0}, LFq;->b()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-direct {p0}, LFq;->a()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 4906
    if-eqz p2, :cond_13

    .line 4907
    int-to-float v0, p2

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 4910
    :cond_13
    iget-object v0, p0, LFq;->a:Landroid/graphics/Path;

    iget-object v1, p0, LFq;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 4912
    if-eqz p2, :cond_21

    .line 4913
    neg-int v0, p2

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 4915
    :cond_21
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LFq;->a(Z)V

    .line 4920
    :goto_25
    return-void

    .line 4917
    :cond_26
    invoke-direct {p0}, LFq;->a()V

    .line 4918
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LFq;->a(Z)V

    goto :goto_25
.end method

.method public a(Landroid/view/inputmethod/CorrectionInfo;)V
    .registers 4
    .parameter

    .prologue
    .line 4895
    invoke-virtual {p1}, Landroid/view/inputmethod/CorrectionInfo;->getOffset()I

    move-result v0

    iput v0, p0, LFq;->a:I

    .line 4896
    iget v0, p0, LFq;->a:I

    invoke-virtual {p1}, Landroid/view/inputmethod/CorrectionInfo;->getNewText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LFq;->b:I

    .line 4897
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LFq;->a:J

    .line 4899
    iget v0, p0, LFq;->a:I

    if-ltz v0, :cond_21

    iget v0, p0, LFq;->b:I

    if-gez v0, :cond_24

    .line 4900
    :cond_21
    invoke-direct {p0}, LFq;->a()V

    .line 4902
    :cond_24
    return-void
.end method
