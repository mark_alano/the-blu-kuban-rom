.class public abstract LFu;
.super Landroid/view/View;
.source "TextView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field protected a:F

.field private a:I

.field protected a:Landroid/graphics/drawable/Drawable;

.field private final a:Landroid/widget/PopupWindow;

.field final synthetic a:Lcom/google/android/apps/docs/editors/text/TextView;

.field private a:Z

.field private final a:[I

.field private final a:[J

.field private b:F

.field private b:I

.field private b:Z

.field private c:F

.field private c:I

.field private final d:F

.field private d:I

.field private final e:F

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/text/TextView;)V
    .registers 5
    .parameter

    .prologue
    const/4 v1, 0x5

    const/4 v2, 0x0

    .line 7971
    iput-object p1, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    .line 7972
    invoke-static {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 7969
    iput-boolean v2, p0, LFu;->b:Z

    .line 7993
    new-array v0, v1, [J

    iput-object v0, p0, LFu;->a:[J

    .line 7994
    new-array v0, v1, [I

    iput-object v0, p0, LFu;->a:[I

    .line 7995
    iput v2, p0, LFu;->g:I

    .line 7996
    iput v2, p0, LFu;->h:I

    .line 7973
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-static {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    .line 7974
    invoke-static {p1}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LEY;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_38

    .line 7975
    iget-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setSplitTouchEnabled(Z)V

    .line 7977
    :cond_38
    iget-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 7978
    iget-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p0}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 7979
    iget-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 7980
    invoke-virtual {p0}, LFu;->a()V

    .line 7982
    iget-object v0, p0, LFu;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 7983
    const v1, -0x41666666

    int-to-float v2, v0

    mul-float/2addr v1, v2

    iput v1, p0, LFu;->d:F

    .line 7984
    const v1, 0x3f333333

    int-to-float v0, v0

    mul-float/2addr v0, v1

    iput v0, p0, LFu;->e:F

    .line 7985
    return-void
.end method

.method private c(I)V
    .registers 3
    .parameter

    .prologue
    .line 7999
    const/4 v0, 0x0

    iput v0, p0, LFu;->h:I

    .line 8000
    invoke-direct {p0, p1}, LFu;->d(I)V

    .line 8001
    return-void
.end method

.method private d(I)V
    .registers 6
    .parameter

    .prologue
    .line 8004
    iget v0, p0, LFu;->h:I

    if-lez v0, :cond_d

    iget-object v0, p0, LFu;->a:[I

    iget v1, p0, LFu;->g:I

    aget v0, v0, v1

    if-ne v0, p1, :cond_d

    .line 8013
    :goto_c
    return-void

    .line 8009
    :cond_d
    iget v0, p0, LFu;->g:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x5

    iput v0, p0, LFu;->g:I

    .line 8010
    iget-object v0, p0, LFu;->a:[I

    iget v1, p0, LFu;->g:I

    aput p1, v0, v1

    .line 8011
    iget-object v0, p0, LFu;->a:[J

    iget v1, p0, LFu;->g:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 8012
    iget v0, p0, LFu;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LFu;->h:I

    goto :goto_c
.end method

.method private d()Z
    .registers 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8071
    iget-boolean v2, p0, LFu;->a:Z

    if-eqz v2, :cond_7

    .line 8105
    :cond_6
    :goto_6
    return v0

    .line 8075
    :cond_7
    iget-object v2, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/text/TextView;->r()Z

    move-result v2

    if-eqz v2, :cond_11

    move v0, v1

    .line 8076
    goto :goto_6

    .line 8079
    :cond_11
    iget-object v2, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/text/TextView;->h()I

    move-result v2

    .line 8080
    iget-object v3, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/text/TextView;->i()I

    move-result v3

    .line 8081
    iget-object v4, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v4}, Lcom/google/android/apps/docs/editors/text/TextView;->f()I

    move-result v4

    .line 8082
    iget-object v5, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v5}, Lcom/google/android/apps/docs/editors/text/TextView;->g()I

    move-result v5

    .line 8084
    iget-object v6, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    .line 8086
    iget-object v7, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v7, v7, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    if-nez v7, :cond_3a

    iget-object v7, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    iput-object v8, v7, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    .line 8087
    :cond_3a
    iget-object v7, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v7, v7, Lcom/google/android/apps/docs/editors/text/TextView;->a:Landroid/graphics/Rect;

    .line 8088
    iput v4, v7, Landroid/graphics/Rect;->left:I

    .line 8089
    iput v2, v7, Landroid/graphics/Rect;->top:I

    .line 8090
    invoke-virtual {v6}, Lcom/google/android/apps/docs/editors/text/TextView;->getWidth()I

    move-result v2

    sub-int/2addr v2, v5

    iput v2, v7, Landroid/graphics/Rect;->right:I

    .line 8091
    invoke-virtual {v6}, Lcom/google/android/apps/docs/editors/text/TextView;->getHeight()I

    move-result v2

    sub-int/2addr v2, v3

    iput v2, v7, Landroid/graphics/Rect;->bottom:I

    .line 8093
    invoke-virtual {v6}, Lcom/google/android/apps/docs/editors/text/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 8094
    if-eqz v2, :cond_5d

    const/4 v3, 0x0

    invoke-interface {v2, v6, v7, v3}, Landroid/view/ViewParent;->getChildVisibleRect(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v2

    if-nez v2, :cond_5f

    :cond_5d
    move v0, v1

    .line 8095
    goto :goto_6

    .line 8098
    :cond_5f
    iget-object v2, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v2, v2, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    .line 8099
    invoke-virtual {v6, v2}, Lcom/google/android/apps/docs/editors/text/TextView;->getLocationInWindow([I)V

    .line 8100
    aget v3, v2, v1

    iget v4, p0, LFu;->a:I

    add-int/2addr v3, v4

    iget v4, p0, LFu;->a:F

    float-to-int v4, v4

    add-int/2addr v3, v4

    .line 8101
    aget v2, v2, v0

    iget v4, p0, LFu;->b:I

    add-int/2addr v2, v4

    .line 8105
    iget v4, v7, Landroid/graphics/Rect;->left:I

    add-int/lit8 v4, v4, -0x1

    if-lt v3, v4, :cond_88

    iget v4, v7, Landroid/graphics/Rect;->right:I

    add-int/lit8 v4, v4, 0x1

    if-gt v3, v4, :cond_88

    iget v3, v7, Landroid/graphics/Rect;->top:I

    if-lt v2, v3, :cond_88

    iget v3, v7, Landroid/graphics/Rect;->bottom:I

    if-le v2, v3, :cond_6

    :cond_88
    move v0, v1

    goto/16 :goto_6
.end method

.method private g()V
    .registers 10

    .prologue
    .line 8016
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 8017
    const/4 v1, 0x0

    .line 8018
    iget v0, p0, LFu;->g:I

    .line 8019
    iget v4, p0, LFu;->h:I

    const/4 v5, 0x5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 8020
    :goto_e
    if-ge v1, v4, :cond_26

    iget-object v5, p0, LFu;->a:[J

    aget-wide v5, v5, v0

    sub-long v5, v2, v5

    const-wide/16 v7, 0x96

    cmp-long v5, v5, v7

    if-gez v5, :cond_26

    .line 8021
    add-int/lit8 v1, v1, 0x1

    .line 8022
    iget v0, p0, LFu;->g:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x5

    rem-int/lit8 v0, v0, 0x5

    goto :goto_e

    .line 8025
    :cond_26
    if-lez v1, :cond_3d

    if-ge v1, v4, :cond_3d

    iget-object v1, p0, LFu;->a:[J

    aget-wide v4, v1, v0

    sub-long v1, v2, v4

    const-wide/16 v3, 0x15e

    cmp-long v1, v1, v3

    if-lez v1, :cond_3d

    .line 8026
    iget-object v1, p0, LFu;->a:[I

    aget v0, v1, v0

    invoke-virtual {p0, v0}, LFu;->a(I)V

    .line 8028
    :cond_3d
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method protected abstract a()V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(II)V
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 8066
    iget-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public b()V
    .registers 7

    .prologue
    .line 8038
    invoke-virtual {p0}, LFu;->a()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 8039
    iget-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    iget v1, p0, LFu;->e:I

    iget v2, p0, LFu;->f:I

    invoke-virtual {p0}, LFu;->getRight()I

    move-result v3

    invoke-virtual {p0}, LFu;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, LFu;->getBottom()I

    move-result v4

    invoke-virtual {p0}, LFu;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 8049
    :goto_21
    return-void

    .line 8042
    :cond_22
    iget-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    iget-object v1, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    const/4 v2, 0x0

    iget v3, p0, LFu;->e:I

    iget v4, p0, LFu;->f:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 8044
    const/4 v0, 0x1

    iput-boolean v0, p0, LFu;->b:Z

    .line 8046
    iget-object v0, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 8047
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    goto :goto_21
.end method

.method protected b(I)V
    .registers 5
    .parameter

    .prologue
    .line 8116
    invoke-direct {p0, p1}, LFu;->d(I)V

    .line 8117
    iget-object v0, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v0, p1}, LEj;->g(I)I

    move-result v0

    .line 8118
    iget-object v1, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, v1, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v1, v0}, LEj;->d(I)I

    move-result v0

    .line 8120
    iget-object v1, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, v1, Lcom/google/android/apps/docs/editors/text/TextView;->a:LEj;

    invoke-interface {v1, p1}, LEj;->a(I)F

    move-result v1

    const/high16 v2, 0x3f00

    sub-float/2addr v1, v2

    iget v2, p0, LFu;->a:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, LFu;->a:I

    .line 8121
    iput v0, p0, LFu;->b:I

    .line 8124
    iget v0, p0, LFu;->a:I

    iget-object v1, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/text/TextView;->e(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LFu;->a:I

    .line 8125
    iget v0, p0, LFu;->b:I

    iget-object v1, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/text/TextView;->f(Lcom/google/android/apps/docs/editors/text/TextView;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LFu;->b:I

    .line 8126
    return-void
.end method

.method protected b()Z
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 8129
    invoke-virtual {p0}, LFu;->a()I

    move-result v2

    invoke-virtual {p0, v2}, LFu;->b(I)V

    .line 8131
    iget v2, p0, LFu;->e:I

    .line 8132
    iget v3, p0, LFu;->f:I

    .line 8134
    iget-object v4, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v5, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v5, v5, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/docs/editors/text/TextView;->getLocationInWindow([I)V

    .line 8135
    iget-object v4, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v4, v4, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    aget v4, v4, v0

    iget v5, p0, LFu;->a:I

    add-int/2addr v4, v5

    iput v4, p0, LFu;->e:I

    .line 8136
    iget-object v4, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v4, v4, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    aget v4, v4, v1

    iget v5, p0, LFu;->b:I

    add-int/2addr v4, v5

    iput v4, p0, LFu;->f:I

    .line 8138
    iget v4, p0, LFu;->e:I

    if-ne v2, v4, :cond_34

    iget v2, p0, LFu;->f:I

    if-eq v3, v2, :cond_35

    :cond_34
    move v0, v1

    :cond_35
    return v0
.end method

.method protected c()V
    .registers 2

    .prologue
    .line 8052
    const/4 v0, 0x0

    iput-boolean v0, p0, LFu;->a:Z

    .line 8053
    iget-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 8054
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 8231
    iget-boolean v0, p0, LFu;->a:Z

    return v0
.end method

.method public d()V
    .registers 2

    .prologue
    .line 8057
    invoke-virtual {p0}, LFu;->c()V

    .line 8059
    const/4 v0, 0x0

    iput-boolean v0, p0, LFu;->b:Z

    .line 8061
    iget-object v0, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 8062
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 8063
    return-void
.end method

.method e()V
    .registers 1

    .prologue
    .line 8236
    return-void
.end method

.method public f()V
    .registers 1

    .prologue
    .line 8240
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 8173
    iget-object v0, p0, LFu;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LFu;->getRight()I

    move-result v1

    invoke-virtual {p0}, LFu;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, LFu;->getBottom()I

    move-result v2

    invoke-virtual {p0}, LFu;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 8174
    iget-object v0, p0, LFu;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 8175
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 8032
    iget-object v0, p0, LFu;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, LFu;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LFu;->setMeasuredDimension(II)V

    .line 8034
    iget-object v0, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/text/TextView;->a(Lcom/google/android/apps/docs/editors/text/TextView;)V

    .line 8035
    return-void
.end method

.method public onPreDraw()Z
    .registers 7

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 8143
    invoke-virtual {p0}, LFu;->b()Z

    move-result v0

    if-eqz v0, :cond_79

    .line 8144
    iget-boolean v0, p0, LFu;->a:Z

    if-eqz v0, :cond_4e

    .line 8145
    iget-object v0, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    aget v0, v0, v3

    iget v1, p0, LFu;->c:I

    if-ne v0, v1, :cond_20

    iget-object v0, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    aget v0, v0, v5

    iget v1, p0, LFu;->d:I

    if-eq v0, v1, :cond_4e

    .line 8146
    :cond_20
    iget v0, p0, LFu;->b:F

    iget-object v1, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, v1, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    aget v1, v1, v3

    iget v2, p0, LFu;->c:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, LFu;->b:F

    .line 8147
    iget v0, p0, LFu;->c:F

    iget-object v1, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v1, v1, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    aget v1, v1, v5

    iget v2, p0, LFu;->d:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, LFu;->c:F

    .line 8148
    iget-object v0, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    aget v0, v0, v3

    iput v0, p0, LFu;->c:I

    .line 8149
    iget-object v0, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    aget v0, v0, v5

    iput v0, p0, LFu;->d:I

    .line 8153
    :cond_4e
    invoke-virtual {p0}, LFu;->e()V

    .line 8155
    invoke-direct {p0}, LFu;->d()Z

    move-result v0

    if-eqz v0, :cond_7a

    .line 8156
    iget-object v0, p0, LFu;->a:Landroid/widget/PopupWindow;

    iget v1, p0, LFu;->e:I

    iget v2, p0, LFu;->f:I

    iget-object v3, p0, LFu;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iget-object v4, p0, LFu;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 8159
    iget-boolean v0, p0, LFu;->b:Z

    if-eqz v0, :cond_79

    invoke-virtual {p0}, LFu;->a()Z

    move-result v0

    if-nez v0, :cond_79

    .line 8160
    invoke-virtual {p0}, LFu;->b()V

    .line 8168
    :cond_79
    :goto_79
    return v5

    .line 8163
    :cond_7a
    invoke-virtual {p0}, LFu;->a()Z

    move-result v0

    if-eqz v0, :cond_79

    .line 8164
    invoke-virtual {p0}, LFu;->c()V

    goto :goto_79
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 8179
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_96

    .line 8227
    :goto_9
    return v5

    .line 8181
    :pswitch_a
    invoke-virtual {p0}, LFu;->a()I

    move-result v0

    invoke-direct {p0, v0}, LFu;->c(I)V

    .line 8182
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v1, p0, LFu;->a:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, LFu;->b:F

    .line 8183
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v1, p0, LFu;->b:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, LFu;->c:F

    .line 8185
    iget-object v0, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/text/TextView;->a:[I

    .line 8186
    iget-object v1, p0, LFu;->a:Lcom/google/android/apps/docs/editors/text/TextView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/text/TextView;->getLocationInWindow([I)V

    .line 8187
    aget v1, v0, v2

    iput v1, p0, LFu;->c:I

    .line 8188
    aget v0, v0, v5

    iput v0, p0, LFu;->d:I

    .line 8189
    iput-boolean v5, p0, LFu;->a:Z

    goto :goto_9

    .line 8194
    :pswitch_39
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    .line 8195
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    .line 8199
    iget v0, p0, LFu;->c:F

    iget v3, p0, LFu;->d:I

    int-to-float v3, v3

    sub-float/2addr v0, v3

    .line 8200
    iget v3, p0, LFu;->b:I

    int-to-float v3, v3

    sub-float v3, v2, v3

    iget v4, p0, LFu;->d:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    .line 8202
    iget v4, p0, LFu;->e:F

    cmpg-float v4, v0, v4

    if-gez v4, :cond_80

    .line 8203
    iget v4, p0, LFu;->e:F

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 8204
    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 8209
    :goto_60
    iget v3, p0, LFu;->d:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    iput v0, p0, LFu;->c:F

    .line 8211
    iget v0, p0, LFu;->b:F

    sub-float v0, v1, v0

    iget v1, p0, LFu;->a:F

    add-float/2addr v0, v1

    .line 8212
    iget v1, p0, LFu;->c:F

    sub-float v1, v2, v1

    iget v2, p0, LFu;->d:F

    add-float/2addr v1, v2

    .line 8214
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LFu;->a(II)V

    goto :goto_9

    .line 8206
    :cond_80
    iget v4, p0, LFu;->e:F

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 8207
    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto :goto_60

    .line 8219
    :pswitch_8b
    invoke-direct {p0}, LFu;->g()V

    .line 8220
    iput-boolean v2, p0, LFu;->a:Z

    goto/16 :goto_9

    .line 8224
    :pswitch_92
    iput-boolean v2, p0, LFu;->a:Z

    goto/16 :goto_9

    .line 8179
    :pswitch_data_96
    .packed-switch 0x0
        :pswitch_a
        :pswitch_8b
        :pswitch_39
        :pswitch_92
    .end packed-switch
.end method
