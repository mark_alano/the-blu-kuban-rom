.class public final LGL;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LGJ;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LGY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 32
    iput-object p1, p0, LGL;->a:LYD;

    .line 33
    const-class v0, LGJ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LGL;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LGL;->a:LZb;

    .line 36
    const-class v0, LGY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LGL;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LGL;->b:LZb;

    .line 39
    return-void
.end method

.method static synthetic a(LGL;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LGL;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 146
    const-class v0, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;

    new-instance v1, LGM;

    invoke-direct {v1, p0}, LGM;-><init>(LGL;)V

    invoke-virtual {p0, v0, v1}, LGL;->a(Ljava/lang/Class;Laou;)V

    .line 154
    const-class v0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    new-instance v1, LGN;

    invoke-direct {v1, p0}, LGN;-><init>(LGL;)V

    invoke-virtual {p0, v0, v1}, LGL;->a(Ljava/lang/Class;Laou;)V

    .line 162
    const-class v0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;

    new-instance v1, LGO;

    invoke-direct {v1, p0}, LGO;-><init>(LGL;)V

    invoke-virtual {p0, v0, v1}, LGL;->a(Ljava/lang/Class;Laou;)V

    .line 170
    const-class v0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;

    new-instance v1, LGP;

    invoke-direct {v1, p0}, LGP;-><init>(LGL;)V

    invoke-virtual {p0, v0, v1}, LGL;->a(Ljava/lang/Class;Laou;)V

    .line 178
    const-class v0, LGJ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LGL;->a:LZb;

    invoke-virtual {p0, v0, v1}, LGL;->a(Laop;LZb;)V

    .line 179
    const-class v0, LGY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LGL;->b:LZb;

    invoke-virtual {p0, v0, v1}, LGL;->a(Laop;LZb;)V

    .line 180
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LHW;

    iget-object v0, v0, LHW;->a:LZb;

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->b:Laoz;

    .line 135
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LGL;

    iget-object v0, v0, LGL;->b:LZb;

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;->a:Laoz;

    .line 141
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LCO;

    iget-object v0, v0, LCO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    .line 79
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LHo;

    iget-object v0, v0, LHo;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHq;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LHq;

    .line 85
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LNl;

    iget-object v0, v0, LNl;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNe;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LNe;

    .line 91
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Lgl;

    .line 97
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LKS;

    .line 103
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LHo;

    iget-object v0, v0, LHo;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHq;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LHq;

    .line 51
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LJT;

    iget-object v0, v0, LJT;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKt;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LKt;

    .line 57
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LKS;

    .line 63
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LNl;

    iget-object v0, v0, LNl;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNe;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixDemoActivity;->a:LNe;

    .line 69
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LHw;

    iget-object v0, v0, LHw;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;

    .line 113
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LJT;

    iget-object v0, v0, LJT;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKt;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LKt;

    .line 119
    iget-object v0, p0, LGL;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LGL;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LKS;

    .line 125
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 184
    return-void
.end method
