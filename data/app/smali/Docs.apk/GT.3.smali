.class public LGT;
.super Ljava/lang/Object;
.source "TrixDataFragment.java"

# interfaces
.implements Lcom/google/android/apps/docs/editors/trix/events/GenericEventHandler;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 286
    iput-object p1, p0, LGT;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent()V
    .registers 4

    .prologue
    .line 289
    iget-object v0, p0, LGT;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LIP;

    move-result-object v0

    invoke-virtual {v0}, LIP;->a()LIN;

    move-result-object v0

    .line 290
    invoke-virtual {v0}, LIN;->a()LIO;

    move-result-object v1

    sget-object v2, LIO;->a:LIO;

    if-ne v1, v2, :cond_18

    .line 291
    iget-object v0, p0, LGT;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V

    .line 306
    :cond_17
    :goto_17
    return-void

    .line 295
    :cond_18
    invoke-virtual {v0}, LIN;->a()LIO;

    move-result-object v1

    sget-object v2, LIO;->b:LIO;

    if-eq v1, v2, :cond_30

    invoke-virtual {v0}, LIN;->a()LIO;

    move-result-object v1

    sget-object v2, LIO;->c:LIO;

    if-eq v1, v2, :cond_30

    invoke-virtual {v0}, LIN;->a()LIO;

    move-result-object v1

    sget-object v2, LIO;->e:LIO;

    if-ne v1, v2, :cond_36

    .line 298
    :cond_30
    iget-object v0, p0, LGT;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->c(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V

    goto :goto_17

    .line 302
    :cond_36
    invoke-virtual {v0}, LIN;->a()LIO;

    move-result-object v1

    sget-object v2, LIO;->d:LIO;

    if-ne v1, v2, :cond_17

    .line 303
    iget-object v1, p0, LGT;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-virtual {v0}, LIN;->a()I

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;I)V

    goto :goto_17
.end method
