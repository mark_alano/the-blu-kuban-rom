.class public final enum LGp;
.super Ljava/lang/Enum;
.source "TextKeyListener.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LGp;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LGp;

.field private static final synthetic a:[LGp;

.field public static final enum b:LGp;

.field public static final enum c:LGp;

.field public static final enum d:LGp;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 219
    new-instance v0, LGp;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LGp;->a:LGp;

    new-instance v0, LGp;

    const-string v1, "SENTENCES"

    invoke-direct {v0, v1, v3}, LGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LGp;->b:LGp;

    new-instance v0, LGp;

    const-string v1, "WORDS"

    invoke-direct {v0, v1, v4}, LGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LGp;->c:LGp;

    new-instance v0, LGp;

    const-string v1, "CHARACTERS"

    invoke-direct {v0, v1, v5}, LGp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LGp;->d:LGp;

    .line 218
    const/4 v0, 0x4

    new-array v0, v0, [LGp;

    sget-object v1, LGp;->a:LGp;

    aput-object v1, v0, v2

    sget-object v1, LGp;->b:LGp;

    aput-object v1, v0, v3

    sget-object v1, LGp;->c:LGp;

    aput-object v1, v0, v4

    sget-object v1, LGp;->d:LGp;

    aput-object v1, v0, v5

    sput-object v0, LGp;->a:[LGp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 218
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LGp;
    .registers 2
    .parameter

    .prologue
    .line 218
    const-class v0, LGp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LGp;

    return-object v0
.end method

.method public static values()[LGp;
    .registers 1

    .prologue
    .line 218
    sget-object v0, LGp;->a:[LGp;

    invoke-virtual {v0}, [LGp;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LGp;

    return-object v0
.end method
