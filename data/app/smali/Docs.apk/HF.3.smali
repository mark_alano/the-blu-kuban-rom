.class public LHF;
.super Ljava/lang/Object;
.source "SheetTabBarFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 77
    iput-object p1, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;LHA;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 77
    invoke-direct {p0, p1}, LHF;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    .line 82
    check-cast p1, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    .line 83
    iget-object v0, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->b()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    move-result-object v0

    .line 85
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()Z

    move-result v1

    if-eqz v1, :cond_62

    iget-object v1, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)LGY;

    move-result-object v1

    invoke-interface {v1}, LGY;->h()Z

    move-result v1

    if-eqz v1, :cond_62

    .line 86
    iget-object v1, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)LHG;

    move-result-object v1

    invoke-interface {v1}, LHG;->a_()Z

    move-result v1

    if-eqz v1, :cond_34

    .line 87
    iget-object v0, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)LHG;

    move-result-object v0

    invoke-interface {v0}, LHG;->a()V

    .line 97
    :goto_33
    return-void

    .line 90
    :cond_34
    iget-object v1, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->getLeft()I

    move-result v0

    add-int/2addr v0, v1

    iget-object v1, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)Landroid/widget/HorizontalScrollView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v1

    sub-int/2addr v0, v1

    .line 91
    iget-object v1, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)LHG;

    move-result-object v1

    iget-object v2, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, LHG;->a(IILjava/lang/String;)V

    goto :goto_33

    .line 94
    :cond_62
    iget-object v0, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)LHG;

    move-result-object v0

    invoke-interface {v0}, LHG;->a()V

    .line 95
    iget-object v0, p0, LHF;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)LGY;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()I

    move-result v1

    invoke-interface {v0, v1}, LGY;->f(I)V

    goto :goto_33
.end method
