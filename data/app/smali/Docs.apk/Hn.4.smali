.class public final enum LHn;
.super Ljava/lang/Enum;
.source "TrixGLRenderer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LHn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LHn;

.field private static final synthetic a:[LHn;

.field public static final enum b:LHn;

.field public static final enum c:LHn;


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, LHn;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v2, v2}, LHn;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHn;->a:LHn;

    .line 17
    new-instance v0, LHn;

    const-string v1, "MOVE"

    invoke-direct {v0, v1, v3, v3}, LHn;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHn;->b:LHn;

    .line 18
    new-instance v0, LHn;

    const-string v1, "UP"

    invoke-direct {v0, v1, v4, v4}, LHn;-><init>(Ljava/lang/String;II)V

    sput-object v0, LHn;->c:LHn;

    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [LHn;

    sget-object v1, LHn;->a:LHn;

    aput-object v1, v0, v2

    sget-object v1, LHn;->b:LHn;

    aput-object v1, v0, v3

    sget-object v1, LHn;->c:LHn;

    aput-object v1, v0, v4

    sput-object v0, LHn;->a:[LHn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    iput p3, p0, LHn;->a:I

    .line 23
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LHn;
    .registers 2
    .parameter

    .prologue
    .line 15
    const-class v0, LHn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LHn;

    return-object v0
.end method

.method public static values()[LHn;
    .registers 1

    .prologue
    .line 15
    sget-object v0, LHn;->a:[LHn;

    invoke-virtual {v0}, [LHn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LHn;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 26
    iget v0, p0, LHn;->a:I

    return v0
.end method
