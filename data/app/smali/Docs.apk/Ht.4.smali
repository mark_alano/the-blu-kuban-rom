.class public final LHt;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 33
    iput-object p1, p0, LHt;->a:LYD;

    .line 34
    const-class v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LHt;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LHt;->a:LZb;

    .line 37
    const-class v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LHt;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LHt;->b:LZb;

    .line 40
    const-class v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LHt;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LHt;->c:LZb;

    .line 43
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 50
    const-class v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LHt;->a:LZb;

    invoke-virtual {p0, v0, v1}, LHt;->a(Laop;LZb;)V

    .line 51
    const-class v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LHt;->b:LZb;

    invoke-virtual {p0, v0, v1}, LHt;->a(Laop;LZb;)V

    .line 52
    const-class v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LHt;->c:LZb;

    invoke-virtual {p0, v0, v1}, LHt;->a(Laop;LZb;)V

    .line 53
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 57
    return-void
.end method
