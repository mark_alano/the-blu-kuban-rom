.class public LIG;
.super Ljava/lang/Object;
.source "ListViewModelHelper.java"


# instance fields
.field private final a:LIL;

.field private final a:LIy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LIy",
            "<",
            "LHR;",
            ">;"
        }
    .end annotation
.end field

.field private a:LJj;

.field private final a:LJk;

.field private a:LJs;

.field private final a:LJv;


# direct methods
.method private constructor <init>(LJv;LIy;LJk;LIL;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LJv;",
            "LIy",
            "<",
            "LHR;",
            ">;",
            "LJk;",
            "LIL;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v0, p0, LIG;->a:LJj;

    .line 40
    iput-object v0, p0, LIG;->a:LJs;

    .line 45
    iput-object p1, p0, LIG;->a:LJv;

    .line 46
    iput-object p2, p0, LIG;->a:LIy;

    .line 47
    iput-object p3, p0, LIG;->a:LJk;

    .line 48
    iput-object p4, p0, LIG;->a:LIL;

    .line 49
    return-void
.end method

.method public static a(LJv;LIL;)LIG;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 131
    invoke-virtual {p1}, LIL;->c()Z

    move-result v0

    if-nez v0, :cond_8

    .line 132
    const/4 v0, 0x0

    .line 152
    :goto_7
    return-object v0

    .line 136
    :cond_8
    new-instance v1, LIy;

    new-instance v0, LIJ;

    invoke-direct {v0}, LIJ;-><init>()V

    invoke-direct {v1, v0}, LIy;-><init>(LIA;)V

    .line 149
    invoke-virtual {p1}, LIL;->a()LJw;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    invoke-direct {v2, v1}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, v0, v2}, LJv;->a(LJw;LIR;)LJk;

    move-result-object v2

    .line 152
    new-instance v0, LIG;

    invoke-direct {v0, p0, v1, v2, p1}, LIG;-><init>(LJv;LIy;LJk;LIL;)V

    goto :goto_7
.end method

.method static synthetic a(LIG;)LJj;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LIG;->a:LJj;

    return-object v0
.end method

.method static synthetic a(LIG;)LJs;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LIG;->a:LJs;

    return-object v0
.end method


# virtual methods
.method public a(I)LHR;
    .registers 4
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, LIG;->a:LIy;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LIy;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHR;

    .line 90
    if-eqz v0, :cond_c

    .line 97
    :goto_b
    return-object v0

    .line 96
    :cond_c
    iget-object v0, p0, LIG;->a:LJk;

    invoke-static {v0, p1}, LJB;->a(LJk;I)I

    move-result v0

    .line 97
    iget-object v1, p0, LIG;->a:LIy;

    invoke-virtual {v1, v0}, LIy;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHR;

    goto :goto_b
.end method

.method public a()LIL;
    .registers 2

    .prologue
    .line 105
    iget-object v0, p0, LIG;->a:LIL;

    return-object v0
.end method

.method public a()LJk;
    .registers 2

    .prologue
    .line 101
    iget-object v0, p0, LIG;->a:LJk;

    return-object v0
.end method

.method public a()LKj;
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 109
    iget-object v1, p0, LIG;->a:LIL;

    invoke-virtual {v1}, LIL;->a()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 110
    iget-object v1, p0, LIG;->a:LJv;

    invoke-virtual {v1}, LJv;->b()I

    move-result v1

    .line 111
    iget-object v2, p0, LIG;->a:LIL;

    invoke-virtual {v2}, LIL;->e()Z

    move-result v2

    if-eqz v2, :cond_24

    .line 114
    new-instance v0, LKj;

    iget-object v2, p0, LIG;->a:LJk;

    invoke-virtual {v2}, LJk;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, LKj;-><init>(II)V

    .line 125
    :cond_23
    :goto_23
    return-object v0

    .line 116
    :cond_24
    if-eqz v1, :cond_23

    new-instance v0, LKj;

    invoke-direct {v0, v3, v1}, LKj;-><init>(II)V

    goto :goto_23

    .line 119
    :cond_2c
    iget-object v1, p0, LIG;->a:LJv;

    invoke-virtual {v1}, LJv;->a()I

    move-result v1

    .line 120
    iget-object v2, p0, LIG;->a:LIL;

    invoke-virtual {v2}, LIL;->d()Z

    move-result v2

    if-eqz v2, :cond_46

    .line 123
    new-instance v0, LKj;

    iget-object v2, p0, LIG;->a:LJk;

    invoke-virtual {v2}, LJk;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, LKj;-><init>(II)V

    goto :goto_23

    .line 125
    :cond_46
    if-eqz v1, :cond_23

    new-instance v0, LKj;

    invoke-direct {v0, v3, v1}, LKj;-><init>(II)V

    goto :goto_23
.end method

.method public a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 76
    iget-object v0, p0, LIG;->a:LJj;

    if-eqz v0, :cond_c

    .line 77
    iget-object v0, p0, LIG;->a:LJj;

    invoke-virtual {v0}, LJj;->a()V

    .line 78
    iput-object v1, p0, LIG;->a:LJj;

    .line 81
    :cond_c
    iget-object v0, p0, LIG;->a:LJs;

    if-eqz v0, :cond_17

    .line 82
    iget-object v0, p0, LIG;->a:LJs;

    invoke-virtual {v0}, LJs;->a()V

    .line 83
    iput-object v1, p0, LIG;->a:LJs;

    .line 85
    :cond_17
    return-void
.end method

.method public a(LIK;)V
    .registers 6
    .parameter

    .prologue
    .line 52
    invoke-virtual {p0}, LIG;->a()V

    .line 54
    new-instance v0, LJj;

    iget-object v1, p0, LIG;->a:LJk;

    new-instance v2, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    new-instance v3, LIH;

    invoke-direct {v3, p0, p1}, LIH;-><init>(LIG;LIK;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1, v2}, LJj;-><init>(LJk;LIR;)V

    iput-object v0, p0, LIG;->a:LJj;

    .line 64
    new-instance v0, LJs;

    iget-object v1, p0, LIG;->a:LJk;

    new-instance v2, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    new-instance v3, LII;

    invoke-direct {v3, p0, p1}, LII;-><init>(LIG;LIK;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1, v2}, LJs;-><init>(LJt;LIR;)V

    iput-object v0, p0, LIG;->a:LJs;

    .line 73
    return-void
.end method
