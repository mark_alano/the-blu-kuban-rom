.class public final enum LIL;
.super Ljava/lang/Enum;
.source "SectionIndex.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LIL;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LIL;

.field private static final synthetic a:[LIL;

.field public static final enum b:LIL;

.field public static final enum c:LIL;

.field public static final enum d:LIL;

.field public static final enum e:LIL;

.field public static final enum f:LIL;

.field public static final enum g:LIL;

.field public static final enum h:LIL;


# instance fields
.field private a:I

.field private a:LJw;

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .registers 16

    .prologue
    const/4 v15, 0x4

    const/4 v14, 0x3

    const/4 v2, 0x0

    const/4 v7, 0x2

    const/4 v3, 0x1

    .line 13
    new-instance v0, LIL;

    const-string v1, "FROZEN_HEADER_ROW"

    sget-object v5, LJw;->a:LJw;

    move v4, v2

    invoke-direct/range {v0 .. v5}, LIL;-><init>(Ljava/lang/String;IIILJw;)V

    sput-object v0, LIL;->a:LIL;

    .line 15
    new-instance v4, LIL;

    const-string v5, "SCROLLABLE_HEADER_ROW"

    sget-object v9, LJw;->b:LJw;

    move v6, v3

    move v8, v2

    invoke-direct/range {v4 .. v9}, LIL;-><init>(Ljava/lang/String;IIILJw;)V

    sput-object v4, LIL;->b:LIL;

    .line 17
    new-instance v5, LIL;

    const-string v6, "FROZEN_HEADER_COLUMN"

    sget-object v10, LJw;->c:LJw;

    move v8, v2

    move v9, v3

    invoke-direct/range {v5 .. v10}, LIL;-><init>(Ljava/lang/String;IIILJw;)V

    sput-object v5, LIL;->c:LIL;

    .line 19
    new-instance v8, LIL;

    const-string v9, "FROZEN_ROW_COLUMN_GRID"

    sget-object v13, LJw;->d:LJw;

    move v10, v14

    move v11, v3

    move v12, v3

    invoke-direct/range {v8 .. v13}, LIL;-><init>(Ljava/lang/String;IIILJw;)V

    sput-object v8, LIL;->d:LIL;

    .line 21
    new-instance v4, LIL;

    const-string v5, "FROZEN_ROW_SCROLLABLE_COLUMN_GRID"

    sget-object v9, LJw;->e:LJw;

    move v6, v15

    move v8, v3

    invoke-direct/range {v4 .. v9}, LIL;-><init>(Ljava/lang/String;IIILJw;)V

    sput-object v4, LIL;->e:LIL;

    .line 23
    new-instance v8, LIL;

    const-string v9, "SCROLLABLE_HEADER_COLUMN"

    const/4 v10, 0x5

    sget-object v13, LJw;->f:LJw;

    move v11, v2

    move v12, v7

    invoke-direct/range {v8 .. v13}, LIL;-><init>(Ljava/lang/String;IIILJw;)V

    sput-object v8, LIL;->f:LIL;

    .line 25
    new-instance v8, LIL;

    const-string v9, "SCROLLABLE_ROW_FROZEN_COLUMN_GRID"

    const/4 v10, 0x6

    sget-object v13, LJw;->g:LJw;

    move v11, v3

    move v12, v7

    invoke-direct/range {v8 .. v13}, LIL;-><init>(Ljava/lang/String;IIILJw;)V

    sput-object v8, LIL;->g:LIL;

    .line 27
    new-instance v4, LIL;

    const-string v5, "SCROLLABLE_GRID"

    const/4 v6, 0x7

    sget-object v9, LJw;->h:LJw;

    move v8, v7

    invoke-direct/range {v4 .. v9}, LIL;-><init>(Ljava/lang/String;IIILJw;)V

    sput-object v4, LIL;->h:LIL;

    .line 11
    const/16 v0, 0x8

    new-array v0, v0, [LIL;

    sget-object v1, LIL;->a:LIL;

    aput-object v1, v0, v2

    sget-object v1, LIL;->b:LIL;

    aput-object v1, v0, v3

    sget-object v1, LIL;->c:LIL;

    aput-object v1, v0, v7

    sget-object v1, LIL;->d:LIL;

    aput-object v1, v0, v14

    sget-object v1, LIL;->e:LIL;

    aput-object v1, v0, v15

    const/4 v1, 0x5

    sget-object v2, LIL;->f:LIL;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LIL;->g:LIL;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LIL;->h:LIL;

    aput-object v2, v0, v1

    sput-object v0, LIL;->a:[LIL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILJw;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "LJw;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, LIL;->a:I

    .line 32
    iput p4, p0, LIL;->b:I

    .line 33
    iput-object p5, p0, LIL;->a:LJw;

    .line 34
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LIL;
    .registers 2
    .parameter

    .prologue
    .line 11
    const-class v0, LIL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LIL;

    return-object v0
.end method

.method public static values()[LIL;
    .registers 1

    .prologue
    .line 11
    sget-object v0, LIL;->a:[LIL;

    invoke-virtual {v0}, [LIL;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIL;

    return-object v0
.end method


# virtual methods
.method public a()LJw;
    .registers 2

    .prologue
    .line 73
    iget-object v0, p0, LIL;->a:LJw;

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 37
    iget v0, p0, LIL;->b:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 41
    iget v0, p0, LIL;->a:I

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 45
    invoke-virtual {p0}, LIL;->a()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p0}, LIL;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public d()Z
    .registers 3

    .prologue
    .line 65
    iget v0, p0, LIL;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public e()Z
    .registers 3

    .prologue
    .line 69
    iget v0, p0, LIL;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method
