.class public LIM;
.super Ljava/lang/Object;
.source "Application.java"


# instance fields
.field private a:J

.field protected a:Z


# direct methods
.method public constructor <init>(JZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, LIM;->a:Z

    .line 17
    iput-wide p1, p0, LIM;->a:J

    .line 18
    return-void
.end method

.method public static a(LIM;)J
    .registers 3
    .parameter

    .prologue
    .line 21
    if-nez p0, :cond_5

    const-wide/16 v0, 0x0

    :goto_4
    return-wide v0

    :cond_5
    iget-wide v0, p0, LIM;->a:J

    goto :goto_4
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Z)LIQ;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 39
    new-instance v0, LIQ;

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Application_CreateApplication(Ljava/lang/String;Ljava/lang/String;Z)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LIQ;-><init>(JZ)V

    return-object v0
.end method


# virtual methods
.method public a(I)LJA;
    .registers 6
    .parameter

    .prologue
    .line 67
    new-instance v0, LJA;

    iget-wide v1, p0, LIM;->a:J

    invoke-static {v1, v2, p0, p1}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Application_activateSheet(JLIM;I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LJA;-><init>(JZ)V

    return-object v0
.end method

.method public a()LJH;
    .registers 5

    .prologue
    .line 75
    new-instance v0, LJH;

    iget-wide v1, p0, LIM;->a:J

    invoke-static {v1, v2, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Application_getUserSessions(JLIM;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LJH;-><init>(JZ)V

    return-object v0
.end method

.method public a()LJK;
    .registers 5

    .prologue
    .line 59
    new-instance v0, LJK;

    iget-wide v1, p0, LIM;->a:J

    invoke-static {v1, v2, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Application_getSheetIds(JLIM;)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, LJK;-><init>(JZ)V

    return-object v0
.end method

.method public a(I)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 63
    iget-wide v0, p0, LIM;->a:J

    invoke-static {v0, v1, p0, p1}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Application_getSheetNameById(JLIM;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_3
    iget-wide v0, p0, LIM;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_19

    .line 30
    iget-boolean v0, p0, LIM;->a:Z

    if-eqz v0, :cond_15

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, LIM;->a:Z

    .line 32
    iget-wide v0, p0, LIM;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->delete_Application(J)V

    .line 34
    :cond_15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LIM;->a:J
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1b

    .line 36
    :cond_19
    monitor-exit p0

    return-void

    .line 29
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LIR;)V
    .registers 8
    .parameter

    .prologue
    .line 71
    iget-wide v0, p0, LIM;->a:J

    invoke-static {p1}, LIR;->a(LIR;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Application_CreateIdleQueue(JLIM;JLIR;)V

    .line 72
    return-void
.end method

.method public a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 43
    iget-wide v0, p0, LIM;->a:J

    invoke-static {v0, v1, p0, p1}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Application_SetConnectedState(JLIM;Z)V

    .line 44
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 47
    iget-wide v0, p0, LIM;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Application_Pause(JLIM;)V

    .line 48
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 51
    iget-wide v0, p0, LIM;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Application_Resume(JLIM;)V

    .line 52
    return-void
.end method

.method protected finalize()V
    .registers 1

    .prologue
    .line 25
    invoke-virtual {p0}, LIM;->a()V

    .line 26
    return-void
.end method
