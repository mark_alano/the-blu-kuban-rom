.class public LIN;
.super Ljava/lang/Object;
.source "ApplicationEvent.java"


# instance fields
.field private a:J

.field protected a:Z


# direct methods
.method public constructor <init>(JZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, LIN;->a:Z

    .line 17
    iput-wide p1, p0, LIN;->a:J

    .line 18
    return-void
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 47
    iget-wide v0, p0, LIN;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->ApplicationEvent_getSheetId(JLIN;)I

    move-result v0

    return v0
.end method

.method public a()LIO;
    .registers 3

    .prologue
    .line 43
    iget-wide v0, p0, LIN;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->ApplicationEvent_getType(JLIN;)I

    move-result v0

    invoke-static {v0}, LIO;->a(I)LIO;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_3
    iget-wide v0, p0, LIN;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_19

    .line 30
    iget-boolean v0, p0, LIN;->a:Z

    if-eqz v0, :cond_15

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, LIN;->a:Z

    .line 32
    iget-wide v0, p0, LIN;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->delete_ApplicationEvent(J)V

    .line 34
    :cond_15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LIN;->a:J
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1b

    .line 36
    :cond_19
    monitor-exit p0

    return-void

    .line 29
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .registers 1

    .prologue
    .line 25
    invoke-virtual {p0}, LIN;->a()V

    .line 26
    return-void
.end method
