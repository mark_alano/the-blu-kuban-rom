.class public LIb;
.super Ljava/lang/Object;
.source "SpreadsheetController.java"

# interfaces
.implements LGK;


# instance fields
.field private a:D

.field private a:I

.field a:LGJ;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LGY;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LIj;

.field private a:LJJ;

.field private a:LJv;

.field private final a:Landroid/os/Handler;

.field private a:Landroid/widget/Scroller;

.field a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

.field private a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

.field private a:Z

.field private b:D

.field private b:I

.field private b:LJJ;

.field private c:D

.field private d:D


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    new-instance v0, LIj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LIj;-><init>(LIb;LIc;)V

    iput-object v0, p0, LIb;->a:LIj;

    .line 177
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LIb;->a:Landroid/os/Handler;

    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, LIb;->a:Z

    return-void
.end method

.method private static a(DDD)D
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 560
    cmpl-double v0, p0, p2

    if-lez v0, :cond_f

    move-wide v2, p0

    .line 561
    :goto_5
    cmpg-double v0, p4, p0

    if-gez v0, :cond_13

    move-wide v0, p0

    .line 564
    :goto_a
    cmpl-double v4, v2, v0

    if-lez v4, :cond_11

    :goto_e
    return-wide v0

    :cond_f
    move-wide v2, p2

    .line 560
    goto :goto_5

    :cond_11
    move-wide v0, v2

    .line 564
    goto :goto_e

    :cond_13
    move-wide v0, p4

    goto :goto_a
.end method

.method static synthetic a(LIb;)D
    .registers 3
    .parameter

    .prologue
    .line 51
    iget-wide v0, p0, LIb;->c:D

    return-wide v0
.end method

.method static synthetic a(LIb;)LJJ;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, LIb;->b:LJJ;

    return-object v0
.end method

.method static synthetic a(LIb;)LJv;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, LIb;->a:LJv;

    return-object v0
.end method

.method private a()Landroid/graphics/Rect;
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 461
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v0

    .line 462
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 463
    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 464
    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 465
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getWidth()I

    move-result v4

    int-to-double v4, v4

    iget-wide v6, p0, LIb;->a:D

    div-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 466
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getHeight()I

    move-result v0

    int-to-double v4, v0

    iget-wide v6, p0, LIb;->a:D

    div-double/2addr v4, v6

    sub-double/2addr v2, v4

    iget v0, p0, LIb;->a:I

    int-to-double v4, v0

    iget-wide v6, p0, LIb;->a:D

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 468
    return-object v1
.end method

.method static synthetic a(LIb;)Landroid/graphics/Rect;
    .registers 2
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, LIb;->a()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 596
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 598
    iget-object v1, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v1

    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 599
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 600
    iget-wide v1, p0, LIb;->c:D

    neg-double v1, v1

    double-to-int v1, v1

    iget-wide v2, p0, LIb;->d:D

    neg-double v2, v2

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 601
    return-object v0
.end method

.method static synthetic a(LIb;)Landroid/widget/Scroller;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, LIb;->a:Landroid/widget/Scroller;

    return-object v0
.end method

.method static synthetic a(LIb;)Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    return-object v0
.end method

.method private a(D)V
    .registers 9
    .parameter

    .prologue
    .line 524
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->c()D

    move-result-wide v0

    iget-wide v4, p0, LIb;->b:D

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, LIb;->a(DDD)D

    move-result-wide v0

    .line 525
    iget-wide v2, p0, LIb;->a:D

    cmpl-double v2, v0, v2

    if-nez v2, :cond_18

    .line 550
    :cond_17
    :goto_17
    return-void

    .line 529
    :cond_18
    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;D)V

    .line 530
    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;D)V

    .line 531
    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;D)V

    .line 532
    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;D)V

    .line 533
    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;D)V

    .line 534
    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;D)V

    .line 535
    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;D)V

    .line 536
    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    move-result-object v2

    invoke-direct {p0, v2, v0, v1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;D)V

    .line 537
    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a()Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->invalidate()V

    .line 539
    iput-wide v0, p0, LIb;->a:D

    .line 540
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->requestLayout()V

    .line 543
    iget-wide v0, p0, LIb;->c:D

    iget-wide v2, p0, LIb;->d:D

    invoke-direct {p0, v0, v1, v2, v3}, LIb;->a(DD)V

    .line 544
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->invalidate()V

    .line 547
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eqz v0, :cond_17

    .line 548
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    iget-object v1, p0, LIb;->a:LJJ;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(LJJ;)V

    goto :goto_17
.end method

.method private a(DD)V
    .registers 25
    .parameter
    .parameter

    .prologue
    .line 472
    invoke-direct/range {p0 .. p0}, LIb;->a()Landroid/graphics/Rect;

    move-result-object v15

    .line 474
    const-wide/16 v3, 0x0

    invoke-virtual {v15}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-double v7, v5

    move-wide/from16 v5, p1

    invoke-static/range {v3 .. v8}, LIb;->a(DDD)D

    move-result-wide v16

    .line 475
    const-wide/16 v3, 0x0

    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-double v7, v5

    move-wide/from16 v5, p3

    invoke-static/range {v3 .. v8}, LIb;->a(DDD)D

    move-result-wide v18

    .line 478
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()D

    move-result-wide v3

    add-double v5, v16, v3

    .line 479
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b()D

    move-result-wide v3

    add-double v7, v18, v3

    .line 481
    move-object/from16 v0, p0

    iget-wide v3, v0, LIb;->c:D

    cmpl-double v3, v16, v3

    if-nez v3, :cond_4a

    move-object/from16 v0, p0

    iget-wide v3, v0, LIb;->d:D

    cmpl-double v3, v18, v3

    if-eqz v3, :cond_62

    .line 482
    :cond_4a
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v4

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;DD)V

    .line 483
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a()Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->invalidate()V

    .line 486
    :cond_62
    move-object/from16 v0, p0

    iget-wide v3, v0, LIb;->c:D

    cmpl-double v3, v16, v3

    if-eqz v3, :cond_8a

    .line 487
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v10

    const-wide/16 v13, 0x0

    move-object/from16 v9, p0

    move-wide v11, v5

    invoke-direct/range {v9 .. v14}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;DD)V

    .line 488
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    move-result-object v10

    const-wide/16 v13, 0x0

    move-object/from16 v9, p0

    move-wide v11, v5

    invoke-direct/range {v9 .. v14}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;DD)V

    .line 491
    :cond_8a
    move-object/from16 v0, p0

    iget-wide v3, v0, LIb;->d:D

    cmpl-double v3, v18, v3

    if-eqz v3, :cond_b0

    .line 492
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v4

    const-wide/16 v5, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;DD)V

    .line 493
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d()Lcom/google/android/apps/docs/editors/trix/view/NativeListView;

    move-result-object v4

    const-wide/16 v5, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;DD)V

    .line 497
    :cond_b0
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:LJv;

    invoke-virtual {v3}, LJv;->b()Z

    move-result v3

    if-nez v3, :cond_d8

    .line 498
    invoke-virtual {v15}, Landroid/graphics/Rect;->height()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, LIb;->a:I

    sub-int/2addr v3, v4

    .line 500
    int-to-double v4, v3

    cmpl-double v4, v18, v4

    if-lez v4, :cond_e5

    .line 502
    move-object/from16 v0, p0

    iget-object v4, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    int-to-double v5, v3

    sub-double v5, v18, v5

    move-object/from16 v0, p0

    iget-wide v7, v0, LIb;->a:D

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v4, v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a(I)V

    .line 509
    :cond_d8
    :goto_d8
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, LIb;->c:D

    .line 510
    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, LIb;->d:D

    .line 511
    return-void

    .line 505
    :cond_e5
    move-object/from16 v0, p0

    iget-object v3, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a()V

    goto :goto_d8
.end method

.method private a(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 636
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a(II)Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v0

    .line 637
    invoke-direct {p0, v0}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    .line 639
    invoke-virtual {p0, p1, p2}, LIb;->a(II)Landroid/graphics/Rect;

    move-result-object v0

    .line 640
    new-instance v1, LJJ;

    invoke-direct {v1, p1, p2}, LJJ;-><init>(II)V

    iput-object v1, p0, LIb;->a:LJJ;

    .line 643
    iget-wide v1, p0, LIb;->c:D

    double-to-int v1, v1

    iget-wide v2, p0, LIb;->d:D

    double-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 645
    iget-object v1, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->c()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v1

    neg-int v1, v1

    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->b()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 647
    iget-object v1, p0, LIb;->a:LJv;

    iget-object v2, p0, LIb;->a:LJJ;

    invoke-virtual {v1, v2}, LJv;->a(LJJ;)Ljava/lang/String;

    move-result-object v1

    .line 648
    iget-object v2, p0, LIb;->a:LJv;

    iget-object v3, p0, LIb;->a:LJJ;

    invoke-virtual {v2, v3}, LJv;->a(LJJ;)LIW;

    move-result-object v2

    .line 650
    iget-object v3, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v3, v0, v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Landroid/graphics/Rect;LIW;)V

    .line 652
    iget-object v0, p0, LIb;->a:LGJ;

    invoke-interface {v0, v1, p0}, LGJ;->a(Ljava/lang/CharSequence;LGK;)V

    .line 653
    return-void
.end method

.method private a(IIZ)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 620
    iget-object v0, p0, LIb;->a:LJv;

    new-instance v1, LJl;

    new-instance v2, LIX;

    new-instance v3, LJq;

    invoke-direct {v3, p1, p2, v4, v4}, LJq;-><init>(IIII)V

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, LIX;-><init>(LJq;Z)V

    invoke-direct {v1, v2}, LJl;-><init>(LIX;)V

    invoke-virtual {v0, v1}, LJv;->a(LJl;)V

    .line 622
    new-instance v0, LJJ;

    invoke-direct {v0, p1, p2}, LJJ;-><init>(II)V

    iput-object v0, p0, LIb;->b:LJJ;

    .line 626
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a(II)Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v0

    .line 627
    invoke-virtual {p0, p1, p2}, LIb;->a(II)Landroid/graphics/Rect;

    move-result-object v1

    .line 628
    invoke-direct {p0, v1, v0}, LIb;->a(Landroid/graphics/Rect;Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    .line 630
    if-nez p3, :cond_30

    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eqz v0, :cond_33

    .line 631
    :cond_30
    invoke-direct {p0, p1, p2}, LIb;->a(II)V

    .line 633
    :cond_33
    return-void
.end method

.method private a(IIZZ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 384
    iget-object v0, p0, LIb;->b:LJJ;

    if-eqz v0, :cond_6

    if-nez p4, :cond_7

    .line 403
    :cond_6
    :goto_6
    return-void

    .line 389
    :cond_7
    iget-object v0, p0, LIb;->b:LJJ;

    invoke-virtual {v0}, LJJ;->a()I

    move-result v0

    add-int/2addr v0, p1

    .line 390
    iget-object v1, p0, LIb;->b:LJJ;

    invoke-virtual {v1}, LJJ;->b()I

    move-result v1

    add-int/2addr v1, p2

    .line 392
    if-nez p3, :cond_6

    .line 397
    invoke-direct {p0, v0, v1}, LIb;->a(II)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 398
    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, LIb;->a(IIZ)V

    .line 400
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->sendAccessibilityEvent(I)V

    goto :goto_6
.end method

.method static synthetic a(LIb;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, LIb;->h()V

    return-void
.end method

.method static synthetic a(LIb;D)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, LIb;->a(D)V

    return-void
.end method

.method static synthetic a(LIb;DD)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, LIb;->a(DD)V

    return-void
.end method

.method static synthetic a(LIb;II)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, LIb;->a(II)V

    return-void
.end method

.method static synthetic a(LIb;IIZ)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, LIb;->a(IIZ)V

    return-void
.end method

.method static synthetic a(LIb;IIZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, LIb;->a(IIZZ)V

    return-void
.end method

.method static synthetic a(LIb;Landroid/graphics/Point;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, LIb;->a(Landroid/graphics/Point;)V

    return-void
.end method

.method static synthetic a(LIb;Landroid/view/MotionEvent;Z)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, LIb;->a(Landroid/view/MotionEvent;Z)V

    return-void
.end method

.method static synthetic a(LIb;Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    return-void
.end method

.method private a(Landroid/graphics/Point;)V
    .registers 3
    .parameter

    .prologue
    .line 294
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;->j()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 295
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;->v()V

    .line 299
    :goto_d
    return-void

    .line 297
    :cond_e
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;->a(Landroid/graphics/Point;)V

    goto :goto_d
.end method

.method private a(Landroid/graphics/Rect;I)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 731
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    if-le v0, p2, :cond_20

    .line 734
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-ge v0, p2, :cond_21

    .line 735
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, p2

    div-int/lit8 v1, p2, 0x2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 741
    :goto_17
    iget-wide v1, p0, LIb;->c:D

    iget-wide v3, p0, LIb;->d:D

    int-to-double v5, v0

    add-double/2addr v3, v5

    invoke-direct {p0, v1, v2, v3, v4}, LIb;->a(DD)V

    .line 743
    :cond_20
    return-void

    .line 738
    :cond_21
    iget v0, p1, Landroid/graphics/Rect;->top:I

    goto :goto_17
.end method

.method private a(Landroid/graphics/Rect;Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 752
    iget v0, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v1

    if-ge v0, v1, :cond_1d

    .line 753
    iget-wide v0, p0, LIb;->c:D

    iget-wide v2, p0, LIb;->d:D

    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v4

    iget v5, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    int-to-double v4, v4

    sub-double/2addr v2, v4

    iget v4, p0, LIb;->b:I

    int-to-double v4, v4

    sub-double/2addr v2, v4

    invoke-direct {p0, v0, v1, v2, v3}, LIb;->a(DD)V

    .line 764
    :cond_1c
    :goto_1c
    return-void

    .line 755
    :cond_1d
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getBottom()I

    move-result v1

    if-le v0, v1, :cond_3a

    .line 756
    iget-wide v0, p0, LIb;->c:D

    iget-wide v2, p0, LIb;->d:D

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getBottom()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-double v4, v4

    add-double/2addr v2, v4

    iget v4, p0, LIb;->b:I

    int-to-double v4, v4

    add-double/2addr v2, v4

    invoke-direct {p0, v0, v1, v2, v3}, LIb;->a(DD)V

    goto :goto_1c

    .line 758
    :cond_3a
    iget v0, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v1

    if-ge v0, v1, :cond_57

    .line 759
    iget-wide v0, p0, LIb;->c:D

    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v2

    iget v3, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-double v2, v2

    sub-double/2addr v0, v2

    iget v2, p0, LIb;->b:I

    int-to-double v2, v2

    sub-double/2addr v0, v2

    iget-wide v2, p0, LIb;->d:D

    invoke-direct {p0, v0, v1, v2, v3}, LIb;->a(DD)V

    goto :goto_1c

    .line 761
    :cond_57
    iget v0, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getRight()I

    move-result v1

    if-le v0, v1, :cond_1c

    .line 762
    iget-wide v0, p0, LIb;->c:D

    iget v2, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getRight()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-double v2, v2

    add-double/2addr v0, v2

    iget v2, p0, LIb;->b:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    iget-wide v2, p0, LIb;->d:D

    invoke-direct {p0, v0, v1, v2, v3}, LIb;->a(DD)V

    goto :goto_1c
.end method

.method private a(Landroid/view/MotionEvent;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 605
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 606
    iget-object v1, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a(Landroid/graphics/Point;)Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v1

    .line 608
    if-eqz v1, :cond_38

    .line 610
    iget v2, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Point;->x:I

    .line 611
    iget v2, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Point;->y:I

    .line 613
    invoke-virtual {p0, v0, v1}, LIb;->a(Landroid/graphics/Point;Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LJJ;

    move-result-object v0

    .line 614
    invoke-virtual {v0}, LJJ;->a()I

    move-result v1

    invoke-virtual {v0}, LJJ;->b()I

    move-result v0

    invoke-direct {p0, v1, v0, p2}, LIb;->a(IIZ)V

    .line 617
    :cond_38
    return-void
.end method

.method private a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;D)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 519
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a()LJk;

    .line 520
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a()LJk;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LJk;->a(D)V

    .line 521
    return-void
.end method

.method private a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;DD)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 457
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a()LJk;

    move-result-object v0

    new-instance v1, LJI;

    invoke-direct {v1, p2, p3, p4, p5}, LJI;-><init>(DD)V

    invoke-virtual {v0, v1}, LJk;->a(LJI;)V

    .line 458
    return-void
.end method

.method private a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V
    .registers 3
    .parameter

    .prologue
    .line 656
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eqz v0, :cond_12

    .line 657
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()Ljava/lang/String;

    .line 659
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eq p1, v0, :cond_12

    .line 660
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b()V

    .line 663
    :cond_12
    iput-object p1, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    .line 664
    return-void
.end method

.method private a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;D)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 514
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()LJb;

    .line 515
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()LJb;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LJb;->a(D)V

    .line 516
    return-void
.end method

.method private a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;DD)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 453
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()LJb;

    move-result-object v0

    new-instance v1, LJI;

    invoke-direct {v1, p2, p3, p4, p5}, LJI;-><init>(DD)V

    invoke-virtual {v0, v1}, LJb;->a(LJI;)V

    .line 454
    return-void
.end method

.method private a(II)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 408
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()LJb;

    move-result-object v0

    invoke-virtual {v0}, LJb;->a()LJJ;

    move-result-object v0

    .line 409
    if-ltz p1, :cond_20

    invoke-virtual {v0}, LJJ;->a()I

    move-result v1

    if-ge p1, v1, :cond_20

    if-ltz p2, :cond_20

    invoke-virtual {v0}, LJJ;->b()I

    move-result v0

    if-ge p2, v0, :cond_20

    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method static synthetic b(LIb;)D
    .registers 3
    .parameter

    .prologue
    .line 51
    iget-wide v0, p0, LIb;->a:D

    return-wide v0
.end method

.method static synthetic b(LIb;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, LIb;->j()V

    return-void
.end method

.method static synthetic c(LIb;)D
    .registers 3
    .parameter

    .prologue
    .line 51
    iget-wide v0, p0, LIb;->d:D

    return-wide v0
.end method

.method static synthetic c(LIb;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, LIb;->k()V

    return-void
.end method

.method private g()V
    .registers 3

    .prologue
    .line 241
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    iget-object v1, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;->a(Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;)V

    .line 242
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    new-instance v1, LId;

    invoke-direct {v1, p0}, LId;-><init>(LIb;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;->a(LHv;)V

    .line 269
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    new-instance v1, LIe;

    invoke-direct {v1, p0}, LIe;-><init>(LIb;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;->a(LHu;)V

    .line 288
    return-void
.end method

.method private h()V
    .registers 2

    .prologue
    .line 305
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;->v()V

    .line 306
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;->v()V

    .line 307
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;->v()V

    .line 308
    return-void
.end method

.method private i()V
    .registers 3

    .prologue
    .line 312
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LaS;->a(Landroid/view/View;I)V

    .line 313
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    new-instance v1, LIf;

    invoke-direct {v1, p0}, LIf;-><init>(LIb;)V

    invoke-static {v0, v1}, LaS;->a(Landroid/view/View;Lag;)V

    .line 325
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    new-instance v1, LIg;

    invoke-direct {v1, p0}, LIg;-><init>(LIb;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 367
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    new-instance v1, LIh;

    invoke-direct {v1, p0}, LIh;-><init>(LIb;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 378
    return-void
.end method

.method private j()V
    .registers 3

    .prologue
    .line 413
    iget-boolean v0, p0, LIb;->a:Z

    if-nez v0, :cond_11

    .line 416
    const/4 v0, 0x1

    iput-boolean v0, p0, LIb;->a:Z

    .line 417
    iget-object v0, p0, LIb;->a:Landroid/os/Handler;

    new-instance v1, LIi;

    invoke-direct {v1, p0}, LIi;-><init>(LIb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 424
    :cond_11
    return-void
.end method

.method private k()V
    .registers 2

    .prologue
    .line 427
    iget-object v0, p0, LIb;->a:LJv;

    if-nez v0, :cond_5

    .line 437
    :cond_4
    :goto_4
    return-void

    .line 432
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, LIb;->a:Z

    .line 433
    iget-object v0, p0, LIb;->a:LJv;

    invoke-virtual {v0}, LJv;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 435
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a()V

    goto :goto_4
.end method


# virtual methods
.method public a(Landroid/graphics/Point;Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LJJ;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 573
    if-nez p2, :cond_4

    .line 574
    const/4 v0, 0x0

    .line 577
    :goto_3
    return-object v0

    :cond_4
    new-instance v0, Landroid/graphics/Point;

    iget v1, p1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p1, Landroid/graphics/Point;->y:I

    iget-object v3, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Landroid/graphics/Point;)LJJ;

    move-result-object v0

    goto :goto_3
.end method

.method public a(II)Landroid/graphics/Rect;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 582
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a(II)Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v0

    .line 583
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(II)LJq;

    move-result-object v1

    .line 584
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v1}, LJq;->a()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v1}, LJq;->b()I

    move-result v4

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1}, LJq;->c()I

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v1}, LJq;->d()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v0

    add-int/2addr v0, v1

    invoke-direct {v2, v3, v4, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v2
.end method

.method public a()V
    .registers 4

    .prologue
    .line 678
    iget-object v0, p0, LIb;->b:LJJ;

    invoke-virtual {v0}, LJJ;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, LIb;->b:LJJ;

    invoke-virtual {v1}, LJJ;->b()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, LIb;->a(IIZ)V

    .line 679
    return-void
.end method

.method public a(IIII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 708
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    if-nez v0, :cond_5

    .line 721
    :cond_4
    :goto_4
    return-void

    .line 714
    :cond_5
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eqz v0, :cond_4

    .line 716
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    iget-object v1, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LIb;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    .line 718
    iget-object v1, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v1

    sub-int v1, p2, v1

    .line 719
    invoke-direct {p0, v0, v1}, LIb;->a(Landroid/graphics/Rect;I)V

    goto :goto_4
.end method

.method public a(LJv;Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;)V
    .registers 11
    .parameter
    .parameter

    .prologue
    const-wide/high16 v6, 0x3ff0

    const-wide/16 v4, 0x0

    .line 191
    iput-object p1, p0, LIb;->a:LJv;

    .line 192
    iput-object p2, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    .line 193
    new-instance v1, Landroid/widget/Scroller;

    iget-object v0, p0, LIb;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LIb;->a:Landroid/widget/Scroller;

    .line 195
    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LsB;->trix_vertical_overscroll:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LIb;->a:I

    .line 197
    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LsB;->trix_cell_edge_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LIb;->b:I

    .line 200
    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->d()Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()LIB;

    move-result-object v0

    .line 201
    new-instance v1, LIc;

    invoke-direct {v1, p0}, LIc;-><init>(LIb;)V

    invoke-virtual {v0, v1}, LIB;->a(LIF;)V

    .line 212
    invoke-direct {p0}, LIb;->i()V

    .line 213
    invoke-direct {p0}, LIb;->g()V

    .line 221
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 222
    iget-object v0, p0, LIb;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 224
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 225
    iget v0, v1, Landroid/util/DisplayMetrics;->xdpi:F

    iget v1, v1, Landroid/util/DisplayMetrics;->ydpi:F

    add-float/2addr v0, v1

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    const/high16 v1, 0x4290

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 227
    iput-wide v6, p0, LIb;->a:D

    .line 228
    mul-double v2, v6, v0

    iput-wide v2, p0, LIb;->b:D

    .line 229
    iput-wide v4, p0, LIb;->c:D

    .line 230
    iput-wide v4, p0, LIb;->d:D

    .line 232
    const-wide/high16 v2, 0x3fe0

    mul-double/2addr v0, v2

    invoke-direct {p0, v0, v1}, LIb;->a(D)V

    .line 233
    invoke-direct {p0}, LIb;->j()V

    .line 234
    return-void
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 700
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    if-eqz v0, :cond_9

    .line 701
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Ljava/lang/CharSequence;I)V

    .line 703
    :cond_9
    return-void
.end method

.method public b()V
    .registers 4

    .prologue
    .line 683
    iget-object v0, p0, LIb;->b:LJJ;

    invoke-virtual {v0}, LJJ;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LIb;->b:LJJ;

    invoke-virtual {v1}, LJJ;->b()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, LIb;->a(IIZ)V

    .line 684
    return-void
.end method

.method public c()V
    .registers 4

    .prologue
    .line 688
    iget-object v0, p0, LIb;->b:LJJ;

    invoke-virtual {v0}, LJJ;->a()I

    move-result v0

    iget-object v1, p0, LIb;->b:LJJ;

    invoke-virtual {v1}, LJJ;->b()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, LIb;->a(IIZ)V

    .line 689
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 693
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b()V

    .line 694
    const/4 v0, 0x0

    iput-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    .line 695
    iget-object v0, p0, LIb;->a:LGJ;

    invoke-interface {v0}, LGJ;->a()V

    .line 696
    return-void
.end method

.method public e()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 440
    iput-object v0, p0, LIb;->a:LJv;

    .line 441
    iput-object v0, p0, LIb;->a:Landroid/widget/Scroller;

    .line 442
    return-void
.end method

.method public f()V
    .registers 7

    .prologue
    .line 553
    iget-object v0, p0, LIb;->a:Landroid/widget/Scroller;

    if-eqz v0, :cond_28

    iget-object v0, p0, LIb;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 554
    iget-object v0, p0, LIb;->a:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    int-to-double v0, v0

    iget-wide v2, p0, LIb;->a:D

    div-double/2addr v0, v2

    iget-object v2, p0, LIb;->a:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    int-to-double v2, v2

    iget-wide v4, p0, LIb;->a:D

    div-double/2addr v2, v4

    invoke-direct {p0, v0, v1, v2, v3}, LIb;->a(DD)V

    .line 555
    iget-object v0, p0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->postInvalidate()V

    .line 557
    :cond_28
    return-void
.end method
