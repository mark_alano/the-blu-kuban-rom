.class LId;
.super Ljava/lang/Object;
.source "SpreadsheetController.java"

# interfaces
.implements LHv;


# instance fields
.field final synthetic a:LIb;


# direct methods
.method constructor <init>(LIb;)V
    .registers 2
    .parameter

    .prologue
    .line 242
    iput-object p1, p0, LId;->a:LIb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(II)Landroid/graphics/Point;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 248
    iget-object v0, p0, LId;->a:LIb;

    iget-object v0, v0, LIb;->a:Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;->a()Landroid/graphics/Point;

    move-result-object v1

    .line 250
    iget v0, v1, Landroid/graphics/Point;->x:I

    add-int/2addr v0, p1

    iget-object v2, p0, LId;->a:LIb;

    invoke-static {v2}, LIb;->a(LIb;)Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getRight()I

    move-result v2

    if-ge v0, v2, :cond_31

    .line 251
    iget v0, v1, Landroid/graphics/Point;->x:I

    .line 258
    :goto_19
    iget v2, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v2, p2

    iget-object v3, p0, LId;->a:LIb;

    invoke-static {v3}, LIb;->a(LIb;)Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->getTop()I

    move-result v3

    if-le v2, v3, :cond_35

    .line 259
    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, p2

    .line 265
    :goto_2b
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2

    .line 254
    :cond_31
    iget v0, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, p1

    goto :goto_19

    .line 262
    :cond_35
    iget v1, v1, Landroid/graphics/Point;->y:I

    goto :goto_2b
.end method
