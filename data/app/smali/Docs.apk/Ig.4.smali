.class LIg;
.super Ljava/lang/Object;
.source "SpreadsheetController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:LIb;


# direct methods
.method constructor <init>(LIb;)V
    .registers 2
    .parameter

    .prologue
    .line 325
    iput-object p1, p0, LIg;->a:LIb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 330
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_13

    move v0, v1

    .line 332
    :goto_a
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v3

    .line 334
    sparse-switch p2, :sswitch_data_74

    move v1, v2

    .line 363
    :cond_12
    :goto_12
    return v1

    :cond_13
    move v0, v2

    .line 330
    goto :goto_a

    .line 336
    :sswitch_15
    iget-object v4, p0, LIg;->a:LIb;

    invoke-static {v4, v5, v2, v3, v0}, LIb;->a(LIb;IIZZ)V

    goto :goto_12

    .line 340
    :sswitch_1b
    iget-object v4, p0, LIg;->a:LIb;

    invoke-static {v4, v1, v2, v3, v0}, LIb;->a(LIb;IIZZ)V

    goto :goto_12

    .line 344
    :sswitch_21
    iget-object v4, p0, LIg;->a:LIb;

    invoke-static {v4, v2, v1, v3, v0}, LIb;->a(LIb;IIZZ)V

    goto :goto_12

    .line 348
    :sswitch_27
    iget-object v4, p0, LIg;->a:LIb;

    invoke-static {v4, v2, v5, v3, v0}, LIb;->a(LIb;IIZZ)V

    goto :goto_12

    .line 353
    :sswitch_2d
    if-eqz v0, :cond_12

    .line 354
    iget-object v0, p0, LIg;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)LJJ;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 355
    iget-object v0, p0, LIg;->a:LIb;

    iget-object v2, p0, LIg;->a:LIb;

    invoke-static {v2}, LIb;->a(LIb;)Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    move-result-object v2

    iget-object v3, p0, LIg;->a:LIb;

    invoke-static {v3}, LIb;->a(LIb;)LJJ;

    move-result-object v3

    invoke-virtual {v3}, LJJ;->a()I

    move-result v3

    iget-object v4, p0, LIg;->a:LIb;

    invoke-static {v4}, LIb;->a(LIb;)LJJ;

    move-result-object v4

    invoke-virtual {v4}, LJJ;->b()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a(II)Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    move-result-object v2

    invoke-static {v0, v2}, LIb;->a(LIb;Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    .line 357
    iget-object v0, p0, LIg;->a:LIb;

    iget-object v2, p0, LIg;->a:LIb;

    invoke-static {v2}, LIb;->a(LIb;)LJJ;

    move-result-object v2

    invoke-virtual {v2}, LJJ;->a()I

    move-result v2

    iget-object v3, p0, LIg;->a:LIb;

    invoke-static {v3}, LIb;->a(LIb;)LJJ;

    move-result-object v3

    invoke-virtual {v3}, LJJ;->b()I

    move-result v3

    invoke-static {v0, v2, v3}, LIb;->a(LIb;II)V

    goto :goto_12

    .line 334
    :sswitch_data_74
    .sparse-switch
        0x13 -> :sswitch_27
        0x14 -> :sswitch_21
        0x15 -> :sswitch_15
        0x16 -> :sswitch_1b
        0x17 -> :sswitch_2d
        0x42 -> :sswitch_2d
    .end sparse-switch
.end method
