.class LIj;
.super LMA;
.source "SpreadsheetController.java"

# interfaces
.implements LMC;


# instance fields
.field private a:D

.field final synthetic a:LIb;

.field private b:D


# direct methods
.method private constructor <init>(LIb;)V
    .registers 2
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, LIj;->a:LIb;

    invoke-direct {p0}, LMA;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LIb;LIc;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 59
    invoke-direct {p0, p1}, LIj;-><init>(LIb;)V

    return-void
.end method


# virtual methods
.method public a(LMB;)V
    .registers 2
    .parameter

    .prologue
    .line 147
    return-void
.end method

.method public a(LMB;)Z
    .registers 8
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)V

    .line 140
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)D

    move-result-wide v0

    invoke-virtual {p1}, LMB;->a()F

    move-result v2

    float-to-double v2, v2

    iget-object v4, p0, LIj;->a:LIb;

    invoke-static {v4}, LIb;->b(LIb;)D

    move-result-wide v4

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, LIj;->a:D

    .line 141
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->c(LIb;)D

    move-result-wide v0

    invoke-virtual {p1}, LMB;->b()F

    move-result v2

    float-to-double v2, v2

    iget-object v4, p0, LIj;->a:LIb;

    invoke-static {v4}, LIb;->b(LIb;)D

    move-result-wide v4

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, LIj;->b:D

    .line 142
    const/4 v0, 0x1

    return v0
.end method

.method public a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 80
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)Landroid/widget/Scroller;

    move-result-object v0

    if-nez v0, :cond_a

    .line 96
    :goto_9
    return v5

    .line 84
    :cond_a
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)V

    .line 85
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 86
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)Landroid/graphics/Rect;

    move-result-object v7

    .line 87
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)Landroid/widget/Scroller;

    move-result-object v0

    iget-object v1, p0, LIj;->a:LIb;

    invoke-static {v1}, LIb;->a(LIb;)D

    move-result-wide v1

    iget-object v3, p0, LIj;->a:LIb;

    invoke-static {v3}, LIb;->b(LIb;)D

    move-result-wide v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    iget-object v2, p0, LIj;->a:LIb;

    invoke-static {v2}, LIb;->c(LIb;)D

    move-result-wide v2

    iget-object v4, p0, LIj;->a:LIb;

    invoke-static {v4}, LIb;->b(LIb;)D

    move-result-wide v8

    mul-double/2addr v2, v8

    double-to-int v2, v2

    neg-float v3, p3

    float-to-int v3, v3

    neg-float v4, p4

    float-to-int v4, v4

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v8

    move v7, v5

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 96
    const/4 v5, 0x1

    goto :goto_9
.end method

.method public b(LMB;)Z
    .registers 11
    .parameter

    .prologue
    .line 124
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)Landroid/widget/Scroller;

    move-result-object v0

    if-nez v0, :cond_a

    .line 125
    const/4 v0, 0x0

    .line 134
    :goto_9
    return v0

    .line 128
    :cond_a
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 129
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->b(LIb;)D

    move-result-wide v0

    invoke-virtual {p1}, LMB;->c()F

    move-result v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    .line 130
    iget-object v2, p0, LIj;->a:LIb;

    invoke-static {v2, v0, v1}, LIb;->a(LIb;D)V

    .line 131
    iget-object v2, p0, LIj;->a:LIb;

    iget-wide v3, p0, LIj;->a:D

    invoke-virtual {p1}, LMB;->a()F

    move-result v5

    float-to-double v5, v5

    div-double/2addr v5, v0

    sub-double/2addr v3, v5

    iget-wide v5, p0, LIj;->b:D

    invoke-virtual {p1}, LMB;->b()F

    move-result v7

    float-to-double v7, v7

    div-double v0, v7, v0

    sub-double v0, v5, v0

    invoke-static {v2, v3, v4, v0, v1}, LIb;->a(LIb;DD)V

    .line 134
    const/4 v0, 0x1

    goto :goto_9
.end method

.method public b(Landroid/view/MotionEvent;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 101
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SpreadsheetView;->a()Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/SelectionOverlay;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_36

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 103
    iget-object v0, p0, LIj;->a:LIb;

    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-static {v0, v1}, LIb;->a(LIb;Landroid/graphics/Point;)V

    .line 109
    :goto_35
    return v4

    .line 107
    :cond_36
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)V

    .line 108
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0, p1, v4}, LIb;->a(LIb;Landroid/view/MotionEvent;Z)V

    goto :goto_35
.end method

.method public b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)Landroid/widget/Scroller;

    move-result-object v0

    if-nez v0, :cond_a

    .line 68
    const/4 v0, 0x0

    .line 75
    :goto_9
    return v0

    .line 71
    :cond_a
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)V

    .line 73
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 74
    iget-object v0, p0, LIj;->a:LIb;

    iget-object v1, p0, LIj;->a:LIb;

    invoke-static {v1}, LIb;->a(LIb;)D

    move-result-wide v1

    float-to-double v3, p3

    iget-object v5, p0, LIj;->a:LIb;

    invoke-static {v5}, LIb;->b(LIb;)D

    move-result-wide v5

    div-double/2addr v3, v5

    add-double/2addr v1, v3

    iget-object v3, p0, LIj;->a:LIb;

    invoke-static {v3}, LIb;->c(LIb;)D

    move-result-wide v3

    float-to-double v5, p4

    iget-object v7, p0, LIj;->a:LIb;

    invoke-static {v7}, LIb;->b(LIb;)D

    move-result-wide v7

    div-double/2addr v5, v7

    add-double/2addr v3, v5

    invoke-static {v0, v1, v2, v3, v4}, LIb;->a(LIb;DD)V

    .line 75
    const/4 v0, 0x1

    goto :goto_9
.end method

.method public c(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, LIj;->a:LIb;

    invoke-static {v0}, LIb;->a(LIb;)V

    .line 117
    iget-object v0, p0, LIj;->a:LIb;

    iget-object v0, v0, LIb;->a:LGY;

    if-eqz v0, :cond_1c

    iget-object v0, p0, LIj;->a:LIb;

    iget-object v0, v0, LIb;->a:LGY;

    invoke-interface {v0}, LGY;->h()Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    .line 118
    :goto_16
    iget-object v1, p0, LIj;->a:LIb;

    invoke-static {v1, p1, v0}, LIb;->a(LIb;Landroid/view/MotionEvent;Z)V

    .line 119
    return v0

    .line 117
    :cond_1c
    const/4 v0, 0x0

    goto :goto_16
.end method
