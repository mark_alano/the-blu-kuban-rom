.class public LIo;
.super Ljava/lang/Object;
.source "TrixNativeGridView.java"

# interfaces
.implements LIF;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V
    .registers 2
    .parameter

    .prologue
    .line 184
    iput-object p1, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LIY;)V
    .registers 5
    .parameter

    .prologue
    .line 187
    invoke-virtual {p1}, LIY;->a()LIZ;

    move-result-object v0

    .line 189
    iget-object v1, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    .line 190
    iget-object v1, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    .line 192
    sget-object v1, LIZ;->b:LIZ;

    if-ne v0, v1, :cond_32

    invoke-virtual {p1}, LIY;->a()LJo;

    move-result-object v1

    invoke-virtual {v1}, LJo;->a()I

    move-result v1

    iget-object v2, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-static {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LIB;

    move-result-object v2

    invoke-virtual {v2}, LIB;->a()I

    move-result v2

    if-ge v1, v2, :cond_32

    .line 195
    const-string v1, "TrixGridView"

    const-string v2, "Frozen row sizes changed. Requesting layout..."

    invoke-static {v1, v2}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v1, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->requestLayout()V

    .line 199
    :cond_32
    sget-object v1, LIZ;->c:LIZ;

    if-ne v0, v1, :cond_56

    invoke-virtual {p1}, LIY;->a()LJo;

    move-result-object v0

    invoke-virtual {v0}, LJo;->a()I

    move-result v0

    iget-object v1, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LIB;

    move-result-object v1

    invoke-virtual {v1}, LIB;->b()I

    move-result v1

    if-ge v0, v1, :cond_56

    .line 202
    const-string v0, "TrixGridView"

    const-string v1, "Frozen row sizes changed. Requesting layout..."

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    iget-object v0, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->requestLayout()V

    .line 205
    :cond_56
    return-void
.end method

.method public a(LJr;)V
    .registers 9
    .parameter

    .prologue
    .line 209
    iget-object v0, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LJb;

    move-result-object v0

    invoke-virtual {v0}, LJb;->b()LJI;

    move-result-object v0

    .line 210
    iget-object v1, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LJb;

    move-result-object v1

    invoke-virtual {v1}, LJb;->a()D

    move-result-wide v1

    .line 211
    iget-object v3, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-virtual {v0}, LJI;->a()D

    move-result-wide v4

    mul-double/2addr v4, v1

    double-to-int v4, v4

    invoke-virtual {v0}, LJI;->b()D

    move-result-wide v5

    mul-double v0, v5, v1

    double-to-int v0, v0

    invoke-virtual {v3, v4, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->scrollTo(II)V

    .line 212
    iget-object v0, p0, LIo;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    .line 213
    return-void
.end method
