.class public final enum LIx;
.super Ljava/lang/Enum;
.source "TrixSheetEdgeView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LIx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LIx;

.field private static final synthetic a:[LIx;

.field public static final enum b:LIx;

.field public static final enum c:LIx;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, LIx;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, LIx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIx;->a:LIx;

    new-instance v0, LIx;

    const-string v1, "CONNECTOR"

    invoke-direct {v0, v1, v3}, LIx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIx;->b:LIx;

    new-instance v0, LIx;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, LIx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LIx;->c:LIx;

    .line 24
    const/4 v0, 0x3

    new-array v0, v0, [LIx;

    sget-object v1, LIx;->a:LIx;

    aput-object v1, v0, v2

    sget-object v1, LIx;->b:LIx;

    aput-object v1, v0, v3

    sget-object v1, LIx;->c:LIx;

    aput-object v1, v0, v4

    sput-object v0, LIx;->a:[LIx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LIx;
    .registers 2
    .parameter

    .prologue
    .line 24
    const-class v0, LIx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LIx;

    return-object v0
.end method

.method public static values()[LIx;
    .registers 1

    .prologue
    .line 24
    sget-object v0, LIx;->a:[LIx;

    invoke-virtual {v0}, [LIx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LIx;

    return-object v0
.end method
