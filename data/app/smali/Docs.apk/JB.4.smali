.class public LJB;
.super Ljava/lang/Object;
.source "SpreadsheetUtil.java"


# instance fields
.field private a:J

.field protected a:Z


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    .line 55
    invoke-static {}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->new_SpreadsheetUtil()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, LJB;-><init>(JZ)V

    .line 56
    return-void
.end method

.method public constructor <init>(JZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, LJB;->a:Z

    .line 17
    iput-wide p1, p0, LJB;->a:J

    .line 18
    return-void
.end method

.method public static a(LJb;LJJ;)I
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-static {p0}, LJb;->a(LJb;)J

    move-result-wide v0

    invoke-static {p1}, LJJ;->a(LJJ;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->SpreadsheetUtil_GetCellSlotId__SWIG_0(JLJb;JLJJ;)I

    move-result v0

    return v0
.end method

.method public static a(LJk;I)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-static {p0}, LJk;->a(LJk;)J

    move-result-wide v0

    invoke-static {v0, v1, p0, p1}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->SpreadsheetUtil_GetCellSlotId__SWIG_1(JLJk;I)I

    move-result v0

    return v0
.end method

.method public static a(LJb;LJq;LJK;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-static {p0}, LJb;->a(LJb;)J

    move-result-wide v0

    invoke-static {p1}, LJq;->a(LJq;)J

    move-result-wide v3

    invoke-static {p2}, LJK;->a(LJK;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->SpreadsheetUtil_EnumerateGridCellsForRect(JLJb;JLJq;JLJK;)V

    .line 48
    return-void
.end method

.method public static a(LJk;LJo;LJK;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-static {p0}, LJk;->a(LJk;)J

    move-result-wide v0

    invoke-static {p1}, LJo;->a(LJo;)J

    move-result-wide v3

    invoke-static {p2}, LJK;->a(LJK;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->SpreadsheetUtil_EnumerateListItemsForRange(JLJk;JLJo;JLJK;)V

    .line 52
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_3
    iget-wide v0, p0, LJB;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_19

    .line 30
    iget-boolean v0, p0, LJB;->a:Z

    if-eqz v0, :cond_15

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, LJB;->a:Z

    .line 32
    iget-wide v0, p0, LJB;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->delete_SpreadsheetUtil(J)V

    .line 34
    :cond_15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LJB;->a:J
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1b

    .line 36
    :cond_19
    monitor-exit p0

    return-void

    .line 29
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .registers 1

    .prologue
    .line 25
    invoke-virtual {p0}, LJB;->a()V

    .line 26
    return-void
.end method
