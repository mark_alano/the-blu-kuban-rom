.class public LJY;
.super Ljava/lang/Object;
.source "JSVMTimer.java"


# instance fields
.field private final a:LKb;

.field private final a:LKl;

.field private final a:Ljava/util/Timer;

.field private a:Ljava/util/TimerTask;

.field private a:Z


# direct methods
.method public constructor <init>(LKb;LKl;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LJY;->a:Ljava/util/Timer;

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, LJY;->a:Ljava/util/TimerTask;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, LJY;->a:Z

    .line 50
    iput-object p1, p0, LJY;->a:LKb;

    .line 51
    iput-object p2, p0, LJY;->a:LKl;

    .line 52
    return-void
.end method

.method static synthetic a(LJY;)LKl;
    .registers 2
    .parameter

    .prologue
    .line 16
    iget-object v0, p0, LJY;->a:LKl;

    return-object v0
.end method

.method static synthetic a(LJY;)V
    .registers 1
    .parameter

    .prologue
    .line 16
    invoke-direct {p0}, LJY;->b()V

    return-void
.end method

.method private b()V
    .registers 5

    .prologue
    .line 115
    monitor-enter p0

    .line 118
    :try_start_1
    iget-boolean v0, p0, LJY;->a:Z

    if-eqz v0, :cond_7

    .line 119
    monitor-exit p0

    .line 129
    :goto_6
    return-void

    .line 121
    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, LJY;->a:Ljava/util/TimerTask;

    .line 122
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_1b

    .line 123
    iget-object v0, p0, LJY;->a:LKb;

    invoke-virtual {p0}, LJY;->a()D

    move-result-wide v1

    invoke-interface {v0, v1, v2}, LKb;->invokeTimer(D)I

    move-result v0

    .line 124
    if-ltz v0, :cond_1e

    .line 125
    invoke-virtual {p0, v0}, LJY;->a(I)V

    goto :goto_6

    .line 122
    :catchall_1b
    move-exception v0

    :try_start_1c
    monitor-exit p0
    :try_end_1d
    .catchall {:try_start_1c .. :try_end_1d} :catchall_1b

    throw v0

    .line 127
    :cond_1e
    const-string v1, "JSVMTimer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stopped timer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6
.end method


# virtual methods
.method public a()D
    .registers 3

    .prologue
    .line 73
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    long-to-double v0, v0

    return-wide v0
.end method

.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 60
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LJY;->a:Ljava/util/TimerTask;

    if-eqz v0, :cond_d

    .line 61
    iget-object v0, p0, LJY;->a:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, LJY;->a:Ljava/util/TimerTask;

    .line 65
    :cond_d
    iget-object v0, p0, LJY;->a:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, LJY;->a:Z
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    .line 67
    monitor-exit p0

    return-void

    .line 60
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .registers 6
    .parameter

    .prologue
    .line 84
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, LJY;->a:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_3b

    if-eqz v0, :cond_7

    .line 112
    :goto_5
    monitor-exit p0

    return-void

    .line 88
    :cond_7
    :try_start_7
    iget-object v0, p0, LJY;->a:Ljava/util/TimerTask;

    if-eqz v0, :cond_13

    .line 89
    iget-object v0, p0, LJY;->a:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, LJY;->a:Ljava/util/TimerTask;

    .line 93
    :cond_13
    new-instance v0, LJZ;

    invoke-direct {v0, p0}, LJZ;-><init>(LJY;)V

    iput-object v0, p0, LJY;->a:Ljava/util/TimerTask;

    .line 110
    const-string v0, "JSVMTimer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Starting timer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v0, p0, LJY;->a:Ljava/util/Timer;

    iget-object v1, p0, LJY;->a:Ljava/util/TimerTask;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_3a
    .catchall {:try_start_7 .. :try_end_3a} :catchall_3b

    goto :goto_5

    .line 84
    :catchall_3b
    move-exception v0

    monitor-exit p0

    throw v0
.end method
