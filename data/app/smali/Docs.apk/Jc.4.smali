.class public final LJc;
.super Ljava/lang/Object;
.source "HorizontalAlignment.java"


# static fields
.field private static a:I

.field public static final a:LJc;

.field private static a:[LJc;

.field public static final b:LJc;

.field public static final c:LJc;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 12
    new-instance v0, LJc;

    const-string v1, "kHorizontalAlignmentLeft"

    invoke-direct {v0, v1}, LJc;-><init>(Ljava/lang/String;)V

    sput-object v0, LJc;->a:LJc;

    .line 13
    new-instance v0, LJc;

    const-string v1, "kHorizontalAlignmentRight"

    invoke-direct {v0, v1}, LJc;-><init>(Ljava/lang/String;)V

    sput-object v0, LJc;->b:LJc;

    .line 14
    new-instance v0, LJc;

    const-string v1, "kHorizontalAlignmentCenter"

    invoke-direct {v0, v1}, LJc;-><init>(Ljava/lang/String;)V

    sput-object v0, LJc;->c:LJc;

    .line 50
    const/4 v0, 0x3

    new-array v0, v0, [LJc;

    sget-object v1, LJc;->a:LJc;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, LJc;->b:LJc;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LJc;->c:LJc;

    aput-object v2, v0, v1

    sput-object v0, LJc;->a:[LJc;

    .line 51
    sput v3, LJc;->a:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, LJc;->a:Ljava/lang/String;

    .line 35
    sget v0, LJc;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LJc;->a:I

    iput v0, p0, LJc;->b:I

    .line 36
    return-void
.end method

.method public static a(I)LJc;
    .registers 4
    .parameter

    .prologue
    .line 25
    sget-object v0, LJc;->a:[LJc;

    array-length v0, v0

    if-ge p0, v0, :cond_14

    if-ltz p0, :cond_14

    sget-object v0, LJc;->a:[LJc;

    aget-object v0, v0, p0

    iget v0, v0, LJc;->b:I

    if-ne v0, p0, :cond_14

    .line 26
    sget-object v0, LJc;->a:[LJc;

    aget-object v0, v0, p0

    .line 29
    :goto_13
    return-object v0

    .line 27
    :cond_14
    const/4 v0, 0x0

    :goto_15
    sget-object v1, LJc;->a:[LJc;

    array-length v1, v1

    if-ge v0, v1, :cond_2a

    .line 28
    sget-object v1, LJc;->a:[LJc;

    aget-object v1, v1, v0

    iget v1, v1, LJc;->b:I

    if-ne v1, p0, :cond_27

    .line 29
    sget-object v1, LJc;->a:[LJc;

    aget-object v0, v1, v0

    goto :goto_13

    .line 27
    :cond_27
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 30
    :cond_2a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, LJc;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 21
    iget-object v0, p0, LJc;->a:Ljava/lang/String;

    return-object v0
.end method
