.class public final LJi;
.super Ljava/lang/Object;
.source "ListViewModelChangedEvent.java"


# static fields
.field private static a:I

.field public static final a:LJi;

.field private static a:[LJi;

.field public static final b:LJi;

.field public static final c:LJi;

.field public static final d:LJi;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 55
    new-instance v0, LJi;

    const-string v1, "kItemContentsChanged"

    invoke-direct {v0, v1}, LJi;-><init>(Ljava/lang/String;)V

    sput-object v0, LJi;->a:LJi;

    .line 56
    new-instance v0, LJi;

    const-string v1, "kItemSizesChanged"

    invoke-direct {v0, v1}, LJi;-><init>(Ljava/lang/String;)V

    sput-object v0, LJi;->b:LJi;

    .line 57
    new-instance v0, LJi;

    const-string v1, "kSelectionChanged"

    invoke-direct {v0, v1}, LJi;-><init>(Ljava/lang/String;)V

    sput-object v0, LJi;->c:LJi;

    .line 58
    new-instance v0, LJi;

    const-string v1, "kOptionsChanged"

    invoke-direct {v0, v1}, LJi;-><init>(Ljava/lang/String;)V

    sput-object v0, LJi;->d:LJi;

    .line 94
    const/4 v0, 0x4

    new-array v0, v0, [LJi;

    sget-object v1, LJi;->a:LJi;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, LJi;->b:LJi;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LJi;->c:LJi;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LJi;->d:LJi;

    aput-object v2, v0, v1

    sput-object v0, LJi;->a:[LJi;

    .line 95
    sput v3, LJi;->a:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, LJi;->a:Ljava/lang/String;

    .line 79
    sget v0, LJi;->a:I

    add-int/lit8 v1, v0, 0x1

    sput v1, LJi;->a:I

    iput v0, p0, LJi;->b:I

    .line 80
    return-void
.end method

.method public static a(I)LJi;
    .registers 4
    .parameter

    .prologue
    .line 69
    sget-object v0, LJi;->a:[LJi;

    array-length v0, v0

    if-ge p0, v0, :cond_14

    if-ltz p0, :cond_14

    sget-object v0, LJi;->a:[LJi;

    aget-object v0, v0, p0

    iget v0, v0, LJi;->b:I

    if-ne v0, p0, :cond_14

    .line 70
    sget-object v0, LJi;->a:[LJi;

    aget-object v0, v0, p0

    .line 73
    :goto_13
    return-object v0

    .line 71
    :cond_14
    const/4 v0, 0x0

    :goto_15
    sget-object v1, LJi;->a:[LJi;

    array-length v1, v1

    if-ge v0, v1, :cond_2a

    .line 72
    sget-object v1, LJi;->a:[LJi;

    aget-object v1, v1, v0

    iget v1, v1, LJi;->b:I

    if-ne v1, p0, :cond_27

    .line 73
    sget-object v1, LJi;->a:[LJi;

    aget-object v0, v1, v0

    goto :goto_13

    .line 71
    :cond_27
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 74
    :cond_2a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No enum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, LJi;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with value "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 65
    iget-object v0, p0, LJi;->a:Ljava/lang/String;

    return-object v0
.end method
