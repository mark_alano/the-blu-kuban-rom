.class public LJv;
.super Ljava/lang/Object;
.source "Spreadsheet.java"


# instance fields
.field private a:J

.field protected a:Z


# direct methods
.method public constructor <init>(JZ)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, LJv;->a:Z

    .line 17
    iput-wide p1, p0, LJv;->a:J

    .line 18
    return-void
.end method

.method public static a(LJv;)J
    .registers 3
    .parameter

    .prologue
    .line 21
    if-nez p0, :cond_5

    const-wide/16 v0, 0x0

    :goto_4
    return-wide v0

    :cond_5
    iget-wide v0, p0, LJv;->a:J

    goto :goto_4
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 79
    iget-wide v0, p0, LJv;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_GetFrozenRowCount(JLJv;)I

    move-result v0

    return v0
.end method

.method public a(LJJ;)LIW;
    .registers 9
    .parameter

    .prologue
    .line 91
    new-instance v6, LIW;

    iget-wide v0, p0, LJv;->a:J

    invoke-static {p1}, LJJ;->a(LJJ;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_GetCellFormat(JLJv;JLJJ;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, LIW;-><init>(JZ)V

    return-object v6
.end method

.method public a(LJw;LIR;)LJb;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 59
    iget-wide v0, p0, LJv;->a:J

    invoke-virtual {p1}, LJw;->a()I

    move-result v3

    invoke-static {p2}, LIR;->a(LIR;)J

    move-result-wide v4

    move-object v2, p0

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_InitializeGridViewModel(JLJv;IJLIR;)J

    move-result-wide v1

    .line 60
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_18

    const/4 v0, 0x0

    :goto_17
    return-object v0

    :cond_18
    new-instance v0, LJb;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LJb;-><init>(JZ)V

    goto :goto_17
.end method

.method public a(LJw;LIR;)LJk;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 69
    iget-wide v0, p0, LJv;->a:J

    invoke-virtual {p1}, LJw;->a()I

    move-result v3

    invoke-static {p2}, LIR;->a(LIR;)J

    move-result-wide v4

    move-object v2, p0

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_InitializeListViewModel(JLJv;IJLIR;)J

    move-result-wide v1

    .line 70
    const-wide/16 v3, 0x0

    cmp-long v0, v1, v3

    if-nez v0, :cond_18

    const/4 v0, 0x0

    :goto_17
    return-object v0

    :cond_18
    new-instance v0, LJk;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, LJk;-><init>(JZ)V

    goto :goto_17
.end method

.method public a(LJJ;)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    .line 87
    iget-wide v0, p0, LJv;->a:J

    invoke-static {p1}, LJJ;->a(LJJ;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_GetCellEditableValue(JLJv;JLJJ;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    monitor-enter p0

    :try_start_3
    iget-wide v0, p0, LJv;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_19

    .line 30
    iget-boolean v0, p0, LJv;->a:Z

    if-eqz v0, :cond_15

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, LJv;->a:Z

    .line 32
    iget-wide v0, p0, LJv;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->delete_Spreadsheet(J)V

    .line 34
    :cond_15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LJv;->a:J
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_1b

    .line 36
    :cond_19
    monitor-exit p0

    return-void

    .line 29
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(LJl;)V
    .registers 8
    .parameter

    .prologue
    .line 95
    iget-wide v0, p0, LJv;->a:J

    invoke-static {p1}, LJl;->a(LJl;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_SetSelection(JLJv;JLJl;)V

    .line 96
    return-void
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 47
    iget-wide v0, p0, LJv;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_IsDataAvailable(JLJv;)Z

    move-result v0

    return v0
.end method

.method public b()I
    .registers 3

    .prologue
    .line 83
    iget-wide v0, p0, LJv;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_GetFrozenColumnCount(JLJv;)I

    move-result v0

    return v0
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 51
    iget-wide v0, p0, LJv;->a:J

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_IsDataLoadComplete(JLJv;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .registers 1

    .prologue
    .line 25
    invoke-virtual {p0}, LJv;->a()V

    .line 26
    return-void
.end method
