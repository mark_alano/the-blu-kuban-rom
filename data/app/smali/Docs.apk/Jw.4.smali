.class public final LJw;
.super Ljava/lang/Object;
.source "Spreadsheet.java"


# static fields
.field private static a:I

.field public static final a:LJw;

.field private static a:[LJw;

.field public static final b:LJw;

.field public static final c:LJw;

.field public static final d:LJw;

.field public static final e:LJw;

.field public static final f:LJw;

.field public static final g:LJw;

.field public static final h:LJw;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 99
    new-instance v0, LJw;

    const-string v1, "kFrozenHeaderRow"

    invoke-static {}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_kFrozenHeaderRow_get()I

    move-result v2

    invoke-direct {v0, v1, v2}, LJw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJw;->a:LJw;

    .line 100
    new-instance v0, LJw;

    const-string v1, "kScrollableHeaderRow"

    invoke-static {}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_kScrollableHeaderRow_get()I

    move-result v2

    invoke-direct {v0, v1, v2}, LJw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJw;->b:LJw;

    .line 101
    new-instance v0, LJw;

    const-string v1, "kFrozenHeaderColumn"

    invoke-static {}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_kFrozenHeaderColumn_get()I

    move-result v2

    invoke-direct {v0, v1, v2}, LJw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJw;->c:LJw;

    .line 102
    new-instance v0, LJw;

    const-string v1, "kFrozenRowColumnGrid"

    invoke-static {}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_kFrozenRowColumnGrid_get()I

    move-result v2

    invoke-direct {v0, v1, v2}, LJw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJw;->d:LJw;

    .line 103
    new-instance v0, LJw;

    const-string v1, "kFrozenRowScrollableColumnGrid"

    invoke-static {}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_kFrozenRowScrollableColumnGrid_get()I

    move-result v2

    invoke-direct {v0, v1, v2}, LJw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJw;->e:LJw;

    .line 104
    new-instance v0, LJw;

    const-string v1, "kScrollableHeaderColumn"

    invoke-static {}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_kScrollableHeaderColumn_get()I

    move-result v2

    invoke-direct {v0, v1, v2}, LJw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJw;->f:LJw;

    .line 105
    new-instance v0, LJw;

    const-string v1, "kScrollableRowFrozenColumnGrid"

    invoke-static {}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_kScrollableRowFrozenColumnGrid_get()I

    move-result v2

    invoke-direct {v0, v1, v2}, LJw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJw;->g:LJw;

    .line 106
    new-instance v0, LJw;

    const-string v1, "kScrollableRowColumnGrid"

    invoke-static {}, Lcom/google/android/apps/docs/editors/trixnative/mobilenativeJNI;->Spreadsheet_kScrollableRowColumnGrid_get()I

    move-result v2

    invoke-direct {v0, v1, v2}, LJw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LJw;->h:LJw;

    .line 142
    const/16 v0, 0x8

    new-array v0, v0, [LJw;

    sget-object v1, LJw;->a:LJw;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, LJw;->b:LJw;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LJw;->c:LJw;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LJw;->d:LJw;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LJw;->e:LJw;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LJw;->f:LJw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LJw;->g:LJw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LJw;->h:LJw;

    aput-object v2, v0, v1

    sput-object v0, LJw;->a:[LJw;

    .line 143
    sput v3, LJw;->a:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p1, p0, LJw;->a:Ljava/lang/String;

    .line 132
    iput p2, p0, LJw;->b:I

    .line 133
    add-int/lit8 v0, p2, 0x1

    sput v0, LJw;->a:I

    .line 134
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 109
    iget v0, p0, LJw;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, LJw;->a:Ljava/lang/String;

    return-object v0
.end method
