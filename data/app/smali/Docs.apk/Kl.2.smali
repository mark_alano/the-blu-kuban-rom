.class public LKl;
.super Ljava/lang/Object;
.source "SwitchableQueue.java"


# instance fields
.field private a:I

.field private final a:LKo;

.field private a:LKq;

.field private a:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "LKr;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private final b:LKo;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/os/MessageQueue;LKq;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, LKl;->a:Ljava/util/PriorityQueue;

    .line 174
    const/4 v0, 0x0

    iput v0, p0, LKl;->a:I

    .line 190
    new-instance v0, LKm;

    invoke-direct {v0, p0, p2, p1}, LKm;-><init>(LKl;Landroid/os/MessageQueue;Landroid/os/Handler;)V

    iput-object v0, p0, LKl;->a:LKo;

    .line 202
    new-instance v0, LKn;

    invoke-direct {v0, p0, p1}, LKn;-><init>(LKl;Landroid/os/Handler;)V

    iput-object v0, p0, LKl;->b:LKo;

    .line 209
    iput-object p3, p0, LKl;->a:LKq;

    .line 210
    return-void
.end method

.method static synthetic a(LKl;LKo;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1}, LKl;->a(LKo;)V

    return-void
.end method

.method private a(LKo;)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 295
    iget-object v1, p0, LKl;->b:LKo;

    if-ne p1, v1, :cond_6

    const/4 v0, 0x1

    .line 296
    :cond_6
    invoke-direct {p0, v0}, LKl;->a(Z)V

    .line 298
    monitor-enter p0

    .line 301
    const/4 v0, 0x0

    :try_start_b
    invoke-static {p1, v0}, LKo;->a(LKo;Z)V

    .line 302
    invoke-direct {p0}, LKl;->c()V

    .line 303
    monitor-exit p0

    .line 304
    return-void

    .line 303
    :catchall_13
    move-exception v0

    monitor-exit p0
    :try_end_15
    .catchall {:try_start_b .. :try_end_15} :catchall_13

    throw v0
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    .line 267
    .line 268
    monitor-enter p0

    .line 269
    :try_start_1
    iget-boolean v0, p0, LKl;->a:Z

    if-nez v0, :cond_d

    iget-object v0, p0, LKl;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 270
    :cond_d
    monitor-exit p0

    .line 292
    :goto_e
    return-void

    .line 273
    :cond_f
    iget-object v0, p0, LKl;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKr;

    .line 274
    if-eqz p1, :cond_24

    invoke-direct {p0, v0}, LKl;->a(LKr;)Z

    move-result v1

    if-nez v1, :cond_24

    .line 276
    monitor-exit p0

    goto :goto_e

    .line 280
    :catchall_21
    move-exception v0

    monitor-exit p0
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_21

    throw v0

    .line 279
    :cond_24
    :try_start_24
    iget-object v1, p0, LKl;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v1}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    .line 280
    monitor-exit p0
    :try_end_2a
    .catchall {:try_start_24 .. :try_end_2a} :catchall_21

    .line 291
    invoke-static {v0}, LKr;->a(LKr;)LKp;

    move-result-object v0

    invoke-interface {v0}, LKp;->a()V

    goto :goto_e
.end method

.method private a(LKr;)Z
    .registers 4
    .parameter

    .prologue
    .line 213
    invoke-static {p1}, LKr;->a(LKr;)LKs;

    move-result-object v0

    invoke-virtual {v0}, LKs;->a()I

    move-result v0

    iget-object v1, p0, LKl;->a:LKq;

    invoke-virtual {v1}, LKq;->a()I

    move-result v1

    if-lt v0, v1, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private declared-synchronized c()V
    .registers 3

    .prologue
    .line 220
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LKl;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    iget-boolean v0, p0, LKl;->a:Z
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_27

    if-eqz v0, :cond_f

    .line 228
    :cond_d
    :goto_d
    monitor-exit p0

    return-void

    .line 224
    :cond_f
    :try_start_f
    iget-object v0, p0, LKl;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKr;

    .line 225
    invoke-direct {p0, v0}, LKl;->a(LKr;)Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, LKl;->b:LKo;

    .line 226
    :goto_1f
    invoke-static {v0}, LKo;->a(LKo;)V

    .line 227
    const/4 v1, 0x1

    invoke-static {v0, v1}, LKo;->a(LKo;Z)V
    :try_end_26
    .catchall {:try_start_f .. :try_end_26} :catchall_27

    goto :goto_d

    .line 220
    :catchall_27
    move-exception v0

    monitor-exit p0

    throw v0

    .line 225
    :cond_2a
    :try_start_2a
    iget-object v0, p0, LKl;->a:LKo;
    :try_end_2c
    .catchall {:try_start_2a .. :try_end_2c} :catchall_27

    goto :goto_1f
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 234
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, LKl;->a:Z

    .line 235
    invoke-direct {p0}, LKl;->c()V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_9

    .line 236
    monitor-exit p0

    return-void

    .line 234
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LKp;LKs;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 258
    monitor-enter p0

    :try_start_1
    new-instance v0, LKr;

    iget v1, p0, LKl;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, LKl;->a:I

    const/4 v2, 0x0

    invoke-direct {v0, p1, p2, v1, v2}, LKr;-><init>(LKp;LKs;ILKm;)V

    .line 259
    iget-object v1, p0, LKl;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 260
    invoke-direct {p0}, LKl;->c()V
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    .line 261
    monitor-exit p0

    return-void

    .line 258
    :catchall_17
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(LKq;)V
    .registers 3
    .parameter

    .prologue
    .line 311
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, LKl;->a:LKq;

    .line 312
    invoke-direct {p0}, LKl;->c()V
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_8

    .line 313
    monitor-exit p0

    return-void

    .line 311
    :catchall_8
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .registers 5

    .prologue
    .line 242
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LKl;->a:Z

    .line 243
    new-instance v1, Ljava/util/PriorityQueue;

    invoke-direct {v1}, Ljava/util/PriorityQueue;-><init>()V

    .line 244
    iget-object v0, p0, LKl;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKr;

    .line 245
    invoke-static {v0}, LKr;->a(LKr;)LKp;

    move-result-object v3

    invoke-interface {v3}, LKp;->a()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 246
    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z
    :try_end_28
    .catchall {:try_start_2 .. :try_end_28} :catchall_29

    goto :goto_f

    .line 242
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0

    .line 249
    :cond_2c
    :try_start_2c
    iput-object v1, p0, LKl;->a:Ljava/util/PriorityQueue;
    :try_end_2e
    .catchall {:try_start_2c .. :try_end_2e} :catchall_29

    .line 250
    monitor-exit p0

    return-void
.end method
