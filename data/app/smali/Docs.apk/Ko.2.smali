.class abstract LKo;
.super Ljava/lang/Object;
.source "SwitchableQueue.java"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;
.implements Ljava/lang/Runnable;


# instance fields
.field protected a:Z

.field final synthetic b:LKl;


# direct methods
.method private constructor <init>(LKl;)V
    .registers 3
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, LKo;->b:LKl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, LKo;->a:Z

    return-void
.end method

.method synthetic constructor <init>(LKl;LKm;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-direct {p0, p1}, LKo;-><init>(LKl;)V

    return-void
.end method

.method static synthetic a(LKo;)V
    .registers 1
    .parameter

    .prologue
    .line 109
    invoke-direct {p0}, LKo;->b()V

    return-void
.end method

.method static synthetic a(LKo;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-direct {p0, p1}, LKo;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 128
    iput-boolean p1, p0, LKo;->a:Z

    .line 129
    return-void
.end method

.method private b()V
    .registers 2

    .prologue
    .line 132
    iget-boolean v0, p0, LKo;->a:Z

    if-nez v0, :cond_7

    .line 133
    invoke-virtual {p0}, LKo;->a()V

    .line 135
    :cond_7
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public queueIdle()Z
    .registers 2

    .prologue
    .line 123
    iget-object v0, p0, LKo;->b:LKl;

    invoke-static {v0, p0}, LKl;->a(LKl;LKo;)V

    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public run()V
    .registers 2

    .prologue
    .line 118
    iget-object v0, p0, LKo;->b:LKl;

    invoke-static {v0, p0}, LKl;->a(LKl;LKo;)V

    .line 119
    return-void
.end method
