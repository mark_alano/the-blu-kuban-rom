.class public final enum LKq;
.super Ljava/lang/Enum;
.source "SwitchableQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LKq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LKq;

.field private static final synthetic a:[LKq;

.field public static final enum b:LKq;


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, LKq;

    const-string v1, "PRIORITY_THRESHOLD_HIGH"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v3, v2}, LKq;-><init>(Ljava/lang/String;II)V

    sput-object v0, LKq;->a:LKq;

    .line 38
    new-instance v0, LKq;

    const-string v1, "PRIORITY_THRESHOLD_LOW"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v4, v2}, LKq;-><init>(Ljava/lang/String;II)V

    sput-object v0, LKq;->b:LKq;

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [LKq;

    sget-object v1, LKq;->a:LKq;

    aput-object v1, v0, v3

    sget-object v1, LKq;->b:LKq;

    aput-object v1, v0, v4

    sput-object v0, LKq;->a:[LKq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, LKq;->a:I

    .line 42
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LKq;
    .registers 2
    .parameter

    .prologue
    .line 27
    const-class v0, LKq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LKq;

    return-object v0
.end method

.method public static values()[LKq;
    .registers 1

    .prologue
    .line 27
    sget-object v0, LKq;->a:[LKq;

    invoke-virtual {v0}, [LKq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LKq;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 45
    iget v0, p0, LKq;->a:I

    return v0
.end method
