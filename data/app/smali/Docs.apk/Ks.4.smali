.class public final enum LKs;
.super Ljava/lang/Enum;
.source "SwitchableQueue.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LKs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LKs;

.field private static final synthetic a:[LKs;

.field public static final enum b:LKs;

.field public static final enum c:LKs;


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 58
    new-instance v0, LKs;

    const-string v1, "PRIORITY_JSVM_TIMER"

    const/16 v2, 0xbe

    invoke-direct {v0, v1, v3, v2}, LKs;-><init>(Ljava/lang/String;II)V

    sput-object v0, LKs;->a:LKs;

    .line 63
    new-instance v0, LKs;

    const-string v1, "PRIORITY_HTTP_DATA_LOADER"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v4, v2}, LKs;-><init>(Ljava/lang/String;II)V

    sput-object v0, LKs;->b:LKs;

    .line 68
    new-instance v0, LKs;

    const-string v1, "PRIORITY_JNI_IDLE_HANDLER"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v5, v2}, LKs;-><init>(Ljava/lang/String;II)V

    sput-object v0, LKs;->c:LKs;

    .line 54
    const/4 v0, 0x3

    new-array v0, v0, [LKs;

    sget-object v1, LKs;->a:LKs;

    aput-object v1, v0, v3

    sget-object v1, LKs;->b:LKs;

    aput-object v1, v0, v4

    sget-object v1, LKs;->c:LKs;

    aput-object v1, v0, v5

    sput-object v0, LKs;->a:[LKs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 71
    iput p3, p0, LKs;->a:I

    .line 72
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LKs;
    .registers 2
    .parameter

    .prologue
    .line 54
    const-class v0, LKs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LKs;

    return-object v0
.end method

.method public static values()[LKs;
    .registers 1

    .prologue
    .line 54
    sget-object v0, LKs;->a:[LKs;

    invoke-virtual {v0}, [LKs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LKs;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 75
    iget v0, p0, LKs;->a:I

    return v0
.end method
