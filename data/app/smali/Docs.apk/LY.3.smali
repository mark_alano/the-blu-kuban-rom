.class public LLY;
.super Ljava/lang/Object;
.source "NavigationCollectionGroup.java"


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LmK;

.field private final a:Z

.field private final b:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/util/List;LmK;ZIZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;",
            "LmK;",
            "ZIZ)V"
        }
    .end annotation

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    iput-object p1, p0, LLY;->a:Ljava/lang/String;

    .line 194
    iput-object p2, p0, LLY;->a:Ljava/util/List;

    .line 195
    iput-object p3, p0, LLY;->a:LmK;

    .line 196
    iput-boolean p4, p0, LLY;->a:Z

    .line 197
    iput p5, p0, LLY;->a:I

    .line 198
    iput-boolean p6, p0, LLY;->b:Z

    .line 199
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LmK;ZIZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 83
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, LLY;-><init>(Ljava/lang/String;Ljava/util/List;LmK;ZIZ)V

    .line 84
    return-void
.end method

.method private a(Llf;LkB;)Lnh;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 179
    iget-object v0, p0, LLY;->a:Ljava/util/List;

    if-eqz v0, :cond_19

    .line 180
    iget-object v0, p0, LLY;->a:Ljava/util/List;

    iget-object v1, p0, LLY;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    invoke-interface {v0}, LiQ;->a()Lnh;

    move-result-object v0

    .line 184
    :goto_18
    return-object v0

    .line 182
    :cond_19
    iget-object v0, p0, LLY;->a:LmK;

    invoke-virtual {v0, p2}, LmK;->a(LkB;)Lnh;

    move-result-object v0

    goto :goto_18
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 165
    iget v0, p0, LLY;->a:I

    return v0
.end method

.method public a(Llf;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 94
    invoke-interface {p1, p2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 95
    invoke-direct {p0, p1, v0}, LLY;->a(Llf;LkB;)Lnh;

    move-result-object v1

    .line 96
    const v2, 0x7fffffff

    invoke-interface {p1, v0, v1, v2}, Llf;->a(LkB;Lnh;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;LiG;)LiQ;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, LLY;->a:Ljava/util/List;

    .line 120
    if-eqz v0, :cond_11

    .line 121
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    .line 123
    :goto_10
    return-object v0

    :cond_11
    new-instance v0, LiR;

    invoke-direct {v0}, LiR;-><init>()V

    iget-object v1, p0, LLY;->a:LmK;

    invoke-interface {p2, v1, p1}, LiG;->b(LmK;Ljava/lang/String;)LiE;

    move-result-object v1

    invoke-virtual {v0, v1}, LiR;->a(LiE;)LiR;

    move-result-object v0

    invoke-virtual {v0}, LiR;->a()LiQ;

    move-result-object v0

    goto :goto_10
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 134
    iget-object v0, p0, LLY;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Llf;Ljava/lang/String;)Lnh;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-interface {p1, p2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 108
    invoke-direct {p0, p1, v0}, LLY;->a(Llf;LkB;)Lnh;

    move-result-object v0

    return-object v0
.end method

.method public a(LMe;)V
    .registers 3
    .parameter

    .prologue
    .line 143
    iget-object v0, p0, LLY;->a:Ljava/util/List;

    if-eqz v0, :cond_a

    .line 144
    iget-object v0, p0, LLY;->a:Ljava/util/List;

    invoke-interface {p1, v0}, LMe;->a(Ljava/util/List;)V

    .line 148
    :goto_9
    return-void

    .line 146
    :cond_a
    iget-object v0, p0, LLY;->a:LmK;

    invoke-interface {p1, v0}, LMe;->a(LmK;)V

    goto :goto_9
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 156
    iget-boolean v0, p0, LLY;->a:Z

    return v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 174
    iget-boolean v0, p0, LLY;->b:Z

    return v0
.end method
