.class public LLf;
.super Les;
.source "ClientFlagSynchronizerThread.java"


# instance fields
.field private final a:LLc;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LLc;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, Les;-><init>()V

    .line 26
    iput-object p1, p0, LLf;->a:LLc;

    .line 27
    iput-object p2, p0, LLf;->a:Ljava/lang/String;

    .line 28
    iput-object p3, p0, LLf;->b:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method protected c()V
    .registers 4

    .prologue
    .line 34
    :try_start_0
    iget-object v0, p0, LLf;->a:LLc;

    iget-object v1, p0, LLf;->a:Ljava/lang/String;

    iget-object v2, p0, LLf;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LLc;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catch LLd; {:try_start_0 .. :try_end_9} :catch_a

    .line 38
    :goto_9
    return-void

    .line 35
    :catch_a
    move-exception v0

    .line 36
    const-string v1, "ClientFlagSynchronizerThread"

    const-string v2, "Failed synchronizing client flags."

    invoke-static {v1, v2, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9
.end method
