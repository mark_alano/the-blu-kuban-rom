.class public final LLi;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LLc;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LKZ;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LKU;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LKS;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LLe;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LLh;",
            ">;"
        }
    .end annotation
.end field

.field public g:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LLb;",
            ">;"
        }
    .end annotation
.end field

.field public h:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LKW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 38
    iput-object p1, p0, LLi;->a:LYD;

    .line 39
    const-class v0, LLc;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LLi;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLi;->a:LZb;

    .line 42
    const-class v0, LKZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LLi;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLi;->b:LZb;

    .line 45
    const-class v0, LKU;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LLi;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLi;->c:LZb;

    .line 48
    const-class v0, LKS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, LLi;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLi;->d:LZb;

    .line 51
    const-class v0, LLe;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LLi;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLi;->e:LZb;

    .line 54
    const-class v0, LLh;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LLi;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLi;->f:LZb;

    .line 57
    const-class v0, LLb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LLi;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLi;->g:LZb;

    .line 60
    const-class v0, LKW;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LLi;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LLi;->h:LZb;

    .line 63
    return-void
.end method

.method static synthetic a(LLi;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LLi;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LLi;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LLi;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LLi;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LLi;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LLi;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 80
    const-class v0, LLh;

    new-instance v1, LLj;

    invoke-direct {v1, p0}, LLj;-><init>(LLi;)V

    invoke-virtual {p0, v0, v1}, LLi;->a(Ljava/lang/Class;Laou;)V

    .line 88
    const-class v0, LLc;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLi;->a:LZb;

    invoke-virtual {p0, v0, v1}, LLi;->a(Laop;LZb;)V

    .line 89
    const-class v0, LKZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLi;->b:LZb;

    invoke-virtual {p0, v0, v1}, LLi;->a(Laop;LZb;)V

    .line 90
    const-class v0, LKU;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLi;->c:LZb;

    invoke-virtual {p0, v0, v1}, LLi;->a(Laop;LZb;)V

    .line 91
    const-class v0, LKS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLi;->d:LZb;

    invoke-virtual {p0, v0, v1}, LLi;->a(Laop;LZb;)V

    .line 92
    const-class v0, LLe;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLi;->e:LZb;

    invoke-virtual {p0, v0, v1}, LLi;->a(Laop;LZb;)V

    .line 93
    const-class v0, LLh;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLi;->f:LZb;

    invoke-virtual {p0, v0, v1}, LLi;->a(Laop;LZb;)V

    .line 94
    const-class v0, LLb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLi;->g:LZb;

    invoke-virtual {p0, v0, v1}, LLi;->a(Laop;LZb;)V

    .line 95
    const-class v0, LKW;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LLi;->h:LZb;

    invoke-virtual {p0, v0, v1}, LLi;->a(Laop;LZb;)V

    .line 96
    iget-object v0, p0, LLi;->a:LZb;

    iget-object v1, p0, LLi;->a:LYD;

    iget-object v1, v1, LYD;->a:LLi;

    iget-object v1, v1, LLi;->e:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 98
    iget-object v0, p0, LLi;->b:LZb;

    iget-object v1, p0, LLi;->a:LYD;

    iget-object v1, v1, LYD;->a:LLi;

    iget-object v1, v1, LLi;->g:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 100
    iget-object v0, p0, LLi;->c:LZb;

    iget-object v1, p0, LLi;->a:LYD;

    iget-object v1, v1, LYD;->a:LLi;

    iget-object v1, v1, LLi;->h:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 102
    iget-object v0, p0, LLi;->d:LZb;

    iget-object v1, p0, LLi;->a:LYD;

    iget-object v1, v1, LYD;->a:LLi;

    iget-object v1, v1, LLi;->f:LZb;

    invoke-static {v1}, LLi;->a(Laoz;)Laoz;

    move-result-object v1

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 105
    iget-object v0, p0, LLi;->e:LZb;

    new-instance v1, LLk;

    invoke-direct {v1, p0}, LLk;-><init>(LLi;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 134
    iget-object v0, p0, LLi;->f:LZb;

    new-instance v1, LLl;

    invoke-direct {v1, p0}, LLl;-><init>(LLi;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 145
    iget-object v0, p0, LLi;->g:LZb;

    new-instance v1, LLm;

    invoke-direct {v1, p0}, LLm;-><init>(LLi;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 154
    iget-object v0, p0, LLi;->h:LZb;

    new-instance v1, LLn;

    invoke-direct {v1, p0}, LLn;-><init>(LLi;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 168
    return-void
.end method

.method public a(LLh;)V
    .registers 3
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, LLi;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->c:LZb;

    invoke-static {v0}, LLi;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, LLh;->a:Laoz;

    .line 75
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 172
    return-void
.end method
