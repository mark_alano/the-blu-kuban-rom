.class public LMB;
.super Ljava/lang/Object;
.source "ScaleGestureDetector.java"


# instance fields
.field private a:F

.field private final a:I

.field private final a:LMC;

.field private a:Z

.field private b:F

.field private final b:Z

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F


# direct methods
.method public constructor <init>(Landroid/content/Context;LMC;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_1b

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, LMB;->b:Z

    .line 150
    iput-object p2, p0, LMB;->a:LMC;

    .line 151
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, LMB;->a:I

    .line 152
    return-void

    .line 149
    :cond_1b
    const/4 v0, 0x0

    goto :goto_a
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 288
    iget v0, p0, LMB;->a:F

    return v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 274
    iget-boolean v0, p0, LMB;->a:Z

    return v0
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .registers 14
    .parameter

    .prologue
    .line 167
    iget-boolean v0, p0, LMB;->b:Z

    if-eqz v0, :cond_6

    .line 168
    const/4 v0, 0x0

    .line 267
    :goto_5
    return v0

    .line 171
    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v7

    .line 173
    const/4 v0, 0x1

    if-eq v7, v0, :cond_10

    const/4 v0, 0x3

    if-ne v7, v0, :cond_28

    :cond_10
    const/4 v0, 0x1

    .line 175
    :goto_11
    if-eqz v7, :cond_15

    if-eqz v0, :cond_2a

    .line 179
    :cond_15
    iget-boolean v1, p0, LMB;->a:Z

    if-eqz v1, :cond_24

    .line 180
    iget-object v1, p0, LMB;->a:LMC;

    invoke-interface {v1, p0}, LMC;->a(LMB;)V

    .line 181
    const/4 v1, 0x0

    iput-boolean v1, p0, LMB;->a:Z

    .line 182
    const/4 v1, 0x0

    iput v1, p0, LMB;->e:F

    .line 185
    :cond_24
    if-eqz v0, :cond_2a

    .line 186
    const/4 v0, 0x1

    goto :goto_5

    .line 173
    :cond_28
    const/4 v0, 0x0

    goto :goto_11

    .line 190
    :cond_2a
    const/4 v0, 0x6

    if-eq v7, v0, :cond_30

    const/4 v0, 0x5

    if-ne v7, v0, :cond_4f

    :cond_30
    const/4 v0, 0x1

    move v6, v0

    .line 193
    :goto_32
    const/4 v0, 0x6

    if-ne v7, v0, :cond_52

    const/4 v0, 0x1

    move v5, v0

    .line 194
    :goto_37
    if-eqz v5, :cond_55

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 197
    :goto_3d
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 198
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    .line 199
    const/4 v1, 0x0

    move v11, v1

    move v1, v2

    move v2, v3

    move v3, v11

    :goto_48
    if-ge v3, v4, :cond_62

    .line 200
    if-ne v0, v3, :cond_57

    .line 199
    :goto_4c
    add-int/lit8 v3, v3, 0x1

    goto :goto_48

    .line 190
    :cond_4f
    const/4 v0, 0x0

    move v6, v0

    goto :goto_32

    .line 193
    :cond_52
    const/4 v0, 0x0

    move v5, v0

    goto :goto_37

    .line 194
    :cond_55
    const/4 v0, -0x1

    goto :goto_3d

    .line 201
    :cond_57
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v8

    add-float/2addr v2, v8

    .line 202
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    add-float/2addr v1, v8

    goto :goto_4c

    .line 204
    :cond_62
    if-eqz v5, :cond_7a

    add-int/lit8 v3, v4, -0x1

    .line 205
    :goto_66
    int-to-float v5, v3

    div-float v8, v2, v5

    .line 206
    int-to-float v2, v3

    div-float v9, v1, v2

    .line 209
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 210
    const/4 v1, 0x0

    move v11, v1

    move v1, v2

    move v2, v5

    move v5, v11

    :goto_73
    if-ge v5, v4, :cond_91

    .line 211
    if-ne v0, v5, :cond_7c

    .line 210
    :goto_77
    add-int/lit8 v5, v5, 0x1

    goto :goto_73

    :cond_7a
    move v3, v4

    .line 204
    goto :goto_66

    .line 212
    :cond_7c
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    sub-float/2addr v10, v8

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    add-float/2addr v2, v10

    .line 213
    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    sub-float/2addr v10, v9

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    add-float/2addr v1, v10

    goto :goto_77

    .line 215
    :cond_91
    int-to-float v0, v3

    div-float v0, v2, v0

    .line 216
    int-to-float v2, v3

    div-float/2addr v1, v2

    .line 221
    const/high16 v2, 0x4000

    mul-float/2addr v0, v2

    .line 222
    const/high16 v2, 0x4000

    mul-float/2addr v1, v2

    .line 223
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    .line 228
    iget-boolean v3, p0, LMB;->a:Z

    .line 229
    iput v8, p0, LMB;->a:F

    .line 230
    iput v9, p0, LMB;->b:F

    .line 231
    iget-boolean v4, p0, LMB;->a:Z

    if-eqz v4, :cond_c0

    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_b6

    if-eqz v6, :cond_c0

    .line 232
    :cond_b6
    iget-object v4, p0, LMB;->a:LMC;

    invoke-interface {v4, p0}, LMC;->a(LMB;)V

    .line 233
    const/4 v4, 0x0

    iput-boolean v4, p0, LMB;->a:Z

    .line 234
    iput v2, p0, LMB;->e:F

    .line 236
    :cond_c0
    if-eqz v6, :cond_d0

    .line 237
    iput v0, p0, LMB;->f:F

    iput v0, p0, LMB;->h:F

    .line 238
    iput v1, p0, LMB;->g:F

    iput v1, p0, LMB;->i:F

    .line 239
    iput v2, p0, LMB;->c:F

    iput v2, p0, LMB;->d:F

    iput v2, p0, LMB;->e:F

    .line 241
    :cond_d0
    iget-boolean v4, p0, LMB;->a:Z

    if-nez v4, :cond_fe

    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_fe

    if-nez v3, :cond_ea

    iget v3, p0, LMB;->e:F

    sub-float v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, LMB;->a:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_fe

    .line 243
    :cond_ea
    iput v0, p0, LMB;->f:F

    iput v0, p0, LMB;->h:F

    .line 244
    iput v1, p0, LMB;->g:F

    iput v1, p0, LMB;->i:F

    .line 245
    iput v2, p0, LMB;->c:F

    iput v2, p0, LMB;->d:F

    .line 246
    iget-object v3, p0, LMB;->a:LMC;

    invoke-interface {v3, p0}, LMC;->a(LMB;)Z

    move-result v3

    iput-boolean v3, p0, LMB;->a:Z

    .line 250
    :cond_fe
    const/4 v3, 0x2

    if-ne v7, v3, :cond_120

    .line 251
    iput v0, p0, LMB;->f:F

    .line 252
    iput v1, p0, LMB;->g:F

    .line 253
    iput v2, p0, LMB;->c:F

    .line 255
    const/4 v0, 0x1

    .line 256
    iget-boolean v1, p0, LMB;->a:Z

    if-eqz v1, :cond_112

    .line 257
    iget-object v0, p0, LMB;->a:LMC;

    invoke-interface {v0, p0}, LMC;->b(LMB;)Z

    move-result v0

    .line 260
    :cond_112
    if-eqz v0, :cond_120

    .line 261
    iget v0, p0, LMB;->f:F

    iput v0, p0, LMB;->h:F

    .line 262
    iget v0, p0, LMB;->g:F

    iput v0, p0, LMB;->i:F

    .line 263
    iget v0, p0, LMB;->c:F

    iput v0, p0, LMB;->d:F

    .line 267
    :cond_120
    const/4 v0, 0x1

    goto/16 :goto_5
.end method

.method public b()F
    .registers 2

    .prologue
    .line 302
    iget v0, p0, LMB;->b:F

    return v0
.end method

.method public c()F
    .registers 3

    .prologue
    .line 373
    iget v0, p0, LMB;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_d

    iget v0, p0, LMB;->c:F

    iget v1, p0, LMB;->d:F

    div-float/2addr v0, v1

    :goto_c
    return v0

    :cond_d
    const/high16 v0, 0x3f80

    goto :goto_c
.end method
