.class public final LMQ;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LMO;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LMY;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LMM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 33
    iput-object p1, p0, LMQ;->a:LYD;

    .line 34
    const-class v0, LMO;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LMQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LMQ;->a:LZb;

    .line 37
    const-class v0, LMY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LMQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LMQ;->b:LZb;

    .line 40
    const-class v0, LMM;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LMQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LMQ;->c:LZb;

    .line 43
    return-void
.end method

.method static synthetic a(LMQ;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LMQ;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LMQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LMQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 50
    const-class v0, LMO;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LMQ;->a:LZb;

    invoke-virtual {p0, v0, v1}, LMQ;->a(Laop;LZb;)V

    .line 51
    const-class v0, LMY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LMQ;->b:LZb;

    invoke-virtual {p0, v0, v1}, LMQ;->a(Laop;LZb;)V

    .line 52
    const-class v0, LMM;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LMQ;->c:LZb;

    invoke-virtual {p0, v0, v1}, LMQ;->a(Laop;LZb;)V

    .line 53
    iget-object v0, p0, LMQ;->a:LZb;

    iget-object v1, p0, LMQ;->a:LYD;

    iget-object v1, v1, LYD;->a:Lacy;

    iget-object v1, v1, Lacy;->b:LZb;

    invoke-static {v1}, LMQ;->a(Laoz;)Laoz;

    move-result-object v1

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 56
    iget-object v0, p0, LMQ;->b:LZb;

    new-instance v1, LMR;

    invoke-direct {v1, p0}, LMR;-><init>(LMQ;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 75
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 79
    return-void
.end method
