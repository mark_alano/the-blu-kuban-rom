.class public final LMS;
.super LML;
.source "HoneycombActionBarHelper.java"


# static fields
.field private static final a:Z


# instance fields
.field private a:LMP;

.field private final a:LNa;

.field private a:Landroid/content/ComponentName;

.field private a:Landroid/os/Bundle;

.field private a:Landroid/widget/SearchView;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private a:[Landroid/accounts/Account;

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 77
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_a

    const/4 v0, 0x1

    :goto_7
    sput-boolean v0, LMS;->a:Z

    return-void

    :cond_a
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;ILNa;Laoz;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "I",
            "LNa;",
            "Laoz",
            "<",
            "Lfb;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0, p1, p2, p3}, LML;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 86
    new-array v0, v1, [Landroid/accounts/Account;

    iput-object v0, p0, LMS;->a:[Landroid/accounts/Account;

    .line 87
    iput-boolean v1, p0, LMS;->b:Z

    .line 94
    iput-object p4, p0, LMS;->a:LNa;

    .line 95
    iput-object p5, p0, LMS;->a:Laoz;

    .line 96
    invoke-virtual {p0}, LMS;->b()V

    .line 97
    return-void
.end method

.method private static a([Landroid/accounts/Account;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 157
    const/4 v0, 0x0

    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_12

    .line 158
    aget-object v1, p0, v0

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 162
    :goto_e
    return v0

    .line 157
    :cond_f
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 162
    :cond_12
    const/4 v0, -0x1

    goto :goto_e
.end method

.method static synthetic a(LMS;)Landroid/widget/SearchView;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    return-object v0
.end method

.method private a([Landroid/accounts/Account;)Ljava/util/Set;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/accounts/Account;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 167
    array-length v2, p1

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v2, :cond_13

    aget-object v3, p1, v0

    .line 168
    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 170
    :cond_13
    return-object v1
.end method

.method static synthetic a(LMS;Ljava/lang/String;Landroid/view/MenuItem;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, LMS;->a(Ljava/lang/String;Landroid/view/MenuItem;)V

    return-void
.end method

.method private a(Landroid/app/ActionBar;)V
    .registers 4
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, LMS;->a:[Landroid/accounts/Account;

    invoke-virtual {p0}, LMS;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LMS;->a([Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    .line 176
    if-ltz v0, :cond_f

    .line 177
    invoke-virtual {p1, v0}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 179
    :cond_f
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/view/MenuItem;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 331
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setIconified(Z)V

    .line 332
    sget-boolean v0, LMS;->a:Z

    if-eqz v0, :cond_d

    .line 333
    invoke-interface {p2}, Landroid/view/MenuItem;->collapseActionView()Z

    .line 336
    :cond_d
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEARCH"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, LMS;->a:Landroid/app/Activity;

    const-string v2, "search"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 341
    iget-object v2, p0, LMS;->a:Landroid/content/ComponentName;

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    .line 342
    if-eqz v0, :cond_49

    .line 343
    invoke-virtual {v0}, Landroid/app/SearchableInfo;->getSearchActivity()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 349
    :goto_2d
    if-eqz p1, :cond_34

    .line 350
    const-string v0, "query"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    :cond_34
    iget-object v0, p0, LMS;->a:Landroid/os/Bundle;

    if-eqz v0, :cond_3f

    .line 353
    const-string v0, "app_data"

    iget-object v2, p0, LMS;->a:Landroid/os/Bundle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 356
    :cond_3f
    iget-object v0, p0, LMS;->a:LMP;

    if-eqz v0, :cond_4f

    .line 357
    iget-object v0, p0, LMS;->a:LMP;

    invoke-interface {v0, v1}, LMP;->b(Landroid/content/Intent;)V

    .line 361
    :goto_48
    return-void

    .line 346
    :cond_49
    iget-object v0, p0, LMS;->a:Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_2d

    .line 359
    :cond_4f
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_48
.end method

.method static synthetic a()Z
    .registers 1

    .prologue
    .line 51
    sget-boolean v0, LMS;->a:Z

    return v0
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 107
    iget-object v0, p0, LMS;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/docs/app/BaseActivity;

    if-eqz v0, :cond_f

    .line 108
    iget-object v0, p0, LMS;->a:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/apps/docs/app/BaseActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/BaseActivity;->c()Z

    move-result v0

    .line 110
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 421
    const/4 v0, 0x1

    iput-boolean v0, p0, LMS;->b:Z

    .line 422
    return-void
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 404
    .line 405
    sget v1, Leg;->launcher_docs_icon:I

    if-ne p1, v1, :cond_12

    .line 406
    const/4 v0, 0x1

    .line 413
    :cond_6
    :goto_6
    iget-object v1, p0, LMS;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 414
    if-eqz v1, :cond_11

    .line 415
    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayUseLogoEnabled(Z)V

    .line 417
    :cond_11
    return-void

    .line 407
    :cond_12
    sget v1, Leg;->launcher_drive_icon:I

    if-eq p1, v1, :cond_6

    .line 410
    const-string v1, "ActionBar cannot display requested resId"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    goto :goto_6
.end method

.method public a(LMZ;)V
    .registers 2
    .parameter

    .prologue
    .line 225
    return-void
.end method

.method public a(Landroid/view/MenuItem;LMP;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 232
    if-nez p1, :cond_3

    .line 323
    :cond_2
    :goto_2
    return-void

    .line 237
    :cond_3
    iget-object v0, p0, LMS;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    if-nez v0, :cond_18

    .line 238
    iget-object v0, p0, LMS;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 239
    invoke-virtual {v0}, Lfb;->c()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 242
    :cond_18
    iput-object p2, p0, LMS;->a:LMP;

    .line 243
    iget-object v0, p0, LMS;->a:Landroid/app/Activity;

    const-string v1, "search"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 244
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    .line 245
    instance-of v2, v1, Landroid/widget/SearchView;

    if-eqz v2, :cond_2

    .line 249
    check-cast v1, Landroid/widget/SearchView;

    iput-object v1, p0, LMS;->a:Landroid/widget/SearchView;

    .line 250
    iget-object v1, p0, LMS;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, LMS;->a(Ljava/lang/String;)V

    .line 251
    iget-object v1, p0, LMS;->a:Landroid/widget/SearchView;

    iget-object v2, p0, LMS;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 252
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    new-instance v1, LMV;

    invoke-direct {v1, p0, p1}, LMV;-><init>(LMS;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 279
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    new-instance v1, LMW;

    invoke-direct {v1, p0, p1}, LMW;-><init>(LMS;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 293
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    new-instance v1, LMX;

    invoke-direct {v1, p0, p1}, LMX;-><init>(LMS;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnSuggestionListener(Landroid/widget/SearchView$OnSuggestionListener;)V

    goto :goto_2
.end method

.method public a(Landroid/widget/Button;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 136
    invoke-virtual {p0}, LMS;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 144
    :cond_a
    :goto_a
    return-void

    .line 139
    :cond_b
    invoke-super {p0, p1, p2}, LML;->a(Landroid/widget/Button;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, LMS;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_a

    .line 142
    invoke-direct {p0, v0}, LMS;->a(Landroid/app/ActionBar;)V

    goto :goto_a
.end method

.method public a(Landroid/widget/Button;[Landroid/accounts/Account;LMN;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 184
    invoke-direct {p0, p2}, LMS;->a([Landroid/accounts/Account;)Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, LMS;->a:[Landroid/accounts/Account;

    invoke-direct {p0, v1}, LMS;->a([Landroid/accounts/Account;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 187
    iget-object v1, p0, LMS;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 188
    if-eqz v1, :cond_3c

    if-nez v0, :cond_3c

    .line 189
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 190
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 192
    iput-object p2, p0, LMS;->a:[Landroid/accounts/Account;

    .line 193
    new-instance v0, LMT;

    invoke-direct {v0, p0, p2}, LMT;-><init>(LMS;[Landroid/accounts/Account;)V

    .line 204
    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, LMS;->a:Landroid/app/Activity;

    const v4, 0x1090009

    invoke-direct {v2, v3, v4, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 207
    new-instance v0, LMU;

    invoke-direct {v0, p0, p2, p3}, LMU;-><init>(LMS;[Landroid/accounts/Account;LMN;)V

    invoke-virtual {v1, v2, v0}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 217
    invoke-direct {p0, v1}, LMS;->a(Landroid/app/ActionBar;)V

    .line 219
    :cond_3c
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 392
    iput-object p1, p0, LMS;->a:Ljava/lang/String;

    .line 393
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    if-eqz v0, :cond_11

    .line 394
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 395
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 397
    :cond_11
    return-void
.end method

.method public a(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 372
    if-nez p5, :cond_6

    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    if-nez v0, :cond_19

    .line 373
    :cond_6
    iget-object v0, p0, LMS;->a:Landroid/app/Activity;

    const-string v1, "search"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    .line 375
    invoke-virtual/range {v0 .. v5}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    .line 388
    :goto_18
    return-void

    .line 381
    :cond_19
    if-eqz p1, :cond_23

    .line 382
    iput-object p1, p0, LMS;->a:Ljava/lang/String;

    .line 383
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 385
    :cond_23
    iput-object p4, p0, LMS;->a:Landroid/os/Bundle;

    .line 386
    iput-object p3, p0, LMS;->a:Landroid/content/ComponentName;

    .line 387
    iget-object v0, p0, LMS;->a:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    goto :goto_18
.end method

.method public a(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 116
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_28

    iget-boolean v1, p0, LMS;->b:Z

    if-nez v1, :cond_28

    .line 117
    invoke-direct {p0}, LMS;->b()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 118
    iget-object v1, p0, LMS;->a:LNa;

    invoke-virtual {p0}, LMS;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LNa;->c(Ljava/lang/String;)V

    .line 130
    :goto_1d
    return v0

    .line 120
    :cond_1e
    iget-object v1, p0, LMS;->a:LNa;

    invoke-virtual {p0}, LMS;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LNa;->b(Ljava/lang/String;)V

    goto :goto_1d

    .line 123
    :cond_28
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_create_new_doc:I

    if-ne v1, v2, :cond_3a

    .line 124
    iget-object v1, p0, LMS;->a:LNa;

    invoke-virtual {p0}, LMS;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LNa;->a(Ljava/lang/String;)V

    goto :goto_1d

    .line 126
    :cond_3a
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_search:I

    if-ne v1, v2, :cond_48

    .line 127
    iget-object v1, p0, LMS;->a:LNa;

    invoke-interface {v1}, LNa;->a()V

    goto :goto_1d

    .line 130
    :cond_48
    const/4 v0, 0x0

    goto :goto_1d
.end method

.method public b()V
    .registers 3

    .prologue
    .line 436
    iget-object v0, p0, LMS;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 437
    if-eqz v0, :cond_f

    .line 438
    invoke-direct {p0}, LMS;->b()Z

    move-result v1

    .line 439
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 441
    :cond_f
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, LMS;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_15

    .line 150
    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 151
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_12

    const/4 p2, 0x0

    :cond_12
    invoke-virtual {v0, p2}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 153
    :cond_15
    return-void
.end method
