.class public LMc;
.super Ljava/lang/Object;
.source "NavigationFragment.java"

# interfaces
.implements Landroid/widget/ExpandableListView$OnChildClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/fragment/NavigationFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/fragment/NavigationFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 224
    iput-object p1, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildClick(Landroid/widget/ExpandableListView;Landroid/view/View;IIJ)Z
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 228
    iget-object v0, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a(Lcom/google/android/apps/docs/fragment/NavigationFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 230
    invoke-static {p3, p4}, Landroid/widget/ExpandableListView;->getPackedPositionForChild(II)J

    move-result-wide v0

    .line 232
    iget-object v2, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v2, v2, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v0

    .line 233
    iget-object v1, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v1, v1, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Landroid/widget/ExpandableListView;

    invoke-virtual {v1, v0}, Landroid/widget/ExpandableListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 234
    sget-object v1, LPX;->n:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1, v0}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 236
    new-instance v1, LiR;

    invoke-direct {v1}, LiR;-><init>()V

    .line 237
    iget-object v2, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v2, v2, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LiG;

    iget-object v3, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v3, v3, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Liv;

    invoke-interface {v3}, Liv;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, LiG;->a(Ljava/lang/String;Ljava/lang/String;)LiE;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(LiE;)LiR;

    .line 239
    iget-object v0, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LiG;

    invoke-interface {v0}, LiG;->a()LiE;

    move-result-object v0

    invoke-virtual {v1, v0}, LiR;->a(LiE;)LiR;

    .line 240
    iget-object v0, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:LMe;

    iget-object v2, p0, LMc;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    iget-object v2, v2, Lcom/google/android/apps/docs/fragment/NavigationFragment;->a:Ljl;

    invoke-interface {v2}, Ljl;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1}, LiR;->a()LiQ;

    move-result-object v1

    invoke-static {v2, v1}, Ljr;->a(Ljava/util/List;LiQ;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, LMe;->a(Ljava/util/List;)V

    .line 243
    const/4 v0, 0x1

    return v0
.end method
