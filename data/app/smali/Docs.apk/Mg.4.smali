.class public LMg;
.super Ljava/lang/Object;
.source "OnlineSearchFragment.java"

# interfaces
.implements LMf;


# instance fields
.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;


# direct methods
.method constructor <init>(Laoz;)V
    .registers 2
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, LMg;->a:Laoz;

    .line 59
    return-void
.end method

.method private a()Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;
    .registers 3

    .prologue
    .line 100
    iget-object v0, p0, LMg;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    if-eqz v0, :cond_7

    .line 101
    iget-object v0, p0, LMg;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    .line 103
    :goto_6
    return-object v0

    :cond_7
    invoke-direct {p0}, LMg;->a()Lo;

    move-result-object v0

    const-string v1, "OnlineSearchFragment"

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    goto :goto_6
.end method

.method private a()Lo;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, LMg;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 109
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->a()Lo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()LSC;
    .registers 2

    .prologue
    .line 95
    invoke-direct {p0}, LMg;->a()Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    move-result-object v0

    .line 96
    if-eqz v0, :cond_b

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)LSC;

    move-result-object v0

    :goto_a
    return-object v0

    :cond_b
    sget-object v0, LSC;->a:LSC;

    goto :goto_a
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)LamQ;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LamQ",
            "<",
            "LSF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    invoke-direct {p0}, LMg;->a()Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_29

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_29

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->b(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 70
    iput-object v0, p0, LMg;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    .line 90
    :goto_22
    iget-object v0, p0, LMg;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)LamQ;

    move-result-object v0

    return-object v0

    .line 72
    :cond_29
    invoke-direct {p0}, LMg;->a()Lo;

    move-result-object v1

    .line 74
    if-eqz v0, :cond_3a

    .line 75
    invoke-virtual {v1}, Lo;->a()Lz;

    move-result-object v2

    invoke-virtual {v2, v0}, Lz;->a(Landroid/support/v4/app/Fragment;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 78
    :cond_3a
    new-instance v0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;-><init>()V

    .line 79
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 80
    const-string v3, "accountName"

    invoke-virtual {v2, v3, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v3, "query"

    invoke-virtual {v2, v3, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->d(Landroid/os/Bundle;)V

    .line 83
    invoke-virtual {v1}, Lo;->a()Lz;

    move-result-object v1

    const-string v2, "OnlineSearchFragment"

    invoke-virtual {v1, v0, v2}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v1

    invoke-virtual {v1}, Lz;->a()I

    .line 87
    iput-object v0, p0, LMg;->a:Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;

    goto :goto_22
.end method
