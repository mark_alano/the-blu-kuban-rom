.class public LMn;
.super Ljava/lang/Object;
.source "PreviewFragment.java"

# interfaces
.implements LamE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LamE",
        "<",
        "LUq;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

.field final synthetic a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 558
    iput-object p1, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    iput-object p2, p0, LMn;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LUq;)V
    .registers 4
    .parameter

    .prologue
    .line 574
    iget-object v0, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->d(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 577
    iget-object v0, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x0

    .line 579
    :goto_1e
    iget-object v1, p0, LMn;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 584
    :goto_26
    return-void

    .line 577
    :cond_27
    iget-object v0, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1e

    .line 582
    :cond_3a
    iget-object v0, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LUq;)LUq;

    .line 583
    iget-object v0, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    sget-object v1, LMs;->b:LMs;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LMs;)V

    goto :goto_26
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 558
    check-cast p1, LUq;

    invoke-virtual {p0, p1}, LMn;->a(LUq;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .registers 5
    .parameter

    .prologue
    .line 561
    iget-object v0, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->c(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 562
    instance-of v0, p1, LKw;

    if-eqz v0, :cond_1b

    .line 563
    iget-object v0, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    sget-object v1, LMs;->a:LMs;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LMs;)V

    .line 570
    :cond_1a
    :goto_1a
    return-void

    .line 564
    :cond_1b
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_1a

    .line 565
    const-string v0, "PreviewFragment"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-static {v0, v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 566
    iget-object v0, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    iget-object v0, v0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LZM;

    iget-object v1, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Len;->sharing_error:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 568
    iget-object v0, p0, LMn;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    sget-object v1, LMs;->b:LMs;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LMs;)V

    goto :goto_1a
.end method
