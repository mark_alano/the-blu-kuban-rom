.class public LMq;
.super Ljava/lang/Object;
.source "PreviewFragment.java"

# interfaces
.implements LamE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LamE",
        "<",
        "LUq;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/fragment/PreviewFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 928
    iput-object p1, p0, LMq;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LUq;)V
    .registers 4
    .parameter

    .prologue
    .line 942
    iget-object v0, p0, LMq;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->g(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LMq;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 943
    iget-object v0, p0, LMq;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LUq;)LUq;

    .line 944
    iget-object v0, p0, LMq;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V

    .line 945
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 928
    check-cast p1, LUq;

    invoke-virtual {p0, p1}, LMq;->a(LUq;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .registers 5
    .parameter

    .prologue
    .line 931
    iget-object v0, p0, LMq;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->f(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;

    move-result-object v0

    iget-object v1, p0, LMq;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 932
    instance-of v0, p1, LKw;

    if-eqz v0, :cond_1b

    .line 933
    iget-object v0, p0, LMq;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    sget-object v1, LMs;->a:LMs;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LMs;)V

    .line 938
    :cond_1a
    :goto_1a
    return-void

    .line 934
    :cond_1b
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_1a

    .line 935
    const-string v0, "PreviewFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-static {v0, v1, v2}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 936
    iget-object v0, p0, LMq;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    sget-object v1, LMs;->b:LMs;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LMs;)V

    goto :goto_1a
.end method
