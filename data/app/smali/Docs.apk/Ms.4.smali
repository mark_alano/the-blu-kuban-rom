.class public final enum LMs;
.super Ljava/lang/Enum;
.source "PreviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LMs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LMs;

.field private static final synthetic a:[LMs;

.field public static final enum b:LMs;

.field public static final enum c:LMs;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 108
    new-instance v0, LMs;

    const-string v1, "OFFLINE"

    invoke-direct {v0, v1, v2}, LMs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LMs;->a:LMs;

    new-instance v0, LMs;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, LMs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LMs;->b:LMs;

    new-instance v0, LMs;

    const-string v1, "COMMUNICATING"

    invoke-direct {v0, v1, v4}, LMs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LMs;->c:LMs;

    .line 107
    const/4 v0, 0x3

    new-array v0, v0, [LMs;

    sget-object v1, LMs;->a:LMs;

    aput-object v1, v0, v2

    sget-object v1, LMs;->b:LMs;

    aput-object v1, v0, v3

    sget-object v1, LMs;->c:LMs;

    aput-object v1, v0, v4

    sput-object v0, LMs;->a:[LMs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LMs;
    .registers 2
    .parameter

    .prologue
    .line 107
    const-class v0, LMs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LMs;

    return-object v0
.end method

.method public static values()[LMs;
    .registers 1

    .prologue
    .line 107
    sget-object v0, LMs;->a:[LMs;

    invoke-virtual {v0}, [LMs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LMs;

    return-object v0
.end method
