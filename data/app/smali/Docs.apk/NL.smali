.class public abstract LNL;
.super Ljava/lang/Object;
.source "HttpIssuerBase.java"

# interfaces
.implements LNK;


# instance fields
.field private a:I

.field private a:LNO;

.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "LNN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, LNL;->a:Ljava/lang/ThreadLocal;

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, LNL;->a:LNO;

    .line 102
    const/4 v0, 0x0

    iput v0, p0, LNL;->a:I

    return-void
.end method

.method private a(Lorg/apache/http/params/HttpParams;)V
    .registers 3
    .parameter

    .prologue
    .line 223
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxTotalConnections(Lorg/apache/http/params/HttpParams;I)V

    .line 224
    new-instance v0, LNM;

    invoke-direct {v0, p0}, LNM;-><init>(LNL;)V

    invoke-static {p1, v0}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 230
    return-void
.end method

.method private declared-synchronized b()LNO;
    .registers 4

    .prologue
    .line 206
    monitor-enter p0

    :try_start_1
    iget v0, p0, LNL;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LNL;->a:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_1d

    .line 207
    const-string v0, "HttpIssuerBase"

    const-string v1, "HttpIssuer connection leak, number of active connections exceeded 10"

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    invoke-static {v0, v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, LNL;->a:LNO;

    .line 210
    const/4 v0, 0x1

    iput v0, p0, LNL;->a:I

    .line 212
    :cond_1d
    iget-object v0, p0, LNL;->a:LNO;

    if-nez v0, :cond_30

    .line 213
    invoke-virtual {p0}, LNL;->a()LNO;

    move-result-object v0

    iput-object v0, p0, LNL;->a:LNO;

    .line 214
    iget-object v0, p0, LNL;->a:LNO;

    invoke-interface {v0}, LNO;->a()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    invoke-direct {p0, v0}, LNL;->a(Lorg/apache/http/params/HttpParams;)V

    .line 216
    :cond_30
    iget-object v0, p0, LNL;->a:LNO;
    :try_end_32
    .catchall {:try_start_1 .. :try_end_32} :catchall_34

    monitor-exit p0

    return-object v0

    .line 206
    :catchall_34
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .registers 6
    .parameter

    .prologue
    .line 117
    const/4 v1, 0x0

    .line 119
    :try_start_1
    new-instance v0, LNR;

    invoke-direct {p0}, LNL;->b()LNO;

    move-result-object v2

    invoke-interface {v2, p1}, LNO;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-direct {v0, v2}, LNR;-><init>(Lorg/apache/http/HttpResponse;)V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_19

    .line 122
    iget-object v1, p0, LNL;->a:Ljava/lang/ThreadLocal;

    new-instance v2, LNN;

    invoke-direct {v2, p1, v0}, LNN;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;LNR;)V

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-object v0

    :catchall_19
    move-exception v0

    iget-object v2, p0, LNL;->a:Ljava/lang/ThreadLocal;

    new-instance v3, LNN;

    invoke-direct {v3, p1, v1}, LNN;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;LNR;)V

    invoke-virtual {v2, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0
.end method

.method private declared-synchronized d()V
    .registers 2

    .prologue
    .line 233
    monitor-enter p0

    :try_start_1
    iget v0, p0, LNL;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LNL;->a:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    .line 234
    monitor-exit p0

    return-void

    .line 233
    :catchall_9
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected abstract a()LNO;
.end method

.method public a(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .registers 4
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, LNL;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_d

    .line 107
    invoke-direct {p0, p1}, LNL;->b(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0

    .line 109
    :cond_d
    new-instance v1, Ljava/io/IOException;

    const-string v0, "More than 1 active request per thread is not allowed."

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, LNL;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNN;

    invoke-virtual {v0}, LNN;->a()Ljava/io/IOException;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 112
    throw v1
.end method

.method public a()V
    .registers 3

    .prologue
    .line 128
    iget-object v0, p0, LNL;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNN;

    .line 129
    if-nez v0, :cond_12

    .line 130
    const-string v0, "HttpIssuerBase"

    const-string v1, "Attempt to consume entity of HttpIssuer when no request is executing."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_11
    :goto_11
    return-void

    .line 133
    :cond_12
    invoke-virtual {v0}, LNN;->a()LNR;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_11

    .line 137
    invoke-virtual {v0}, LNR;->a()LNP;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_11

    .line 140
    :try_start_1e
    invoke-virtual {v0}, LNP;->consumeContent()V
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_21} :catch_22

    goto :goto_11

    .line 141
    :catch_22
    move-exception v0

    goto :goto_11
.end method

.method public b()V
    .registers 7

    .prologue
    .line 149
    iget-object v0, p0, LNL;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNN;

    .line 150
    if-nez v0, :cond_17

    .line 151
    const-string v0, "HttpIssuerBase"

    const-string v1, "Attempt to close HttpIssuer when no request is executing."

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    invoke-static {v0, v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 181
    :goto_16
    return-void

    .line 154
    :cond_17
    const-wide/16 v2, 0x0

    .line 155
    const/4 v1, 0x0

    .line 157
    :try_start_1a
    const-string v4, "HttpIssuerBase"

    const-string v5, "Start closing time"

    invoke-static {v4, v5}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-direct {p0}, LNL;->d()V

    .line 159
    invoke-virtual {v0}, LNN;->a()Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v4

    .line 160
    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->isAborted()Z
    :try_end_2b
    .catchall {:try_start_1a .. :try_end_2b} :catchall_8b

    move-result v5

    if-eqz v5, :cond_3a

    .line 170
    invoke-virtual {p0}, LNL;->a()V

    .line 178
    invoke-virtual {v0}, LNN;->a()V

    .line 179
    iget-object v0, p0, LNL;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_16

    .line 163
    :cond_3a
    :try_start_3a
    invoke-virtual {v0}, LNN;->a()LNR;

    move-result-object v5

    .line 164
    if-eqz v5, :cond_46

    invoke-virtual {v5}, LNR;->a()Z

    move-result v5

    if-nez v5, :cond_4e

    .line 165
    :cond_46
    invoke-interface {v4}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V

    .line 166
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_4c
    .catchall {:try_start_3a .. :try_end_4c} :catchall_8b

    move-result-wide v2

    .line 167
    const/4 v1, 0x1

    .line 170
    :cond_4e
    invoke-virtual {p0}, LNL;->a()V

    .line 171
    if-eqz v1, :cond_82

    .line 172
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long v1, v4, v2

    .line 173
    const-wide/16 v3, 0x3e8

    cmp-long v3, v1, v3

    if-lez v3, :cond_82

    .line 174
    const-string v3, "HttpIssuerBase"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Excessive delay between abort and stream closure: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2}, Ljava/io/IOException;-><init>()V

    invoke-static {v3, v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 178
    :cond_82
    invoke-virtual {v0}, LNN;->a()V

    .line 179
    iget-object v0, p0, LNL;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_16

    .line 170
    :catchall_8b
    move-exception v1

    invoke-virtual {p0}, LNL;->a()V

    .line 178
    invoke-virtual {v0}, LNN;->a()V

    .line 179
    iget-object v0, p0, LNL;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    throw v1
.end method

.method public declared-synchronized c()V
    .registers 2

    .prologue
    .line 185
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LNL;->a:LNO;

    if-eqz v0, :cond_d

    .line 186
    iget-object v0, p0, LNL;->a:LNO;

    invoke-interface {v0}, LNO;->a()V

    .line 187
    const/4 v0, 0x0

    iput v0, p0, LNL;->a:I
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 189
    :cond_d
    monitor-exit p0

    return-void

    .line 185
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method
