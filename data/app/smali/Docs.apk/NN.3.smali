.class LNN;
.super Ljava/lang/Object;
.source "HttpIssuerBase.java"


# instance fields
.field private final a:LNR;

.field private final a:Ljava/io/IOException;

.field private final a:Lorg/apache/http/client/methods/HttpUriRequest;

.field private a:Z


# direct methods
.method public constructor <init>(Lorg/apache/http/client/methods/HttpUriRequest;LNR;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, LNN;->a:Z

    .line 39
    iput-object p1, p0, LNN;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    .line 40
    iput-object p2, p0, LNN;->a:LNR;

    .line 41
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    iput-object v0, p0, LNN;->a:Ljava/io/IOException;

    .line 42
    return-void
.end method


# virtual methods
.method public a()LNR;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, LNN;->a:LNR;

    return-object v0
.end method

.method public a()Ljava/io/IOException;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, LNN;->a:Ljava/io/IOException;

    return-object v0
.end method

.method public a()Lorg/apache/http/client/methods/HttpUriRequest;
    .registers 2

    .prologue
    .line 45
    iget-object v0, p0, LNN;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, LNN;->a:Z

    .line 63
    return-void
.end method

.method protected finalize()V
    .registers 5

    .prologue
    .line 67
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 68
    iget-boolean v0, p0, LNN;->a:Z

    if-nez v0, :cond_1d

    .line 69
    const-string v0, "HttpIssuerBase"

    const-string v1, "HttpIssuer request NOT closed."

    new-instance v2, Ljava/io/IOException;

    const-string v3, "HttpIsser request leak."

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LNN;->a()Ljava/io/IOException;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    invoke-static {v0, v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 72
    :cond_1d
    return-void
.end method
