.class LNi;
.super Ljava/lang/Object;
.source "AuthTokenManagerImpl.java"


# instance fields
.field final synthetic a:LNg;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LNg;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, LNi;->a:LNg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LNi;->a:Ljava/util/Map;

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LNi;->b:Ljava/util/Map;

    .line 72
    iput-object p2, p0, LNi;->a:Ljava/lang/String;

    .line 73
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 80
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LNi;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 81
    if-nez v0, :cond_2e

    .line 82
    iget-object v0, p0, LNi;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->a()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_42

    .line 84
    :try_start_14
    iget-object v0, p0, LNi;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    iget-object v1, p0, LNi;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, LNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v1, p0, LNi;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_25
    .catchall {:try_start_14 .. :try_end_25} :catchall_37
    .catch Landroid/accounts/OperationCanceledException; {:try_start_14 .. :try_end_25} :catch_30
    .catch Landroid/accounts/AuthenticatorException; {:try_start_14 .. :try_end_25} :catch_45
    .catch LNt; {:try_start_14 .. :try_end_25} :catch_47
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_25} :catch_49

    .line 95
    :try_start_25
    iget-object v1, p0, LNi;->a:LNg;

    invoke-static {v1}, LNg;->a(LNg;)LNb;

    move-result-object v1

    invoke-interface {v1}, LNb;->b()V
    :try_end_2e
    .catchall {:try_start_25 .. :try_end_2e} :catchall_42

    .line 98
    :cond_2e
    monitor-exit p0

    return-object v0

    .line 86
    :catch_30
    move-exception v0

    .line 87
    :try_start_31
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_37
    .catchall {:try_start_31 .. :try_end_37} :catchall_37

    .line 95
    :catchall_37
    move-exception v0

    :try_start_38
    iget-object v1, p0, LNi;->a:LNg;

    invoke-static {v1}, LNg;->a(LNg;)LNb;

    move-result-object v1

    invoke-interface {v1}, LNb;->b()V

    throw v0
    :try_end_42
    .catchall {:try_start_38 .. :try_end_42} :catchall_42

    .line 80
    :catchall_42
    move-exception v0

    monitor-exit p0

    throw v0

    .line 88
    :catch_45
    move-exception v0

    .line 89
    :try_start_46
    throw v0

    .line 90
    :catch_47
    move-exception v0

    .line 91
    throw v0

    .line 92
    :catch_49
    move-exception v0

    .line 93
    throw v0
    :try_end_4b
    .catchall {:try_start_46 .. :try_end_4b} :catchall_37
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 121
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LNi;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 122
    iget-object v1, p0, LNi;->a:LNg;

    invoke-static {v1}, LNg;->a(LNg;)LNb;

    move-result-object v1

    invoke-interface {v1}, LNb;->a()V
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_40

    .line 125
    :try_start_12
    iget-object v1, p0, LNi;->a:LNg;

    invoke-static {v1}, LNg;->a(LNg;)LNb;

    move-result-object v1

    iget-object v2, p0, LNi;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, LNb;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, LNi;->b:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2a
    .catchall {:try_start_12 .. :try_end_2a} :catchall_35

    .line 128
    :try_start_2a
    iget-object v0, p0, LNi;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->b()V
    :try_end_33
    .catchall {:try_start_2a .. :try_end_33} :catchall_40

    .line 130
    monitor-exit p0

    return-void

    .line 128
    :catchall_35
    move-exception v0

    :try_start_36
    iget-object v1, p0, LNi;->a:LNg;

    invoke-static {v1}, LNg;->a(LNg;)LNb;

    move-result-object v1

    invoke-interface {v1}, LNb;->b()V

    throw v0
    :try_end_40
    .catchall {:try_start_36 .. :try_end_40} :catchall_40

    .line 121
    :catchall_40
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 133
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LNi;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->a()V
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_2b

    .line 136
    :try_start_a
    iget-object v0, p0, LNi;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    iget-object v1, p0, LNi;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, LNb;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_15
    .catchall {:try_start_a .. :try_end_15} :catchall_20

    .line 138
    :try_start_15
    iget-object v0, p0, LNi;->a:LNg;

    invoke-static {v0}, LNg;->a(LNg;)LNb;

    move-result-object v0

    invoke-interface {v0}, LNb;->b()V
    :try_end_1e
    .catchall {:try_start_15 .. :try_end_1e} :catchall_2b

    .line 140
    monitor-exit p0

    return-void

    .line 138
    :catchall_20
    move-exception v0

    :try_start_21
    iget-object v1, p0, LNi;->a:LNg;

    invoke-static {v1}, LNg;->a(LNg;)LNb;

    move-result-object v1

    invoke-interface {v1}, LNb;->b()V

    throw v0
    :try_end_2b
    .catchall {:try_start_21 .. :try_end_2b} :catchall_2b

    .line 133
    :catchall_2b
    move-exception v0

    monitor-exit p0

    throw v0
.end method
