.class public LNu;
.super Ljava/lang/Object;
.source "RedirectHelper.java"


# direct methods
.method public static a(LNj;Ljava/lang/String;Ljava/net/URI;IZ)Lorg/apache/http/HttpResponse;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 59
    if-eqz p4, :cond_a

    .line 60
    invoke-interface {p0, v0}, LNj;->a(Lorg/apache/http/HttpRequest;)V

    .line 63
    :cond_a
    const/4 v1, 0x0

    invoke-static {p0, p1, v0, p3, v1}, LNu;->a(LNj;Ljava/lang/String;Lorg/apache/http/client/methods/HttpRequestBase;ILjava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method public static a(LNj;Ljava/lang/String;Lorg/apache/http/client/methods/HttpRequestBase;ILjava/lang/String;)Lorg/apache/http/HttpResponse;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-virtual {p2}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_79

    .line 86
    new-instance v0, Ljava/net/URI;

    const-string v3, "https"

    invoke-virtual {v1}, Ljava/net/URI;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/net/URI;->getFragment()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v4, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p2, v0}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 90
    :goto_1d
    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2b

    .line 91
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Request URI host should not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2b
    move v1, v2

    move-object v3, v0

    .line 93
    :goto_2d
    if-gt v1, p3, :cond_70

    .line 94
    if-nez p4, :cond_6b

    invoke-interface {p0, p1, p2}, LNj;->a(Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 96
    :goto_35
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    .line 97
    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    const/16 v6, 0x12e

    if-eq v5, v6, :cond_49

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    const/16 v5, 0x12d

    if-ne v4, v5, :cond_78

    .line 99
    :cond_49
    invoke-interface {p0}, LNj;->a()V

    .line 100
    invoke-interface {p0}, LNj;->b()V

    .line 101
    new-instance v4, Ljava/net/URI;

    const-string v5, "Location"

    invoke-interface {v0, v5}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    aget-object v0, v0, v2

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 102
    invoke-virtual {v3, v4}, Ljava/net/URI;->resolve(Ljava/net/URI;)Ljava/net/URI;

    move-result-object v3

    .line 103
    invoke-virtual {p2, v3}, Lorg/apache/http/client/methods/HttpRequestBase;->setURI(Ljava/net/URI;)V

    .line 93
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2d

    .line 94
    :cond_6b
    invoke-interface {p0, p1, p2, p4}, LNj;->a(Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    goto :goto_35

    .line 110
    :cond_70
    new-instance v0, LNv;

    const-string v1, "Excessive redirects."

    invoke-direct {v0, v1}, LNv;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_78
    return-object v0

    :cond_79
    move-object v0, v1

    goto :goto_1d
.end method
