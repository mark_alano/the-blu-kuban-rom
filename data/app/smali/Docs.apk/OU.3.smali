.class public final LOU;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LPa;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LPd;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LOS;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LPb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 34
    iput-object p1, p0, LOU;->a:LYD;

    .line 35
    const-class v0, LPa;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, LOU;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LOU;->a:LZb;

    .line 38
    const-class v0, LPd;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LOU;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LOU;->b:LZb;

    .line 41
    const-class v0, LOS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LOU;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LOU;->c:LZb;

    .line 44
    const-class v0, LPb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LOU;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LOU;->d:LZb;

    .line 47
    return-void
.end method

.method static synthetic a(LOU;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LOU;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 116
    const-class v0, LOS;

    new-instance v1, LOV;

    invoke-direct {v1, p0}, LOV;-><init>(LOU;)V

    invoke-virtual {p0, v0, v1}, LOU;->a(Ljava/lang/Class;Laou;)V

    .line 124
    const-class v0, Lcom/google/android/apps/docs/notification/DocsNotificationProxyActivity;

    new-instance v1, LOW;

    invoke-direct {v1, p0}, LOW;-><init>(LOU;)V

    invoke-virtual {p0, v0, v1}, LOU;->a(Ljava/lang/Class;Laou;)V

    .line 132
    const-class v0, LPa;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LOU;->a:LZb;

    invoke-virtual {p0, v0, v1}, LOU;->a(Laop;LZb;)V

    .line 133
    const-class v0, LPd;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LOU;->b:LZb;

    invoke-virtual {p0, v0, v1}, LOU;->a(Laop;LZb;)V

    .line 134
    const-class v0, LOS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LOU;->c:LZb;

    invoke-virtual {p0, v0, v1}, LOU;->a(Laop;LZb;)V

    .line 135
    const-class v0, LPb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LOU;->d:LZb;

    invoke-virtual {p0, v0, v1}, LOU;->a(Laop;LZb;)V

    .line 136
    iget-object v0, p0, LOU;->a:LZb;

    iget-object v1, p0, LOU;->a:LYD;

    iget-object v1, v1, LYD;->a:LOU;

    iget-object v1, v1, LOU;->d:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 138
    iget-object v0, p0, LOU;->b:LZb;

    new-instance v1, LOX;

    invoke-direct {v1, p0}, LOX;-><init>(LOU;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 172
    iget-object v0, p0, LOU;->c:LZb;

    new-instance v1, LOY;

    invoke-direct {v1, p0}, LOY;-><init>(LOU;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 183
    iget-object v0, p0, LOU;->d:LZb;

    new-instance v1, LOZ;

    invoke-direct {v1, p0}, LOZ;-><init>(LOU;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 197
    return-void
.end method

.method public a(LOS;)V
    .registers 3
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, LOU;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTI;

    iput-object v0, p1, LOS;->a:LTI;

    .line 59
    iget-object v0, p0, LOU;->a:LYD;

    iget-object v0, v0, LYD;->a:Lez;

    iget-object v0, v0, Lez;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lev;

    iput-object v0, p1, LOS;->a:Lev;

    .line 65
    iget-object v0, p0, LOU;->a:LYD;

    iget-object v0, v0, LYD;->a:LOU;

    iget-object v0, v0, LOU;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPa;

    iput-object v0, p1, LOS;->a:LPa;

    .line 71
    iget-object v0, p0, LOU;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgb;

    iput-object v0, p1, LOS;->a:Lgb;

    .line 77
    iget-object v0, p0, LOU;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, LOS;->a:Llf;

    .line 83
    iget-object v0, p0, LOU;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaJ;

    iput-object v0, p1, LOS;->a:LaaJ;

    .line 89
    iget-object v0, p0, LOU;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, LOS;->a:Lgl;

    .line 95
    iget-object v0, p0, LOU;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, LOS;->a:Laoz;

    .line 101
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/notification/DocsNotificationProxyActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, LOU;->a:LYD;

    iget-object v0, v0, LYD;->a:Ld;

    iget-object v0, v0, Ld;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LOU;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p1, Lcom/google/android/apps/docs/notification/DocsNotificationProxyActivity;->b:Landroid/os/Handler;

    .line 111
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 201
    return-void
.end method
