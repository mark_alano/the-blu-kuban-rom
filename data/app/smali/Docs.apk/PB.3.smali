.class public final enum LPB;
.super Ljava/lang/Enum;
.source "CachedSearchResultTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPB;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPB;

.field private static final synthetic a:[LPB;

.field public static final enum b:LPB;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 42
    new-instance v0, LPB;

    const-string v1, "CACHED_SEARCH_ID"

    invoke-static {}, LPA;->b()LPA;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "cachedSearchId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->a()LQa;

    move-result-object v3

    invoke-static {}, LPC;->a()LPC;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LPB;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPB;->a:LPB;

    .line 49
    new-instance v0, LPB;

    const-string v1, "RESOURCE_ID"

    invoke-static {}, LPA;->b()LPA;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "resourceId"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LPB;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPB;->b:LPB;

    .line 41
    const/4 v0, 0x2

    new-array v0, v0, [LPB;

    sget-object v1, LPB;->a:LPB;

    aput-object v1, v0, v6

    sget-object v1, LPB;->b:LPB;

    aput-object v1, v0, v7

    sput-object v0, LPB;->a:[LPB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPB;->a:LPI;

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPB;
    .registers 2
    .parameter

    .prologue
    .line 41
    const-class v0, LPB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPB;

    return-object v0
.end method

.method public static values()[LPB;
    .registers 1

    .prologue
    .line 41
    sget-object v0, LPB;->a:[LPB;

    invoke-virtual {v0}, [LPB;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPB;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, LPB;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 41
    invoke-virtual {p0}, LPB;->a()LPI;

    move-result-object v0

    return-object v0
.end method
