.class public final enum LPH;
.super Ljava/lang/Enum;
.source "ContainsIdTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPH;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPH;

.field private static final synthetic a:[LPH;

.field public static final enum b:LPH;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 43
    new-instance v0, LPH;

    const-string v1, "ENTRY_ID"

    invoke-static {}, LPG;->b()LPG;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "entryId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->a()LQa;

    move-result-object v3

    invoke-static {}, LPW;->a()LPW;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LPH;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPH;->a:LPH;

    .line 47
    new-instance v0, LPH;

    const-string v1, "COLLECTION_ID"

    invoke-static {}, LPG;->b()LPG;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "collectionId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->a()LQa;

    move-result-object v3

    invoke-static {}, LPE;->a()LPE;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LPH;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPH;->b:LPH;

    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [LPH;

    sget-object v1, LPH;->a:LPH;

    aput-object v1, v0, v6

    sget-object v1, LPH;->b:LPH;

    aput-object v1, v0, v7

    sput-object v0, LPH;->a:[LPH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPH;->a:LPI;

    .line 56
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPH;
    .registers 2
    .parameter

    .prologue
    .line 42
    const-class v0, LPH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPH;

    return-object v0
.end method

.method public static values()[LPH;
    .registers 1

    .prologue
    .line 42
    sget-object v0, LPH;->a:[LPH;

    invoke-virtual {v0}, [LPH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPH;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, LPH;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 42
    invoke-virtual {p0}, LPH;->a()LPI;

    move-result-object v0

    return-object v0
.end method
