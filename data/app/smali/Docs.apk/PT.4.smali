.class public final enum LPT;
.super Ljava/lang/Enum;
.source "DocumentContentTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPT;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPT;

.field private static final synthetic a:[LPT;

.field public static final enum b:LPT;

.field public static final enum c:LPT;

.field public static final enum d:LPT;

.field public static final enum e:LPT;

.field public static final enum f:LPT;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/16 v7, 0xe

    .line 42
    new-instance v0, LPT;

    const-string v1, "CONTENT_ETAG"

    invoke-static {}, LPS;->b()LPS;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "contentETag"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LPT;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPT;->a:LPT;

    .line 45
    new-instance v0, LPT;

    const-string v1, "CONTENT_TYPE"

    invoke-static {}, LPS;->b()LPS;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "contentType"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LPT;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPT;->b:LPT;

    .line 48
    new-instance v0, LPT;

    const-string v1, "ENCRYPTION_KEY"

    invoke-static {}, LPS;->b()LPS;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "encryptionKey"

    sget-object v5, LQc;->d:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LPT;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPT;->c:LPT;

    .line 51
    new-instance v0, LPT;

    const-string v1, "ENCRYPTION_ALGORITHM"

    invoke-static {}, LPS;->b()LPS;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "encryptionAlgorithm"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v2, v7, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, LPT;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPT;->d:LPT;

    .line 54
    new-instance v0, LPT;

    const-string v1, "FILE_PATH"

    const/4 v2, 0x4

    invoke-static {}, LPS;->b()LPS;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    new-instance v4, LQa;

    const-string v5, "filePath"

    sget-object v6, LQc;->c:LQc;

    invoke-direct {v4, v5, v6}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3, v7, v4}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPT;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPT;->e:LPT;

    .line 57
    new-instance v0, LPT;

    const-string v1, "LAST_OPENED_TIME"

    const/4 v2, 0x5

    invoke-static {}, LPS;->b()LPS;

    move-result-object v3

    invoke-static {v3}, LPK;->a(LPN;)LPK;

    move-result-object v3

    new-instance v4, LQa;

    const-string v5, "lastOpenedTime"

    sget-object v6, LQc;->a:LQc;

    invoke-direct {v4, v5, v6}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3, v7, v4}, LPK;->a(ILQa;)LPK;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LPT;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPT;->f:LPT;

    .line 41
    const/4 v0, 0x6

    new-array v0, v0, [LPT;

    sget-object v1, LPT;->a:LPT;

    aput-object v1, v0, v8

    sget-object v1, LPT;->b:LPT;

    aput-object v1, v0, v9

    sget-object v1, LPT;->c:LPT;

    aput-object v1, v0, v10

    sget-object v1, LPT;->d:LPT;

    aput-object v1, v0, v11

    const/4 v1, 0x4

    sget-object v2, LPT;->e:LPT;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LPT;->f:LPT;

    aput-object v2, v0, v1

    sput-object v0, LPT;->a:[LPT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPT;->a:LPI;

    .line 65
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPT;
    .registers 2
    .parameter

    .prologue
    .line 41
    const-class v0, LPT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPT;

    return-object v0
.end method

.method public static values()[LPT;
    .registers 1

    .prologue
    .line 41
    sget-object v0, LPT;->a:[LPT;

    invoke-virtual {v0}, [LPT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPT;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 69
    iget-object v0, p0, LPT;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 41
    invoke-virtual {p0}, LPT;->a()LPI;

    move-result-object v0

    return-object v0
.end method
