.class public final LPe;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LPm;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LPn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 32
    iput-object p1, p0, LPe;->a:LYD;

    .line 33
    const-class v0, LPm;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LPe;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LPe;->a:LZb;

    .line 36
    const-class v0, LPn;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LPe;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LPe;->b:LZb;

    .line 39
    return-void
.end method

.method static synthetic a(LPe;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LPe;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 100
    const-class v0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    new-instance v1, LPf;

    invoke-direct {v1, p0}, LPf;-><init>(LPe;)V

    invoke-virtual {p0, v0, v1}, LPe;->a(Ljava/lang/Class;Laou;)V

    .line 108
    const-class v0, LPm;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LPe;->a:LZb;

    invoke-virtual {p0, v0, v1}, LPe;->a(Laop;LZb;)V

    .line 109
    const-class v0, LPn;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LPe;->b:LZb;

    invoke-virtual {p0, v0, v1}, LPe;->a(Laop;LZb;)V

    .line 110
    iget-object v0, p0, LPe;->a:LZb;

    iget-object v1, p0, LPe;->a:LYD;

    iget-object v1, v1, LYD;->a:LPe;

    iget-object v1, v1, LPe;->b:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 112
    iget-object v0, p0, LPe;->b:LZb;

    new-instance v1, LPg;

    invoke-direct {v1, p0}, LPg;-><init>(LPe;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 121
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)V
    .registers 3
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, LPe;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 47
    iget-object v0, p0, LPe;->a:LYD;

    iget-object v0, v0, LYD;->a:LPe;

    iget-object v0, v0, LPe;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LPe;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPm;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LPm;

    .line 53
    iget-object v0, p0, LPe;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LPe;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWY;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LWY;

    .line 59
    iget-object v0, p0, LPe;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LPe;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LZM;

    .line 65
    iget-object v0, p0, LPe;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->f:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LPe;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LkK;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LkK;

    .line 71
    iget-object v0, p0, LPe;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LPe;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LME;

    .line 77
    iget-object v0, p0, LPe;->a:LYD;

    iget-object v0, v0, LYD;->a:LVM;

    iget-object v0, v0, LVM;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LPe;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVH;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LVH;

    .line 83
    iget-object v0, p0, LPe;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LPe;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LeQ;

    .line 89
    iget-object v0, p0, LPe;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    iget-object v0, v0, Lob;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LPe;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LnF;

    iput-object v0, p1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LnF;

    .line 95
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 125
    return-void
.end method
