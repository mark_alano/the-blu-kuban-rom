.class public LPh;
.super Les;
.source "OpenUrlActivityDelegate.java"


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:LkB;

.field final synthetic b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Ljava/lang/String;Ljava/lang/String;LkB;Landroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 217
    iput-object p1, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iput-object p2, p0, LPh;->a:Ljava/lang/String;

    iput-object p3, p0, LPh;->b:Ljava/lang/String;

    iput-object p4, p0, LPh;->a:LkB;

    iput-object p5, p0, LPh;->a:Landroid/content/Intent;

    invoke-direct {p0}, Les;-><init>()V

    return-void
.end method


# virtual methods
.method public c()V
    .registers 5

    .prologue
    .line 221
    :try_start_0
    iget-object v0, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v0, v0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LVH;

    iget-object v1, p0, LPh;->a:Ljava/lang/String;

    iget-object v2, p0, LPh;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LVH;->a(Ljava/lang/String;Ljava/lang/String;)LVV;

    move-result-object v0

    .line 222
    const-string v1, "OpenUrlActivity"

    const-string v2, "Got Entry!"

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v1, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v1, v1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LWY;

    iget-object v2, p0, LPh;->a:LkB;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, LWY;->a(LkB;LVV;Ljava/lang/Boolean;)V

    .line 224
    invoke-virtual {v0}, LVV;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LkP;->h:LkP;

    invoke-virtual {v2}, LkP;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 227
    iget-object v1, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v1, v1, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LME;

    iget-object v2, p0, LPh;->a:LkB;

    invoke-interface {v1, v2}, LME;->a(LkB;)V

    .line 232
    :cond_3a
    iget-object v1, p0, LPh;->a:Landroid/content/Intent;

    const-string v2, "resourceId"

    invoke-virtual {v0}, LVV;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    iget-object v0, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LPi;

    invoke-direct {v1, p0}, LPi;-><init>(LPh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_53
    .catch LasH; {:try_start_0 .. :try_end_53} :catch_54
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_53} :catch_67
    .catch Latc; {:try_start_0 .. :try_end_53} :catch_7a
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_53} :catch_8d

    .line 258
    :goto_53
    return-void

    .line 239
    :catch_54
    move-exception v0

    .line 240
    iget-object v1, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v2, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Len;->open_url_authentication_error:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_53

    .line 241
    :catch_67
    move-exception v0

    .line 242
    iget-object v1, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v2, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Len;->open_url_io_error:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_53

    .line 243
    :catch_7a
    move-exception v0

    .line 247
    iget-object v1, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    iget-object v2, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Len;->open_url_authentication_error:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_53

    .line 248
    :catch_8d
    move-exception v0

    .line 249
    const-string v1, "OpenUrlActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception processing entry: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iget-object v0, p0, LPh;->a:Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;

    invoke-static {v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LPj;

    invoke-direct {v1, p0}, LPj;-><init>(LPh;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_53
.end method
