.class public final enum LPv;
.super Ljava/lang/Enum;
.source "AclTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPv;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPv;

.field private static final synthetic a:[LPv;

.field public static final enum b:LPv;

.field public static final enum c:LPv;

.field public static final enum d:LPv;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0xe

    .line 45
    new-instance v0, LPv;

    const-string v1, "RESOURCE_ID"

    invoke-static {}, LPu;->b()LPu;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "resourceId"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->a()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LPv;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPv;->a:LPv;

    .line 48
    new-instance v0, LPv;

    const-string v1, "ACCOUNT_NAME"

    invoke-static {}, LPu;->b()LPu;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "accountName"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LPv;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPv;->b:LPv;

    .line 51
    new-instance v0, LPv;

    const-string v1, "ROLE"

    invoke-static {}, LPu;->b()LPu;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "role"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LPv;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPv;->c:LPv;

    .line 54
    new-instance v0, LPv;

    const-string v1, "SCOPE"

    invoke-static {}, LPu;->b()LPu;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "scope"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LPv;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPv;->d:LPv;

    .line 44
    const/4 v0, 0x4

    new-array v0, v0, [LPv;

    sget-object v1, LPv;->a:LPv;

    aput-object v1, v0, v7

    sget-object v1, LPv;->b:LPv;

    aput-object v1, v0, v8

    sget-object v1, LPv;->c:LPv;

    aput-object v1, v0, v9

    sget-object v1, LPv;->d:LPv;

    aput-object v1, v0, v10

    sput-object v0, LPv;->a:[LPv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPv;->a:LPI;

    .line 62
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPv;
    .registers 2
    .parameter

    .prologue
    .line 44
    const-class v0, LPv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPv;

    return-object v0
.end method

.method public static values()[LPv;
    .registers 1

    .prologue
    .line 44
    sget-object v0, LPv;->a:[LPv;

    invoke-virtual {v0}, [LPv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPv;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, LPv;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 44
    invoke-virtual {p0}, LPv;->a()LPI;

    move-result-object v0

    return-object v0
.end method
