.class public final enum LPz;
.super Ljava/lang/Enum;
.source "CacheListTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LPz;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LPz;

.field private static final synthetic a:[LPz;

.field public static final enum b:LPz;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 42
    new-instance v0, LPz;

    const-string v1, "APP_ID"

    invoke-static {}, LPy;->b()LPy;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "appId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->a()LQa;

    move-result-object v3

    invoke-static {}, LPw;->a()LPw;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, LPz;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPz;->a:LPz;

    .line 47
    new-instance v0, LPz;

    const-string v1, "CONTENT_ID"

    invoke-static {}, LPy;->b()LPy;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "contentId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-static {}, LPS;->a()LPS;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LPz;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LPz;->b:LPz;

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [LPz;

    sget-object v1, LPz;->a:LPz;

    aput-object v1, v0, v6

    sget-object v1, LPz;->b:LPz;

    aput-object v1, v0, v7

    sput-object v0, LPz;->a:[LPz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LPz;->a:LPI;

    .line 56
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LPz;
    .registers 2
    .parameter

    .prologue
    .line 40
    const-class v0, LPz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LPz;

    return-object v0
.end method

.method public static values()[LPz;
    .registers 1

    .prologue
    .line 40
    sget-object v0, LPz;->a:[LPz;

    invoke-virtual {v0}, [LPz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LPz;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, LPz;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 40
    invoke-virtual {p0}, LPz;->a()LPI;

    move-result-object v0

    return-object v0
.end method
