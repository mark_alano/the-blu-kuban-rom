.class public LQF;
.super LQK;
.source "InterceptingPunchWebViewTouchListener.java"


# instance fields
.field private final a:LMw;

.field private final a:LQH;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LRb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LQR;LRC;Laoz;LeQ;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LQR;",
            "LRC;",
            "Laoz",
            "<",
            "LRb;",
            ">;",
            "LeQ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p5}, LQK;-><init>(Landroid/content/Context;LQR;LRC;LeQ;)V

    .line 44
    new-instance v0, LQH;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LQH;-><init>(LQG;)V

    iput-object v0, p0, LQF;->a:LQH;

    .line 52
    new-instance v0, LMw;

    iget-object v1, p0, LQF;->a:LQH;

    invoke-direct {v0, p1, v1}, LMw;-><init>(Landroid/content/Context;LMz;)V

    iput-object v0, p0, LQF;->a:LMw;

    .line 53
    iput-object p4, p0, LQF;->a:Laoz;

    .line 54
    return-void
.end method


# virtual methods
.method protected a(I)V
    .registers 6
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, LQF;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRb;

    .line 75
    if-gez p1, :cond_17

    .line 76
    iget-object v1, p0, LQF;->a:LeQ;

    const-string v2, "punch"

    const-string v3, "webViewPunchSlideSwipeNext"

    invoke-virtual {v1, v2, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-interface {v0}, LRb;->m_()V

    .line 84
    :goto_16
    return-void

    .line 80
    :cond_17
    iget-object v1, p0, LQF;->a:LeQ;

    const-string v2, "punch"

    const-string v3, "webViewPunchSlideSwipePrevious"

    invoke-virtual {v1, v2, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-interface {v0}, LRb;->b()V

    goto :goto_16
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 58
    invoke-super {p0, p1, p2}, LQK;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 61
    iget-object v0, p0, LQF;->a:LQH;

    iput-object p1, v0, LQH;->a:Landroid/view/View;

    .line 63
    :try_start_8
    iget-object v0, p0, LQF;->a:LMw;

    invoke-virtual {v0, p2}, LMw;->a(Landroid/view/MotionEvent;)Z
    :try_end_d
    .catchall {:try_start_8 .. :try_end_d} :catchall_13

    .line 65
    iget-object v0, p0, LQF;->a:LQH;

    iput-object v2, v0, LQH;->a:Landroid/view/View;

    .line 68
    const/4 v0, 0x1

    return v0

    .line 65
    :catchall_13
    move-exception v0

    iget-object v1, p0, LQF;->a:LQH;

    iput-object v2, v1, LQH;->a:Landroid/view/View;

    throw v0
.end method
