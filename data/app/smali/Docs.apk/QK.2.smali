.class public LQK;
.super Ljava/lang/Object;
.source "PassThroughPunchWebViewTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:LMB;

.field private final a:LQM;

.field private final a:LRC;

.field private final a:Lacq;

.field protected final a:LeQ;


# direct methods
.method protected constructor <init>(Landroid/content/Context;LQR;LRC;LeQ;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, LQM;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LQM;-><init>(LQK;LQL;)V

    iput-object v0, p0, LQK;->a:LQM;

    .line 81
    new-instance v0, LMB;

    iget-object v1, p0, LQK;->a:LQM;

    invoke-direct {v0, p1, v1}, LMB;-><init>(Landroid/content/Context;LMC;)V

    iput-object v0, p0, LQK;->a:LMB;

    .line 83
    iput-object p3, p0, LQK;->a:LRC;

    .line 84
    iput-object p4, p0, LQK;->a:LeQ;

    .line 86
    new-instance v0, Lacq;

    new-instance v1, LQL;

    invoke-direct {v1, p0, p3, p2}, LQL;-><init>(LQK;LRC;LQR;)V

    invoke-direct {v0, v1}, Lacq;-><init>(Lacr;)V

    iput-object v0, p0, LQK;->a:Lacq;

    .line 112
    return-void
.end method

.method static synthetic a(LQK;)LQM;
    .registers 2
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, LQK;->a:LQM;

    return-object v0
.end method

.method static synthetic a(LQK;)LRC;
    .registers 2
    .parameter

    .prologue
    .line 22
    iget-object v0, p0, LQK;->a:LRC;

    return-object v0
.end method


# virtual methods
.method protected a(I)V
    .registers 2
    .parameter

    .prologue
    .line 130
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 116
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 118
    iget-object v0, p0, LQK;->a:LMB;

    invoke-virtual {v0, p2}, LMB;->a(Landroid/view/MotionEvent;)Z

    .line 119
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 120
    and-int/lit16 v0, v0, 0xff

    .line 122
    iget-object v0, p0, LQK;->a:LMB;

    invoke-virtual {v0}, LMB;->a()Z

    move-result v0

    if-nez v0, :cond_25

    iget-object v0, p0, LQK;->a:LQM;

    iget-boolean v0, v0, LQM;->a:Z

    if-nez v0, :cond_25

    .line 123
    iget-object v0, p0, LQK;->a:Lacq;

    invoke-virtual {v0, p2}, Lacq;->a(Landroid/view/MotionEvent;)V

    .line 126
    :cond_25
    const/4 v0, 0x0

    return v0
.end method
