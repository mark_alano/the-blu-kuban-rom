.class LQM;
.super Ljava/lang/Object;
.source "PassThroughPunchWebViewTouchListener.java"

# interfaces
.implements LMC;


# instance fields
.field final synthetic a:LQK;

.field a:Z


# direct methods
.method private constructor <init>(LQK;)V
    .registers 3
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, LQM;->a:LQK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, LQM;->a:Z

    return-void
.end method

.method synthetic constructor <init>(LQK;LQL;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1}, LQM;-><init>(LQK;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, LQM;->a:Z

    .line 30
    return-void
.end method

.method public a(LMB;)V
    .registers 4
    .parameter

    .prologue
    .line 40
    const-string v0, "BasePunchWebViewTouchHandler"

    const-string v1, "onScaleEnd()"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    iget-object v0, p0, LQM;->a:LQK;

    invoke-static {v0}, LQK;->a(LQK;)LQM;

    move-result-object v0

    invoke-virtual {v0}, LQM;->a()V

    .line 42
    return-void
.end method

.method public a(LMB;)Z
    .registers 4
    .parameter

    .prologue
    .line 34
    const-string v0, "BasePunchWebViewTouchHandler"

    const-string v1, "onScaleBegin()"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public b(LMB;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 46
    iget-boolean v0, p0, LQM;->a:Z

    if-eqz v0, :cond_6

    .line 66
    :cond_5
    :goto_5
    return v4

    .line 50
    :cond_6
    invoke-virtual {p1}, LMB;->c()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3ff199999999999aL

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2b

    .line 52
    iget-object v0, p0, LQM;->a:LQK;

    iget-object v0, v0, LQK;->a:LeQ;

    const-string v1, "punch"

    const-string v2, "webViewPunchFullscreenPinchOpen"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, LQM;->a:LQK;

    invoke-static {v0}, LQK;->a(LQK;)LRC;

    move-result-object v0

    invoke-interface {v0, v4}, LRC;->a(Z)V

    .line 56
    iput-boolean v4, p0, LQM;->a:Z

    goto :goto_5

    .line 57
    :cond_2b
    invoke-virtual {p1}, LMB;->c()F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3feccccccccccccdL

    cmpg-double v0, v0, v2

    if-gez v0, :cond_5

    .line 59
    iget-object v0, p0, LQM;->a:LQK;

    iget-object v0, v0, LQK;->a:LeQ;

    const-string v1, "punch"

    const-string v2, "webViewPunchFullscreenPinchClose"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, LQM;->a:LQK;

    invoke-static {v0}, LQK;->a(LQK;)LRC;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LRC;->a(Z)V

    .line 63
    iput-boolean v4, p0, LQM;->a:Z

    goto :goto_5
.end method
