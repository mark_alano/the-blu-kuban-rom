.class public final enum LQj;
.super Ljava/lang/Enum;
.source "PendingOperationTable.java"

# interfaces
.implements LagF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LQj;",
        ">;",
        "LagF",
        "<",
        "LPI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LQj;

.field private static final synthetic a:[LQj;

.field public static final enum b:LQj;

.field public static final enum c:LQj;

.field public static final enum d:LQj;


# instance fields
.field private final a:LPI;


# direct methods
.method static constructor <clinit>()V
    .registers 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x15

    .line 39
    new-instance v0, LQj;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, LQi;->b()LQi;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "accountId"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-static {}, LPs;->a()LPs;

    move-result-object v4

    invoke-virtual {v3, v4}, LQa;->a(LPN;)LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->a()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, LQj;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LQj;->a:LQj;

    .line 44
    new-instance v0, LQj;

    const-string v1, "PAYLOAD"

    invoke-static {}, LQi;->b()LQi;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "payload"

    sget-object v5, LQc;->c:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->a()LQa;

    move-result-object v3

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, LQj;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LQj;->b:LQj;

    .line 47
    new-instance v0, LQj;

    const-string v1, "TIMESTAMP"

    invoke-static {}, LQi;->b()LQi;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "timestamp"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, LQj;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LQj;->c:LQj;

    .line 50
    new-instance v0, LQj;

    const-string v1, "RETRY_COUNT"

    invoke-static {}, LQi;->b()LQi;

    move-result-object v2

    invoke-static {v2}, LPK;->a(LPN;)LPK;

    move-result-object v2

    new-instance v3, LQa;

    const-string v4, "retryCount"

    sget-object v5, LQc;->a:LQc;

    invoke-direct {v3, v4, v5}, LQa;-><init>(Ljava/lang/String;LQc;)V

    invoke-virtual {v3}, LQa;->b()LQa;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, LPK;->a(ILQa;)LPK;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, LQj;-><init>(Ljava/lang/String;ILPK;)V

    sput-object v0, LQj;->d:LQj;

    .line 38
    const/4 v0, 0x4

    new-array v0, v0, [LQj;

    sget-object v1, LQj;->a:LQj;

    aput-object v1, v0, v7

    sget-object v1, LQj;->b:LQj;

    aput-object v1, v0, v8

    sget-object v1, LQj;->c:LQj;

    aput-object v1, v0, v9

    sget-object v1, LQj;->d:LQj;

    aput-object v1, v0, v10

    sput-object v0, LQj;->a:[LQj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILPK;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LPK;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    invoke-virtual {p3}, LPK;->a()LPI;

    move-result-object v0

    iput-object v0, p0, LQj;->a:LPI;

    .line 59
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LQj;
    .registers 2
    .parameter

    .prologue
    .line 38
    const-class v0, LQj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LQj;

    return-object v0
.end method

.method public static values()[LQj;
    .registers 1

    .prologue
    .line 38
    sget-object v0, LQj;->a:[LQj;

    invoke-virtual {v0}, [LQj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LQj;

    return-object v0
.end method


# virtual methods
.method public a()LPI;
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, LQj;->a:LPI;

    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 38
    invoke-virtual {p0}, LQj;->a()LPI;

    move-result-object v0

    return-object v0
.end method
