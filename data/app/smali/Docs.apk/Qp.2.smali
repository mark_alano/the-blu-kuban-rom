.class public final LQp;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LQU;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LQR;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LQY;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LQo;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LRk;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LPm;",
            ">;"
        }
    .end annotation
.end field

.field public g:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LRm;",
            ">;"
        }
    .end annotation
.end field

.field public h:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LRd;",
            ">;"
        }
    .end annotation
.end field

.field public i:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LRJ;",
            ">;"
        }
    .end annotation
.end field

.field public j:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSf;",
            ">;"
        }
    .end annotation
.end field

.field public k:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LQV;",
            ">;"
        }
    .end annotation
.end field

.field public l:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSi;",
            ">;"
        }
    .end annotation
.end field

.field public m:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LRC;",
            ">;"
        }
    .end annotation
.end field

.field public n:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LRb;",
            ">;"
        }
    .end annotation
.end field

.field public o:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSg;",
            ">;"
        }
    .end annotation
.end field

.field public p:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LRU;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 46
    iput-object p1, p0, LQp;->a:LYD;

    .line 47
    const-class v0, LQU;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->a:LZb;

    .line 50
    const-class v0, LQR;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->b:LZb;

    .line 53
    const-class v0, LQY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->c:LZb;

    .line 56
    const-class v0, LQo;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->d:LZb;

    .line 59
    const-class v0, LRk;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->e:LZb;

    .line 62
    const-class v0, LQN;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->f:LZb;

    .line 65
    const-class v0, LRm;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->g:LZb;

    .line 68
    const-class v0, LRd;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->h:LZb;

    .line 71
    const-class v0, LRJ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->i:LZb;

    .line 74
    const-class v0, LSf;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->j:LZb;

    .line 77
    const-class v0, LQV;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->k:LZb;

    .line 80
    const-class v0, LSi;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->l:LZb;

    .line 83
    const-class v0, LRC;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->m:LZb;

    .line 86
    const-class v0, LRb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->n:LZb;

    .line 89
    const-class v0, LSg;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->o:LZb;

    .line 92
    const-class v0, LRU;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LQp;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LQp;->p:LZb;

    .line 95
    return-void
.end method

.method static synthetic a(LQp;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LQp;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 390
    const-class v0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    new-instance v1, LQq;

    invoke-direct {v1, p0}, LQq;-><init>(LQp;)V

    invoke-virtual {p0, v0, v1}, LQp;->a(Ljava/lang/Class;Laou;)V

    .line 398
    const-class v0, LRd;

    new-instance v1, LQv;

    invoke-direct {v1, p0}, LQv;-><init>(LQp;)V

    invoke-virtual {p0, v0, v1}, LQp;->a(Ljava/lang/Class;Laou;)V

    .line 406
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;

    new-instance v1, LQw;

    invoke-direct {v1, p0}, LQw;-><init>(LQp;)V

    invoke-virtual {p0, v0, v1}, LQp;->a(Ljava/lang/Class;Laou;)V

    .line 414
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;

    new-instance v1, LQx;

    invoke-direct {v1, p0}, LQx;-><init>(LQp;)V

    invoke-virtual {p0, v0, v1}, LQp;->a(Ljava/lang/Class;Laou;)V

    .line 422
    const-class v0, LRm;

    new-instance v1, LQy;

    invoke-direct {v1, p0}, LQy;-><init>(LQp;)V

    invoke-virtual {p0, v0, v1}, LQp;->a(Ljava/lang/Class;Laou;)V

    .line 430
    const-class v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    new-instance v1, LQz;

    invoke-direct {v1, p0}, LQz;-><init>(LQp;)V

    invoke-virtual {p0, v0, v1}, LQp;->a(Ljava/lang/Class;Laou;)V

    .line 438
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;

    new-instance v1, LQA;

    invoke-direct {v1, p0}, LQA;-><init>(LQp;)V

    invoke-virtual {p0, v0, v1}, LQp;->a(Ljava/lang/Class;Laou;)V

    .line 446
    const-class v0, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;

    new-instance v1, LQB;

    invoke-direct {v1, p0}, LQB;-><init>(LQp;)V

    invoke-virtual {p0, v0, v1}, LQp;->a(Ljava/lang/Class;Laou;)V

    .line 454
    const-class v0, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;

    new-instance v1, LQC;

    invoke-direct {v1, p0}, LQC;-><init>(LQp;)V

    invoke-virtual {p0, v0, v1}, LQp;->a(Ljava/lang/Class;Laou;)V

    .line 462
    const-class v0, LQU;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->a:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 463
    const-class v0, LQR;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->b:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 464
    const-class v0, LQY;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->c:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 465
    const-class v0, LQo;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->d:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 466
    const-class v0, LRk;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->e:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 467
    const-class v0, LQN;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->f:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 468
    const-class v0, LRm;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->g:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 469
    const-class v0, LRd;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->h:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 470
    const-class v0, LRJ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->i:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 471
    const-class v0, LSf;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->j:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 472
    const-class v0, LQV;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->k:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 473
    const-class v0, LSi;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->l:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 474
    const-class v0, LRC;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->m:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 475
    const-class v0, LRb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->n:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 476
    const-class v0, LSg;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->o:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 477
    const-class v0, LRU;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LQp;->p:LZb;

    invoke-virtual {p0, v0, v1}, LQp;->a(Laop;LZb;)V

    .line 478
    iget-object v0, p0, LQp;->a:LZb;

    iget-object v1, p0, LQp;->a:LYD;

    iget-object v1, v1, LYD;->a:LQp;

    iget-object v1, v1, LQp;->i:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 480
    iget-object v0, p0, LQp;->b:LZb;

    iget-object v1, p0, LQp;->a:LYD;

    iget-object v1, v1, LYD;->a:LQp;

    iget-object v1, v1, LQp;->c:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 482
    iget-object v0, p0, LQp;->c:LZb;

    iget-object v1, p0, LQp;->a:LYD;

    iget-object v1, v1, LYD;->a:LQp;

    iget-object v1, v1, LQp;->k:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 484
    iget-object v0, p0, LQp;->d:LZb;

    iget-object v1, p0, LQp;->a:LYD;

    iget-object v1, v1, LYD;->a:LQp;

    iget-object v1, v1, LQp;->l:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 486
    iget-object v0, p0, LQp;->e:LZb;

    iget-object v1, p0, LQp;->a:LYD;

    iget-object v1, v1, LYD;->a:LQp;

    iget-object v1, v1, LQp;->g:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 488
    iget-object v0, p0, LQp;->f:LZb;

    iget-object v1, p0, LQp;->a:LYD;

    iget-object v1, v1, LYD;->a:LQp;

    iget-object v1, v1, LQp;->j:LZb;

    invoke-static {v1}, LQp;->a(Laoz;)Laoz;

    move-result-object v1

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 491
    iget-object v0, p0, LQp;->g:LZb;

    new-instance v1, LQr;

    invoke-direct {v1, p0}, LQr;-><init>(LQp;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 517
    iget-object v0, p0, LQp;->h:LZb;

    new-instance v1, LQs;

    invoke-direct {v1, p0}, LQs;-><init>(LQp;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 528
    iget-object v0, p0, LQp;->i:LZb;

    new-instance v1, LQt;

    invoke-direct {v1, p0}, LQt;-><init>(LQp;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 537
    iget-object v0, p0, LQp;->j:LZb;

    new-instance v1, LQu;

    invoke-direct {v1, p0}, LQu;-><init>(LQp;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 551
    return-void
.end method

.method public a(LRd;)V
    .registers 3
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LNE;

    iget-object v0, v0, LNE;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNS;

    iput-object v0, p1, LRd;->a:LNS;

    .line 113
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->n:LZb;

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, LRd;->a:Laoz;

    .line 119
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQV;

    iput-object v0, p1, LRd;->a:LQV;

    .line 125
    return-void
.end method

.method public a(LRm;)V
    .registers 3
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, LRm;->a:I

    .line 193
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 325
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LeQ;

    .line 331
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQo;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQo;

    .line 337
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRk;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LRk;

    .line 343
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQR;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LQR;

    .line 349
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->a:LKS;

    .line 355
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    invoke-virtual {v0, p1}, LQp;->a(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;)V

    .line 103
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 197
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    invoke-virtual {v0, p1}, LQp;->a(Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;)V

    .line 199
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 203
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->m:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRC;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LRC;

    .line 209
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->n:LZb;

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->b:Laoz;

    .line 215
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LKS;

    .line 221
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQR;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LQR;

    .line 227
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->o:LZb;

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:Laoz;

    .line 233
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchFullScreenFragment;->a:LZM;

    .line 239
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V
    .registers 3
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQR;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQR;

    .line 165
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->n:LZb;

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Laoz;

    .line 171
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LeQ;

    .line 177
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->m:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRC;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LRC;

    .line 183
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 129
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 131
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LeQ;

    .line 137
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQU;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQU;

    .line 143
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LKS;

    .line 149
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->f:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQN;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewActivity;->a:LQN;

    .line 155
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 243
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LKS;

    .line 249
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:Ljava/lang/Class;

    .line 255
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQV;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LQV;

    .line 261
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LeQ;

    .line 267
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LNl;

    iget-object v0, v0, LNl;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNe;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNe;

    .line 273
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->m:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRC;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRC;

    .line 279
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->g:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZS;

    .line 285
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LPe;

    iget-object v0, v0, LPe;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPm;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LPm;

    .line 291
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LZM;

    .line 297
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->p:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRU;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRU;

    .line 303
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->h:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRd;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LRd;

    .line 309
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->b:LKS;

    .line 315
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LNE;

    iget-object v0, v0, LNE;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNS;

    iput-object v0, p1, Lcom/google/android/apps/docs/punchwebview/PunchWebViewFragment;->a:LNS;

    .line 321
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 555
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    invoke-virtual {v0}, LQp;->c()V

    .line 557
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    invoke-virtual {v0}, LQp;->d()V

    .line 559
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 358
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LdR;

    iget-object v0, v0, LdR;->a:LZb;

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    sput-object v0, LQk;->b:Laoz;

    .line 364
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    sput-object v0, LQk;->a:Laoz;

    .line 370
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 373
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    sput-object v0, LQl;->a:Laoz;

    .line 379
    iget-object v0, p0, LQp;->a:LYD;

    iget-object v0, v0, LYD;->a:LdR;

    iget-object v0, v0, LdR;->a:LZb;

    invoke-static {v0}, LQp;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    sput-object v0, LQl;->b:Laoz;

    .line 385
    return-void
.end method
