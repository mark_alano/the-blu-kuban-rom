.class public LRF;
.super Ljava/lang/Object;
.source "PunchUiModelImpl.java"

# interfaces
.implements LRC;


# instance fields
.field private a:LRE;

.field private a:LRy;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LRD;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, LRF;->a:Ljava/util/Set;

    .line 18
    sget-object v0, LRE;->a:LRE;

    iput-object v0, p0, LRF;->a:LRE;

    .line 20
    sget-object v0, LRy;->a:LRy;

    iput-object v0, p0, LRF;->a:LRy;

    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    .line 109
    iget-object v0, p0, LRF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRD;

    .line 110
    invoke-interface {v0}, LRD;->a()V

    goto :goto_6

    .line 112
    :cond_16
    return-void
.end method

.method private b()V
    .registers 3

    .prologue
    .line 118
    iget-object v0, p0, LRF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRD;

    .line 119
    invoke-interface {v0}, LRD;->c()V

    goto :goto_6

    .line 121
    :cond_16
    return-void
.end method

.method private c()V
    .registers 3

    .prologue
    .line 127
    iget-object v0, p0, LRF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRD;

    .line 128
    invoke-interface {v0}, LRD;->e()V

    goto :goto_6

    .line 130
    :cond_16
    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 136
    iget-object v0, p0, LRF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRD;

    .line 137
    invoke-interface {v0}, LRD;->g()V

    goto :goto_6

    .line 139
    :cond_16
    return-void
.end method

.method private e()V
    .registers 3

    .prologue
    .line 145
    iget-object v0, p0, LRF;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LRD;

    .line 146
    invoke-interface {v0}, LRD;->i()V

    goto :goto_6

    .line 148
    :cond_16
    return-void
.end method


# virtual methods
.method public a()LRE;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, LRF;->a:LRE;

    return-object v0
.end method

.method public a()LRy;
    .registers 2

    .prologue
    .line 76
    iget-object v0, p0, LRF;->a:LRy;

    return-object v0
.end method

.method public a(LRD;)V
    .registers 3
    .parameter

    .prologue
    .line 26
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    iget-object v0, p0, LRF;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    const/4 v0, 0x1

    :goto_c
    invoke-static {v0}, Lagu;->b(Z)V

    .line 28
    iget-object v0, p0, LRF;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 29
    return-void

    .line 27
    :cond_15
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public a(LRE;)V
    .registers 3
    .parameter

    .prologue
    .line 40
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, LRF;->a:LRE;

    if-eq v0, p1, :cond_c

    .line 42
    iput-object p1, p0, LRF;->a:LRE;

    .line 43
    invoke-direct {p0}, LRF;->a()V

    .line 45
    :cond_c
    return-void
.end method

.method public a(LRy;)V
    .registers 3
    .parameter

    .prologue
    .line 67
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object v0, p0, LRF;->a:LRy;

    invoke-virtual {v0, p1}, LRy;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 69
    iput-object p1, p0, LRF;->a:LRy;

    .line 70
    invoke-direct {p0}, LRF;->c()V

    .line 72
    :cond_10
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 54
    iget-boolean v0, p0, LRF;->a:Z

    if-eq v0, p1, :cond_9

    .line 55
    iput-boolean p1, p0, LRF;->a:Z

    .line 56
    invoke-direct {p0}, LRF;->b()V

    .line 58
    :cond_9
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 62
    iget-boolean v0, p0, LRF;->a:Z

    return v0
.end method

.method public b(LRD;)V
    .registers 3
    .parameter

    .prologue
    .line 33
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    iget-object v0, p0, LRF;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 35
    iget-object v0, p0, LRF;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 36
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 81
    iget-boolean v0, p0, LRF;->b:Z

    if-eq v0, p1, :cond_9

    .line 82
    iput-boolean p1, p0, LRF;->b:Z

    .line 83
    invoke-direct {p0}, LRF;->d()V

    .line 85
    :cond_9
    return-void
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 89
    iget-boolean v0, p0, LRF;->b:Z

    return v0
.end method

.method public c(Z)V
    .registers 3
    .parameter

    .prologue
    .line 94
    iget-boolean v0, p0, LRF;->c:Z

    if-eq v0, p1, :cond_9

    .line 95
    iput-boolean p1, p0, LRF;->c:Z

    .line 96
    invoke-direct {p0}, LRF;->e()V

    .line 98
    :cond_9
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 102
    iget-boolean v0, p0, LRF;->c:Z

    return v0
.end method
