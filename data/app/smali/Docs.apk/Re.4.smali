.class LRe;
.super Ljava/lang/Object;
.source "PunchSvgViewer.java"

# interfaces
.implements LQT;


# instance fields
.field final synthetic a:I

.field final synthetic a:LRd;


# direct methods
.method constructor <init>(LRd;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, LRe;->a:LRd;

    iput p2, p0, LRe;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 5

    .prologue
    .line 148
    const-string v0, "PunchSvgViewer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSlideContentRequestFailed: slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRe;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-object v0, p0, LRe;->a:LRd;

    invoke-static {v0}, LRd;->a(LRd;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LRf;

    invoke-direct {v1, p0}, LRf;-><init>(LRe;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 155
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 137
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string v0, "PunchSvgViewer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received content: slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRe;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-object v0, p0, LRe;->a:LRd;

    invoke-static {v0}, LRd;->a(LRd;)I

    move-result v0

    iget v1, p0, LRe;->a:I

    if-ne v0, v1, :cond_2f

    iget-object v0, p0, LRe;->a:LRd;

    invoke-static {v0}, LRd;->a(LRd;)Landroid/webkit/WebView;

    move-result-object v0

    if-nez v0, :cond_30

    .line 144
    :cond_2f
    :goto_2f
    return-void

    .line 143
    :cond_30
    iget-object v0, p0, LRe;->a:LRd;

    invoke-static {v0, p1, p2}, LRd;->a(LRd;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2f
.end method
