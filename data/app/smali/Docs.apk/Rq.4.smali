.class LRq;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"


# instance fields
.field final synthetic a:I

.field final synthetic a:LRo;

.field final synthetic a:LRp;


# direct methods
.method constructor <init>(LRp;LRo;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 137
    iput-object p1, p0, LRq;->a:LRp;

    iput-object p2, p0, LRq;->a:LRo;

    iput p3, p0, LRq;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoadImage()V
    .registers 4
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, LRq;->a:LRp;

    iget-object v0, v0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 142
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRq;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " img.onload(): slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LRq;->a:LRp;

    invoke-static {v2}, LRp;->a(LRp;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-object v0, p0, LRq;->a:LRp;

    invoke-static {v0}, LRp;->a(LRp;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 145
    iget-object v0, p0, LRq;->a:LRp;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LRp;->a(LRp;Z)Z

    .line 146
    iget-object v0, p0, LRq;->a:LRp;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LRp;->b(LRp;Z)Z

    .line 148
    :cond_49
    return-void
.end method
