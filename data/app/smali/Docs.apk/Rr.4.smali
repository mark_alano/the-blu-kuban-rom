.class LRr;
.super Ljava/lang/Object;
.source "PunchThumbnailGeneratorFactoryImpl.java"

# interfaces
.implements Landroid/webkit/WebView$PictureListener;


# instance fields
.field final synthetic a:I

.field final synthetic a:LRo;

.field final synthetic a:LRp;


# direct methods
.method constructor <init>(LRp;LRo;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 152
    iput-object p1, p0, LRr;->a:LRp;

    iput-object p2, p0, LRr;->a:LRo;

    iput p3, p0, LRr;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewPicture(Landroid/webkit/WebView;Landroid/graphics/Picture;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 155
    iget-object v0, p0, LRr;->a:LRp;

    iget-object v0, v0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 156
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRr;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onNewPicture(): slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LRr;->a:LRp;

    invoke-static {v2}, LRp;->a(LRp;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " imageLoaded="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LRr;->a:LRp;

    invoke-static {v2}, LRp;->b(LRp;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " publishOnPageFinished="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LRr;->a:LRp;

    invoke-static {v2}, LRp;->c(LRp;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v0, p0, LRr;->a:LRp;

    invoke-static {v0}, LRp;->b(LRp;)Z

    move-result v0

    if-nez v0, :cond_90

    iget-object v0, p0, LRr;->a:LRp;

    invoke-static {v0}, LRp;->c(LRp;)Z

    move-result v0

    if-eqz v0, :cond_90

    .line 160
    const-string v0, "PunchThumbnailGeneratorFactoryImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "t="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LRr;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " aborting onNewPicture() for slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LRr;->a:LRp;

    invoke-static {v2}, LRp;->a(LRp;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :goto_8f
    return-void

    .line 164
    :cond_90
    iget-object v0, p0, LRr;->a:LRp;

    iget-object v0, v0, LRp;->a:LRo;

    invoke-static {v0}, LRo;->a(LRo;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, LRs;

    invoke-direct {v1, p0}, LRs;-><init>(LRr;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_8f
.end method
