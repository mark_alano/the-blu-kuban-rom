.class public LSH;
.super Ljava/lang/Object;
.source "ExtraStreamDataSource.java"

# interfaces
.implements LSG;


# instance fields
.field private final a:LSQ;

.field private final a:LSR;

.field private final a:LSW;

.field private final a:Landroid/net/Uri;

.field private final a:Ljava/lang/String;

.field private final a:Z

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(LSJ;)V
    .registers 3
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {p1}, LSJ;->a(LSJ;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LSH;->a:Landroid/net/Uri;

    .line 81
    invoke-static {p1}, LSJ;->a(LSJ;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LSH;->a:Ljava/lang/String;

    .line 82
    invoke-static {p1}, LSJ;->a(LSJ;)Z

    move-result v0

    iput-boolean v0, p0, LSH;->a:Z

    .line 83
    invoke-static {p1}, LSJ;->b(LSJ;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LSH;->b:Ljava/lang/String;

    .line 84
    invoke-static {p1}, LSJ;->a(LSJ;)LSW;

    move-result-object v0

    iput-object v0, p0, LSH;->a:LSW;

    .line 85
    invoke-static {p1}, LSJ;->a(LSJ;)LSR;

    move-result-object v0

    iput-object v0, p0, LSH;->a:LSR;

    .line 86
    invoke-static {p1}, LSJ;->a(LSJ;)LSQ;

    move-result-object v0

    iput-object v0, p0, LSH;->a:LSQ;

    .line 87
    return-void
.end method

.method synthetic constructor <init>(LSJ;LSI;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-direct {p0, p1}, LSH;-><init>(LSJ;)V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/graphics/Bitmap;
    .registers 5
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, LSH;->a:LSW;

    iget-object v1, p0, LSH;->a:Landroid/net/Uri;

    invoke-virtual {p0}, LSH;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LSW;->a(Landroid/net/Uri;ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, LSH;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(LrM;)LrK;
    .registers 5
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, LSH;->a:Landroid/net/Uri;

    invoke-virtual {p0}, LSH;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LrM;->a(Landroid/net/Uri;Ljava/lang/String;)LrM;

    move-result-object v0

    iget-object v1, p0, LSH;->a:LSW;

    iget-object v2, p0, LSH;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2}, LSW;->a(Landroid/net/Uri;)I

    move-result v1

    invoke-virtual {v0, v1}, LrM;->a(I)LrM;

    .line 125
    invoke-virtual {p1}, LrM;->a()LrK;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    .line 106
    iget-boolean v0, p0, LSH;->a:Z

    if-nez v0, :cond_5

    .line 110
    :goto_4
    return-void

    .line 109
    :cond_5
    iget-object v0, p0, LSH;->a:LSR;

    iget-object v1, p0, LSH;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, LSR;->a(Landroid/net/Uri;)V

    goto :goto_4
.end method

.method public b()Ljava/lang/String;
    .registers 5

    .prologue
    .line 118
    iget-object v0, p0, LSH;->a:LSQ;

    iget-object v1, p0, LSH;->a:Landroid/net/Uri;

    iget-object v2, p0, LSH;->b:Ljava/lang/String;

    iget-object v3, p0, LSH;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LSQ;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
