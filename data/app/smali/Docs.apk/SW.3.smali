.class public LSW;
.super Ljava/lang/Object;
.source "ThumbnailGenerator.java"


# instance fields
.field private final a:LSP;

.field private final a:LZv;

.field private final a:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(LSP;Landroid/content/ContentResolver;LZv;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSP;

    iput-object v0, p0, LSW;->a:LSP;

    .line 38
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, LSW;->a:Landroid/content/ContentResolver;

    .line 39
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZv;

    iput-object v0, p0, LSW;->a:LZv;

    .line 40
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)I
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 106
    iget-object v0, p0, LSW;->a:LSP;

    const-string v1, "orientation"

    invoke-virtual {v0, p1, v1, v2}, LSP;->a(Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v0

    .line 108
    const-string v1, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 110
    :try_start_15
    new-instance v1, Landroid/media/ExifInterface;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 111
    const-string v2, "Orientation"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_24} :catch_32

    move-result v1

    .line 113
    packed-switch v1, :pswitch_data_34

    .line 130
    :cond_28
    :goto_28
    :pswitch_28
    return v0

    .line 115
    :pswitch_29
    add-int/lit8 v0, v0, 0x5a

    .line 116
    goto :goto_28

    .line 118
    :pswitch_2c
    add-int/lit16 v0, v0, 0xb4

    .line 119
    goto :goto_28

    .line 121
    :pswitch_2f
    add-int/lit16 v0, v0, 0x10e

    .line 122
    goto :goto_28

    .line 126
    :catch_32
    move-exception v1

    goto :goto_28

    .line 113
    :pswitch_data_34
    .packed-switch 0x3
        :pswitch_2c
        :pswitch_28
        :pswitch_28
        :pswitch_29
        :pswitch_28
        :pswitch_2f
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;ILjava/lang/String;)Landroid/graphics/Bitmap;
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v8, 0x0

    const-wide/16 v6, -0x1

    const/4 v5, 0x3

    const/4 v1, 0x0

    .line 43
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "image/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    .line 44
    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "video/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    .line 46
    if-eqz v2, :cond_31

    .line 48
    iget-object v0, p0, LSW;->a:LSP;

    const-string v2, "_id"

    invoke-virtual {v0, p1, v2, v6, v7}, LSP;->a(Landroid/net/Uri;Ljava/lang/String;J)J

    move-result-wide v2

    .line 49
    cmp-long v0, v2, v8

    if-lez v0, :cond_a1

    .line 50
    iget-object v0, p0, LSW;->a:Landroid/content/ContentResolver;

    invoke-static {v0, v2, v3, v5, v1}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_a1

    .line 98
    :goto_30
    return-object v0

    .line 57
    :cond_31
    if-eqz v0, :cond_a1

    .line 58
    invoke-virtual {p0, p1}, LSW;->a(Landroid/net/Uri;)I

    move-result v2

    .line 60
    iget-object v0, p0, LSW;->a:LSP;

    const-string v3, "_id"

    invoke-virtual {v0, p1, v3, v6, v7}, LSP;->a(Landroid/net/Uri;Ljava/lang/String;J)J

    move-result-wide v3

    .line 62
    cmp-long v0, v3, v8

    if-lez v0, :cond_58

    .line 63
    iget-object v0, p0, LSW;->a:Landroid/content/ContentResolver;

    invoke-static {v0, v3, v4, v5, v1}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_58

    .line 66
    invoke-static {v0}, LZu;->a(Landroid/graphics/Bitmap;)LZu;

    move-result-object v0

    invoke-virtual {v0, v2}, LZu;->a(I)LZu;

    move-result-object v0

    invoke-virtual {v0}, LZu;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_30

    .line 77
    :cond_58
    :try_start_58
    iget-object v0, p0, LSW;->a:LZv;

    iget-object v3, p0, LSW;->a:Landroid/content/ContentResolver;

    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    invoke-interface {v0, v3}, LZv;->a(Ljava/io/InputStream;)LZw;

    move-result-object v3

    .line 78
    const/4 v0, 0x1

    .line 80
    :goto_65
    invoke-virtual {v3}, LZw;->a()I

    move-result v4

    mul-int/lit8 v5, v0, 0x2

    div-int/2addr v4, v5

    if-lt v4, p2, :cond_77

    invoke-virtual {v3}, LZw;->b()I

    move-result v4

    mul-int/lit8 v5, v0, 0x2

    div-int/2addr v4, v5

    if-ge v4, p2, :cond_8a

    .line 86
    :cond_77
    iget-object v3, p0, LSW;->a:Landroid/content/ContentResolver;

    invoke-virtual {v3, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v3

    invoke-static {v3, v0}, LZu;->a(Ljava/io/InputStream;I)LZu;

    move-result-object v0

    invoke-virtual {v0, v2}, LZu;->a(I)LZu;

    move-result-object v0

    invoke-virtual {v0}, LZu;->a()Landroid/graphics/Bitmap;
    :try_end_88
    .catch Ljava/io/FileNotFoundException; {:try_start_58 .. :try_end_88} :catch_8d
    .catch Ljava/lang/IllegalArgumentException; {:try_start_58 .. :try_end_88} :catch_97

    move-result-object v0

    goto :goto_30

    .line 84
    :cond_8a
    mul-int/lit8 v0, v0, 0x2

    goto :goto_65

    .line 90
    :catch_8d
    move-exception v0

    .line 91
    const-string v2, "ThumbnailGenerator"

    const-string v3, "Could not open image for thumbnail creation"

    invoke-static {v2, v3, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 92
    goto :goto_30

    .line 93
    :catch_97
    move-exception v0

    .line 94
    const-string v2, "ThumbnailGenerator"

    const-string v3, "Could not open image for thumbnail creation"

    invoke-static {v2, v3, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 95
    goto :goto_30

    :cond_a1
    move-object v0, v1

    .line 98
    goto :goto_30
.end method
