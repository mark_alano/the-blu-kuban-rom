.class public final enum LSh;
.super Ljava/lang/Enum;
.source "PunchWebViewSupportLevel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LSh;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LSh;

.field private static final synthetic a:[LSh;

.field public static final enum b:LSh;

.field public static final enum c:LSh;

.field public static final enum d:LSh;


# instance fields
.field private final a:LRy;

.field private final a:Z

.field private final b:LRy;

.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, LSh;

    const-string v1, "NO_SVG"

    sget-object v3, LRy;->b:LRy;

    sget-object v5, LRy;->b:LRy;

    move v4, v2

    move v6, v2

    invoke-direct/range {v0 .. v6}, LSh;-><init>(Ljava/lang/String;ILRy;ZLRy;Z)V

    sput-object v0, LSh;->a:LSh;

    .line 23
    new-instance v3, LSh;

    const-string v4, "HARDWARE_UNUSABLE"

    sget-object v6, LRy;->a:LRy;

    sget-object v8, LRy;->b:LRy;

    move v5, v10

    move v7, v2

    move v9, v2

    invoke-direct/range {v3 .. v9}, LSh;-><init>(Ljava/lang/String;ILRy;ZLRy;Z)V

    sput-object v3, LSh;->b:LSh;

    .line 27
    new-instance v3, LSh;

    const-string v4, "BUGGY_SUPPORT"

    sget-object v6, LRy;->a:LRy;

    sget-object v8, LRy;->b:LRy;

    move v5, v11

    move v7, v2

    move v9, v10

    invoke-direct/range {v3 .. v9}, LSh;-><init>(Ljava/lang/String;ILRy;ZLRy;Z)V

    sput-object v3, LSh;->c:LSh;

    .line 30
    new-instance v3, LSh;

    const-string v4, "FULL_SUPPORT"

    sget-object v6, LRy;->a:LRy;

    sget-object v8, LRy;->b:LRy;

    move v5, v12

    move v7, v10

    move v9, v10

    invoke-direct/range {v3 .. v9}, LSh;-><init>(Ljava/lang/String;ILRy;ZLRy;Z)V

    sput-object v3, LSh;->d:LSh;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [LSh;

    sget-object v1, LSh;->a:LSh;

    aput-object v1, v0, v2

    sget-object v1, LSh;->b:LSh;

    aput-object v1, v0, v10

    sget-object v1, LSh;->c:LSh;

    aput-object v1, v0, v11

    sget-object v1, LSh;->d:LSh;

    aput-object v1, v0, v12

    sput-object v0, LSh;->a:[LSh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILRy;ZLRy;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LRy;",
            "Z",
            "LRy;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput-object p3, p0, LSh;->a:LRy;

    .line 49
    iput-boolean p4, p0, LSh;->a:Z

    .line 50
    iput-object p5, p0, LSh;->b:LRy;

    .line 51
    iput-boolean p6, p0, LSh;->b:Z

    .line 52
    return-void
.end method

.method public static a(LKS;)LSh;
    .registers 4
    .parameter

    .prologue
    .line 58
    const-string v0, "punchWebViewBuggySupportMinVersion"

    const/16 v1, 0xb

    invoke-interface {p0, v0, v1}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    .line 61
    const-string v1, "punchWebViewFullSupportMinVersion"

    const/16 v2, 0x10

    invoke-interface {p0, v1, v2}, LKS;->a(Ljava/lang/String;I)I

    move-result v1

    .line 65
    invoke-static {}, LZL;->b()Z

    move-result v2

    if-nez v2, :cond_19

    .line 66
    sget-object v0, LSh;->a:LSh;

    .line 72
    :goto_18
    return-object v0

    .line 67
    :cond_19
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v0, :cond_20

    .line 68
    sget-object v0, LSh;->b:LSh;

    goto :goto_18

    .line 69
    :cond_20
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v1, :cond_27

    .line 70
    sget-object v0, LSh;->c:LSh;

    goto :goto_18

    .line 72
    :cond_27
    sget-object v0, LSh;->d:LSh;

    goto :goto_18
.end method

.method public static valueOf(Ljava/lang/String;)LSh;
    .registers 2
    .parameter

    .prologue
    .line 15
    const-class v0, LSh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LSh;

    return-object v0
.end method

.method public static values()[LSh;
    .registers 1

    .prologue
    .line 15
    sget-object v0, LSh;->a:[LSh;

    invoke-virtual {v0}, [LSh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LSh;

    return-object v0
.end method


# virtual methods
.method public a()LRy;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, LSh;->a:LRy;

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 95
    iget-boolean v0, p0, LSh;->a:Z

    return v0
.end method

.method public b()LRy;
    .registers 2

    .prologue
    .line 88
    iget-object v0, p0, LSh;->b:LRy;

    return-object v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 102
    iget-boolean v0, p0, LSh;->b:Z

    return v0
.end method
