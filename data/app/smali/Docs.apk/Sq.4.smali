.class public final LSq;
.super Ljava/lang/Object;
.source "CachedAsyncSearchHandler.java"

# interfaces
.implements LSB;


# instance fields
.field private final a:LSD;

.field private a:LSo;

.field private final a:LWY;

.field private final a:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "LSC;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Llf;


# direct methods
.method public constructor <init>(Llf;LWY;LSD;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LSC;->a:LSC;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LSq;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, LSq;->a:LSo;

    .line 39
    const-string v0, "null loader"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p0, LSq;->a:Llf;

    .line 40
    const-string v0, "null synchronizer"

    invoke-static {p2, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWY;

    iput-object v0, p0, LSq;->a:LWY;

    .line 41
    const-string v0, "null launcher"

    invoke-static {p3, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSD;

    iput-object v0, p0, LSq;->a:LSD;

    .line 42
    return-void
.end method

.method static synthetic a(LSq;LSo;)LSo;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, LSq;->a:LSo;

    return-object p1
.end method

.method static synthetic a(LSq;)LWY;
    .registers 2
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, LSq;->a:LWY;

    return-object v0
.end method

.method static synthetic a(LSq;)Ljava/util/concurrent/atomic/AtomicReference;
    .registers 2
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, LSq;->a:Ljava/util/concurrent/atomic/AtomicReference;

    return-object v0
.end method

.method static synthetic a(LSq;)Llf;
    .registers 2
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, LSq;->a:Llf;

    return-object v0
.end method


# virtual methods
.method public a()LSC;
    .registers 2

    .prologue
    .line 129
    iget-object v0, p0, LSq;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSC;

    return-object v0
.end method

.method public a(LkB;Ljava/lang/String;J)LSF;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-virtual {p0}, LSq;->a()V

    .line 54
    if-nez p2, :cond_11

    .line 55
    invoke-static {}, LSF;->a()LSF;

    move-result-object v0

    .line 56
    iget-object v1, p0, LSq;->a:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v2, LSC;->e:LSC;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 115
    :goto_10
    return-object v0

    .line 58
    :cond_11
    iget-object v0, p0, LSq;->a:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LSC;->b:LSC;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 64
    iget-object v0, p0, LSq;->a:Llf;

    invoke-interface {v0, p1, p2, p3, p4}, Llf;->a(LkB;Ljava/lang/String;J)LkF;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, LkF;->c()J

    move-result-wide v1

    .line 67
    new-instance v3, LSr;

    invoke-direct {v3, p0, p1, v0}, LSr;-><init>(LSq;LkB;LkF;)V

    .line 111
    invoke-virtual {p1}, LkB;->b()Ljava/lang/String;

    move-result-object v0

    .line 112
    iget-object v4, p0, LSq;->a:LSD;

    invoke-interface {v4, v0, p2, v3}, LSD;->a(Ljava/lang/String;Ljava/lang/String;LSp;)LSo;

    move-result-object v0

    iput-object v0, p0, LSq;->a:LSo;

    .line 113
    invoke-static {p2, v1, v2}, LSF;->a(Ljava/lang/String;J)LSF;

    move-result-object v0

    goto :goto_10
.end method

.method public a()V
    .registers 3

    .prologue
    .line 120
    iget-object v0, p0, LSq;->a:LSo;

    if-eqz v0, :cond_c

    .line 121
    iget-object v0, p0, LSq;->a:LSo;

    invoke-interface {v0}, LSo;->a()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, LSq;->a:LSo;

    .line 124
    :cond_c
    iget-object v0, p0, LSq;->a:Ljava/util/concurrent/atomic/AtomicReference;

    sget-object v1, LSC;->d:LSC;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 125
    return-void
.end method
