.class public LTH;
.super Ljava/lang/Object;
.source "ContactInfoImpl.java"

# interfaces
.implements LTG;


# instance fields
.field private final a:J

.field private final a:Landroid/net/Uri;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:J


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/util/List;Landroid/net/Uri;J)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/net/Uri;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    if-eqz p4, :cond_b

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_24

    :cond_b
    const/4 v0, 0x1

    :goto_c
    const-string v1, "Email list should be either null or non-empty."

    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    .line 49
    iput-wide p1, p0, LTH;->a:J

    .line 50
    if-eqz p3, :cond_26

    :goto_15
    iput-object p3, p0, LTH;->a:Ljava/lang/String;

    .line 51
    if-eqz p4, :cond_29

    invoke-static {p4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_1d
    iput-object v0, p0, LTH;->a:Ljava/util/List;

    .line 52
    iput-object p5, p0, LTH;->a:Landroid/net/Uri;

    .line 53
    iput-wide p6, p0, LTH;->b:J

    .line 54
    return-void

    .line 47
    :cond_24
    const/4 v0, 0x0

    goto :goto_c

    .line 50
    :cond_26
    const-string p3, ""

    goto :goto_15

    .line 51
    :cond_29
    const/4 v0, 0x0

    goto :goto_1d
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 73
    iget-wide v0, p0, LTH;->b:J

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 83
    iget-object v0, p0, LTH;->a:Ljava/util/List;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LTH;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_5
.end method

.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, LTH;->a:Ljava/util/List;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, LTH;->a:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 94
    instance-of v1, p1, LTH;

    if-nez v1, :cond_6

    .line 101
    :cond_5
    :goto_5
    return v0

    .line 97
    :cond_6
    check-cast p1, LTH;

    .line 98
    iget-wide v1, p0, LTH;->a:J

    iget-wide v3, p1, LTH;->a:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    iget-object v1, p0, LTH;->a:Ljava/lang/String;

    iget-object v2, p1, LTH;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LTH;->a:Ljava/util/List;

    iget-object v2, p1, LTH;->a:Ljava/util/List;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LTH;->a:Landroid/net/Uri;

    iget-object v2, p1, LTH;->a:Landroid/net/Uri;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-wide v1, p0, LTH;->b:J

    iget-wide v3, p1, LTH;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 5

    .prologue
    .line 105
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, LTH;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LTH;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LTH;->a:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LTH;->a:Landroid/net/Uri;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, LTH;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 88
    const-string v0, "%s[%s, %s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LTH;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, LTH;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
