.class public final LTQ;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LTI;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LUr;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LTn;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LTl;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LUm;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LUs;",
            ">;"
        }
    .end annotation
.end field

.field public g:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LSi;",
            ">;"
        }
    .end annotation
.end field

.field public h:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LTo;",
            ">;"
        }
    .end annotation
.end field

.field public i:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LTr;",
            ">;"
        }
    .end annotation
.end field

.field public j:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LUq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 40
    iput-object p1, p0, LTQ;->a:LYD;

    .line 41
    const-class v0, LTI;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->a:LZb;

    .line 44
    const-class v0, LUr;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->b:LZb;

    .line 47
    const-class v0, LTn;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->c:LZb;

    .line 50
    const-class v0, LTl;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->d:LZb;

    .line 53
    const-class v0, LUm;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->e:LZb;

    .line 56
    const-class v0, LUs;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->f:LZb;

    .line 59
    const-class v0, LTJ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->g:LZb;

    .line 62
    const-class v0, LTo;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->h:LZb;

    .line 65
    const-class v0, LTr;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->i:LZb;

    .line 68
    const-class v0, LUq;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LTQ;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LTQ;->j:LZb;

    .line 71
    return-void
.end method

.method static synthetic a(LTQ;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LTQ;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 270
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    new-instance v1, LTR;

    invoke-direct {v1, p0}, LTR;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 278
    const-class v0, LTo;

    new-instance v1, LTX;

    invoke-direct {v1, p0}, LTX;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 286
    const-class v0, LTJ;

    new-instance v1, LTY;

    invoke-direct {v1, p0}, LTY;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 294
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;

    new-instance v1, LTZ;

    invoke-direct {v1, p0}, LTZ;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 302
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;

    new-instance v1, LUa;

    invoke-direct {v1, p0}, LUa;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 310
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;

    new-instance v1, LUb;

    invoke-direct {v1, p0}, LUb;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 318
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;

    new-instance v1, LUc;

    invoke-direct {v1, p0}, LUc;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 326
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    new-instance v1, LUd;

    invoke-direct {v1, p0}, LUd;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 334
    const-class v0, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;

    new-instance v1, LUe;

    invoke-direct {v1, p0}, LUe;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 342
    const-class v0, LUs;

    new-instance v1, LTS;

    invoke-direct {v1, p0}, LTS;-><init>(LTQ;)V

    invoke-virtual {p0, v0, v1}, LTQ;->a(Ljava/lang/Class;Laou;)V

    .line 350
    const-class v0, LTI;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->a:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 351
    const-class v0, LUr;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->b:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 352
    const-class v0, LTn;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->c:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 353
    const-class v0, LTl;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->d:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 354
    const-class v0, LUm;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->e:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 355
    const-class v0, LUs;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->f:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 356
    const-class v0, LTJ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->g:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 357
    const-class v0, LTo;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->h:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 358
    const-class v0, LTr;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->i:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 359
    const-class v0, LUq;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LTQ;->j:LZb;

    invoke-virtual {p0, v0, v1}, LTQ;->a(Laop;LZb;)V

    .line 360
    iget-object v0, p0, LTQ;->a:LZb;

    iget-object v1, p0, LTQ;->a:LYD;

    iget-object v1, v1, LYD;->a:LTQ;

    iget-object v1, v1, LTQ;->g:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 362
    iget-object v0, p0, LTQ;->b:LZb;

    iget-object v1, p0, LTQ;->a:LYD;

    iget-object v1, v1, LYD;->a:LTQ;

    iget-object v1, v1, LTQ;->f:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 364
    iget-object v0, p0, LTQ;->c:LZb;

    iget-object v1, p0, LTQ;->a:LYD;

    iget-object v1, v1, LYD;->a:LTQ;

    iget-object v1, v1, LTQ;->h:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 366
    iget-object v0, p0, LTQ;->d:LZb;

    iget-object v1, p0, LTQ;->a:LYD;

    iget-object v1, v1, LYD;->a:LTQ;

    iget-object v1, v1, LTQ;->e:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 368
    iget-object v0, p0, LTQ;->e:LZb;

    new-instance v1, LTT;

    invoke-direct {v1, p0}, LTT;-><init>(LTQ;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 382
    iget-object v0, p0, LTQ;->f:LZb;

    new-instance v1, LTU;

    invoke-direct {v1, p0}, LTU;-><init>(LTQ;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 393
    iget-object v0, p0, LTQ;->g:LZb;

    new-instance v1, LTV;

    invoke-direct {v1, p0}, LTV;-><init>(LTQ;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 404
    iget-object v0, p0, LTQ;->h:LZb;

    new-instance v1, LTW;

    invoke-direct {v1, p0}, LTW;-><init>(LTQ;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 415
    return-void
.end method

.method public a(LTJ;)V
    .registers 3
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, LTJ;->a:Landroid/content/Context;

    .line 159
    return-void
.end method

.method public a(LTo;)V
    .registers 3
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUr;

    iput-object v0, p1, LTo;->a:LUr;

    .line 131
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, LTo;->a:Llf;

    .line 137
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, LTo;->a:Laoz;

    .line 143
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXS;

    iput-object v0, p1, LTo;->a:LXS;

    .line 149
    return-void
.end method

.method public a(LUs;)V
    .registers 3
    .parameter

    .prologue
    .line 247
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p1, LUs;->a:Landroid/content/Context;

    .line 253
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTl;

    iput-object v0, p1, LUs;->a:LTl;

    .line 259
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTI;

    iput-object v0, p1, LUs;->a:LTI;

    .line 265
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 189
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUq;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUq;

    .line 195
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Llf;

    .line 201
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZj;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LZj;

    .line 207
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->i:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTr;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LTr;

    .line 213
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTI;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LTI;

    .line 219
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LKS;

    .line 225
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b:Laoz;

    .line 231
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 181
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 183
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 235
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    invoke-virtual {v0, p1}, LTQ;->a(Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;)V

    .line 237
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->b:Laoz;

    .line 243
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;)V
    .registers 3
    .parameter

    .prologue
    .line 163
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    invoke-virtual {v0, p1}, LTQ;->a(Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;)V

    .line 165
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    invoke-virtual {v0, p1}, LTQ;->a(Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;)V

    .line 171
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 79
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTn;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LTn;

    .line 85
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lgl;

    .line 91
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LeQ;

    .line 97
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Llf;

    .line 103
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZj;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LZj;

    .line 109
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LZM;

    .line 115
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:LTQ;

    iget-object v0, v0, LTQ;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LTQ;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUr;

    iput-object v0, p1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUr;

    .line 121
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, LTQ;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 177
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 419
    return-void
.end method
