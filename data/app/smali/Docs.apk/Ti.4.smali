.class public LTi;
.super Ljava/lang/Object;
.source "UploadSharedItemActivityDelegate.java"


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 820
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LTi;->a:Landroid/content/Intent;

    .line 823
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 824
    return-void
.end method


# virtual methods
.method public a()LTi;
    .registers 4

    .prologue
    .line 878
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-string v1, "forceShowDialog"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 879
    return-object p0
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;)LTi;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 835
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 836
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 837
    return-object p0
.end method

.method public a(Ljava/lang/String;)LTi;
    .registers 4
    .parameter

    .prologue
    .line 856
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-string v1, "docListTitle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 857
    return-object p0
.end method

.method public a(LrK;)LTi;
    .registers 3
    .parameter

    .prologue
    .line 827
    invoke-virtual {p1}, LrK;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LTi;->a(Ljava/lang/String;)LTi;

    .line 828
    invoke-virtual {p1}, LrK;->a()Z

    move-result v0

    invoke-virtual {p0, v0}, LTi;->b(Z)LTi;

    .line 829
    invoke-virtual {p1}, LrK;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LTi;->c(Ljava/lang/String;)LTi;

    .line 830
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    invoke-virtual {p1, v0}, LrK;->a(Landroid/content/Intent;)V

    .line 831
    return-object p0
.end method

.method public a(Z)LTi;
    .registers 4
    .parameter

    .prologue
    .line 841
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-string v1, "showConversionOption"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 842
    return-object p0
.end method

.method public a()Landroid/content/Intent;
    .registers 3

    .prologue
    .line 883
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LTi;->a:Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    return-object v0
.end method

.method public b(Ljava/lang/String;)LTi;
    .registers 4
    .parameter

    .prologue
    .line 866
    if-eqz p1, :cond_9

    .line 867
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 869
    :cond_9
    return-object p0
.end method

.method public b(Z)LTi;
    .registers 4
    .parameter

    .prologue
    .line 846
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-string v1, "convertDocument"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 847
    return-object p0
.end method

.method public c(Ljava/lang/String;)LTi;
    .registers 4
    .parameter

    .prologue
    .line 873
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-string v1, "collectionResourceId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 874
    return-object p0
.end method

.method public c(Z)LTi;
    .registers 4
    .parameter

    .prologue
    .line 851
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-string v1, "deleteAfterUpload"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 852
    return-object p0
.end method

.method public d(Z)LTi;
    .registers 4
    .parameter

    .prologue
    .line 861
    iget-object v0, p0, LTi;->a:Landroid/content/Intent;

    const-string v1, "evaluateForOcr"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 862
    return-object p0
.end method
