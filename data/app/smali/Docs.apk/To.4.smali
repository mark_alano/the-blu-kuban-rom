.class public LTo;
.super Ljava/lang/Object;
.source "AclRefreshHelperImpl.java"

# interfaces
.implements LTn;


# instance fields
.field a:LUr;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LXS;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LamS;

.field a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, LamU;->a(Ljava/util/concurrent/ExecutorService;)LamS;

    move-result-object v0

    iput-object v0, p0, LTo;->a:LamS;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)LamQ;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LamQ",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, LTo;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 64
    iget-object v1, p0, LTo;->a:LamS;

    new-instance v2, LTq;

    invoke-direct {v2, p0, p1, p2, v0}, LTq;-><init>(LTo;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    invoke-interface {v1, v2}, LamS;->a(Ljava/lang/Runnable;)LamQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LamQ;
    .registers 8
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LamQ",
            "<",
            "LUq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, LTo;->a:LUr;

    invoke-interface {v0, p1, p2, p3}, LUr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LamQ;

    move-result-object v0

    .line 47
    invoke-direct {p0, p1, p2}, LTo;->a(Ljava/lang/String;Ljava/lang/String;)LamQ;

    move-result-object v1

    .line 50
    const/4 v2, 0x2

    new-array v2, v2, [LamQ;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    invoke-static {v2}, LamF;->a([LamQ;)LamQ;

    move-result-object v0

    .line 51
    new-instance v1, LTp;

    invoke-direct {v1, p0}, LTp;-><init>(LTo;)V

    .line 58
    invoke-static {v0, v1}, LamF;->a(LamQ;Lagl;)LamQ;

    move-result-object v0

    return-object v0
.end method
