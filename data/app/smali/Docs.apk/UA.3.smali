.class LUA;
.super Ljava/lang/Object;
.source "SharingListAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LUB;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LUz;)V
    .registers 2
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, LUA;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 65
    if-nez p1, :cond_4

    const-string p1, ""

    :cond_4
    return-object p1
.end method


# virtual methods
.method public a(LUB;LUB;)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 38
    if-ne p1, p2, :cond_4

    .line 39
    const/4 v0, 0x0

    .line 61
    :cond_3
    :goto_3
    return v0

    .line 41
    :cond_4
    if-nez p1, :cond_8

    .line 42
    const/4 v0, 0x1

    goto :goto_3

    .line 44
    :cond_8
    if-nez p2, :cond_c

    .line 45
    const/4 v0, -0x1

    goto :goto_3

    .line 47
    :cond_c
    invoke-virtual {p1}, LUB;->a()LUp;

    move-result-object v0

    invoke-virtual {v0}, LUp;->a()LeG;

    move-result-object v0

    .line 48
    invoke-virtual {p2}, LUB;->a()LUp;

    move-result-object v1

    invoke-virtual {v1}, LUp;->a()LeG;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, LeG;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    .line 50
    if-nez v0, :cond_3

    .line 53
    invoke-virtual {p1}, LUB;->a()LTG;

    move-result-object v0

    invoke-interface {v0}, LTG;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LUA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-virtual {p2}, LUB;->a()LTG;

    move-result-object v1

    invoke-interface {v1}, LTG;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LUA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    .line 56
    if-nez v0, :cond_3

    .line 59
    invoke-virtual {p1}, LUB;->a()LTG;

    move-result-object v0

    invoke-interface {v0}, LTG;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LUA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-virtual {p2}, LUB;->a()LTG;

    move-result-object v1

    invoke-interface {v1}, LTG;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, LUA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    goto :goto_3
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 35
    check-cast p1, LUB;

    check-cast p2, LUB;

    invoke-virtual {p0, p1, p2}, LUA;->a(LUB;LUB;)I

    move-result v0

    return v0
.end method
