.class public LUB;
.super Ljava/lang/Object;
.source "SharingListElement.java"

# interfaces
.implements LUF;


# instance fields
.field private final a:LTG;

.field private a:LUp;


# direct methods
.method public constructor <init>(LTG;LUp;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, LUB;->a:LTG;

    .line 26
    iput-object p2, p0, LUB;->a:LUp;

    .line 27
    return-void
.end method


# virtual methods
.method public a()LTG;
    .registers 2

    .prologue
    .line 30
    iget-object v0, p0, LUB;->a:LTG;

    return-object v0
.end method

.method public a()LUp;
    .registers 2

    .prologue
    .line 34
    iget-object v0, p0, LUB;->a:LUp;

    return-object v0
.end method

.method public a()LeB;
    .registers 2

    .prologue
    .line 42
    iget-object v0, p0, LUB;->a:LUp;

    invoke-virtual {v0}, LUp;->a()LeB;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, LUB;->a:LTG;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LUB;->a:LTG;

    invoke-interface {v0}, LTG;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method public a(LUp;)V
    .registers 2
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, LUB;->a:LUp;

    .line 39
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 52
    instance-of v1, p1, LUB;

    if-nez v1, :cond_6

    .line 56
    :cond_5
    :goto_5
    return v0

    .line 55
    :cond_6
    check-cast p1, LUB;

    .line 56
    iget-object v1, p0, LUB;->a:LTG;

    iget-object v2, p1, LUB;->a:LTG;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUB;->a:LUp;

    iget-object v2, p1, LUB;->a:LUp;

    invoke-virtual {v1, v2}, LUp;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 61
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LUB;->a:LTG;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LUB;->a:LUp;

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
