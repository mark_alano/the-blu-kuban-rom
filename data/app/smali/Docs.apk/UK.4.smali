.class public final enum LUK;
.super Ljava/lang/Enum;
.source "ContentKind.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LUK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LUK;

.field private static final synthetic a:[LUK;

.field public static final enum b:LUK;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, LUK;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LUK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LUK;->a:LUK;

    .line 13
    new-instance v0, LUK;

    const-string v1, "PDF"

    invoke-direct {v0, v1, v3}, LUK;-><init>(Ljava/lang/String;I)V

    sput-object v0, LUK;->b:LUK;

    .line 11
    const/4 v0, 0x2

    new-array v0, v0, [LUK;

    sget-object v1, LUK;->a:LUK;

    aput-object v1, v0, v2

    sget-object v1, LUK;->b:LUK;

    aput-object v1, v0, v3

    sput-object v0, LUK;->a:[LUK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LUK;
    .registers 2
    .parameter

    .prologue
    .line 11
    const-class v0, LUK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LUK;

    return-object v0
.end method

.method public static values()[LUK;
    .registers 1

    .prologue
    .line 11
    sget-object v0, LUK;->a:[LUK;

    invoke-virtual {v0}, [LUK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LUK;

    return-object v0
.end method
