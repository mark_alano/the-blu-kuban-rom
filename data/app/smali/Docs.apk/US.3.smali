.class public LUS;
.super Ljava/lang/Object;
.source "FileContentInstance.java"


# instance fields
.field private final a:LUX;

.field private final a:LZB;

.field private final a:LZS;

.field private final a:LaaJ;


# direct methods
.method public constructor <init>(LZS;LUX;LZB;LaaJ;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, LUS;->a:LZS;

    .line 43
    iput-object p2, p0, LUS;->a:LUX;

    .line 44
    iput-object p3, p0, LUS;->a:LZB;

    .line 45
    iput-object p4, p0, LUS;->a:LaaJ;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;ZLjava/lang/String;)LUQ;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    .line 61
    if-eqz p2, :cond_24

    .line 62
    if-eqz p2, :cond_22

    iget-object v0, p0, LUS;->a:LaaJ;

    invoke-interface {v0}, LaaJ;->e()Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, LUS;->a:LZB;

    invoke-interface {v0}, LZB;->a()Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 64
    :goto_12
    iget-object v1, p0, LUS;->a:LUX;

    invoke-interface {v1, p1, v0, p3}, LUX;->a(Ljava/lang/String;Ljavax/crypto/SecretKey;Ljava/lang/String;)LkN;

    move-result-object v0

    .line 68
    :goto_18
    new-instance v1, LUQ;

    iget-object v2, p0, LUS;->a:LZS;

    iget-object v3, p0, LUS;->a:LUX;

    invoke-direct {v1, v2, v3, v0}, LUQ;-><init>(LZS;LUX;LkN;)V

    return-object v1

    .line 62
    :cond_22
    const/4 v0, 0x0

    goto :goto_12

    .line 66
    :cond_24
    iget-object v0, p0, LUS;->a:LUX;

    invoke-interface {v0, p1, p3}, LUX;->a(Ljava/lang/String;Ljava/lang/String;)LkN;

    move-result-object v0

    goto :goto_18
.end method

.method public a(LkN;)LUQ;
    .registers 5
    .parameter

    .prologue
    .line 52
    new-instance v0, LUQ;

    iget-object v1, p0, LUS;->a:LZS;

    iget-object v2, p0, LUS;->a:LUX;

    invoke-direct {v0, v1, v2, p1}, LUQ;-><init>(LZS;LUX;LkN;)V

    return-object v0
.end method
