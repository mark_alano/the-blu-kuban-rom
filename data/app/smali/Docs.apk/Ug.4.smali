.class public LUg;
.super Ljava/lang/Object;
.source "ModifySharingActivity.java"

# interfaces
.implements LamE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LamE",
        "<",
        "LUq;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 482
    iput-object p1, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LUq;)V
    .registers 4
    .parameter

    .prologue
    .line 496
    iget-object v0, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    iget-object v1, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 497
    const-string v0, "ModifySharingActivity"

    const-string v1, "Updated ACL."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    iget-object v0, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;LUq;)LUq;

    .line 499
    iget-object v0, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Len;->sharing_message_saved:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V

    .line 500
    iget-object v0, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v1, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v1, v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;Landroid/view/View;)V

    .line 501
    iget-object v0, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 502
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 482
    check-cast p1, LUq;

    invoke-virtual {p0, p1}, LUg;->a(LUq;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .registers 5
    .parameter

    .prologue
    .line 485
    iget-object v0, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    iget-object v1, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 486
    instance-of v0, p1, LKw;

    if-eqz v0, :cond_15

    .line 487
    iget-object v0, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Len;->sharing_list_offline:I

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V

    .line 492
    :cond_14
    :goto_14
    return-void

    .line 488
    :cond_15
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_14

    .line 489
    const-string v0, "ModifySharingActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Update error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-static {v0, v1, v2}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 490
    iget-object v0, p0, LUg;->a:Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    sget v1, Len;->sharing_message_unable_to_change:I

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;ILjava/lang/Throwable;)V

    goto :goto_14
.end method
