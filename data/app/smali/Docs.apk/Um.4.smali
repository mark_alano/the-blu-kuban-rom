.class public LUm;
.super Ljava/lang/Object;
.source "ServerAclManager.java"

# interfaces
.implements LTl;


# instance fields
.field private final a:LVy;

.field private final a:LamS;


# direct methods
.method public constructor <init>(LVy;)V
    .registers 3
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-static {v0}, LamU;->a(Ljava/util/concurrent/ExecutorService;)LamS;

    move-result-object v0

    iput-object v0, p0, LUm;->a:LamS;

    .line 45
    iput-object p1, p0, LUm;->a:LVy;

    .line 46
    return-void
.end method

.method static synthetic a(LUm;)LVy;
    .registers 2
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, LUm;->a:LVy;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)LamQ;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LamQ",
            "<",
            "Ljava/util/Set",
            "<",
            "LeB;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, LUm;->a:LamS;

    new-instance v1, LUn;

    invoke-direct {v1, p0, p2, p1}, LUn;-><init>(LUm;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LamS;->a(Ljava/util/concurrent/Callable;)LamQ;

    move-result-object v0

    .line 94
    const-string v1, "ServerAclLoader"

    const-string v2, "Started parsing..."

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/util/Set;)LamQ;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "LeB;",
            ">;)",
            "LamQ",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 100
    invoke-static {p2}, Lajm;->a(Ljava/util/Collection;)Lajm;

    move-result-object v0

    .line 101
    iget-object v1, p0, LUm;->a:LamS;

    new-instance v2, LUo;

    invoke-direct {v2, p0, v0, p1}, LUo;-><init>(LUm;Ljava/util/Set;Ljava/lang/String;)V

    invoke-interface {v1, v2}, LamS;->a(Ljava/util/concurrent/Callable;)LamQ;

    move-result-object v0

    .line 153
    const-string v1, "ServerAclLoader"

    const-string v2, "Started updating..."

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    return-object v0
.end method
