.class LUo;
.super Ljava/lang/Object;
.source "ServerAclManager.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:LUm;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:Ljava/util/Set;


# direct methods
.method constructor <init>(LUm;Ljava/util/Set;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    iput-object p1, p0, LUo;->a:LUm;

    iput-object p2, p0, LUo;->a:Ljava/util/Set;

    iput-object p3, p0, LUo;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .registers 8

    .prologue
    const/4 v3, 0x0

    .line 104
    iget-object v0, p0, LUo;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 150
    :cond_9
    :goto_9
    return-object v3

    .line 107
    :cond_a
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 108
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 110
    iget-object v0, p0, LUo;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v3

    :goto_1b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeB;

    .line 111
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v1

    if-nez v1, :cond_9

    .line 114
    invoke-virtual {v0}, LeB;->a()Ljava/lang/String;

    move-result-object v1

    .line 115
    if-nez v1, :cond_3f

    .line 116
    new-instance v0, LTm;

    const-string v1, "Resource IDs in acl list should not be NULL."

    invoke-direct {v0, v1, v3}, LTm;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 118
    :cond_3f
    if-nez v2, :cond_57

    .line 123
    :goto_41
    invoke-static {v1, v0}, LVS;->a(Ljava/lang/String;LeB;)LVS;

    move-result-object v2

    .line 124
    invoke-virtual {v0}, LeB;->a()LeI;

    move-result-object v0

    sget-object v6, LeI;->e:LeI;

    if-ne v0, v6, :cond_65

    .line 125
    const-string v0, "delete"

    invoke-static {v2, v0}, Lata;->b(LasT;Ljava/lang/String;)V

    .line 129
    :goto_52
    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    .line 130
    goto :goto_1b

    .line 120
    :cond_57
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f1

    .line 121
    new-instance v0, LTm;

    const-string v1, "All resource IDs in acl list should be the same."

    invoke-direct {v0, v1, v3}, LTm;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 127
    :cond_65
    const-string v0, "update"

    invoke-static {v2, v0}, Lata;->b(LasT;Ljava/lang/String;)V

    goto :goto_52

    .line 131
    :cond_6b
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_9

    .line 136
    :try_start_75
    iget-object v0, p0, LUo;->a:LUm;

    invoke-static {v0}, LUm;->a(LUm;)LVy;

    move-result-object v0

    invoke-static {v2}, LeM;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LUo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v4}, LVy;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;)Latb;
    :try_end_88
    .catchall {:try_start_75 .. :try_end_88} :catchall_ae
    .catch Ljava/io/IOException; {:try_start_75 .. :try_end_88} :catch_90
    .catch Latc; {:try_start_75 .. :try_end_88} :catch_b5
    .catch LasH; {:try_start_75 .. :try_end_88} :catch_d3

    move-result-object v0

    .line 146
    if-eqz v0, :cond_9

    .line 147
    invoke-interface {v0}, Latb;->a()V

    goto/16 :goto_9

    .line 138
    :catch_90
    move-exception v0

    .line 139
    :try_start_91
    new-instance v1, LTm;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException in ACL update: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LTm;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_ae
    .catchall {:try_start_91 .. :try_end_ae} :catchall_ae

    .line 146
    :catchall_ae
    move-exception v0

    if-eqz v3, :cond_b4

    .line 147
    invoke-interface {v3}, Latb;->a()V

    :cond_b4
    throw v0

    .line 140
    :catch_b5
    move-exception v0

    .line 141
    :try_start_b6
    new-instance v1, LTm;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Parse Exception in ACL update: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Latc;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LTm;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 142
    :catch_d3
    move-exception v0

    .line 143
    new-instance v1, LTm;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Authentication Exception in ACL update: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LasH;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LTm;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_f1
    .catchall {:try_start_b6 .. :try_end_f1} :catchall_ae

    :cond_f1
    move-object v1, v2

    goto/16 :goto_41
.end method

.method public synthetic call()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 101
    invoke-virtual {p0}, LUo;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
