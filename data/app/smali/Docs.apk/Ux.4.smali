.class LUx;
.super Ljava/lang/Object;
.source "SharingInfoManagerImpl.java"

# interfaces
.implements LUq;


# instance fields
.field final synthetic a:LUs;

.field private a:LUw;

.field private a:LeH;

.field private final a:Ljava/lang/String;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LUB;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LeB;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(LUs;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    iput-object p1, p0, LUx;->a:LUs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, ""

    iput-object v0, p0, LUx;->e:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LUx;->b:Ljava/util/List;

    .line 74
    iput-object p2, p0, LUx;->c:Ljava/lang/String;

    .line 75
    iput-object p3, p0, LUx;->a:Ljava/lang/String;

    .line 76
    iput-object p4, p0, LUx;->b:Ljava/lang/String;

    .line 77
    invoke-static {p2}, LUD;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LUx;->e:Ljava/lang/String;

    .line 78
    new-instance v0, LUw;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LUw;-><init>(LUt;)V

    iput-object v0, p0, LUx;->a:LUw;

    .line 79
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LUx;->a:Ljava/util/List;

    .line 80
    return-void
.end method

.method private a(LUw;LeH;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, LUx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 85
    iput-object p3, p0, LUx;->f:Ljava/lang/String;

    .line 86
    iput-object p4, p0, LUx;->d:Ljava/lang/String;

    .line 87
    iput-object p1, p0, LUx;->a:LUw;

    .line 88
    iput-object p2, p0, LUx;->a:LeH;

    .line 89
    iget-object v0, p0, LUx;->a:LeH;

    sget-object v1, LeH;->n:LeH;

    if-ne v0, v1, :cond_1d

    invoke-virtual {p0}, LUx;->b()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 90
    sget-object v0, LeH;->a:LeH;

    iput-object v0, p0, LUx;->a:LeH;

    .line 92
    :cond_1d
    iget-object v0, p0, LUx;->a:LUw;

    iput-object v0, p0, LUx;->a:Ljava/util/List;

    .line 93
    return-void
.end method

.method static synthetic a(LUx;LUw;LeH;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3, p4}, LUx;->a(LUw;LeH;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private c()Z
    .registers 6

    .prologue
    const/4 v1, 0x1

    .line 137
    iget-object v0, p0, LUx;->a:LUw;

    invoke-virtual {v0}, LUw;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUB;

    .line 138
    invoke-virtual {v0}, LUB;->a()LeB;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, LeB;->a()LeK;

    move-result-object v3

    sget-object v4, LeK;->a:LeK;

    if-eq v3, v4, :cond_27

    invoke-virtual {v0}, LeB;->a()LeK;

    move-result-object v3

    sget-object v4, LeK;->b:LeK;

    if-ne v3, v4, :cond_7

    .line 140
    :cond_27
    invoke-virtual {v0}, LeB;->a()LeI;

    move-result-object v3

    sget-object v4, LeI;->a:LeI;

    if-ne v3, v4, :cond_3d

    .line 141
    iget-object v3, p0, LUx;->c:Ljava/lang/String;

    invoke-virtual {v0}, LeB;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 150
    :goto_3c
    return v0

    :cond_3d
    move v0, v1

    .line 146
    goto :goto_3c

    .line 150
    :cond_3f
    const/4 v0, 0x0

    goto :goto_3c
.end method


# virtual methods
.method public a(Ljava/lang/String;)LUB;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 213
    invoke-virtual {p0}, LUx;->a()Ljava/util/List;

    move-result-object v0

    .line 215
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUB;

    .line 216
    if-nez v0, :cond_24

    move-object v2, v1

    .line 217
    :goto_18
    if-nez v2, :cond_29

    move-object v2, v1

    .line 218
    :goto_1b
    if-eqz v2, :cond_9

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 223
    :goto_23
    return-object v0

    .line 216
    :cond_24
    invoke-virtual {v0}, LUB;->a()LTG;

    move-result-object v2

    goto :goto_18

    .line 217
    :cond_29
    invoke-interface {v2}, LTG;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_1b

    :cond_2e
    move-object v0, v1

    goto :goto_23
.end method

.method public a()LeH;
    .registers 2

    .prologue
    .line 168
    iget-object v0, p0, LUx;->a:LeH;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 173
    iget-object v0, p0, LUx;->f:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LUB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, LUx;->a:LUw;

    return-object v0
.end method

.method public a()V
    .registers 7

    .prologue
    .line 155
    new-instance v1, LUw;

    const/4 v0, 0x0

    invoke-direct {v1, v0}, LUw;-><init>(LUt;)V

    .line 156
    iget-object v0, p0, LUx;->a:LUw;

    invoke-virtual {v0}, LUw;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUB;

    .line 157
    invoke-virtual {v0}, LUB;->a()LUp;

    move-result-object v3

    .line 158
    iget-object v4, p0, LUx;->a:LUs;

    iget-object v4, v4, LUs;->a:LTI;

    invoke-virtual {v0}, LUB;->a()LTG;

    move-result-object v0

    invoke-interface {v0}, LTG;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, LUp;->a()LeK;

    move-result-object v5

    invoke-interface {v4, v0, v5}, LTI;->a(Ljava/lang/String;LeK;)LTG;

    move-result-object v0

    .line 160
    new-instance v4, LUB;

    invoke-direct {v4, v0, v3}, LUB;-><init>(LTG;LUp;)V

    invoke-virtual {v1, v4}, LUw;->add(Ljava/lang/Object;)Z

    goto :goto_c

    .line 162
    :cond_39
    iput-object v1, p0, LUx;->a:LUw;

    .line 163
    iget-object v0, p0, LUx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 164
    return-void
.end method

.method public a(LeB;)V
    .registers 3
    .parameter

    .prologue
    .line 228
    iget-object v0, p0, LUx;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    return-void
.end method

.method public a(LeH;)V
    .registers 2
    .parameter

    .prologue
    .line 193
    iput-object p1, p0, LUx;->a:LeH;

    .line 194
    return-void
.end method

.method public a()Z
    .registers 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 97
    iget-object v0, p0, LUx;->a:LUw;

    if-nez v0, :cond_f

    .line 98
    const-string v0, "SharingWorkflowImpl"

    const-string v2, "ACL modification tested while ACL is null."

    invoke-static {v0, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 109
    :goto_e
    return v0

    .line 101
    :cond_f
    iget-object v0, p0, LUx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_19

    move v0, v2

    .line 102
    goto :goto_e

    .line 104
    :cond_19
    iget-object v0, p0, LUx;->a:LUw;

    invoke-virtual {v0}, LUw;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUB;

    .line 105
    invoke-virtual {v0}, LUB;->a()LUp;

    move-result-object v0

    invoke-virtual {v0}, LUp;->a()Z

    move-result v0

    if-eqz v0, :cond_1f

    move v0, v2

    .line 106
    goto :goto_e

    :cond_37
    move v0, v1

    .line 109
    goto :goto_e
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 178
    iget-object v0, p0, LUx;->d:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LUB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, LUx;->a:Ljava/util/List;

    return-object v0
.end method

.method public b()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 114
    iget-object v2, p0, LUx;->a:LUw;

    invoke-virtual {v2}, LUw;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 133
    :cond_a
    :goto_a
    return v0

    .line 117
    :cond_b
    invoke-direct {p0}, LUx;->c()Z

    move-result v2

    if-eqz v2, :cond_13

    move v0, v1

    .line 118
    goto :goto_a

    .line 120
    :cond_13
    sget-object v2, LUv;->a:[I

    iget-object v3, p0, LUx;->a:LeH;

    invoke-virtual {v3}, LeH;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_30

    goto :goto_a

    :pswitch_21
    move v0, v1

    .line 123
    goto :goto_a

    .line 126
    :pswitch_23
    iget-object v2, p0, LUx;->e:Ljava/lang/String;

    iget-object v3, p0, LUx;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    move v0, v1

    .line 127
    goto :goto_a

    .line 120
    nop

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_21
        :pswitch_21
        :pswitch_23
        :pswitch_23
    .end packed-switch
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 203
    iget-object v0, p0, LUx;->a:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LeB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, LUx;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 208
    iget-object v0, p0, LUx;->b:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 198
    iget-object v0, p0, LUx;->c:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 238
    instance-of v1, p1, LUx;

    if-nez v1, :cond_6

    .line 249
    :cond_5
    :goto_5
    return v0

    .line 242
    :cond_6
    check-cast p1, LUx;

    .line 243
    iget-object v1, p0, LUx;->a:Ljava/lang/String;

    iget-object v2, p1, LUx;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUx;->b:Ljava/lang/String;

    iget-object v2, p1, LUx;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUx;->c:Ljava/lang/String;

    iget-object v2, p1, LUx;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUx;->a:Ljava/util/List;

    iget-object v2, p1, LUx;->a:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUx;->d:Ljava/lang/String;

    iget-object v2, p1, LUx;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUx;->a:LUw;

    iget-object v2, p1, LUx;->a:LUw;

    invoke-virtual {v1, v2}, LUw;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUx;->a:LeH;

    iget-object v2, p1, LUx;->a:LeH;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUx;->f:Ljava/lang/String;

    iget-object v2, p1, LUx;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LUx;->b:Ljava/util/List;

    iget-object v2, p1, LUx;->b:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 253
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LUx;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LUx;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LUx;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LUx;->a:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LUx;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LUx;->a:LUw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LUx;->a:LeH;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, LUx;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, LUx;->b:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
