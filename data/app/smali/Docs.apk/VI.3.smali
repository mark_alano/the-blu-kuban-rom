.class public LVI;
.super LVJ;
.source "DocsListFeedClientImpl.java"

# interfaces
.implements LVH;


# direct methods
.method public constructor <init>(LasL;LVL;LNe;)V
    .registers 4
    .parameter
        .annotation runtime LaqW;
            value = "DocFeed"
        .end annotation
    .end parameter
    .parameter
    .end parameter
    .parameter
    .end parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, LVJ;-><init>(LasL;LVL;LNe;)V

    .line 42
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 108
    const-string v0, "https://docs.google.com/feeds/default/private/full/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 109
    const-string v1, "root"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_37

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "folder:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 116
    :goto_28
    const-string v1, "showdeleted=true&showroot=true"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 112
    :cond_37
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "document:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_28
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LVV;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 76
    const-class v0, LVV;

    invoke-virtual {p0, v0, p1, p2, p3}, LVI;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LasT;

    move-result-object v0

    .line 77
    instance-of v1, v0, LVV;

    if-eqz v1, :cond_d

    .line 78
    check-cast v0, LVV;

    return-object v0

    .line 80
    :cond_d
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected DocEntry, got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)LVV;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 55
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, p1, p2, v0}, LVI;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LVV;
    :try_end_4
    .catch LasS; {:try_start_1 .. :try_end_4} :catch_6

    move-result-object v0

    .line 56
    return-object v0

    .line 57
    :catch_6
    move-exception v0

    .line 58
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LVV;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    if-eqz p3, :cond_b

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lagu;->a(Z)V

    .line 67
    :try_start_6
    invoke-direct {p0, p1, p2, p3}, LVI;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LVV;
    :try_end_9
    .catch LasS; {:try_start_6 .. :try_end_9} :catch_d

    move-result-object v0

    .line 70
    :goto_a
    return-object v0

    .line 65
    :cond_b
    const/4 v0, 0x0

    goto :goto_3

    .line 69
    :catch_d
    move-exception v0

    .line 70
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Latb;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    const-class v0, LVV;

    invoke-virtual {p0, v0, p1, p2, p3}, LVI;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Latb;

    move-result-object v0

    return-object v0
.end method

.method public a(LkY;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 88
    iget-object v0, p1, LkY;->b:Ljava/lang/String;

    invoke-static {v0}, LVI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LVI;->a(Ljava/lang/String;Ljava/lang/String;)LVV;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, LVV;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
