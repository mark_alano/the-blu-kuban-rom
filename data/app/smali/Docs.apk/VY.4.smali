.class public LVY;
.super Latd;
.source "AccountMetadataGDataParser.java"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Latd;-><init>(Ljava/io/InputStream;Lorg/xmlpull/v1/XmlPullParser;)V

    .line 68
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)LWi;
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 142
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v8, v0, :cond_52

    move v0, v1

    :goto_b
    invoke-static {v0}, Lagu;->b(Z)V

    .line 143
    invoke-direct {p0, p1}, LVY;->a(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 145
    const-string v0, "http://schemas.google.com/docs/2007"

    const-string v3, "installedAppId"

    invoke-interface {p1, v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    const-string v3, "http://schemas.google.com/docs/2007"

    const-string v4, "installedAppName"

    invoke-interface {p1, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 147
    const-string v4, "http://schemas.google.com/docs/2007"

    const-string v5, "installedAppObjectType"

    invoke-interface {p1, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 148
    const-string v5, "http://schemas.google.com/docs/2007"

    const-string v6, "installedAppSupportsCreate"

    invoke-interface {p1, v5, v6}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 150
    new-instance v6, LWi;

    invoke-direct {v6, v0, v3, v4, v5}, LWi;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 153
    :cond_42
    :goto_42
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 154
    packed-switch v0, :pswitch_data_ec

    goto :goto_42

    .line 188
    :pswitch_4a
    new-instance v0, Latc;

    const-string v1, "Premature end of document before </installedApp>"

    invoke-direct {v0, v1}, Latc;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_52
    move v0, v2

    .line 142
    goto :goto_b

    .line 156
    :pswitch_54
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v3

    .line 158
    sget-object v4, Latf;->i:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_82

    .line 159
    const-string v0, "http://schemas.google.com/docs/2007#product"

    sget-object v3, Latf;->h:Ljava/lang/String;

    invoke-interface {p1, v9, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 160
    sget-object v0, Latf;->l:Ljava/lang/String;

    invoke-interface {p1, v9, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    sget-object v3, Latf;->j:Ljava/lang/String;

    invoke-interface {p1, v9, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 162
    invoke-virtual {v6, v3, v0}, LWi;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_42

    .line 165
    :cond_82
    const-string v4, "http://schemas.google.com/docs/2007"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d1

    .line 166
    invoke-static {p1}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    .line 167
    const-string v4, "installedAppPrimaryMimeType"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9a

    .line 168
    invoke-virtual {v6, v3}, LWi;->a(Ljava/lang/String;)V

    goto :goto_42

    .line 169
    :cond_9a
    const-string v4, "installedAppSecondaryMimeType"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a6

    .line 170
    invoke-virtual {v6, v3}, LWi;->b(Ljava/lang/String;)V

    goto :goto_42

    .line 171
    :cond_a6
    const-string v4, "installedAppPrimaryFileExtension"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b2

    .line 172
    invoke-virtual {v6, v3}, LWi;->c(Ljava/lang/String;)V

    goto :goto_42

    .line 173
    :cond_b2
    const-string v4, "installedAppSecondaryFileExtension"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_be

    .line 174
    invoke-virtual {v6, v3}, LWi;->d(Ljava/lang/String;)V

    goto :goto_42

    .line 176
    :cond_be
    const-string v4, "AccountMetadataGDataParser"

    const-string v5, "Unknown tag: %s with text %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v0, v7, v2

    aput-object v3, v7, v1

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_42

    .line 179
    :cond_d1
    const-string v4, "AccountMetadataGDataParser"

    const-string v5, "Unknwon tag: %s in namespace %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v0, v7, v2

    aput-object v3, v7, v1

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_42

    .line 183
    :pswitch_e4
    invoke-direct {p0, p1}, LVY;->a(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 184
    return-object v6

    .line 154
    nop

    :pswitch_data_ec
    .packed-switch 0x1
        :pswitch_4a
        :pswitch_54
        :pswitch_e4
    .end packed-switch
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)LeG;
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 244
    const-string v0, "primaryRole"

    invoke-interface {p1, v9, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 245
    invoke-static {v0}, LeI;->a(Ljava/lang/String;)LeI;

    move-result-object v1

    .line 246
    sget-object v2, LeI;->f:LeI;

    invoke-virtual {v1, v2}, LeI;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_76

    .line 247
    const-class v2, LeD;

    invoke-static {v2}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    .line 249
    :cond_1b
    :goto_1b
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 250
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 251
    packed-switch v3, :pswitch_data_8c

    goto :goto_1b

    .line 272
    :pswitch_27
    new-instance v0, Latc;

    const-string v1, "Premature end of document before </additionalRoleSet>"

    invoke-direct {v0, v1}, Latc;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :pswitch_2f
    const-string v3, "additionalRole"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_59

    .line 254
    const-string v3, "value"

    invoke-interface {p1, v9, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 255
    invoke-static {v3}, LeD;->a(Ljava/lang/String;)LeD;

    move-result-object v3

    .line 256
    sget-object v4, LeD;->b:LeD;

    if-eq v3, v4, :cond_49

    .line 257
    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1b

    .line 259
    :cond_49
    const-string v3, "AccountMetadataGDataParser"

    const-string v4, "Unknown additional role: %s"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object v0, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1b

    .line 262
    :cond_59
    const-string v3, "AccountMetadataGDataParser"

    const-string v5, "Unknown tag: %s"

    new-array v6, v8, [Ljava/lang/Object;

    aput-object v4, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1b

    .line 266
    :pswitch_69
    const-string v3, "additionalRoleSet"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 267
    invoke-static {v1, v2}, LeG;->a(LeI;Ljava/util/Set;)LeG;

    move-result-object v0

    .line 280
    :goto_75
    return-object v0

    .line 277
    :cond_76
    const-string v1, "AccountMetadataGDataParser"

    const-string v2, "Unknown primary role: %s"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-virtual {p0}, LVY;->c()V

    .line 280
    sget-object v0, LeG;->g:LeG;

    goto :goto_75

    .line 251
    nop

    :pswitch_data_8c
    .packed-switch 0x1
        :pswitch_27
        :pswitch_2f
        :pswitch_69
    .end packed-switch
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)LkP;
    .registers 4
    .parameter

    .prologue
    .line 289
    const/4 v0, 0x0

    const-string v1, "kind"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 290
    invoke-static {v0}, LkO;->a(Ljava/lang/String;)LkP;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;LVR;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 198
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    if-ne v8, v0, :cond_2b

    move v0, v1

    :goto_a
    invoke-static {v0}, Lagu;->b(Z)V

    .line 199
    invoke-direct {p0, p1}, LVY;->b(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 201
    const/4 v0, 0x0

    const-string v3, "kind"

    invoke-interface {p1, v0, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    :cond_1b
    :goto_1b
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    .line 205
    packed-switch v3, :pswitch_data_94

    goto :goto_1b

    .line 233
    :pswitch_23
    new-instance v0, Latc;

    const-string v1, "Premature end of document before </additionalRoleInfo>"

    invoke-direct {v0, v1}, Latc;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2b
    move v0, v2

    .line 198
    goto :goto_a

    .line 207
    :pswitch_2d
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 208
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v4

    .line 209
    const-string v5, "http://schemas.google.com/docs/2007"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_78

    .line 210
    const-string v4, "additionalRoleSet"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_65

    .line 211
    invoke-direct {p0, p1}, LVY;->a(Lorg/xmlpull/v1/XmlPullParser;)LeG;

    move-result-object v3

    .line 212
    sget-object v4, LeG;->g:LeG;

    invoke-virtual {v3, v4}, LeG;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_55

    .line 213
    invoke-virtual {p2, v0, v3}, LVR;->a(Ljava/lang/String;LeG;)V

    goto :goto_1b

    .line 215
    :cond_55
    const-string v3, "AccountMetadataGDataParser"

    const-string v4, "Unknown combined role in document kind: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1b

    .line 219
    :cond_65
    const-string v4, "AccountMetadataGDataParser"

    const-string v5, "Unknown tag: %s"

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v3, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-virtual {p0}, LVY;->c()V

    goto :goto_1b

    .line 223
    :cond_78
    const-string v5, "AccountMetadataGDataParser"

    const-string v6, "Unknown tag: %s in namespace %s"

    new-array v7, v8, [Ljava/lang/Object;

    aput-object v3, v7, v2

    aput-object v4, v7, v1

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    invoke-virtual {p0}, LVY;->c()V

    goto :goto_1b

    .line 228
    :pswitch_8d
    invoke-direct {p0, p1}, LVY;->b(Lorg/xmlpull/v1/XmlPullParser;)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 229
    return-void

    .line 205
    :pswitch_data_94
    .packed-switch 0x1
        :pswitch_23
        :pswitch_2d
        :pswitch_8d
    .end packed-switch
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;)Z
    .registers 4
    .parameter

    .prologue
    .line 136
    const-string v0, "installedApp"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const-string v0, "http://schemas.google.com/docs/2007"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method private b(Lorg/xmlpull/v1/XmlPullParser;)Z
    .registers 4
    .parameter

    .prologue
    .line 284
    const-string v0, "additionalRoleInfo"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const-string v0, "http://schemas.google.com/docs/2007"

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method


# virtual methods
.method protected a()LVR;
    .registers 2

    .prologue
    .line 72
    new-instance v0, LVR;

    invoke-direct {v0}, LVR;-><init>()V

    return-object v0
.end method

.method protected bridge synthetic a()LasT;
    .registers 2

    .prologue
    .line 33
    invoke-virtual {p0}, LVY;->a()LVR;

    move-result-object v0

    return-object v0
.end method

.method protected a(LasT;)V
    .registers 8
    .parameter

    .prologue
    .line 78
    invoke-virtual {p0}, LVY;->a()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    .line 80
    instance-of v0, p1, LVR;

    if-nez v0, :cond_10

    .line 81
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected AccountMetadataEntry!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    move-object v0, p1

    .line 84
    check-cast v0, LVR;

    .line 85
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 86
    invoke-interface {v1}, Lorg/xmlpull/v1/XmlPullParser;->getNamespace()Ljava/lang/String;

    move-result-object v3

    .line 88
    :try_start_1b
    const-string v4, "http://schemas.google.com/g/2005"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_65

    .line 89
    const-string v3, "quotaBytesTotal"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_37

    .line 90
    invoke-static {v1}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LVR;->a(J)V

    .line 133
    :cond_36
    :goto_36
    return-void

    .line 92
    :cond_37
    const-string v3, "quotaBytesUsed"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_36

    .line 93
    invoke-static {v1}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 94
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LVR;->b(J)V
    :try_end_4a
    .catch Ljava/lang/NumberFormatException; {:try_start_1b .. :try_end_4a} :catch_4b

    goto :goto_36

    .line 130
    :catch_4b
    move-exception v0

    .line 131
    new-instance v1, Latc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error parsing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Latc;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 96
    :cond_65
    :try_start_65
    const-string v4, "http://schemas.google.com/docs/2007"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_36

    .line 97
    const-string v3, "quotaBytesUsedInTrash"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_81

    .line 98
    invoke-static {v1}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, LVR;->c(J)V

    goto :goto_36

    .line 100
    :cond_81
    const-string v3, "importFormat"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_91

    const-string v3, "exportFormat"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_af

    .line 101
    :cond_91
    const/4 v3, 0x0

    const-string v4, "source"

    invoke-interface {v1, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 103
    const/4 v4, 0x0

    const-string v5, "target"

    invoke-interface {v1, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 105
    const-string v4, "importFormat"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_ab

    .line 106
    invoke-virtual {v0, v3, v1}, LVR;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_36

    .line 108
    :cond_ab
    invoke-virtual {v0, v3, v1}, LVR;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_36

    .line 110
    :cond_af
    const-string v3, "featureName"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c0

    .line 111
    invoke-static {v1}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, LVR;->a(Ljava/lang/String;)V

    goto/16 :goto_36

    .line 113
    :cond_c0
    const-string v3, "maxUploadSize"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d9

    .line 114
    invoke-direct {p0, v1}, LVY;->a(Lorg/xmlpull/v1/XmlPullParser;)LkP;

    move-result-object v2

    .line 115
    invoke-static {v1}, LasW;->a(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 116
    invoke-virtual {v0, v2, v3, v4}, LVR;->a(LkP;J)V

    goto/16 :goto_36

    .line 117
    :cond_d9
    const-string v3, "largestChangestamp"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f1

    .line 118
    const/4 v2, 0x0

    const-string v3, "value"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LVR;->a(I)V

    goto/16 :goto_36

    .line 120
    :cond_f1
    const-string v3, "remainingChangestamps"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_109

    .line 121
    const/4 v2, 0x0

    const-string v3, "value"

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, LVR;->b(I)V

    goto/16 :goto_36

    .line 123
    :cond_109
    const-string v3, "installedApp"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11a

    .line 124
    invoke-direct {p0, v1}, LVY;->a(Lorg/xmlpull/v1/XmlPullParser;)LWi;

    move-result-object v1

    .line 125
    invoke-virtual {v0, v1}, LVR;->a(LWi;)Z

    goto/16 :goto_36

    .line 126
    :cond_11a
    const-string v3, "additionalRoleInfo"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_36

    .line 127
    invoke-direct {p0, v1, v0}, LVY;->a(Lorg/xmlpull/v1/XmlPullParser;LVR;)V
    :try_end_125
    .catch Ljava/lang/NumberFormatException; {:try_start_65 .. :try_end_125} :catch_4b

    goto/16 :goto_36
.end method
