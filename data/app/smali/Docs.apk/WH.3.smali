.class final LWH;
.super LWB;
.source "SingleFeedFilter.java"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic a:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 157
    iput-object p2, p0, LWH;->a:Ljava/lang/String;

    iput-boolean p3, p0, LWH;->a:Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LWB;-><init>(Ljava/lang/String;LWC;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Landroid/net/Uri;
    .registers 11
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 160
    if-nez p1, :cond_7

    move-object v0, v1

    .line 184
    :goto_6
    return-object v0

    .line 164
    :cond_7
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 165
    const-string v0, "-"

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 166
    if-ne v0, v7, :cond_17

    .line 167
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 169
    :cond_17
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    .line 170
    invoke-interface {v2, v8, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    .line 171
    invoke-static {v3, v4}, LWB;->a(Landroid/net/Uri$Builder;Ljava/util/List;)V

    .line 172
    iget-object v1, p0, LWH;->a:Ljava/lang/String;

    .line 173
    iget-boolean v5, p0, LWH;->a:Z

    if-eqz v5, :cond_3f

    .line 174
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "folder:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 177
    :cond_3f
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    aput-object v1, v5, v8

    const/4 v1, 0x1

    const-string v6, "contents"

    aput-object v6, v5, v1

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 179
    invoke-static {v4, v1}, Ljava/util/Collections;->indexOfSubList(Ljava/util/List;Ljava/util/List;)I

    move-result v4

    if-ne v4, v7, :cond_56

    .line 180
    invoke-static {v3, v1}, LWB;->a(Landroid/net/Uri$Builder;Ljava/util/List;)V

    .line 182
    :cond_56
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 183
    invoke-static {v3, v0}, LWB;->a(Landroid/net/Uri$Builder;Ljava/util/List;)V

    .line 184
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_6
.end method
