.class LWc;
.super Ljava/lang/Object;
.source "AddChildEntrySerializer.java"

# interfaces
.implements Lath;


# instance fields
.field private final a:LVT;

.field private final a:Latg;


# direct methods
.method public constructor <init>(Latg;LVT;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latg;

    iput-object v0, p0, LWc;->a:Latg;

    .line 33
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVT;

    iput-object v0, p0, LWc;->a:LVT;

    .line 34
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 4
    .parameter

    .prologue
    .line 75
    const-string v0, ""

    const-string v1, "http://www.w3.org/2005/Atom"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method private a(Lorg/xmlpull/v1/XmlSerializer;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, LWc;->a:LVT;

    invoke-virtual {v0}, LVT;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LWc;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-static {p1}, LasV;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 72
    :goto_7
    return-void

    .line 69
    :cond_8
    sget-object v0, Latf;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 70
    invoke-interface {p0, p1}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 71
    sget-object v0, Latf;->p:Ljava/lang/String;

    invoke-interface {p0, v1, v0}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_7
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 80
    const-string v0, "application/atom+xml"

    return-object v0
.end method

.method public a(Ljava/io/OutputStream;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x3

    .line 38
    .line 40
    :try_start_1
    iget-object v0, p0, LWc;->a:Latg;

    invoke-interface {v0}, Latg;->a()Lorg/xmlpull/v1/XmlSerializer;
    :try_end_6
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_6} :catch_32

    move-result-object v0

    .line 45
    sget-object v1, Latf;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 46
    if-eq p2, v3, :cond_18

    .line 47
    sget-object v1, Latf;->e:Ljava/lang/String;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 48
    invoke-direct {p0, v0}, LWc;->a(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 51
    :cond_18
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Latf;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 52
    invoke-direct {p0, v0, p2}, LWc;->a(Lorg/xmlpull/v1/XmlSerializer;I)V

    .line 53
    const-string v1, "http://www.w3.org/2005/Atom"

    sget-object v2, Latf;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 55
    if-eq p2, v3, :cond_2e

    .line 56
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 58
    :cond_2e
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlSerializer;->flush()V

    .line 59
    return-void

    .line 41
    :catch_32
    move-exception v0

    .line 42
    new-instance v1, Latc;

    const-string v2, "Unable to create XmlSerializer."

    invoke-direct {v1, v2, v0}, Latc;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 85
    const/4 v0, 0x0

    return v0
.end method
