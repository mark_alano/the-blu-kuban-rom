.class public final LWj;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LVZ;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LWf;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LWb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 33
    iput-object p1, p0, LWj;->a:LYD;

    .line 34
    const-class v0, LVZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LWj;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LWj;->a:LZb;

    .line 37
    const-class v0, LWf;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LWj;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LWj;->b:LZb;

    .line 40
    const-class v0, LWb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LWj;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LWj;->c:LZb;

    .line 43
    return-void
.end method

.method static synthetic a(LWj;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LWj;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWj;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWj;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWj;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWj;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 50
    const-class v0, LVZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LWj;->a:LZb;

    invoke-virtual {p0, v0, v1}, LWj;->a(Laop;LZb;)V

    .line 51
    const-class v0, LWf;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LWj;->b:LZb;

    invoke-virtual {p0, v0, v1}, LWj;->a(Laop;LZb;)V

    .line 52
    const-class v0, LWb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LWj;->c:LZb;

    invoke-virtual {p0, v0, v1}, LWj;->a(Laop;LZb;)V

    .line 53
    iget-object v0, p0, LWj;->a:LZb;

    new-instance v1, LWk;

    invoke-direct {v1, p0}, LWk;-><init>(LWj;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 67
    iget-object v0, p0, LWj;->b:LZb;

    new-instance v1, LWl;

    invoke-direct {v1, p0}, LWl;-><init>(LWj;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 81
    iget-object v0, p0, LWj;->c:LZb;

    new-instance v1, LWm;

    invoke-direct {v1, p0}, LWm;-><init>(LWj;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 100
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 104
    return-void
.end method
