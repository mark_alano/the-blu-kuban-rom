.class public LWn;
.super LWh;
.source "XmlAclGDataSerializer.java"


# direct methods
.method constructor <init>(Latg;LVS;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, LWh;-><init>(Latg;LasT;)V

    .line 36
    return-void
.end method

.method private a()LVS;
    .registers 2

    .prologue
    .line 39
    invoke-virtual {p0}, LWn;->a()LasT;

    move-result-object v0

    check-cast v0, LVS;

    return-object v0
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;LeI;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 57
    sget-object v0, LeI;->f:LeI;

    if-eq p1, v0, :cond_9

    sget-object v0, LeI;->e:LeI;

    if-ne p1, v0, :cond_a

    .line 69
    :cond_9
    :goto_9
    return-void

    .line 60
    :cond_a
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "role"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 61
    const-string v0, "value"

    invoke-virtual {p1}, LeI;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 62
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "role"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 64
    if-eqz p2, :cond_9

    .line 65
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "role"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 66
    const-string v0, "value"

    sget-object v1, LeI;->d:LeI;

    invoke-virtual {v1}, LeI;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 67
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "role"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_9
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;LeK;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 73
    sget-object v0, LeK;->e:LeK;

    if-ne p1, v0, :cond_6

    .line 82
    :goto_5
    return-void

    .line 76
    :cond_6
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "scope"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 77
    sget-object v0, Latf;->l:Ljava/lang/String;

    invoke-virtual {p1}, LeK;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 78
    if-eqz p2, :cond_1d

    .line 79
    const-string v0, "value"

    invoke-interface {p0, v2, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 81
    :cond_1d
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v1, "scope"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_5
.end method

.method private static a(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/Set;)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/xmlpull/v1/XmlSerializer;",
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    sget-object v0, LeD;->b:LeD;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_35

    const/4 v0, 0x1

    :goto_9
    invoke-static {v0}, Lagu;->a(Z)V

    .line 87
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeD;

    .line 88
    const-string v2, "http://schemas.google.com/acl/2007"

    const-string v3, "additionalRole"

    invoke-interface {p0, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 89
    const/4 v2, 0x0

    const-string v3, "value"

    invoke-virtual {v0}, LeD;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v2, v3, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 90
    const-string v0, "http://schemas.google.com/acl/2007"

    const-string v2, "additionalRole"

    invoke-interface {p0, v0, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_10

    .line 86
    :cond_35
    const/4 v0, 0x0

    goto :goto_9

    .line 92
    :cond_37
    return-void
.end method


# virtual methods
.method protected a(Lorg/xmlpull/v1/XmlSerializer;)V
    .registers 4
    .parameter

    .prologue
    .line 97
    const-string v0, ""

    const-string v1, "http://www.w3.org/2005/Atom"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const-string v0, "gd"

    const-string v1, "http://schemas.google.com/g/2005"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v0, "gAcl"

    const-string v1, "http://schemas.google.com/acl/2007"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method protected b(Lorg/xmlpull/v1/XmlSerializer;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, LWn;->a()LVS;

    move-result-object v0

    .line 48
    const-string v1, "XmlAclGDataSerializer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Serializing "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-virtual {v0}, LVS;->a()LeI;

    move-result-object v1

    invoke-virtual {v0}, LVS;->a()Z

    move-result v2

    invoke-static {p1, v1, v2}, LWn;->a(Lorg/xmlpull/v1/XmlSerializer;LeI;Z)V

    .line 51
    invoke-virtual {v0}, LVS;->a()LeK;

    move-result-object v1

    invoke-virtual {v0}, LVS;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2}, LWn;->a(Lorg/xmlpull/v1/XmlSerializer;LeK;Ljava/lang/String;)V

    .line 52
    invoke-virtual {v0}, LVS;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {p1, v0}, LWn;->a(Lorg/xmlpull/v1/XmlSerializer;Ljava/util/Set;)V

    .line 53
    return-void
.end method
