.class public final LWr;
.super Ljava/lang/Object;
.source "DocumentFeedAndFolderFeedFilter.java"


# static fields
.field public static final a:LWr;

.field public static final b:LWr;

.field public static final c:LWr;

.field public static final d:LWr;


# instance fields
.field final a:LWB;

.field final b:LWB;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 41
    new-instance v0, LWr;

    sget-object v1, LWB;->a:LWB;

    sget-object v2, LWB;->a:LWB;

    invoke-direct {v0, v1, v2}, LWr;-><init>(LWB;LWB;)V

    sput-object v0, LWr;->a:LWr;

    .line 47
    new-instance v0, LWr;

    sget-object v1, LWB;->a:LWB;

    sget-object v2, LWB;->b:LWB;

    invoke-direct {v0, v1, v2}, LWr;-><init>(LWB;LWB;)V

    sput-object v0, LWr;->b:LWr;

    .line 53
    new-instance v0, LWr;

    sget-object v1, LWB;->b:LWB;

    sget-object v2, LWB;->a:LWB;

    invoke-direct {v0, v1, v2}, LWr;-><init>(LWB;LWB;)V

    sput-object v0, LWr;->c:LWr;

    .line 59
    new-instance v0, LWr;

    sget-object v1, LWB;->b:LWB;

    sget-object v2, LWB;->b:LWB;

    invoke-direct {v0, v1, v2}, LWr;-><init>(LWB;LWB;)V

    sput-object v0, LWr;->d:LWr;

    return-void
.end method

.method constructor <init>(LWB;LWB;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWB;

    iput-object v0, p0, LWr;->a:LWB;

    .line 30
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWB;

    iput-object v0, p0, LWr;->b:LWB;

    .line 31
    return-void
.end method

.method public static a(Ljava/lang/String;)LWr;
    .registers 4
    .parameter

    .prologue
    .line 66
    new-instance v0, LWr;

    invoke-static {p0}, LWB;->a(Ljava/lang/String;)LWB;

    move-result-object v1

    sget-object v2, LWB;->b:LWB;

    invoke-direct {v0, v1, v2}, LWr;-><init>(LWB;LWB;)V

    return-object v0
.end method

.method static a(Ljava/util/Date;Ljava/util/Date;LkZ;)LWr;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    new-instance v0, LWr;

    invoke-static {p0, p2}, LWB;->a(Ljava/util/Date;LkZ;)LWB;

    move-result-object v1

    invoke-static {p1, p2}, LWB;->a(Ljava/util/Date;LkZ;)LWB;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LWr;-><init>(LWB;LWB;)V

    return-object v0
.end method

.method public static a(LmK;Ljava/lang/String;)LWr;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 122
    sget-object v0, LWr;->a:LWr;

    .line 123
    invoke-virtual {p0}, LmK;->a()LWr;

    move-result-object v1

    invoke-virtual {v0, v1}, LWr;->a(LWr;)LWr;

    move-result-object v0

    .line 124
    if-eqz p1, :cond_14

    .line 125
    invoke-static {p1}, LWr;->c(Ljava/lang/String;)LWr;

    move-result-object v1

    invoke-virtual {v0, v1}, LWr;->a(LWr;)LWr;

    move-result-object v0

    .line 128
    :cond_14
    return-object v0
.end method

.method public static b(Ljava/lang/String;)LWr;
    .registers 3
    .parameter

    .prologue
    .line 76
    invoke-static {p0}, LWB;->a(Ljava/lang/String;)LWB;

    move-result-object v0

    .line 77
    new-instance v1, LWr;

    invoke-direct {v1, v0, v0}, LWr;-><init>(LWB;LWB;)V

    return-object v1
.end method

.method public static c(Ljava/lang/String;)LWr;
    .registers 3
    .parameter

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-static {p0, v0}, LWB;->a(Ljava/lang/String;Z)LWB;

    move-result-object v0

    .line 86
    new-instance v1, LWr;

    invoke-direct {v1, v0, v0}, LWr;-><init>(LWB;LWB;)V

    return-object v1
.end method

.method public static d(Ljava/lang/String;)LWr;
    .registers 4
    .parameter

    .prologue
    .line 93
    invoke-static {p0}, LWB;->b(Ljava/lang/String;)LWB;

    move-result-object v0

    .line 94
    new-instance v1, LWr;

    sget-object v2, LWB;->b:LWB;

    invoke-direct {v1, v0, v2}, LWr;-><init>(LWB;LWB;)V

    return-object v1
.end method


# virtual methods
.method public a(LWr;)LWr;
    .registers 6
    .parameter

    .prologue
    .line 34
    new-instance v0, LWr;

    iget-object v1, p0, LWr;->a:LWB;

    iget-object v2, p1, LWr;->a:LWB;

    invoke-virtual {v1, v2}, LWB;->a(LWB;)LWB;

    move-result-object v1

    iget-object v2, p0, LWr;->b:LWB;

    iget-object v3, p1, LWr;->b:LWB;

    invoke-virtual {v2, v3}, LWB;->a(LWB;)LWB;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LWr;-><init>(LWB;LWB;)V

    return-object v0
.end method
