.class public final LWu;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LWK;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LWs;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LWt;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LWL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 34
    iput-object p1, p0, LWu;->a:LYD;

    .line 35
    const-class v0, LWK;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LWu;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LWu;->a:LZb;

    .line 38
    const-class v0, LWs;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LWu;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LWu;->b:LZb;

    .line 41
    const-class v0, LWt;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LWu;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LWu;->c:LZb;

    .line 44
    const-class v0, LWL;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LWu;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LWu;->d:LZb;

    .line 47
    return-void
.end method

.method static synthetic a(LWu;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LWu;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWu;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWu;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWu;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWu;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWu;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LWu;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 54
    const-class v0, LWK;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LWu;->a:LZb;

    invoke-virtual {p0, v0, v1}, LWu;->a(Laop;LZb;)V

    .line 55
    const-class v0, LWs;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LWu;->b:LZb;

    invoke-virtual {p0, v0, v1}, LWu;->a(Laop;LZb;)V

    .line 56
    const-class v0, LWt;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LWu;->c:LZb;

    invoke-virtual {p0, v0, v1}, LWu;->a(Laop;LZb;)V

    .line 57
    const-class v0, LWL;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LWu;->d:LZb;

    invoke-virtual {p0, v0, v1}, LWu;->a(Laop;LZb;)V

    .line 58
    iget-object v0, p0, LWu;->a:LZb;

    iget-object v1, p0, LWu;->a:LYD;

    iget-object v1, v1, LYD;->a:LWu;

    iget-object v1, v1, LWu;->d:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 60
    iget-object v0, p0, LWu;->b:LZb;

    iget-object v1, p0, LWu;->a:LYD;

    iget-object v1, v1, LYD;->a:LWu;

    iget-object v1, v1, LWu;->c:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 62
    iget-object v0, p0, LWu;->c:LZb;

    new-instance v1, LWv;

    invoke-direct {v1, p0}, LWv;-><init>(LWu;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 71
    iget-object v0, p0, LWu;->d:LZb;

    new-instance v1, LWw;

    invoke-direct {v1, p0}, LWw;-><init>(LWu;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 110
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 114
    return-void
.end method
