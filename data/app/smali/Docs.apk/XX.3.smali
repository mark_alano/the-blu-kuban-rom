.class public LXX;
.super Ljava/lang/Object;
.source "SyncManager.java"


# instance fields
.field a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LLc;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LUL;

.field a:LWM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LWY;

.field private final a:LXY;

.field private final a:LXm;

.field a:LZj;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LaaJ;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LaaZ;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:Lev;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:Lin;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Llf;


# direct methods
.method public constructor <init>(LXY;LWY;Llf;LXm;LUL;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LXX;->a:Ljava/util/Set;

    .line 161
    iput-object p1, p0, LXX;->a:LXY;

    .line 162
    iput-object p3, p0, LXX;->a:Llf;

    .line 163
    iput-object p4, p0, LXX;->a:LXm;

    .line 164
    iput-object p2, p0, LXX;->a:LWY;

    .line 165
    iput-object p5, p0, LXX;->a:LUL;

    .line 166
    return-void
.end method

.method private a(Ljava/lang/String;)J
    .registers 6
    .parameter

    .prologue
    .line 272
    iget-object v0, p0, LXX;->a:Lev;

    invoke-interface {v0, p1}, Lev;->a(Ljava/lang/String;)Let;

    move-result-object v0

    .line 273
    const-string v1, "lastContentSyncMilliseconds"

    invoke-interface {v0, v1}, Let;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lagu;->b(Z)V

    .line 275
    const-string v1, "lastContentSyncMilliseconds"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Let;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Ljava/lang/String;J)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 265
    iget-object v0, p0, LXX;->a:Lev;

    invoke-interface {v0, p1}, Lev;->a(Ljava/lang/String;)Let;

    move-result-object v0

    .line 266
    const-string v1, "lastContentSyncMilliseconds"

    sget-object v2, LZF;->a:LZF;

    invoke-virtual {v2}, LZF;->a()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Let;->a(Ljava/lang/String;J)V

    .line 267
    iget-object v1, p0, LXX;->a:Lev;

    invoke-interface {v1, v0}, Lev;->a(Let;)V

    .line 268
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .registers 4
    .parameter

    .prologue
    .line 279
    iget-object v0, p0, LXX;->a:Lev;

    invoke-interface {v0, p1}, Lev;->a(Ljava/lang/String;)Let;

    move-result-object v0

    .line 280
    const-string v1, "lastContentSyncMilliseconds"

    invoke-interface {v0, v1}, Let;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private a(LkB;Landroid/content/SyncResult;LYx;)Z
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 386
    .line 388
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 390
    :try_start_4
    iget-object v0, p0, LXX;->a:LXY;

    invoke-interface {v0}, LXY;->a()LXf;

    move-result-object v0

    .line 391
    invoke-interface {p3, v0, p1, p2}, LYx;->a(LXf;LkB;Landroid/content/SyncResult;)V

    .line 392
    invoke-interface {v0, p2}, LXf;->a(Landroid/content/SyncResult;)V

    .line 394
    const/4 v0, 0x1

    .line 396
    invoke-interface {p3, p2, v0}, LYx;->a(Landroid/content/SyncResult;Z)V
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_3d

    .line 398
    const-string v3, "SyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Time taken to sync doc list only "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v1, v5, v1

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    return v0

    .line 398
    :catchall_3d
    move-exception v0

    const-string v3, "SyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Time taken to sync doc list only "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v1, v5, v1

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    throw v0
.end method

.method private a(ZLkB;Landroid/content/SyncResult;)Z
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 336
    invoke-virtual {p2}, LkB;->b()Ljava/lang/String;

    move-result-object v2

    .line 338
    iget-object v3, p0, LXX;->a:LKS;

    const-string v4, "changelogSyncLimit"

    const/16 v5, 0x9c4

    invoke-interface {v3, v4, v5}, LKS;->a(Ljava/lang/String;I)I

    move-result v3

    .line 340
    iget-object v4, p0, LXX;->a:LWM;

    invoke-virtual {p2}, LkB;->a()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v4, v2, v5, v6}, LWM;->a(Ljava/lang/String;II)LVR;

    move-result-object v4

    .line 345
    invoke-virtual {v4}, LVR;->a()Z

    move-result v2

    if-nez v2, :cond_30

    .line 346
    iget-object v1, p0, LXX;->a:LeQ;

    const-string v2, "sync"

    const-string v3, "error"

    const-string v4, "Error fetching remainingChangestamps"

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    :cond_2f
    :goto_2f
    return v0

    .line 351
    :cond_30
    if-nez p1, :cond_38

    invoke-virtual {v4}, LVR;->b()I

    move-result v2

    if-le v2, v3, :cond_6b

    :cond_38
    move v2, v1

    .line 354
    :goto_39
    if-eqz v2, :cond_6d

    .line 355
    iget-object v0, p0, LXX;->a:LKS;

    const-string v1, "syncstarMaxFeedsToRetrieve"

    const/16 v3, 0x14

    invoke-interface {v0, v1, v3}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    .line 357
    new-instance v1, LYw;

    iget-object v3, p0, LXX;->a:LWY;

    iget-object v5, p0, LXX;->a:Llf;

    iget-object v6, p0, LXX;->a:LXm;

    invoke-direct {v1, v3, v5, v6, v0}, LYw;-><init>(LWY;Llf;LXm;I)V

    .line 359
    invoke-direct {p0, p2, p3, v1}, LXX;->a(LkB;Landroid/content/SyncResult;LYx;)Z

    move-result v0

    .line 370
    :goto_54
    if-eqz v0, :cond_2f

    .line 371
    if-eqz v2, :cond_5f

    .line 375
    invoke-virtual {v4}, LVR;->a()I

    move-result v1

    invoke-virtual {p2, v1}, LkB;->a(I)V

    .line 377
    :cond_5f
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p2, v1}, LkB;->a(Ljava/util/Date;)V

    .line 378
    invoke-virtual {p2}, LkB;->c()V

    goto :goto_2f

    :cond_6b
    move v2, v0

    .line 351
    goto :goto_39

    .line 360
    :cond_6d
    invoke-virtual {v4}, LVR;->b()I

    move-result v0

    if-lez v0, :cond_81

    .line 364
    new-instance v0, LYv;

    iget-object v1, p0, LXX;->a:LWY;

    iget-object v3, p0, LXX;->a:LXm;

    invoke-direct {v0, v1, v3}, LYv;-><init>(LWY;LXm;)V

    .line 365
    invoke-direct {p0, p2, p3, v0}, LXX;->a(LkB;Landroid/content/SyncResult;LYx;)Z

    move-result v0

    goto :goto_54

    :cond_81
    move v0, v1

    .line 367
    goto :goto_54
.end method

.method private b(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 170
    :try_start_0
    iget-object v0, p0, LXX;->a:LLc;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, LLc;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch LLd; {:try_start_0 .. :try_end_6} :catch_7

    .line 176
    :goto_6
    return-void

    .line 171
    :catch_7
    move-exception v0

    .line 172
    const-string v1, "SyncManager"

    const-string v2, "ClientFlagSyncException"

    invoke-static {v1, v2, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 173
    iget-object v0, p0, LXX;->a:LeQ;

    const-string v1, "sync"

    const-string v2, "error"

    const-string v3, "ClientFlagSyncException"

    invoke-virtual {v0, v1, v2, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6
.end method


# virtual methods
.method public a()V
    .registers 6

    .prologue
    .line 183
    iget-object v0, p0, LXX;->a:LME;

    invoke-interface {v0}, LME;->a()[Landroid/accounts/Account;

    move-result-object v1

    .line 184
    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_22

    aget-object v3, v1, v0

    .line 185
    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 186
    iget-object v4, p0, LXX;->a:LZj;

    invoke-interface {v4, v3}, LZj;->a(Ljava/lang/String;)LZi;

    move-result-object v4

    .line 188
    invoke-virtual {v4}, LZi;->a()Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 190
    iget-object v4, p0, LXX;->a:LWM;

    invoke-virtual {v4, v3}, LWM;->a(Ljava/lang/String;)LVR;

    .line 184
    :cond_1f
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 193
    :cond_22
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Landroid/content/SyncResult;)V
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 199
    const-string v0, "SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "in performSync for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v0, p0, LXX;->a:Llf;

    invoke-interface {v0, p2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v4

    .line 201
    invoke-virtual {v4, v1}, LkB;->a(Z)V

    .line 202
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/docs/providers/DocListProvider;->i:Landroid/net/Uri;

    invoke-virtual {v0, v3, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 207
    invoke-virtual {v4}, LkB;->c()V

    .line 209
    invoke-virtual {v4}, LkB;->a()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v0, v5, v7

    if-nez v0, :cond_5d

    move v0, v1

    .line 213
    :goto_3f
    :try_start_3f
    invoke-direct {p0, p2}, LXX;->b(Ljava/lang/String;)V

    .line 216
    iget-object v3, p0, LXX;->a:Lin;

    invoke-interface {v3, p1}, Lin;->a(Landroid/content/Context;)Z
    :try_end_47
    .catchall {:try_start_3f .. :try_end_47} :catchall_f4

    move-result v3

    if-nez v3, :cond_5f

    .line 252
    invoke-virtual {v4, v2}, LkB;->a(Z)V

    .line 253
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/docs/providers/DocListProvider;->i:Landroid/net/Uri;

    invoke-virtual {v0, v2, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 254
    invoke-virtual {v4, v1}, LkB;->b(Z)V

    .line 255
    invoke-virtual {v4}, LkB;->c()V

    .line 257
    :goto_5c
    return-void

    :cond_5d
    move v0, v2

    .line 209
    goto :goto_3f

    .line 220
    :cond_5f
    :try_start_5f
    invoke-virtual {p0}, LXX;->a()V

    .line 221
    invoke-direct {p0, v0, v4, p3}, LXX;->a(ZLkB;Landroid/content/SyncResult;)Z
    :try_end_65
    .catchall {:try_start_5f .. :try_end_65} :catchall_f4

    move-result v3

    .line 226
    :try_start_66
    iget-object v0, p0, LXX;->a:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v5

    .line 227
    sget-object v0, LZF;->a:LZF;

    invoke-virtual {v0}, LZF;->a()J

    move-result-wide v6

    .line 228
    iget-object v0, p0, LXX;->a:LKS;

    const-string v8, "autoContentSyncIntervalSeconds"

    const/16 v9, 0x5460

    invoke-interface {v0, v8, v9}, LKS;->a(Ljava/lang/String;I)I
    :try_end_7b
    .catchall {:try_start_66 .. :try_end_7b} :catchall_da

    move-result v0

    .line 233
    :try_start_7c
    invoke-direct {p0, p2}, LXX;->a(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8f

    invoke-direct {p0, p2}, LXX;->a(Ljava/lang/String;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8
    :try_end_8a
    .catchall {:try_start_7c .. :try_end_8a} :catchall_da
    .catch Lew; {:try_start_7c .. :try_end_8a} :catch_c7

    int-to-long v8, v0

    cmp-long v0, v6, v8

    if-lez v0, :cond_c5

    :cond_8f
    move v0, v1

    .line 240
    :goto_90
    :try_start_90
    iget-object v6, p0, LXX;->a:LaaJ;

    invoke-interface {v6}, LaaJ;->a()LaaM;

    move-result-object v6

    .line 241
    invoke-static {p1}, LZJ;->a(Landroid/content/Context;)LZK;

    move-result-object v7

    .line 242
    invoke-virtual {v6, v7}, LaaM;->a(LZK;)Z

    move-result v6

    .line 243
    if-nez v5, :cond_a4

    if-eqz v0, :cond_b0

    if-eqz v6, :cond_b0

    .line 244
    :cond_a4
    invoke-virtual {p0, p1, p2, v5}, LXX;->a(Landroid/content/Context;Ljava/lang/String;Z)V
    :try_end_a7
    .catchall {:try_start_90 .. :try_end_a7} :catchall_da

    .line 246
    :try_start_a7
    sget-object v0, LZF;->a:LZF;

    invoke-virtual {v0}, LZF;->a()J

    move-result-wide v5

    invoke-direct {p0, p2, v5, v6}, LXX;->a(Ljava/lang/String;J)V
    :try_end_b0
    .catchall {:try_start_a7 .. :try_end_b0} :catchall_da
    .catch Lew; {:try_start_a7 .. :try_end_b0} :catch_d1

    .line 252
    :cond_b0
    :goto_b0
    invoke-virtual {v4, v2}, LkB;->a(Z)V

    .line 253
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/docs/providers/DocListProvider;->i:Landroid/net/Uri;

    invoke-virtual {v0, v5, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 254
    if-nez v3, :cond_f0

    :goto_be
    invoke-virtual {v4, v1}, LkB;->b(Z)V

    .line 255
    invoke-virtual {v4}, LkB;->c()V

    goto :goto_5c

    :cond_c5
    move v0, v2

    .line 233
    goto :goto_90

    .line 236
    :catch_c7
    move-exception v0

    .line 237
    :try_start_c8
    const-string v6, "SyncManager"

    const-string v7, "Failed to get lastContentSyncMillisecond"

    invoke-static {v6, v7, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    goto :goto_90

    .line 247
    :catch_d1
    move-exception v0

    .line 248
    const-string v5, "SyncManager"

    const-string v6, "Failed to store lastContentSyncMilliseconds"

    invoke-static {v5, v6, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d9
    .catchall {:try_start_c8 .. :try_end_d9} :catchall_da

    goto :goto_b0

    .line 252
    :catchall_da
    move-exception v0

    :goto_db
    invoke-virtual {v4, v2}, LkB;->a(Z)V

    .line 253
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/docs/providers/DocListProvider;->i:Landroid/net/Uri;

    invoke-virtual {v5, v6, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 254
    if-nez v3, :cond_f2

    :goto_e9
    invoke-virtual {v4, v1}, LkB;->b(Z)V

    .line 255
    invoke-virtual {v4}, LkB;->c()V

    throw v0

    :cond_f0
    move v1, v2

    .line 254
    goto :goto_be

    :cond_f2
    move v1, v2

    goto :goto_e9

    .line 252
    :catchall_f4
    move-exception v0

    move v3, v2

    goto :goto_db
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;Z)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 309
    iget-object v0, p0, LXX;->a:LaaZ;

    invoke-interface {v0}, LaaZ;->b()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-static {p1}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 310
    iget-object v0, p0, LXX;->a:Llf;

    invoke-interface {v0, p2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v3

    .line 311
    iget-object v0, p0, LXX;->a:Llf;

    invoke-interface {v0, v3}, Llf;->b(LkB;)Ljava/util/Set;

    move-result-object v0

    .line 312
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_20
    :goto_20
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 313
    iget-object v5, p0, LXX;->a:Llf;

    invoke-interface {v5, v3, v0}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v5

    .line 314
    if-eqz v5, :cond_61

    move v0, v1

    :goto_35
    invoke-static {v0}, Lagu;->b(Z)V

    .line 315
    iget-object v0, p0, LXX;->a:LUL;

    sget-object v6, LUK;->a:LUK;

    invoke-interface {v0, v5, v6}, LUL;->c(LkM;LUK;)Z

    move-result v0

    if-nez v0, :cond_20

    invoke-virtual {v5}, LkM;->d()J

    move-result-wide v6

    const-wide/16 v8, 0x5

    cmp-long v0, v6, v8

    if-gez v0, :cond_20

    .line 319
    if-nez p3, :cond_63

    iget-object v0, p0, LXX;->a:LUL;

    sget-object v6, LUK;->a:LUK;

    invoke-interface {v0, v5, v6}, LUL;->b(LkM;LUK;)Z

    move-result v0

    if-eqz v0, :cond_63

    move v0, v1

    .line 322
    :goto_59
    invoke-virtual {v5}, LkM;->a()LkY;

    move-result-object v5

    .line 323
    invoke-static {p1, v5, v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncService;->a(Landroid/content/Context;LkY;Z)V

    goto :goto_20

    :cond_61
    move v0, v2

    .line 314
    goto :goto_35

    :cond_63
    move v0, v2

    .line 319
    goto :goto_59

    .line 327
    :cond_65
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 260
    iget-object v0, p0, LXX;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 261
    return-void
.end method

.method public a(LkB;Z)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 289
    iget-object v0, p0, LXX;->a:Llf;

    invoke-interface {v0, p1}, Llf;->b(LkB;)Ljava/util/Set;

    move-result-object v0

    .line 290
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 291
    iget-object v2, p0, LXX;->a:Llf;

    invoke-interface {v2, p1, v0}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v0

    .line 292
    if-eqz v0, :cond_a

    .line 295
    if-eqz p2, :cond_2a

    invoke-virtual {v0}, LkM;->d()J

    move-result-wide v2

    const-wide/16 v4, 0x5

    cmp-long v2, v2, v4

    if-gez v2, :cond_a

    .line 298
    :cond_2a
    iget-object v2, p0, LXX;->a:LUL;

    sget-object v3, LUK;->a:LUK;

    invoke-interface {v2, v0, v3}, LUL;->b(LkM;LUK;)Z

    move-result v2

    if-eqz v2, :cond_3e

    iget-object v2, p0, LXX;->a:LUL;

    sget-object v3, LUK;->a:LUK;

    invoke-interface {v2, v0, v3}, LUL;->a(LkM;LUK;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 301
    :cond_3e
    const/4 v0, 0x1

    .line 304
    :goto_3f
    return v0

    :cond_40
    const/4 v0, 0x0

    goto :goto_3f
.end method
