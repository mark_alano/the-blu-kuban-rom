.class final LYJ;
.super Ljava/lang/Object;
.source "GellyInjector.java"

# interfaces
.implements Laoo;


# static fields
.field private static final a:Laou;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laou",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Laop",
            "<*>;",
            "LZb",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Laou",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LaoC;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Laop",
            "<*>;",
            "Laof",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 37
    new-instance v0, LYK;

    invoke-direct {v0}, LYK;-><init>()V

    sput-object v0, LYJ;->a:Laou;

    return-void
.end method

.method constructor <init>(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Laop",
            "<*>;",
            "LZb",
            "<*>;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Laou",
            "<*>;>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;",
            "LaoC;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Laop",
            "<*>;",
            "Laof",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {p1}, Lajk;->a(Ljava/util/Map;)Lajk;

    move-result-object v0

    iput-object v0, p0, LYJ;->a:Ljava/util/Map;

    .line 55
    invoke-static {p2}, Lajk;->a(Ljava/util/Map;)Lajk;

    move-result-object v0

    iput-object v0, p0, LYJ;->b:Ljava/util/Map;

    .line 56
    invoke-static {p3}, Lajk;->a(Ljava/util/Map;)Lajk;

    move-result-object v0

    iput-object v0, p0, LYJ;->c:Ljava/util/Map;

    .line 57
    invoke-static {p4}, Lajk;->a(Ljava/util/Map;)Lajk;

    move-result-object v0

    iput-object v0, p0, LYJ;->d:Ljava/util/Map;

    .line 58
    return-void
.end method


# virtual methods
.method public a(LaoL;)Laou;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LaoL",
            "<TT;>;)",
            "Laou",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 105
    invoke-virtual {p1}, LaoL;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, LYJ;->a(Ljava/lang/Class;)Laou;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;)Laou;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Laou",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, LYJ;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laou;

    .line 112
    if-nez v0, :cond_c

    .line 113
    sget-object v0, LYJ;->a:Laou;

    .line 115
    :cond_c
    return-object v0
.end method

.method public final a(Laop;)Laoz;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;)",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, LYJ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    .line 69
    if-nez v0, :cond_2c

    .line 70
    new-instance v0, LarB;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot find a provider for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LarB;-><init>(Ljava/lang/String;)V

    .line 71
    new-instance v1, Laoh;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {v1, v0}, Laoh;-><init>(Ljava/lang/Iterable;)V

    throw v1

    .line 73
    :cond_2c
    return-object v0
.end method

.method public final a(Laop;)Ljava/lang/Object;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {p1}, LZg;->a(Laop;)Laop;

    move-result-object v0

    .line 85
    if-eqz v0, :cond_b

    .line 87
    invoke-virtual {p0, v0}, LYJ;->a(Laop;)Laoz;

    move-result-object v0

    .line 90
    :goto_a
    return-object v0

    :cond_b
    invoke-virtual {p0, p1}, LYJ;->a(Laop;)Laoz;

    move-result-object v0

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    goto :goto_a
.end method

.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 78
    invoke-static {p1}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-virtual {p0, v0}, LYJ;->a(Laop;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 98
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, LYJ;->a(Ljava/lang/Class;)Laou;

    move-result-object v0

    .line 99
    invoke-interface {v0, p1}, Laou;->a(Ljava/lang/Object;)V

    .line 100
    return-void
.end method
