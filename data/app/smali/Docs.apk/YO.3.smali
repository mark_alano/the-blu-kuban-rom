.class abstract enum LYO;
.super Ljava/lang/Enum;
.source "GellyInjectorBuilderBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LYO;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LYO;

.field private static final synthetic a:[LYO;

.field public static final enum b:LYO;

.field public static final enum c:LYO;

.field public static final enum d:LYO;

.field public static final enum e:LYO;

.field public static final enum f:LYO;

.field public static final enum g:LYO;

.field public static final enum h:LYO;


# instance fields
.field private final a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "LYO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 64
    new-instance v0, LYP;

    const-string v1, "LOAD_USER_ELEMENTS"

    new-array v2, v5, [LYO;

    invoke-direct {v0, v1, v5, v2}, LYP;-><init>(Ljava/lang/String;I[LYO;)V

    sput-object v0, LYO;->a:LYO;

    .line 74
    new-instance v0, LYQ;

    const-string v1, "PROCESS_SCOPE_BINDING"

    new-array v2, v6, [LYO;

    sget-object v3, LYO;->a:LYO;

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v6, v2}, LYQ;-><init>(Ljava/lang/String;I[LYO;)V

    sput-object v0, LYO;->b:LYO;

    .line 90
    new-instance v0, LYR;

    const-string v1, "INITIALIZE_INJECTOR_STORE"

    new-array v2, v6, [LYO;

    sget-object v3, LYO;->b:LYO;

    aput-object v3, v2, v5

    invoke-direct {v0, v1, v7, v2}, LYR;-><init>(Ljava/lang/String;I[LYO;)V

    sput-object v0, LYO;->c:LYO;

    .line 105
    new-instance v0, LYS;

    const-string v1, "SET_INSTANCE_BINDING_INSTANCES"

    new-array v2, v7, [LYO;

    sget-object v3, LYO;->c:LYO;

    aput-object v3, v2, v5

    sget-object v3, LYO;->a:LYO;

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v8, v2}, LYS;-><init>(Ljava/lang/String;I[LYO;)V

    sput-object v0, LYO;->d:LYO;

    .line 138
    new-instance v0, LYT;

    const-string v1, "APPLY_SCOPE"

    new-array v2, v8, [LYO;

    sget-object v3, LYO;->a:LYO;

    aput-object v3, v2, v5

    sget-object v3, LYO;->c:LYO;

    aput-object v3, v2, v6

    sget-object v3, LYO;->b:LYO;

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v9, v2}, LYT;-><init>(Ljava/lang/String;I[LYO;)V

    sput-object v0, LYO;->e:LYO;

    .line 149
    new-instance v0, LYU;

    const-string v1, "CREATE_INJECTOR"

    const/4 v2, 0x5

    new-array v3, v9, [LYO;

    sget-object v4, LYO;->a:LYO;

    aput-object v4, v3, v5

    sget-object v4, LYO;->c:LYO;

    aput-object v4, v3, v6

    sget-object v4, LYO;->b:LYO;

    aput-object v4, v3, v7

    sget-object v4, LYO;->e:LYO;

    aput-object v4, v3, v8

    invoke-direct {v0, v1, v2, v3}, LYU;-><init>(Ljava/lang/String;I[LYO;)V

    sput-object v0, LYO;->f:LYO;

    .line 161
    new-instance v0, LYV;

    const-string v1, "POST_INITIALIZATION_INJECTIONS"

    const/4 v2, 0x6

    new-array v3, v6, [LYO;

    sget-object v4, LYO;->f:LYO;

    aput-object v4, v3, v5

    invoke-direct {v0, v1, v2, v3}, LYV;-><init>(Ljava/lang/String;I[LYO;)V

    sput-object v0, LYO;->g:LYO;

    .line 172
    new-instance v0, LYW;

    const-string v1, "PROCESS_LOOKUP_REQUESTS"

    const/4 v2, 0x7

    new-array v3, v7, [LYO;

    sget-object v4, LYO;->f:LYO;

    aput-object v4, v3, v5

    sget-object v4, LYO;->g:LYO;

    aput-object v4, v3, v6

    invoke-direct {v0, v1, v2, v3}, LYW;-><init>(Ljava/lang/String;I[LYO;)V

    sput-object v0, LYO;->h:LYO;

    .line 60
    const/16 v0, 0x8

    new-array v0, v0, [LYO;

    sget-object v1, LYO;->a:LYO;

    aput-object v1, v0, v5

    sget-object v1, LYO;->b:LYO;

    aput-object v1, v0, v6

    sget-object v1, LYO;->c:LYO;

    aput-object v1, v0, v7

    sget-object v1, LYO;->d:LYO;

    aput-object v1, v0, v8

    sget-object v1, LYO;->e:LYO;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LYO;->f:LYO;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LYO;->g:LYO;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LYO;->h:LYO;

    aput-object v2, v0, v1

    sput-object v0, LYO;->a:[LYO;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;I[LYO;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "LYO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 187
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LYO;->a:Ljava/util/Collection;

    .line 188
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;I[LYO;LYM;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, LYO;-><init>(Ljava/lang/String;I[LYO;)V

    return-void
.end method

.method private final a(Ljava/util/Collection;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LYO;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, LYO;->a:Ljava/util/Collection;

    invoke-interface {p1, v0}, Ljava/util/Collection;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LYO;->a:Ljava/util/Collection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Completed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 199
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LYO;
    .registers 2
    .parameter

    .prologue
    .line 60
    const-class v0, LYO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LYO;

    return-object v0
.end method

.method public static values()[LYO;
    .registers 1

    .prologue
    .line 60
    sget-object v0, LYO;->a:[LYO;

    invoke-virtual {v0}, [LYO;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LYO;

    return-object v0
.end method


# virtual methods
.method final a(LYL;)V
    .registers 3
    .parameter

    .prologue
    .line 191
    invoke-static {p1}, LYL;->a(LYL;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, LYO;->a(Ljava/util/Collection;)V

    .line 192
    invoke-virtual {p0, p1}, LYO;->b(LYL;)V

    .line 193
    invoke-static {p1}, LYL;->a(LYL;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 194
    return-void
.end method

.method abstract b(LYL;)V
.end method
