.class public final LZb;
.super Ljava/lang/Object;
.source "RuntimeProvider.java"

# interfaces
.implements Laoz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Laoz",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<TT;>;"
        }
    .end annotation
.end field

.field private a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<+TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z


# direct methods
.method public constructor <init>(Laop;Ljava/lang/Class;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laop;

    iput-object v0, p0, LZb;->a:Laop;

    .line 33
    iput-object p2, p0, LZb;->a:Ljava/lang/Class;

    .line 34
    return-void
.end method

.method private a()Laoz;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoz",
            "<+TT;>;"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, LZb;->a:Laoz;

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provider of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LZb;->a:Laop;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not initialized."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 92
    iget-object v0, p0, LZb;->a:Laoz;

    return-object v0

    .line 91
    :cond_26
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private static a(Laoz;)Laoz;
    .registers 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laoz",
            "<+TT;>;)",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 51
    return-object p0
.end method

.method private a()V
    .registers 4

    .prologue
    .line 82
    iget-boolean v0, p0, LZb;->a:Z

    if-nez v0, :cond_24

    const/4 v0, 0x1

    :goto_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Provider of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LZb;->a:Laop;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has already been scoped."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 83
    return-void

    .line 82
    :cond_24
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public a()Ljava/lang/Class;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, LZb;->a:Ljava/lang/Class;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0}, LZb;->a()Laoz;

    move-result-object v0

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a(LaoC;)V
    .registers 4
    .parameter

    .prologue
    .line 42
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, LZb;->a()V

    .line 44
    iget-object v0, p0, LZb;->a:Laop;

    invoke-direct {p0}, LZb;->a()Laoz;

    move-result-object v1

    invoke-static {v1}, LZb;->a(Laoz;)Laoz;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LaoC;->a(Laop;Laoz;)Laoz;

    move-result-object v0

    iput-object v0, p0, LZb;->a:Laoz;

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, LZb;->a:Z
    :try_end_17
    .catchall {:try_start_1 .. :try_end_17} :catchall_19

    .line 46
    monitor-exit p0

    return-void

    .line 42
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Laoz;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, LZb;->a()V

    .line 78
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p0, LZb;->a:Laoz;
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_e

    .line 79
    monitor-exit p0

    return-void

    .line 77
    :catchall_e
    move-exception v0

    monitor-exit p0

    throw v0
.end method
