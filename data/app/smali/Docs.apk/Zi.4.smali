.class public LZi;
.super Ljava/lang/Object;
.source "AccountCapability.java"


# instance fields
.field private final a:LVR;

.field private final a:Ljava/util/Date;


# direct methods
.method public constructor <init>(LVR;Ljava/util/Date;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, LZi;->a:LVR;

    .line 53
    iput-object p2, p0, LZi;->a:Ljava/util/Date;

    .line 54
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 114
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {v0}, LVR;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(LkP;)J
    .registers 4
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {v0, p1}, LVR;->a(LkP;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Set;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {v0, p1}, LVR;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a(LkO;)Ljava/util/Set;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkO;",
            ")",
            "Ljava/util/Set",
            "<",
            "LeG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {p1}, LkO;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LVR;->b(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public a()Lqu;
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {v0}, LVR;->a()Lqu;

    move-result-object v0

    invoke-static {v0}, LqP;->a(Lqu;)LqP;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 57
    sget-object v0, LkC;->a:Ljava/util/Date;

    iget-object v1, p0, LZi;->a:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {v0, p1}, LVR;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public b()J
    .registers 3

    .prologue
    .line 118
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {v0}, LVR;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 68
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {v0}, LVR;->a()Ljava/util/Set;

    move-result-object v0

    const-string v1, "upload_any"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .registers 3

    .prologue
    .line 82
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {v0}, LVR;->a()Ljava/util/Set;

    move-result-object v0

    const-string v1, "drive"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .registers 3

    .prologue
    .line 89
    iget-object v0, p0, LZi;->a:LVR;

    invoke-virtual {v0}, LVR;->a()Ljava/util/Set;

    move-result-object v0

    const-string v1, "drive_requested"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
