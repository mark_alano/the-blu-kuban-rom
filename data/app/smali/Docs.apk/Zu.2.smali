.class public LZu;
.super Ljava/lang/Object;
.source "BitmapManipulator.java"


# instance fields
.field private a:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    .line 36
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;)LZu;
    .registers 2
    .parameter

    .prologue
    .line 334
    new-instance v0, LZu;

    invoke-direct {v0, p0}, LZu;-><init>(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;I)LZu;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 350
    if-lt p1, v1, :cond_22

    move v0, v1

    :goto_5
    const-string v3, "Need sampleSize >= 1"

    invoke-static {v0, v3}, Lagu;->a(ZLjava/lang/Object;)V

    .line 352
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 353
    iput p1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 354
    const/4 v3, 0x0

    invoke-static {p0, v3, v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_24

    :goto_18
    const-string v2, "Failed decoding bitmap"

    invoke-static {v1, v2}, Lagu;->a(ZLjava/lang/Object;)V

    .line 357
    invoke-static {v0}, LZu;->a(Landroid/graphics/Bitmap;)LZu;

    move-result-object v0

    return-object v0

    :cond_22
    move v0, v2

    .line 350
    goto :goto_5

    :cond_24
    move v1, v2

    .line 356
    goto :goto_18
.end method

.method private a()Landroid/graphics/Bitmap$Config;
    .registers 2

    .prologue
    .line 279
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 280
    if-nez v0, :cond_a

    .line 281
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 283
    :cond_a
    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .registers 3
    .parameter

    .prologue
    .line 317
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_9

    .line 318
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 320
    :cond_9
    iput-object p1, p0, LZu;->a:Landroid/graphics/Bitmap;

    .line 321
    return-void
.end method

.method public static a(LZw;LZw;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 296
    invoke-virtual {p0}, LZw;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, LZw;->b()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 297
    invoke-virtual {p1}, LZw;->a()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, LZw;->b()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 299
    cmpl-float v0, v0, v1

    if-lez v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method


# virtual methods
.method public a()LZu;
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 74
    const-string v0, "BitmapManipulator"

    const-string v1, "Converting bitmap to grayscale"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 77
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 79
    new-instance v2, Landroid/graphics/ColorMatrix;

    invoke-direct {v2}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 80
    invoke-virtual {v2, v4}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 81
    new-instance v3, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v3, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 82
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 83
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 85
    iget-object v3, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 86
    invoke-direct {p0, v0}, LZu;->a(Landroid/graphics/Bitmap;)V

    .line 87
    return-object p0
.end method

.method public a(FI)LZu;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 100
    cmpl-float v0, p1, v6

    if-nez v0, :cond_6

    .line 121
    :goto_5
    return-object p0

    .line 104
    :cond_6
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-direct {p0}, LZu;->a()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 106
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 108
    new-instance v2, Landroid/graphics/ColorMatrix;

    const/16 v3, 0x14

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v6, v3, v4

    const/4 v4, 0x1

    aput v6, v3, v4

    const/4 v4, 0x2

    aput v6, v3, v4

    const/4 v4, 0x3

    aput v6, v3, v4

    const/4 v4, 0x4

    invoke-static {p2}, Landroid/graphics/Color;->red(I)I

    move-result v5

    int-to-float v5, v5

    aput v5, v3, v4

    const/4 v4, 0x5

    aput v6, v3, v4

    const/4 v4, 0x6

    aput v6, v3, v4

    const/4 v4, 0x7

    aput v6, v3, v4

    const/16 v4, 0x8

    aput v6, v3, v4

    const/16 v4, 0x9

    invoke-static {p2}, Landroid/graphics/Color;->green(I)I

    move-result v5

    int-to-float v5, v5

    aput v5, v3, v4

    const/16 v4, 0xa

    aput v6, v3, v4

    const/16 v4, 0xb

    aput v6, v3, v4

    const/16 v4, 0xc

    aput v6, v3, v4

    const/16 v4, 0xd

    aput v6, v3, v4

    const/16 v4, 0xe

    invoke-static {p2}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    int-to-float v5, v5

    aput v5, v3, v4

    const/16 v4, 0xf

    aput v6, v3, v4

    const/16 v4, 0x10

    aput v6, v3, v4

    const/16 v4, 0x11

    aput v6, v3, v4

    const/16 v4, 0x12

    const/high16 v5, 0x3f80

    div-float/2addr v5, p1

    aput v5, v3, v4

    const/16 v4, 0x13

    aput v6, v3, v4

    invoke-direct {v2, v3}, Landroid/graphics/ColorMatrix;-><init>([F)V

    .line 115
    new-instance v3, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v3, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 116
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 117
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 119
    iget-object v3, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 120
    invoke-direct {p0, v0}, LZu;->a(Landroid/graphics/Bitmap;)V

    goto/16 :goto_5
.end method

.method public a(I)LZu;
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 42
    rem-int/lit16 v0, p1, 0x168

    if-eqz v0, :cond_38

    .line 43
    const-string v0, "BitmapManipulator"

    const-string v2, "Rotating image by %d deg"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 45
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 47
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 48
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 50
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, LZu;->a(Landroid/graphics/Bitmap;)V

    .line 52
    :cond_38
    return-object p0
.end method

.method public a(LZw;I)LZu;
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 245
    .line 248
    invoke-virtual {p0}, LZu;->a()LZw;

    move-result-object v0

    invoke-static {v0, p1}, LZu;->a(LZw;LZw;)Z

    move-result v0

    if-eqz v0, :cond_71

    .line 252
    invoke-virtual {p1}, LZw;->a()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 253
    invoke-virtual {p1}, LZw;->b()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    move v2, v1

    .line 262
    :goto_2b
    invoke-virtual {p1}, LZw;->a()I

    move-result v3

    invoke-virtual {p1}, LZw;->b()I

    move-result v4

    invoke-direct {p0}, LZu;->a()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 265
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 266
    invoke-virtual {v3, p2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 267
    iget-object v5, p0, LZu;->a:Landroid/graphics/Bitmap;

    new-instance v6, Landroid/graphics/Rect;

    iget-object v7, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    iget-object v8, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    invoke-direct {v6, v1, v1, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {p1}, LZw;->a()I

    move-result v7

    sub-int/2addr v7, v2

    invoke-virtual {p1}, LZw;->b()I

    move-result v8

    sub-int/2addr v8, v0

    invoke-direct {v1, v2, v0, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v4, v5, v6, v1, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 274
    invoke-direct {p0, v3}, LZu;->a(Landroid/graphics/Bitmap;)V

    .line 275
    return-object p0

    .line 258
    :cond_71
    invoke-virtual {p1}, LZw;->b()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 259
    invoke-virtual {p1}, LZw;->a()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    sub-float v0, v2, v0

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    move v2, v0

    move v0, v1

    goto :goto_2b
.end method

.method public a(Ljava/io/OutputStream;I)LZu;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v0, v1, p2, p1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 179
    return-object p0
.end method

.method public a()LZw;
    .registers 4

    .prologue
    .line 306
    new-instance v0, LZw;

    iget-object v1, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, LZw;-><init>(II)V

    return-object v0
.end method

.method public a()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 327
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 313
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LZu;->a(Landroid/graphics/Bitmap;)V

    .line 314
    return-void
.end method

.method public b(I)LZu;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 163
    iget-object v0, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-direct {p0}, LZu;->a()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 165
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 167
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 168
    invoke-virtual {v2, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 169
    iget-object v3, p0, LZu;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3, v4, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 170
    invoke-direct {p0, v0}, LZu;->a(Landroid/graphics/Bitmap;)V

    .line 171
    return-object p0
.end method
