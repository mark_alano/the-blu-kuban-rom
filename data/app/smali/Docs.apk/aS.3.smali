.class public LaS;
.super Ljava/lang/Object;
.source "ViewCompat.java"


# static fields
.field static final a:LaY;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 231
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 232
    const/16 v1, 0x10

    if-ge v0, v1, :cond_10

    sget-object v1, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    const-string v2, "JellyBean"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 233
    :cond_10
    new-instance v0, LaX;

    invoke-direct {v0}, LaX;-><init>()V

    sput-object v0, LaS;->a:LaY;

    .line 243
    :goto_17
    return-void

    .line 234
    :cond_18
    const/16 v1, 0xe

    if-lt v0, v1, :cond_24

    .line 235
    new-instance v0, LaW;

    invoke-direct {v0}, LaW;-><init>()V

    sput-object v0, LaS;->a:LaY;

    goto :goto_17

    .line 236
    :cond_24
    const/16 v1, 0xb

    if-lt v0, v1, :cond_30

    .line 237
    new-instance v0, LaV;

    invoke-direct {v0}, LaV;-><init>()V

    sput-object v0, LaS;->a:LaY;

    goto :goto_17

    .line 238
    :cond_30
    const/16 v1, 0x9

    if-lt v0, v1, :cond_3c

    .line 239
    new-instance v0, LaU;

    invoke-direct {v0}, LaU;-><init>()V

    sput-object v0, LaS;->a:LaY;

    goto :goto_17

    .line 241
    :cond_3c
    new-instance v0, LaT;

    invoke-direct {v0}, LaT;-><init>()V

    sput-object v0, LaS;->a:LaY;

    goto :goto_17
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    return-void
.end method

.method public static a(Landroid/view/View;)I
    .registers 2
    .parameter

    .prologue
    .line 277
    sget-object v0, LaS;->a:LaY;

    invoke-interface {v0, p0}, LaY;->a(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 450
    sget-object v0, LaS;->a:LaY;

    invoke-interface {v0, p0}, LaY;->a(Landroid/view/View;)V

    .line 451
    return-void
.end method

.method public static a(Landroid/view/View;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 531
    sget-object v0, LaS;->a:LaY;

    invoke-interface {v0, p0, p1}, LaY;->a(Landroid/view/View;I)V

    .line 532
    return-void
.end method

.method public static a(Landroid/view/View;Lag;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 414
    sget-object v0, LaS;->a:LaY;

    invoke-interface {v0, p0, p1}, LaY;->a(Landroid/view/View;Lag;)V

    .line 415
    return-void
.end method

.method public static a(Landroid/view/View;I)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 253
    sget-object v0, LaS;->a:LaY;

    invoke-interface {v0, p0, p1}, LaY;->a(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;)I
    .registers 2
    .parameter

    .prologue
    .line 515
    sget-object v0, LaS;->a:LaY;

    invoke-interface {v0, p0}, LaY;->b(Landroid/view/View;)I

    move-result v0

    return v0
.end method
