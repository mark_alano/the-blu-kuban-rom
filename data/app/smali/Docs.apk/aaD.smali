.class public LaaD;
.super Ljava/lang/Object;
.source "OcrImageEvaluator.java"


# instance fields
.field a:Laax;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field a:LrU;
    .annotation runtime Laon;
    .end annotation
.end field

.field b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method private a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 74
    new-instance v2, LrM;

    iget-object v0, p0, LaaD;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, LaaD;->b:Laoz;

    invoke-direct {v2, v0, v3}, LrM;-><init>(Landroid/content/ContentResolver;Laoz;)V

    const-string v0, "image/jpeg"

    invoke-virtual {v2, p1, v0}, LrM;->a(Landroid/net/Uri;Ljava/lang/String;)LrM;

    move-result-object v0

    invoke-virtual {v0, v4}, LrM;->a(Z)LrM;

    move-result-object v0

    invoke-virtual {v0}, LrM;->a()LrK;

    move-result-object v0

    .line 79
    :try_start_23
    invoke-virtual {v0}, LrK;->a()LrK;
    :try_end_26
    .catch Lrw; {:try_start_23 .. :try_end_26} :catch_46
    .catch LrT; {:try_start_23 .. :try_end_26} :catch_4f

    move-result-object v2

    .line 92
    :try_start_27
    iget-object v0, p0, LaaD;->a:LrU;

    invoke-interface {v0, v2}, LrU;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LrK;
    :try_end_2f
    .catchall {:try_start_27 .. :try_end_2f} :catchall_62

    .line 95
    :try_start_2f
    new-instance v2, Ljava/io/FileInputStream;

    invoke-virtual {v0}, LrK;->a()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_38
    .catchall {:try_start_2f .. :try_end_38} :catchall_68
    .catch Ljava/io/FileNotFoundException; {:try_start_2f .. :try_end_38} :catch_57

    .line 100
    const/4 v1, 0x1

    :try_start_39
    invoke-static {v2, v1}, LZu;->a(Ljava/io/InputStream;I)LZu;

    move-result-object v1

    invoke-virtual {v1}, LZu;->a()Landroid/graphics/Bitmap;
    :try_end_40
    .catchall {:try_start_39 .. :try_end_40} :catchall_68

    move-result-object v1

    .line 102
    invoke-virtual {v0}, LrK;->a()V

    move-object v0, v1

    :goto_45
    return-object v0

    .line 80
    :catch_46
    move-exception v0

    .line 81
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "ItemToUpload hasn\'t been published, so how can it be canceled?"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 82
    :catch_4f
    move-exception v0

    .line 83
    const-string v2, "OcrImageEvaluator"

    invoke-static {v2, v0}, Laaz;->a(Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 84
    goto :goto_45

    .line 96
    :catch_57
    move-exception v2

    .line 97
    :try_start_58
    const-string v3, "OcrImageEvaluator"

    invoke-static {v3, v2}, Laaz;->a(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5d
    .catchall {:try_start_58 .. :try_end_5d} :catchall_68

    .line 102
    invoke-virtual {v0}, LrK;->a()V

    move-object v0, v1

    goto :goto_45

    :catchall_62
    move-exception v0

    move-object v1, v2

    :goto_64
    invoke-virtual {v1}, LrK;->a()V

    throw v0

    :catchall_68
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_64
.end method


# virtual methods
.method public a(Landroid/net/Uri;)LaaE;
    .registers 7
    .parameter

    .prologue
    .line 54
    new-instance v0, LagC;

    invoke-direct {v0}, LagC;-><init>()V

    .line 56
    sget-object v1, LaaE;->a:LaaE;

    .line 57
    invoke-virtual {v0}, LagC;->a()LagC;

    .line 58
    invoke-direct {p0, p1}, LaaD;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 59
    invoke-virtual {v0}, LagC;->b()LagC;

    .line 60
    const-string v2, "OcrImageEvaluator"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "toBitmap took "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    if-eqz v1, :cond_58

    .line 62
    invoke-virtual {v0}, LagC;->c()LagC;

    move-result-object v2

    invoke-virtual {v2}, LagC;->a()LagC;

    .line 63
    iget-object v2, p0, LaaD;->a:Laax;

    invoke-virtual {v2, v1}, Laax;->a(Landroid/graphics/Bitmap;)Z

    move-result v1

    .line 64
    invoke-virtual {v0}, LagC;->b()LagC;

    .line 65
    const-string v2, "OcrImageEvaluator"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isBlurred took "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    if-eqz v1, :cond_58

    .line 67
    sget-object v0, LaaE;->b:LaaE;

    .line 70
    :goto_57
    return-object v0

    :cond_58
    sget-object v0, LaaE;->a:LaaE;

    goto :goto_57
.end method
