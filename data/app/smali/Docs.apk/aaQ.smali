.class public LaaQ;
.super Ljava/lang/Object;
.source "PreferenceUtilsImpl.java"

# interfaces
.implements LaaJ;


# static fields
.field private static final a:[I


# instance fields
.field private final a:LKS;

.field private a:LaaR;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lgl;

.field private a:Ljava/io/File;

.field private b:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 83
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, LaaQ;->a:[I

    return-void

    :array_a
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0xat 0x0t 0x0t 0x0t
        0x19t 0x0t 0x0t 0x0t
        0x32t 0x0t 0x0t 0x0t
        0x64t 0x0t 0x0t 0x0t
        0xfat 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Laoz;LKS;Lgl;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LKS;",
            "Lgl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, LaaQ;->a:LaaR;

    .line 106
    iput-object p1, p0, LaaQ;->a:Laoz;

    .line 107
    iput-object p2, p0, LaaQ;->a:LKS;

    .line 108
    iput-object p3, p0, LaaQ;->a:Lgl;

    .line 109
    return-void
.end method

.method private a()Landroid/content/SharedPreferences;
    .registers 2

    .prologue
    .line 113
    iget-object v0, p0, LaaQ;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 188
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 189
    invoke-direct {p0, v0}, LaaQ;->a(Ljava/io/File;)V

    .line 190
    return-object v0
.end method

.method private a(LaaR;)V
    .registers 5
    .parameter

    .prologue
    .line 313
    invoke-direct {p0}, LaaQ;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 314
    const-string v1, "shared_preferences.cache_size"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_14

    .line 316
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, LaaR;->a(I)V

    .line 319
    :cond_14
    const-string v0, "PreferenceUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cache size set to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LaaR;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    return-void
.end method

.method private a(Ljava/io/File;)V
    .registers 5
    .parameter

    .prologue
    .line 194
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 195
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 209
    :cond_c
    return-void

    .line 199
    :cond_d
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_36

    .line 200
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to create directory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - file exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_36
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    move-result v0

    if-nez v0, :cond_c

    .line 207
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to create cache directory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a()[I
    .registers 9

    .prologue
    const-wide/high16 v0, 0x3ff0

    const/4 v3, 0x0

    .line 254
    invoke-virtual {p0}, LaaQ;->b()J

    move-result-wide v4

    long-to-double v4, v4

    const-wide/high16 v6, 0x4164

    div-double/2addr v4, v6

    .line 256
    sget-object v2, LaaQ;->a:[I

    sget-object v6, LaaQ;->a:[I

    array-length v6, v6

    add-int/lit8 v6, v6, -0x1

    aget v2, v2, v6

    int-to-double v6, v2

    cmpl-double v2, v4, v6

    if-lez v2, :cond_23

    .line 257
    sget-object v2, LaaQ;->a:[I

    sget-object v4, LaaQ;->a:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget v2, v2, v4

    int-to-double v4, v2

    .line 261
    :cond_23
    const-wide v6, 0x408f400000000000L

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_4e

    .line 262
    const-wide v0, 0x406f400000000000L

    div-double v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    const-wide/16 v4, 0xfa

    mul-long/2addr v0, v4

    long-to-double v0, v0

    .line 275
    :cond_3b
    :goto_3b
    sget-object v2, LaaQ;->a:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    .line 277
    :goto_40
    sget-object v4, LaaQ;->a:[I

    aget v4, v4, v2

    int-to-double v4, v4

    cmpl-double v4, v4, v0

    if-lez v4, :cond_7e

    if-ltz v2, :cond_7e

    .line 278
    add-int/lit8 v2, v2, -0x1

    goto :goto_40

    .line 263
    :cond_4e
    const-wide/high16 v6, 0x4059

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_61

    .line 264
    const-wide/high16 v0, 0x4039

    div-double v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    const-wide/16 v4, 0x19

    mul-long/2addr v0, v4

    long-to-double v0, v0

    goto :goto_3b

    .line 265
    :cond_61
    const-wide/high16 v6, 0x4024

    cmpl-double v2, v4, v6

    if-ltz v2, :cond_74

    .line 266
    const-wide/high16 v0, 0x4014

    div-double v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    const-wide/16 v4, 0x5

    mul-long/2addr v0, v4

    long-to-double v0, v0

    goto :goto_3b

    .line 267
    :cond_74
    cmpl-double v2, v4, v0

    if-ltz v2, :cond_3b

    .line 268
    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-double v0, v0

    goto :goto_3b

    .line 283
    :cond_7e
    if-ltz v2, :cond_b8

    sget-object v4, LaaQ;->a:[I

    aget v4, v4, v2

    int-to-double v4, v4

    div-double v4, v0, v4

    const-wide/high16 v6, 0x3ff4

    cmpg-double v4, v4, v6

    if-gez v4, :cond_b8

    .line 286
    add-int/lit8 v2, v2, -0x1

    move v4, v2

    .line 289
    :goto_90
    if-gez v4, :cond_9c

    .line 291
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 292
    sget-object v1, LaaQ;->a:[I

    aget v1, v1, v3

    aput v1, v0, v3

    .line 309
    :goto_9b
    return-object v0

    .line 298
    :cond_9c
    add-int/lit8 v2, v4, -0x4

    add-int/lit8 v2, v2, 0x2

    .line 299
    if-gez v2, :cond_a3

    move v2, v3

    .line 304
    :cond_a3
    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, 0x2

    new-array v4, v4, [I

    .line 306
    array-length v5, v4

    add-int/lit8 v5, v5, -0x1

    double-to-int v0, v0

    aput v0, v4, v5

    .line 308
    sget-object v0, LaaQ;->a:[I

    array-length v1, v4

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v2, v4, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, v4

    .line 309
    goto :goto_9b

    :cond_b8
    move v4, v2

    goto :goto_90
.end method

.method private c()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 137
    iget-object v0, p0, LaaQ;->a:Ljava/io/File;

    if-nez v0, :cond_45

    const/4 v0, 0x1

    :goto_6
    const-string v2, "createFileCacheDir called while cacheDir exists"

    invoke-static {v0, v2}, Lagu;->b(ZLjava/lang/Object;)V

    .line 138
    iget-object v0, p0, LaaQ;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 144
    new-instance v2, Ljava/io/File;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string v4, "filecache2"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 151
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2d

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_2d

    .line 152
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 154
    :cond_2d
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_42

    .line 155
    const-string v3, "filecache2"

    invoke-virtual {v0, v3, v1}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 156
    invoke-virtual {v0, v2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    if-nez v1, :cond_42

    .line 162
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 165
    :cond_42
    iput-object v2, p0, LaaQ;->a:Ljava/io/File;

    .line 166
    return-void

    :cond_45
    move v0, v1

    .line 137
    goto :goto_6
.end method

.method private d()V
    .registers 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 169
    iget-object v0, p0, LaaQ;->b:Ljava/io/File;

    if-nez v0, :cond_29

    move v0, v1

    :goto_7
    const-string v3, "createPinDir called while pinDir exists"

    invoke-static {v0, v3}, Lagu;->b(ZLjava/lang/Object;)V

    .line 171
    iget-object v0, p0, LaaQ;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 172
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x8

    if-lt v3, v4, :cond_2b

    .line 174
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 181
    :goto_1f
    if-nez v0, :cond_44

    .line 182
    new-instance v0, Ljava/io/IOException;

    const-string v1, "External storage not ready"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_29
    move v0, v2

    .line 169
    goto :goto_7

    .line 176
    :cond_2b
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    .line 177
    new-instance v3, Ljava/io/File;

    const-string v5, "/Android/data/%s/files/"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v5, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v3

    goto :goto_1f

    .line 184
    :cond_44
    const-string v1, "pinned_docs_files_do_not_edit"

    invoke-direct {p0, v0, v1}, LaaQ;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, LaaQ;->b:Ljava/io/File;

    .line 185
    return-void
.end method


# virtual methods
.method public a()J
    .registers 5

    .prologue
    .line 229
    invoke-virtual {p0}, LaaQ;->a()LaaL;

    move-result-object v0

    .line 230
    invoke-interface {v0}, LaaL;->a()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x100000

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public a()LaaL;
    .registers 4

    .prologue
    .line 214
    iget-object v0, p0, LaaQ;->a:LaaR;

    if-eqz v0, :cond_7

    .line 215
    iget-object v0, p0, LaaQ;->a:LaaR;

    .line 223
    :goto_6
    return-object v0

    .line 218
    :cond_7
    invoke-direct {p0}, LaaQ;->a()[I

    move-result-object v0

    .line 219
    new-instance v1, LaaR;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget v2, v0, v2

    div-int/lit8 v2, v2, 0x3

    invoke-direct {v1, p0, v0, v2}, LaaR;-><init>(LaaQ;[II)V

    iput-object v1, p0, LaaQ;->a:LaaR;

    .line 222
    iget-object v0, p0, LaaQ;->a:LaaR;

    invoke-direct {p0, v0}, LaaQ;->a(LaaR;)V

    .line 223
    iget-object v0, p0, LaaQ;->a:LaaR;

    goto :goto_6
.end method

.method public a()LaaM;
    .registers 4

    .prologue
    .line 383
    invoke-direct {p0}, LaaQ;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "shared_preferences.pinned_files_auto_sync_options"

    sget-object v2, LaaM;->b:LaaM;

    invoke-virtual {v2}, LaaM;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 385
    invoke-static {v0}, LaaM;->a(Ljava/lang/String;)LaaM;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized a()Ljava/io/File;
    .registers 2

    .prologue
    .line 119
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaaQ;->a:Ljava/io/File;

    if-nez v0, :cond_8

    .line 120
    invoke-direct {p0}, LaaQ;->c()V

    .line 123
    :cond_8
    iget-object v0, p0, LaaQ;->a:Ljava/io/File;
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    monitor-exit p0

    return-object v0

    .line 119
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 236
    iget-object v0, p0, LaaQ;->a:LaaR;

    invoke-direct {p0, v0}, LaaQ;->a(LaaR;)V

    .line 237
    return-void
.end method

.method public a()Z
    .registers 4

    .prologue
    .line 241
    invoke-direct {p0}, LaaQ;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "shared_preferences.notify_newdoc"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a(LZK;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 364
    invoke-virtual {p0}, LaaQ;->a()LaaM;

    move-result-object v1

    .line 365
    sget-object v2, LZK;->c:LZK;

    invoke-virtual {p1, v2}, LZK;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 366
    invoke-virtual {v1, p1}, LaaM;->a(LZK;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 372
    :cond_13
    :goto_13
    return v0

    .line 369
    :cond_14
    invoke-direct {p0}, LaaQ;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "shared_preferences.warn_offline_sync_broadband"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_13
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 347
    if-nez p1, :cond_1f

    .line 348
    const-string v0, "%s.%s"

    new-array v3, v5, [Ljava/lang/Object;

    const-string v4, "shared_preferences.state"

    aput-object v4, v3, v2

    aput-object p2, v3, v1

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 353
    :goto_13
    invoke-direct {p0}, LaaQ;->a()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 354
    invoke-interface {v3, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_31

    move v0, v1

    .line 358
    :goto_1e
    return v0

    .line 350
    :cond_1f
    const-string v0, "%s.%s~%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "shared_preferences.state"

    aput-object v4, v3, v2

    aput-object p2, v3, v1

    aput-object p1, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_13

    .line 357
    :cond_31
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move v0, v2

    .line 358
    goto :goto_1e
.end method

.method b()J
    .registers 5

    .prologue
    .line 245
    invoke-virtual {p0}, LaaQ;->a()Ljava/io/File;

    move-result-object v0

    .line 246
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 247
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v2, v0

    .line 248
    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockCount()I

    move-result v0

    int-to-long v0, v0

    .line 249
    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public declared-synchronized b()Ljava/io/File;
    .registers 2

    .prologue
    .line 129
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, LaaQ;->b:Ljava/io/File;

    if-nez v0, :cond_8

    .line 130
    invoke-direct {p0}, LaaQ;->d()V

    .line 133
    :cond_8
    iget-object v0, p0, LaaQ;->b:Ljava/io/File;
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_c

    monitor-exit p0

    return-object v0

    .line 129
    :catchall_c
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()V
    .registers 4

    .prologue
    .line 378
    invoke-direct {p0}, LaaQ;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "shared_preferences.warn_offline_sync_broadband"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 379
    return-void
.end method

.method public b()Z
    .registers 3

    .prologue
    .line 324
    iget-object v0, p0, LaaQ;->a:Lgl;

    sget-object v1, Lgi;->p:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-static {}, LVj;->a()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public c()Z
    .registers 4

    .prologue
    .line 330
    invoke-direct {p0}, LaaQ;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "streaming_decryption"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .registers 6

    .prologue
    .line 335
    invoke-direct {p0}, LaaQ;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "additional_filters"

    iget-object v2, p0, LaaQ;->a:LKS;

    const-string v3, "additionalFiltersDefault"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .registers 4

    .prologue
    .line 341
    invoke-direct {p0}, LaaQ;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "enable_pin_encryption"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
