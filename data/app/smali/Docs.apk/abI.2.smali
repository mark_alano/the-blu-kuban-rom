.class public final LabI;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lacu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 31
    iput-object p1, p0, LabI;->a:LYD;

    .line 32
    const-class v0, Lacu;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LabI;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LabI;->a:LZb;

    .line 35
    return-void
.end method

.method static synthetic a(LabI;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LabI;->a:LYD;

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 194
    const-class v0, Lcom/google/android/apps/docs/view/SpeakerNotesPresence;

    new-instance v1, LabJ;

    invoke-direct {v1, p0}, LabJ;-><init>(LabI;)V

    invoke-virtual {p0, v0, v1}, LabI;->a(Ljava/lang/Class;Laou;)V

    .line 202
    const-class v0, Lcom/google/android/apps/docs/view/PinWarningDialogFragment;

    new-instance v1, LabK;

    invoke-direct {v1, p0}, LabK;-><init>(LabI;)V

    invoke-virtual {p0, v0, v1}, LabI;->a(Ljava/lang/Class;Laou;)V

    .line 210
    const-class v0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;

    new-instance v1, LabL;

    invoke-direct {v1, p0}, LabL;-><init>(LabI;)V

    invoke-virtual {p0, v0, v1}, LabI;->a(Ljava/lang/Class;Laou;)V

    .line 218
    const-class v0, Lcom/google/android/apps/docs/view/DocListView;

    new-instance v1, LabM;

    invoke-direct {v1, p0}, LabM;-><init>(LabI;)V

    invoke-virtual {p0, v0, v1}, LabI;->a(Ljava/lang/Class;Laou;)V

    .line 226
    const-class v0, Lcom/google/android/apps/docs/view/TitleBar;

    new-instance v1, LabN;

    invoke-direct {v1, p0}, LabN;-><init>(LabI;)V

    invoke-virtual {p0, v0, v1}, LabI;->a(Ljava/lang/Class;Laou;)V

    .line 234
    const-class v0, Lcom/google/android/apps/docs/view/ThumbnailView;

    new-instance v1, LabO;

    invoke-direct {v1, p0}, LabO;-><init>(LabI;)V

    invoke-virtual {p0, v0, v1}, LabI;->a(Ljava/lang/Class;Laou;)V

    .line 242
    const-class v0, Lacu;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LabI;->a:LZb;

    invoke-virtual {p0, v0, v1}, LabI;->a(Laop;LZb;)V

    .line 243
    iget-object v0, p0, LabI;->a:LZb;

    iget-object v1, p0, LabI;->a:LYD;

    iget-object v1, v1, LYD;->a:Lju;

    iget-object v1, v1, Lju;->a:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 245
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/DocListView;)V
    .registers 3
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LVc;

    iget-object v0, v0, LVc;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUL;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LUL;

    .line 73
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LeQ;

    .line 79
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:Lgl;

    .line 85
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXS;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LXS;

    .line 91
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlE;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LlE;

    .line 97
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LZM;

    .line 103
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LME;

    .line 109
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:Laoz;

    .line 115
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LKS;

    .line 121
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiX;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LiX;

    .line 127
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LWu;

    iget-object v0, v0, LWu;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWK;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LWK;

    .line 133
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->i:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaZ;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LaaZ;

    .line 139
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiG;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LiG;

    .line 145
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->n:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lji;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:Lji;

    .line 151
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:Llf;

    .line 157
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->F:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZE;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/DocListView;->a:LZE;

    .line 163
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/PinWarningDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 53
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/SpeakerNotesContent;)V
    .registers 3
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQR;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQR;

    .line 63
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/SpeakerNotesPresence;)V
    .registers 3
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LQp;

    iget-object v0, v0, LQp;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LQR;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/SpeakerNotesPresence;->a:LQR;

    .line 47
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/ThumbnailView;)V
    .registers 3
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->n:LZb;

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Laoz;

    .line 183
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LabI;

    iget-object v0, v0, LabI;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacu;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/ThumbnailView;->a:Lacu;

    .line 189
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/view/TitleBar;)V
    .registers 3
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, LabI;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LabI;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/view/TitleBar;->a:LZM;

    .line 173
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 249
    return-void
.end method
