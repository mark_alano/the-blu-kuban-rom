.class public LacB;
.super LML;
.source "PreHoneycombActionBarHelper.java"


# instance fields
.field private a:LMZ;

.field private final a:LNa;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;ILNa;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, LML;-><init>(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 59
    iput-object p4, p0, LacB;->a:LNa;

    .line 60
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 139
    iget-object v0, p0, LacB;->a:LMZ;

    if-eqz v0, :cond_9

    .line 140
    iget-object v0, p0, LacB;->a:LMZ;

    invoke-interface {v0}, LMZ;->a()V

    .line 142
    :cond_9
    return-void
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 128
    sget v0, Leg;->launcher_docs_icon:I

    if-ne p1, v0, :cond_6

    .line 129
    sget p1, Leg;->topbar_docshome:I

    .line 132
    :cond_6
    iget-object v0, p0, LacB;->a:LMZ;

    if-eqz v0, :cond_f

    .line 133
    iget-object v0, p0, LacB;->a:LMZ;

    invoke-interface {v0, p1}, LMZ;->setLogo(I)V

    .line 135
    :cond_f
    return-void
.end method

.method public a(LMZ;)V
    .registers 4
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, LacB;->a:LMZ;

    if-eqz v0, :cond_a

    .line 97
    iget-object v0, p0, LacB;->a:LMZ;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LMZ;->setListener(LNa;)V

    .line 100
    :cond_a
    invoke-virtual {p0}, LacB;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, LMZ;->setAccountName(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, LacB;->a:LNa;

    invoke-interface {p1, v0}, LMZ;->setListener(LNa;)V

    .line 102
    iput-object p1, p0, LacB;->a:LMZ;

    .line 103
    return-void
.end method

.method public a(Landroid/view/MenuItem;LMP;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 117
    return-void
.end method

.method public a(Landroid/widget/Button;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, LML;->a(Landroid/widget/Button;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, LacB;->a:LMZ;

    if-eqz v0, :cond_c

    .line 71
    iget-object v0, p0, LacB;->a:LMZ;

    invoke-interface {v0, p2}, LMZ;->setAccountName(Ljava/lang/String;)V

    .line 73
    :cond_c
    if-eqz p1, :cond_11

    .line 74
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :cond_11
    return-void
.end method

.method public a(Landroid/widget/Button;[Landroid/accounts/Account;LMN;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 88
    if-eqz p1, :cond_b

    .line 89
    array-length v0, p2

    const/4 v1, 0x1

    if-gt v0, v1, :cond_c

    const/16 v0, 0x8

    :goto_8
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 92
    :cond_b
    return-void

    .line 89
    :cond_c
    const/4 v0, 0x0

    goto :goto_8
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 122
    return-void
.end method

.method public a(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, LacB;->a:Landroid/app/Activity;

    const-string v1, "search"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    .line 110
    invoke-virtual/range {v0 .. v5}, Landroid/app/SearchManager;->startSearch(Ljava/lang/String;ZLandroid/content/ComponentName;Landroid/os/Bundle;Z)V

    .line 112
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .registers 1

    .prologue
    .line 154
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, LacB;->a:LMZ;

    if-eqz v0, :cond_9

    .line 81
    iget-object v0, p0, LacB;->a:LMZ;

    invoke-interface {v0, p1, p2}, LMZ;->setTitles(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    :cond_9
    return-void
.end method
