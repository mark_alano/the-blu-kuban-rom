.class public abstract Lacv;
.super Ljava/lang/Object;
.source "ActionBarHelperFactory.java"

# interfaces
.implements LMO;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)LMM;
    .registers 3
    .parameter

    .prologue
    .line 30
    instance-of v0, p1, LdZ;

    invoke-static {v0}, Lagu;->a(Z)V

    .line 31
    new-instance v0, LfA;

    invoke-direct {v0, p1}, LfA;-><init>(Landroid/app/Activity;)V

    .line 32
    invoke-virtual {p0, p1, v0}, Lacv;->a(Landroid/app/Activity;LNa;)LMM;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/app/Activity;LNa;)LMM;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 25
    invoke-virtual {p0, p1, p2, v0}, Lacv;->a(Landroid/app/Activity;LNa;Ljava/lang/String;)LMM;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Landroid/app/Activity;LNa;Ljava/lang/String;)LMM;
.end method
