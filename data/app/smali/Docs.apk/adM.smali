.class public final LadM;
.super Ljava/lang/Object;
.source "MethodOverride.java"

# interfaces
.implements Laed;
.implements Laem;


# instance fields
.field private final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Laeh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const-class v0, Laeh;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, LadM;->a:Ljava/util/EnumSet;

    .line 62
    return-void
.end method

.method private a(Laej;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 91
    invoke-virtual {p1}, Laej;->a()Laeh;

    move-result-object v2

    .line 92
    sget-object v3, Laeh;->b:Laeh;

    if-eq v2, v3, :cond_17

    sget-object v3, Laeh;->f:Laeh;

    if-eq v2, v3, :cond_17

    iget-object v3, p0, LadM;->a:Ljava/util/EnumSet;

    invoke-virtual {v3, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 101
    :cond_16
    :goto_16
    return v0

    .line 95
    :cond_17
    sget-object v3, LadN;->a:[I

    invoke-virtual {v2}, Laeh;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_3c

    move v0, v1

    .line 101
    goto :goto_16

    .line 97
    :pswitch_24
    invoke-virtual {p1}, Laej;->a()Laeq;

    move-result-object v2

    invoke-virtual {v2}, Laeq;->b()Z

    move-result v2

    if-eqz v2, :cond_16

    move v0, v1

    goto :goto_16

    .line 99
    :pswitch_30
    invoke-virtual {p1}, Laej;->a()Laeq;

    move-result-object v2

    invoke-virtual {v2}, Laeq;->a()Z

    move-result v2

    if-eqz v2, :cond_16

    move v0, v1

    goto :goto_16

    .line 95
    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_24
        :pswitch_30
    .end packed-switch
.end method


# virtual methods
.method public a(Laej;)V
    .registers 2
    .parameter

    .prologue
    .line 75
    invoke-virtual {p1, p0}, Laej;->a(Laed;)Laej;

    .line 76
    return-void
.end method

.method public b(Laej;)V
    .registers 5
    .parameter

    .prologue
    .line 79
    invoke-direct {p0, p1}, LadM;->a(Laej;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 80
    invoke-virtual {p1}, Laej;->a()Laeh;

    move-result-object v0

    .line 81
    sget-object v1, Laeh;->f:Laeh;

    invoke-virtual {p1, v1}, Laej;->a(Laeh;)Laej;

    .line 82
    invoke-virtual {p1}, Laej;->a()Laee;

    move-result-object v1

    const-string v2, "X-HTTP-Method-Override"

    invoke-virtual {v0}, Laeh;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Laee;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    invoke-virtual {p1}, Laej;->a()Laec;

    move-result-object v0

    if-nez v0, :cond_2a

    .line 85
    new-instance v0, LadZ;

    invoke-direct {v0}, LadZ;-><init>()V

    invoke-virtual {p1, v0}, Laej;->a(Laec;)Laej;

    .line 88
    :cond_2a
    return-void
.end method
