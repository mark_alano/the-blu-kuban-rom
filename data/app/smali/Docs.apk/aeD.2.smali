.class final LaeD;
.super Laet;
.source "NetHttpRequest.java"


# instance fields
.field private a:Laec;

.field private final a:Ljava/net/HttpURLConnection;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 37
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    invoke-direct {p0, p1, v0}, LaeD;-><init>(Ljava/lang/String;Ljava/net/HttpURLConnection;)V

    .line 38
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/net/HttpURLConnection;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Laet;-><init>()V

    .line 41
    iput-object p2, p0, LaeD;->a:Ljava/net/HttpURLConnection;

    .line 42
    invoke-virtual {p2, p1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 43
    invoke-virtual {p2, v0}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 44
    invoke-virtual {p2, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 45
    return-void
.end method


# virtual methods
.method public a()Laeu;
    .registers 10

    .prologue
    const-wide/16 v7, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    iget-object v3, p0, LaeD;->a:Ljava/net/HttpURLConnection;

    .line 62
    iget-object v0, p0, LaeD;->a:Laec;

    if-eqz v0, :cond_69

    .line 63
    iget-object v0, p0, LaeD;->a:Laec;

    invoke-interface {v0}, Laec;->b()Ljava/lang/String;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_17

    .line 65
    const-string v4, "Content-Type"

    invoke-virtual {p0, v4, v0}, LaeD;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_17
    iget-object v0, p0, LaeD;->a:Laec;

    invoke-interface {v0}, Laec;->a()Ljava/lang/String;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_24

    .line 69
    const-string v4, "Content-Encoding"

    invoke-virtual {p0, v4, v0}, LaeD;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_24
    iget-object v0, p0, LaeD;->a:Laec;

    invoke-interface {v0}, Laec;->a()J

    move-result-wide v4

    .line 72
    cmp-long v0, v4, v7

    if-ltz v0, :cond_37

    .line 73
    const-string v0, "Content-Length"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v0, v6}, LaeD;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_37
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v6

    .line 76
    const-string v0, "POST"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4b

    const-string v0, "PUT"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 77
    :cond_4b
    invoke-virtual {v3, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 79
    cmp-long v0, v4, v7

    if-ltz v0, :cond_72

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v4, v0

    if-gtz v0, :cond_72

    .line 80
    long-to-int v0, v4

    invoke-virtual {v3, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 84
    :goto_5d
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 86
    :try_start_61
    iget-object v0, p0, LaeD;->a:Laec;

    invoke-interface {v0, v1}, Laec;->a(Ljava/io/OutputStream;)V
    :try_end_66
    .catchall {:try_start_61 .. :try_end_66} :catchall_76

    .line 88
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 100
    :cond_69
    :goto_69
    :try_start_69
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V

    .line 101
    new-instance v0, LaeE;

    invoke-direct {v0, v3}, LaeE;-><init>(Ljava/net/HttpURLConnection;)V
    :try_end_71
    .catchall {:try_start_69 .. :try_end_71} :catchall_8c

    .line 106
    return-object v0

    .line 82
    :cond_72
    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    goto :goto_5d

    .line 88
    :catchall_76
    move-exception v0

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    throw v0

    .line 93
    :cond_7b
    cmp-long v0, v4, v7

    if-nez v0, :cond_8a

    move v0, v1

    :goto_80
    const-string v4, "%s with non-zero content length is not supported"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v6, v1, v2

    invoke-static {v0, v4, v1}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_69

    :cond_8a
    move v0, v2

    goto :goto_80

    .line 105
    :catchall_8c
    move-exception v0

    .line 106
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v0
.end method

.method public a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, LaeD;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 55
    iget-object v0, p0, LaeD;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 56
    return-void
.end method

.method public a(Laec;)V
    .registers 2
    .parameter

    .prologue
    .line 113
    iput-object p1, p0, LaeD;->a:Laec;

    .line 114
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, LaeD;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, p1, p2}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    return-void
.end method
