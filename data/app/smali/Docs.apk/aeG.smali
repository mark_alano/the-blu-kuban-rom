.class public LaeG;
.super Ljava/lang/Object;
.source "JsonHttpClient.java"


# static fields
.field static final LOGGER:Ljava/util/logging/Logger;


# instance fields
.field private final applicationName:Ljava/lang/String;

.field private final baseUrl:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final baseUrlUsed:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final jsonFactory:LaeP;

.field private jsonHttpParser:LaeJ;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final jsonHttpRequestInitializer:LaeL;

.field private final jsonObjectParser:LaeR;

.field private final requestFactory:Lael;

.field private final rootUrl:Ljava/lang/String;

.field private final servicePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 52
    const-class v0, LaeG;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, LaeG;->LOGGER:Ljava/util/logging/Logger;

    return-void
.end method

.method protected constructor <init>(Laeq;LaeL;Laem;LaeP;LaeR;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343
    iput-object p2, p0, LaeG;->jsonHttpRequestInitializer:LaeL;

    .line 344
    invoke-static {p6}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaeG;->baseUrl:Ljava/lang/String;

    .line 345
    const-string v0, "/"

    invoke-virtual {p6, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lagu;->a(Z)V

    .line 346
    iput-object p7, p0, LaeG;->applicationName:Ljava/lang/String;

    .line 347
    invoke-static {p4}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaeP;

    iput-object v0, p0, LaeG;->jsonFactory:LaeP;

    .line 348
    iput-object p5, p0, LaeG;->jsonObjectParser:LaeR;

    .line 349
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    if-nez p3, :cond_36

    invoke-virtual {p1}, Laeq;->a()Lael;

    move-result-object v0

    :goto_2c
    iput-object v0, p0, LaeG;->requestFactory:Lael;

    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, LaeG;->baseUrlUsed:Z

    .line 353
    iput-object v1, p0, LaeG;->rootUrl:Ljava/lang/String;

    .line 354
    iput-object v1, p0, LaeG;->servicePath:Ljava/lang/String;

    .line 355
    return-void

    .line 350
    :cond_36
    invoke-virtual {p1, p3}, Laeq;->a(Laem;)Lael;

    move-result-object v0

    goto :goto_2c
.end method

.method protected constructor <init>(Laeq;LaeL;Laem;LaeP;LaeR;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 385
    iput-object p2, p0, LaeG;->jsonHttpRequestInitializer:LaeL;

    .line 386
    invoke-static {p6}, LaeG;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaeG;->rootUrl:Ljava/lang/String;

    .line 387
    invoke-static {p7}, LaeG;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LaeG;->servicePath:Ljava/lang/String;

    .line 388
    iput-object p8, p0, LaeG;->applicationName:Ljava/lang/String;

    .line 389
    invoke-static {p4}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaeP;

    iput-object v0, p0, LaeG;->jsonFactory:LaeP;

    .line 390
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    iput-object p5, p0, LaeG;->jsonObjectParser:LaeR;

    .line 392
    if-nez p3, :cond_2f

    invoke-virtual {p1}, Laeq;->a()Lael;

    move-result-object v0

    :goto_26
    iput-object v0, p0, LaeG;->requestFactory:Lael;

    .line 394
    const/4 v0, 0x0

    iput-object v0, p0, LaeG;->baseUrl:Ljava/lang/String;

    .line 395
    const/4 v0, 0x0

    iput-boolean v0, p0, LaeG;->baseUrlUsed:Z

    .line 396
    return-void

    .line 392
    :cond_2f
    invoke-virtual {p1, p3}, Laeq;->a(Laem;)Lael;

    move-result-object v0

    goto :goto_26
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 400
    const-string v0, "root URL cannot be null."

    invoke-static {p0, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 402
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 404
    :cond_20
    return-object p0
.end method

.method static b(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 412
    const-string v0, "service path cannot be null"

    invoke-static {p0, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v2, :cond_1a

    .line 414
    const-string v0, "/"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "service path must equal \"/\" if it is of length 1."

    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    .line 416
    const-string p0, ""

    .line 425
    :cond_19
    :goto_19
    return-object p0

    .line 417
    :cond_1a
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_19

    .line 418
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3b

    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 421
    :cond_3b
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 422
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_19
.end method


# virtual methods
.method protected a(Ljava/lang/Object;)LaeI;
    .registers 4
    .parameter

    .prologue
    .line 215
    new-instance v0, LaeI;

    invoke-virtual {p0}, LaeG;->a()LaeP;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LaeI;-><init>(LaeP;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final a()LaeJ;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, LaeG;->jsonHttpParser:LaeJ;

    if-nez v0, :cond_a

    .line 182
    invoke-virtual {p0}, LaeG;->b()LaeJ;

    move-result-object v0

    iput-object v0, p0, LaeG;->jsonHttpParser:LaeJ;

    .line 184
    :cond_a
    iget-object v0, p0, LaeG;->jsonHttpParser:LaeJ;

    return-object v0
.end method

.method public final a()LaeL;
    .registers 2

    .prologue
    .line 170
    iget-object v0, p0, LaeG;->jsonHttpRequestInitializer:LaeL;

    return-object v0
.end method

.method public final a()LaeP;
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, LaeG;->jsonFactory:LaeP;

    return-object v0
.end method

.method public final a()LaeR;
    .registers 2

    .prologue
    .line 205
    iget-object v0, p0, LaeG;->jsonObjectParser:LaeR;

    return-object v0
.end method

.method protected a(Laeh;Laeb;Ljava/lang/Object;)Laej;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 448
    iget-object v0, p0, LaeG;->requestFactory:Lael;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lael;->a(Laeh;Laeb;Laec;)Laej;

    move-result-object v0

    .line 449
    invoke-virtual {p0}, LaeG;->a()LaeR;

    move-result-object v1

    .line 451
    if-eqz v1, :cond_2b

    .line 452
    invoke-virtual {v0, v1}, Laej;->a(Lafz;)Laej;

    .line 456
    :goto_10
    invoke-virtual {p0}, LaeG;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_21

    .line 457
    invoke-virtual {v0}, Laej;->a()Laee;

    move-result-object v1

    invoke-virtual {p0}, LaeG;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Laee;->a(Ljava/lang/String;)V

    .line 459
    :cond_21
    if-eqz p3, :cond_2a

    .line 460
    invoke-virtual {p0, p3}, LaeG;->a(Ljava/lang/Object;)LaeI;

    move-result-object v1

    invoke-virtual {v0, v1}, Laej;->a(Laec;)Laej;

    .line 462
    :cond_2a
    return-object v0

    .line 454
    :cond_2b
    invoke-virtual {p0}, LaeG;->a()LaeJ;

    move-result-object v1

    invoke-virtual {v0, v1}, Laej;->a(Laei;)V

    goto :goto_10
.end method

.method protected a(Laej;)Laen;
    .registers 3
    .parameter

    .prologue
    .line 520
    invoke-virtual {p1}, Laej;->a()Laen;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 120
    iget-boolean v0, p0, LaeG;->baseUrlUsed:Z

    if-eqz v0, :cond_7

    .line 121
    iget-object v0, p0, LaeG;->baseUrl:Ljava/lang/String;

    .line 123
    :goto_6
    return-object v0

    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LaeG;->rootUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LaeG;->servicePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_6
.end method

.method public a(LaeK;)V
    .registers 3
    .parameter

    .prologue
    .line 242
    invoke-virtual {p0}, LaeG;->a()LaeL;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 243
    invoke-virtual {p0}, LaeG;->a()LaeL;

    move-result-object v0

    invoke-interface {v0, p1}, LaeL;->a(LaeK;)V

    .line 245
    :cond_d
    return-void
.end method

.method protected final a()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 111
    iget-boolean v0, p0, LaeG;->baseUrlUsed:Z

    return v0
.end method

.method protected b()LaeJ;
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 196
    new-instance v0, LaeJ;

    iget-object v1, p0, LaeG;->jsonFactory:LaeP;

    invoke-direct {v0, v1}, LaeJ;-><init>(LaeP;)V

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 133
    iget-boolean v0, p0, LaeG;->baseUrlUsed:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    const-string v1, "root URL cannot be used if base URL is used."

    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    .line 134
    iget-object v0, p0, LaeG;->rootUrl:Ljava/lang/String;

    return-object v0

    .line 133
    :cond_d
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 146
    iget-boolean v0, p0, LaeG;->baseUrlUsed:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    const-string v1, "service path cannot be used if base URL is used."

    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    .line 147
    iget-object v0, p0, LaeG;->servicePath:Ljava/lang/String;

    return-object v0

    .line 146
    :cond_d
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 155
    iget-object v0, p0, LaeG;->applicationName:Ljava/lang/String;

    return-object v0
.end method
