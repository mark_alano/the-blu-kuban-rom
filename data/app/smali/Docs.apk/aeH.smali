.class public LaeH;
.super Ljava/lang/Object;
.source "JsonHttpClient.java"


# instance fields
.field private applicationName:Ljava/lang/String;

.field private baseUrl:Laeb;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private baseUrlUsed:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private httpRequestInitializer:Laem;

.field private final jsonFactory:LaeP;

.field private jsonHttpRequestInitializer:LaeL;

.field private jsonObjectParser:LaeR;

.field private rootUrl:Ljava/lang/String;

.field private servicePath:Ljava/lang/String;

.field private final transport:Laeq;


# direct methods
.method protected constructor <init>(Laeq;LaeP;Laeb;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 617
    iput-object p1, p0, LaeH;->transport:Laeq;

    .line 618
    iput-object p2, p0, LaeH;->jsonFactory:LaeP;

    .line 619
    const/4 v0, 0x1

    iput-boolean v0, p0, LaeH;->baseUrlUsed:Z

    .line 620
    invoke-virtual {p0, p3}, LaeH;->a(Laeb;)LaeH;

    .line 621
    return-void
.end method


# virtual methods
.method public a(LaeL;)LaeH;
    .registers 2
    .parameter

    .prologue
    .line 849
    iput-object p1, p0, LaeH;->jsonHttpRequestInitializer:LaeL;

    .line 850
    return-object p0
.end method

.method public a(Laeb;)LaeH;
    .registers 4
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 757
    iget-boolean v0, p0, LaeH;->baseUrlUsed:Z

    invoke-static {v0}, Lagu;->a(Z)V

    .line 758
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeb;

    iput-object v0, p0, LaeH;->baseUrl:Laeb;

    .line 759
    invoke-virtual {p1}, Laeb;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lagu;->a(Z)V

    .line 760
    return-object p0
.end method

.method public a()LaeL;
    .registers 2

    .prologue
    .line 855
    iget-object v0, p0, LaeH;->jsonHttpRequestInitializer:LaeL;

    return-object v0
.end method

.method public final a()LaeP;
    .registers 2

    .prologue
    .line 685
    iget-object v0, p0, LaeH;->jsonFactory:LaeP;

    return-object v0
.end method

.method public final a()LaeR;
    .registers 2

    .prologue
    .line 704
    iget-object v0, p0, LaeH;->jsonObjectParser:LaeR;

    return-object v0
.end method

.method public final a()Laeb;
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 737
    iget-boolean v0, p0, LaeH;->baseUrlUsed:Z

    invoke-static {v0}, Lagu;->a(Z)V

    .line 738
    iget-object v0, p0, LaeH;->baseUrl:Laeb;

    return-object v0
.end method

.method public final a()Laem;
    .registers 2

    .prologue
    .line 869
    iget-object v0, p0, LaeH;->httpRequestInitializer:Laem;

    return-object v0
.end method

.method public final a()Laeq;
    .registers 2

    .prologue
    .line 690
    iget-object v0, p0, LaeH;->transport:Laeq;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 775
    iget-boolean v0, p0, LaeH;->baseUrlUsed:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    const-string v1, "root URL cannot be used if base URL is used."

    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    .line 776
    iget-object v0, p0, LaeH;->rootUrl:Ljava/lang/String;

    return-object v0

    .line 775
    :cond_d
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected final a()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 680
    iget-boolean v0, p0, LaeH;->baseUrlUsed:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 815
    iget-boolean v0, p0, LaeH;->baseUrlUsed:Z

    if-nez v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    const-string v1, "service path cannot be used if base URL is used."

    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    .line 816
    iget-object v0, p0, LaeH;->servicePath:Ljava/lang/String;

    return-object v0

    .line 815
    :cond_d
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 886
    iget-object v0, p0, LaeH;->applicationName:Ljava/lang/String;

    return-object v0
.end method
