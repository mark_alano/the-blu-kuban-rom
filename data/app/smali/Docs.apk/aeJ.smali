.class public LaeJ;
.super Ljava/lang/Object;
.source "JsonHttpParser.java"

# interfaces
.implements Laei;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:LaeP;

.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(LaeP;)V
    .registers 3
    .parameter

    .prologue
    .line 72
    const-string v0, "application/json"

    invoke-direct {p0, p1, v0}, LaeJ;-><init>(LaeP;Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method protected constructor <init>(LaeP;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaeP;

    iput-object v0, p0, LaeJ;->a:LaeP;

    .line 82
    iput-object p2, p0, LaeJ;->a:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public static a(LaeP;Laen;)LaeS;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 118
    invoke-virtual {p1}, Laen;->a()Ljava/io/InputStream;

    move-result-object v1

    .line 120
    :try_start_4
    invoke-virtual {p0, v1}, LaeP;->a(Ljava/io/InputStream;)LaeS;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, LaeS;->a()LaeV;
    :try_end_b
    .catchall {:try_start_4 .. :try_end_b} :catchall_12

    .line 122
    const/4 v1, 0x0

    .line 125
    if-eqz v1, :cond_11

    .line 126
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_11
    return-object v0

    .line 125
    :catchall_12
    move-exception v0

    if-eqz v1, :cond_18

    .line 126
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_18
    throw v0
.end method


# virtual methods
.method public a(Laen;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laen;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, LaeJ;->a:LaeP;

    invoke-static {v0, p1}, LaeJ;->a(LaeP;Laen;)LaeS;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, LaeS;->a(Ljava/lang/Class;LaeM;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 86
    iget-object v0, p0, LaeJ;->a:Ljava/lang/String;

    return-object v0
.end method
