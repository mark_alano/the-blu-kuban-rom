.class public LaeK;
.super Lafq;
.source "JsonHttpRequest.java"


# instance fields
.field private final client:LaeG;

.field private final content:Ljava/lang/Object;

.field private enableGZipContent:Z

.field private lastResponseHeaders:Laee;

.field private final method:Laeh;

.field private requestHeaders:Laee;

.field private final uriTemplate:Ljava/lang/String;


# direct methods
.method public constructor <init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0}, Lafq;-><init>()V

    .line 49
    new-instance v0, Laee;

    invoke-direct {v0}, Laee;-><init>()V

    iput-object v0, p0, LaeK;->requestHeaders:Laee;

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, LaeK;->enableGZipContent:Z

    .line 70
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaeG;

    iput-object v0, p0, LaeK;->client:LaeG;

    .line 71
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    iput-object v0, p0, LaeK;->method:Laeh;

    .line 72
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LaeK;->uriTemplate:Ljava/lang/String;

    .line 73
    iput-object p4, p0, LaeK;->content:Ljava/lang/Object;

    .line 74
    return-void
.end method


# virtual methods
.method public final a()LaeG;
    .registers 2

    .prologue
    .line 117
    iget-object v0, p0, LaeK;->client:LaeG;

    return-object v0
.end method

.method public final a()Laeb;
    .registers 5

    .prologue
    .line 162
    invoke-virtual {p0}, LaeK;->a()LaeG;

    move-result-object v0

    invoke-virtual {v0}, LaeG;->a()Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 163
    invoke-virtual {p0}, LaeK;->a()LaeG;

    move-result-object v0

    invoke-virtual {v0}, LaeG;->a()Ljava/lang/String;

    move-result-object v0

    .line 167
    :goto_12
    new-instance v1, Laeb;

    iget-object v2, p0, LaeK;->uriTemplate:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v2, p0, v3}, Laev;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Laeb;-><init>(Ljava/lang/String;)V

    return-object v1

    .line 165
    :cond_1f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LaeK;->a()LaeG;

    move-result-object v1

    invoke-virtual {v1}, LaeG;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LaeK;->a()LaeG;

    move-result-object v1

    invoke-virtual {v1}, LaeG;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_12
.end method

.method public final a()Laee;
    .registers 2

    .prologue
    .line 142
    iget-object v0, p0, LaeK;->requestHeaders:Laee;

    return-object v0
.end method

.method public a()Laej;
    .registers 5

    .prologue
    .line 177
    iget-object v0, p0, LaeK;->client:LaeG;

    iget-object v1, p0, LaeK;->method:Laeh;

    invoke-virtual {p0}, LaeK;->a()Laeb;

    move-result-object v2

    iget-object v3, p0, LaeK;->content:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, LaeG;->a(Laeh;Laeb;Ljava/lang/Object;)Laej;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Laej;->a()Laee;

    move-result-object v1

    invoke-virtual {p0}, LaeK;->a()Laee;

    move-result-object v2

    invoke-virtual {v1, v2}, Laee;->putAll(Ljava/util/Map;)V

    .line 180
    return-object v0
.end method

.method public a()Laen;
    .registers 3

    .prologue
    .line 205
    invoke-virtual {p0}, LaeK;->a()Laej;

    move-result-object v0

    .line 206
    iget-boolean v1, p0, LaeK;->enableGZipContent:Z

    invoke-virtual {v0, v1}, Laej;->a(Z)Laej;

    .line 207
    iget-object v1, p0, LaeK;->client:LaeG;

    invoke-virtual {v1, v0}, LaeG;->a(Laej;)Laen;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Laen;->a()Laee;

    move-result-object v1

    iput-object v1, p0, LaeK;->lastResponseHeaders:Laee;

    .line 209
    return-object v0
.end method
