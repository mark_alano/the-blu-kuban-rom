.class public abstract LaeP;
.super Ljava/lang/Object;
.source "JsonFactory.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/Object;Z)Ljava/io/ByteArrayOutputStream;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 183
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 185
    :try_start_5
    sget-object v1, Lagg;->c:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0, v1}, LaeP;->a(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)LaeQ;

    move-result-object v1

    .line 186
    if-eqz p2, :cond_10

    .line 187
    invoke-virtual {v1}, LaeQ;->g()V

    .line 189
    :cond_10
    invoke-virtual {v1, p1}, LaeQ;->a(Ljava/lang/Object;)V

    .line 190
    invoke-virtual {v1}, LaeQ;->a()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_16} :catch_17

    .line 194
    return-object v0

    .line 191
    :catch_17
    move-exception v0

    .line 192
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Ljava/lang/Object;Z)Ljava/lang/String;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 167
    :try_start_0
    invoke-direct {p0, p1, p2}, LaeP;->a(Ljava/lang/Object;Z)Ljava/io/ByteArrayOutputStream;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_9} :catch_b

    move-result-object v0

    return-object v0

    .line 168
    :catch_b
    move-exception v0

    .line 170
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public abstract a(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)LaeQ;
.end method

.method public abstract a(Ljava/io/InputStream;)LaeS;
.end method

.method public abstract a(Ljava/io/InputStream;Ljava/nio/charset/Charset;)LaeS;
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LaeP;->a(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 142
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LaeP;->a(Ljava/lang/Object;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
