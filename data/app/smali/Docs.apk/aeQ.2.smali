.class public abstract LaeQ;
.super Ljava/lang/Object;
.source "JsonGenerator.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(ZLjava/lang/Object;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 131
    if-nez p2, :cond_5

    .line 202
    :goto_4
    return-void

    .line 134
    :cond_5
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 135
    invoke-static {p2}, Lafj;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 136
    invoke-virtual {p0}, LaeQ;->f()V

    goto :goto_4

    .line 137
    :cond_13
    instance-of v3, p2, Ljava/lang/String;

    if-eqz v3, :cond_1d

    .line 138
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p2}, LaeQ;->b(Ljava/lang/String;)V

    goto :goto_4

    .line 139
    :cond_1d
    instance-of v3, p2, Ljava/lang/Number;

    if-eqz v3, :cond_b4

    .line 140
    if-eqz p1, :cond_2b

    .line 141
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaeQ;->b(Ljava/lang/String;)V

    goto :goto_4

    .line 142
    :cond_2b
    instance-of v0, p2, Ljava/math/BigDecimal;

    if-eqz v0, :cond_35

    .line 143
    check-cast p2, Ljava/math/BigDecimal;

    invoke-virtual {p0, p2}, LaeQ;->a(Ljava/math/BigDecimal;)V

    goto :goto_4

    .line 144
    :cond_35
    instance-of v0, p2, Ljava/math/BigInteger;

    if-eqz v0, :cond_3f

    .line 145
    check-cast p2, Ljava/math/BigInteger;

    invoke-virtual {p0, p2}, LaeQ;->a(Ljava/math/BigInteger;)V

    goto :goto_4

    .line 146
    :cond_3f
    instance-of v0, p2, Lamt;

    if-eqz v0, :cond_49

    .line 147
    check-cast p2, Lamt;

    invoke-virtual {p0, p2}, LaeQ;->a(Lamt;)V

    goto :goto_4

    .line 148
    :cond_49
    instance-of v0, p2, Lamv;

    if-eqz v0, :cond_53

    .line 149
    check-cast p2, Lamv;

    invoke-virtual {p0, p2}, LaeQ;->a(Lamv;)V

    goto :goto_4

    .line 150
    :cond_53
    instance-of v0, p2, Ljava/lang/Long;

    if-eqz v0, :cond_61

    .line 151
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, LaeQ;->a(J)V

    goto :goto_4

    .line 152
    :cond_61
    instance-of v0, p2, Ljava/lang/Float;

    if-eqz v0, :cond_81

    .line 153
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F

    move-result v3

    .line 154
    invoke-static {v3}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-nez v0, :cond_7f

    invoke-static {v3}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_7f

    move v0, v1

    :goto_78
    invoke-static {v0}, Lagu;->a(Z)V

    .line 155
    invoke-virtual {p0, v3}, LaeQ;->a(F)V

    goto :goto_4

    :cond_7f
    move v0, v2

    .line 154
    goto :goto_78

    .line 156
    :cond_81
    instance-of v0, p2, Ljava/lang/Integer;

    if-nez v0, :cond_8d

    instance-of v0, p2, Ljava/lang/Short;

    if-nez v0, :cond_8d

    instance-of v0, p2, Ljava/lang/Byte;

    if-eqz v0, :cond_98

    .line 157
    :cond_8d
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LaeQ;->a(I)V

    goto/16 :goto_4

    .line 159
    :cond_98
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v3

    .line 160
    invoke-static {v3, v4}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_b2

    invoke-static {v3, v4}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_b2

    :goto_aa
    invoke-static {v1}, Lagu;->a(Z)V

    .line 161
    invoke-virtual {p0, v3, v4}, LaeQ;->a(D)V

    goto/16 :goto_4

    :cond_b2
    move v1, v2

    .line 160
    goto :goto_aa

    .line 163
    :cond_b4
    instance-of v3, p2, Ljava/lang/Boolean;

    if-eqz v3, :cond_c3

    .line 164
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, LaeQ;->a(Z)V

    goto/16 :goto_4

    .line 165
    :cond_c3
    instance-of v3, p2, Lafo;

    if-eqz v3, :cond_d2

    .line 166
    check-cast p2, Lafo;

    invoke-virtual {p2}, Lafo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LaeQ;->b(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 167
    :cond_d2
    instance-of v3, p2, Ljava/lang/Iterable;

    if-nez v3, :cond_dc

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v3

    if-eqz v3, :cond_fa

    .line 168
    :cond_dc
    invoke-virtual {p0}, LaeQ;->b()V

    .line 169
    invoke-static {p2}, LafB;->a(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_e7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 170
    invoke-direct {p0, p1, v1}, LaeQ;->a(ZLjava/lang/Object;)V

    goto :goto_e7

    .line 172
    :cond_f5
    invoke-virtual {p0}, LaeQ;->c()V

    goto/16 :goto_4

    .line 173
    :cond_fa
    invoke-virtual {v0}, Ljava/lang/Class;->isEnum()Z

    move-result v3

    if-eqz v3, :cond_116

    .line 174
    check-cast p2, Ljava/lang/Enum;

    invoke-static {p2}, Lafp;->a(Ljava/lang/Enum;)Lafp;

    move-result-object v0

    invoke-virtual {v0}, Lafp;->a()Ljava/lang/String;

    move-result-object v0

    .line 175
    if-nez v0, :cond_111

    .line 176
    invoke-virtual {p0}, LaeQ;->f()V

    goto/16 :goto_4

    .line 178
    :cond_111
    invoke-virtual {p0, v0}, LaeQ;->b(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 181
    :cond_116
    invoke-virtual {p0}, LaeQ;->d()V

    .line 183
    instance-of v3, p2, Ljava/util/Map;

    if-eqz v3, :cond_154

    instance-of v3, p2, Lafq;

    if-nez v3, :cond_154

    move v5, v1

    .line 184
    :goto_122
    if-eqz v5, :cond_156

    const/4 v0, 0x0

    move-object v3, v0

    .line 185
    :goto_126
    invoke-static {p2}, Lafj;->a(Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_132
    :goto_132
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 186
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    .line 187
    if-eqz v7, :cond_132

    .line 188
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 190
    if-eqz v5, :cond_15c

    move v4, p1

    .line 196
    :goto_14d
    invoke-virtual {p0, v0}, LaeQ;->a(Ljava/lang/String;)V

    .line 197
    invoke-direct {p0, v4, v7}, LaeQ;->a(ZLjava/lang/Object;)V

    goto :goto_132

    :cond_154
    move v5, v2

    .line 183
    goto :goto_122

    .line 184
    :cond_156
    invoke-static {v0}, Lafh;->a(Ljava/lang/Class;)Lafh;

    move-result-object v0

    move-object v3, v0

    goto :goto_126

    .line 193
    :cond_15c
    invoke-virtual {v3, v0}, Lafh;->a(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 194
    if-eqz v4, :cond_16c

    const-class v8, LaeU;

    invoke-virtual {v4, v8}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v4

    if-eqz v4, :cond_16c

    move v4, v1

    goto :goto_14d

    :cond_16c
    move v4, v2

    goto :goto_14d

    .line 200
    :cond_16e
    invoke-virtual {p0}, LaeQ;->e()V

    goto/16 :goto_4
.end method


# virtual methods
.method public abstract a()V
.end method

.method public abstract a(D)V
.end method

.method public abstract a(F)V
.end method

.method public abstract a(I)V
.end method

.method public abstract a(J)V
.end method

.method public abstract a(Lamt;)V
.end method

.method public abstract a(Lamv;)V
.end method

.method public final a(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LaeQ;->a(ZLjava/lang/Object;)V

    .line 128
    return-void
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/math/BigDecimal;)V
.end method

.method public abstract a(Ljava/math/BigInteger;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()V
.end method

.method public abstract b(Ljava/lang/String;)V
.end method

.method public abstract c()V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method

.method public abstract f()V
.end method

.method public g()V
    .registers 1

    .prologue
    .line 217
    return-void
.end method
