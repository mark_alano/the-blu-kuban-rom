.class public abstract LaeS;
.super Ljava/lang/Object;
.source "JsonParser.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    return-void
.end method

.method private final a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;LaeM;)Ljava/lang/Object;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Ljava/lang/reflect/Type;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;",
            "Ljava/lang/Object;",
            "LaeM;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 509
    invoke-static {p3, p2}, Lafj;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 511
    instance-of v0, v1, Ljava/lang/Class;

    if-eqz v0, :cond_7e

    move-object v0, v1

    check-cast v0, Ljava/lang/Class;

    .line 512
    :goto_f
    instance-of v2, v1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v2, :cond_1a

    move-object v0, v1

    .line 513
    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-static {v0}, LafB;->a(Ljava/lang/reflect/ParameterizedType;)Ljava/lang/Class;

    move-result-object v0

    .line 516
    :cond_1a
    invoke-virtual {p0}, LaeS;->b()LaeV;

    move-result-object v2

    .line 518
    invoke-virtual {p0}, LaeS;->a()Ljava/lang/String;

    move-result-object v6

    .line 519
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 520
    if-nez v6, :cond_2b

    if-eqz p1, :cond_52

    .line 521
    :cond_2b
    const-string v8, " ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522
    if-eqz v6, :cond_3b

    .line 523
    const-string v8, "key "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 525
    :cond_3b
    if-eqz p1, :cond_4d

    .line 526
    if-eqz v6, :cond_44

    .line 527
    const-string v6, ", "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 529
    :cond_44
    const-string v6, "field "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 531
    :cond_4d
    const-string v6, "]"

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 533
    :cond_52
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 535
    sget-object v7, LaeT;->a:[I

    invoke-virtual {v2}, LaeV;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_2ca

    .line 667
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unexpected JSON node type: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7e
    move-object v0, v3

    .line 511
    goto :goto_f

    .line 538
    :pswitch_80
    invoke-static {v1}, LafB;->a(Ljava/lang/reflect/Type;)Z

    move-result v7

    .line 539
    if-eqz v1, :cond_92

    if-nez v7, :cond_92

    if-eqz v0, :cond_c8

    const-class v2, Ljava/util/Collection;

    invoke-static {v0, v2}, LafB;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_c8

    :cond_92
    move v2, v5

    :goto_93
    const-string v8, "expected collection or array type but got %s%s"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v1, v9, v4

    aput-object v6, v9, v5

    invoke-static {v2, v8, v9}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 543
    if-eqz p5, :cond_2c6

    if-eqz p1, :cond_2c6

    .line 544
    invoke-virtual {p5, p4, p1}, LaeM;->a(Ljava/lang/Object;Ljava/lang/reflect/Field;)Ljava/util/Collection;

    move-result-object v2

    .line 546
    :goto_a6
    if-nez v2, :cond_ac

    .line 547
    invoke-static {v1}, Lafj;->a(Ljava/lang/reflect/Type;)Ljava/util/Collection;

    move-result-object v2

    .line 550
    :cond_ac
    if-eqz v7, :cond_ca

    .line 551
    invoke-static {v1}, LafB;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    .line 555
    :cond_b2
    :goto_b2
    invoke-static {p3, v3}, Lafj;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p5

    .line 556
    invoke-direct/range {v0 .. v5}, LaeS;->a(Ljava/lang/reflect/Field;Ljava/util/Collection;Ljava/lang/reflect/Type;Ljava/util/ArrayList;LaeM;)V

    .line 557
    if-eqz v7, :cond_d9

    .line 558
    invoke-static {p3, v3}, LafB;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v2, v0}, LafB;->a(Ljava/util/Collection;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    .line 665
    :cond_c7
    :goto_c7
    return-object v6

    :cond_c8
    move v2, v4

    .line 539
    goto :goto_93

    .line 552
    :cond_ca
    if-eqz v0, :cond_b2

    const-class v4, Ljava/lang/Iterable;

    invoke-virtual {v4, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_b2

    .line 553
    invoke-static {v1}, LafB;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    goto :goto_b2

    :cond_d9
    move-object v6, v2

    .line 560
    goto :goto_c7

    .line 564
    :pswitch_db
    invoke-static {v1}, LafB;->a(Ljava/lang/reflect/Type;)Z

    move-result v2

    if-nez v2, :cond_135

    move v2, v5

    :goto_e2
    const-string v7, "expected object or map type but got %s%s"

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v1, v8, v4

    aput-object v6, v8, v5

    invoke-static {v2, v7, v8}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 567
    if-eqz v0, :cond_2c3

    if-eqz p5, :cond_2c3

    .line 568
    invoke-virtual {p5, p4, v0}, LaeM;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    .line 570
    :goto_f5
    if-eqz v0, :cond_137

    const-class v2, Ljava/util/Map;

    invoke-static {v0, v2}, LafB;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_137

    .line 571
    :goto_ff
    if-nez v6, :cond_109

    .line 573
    if-nez v5, :cond_105

    if-nez v0, :cond_139

    .line 574
    :cond_105
    invoke-static {v0}, Lafj;->a(Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v6

    .line 579
    :cond_109
    :goto_109
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 580
    if-eqz v1, :cond_112

    .line 581
    invoke-virtual {p3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    :cond_112
    if-eqz v5, :cond_13e

    const-class v4, Lafq;

    invoke-virtual {v4, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_13e

    .line 584
    const-class v4, Ljava/util/Map;

    invoke-virtual {v4, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_128

    invoke-static {v1}, LafB;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    .line 586
    :cond_128
    if-eqz v3, :cond_13e

    move-object v2, v6

    .line 588
    check-cast v2, Ljava/util/Map;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p5

    .line 589
    invoke-direct/range {v0 .. v5}, LaeS;->a(Ljava/lang/reflect/Field;Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/util/ArrayList;LaeM;)V

    goto :goto_c7

    :cond_135
    move v2, v4

    .line 564
    goto :goto_e2

    :cond_137
    move v5, v4

    .line 570
    goto :goto_ff

    .line 576
    :cond_139
    invoke-static {v0}, LafB;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    goto :goto_109

    .line 593
    :cond_13e
    invoke-direct {p0, p3, v6, p5}, LaeS;->a(Ljava/util/ArrayList;Ljava/lang/Object;LaeM;)V

    .line 594
    if-eqz v1, :cond_c7

    .line 595
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_c7

    .line 600
    :pswitch_147
    if-eqz v1, :cond_157

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq v0, v3, :cond_157

    if-eqz v0, :cond_16c

    const-class v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_16c

    :cond_157
    move v0, v5

    :goto_158
    const-string v3, "expected type Boolean or boolean but got %s%s"

    new-array v7, v9, [Ljava/lang/Object;

    aput-object v1, v7, v4

    aput-object v6, v7, v5

    invoke-static {v0, v3, v7}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 603
    sget-object v0, LaeV;->i:LaeV;

    if-ne v2, v0, :cond_16e

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_169
    move-object v6, v0

    goto/16 :goto_c7

    :cond_16c
    move v0, v4

    .line 600
    goto :goto_158

    .line 603
    :cond_16e
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_169

    .line 606
    :pswitch_171
    if-eqz p1, :cond_17b

    const-class v2, LaeU;

    invoke-virtual {p1, v2}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v2

    if-nez v2, :cond_195

    :cond_17b
    move v2, v5

    :goto_17c
    const-string v3, "number type formatted as a JSON number cannot use @JsonString annotation%s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v6, v5, v4

    invoke-static {v2, v3, v5}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 610
    if-eqz v0, :cond_18f

    const-class v2, Ljava/math/BigDecimal;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_197

    .line 611
    :cond_18f
    invoke-virtual {p0}, LaeS;->a()Ljava/math/BigDecimal;

    move-result-object v6

    goto/16 :goto_c7

    :cond_195
    move v2, v4

    .line 606
    goto :goto_17c

    .line 613
    :cond_197
    const-class v2, Ljava/math/BigInteger;

    if-ne v0, v2, :cond_1a1

    .line 614
    invoke-virtual {p0}, LaeS;->a()Ljava/math/BigInteger;

    move-result-object v6

    goto/16 :goto_c7

    .line 616
    :cond_1a1
    const-class v2, Lamt;

    if-ne v0, v2, :cond_1ab

    .line 617
    invoke-virtual {p0}, LaeS;->a()Lamt;

    move-result-object v6

    goto/16 :goto_c7

    .line 619
    :cond_1ab
    const-class v2, Lamv;

    if-ne v0, v2, :cond_1b5

    .line 620
    invoke-virtual {p0}, LaeS;->a()Lamv;

    move-result-object v6

    goto/16 :goto_c7

    .line 622
    :cond_1b5
    const-class v2, Ljava/lang/Double;

    if-eq v0, v2, :cond_1bd

    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_1c7

    .line 623
    :cond_1bd
    invoke-virtual {p0}, LaeS;->a()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    goto/16 :goto_c7

    .line 625
    :cond_1c7
    const-class v2, Ljava/lang/Long;

    if-eq v0, v2, :cond_1cf

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_1d9

    .line 626
    :cond_1cf
    invoke-virtual {p0}, LaeS;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    goto/16 :goto_c7

    .line 628
    :cond_1d9
    const-class v2, Ljava/lang/Float;

    if-eq v0, v2, :cond_1e1

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_1eb

    .line 629
    :cond_1e1
    invoke-virtual {p0}, LaeS;->a()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    goto/16 :goto_c7

    .line 631
    :cond_1eb
    const-class v2, Ljava/lang/Integer;

    if-eq v0, v2, :cond_1f3

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_1fd

    .line 632
    :cond_1f3
    invoke-virtual {p0}, LaeS;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    goto/16 :goto_c7

    .line 634
    :cond_1fd
    const-class v2, Ljava/lang/Short;

    if-eq v0, v2, :cond_205

    sget-object v2, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_20f

    .line 635
    :cond_205
    invoke-virtual {p0}, LaeS;->a()S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    goto/16 :goto_c7

    .line 637
    :cond_20f
    const-class v2, Ljava/lang/Byte;

    if-eq v0, v2, :cond_217

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_221

    .line 638
    :cond_217
    invoke-virtual {p0}, LaeS;->a()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    goto/16 :goto_c7

    .line 640
    :cond_221
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "expected numeric type but got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 643
    :pswitch_23e
    if-eqz v0, :cond_252

    const-class v2, Ljava/lang/Number;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_252

    if-eqz p1, :cond_266

    const-class v0, LaeU;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    if-eqz v0, :cond_266

    :cond_252
    move v0, v5

    :goto_253
    const-string v2, "number field formatted as a JSON string must use the @JsonString annotation%s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v6, v3, v4

    invoke-static {v0, v2, v3}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 649
    :try_start_25c
    invoke-virtual {p0}, LaeS;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lafj;->a(Ljava/lang/reflect/Type;Ljava/lang/String;)Ljava/lang/Object;
    :try_end_263
    .catch Ljava/lang/IllegalArgumentException; {:try_start_25c .. :try_end_263} :catch_268

    move-result-object v6

    goto/16 :goto_c7

    :cond_266
    move v0, v4

    .line 643
    goto :goto_253

    .line 650
    :catch_268
    move-exception v0

    .line 651
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v6, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 654
    :pswitch_26f
    if-eqz v0, :cond_277

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v2

    if-nez v2, :cond_2a1

    :cond_277
    move v2, v5

    :goto_278
    const-string v3, "primitive number field but found a JSON null%s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v6, v5, v4

    invoke-static {v2, v3, v5}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 656
    if-eqz v0, :cond_2b9

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v2

    and-int/lit16 v2, v2, 0x600

    if-eqz v2, :cond_2b9

    .line 658
    const-class v2, Ljava/util/Collection;

    invoke-static {v0, v2}, LafB;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_2a3

    .line 659
    invoke-static {v1}, Lafj;->a(Ljava/lang/reflect/Type;)Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lafj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    goto/16 :goto_c7

    :cond_2a1
    move v2, v4

    .line 654
    goto :goto_278

    .line 661
    :cond_2a3
    const-class v2, Ljava/util/Map;

    invoke-static {v0, v2}, LafB;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_2b9

    .line 662
    invoke-static {v0}, Lafj;->a(Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lafj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    goto/16 :goto_c7

    .line 665
    :cond_2b9
    invoke-static {p3, v1}, LafB;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lafj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    goto/16 :goto_c7

    :cond_2c3
    move-object v6, v3

    goto/16 :goto_f5

    :cond_2c6
    move-object v2, v3

    goto/16 :goto_a6

    .line 535
    nop

    :pswitch_data_2ca
    .packed-switch 0x1
        :pswitch_db
        :pswitch_80
        :pswitch_80
        :pswitch_db
        :pswitch_db
        :pswitch_147
        :pswitch_147
        :pswitch_171
        :pswitch_171
        :pswitch_23e
        :pswitch_26f
    .end packed-switch
.end method

.method private a(Ljava/lang/reflect/Field;Ljava/util/Collection;Ljava/lang/reflect/Type;Ljava/util/ArrayList;LaeM;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Field;",
            "Ljava/util/Collection",
            "<TT;>;",
            "Ljava/lang/reflect/Type;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;",
            "LaeM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 460
    invoke-direct {p0}, LaeS;->d()LaeV;

    move-result-object v0

    .line 461
    :goto_4
    sget-object v1, LaeV;->b:LaeV;

    if-eq v0, v1, :cond_1a

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    move-object v5, p5

    .line 463
    invoke-direct/range {v0 .. v5}, LaeS;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;LaeM;)Ljava/lang/Object;

    move-result-object v0

    .line 465
    invoke-interface {p2, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 466
    invoke-virtual {p0}, LaeS;->a()LaeV;

    move-result-object v0

    goto :goto_4

    .line 468
    :cond_1a
    return-void
.end method

.method private a(Ljava/lang/reflect/Field;Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/util/ArrayList;LaeM;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/reflect/Type;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;",
            "LaeM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 482
    invoke-direct {p0}, LaeS;->d()LaeV;

    move-result-object v0

    .line 483
    :goto_4
    sget-object v1, LaeV;->e:LaeV;

    if-ne v0, v1, :cond_17

    .line 484
    invoke-virtual {p0}, LaeS;->b()Ljava/lang/String;

    move-result-object v6

    .line 485
    invoke-virtual {p0}, LaeS;->a()LaeV;

    .line 487
    if-eqz p5, :cond_18

    invoke-virtual {p5, p2, v6}, LaeM;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 494
    :cond_17
    return-void

    :cond_18
    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    move-object v5, p5

    .line 490
    invoke-direct/range {v0 .. v5}, LaeS;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;LaeM;)Ljava/lang/Object;

    move-result-object v0

    .line 491
    invoke-interface {p2, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    invoke-virtual {p0}, LaeS;->a()LaeV;

    move-result-object v0

    goto :goto_4
.end method

.method private a(Ljava/util/ArrayList;Ljava/lang/Object;LaeM;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;",
            "Ljava/lang/Object;",
            "LaeM;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 326
    instance-of v0, p2, LaeN;

    if-eqz v0, :cond_f

    move-object v0, p2

    .line 327
    check-cast v0, LaeN;

    invoke-virtual {p0}, LaeS;->a()LaeP;

    move-result-object v2

    invoke-virtual {v0, v2}, LaeN;->a(LaeP;)V

    .line 329
    :cond_f
    invoke-direct {p0}, LaeS;->d()LaeV;

    move-result-object v0

    .line 330
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 331
    invoke-static {v3}, Lafh;->a(Ljava/lang/Class;)Lafh;

    move-result-object v8

    .line 332
    const-class v2, Lafq;

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v9

    .line 333
    if-nez v9, :cond_5e

    const-class v2, Ljava/util/Map;

    invoke-virtual {v2, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_5e

    move-object v2, p2

    .line 335
    check-cast v2, Ljava/util/Map;

    .line 336
    invoke-static {v3}, LafB;->c(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v3

    move-object v0, p0

    move-object v4, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LaeS;->a(Ljava/lang/reflect/Field;Ljava/util/Map;Ljava/lang/reflect/Type;Ljava/util/ArrayList;LaeM;)V

    .line 374
    :cond_38
    return-void

    .line 354
    :cond_39
    invoke-virtual {v0}, Lafp;->a()Ljava/lang/reflect/Field;

    move-result-object v3

    .line 355
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 356
    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    invoke-virtual {v0}, Lafp;->a()Ljava/lang/reflect/Type;

    move-result-object v4

    move-object v2, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, LaeS;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;LaeM;)Ljava/lang/Object;

    move-result-object v2

    .line 359
    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 360
    invoke-virtual {v0, p2, v2}, Lafp;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 372
    :goto_5a
    invoke-virtual {p0}, LaeS;->a()LaeV;

    move-result-object v0

    .line 340
    :cond_5e
    sget-object v2, LaeV;->e:LaeV;

    if-ne v0, v2, :cond_38

    .line 341
    invoke-virtual {p0}, LaeS;->b()Ljava/lang/String;

    move-result-object v7

    .line 342
    invoke-virtual {p0}, LaeS;->a()LaeV;

    .line 344
    if-eqz p3, :cond_71

    invoke-virtual {p3, p2, v7}, LaeM;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_38

    .line 348
    :cond_71
    invoke-virtual {v8, v7}, Lafh;->a(Ljava/lang/String;)Lafp;

    move-result-object v0

    .line 349
    if-eqz v0, :cond_8b

    .line 351
    invoke-virtual {v0}, Lafp;->a()Z

    move-result v2

    if-eqz v2, :cond_39

    invoke-virtual {v0}, Lafp;->b()Z

    move-result v2

    if-nez v2, :cond_39

    .line 352
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "final array/object fields are not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 361
    :cond_8b
    if-eqz v9, :cond_9d

    move-object v6, p2

    .line 363
    check-cast v6, Lafq;

    move-object v0, p0

    move-object v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    .line 364
    invoke-direct/range {v0 .. v5}, LaeS;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;LaeM;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Lafq;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_5a

    .line 367
    :cond_9d
    if-eqz p3, :cond_a2

    .line 368
    invoke-virtual {p3, p2, v7}, LaeM;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 370
    :cond_a2
    invoke-virtual {p0}, LaeS;->a()LaeS;

    goto :goto_5a
.end method

.method private c()LaeV;
    .registers 4

    .prologue
    .line 198
    invoke-virtual {p0}, LaeS;->b()LaeV;

    move-result-object v0

    .line 200
    if-nez v0, :cond_16

    .line 201
    invoke-virtual {p0}, LaeS;->a()LaeV;

    move-result-object v0

    move-object v1, v0

    .line 203
    :goto_b
    if-eqz v1, :cond_14

    const/4 v0, 0x1

    :goto_e
    const-string v2, "no JSON input found"

    invoke-static {v0, v2}, Lagu;->a(ZLjava/lang/Object;)V

    .line 204
    return-object v1

    .line 203
    :cond_14
    const/4 v0, 0x0

    goto :goto_e

    :cond_16
    move-object v1, v0

    goto :goto_b
.end method

.method private d()LaeV;
    .registers 4

    .prologue
    .line 219
    invoke-direct {p0}, LaeS;->c()LaeV;

    move-result-object v0

    .line 220
    sget-object v1, LaeT;->a:[I

    invoke-virtual {v0}, LaeV;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2a

    .line 231
    :goto_f
    return-object v0

    .line 222
    :pswitch_10
    invoke-virtual {p0}, LaeS;->a()LaeV;

    move-result-object v1

    .line 223
    sget-object v0, LaeV;->e:LaeV;

    if-eq v1, v0, :cond_1c

    sget-object v0, LaeV;->d:LaeV;

    if-ne v1, v0, :cond_22

    :cond_1c
    const/4 v0, 0x1

    :goto_1d
    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    move-object v0, v1

    .line 226
    goto :goto_f

    .line 223
    :cond_22
    const/4 v0, 0x0

    goto :goto_1d

    .line 228
    :pswitch_24
    invoke-virtual {p0}, LaeS;->a()LaeV;

    move-result-object v0

    goto :goto_f

    .line 220
    nop

    :pswitch_data_2a
    .packed-switch 0x1
        :pswitch_10
        :pswitch_24
    .end packed-switch
.end method


# virtual methods
.method public abstract a()B
.end method

.method public abstract a()D
.end method

.method public abstract a()F
.end method

.method public abstract a()I
.end method

.method public abstract a()J
.end method

.method public abstract a()LaeP;
.end method

.method public abstract a()LaeS;
.end method

.method public abstract a()LaeV;
.end method

.method public abstract a()Lamt;
.end method

.method public abstract a()Lamv;
.end method

.method public final a(Ljava/lang/Class;LaeM;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "LaeM;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 148
    :try_start_0
    invoke-virtual {p0, p1, p2}, LaeS;->b(Ljava/lang/Class;LaeM;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_8

    move-result-object v0

    .line 150
    invoke-virtual {p0}, LaeS;->a()V

    return-object v0

    :catchall_8
    move-exception v0

    invoke-virtual {p0}, LaeS;->a()V

    throw v0
.end method

.method public a(Ljava/lang/reflect/Type;ZLaeM;)Ljava/lang/Object;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 295
    :try_start_0
    invoke-direct {p0}, LaeS;->c()LaeV;

    .line 296
    const/4 v1, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LaeS;->a(Ljava/lang/reflect/Field;Ljava/lang/reflect/Type;Ljava/util/ArrayList;Ljava/lang/Object;LaeM;)Ljava/lang/Object;
    :try_end_10
    .catchall {:try_start_0 .. :try_end_10} :catchall_17

    move-result-object v0

    .line 298
    if-eqz p2, :cond_16

    .line 299
    invoke-virtual {p0}, LaeS;->a()V

    :cond_16
    return-object v0

    .line 298
    :catchall_17
    move-exception v0

    if-eqz p2, :cond_1d

    .line 299
    invoke-virtual {p0}, LaeS;->a()V

    :cond_1d
    throw v0
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public final a(Ljava/util/Set;)Ljava/lang/String;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 183
    invoke-direct {p0}, LaeS;->d()LaeV;

    move-result-object v0

    .line 184
    :goto_4
    sget-object v1, LaeV;->e:LaeV;

    if-ne v0, v1, :cond_1e

    .line 185
    invoke-virtual {p0}, LaeS;->b()Ljava/lang/String;

    move-result-object v0

    .line 186
    invoke-virtual {p0}, LaeS;->a()LaeV;

    .line 187
    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 193
    :goto_15
    return-object v0

    .line 190
    :cond_16
    invoke-virtual {p0}, LaeS;->a()LaeS;

    .line 191
    invoke-virtual {p0}, LaeS;->a()LaeV;

    move-result-object v0

    goto :goto_4

    .line 193
    :cond_1e
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public abstract a()Ljava/math/BigDecimal;
.end method

.method public abstract a()Ljava/math/BigInteger;
.end method

.method public abstract a()S
.end method

.method public abstract a()V
.end method

.method public final a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 166
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, LaeS;->a(Ljava/util/Set;)Ljava/lang/String;

    .line 167
    return-void
.end method

.method public abstract b()LaeV;
.end method

.method public final b(Ljava/lang/Class;LaeM;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "LaeM;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 271
    invoke-direct {p0}, LaeS;->c()LaeV;

    .line 273
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, LaeS;->a(Ljava/lang/reflect/Type;ZLaeM;)Ljava/lang/Object;

    move-result-object v0

    .line 274
    return-object v0
.end method

.method public abstract b()Ljava/lang/String;
.end method
