.class public final enum LaeV;
.super Ljava/lang/Enum;
.source "JsonToken.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LaeV;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LaeV;

.field private static final synthetic a:[LaeV;

.field public static final enum b:LaeV;

.field public static final enum c:LaeV;

.field public static final enum d:LaeV;

.field public static final enum e:LaeV;

.field public static final enum f:LaeV;

.field public static final enum g:LaeV;

.field public static final enum h:LaeV;

.field public static final enum i:LaeV;

.field public static final enum j:LaeV;

.field public static final enum k:LaeV;

.field public static final enum l:LaeV;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v0, LaeV;

    const-string v1, "START_ARRAY"

    invoke-direct {v0, v1, v3}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->a:LaeV;

    .line 29
    new-instance v0, LaeV;

    const-string v1, "END_ARRAY"

    invoke-direct {v0, v1, v4}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->b:LaeV;

    .line 32
    new-instance v0, LaeV;

    const-string v1, "START_OBJECT"

    invoke-direct {v0, v1, v5}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->c:LaeV;

    .line 35
    new-instance v0, LaeV;

    const-string v1, "END_OBJECT"

    invoke-direct {v0, v1, v6}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->d:LaeV;

    .line 38
    new-instance v0, LaeV;

    const-string v1, "FIELD_NAME"

    invoke-direct {v0, v1, v7}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->e:LaeV;

    .line 41
    new-instance v0, LaeV;

    const-string v1, "VALUE_STRING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->f:LaeV;

    .line 47
    new-instance v0, LaeV;

    const-string v1, "VALUE_NUMBER_INT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->g:LaeV;

    .line 50
    new-instance v0, LaeV;

    const-string v1, "VALUE_NUMBER_FLOAT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->h:LaeV;

    .line 53
    new-instance v0, LaeV;

    const-string v1, "VALUE_TRUE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->i:LaeV;

    .line 56
    new-instance v0, LaeV;

    const-string v1, "VALUE_FALSE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->j:LaeV;

    .line 59
    new-instance v0, LaeV;

    const-string v1, "VALUE_NULL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->k:LaeV;

    .line 62
    new-instance v0, LaeV;

    const-string v1, "NOT_AVAILABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LaeV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LaeV;->l:LaeV;

    .line 23
    const/16 v0, 0xc

    new-array v0, v0, [LaeV;

    sget-object v1, LaeV;->a:LaeV;

    aput-object v1, v0, v3

    sget-object v1, LaeV;->b:LaeV;

    aput-object v1, v0, v4

    sget-object v1, LaeV;->c:LaeV;

    aput-object v1, v0, v5

    sget-object v1, LaeV;->d:LaeV;

    aput-object v1, v0, v6

    sget-object v1, LaeV;->e:LaeV;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LaeV;->f:LaeV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LaeV;->g:LaeV;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LaeV;->h:LaeV;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LaeV;->i:LaeV;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LaeV;->j:LaeV;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LaeV;->k:LaeV;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LaeV;->l:LaeV;

    aput-object v2, v0, v1

    sput-object v0, LaeV;->a:[LaeV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LaeV;
    .registers 2
    .parameter

    .prologue
    .line 23
    const-class v0, LaeV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LaeV;

    return-object v0
.end method

.method public static values()[LaeV;
    .registers 1

    .prologue
    .line 23
    sget-object v0, LaeV;->a:[LaeV;

    invoke-virtual {v0}, [LaeV;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LaeV;

    return-object v0
.end method
