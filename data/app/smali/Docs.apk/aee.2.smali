.class public Laee;
.super Lafq;
.source "HttpHeaders.java"


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lafu;
        a = "Accept-Encoding"
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation runtime Lafu;
        a = "Content-Type"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lafu;
        a = "Location"
    .end annotation
.end field

.field private d:Ljava/lang/String;
    .annotation runtime Lafu;
        a = "User-Agent"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 59
    sget-object v0, Laft;->a:Laft;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-direct {p0, v0}, Lafq;-><init>(Ljava/util/EnumSet;)V

    .line 67
    const-string v0, "gzip"

    iput-object v0, p0, Laee;->a:Ljava/lang/String;

    .line 60
    return-void
.end method

.method private static a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Type;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/reflect/Type;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 902
    invoke-static {p1, p0}, Lafj;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 903
    invoke-static {v0, p2}, Lafj;->a(Ljava/lang/reflect/Type;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static a(Laee;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Laet;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 696
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Laee;->a(Laee;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Laet;Ljava/io/Writer;)V

    .line 697
    return-void
.end method

.method private static a(Laee;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Laet;Ljava/io/Writer;)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 721
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 722
    invoke-virtual {p0}, Laee;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_d
    :goto_d
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_74

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 723
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 724
    invoke-virtual {v7, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "multiple headers of the same name (headers are case insensitive): %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Lagu;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 726
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 727
    if-eqz v5, :cond_d

    .line 730
    invoke-virtual {p0}, Laee;->a()Lafh;

    move-result-object v0

    invoke-virtual {v0, v1}, Lafh;->a(Ljava/lang/String;)Lafp;

    move-result-object v0

    .line 731
    if-eqz v0, :cond_7a

    .line 732
    invoke-virtual {v0}, Lafp;->a()Ljava/lang/String;

    move-result-object v4

    .line 734
    :goto_42
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 735
    instance-of v1, v5, Ljava/lang/Iterable;

    if-nez v1, :cond_50

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 736
    :cond_50
    invoke-static {v5}, LafB;->a(Ljava/lang/Object;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_58
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v6, p5

    .line 737
    invoke-static/range {v0 .. v6}, Laee;->a(Ljava/util/logging/Logger;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Laet;Ljava/lang/String;Ljava/lang/Object;Ljava/io/Writer;)V

    goto :goto_58

    :cond_6b
    move-object v0, p3

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v6, p5

    .line 746
    invoke-static/range {v0 .. v6}, Laee;->a(Ljava/util/logging/Logger;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Laet;Ljava/lang/String;Ljava/lang/Object;Ljava/io/Writer;)V

    goto :goto_d

    .line 750
    :cond_74
    if-eqz p5, :cond_79

    .line 751
    invoke-virtual {p5}, Ljava/io/Writer;->flush()V

    .line 753
    :cond_79
    return-void

    :cond_7a
    move-object v4, v1

    goto :goto_42
.end method

.method private static a(Ljava/util/logging/Logger;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Laet;Ljava/lang/String;Ljava/lang/Object;Ljava/io/Writer;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 630
    if-eqz p5, :cond_8

    invoke-static {p5}, Lafj;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 661
    :cond_8
    :goto_8
    return-void

    .line 634
    :cond_9
    instance-of v0, p5, Ljava/lang/Enum;

    if-eqz v0, :cond_79

    check-cast p5, Ljava/lang/Enum;

    invoke-static {p5}, Lafp;->a(Ljava/lang/Enum;)Lafp;

    move-result-object v0

    invoke-virtual {v0}, Lafp;->a()Ljava/lang/String;

    move-result-object v0

    .line 638
    :goto_17
    const-string v1, "Authorization"

    invoke-virtual {v1, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_27

    const-string v1, "Cookie"

    invoke-virtual {v1, p4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7e

    :cond_27
    if-eqz p0, :cond_31

    sget-object v1, Ljava/util/logging/Level;->ALL:Ljava/util/logging/Level;

    invoke-virtual {p0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-nez v1, :cond_7e

    .line 640
    :cond_31
    const-string v1, "<Not Logged>"

    .line 642
    :goto_33
    if-eqz p1, :cond_46

    .line 643
    invoke-virtual {p1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 644
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 645
    sget-object v2, LafA;->a:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 647
    :cond_46
    if-eqz p2, :cond_61

    .line 648
    const-string v2, " -H \'"

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 651
    :cond_61
    if-eqz p3, :cond_66

    .line 652
    invoke-virtual {p3, p4, v0}, Laet;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    :cond_66
    if-eqz p6, :cond_8

    .line 656
    invoke-virtual {p6, p4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 657
    const-string v1, ": "

    invoke-virtual {p6, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 658
    invoke-virtual {p6, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 659
    const-string v0, "\r\n"

    invoke-virtual {p6, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_8

    .line 634
    :cond_79
    invoke-virtual {p5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_17

    :cond_7e
    move-object v1, v0

    goto :goto_33
.end method


# virtual methods
.method public a()Laee;
    .registers 2

    .prologue
    .line 160
    invoke-super {p0}, Lafq;->a()Lafq;

    move-result-object v0

    check-cast v0, Laee;

    return-object v0
.end method

.method public bridge synthetic a()Lafq;
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Laee;->a()Laee;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 327
    iget-object v0, p0, Laee;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Laeu;Ljava/lang/StringBuilder;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 764
    new-instance v1, Laef;

    invoke-direct {v1, p0, p2}, Laef;-><init>(Laee;Ljava/lang/StringBuilder;)V

    .line 765
    invoke-virtual {p1}, Laeu;->b()I

    move-result v2

    .line 766
    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_1a

    .line 767
    invoke-virtual {p1, v0}, Laeu;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v0}, Laeu;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4, v1}, Laee;->a(Ljava/lang/String;Ljava/lang/String;Laef;)V

    .line 766
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 769
    :cond_1a
    invoke-virtual {v1}, Laef;->a()V

    .line 770
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 588
    iput-object p1, p0, Laee;->d:Ljava/lang/String;

    .line 589
    return-void
.end method

.method a(Ljava/lang/String;Ljava/lang/String;Laef;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 855
    iget-object v2, p3, Laef;->a:Ljava/util/List;

    .line 856
    iget-object v0, p3, Laef;->a:Lafh;

    .line 857
    iget-object v1, p3, Laef;->a:Laff;

    .line 858
    iget-object v3, p3, Laef;->a:Ljava/lang/StringBuilder;

    .line 860
    if-eqz v3, :cond_2a

    .line 861
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LafA;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 864
    :cond_2a
    invoke-virtual {v0, p1}, Lafh;->a(Ljava/lang/String;)Lafp;

    move-result-object v3

    .line 865
    if-eqz v3, :cond_87

    .line 866
    invoke-virtual {v3}, Lafp;->a()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v2, v0}, Lafj;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v4

    .line 868
    invoke-static {v4}, LafB;->a(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 870
    invoke-static {v4}, LafB;->a(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v2, v0}, LafB;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    .line 872
    invoke-virtual {v3}, Lafp;->a()Ljava/lang/reflect/Field;

    move-result-object v3

    invoke-static {v0, v2, p2}, Laee;->a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v3, v0, v2}, Laff;->a(Ljava/lang/reflect/Field;Ljava/lang/Class;Ljava/lang/Object;)V

    .line 899
    :goto_51
    return-void

    .line 874
    :cond_52
    invoke-static {v2, v4}, LafB;->a(Ljava/util/List;Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/lang/Iterable;

    invoke-static {v0, v1}, LafB;->a(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_7f

    .line 878
    invoke-virtual {v3, p0}, Lafp;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 879
    if-nez v0, :cond_6d

    .line 880
    invoke-static {v4}, Lafj;->a(Ljava/lang/reflect/Type;)Ljava/util/Collection;

    move-result-object v0

    .line 881
    invoke-virtual {v3, p0, v0}, Lafp;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 883
    :cond_6d
    const-class v1, Ljava/lang/Object;

    if-ne v4, v1, :cond_7a

    const/4 v1, 0x0

    .line 884
    :goto_72
    invoke-static {v1, v2, p2}, Laee;->a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_51

    .line 883
    :cond_7a
    invoke-static {v4}, LafB;->b(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    goto :goto_72

    .line 887
    :cond_7f
    invoke-static {v4, v2, p2}, Laee;->a(Ljava/lang/reflect/Type;Ljava/util/List;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, p0, v0}, Lafp;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_51

    .line 892
    :cond_87
    invoke-virtual {p0, p1}, Laee;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 893
    if-nez v0, :cond_97

    .line 894
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 895
    invoke-virtual {p0, p1, v0}, Laee;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 897
    :cond_97
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_51
.end method

.method public final b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 507
    iget-object v0, p0, Laee;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 579
    iget-object v0, p0, Laee;->d:Ljava/lang/String;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 56
    invoke-virtual {p0}, Laee;->a()Laee;

    move-result-object v0

    return-object v0
.end method
