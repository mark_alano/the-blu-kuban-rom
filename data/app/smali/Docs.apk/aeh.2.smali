.class public final enum Laeh;
.super Ljava/lang/Enum;
.source "HttpMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laeh;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laeh;

.field private static final synthetic a:[Laeh;

.field public static final enum b:Laeh;

.field public static final enum c:Laeh;

.field public static final enum d:Laeh;

.field public static final enum e:Laeh;

.field public static final enum f:Laeh;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Laeh;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v3}, Laeh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laeh;->a:Laeh;

    new-instance v0, Laeh;

    const-string v1, "GET"

    invoke-direct {v0, v1, v4}, Laeh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laeh;->b:Laeh;

    new-instance v0, Laeh;

    const-string v1, "HEAD"

    invoke-direct {v0, v1, v5}, Laeh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laeh;->c:Laeh;

    new-instance v0, Laeh;

    const-string v1, "PATCH"

    invoke-direct {v0, v1, v6}, Laeh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laeh;->d:Laeh;

    new-instance v0, Laeh;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v7}, Laeh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laeh;->e:Laeh;

    new-instance v0, Laeh;

    const-string v1, "POST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Laeh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laeh;->f:Laeh;

    .line 23
    const/4 v0, 0x6

    new-array v0, v0, [Laeh;

    sget-object v1, Laeh;->a:Laeh;

    aput-object v1, v0, v3

    sget-object v1, Laeh;->b:Laeh;

    aput-object v1, v0, v4

    sget-object v1, Laeh;->c:Laeh;

    aput-object v1, v0, v5

    sget-object v1, Laeh;->d:Laeh;

    aput-object v1, v0, v6

    sget-object v1, Laeh;->e:Laeh;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Laeh;->f:Laeh;

    aput-object v2, v0, v1

    sput-object v0, Laeh;->a:[Laeh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laeh;
    .registers 2
    .parameter

    .prologue
    .line 23
    const-class v0, Laeh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laeh;

    return-object v0
.end method

.method public static values()[Laeh;
    .registers 1

    .prologue
    .line 23
    sget-object v0, Laeh;->a:[Laeh;

    invoke-virtual {v0}, [Laeh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laeh;

    return-object v0
.end method
