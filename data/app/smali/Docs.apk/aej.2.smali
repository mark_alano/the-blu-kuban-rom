.class public final Laej;
.super Ljava/lang/Object;
.source "HttpRequest.java"


# instance fields
.field private a:I

.field private a:LadW;

.field private a:Laeb;

.field private a:Laec;

.field private a:Laed;

.field private a:Laee;

.field private a:Laeh;

.field private final a:Laeq;

.field private a:Laer;

.field private a:Lafz;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Laei;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private a:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private b:I

.field private b:Laee;

.field private b:Z

.field private c:I

.field private c:Z

.field private d:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method constructor <init>(Laeq;Laeh;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x4e20

    const/4 v1, 0x1

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Laee;

    invoke-direct {v0}, Laee;-><init>()V

    iput-object v0, p0, Laej;->a:Laee;

    .line 84
    new-instance v0, Laee;

    invoke-direct {v0}, Laee;-><init>()V

    iput-object v0, p0, Laej;->b:Laee;

    .line 91
    iput-boolean v1, p0, Laej;->a:Z

    .line 99
    const/16 v0, 0xa

    iput v0, p0, Laej;->a:I

    .line 126
    const/16 v0, 0x4000

    iput v0, p0, Laej;->b:I

    .line 129
    iput-boolean v1, p0, Laej;->b:Z

    .line 132
    iput-boolean v1, p0, Laej;->c:Z

    .line 147
    iput v2, p0, Laej;->c:I

    .line 153
    iput v2, p0, Laej;->d:I

    .line 159
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laej;->a:Ljava/util/Map;

    .line 172
    iput-boolean v1, p0, Laej;->e:Z

    .line 178
    iput-boolean v1, p0, Laej;->f:Z

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Laej;->g:Z

    .line 197
    iput-object p1, p0, Laej;->a:Laeq;

    .line 198
    iput-object p2, p0, Laej;->a:Laeh;

    .line 199
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1137
    if-nez p0, :cond_4

    .line 1138
    const/4 p0, 0x0

    .line 1141
    :cond_3
    :goto_3
    return-object p0

    .line 1140
    :cond_4
    const/16 v0, 0x3b

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 1141
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_3
.end method

.method private a(J)V
    .registers 4
    .parameter

    .prologue
    .line 1118
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_3} :catch_4

    .line 1122
    :goto_3
    return-void

    .line 1119
    :catch_4
    move-exception v0

    goto :goto_3
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 352
    iget v0, p0, Laej;->b:I

    return v0
.end method

.method public a()Laec;
    .registers 2

    .prologue
    .line 254
    iget-object v0, p0, Laej;->a:Laec;

    return-object v0
.end method

.method public a()Laee;
    .registers 2

    .prologue
    .line 497
    iget-object v0, p0, Laej;->a:Laee;

    return-object v0
.end method

.method public a()Laeh;
    .registers 2

    .prologue
    .line 216
    iget-object v0, p0, Laej;->a:Laeh;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Laei;
    .registers 4
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 696
    invoke-static {p1}, Laej;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 697
    iget-object v1, p0, Laej;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laei;

    return-object v0
.end method

.method public a(Laeb;)Laej;
    .registers 3
    .parameter

    .prologue
    .line 244
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeb;

    iput-object v0, p0, Laej;->a:Laeb;

    .line 245
    return-object p0
.end method

.method public a(Laec;)Laej;
    .registers 2
    .parameter

    .prologue
    .line 263
    iput-object p1, p0, Laej;->a:Laec;

    .line 264
    return-object p0
.end method

.method public a(Laed;)Laej;
    .registers 2
    .parameter

    .prologue
    .line 568
    iput-object p1, p0, Laej;->a:Laed;

    .line 569
    return-object p0
.end method

.method public a(Laeh;)Laej;
    .registers 3
    .parameter

    .prologue
    .line 225
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeh;

    iput-object v0, p0, Laej;->a:Laeh;

    .line 226
    return-object p0
.end method

.method public a(Lafz;)Laej;
    .registers 2
    .parameter

    .prologue
    .line 681
    iput-object p1, p0, Laej;->a:Lafz;

    .line 682
    return-object p0
.end method

.method public a(Z)Laej;
    .registers 2
    .parameter

    .prologue
    .line 301
    iput-boolean p1, p0, Laej;->d:Z

    .line 302
    return-object p0
.end method

.method public a()Laen;
    .registers 18

    .prologue
    .line 836
    .line 837
    move-object/from16 v0, p0

    iget v1, v0, Laej;->a:I

    if-ltz v1, :cond_28d

    const/4 v1, 0x1

    :goto_7
    invoke-static {v1}, Lagu;->a(Z)V

    .line 838
    move-object/from16 v0, p0

    iget v1, v0, Laej;->a:I

    .line 839
    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:LadW;

    if-eqz v2, :cond_1b

    .line 841
    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:LadW;

    invoke-interface {v2}, LadW;->a()V

    .line 843
    :cond_1b
    const/4 v3, 0x0

    .line 846
    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:Laeh;

    invoke-static {v2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 847
    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:Laeb;

    invoke-static {v2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move v13, v1

    .line 851
    :goto_2b
    if-eqz v3, :cond_30

    .line 852
    invoke-virtual {v3}, Laen;->a()V

    .line 855
    :cond_30
    const/4 v12, 0x0

    .line 856
    const/4 v11, 0x0

    .line 859
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laed;

    if-eqz v1, :cond_41

    .line 860
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laed;

    move-object/from16 v0, p0

    invoke-interface {v1, v0}, Laed;->b(Laej;)V

    .line 863
    :cond_41
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeb;

    invoke-virtual {v1}, Laeb;->a()Ljava/lang/String;

    move-result-object v15

    .line 865
    sget-object v1, Laek;->a:[I

    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:Laeh;

    invoke-virtual {v2}, Laeh;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_38e

    .line 870
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeq;

    invoke-virtual {v1, v15}, Laeq;->b(Ljava/lang/String;)Laet;

    move-result-object v1

    move-object v8, v1

    .line 889
    :goto_61
    sget-object v16, Laeq;->a:Ljava/util/logging/Logger;

    .line 890
    move-object/from16 v0, p0

    iget-boolean v1, v0, Laej;->b:Z

    if-eqz v1, :cond_2e1

    sget-object v1, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_2e1

    const/4 v1, 0x1

    move v14, v1

    .line 891
    :goto_75
    const/4 v2, 0x0

    .line 892
    const/4 v1, 0x0

    .line 894
    if-eqz v14, :cond_38a

    .line 895
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 896
    const-string v3, "-------------- REQUEST  --------------"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LafA;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 897
    move-object/from16 v0, p0

    iget-object v3, v0, Laej;->a:Laeh;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LafA;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 900
    move-object/from16 v0, p0

    iget-boolean v3, v0, Laej;->c:Z

    if-eqz v3, :cond_386

    .line 901
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "curl -v --compressed"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 902
    move-object/from16 v0, p0

    iget-object v3, v0, Laej;->a:Laeh;

    sget-object v4, Laeh;->b:Laeh;

    invoke-virtual {v3, v4}, Laeh;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c6

    .line 903
    const-string v3, " -X "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Laej;->a:Laeh;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_c6
    move-object v9, v1

    move-object v10, v2

    .line 908
    :goto_c8
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laee;

    invoke-virtual {v1}, Laee;->c()Ljava/lang/String;

    move-result-object v1

    .line 909
    move-object/from16 v0, p0

    iget-boolean v2, v0, Laej;->h:Z

    if-nez v2, :cond_e1

    .line 910
    if-nez v1, :cond_2e5

    .line 911
    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:Laee;

    const-string v3, "Google-HTTP-Java-Client/1.11.0-beta-SNAPSHOT (gzip)"

    invoke-virtual {v2, v3}, Laee;->a(Ljava/lang/String;)V

    .line 917
    :cond_e1
    :goto_e1
    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:Laee;

    move-object/from16 v0, v16

    invoke-static {v2, v10, v9, v0, v8}, Laee;->a(Laee;Ljava/lang/StringBuilder;Ljava/lang/StringBuilder;Ljava/util/logging/Logger;Laet;)V

    .line 918
    move-object/from16 v0, p0

    iget-boolean v2, v0, Laej;->h:Z

    if-nez v2, :cond_f7

    .line 920
    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:Laee;

    invoke-virtual {v2, v1}, Laee;->a(Ljava/lang/String;)V

    .line 924
    :cond_f7
    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:Laec;

    .line 925
    move-object/from16 v0, p0

    iget-boolean v1, v0, Laej;->a:Z

    if-nez v1, :cond_12c

    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeh;

    sget-object v3, Laeh;->e:Laeh;

    if-eq v1, v3, :cond_119

    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeh;

    sget-object v3, Laeh;->f:Laeh;

    if-eq v1, v3, :cond_119

    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeh;

    sget-object v3, Laeh;->d:Laeh;

    if-ne v1, v3, :cond_12c

    :cond_119
    if-eqz v2, :cond_125

    invoke-interface {v2}, Laec;->a()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-nez v1, :cond_12c

    .line 928
    :cond_125
    const/4 v1, 0x0

    const-string v2, " "

    invoke-static {v1, v2}, LadX;->a(Ljava/lang/String;Ljava/lang/String;)LadX;

    move-result-object v2

    .line 930
    :cond_12c
    if-eqz v2, :cond_201

    .line 931
    invoke-interface {v2}, Laec;->a()Ljava/lang/String;

    move-result-object v4

    .line 932
    invoke-interface {v2}, Laec;->a()J

    move-result-wide v5

    .line 933
    invoke-interface {v2}, Laec;->b()Ljava/lang/String;

    move-result-object v3

    .line 935
    if-eqz v14, :cond_383

    .line 936
    new-instance v1, Laes;

    move-object/from16 v0, p0

    iget v7, v0, Laej;->b:I

    invoke-direct/range {v1 .. v7}, Laes;-><init>(Laec;Ljava/lang/String;Ljava/lang/String;JI)V

    .line 940
    :goto_145
    invoke-interface {v1}, Laec;->a()Ljava/lang/String;

    move-result-object v4

    .line 941
    move-object/from16 v0, p0

    iget-boolean v2, v0, Laej;->d:Z

    if-eqz v2, :cond_380

    .line 942
    new-instance v2, Laea;

    invoke-direct {v2, v1, v3}, Laea;-><init>(Laec;Ljava/lang/String;)V

    .line 943
    invoke-interface {v2}, Laec;->a()Ljava/lang/String;

    .line 944
    invoke-interface {v2}, Laec;->a()J

    move-result-wide v5

    .line 947
    :goto_15b
    if-eqz v14, :cond_1f7

    .line 948
    if-eqz v3, :cond_199

    .line 949
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Content-Type: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 950
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v7, LafA;->a:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 951
    if-eqz v9, :cond_199

    .line 952
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " -H \'"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 955
    :cond_199
    if-eqz v4, :cond_1d5

    .line 957
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Content-Encoding: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 958
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, LafA;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 959
    if-eqz v9, :cond_1d5

    .line 960
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " -H \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 963
    :cond_1d5
    const-wide/16 v3, 0x0

    cmp-long v1, v5, v3

    if-ltz v1, :cond_1f7

    .line 964
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Content-Length: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 965
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, LafA;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 969
    :cond_1f7
    if-eqz v9, :cond_1fe

    .line 970
    const-string v1, " -d \'@-\'"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 972
    :cond_1fe
    invoke-virtual {v8, v2}, Laet;->a(Laec;)V

    .line 975
    :cond_201
    if-eqz v14, :cond_226

    .line 976
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 977
    if-eqz v9, :cond_226

    .line 978
    const-string v1, " -- "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 979
    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 980
    if-eqz v2, :cond_21d

    .line 981
    const-string v1, " << $$$"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 983
    :cond_21d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->config(Ljava/lang/String;)V

    .line 989
    :cond_226
    if-lez v13, :cond_307

    if-eqz v2, :cond_230

    invoke-interface {v2}, Laec;->a()Z

    move-result v1

    if-eqz v1, :cond_307

    :cond_230
    const/4 v1, 0x1

    .line 992
    :goto_231
    move-object/from16 v0, p0

    iget v2, v0, Laej;->c:I

    move-object/from16 v0, p0

    iget v3, v0, Laej;->d:I

    invoke-virtual {v8, v2, v3}, Laet;->a(II)V

    .line 994
    :try_start_23c
    invoke-virtual {v8}, Laet;->a()Laeu;
    :try_end_23f
    .catch Ljava/io/IOException; {:try_start_23c .. :try_end_23f} :catch_313

    move-result-object v3

    .line 998
    :try_start_240
    new-instance v2, Laen;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Laen;-><init>(Laej;Laeu;)V
    :try_end_247
    .catchall {:try_start_240 .. :try_end_247} :catchall_30a

    move-object v3, v2

    move-object v2, v11

    .line 1018
    :goto_249
    if-eqz v3, :cond_356

    :try_start_24b
    invoke-virtual {v3}, Laen;->a()Z

    move-result v4

    if-nez v4, :cond_356

    .line 1019
    const/4 v6, 0x0

    .line 1020
    const/4 v5, 0x0

    .line 1021
    const/4 v4, 0x0

    .line 1022
    move-object/from16 v0, p0

    iget-object v7, v0, Laej;->a:Laer;

    if-eqz v7, :cond_264

    .line 1026
    move-object/from16 v0, p0

    iget-object v6, v0, Laej;->a:Laer;

    move-object/from16 v0, p0

    invoke-interface {v6, v0, v3, v1}, Laer;->a(Laej;Laen;Z)Z

    move-result v6

    .line 1029
    :cond_264
    if-nez v6, :cond_277

    .line 1030
    invoke-virtual {v3}, Laen;->a()I

    move-result v7

    invoke-virtual {v3}, Laen;->a()Laee;

    move-result-object v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v8}, Laej;->a(ILaee;)Z

    move-result v7

    if-eqz v7, :cond_327

    .line 1032
    const/4 v5, 0x1

    .line 1048
    :cond_277
    :goto_277
    if-nez v6, :cond_27d

    if-nez v5, :cond_27d

    if-eqz v4, :cond_353

    :cond_27d
    const/4 v4, 0x1

    :goto_27e
    and-int/2addr v1, v4

    .line 1050
    if-eqz v1, :cond_284

    .line 1051
    invoke-virtual {v3}, Laen;->a()V
    :try_end_284
    .catchall {:try_start_24b .. :try_end_284} :catchall_35e

    .line 1059
    :cond_284
    :goto_284
    add-int/lit8 v4, v13, -0x1

    .line 1063
    if-eqz v3, :cond_288

    .line 1067
    :cond_288
    if-nez v1, :cond_37d

    .line 1069
    if-nez v3, :cond_365

    .line 1071
    throw v2

    .line 837
    :cond_28d
    const/4 v1, 0x0

    goto/16 :goto_7

    .line 867
    :pswitch_290
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeq;

    invoke-virtual {v1, v15}, Laeq;->a(Ljava/lang/String;)Laet;

    move-result-object v1

    move-object v8, v1

    .line 868
    goto/16 :goto_61

    .line 873
    :pswitch_29b
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeq;

    invoke-virtual {v1}, Laeq;->a()Z

    move-result v1

    const-string v2, "HTTP transport doesn\'t support HEAD"

    invoke-static {v1, v2}, Lagu;->a(ZLjava/lang/Object;)V

    .line 875
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeq;

    invoke-virtual {v1, v15}, Laeq;->c(Ljava/lang/String;)Laet;

    move-result-object v1

    move-object v8, v1

    .line 876
    goto/16 :goto_61

    .line 878
    :pswitch_2b3
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeq;

    invoke-virtual {v1}, Laeq;->b()Z

    move-result v1

    const-string v2, "HTTP transport doesn\'t support PATCH"

    invoke-static {v1, v2}, Lagu;->a(ZLjava/lang/Object;)V

    .line 880
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeq;

    invoke-virtual {v1, v15}, Laeq;->d(Ljava/lang/String;)Laet;

    move-result-object v1

    move-object v8, v1

    .line 881
    goto/16 :goto_61

    .line 883
    :pswitch_2cb
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeq;

    invoke-virtual {v1, v15}, Laeq;->e(Ljava/lang/String;)Laet;

    move-result-object v1

    move-object v8, v1

    .line 884
    goto/16 :goto_61

    .line 886
    :pswitch_2d6
    move-object/from16 v0, p0

    iget-object v1, v0, Laej;->a:Laeq;

    invoke-virtual {v1, v15}, Laeq;->f(Ljava/lang/String;)Laet;

    move-result-object v1

    move-object v8, v1

    goto/16 :goto_61

    .line 890
    :cond_2e1
    const/4 v1, 0x0

    move v14, v1

    goto/16 :goto_75

    .line 913
    :cond_2e5
    move-object/from16 v0, p0

    iget-object v2, v0, Laej;->a:Laee;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Google-HTTP-Java-Client/1.11.0-beta-SNAPSHOT (gzip)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Laee;->a(Ljava/lang/String;)V

    goto/16 :goto_e1

    .line 989
    :cond_307
    const/4 v1, 0x0

    goto/16 :goto_231

    .line 1001
    :catchall_30a
    move-exception v2

    .line 1002
    :try_start_30b
    invoke-virtual {v3}, Laeu;->a()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v2
    :try_end_313
    .catch Ljava/io/IOException; {:try_start_30b .. :try_end_313} :catch_313

    .line 1005
    :catch_313
    move-exception v2

    move-object v3, v12

    .line 1006
    move-object/from16 v0, p0

    iget-boolean v4, v0, Laej;->g:Z

    if-nez v4, :cond_31c

    .line 1007
    throw v2

    .line 1011
    :cond_31c
    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "exception thrown while executing request"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5, v2}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_249

    .line 1033
    :cond_327
    if-eqz v1, :cond_277

    :try_start_329
    move-object/from16 v0, p0

    iget-object v7, v0, Laej;->a:LadW;

    if-eqz v7, :cond_277

    move-object/from16 v0, p0

    iget-object v7, v0, Laej;->a:LadW;

    invoke-virtual {v3}, Laen;->a()I

    move-result v8

    invoke-interface {v7, v8}, LadW;->a(I)Z

    move-result v7

    if-eqz v7, :cond_277

    .line 1038
    move-object/from16 v0, p0

    iget-object v7, v0, Laej;->a:LadW;

    invoke-interface {v7}, LadW;->a()J

    move-result-wide v7

    .line 1039
    const-wide/16 v9, -0x1

    cmp-long v9, v7, v9

    if-eqz v9, :cond_277

    .line 1040
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v8}, Laej;->a(J)V
    :try_end_350
    .catchall {:try_start_329 .. :try_end_350} :catchall_35e

    .line 1041
    const/4 v4, 0x1

    goto/16 :goto_277

    .line 1048
    :cond_353
    const/4 v4, 0x0

    goto/16 :goto_27e

    .line 1055
    :cond_356
    if-nez v3, :cond_35c

    const/4 v4, 0x1

    :goto_359
    and-int/2addr v1, v4

    goto/16 :goto_284

    :cond_35c
    const/4 v4, 0x0

    goto :goto_359

    .line 1063
    :catchall_35e
    move-exception v1

    if-eqz v3, :cond_364

    .line 1064
    invoke-virtual {v3}, Laen;->b()V

    :cond_364
    throw v1

    .line 1074
    :cond_365
    move-object/from16 v0, p0

    iget-boolean v1, v0, Laej;->f:Z

    if-eqz v1, :cond_37c

    invoke-virtual {v3}, Laen;->a()Z

    move-result v1

    if-nez v1, :cond_37c

    .line 1076
    :try_start_371
    new-instance v1, Laeo;

    invoke-direct {v1, v3}, Laeo;-><init>(Laen;)V

    throw v1
    :try_end_377
    .catchall {:try_start_371 .. :try_end_377} :catchall_377

    .line 1078
    :catchall_377
    move-exception v1

    invoke-virtual {v3}, Laen;->b()V

    throw v1

    .line 1081
    :cond_37c
    return-object v3

    :cond_37d
    move v13, v4

    goto/16 :goto_2b

    :cond_380
    move-object v2, v1

    goto/16 :goto_15b

    :cond_383
    move-object v1, v2

    goto/16 :goto_145

    :cond_386
    move-object v9, v1

    move-object v10, v2

    goto/16 :goto_c8

    :cond_38a
    move-object v9, v1

    move-object v10, v2

    goto/16 :goto_c8

    .line 865
    :pswitch_data_38e
    .packed-switch 0x1
        :pswitch_290
        :pswitch_29b
        :pswitch_2b3
        :pswitch_2cb
        :pswitch_2d6
    .end packed-switch
.end method

.method public a()Laeq;
    .registers 2

    .prologue
    .line 207
    iget-object v0, p0, Laej;->a:Laeq;

    return-object v0
.end method

.method public final a()Lafz;
    .registers 2

    .prologue
    .line 706
    iget-object v0, p0, Laej;->a:Lafz;

    return-object v0
.end method

.method public a(Laei;)V
    .registers 4
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 666
    invoke-interface {p1}, Laei;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Laej;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 667
    iget-object v1, p0, Laej;->a:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 668
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 399
    iget-boolean v0, p0, Laej;->b:Z

    return v0
.end method

.method public a(ILaee;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1096
    invoke-virtual {p2}, Laee;->b()Ljava/lang/String;

    move-result-object v0

    .line 1097
    invoke-virtual {p0}, Laej;->b()Z

    move-result v1

    if-eqz v1, :cond_25

    invoke-static {p1}, Laep;->b(I)Z

    move-result v1

    if-eqz v1, :cond_25

    if-eqz v0, :cond_25

    .line 1099
    new-instance v1, Laeb;

    invoke-direct {v1, v0}, Laeb;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Laej;->a(Laeb;)Laej;

    .line 1101
    const/16 v0, 0x12f

    if-ne p1, v0, :cond_23

    .line 1102
    sget-object v0, Laeh;->b:Laeh;

    invoke-virtual {p0, v0}, Laej;->a(Laeh;)Laej;

    .line 1104
    :cond_23
    const/4 v0, 0x1

    .line 1106
    :goto_24
    return v0

    :cond_25
    const/4 v0, 0x0

    goto :goto_24
.end method

.method public b()Laee;
    .registers 2

    .prologue
    .line 520
    iget-object v0, p0, Laej;->b:Laee;

    return-object v0
.end method

.method public b(Z)Laej;
    .registers 2
    .parameter

    .prologue
    .line 753
    iput-boolean p1, p0, Laej;->f:Z

    .line 754
    return-object p0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 715
    iget-boolean v0, p0, Laej;->e:Z

    return v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 739
    iget-boolean v0, p0, Laej;->f:Z

    return v0
.end method
