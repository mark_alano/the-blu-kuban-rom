.class public abstract LafP;
.super Ljava/lang/Object;
.source "CharMatcher.java"

# interfaces
.implements Lagv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lagv",
        "<",
        "Ljava/lang/Character;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LafP;

.field public static final b:LafP;

.field public static final c:LafP;

.field public static final d:LafP;

.field public static final e:LafP;

.field public static final f:LafP;

.field public static final g:LafP;

.field public static final h:LafP;

.field public static final i:LafP;

.field public static final j:LafP;

.field public static final k:LafP;

.field public static final l:LafP;

.field public static final m:LafP;

.field public static final n:LafP;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/16 v3, 0x200a

    const/16 v9, 0x600

    const/16 v8, 0x2000

    const/16 v7, 0x7f

    const/4 v1, 0x0

    .line 75
    const-string v0, "\t\n\u000b\u000c\r \u0085\u1680\u2028\u2029\u205f\u3000\u00a0\u180e\u202f"

    invoke-static {v0}, LafP;->a(Ljava/lang/CharSequence;)LafP;

    move-result-object v0

    invoke-static {v8, v3}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    invoke-virtual {v0}, LafP;->b()LafP;

    move-result-object v0

    sput-object v0, LafP;->a:LafP;

    .line 87
    const-string v0, "\t\n\u000b\u000c\r \u0085\u1680\u2028\u2029\u205f\u3000"

    invoke-static {v0}, LafP;->a(Ljava/lang/CharSequence;)LafP;

    move-result-object v0

    const/16 v2, 0x2006

    invoke-static {v8, v2}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v2, 0x2008

    invoke-static {v2, v3}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    invoke-virtual {v0}, LafP;->b()LafP;

    move-result-object v0

    sput-object v0, LafP;->b:LafP;

    .line 96
    invoke-static {v1, v7}, LafP;->a(CC)LafP;

    move-result-object v0

    sput-object v0, LafP;->c:LafP;

    .line 105
    const/16 v0, 0x30

    const/16 v2, 0x39

    invoke-static {v0, v2}, LafP;->a(CC)LafP;

    move-result-object v0

    .line 106
    const-string v2, "\u0660\u06f0\u07c0\u0966\u09e6\u0a66\u0ae6\u0b66\u0be6\u0c66\u0ce6\u0d66\u0e50\u0ed0\u0f20\u1040\u1090\u17e0\u1810\u1946\u19d0\u1b50\u1bb0\u1c40\u1c50\ua620\ua8d0\ua900\uaa50\uff10"

    .line 110
    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    array-length v4, v3

    move-object v2, v0

    move v0, v1

    :goto_54
    if-ge v0, v4, :cond_66

    aget-char v5, v3, v0

    .line 111
    add-int/lit8 v6, v5, 0x9

    int-to-char v6, v6

    invoke-static {v5, v6}, LafP;->a(CC)LafP;

    move-result-object v5

    invoke-virtual {v2, v5}, LafP;->a(LafP;)LafP;

    move-result-object v2

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_54

    .line 113
    :cond_66
    invoke-virtual {v2}, LafP;->b()LafP;

    move-result-object v0

    sput-object v0, LafP;->d:LafP;

    .line 120
    new-instance v0, LafQ;

    invoke-direct {v0}, LafQ;-><init>()V

    sput-object v0, LafP;->e:LafP;

    .line 131
    new-instance v0, LafW;

    invoke-direct {v0}, LafW;-><init>()V

    sput-object v0, LafP;->f:LafP;

    .line 141
    new-instance v0, LafX;

    invoke-direct {v0}, LafX;-><init>()V

    sput-object v0, LafP;->g:LafP;

    .line 151
    new-instance v0, LafY;

    invoke-direct {v0}, LafY;-><init>()V

    sput-object v0, LafP;->h:LafP;

    .line 161
    new-instance v0, LafZ;

    invoke-direct {v0}, LafZ;-><init>()V

    sput-object v0, LafP;->i:LafP;

    .line 171
    const/16 v0, 0x1f

    invoke-static {v1, v0}, LafP;->a(CC)LafP;

    move-result-object v0

    const/16 v2, 0x9f

    invoke-static {v7, v2}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    sput-object v0, LafP;->j:LafP;

    .line 179
    const/16 v0, 0x20

    invoke-static {v1, v0}, LafP;->a(CC)LafP;

    move-result-object v0

    const/16 v2, 0xa0

    invoke-static {v7, v2}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v2, 0xad

    invoke-static {v2}, LafP;->a(C)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v2, 0x603

    invoke-static {v9, v2}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const-string v2, "\u06dd\u070f\u1680\u17b4\u17b5\u180e"

    invoke-static {v2}, LafP;->a(Ljava/lang/CharSequence;)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v2, 0x200f

    invoke-static {v8, v2}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v2, 0x2028

    const/16 v3, 0x202f

    invoke-static {v2, v3}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v2, 0x205f

    const/16 v3, 0x2064

    invoke-static {v2, v3}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v2, 0x206a

    const/16 v3, 0x206f

    invoke-static {v2, v3}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v2, 0x3000

    invoke-static {v2}, LafP;->a(C)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const v2, 0xd800

    const v3, 0xf8ff

    invoke-static {v2, v3}, LafP;->a(CC)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const-string v2, "\ufeff\ufff9\ufffa\ufffb"

    invoke-static {v2}, LafP;->a(Ljava/lang/CharSequence;)LafP;

    move-result-object v2

    invoke-virtual {v0, v2}, LafP;->a(LafP;)LafP;

    move-result-object v0

    invoke-virtual {v0}, LafP;->b()LafP;

    move-result-object v0

    sput-object v0, LafP;->k:LafP;

    .line 201
    const/16 v0, 0x4f9

    invoke-static {v1, v0}, LafP;->a(CC)LafP;

    move-result-object v0

    const/16 v1, 0x5be

    invoke-static {v1}, LafP;->a(C)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v1, 0x5d0

    const/16 v2, 0x5ea

    invoke-static {v1, v2}, LafP;->a(CC)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v1, 0x5f3

    invoke-static {v1}, LafP;->a(C)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v1, 0x5f4

    invoke-static {v1}, LafP;->a(C)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v1, 0x6ff

    invoke-static {v9, v1}, LafP;->a(CC)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v1, 0x750

    const/16 v2, 0x77f

    invoke-static {v1, v2}, LafP;->a(CC)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v1, 0xe00

    const/16 v2, 0xe7f

    invoke-static {v1, v2}, LafP;->a(CC)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v1, 0x1e00

    const/16 v2, 0x20af

    invoke-static {v1, v2}, LafP;->a(CC)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const/16 v1, 0x2100

    const/16 v2, 0x213a

    invoke-static {v1, v2}, LafP;->a(CC)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const v1, 0xfb50

    const v2, 0xfdff

    invoke-static {v1, v2}, LafP;->a(CC)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const v1, 0xfe70

    const v2, 0xfeff

    invoke-static {v1, v2}, LafP;->a(CC)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    const v1, 0xff61

    const v2, 0xffdc

    invoke-static {v1, v2}, LafP;->a(CC)LafP;

    move-result-object v1

    invoke-virtual {v0, v1}, LafP;->a(LafP;)LafP;

    move-result-object v0

    invoke-virtual {v0}, LafP;->b()LafP;

    move-result-object v0

    sput-object v0, LafP;->l:LafP;

    .line 217
    new-instance v0, Laga;

    invoke-direct {v0}, Laga;-><init>()V

    sput-object v0, LafP;->m:LafP;

    .line 297
    new-instance v0, Lagb;

    invoke-direct {v0}, Lagb;-><init>()V

    sput-object v0, LafP;->n:LafP;

    return-void
.end method

.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 544
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(C)LafP;
    .registers 2
    .parameter

    .prologue
    .line 380
    new-instance v0, Lagc;

    invoke-direct {v0, p0}, Lagc;-><init>(C)V

    return-object v0
.end method

.method public static a(CC)LafP;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 497
    if-lt p1, p0, :cond_c

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lagu;->a(Z)V

    .line 498
    new-instance v0, LafT;

    invoke-direct {v0, p0, p1}, LafT;-><init>(CC)V

    return-object v0

    .line 497
    :cond_c
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a(Ljava/lang/CharSequence;)LafP;
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 441
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    packed-switch v0, :pswitch_data_34

    .line 465
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 466
    invoke-static {v1}, Ljava/util/Arrays;->sort([C)V

    .line 468
    new-instance v0, LafS;

    invoke-direct {v0, v1}, LafS;-><init>([C)V

    :goto_18
    return-object v0

    .line 443
    :pswitch_19
    sget-object v0, LafP;->n:LafP;

    goto :goto_18

    .line 445
    :pswitch_1c
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, LafP;->a(C)LafP;

    move-result-object v0

    goto :goto_18

    .line 447
    :pswitch_25
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 448
    const/4 v0, 0x1

    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    .line 449
    new-instance v0, LafR;

    invoke-direct {v0, v1, v2}, LafR;-><init>(CC)V

    goto :goto_18

    .line 441
    :pswitch_data_34
    .packed-switch 0x0
        :pswitch_19
        :pswitch_1c
        :pswitch_25
    .end packed-switch
.end method

.method public static b(C)LafP;
    .registers 2
    .parameter

    .prologue
    .line 417
    new-instance v0, Lagd;

    invoke-direct {v0, p0}, Lagd;-><init>(C)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;)I
    .registers 5
    .parameter

    .prologue
    .line 789
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 790
    const/4 v0, 0x0

    :goto_5
    if-ge v0, v1, :cond_15

    .line 791
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, LafP;->a(C)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 795
    :goto_11
    return v0

    .line 790
    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 795
    :cond_15
    const/4 v0, -0x1

    goto :goto_11
.end method

.method public a(Ljava/lang/CharSequence;I)I
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 814
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 815
    invoke-static {p2, v1}, Lagu;->b(II)I

    move v0, p2

    .line 816
    :goto_8
    if-ge v0, v1, :cond_18

    .line 817
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, LafP;->a(C)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 821
    :goto_14
    return v0

    .line 816
    :cond_15
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 821
    :cond_18
    const/4 v0, -0x1

    goto :goto_14
.end method

.method public a()LafP;
    .registers 2

    .prologue
    .line 557
    .line 558
    new-instance v0, LafU;

    invoke-direct {v0, p0, p0}, LafU;-><init>(LafP;LafP;)V

    return-object v0
.end method

.method public a(LafP;)LafP;
    .registers 6
    .parameter

    .prologue
    .line 615
    new-instance v1, Lagf;

    const/4 v0, 0x2

    new-array v2, v0, [LafP;

    const/4 v0, 0x0

    aput-object p0, v2, v0

    const/4 v3, 0x1

    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LafP;

    aput-object v0, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Lagf;-><init>(Ljava/util/List;)V

    return-object v1
.end method

.method public a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 1053
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 1056
    add-int/lit8 v0, v0, -0x1

    :goto_6
    if-ltz v0, :cond_12

    .line 1057
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, LafP;->a(C)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 1062
    :cond_12
    const/4 v1, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1056
    :cond_1e
    add-int/lit8 v0, v0, -0x1

    goto :goto_6
.end method

.method public a(Ljava/lang/CharSequence;C)Ljava/lang/String;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1085
    invoke-virtual {p0, p1}, LafP;->a(Ljava/lang/CharSequence;)I

    move-result v0

    .line 1086
    const/4 v1, -0x1

    if-ne v0, v1, :cond_e

    .line 1087
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1107
    :goto_d
    return-object v0

    .line 1091
    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-interface {p1, v3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1095
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    :goto_26
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-ge v0, v5, :cond_48

    .line 1096
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 1097
    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-virtual {p0, v6}, LafP;->a(Ljava/lang/Character;)Z

    move-result v6

    if-eqz v6, :cond_43

    .line 1098
    if-nez v1, :cond_40

    .line 1099
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v1, v2

    .line 1095
    :cond_40
    :goto_40
    add-int/lit8 v0, v0, 0x1

    goto :goto_26

    .line 1103
    :cond_43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v1, v3

    .line 1104
    goto :goto_40

    .line 1107
    :cond_48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_d
.end method

.method a(Lage;)V
    .registers 5
    .parameter

    .prologue
    .line 696
    const/4 v0, 0x0

    .line 698
    :goto_1
    invoke-virtual {p0, v0}, LafP;->a(C)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 699
    invoke-virtual {p1, v0}, Lage;->a(C)V

    .line 701
    :cond_a
    add-int/lit8 v1, v0, 0x1

    int-to-char v1, v1

    const v2, 0xffff

    if-ne v0, v2, :cond_13

    .line 705
    return-void

    :cond_13
    move v0, v1

    goto :goto_1
.end method

.method public abstract a(C)Z
.end method

.method public a(Ljava/lang/CharSequence;)Z
    .registers 4
    .parameter

    .prologue
    .line 753
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_6
    if-ltz v0, :cond_17

    .line 754
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, LafP;->a(C)Z

    move-result v1

    if-nez v1, :cond_14

    .line 755
    const/4 v0, 0x0

    .line 758
    :goto_13
    return v0

    .line 753
    :cond_14
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 758
    :cond_17
    const/4 v0, 0x1

    goto :goto_13
.end method

.method public a(Ljava/lang/Character;)Z
    .registers 3
    .parameter

    .prologue
    .line 1146
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p0, v0}, LafP;->a(C)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 51
    check-cast p1, Ljava/lang/Character;

    invoke-virtual {p0, p1}, LafP;->a(Ljava/lang/Character;)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/CharSequence;)I
    .registers 4
    .parameter

    .prologue
    .line 835
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_6
    if-ltz v0, :cond_16

    .line 836
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-virtual {p0, v1}, LafP;->a(C)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 840
    :goto_12
    return v0

    .line 835
    :cond_13
    add-int/lit8 v0, v0, -0x1

    goto :goto_6

    .line 840
    :cond_16
    const/4 v0, -0x1

    goto :goto_12
.end method

.method public b()LafP;
    .registers 2

    .prologue
    .line 657
    invoke-static {p0}, Lags;->a(LafP;)LafP;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/CharSequence;)Z
    .registers 4
    .parameter

    .prologue
    .line 773
    invoke-virtual {p0, p1}, LafP;->a(Ljava/lang/CharSequence;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_8
    return v0

    :cond_9
    const/4 v0, 0x0

    goto :goto_8
.end method

.method c()LafP;
    .registers 3

    .prologue
    .line 672
    new-instance v0, Lage;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lage;-><init>(LafQ;)V

    .line 673
    invoke-virtual {p0, v0}, LafP;->a(Lage;)V

    .line 675
    new-instance v1, LafV;

    invoke-direct {v1, p0, v0}, LafV;-><init>(LafP;Lage;)V

    return-object v1
.end method
