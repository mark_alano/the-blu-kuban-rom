.class final Lafm;
.super Ljava/lang/Object;
.source "DataMap.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:I

.field final synthetic a:Lafk;

.field private a:Lafp;

.field private a:Ljava/lang/Object;

.field private a:Z

.field private b:Lafp;

.field private b:Z


# direct methods
.method constructor <init>(Lafk;)V
    .registers 3
    .parameter

    .prologue
    .line 119
    iput-object p1, p0, Lafm;->a:Lafk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const/4 v0, -0x1

    iput v0, p0, Lafm;->a:I

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Map$Entry;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 164
    invoke-virtual {p0}, Lafm;->hasNext()Z

    move-result v0

    if-nez v0, :cond_e

    .line 165
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 167
    :cond_e
    iget-object v0, p0, Lafm;->a:Lafp;

    iput-object v0, p0, Lafm;->b:Lafp;

    .line 168
    iget-object v0, p0, Lafm;->a:Ljava/lang/Object;

    .line 169
    iput-boolean v1, p0, Lafm;->b:Z

    .line 170
    iput-boolean v1, p0, Lafm;->a:Z

    .line 171
    iput-object v2, p0, Lafm;->a:Lafp;

    .line 172
    iput-object v2, p0, Lafm;->a:Ljava/lang/Object;

    .line 173
    new-instance v1, Lafl;

    iget-object v2, p0, Lafm;->a:Lafk;

    iget-object v3, p0, Lafm;->b:Lafp;

    invoke-direct {v1, v2, v3, v0}, Lafl;-><init>(Lafk;Lafp;Ljava/lang/Object;)V

    return-object v1
.end method

.method public hasNext()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 152
    iget-boolean v0, p0, Lafm;->b:Z

    if-nez v0, :cond_45

    .line 153
    iput-boolean v1, p0, Lafm;->b:Z

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lafm;->a:Ljava/lang/Object;

    .line 155
    :goto_a
    iget-object v0, p0, Lafm;->a:Ljava/lang/Object;

    if-nez v0, :cond_45

    iget v0, p0, Lafm;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lafm;->a:I

    iget-object v2, p0, Lafm;->a:Lafk;

    iget-object v2, v2, Lafk;->a:Lafh;

    iget-object v2, v2, Lafh;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_45

    .line 156
    iget-object v0, p0, Lafm;->a:Lafk;

    iget-object v2, v0, Lafk;->a:Lafh;

    iget-object v0, p0, Lafm;->a:Lafk;

    iget-object v0, v0, Lafk;->a:Lafh;

    iget-object v0, v0, Lafh;->a:Ljava/util/List;

    iget v3, p0, Lafm;->a:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lafh;->a(Ljava/lang/String;)Lafp;

    move-result-object v0

    iput-object v0, p0, Lafm;->a:Lafp;

    .line 157
    iget-object v0, p0, Lafm;->a:Lafp;

    iget-object v2, p0, Lafm;->a:Lafk;

    iget-object v2, v2, Lafk;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lafp;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lafm;->a:Ljava/lang/Object;

    goto :goto_a

    .line 160
    :cond_45
    iget-object v0, p0, Lafm;->a:Ljava/lang/Object;

    if-eqz v0, :cond_4b

    move v0, v1

    :goto_4a
    return v0

    :cond_4b
    const/4 v0, 0x0

    goto :goto_4a
.end method

.method public synthetic next()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lafm;->a()Ljava/util/Map$Entry;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    .line 177
    iget-object v0, p0, Lafm;->b:Lafp;

    if-eqz v0, :cond_1a

    iget-boolean v0, p0, Lafm;->a:Z

    if-nez v0, :cond_1a

    move v0, v1

    :goto_a
    invoke-static {v0}, Lagu;->b(Z)V

    .line 178
    iput-boolean v1, p0, Lafm;->a:Z

    .line 179
    iget-object v0, p0, Lafm;->b:Lafp;

    iget-object v1, p0, Lafm;->a:Lafk;

    iget-object v1, v1, Lafk;->a:Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lafp;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 180
    return-void

    .line 177
    :cond_1a
    const/4 v0, 0x0

    goto :goto_a
.end method
