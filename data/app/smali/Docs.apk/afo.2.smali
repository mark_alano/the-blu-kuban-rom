.class public final Lafo;
.super Ljava/lang/Object;
.source "DateTime.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final a:Ljava/util/TimeZone;


# instance fields
.field private final a:J

.field private final a:Ljava/lang/Integer;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 40
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lafo;->a:Ljava/util/TimeZone;

    return-void
.end method

.method public constructor <init>(J)V
    .registers 5
    .parameter

    .prologue
    .line 69
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Lafo;-><init>(ZJLjava/lang/Integer;)V

    .line 70
    return-void
.end method

.method public constructor <init>(ZJLjava/lang/Integer;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-boolean p1, p0, Lafo;->a:Z

    .line 82
    iput-wide p2, p0, Lafo;->a:J

    .line 83
    iput-object p4, p0, Lafo;->a:Ljava/lang/Integer;

    .line 84
    return-void
.end method

.method public static a(Ljava/lang/String;)Lafo;
    .registers 18
    .parameter

    .prologue
    .line 225
    const/4 v1, 0x0

    const/4 v2, 0x4

    :try_start_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 226
    const/4 v1, 0x5

    const/4 v3, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v3, v1, -0x1

    .line 227
    const/16 v1, 0x8

    const/16 v4, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 229
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v12

    .line 230
    const/16 v1, 0xa

    if-le v12, v1, :cond_40

    const/16 v1, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    const/16 v5, 0x54

    if-eq v1, v5, :cond_81

    :cond_40
    const/4 v1, 0x1

    move v11, v1

    .line 231
    :goto_42
    const/4 v9, 0x0

    .line 232
    const/4 v6, 0x0

    .line 233
    const/4 v7, 0x0

    .line 234
    const/4 v5, 0x0

    .line 235
    const/4 v8, 0x0

    .line 236
    if-eqz v11, :cond_84

    .line 237
    const v1, 0x7fffffff

    move v10, v5

    move v5, v9

    move v9, v1

    .line 249
    :goto_4f
    new-instance v1, Ljava/util/GregorianCalendar;

    sget-object v13, Lafo;->a:Ljava/util/TimeZone;

    invoke-direct {v1, v13}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 250
    invoke-virtual/range {v1 .. v7}, Ljava/util/Calendar;->set(IIIIII)V

    .line 251
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v10}, Ljava/util/Calendar;->set(II)V

    .line 252
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 253
    if-le v12, v9, :cond_114

    .line 255
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v1

    const/16 v4, 0x5a

    if-ne v1, v4, :cond_d5

    .line 256
    const/4 v1, 0x0

    move v14, v1

    move-wide v15, v2

    move-wide v1, v15

    move v3, v14

    .line 265
    :goto_77
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 267
    :goto_7b
    new-instance v4, Lafo;

    invoke-direct {v4, v11, v1, v2, v3}, Lafo;-><init>(ZJLjava/lang/Integer;)V

    return-object v4

    .line 230
    :cond_81
    const/4 v1, 0x0

    move v11, v1

    goto :goto_42

    .line 239
    :cond_84
    const/16 v1, 0xb

    const/16 v6, 0xd

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 240
    const/16 v1, 0xe

    const/16 v6, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 241
    const/16 v1, 0x11

    const/16 v7, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 242
    const/16 v1, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v10, 0x2e

    if-ne v1, v10, :cond_ce

    .line 243
    const/16 v1, 0x14

    const/16 v5, 0x17

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 244
    const/16 v1, 0x17

    move v10, v5

    move v5, v9

    move v9, v1

    goto :goto_4f

    .line 246
    :cond_ce
    const/16 v1, 0x13

    move v10, v5

    move v5, v9

    move v9, v1

    goto/16 :goto_4f

    .line 258
    :cond_d5
    add-int/lit8 v1, v9, 0x1

    add-int/lit8 v4, v9, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3c

    add-int/lit8 v4, v9, 0x4

    add-int/lit8 v5, v9, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 260
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C
    :try_end_f9
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_2 .. :try_end_f9} :catch_10b

    move-result v4

    const/16 v5, 0x2d

    if-ne v4, v5, :cond_ff

    .line 261
    neg-int v1, v1

    .line 263
    :cond_ff
    const v4, 0xea60

    mul-int/2addr v4, v1

    int-to-long v4, v4

    sub-long/2addr v2, v4

    move v14, v1

    move-wide v15, v2

    move-wide v1, v15

    move v3, v14

    goto/16 :goto_77

    .line 268
    :catch_10b
    move-exception v1

    .line 269
    new-instance v1, Ljava/lang/NumberFormatException;

    const-string v2, "Invalid date/time format."

    invoke-direct {v1, v2}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_114
    move-wide v14, v2

    move-wide v1, v14

    move-object v3, v8

    goto/16 :goto_7b
.end method

.method private static a(Ljava/lang/StringBuilder;II)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 275
    if-gez p1, :cond_8

    .line 276
    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 277
    neg-int p1, p1

    :cond_8
    move v0, p1

    .line 280
    :goto_9
    if-lez v0, :cond_10

    .line 281
    div-int/lit8 v0, v0, 0xa

    .line 282
    add-int/lit8 p2, p2, -0x1

    goto :goto_9

    .line 284
    :cond_10
    const/4 v0, 0x0

    :goto_11
    if-ge v0, p2, :cond_1b

    .line 285
    const/16 v1, 0x30

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 284
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 287
    :cond_1b
    if-eqz p1, :cond_20

    .line 288
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 290
    :cond_20
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 112
    iget-wide v0, p0, Lafo;->a:J

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .registers 14

    .prologue
    const/16 v12, 0xe

    const/16 v11, 0x3a

    const/16 v10, 0x2d

    const/4 v9, 0x2

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    new-instance v3, Ljava/util/GregorianCalendar;

    sget-object v0, Lafo;->a:Ljava/util/TimeZone;

    invoke-direct {v3, v0}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 139
    iget-wide v0, p0, Lafo;->a:J

    .line 140
    iget-object v4, p0, Lafo;->a:Ljava/lang/Integer;

    .line 141
    if-eqz v4, :cond_22

    .line 142
    invoke-virtual {v4}, Ljava/lang/Integer;->longValue()J

    move-result-wide v5

    const-wide/32 v7, 0xea60

    mul-long/2addr v5, v7

    add-long/2addr v0, v5

    .line 144
    :cond_22
    invoke-virtual {v3, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 146
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x4

    invoke-static {v2, v0, v1}, Lafo;->a(Ljava/lang/StringBuilder;II)V

    .line 147
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 148
    invoke-virtual {v3, v9}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v2, v0, v9}, Lafo;->a(Ljava/lang/StringBuilder;II)V

    .line 149
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 150
    const/4 v0, 0x5

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v2, v0, v9}, Lafo;->a(Ljava/lang/StringBuilder;II)V

    .line 152
    iget-boolean v0, p0, Lafo;->a:Z

    if-nez v0, :cond_82

    .line 154
    const/16 v0, 0x54

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 155
    const/16 v0, 0xb

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v2, v0, v9}, Lafo;->a(Ljava/lang/StringBuilder;II)V

    .line 156
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 157
    const/16 v0, 0xc

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v2, v0, v9}, Lafo;->a(Ljava/lang/StringBuilder;II)V

    .line 158
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 159
    const/16 v0, 0xd

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v2, v0, v9}, Lafo;->a(Ljava/lang/StringBuilder;II)V

    .line 161
    invoke-virtual {v3, v12}, Ljava/util/Calendar;->isSet(I)Z

    move-result v0

    if-eqz v0, :cond_82

    .line 162
    const/16 v0, 0x2e

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 163
    invoke-virtual {v3, v12}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x3

    invoke-static {v2, v0, v1}, Lafo;->a(Ljava/lang/StringBuilder;II)V

    .line 167
    :cond_82
    if-eqz v4, :cond_8f

    .line 169
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_94

    .line 171
    const/16 v0, 0x5a

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 191
    :cond_8f
    :goto_8f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 175
    :cond_94
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 176
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_b1

    .line 177
    const/16 v1, 0x2b

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 183
    :goto_a3
    div-int/lit8 v1, v0, 0x3c

    .line 184
    rem-int/lit8 v0, v0, 0x3c

    .line 185
    invoke-static {v2, v1, v9}, Lafo;->a(Ljava/lang/StringBuilder;II)V

    .line 186
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 187
    invoke-static {v2, v0, v9}, Lafo;->a(Ljava/lang/StringBuilder;II)V

    goto :goto_8f

    .line 179
    :cond_b1
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 180
    neg-int v0, v0

    goto :goto_a3
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 204
    if-ne p1, p0, :cond_5

    .line 211
    :cond_4
    :goto_4
    return v0

    .line 207
    :cond_5
    instance-of v2, p1, Lafo;

    if-nez v2, :cond_b

    move v0, v1

    .line 208
    goto :goto_4

    .line 210
    :cond_b
    check-cast p1, Lafo;

    .line 211
    iget-boolean v2, p0, Lafo;->a:Z

    iget-boolean v3, p1, Lafo;->a:Z

    if-ne v2, v3, :cond_25

    iget-wide v2, p0, Lafo;->a:J

    iget-wide v4, p1, Lafo;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_25

    iget-object v2, p0, Lafo;->a:Ljava/lang/Integer;

    iget-object v3, p1, Lafo;->a:Ljava/lang/Integer;

    invoke-static {v2, v3}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_25
    move v0, v1

    goto :goto_4
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 196
    invoke-virtual {p0}, Lafo;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
