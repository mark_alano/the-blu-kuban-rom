.class final Lafs;
.super Ljava/util/AbstractSet;
.source "GenericData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<",
        "Ljava/util/Map$Entry",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:Lafn;

.field final synthetic a:Lafq;


# direct methods
.method constructor <init>(Lafq;)V
    .registers 4
    .parameter

    .prologue
    .line 206
    iput-object p1, p0, Lafs;->a:Lafq;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 207
    new-instance v0, Lafk;

    iget-object v1, p1, Lafq;->classInfo:Lafh;

    invoke-virtual {v1}, Lafh;->a()Z

    move-result v1

    invoke-direct {v0, p1, v1}, Lafk;-><init>(Ljava/lang/Object;Z)V

    invoke-virtual {v0}, Lafk;->a()Lafn;

    move-result-object v0

    iput-object v0, p0, Lafs;->a:Lafn;

    .line 208
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    .prologue
    .line 222
    iget-object v0, p0, Lafs;->a:Lafq;

    iget-object v0, v0, Lafq;->unknownFields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 223
    iget-object v0, p0, Lafs;->a:Lafn;

    invoke-virtual {v0}, Lafn;->clear()V

    .line 224
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 212
    new-instance v0, Lafr;

    iget-object v1, p0, Lafs;->a:Lafq;

    iget-object v2, p0, Lafs;->a:Lafn;

    invoke-direct {v0, v1, v2}, Lafr;-><init>(Lafq;Lafn;)V

    return-object v0
.end method

.method public size()I
    .registers 3

    .prologue
    .line 217
    iget-object v0, p0, Lafs;->a:Lafq;

    iget-object v0, v0, Lafq;->unknownFields:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iget-object v1, p0, Lafs;->a:Lafn;

    invoke-virtual {v1}, Lafn;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
