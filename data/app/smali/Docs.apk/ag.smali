.class public Lag;
.super Ljava/lang/Object;
.source "AccessibilityDelegateCompat.java"


# static fields
.field private static final a:Laj;

.field private static final b:Ljava/lang/Object;


# instance fields
.field final a:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 295
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_16

    .line 296
    new-instance v0, Lak;

    invoke-direct {v0}, Lak;-><init>()V

    sput-object v0, Lag;->a:Laj;

    .line 302
    :goto_d
    sget-object v0, Lag;->a:Laj;

    invoke-interface {v0}, Laj;->a()Ljava/lang/Object;

    move-result-object v0

    sput-object v0, Lag;->b:Ljava/lang/Object;

    .line 303
    return-void

    .line 297
    :cond_16
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_24

    .line 298
    new-instance v0, Lah;

    invoke-direct {v0}, Lah;-><init>()V

    sput-object v0, Lag;->a:Laj;

    goto :goto_d

    .line 300
    :cond_24
    new-instance v0, Lam;

    invoke-direct {v0}, Lam;-><init>()V

    sput-object v0, Lag;->a:Laj;

    goto :goto_d
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    sget-object v0, Lag;->a:Laj;

    invoke-interface {v0, p0}, Laj;->a(Lag;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lag;->a:Ljava/lang/Object;

    .line 312
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)LbA;
    .registers 4
    .parameter

    .prologue
    .line 480
    sget-object v0, Lag;->a:Laj;

    sget-object v1, Lag;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1}, Laj;->a(Ljava/lang/Object;Landroid/view/View;)LbA;

    move-result-object v0

    return-object v0
.end method

.method a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 318
    iget-object v0, p0, Lag;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Landroid/view/View;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 336
    sget-object v0, Lag;->a:Laj;

    sget-object v1, Lag;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Laj;->a(Ljava/lang/Object;Landroid/view/View;I)V

    .line 337
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 358
    sget-object v0, Lag;->a:Laj;

    sget-object v1, Lag;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Laj;->c(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 359
    return-void
.end method

.method public a(Landroid/view/View;Lbt;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 438
    sget-object v0, Lag;->a:Laj;

    sget-object v1, Lag;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Laj;->a(Ljava/lang/Object;Landroid/view/View;Lbt;)V

    .line 439
    return-void
.end method

.method public a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 500
    sget-object v0, Lag;->a:Laj;

    sget-object v1, Lag;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, Laj;->a(Ljava/lang/Object;Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 379
    sget-object v0, Lag;->a:Laj;

    sget-object v1, Lag;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Laj;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 462
    sget-object v0, Lag;->a:Laj;

    sget-object v1, Lag;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2, p3}, Laj;->a(Ljava/lang/Object;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 399
    sget-object v0, Lag;->a:Laj;

    sget-object v1, Lag;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Laj;->b(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 400
    return-void
.end method

.method public c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 419
    sget-object v0, Lag;->a:Laj;

    sget-object v1, Lag;->b:Ljava/lang/Object;

    invoke-interface {v0, v1, p1, p2}, Laj;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 420
    return-void
.end method
