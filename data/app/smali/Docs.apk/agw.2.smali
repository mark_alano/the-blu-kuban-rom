.class public final Lagw;
.super Ljava/lang/Object;
.source "Splitter.java"


# instance fields
.field private final a:I

.field private final a:LafP;

.field private final a:LagB;

.field private final a:Z


# direct methods
.method private constructor <init>(LagB;)V
    .registers 5
    .parameter

    .prologue
    .line 105
    const/4 v0, 0x0

    sget-object v1, LafP;->n:LafP;

    const v2, 0x7fffffff

    invoke-direct {p0, p1, v0, v1, v2}, Lagw;-><init>(LagB;ZLafP;I)V

    .line 106
    return-void
.end method

.method private constructor <init>(LagB;ZLafP;I)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lagw;->a:LagB;

    .line 111
    iput-boolean p2, p0, Lagw;->a:Z

    .line 112
    iput-object p3, p0, Lagw;->a:LafP;

    .line 113
    iput p4, p0, Lagw;->a:I

    .line 114
    return-void
.end method

.method static synthetic a(Lagw;)I
    .registers 2
    .parameter

    .prologue
    .line 98
    iget v0, p0, Lagw;->a:I

    return v0
.end method

.method static synthetic a(Lagw;)LafP;
    .registers 2
    .parameter

    .prologue
    .line 98
    iget-object v0, p0, Lagw;->a:LafP;

    return-object v0
.end method

.method public static a(LafP;)Lagw;
    .registers 3
    .parameter

    .prologue
    .line 139
    invoke-static {p0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    new-instance v0, Lagw;

    new-instance v1, Lagx;

    invoke-direct {v1, p0}, Lagx;-><init>(LafP;)V

    invoke-direct {v0, v1}, Lagw;-><init>(LagB;)V

    return-object v0
.end method

.method static synthetic a(Lagw;Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lagw;->a(Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    iget-object v0, p0, Lagw;->a:LagB;

    invoke-interface {v0, p0, p1}, LagB;->a(Lagw;Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lagw;)Z
    .registers 2
    .parameter

    .prologue
    .line 98
    iget-boolean v0, p0, Lagw;->a:Z

    return v0
.end method


# virtual methods
.method public a()Lagw;
    .registers 6

    .prologue
    .line 302
    new-instance v0, Lagw;

    iget-object v1, p0, Lagw;->a:LagB;

    const/4 v2, 0x1

    iget-object v3, p0, Lagw;->a:LafP;

    iget v4, p0, Lagw;->a:I

    invoke-direct {v0, v1, v2, v3, v4}, Lagw;-><init>(LagB;ZLafP;I)V

    return-object v0
.end method

.method public a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 371
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    new-instance v0, Lagz;

    invoke-direct {v0, p0, p1}, Lagz;-><init>(Lagw;Ljava/lang/CharSequence;)V

    return-object v0
.end method
