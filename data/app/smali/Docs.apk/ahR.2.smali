.class LahR;
.super Ljava/lang/ref/WeakReference;
.source "LocalCache.java"

# interfaces
.implements Lahz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/WeakReference",
        "<TK;>;",
        "Lahz",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:I

.field volatile a:LahN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LahN",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final e:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILahz;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1347
    invoke-direct {p0, p2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 1425
    invoke-static {}, LagZ;->a()LahN;

    move-result-object v0

    iput-object v0, p0, LahR;->a:LahN;

    .line 1348
    iput p3, p0, LahR;->a:I

    .line 1349
    iput-object p4, p0, LahR;->e:Lahz;

    .line 1350
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 1439
    iget v0, p0, LahR;->a:I

    return v0
.end method

.method public a()J
    .registers 2

    .prologue
    .line 1361
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()LahN;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LahN",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1429
    iget-object v0, p0, LahR;->a:LahN;

    return-object v0
.end method

.method public a()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1444
    iget-object v0, p0, LahR;->e:Lahz;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 1354
    invoke-virtual {p0}, LahR;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 1366
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LahN;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LahN",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1434
    iput-object p1, p0, LahR;->a:LahN;

    .line 1435
    return-void
.end method

.method public a(Lahz;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1376
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()J
    .registers 2

    .prologue
    .line 1393
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1371
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(J)V
    .registers 4
    .parameter

    .prologue
    .line 1398
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(Lahz;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1386
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1381
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(Lahz;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1408
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1403
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d(Lahz;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1418
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()Lahz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1413
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
