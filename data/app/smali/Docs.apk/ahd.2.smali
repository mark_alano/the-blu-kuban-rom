.class final Lahd;
.super Ljava/util/AbstractQueue;
.source "LocalCache.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractQueue",
        "<",
        "Lahz",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final a:Lahz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    .prologue
    .line 3759
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 3760
    new-instance v0, Lahe;

    invoke-direct {v0, p0}, Lahe;-><init>(Lahd;)V

    iput-object v0, p0, Lahd;->a:Lahz;

    return-void
.end method


# virtual methods
.method public a()Lahz;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3811
    iget-object v0, p0, Lahd;->a:Lahz;

    invoke-interface {v0}, Lahz;->b()Lahz;

    move-result-object v0

    .line 3812
    iget-object v1, p0, Lahd;->a:Lahz;

    if-ne v0, v1, :cond_b

    const/4 v0, 0x0

    :cond_b
    return-object v0
.end method

.method public a(Lahz;)Z
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lahz",
            "<TK;TV;>;)Z"
        }
    .end annotation

    .prologue
    .line 3800
    invoke-interface {p1}, Lahz;->c()Lahz;

    move-result-object v0

    invoke-interface {p1}, Lahz;->b()Lahz;

    move-result-object v1

    invoke-static {v0, v1}, LagZ;->a(Lahz;Lahz;)V

    .line 3803
    iget-object v0, p0, Lahd;->a:Lahz;

    invoke-interface {v0}, Lahz;->c()Lahz;

    move-result-object v0

    invoke-static {v0, p1}, LagZ;->a(Lahz;Lahz;)V

    .line 3804
    iget-object v0, p0, Lahd;->a:Lahz;

    invoke-static {p1, v0}, LagZ;->a(Lahz;Lahz;)V

    .line 3806
    const/4 v0, 0x1

    return v0
.end method

.method public b()Lahz;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lahz",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 3817
    iget-object v0, p0, Lahd;->a:Lahz;

    invoke-interface {v0}, Lahz;->b()Lahz;

    move-result-object v0

    .line 3818
    iget-object v1, p0, Lahd;->a:Lahz;

    if-ne v0, v1, :cond_c

    .line 3819
    const/4 v0, 0x0

    .line 3823
    :goto_b
    return-object v0

    .line 3822
    :cond_c
    invoke-virtual {p0, v0}, Lahd;->remove(Ljava/lang/Object;)Z

    goto :goto_b
.end method

.method public clear()V
    .registers 3

    .prologue
    .line 3862
    iget-object v0, p0, Lahd;->a:Lahz;

    invoke-interface {v0}, Lahz;->b()Lahz;

    move-result-object v0

    .line 3863
    :goto_6
    iget-object v1, p0, Lahd;->a:Lahz;

    if-eq v0, v1, :cond_13

    .line 3864
    invoke-interface {v0}, Lahz;->b()Lahz;

    move-result-object v1

    .line 3865
    invoke-static {v0}, LagZ;->b(Lahz;)V

    move-object v0, v1

    .line 3867
    goto :goto_6

    .line 3869
    :cond_13
    iget-object v0, p0, Lahd;->a:Lahz;

    iget-object v1, p0, Lahd;->a:Lahz;

    invoke-interface {v0, v1}, Lahz;->a(Lahz;)V

    .line 3870
    iget-object v0, p0, Lahd;->a:Lahz;

    iget-object v1, p0, Lahd;->a:Lahz;

    invoke-interface {v0, v1}, Lahz;->b(Lahz;)V

    .line 3871
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 3841
    check-cast p1, Lahz;

    .line 3842
    invoke-interface {p1}, Lahz;->b()Lahz;

    move-result-object v0

    sget-object v1, Lahy;->a:Lahy;

    if-eq v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public isEmpty()Z
    .registers 3

    .prologue
    .line 3847
    iget-object v0, p0, Lahd;->a:Lahz;

    invoke-interface {v0}, Lahz;->b()Lahz;

    move-result-object v0

    iget-object v1, p0, Lahd;->a:Lahz;

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lahz",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 3875
    new-instance v0, Lahf;

    invoke-virtual {p0}, Lahd;->a()Lahz;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lahf;-><init>(Lahd;Lahz;)V

    return-object v0
.end method

.method public synthetic offer(Ljava/lang/Object;)Z
    .registers 3
    .parameter

    .prologue
    .line 3759
    check-cast p1, Lahz;

    invoke-virtual {p0, p1}, Lahd;->a(Lahz;)Z

    move-result v0

    return v0
.end method

.method public synthetic peek()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3759
    invoke-virtual {p0}, Lahd;->a()Lahz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic poll()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 3759
    invoke-virtual {p0}, Lahd;->b()Lahz;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .registers 4
    .parameter

    .prologue
    .line 3829
    check-cast p1, Lahz;

    .line 3830
    invoke-interface {p1}, Lahz;->c()Lahz;

    move-result-object v0

    .line 3831
    invoke-interface {p1}, Lahz;->b()Lahz;

    move-result-object v1

    .line 3832
    invoke-static {v0, v1}, LagZ;->a(Lahz;Lahz;)V

    .line 3833
    invoke-static {p1}, LagZ;->b(Lahz;)V

    .line 3835
    sget-object v0, Lahy;->a:Lahy;

    if-eq v1, v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public size()I
    .registers 4

    .prologue
    .line 3852
    const/4 v1, 0x0

    .line 3853
    iget-object v0, p0, Lahd;->a:Lahz;

    invoke-interface {v0}, Lahz;->b()Lahz;

    move-result-object v0

    :goto_7
    iget-object v2, p0, Lahd;->a:Lahz;

    if-eq v0, v2, :cond_12

    .line 3855
    add-int/lit8 v1, v1, 0x1

    .line 3854
    invoke-interface {v0}, Lahz;->b()Lahz;

    move-result-object v0

    goto :goto_7

    .line 3857
    :cond_12
    return v1
.end method
