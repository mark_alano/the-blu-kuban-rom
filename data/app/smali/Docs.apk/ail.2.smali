.class public abstract Lail;
.super Laml;
.source "AbstractIterator.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Laml",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private a:Lain;

.field private a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .registers 2

    .prologue
    .line 68
    invoke-direct {p0}, Laml;-><init>()V

    .line 65
    sget-object v0, Lain;->b:Lain;

    iput-object v0, p0, Lail;->a:Lain;

    .line 68
    return-void
.end method

.method private a()Z
    .registers 3

    .prologue
    .line 142
    sget-object v0, Lain;->d:Lain;

    iput-object v0, p0, Lail;->a:Lain;

    .line 143
    invoke-virtual {p0}, Lail;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lail;->a:Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lail;->a:Lain;

    sget-object v1, Lain;->c:Lain;

    if-eq v0, v1, :cond_16

    .line 145
    sget-object v0, Lain;->a:Lain;

    iput-object v0, p0, Lail;->a:Lain;

    .line 146
    const/4 v0, 0x1

    .line 148
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected final b()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 124
    sget-object v0, Lain;->c:Lain;

    iput-object v0, p0, Lail;->a:Lain;

    .line 125
    const/4 v0, 0x0

    return-object v0
.end method

.method public final hasNext()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    iget-object v0, p0, Lail;->a:Lain;

    sget-object v3, Lain;->d:Lain;

    if-eq v0, v3, :cond_1e

    move v0, v1

    :goto_9
    invoke-static {v0}, Lagu;->b(Z)V

    .line 131
    sget-object v0, Laim;->a:[I

    iget-object v3, p0, Lail;->a:Lain;

    invoke-virtual {v3}, Lain;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_22

    .line 138
    invoke-direct {p0}, Lail;->a()Z

    move-result v2

    :goto_1d
    :pswitch_1d
    return v2

    :cond_1e
    move v0, v2

    .line 130
    goto :goto_9

    :pswitch_20
    move v2, v1

    .line 135
    goto :goto_1d

    .line 131
    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_1d
        :pswitch_20
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 153
    invoke-virtual {p0}, Lail;->hasNext()Z

    move-result v0

    if-nez v0, :cond_c

    .line 154
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 156
    :cond_c
    sget-object v0, Lain;->b:Lain;

    iput-object v0, p0, Lail;->a:Lain;

    .line 157
    iget-object v0, p0, Lail;->a:Ljava/lang/Object;

    return-object v0
.end method
