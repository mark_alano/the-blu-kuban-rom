.class interface abstract LakQ;
.super Ljava/lang/Object;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract a()I
.end method

.method public abstract a()J
.end method

.method public abstract a()LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract a()Lalh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lalh",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation
.end method

.method public abstract a(J)V
.end method

.method public abstract a(LakQ;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract a(Lalh;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lalh",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract b()LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract b(LakQ;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract c()LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract c(LakQ;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract d()LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract d(LakQ;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract e()LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end method
