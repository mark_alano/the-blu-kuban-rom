.class final LakU;
.super LakS;
.source "MapMakerInternalMap.java"

# interfaces
.implements LakQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LakS",
        "<TK;TV;>;",
        "LakQ",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field b:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILakQ;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1321
    invoke-direct {p0, p1, p2, p3, p4}, LakS;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILakQ;)V

    .line 1326
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LakU;->a:J

    .line 1338
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, LakU;->b:LakQ;

    .line 1351
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, LakU;->c:LakQ;

    .line 1322
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 1330
    iget-wide v0, p0, LakU;->a:J

    return-wide v0
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 1335
    iput-wide p1, p0, LakU;->a:J

    .line 1336
    return-void
.end method

.method public a(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1348
    iput-object p1, p0, LakU;->b:LakQ;

    .line 1349
    return-void
.end method

.method public b()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1343
    iget-object v0, p0, LakU;->b:LakQ;

    return-object v0
.end method

.method public b(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1361
    iput-object p1, p0, LakU;->c:LakQ;

    .line 1362
    return-void
.end method

.method public c()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1356
    iget-object v0, p0, LakU;->c:LakQ;

    return-object v0
.end method
