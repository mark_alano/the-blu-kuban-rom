.class final LakV;
.super LakS;
.source "MapMakerInternalMap.java"

# interfaces
.implements LakQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "LakS",
        "<TK;TV;>;",
        "LakQ",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field b:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field e:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILakQ;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1405
    invoke-direct {p0, p1, p2, p3, p4}, LakS;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILakQ;)V

    .line 1410
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, LakV;->a:J

    .line 1422
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, LakV;->b:LakQ;

    .line 1435
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, LakV;->c:LakQ;

    .line 1450
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, LakV;->d:LakQ;

    .line 1463
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, LakV;->e:LakQ;

    .line 1406
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 1414
    iget-wide v0, p0, LakV;->a:J

    return-wide v0
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 1419
    iput-wide p1, p0, LakV;->a:J

    .line 1420
    return-void
.end method

.method public a(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1432
    iput-object p1, p0, LakV;->b:LakQ;

    .line 1433
    return-void
.end method

.method public b()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1427
    iget-object v0, p0, LakV;->b:LakQ;

    return-object v0
.end method

.method public b(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1445
    iput-object p1, p0, LakV;->c:LakQ;

    .line 1446
    return-void
.end method

.method public c()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1440
    iget-object v0, p0, LakV;->c:LakQ;

    return-object v0
.end method

.method public c(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1460
    iput-object p1, p0, LakV;->d:LakQ;

    .line 1461
    return-void
.end method

.method public d()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1455
    iget-object v0, p0, LakV;->d:LakQ;

    return-object v0
.end method

.method public d(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1473
    iput-object p1, p0, LakV;->e:LakQ;

    .line 1474
    return-void
.end method

.method public e()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1468
    iget-object v0, p0, LakV;->e:LakQ;

    return-object v0
.end method
