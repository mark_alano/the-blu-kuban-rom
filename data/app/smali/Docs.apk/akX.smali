.class abstract enum LakX;
.super Ljava/lang/Enum;
.source "MapMakerInternalMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LakX;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LakX;

.field private static final synthetic a:[LakX;

.field public static final enum b:LakX;

.field public static final enum c:LakX;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 296
    new-instance v0, LakY;

    const-string v1, "STRONG"

    invoke-direct {v0, v1, v2}, LakY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LakX;->a:LakX;

    .line 309
    new-instance v0, LakZ;

    const-string v1, "SOFT"

    invoke-direct {v0, v1, v3}, LakZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LakX;->b:LakX;

    .line 322
    new-instance v0, Lala;

    const-string v1, "WEAK"

    invoke-direct {v0, v1, v4}, Lala;-><init>(Ljava/lang/String;I)V

    sput-object v0, LakX;->c:LakX;

    .line 290
    const/4 v0, 0x3

    new-array v0, v0, [LakX;

    sget-object v1, LakX;->a:LakX;

    aput-object v1, v0, v2

    sget-object v1, LakX;->b:LakX;

    aput-object v1, v0, v3

    sget-object v1, LakX;->c:LakX;

    aput-object v1, v0, v4

    sput-object v0, LakX;->a:[LakX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 290
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILako;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 290
    invoke-direct {p0, p1, p2}, LakX;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LakX;
    .registers 2
    .parameter

    .prologue
    .line 290
    const-class v0, LakX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LakX;

    return-object v0
.end method

.method public static values()[LakX;
    .registers 1

    .prologue
    .line 290
    sget-object v0, LakX;->a:[LakX;

    invoke-virtual {v0}, [LakX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LakX;

    return-object v0
.end method


# virtual methods
.method abstract a()Lagh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lagh",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method abstract a(LakR;LakQ;Ljava/lang/Object;)Lalh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "LakR",
            "<TK;TV;>;",
            "LakQ",
            "<TK;TV;>;TV;)",
            "Lalh",
            "<TK;TV;>;"
        }
    .end annotation
.end method
