.class abstract enum Lakf;
.super Ljava/lang/Enum;
.source "MapMaker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lakf;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lakf;

.field private static final synthetic a:[Lakf;

.field public static final enum b:Lakf;

.field public static final enum c:Lakf;

.field public static final enum d:Lakf;

.field public static final enum e:Lakf;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 776
    new-instance v0, Lakg;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v2}, Lakg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakf;->a:Lakf;

    .line 789
    new-instance v0, Lakh;

    const-string v1, "REPLACED"

    invoke-direct {v0, v1, v3}, Lakh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakf;->b:Lakf;

    .line 801
    new-instance v0, Laki;

    const-string v1, "COLLECTED"

    invoke-direct {v0, v1, v4}, Laki;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakf;->c:Lakf;

    .line 812
    new-instance v0, Lakj;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v5}, Lakj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakf;->d:Lakf;

    .line 823
    new-instance v0, Lakk;

    const-string v1, "SIZE"

    invoke-direct {v0, v1, v6}, Lakk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lakf;->e:Lakf;

    .line 771
    const/4 v0, 0x5

    new-array v0, v0, [Lakf;

    sget-object v1, Lakf;->a:Lakf;

    aput-object v1, v0, v2

    sget-object v1, Lakf;->b:Lakf;

    aput-object v1, v0, v3

    sget-object v1, Lakf;->c:Lakf;

    aput-object v1, v0, v4

    sget-object v1, Lakf;->d:Lakf;

    aput-object v1, v0, v5

    sget-object v1, Lakf;->e:Lakf;

    aput-object v1, v0, v6

    sput-object v0, Lakf;->a:[Lakf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 771
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILakc;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 771
    invoke-direct {p0, p1, p2}, Lakf;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lakf;
    .registers 2
    .parameter

    .prologue
    .line 771
    const-class v0, Lakf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lakf;

    return-object v0
.end method

.method public static values()[Lakf;
    .registers 1

    .prologue
    .line 771
    sget-object v0, Lakf;->a:[Lakf;

    invoke-virtual {v0}, [Lakf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lakf;

    return-object v0
.end method
