.class Lal;
.super Ljava/lang/Object;
.source "AccessibilityDelegateCompat.java"

# interfaces
.implements Las;


# instance fields
.field final synthetic a:Lag;

.field final synthetic a:Lak;


# direct methods
.method constructor <init>(Lak;Lag;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 219
    iput-object p1, p0, Lal;->a:Lak;

    iput-object p2, p0, Lal;->a:Lag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 260
    iget-object v0, p0, Lal;->a:Lag;

    invoke-virtual {v0, p1}, Lag;->a(Landroid/view/View;)LbA;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_d

    invoke-virtual {v0}, LbA;->a()Ljava/lang/Object;

    move-result-object v0

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public a(Landroid/view/View;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 250
    iget-object v0, p0, Lal;->a:Lag;

    invoke-virtual {v0, p1, p2}, Lag;->a(Landroid/view/View;I)V

    .line 251
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 228
    iget-object v0, p0, Lal;->a:Lag;

    invoke-virtual {v0, p1, p2}, Lag;->c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 229
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 233
    iget-object v0, p0, Lal;->a:Lag;

    new-instance v1, Lbt;

    invoke-direct {v1, p2}, Lbt;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v1}, Lag;->a(Landroid/view/View;Lbt;)V

    .line 235
    return-void
.end method

.method public a(Landroid/view/View;ILandroid/os/Bundle;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 267
    iget-object v0, p0, Lal;->a:Lag;

    invoke-virtual {v0, p1, p2, p3}, Lag;->a(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 223
    iget-object v0, p0, Lal;->a:Lag;

    invoke-virtual {v0, p1, p2}, Lag;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 245
    iget-object v0, p0, Lal;->a:Lag;

    invoke-virtual {v0, p1, p2, p3}, Lag;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 239
    iget-object v0, p0, Lal;->a:Lag;

    invoke-virtual {v0, p1, p2}, Lag;->b(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 240
    return-void
.end method

.method public c(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 255
    iget-object v0, p0, Lal;->a:Lag;

    invoke-virtual {v0, p1, p2}, Lag;->a(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 256
    return-void
.end method
