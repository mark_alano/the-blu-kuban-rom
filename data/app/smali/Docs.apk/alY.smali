.class public abstract enum LalY;
.super Ljava/lang/Enum;
.source "SortedLists.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LalY;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LalY;

.field private static final synthetic a:[LalY;

.field public static final enum b:LalY;

.field public static final enum c:LalY;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144
    new-instance v0, LalZ;

    const-string v1, "NEXT_LOWER"

    invoke-direct {v0, v1, v2}, LalZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LalY;->a:LalY;

    .line 154
    new-instance v0, Lama;

    const-string v1, "NEXT_HIGHER"

    invoke-direct {v0, v1, v3}, Lama;-><init>(Ljava/lang/String;I)V

    sput-object v0, LalY;->b:LalY;

    .line 172
    new-instance v0, Lamb;

    const-string v1, "INVERTED_INSERTION_INDEX"

    invoke-direct {v0, v1, v4}, Lamb;-><init>(Ljava/lang/String;I)V

    sput-object v0, LalY;->c:LalY;

    .line 139
    const/4 v0, 0x3

    new-array v0, v0, [LalY;

    sget-object v1, LalY;->a:LalY;

    aput-object v1, v0, v2

    sget-object v1, LalY;->b:LalY;

    aput-object v1, v0, v3

    sget-object v1, LalY;->c:LalY;

    aput-object v1, v0, v4

    sput-object v0, LalY;->a:[LalY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILalX;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, LalY;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LalY;
    .registers 2
    .parameter

    .prologue
    .line 139
    const-class v0, LalY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LalY;

    return-object v0
.end method

.method public static values()[LalY;
    .registers 1

    .prologue
    .line 139
    sget-object v0, LalY;->a:[LalY;

    invoke-virtual {v0}, [LalY;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LalY;

    return-object v0
.end method


# virtual methods
.method abstract a(I)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(I)I"
        }
    .end annotation
.end method
