.class Lalb;
.super Ljava/lang/Object;
.source "MapMakerInternalMap.java"

# interfaces
.implements LakQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LakQ",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field final a:I

.field final a:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field volatile a:Lalh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lalh",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;ILakQ;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;I",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1038
    invoke-static {}, Lakn;->a()Lalh;

    move-result-object v0

    iput-object v0, p0, Lalb;->a:Lalh;

    .line 970
    iput-object p1, p0, Lalb;->a:Ljava/lang/Object;

    .line 971
    iput p2, p0, Lalb;->a:I

    .line 972
    iput-object p3, p0, Lalb;->a:LakQ;

    .line 973
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 1054
    iget v0, p0, Lalb;->a:I

    return v0
.end method

.method public a()J
    .registers 2

    .prologue
    .line 984
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1059
    iget-object v0, p0, Lalb;->a:LakQ;

    return-object v0
.end method

.method public a()Lalh;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lalh",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1042
    iget-object v0, p0, Lalb;->a:Lalh;

    return-object v0
.end method

.method public a()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation

    .prologue
    .line 977
    iget-object v0, p0, Lalb;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public a(J)V
    .registers 4
    .parameter

    .prologue
    .line 989
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(LakQ;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 999
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public a(Lalh;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lalh",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1047
    iget-object v0, p0, Lalb;->a:Lalh;

    .line 1048
    iput-object p1, p0, Lalb;->a:Lalh;

    .line 1049
    invoke-interface {v0, p1}, Lalh;->a(Lalh;)V

    .line 1050
    return-void
.end method

.method public b()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 994
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(LakQ;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1009
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1004
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public c(LakQ;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1021
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1016
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public d(LakQ;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1031
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public e()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1026
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
