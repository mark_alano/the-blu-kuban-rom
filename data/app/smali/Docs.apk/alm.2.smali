.class final Lalm;
.super Lalj;
.source "MapMakerInternalMap.java"

# interfaces
.implements LakQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lalj",
        "<TK;TV;>;",
        "LakQ",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field volatile a:J

.field b:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field e:LakQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILakQ;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/ReferenceQueue",
            "<TK;>;TK;I",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1663
    invoke-direct {p0, p1, p2, p3, p4}, Lalj;-><init>(Ljava/lang/ref/ReferenceQueue;Ljava/lang/Object;ILakQ;)V

    .line 1668
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lalm;->a:J

    .line 1680
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, Lalm;->b:LakQ;

    .line 1693
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, Lalm;->c:LakQ;

    .line 1708
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, Lalm;->d:LakQ;

    .line 1721
    invoke-static {}, Lakn;->a()LakQ;

    move-result-object v0

    iput-object v0, p0, Lalm;->e:LakQ;

    .line 1664
    return-void
.end method


# virtual methods
.method public a()J
    .registers 3

    .prologue
    .line 1672
    iget-wide v0, p0, Lalm;->a:J

    return-wide v0
.end method

.method public a(J)V
    .registers 3
    .parameter

    .prologue
    .line 1677
    iput-wide p1, p0, Lalm;->a:J

    .line 1678
    return-void
.end method

.method public a(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1690
    iput-object p1, p0, Lalm;->b:LakQ;

    .line 1691
    return-void
.end method

.method public b()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1685
    iget-object v0, p0, Lalm;->b:LakQ;

    return-object v0
.end method

.method public b(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1703
    iput-object p1, p0, Lalm;->c:LakQ;

    .line 1704
    return-void
.end method

.method public c()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1698
    iget-object v0, p0, Lalm;->c:LakQ;

    return-object v0
.end method

.method public c(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1718
    iput-object p1, p0, Lalm;->d:LakQ;

    .line 1719
    return-void
.end method

.method public d()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1713
    iget-object v0, p0, Lalm;->d:LakQ;

    return-object v0
.end method

.method public d(LakQ;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LakQ",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 1731
    iput-object p1, p0, Lalm;->e:LakQ;

    .line 1732
    return-void
.end method

.method public e()LakQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LakQ",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 1726
    iget-object v0, p0, Lalm;->e:LakQ;

    return-object v0
.end method
