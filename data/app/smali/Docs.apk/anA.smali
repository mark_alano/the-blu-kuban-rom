.class final LanA;
.super Lanl;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanl",
        "<",
        "Ljava/net/URI;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 411
    invoke-direct {p0}, Lanl;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(LanV;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 411
    invoke-virtual {p0, p1}, LanA;->a(LanV;)Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method

.method public a(LanV;)Ljava/net/URI;
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 414
    invoke-virtual {p1}, LanV;->a()LanZ;

    move-result-object v1

    sget-object v2, LanZ;->i:LanZ;

    if-ne v1, v2, :cond_d

    .line 415
    invoke-virtual {p1}, LanV;->e()V

    .line 420
    :cond_c
    :goto_c
    return-object v0

    .line 419
    :cond_d
    :try_start_d
    invoke-virtual {p1}, LanV;->b()Ljava/lang/String;

    move-result-object v1

    .line 420
    const-string v2, "null"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_1e
    .catch Ljava/net/URISyntaxException; {:try_start_d .. :try_end_1e} :catch_1f

    goto :goto_c

    .line 421
    :catch_1f
    move-exception v0

    .line 422
    new-instance v1, Lane;

    invoke-direct {v1, v0}, Lane;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic a(Laoa;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 411
    check-cast p2, Ljava/net/URI;

    invoke-virtual {p0, p1, p2}, LanA;->a(Laoa;Ljava/net/URI;)V

    return-void
.end method

.method public a(Laoa;Ljava/net/URI;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 427
    if-nez p2, :cond_7

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p1, v0}, Laoa;->b(Ljava/lang/String;)Laoa;

    .line 428
    return-void

    .line 427
    :cond_7
    invoke-virtual {p2}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method
