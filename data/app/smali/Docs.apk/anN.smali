.class final LanN;
.super Lanl;
.source "TypeAdapters.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lanl",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 132
    invoke-direct {p0}, Lanl;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LanV;)Ljava/lang/Boolean;
    .registers 4
    .parameter

    .prologue
    .line 135
    invoke-virtual {p1}, LanV;->a()LanZ;

    move-result-object v0

    sget-object v1, LanZ;->i:LanZ;

    if-ne v0, v1, :cond_d

    .line 136
    invoke-virtual {p1}, LanV;->e()V

    .line 137
    const/4 v0, 0x0

    .line 142
    :goto_c
    return-object v0

    .line 138
    :cond_d
    invoke-virtual {p1}, LanV;->a()LanZ;

    move-result-object v0

    sget-object v1, LanZ;->f:LanZ;

    if-ne v0, v1, :cond_22

    .line 140
    invoke-virtual {p1}, LanV;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_c

    .line 142
    :cond_22
    invoke-virtual {p1}, LanV;->c()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_c
.end method

.method public bridge synthetic a(LanV;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 132
    invoke-virtual {p0, p1}, LanN;->a(LanV;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public a(Laoa;Ljava/lang/Boolean;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 146
    if-nez p2, :cond_6

    .line 147
    invoke-virtual {p1}, Laoa;->e()Laoa;

    .line 151
    :goto_5
    return-void

    .line 150
    :cond_6
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Laoa;->a(Z)Laoa;

    goto :goto_5
.end method

.method public bridge synthetic a(Laoa;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 132
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, LanN;->a(Laoa;Ljava/lang/Boolean;)V

    return-void
.end method
