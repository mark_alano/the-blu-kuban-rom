.class public final Lanr;
.super Ljava/lang/Object;
.source "TypeAdapters.java"


# static fields
.field public static final a:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Class;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lanm;

.field public static final b:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lanm;

.field public static final c:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lanm;

.field public static final d:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lanm;

.field public static final e:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lanm;

.field public static final f:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lanm;

.field public static final g:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lanm;

.field public static final h:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Lanm;

.field public static final i:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lanm;

.field public static final j:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:Lanm;

.field public static final k:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lanm;

.field public static final l:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lanm;

.field public static final m:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lanm;

.field public static final n:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final n:Lanm;

.field public static final o:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lanm;

.field public static final p:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final p:Lanm;

.field public static final q:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lanm;

.field public static final r:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final r:Lanm;

.field public static final s:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:Lanm;

.field public static final t:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final t:Lanm;

.field public static final u:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final v:Lanl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lanl",
            "<",
            "Land;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 59
    new-instance v0, Lans;

    invoke-direct {v0}, Lans;-><init>()V

    sput-object v0, Lanr;->a:Lanl;

    .line 71
    const-class v0, Ljava/lang/Class;

    sget-object v1, Lanr;->a:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->a:Lanm;

    .line 73
    new-instance v0, LanD;

    invoke-direct {v0}, LanD;-><init>()V

    sput-object v0, Lanr;->b:Lanl;

    .line 130
    const-class v0, Ljava/util/BitSet;

    sget-object v1, Lanr;->b:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->b:Lanm;

    .line 132
    new-instance v0, LanN;

    invoke-direct {v0}, LanN;-><init>()V

    sput-object v0, Lanr;->c:Lanl;

    .line 158
    new-instance v0, LanP;

    invoke-direct {v0}, LanP;-><init>()V

    sput-object v0, Lanr;->d:Lanl;

    .line 172
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lanr;->c:Lanl;

    invoke-static {v0, v1, v2}, Lanr;->a(Ljava/lang/Class;Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->c:Lanm;

    .line 175
    new-instance v0, LanQ;

    invoke-direct {v0}, LanQ;-><init>()V

    sput-object v0, Lanr;->e:Lanl;

    .line 195
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lanr;->e:Lanl;

    invoke-static {v0, v1, v2}, Lanr;->a(Ljava/lang/Class;Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->d:Lanm;

    .line 198
    new-instance v0, LanR;

    invoke-direct {v0}, LanR;-><init>()V

    sput-object v0, Lanr;->f:Lanl;

    .line 217
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Short;

    sget-object v2, Lanr;->f:Lanl;

    invoke-static {v0, v1, v2}, Lanr;->a(Ljava/lang/Class;Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->e:Lanm;

    .line 220
    new-instance v0, LanS;

    invoke-direct {v0}, LanS;-><init>()V

    sput-object v0, Lanr;->g:Lanl;

    .line 239
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lanr;->g:Lanl;

    invoke-static {v0, v1, v2}, Lanr;->a(Ljava/lang/Class;Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->f:Lanm;

    .line 242
    new-instance v0, LanT;

    invoke-direct {v0}, LanT;-><init>()V

    sput-object v0, Lanr;->h:Lanl;

    .line 261
    new-instance v0, LanU;

    invoke-direct {v0}, LanU;-><init>()V

    sput-object v0, Lanr;->i:Lanl;

    .line 276
    new-instance v0, Lant;

    invoke-direct {v0}, Lant;-><init>()V

    sput-object v0, Lanr;->j:Lanl;

    .line 291
    new-instance v0, Lanu;

    invoke-direct {v0}, Lanu;-><init>()V

    sput-object v0, Lanr;->k:Lanl;

    .line 311
    const-class v0, Ljava/lang/Number;

    sget-object v1, Lanr;->k:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->g:Lanm;

    .line 313
    new-instance v0, Lanv;

    invoke-direct {v0}, Lanv;-><init>()V

    sput-object v0, Lanr;->l:Lanl;

    .line 332
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Character;

    sget-object v2, Lanr;->l:Lanl;

    invoke-static {v0, v1, v2}, Lanr;->a(Ljava/lang/Class;Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->h:Lanm;

    .line 335
    new-instance v0, Lanw;

    invoke-direct {v0}, Lanw;-><init>()V

    sput-object v0, Lanr;->m:Lanl;

    .line 355
    const-class v0, Ljava/lang/String;

    sget-object v1, Lanr;->m:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->i:Lanm;

    .line 357
    new-instance v0, Lanx;

    invoke-direct {v0}, Lanx;-><init>()V

    sput-object v0, Lanr;->n:Lanl;

    .line 372
    const-class v0, Ljava/lang/StringBuilder;

    sget-object v1, Lanr;->n:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->j:Lanm;

    .line 375
    new-instance v0, Lany;

    invoke-direct {v0}, Lany;-><init>()V

    sput-object v0, Lanr;->o:Lanl;

    .line 390
    const-class v0, Ljava/lang/StringBuffer;

    sget-object v1, Lanr;->o:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->k:Lanm;

    .line 393
    new-instance v0, Lanz;

    invoke-direct {v0}, Lanz;-><init>()V

    sput-object v0, Lanr;->p:Lanl;

    .line 409
    const-class v0, Ljava/net/URL;

    sget-object v1, Lanr;->p:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->l:Lanm;

    .line 411
    new-instance v0, LanA;

    invoke-direct {v0}, LanA;-><init>()V

    sput-object v0, Lanr;->q:Lanl;

    .line 431
    const-class v0, Ljava/net/URI;

    sget-object v1, Lanr;->q:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->m:Lanm;

    .line 433
    new-instance v0, LanB;

    invoke-direct {v0}, LanB;-><init>()V

    sput-object v0, Lanr;->r:Lanl;

    .line 449
    const-class v0, Ljava/net/InetAddress;

    sget-object v1, Lanr;->r:Lanl;

    invoke-static {v0, v1}, Lanr;->b(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->n:Lanm;

    .line 452
    new-instance v0, LanC;

    invoke-direct {v0}, LanC;-><init>()V

    sput-object v0, Lanr;->s:Lanl;

    .line 467
    const-class v0, Ljava/util/UUID;

    sget-object v1, Lanr;->s:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->o:Lanm;

    .line 469
    new-instance v0, LanE;

    invoke-direct {v0}, LanE;-><init>()V

    sput-object v0, Lanr;->p:Lanm;

    .line 490
    new-instance v0, LanF;

    invoke-direct {v0}, LanF;-><init>()V

    sput-object v0, Lanr;->t:Lanl;

    .line 555
    const-class v0, Ljava/util/Calendar;

    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lanr;->t:Lanl;

    invoke-static {v0, v1, v2}, Lanr;->b(Ljava/lang/Class;Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->q:Lanm;

    .line 558
    new-instance v0, LanG;

    invoke-direct {v0}, LanG;-><init>()V

    sput-object v0, Lanr;->u:Lanl;

    .line 593
    const-class v0, Ljava/util/Locale;

    sget-object v1, Lanr;->u:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->r:Lanm;

    .line 595
    new-instance v0, LanH;

    invoke-direct {v0}, LanH;-><init>()V

    sput-object v0, Lanr;->v:Lanl;

    .line 667
    const-class v0, Land;

    sget-object v1, Lanr;->v:Lanl;

    invoke-static {v0, v1}, Lanr;->a(Ljava/lang/Class;Lanl;)Lanm;

    move-result-object v0

    sput-object v0, Lanr;->s:Lanm;

    .line 702
    invoke-static {}, Lanr;->a()Lanm;

    move-result-object v0

    sput-object v0, Lanr;->t:Lanm;

    return-void
.end method

.method public static a()Lanm;
    .registers 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">()",
            "Lanm;"
        }
    .end annotation

    .prologue
    .line 705
    new-instance v0, LanI;

    invoke-direct {v0}, LanI;-><init>()V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Lanl;)Lanm;
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lanl",
            "<TTT;>;)",
            "Lanm;"
        }
    .end annotation

    .prologue
    .line 732
    new-instance v0, LanJ;

    invoke-direct {v0, p0, p1}, LanJ;-><init>(Ljava/lang/Class;Lanl;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;Lanl;)Lanm;
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lanl",
            "<-TTT;>;)",
            "Lanm;"
        }
    .end annotation

    .prologue
    .line 745
    new-instance v0, LanK;

    invoke-direct {v0, p0, p1, p2}, LanK;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lanl;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Lanl;)Lanm;
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lanl",
            "<TTT;>;)",
            "Lanm;"
        }
    .end annotation

    .prologue
    .line 775
    new-instance v0, LanM;

    invoke-direct {v0, p0, p1}, LanM;-><init>(Ljava/lang/Class;Lanl;)V

    return-object v0
.end method

.method public static b(Ljava/lang/Class;Ljava/lang/Class;Lanl;)Lanm;
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Ljava/lang/Class",
            "<+TTT;>;",
            "Lanl",
            "<-TTT;>;)",
            "Lanm;"
        }
    .end annotation

    .prologue
    .line 760
    new-instance v0, LanL;

    invoke-direct {v0, p0, p1, p2}, LanL;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lanl;)V

    return-object v0
.end method
