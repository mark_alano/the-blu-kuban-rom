.class public abstract LaoS;
.super Ljava/lang/Object;
.source "AbstractBindingBuilder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field protected static final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field protected a:I

.field private a:LaoY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaoY",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected final a:Lcom/google/inject/Binder;

.field protected a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lari;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 47
    const-class v0, Ljava/lang/Void;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    sput-object v0, LaoS;->a:Laop;

    return-void
.end method

.method public constructor <init>(Lcom/google/inject/Binder;Ljava/util/List;Ljava/lang/Object;Laop;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/inject/Binder;",
            "Ljava/util/List",
            "<",
            "Lari;",
            ">;",
            "Ljava/lang/Object;",
            "Laop",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, LaoS;->a:Lcom/google/inject/Binder;

    .line 56
    iput-object p2, p0, LaoS;->a:Ljava/util/List;

    .line 57
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, LaoS;->a:I

    .line 58
    new-instance v0, LaqQ;

    sget-object v1, LaqC;->a:LaqC;

    invoke-direct {v0, p3, p4, v1}, LaqQ;-><init>(Ljava/lang/Object;Laop;LaqC;)V

    iput-object v0, p0, LaoS;->a:LaoY;

    .line 59
    iget v0, p0, LaoS;->a:I

    iget-object v1, p0, LaoS;->a:LaoY;

    invoke-interface {p2, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 60
    return-void
.end method


# virtual methods
.method protected a()LaoY;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, LaoS;->a:LaoY;

    return-object v0
.end method

.method protected a(LaoY;)LaoY;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaoY",
            "<TT;>;)",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 67
    iput-object p1, p0, LaoS;->a:LaoY;

    .line 68
    iget-object v0, p0, LaoS;->a:Ljava/util/List;

    iget v1, p0, LaoS;->a:I

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-object p1
.end method

.method protected a(Ljava/lang/Class;)LaoY;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 74
    const-string v0, "annotationType"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-virtual {p0}, LaoS;->b()V

    .line 76
    iget-object v0, p0, LaoS;->a:LaoY;

    iget-object v1, p0, LaoS;->a:LaoY;

    invoke-virtual {v1}, LaoY;->a()Laop;

    move-result-object v1

    invoke-virtual {v1}, Laop;->a()LaoL;

    move-result-object v1

    invoke-static {v1, p1}, Laop;->a(LaoL;Ljava/lang/Class;)Laop;

    move-result-object v1

    invoke-virtual {v0, v1}, LaoY;->a(Laop;)LaoY;

    move-result-object v0

    invoke-virtual {p0, v0}, LaoS;->a(LaoY;)LaoY;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/annotation/Annotation;)LaoY;
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 82
    const-string v0, "annotation"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-virtual {p0}, LaoS;->b()V

    .line 84
    iget-object v0, p0, LaoS;->a:LaoY;

    iget-object v1, p0, LaoS;->a:LaoY;

    invoke-virtual {v1}, LaoY;->a()Laop;

    move-result-object v1

    invoke-virtual {v1}, Laop;->a()LaoL;

    move-result-object v1

    invoke-static {v1, p1}, Laop;->a(LaoL;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v1

    invoke-virtual {v0, v1}, LaoY;->a(Laop;)LaoY;

    move-result-object v0

    invoke-virtual {p0, v0}, LaoS;->a(LaoY;)LaoY;

    move-result-object v0

    return-object v0
.end method

.method protected a()V
    .registers 4

    .prologue
    .line 110
    iget-object v0, p0, LaoS;->a:LaoY;

    instance-of v0, v0, LaqQ;

    if-nez v0, :cond_10

    .line 111
    iget-object v0, p0, LaoS;->a:Lcom/google/inject/Binder;

    const-string v1, "Implementation is set more than once."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/inject/Binder;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    :cond_10
    return-void
.end method

.method public a(LaoC;)V
    .registers 4
    .parameter

    .prologue
    .line 95
    const-string v0, "scope"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    invoke-virtual {p0}, LaoS;->c()V

    .line 97
    invoke-virtual {p0}, LaoS;->a()LaoY;

    move-result-object v0

    invoke-static {p1}, LaqC;->a(LaoC;)LaqC;

    move-result-object v1

    invoke-virtual {v0, v1}, LaoY;->a(LaqC;)LaoY;

    move-result-object v0

    invoke-virtual {p0, v0}, LaoS;->a(LaoY;)LaoY;

    .line 98
    return-void
.end method

.method public a(Ljava/lang/Class;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    const-string v0, "scopeAnnotation"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    invoke-virtual {p0}, LaoS;->c()V

    .line 91
    invoke-virtual {p0}, LaoS;->a()LaoY;

    move-result-object v0

    invoke-static {p1}, LaqC;->a(Ljava/lang/Class;)LaqC;

    move-result-object v1

    invoke-virtual {v0, v1}, LaoY;->a(LaqC;)LaoY;

    move-result-object v0

    invoke-virtual {p0, v0}, LaoS;->a(LaoY;)LaoY;

    .line 92
    return-void
.end method

.method protected a()Z
    .registers 3

    .prologue
    .line 106
    const-class v0, Ljava/lang/Void;

    iget-object v1, p0, LaoS;->a:LaoY;

    invoke-virtual {v1}, LaoY;->a()Laop;

    move-result-object v1

    invoke-virtual {v1}, Laop;->a()LaoL;

    move-result-object v1

    invoke-virtual {v1}, LaoL;->a()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18

    const/4 v0, 0x1

    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method protected b()V
    .registers 4

    .prologue
    .line 116
    iget-object v0, p0, LaoS;->a:LaoY;

    invoke-virtual {v0}, LaoY;->a()Laop;

    move-result-object v0

    invoke-virtual {v0}, Laop;->a()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_16

    .line 117
    iget-object v0, p0, LaoS;->a:Lcom/google/inject/Binder;

    const-string v1, "More than one annotation is specified for this binding."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/inject/Binder;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    :cond_16
    return-void
.end method

.method protected c()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 123
    iget-object v0, p0, LaoS;->a:LaoY;

    instance-of v0, v0, Lary;

    if-eqz v0, :cond_11

    .line 124
    iget-object v0, p0, LaoS;->a:Lcom/google/inject/Binder;

    const-string v1, "Setting the scope is not permitted when binding to a single instance."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/inject/Binder;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    :cond_10
    :goto_10
    return-void

    .line 128
    :cond_11
    iget-object v0, p0, LaoS;->a:LaoY;

    invoke-virtual {v0}, LaoY;->a()LaqC;

    move-result-object v0

    invoke-virtual {v0}, LaqC;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 129
    iget-object v0, p0, LaoS;->a:Lcom/google/inject/Binder;

    const-string v1, "Scope is set more than once."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/inject/Binder;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_10
.end method
