.class public LaoX;
.super LaoS;
.source "BindingBuilder.java"

# interfaces
.implements LaoM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LaoS",
        "<TT;>;",
        "LaoM",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/inject/Binder;Ljava/util/List;Ljava/lang/Object;Laop;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/inject/Binder;",
            "Ljava/util/List",
            "<",
            "Lari;",
            ">;",
            "Ljava/lang/Object;",
            "Laop",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, LaoS;-><init>(Lcom/google/inject/Binder;Ljava/util/List;Ljava/lang/Object;Laop;)V

    .line 48
    return-void
.end method

.method private a(Laoh;)V
    .registers 5
    .parameter

    .prologue
    .line 176
    invoke-virtual {p1}, Laoh;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarB;

    .line 177
    iget-object v2, p0, LaoX;->a:Lcom/google/inject/Binder;

    invoke-interface {v2, v0}, Lcom/google/inject/Binder;->a(LarB;)V

    goto :goto_8

    .line 179
    :cond_1a
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/annotation/Annotation;)LaoQ;
    .registers 3
    .parameter

    .prologue
    .line 43
    invoke-virtual {p0, p1}, LaoX;->a(Ljava/lang/annotation/Annotation;)LaoX;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Laoz;)LaoR;
    .registers 3
    .parameter

    .prologue
    .line 43
    invoke-virtual {p0, p1}, LaoX;->a(Laoz;)LaoX;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Ljava/lang/Class;)LaoR;
    .registers 3
    .parameter

    .prologue
    .line 43
    invoke-virtual {p0, p1}, LaoX;->a(Ljava/lang/Class;)LaoX;

    move-result-object v0

    return-object v0
.end method

.method public a(Laop;)LaoX;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<+TT;>;)",
            "LaoX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 69
    const-string v0, "linkedKey"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-virtual {p0}, LaoX;->a()V

    .line 71
    invoke-virtual {p0}, LaoX;->a()LaoY;

    move-result-object v0

    .line 72
    new-instance v1, Laqb;

    invoke-virtual {v0}, LaoY;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0}, LaoY;->a()Laop;

    move-result-object v3

    invoke-virtual {v0}, LaoY;->a()LaqC;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0, p1}, Laqb;-><init>(Ljava/lang/Object;Laop;LaqC;Laop;)V

    invoke-virtual {p0, v1}, LaoX;->a(LaoY;)LaoY;

    .line 74
    return-object p0
.end method

.method public a(Laoz;)LaoX;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<+TT;>;)",
            "LaoX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 100
    const-string v0, "provider"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-virtual {p0}, LaoX;->a()V

    .line 106
    :try_start_8
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Larp;->a(Ljava/lang/Class;)Ljava/util/Set;
    :try_end_f
    .catch Laoh; {:try_start_8 .. :try_end_f} :catch_2a

    move-result-object v4

    .line 112
    :goto_10
    invoke-virtual {p0}, LaoX;->a()LaoY;

    move-result-object v3

    .line 113
    new-instance v0, Laqp;

    invoke-virtual {v3}, LaoY;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3}, LaoY;->a()Laop;

    move-result-object v2

    invoke-virtual {v3}, LaoY;->a()LaqC;

    move-result-object v3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Laqp;-><init>(Ljava/lang/Object;Laop;LaqC;Ljava/util/Set;Laoz;)V

    invoke-virtual {p0, v0}, LaoX;->a(LaoY;)LaoY;

    .line 115
    return-object p0

    .line 107
    :catch_2a
    move-exception v0

    .line 108
    invoke-direct {p0, v0}, LaoX;->a(Laoh;)V

    .line 109
    invoke-virtual {v0}, Laoh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    move-object v4, v0

    goto :goto_10
.end method

.method public a(Ljava/lang/Class;)LaoX;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+TT;>;)",
            "LaoX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 61
    invoke-static {p1}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-virtual {p0, v0}, LaoX;->a(Laop;)LaoX;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/annotation/Annotation;)LaoX;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/annotation/Annotation;",
            ")",
            "LaoX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0, p1}, LaoX;->a(Ljava/lang/annotation/Annotation;)LaoY;

    .line 57
    return-object p0
.end method

.method public a(Ljava/lang/Object;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p0}, LaoX;->a()V

    .line 82
    if-eqz p1, :cond_31

    .line 84
    :try_start_5
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Larp;->a(Ljava/lang/Class;)Ljava/util/Set;
    :try_end_c
    .catch Laoh; {:try_start_5 .. :try_end_c} :catch_25

    move-result-object v4

    .line 94
    :goto_d
    invoke-virtual {p0}, LaoX;->a()LaoY;

    move-result-object v2

    .line 95
    new-instance v0, LapW;

    invoke-virtual {v2}, LaoY;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2}, LaoY;->a()Laop;

    move-result-object v2

    sget-object v3, LaqC;->d:LaqC;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LapW;-><init>(Ljava/lang/Object;Laop;LaqC;Ljava/util/Set;Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, LaoX;->a(LaoY;)LaoY;

    .line 97
    return-void

    .line 85
    :catch_25
    move-exception v0

    .line 86
    invoke-direct {p0, v0}, LaoX;->a(Laoh;)V

    .line 87
    invoke-virtual {v0}, Laoh;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    move-object v4, v0

    .line 88
    goto :goto_d

    .line 90
    :cond_31
    iget-object v0, p0, LaoX;->a:Lcom/google/inject/Binder;

    const-string v1, "Binding to null instances is not allowed. Use toProvider(Providers.of(null)) if this is your intended behaviour."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/google/inject/Binder;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    invoke-static {}, Lajm;->a()Lajm;

    move-result-object v4

    goto :goto_d
.end method

.method public bridge synthetic b(Ljava/lang/Class;)LaoR;
    .registers 3
    .parameter

    .prologue
    .line 43
    invoke-virtual {p0, p1}, LaoX;->b(Ljava/lang/Class;)LaoX;

    move-result-object v0

    return-object v0
.end method

.method public b(Laop;)LaoX;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<+",
            "LatG",
            "<+TT;>;>;)",
            "LaoX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 130
    const-string v0, "providerKey"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    invoke-virtual {p0}, LaoX;->a()V

    .line 133
    invoke-virtual {p0}, LaoX;->a()LaoY;

    move-result-object v0

    .line 134
    new-instance v1, Laqc;

    invoke-virtual {v0}, LaoY;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0}, LaoY;->a()Laop;

    move-result-object v3

    invoke-virtual {v0}, LaoY;->a()LaqC;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0, p1}, Laqc;-><init>(Ljava/lang/Object;Laop;LaqC;Laop;)V

    invoke-virtual {p0, v1}, LaoX;->a(LaoY;)LaoY;

    .line 136
    return-object p0
.end method

.method public b(Ljava/lang/Class;)LaoX;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LatG",
            "<+TT;>;>;)",
            "LaoX",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 120
    invoke-static {p1}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-virtual {p0, v0}, LaoX;->b(Laop;)LaoX;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BindingBuilder<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LaoX;->a()LaoY;

    move-result-object v1

    invoke-virtual {v1}, LaoY;->a()Laop;

    move-result-object v1

    invoke-virtual {v1}, Laop;->a()LaoL;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
