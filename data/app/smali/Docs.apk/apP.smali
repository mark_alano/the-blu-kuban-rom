.class LapP;
.super LaoY;
.source "InjectorImpl.java"

# interfaces
.implements Larc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LaoY",
        "<TT;>;",
        "Larc",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final a:Laof;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laof",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<TT;>;"
        }
    .end annotation
.end field

.field final a:LarP;

.field final a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(LapL;Laop;Ljava/lang/Object;Laof;LarP;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LapL;",
            "Laop",
            "<TT;>;TT;",
            "Laof",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LarP;",
            ")V"
        }
    .end annotation

    .prologue
    .line 470
    invoke-interface {p4}, Laof;->a()Ljava/lang/Object;

    move-result-object v3

    new-instance v4, Lapj;

    invoke-static {p3}, LapH;->a(Ljava/lang/Object;)LapG;

    move-result-object v0

    invoke-direct {v4, v0}, Lapj;-><init>(LapG;)V

    sget-object v5, LaqC;->a:LaqC;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LaoY;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;)V

    .line 472
    iput-object p3, p0, LapP;->a:Ljava/lang/Object;

    .line 473
    invoke-static {p3}, LarT;->a(Ljava/lang/Object;)Laoz;

    move-result-object v0

    iput-object v0, p0, LapP;->a:Laoz;

    .line 474
    iput-object p4, p0, LapP;->a:Laof;

    .line 475
    iput-object p5, p0, LapP;->a:LarP;

    .line 476
    return-void
.end method


# virtual methods
.method public a()Laoz;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 479
    iget-object v0, p0, LapP;->a:Laoz;

    return-object v0
.end method

.method public b()Laop;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Laop",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 495
    iget-object v0, p0, LapP;->a:Laof;

    invoke-interface {v0}, Laof;->a()Laop;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Larg",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 499
    invoke-virtual {p0}, LapP;->b()Laop;

    move-result-object v0

    invoke-static {v0}, Larg;->a(Laop;)Larg;

    move-result-object v0

    invoke-static {v0}, Lajm;->a(Ljava/lang/Object;)Lajm;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 516
    instance-of v1, p1, LapP;

    if-eqz v1, :cond_2e

    .line 517
    check-cast p1, LapP;

    .line 518
    invoke-virtual {p0}, LapP;->a()Laop;

    move-result-object v1

    invoke-virtual {p1}, LapP;->a()Laop;

    move-result-object v2

    invoke-virtual {v1, v2}, Laop;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    invoke-virtual {p0}, LapP;->a()LaqC;

    move-result-object v1

    invoke-virtual {p1}, LapP;->a()LaqC;

    move-result-object v2

    invoke-virtual {v1, v2}, LaqC;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    iget-object v1, p0, LapP;->a:Ljava/lang/Object;

    iget-object v2, p1, LapP;->a:Ljava/lang/Object;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    const/4 v0, 0x1

    .line 522
    :cond_2e
    return v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 528
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, LapP;->a()Laop;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, LapP;->a()LaqC;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LapP;->a:Ljava/lang/Object;

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 507
    const-class v0, Larc;

    invoke-static {v0}, Lagp;->a(Ljava/lang/Class;)Lagr;

    move-result-object v0

    const-string v1, "key"

    invoke-virtual {p0}, LapP;->a()Laop;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "sourceKey"

    invoke-virtual {p0}, LapP;->b()Laop;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "value"

    iget-object v2, p0, LapP;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    invoke-virtual {v0}, Lagr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
