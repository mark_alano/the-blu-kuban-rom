.class final Lapm;
.super LaoY;
.source "ConstructorBindingImpl.java"

# interfaces
.implements Laps;
.implements Larb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LaoY",
        "<TT;>;",
        "Laps;",
        "Larb",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lapn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lapn",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Larp;


# direct methods
.method private constructor <init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Lapn;Larp;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LapL;",
            "Laop",
            "<TT;>;",
            "Ljava/lang/Object;",
            "LapY",
            "<+TT;>;",
            "LaqC;",
            "Lapn",
            "<TT;>;",
            "Larp;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct/range {p0 .. p5}, LaoY;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;)V

    .line 53
    iput-object p6, p0, Lapm;->a:Lapn;

    .line 54
    iput-object p7, p0, Lapm;->a:Larp;

    .line 55
    return-void
.end method

.method static a(LapL;Laop;Larp;Ljava/lang/Object;LaqC;Lapu;ZZ)Lapm;
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LapL;",
            "Laop",
            "<TT;>;",
            "Larp;",
            "Ljava/lang/Object;",
            "LaqC;",
            "Lapu;",
            "ZZ)",
            "Lapm",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-virtual {p5}, Lapu;->a()I

    move-result v2

    .line 80
    if-nez p2, :cond_79

    invoke-virtual {p1}, Laop;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v0

    move-object v1, v0

    .line 85
    :goto_f
    invoke-virtual {v1}, Ljava/lang/Class;->getModifiers()I

    move-result v0

    invoke-static {v0}, Ljava/lang/reflect/Modifier;->isAbstract(I)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 86
    invoke-virtual {p5, p1}, Lapu;->a(Laop;)Lapu;

    .line 90
    :cond_1c
    invoke-static {v1}, LaqR;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 91
    invoke-virtual {p5, v1}, Lapu;->e(Ljava/lang/Class;)Lapu;

    .line 94
    :cond_25
    invoke-virtual {p5, v2}, Lapu;->a(I)V

    .line 97
    if-nez p2, :cond_93

    .line 99
    :try_start_2a
    invoke-virtual {p1}, Laop;->a()LaoL;

    move-result-object v0

    invoke-static {v0}, Larp;->a(LaoL;)Larp;

    move-result-object v7

    .line 100
    if-eqz p7, :cond_43

    invoke-virtual {v7}, Larp;->a()Ljava/lang/reflect/Member;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/Constructor;

    invoke-static {v0}, Lapm;->a(Ljava/lang/reflect/Constructor;)Z

    move-result v0

    if-nez v0, :cond_43

    .line 101
    invoke-virtual {p5, v1}, Lapu;->a(Ljava/lang/Class;)Lapu;
    :try_end_43
    .catch Laoh; {:try_start_2a .. :try_end_43} :catch_83

    .line 109
    :cond_43
    :goto_43
    invoke-virtual {p4}, LaqC;->a()Z

    move-result v0

    if-nez v0, :cond_91

    .line 110
    invoke-virtual {v7}, Larp;->a()Ljava/lang/reflect/Member;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/reflect/Member;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    .line 111
    invoke-static {p5, v0}, LaoU;->a(Lapu;Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_91

    .line 113
    invoke-static {v0}, LaqC;->a(Ljava/lang/Class;)LaqC;

    move-result-object v0

    invoke-virtual {p5, v1}, Lapu;->a(Ljava/lang/Object;)Lapu;

    move-result-object v1

    invoke-static {v0, p0, v1}, LaqC;->a(LaqC;LapL;Lapu;)LaqC;

    move-result-object p4

    move-object v5, p4

    .line 118
    :goto_64
    invoke-virtual {p5, v2}, Lapu;->a(I)V

    .line 120
    new-instance v6, Lapn;

    invoke-direct {v6, p6, p1}, Lapn;-><init>(ZLaop;)V

    .line 121
    invoke-static {p1, p0, v6, p3, v5}, LaqC;->a(Laop;LapL;LapY;Ljava/lang/Object;LaqC;)LapY;

    move-result-object v4

    .line 124
    new-instance v0, Lapm;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lapm;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Lapn;Larp;)V

    return-object v0

    .line 80
    :cond_79
    invoke-virtual {p2}, Larp;->a()LaoL;

    move-result-object v0

    invoke-virtual {v0}, LaoL;->a()Ljava/lang/Class;

    move-result-object v0

    move-object v1, v0

    goto :goto_f

    .line 103
    :catch_83
    move-exception v0

    .line 104
    invoke-virtual {v0}, Laoh;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p5, v0}, Lapu;->a(Ljava/util/Collection;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    :cond_91
    move-object v5, p4

    goto :goto_64

    :cond_93
    move-object v7, p2

    goto :goto_43
.end method

.method private static a(Ljava/lang/reflect/Constructor;)Z
    .registers 2
    .parameter

    .prologue
    .line 130
    const-class v0, Laon;

    invoke-virtual {p0, v0}, Ljava/lang/reflect/Constructor;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_10

    const-class v0, LatE;

    invoke-virtual {p0, v0}, Ljava/lang/reflect/Constructor;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method


# virtual methods
.method protected a(Laop;)LaoY;
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laop",
            "<TT;>;)",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 210
    new-instance v0, Lapm;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lapm;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lapm;->a:Lapn;

    invoke-virtual {p0}, Lapm;->a()LaqC;

    move-result-object v5

    iget-object v6, p0, Lapm;->a:Lapn;

    iget-object v7, p0, Lapm;->a:Larp;

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lapm;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Lapn;Larp;)V

    return-object v0
.end method

.method protected a(LaqC;)LaoY;
    .registers 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LaqC;",
            ")",
            "LaoY",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 205
    new-instance v0, Lapm;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lapm;->a()Laop;

    move-result-object v2

    invoke-virtual {p0}, Lapm;->a()Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lapm;->a:Lapn;

    iget-object v6, p0, Lapm;->a:Lapn;

    iget-object v7, p0, Lapm;->a:Larp;

    move-object v5, p1

    invoke-direct/range {v0 .. v7}, Lapm;-><init>(LapL;Laop;Ljava/lang/Object;LapY;LaqC;Lapn;Larp;)V

    return-object v0
.end method

.method a()Larp;
    .registers 2

    .prologue
    .line 150
    iget-object v0, p0, Lapm;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapo;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 151
    iget-object v0, p0, Lapm;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapo;

    move-result-object v0

    invoke-virtual {v0}, Lapo;->a()Lapl;

    move-result-object v0

    invoke-interface {v0}, Lapl;->a()Larp;

    move-result-object v0

    .line 153
    :goto_16
    return-object v0

    :cond_17
    iget-object v0, p0, Lapm;->a:Larp;

    goto :goto_16
.end method

.method a()Ljava/util/Set;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Larg",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 159
    invoke-static {}, Lajm;->a()Lajo;

    move-result-object v0

    .line 160
    iget-object v1, p0, Lapm;->a:Lapn;

    invoke-static {v1}, Lapn;->a(Lapn;)Lapo;

    move-result-object v1

    if-nez v1, :cond_27

    .line 161
    iget-object v1, p0, Lapm;->a:Larp;

    invoke-virtual {v0, v1}, Lajo;->a(Ljava/lang/Object;)Lajo;

    .line 165
    :try_start_11
    iget-object v1, p0, Lapm;->a:Larp;

    invoke-virtual {v1}, Larp;->a()LaoL;

    move-result-object v1

    invoke-static {v1}, Larp;->a(LaoL;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajo;->a(Ljava/lang/Iterable;)Lajo;
    :try_end_1e
    .catch Laoh; {:try_start_11 .. :try_end_1e} :catch_37

    .line 172
    :goto_1e
    invoke-virtual {v0}, Lajo;->a()Lajm;

    move-result-object v0

    invoke-static {v0}, Larg;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0

    .line 168
    :cond_27
    invoke-virtual {p0}, Lapm;->b()Larp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajo;->a(Ljava/lang/Object;)Lajo;

    move-result-object v1

    invoke-virtual {p0}, Lapm;->b()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Lajo;->a(Ljava/lang/Iterable;)Lajo;

    goto :goto_1e

    .line 166
    :catch_37
    move-exception v1

    goto :goto_1e
.end method

.method public a(LapL;Lapu;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 136
    iget-object v1, p0, Lapm;->a:Lapn;

    iget-object v0, p1, LapL;->a:LapQ;

    iget-boolean v0, v0, LapQ;->b:Z

    if-nez v0, :cond_25

    const/4 v0, 0x1

    :goto_9
    invoke-static {v1, v0}, Lapn;->a(Lapn;Z)Z

    .line 137
    iget-object v0, p0, Lapm;->a:Lapn;

    iget-object v1, p1, LapL;->a:Lapq;

    iget-object v2, p0, Lapm;->a:Larp;

    invoke-virtual {v1, v2, p2}, Lapq;->a(Larp;Lapu;)Lapo;

    move-result-object v1

    invoke-static {v0, v1}, Lapn;->a(Lapn;Lapo;)Lapo;

    .line 139
    iget-object v0, p0, Lapm;->a:Lapn;

    iget-object v1, p1, LapL;->a:Laqx;

    invoke-virtual {v1, p0}, Laqx;->a(Laof;)Laqz;

    move-result-object v1

    invoke-static {v0, v1}, Lapn;->a(Lapn;Laqz;)Laqz;

    .line 141
    return-void

    .line 136
    :cond_25
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public b()Larp;
    .registers 3

    .prologue
    .line 181
    iget-object v0, p0, Lapm;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapo;

    move-result-object v0

    if-eqz v0, :cond_1d

    const/4 v0, 0x1

    :goto_9
    const-string v1, "Binding is not ready"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 182
    iget-object v0, p0, Lapm;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapo;

    move-result-object v0

    invoke-virtual {v0}, Lapo;->a()Lapl;

    move-result-object v0

    invoke-interface {v0}, Lapl;->a()Larp;

    move-result-object v0

    return-object v0

    .line 181
    :cond_1d
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public b()Ljava/util/Set;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Larp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lapm;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapo;

    move-result-object v0

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    :goto_9
    const-string v1, "Binding is not ready"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 187
    iget-object v0, p0, Lapm;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapo;

    move-result-object v0

    invoke-virtual {v0}, Lapo;->a()Lajm;

    move-result-object v0

    return-object v0

    .line 186
    :cond_19
    const/4 v0, 0x0

    goto :goto_9
.end method

.method b()Z
    .registers 2

    .prologue
    .line 145
    iget-object v0, p0, Lapm;->a:Lapn;

    invoke-static {v0}, Lapn;->a(Lapn;)Lapo;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public c()Ljava/util/Set;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Larg",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 198
    new-instance v0, Lajo;

    invoke-direct {v0}, Lajo;-><init>()V

    invoke-virtual {p0}, Lapm;->b()Larp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajo;->a(Ljava/lang/Object;)Lajo;

    move-result-object v0

    invoke-virtual {p0}, Lapm;->b()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lajo;->a(Ljava/lang/Iterable;)Lajo;

    move-result-object v0

    invoke-virtual {v0}, Lajo;->a()Lajm;

    move-result-object v0

    invoke-static {v0}, Larg;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 231
    instance-of v1, p1, Lapm;

    if-eqz v1, :cond_2e

    .line 232
    check-cast p1, Lapm;

    .line 233
    invoke-virtual {p0}, Lapm;->a()Laop;

    move-result-object v1

    invoke-virtual {p1}, Lapm;->a()Laop;

    move-result-object v2

    invoke-virtual {v1, v2}, Laop;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    invoke-virtual {p0}, Lapm;->a()LaqC;

    move-result-object v1

    invoke-virtual {p1}, Lapm;->a()LaqC;

    move-result-object v2

    invoke-virtual {v1, v2}, LaqC;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    iget-object v1, p0, Lapm;->a:Larp;

    iget-object v2, p1, Lapm;->a:Larp;

    invoke-static {v1, v2}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    const/4 v0, 0x1

    .line 237
    :cond_2e
    return v0
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 243
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lapm;->a()Laop;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lapm;->a()LaqC;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lapm;->a:Larp;

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 222
    const-class v0, Larb;

    invoke-static {v0}, Lagp;->a(Ljava/lang/Class;)Lagr;

    move-result-object v0

    const-string v1, "key"

    invoke-virtual {p0}, Lapm;->a()Laop;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {p0}, Lapm;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    const-string v1, "scope"

    invoke-virtual {p0}, Lapm;->a()LaqC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lagr;->a(Ljava/lang/String;Ljava/lang/Object;)Lagr;

    move-result-object v0

    invoke-virtual {v0}, Lagr;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
