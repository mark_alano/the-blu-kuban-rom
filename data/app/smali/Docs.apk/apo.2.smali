.class final Lapo;
.super Ljava/lang/Object;
.source "ConstructorInjector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lajm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lajm",
            "<",
            "Larp;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lapl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lapl",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Laqd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laqd",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:[LaqK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LaqK",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static synthetic a(Lapo;Lapu;LapX;Lapk;)Ljava/lang/Object;
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lapo;->a(Lapu;LapX;Lapk;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private a(Lapu;LapX;Lapk;)Ljava/lang/Object;
    .registers 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapu;",
            "LapX;",
            "Lapk",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 108
    :try_start_0
    iget-object v0, p0, Lapo;->a:[LaqK;

    invoke-static {p1, p2, v0}, LaqK;->a(Lapu;LapX;[LaqK;)[Ljava/lang/Object;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lapo;->a:Lapl;

    invoke-interface {v1, v0}, Lapl;->a([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 110
    invoke-virtual {p3, v0}, Lapk;->b(Ljava/lang/Object;)V
    :try_end_f
    .catchall {:try_start_0 .. :try_end_f} :catchall_21

    .line 112
    :try_start_f
    invoke-virtual {p3}, Lapk;->c()V

    .line 116
    invoke-virtual {p3, v0}, Lapk;->a(Ljava/lang/Object;)V

    .line 118
    iget-object v1, p0, Lapo;->a:Laqd;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, p2, v2}, Laqd;->a(Ljava/lang/Object;Lapu;LapX;Z)V

    .line 119
    iget-object v1, p0, Lapo;->a:Laqd;

    invoke-virtual {v1, v0, p1}, Laqd;->a(Ljava/lang/Object;Lapu;)V

    .line 121
    return-object v0

    .line 112
    :catchall_21
    move-exception v0

    invoke-virtual {p3}, Lapk;->c()V

    throw v0
    :try_end_26
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_f .. :try_end_26} :catch_26

    .line 122
    :catch_26
    move-exception v0

    .line 123
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_31

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 126
    :cond_31
    iget-object v1, p0, Lapo;->a:Lapl;

    invoke-interface {v1}, Lapl;->a()Larp;

    move-result-object v1

    invoke-virtual {p1, v1}, Lapu;->a(Ljava/lang/Object;)Lapu;

    move-result-object v1

    invoke-virtual {v1, v0}, Lapu;->a(Ljava/lang/Throwable;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a()Lajm;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lajm",
            "<",
            "Larp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lapo;->a:Lajm;

    return-object v0
.end method

.method a()Lapl;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lapl",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v0, p0, Lapo;->a:Lapl;

    return-object v0
.end method

.method a(Lapu;LapX;Ljava/lang/Class;ZLaqz;)Ljava/lang/Object;
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapu;",
            "LapX;",
            "Ljava/lang/Class",
            "<*>;Z",
            "Laqz",
            "<TT;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p2, p0}, LapX;->a(Ljava/lang/Object;)Lapk;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Lapk;->a()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 69
    if-nez p4, :cond_15

    .line 70
    invoke-virtual {p1, p3}, Lapu;->g(Ljava/lang/Class;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 73
    :cond_15
    invoke-virtual {v1, p1, p3}, Lapk;->a(Lapu;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 98
    :cond_19
    :goto_19
    return-object v0

    .line 79
    :cond_1a
    invoke-virtual {v1}, Lapk;->a()Ljava/lang/Object;

    move-result-object v0

    .line 80
    if-nez v0, :cond_19

    .line 84
    invoke-virtual {v1}, Lapk;->b()V

    .line 87
    :try_start_23
    invoke-virtual {p5}, Laqz;->a()Z

    move-result v0

    if-nez v0, :cond_34

    .line 88
    invoke-direct {p0, p1, p2, v1}, Lapo;->a(Lapu;LapX;Lapk;)Ljava/lang/Object;
    :try_end_2c
    .catchall {:try_start_23 .. :try_end_2c} :catchall_44

    move-result-object v0

    .line 97
    invoke-virtual {v1}, Lapk;->a()V

    .line 98
    invoke-virtual {v1}, Lapk;->c()V

    goto :goto_19

    .line 90
    :cond_34
    :try_start_34
    new-instance v0, Lapp;

    invoke-direct {v0, p0, p1, p2, v1}, Lapp;-><init>(Lapo;Lapu;LapX;Lapk;)V

    invoke-virtual {p5, p1, p2, v0}, Laqz;->a(Lapu;LapX;LaqB;)Ljava/lang/Object;
    :try_end_3c
    .catchall {:try_start_34 .. :try_end_3c} :catchall_44

    move-result-object v0

    .line 97
    invoke-virtual {v1}, Lapk;->a()V

    .line 98
    invoke-virtual {v1}, Lapk;->c()V

    goto :goto_19

    .line 97
    :catchall_44
    move-exception v0

    invoke-virtual {v1}, Lapk;->a()V

    .line 98
    invoke-virtual {v1}, Lapk;->c()V

    throw v0
.end method
