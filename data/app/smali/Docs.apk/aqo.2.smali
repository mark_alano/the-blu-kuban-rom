.class Laqo;
.super Laqq;
.source "ProvidedByInternalFactory.java"

# interfaces
.implements Laps;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Laqq",
        "<TT;>;",
        "Laps;"
    }
.end annotation


# instance fields
.field private a:LaoY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LaoY",
            "<+",
            "Laoz",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final a:Laop;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laop",
            "<+",
            "Laoz",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private a:Laqz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laqz",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Laoz",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Class;Ljava/lang/Class;Laop;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Class",
            "<+",
            "Laoz",
            "<*>;>;",
            "Laop",
            "<+",
            "Laoz",
            "<TT;>;>;Z)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p3, p4}, Laqq;-><init>(Ljava/lang/Object;Z)V

    .line 48
    iput-object p1, p0, Laqo;->a:Ljava/lang/Class;

    .line 49
    iput-object p2, p0, Laqo;->b:Ljava/lang/Class;

    .line 50
    iput-object p3, p0, Laqo;->a:Laop;

    .line 51
    return-void
.end method


# virtual methods
.method public a(Lapu;LapX;Larg;Z)Ljava/lang/Object;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lapu;",
            "LapX;",
            "Larg;",
            "Z)TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 65
    iget-object v1, p0, Laqo;->a:LaoY;

    if-eqz v1, :cond_36

    :goto_5
    const-string v1, "not initialized"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 67
    iget-object v0, p0, Laqo;->a:Laop;

    iget-object v1, p0, Laqo;->a:LaoY;

    invoke-virtual {v1}, LaoY;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, LapX;->a(Laop;Ljava/lang/Object;)V

    .line 69
    :try_start_15
    iget-object v0, p0, Laqo;->a:Laop;

    invoke-virtual {p1, v0}, Lapu;->a(Ljava/lang/Object;)Lapu;

    move-result-object v2

    .line 70
    iget-object v0, p0, Laqo;->a:LaoY;

    invoke-virtual {v0}, LaoY;->a()LapY;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v2, p2, p3, v1}, LapY;->a(Lapu;LapX;Larg;Z)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laoz;

    .line 72
    iget-object v6, p0, Laqo;->a:Laqz;

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v6}, Laqo;->a(LatG;Lapu;LapX;Larg;ZLaqz;)Ljava/lang/Object;
    :try_end_31
    .catchall {:try_start_15 .. :try_end_31} :catchall_38

    move-result-object v0

    .line 74
    invoke-virtual {p2}, LapX;->a()V

    return-object v0

    .line 65
    :cond_36
    const/4 v0, 0x0

    goto :goto_5

    .line 74
    :catchall_38
    move-exception v0

    invoke-virtual {p2}, LapX;->a()V

    throw v0
.end method

.method protected a(LatG;Lapu;Larg;Lapk;)Ljava/lang/Object;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LatG",
            "<+TT;>;",
            "Lapu;",
            "Larg",
            "<*>;",
            "Lapk",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 83
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Laqq;->a(LatG;Lapu;Larg;Lapk;)Ljava/lang/Object;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_25

    iget-object v1, p0, Laqo;->a:Ljava/lang/Class;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_25

    .line 85
    iget-object v0, p0, Laqo;->b:Ljava/lang/Class;

    iget-object v1, p0, Laqo;->a:Ljava/lang/Class;

    invoke-virtual {p2, v0, v1}, Lapu;->a(Ljava/lang/Class;Ljava/lang/Class;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0
    :try_end_1b
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_1b} :catch_1b

    .line 90
    :catch_1b
    move-exception v0

    .line 91
    invoke-virtual {p2, v0}, Lapu;->a(Ljava/lang/RuntimeException;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->a()LapA;

    move-result-object v0

    throw v0

    .line 89
    :cond_25
    return-object v0
.end method

.method public a(LapL;Lapu;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Laqo;->a:Laop;

    sget-object v1, LapR;->c:LapR;

    invoke-virtual {p1, v0, p2, v1}, LapL;->a(Laop;Lapu;LapR;)LaoY;

    move-result-object v0

    iput-object v0, p0, Laqo;->a:LaoY;

    .line 60
    return-void
.end method

.method a(Laqz;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laqz",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 54
    iput-object p1, p0, Laqo;->a:Laqz;

    .line 55
    return-void
.end method
