.class final Laqv;
.super Ljava/lang/Object;
.source "ProviderToInternalFactoryAdapter.java"

# interfaces
.implements Laoz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Laoz",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LapL;

.field private final a:LapY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LapY",
            "<+TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LapL;LapY;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LapL;",
            "LapY",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Laqv;->a:LapL;

    .line 34
    iput-object p2, p0, Laqv;->a:LapY;

    .line 35
    return-void
.end method

.method static synthetic a(Laqv;)LapY;
    .registers 2
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Laqv;->a:LapY;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v1, Lapu;

    invoke-direct {v1}, Lapu;-><init>()V

    .line 40
    :try_start_5
    iget-object v0, p0, Laqv;->a:LapL;

    new-instance v2, Laqw;

    invoke-direct {v2, p0, v1}, Laqw;-><init>(Laqv;Lapu;)V

    invoke-virtual {v0, v2}, LapL;->a(Lapr;)Ljava/lang/Object;

    move-result-object v0

    .line 49
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lapu;->a(I)V
    :try_end_14
    .catch LapA; {:try_start_5 .. :try_end_14} :catch_15

    .line 50
    return-object v0

    .line 51
    :catch_15
    move-exception v0

    .line 52
    new-instance v2, LaoB;

    invoke-virtual {v0}, LapA;->a()Lapu;

    move-result-object v0

    invoke-virtual {v1, v0}, Lapu;->a(Lapu;)Lapu;

    move-result-object v0

    invoke-virtual {v0}, Lapu;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, LaoB;-><init>(Ljava/lang/Iterable;)V

    throw v2
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Laqv;->a:LapY;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
