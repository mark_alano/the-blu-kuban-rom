.class public LasA;
.super Ljava/lang/Object;
.source "UserFeedbackSerializer.java"


# instance fields
.field private a:Lasv;


# direct methods
.method constructor <init>(Lasv;)V
    .registers 2
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, LasA;->a:Lasv;

    .line 48
    return-void
.end method

.method private b()Lats;
    .registers 4

    .prologue
    .line 55
    new-instance v0, Lats;

    sget-object v1, Ladt;->k:Latv;

    invoke-direct {v0, v1}, Lats;-><init>(Latv;)V

    .line 56
    const/4 v1, 0x1

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->device:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 57
    const/4 v1, 0x2

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->buildId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 58
    const/4 v1, 0x3

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->buildType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 59
    const/4 v1, 0x4

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->model:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 60
    const/4 v1, 0x5

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->product:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 61
    const/4 v1, 0x7

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->release:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 62
    const/16 v1, 0x8

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->incremental:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 63
    const/16 v1, 0x9

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->codename:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 64
    const/16 v1, 0xa

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->board:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 65
    const/16 v1, 0xb

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->brand:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 66
    const/4 v1, 0x6

    iget-object v2, p0, LasA;->a:Lasv;

    iget v2, v2, Lasv;->sdkInt:I

    invoke-virtual {v0, v1, v2}, Lats;->a(II)V

    .line 67
    return-object v0
.end method

.method private c()Lats;
    .registers 4

    .prologue
    .line 71
    new-instance v0, Lats;

    sget-object v1, Ladt;->d:Latv;

    invoke-direct {v0, v1}, Lats;-><init>(Latv;)V

    .line 72
    const/4 v1, 0x2

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->description:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 73
    const/4 v1, 0x6

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->uiLanguage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 74
    const-string v1, ""

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->chosenAccount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2b

    .line 75
    const/4 v1, 0x3

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->chosenAccount:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 77
    :cond_2b
    return-object v0
.end method

.method private d()Lats;
    .registers 5

    .prologue
    .line 81
    new-instance v1, Lats;

    sget-object v0, Ladt;->i:Latv;

    invoke-direct {v1, v0}, Lats;-><init>(Latv;)V

    .line 82
    invoke-static {}, Lasi;->a()Lasi;

    move-result-object v0

    invoke-virtual {v0}, Lasi;->b()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 83
    const/4 v0, 0x2

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->systemLog:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lats;->b(ILjava/lang/String;)V

    .line 85
    :cond_19
    const/4 v0, 0x1

    iget-object v2, p0, LasA;->a:Lasv;

    iget-wide v2, v2, Lasv;->timestamp:J

    invoke-virtual {v1, v0, v2, v3}, Lats;->a(IJ)V

    .line 86
    const/4 v0, 0x6

    invoke-direct {p0}, LasA;->e()Lats;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lats;->b(ILats;)V

    .line 87
    iget-object v0, p0, LasA;->a:Lasv;

    iget-object v0, v0, Lasv;->runningApplications:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_31
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_42

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 88
    const/4 v3, 0x5

    invoke-virtual {v1, v3, v0}, Lats;->a(ILjava/lang/String;)V

    goto :goto_31

    .line 90
    :cond_42
    return-object v1
.end method

.method private e()Lats;
    .registers 4

    .prologue
    .line 94
    new-instance v0, Lats;

    sget-object v1, Ladt;->q:Latv;

    invoke-direct {v0, v1}, Lats;-><init>(Latv;)V

    .line 95
    const/4 v1, 0x1

    iget-object v2, p0, LasA;->a:Lasv;

    iget v2, v2, Lasv;->phoneType:I

    invoke-virtual {v0, v1, v2}, Lats;->a(II)V

    .line 96
    const/4 v1, 0x3

    iget-object v2, p0, LasA;->a:Lasv;

    iget v2, v2, Lasv;->networkType:I

    invoke-virtual {v0, v1, v2}, Lats;->a(II)V

    .line 97
    const/4 v1, 0x2

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->networkName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 98
    return-object v0
.end method

.method private f()Lats;
    .registers 4

    .prologue
    .line 102
    new-instance v0, Lats;

    sget-object v1, Ladt;->j:Latv;

    invoke-direct {v0, v1}, Lats;-><init>(Latv;)V

    .line 103
    const/4 v1, 0x1

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 104
    const/4 v1, 0x2

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->installerPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 105
    const/4 v1, 0x3

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 106
    const/4 v1, 0x4

    iget-object v2, p0, LasA;->a:Lasv;

    iget v2, v2, Lasv;->packageVersion:I

    invoke-virtual {v0, v1, v2}, Lats;->a(II)V

    .line 107
    const/4 v1, 0x5

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->packageVersionName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 108
    const/4 v1, 0x6

    iget-object v2, p0, LasA;->a:Lasv;

    iget-boolean v2, v2, Lasv;->isSystemApp:Z

    invoke-virtual {v0, v1, v2}, Lats;->a(IZ)V

    .line 109
    return-object v0
.end method

.method private g()Lats;
    .registers 9

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 113
    new-instance v1, Lats;

    sget-object v0, Ladt;->p:Latv;

    invoke-direct {v1, v0}, Lats;-><init>(Latv;)V

    .line 115
    invoke-static {}, Lasi;->a()Lasi;

    move-result-object v0

    invoke-virtual {v0}, Lasi;->a()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-static {}, Lasi;->a()Lasi;

    move-result-object v0

    invoke-virtual {v0}, Lasi;->a()LasB;

    move-result-object v0

    invoke-virtual {v0}, LasB;->a()Z

    move-result v0

    if-eqz v0, :cond_2f

    iget-object v0, p0, LasA;->a:Lasv;

    iget-object v0, v0, Lasv;->screenshot:[B

    if-eqz v0, :cond_2f

    .line 118
    const/4 v0, 0x4

    invoke-direct {p0}, LasA;->h()Lats;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lats;->b(ILats;)V

    .line 121
    :cond_2f
    iget-object v0, p0, LasA;->a:Lasv;

    iget-object v0, v0, Lasv;->categoryTag:Ljava/lang/String;

    if-eqz v0, :cond_3d

    .line 122
    const/4 v0, 0x6

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->categoryTag:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lats;->b(ILjava/lang/String;)V

    .line 125
    :cond_3d
    iget-object v0, p0, LasA;->a:Lasv;

    iget-object v0, v0, Lasv;->bucket:Ljava/lang/String;

    if-eqz v0, :cond_4b

    .line 126
    const/4 v0, 0x7

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->bucket:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lats;->b(ILjava/lang/String;)V

    .line 132
    :cond_4b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LasA;->a:Lasv;

    iget v2, v2, Lasv;->numGoogleAccounts:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v6, v0}, Lats;->b(ILjava/lang/String;)V

    .line 135
    iget-object v0, p0, LasA;->a:Lasv;

    iget-object v0, v0, Lasv;->productSpecificBinaryData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6d
    :goto_6d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LarY;

    .line 136
    invoke-virtual {v0}, LarY;->a()[B

    move-result-object v3

    .line 138
    if-eqz v3, :cond_6d

    .line 139
    new-instance v4, Lats;

    sget-object v5, Ladt;->e:Latv;

    invoke-direct {v4, v5}, Lats;-><init>(Latv;)V

    .line 141
    invoke-virtual {v0}, LarY;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, Lats;->b(ILjava/lang/String;)V

    .line 142
    invoke-virtual {v0}, LarY;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v7, v0}, Lats;->b(ILjava/lang/String;)V

    .line 144
    const/4 v0, 0x3

    invoke-virtual {v4, v0, v3}, Lats;->a(I[B)V

    .line 145
    invoke-virtual {v1, v7, v4}, Lats;->a(ILats;)V

    goto :goto_6d

    .line 149
    :cond_9c
    return-object v1
.end method

.method private h()Lats;
    .registers 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 153
    new-instance v0, Lats;

    sget-object v1, Ladt;->f:Latv;

    invoke-direct {v0, v1}, Lats;-><init>(Latv;)V

    .line 154
    const-string v1, "image/jpeg"

    invoke-virtual {v0, v3, v1}, Lats;->b(ILjava/lang/String;)V

    .line 155
    iget-object v1, p0, LasA;->a:Lasv;

    iget-object v1, v1, Lasv;->screenshot:[B

    const/4 v2, 0x0

    invoke-static {v1, v2}, LasC;->a([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lats;->b(ILjava/lang/String;)V

    .line 157
    new-instance v1, Lats;

    sget-object v2, Ladt;->a:Latv;

    invoke-direct {v1, v2}, Lats;-><init>(Latv;)V

    .line 158
    iget-object v2, p0, LasA;->a:Lasv;

    iget v2, v2, Lasv;->screenshotHeight:I

    int-to-float v2, v2

    invoke-virtual {v1, v4, v2}, Lats;->a(IF)V

    .line 159
    iget-object v2, p0, LasA;->a:Lasv;

    iget v2, v2, Lasv;->screenshotWidth:I

    int-to-float v2, v2

    invoke-virtual {v1, v3, v2}, Lats;->a(IF)V

    .line 160
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v1}, Lats;->b(ILats;)V

    .line 161
    return-object v0
.end method

.method private i()Lats;
    .registers 4

    .prologue
    .line 165
    new-instance v0, Lats;

    sget-object v1, Ladt;->h:Latv;

    invoke-direct {v0, v1}, Lats;-><init>(Latv;)V

    .line 166
    const/4 v1, 0x1

    invoke-direct {p0}, LasA;->d()Lats;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lats;->b(ILats;)V

    .line 167
    const/4 v1, 0x2

    invoke-direct {p0}, LasA;->f()Lats;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lats;->b(ILats;)V

    .line 168
    const/4 v1, 0x3

    invoke-direct {p0}, LasA;->b()Lats;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lats;->b(ILats;)V

    .line 169
    const/16 v1, 0x9

    invoke-direct {p0}, LasA;->g()Lats;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lats;->b(ILats;)V

    .line 171
    iget-object v1, p0, LasA;->a:Lasv;

    iget-object v1, v1, Lasv;->crashData:Lasu;

    if-eqz v1, :cond_36

    .line 172
    const/4 v1, 0x4

    invoke-direct {p0}, LasA;->j()Lats;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lats;->b(ILats;)V

    .line 174
    :cond_36
    return-object v0
.end method

.method private j()Lats;
    .registers 4

    .prologue
    .line 178
    new-instance v0, Lats;

    sget-object v1, Ladt;->l:Latv;

    invoke-direct {v0, v1}, Lats;-><init>(Latv;)V

    .line 179
    const/4 v1, 0x1

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->crashData:Lasu;

    iget-object v2, v2, Lasu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 180
    const/4 v1, 0x3

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->crashData:Lasu;

    iget-object v2, v2, Lasu;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 181
    const/4 v1, 0x4

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->crashData:Lasu;

    iget v2, v2, Lasu;->a:I

    invoke-virtual {v0, v1, v2}, Lats;->a(II)V

    .line 182
    const/4 v1, 0x5

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->crashData:Lasu;

    iget-object v2, v2, Lasu;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 183
    const/4 v1, 0x6

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->crashData:Lasu;

    iget-object v2, v2, Lasu;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 184
    const/4 v1, 0x7

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->crashData:Lasu;

    iget-object v2, v2, Lasu;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 185
    iget-object v1, p0, LasA;->a:Lasv;

    iget-object v1, v1, Lasv;->crashData:Lasu;

    iget-object v1, v1, Lasu;->f:Ljava/lang/String;

    if-eqz v1, :cond_55

    .line 186
    const/4 v1, 0x2

    iget-object v2, p0, LasA;->a:Lasv;

    iget-object v2, v2, Lasv;->crashData:Lasu;

    iget-object v2, v2, Lasu;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lats;->b(ILjava/lang/String;)V

    .line 188
    :cond_55
    return-object v0
.end method

.method private k()Lats;
    .registers 4

    .prologue
    .line 192
    new-instance v0, Lats;

    sget-object v1, Ladt;->g:Latv;

    invoke-direct {v0, v1}, Lats;-><init>(Latv;)V

    .line 193
    const/4 v1, 0x1

    invoke-direct {p0}, LasA;->c()Lats;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lats;->b(ILats;)V

    .line 194
    const/4 v1, 0x2

    invoke-direct {p0}, LasA;->i()Lats;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lats;->b(ILats;)V

    .line 195
    return-object v0
.end method


# virtual methods
.method public a()Lats;
    .registers 2

    .prologue
    .line 51
    invoke-direct {p0}, LasA;->k()Lats;

    move-result-object v0

    return-object v0
.end method
