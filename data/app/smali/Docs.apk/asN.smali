.class public final LasN;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LasL;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LasL;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LasL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 32
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 33
    iput-object p1, p0, LasN;->a:LYD;

    .line 34
    const-class v0, LasL;

    const-string v1, "AclFeed"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LasN;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LasN;->a:LZb;

    .line 37
    const-class v0, LasL;

    const-string v1, "AccountMetadata"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LasN;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LasN;->b:LZb;

    .line 40
    const-class v0, LasL;

    const-string v1, "DocFeed"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LasN;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LasN;->c:LZb;

    .line 43
    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 50
    const-class v0, LasL;

    const-string v1, "AclFeed"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LasN;->a:LZb;

    invoke-virtual {p0, v0, v1}, LasN;->a(Laop;LZb;)V

    .line 51
    const-class v0, LasL;

    const-string v1, "AccountMetadata"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LasN;->b:LZb;

    invoke-virtual {p0, v0, v1}, LasN;->a(Laop;LZb;)V

    .line 52
    const-class v0, LasL;

    const-string v1, "DocFeed"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-static {v0, v1}, Laop;->a(Ljava/lang/Class;Ljava/lang/annotation/Annotation;)Laop;

    move-result-object v0

    iget-object v1, p0, LasN;->c:LZb;

    invoke-virtual {p0, v0, v1}, LasN;->a(Laop;LZb;)V

    .line 53
    iget-object v0, p0, LasN;->a:LZb;

    iget-object v1, p0, LasN;->a:LYD;

    iget-object v1, v1, LYD;->a:LWj;

    iget-object v1, v1, LWj;->c:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 55
    iget-object v0, p0, LasN;->b:LZb;

    iget-object v1, p0, LasN;->a:LYD;

    iget-object v1, v1, LYD;->a:LWj;

    iget-object v1, v1, LWj;->a:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 57
    iget-object v0, p0, LasN;->c:LZb;

    iget-object v1, p0, LasN;->a:LYD;

    iget-object v1, v1, LYD;->a:LWj;

    iget-object v1, v1, LWj;->b:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 59
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 63
    return-void
.end method
