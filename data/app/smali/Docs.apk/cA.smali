.class final enum LcA;
.super Ljava/lang/Enum;
.source "GAServiceProxy.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LcA;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LcA;

.field private static final synthetic a:[LcA;

.field public static final enum b:LcA;

.field public static final enum c:LcA;

.field public static final enum d:LcA;

.field public static final enum e:LcA;

.field public static final enum f:LcA;

.field public static final enum g:LcA;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, LcA;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, LcA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LcA;->a:LcA;

    .line 31
    new-instance v0, LcA;

    const-string v1, "CONNECTED_SERVICE"

    invoke-direct {v0, v1, v4}, LcA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LcA;->b:LcA;

    .line 32
    new-instance v0, LcA;

    const-string v1, "CONNECTED_LOCAL"

    invoke-direct {v0, v1, v5}, LcA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LcA;->c:LcA;

    .line 33
    new-instance v0, LcA;

    const-string v1, "BLOCKED"

    invoke-direct {v0, v1, v6}, LcA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LcA;->d:LcA;

    .line 34
    new-instance v0, LcA;

    const-string v1, "PENDING_CONNECTION"

    invoke-direct {v0, v1, v7}, LcA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LcA;->e:LcA;

    .line 35
    new-instance v0, LcA;

    const-string v1, "PENDING_DISCONNECT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LcA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LcA;->f:LcA;

    .line 36
    new-instance v0, LcA;

    const-string v1, "DISCONNECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LcA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LcA;->g:LcA;

    .line 29
    const/4 v0, 0x7

    new-array v0, v0, [LcA;

    sget-object v1, LcA;->a:LcA;

    aput-object v1, v0, v3

    sget-object v1, LcA;->b:LcA;

    aput-object v1, v0, v4

    sget-object v1, LcA;->c:LcA;

    aput-object v1, v0, v5

    sget-object v1, LcA;->d:LcA;

    aput-object v1, v0, v6

    sget-object v1, LcA;->e:LcA;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LcA;->f:LcA;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LcA;->g:LcA;

    aput-object v2, v0, v1

    sput-object v0, LcA;->a:[LcA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LcA;
    .registers 2
    .parameter

    .prologue
    .line 29
    const-class v0, LcA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LcA;

    return-object v0
.end method

.method public static values()[LcA;
    .registers 1

    .prologue
    .line 29
    sget-object v0, LcA;->a:[LcA;

    invoke-virtual {v0}, [LcA;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LcA;

    return-object v0
.end method
