.class LcB;
.super Ljava/util/TimerTask;
.source "GAServiceProxy.java"


# instance fields
.field final synthetic a:Lcw;


# direct methods
.method private constructor <init>(Lcw;)V
    .registers 2
    .parameter

    .prologue
    .line 360
    iput-object p1, p0, LcB;->a:Lcw;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcw;Lcx;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 360
    invoke-direct {p0, p1}, LcB;-><init>(Lcw;)V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    .prologue
    .line 363
    iget-object v0, p0, LcB;->a:Lcw;

    invoke-static {v0}, Lcw;->a(Lcw;)LcA;

    move-result-object v0

    sget-object v1, LcA;->b:LcA;

    if-ne v0, v1, :cond_3c

    iget-object v0, p0, LcB;->a:Lcw;

    invoke-static {v0}, Lcw;->a(Lcw;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3c

    iget-object v0, p0, LcB;->a:Lcw;

    invoke-static {v0}, Lcw;->a(Lcw;)J

    move-result-wide v0

    iget-object v2, p0, LcB;->a:Lcw;

    invoke-static {v2}, Lcw;->b(Lcw;)J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, LcB;->a:Lcw;

    invoke-static {v2}, Lcw;->a(Lcw;)Lck;

    move-result-object v2

    invoke-interface {v2}, Lck;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_3c

    .line 366
    const-string v0, "Disconnecting due to inactivity"

    invoke-static {v0}, LcT;->e(Ljava/lang/String;)I

    .line 367
    iget-object v0, p0, LcB;->a:Lcw;

    invoke-static {v0}, Lcw;->d(Lcw;)V

    .line 371
    :goto_3b
    return-void

    .line 369
    :cond_3c
    iget-object v0, p0, LcB;->a:Lcw;

    invoke-static {v0}, Lcw;->a(Lcw;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, LcB;

    iget-object v2, p0, LcB;->a:Lcw;

    invoke-direct {v1, v2}, LcB;-><init>(Lcw;)V

    iget-object v2, p0, LcB;->a:Lcw;

    invoke-static {v2}, Lcw;->b(Lcw;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_3b
.end method
