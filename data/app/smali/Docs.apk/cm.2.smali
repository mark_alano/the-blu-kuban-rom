.class public Lcm;
.super Ljava/lang/Object;
.source "EasyTracker.java"


# static fields
.field private static a:Lcm;


# instance fields
.field private a:I

.field private a:J

.field private a:Landroid/content/Context;

.field private a:LbZ;

.field private a:Lck;

.field private a:Lda;

.field private a:Ldf;

.field private a:Ldj;

.field private a:Ljava/lang/Double;

.field private a:Ljava/lang/String;

.field private a:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/Timer;

.field private a:Ljava/util/TimerTask;

.field private a:Z

.field private b:I

.field private b:J

.field private b:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z


# direct methods
.method private constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput-boolean v1, p0, Lcm;->a:Z

    .line 136
    const/16 v0, 0x708

    iput v0, p0, Lcm;->a:I

    .line 163
    iput-boolean v1, p0, Lcm;->e:Z

    .line 168
    iput v1, p0, Lcm;->b:I

    .line 181
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcm;->a:Ljava/util/Map;

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lcm;->a:Ldj;

    .line 204
    iput-boolean v1, p0, Lcm;->f:Z

    .line 83
    new-instance v0, Lcn;

    invoke-direct {v0, p0}, Lcn;-><init>(Lcm;)V

    iput-object v0, p0, Lcm;->a:Lck;

    .line 89
    return-void
.end method

.method public static a()Lcm;
    .registers 1

    .prologue
    .line 97
    sget-object v0, Lcm;->a:Lcm;

    if-nez v0, :cond_b

    .line 98
    new-instance v0, Lcm;

    invoke-direct {v0}, Lcm;-><init>()V

    sput-object v0, Lcm;->a:Lcm;

    .line 100
    :cond_b
    sget-object v0, Lcm;->a:Lcm;

    return-object v0
.end method

.method static synthetic a(Lcm;)Ldj;
    .registers 2
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcm;->a:Ldj;

    return-object v0
.end method

.method private a(Landroid/app/Activity;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 413
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    .line 414
    iget-object v0, p0, Lcm;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 415
    iget-object v0, p0, Lcm;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 422
    :goto_18
    return-object v0

    .line 417
    :cond_19
    iget-object v0, p0, Lcm;->a:Lda;

    invoke-interface {v0, v1}, Lda;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 418
    if-nez v0, :cond_22

    move-object v0, v1

    .line 421
    :cond_22
    iget-object v2, p0, Lcm;->a:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_18
.end method

.method private a()V
    .registers 6

    .prologue
    const/4 v0, 0x1

    .line 219
    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_trackingId"

    invoke-interface {v1, v2}, Lda;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcm;->a:Ljava/lang/String;

    .line 220
    iget-object v1, p0, Lcm;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 223
    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_api_key"

    invoke-interface {v1, v2}, Lda;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcm;->a:Ljava/lang/String;

    .line 224
    iget-object v1, p0, Lcm;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_32

    .line 225
    const-string v0, "EasyTracker requested, but missing required ga_trackingId"

    invoke-static {v0}, LcT;->c(Ljava/lang/String;)I

    .line 226
    new-instance v0, Lco;

    invoke-direct {v0, p0}, Lco;-><init>(Lcm;)V

    iput-object v0, p0, Lcm;->a:Ldj;

    .line 272
    :cond_31
    :goto_31
    return-void

    .line 230
    :cond_32
    iput-boolean v0, p0, Lcm;->a:Z

    .line 231
    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_appName"

    invoke-interface {v1, v2}, Lda;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcm;->b:Ljava/lang/String;

    .line 232
    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_appVersion"

    invoke-interface {v1, v2}, Lda;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcm;->c:Ljava/lang/String;

    .line 233
    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_debug"

    invoke-interface {v1, v2}, Lda;->a(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcm;->b:Z

    .line 235
    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_sampleFrequency"

    invoke-interface {v1, v2}, Lda;->a(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, p0, Lcm;->a:Ljava/lang/Double;

    .line 236
    iget-object v1, p0, Lcm;->a:Ljava/lang/Double;

    if-nez v1, :cond_72

    .line 237
    new-instance v1, Ljava/lang/Double;

    iget-object v2, p0, Lcm;->a:Lda;

    const-string v3, "ga_sampleRate"

    const/16 v4, 0x64

    invoke-interface {v2, v3, v4}, Lda;->a(Ljava/lang/String;I)I

    move-result v2

    int-to-double v2, v2

    invoke-direct {v1, v2, v3}, Ljava/lang/Double;-><init>(D)V

    iput-object v1, p0, Lcm;->a:Ljava/lang/Double;

    .line 239
    :cond_72
    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_dispatchPeriod"

    const/16 v3, 0x708

    invoke-interface {v1, v2, v3}, Lda;->a(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcm;->a:I

    .line 240
    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_sessionTimeout"

    const/16 v3, 0x1e

    invoke-interface {v1, v2, v3}, Lda;->a(Ljava/lang/String;I)I

    move-result v1

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v1, v1

    iput-wide v1, p0, Lcm;->a:J

    .line 241
    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_autoActivityTracking"

    invoke-interface {v1, v2}, Lda;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a1

    iget-object v1, p0, Lcm;->a:Lda;

    const-string v2, "ga_auto_activity_tracking"

    invoke-interface {v1, v2}, Lda;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12d

    :cond_a1
    :goto_a1
    iput-boolean v0, p0, Lcm;->e:Z

    .line 244
    iget-object v0, p0, Lcm;->a:Lda;

    const-string v1, "ga_anonymizeIp"

    invoke-interface {v0, v1}, Lda;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcm;->c:Z

    .line 245
    iget-object v0, p0, Lcm;->a:Lda;

    const-string v1, "ga_reportUncaughtExceptions"

    invoke-interface {v0, v1}, Lda;->a(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcm;->d:Z

    .line 246
    iget-object v0, p0, Lcm;->a:LbZ;

    iget-object v1, p0, Lcm;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LbZ;->a(Ljava/lang/String;)Ldj;

    move-result-object v0

    iput-object v0, p0, Lcm;->a:Ldj;

    .line 247
    iget-object v0, p0, Lcm;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e8

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setting appName to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcm;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LcT;->d(Ljava/lang/String;)I

    .line 249
    iget-object v0, p0, Lcm;->a:Ldj;

    iget-object v1, p0, Lcm;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ldj;->a(Ljava/lang/String;)V

    .line 255
    :cond_e8
    iget-object v0, p0, Lcm;->c:Ljava/lang/String;

    if-eqz v0, :cond_f3

    .line 256
    iget-object v0, p0, Lcm;->a:Ldj;

    iget-object v1, p0, Lcm;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ldj;->b(Ljava/lang/String;)V

    .line 258
    :cond_f3
    iget-object v0, p0, Lcm;->a:Ldj;

    iget-boolean v1, p0, Lcm;->c:Z

    invoke-interface {v0, v1}, Ldj;->b(Z)V

    .line 259
    iget-object v0, p0, Lcm;->a:Ldj;

    iget-object v1, p0, Lcm;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Ldj;->a(D)V

    .line 260
    iget-object v0, p0, Lcm;->a:LbZ;

    iget-boolean v1, p0, Lcm;->b:Z

    invoke-interface {v0, v1}, LbZ;->a(Z)V

    .line 261
    iget-object v0, p0, Lcm;->a:Ldf;

    iget v1, p0, Lcm;->a:I

    invoke-interface {v0, v1}, Ldf;->a(I)V

    .line 263
    iget-boolean v0, p0, Lcm;->d:Z

    if-eqz v0, :cond_31

    .line 264
    iget-object v0, p0, Lcm;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 265
    if-nez v0, :cond_128

    .line 266
    new-instance v0, Lcr;

    iget-object v1, p0, Lcm;->a:Ldj;

    iget-object v2, p0, Lcm;->a:Ldf;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcr;-><init>(Ldj;Ldf;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 270
    :cond_128
    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    goto/16 :goto_31

    .line 241
    :cond_12d
    const/4 v0, 0x0

    goto/16 :goto_a1
.end method

.method static synthetic a(Lcm;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 80
    iput-boolean p1, p0, Lcm;->f:Z

    return p1
.end method

.method private declared-synchronized b()V
    .registers 2

    .prologue
    .line 397
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcm;->a:Ljava/util/Timer;

    if-eqz v0, :cond_d

    .line 398
    iget-object v0, p0, Lcm;->a:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 399
    const/4 v0, 0x0

    iput-object v0, p0, Lcm;->a:Ljava/util/Timer;
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    .line 401
    :cond_d
    monitor-exit p0

    return-void

    .line 397
    :catchall_f
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 335
    invoke-virtual {p0, p1}, Lcm;->a(Landroid/content/Context;)V

    .line 336
    iget-boolean v0, p0, Lcm;->a:Z

    if-nez v0, :cond_9

    .line 356
    :cond_8
    :goto_8
    return-void

    .line 342
    :cond_9
    invoke-direct {p0}, Lcm;->b()V

    .line 344
    iget-boolean v0, p0, Lcm;->f:Z

    if-nez v0, :cond_2f

    iget v0, p0, Lcm;->b:I

    if-nez v0, :cond_2f

    invoke-virtual {p0}, Lcm;->a()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 345
    iget-object v0, p0, Lcm;->a:Ldj;

    invoke-interface {v0, v5}, Ldj;->a(Z)V

    .line 346
    iget-boolean v0, p0, Lcm;->e:Z

    if-nez v0, :cond_2f

    .line 348
    iget-object v0, p0, Lcm;->a:Ldj;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Ldj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 351
    :cond_2f
    iput-boolean v5, p0, Lcm;->f:Z

    .line 352
    iget v0, p0, Lcm;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcm;->b:I

    .line 353
    iget-boolean v0, p0, Lcm;->e:Z

    if-eqz v0, :cond_8

    .line 354
    iget-object v0, p0, Lcm;->a:Ldj;

    invoke-direct {p0, p1}, Lcm;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ldj;->c(Ljava/lang/String;)V

    goto :goto_8
.end method

.method public a(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    .line 292
    if-nez p1, :cond_8

    .line 293
    const-string v0, "Context cannot be null"

    invoke-static {v0}, LcT;->c(Ljava/lang/String;)I

    .line 301
    :goto_7
    return-void

    .line 295
    :cond_8
    invoke-static {}, Lct;->a()Lct;

    move-result-object v0

    .line 296
    new-instance v1, Ldb;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Ldb;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LcK;->a(Landroid/content/Context;)LcK;

    move-result-object v2

    invoke-virtual {p0, p1, v1, v2, v0}, Lcm;->a(Landroid/content/Context;Lda;LbZ;Ldf;)V

    goto :goto_7
.end method

.method a(Landroid/content/Context;Lda;LbZ;Ldf;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 311
    if-nez p1, :cond_7

    .line 312
    const-string v0, "Context cannot be null"

    invoke-static {v0}, LcT;->c(Ljava/lang/String;)I

    .line 314
    :cond_7
    iget-object v0, p0, Lcm;->a:Landroid/content/Context;

    if-nez v0, :cond_1a

    .line 315
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcm;->a:Landroid/content/Context;

    .line 316
    iput-object p3, p0, Lcm;->a:LbZ;

    .line 317
    iput-object p4, p0, Lcm;->a:Ldf;

    .line 318
    iput-object p2, p0, Lcm;->a:Lda;

    .line 319
    invoke-direct {p0}, Lcm;->a()V

    .line 321
    :cond_1a
    return-void
.end method

.method a()Z
    .registers 7

    .prologue
    const-wide/16 v2, 0x0

    .line 210
    iget-wide v0, p0, Lcm;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1d

    iget-wide v0, p0, Lcm;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1f

    iget-object v0, p0, Lcm;->a:Lck;

    invoke-interface {v0}, Lck;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcm;->b:J

    iget-wide v4, p0, Lcm;->a:J

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_1f

    :cond_1d
    const/4 v0, 0x1

    :goto_1e
    return v0

    :cond_1f
    const/4 v0, 0x0

    goto :goto_1e
.end method

.method public b(Landroid/app/Activity;)V
    .registers 6
    .parameter

    .prologue
    .line 366
    invoke-virtual {p0, p1}, Lcm;->a(Landroid/content/Context;)V

    .line 367
    iget-boolean v0, p0, Lcm;->a:Z

    if-nez v0, :cond_8

    .line 385
    :cond_7
    :goto_7
    return-void

    .line 370
    :cond_8
    iget v0, p0, Lcm;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcm;->b:I

    .line 373
    const/4 v0, 0x0

    iget v1, p0, Lcm;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcm;->b:I

    .line 375
    iget-object v0, p0, Lcm;->a:Lck;

    invoke-interface {v0}, Lck;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcm;->b:J

    .line 377
    iget v0, p0, Lcm;->b:I

    if-nez v0, :cond_7

    .line 378
    invoke-direct {p0}, Lcm;->b()V

    .line 381
    new-instance v0, Lcp;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcp;-><init>(Lcm;Lcn;)V

    iput-object v0, p0, Lcm;->a:Ljava/util/TimerTask;

    .line 382
    new-instance v0, Ljava/util/Timer;

    const-string v1, "waitForActivityStart"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcm;->a:Ljava/util/Timer;

    .line 383
    iget-object v0, p0, Lcm;->a:Ljava/util/Timer;

    iget-object v1, p0, Lcm;->a:Ljava/util/TimerTask;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_7
.end method
