.class public abstract Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;
.super Lcom/google/android/apps/docs/view/RoboDialogFragment;
.source "AbstractModalDialogActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter

    .prologue
    .line 34
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->a(Landroid/content/DialogInterface;)V

    .line 25
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_c

    .line 28
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 30
    :cond_c
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 31
    return-void
.end method
