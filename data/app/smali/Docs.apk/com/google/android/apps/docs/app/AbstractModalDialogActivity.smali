.class public abstract Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "AbstractModalDialogActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 21
    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    if-nez p1, :cond_16

    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;->setResult(I)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;->a()Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;

    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;->a()Lo;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 50
    :cond_16
    return-void
.end method
