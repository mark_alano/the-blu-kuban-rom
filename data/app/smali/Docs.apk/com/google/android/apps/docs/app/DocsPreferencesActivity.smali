.class public Lcom/google/android/apps/docs/app/DocsPreferencesActivity;
.super Lcom/google/android/apps/docs/RoboPreferenceActivity;
.source "DocsPreferencesActivity.java"


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LMM;

.field public a:LMO;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LNe;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LOS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LUL;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZj;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaJ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/preference/CheckBoxPreference;

.field private a:Landroid/preference/ListPreference;

.field private a:Landroid/preference/Preference;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lfg;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LnE;

.field public a:LnF;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LoL;

.field private a:Z

.field private b:Landroid/preference/CheckBoxPreference;

.field private b:Landroid/preference/ListPreference;

.field private b:Landroid/preference/Preference;

.field private b:LoL;

.field private b:Z

.field private c:Landroid/preference/CheckBoxPreference;

.field private c:LoL;

.field private d:LoL;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/docs/RoboPreferenceActivity;-><init>()V

    .line 80
    iput-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/CheckBoxPreference;

    .line 81
    iput-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    .line 82
    iput-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/Preference;

    .line 83
    iput-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/Preference;

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Z

    .line 85
    iput-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/CheckBoxPreference;

    .line 86
    iput-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->c:Landroid/preference/CheckBoxPreference;

    .line 87
    iput-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)Landroid/preference/CheckBoxPreference;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->c:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LoL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->c:LoL;

    return-object v0
.end method

.method private a()V
    .registers 3

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "shared_preferences.notify_newdoc"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/CheckBoxPreference;

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/CheckBoxPreference;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Lgl;

    sget-object v1, Lgi;->g:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    if-nez v0, :cond_39

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 174
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    .line 175
    const-string v0, "docs_preference_screen.notify"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    .line 177
    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    .line 183
    :cond_39
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LaaJ;

    invoke-interface {v1}, LaaJ;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/CheckBoxPreference;

    new-instance v1, LfB;

    invoke-direct {v1, p0}, LfB;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 207
    return-void
.end method

.method private a(LaaM;)V
    .registers 7
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 335
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "shared_preferences.warn_offline_sync_broadband"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 338
    sget-object v1, LaaM;->c:LaaM;

    invoke-virtual {p1, v1}, LaaM;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_37

    .line 339
    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_36

    .line 340
    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    .line 341
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 342
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 343
    const-string v3, "warnOfflineSyncEnabled"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 344
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 345
    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 346
    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 355
    :cond_36
    :goto_36
    return-void

    .line 348
    :cond_37
    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_36

    .line 349
    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 350
    const-string v2, "warnOfflineSyncEnabled"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 352
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 353
    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_36
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->g()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->c(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 329
    invoke-static {p1}, LaaM;->a(Ljava/lang/String;)LaaM;

    move-result-object v0

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, LaaM;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 331
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a(LaaM;)V

    .line 332
    return-void
.end method

.method private a(Ljava/lang/String;LoL;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 368
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "preference should not be null: label="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    new-instance v1, LfL;

    invoke-direct {v1, p0, p2}, LfL;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;LoL;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 377
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)Landroid/preference/CheckBoxPreference;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    .line 211
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "shared_preferences.cache_size"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->f()V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "clear_cache"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/Preference;

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/Preference;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    new-instance v1, LfG;

    invoke-direct {v1, p0}, LfG;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/Preference;

    new-instance v1, LfH;

    invoke-direct {v1, p0}, LfH;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V
    :try_end_3b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3b} :catch_3c

    .line 246
    :goto_3b
    return-void

    .line 237
    :catch_3c
    move-exception v0

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "docs_preference_screen"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    const-string v2, "cache_category"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceGroup;

    .line 244
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_3b
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Len;->prefs_cache_size_summary_format_string:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 430
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method private c()V
    .registers 4

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Lgl;

    sget-object v1, Lgi;->i:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    .line 250
    if-nez v0, :cond_1c

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    const-string v2, "encryption"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 271
    :goto_1b
    return-void

    .line 254
    :cond_1c
    const-string v0, "enable_pin_encryption"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/CheckBoxPreference;

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LaaJ;

    invoke-interface {v0}, LaaJ;->b()Z

    move-result v0

    .line 257
    if-nez v0, :cond_4f

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "encryption"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v1

    const-string v2, "streaming_decryption"

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    .line 268
    :goto_47
    const-string v0, "enable_pin_encryption"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->d:LoL;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a(Ljava/lang/String;LoL;)V

    goto :goto_1b

    .line 264
    :cond_4f
    const-string v0, "streaming_decryption"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->c:Landroid/preference/CheckBoxPreference;

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->c:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_47
.end method

.method private c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 433
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Z

    .line 434
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b(Ljava/lang/String;)V

    .line 435
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method private d()V
    .registers 4

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LKS;

    const-string v1, "storageUpgradeUrl"

    const-string v2, "https://accounts.google.com/AddSession?&continue=https://www.google.com/settings/storage"

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 276
    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2d

    const/4 v0, 0x1

    :goto_13
    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Z

    .line 277
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Z

    if-nez v0, :cond_2f

    .line 278
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "storage"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 301
    :goto_2c
    return-void

    .line 276
    :cond_2d
    const/4 v0, 0x0

    goto :goto_13

    .line 282
    :cond_2f
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v2, "storage_add"

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/Preference;

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/Preference;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/Preference;

    new-instance v2, LfI;

    invoke-direct {v2, p0, v1}, LfI;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto :goto_2c
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method private e()V
    .registers 3

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "shared_preferences.pinned_files_auto_sync_options"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    invoke-static {}, LaaM;->values()[LaaM;

    move-result-object v0

    array-length v0, v0

    .line 309
    invoke-static {p0}, LaaM;->a(Landroid/content/Context;)[Ljava/lang/CharSequence;

    move-result-object v0

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 312
    invoke-static {}, LaaM;->a()[Ljava/lang/CharSequence;

    move-result-object v0

    .line 313
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LaaJ;

    invoke-interface {v0}, LaaJ;->a()LaaM;

    move-result-object v0

    .line 316
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0}, LaaM;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    new-instance v1, LfK;

    invoke-direct {v1, p0}, LfK;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 326
    return-void
.end method

.method public static synthetic f(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method private f()V
    .registers 10

    .prologue
    const/4 v1, 0x0

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LaaJ;

    invoke-interface {v0}, LaaJ;->a()LaaL;

    move-result-object v2

    .line 411
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Len;->prefs_cache_size_choice_format_string:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 413
    invoke-interface {v2}, LaaL;->a()[I

    move-result-object v4

    .line 414
    array-length v0, v4

    new-array v5, v0, [Ljava/lang/CharSequence;

    .line 415
    array-length v0, v4

    new-array v6, v0, [Ljava/lang/CharSequence;

    move v0, v1

    .line 416
    :goto_1c
    array-length v7, v4

    if-ge v0, v7, :cond_3b

    .line 417
    aget v7, v4, v0

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    .line 418
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aget v8, v4, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v1

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v0

    .line 416
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 421
    :cond_3b
    invoke-interface {v2}, LaaL;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 422
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    invoke-virtual {v1, v5}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 423
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    invoke-virtual {v1, v6}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 424
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 425
    return-void
.end method

.method public static synthetic g(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method private g()V
    .registers 3

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LUL;

    invoke-interface {v0}, LUL;->b()V

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/Preference;

    sget v1, Len;->clear_cache_cleared_message:I

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(I)V

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/Preference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 441
    return-void
.end method

.method public static synthetic h(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method private h()V
    .registers 13

    .prologue
    const-wide/16 v10, 0x64

    const-wide/16 v2, 0x0

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LZj;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LME;

    invoke-interface {v1}, LME;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LZj;->a(Ljava/lang/String;)LZi;

    move-result-object v0

    .line 447
    invoke-virtual {v0}, LZi;->a()J

    move-result-wide v4

    .line 448
    invoke-static {v4, v5}, LZU;->a(J)Ljava/lang/String;

    move-result-object v6

    .line 449
    invoke-virtual {v0}, LZi;->b()J

    move-result-wide v7

    .line 450
    invoke-static {v7, v8}, LZU;->a(J)Ljava/lang/String;

    move-result-object v9

    .line 452
    cmp-long v0, v4, v2

    if-eqz v0, :cond_6d

    .line 453
    mul-long v0, v7, v10

    div-long/2addr v0, v4

    .line 455
    :goto_27
    cmp-long v2, v0, v2

    if-ltz v2, :cond_2f

    cmp-long v2, v0, v10

    if-lez v2, :cond_51

    .line 456
    :cond_2f
    const-string v2, "DocsPreferenceActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Wrong quota info. Total: "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Used: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :cond_51
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/Preference;

    sget v3, Len;->prefs_storage_add_summary:I

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v9, v4, v5

    const/4 v5, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x2

    aput-object v6, v4, v0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 460
    return-void

    :cond_6d
    move-wide v0, v2

    goto :goto_27
.end method

.method public static synthetic i(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method private i()V
    .registers 3

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnF;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, LnF;->a(Landroid/app/Activity;I)LnE;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnE;

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnE;

    new-instance v1, LfM;

    invoke-direct {v1, p0}, LfM;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V

    invoke-interface {v0, v1}, LnE;->a(LnB;)LoL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LoL;

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnE;

    new-instance v1, LfO;

    invoke-direct {v1, p0}, LfO;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V

    invoke-interface {v0, v1}, LnE;->a(LnB;)LoL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:LoL;

    .line 513
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnE;

    new-instance v1, LfP;

    invoke-direct {v1, p0}, LfP;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V

    invoke-interface {v0, v1}, LnE;->a(LnB;)LoL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->c:LoL;

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnE;

    new-instance v1, LfC;

    invoke-direct {v1, p0}, LfC;-><init>(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V

    invoke-interface {v0, v1}, LnE;->a(LnB;)LoL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->d:LoL;

    .line 616
    return-void
.end method

.method public static synthetic j(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method public static synthetic k(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method

.method public static synthetic l(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 136
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboPreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LMO;

    invoke-interface {v0, p0}, LMO;->a(Landroid/app/Activity;)LMM;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LMM;

    .line 139
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->i()V

    .line 141
    sget v0, Leq;->preferences:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->addPreferencesFromResource(I)V

    .line 143
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a()V

    .line 144
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b()V

    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->c()V

    .line 146
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->d()V

    .line 147
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->e()V

    .line 149
    const-string v0, "about"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LoL;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a(Ljava/lang/String;LoL;)V

    .line 150
    const-string v0, "legal"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:LoL;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a(Ljava/lang/String;LoL;)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    const-string v1, "additional_filters"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 155
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LKS;

    const-string v2, "additionalFilters"

    invoke-interface {v1, v2, v3}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4f

    .line 156
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 161
    :goto_4e
    return-void

    .line 158
    :cond_4f
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LKS;

    const-string v2, "additionalFiltersDefault"

    invoke-interface {v1, v2, v3}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    goto :goto_4e
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnE;

    invoke-interface {v0, p1}, LnE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnE;

    invoke-interface {v0, p1}, LnE;->a(I)Landroid/app/Dialog;

    move-result-object v0

    .line 631
    :goto_13
    return-object v0

    :cond_14
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboPreferenceActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_13
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LMM;

    invoke-interface {v0, p1}, LMM;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_13

    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboPreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_13
    const/4 v0, 0x1

    :goto_14
    return v0

    :cond_15
    const/4 v0, 0x0

    goto :goto_14
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 400
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Z

    if-eqz v0, :cond_16

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LaaJ;

    invoke-interface {v0}, LaaJ;->a()V

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LUL;

    invoke-interface {v0}, LUL;->a()V

    .line 403
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Z

    .line 406
    :cond_16
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboPreferenceActivity;->onPause()V

    .line 407
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 359
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboPreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 363
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LMM;

    invoke-virtual {v0}, Lfb;->a()I

    move-result v0

    invoke-interface {v1, v0}, LMM;->a(I)V

    .line 364
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 637
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnE;

    invoke-interface {v0, p1}, LnE;->a(I)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnE;

    invoke-interface {v0, p1, p2}, LnE;->a(ILandroid/app/Dialog;)V

    .line 643
    :goto_12
    return-void

    .line 640
    :cond_13
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/RoboPreferenceActivity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    goto :goto_12
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 381
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboPreferenceActivity;->onResume()V

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b(Ljava/lang/String;)V

    .line 386
    :cond_18
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Z

    if-eqz v0, :cond_1f

    .line 387
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->h()V

    .line 390
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_34

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->b:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a(Ljava/lang/String;)V

    .line 393
    :cond_34
    return-void
.end method
