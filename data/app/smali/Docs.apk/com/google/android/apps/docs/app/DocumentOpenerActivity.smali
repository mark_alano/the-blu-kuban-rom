.class public Lcom/google/android/apps/docs/app/DocumentOpenerActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "DocumentOpenerActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements LnS;
.implements Lpa;


# static fields
.field public static final a:Landroid/net/Uri;


# instance fields
.field private a:I

.field public a:LUL;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LXS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaZ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/lang/Runnable;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/concurrent/Executor;

.field public a:LlE;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lnm;

.field public a:LoZ;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "DefaultRemote"
    .end annotation
.end field

.field public a:LpX;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lpk;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lqb;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final b:Landroid/os/Handler;

.field private b:Ljava/lang/String;

.field public b:LoZ;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "DefaultLocal"
    .end annotation
.end field

.field private i:Z

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 118
    const-string v0, "content://com.google.android.apps.docs/open"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 160
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    .line 162
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:I

    .line 172
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Landroid/os/Handler;

    .line 173
    new-instance v0, Laat;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Landroid/os/Handler;

    invoke-direct {v0, v1}, Laat;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private a(LkM;Landroid/os/Bundle;)LamQ;
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkM;",
            "Landroid/os/Bundle;",
            ")",
            "LamQ",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 368
    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    .line 369
    invoke-virtual {p1}, LkM;->a()LkP;

    move-result-object v2

    .line 370
    invoke-virtual {v2}, LkP;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LkM;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, LkM;->d()Z

    move-result v5

    invoke-static {v3, v4, v5}, LkO;->b(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:I

    .line 373
    invoke-static {p2}, Lpb;->a(Landroid/os/Bundle;)LfS;

    move-result-object v3

    .line 375
    sget-object v4, LfS;->a:LfS;

    if-ne v3, v4, :cond_98

    .line 376
    invoke-virtual {p1}, LkM;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7f

    .line 377
    invoke-virtual {p1}, LkM;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3e

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LoZ;

    .line 419
    :cond_30
    :goto_30
    invoke-static {}, LamZ;->a()LamZ;

    move-result-object v2

    .line 420
    if-nez v0, :cond_c1

    .line 421
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v2, v0}, LamZ;->a(Ljava/lang/Object;)Z

    .line 440
    :goto_3d
    return-object v2

    .line 384
    :cond_3e
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LUL;

    sget-object v3, LUK;->a:LUK;

    invoke-interface {v0, p1, v3}, LUL;->c(LkM;LUK;)Z

    move-result v0

    .line 389
    if-eqz v0, :cond_56

    invoke-static {p0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6b

    const-string v0, "openOfflineVersion"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6b

    :cond_56
    const/4 v0, 0x1

    .line 391
    :goto_57
    if-eqz v0, :cond_6d

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lqb;

    invoke-virtual {v0, v2}, Lqb;->a(Ljava/lang/Object;)LoZ;

    move-result-object v0

    .line 393
    if-nez v0, :cond_30

    .line 394
    const-string v0, "DocumentOpenerActivity"

    const-string v2, "calling defaultRemoteDocumentOpener.openFile"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LoZ;

    goto :goto_30

    :cond_6b
    move v0, v1

    .line 389
    goto :goto_57

    .line 398
    :cond_6d
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LpX;

    invoke-virtual {v0, v2}, LpX;->a(Ljava/lang/Object;)LoZ;

    move-result-object v0

    .line 399
    if-nez v0, :cond_30

    .line 400
    const-string v0, "DocumentOpenerActivity"

    const-string v2, "calling defaultLocalDocumentOpener.openFile"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:LoZ;

    goto :goto_30

    .line 406
    :cond_7f
    const-string v2, "DocumentOpenerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "document lacks an html uri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_30

    .line 409
    :cond_98
    invoke-virtual {p1}, LkM;->a()LkP;

    move-result-object v0

    invoke-virtual {v3, v0}, LfS;->a(LkP;)LUK;

    move-result-object v0

    .line 410
    invoke-static {p0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_ae

    iget-object v3, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LUL;

    invoke-interface {v3, p1, v0}, LUL;->c(LkM;LUK;)Z

    move-result v0

    if-nez v0, :cond_b6

    .line 412
    :cond_ae
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lpk;

    invoke-virtual {v0, v2}, Lpk;->a(Ljava/lang/Object;)LoZ;

    move-result-object v0

    goto/16 :goto_30

    .line 414
    :cond_b6
    const-string v0, "DocumentOpenerActivity"

    const-string v2, "calling defaultLocalDocumentOpener.openFile"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:LoZ;

    goto/16 :goto_30

    .line 423
    :cond_c1
    const-string v1, "DocumentOpenerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "calling opener.openFile"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    invoke-interface {v0, p0, p1, p2}, LoZ;->a(Lpa;LkM;Landroid/os/Bundle;)LamQ;

    move-result-object v0

    .line 426
    new-instance v1, LfV;

    invoke-direct {v1, p0, v2}, LfV;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;LamZ;)V

    .line 438
    iget-object v3, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v3}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_3d
.end method

.method public static a(Landroid/content/Context;LkY;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 679
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/content/Context;LkY;Z)Landroid/content/Intent;

    move-result-object v0

    .line 680
    const-string v1, "openOfflineVersion"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 684
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 685
    return-object v0
.end method

.method public static a(Landroid/content/Context;LkY;Z)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 670
    const-string v0, "null context"

    invoke-static {p0, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 671
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 672
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 673
    invoke-virtual {p1, v0}, LkY;->a(Landroid/content/Intent;)V

    .line 674
    const-string v1, "editMode"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 675
    return-object v0
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;LkP;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 744
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 745
    invoke-static {p3}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(LkP;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 746
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 747
    const-string v1, "docListTitle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 748
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/Runnable;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(LkP;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 726
    if-nez p0, :cond_5

    .line 727
    const-string v0, "application/vnd.google-apps"

    .line 729
    :goto_4
    return-object v0

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "application/vnd.google-apps."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, LkP;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Lnm;)Lnm;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    return-object p1
.end method

.method private a(Landroid/content/Context;LkY;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 537
    new-instance v0, LfW;

    invoke-direct {v0, p0, p1, p2}, LfW;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Landroid/content/Context;LkY;)V

    .line 580
    invoke-virtual {v0}, LdG;->start()V

    .line 581
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->g()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Landroid/content/Intent;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->c(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Lpc;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lpc;)V

    return-void
.end method

.method private a(Lpc;)V
    .registers 3
    .parameter

    .prologue
    .line 689
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lpc;Ljava/lang/Throwable;)V

    .line 690
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;LkB;LkO;)Z
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(LkB;LkO;)Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 79
    iput-boolean p1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->i:Z

    return p1
.end method

.method private a(LkB;LkO;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 336
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 337
    invoke-virtual {p2}, LkO;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "root"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 338
    if-nez v1, :cond_4a

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lgl;

    sget-object v2, Lgi;->o:Lgi;

    invoke-interface {v1, v2}, Lgl;->a(Lgi;)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 339
    invoke-static {p2, v0}, Llt;->a(LkO;Ljava/util/Date;)Llt;

    move-result-object v1

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LlE;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LZM;

    invoke-static {p0, v2}, LlN;->a(Landroid/content/Context;LZM;)LlK;

    move-result-object v5

    move-object v2, p2

    move-object v3, p0

    move-object v4, p1

    invoke-static/range {v0 .. v5}, LlN;->a(LlE;LlB;LkO;Landroid/content/Context;LkB;LlK;)I

    move-result v0

    .line 346
    if-eqz v0, :cond_50

    .line 347
    const-string v1, "DocumentOpenerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not mark Entry as viewed; result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    const/4 v0, 0x0

    .line 356
    :goto_49
    return v0

    .line 353
    :cond_4a
    invoke-virtual {p2, v0}, LkO;->c(Ljava/util/Date;)V

    .line 354
    invoke-virtual {p2}, LkO;->c()V

    .line 356
    :cond_50
    const/4 v0, 0x1

    goto :goto_49
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method private b(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    .line 248
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 249
    const-string v0, "resourceId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_2f

    .line 251
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_2f

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_2f

    .line 253
    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    .line 256
    :cond_2f
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_52

    .line 257
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_49

    .line 259
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    .line 261
    :cond_49
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    if-nez v0, :cond_53

    .line 262
    sget-object v0, Lpc;->b:Lpc;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lpc;)V

    .line 330
    :cond_52
    :goto_52
    return-void

    .line 266
    :cond_53
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 267
    if-nez v0, :cond_63

    .line 268
    sget-object v0, Lpc;->b:Lpc;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lpc;)V

    goto :goto_52

    .line 272
    :cond_63
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v1

    .line 273
    if-nez v1, :cond_93

    .line 276
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Llf;->a(LkB;Ljava/lang/String;)LkH;

    move-result-object v1

    .line 277
    if-eqz v1, :cond_8d

    .line 278
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(LkB;LkO;)Z

    .line 279
    const-string v0, "docListCollectionName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 280
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    invoke-static {p0, v2, v1, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Ljava/lang/String;LkH;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 282
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->startActivity(Landroid/content/Intent;)V

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    goto :goto_52

    .line 285
    :cond_8d
    sget-object v0, Lpc;->b:Lpc;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lpc;)V

    goto :goto_52

    .line 290
    :cond_93
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(LkM;Landroid/os/Bundle;)LamQ;

    move-result-object v2

    .line 291
    new-instance v3, LfU;

    invoke-direct {v3, p0, v0, v1}, LfU;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;LkB;LkM;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v0}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    goto :goto_52

    .line 326
    :cond_a6
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 327
    const-class v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 328
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/content/Intent;)V

    goto :goto_52
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->i()V

    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 603
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 604
    if-eqz v0, :cond_15

    .line 605
    const-string v1, "uri"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 607
    :cond_15
    return-void
.end method

.method private g()V
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 445
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    if-nez v1, :cond_6

    .line 458
    :goto_5
    return-void

    .line 448
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    invoke-interface {v1}, Lnm;->a()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 449
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->showDialog(I)V

    goto :goto_5

    .line 454
    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    invoke-interface {v1, v0}, Lnm;->a(I)I

    move-result v0

    .line 455
    if-gez v0, :cond_12

    .line 456
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    goto :goto_5
.end method

.method private h()V
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    if-eqz v0, :cond_37

    move v0, v1

    :goto_7
    invoke-static {v0}, Lagu;->b(Z)V

    .line 511
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_39

    :goto_e
    invoke-static {v1}, Lagu;->b(Z)V

    .line 513
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 514
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v0

    .line 516
    invoke-virtual {v0}, LkM;->a()LkY;

    move-result-object v1

    .line 518
    if-eqz v0, :cond_36

    invoke-virtual {v0}, LkM;->g()Z

    move-result v2

    if-eqz v2, :cond_36

    .line 520
    invoke-virtual {v0}, LkM;->m()Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 521
    invoke-direct {p0, p0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/content/Context;LkY;)V

    .line 526
    :cond_36
    :goto_36
    return-void

    :cond_37
    move v0, v2

    .line 510
    goto :goto_7

    :cond_39
    move v1, v2

    .line 511
    goto :goto_e

    .line 523
    :cond_3b
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LXS;

    invoke-interface {v1, p0, v0}, LXS;->a(Landroid/content/Context;LkM;)V

    goto :goto_36
.end method

.method private i()V
    .registers 2

    .prologue
    .line 647
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_c

    .line 648
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 649
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/Runnable;

    .line 651
    :cond_c
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 611
    const-string v0, "DocumentOpenerActivity"

    const-string v1, "in startIntent"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    new-instance v0, LfX;

    invoke-direct {v0, p0, p1}, LfX;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 621
    return-void
.end method

.method public a(Landroid/content/Intent;Ljava/lang/Runnable;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 626
    const-string v0, "DocumentOpenerActivity"

    const-string v1, "in startIntent with Runnable"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    new-instance v0, LfY;

    invoke-direct {v0, p0, p2, p1}, LfY;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Ljava/lang/Runnable;Landroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 644
    return-void
.end method

.method public a(Lpc;Ljava/lang/Throwable;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 694
    const-string v0, "DocumentOpenerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error occured "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    invoke-virtual {p1}, Lpc;->a()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 720
    :goto_1e
    return-void

    .line 698
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lfg;

    invoke-virtual {p1}, Lpc;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lfg;->a(I)I

    move-result v0

    .line 700
    sget v1, Len;->error_page_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 701
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 704
    iget-object v2, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Landroid/os/Handler;

    new-instance v3, LfZ;

    invoke-direct {v3, p0, v1, v0, p1}, LfZ;-><init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Ljava/lang/String;Ljava/lang/String;Lpc;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1e
.end method

.method public e()V
    .registers 2

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->j:Z

    if-eqz v0, :cond_b

    .line 200
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b(Landroid/content/Intent;)V

    .line 202
    :cond_b
    return-void
.end method

.method public f()V
    .registers 2

    .prologue
    .line 361
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    .line 362
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b(Landroid/content/Intent;)V

    .line 363
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->g()V

    .line 364
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 586
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 593
    if-nez p1, :cond_20

    .line 594
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 595
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->h()V

    .line 597
    :cond_1d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    .line 599
    :cond_20
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter

    .prologue
    .line 655
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 658
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    .line 659
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 179
    const-string v0, "DocumentOpenerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Opening "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 182
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LeQ;

    invoke-virtual {v1}, LeQ;->a()V

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LeQ;

    const-string v2, "/documentOpened"

    invoke-virtual {v1, v2, v0}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 184
    if-nez p1, :cond_63

    const/4 v0, 0x1

    :goto_36
    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->j:Z

    .line 185
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->j:Z

    if-nez v0, :cond_62

    .line 186
    const-string v0, "IsViewerStarted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->i:Z

    .line 187
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->i:Z

    if-nez v0, :cond_52

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a()Lo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(Lo;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 189
    :cond_52
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    .line 190
    const-string v0, "resourceId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    .line 195
    :cond_62
    :goto_62
    return-void

    .line 184
    :cond_63
    const/4 v0, 0x0

    goto :goto_36

    .line 192
    :cond_65
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    goto :goto_62
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 489
    packed-switch p1, :pswitch_data_20

    .line 499
    const/4 v0, 0x0

    :goto_a
    return-object v0

    .line 491
    :pswitch_b
    new-instance v0, LmC;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a()Laoo;

    move-result-object v1

    invoke-direct {v0, v1, p0, v2}, LmC;-><init>(Laoo;Landroid/content/Context;I)V

    .line 494
    invoke-virtual {v0, v2}, LmC;->setCancelable(Z)V

    .line 495
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LmC;->setCanceledOnTouchOutside(Z)V

    .line 496
    invoke-virtual {v0, p0}, LmC;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_a

    .line 489
    nop

    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_b
    .end packed-switch
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    .line 230
    const-string v0, "DocumentOpenerActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 233
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->i()V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 235
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onDestroy()V

    .line 236
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 3
    .parameter

    .prologue
    .line 240
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 242
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->setIntent(Landroid/content/Intent;)V

    .line 243
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b(Landroid/content/Intent;)V

    .line 244
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 463
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/app/BaseActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 464
    packed-switch p1, :pswitch_data_3e

    .line 484
    :goto_b
    return-void

    .line 466
    :pswitch_c
    const-string v0, "DocumentOpenerActivity"

    const-string v1, "in onPrepareDialog PROGRESS_SPINNER"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    check-cast p2, LmC;

    .line 470
    iget v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:I

    if-ltz v0, :cond_1e

    .line 471
    iget v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:I

    invoke-virtual {p2, v0}, LmC;->setIcon(I)V

    .line 473
    :cond_1e
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    if-eqz v0, :cond_3a

    .line 474
    invoke-virtual {p2}, LmC;->b()V

    .line 475
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    invoke-virtual {p2, v0}, LmC;->a(Lnm;)V

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    invoke-interface {v0}, Lnm;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LmC;->setTitle(Ljava/lang/CharSequence;)V

    .line 477
    invoke-virtual {p2}, LmC;->a()V

    .line 478
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lnm;

    goto :goto_b

    .line 480
    :cond_3a
    invoke-virtual {p2}, LmC;->dismiss()V

    goto :goto_b

    .line 464
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_c
    .end packed-switch
.end method

.method protected onResume()V
    .registers 3

    .prologue
    .line 215
    const-string v0, "DocumentOpenerActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 217
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onResume()V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_16

    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    .line 226
    :cond_16
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 206
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 208
    const-string v0, "IsViewerStarted"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 209
    const-string v0, "accountName"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    const-string v0, "resourceId"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    return-void
.end method

.method public onSearchRequested()Z
    .registers 2

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 506
    const/4 v0, 0x0

    return v0
.end method
