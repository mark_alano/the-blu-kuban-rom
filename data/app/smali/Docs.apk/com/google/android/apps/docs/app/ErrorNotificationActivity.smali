.class public Lcom/google/android/apps/docs/app/ErrorNotificationActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "ErrorNotificationActivity.java"


# instance fields
.field private a:I

.field public a:LZR;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/app/AlertDialog;

.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Throwable;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 106
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BUG_REPORT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 108
    const-class v1, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 109
    const-string v1, "notification_message"

    sget v2, Len;->ouch_msg_unhandled_exception_drivev2:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 111
    invoke-static {p1}, Laaz;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    .line 112
    const-string v2, "stack_trace"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 122
    invoke-static {}, LaaF;->a()Lfo;

    move-result-object v0

    iget-boolean v0, v0, Lfo;->a:Z

    if-eqz v0, :cond_10

    .line 123
    new-instance v0, Lgh;

    invoke-direct {v0, p0}, Lgh;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 135
    :cond_10
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x2

    .line 47
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 50
    iget-object v1, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Lfg;

    const-string v2, "notification_message"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Lfg;->a(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:I

    .line 52
    const-string v1, "stack_trace"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:LZR;

    if-nez v0, :cond_32

    .line 56
    const-string v0, "ErrorNotificationActivity"

    const-string v1, "This should never happen: feedbackReporter not initialized by guice"

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    new-instance v0, LZQ;

    invoke-direct {v0}, LZQ;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:LZR;

    .line 60
    :cond_32
    invoke-static {p0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 61
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Len;->ouch_title_sawwrie:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Len;->ouch_button_close:I

    new-instance v3, Lgf;

    invoke-direct {v3, p0}, Lgf;-><init>(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Len;->ouch_button_report:I

    new-instance v3, Lge;

    invoke-direct {v3, p0}, Lge;-><init>(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 86
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Landroid/app/AlertDialog;

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Landroid/app/AlertDialog;

    new-instance v1, Lgg;

    invoke-direct {v1, p0}, Lgg;-><init>(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 97
    return-void
.end method
