.class public Lcom/google/android/apps/docs/app/InvitationActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "InvitationActivity.java"


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LNe;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LPm;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZl;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/view/View;

.field private a:Landroid/webkit/WebView;

.field private a:Landroid/widget/Button;

.field private a:Lcom/google/android/apps/docs/view/TitleBar;

.field private a:LdG;

.field public a:Lgb;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Ljava/lang/Class;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "DocListActivity"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private final a:Lqd;

.field private a:Lqe;

.field private a:Lqf;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 100
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->b:Landroid/os/Handler;

    .line 118
    new-instance v0, Lhp;

    invoke-direct {v0, p0}, Lhp;-><init>(Lcom/google/android/apps/docs/app/InvitationActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lqd;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/InvitationActivity;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/InvitationActivity;)Landroid/webkit/WebView;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/InvitationActivity;)LdG;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LdG;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/InvitationActivity;LdG;)LdG;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LdG;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/InvitationActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/InvitationActivity;)Lqe;
    .registers 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lqe;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/InvitationActivity;Lqe;)Lqe;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lqe;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/InvitationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->h()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/InvitationActivity;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/InvitationActivity;->b(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 391
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LKS;

    const-string v1, "driveInvitationFlowForceLogin"

    invoke-interface {v0, v1, v5}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 392
    const-string v1, "InvitationActivity"

    const-string v2, "showUrl(%s), forceLogin=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    if-eqz p1, :cond_29

    .line 395
    if-eqz v0, :cond_2a

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lqf;

    invoke-virtual {v0, p1}, Lqf;->b(Ljava/lang/String;)V

    .line 401
    :cond_29
    :goto_29
    return-void

    .line 398
    :cond_2a
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lqf;

    invoke-virtual {v0, p1}, Lqf;->a(Ljava/lang/String;)V

    goto :goto_29
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/InvitationActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/InvitationActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->f()V

    return-void
.end method

.method private b(Z)V
    .registers 6
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 188
    iget-object v3, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/view/View;

    if-eqz p1, :cond_13

    move v0, v1

    :goto_8
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/webkit/WebView;

    if-nez p1, :cond_15

    :goto_f
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 190
    return-void

    :cond_13
    move v0, v2

    .line 188
    goto :goto_8

    :cond_15
    move v1, v2

    .line 189
    goto :goto_f
.end method

.method private c()Ljava/lang/String;
    .registers 4

    .prologue
    .line 384
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://drive.google.com/start?device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_28

    const-string v0, "androidtablet"

    :goto_17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 386
    iget-object v1, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LKS;

    const-string v2, "invitationUrl"

    invoke-interface {v1, v2, v0}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 387
    return-object v0

    .line 384
    :cond_28
    const-string v0, "androidphone"

    goto :goto_17
.end method

.method private f()V
    .registers 4

    .prologue
    .line 268
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 269
    const-class v1, Lcom/google/android/apps/docs/app/AccountsActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 270
    const-string v1, "accountName"

    invoke-direct {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/InvitationActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 272
    return-void
.end method

.method private g()V
    .registers 2

    .prologue
    .line 380
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/InvitationActivity;->a(Ljava/lang/String;)V

    .line 381
    return-void
.end method

.method private h()V
    .registers 5

    .prologue
    .line 404
    const-string v0, "InvitationActivity"

    const-string v1, "showErrorMessage"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 406
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 407
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Len;->drive_invitation_error_drivev2:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 408
    iget-object v1, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LZM;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/webkit/WebView;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, LZM;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 409
    return-void
.end method


# virtual methods
.method a()Lfb;
    .registers 2

    .prologue
    .line 291
    sget-object v0, Lfb;->b:Lfb;

    return-object v0
.end method

.method protected a()LoT;
    .registers 2

    .prologue
    .line 413
    sget-object v0, LoT;->a:LoT;

    return-object v0
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 376
    const/4 v0, 0x0

    return v0
.end method

.method public goToDocs(Ljava/lang/String;)V
    .registers 5
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 300
    const-string v0, "InvitationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "goToDocs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->b:Landroid/os/Handler;

    new-instance v1, Lhu;

    invoke-direct {v1, p0, p1}, Lhu;-><init>(Lcom/google/android/apps/docs/app/InvitationActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 313
    return-void
.end method

.method public goToDrive(Ljava/lang/String;)V
    .registers 5
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 321
    const-string v0, "InvitationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "goToDrive: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->b:Landroid/os/Handler;

    new-instance v1, Lhv;

    invoke-direct {v1, p0, p1}, Lhv;-><init>(Lcom/google/android/apps/docs/app/InvitationActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 335
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 277
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 278
    if-nez p1, :cond_23

    .line 279
    const/4 v0, -0x1

    if-ne p2, v0, :cond_23

    .line 280
    const-string v0, "accountName"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 281
    iget-object v1, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_23

    .line 282
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->finish()V

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LZl;

    invoke-interface {v1, p0, v0}, LZl;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 287
    :cond_23
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 13
    .parameter

    .prologue
    .line 194
    const-string v0, "InvitationActivity"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 196
    sget v0, Lej;->invitation:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/InvitationActivity;->setContentView(I)V

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Ljava/lang/String;

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Ljava/lang/String;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget v0, Leh;->title_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/InvitationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/TitleBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->a()LMM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-interface {v0, v1}, LMM;->a(LMZ;)V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->a()LMM;

    move-result-object v0

    sget v1, Len;->app_name_drivev2:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/InvitationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LMM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->a()LMM;

    move-result-object v0

    invoke-interface {v0}, LMM;->a()V

    .line 206
    sget v0, Leh;->account_switcher:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/InvitationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/widget/Button;

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/widget/Button;

    new-instance v1, Lhs;

    invoke-direct {v1, p0}, Lhs;-><init>(Lcom/google/android/apps/docs/app/InvitationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    sget v0, Leh;->progress_block:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/InvitationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/view/View;

    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->a()Lo;

    move-result-object v0

    .line 218
    sget v1, Leh;->webview_fragment:I

    invoke-virtual {v0, v1}, Lo;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/WebViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->b()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/webkit/WebView;

    .line 220
    new-instance v0, Lht;

    iget-object v3, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lqd;

    invoke-direct {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LKS;

    iget-object v6, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Ljava/lang/Class;

    iget-object v7, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LPm;

    const-string v1, "webview"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/docs/app/InvitationActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LNe;

    iget-object v10, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->b:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v10}, Lht;-><init>(Lcom/google/android/apps/docs/app/InvitationActivity;Landroid/content/Context;Lqd;Ljava/lang/String;LKS;Ljava/lang/Class;LPm;Landroid/content/SharedPreferences;LNe;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lqf;

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lqf;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/webkit/WebView;

    const-string v1, "Cakemix"

    invoke-virtual {v0, p0, v1}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->g()V

    .line 265
    return-void
.end method

.method public onDestroy()V
    .registers 2

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LdG;

    if-eqz v0, :cond_11

    .line 364
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LdG;

    invoke-virtual {v0}, LdG;->a()V

    .line 365
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LdG;

    .line 367
    :cond_11
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onDestroy()V

    .line 368
    return-void
.end method

.method protected onResume()V
    .registers 5

    .prologue
    .line 339
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onResume()V

    .line 340
    const-string v0, "InvitationActivity"

    const-string v1, "in onResume"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LME;

    invoke-interface {v0}, LME;->a()[Landroid/accounts/Account;

    move-result-object v0

    .line 344
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->a()LMM;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/widget/Button;

    new-instance v3, Lhw;

    invoke-direct {v3, p0}, Lhw;-><init>(Lcom/google/android/apps/docs/app/InvitationActivity;)V

    invoke-interface {v1, v2, v0, v3}, LMM;->a(Landroid/widget/Button;[Landroid/accounts/Account;LMN;)V

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->a()LMM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/google/android/apps/docs/app/InvitationActivity;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LMM;->a(Landroid/widget/Button;Ljava/lang/String;)V

    .line 358
    return-void
.end method
