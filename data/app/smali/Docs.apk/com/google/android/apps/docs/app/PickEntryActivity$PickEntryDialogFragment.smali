.class public Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;
.super Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;
.source "PickEntryActivity.java"

# interfaces
.implements LE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;",
        "LE",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;

.field public a:LatG;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LatG",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private a:LhY;

.field public a:LiG;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LiQ;

.field public a:LiT;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LkP;",
            ">;"
        }
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Landroid/view/View;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;-><init>()V

    .line 218
    invoke-static {}, Lalp;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/HashMap;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/Set;

    return-object v0
.end method

.method private a()LkB;
    .registers 3

    .prologue
    .line 605
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 606
    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Llf;

    invoke-interface {v1, v0}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 607
    return-object v0
.end method

.method private a(Ljava/lang/String;)LkH;
    .registers 4
    .parameter

    .prologue
    .line 591
    if-nez p1, :cond_4

    .line 592
    const/4 v0, 0x0

    .line 594
    :goto_3
    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Llf;

    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()LkB;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Llf;->a(LkB;Ljava/lang/String;)LkH;

    move-result-object v0

    goto :goto_3
.end method

.method private a(LkO;)LkH;
    .registers 6
    .parameter

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Llf;

    invoke-interface {v0, p1}, Llf;->a(LkO;)Ljava/util/Map;

    move-result-object v1

    .line 521
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 522
    const/4 v0, 0x0

    .line 536
    :cond_d
    :goto_d
    return-object v0

    .line 525
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    if-eqz v0, :cond_2a

    .line 526
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(Ljava/lang/String;)LkH;

    move-result-object v0

    .line 527
    if-eqz v0, :cond_2a

    invoke-virtual {v0}, LkH;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 534
    :cond_2a
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 535
    iget-object v2, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Llf;

    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()LkB;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1}, Llf;->a(LkB;J)LkH;

    move-result-object v0

    goto :goto_d
.end method

.method private a(Ljava/lang/String;)LkO;
    .registers 4
    .parameter

    .prologue
    .line 598
    if-nez p1, :cond_4

    .line 599
    const/4 v0, 0x0

    .line 601
    :goto_3
    return-object v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Llf;

    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()LkB;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    goto :goto_3
.end method

.method private a()LmK;
    .registers 5

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LatG;

    invoke-interface {v0}, LatG;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 580
    sget-object v1, LhW;->a:[I

    invoke-virtual {v0}, Lfb;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_32

    .line 586
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown application mode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 582
    :pswitch_2c
    sget-object v0, LmK;->v:LmK;

    .line 584
    :goto_2e
    return-object v0

    :pswitch_2f
    sget-object v0, LmK;->a:LmK;

    goto :goto_2e

    .line 580
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_2c
        :pswitch_2f
    .end packed-switch
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->p()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->b(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 409
    .line 413
    if-eqz p1, :cond_d9

    .line 414
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(Ljava/lang/String;)LkO;

    move-result-object v1

    .line 415
    if-eqz v1, :cond_d6

    .line 416
    instance-of v0, v1, LkH;

    if-eqz v0, :cond_a3

    move-object v0, v1

    .line 417
    check-cast v0, LkH;

    .line 424
    :goto_10
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v3

    .line 425
    const-string v4, "disablePreselectedEntry"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 426
    const-string v5, "resourceId"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 429
    if-eqz v1, :cond_a9

    iget-object v5, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/Set;

    invoke-virtual {v1}, LkO;->a()LkP;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a9

    if-eqz v4, :cond_3a

    invoke-virtual {v1}, LkO;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a9

    :cond_3a
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(LkO;)Z

    move-result v3

    if-nez v3, :cond_a9

    .line 432
    invoke-virtual {v1}, LkO;->i()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c:Ljava/lang/String;

    .line 439
    :goto_46
    new-instance v1, LiR;

    invoke-direct {v1}, LiR;-><init>()V

    .line 440
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "accountName"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 441
    iget-object v4, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiG;

    invoke-interface {v4, v3}, LiG;->a(Ljava/lang/String;)LiE;

    move-result-object v4

    invoke-virtual {v1, v4}, LiR;->a(LiE;)LiR;

    .line 442
    iget-object v4, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiG;

    invoke-interface {v4}, LiG;->a()LiE;

    move-result-object v4

    invoke-virtual {v1, v4}, LiR;->a(LiE;)LiR;

    .line 443
    if-eqz v0, :cond_af

    invoke-virtual {v0}, LkH;->i()Ljava/lang/String;

    move-result-object v4

    const-string v5, "root"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_af

    .line 444
    iget-object v4, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiG;

    invoke-virtual {v0}, LkH;->i()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, LiG;->a(Ljava/lang/String;Ljava/lang/String;)LiE;

    move-result-object v3

    invoke-virtual {v1, v3}, LiR;->a(LiE;)LiR;

    .line 446
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(LkO;)LkH;

    move-result-object v0

    .line 447
    if-eqz v0, :cond_ac

    invoke-virtual {v0}, LkH;->i()Ljava/lang/String;

    move-result-object v0

    :goto_8c
    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Ljava/lang/String;

    .line 457
    :goto_8e
    invoke-virtual {v1}, LiR;->a()LiQ;

    move-result-object v0

    .line 458
    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_bf

    .line 459
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LhY;

    invoke-virtual {v0, p1}, LhY;->a(Ljava/lang/String;)V

    .line 470
    :goto_9f
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->q()V

    .line 471
    return-void

    .line 419
    :cond_a3
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(LkO;)LkH;

    move-result-object v0

    goto/16 :goto_10

    .line 435
    :cond_a9
    iput-object v2, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c:Ljava/lang/String;

    goto :goto_46

    .line 447
    :cond_ac
    const-string v0, "root"

    goto :goto_8c

    .line 450
    :cond_af
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiG;

    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()LmK;

    move-result-object v4

    invoke-interface {v0, v4, v3}, LiG;->b(LmK;Ljava/lang/String;)LiE;

    move-result-object v0

    .line 452
    invoke-virtual {v1, v0}, LiR;->a(LiE;)LiR;

    .line 453
    iput-object v2, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Ljava/lang/String;

    goto :goto_8e

    .line 461
    :cond_bf
    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    .line 463
    :try_start_c1
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    invoke-interface {v0}, LiQ;->a()V
    :try_end_c6
    .catch LiF; {:try_start_c1 .. :try_end_c6} :catch_cf

    .line 467
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()LD;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, LD;->a(ILandroid/os/Bundle;LE;)LL;

    goto :goto_9f

    .line 464
    :catch_cf
    move-exception v0

    .line 465
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_d6
    move-object v0, v2

    goto/16 :goto_10

    :cond_d9
    move-object v1, v2

    move-object v0, v2

    goto/16 :goto_10
.end method

.method private a(LkO;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 507
    :goto_9
    return v1

    .line 484
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-virtual {p1}, LkO;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 485
    if-eqz v0, :cond_1d

    .line 486
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_9

    .line 490
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Llf;

    invoke-interface {v0, p1}, Llf;->a(LkO;)Ljava/util/Map;

    move-result-object v0

    .line 492
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()LkB;

    move-result-object v2

    .line 493
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2f
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Llf;

    invoke-interface {v0, v2, v4, v5}, Llf;->a(LkB;J)LkH;

    move-result-object v0

    .line 496
    if-eqz v0, :cond_2f

    .line 499
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(LkO;)Z

    move-result v0

    .line 500
    if-eqz v0, :cond_2f

    .line 501
    const/4 v0, 0x1

    .line 506
    :goto_4e
    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-virtual {p1}, LkO;->i()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v1, v0

    .line 507
    goto :goto_9

    :cond_5d
    move v0, v1

    goto :goto_4e
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LdL;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 381
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 382
    const-string v1, "selectButtonText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 383
    if-lez v1, :cond_11

    .line 384
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 391
    :cond_10
    :goto_10
    return-object v0

    .line 386
    :cond_11
    const-string v1, "selectButtonText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 387
    if-nez v0, :cond_10

    .line 391
    sget v0, Len;->dialog_select:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_10
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(I)V
    .registers 5
    .parameter

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LhY;

    invoke-virtual {v0, p1}, LhY;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 397
    sget-object v1, LPX;->p:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1, v0}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LkO;->a(Ljava/lang/String;)LkP;

    move-result-object v1

    .line 399
    sget-object v2, LkP;->h:LkP;

    invoke-virtual {v2, v1}, LkP;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_27

    iget-object v2, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_27

    .line 405
    :goto_26
    return-void

    .line 403
    :cond_27
    sget-object v1, LPX;->n:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1, v0}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 404
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(Ljava/lang/String;)V

    goto :goto_26
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->r()V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LdL;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    invoke-interface {v0}, LiQ;->c()Ljava/lang/String;

    move-result-object v0

    .line 557
    if-nez v0, :cond_a

    .line 558
    const-string v0, "root"

    .line 561
    :cond_a
    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LdL;

    return-object v0
.end method

.method private p()V
    .registers 8

    .prologue
    const/4 v6, 0x0

    .line 269
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c()Ljava/lang/String;

    move-result-object v2

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Leh;->icon_layout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Leh;->icon:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 273
    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Landroid/view/View;

    sget v4, Leh;->title:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 274
    const-string v4, "root"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5a

    .line 275
    const/4 v2, 0x4

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 276
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 291
    :goto_32
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    invoke-interface {v0}, LiQ;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 296
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 297
    const v0, 0x106000d

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Landroid/view/View;

    sget v1, Lee;->pick_entry_dialog_selected:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 304
    :goto_59
    return-void

    .line 278
    :cond_5a
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(Ljava/lang/String;)LkH;

    move-result-object v2

    .line 279
    if-nez v2, :cond_66

    .line 280
    const-string v0, "root"

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(Ljava/lang/String;)V

    goto :goto_59

    .line 284
    :cond_66
    invoke-virtual {v2}, LkH;->a()LkP;

    move-result-object v4

    invoke-virtual {v4}, LkP;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, LkH;->g()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, LkH;->d()Z

    move-result v2

    invoke-static {v4, v5, v2}, LkO;->a(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v2

    .line 286
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 287
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 288
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_32

    .line 300
    :cond_84
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 301
    sget v0, Leg;->state_selector_background:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Landroid/view/View;

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_59
.end method

.method private q()V
    .registers 3

    .prologue
    .line 474
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 475
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c:Ljava/lang/String;

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_10
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 477
    return-void

    .line 476
    :cond_14
    const/4 v0, 0x0

    goto :goto_10
.end method

.method private r()V
    .registers 5

    .prologue
    .line 571
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 572
    const-string v1, "resourceId"

    iget-object v2, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 573
    const-string v1, "bundle"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "bundle"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 574
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 575
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)LL;
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "LL",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 542
    sget-object v0, Lrl;->a:Lrl;

    invoke-virtual {v0}, Lrl;->a()LqT;

    move-result-object v0

    .line 543
    new-instance v1, LmJ;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    invoke-interface {v2}, LiQ;->a()Lnh;

    move-result-object v2

    invoke-virtual {v0}, LqT;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, LmJ;-><init>(Lnh;Ljava/lang/String;)V

    .line 545
    new-instance v0, LmI;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Llf;

    const-string v4, "PickEntryDialog"

    invoke-direct {v0, v2, v3, v1, v4}, LmI;-><init>(Landroid/content/Context;Llf;LmJ;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 320
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 321
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 322
    iget-object v2, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LhY;

    const/4 v3, -0x1

    new-instance v4, Lia;

    invoke-direct {v4, p0}, Lia;-><init>(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 330
    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 332
    sget v2, Lej;->pick_entry_dialog_header:I

    invoke-virtual {v0, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Landroid/view/View;

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Landroid/view/View;

    sget v2, Leh;->icon_layout:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 334
    new-instance v2, Lib;

    invoke-direct {v2, p0}, Lib;-><init>(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Landroid/view/View;

    sget v2, Leh;->title:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 342
    new-instance v2, Lic;

    invoke-direct {v2, p0}, Lic;-><init>(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 352
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lid;

    invoke-direct {v2, p0}, Lid;-><init>(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 359
    const/high16 v0, 0x104

    invoke-virtual {v1, v0, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 361
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 362
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Landroid/widget/ListView;

    .line 363
    return-object v0
.end method

.method public a(LL;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LL",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 566
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LhY;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LhY;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 568
    return-void
.end method

.method public a(LL;Landroid/database/Cursor;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LL",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LhY;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, p2, v1}, LhY;->a(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 552
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 553
    return-void
.end method

.method public bridge synthetic a(LL;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 185
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(LL;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 226
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->a(Landroid/os/Bundle;)V

    .line 228
    if-eqz p1, :cond_5a

    move-object v0, p1

    .line 230
    :goto_6
    const-string v1, "resourceId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c:Ljava/lang/String;

    .line 231
    if-eqz p1, :cond_6d

    .line 233
    :try_start_10
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiT;

    invoke-interface {v0, p1}, LiT;->a(Landroid/os/Bundle;)LiQ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    invoke-interface {v0}, LiQ;->a()V
    :try_end_1d
    .catch LiU; {:try_start_10 .. :try_end_1d} :catch_5f
    .catch LiF; {:try_start_10 .. :try_end_1d} :catch_66

    .line 240
    const-string v0, "parentResourceId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Ljava/lang/String;

    .line 242
    const-string v0, "isEntryDisabledMap"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 244
    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 254
    :cond_32
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "enabledKinds"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 256
    if-eqz v0, :cond_92

    :goto_40
    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/Set;

    .line 258
    new-instance v0, LhY;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, LhY;-><init>(Landroid/content/Context;Ljava/util/Set;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LhY;

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LhY;

    new-instance v1, LhZ;

    invoke-direct {v1, p0}, LhZ;-><init>(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)V

    invoke-virtual {v0, v1}, LhY;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 266
    return-void

    .line 228
    :cond_5a
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_6

    .line 235
    :catch_5f
    move-exception v0

    .line 236
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 237
    :catch_66
    move-exception v0

    .line 238
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 246
    :cond_6d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "disabledAncestors"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 248
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_32

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 249
    iget-object v2, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7b

    .line 256
    :cond_92
    const-class v0, LkP;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    goto :goto_40
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 308
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->c(Landroid/os/Bundle;)V

    .line 310
    const-string v0, "resourceId"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    const-string v0, "parentResourceId"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    const-string v0, "isEntryDisabledMap"

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiT;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    invoke-interface {v0, p1, v1}, LiT;->a(Landroid/os/Bundle;LiQ;)V

    .line 314
    return-void
.end method

.method public g()V
    .registers 4

    .prologue
    .line 368
    invoke-super {p0}, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity$ModalDialogFragment;->g()V

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LhY;

    invoke-virtual {v0}, LhY;->a()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1b

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiQ;

    if-eqz v0, :cond_1c

    .line 372
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a()LD;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, LD;->a(ILandroid/os/Bundle;LE;)LL;

    .line 373
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->q()V

    .line 378
    :cond_1b
    :goto_1b
    return-void

    .line 375
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a(Ljava/lang/String;)V

    goto :goto_1b
.end method
