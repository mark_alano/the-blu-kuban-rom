.class public Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;
.super Lcom/google/android/apps/docs/app/AccountListeningActivity;
.source "TabletDocListActivity.java"

# interfaces
.implements LLF;
.implements LLG;
.implements Lgd;
.implements LiY;
.implements Liw;
.implements Liz;
.implements LjZ;
.implements Ljy;
.implements Ljz;
.implements Lka;
.implements Loa;
.implements Lq;


# instance fields
.field private a:I

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LMf;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LPm;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LUL;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LWQ;

.field public a:LXX;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZj;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZl;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaJ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/database/ContentObserver;

.field private a:Landroid/os/Bundle;

.field private a:Landroid/view/Menu;

.field private a:Landroid/view/MenuItem;

.field private a:Landroid/widget/Toast;

.field public a:Lcom/google/android/apps/docs/fragment/DocListFragment;

.field private a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

.field private a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

.field private a:Lcom/google/android/apps/docs/view/TitleBar;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lev;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LiA;

.field public a:LiG;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LiX;

.field private final a:Liv;

.field public a:LjY;

.field private a:Ljava/lang/Runnable;

.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljc;

.field private final a:Lji;

.field public a:Ljl;

.field public a:Ljo;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Ljx;

.field public a:LlE;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llz;
    .annotation runtime Laon;
    .end annotation
.end field

.field private b:I

.field public b:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field private b:Landroid/database/ContentObserver;

.field private final b:Landroid/os/Handler;

.field private b:Landroid/view/MenuItem;

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public b:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field public c:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/AccountListeningActivity;-><init>()V

    .line 238
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/os/Handler;

    .line 240
    invoke-static {}, LZL;->b()Z

    move-result v0

    if-eqz v0, :cond_45

    new-instance v0, LjA;

    invoke-direct {v0, p0, p0, p0}, LjA;-><init>(Ljy;Ljz;Landroid/app/Activity;)V

    :goto_17
    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljx;

    .line 243
    new-instance v0, Lix;

    invoke-direct {v0}, Lix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    .line 244
    new-instance v0, LiZ;

    invoke-direct {v0}, LiZ;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    .line 245
    new-instance v0, Ljj;

    invoke-direct {v0}, Ljj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lji;

    .line 246
    new-instance v0, Ljd;

    invoke-direct {v0}, Ljd;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljc;

    .line 247
    new-instance v0, LiB;

    invoke-direct {v0}, LiB;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiA;

    .line 258
    iput-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljava/lang/Runnable;

    .line 287
    iput-boolean v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->i:Z

    .line 294
    iput-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/view/Menu;

    .line 300
    iput-boolean v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->j:Z

    return-void

    :cond_45
    move-object v0, v1

    .line 240
    goto :goto_17
.end method

.method private A()V
    .registers 3

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/database/ContentObserver;

    if-eqz v0, :cond_14

    .line 1167
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/database/ContentObserver;

    .line 1171
    :cond_14
    return-void
.end method

.method private B()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x10

    .line 1174
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 1176
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiA;

    invoke-interface {v1}, LiA;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 1178
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1180
    invoke-virtual {v0, v3, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 1194
    :goto_1a
    return-void

    .line 1183
    :cond_1b
    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 1185
    invoke-virtual {v0, v2, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 1188
    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_30

    .line 1189
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sget v1, Lej;->navigation_breadcrumb:I

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setCustomView(I)V

    .line 1192
    :cond_30
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->C()V

    goto :goto_1a
.end method

.method private C()V
    .registers 10

    .prologue
    const/4 v5, 0x0

    .line 1197
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    sget v1, Leh;->collection_path:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1199
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1200
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    .line 1202
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiA;

    invoke-interface {v1}, LiA;->a()Ljava/util/List;

    move-result-object v7

    move v4, v5

    .line 1203
    :goto_1f
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_6a

    .line 1204
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LiC;

    .line 1205
    sget v2, Lej;->navigation_breadcrumb_item:I

    invoke-virtual {v6, v2, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 1207
    sget v3, Leh;->breadcrumb_text:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1208
    invoke-virtual {v1}, LiC;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1210
    invoke-virtual {v1}, LiC;->a()Ljava/util/List;

    move-result-object v1

    .line 1211
    new-instance v8, LjN;

    invoke-direct {v8, p0, v1}, LjN;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;Ljava/util/List;)V

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1220
    sget v1, Leh;->breadcrumb_arrow:I

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1221
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v4, v3, :cond_63

    .line 1222
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1225
    :cond_63
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1203
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1f

    .line 1227
    :cond_6a
    return-void
.end method

.method private D()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1323
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->G()V

    .line 1325
    invoke-static {}, LZL;->b()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1326
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljx;

    invoke-interface {v0}, Ljx;->a()V

    .line 1334
    :cond_f
    :goto_f
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Z)V

    .line 1335
    return-void

    .line 1328
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    .line 1329
    invoke-virtual {v0}, Lo;->a()I

    move-result v1

    if-nez v1, :cond_f

    .line 1330
    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/Fragment;

    invoke-direct {v1}, Landroid/support/v4/app/Fragment;-><init>()V

    invoke-virtual {v0, v1, v2}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0, v2}, Lz;->a(Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    goto :goto_f
.end method

.method private E()V
    .registers 3

    .prologue
    .line 1338
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->G()V

    .line 1340
    invoke-static {}, LZL;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1341
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljx;

    invoke-interface {v0}, Ljx;->b()V

    .line 1344
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Z)V

    .line 1345
    return-void
.end method

.method private F()V
    .registers 3

    .prologue
    .line 1363
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v0, v1}, Lz;->b(Landroid/support/v4/app/Fragment;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 1364
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v0, v1}, Lz;->c(Landroid/support/v4/app/Fragment;)Lz;

    move-result-object v0

    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Lz;->a(I)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 1366
    return-void
.end method

.method private G()V
    .registers 3

    .prologue
    .line 1374
    const/16 v0, 0x1001

    const/16 v1, 0x2002

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(II)V

    .line 1376
    return-void
.end method

.method private H()V
    .registers 2

    .prologue
    .line 1474
    new-instance v0, LjP;

    invoke-direct {v0, p0}, LjP;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljava/util/Map;

    .line 1483
    return-void
.end method

.method private a()I
    .registers 4

    .prologue
    const/4 v0, 0x2

    .line 1380
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1381
    invoke-static {v1}, LZL;->e(Landroid/content/res/Resources;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 1382
    invoke-static {v1}, LZL;->f(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_12

    const/4 v0, 0x3

    .line 1384
    :cond_12
    :goto_12
    return v0

    :cond_13
    invoke-static {v1}, LZL;->b(Landroid/content/res/Resources;)Z

    move-result v1

    if-nez v1, :cond_12

    const/4 v0, 0x1

    goto :goto_12
.end method

.method private a(ZZ)LabA;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1130
    if-eqz p1, :cond_5

    .line 1131
    sget-object v0, LabA;->b:LabA;

    .line 1135
    :goto_4
    return-object v0

    .line 1132
    :cond_5
    if-eqz p2, :cond_a

    .line 1133
    sget-object v0, LabA;->a:LabA;

    goto :goto_4

    .line 1135
    :cond_a
    sget-object v0, LabA;->c:LabA;

    goto :goto_4
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljo;)Landroid/content/Intent;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;",
            "Ljava/lang/String;",
            "Ljo;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1560
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1561
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1562
    const-class v1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1564
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1565
    const-string v2, "accountName"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    invoke-interface {p3, v1, p1}, Ljo;->a(Landroid/os/Bundle;Ljava/util/List;)V

    .line 1567
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1569
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/os/Handler;

    return-object v0
.end method

.method private a()Lcom/google/android/apps/docs/fragment/PreviewFragment;
    .registers 3

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    sget v1, Leh;->preview_fragment:I

    invoke-virtual {v0, v1}, Lo;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/PreviewFragment;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)LiA;
    .registers 2
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiA;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)LiX;
    .registers 2
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)Liv;
    .registers 2
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 157
    iput-object p1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljava/lang/Runnable;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)Ljc;
    .registers 2
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljc;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)Lji;
    .registers 2
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lji;

    return-object v0
.end method

.method private a(II)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x2

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1396
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()I

    move-result v5

    .line 1397
    if-ltz v5, :cond_ae

    const/4 v0, 0x4

    if-ge v5, v0, :cond_ae

    move v0, v1

    :goto_e
    invoke-static {v0}, Lagu;->a(Z)V

    .line 1399
    iget-object v4, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    if-ne v5, v1, :cond_b1

    move v0, v1

    :goto_16
    invoke-virtual {v4, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->b(Z)V

    .line 1400
    iget-object v4, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    if-ne v5, v1, :cond_b4

    move v0, v1

    :goto_1e
    invoke-virtual {v4, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Z)V

    .line 1402
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1403
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b7

    move v4, v1

    .line 1404
    :goto_33
    if-eqz v4, :cond_4b

    .line 1405
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1406
    if-ne v5, v1, :cond_ba

    move v0, v3

    .line 1408
    :goto_3d
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v0, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1410
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1412
    :cond_4b
    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v0

    if-ge v0, v5, :cond_69

    .line 1413
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1414
    if-ne v5, v8, :cond_69

    .line 1417
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v0, v3}, Lz;->b(Landroid/support/v4/app/Fragment;)Lz;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Lz;)V

    .line 1421
    :cond_69
    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v0

    if-ge v0, v5, :cond_74

    .line 1422
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1424
    :cond_74
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v3

    .line 1426
    if-eqz v4, :cond_c6

    :goto_7e
    invoke-virtual {v3, p1}, Lz;->a(I)Lz;

    .line 1427
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/docs/view/RoboFragment;

    iget-object v4, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    aput-object v4, v0, v2

    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    aput-object v1, v0, v8

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_98
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_cc

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/RoboFragment;

    .line 1428
    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c8

    .line 1429
    invoke-virtual {v3, v0}, Lz;->c(Landroid/support/v4/app/Fragment;)Lz;

    goto :goto_98

    :cond_ae
    move v0, v2

    .line 1397
    goto/16 :goto_e

    :cond_b1
    move v0, v2

    .line 1399
    goto/16 :goto_16

    :cond_b4
    move v0, v2

    .line 1400
    goto/16 :goto_1e

    :cond_b7
    move v4, v2

    .line 1403
    goto/16 :goto_33

    .line 1406
    :cond_ba
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v7, Lef;->preview_panel_width:I

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto/16 :goto_3d

    :cond_c6
    move p1, p2

    .line 1426
    goto :goto_7e

    .line 1431
    :cond_c8
    invoke-virtual {v3, v0}, Lz;->b(Landroid/support/v4/app/Fragment;)Lz;

    goto :goto_98

    .line 1434
    :cond_cc
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Lz;)V

    .line 1435
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljo;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LiQ;",
            ">;",
            "Ljava/lang/String;",
            "Ljo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1574
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljo;)Landroid/content/Intent;

    move-result-object v0

    .line 1577
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1578
    return-void
.end method

.method private a(Landroid/view/Menu;I)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 725
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    .line 726
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 747
    :goto_c
    return-void

    .line 730
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llz;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Llz;->a(Ljava/lang/String;Ljava/util/Set;)Llv;

    move-result-object v1

    .line 733
    :goto_19
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    if-ge p2, v0, :cond_44

    .line 734
    invoke-interface {p1, p2}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 735
    invoke-static {}, Llx;->values()[Llx;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_29
    if-ge v0, v4, :cond_41

    aget-object v5, v3, v0

    .line 736
    invoke-virtual {v5}, Llx;->a()I

    move-result v6

    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    if-ne v6, v7, :cond_3e

    .line 737
    invoke-virtual {v1, v5}, Llv;->a(Llx;)Z

    move-result v5

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 735
    :cond_3e
    add-int/lit8 v0, v0, 0x1

    goto :goto_29

    .line 733
    :cond_41
    add-int/lit8 p2, p2, 0x1

    goto :goto_19

    .line 742
    :cond_44
    sget v0, Leh;->menu_pin:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {v1}, Llv;->a()Z

    move-result v0

    if-eqz v0, :cond_5f

    sget v0, Len;->menu_unpin:I

    :goto_52
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 745
    sget v0, Leh;->menu_move_to_folder:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 746
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Landroid/view/MenuItem;Llv;)V

    goto :goto_c

    .line 742
    :cond_5f
    sget v0, Len;->menu_offline:I

    goto :goto_52
.end method

.method private a(Landroid/view/Menu;ILjava/lang/Runnable;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 680
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 682
    if-nez v0, :cond_1f

    .line 683
    const-string v0, "TabletDocListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Menu layout does not contain requested item id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    :goto_1e
    return-void

    .line 687
    :cond_1f
    new-instance v1, LjI;

    invoke-direct {v1, p0, p3}, LjI;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1e
.end method

.method private a(Landroid/view/Menu;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 711
    iget v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:I

    :goto_2
    iget v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:I

    if-ge v0, v1, :cond_10

    .line 712
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 711
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 714
    :cond_10
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->y()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->f(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/util/Set;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1445
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    const-string v1, "tabletDoclist"

    const-string v2, "showDeleteEvent"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a(Ljava/lang/String;Ljava/util/Set;)Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;

    move-result-object v0

    .line 1449
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v1

    const-string v2, "deleteDialogFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/DeleteDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 1450
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;Landroid/view/MenuItem;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method private a()[Lii;
    .registers 4

    .prologue
    .line 1521
    .line 1522
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->c:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    invoke-virtual {v0}, Lfb;->b()I

    move-result v0

    const/4 v1, 0x2

    invoke-static {}, LaaF;->a()Lfo;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lii;->a(IILfo;)[Lii;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 4

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljl;

    invoke-interface {v0}, Ljl;->a()Ljava/util/List;

    move-result-object v1

    .line 621
    const/4 v0, 0x0

    .line 622
    if-eqz v1, :cond_1d

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1d

    .line 623
    invoke-static {v1}, LajB;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiQ;

    .line 624
    invoke-interface {v0}, LiQ;->c()Ljava/lang/String;

    move-result-object v0

    .line 625
    if-nez v0, :cond_1d

    .line 626
    const-string v0, "root"

    .line 629
    :cond_1d
    return-object v0
.end method

.method private b(Landroid/view/Menu;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 717
    iget v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:I

    :goto_2
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_12

    .line 718
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 717
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 721
    :cond_12
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->y()V

    .line 722
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->r()V

    return-void
.end method

.method private b(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter

    .prologue
    .line 649
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Leh;->menu_rename:I

    if-ne v0, v1, :cond_d

    .line 650
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->i()V

    .line 669
    :goto_b
    const/4 v0, 0x1

    :goto_c
    return v0

    .line 651
    :cond_d
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Leh;->menu_sharing:I

    if-ne v0, v1, :cond_19

    .line 652
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->w()V

    goto :goto_b

    .line 653
    :cond_19
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Leh;->menu_delete:I

    if-ne v0, v1, :cond_25

    .line 654
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->j()V

    goto :goto_b

    .line 655
    :cond_25
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Leh;->menu_send:I

    if-ne v0, v1, :cond_31

    .line 656
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->k()V

    goto :goto_b

    .line 657
    :cond_31
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Leh;->menu_send_link:I

    if-ne v0, v1, :cond_3d

    .line 658
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->l()V

    goto :goto_b

    .line 659
    :cond_3d
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Leh;->menu_print:I

    if-ne v0, v1, :cond_49

    .line 660
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->m()V

    goto :goto_b

    .line 661
    :cond_49
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Leh;->menu_pin:I

    if-ne v0, v1, :cond_55

    .line 662
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->n()V

    goto :goto_b

    .line 663
    :cond_55
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Leh;->menu_move_to_folder:I

    if-ne v0, v1, :cond_61

    .line 664
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->v()V

    goto :goto_b

    .line 666
    :cond_61
    const/4 v0, 0x0

    goto :goto_c
.end method

.method private c(Landroid/view/Menu;)V
    .registers 5
    .parameter

    .prologue
    .line 474
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lek;->menu_doclist_activity:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 477
    sget v0, Leh;->menu_refresh_icon:I

    new-instance v1, LjS;

    invoke-direct {v1, p0}, LjS;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    .line 489
    sget v0, Leh;->menu_create_new_doc:I

    new-instance v1, LjT;

    invoke-direct {v1, p0}, LjT;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    .line 497
    sget v0, Leh;->menu_create_new_from_upload:I

    new-instance v1, LjU;

    invoke-direct {v1, p0}, LjU;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    .line 504
    sget v0, Leh;->menu_filter_by:I

    new-instance v1, LjV;

    invoke-direct {v1, p0}, LjV;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    .line 537
    sget v0, Leh;->menu_sortings:I

    new-instance v1, LjW;

    invoke-direct {v1, p0}, LjW;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    .line 546
    sget v0, Leh;->menu_settings:I

    new-instance v1, LjX;

    invoke-direct {v1, p0}, LjX;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LKS;

    const-string v1, "enableMenuHelpMinApi"

    const/16 v2, 0x8

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    .line 559
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v0, :cond_a2

    .line 560
    sget v0, Leh;->menu_help:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 578
    :goto_58
    sget v0, Leh;->menu_send_feedback:I

    new-instance v1, LjF;

    invoke-direct {v1, p0}, LjF;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->c:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, Lfb;->a:Lfb;

    if-ne v0, v1, :cond_7f

    .line 591
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lek;->menu_home_page_c:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 592
    sget v0, Leh;->menu_try_google_drive:I

    new-instance v1, LjG;

    invoke-direct {v1, p0}, LjG;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    .line 601
    :cond_7f
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()LMM;

    move-result-object v0

    sget v1, Leh;->menu_search:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-interface {v0, v1, v2}, LMM;->a(Landroid/view/MenuItem;LMP;)V

    .line 602
    sget v0, Leh;->menu_refresh_status:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/view/MenuItem;

    .line 603
    sget v0, Leh;->menu_refresh_icon:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/view/MenuItem;

    .line 604
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->y()V

    .line 605
    return-void

    .line 562
    :cond_a2
    sget v0, Leh;->menu_help:I

    new-instance v1, LjE;

    invoke-direct {v1, p0}, LjE;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    goto :goto_58
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->s()V

    return-void
.end method

.method private d(Landroid/view/Menu;)V
    .registers 7
    .parameter

    .prologue
    .line 633
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    .line 634
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 635
    sget v2, Lek;->menu_selection_action_bar:I

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 636
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    .line 637
    :goto_11
    if-ge v0, v1, :cond_26

    .line 638
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 639
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    new-instance v4, LjH;

    invoke-direct {v4, p0, v2}, LjH;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;Landroid/view/MenuItem;)V

    invoke-direct {p0, p1, v3, v4}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;ILjava/lang/Runnable;)V

    .line 637
    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    .line 646
    :cond_26
    return-void
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->z()V

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    .line 783
    invoke-static {}, LaaF;->a()Lfo;

    move-result-object v0

    iget-boolean v0, v0, Lfo;->a:Z

    if-eqz v0, :cond_42

    .line 784
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LZj;

    invoke-interface {v0, p1}, LZj;->a(Ljava/lang/String;)LZi;

    move-result-object v0

    .line 786
    invoke-virtual {v0}, LZi;->c()Z

    move-result v1

    .line 787
    invoke-virtual {v0}, LZi;->d()Z

    move-result v0

    .line 788
    const-string v2, "TAG"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Capability for: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " drive="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " requested="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    :cond_42
    return-void
.end method

.method private r()V
    .registers 3

    .prologue
    .line 608
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b()Ljava/lang/String;

    move-result-object v0

    .line 609
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    return-void
.end method

.method private s()V
    .registers 3

    .prologue
    .line 614
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b()Ljava/lang/String;

    move-result-object v0

    .line 615
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    return-void
.end method

.method private t()V
    .registers 2

    .prologue
    .line 849
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 850
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->B()V

    .line 854
    :goto_d
    return-void

    .line 852
    :cond_e
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->u()V

    goto :goto_d
.end method

.method private u()V
    .registers 5

    .prologue
    .line 857
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_15

    .line 858
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 860
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 862
    const/16 v1, 0x8

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 865
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljc;

    invoke-interface {v0}, Ljc;->a()LiQ;

    move-result-object v0

    .line 867
    :try_start_1b
    invoke-interface {v0}, LiQ;->a()V
    :try_end_1e
    .catch LiF; {:try_start_1b .. :try_end_1e} :catch_2b

    .line 871
    :goto_1e
    invoke-interface {v0}, LiQ;->a()Ljava/lang/String;

    move-result-object v0

    .line 872
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()LMM;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LMM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    return-void

    .line 868
    :catch_2b
    move-exception v1

    .line 869
    const-string v1, "TabletDocListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "buildLabelAndWhereClause() failed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1e
.end method

.method private v()V
    .registers 2

    .prologue
    .line 935
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 941
    :goto_c
    return-void

    .line 939
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LajB;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 940
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Ljava/lang/String;)V

    goto :goto_c
.end method

.method private w()V
    .registers 2

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->p()V

    .line 1050
    return-void
.end method

.method private x()V
    .registers 5

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/database/ContentObserver;

    if-nez v0, :cond_2c

    .line 1065
    new-instance v0, LjL;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, LjL;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/database/ContentObserver;

    .line 1074
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/docs/providers/DocListProvider;->g:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1076
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljl;

    invoke-interface {v0}, Ljl;->a()LiQ;

    move-result-object v0

    invoke-interface {v0}, LiQ;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 1077
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->z()V

    .line 1080
    :cond_2c
    return-void
.end method

.method private y()V
    .registers 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1090
    const-string v0, "TabletDocListActivity"

    const-string v3, "in updateWorkStatus"

    invoke-static {v0, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1091
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    .line 1092
    iget-object v3, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    invoke-interface {v3, v0}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v4

    .line 1093
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LME;

    invoke-interface {v0, v4}, LME;->a(LkB;)Z

    move-result v5

    .line 1094
    invoke-virtual {v4}, LkB;->a()Z

    move-result v0

    if-nez v0, :cond_23

    if-eqz v5, :cond_74

    :cond_23
    move v0, v2

    .line 1095
    :goto_24
    iget-object v3, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LMf;

    invoke-interface {v3}, LMf;->a()LSC;

    move-result-object v3

    sget-object v6, LSC;->b:LSC;

    if-ne v3, v6, :cond_76

    move v3, v2

    .line 1096
    :goto_2f
    if-nez v0, :cond_33

    if-eqz v3, :cond_78

    :cond_33
    move v0, v2

    .line 1098
    :goto_34
    invoke-virtual {v4}, LkB;->a()Z

    move-result v3

    invoke-direct {p0, v3, v5}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(ZZ)LabA;

    move-result-object v3

    .line 1099
    iget-object v6, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(LabA;)V

    .line 1102
    iget-object v3, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/view/MenuItem;

    if-eqz v3, :cond_7c

    .line 1103
    iget-object v3, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/view/MenuItem;

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1104
    iget-object v3, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/view/MenuItem;

    if-nez v0, :cond_7a

    :goto_4e
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1111
    :goto_51
    if-eqz v5, :cond_73

    invoke-virtual {v4}, LkB;->a()Z

    move-result v0

    if-nez v0, :cond_73

    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b()Z

    move-result v0

    if-nez v0, :cond_73

    .line 1112
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljava/lang/Runnable;

    if-nez v0, :cond_73

    .line 1113
    new-instance v0, LjM;

    invoke-direct {v0, p0}, LjM;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljava/lang/Runnable;

    .line 1124
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1127
    :cond_73
    return-void

    :cond_74
    move v0, v1

    .line 1094
    goto :goto_24

    :cond_76
    move v3, v1

    .line 1095
    goto :goto_2f

    :cond_78
    move v0, v1

    .line 1096
    goto :goto_34

    :cond_7a
    move v2, v1

    .line 1104
    goto :goto_4e

    .line 1106
    :cond_7c
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/TitleBar;->setSyncing(Z)V

    goto :goto_51
.end method

.method private z()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 1140
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LMf;

    invoke-interface {v0}, LMf;->a()LSC;

    move-result-object v0

    .line 1142
    sget-object v1, LjQ;->a:[I

    invoke-virtual {v0}, LSC;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_4a

    .line 1159
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown search state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1144
    :pswitch_2b
    sget v0, Len;->search_in_progress:I

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/widget/Toast;

    .line 1146
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1162
    :goto_38
    :pswitch_38
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->y()V

    .line 1163
    return-void

    .line 1149
    :pswitch_3c
    sget v0, Len;->search_showing_local_results_only:I

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/widget/Toast;

    .line 1151
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_38

    .line 1142
    :pswitch_data_4a
    .packed-switch 0x1
        :pswitch_2b
        :pswitch_3c
        :pswitch_38
        :pswitch_38
        :pswitch_38
    .end packed-switch
.end method


# virtual methods
.method protected a()LNa;
    .registers 2

    .prologue
    .line 443
    new-instance v0, LjR;

    invoke-direct {v0, p0, p0}, LjR;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;Landroid/app/Activity;)V

    return-object v0
.end method

.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1487
    const-class v2, LUq;

    if-ne p1, v2, :cond_1a

    .line 1488
    if-nez p2, :cond_13

    :goto_8
    invoke-static {v0}, Lagu;->a(Z)V

    .line 1489
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lcom/google/android/apps/docs/fragment/PreviewFragment;

    move-result-object v0

    .line 1491
    if-nez v0, :cond_15

    const/4 v0, 0x0

    .line 1516
    :cond_12
    :goto_12
    return-object v0

    :cond_13
    move v0, v1

    .line 1488
    goto :goto_8

    .line 1491
    :cond_15
    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()LUq;

    move-result-object v0

    goto :goto_12

    .line 1493
    :cond_1a
    const-class v2, LTr;

    if-ne p1, v2, :cond_2a

    .line 1494
    if-nez p2, :cond_28

    :goto_20
    invoke-static {v0}, Lagu;->a(Z)V

    .line 1496
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lcom/google/android/apps/docs/fragment/PreviewFragment;

    move-result-object v0

    goto :goto_12

    :cond_28
    move v0, v1

    .line 1494
    goto :goto_20

    .line 1498
    :cond_2a
    const-class v0, [Lii;

    if-ne p1, v0, :cond_33

    .line 1500
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()[Lii;

    move-result-object v0

    goto :goto_12

    .line 1502
    :cond_33
    const-class v0, LoQ;

    if-ne p1, v0, :cond_3a

    .line 1504
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    goto :goto_12

    .line 1507
    :cond_3a
    if-nez p2, :cond_48

    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljava/util/Map;

    if-eqz v0, :cond_48

    .line 1508
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1509
    if-nez v0, :cond_12

    .line 1516
    :cond_48
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/app/AccountListeningActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_12
.end method

.method public a()Ljava/lang/String;
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 1533
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1534
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    .line 1554
    :cond_f
    :goto_f
    return-object v0

    .line 1538
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/os/Bundle;

    .line 1539
    if-nez v0, :cond_1c

    .line 1540
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1543
    :cond_1c
    if-eqz v0, :cond_2c

    const-string v2, "query"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 1544
    const-string v2, "app_data"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1547
    :cond_2c
    if-eqz v0, :cond_3c

    .line 1548
    const-string v2, "accountName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1551
    :goto_34
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_f

    move-object v0, v1

    .line 1554
    goto :goto_f

    :cond_3c
    move-object v0, v1

    goto :goto_34
.end method

.method public a()V
    .registers 3

    .prologue
    .line 1357
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()I

    move-result v0

    if-nez v0, :cond_13

    .line 1358
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, LiX;->a(Ljava/util/Set;)V

    .line 1360
    :cond_13
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .registers 3
    .parameter

    .prologue
    .line 1251
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->setResult(ILandroid/content/Intent;)V

    .line 1252
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->finish()V

    .line 1253
    return-void
.end method

.method public a(Landroid/view/Menu;)V
    .registers 2
    .parameter

    .prologue
    .line 751
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->d(Landroid/view/Menu;)V

    .line 752
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a(Landroid/content/Context;LkY;)Landroid/content/Intent;

    move-result-object v0

    .line 947
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->startActivity(Landroid/content/Intent;)V

    .line 948
    return-void
.end method

.method public a(Ljava/lang/String;LfS;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-interface {v0, p1, p2}, LjY;->a(Ljava/lang/String;LfS;)V

    .line 439
    return-void
.end method

.method public a(LkO;)V
    .registers 6
    .parameter

    .prologue
    .line 995
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    const-string v1, "tabletDoclist"

    const-string v2, "sendLinkEvent"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 998
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lgl;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:LKS;

    invoke-static {p1, v0, v1}, Llv;->a(LkO;Lgl;LKS;)Ljava/lang/String;

    move-result-object v0

    .line 1000
    if-eqz v0, :cond_5c

    .line 1002
    const-string v1, "TabletDocListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sending link: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1004
    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1005
    const-string v2, "android.intent.extra.SUBJECT"

    invoke-virtual {p1}, LkO;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1006
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1008
    :try_start_45
    sget v0, Len;->menu_send_link:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_52
    .catch Landroid/content/ActivityNotFoundException; {:try_start_45 .. :try_end_52} :catch_53

    .line 1016
    :goto_52
    return-void

    .line 1010
    :catch_53
    move-exception v0

    .line 1011
    const-string v1, "TabletDocListActivity"

    const-string v2, "Failed to send link"

    invoke-static {v1, v2, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_52

    .line 1014
    :cond_5c
    const-string v0, "TabletDocListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Send Link failed. HTML URL is null: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, LkO;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_52
.end method

.method public a(LmK;)V
    .registers 4
    .parameter

    .prologue
    .line 1582
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, LjY;->a(Ljava/lang/Iterable;)V

    .line 1583
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter

    .prologue
    .line 761
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .registers 6

    .prologue
    .line 422
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 423
    invoke-virtual {v0}, LkB;->c()J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_19

    .line 424
    invoke-virtual {v0}, LkB;->c()V

    .line 427
    :cond_19
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()LMM;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0}, LkB;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LMM;->a(Landroid/widget/Button;Ljava/lang/String;)V

    .line 430
    invoke-static {v0}, Lnl;->a(LkB;)Lnh;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Lnh;)V

    .line 432
    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 1257
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1258
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1259
    return-void
.end method

.method public b(Landroid/view/Menu;)V
    .registers 3
    .parameter

    .prologue
    .line 756
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;I)V

    .line 757
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1441
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Ljava/util/Set;)V

    .line 1442
    return-void
.end method

.method public b(Ljava/lang/String;LfS;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1240
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->j:Z

    if-nez v0, :cond_1d

    .line 1241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->j:Z

    .line 1242
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v0

    .line 1243
    new-instance v1, Lga;

    invoke-direct {v1, p0, v0, p2}, Lga;-><init>(Landroid/content/Context;LkY;LfS;)V

    invoke-virtual {v1}, Lga;->a()Landroid/content/Intent;

    move-result-object v0

    .line 1245
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->startActivity(Landroid/content/Intent;)V

    .line 1247
    :cond_1d
    return-void
.end method

.method public b(LkO;)V
    .registers 6
    .parameter

    .prologue
    .line 1033
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Landroid/content/Context;LkO;)Landroid/content/Intent;

    move-result-object v0

    .line 1036
    if-eqz v0, :cond_12

    .line 1038
    :try_start_6
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    const-string v2, "tabletDoclist"

    const-string v3, "printEvent"

    invoke-virtual {v1, v2, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_12
    .catch Landroid/content/ActivityNotFoundException; {:try_start_6 .. :try_end_12} :catch_13

    .line 1046
    :cond_12
    :goto_12
    return-void

    .line 1042
    :catch_13
    move-exception v0

    .line 1043
    const-string v1, "TabletDocListActivity"

    const-string v2, "Failed to print"

    invoke-static {v1, v2, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_12
.end method

.method public b_()V
    .registers 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1279
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1280
    iput-boolean v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->i:Z

    .line 1301
    :goto_a
    return-void

    .line 1283
    :cond_b
    iput-boolean v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->i:Z

    .line 1285
    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->e()Z

    move-result v3

    .line 1286
    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v2}, LiX;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_32

    move v2, v0

    .line 1287
    :goto_20
    if-eqz v2, :cond_38

    .line 1288
    if-eqz v3, :cond_34

    .line 1289
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->F()V

    .line 1298
    :cond_27
    :goto_27
    iget-object v3, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    if-nez v2, :cond_3e

    :goto_2b
    invoke-virtual {v3, v0}, Lcom/google/android/apps/docs/view/TitleBar;->setActionsVisible(Z)V

    .line 1300
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->p()V

    goto :goto_a

    :cond_32
    move v2, v1

    .line 1286
    goto :goto_20

    .line 1291
    :cond_34
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->D()V

    goto :goto_27

    .line 1293
    :cond_38
    if-eqz v3, :cond_27

    .line 1294
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->E()V

    goto :goto_27

    :cond_3e
    move v0, v1

    .line 1298
    goto :goto_2b
.end method

.method public c()V
    .registers 1

    .prologue
    .line 1587
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->t()V

    .line 1588
    return-void
.end method

.method public c(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 1263
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/app/NewMainProxyActivity;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1264
    const/high16 v0, 0x2400

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1265
    const/high16 v0, 0x1000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1269
    const-string v0, "wasTaskRoot"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1271
    const-string v0, "desiredApplicationMode"

    sget-object v1, LhA;->c:LhA;

    invoke-virtual {v1}, LhA;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1274
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->startActivity(Landroid/content/Intent;)V

    .line 1275
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 1456
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    const-string v1, "tabletDoclist"

    const-string v2, "showRenameEvent"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1458
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;

    move-result-object v0

    .line 1460
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v1

    const-string v2, "renameDialogFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 1461
    return-void
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 405
    const/4 v0, 0x1

    return v0
.end method

.method public d(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1465
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-interface {v0}, LjY;->a()V

    .line 1466
    return-void
.end method

.method public e()V
    .registers 4

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Lcom/google/android/apps/docs/app/BaseActivity;Ljava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->c:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lev;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Lfb;Ljava/lang/String;Lev;)V

    .line 397
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 1470
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-interface {v0, p1}, LjY;->e(Ljava/lang/String;)V

    .line 1471
    return-void
.end method

.method public e()Z
    .registers 2

    .prologue
    .line 795
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method public f()V
    .registers 1

    .prologue
    .line 1370
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 1528
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 1529
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 931
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-interface {v0}, LjY;->d()V

    .line 932
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 952
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 957
    :goto_c
    return-void

    .line 956
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->c(Ljava/lang/String;)V

    goto :goto_c
.end method

.method public j()V
    .registers 2

    .prologue
    .line 960
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 965
    :goto_c
    return-void

    .line 964
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Ljava/util/Set;)V

    goto :goto_c
.end method

.method public k()V
    .registers 3

    .prologue
    .line 969
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 975
    :goto_c
    return-void

    .line 974
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, LfS;->c:LfS;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Ljava/lang/String;LfS;)V

    goto :goto_c
.end method

.method public l()V
    .registers 4

    .prologue
    .line 979
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 991
    :cond_c
    :goto_c
    return-void

    .line 984
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 985
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    .line 986
    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    invoke-interface {v2, v1, v0}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    .line 988
    if-eqz v0, :cond_c

    .line 989
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(LkO;)V

    goto :goto_c
.end method

.method public m()V
    .registers 4

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1030
    :goto_c
    return-void

    .line 1025
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1026
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    .line 1027
    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    invoke-interface {v2, v1, v0}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    .line 1029
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b(LkO;)V

    goto :goto_c
.end method

.method public n()V
    .registers 4

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    .line 1054
    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v0

    .line 1057
    if-eqz v0, :cond_32

    .line 1058
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Lcom/google/android/apps/docs/view/DocListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/DocListView;->a(LkM;)V

    .line 1059
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-interface {v0}, LjY;->a()V

    .line 1061
    :cond_32
    return-void
.end method

.method public o()V
    .registers 3

    .prologue
    .line 1231
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 1232
    const-string v0, "TabletDocListActivity"

    const-string v1, "in TDLA.onContentObserverNotification"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1234
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-interface {v0}, LjY;->a()V

    .line 1235
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->y()V

    .line 1236
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 919
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 920
    packed-switch p1, :pswitch_data_12

    .line 925
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/AccountListeningActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 927
    :goto_b
    return-void

    .line 922
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-interface {v0, p3}, LjY;->a(Landroid/content/Intent;)V

    goto :goto_b

    .line 920
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_c
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 6
    .parameter

    .prologue
    .line 410
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/AccountListeningActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 411
    const-string v0, "TabletDocListActivity"

    const-string v1, "in onConfigurationChanged"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    const-string v1, "tabletDoclist"

    const-string v2, "configChangedEvent"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 19
    .parameter

    .prologue
    .line 304
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/os/Bundle;

    .line 305
    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/docs/app/AccountListeningActivity;->onCreate(Landroid/os/Bundle;)V

    .line 306
    const-string v2, "TabletDocListActivity"

    const-string v3, "in onCreate"

    invoke-static {v2, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    new-instance v2, Ljn;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljo;

    invoke-direct {v2, v3}, Ljn;-><init>(Ljo;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljl;

    .line 310
    new-instance v2, Lkb;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LME;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljl;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljc;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LMf;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiG;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiA;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->c:Laoz;

    move-object/from16 v9, p0

    move-object/from16 v16, p0

    invoke-direct/range {v2 .. v16}, Lkb;-><init>(Llf;LME;Liv;LiX;Ljl;Ljc;LjZ;LMf;LiG;Ljo;LiA;LeQ;Laoz;Lka;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    .line 325
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->H()V

    .line 327
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v3

    .line 328
    sget v2, Lej;->tablet_doclist:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->setContentView(I)V

    .line 329
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lcom/google/android/apps/docs/fragment/PreviewFragment;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    .line 330
    sget v2, Leh;->doc_list_fragment:I

    invoke-virtual {v3, v2}, Lo;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/docs/fragment/DocListFragment;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    .line 331
    sget v2, Leh;->navigation_fragment:I

    invoke-virtual {v3, v2}, Lo;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/docs/fragment/NavigationFragment;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/NavigationFragment;

    .line 333
    sget v2, Leh;->title_bar:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/docs/view/TitleBar;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/view/TitleBar;->getVisibility()I

    .line 335
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()LMM;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/view/TitleBar;

    invoke-interface {v2, v3}, LMM;->a(LMZ;)V

    .line 337
    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(II)V

    .line 340
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 341
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 343
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, Liv;->a(Liw;)V

    .line 344
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, LiX;->a(LiY;)V

    .line 345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiA;

    move-object/from16 v0, p0

    invoke-interface {v2, v0}, LiA;->a(Liz;)V

    .line 346
    if-eqz p1, :cond_e9

    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lji;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Lji;->b(Landroid/os/Bundle;)V

    .line 354
    :cond_e9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LME;

    invoke-interface {v2}, LME;->a()[Landroid/accounts/Account;

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_13a

    .line 355
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->finish()V

    .line 360
    :goto_f7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->f(Ljava/lang/String;)V

    .line 361
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a(Lo;)V

    .line 363
    new-instance v2, LjC;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/os/Handler;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, LjC;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;Landroid/os/Handler;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/database/ContentObserver;

    .line 376
    new-instance v2, LjO;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, LjO;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LWQ;

    .line 388
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    invoke-virtual {v2}, LeQ;->a()V

    .line 389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    const-string v3, "/tabletDoclist"

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 390
    return-void

    .line 357
    :cond_13a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LjY;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-interface {v2, v0, v3}, LjY;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    goto :goto_f7
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .parameter

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 465
    iput-object p1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/view/Menu;

    .line 466
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:I

    .line 467
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->c(Landroid/view/Menu;)V

    .line 468
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:I

    .line 469
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->d(Landroid/view/Menu;)V

    .line 470
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 912
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 913
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 914
    invoke-super {p0}, Lcom/google/android/apps/docs/app/AccountListeningActivity;->onDestroy()V

    .line 915
    return-void
.end method

.method protected onPause()V
    .registers 3

    .prologue
    .line 878
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 879
    const-string v0, "TabletDocListActivity"

    const-string v1, "in onPause"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LWQ;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncReceiver;->b(LWQ;)V

    .line 882
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 883
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->A()V

    .line 885
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lo;->b(Lq;)V

    .line 887
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/widget/Toast;

    if-eqz v0, :cond_30

    .line 888
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 889
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/widget/Toast;

    .line 891
    :cond_30
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LZL;->f(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_45

    const-string v0, "tabletDoclistLandscapeDuration"

    .line 894
    :goto_3c
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    invoke-virtual {v1, p0, v0}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 895
    invoke-super {p0}, Lcom/google/android/apps/docs/app/AccountListeningActivity;->onPause()V

    .line 896
    return-void

    .line 891
    :cond_45
    const-string v0, "tabletDoclistPortraitDuration"

    goto :goto_3c
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 699
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 700
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 701
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;Z)V

    .line 702
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b(Landroid/view/Menu;Z)V

    .line 707
    :goto_19
    return v2

    .line 704
    :cond_1a
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;Z)V

    .line 705
    iget v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(Landroid/view/Menu;I)V

    goto :goto_19
.end method

.method protected onResume()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 800
    invoke-super {p0}, Lcom/google/android/apps/docs/app/AccountListeningActivity;->onResume()V

    .line 801
    const-string v0, "TabletDocListActivity"

    const-string v1, "in onResume"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    iput-boolean v3, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->j:Z

    .line 805
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->x()V

    .line 807
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()LMM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v4, v1}, LMM;->a(Landroid/widget/Button;Ljava/lang/String;)V

    .line 808
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()LMM;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljl;

    invoke-interface {v1}, Ljl;->a()LiQ;

    move-result-object v1

    invoke-interface {v1}, LiQ;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LMM;->a(Ljava/lang/String;)V

    .line 810
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->y()V

    .line 812
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LME;

    invoke-interface {v0}, LME;->a()[Landroid/accounts/Account;

    move-result-object v0

    .line 814
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()LMM;

    move-result-object v1

    new-instance v2, LjJ;

    invoke-direct {v2, p0}, LjJ;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-interface {v1, v4, v0, v2}, LMM;->a(Landroid/widget/Button;[Landroid/accounts/Account;LMN;)V

    .line 825
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/docs/providers/DocListProvider;->i:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 827
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LWQ;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncReceiver;->a(LWQ;)V

    .line 829
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lo;->a(Lq;)V

    .line 831
    iget-boolean v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->i:Z

    if-eqz v0, :cond_69

    .line 835
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b:Landroid/os/Handler;

    new-instance v1, LjK;

    invoke-direct {v1, p0}, LjK;-><init>(Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 843
    :cond_69
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->t()V

    .line 845
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LeQ;

    invoke-virtual {v0, p0}, LeQ;->a(Ljava/lang/Object;)V

    .line 846
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 900
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/AccountListeningActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 901
    const-string v0, "TabletDocListActivity"

    const-string v1, "in onSaveInstanceState"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0, p1}, Liv;->b(Landroid/os/Bundle;)V

    .line 904
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0, p1}, LiX;->b(Landroid/os/Bundle;)V

    .line 905
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljo;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljl;

    invoke-interface {v1}, Ljl;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljo;->a(Landroid/os/Bundle;Ljava/util/List;)V

    .line 906
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lji;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lcom/google/android/apps/docs/fragment/DocListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lji;->a(I)V

    .line 907
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Lji;

    invoke-interface {v0, p1}, Lji;->a(Landroid/os/Bundle;)V

    .line 908
    return-void
.end method

.method public onSearchRequested()Z
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 767
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 772
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    invoke-static {v0}, Lnl;->a(LkB;)Lnh;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a(Lnh;)V

    .line 774
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/BaseSearchSuggestionProvider;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 776
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Liv;

    invoke-interface {v1, v0}, Liv;->b(Landroid/os/Bundle;)V

    .line 778
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2, v0, v2}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->startSearch(Ljava/lang/String;ZLandroid/os/Bundle;Z)V

    .line 779
    const/4 v0, 0x1

    return v0
.end method

.method public p()V
    .registers 3

    .prologue
    .line 1309
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/view/Menu;

    if-eqz v0, :cond_1c

    .line 1310
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    const/4 v0, 0x1

    .line 1311
    :goto_11
    iget-object v1, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljx;

    if-eqz v1, :cond_1c

    if-eqz v0, :cond_1c

    .line 1312
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Ljx;

    invoke-interface {v0}, Ljx;->c()V

    .line 1315
    :cond_1c
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_28

    .line 1316
    invoke-super {p0}, Lcom/google/android/apps/docs/app/AccountListeningActivity;->invalidateOptionsMenu()V

    .line 1320
    :cond_25
    :goto_25
    return-void

    .line 1310
    :cond_26
    const/4 v0, 0x0

    goto :goto_11

    .line 1317
    :cond_28
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/view/Menu;

    if-eqz v0, :cond_25

    .line 1318
    iget-object v0, p0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a:Landroid/view/Menu;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    goto :goto_25
.end method

.method public q()V
    .registers 2

    .prologue
    .line 1349
    invoke-virtual {p0}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()V

    .line 1350
    return-void
.end method
