.class public Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "CooperateStateMachineProgressFragment.java"

# interfaces
.implements LaaS;


# instance fields
.field private a:LdG;

.field private a:LkM;

.field private a:Lnm;

.field private a:Lny;

.field private m:I

.field private w:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 46
    return-void
.end method

.method public constructor <init>(Lnm;LkM;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 49
    if-eqz p1, :cond_13

    const/4 v0, 0x1

    :goto_6
    invoke-static {v0}, Lagu;->a(Z)V

    .line 50
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lnm;

    .line 51
    invoke-interface {p1, p0}, Lnm;->a(LaaS;)V

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LkM;

    .line 53
    iput p3, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->m:I

    .line 54
    return-void

    .line 49
    :cond_13
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;)Lnm;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lnm;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->w:Z

    return p1
.end method

.method private p()V
    .registers 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LdG;

    if-eqz v0, :cond_c

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LdG;

    invoke-virtual {v0}, LdG;->a()V

    .line 144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LdG;

    .line 146
    :cond_c
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 59
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()Laoo;

    move-result-object v1

    invoke-static {v1, v0}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 61
    new-instance v1, Lny;

    iget v2, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->m:I

    invoke-direct {v1, v0, v2}, Lny;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lny;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LkM;

    invoke-virtual {v0}, LkM;->a()LkP;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, LkP;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LkM;

    invoke-virtual {v1}, LkM;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LkM;

    invoke-virtual {v2}, LkM;->d()Z

    move-result v2

    invoke-static {v0, v1, v2}, LkO;->b(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lny;

    invoke-virtual {v1, v0}, Lny;->setIcon(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lny;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lnm;

    invoke-interface {v1}, Lnm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lny;->setTitle(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lny;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lny;->setCancelable(Z)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lny;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lny;->setCanceledOnTouchOutside(Z)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lny;

    return-object v0
.end method

.method public a(JJLjava/lang/String;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Lny;

    move-wide v1, p1

    move-wide v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lny;->b(JJLjava/lang/String;)V

    .line 78
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 82
    const-string v0, "CooperateStateMachineProgressFragment"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(Landroid/os/Bundle;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:Lnm;

    if-nez v0, :cond_12

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()V

    .line 115
    :goto_11
    return-void

    .line 90
    :cond_12
    new-instance v0, LmF;

    invoke-direct {v0, p0}, LmF;-><init>(Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LdG;

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a:LdG;

    invoke-virtual {v0}, LdG;->start()V

    goto :goto_11
.end method

.method public g()V
    .registers 2

    .prologue
    .line 119
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->g()V

    .line 121
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->w:Z

    if-eqz v0, :cond_a

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()V

    .line 124
    :cond_a
    return-void
.end method

.method public i()V
    .registers 3

    .prologue
    .line 128
    const-string v0, "CooperateStateMachineProgressFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->p()V

    .line 130
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->i()V

    .line 131
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->p()V

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/CooperateStateMachineProgressFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 139
    return-void
.end method
