.class public Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;
.super Ljava/lang/Object;
.source "CreateEntryDialogFragment.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lnw;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final a:LkP;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 149
    new-instance v0, Lnx;

    invoke-direct {v0}, Lnx;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LkP;IIIII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->a:LkP;

    .line 103
    iput p2, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->b:I

    .line 104
    iput p3, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->a:I

    .line 105
    iput p4, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->c:I

    .line 106
    iput p5, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->d:I

    .line 107
    iput p6, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->e:I

    .line 108
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 117
    iget v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->b:I

    return v0
.end method

.method public a(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 112
    invoke-virtual {p1, p0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;)V

    .line 113
    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->a:LkP;

    invoke-virtual {v0}, LkP;->a()I

    move-result v0

    return v0
.end method

.method public describeContents()I
    .registers 2

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->a:LkP;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 138
    iget v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    iget v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 140
    iget v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    iget v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 142
    iget v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 143
    return-void
.end method
