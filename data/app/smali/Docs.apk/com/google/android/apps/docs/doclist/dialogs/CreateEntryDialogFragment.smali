.class public Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "CreateEntryDialogFragment.java"


# instance fields
.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnw;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private w:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->w:Z

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 54
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;-><init>()V

    .line 55
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 60
    invoke-static {v1, p0}, LnL;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 61
    const-string v2, "collectionResourceId"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->d(Landroid/os/Bundle;)V

    .line 63
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->c:Ljava/lang/String;

    return-object v0
.end method

.method private a()Ljava/util/List;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lnw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 288
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    sget-object v1, LkP;->h:LkP;

    sget v2, Len;->create_new_folder:I

    sget v3, Len;->default_new_folder_title:I

    sget v4, Len;->new_folder_title:I

    sget v5, Len;->creating_folder:I

    sget v6, Len;->create_new_error_folder:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;-><init>(LkP;IIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    sget-object v1, LkP;->a:LkP;

    sget v2, Len;->create_new_kix_doc:I

    sget v3, Len;->default_new_kix_title:I

    sget v4, Len;->new_kix_title:I

    sget v5, Len;->creating_document:I

    sget v6, Len;->create_new_error_document:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;-><init>(LkP;IIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    sget-object v1, LkP;->d:LkP;

    sget v2, Len;->create_new_trix_doc:I

    sget v3, Len;->default_new_trix_title:I

    sget v4, Len;->new_trix_title:I

    sget v5, Len;->creating_spreadsheet:I

    sget v6, Len;->create_new_error_spreadsheet:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;-><init>(LkP;IIIII)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()Lo;

    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 308
    new-instance v1, Lnv;

    invoke-direct {v1, p0, v0}, Lnv;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;Landroid/support/v4/app/FragmentActivity;)V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    return-object v7
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->p()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method private p()V
    .registers 2

    .prologue
    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->f()V

    .line 280
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 10
    .parameter

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a:LdL;

    invoke-interface {v0, v2}, LdL;->a(Landroid/content/Context;)V

    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()Landroid/os/Bundle;

    .line 202
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a:Ljava/util/List;

    .line 206
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1f
    :goto_1f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnw;

    .line 209
    invoke-interface {v0, v2}, Lnw;->a(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1f

    .line 210
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1f

    .line 214
    :cond_35
    invoke-static {v2}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    .line 215
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 216
    sget v0, Len;->create_new_doc_title:I

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 218
    const-string v0, "layout_inflater"

    invoke-virtual {v2, v0}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 221
    sget v0, Lej;->create_entry_dialog_list:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ListView;

    .line 223
    new-instance v0, Lns;

    sget v3, Lej;->create_entry_dialog_row:I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lns;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;Landroid/content/Context;ILjava/util/List;Landroid/view/LayoutInflater;)V

    .line 236
    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 238
    invoke-virtual {v7, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 239
    const/high16 v0, 0x104

    new-instance v1, Lnt;

    invoke-direct {v1, p0, v2}, Lnt;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;Lcom/google/android/apps/docs/app/CreateNewDocActivity;)V

    invoke-virtual {v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 246
    sget-object v0, LnO;->a:LnO;

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 247
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 249
    new-instance v1, Lnu;

    invoke-direct {v1, p0, v2, v4, v0}, Lnu;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;Lcom/google/android/apps/docs/app/CreateNewDocActivity;Ljava/util/List;Landroid/app/AlertDialog;)V

    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 261
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 189
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(Landroid/os/Bundle;)V

    .line 190
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 191
    invoke-static {v0}, LnL;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->c:Ljava/lang/String;

    .line 192
    const-string v1, "collectionResourceId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->d:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 268
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()Lo;

    move-result-object v0

    .line 269
    iget-boolean v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->w:Z

    if-nez v1, :cond_1e

    if-eqz v0, :cond_1e

    const-string v1, "editTitleDialog"

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_1e

    .line 272
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->p()V

    .line 275
    :cond_1e
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 276
    return-void
.end method
