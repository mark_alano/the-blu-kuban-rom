.class public Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "DocumentOpenerErrorDialogFragment.java"


# instance fields
.field public a:LUL;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LfS;

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LnS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private w:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 50
    return-void
.end method

.method public static a(Lo;Ljava/lang/String;Ljava/lang/String;LfS;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 165
    invoke-static {p5}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    const-string v0, "DocumentOpenerErrorDialogFragment"

    invoke-virtual {p0, v0}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 167
    if-eqz v0, :cond_10

    .line 168
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 171
    :cond_10
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;-><init>()V

    .line 173
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 174
    const-string v2, "errorTitle"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    const-string v2, "errorHtml"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v2, "canRetry"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 177
    const-string v2, "accountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-string v2, "resourceId"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-static {v1, p3}, Lpb;->a(Landroid/os/Bundle;LfS;)V

    .line 180
    invoke-virtual {v0, v1}, Landroid/support/v4/app/DialogFragment;->d(Landroid/os/Bundle;)V

    .line 181
    const-string v1, "DocumentOpenerErrorDialogFragment"

    invoke-virtual {v0, p0, v1}, Landroid/support/v4/app/DialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method public static a(Lo;LkY;LfS;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 158
    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    iget-object v2, p1, LkY;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(Lo;Ljava/lang/String;Ljava/lang/String;LfS;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 160
    return-void
.end method

.method public static a(Lo;)Z
    .registers 2
    .parameter

    .prologue
    .line 185
    const-string v0, "DocumentOpenerErrorDialogFragment"

    invoke-virtual {p0, v0}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_10

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 7
    .parameter

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a()Laoo;

    move-result-object v0

    invoke-static {v0, v1}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 104
    invoke-static {v1}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->b:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    invoke-virtual {v0}, Lfb;->a()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 108
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 109
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->w:Z

    if-eqz v0, :cond_3a

    .line 110
    sget v0, Len;->button_retry:I

    new-instance v3, LnP;

    invoke-direct {v3, p0, v1}, LnP;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;Landroid/content/Context;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 118
    :cond_3a
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->e:Ljava/lang/String;

    if-eqz v0, :cond_78

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->f:Ljava/lang/String;

    if-eqz v0, :cond_78

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Llf;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->e:Ljava/lang/String;

    invoke-interface {v0, v3}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 120
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:Llf;

    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->f:Ljava/lang/String;

    invoke-interface {v3, v0, v4}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_78

    .line 122
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LfS;

    invoke-virtual {v0}, LkM;->a()LkP;

    move-result-object v4

    invoke-virtual {v3, v4}, LfS;->a(LkP;)LUK;

    move-result-object v3

    .line 123
    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LUL;

    invoke-interface {v4, v0, v3}, LUL;->c(LkM;LUK;)Z

    move-result v0

    if-eqz v0, :cond_78

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->f:Ljava/lang/String;

    invoke-static {v0, v3}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v0

    .line 125
    sget v3, Len;->open_pinned_version:I

    new-instance v4, LnQ;

    invoke-direct {v4, p0, v1, v0}, LnQ;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;Landroid/content/Context;LkY;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 137
    :cond_78
    const/high16 v0, 0x104

    new-instance v3, LnR;

    invoke-direct {v3, p0, v1}, LnR;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;Landroid/content/Context;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 145
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v1

    .line 88
    const-string v0, "errorTitle"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->c:Ljava/lang/String;

    .line 89
    const-string v0, "errorHtml"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->d:Ljava/lang/String;

    .line 90
    const-string v0, "canRetry"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->w:Z

    .line 91
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->w:Z

    if-eqz v0, :cond_35

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LnS;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    :cond_35
    const-string v0, "accountName"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->e:Ljava/lang/String;

    .line 95
    const-string v0, "resourceId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->f:Ljava/lang/String;

    .line 96
    invoke-static {v1}, Lpb;->a(Landroid/os/Bundle;)LfS;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a:LfS;

    .line 97
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a()Laoo;

    move-result-object v1

    invoke-static {v1, v0}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 152
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 153
    return-void
.end method
