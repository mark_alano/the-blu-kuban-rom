.class public Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "EditTitleDialogFragment.java"


# instance fields
.field private a:Landroid/widget/EditText;

.field private a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

.field private a:LnN;

.field private w:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->w:Z

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)Landroid/widget/EditText;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;)Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;
    .registers 4
    .parameter

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;-><init>()V

    .line 46
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 47
    const-string v2, "progressTextId"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 48
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->d(Landroid/os/Bundle;)V

    .line 49
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)LnN;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LnN;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->w:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LdL;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    .line 67
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LdL;

    invoke-interface {v1, v0}, LdL;->a(Landroid/content/Context;)V

    .line 69
    new-instance v1, LnT;

    invoke-direct {v1, p0, v0}, LnT;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;Lcom/google/android/apps/docs/app/CreateNewDocActivity;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LnN;

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, LnL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    .line 84
    new-instance v2, Landroid/widget/EditText;

    invoke-direct {v2, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    iget v1, v1, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->a:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 86
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 88
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->setSingleLine()V

    .line 89
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    const/16 v2, 0x4001

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 90
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    new-instance v2, LnU;

    invoke-direct {v2, p0}, LnU;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 113
    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 114
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    iget v2, v2, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->c:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 115
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 116
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 117
    const v2, 0x104000a

    new-instance v3, LnV;

    invoke-direct {v3, p0, v0}, LnV;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;Lcom/google/android/apps/docs/app/CreateNewDocActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 127
    const/high16 v2, 0x104

    new-instance v3, LnW;

    invoke-direct {v3, p0, v0}, LnW;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;Lcom/google/android/apps/docs/app/CreateNewDocActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 136
    sget-object v2, LnO;->a:LnO;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 137
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 140
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    new-instance v3, LnX;

    invoke-direct {v3, p0, v0, v1}, LnX;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;Lcom/google/android/apps/docs/app/CreateNewDocActivity;Landroid/app/Dialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 151
    return-object v1
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    const-string v1, "progressTextId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    .line 62
    return-void
.end method

.method public g()V
    .registers 3

    .prologue
    .line 165
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->g()V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 167
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->w:Z

    .line 168
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    iget v1, v1, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 170
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 157
    iget-boolean v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->w:Z

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LnN;

    if-eqz v0, :cond_16

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a:LnN;

    invoke-interface {v0}, LnN;->a()V

    .line 160
    :cond_16
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 161
    return-void
.end method
