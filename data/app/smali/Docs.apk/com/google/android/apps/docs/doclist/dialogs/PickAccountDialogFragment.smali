.class public Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "PickAccountDialogFragment.java"


# instance fields
.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Low;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 39
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static a(Lo;)V
    .registers 3
    .parameter

    .prologue
    .line 118
    const-string v0, "PickAccountDialogFragment"

    invoke-virtual {p0, v0}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 119
    if-nez v0, :cond_f

    .line 120
    new-instance v0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;-><init>()V

    .line 123
    :cond_f
    const-string v1, "PickAccountDialogFragment"

    invoke-virtual {v0, p0, v1}, Landroid/support/v4/app/DialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method private static a([Landroid/accounts/Account;)[Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 107
    array-length v1, p0

    new-array v2, v1, [Ljava/lang/String;

    .line 109
    array-length v3, p0

    move v1, v0

    :goto_6
    if-ge v0, v3, :cond_13

    aget-object v4, p0, v0

    .line 110
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v4, v2, v1

    .line 111
    add-int/lit8 v1, v1, 0x1

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 114
    :cond_13
    return-object v2
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 7
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LME;

    invoke-interface {v0}, LME;->a()[Landroid/accounts/Account;

    move-result-object v0

    .line 84
    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a([Landroid/accounts/Account;)[Ljava/lang/String;

    move-result-object v1

    .line 86
    new-instance v2, Lov;

    invoke-direct {v2, p0, v0}, Lov;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;[Landroid/accounts/Account;)V

    .line 95
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LME;

    invoke-interface {v3}, LME;->a()Ljava/lang/String;

    move-result-object v3

    .line 96
    invoke-static {v0, v3}, LMG;->a([Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    .line 98
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    sget v4, Len;->select_account:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 103
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 65
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(Landroid/os/Bundle;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LME;

    invoke-interface {v0}, LME;->a()[Landroid/accounts/Account;

    move-result-object v0

    .line 68
    array-length v1, v0

    .line 69
    if-nez v1, :cond_23

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LZM;

    sget v1, Len;->google_account_needed_all_apps:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Low;

    invoke-interface {v0}, Low;->l_()V

    .line 72
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->b(Z)V

    .line 79
    :goto_22
    return-void

    .line 73
    :cond_23
    if-ne v1, v2, :cond_30

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Low;

    aget-object v0, v0, v3

    invoke-interface {v1, v0}, Low;->a(Landroid/accounts/Account;)V

    .line 75
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->b(Z)V

    goto :goto_22

    .line 77
    :cond_30
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->b(Z)V

    goto :goto_22
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a:Low;

    invoke-interface {v0}, Low;->l_()V

    .line 130
    return-void
.end method
