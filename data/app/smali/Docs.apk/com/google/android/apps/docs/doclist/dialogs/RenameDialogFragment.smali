.class public Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;
.super Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;
.source "RenameDialogFragment.java"


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 48
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 49
    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v1, "resId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    new-instance v1, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;-><init>()V

    .line 52
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->d(Landroid/os/Bundle;)V

    .line 53
    return-object v1
.end method

.method private a()LkB;
    .registers 5

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 71
    if-nez v0, :cond_27

    .line 72
    const-string v1, "RenameDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to load account: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()V

    .line 75
    :cond_27
    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Laoo;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v0, v1}, LdY;->a(Laoo;Landroid/content/Context;)V

    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()LkB;

    move-result-object v0

    .line 85
    if-nez v0, :cond_14

    move-object v0, v2

    .line 121
    :goto_13
    return-object v0

    .line 88
    :cond_14
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:Llf;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->d:Ljava/lang/String;

    invoke-interface {v1, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v4

    .line 89
    if-nez v4, :cond_20

    move-object v0, v2

    .line 90
    goto :goto_13

    .line 93
    :cond_20
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v3

    .line 95
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(Landroid/app/Dialog;)Landroid/view/View;

    move-result-object v0

    sget v1, Leh;->new_name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 96
    const/4 v1, 0x0

    invoke-virtual {p0, v3, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 97
    invoke-virtual {v4}, LkO;->o()Z

    move-result v1

    if-eqz v1, :cond_5f

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:Lfg;

    sget v5, Len;->rename_collection:I

    invoke-virtual {v1, v5}, Lfg;->a(I)I

    move-result v1

    .line 100
    :goto_42
    invoke-virtual {v3, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 102
    if-eqz p1, :cond_62

    const-string v1, "newName"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 103
    :goto_4d
    if-eqz v1, :cond_64

    .line 104
    :goto_4f
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 105
    invoke-static {v0, v3}, Labr;->a(Landroid/widget/EditText;Landroid/app/Dialog;)V

    .line 108
    new-instance v1, LoO;

    invoke-direct {v1, p0}, LoO;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    move-object v0, v3

    .line 121
    goto :goto_13

    .line 97
    :cond_5f
    sget v1, Len;->rename_document:I

    goto :goto_42

    :cond_62
    move-object v1, v2

    .line 102
    goto :goto_4d

    .line 103
    :cond_64
    invoke-virtual {v4}, LkO;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_4f
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 58
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->a(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->c:Ljava/lang/String;

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "resId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->d:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 126
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;->c(Landroid/os/Bundle;)V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    sget v1, Leh;->new_name:I

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 129
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    const-string v1, "newName"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public p()V
    .registers 7

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()LkB;

    move-result-object v4

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(Landroid/app/Dialog;)Landroid/view/View;

    move-result-object v0

    sget v1, Leh;->new_name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 137
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->d:Ljava/lang/String;

    invoke-interface {v1, v4, v2}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v2

    .line 139
    if-nez v2, :cond_53

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Len;->error_document_not_available:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a(Landroid/app/Dialog;ILjava/lang/String;)V

    .line 142
    const-string v0, "RenameDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Trying to rename a null Entry with resourceId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :goto_52
    return-void

    .line 144
    :cond_53
    new-instance v1, LlA;

    invoke-direct {v1, v2, v0}, LlA;-><init>(LkO;Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a:LlE;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/dialogs/RenameDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v5, Lot;

    invoke-direct {v5, p0}, Lot;-><init>(Lcom/google/android/apps/docs/doclist/dialogs/OperationDialogFragment;)V

    invoke-static/range {v0 .. v5}, LlN;->a(LlE;LlB;LkO;Landroid/content/Context;LkB;LlK;)I

    goto :goto_52
.end method
