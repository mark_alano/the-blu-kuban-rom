.class public Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;
.super Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;
.source "LocalFileIntentOpener.java"


# instance fields
.field private final a:LUL;

.field private final a:LZS;

.field private final a:LaaJ;

.field private final a:Landroid/content/Context;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

.field private final a:LeQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;LUL;LZS;LeQ;LaaJ;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/documentopener/AbstractImmediateDocumentOpener;-><init>()V

    .line 104
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LUL;

    .line 105
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LZS;

    .line 106
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Landroid/content/Context;

    .line 107
    iput-object p4, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LeQ;

    .line 108
    iput-object p5, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LaaJ;

    .line 109
    iput-object p6, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    .line 110
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Landroid/content/Context;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)LeQ;
    .registers 2
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LeQ;

    return-object v0
.end method

.method private a(LUM;LVb;LaaS;)Ljava/io/File;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    invoke-interface {p1}, LUM;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 235
    const-string v0, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5b

    move v0, v1

    :goto_13
    invoke-static {v0}, Lagu;->a(Z)V

    .line 236
    invoke-static {}, LVp;->a()LVo;

    move-result-object v0

    invoke-interface {v0, v3}, LVo;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    .line 237
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 238
    if-eqz p3, :cond_62

    .line 239
    invoke-interface {p1}, LUM;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Landroid/content/Context;

    sget v7, Len;->decrypting_progress_message:I

    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5, v6}, LZU;->a(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 242
    new-instance v1, LpV;

    invoke-direct {v1, p0, p3, v0}, LpV;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;LaaS;Ljava/lang/String;)V

    .line 248
    new-instance v0, LYu;

    invoke-direct {v0, v3, v1, v5, v6}, LYu;-><init>(Ljava/io/OutputStream;LXM;J)V

    move-object v1, v0

    .line 252
    :goto_4c
    :try_start_4c
    invoke-interface {p1}, LUM;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0, v1}, LVb;->a(Ljava/lang/String;Ljava/io/OutputStream;)V
    :try_end_57
    .catchall {:try_start_4c .. :try_end_57} :catchall_5d

    .line 254
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 256
    return-object v4

    :cond_5b
    move v0, v2

    .line 235
    goto :goto_13

    .line 254
    :catchall_5d
    move-exception v0

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    throw v0

    :cond_62
    move-object v1, v3

    goto :goto_4c
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;Lpa;LUM;Landroid/os/Bundle;Ljava/lang/String;LaaS;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(Lpa;LUM;Landroid/os/Bundle;Ljava/lang/String;LaaS;)V

    return-void
.end method

.method private a(Lpa;LUM;Landroid/os/Bundle;Ljava/lang/String;LaaS;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 147
    invoke-interface {p2}, LUM;->a()Ljavax/crypto/SecretKey;

    move-result-object v0

    .line 150
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 151
    if-nez v0, :cond_60

    .line 152
    invoke-interface {p2}, LUM;->a()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 179
    :goto_14
    invoke-static {p3}, Lpb;->a(Landroid/os/Bundle;)LfS;

    move-result-object v3

    .line 182
    const-string v0, "uriIntentBuilder"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;

    .line 185
    if-nez v0, :cond_97

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    invoke-interface {p2}, LUM;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4, p4, v1}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;->a(LfS;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 191
    :goto_2c
    if-nez v0, :cond_a0

    .line 192
    sget-object v0, Lpc;->c:Lpc;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    .line 194
    const-string v0, "LocalFileIntentOpener"

    const-string v1, "No installed package can handle file \"%s\" with mime-type \"%s\""

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p4, v3, v4

    const/4 v4, 0x1

    invoke-interface {p2}, LUM;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_50
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 197
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_50

    .line 154
    :cond_60
    new-instance v1, LUP;

    invoke-interface {p2}, LUM;->a()Ljava/io/File;

    move-result-object v3

    invoke-interface {p2}, LUM;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LZS;

    invoke-direct {v1, v0, v3, v4, v5}, LUP;-><init>(Ljavax/crypto/SecretKey;Ljava/io/File;Ljava/lang/String;LZS;)V

    .line 157
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a()Z

    move-result v0

    if-eqz v0, :cond_80

    .line 161
    new-instance v0, LVj;

    invoke-direct {v0, v1}, LVj;-><init>(LVb;)V

    .line 162
    invoke-static {v0, p4}, Lcom/google/android/apps/docs/sync/filemanager/FileProvider;->a(LUU;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 163
    goto :goto_14

    .line 168
    :cond_80
    :try_start_80
    invoke-direct {p0, p2, v1, p5}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(LUM;LVb;LaaS;)Ljava/io/File;

    move-result-object v1

    .line 169
    invoke-virtual {v1}, Ljava/io/File;->deleteOnExit()V
    :try_end_87
    .catch Ljava/lang/Exception; {:try_start_80 .. :try_end_87} :catch_90

    .line 174
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 175
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_14

    .line 170
    :catch_90
    move-exception v0

    .line 171
    sget-object v1, Lpc;->h:Lpc;

    invoke-interface {p1, v1, v0}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    .line 229
    :goto_96
    return-void

    .line 189
    :cond_97
    invoke-interface {v0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator$UriIntentBuilder;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_2c

    .line 199
    :cond_9c
    invoke-interface {p2}, LUM;->b()V

    goto :goto_96

    .line 203
    :cond_a0
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LeQ;

    invoke-virtual {v1, p0}, LeQ;->a(Ljava/lang/Object;)V

    .line 205
    :try_start_a5
    new-instance v1, LpU;

    invoke-direct {v1, p0, v2, p2}, LpU;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;Ljava/util/List;LUM;)V

    invoke-interface {p1, v0, v1}, Lpa;->a(Landroid/content/Intent;Ljava/lang/Runnable;)V
    :try_end_ad
    .catch Landroid/content/ActivityNotFoundException; {:try_start_a5 .. :try_end_ad} :catch_ae

    goto :goto_96

    .line 225
    :catch_ae
    move-exception v0

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LeQ;

    invoke-virtual {v1, p0}, LeQ;->b(Ljava/lang/Object;)V

    .line 227
    sget-object v1, Lpc;->c:Lpc;

    invoke-interface {p1, v1, v0}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_96
.end method

.method private a()Z
    .registers 2

    .prologue
    .line 133
    invoke-static {}, LVj;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LaaJ;

    invoke-interface {v0}, LaaJ;->c()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method private a(LUM;)Z
    .registers 6
    .parameter

    .prologue
    .line 141
    .line 142
    invoke-interface {p1}, LUM;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const/high16 v2, 0x20

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)Z
    .registers 2
    .parameter

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a()Z

    move-result v0

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;LUM;)Z
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(LUM;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Lpa;LkM;Landroid/os/Bundle;)Lnm;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 115
    invoke-static {p3}, Lpb;->a(Landroid/os/Bundle;)LfS;

    move-result-object v0

    .line 116
    invoke-virtual {p2}, LkM;->a()LkP;

    move-result-object v1

    invoke-virtual {v0, v1}, LfS;->a(LkP;)LUK;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LUL;

    invoke-interface {v1, p2, v0}, LUL;->c(LkM;LUK;)Z

    move-result v1

    if-eqz v1, :cond_31

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a:LUL;

    invoke-interface {v1, p2, v0}, LUL;->a(LkM;LUK;)LUM;

    move-result-object v3

    .line 121
    if-eqz v3, :cond_29

    .line 122
    new-instance v0, LpW;

    invoke-virtual {p2}, LkM;->c()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    move-object v2, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LpW;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;Lpa;LUM;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 129
    :goto_28
    return-object v0

    .line 125
    :cond_29
    const-wide/16 v1, -0x1

    invoke-virtual {p2, v1, v2, v0}, LkM;->a(JLUK;)V

    .line 126
    invoke-virtual {p2}, LkM;->c()V

    .line 129
    :cond_31
    const/4 v0, 0x0

    goto :goto_28
.end method
