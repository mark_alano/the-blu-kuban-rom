.class public Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "WebViewOpenActivity.java"

# interfaces
.implements LnS;


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LNS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LNe;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LPm;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Landroid/webkit/WebChromeClient;

.field private a:Landroid/webkit/WebSettings;

.field private a:Landroid/webkit/WebView;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Ljava/lang/Class;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "DocListActivity"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private a:Lnm;

.field private a:Lqc;

.field private final a:Lqd;

.field private a:Lqf;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 115
    new-instance v0, Lqk;

    invoke-direct {v0, p0}, Lqk;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqd;

    .line 161
    new-instance v0, Lql;

    invoke-direct {v0, p0}, Lql;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebChromeClient;

    .line 229
    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lnm;

    .line 233
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->b:Landroid/os/Handler;

    .line 234
    iput-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqf;

    .line 516
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 484
    const/4 v0, 0x0

    invoke-static {p1, p2, p3, v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;LkP;)Landroid/content/Intent;

    move-result-object v0

    .line 485
    const-class v1, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 486
    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 491
    invoke-static {p4}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 493
    const-string v1, "javascriptCode"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 494
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)Landroid/webkit/WebView;
    .registers 2
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;Lnm;)Lnm;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lnm;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;)Lqc;
    .registers 2
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    return-object v0
.end method

.method private a(Landroid/content/Intent;)V
    .registers 14
    .parameter

    .prologue
    const/4 v11, 0x0

    .line 294
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 296
    new-instance v0, Lqm;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqd;

    iget-object v5, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LKS;

    iget-object v6, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Ljava/lang/Class;

    iget-object v7, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LPm;

    const-string v1, "webview"

    invoke-virtual {p0, v1, v11}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LNe;

    iget-object v10, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->b:Landroid/os/Handler;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v10}, Lqm;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;Landroid/content/Context;Lqd;Ljava/lang/String;LKS;Ljava/lang/Class;LPm;Landroid/content/SharedPreferences;LNe;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqf;

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqf;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebChromeClient;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 308
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 309
    if-nez v0, :cond_41

    .line 310
    const-string v0, "WebViewOpenActivity"

    const-string v1, "URL to load is not specified."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->finish()V

    .line 329
    :goto_40
    return-void

    .line 314
    :cond_41
    const-string v1, "docListTitle"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 316
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LPm;

    invoke-interface {v2, p0, v0}, LPm;->a(Landroid/content/Context;Landroid/net/Uri;)LPp;

    move-result-object v2

    .line 318
    sget-object v3, LkP;->c:LkP;

    invoke-virtual {v2}, LPp;->a()LkP;

    move-result-object v4

    invoke-virtual {v3, v4}, LkP;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_63

    .line 322
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v3, v11}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 323
    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v3, v11}, Landroid/webkit/WebView;->setVerticalScrollbarOverlay(Z)V

    .line 326
    :cond_63
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 327
    new-instance v3, Lqc;

    invoke-direct {v3, v0, v1, v2}, Lqc;-><init>(Ljava/lang/String;Ljava/lang/String;LPp;)V

    iput-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    .line 328
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Ljava/lang/String;)V

    goto :goto_40
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 411
    const-string v0, "WebViewOpenActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Start URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    const-string v0, "present"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 414
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_3b

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LNS;

    invoke-interface {v1}, LNS;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 425
    :goto_35
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqf;

    invoke-virtual {v0, p1}, Lqf;->a(Ljava/lang/String;)V

    .line 426
    return-void

    .line 419
    :cond_3b
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    goto :goto_35

    .line 422
    :cond_43
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    goto :goto_35
.end method

.method private b(Ljava/lang/String;)V
    .registers 9
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 461
    sget v0, Len;->error_page_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 462
    sget v0, Len;->error_opening_document_for_html:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 463
    new-array v1, v6, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 464
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 465
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 466
    const-string v2, "resourceId"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 467
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lpb;->a(Landroid/os/Bundle;)LfS;

    move-result-object v3

    .line 469
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a()Lo;

    move-result-object v0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(Lo;Ljava/lang/String;Ljava/lang/String;LfS;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 471
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqf;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqf;

    invoke-virtual {v0}, Lqf;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method public f()V
    .registers 2

    .prologue
    .line 475
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Intent;)V

    .line 476
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 240
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->requestWindowFeature(I)Z

    .line 242
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 246
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LeQ;

    const-string v2, "/webOpen"

    invoke-virtual {v0, v2, v1}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 249
    sget v0, Lej;->web_view_open:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->setContentView(I)V

    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a()Lo;

    move-result-object v0

    .line 252
    sget v2, Leh;->webview:I

    invoke-virtual {v0, v2}, Lo;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/WebViewFragment;

    .line 253
    invoke-virtual {v0}, Lcom/google/android/apps/docs/fragment/WebViewFragment;->b()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    .line 256
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, -0x1

    invoke-virtual {v0, v3, v2}, Landroid/view/Window;->setFeatureInt(II)V

    .line 258
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setPluginsEnabled(Z)V

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setLightTouchEnabled(Z)V

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    new-instance v2, Lqo;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lqo;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;Lqk;)V

    const-string v3, "mkxNativeWrapperApi"

    invoke-virtual {v0, v2, v3}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "appcache"

    invoke-virtual {v0, v2, v5}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 282
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v2, v0}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    const-wide/32 v2, 0x400000

    invoke-virtual {v0, v2, v3}, Landroid/webkit/WebSettings;->setAppCacheMaxSize(J)V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v4}, Landroid/webkit/WebView;->setClipToPadding(Z)V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LNS;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebSettings;

    invoke-virtual {v2}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, LNS;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Ljava/lang/String;

    .line 289
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Intent;)V

    .line 290
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 431
    const/16 v0, 0x64

    if-ne p1, v0, :cond_17

    .line 432
    new-instance v0, LmC;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a()Laoo;

    move-result-object v1

    invoke-direct {v0, v1, p0, v2}, LmC;-><init>(Laoo;Landroid/content/Context;I)V

    .line 434
    invoke-virtual {v0, v2}, LmC;->setCancelable(Z)V

    .line 437
    :goto_16
    return-object v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_16
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 358
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 359
    sget v3, Lek;->menu_webview:I

    invoke-virtual {v0, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 364
    sget v0, Leh;->menu_comments:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lgl;

    sget-object v4, Lgi;->b:Lgi;

    invoke-interface {v0, v4}, Lgl;->a(Lgi;)Z

    move-result v0

    if-eqz v0, :cond_61

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LPm;

    iget-object v4, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    invoke-virtual {v4}, Lqc;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v0, v4}, LPm;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_61

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LKS;

    const-string v4, "enableDocumentlistComments"

    invoke-interface {v0, v4, v2}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_61

    move v0, v1

    .line 369
    :goto_3d
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 371
    sget v0, Leh;->menu_sharing:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 372
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v3

    if-nez v3, :cond_5d

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    invoke-virtual {v3}, Lqc;->a()LPp;

    move-result-object v3

    invoke-virtual {v3}, LPp;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5d

    move v2, v1

    .line 374
    :cond_5d
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 376
    return v1

    :cond_61
    move v0, v2

    .line 366
    goto :goto_3d
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 336
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onDestroy()V

    .line 337
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 381
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LdL;

    invoke-interface {v1, p0}, LdL;->a(Landroid/content/Context;)V

    .line 382
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_comments:I

    if-ne v1, v2, :cond_32

    .line 383
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    invoke-virtual {v2}, Lqc;->a()LPp;

    move-result-object v2

    invoke-virtual {v2}, LPp;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v1

    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    invoke-virtual {v3}, Lqc;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a(Landroid/content/Context;LkY;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 387
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->startActivity(Landroid/content/Intent;)V

    .line 406
    :goto_31
    return v0

    .line 389
    :cond_32
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_refresh:I

    if-ne v1, v2, :cond_4b

    .line 391
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqf;

    invoke-virtual {v1}, Lqf;->a()V

    .line 392
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    invoke-virtual {v2}, Lqc;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_31

    .line 394
    :cond_4b
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_sharing:I

    if-ne v1, v2, :cond_73

    .line 395
    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    invoke-virtual {v2}, Lqc;->a()LPp;

    move-result-object v2

    invoke-virtual {v2}, LPp;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lqc;

    invoke-virtual {v3}, Lqc;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 398
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_31

    .line 400
    :cond_73
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Leh;->menu_send_feedback:I

    if-ne v1, v2, :cond_86

    .line 401
    new-instance v1, LZP;

    const-string v2, "android_docs"

    invoke-direct {v1, p0, v2}, LZP;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 403
    invoke-virtual {v1}, LZP;->a()V

    goto :goto_31

    .line 406
    :cond_86
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_31
.end method

.method protected onPause()V
    .registers 3

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LeQ;

    const-string v1, "/webOpen"

    invoke-virtual {v0, p0, v1}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 350
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 351
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->stopSync()V

    .line 352
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onPause()V

    .line 353
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 443
    const/16 v0, 0x64

    if-ne p1, v0, :cond_23

    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lnm;

    if-eqz v0, :cond_23

    .line 444
    check-cast p2, LmC;

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lnm;

    invoke-interface {v0}, Lnm;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LmC;->a(Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lnm;

    invoke-virtual {p2, v0}, LmC;->a(Lnm;)V

    .line 448
    invoke-virtual {p2}, LmC;->a()V

    .line 449
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:Lnm;

    .line 451
    :cond_23
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 341
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onResume()V

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LeQ;

    invoke-virtual {v0, p0}, LeQ;->a(Ljava/lang/Object;)V

    .line 343
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->startSync()V

    .line 344
    return-void
.end method

.method protected onStop()V
    .registers 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 456
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onStop()V

    .line 457
    return-void
.end method
