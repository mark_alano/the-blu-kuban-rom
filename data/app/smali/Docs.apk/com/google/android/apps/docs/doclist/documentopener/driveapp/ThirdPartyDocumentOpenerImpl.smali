.class public Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;
.super Ljava/lang/Object;
.source "ThirdPartyDocumentOpenerImpl.java"

# interfaces
.implements Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;


# instance fields
.field private final a:Landroid/content/Context;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

.field private final a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

.field private final a:Lgl;

.field private final a:Lqr;

.field private final a:Lqv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;Lqv;Lqr;Lgl;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    .line 94
    iput-object p2, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    .line 95
    iput-object p3, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lqv;

    .line 96
    iput-object p6, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    .line 97
    iput-object p4, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lqr;

    .line 98
    iput-object p5, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lgl;

    .line 99
    return-void
.end method

.method private a(LkM;Landroid/os/Bundle;)Ljava/util/List;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkM;",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "LqH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    invoke-static {p2}, Lpb;->a(Landroid/os/Bundle;)LfS;

    move-result-object v0

    .line 119
    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a(LkM;LfS;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(LkM;LfS;)Ljava/util/List;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LkM;",
            "LfS;",
            ")",
            "Ljava/util/List",
            "<",
            "LqH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    invoke-static {}, LajX;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    invoke-static {p1, p2, v1, v2, v3}, LqG;->a(LkM;LfS;Landroid/content/Context;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 106
    sget-object v1, LfS;->a:LfS;

    sget-object v2, LfS;->b:LfS;

    invoke-static {v1, v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v1

    .line 108
    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lgl;

    sget-object v3, Lgi;->e:Lgi;

    invoke-interface {v2, v3}, Lgl;->a(Lgi;)Z

    move-result v2

    if-eqz v2, :cond_41

    invoke-interface {v1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_41

    .line 110
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lqr;

    invoke-static {p1, v1, v2}, LqF;->a(LkM;Landroid/content/Context;Lqr;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 111
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lqv;

    iget-object v3, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lqr;

    invoke-static {p1, v1, v2, v3}, LqQ;->a(LkM;Landroid/content/Context;Lqv;Lqr;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 114
    :cond_41
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;)Z
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LqH;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 123
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LqH;

    .line 124
    invoke-virtual {v0}, LqH;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 125
    const/4 v0, 0x1

    .line 128
    :goto_17
    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method


# virtual methods
.method public a(Lpa;LkM;Landroid/os/Bundle;)LamQ;
    .registers 12
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lpa;",
            "LkM;",
            "Landroid/os/Bundle;",
            ")",
            "LamQ",
            "<",
            "Lnm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    invoke-virtual {p2}, LkM;->g()Ljava/lang/String;

    .line 135
    invoke-virtual {p2}, LkM;->c()Ljava/lang/String;

    .line 137
    const-string v0, "forceApplicationSelectionDialog"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 140
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a(LkM;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v2

    .line 141
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 142
    const/4 v0, 0x0

    invoke-static {v0}, LamF;->a(Ljava/lang/Object;)LamQ;

    move-result-object v0

    .line 179
    :goto_1c
    return-object v0

    .line 143
    :cond_1d
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a(Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_2c

    if-nez v0, :cond_2c

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;->a(Lpa;LkM;Landroid/os/Bundle;)LamQ;

    move-result-object v0

    goto :goto_1c

    .line 150
    :cond_2c
    invoke-static {}, LamZ;->a()LamZ;

    move-result-object v6

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    .line 152
    new-instance v0, LqK;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, LqK;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;Ljava/util/List;Lpa;LkM;Landroid/os/Bundle;LamZ;)V

    .line 162
    sget v1, Len;->open_with_dialog_title:I

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 163
    iget-object v1, p0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a:Landroid/content/Context;

    invoke-static {v1, v2}, LqH;->a(Landroid/content/Context;Ljava/util/List;)Landroid/widget/BaseAdapter;

    move-result-object v1

    .line 164
    const/4 v2, -0x1

    invoke-virtual {v7, v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 165
    new-instance v0, LqL;

    invoke-direct {v0, p0, v6}, LqL;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;LamZ;)V

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 171
    const/high16 v0, 0x104

    new-instance v1, LqM;

    invoke-direct {v1, p0}, LqM;-><init>(Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;)V

    invoke-virtual {v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 177
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 178
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 179
    invoke-static {v6}, LZV;->a(LamQ;)LamQ;

    move-result-object v0

    goto :goto_1c
.end method

.method public a(LkM;Landroid/os/Bundle;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 185
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;->a(LkM;Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v0

    .line 186
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method
