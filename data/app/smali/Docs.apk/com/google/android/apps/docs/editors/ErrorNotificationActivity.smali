.class public Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;
.super Lcom/google/android/apps/docs/RoboFragmentActivity;
.source "ErrorNotificationActivity.java"


# instance fields
.field public a:LZR;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/content/Intent;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;)Landroid/content/Intent;
    .registers 2
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->i:Z

    return p1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter

    .prologue
    const/high16 v6, 0x2

    const/4 v5, 0x0

    .line 51
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    sget v0, LsF;->error_notification:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->setContentView(I)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 54
    const-string v0, "stack_trace"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    const-string v0, "editor_intent"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a:Landroid/content/Intent;

    .line 56
    const-string v0, "editor_title"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a:LZR;

    if-nez v0, :cond_3a

    .line 61
    const-string v0, "ErrorNotificationActivity"

    const-string v1, "This should never happen: feedbackReporter not initialized by guice"

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    new-instance v0, LZQ;

    invoke-direct {v0}, LZQ;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a:LZR;

    .line 65
    :cond_3a
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a()Lo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a(Lo;)V

    .line 67
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 68
    const v1, 0x1080027

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, LsH;->error_report_title:I

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, LsH;->error_report_description:I

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v3, LsH;->error_report_button_report:I

    new-instance v4, Lss;

    invoke-direct {v4, p0, v2}, Lss;-><init>(Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, LsH;->error_report_button_reload:I

    new-instance v3, Lsr;

    invoke-direct {v3, p0}, Lsr;-><init>(Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, LsH;->error_report_button_close:I

    new-instance v3, Lsq;

    invoke-direct {v3, p0}, Lsq;-><init>(Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 101
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 105
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 106
    iput-boolean v5, p0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->i:Z

    .line 107
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 40
    invoke-super {p0}, Lcom/google/android/apps/docs/RoboFragmentActivity;->onResume()V

    .line 41
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->i:Z

    if-eqz v0, :cond_14

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->startActivity(Landroid/content/Intent;)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/ErrorNotificationActivity;->finish()V

    .line 47
    :cond_14
    return-void
.end method
