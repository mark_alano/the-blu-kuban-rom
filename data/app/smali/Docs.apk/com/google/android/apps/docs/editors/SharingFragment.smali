.class public Lcom/google/android/apps/docs/editors/SharingFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "SharingFragment.java"

# interfaces
.implements LTr;


# instance fields
.field public a:LKt;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LUq;

.field public a:LUr;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LamE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LamE",
            "<",
            "LUq;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/concurrent/Executor;

.field private a:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field private final b:LamE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LamE",
            "<",
            "LUq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 46
    const/4 v0, 0x0

    invoke-static {v0}, LamF;->a(Ljava/lang/Object;)LamQ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:Ljava/util/concurrent/Future;

    .line 50
    new-instance v0, LsI;

    invoke-direct {v0, p0}, LsI;-><init>(Lcom/google/android/apps/docs/editors/SharingFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LamE;

    .line 83
    new-instance v0, LsJ;

    invoke-direct {v0, p0}, LsJ;-><init>(Lcom/google/android/apps/docs/editors/SharingFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->b:LamE;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/SharingFragment;LUq;)LUq;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUq;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/SharingFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/SharingFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/SharingFragment;->p()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/SharingFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/SharingFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/editors/SharingFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LdL;

    return-object v0
.end method

.method private p()V
    .registers 5

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUq;

    invoke-interface {v0}, LUq;->e()Ljava/lang/String;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUq;

    invoke-interface {v1}, LUq;->c()Ljava/lang/String;

    move-result-object v1

    .line 125
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUq;

    invoke-interface {v2}, LUq;->d()Ljava/lang/String;

    move-result-object v2

    .line 127
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUr;

    invoke-interface {v3, v0, v1, v2}, LUr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LamQ;

    move-result-object v0

    .line 129
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:Ljava/util/concurrent/Future;

    .line 130
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->b:LamE;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    .line 131
    return-void
.end method

.method private q()V
    .registers 4

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUr;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUq;

    invoke-interface {v0, v1}, LUr;->a(LUq;)LamQ;

    move-result-object v0

    .line 143
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:Ljava/util/concurrent/Future;

    .line 144
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LamE;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    .line 145
    return-void
.end method


# virtual methods
.method public a()LUq;
    .registers 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUq;

    return-object v0
.end method

.method public a()V
    .registers 1

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/SharingFragment;->q()V

    .line 118
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 102
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 104
    new-instance v0, Laat;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v1}, Laat;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:Ljava/util/concurrent/Executor;

    .line 106
    new-instance v0, LJX;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LKt;

    invoke-virtual {v2}, LKt;->a()Z

    move-result v2

    invoke-direct {v0, v1, v2}, LJX;-><init>(Landroid/content/Intent;Z)V

    .line 108
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUr;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/SharingFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0, v2}, LJX;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, LJX;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, LJX;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, LUr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LUq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUq;

    .line 112
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/SharingFragment;->p()V

    .line 113
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 149
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUq;

    if-eqz v0, :cond_c

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:LUq;

    invoke-interface {v0}, LUq;->a()V

    .line 154
    :cond_c
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/SharingFragment;->a:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 160
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h()V

    .line 161
    return-void
.end method
