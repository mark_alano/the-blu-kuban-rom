.class public Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;
.super Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;
.source "TrixCollaboratorFragment.java"


# instance fields
.field private a:LGY;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LGY;",
            ">;"
        }
    .end annotation
.end field

.field private a:LsK;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;-><init>()V

    return-void
.end method

.method private a()V
    .registers 6

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LGY;

    invoke-interface {v0}, LGY;->a()[LJD;

    move-result-object v1

    .line 46
    if-eqz v1, :cond_16

    .line 47
    array-length v2, v1

    const/4 v0, 0x0

    :goto_a
    if-ge v0, v2, :cond_16

    aget-object v3, v1, v0

    .line 48
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LsK;

    invoke-virtual {v4, v3}, LsK;->a(LJD;)V

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 51
    :cond_16
    return-void
.end method


# virtual methods
.method protected e(Landroid/os/Bundle;)V
    .registers 2
    .parameter

    .prologue
    .line 27
    return-void
.end method

.method public g()V
    .registers 8

    .prologue
    .line 31
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->g()V

    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGY;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LGY;

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LsK;

    if-nez v0, :cond_29

    .line 36
    new-instance v0, LsK;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->d:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:Landroid/widget/ImageView;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, LsK;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/TextView;Landroid/widget/LinearLayout;Landroid/widget/ImageView;LsN;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LsK;

    .line 40
    :cond_29
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a()V

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LGY;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LsK;

    invoke-interface {v0, v1}, LGY;->a(LHa;)V

    .line 42
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LGY;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LsK;

    invoke-interface {v0, v1}, LGY;->b(LHa;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/collab/TrixCollaboratorFragment;->a:LsK;

    invoke-virtual {v0}, LsK;->a()V

    .line 58
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;->h()V

    .line 59
    return-void
.end method
