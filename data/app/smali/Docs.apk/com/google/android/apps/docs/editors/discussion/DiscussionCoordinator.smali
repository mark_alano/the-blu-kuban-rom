.class public Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "DiscussionCoordinator.java"

# interfaces
.implements Ltb;


# instance fields
.field private a:I

.field private final a:Landroid/os/Handler;

.field private a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

.field private a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

.field private a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

.field private a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

.field private final a:Ljava/lang/Runnable;

.field private a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LsS;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lma;

.field private a:Lmx;

.field private a:LsR;

.field public a:LsT;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ltd;

.field private a:Lte;

.field private a:Z

.field private b:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Z

    .line 73
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Landroid/os/Handler;

    .line 78
    new-instance v0, LsU;

    invoke-direct {v0, p0}, LsU;-><init>(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Ljava/lang/Runnable;

    return-void
.end method

.method private A()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 560
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, LsH;->discussion_cant_comment:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 562
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    .line 563
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 564
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;)Ljava/util/Set;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Ljava/util/Set;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;)Lma;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lma;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;Lmx;)Lmx;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lmx;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;)V
    .registers 1
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->z()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;Ljava/util/SortedSet;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(Ljava/util/SortedSet;)V

    return-void
.end method

.method private a(Ljava/util/SortedSet;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 465
    const-string v0, "DiscussionCoordinator"

    const-string v1, "updateAnchorsVisibility"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    invoke-interface {v0}, LsR;->a()Ljava/util/List;

    move-result-object v2

    .line 468
    sget-object v0, Lmz;->a:Lagv;

    invoke-static {p1, v0}, LajB;->a(Ljava/lang/Iterable;Lagv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1c
    :goto_1c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 470
    invoke-interface {v0}, Lmz;->b()Ljava/lang/String;

    move-result-object v4

    .line 471
    if-eqz v4, :cond_1c

    invoke-interface {v2, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 472
    invoke-interface {v0}, Lmz;->d()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 474
    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1c

    .line 480
    :cond_3e
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 481
    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 482
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    const/4 v3, 0x1

    invoke-interface {v2, v1, v3}, LsR;->a(Ljava/util/Set;Z)V

    .line 483
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LsR;->a(Ljava/util/Set;Z)V

    .line 484
    return-void
.end method

.method private b(I)V
    .registers 5
    .parameter

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lma;

    invoke-interface {v0}, Lma;->b()LlV;

    move-result-object v0

    .line 493
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Landroid/os/Handler;

    new-instance v2, LsV;

    invoke-direct {v2, p0, v0, p1}, LsV;-><init>(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;LlV;I)V

    invoke-interface {v0, v1, v2}, LlV;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 516
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(I)V

    return-void
.end method

.method private c(I)V
    .registers 5
    .parameter

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lma;

    invoke-interface {v0}, Lma;->a()LlV;

    move-result-object v0

    .line 532
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Landroid/os/Handler;

    new-instance v2, LsW;

    invoke-direct {v2, p0, v0, p1}, LsW;-><init>(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;LlV;I)V

    invoke-interface {v0, v1, v2}, LlV;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 548
    return-void
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 429
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->x()V

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    sget-object v2, Ltm;->d:Ltm;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a(Ltm;)Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 431
    if-eqz p1, :cond_1b

    new-instance v0, Ltd;

    invoke-direct {v0, v1, p1}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :goto_15
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->b(Ltd;Ljava/lang/String;)V

    .line 433
    return-void

    :cond_1b
    move-object v0, v1

    .line 431
    goto :goto_15
.end method

.method private j()Z
    .registers 5

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()I

    move-result v0

    .line 120
    add-int/lit8 v0, v0, -0x1

    :goto_a
    if-ltz v0, :cond_35

    .line 121
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lo;->a(I)Lp;

    move-result-object v1

    .line 122
    sget-object v2, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:Ljava/util/Map;

    invoke-interface {v1}, Lp;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lo;

    move-result-object v0

    invoke-interface {v1}, Lp;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 125
    const/4 v0, 0x1

    .line 128
    :goto_31
    return v0

    .line 120
    :cond_32
    add-int/lit8 v0, v0, -0x1

    goto :goto_a

    .line 128
    :cond_35
    const/4 v0, 0x0

    goto :goto_31
.end method

.method private x()V
    .registers 5

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->j()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 143
    :goto_6
    return-void

    .line 136
    :cond_7
    new-instance v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 138
    sget-object v0, Ltm;->a:Ltm;

    invoke-virtual {v0}, Ltm;->a()Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lo;

    move-result-object v1

    invoke-virtual {v1}, Lo;->a()Lz;

    move-result-object v1

    sget v2, LsD;->discussion_state:I

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    invoke-virtual {v1, v2, v3, v0}, Lz;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v1

    invoke-virtual {v1, v0}, Lz;->a(Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 142
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Z

    goto :goto_6
.end method

.method private y()V
    .registers 4

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->k()Z

    move-result v0

    if-nez v0, :cond_50

    .line 438
    sget v0, LsD;->discussion_holder_phone:I

    move v2, v0

    .line 446
    :goto_9
    iget v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    if-eq v0, v2, :cond_4f

    .line 447
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, LsD;->discussion_holder_active:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 448
    if-eqz v1, :cond_33

    .line 449
    sget v0, LsD;->discussion_holder_original_id:I

    invoke-virtual {v1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 450
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setId(I)V

    .line 451
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    .line 452
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 454
    :cond_33
    iput v2, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    .line 455
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 456
    sget v1, LsD;->discussion_holder_original_id:I

    iget v2, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 457
    sget v1, LsD;->discussion_holder_active:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 459
    :cond_4f
    return-void

    .line 440
    :cond_50
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->i()Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 441
    sget v0, LsD;->discussion_holder_tablet_landscape:I

    move v2, v0

    goto :goto_9

    .line 443
    :cond_5a
    sget v0, LsD;->discussion_holder_tablet_portrait:I

    move v2, v0

    goto :goto_9
.end method

.method private z()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 551
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 557
    :goto_7
    return-void

    .line 554
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, LsH;->discussion_error:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 555
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 556
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_7
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 390
    iget v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    if-eqz v0, :cond_23

    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 395
    if-eqz v0, :cond_20

    .line 396
    sget v1, LsD;->discussion_holder_original_id:I

    iget v2, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 397
    sget v1, LsD;->discussion_holder_active:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 399
    :cond_20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Z

    .line 401
    :cond_23
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;
    .registers 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;
    .registers 2

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;
    .registers 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    return-object v0
.end method

.method public a()Ljava/util/SortedSet;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lma;

    invoke-interface {v0}, Lma;->a()Ljava/util/SortedSet;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->h()Z

    move-result v0

    if-nez v0, :cond_8

    .line 156
    :goto_7
    return-void

    .line 150
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    invoke-interface {v0}, LsR;->a()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->v()V

    goto :goto_7

    .line 154
    :cond_14
    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_7
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 353
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 355
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lo;

    move-result-object v0

    sget v1, LsD;->discussion_holder_active:I

    invoke-virtual {v0, v1}, Lo;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;

    .line 357
    if-eqz v0, :cond_20

    .line 358
    sget-object v1, LsX;->a:[I

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a()Ltm;

    move-result-object v2

    invoke-virtual {v2}, Ltm;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_64

    .line 370
    :cond_20
    :goto_20
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    if-nez v0, :cond_2a

    .line 371
    invoke-static {}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a()Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    .line 373
    :cond_2a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    if-nez v0, :cond_34

    .line 374
    invoke-static {}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    .line 376
    :cond_34
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    if-nez v0, :cond_3e

    .line 377
    invoke-static {}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    .line 380
    :cond_3e
    if-eqz p1, :cond_60

    const-string v0, "currentDiscussionHolder"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 381
    const-string v0, "currentDiscussionHolder"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    .line 385
    :goto_50
    return-void

    .line 360
    :pswitch_51
    check-cast v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    goto :goto_20

    .line 363
    :pswitch_56
    check-cast v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    goto :goto_20

    .line 366
    :pswitch_5b
    check-cast v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    goto :goto_20

    .line 383
    :cond_60
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    goto :goto_50

    .line 358
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_51
        :pswitch_56
        :pswitch_5b
    .end packed-switch
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 165
    if-nez p1, :cond_3

    .line 169
    :goto_2
    return-void

    .line 168
    :cond_3
    new-instance v0, Ltd;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(Ltd;)V

    goto :goto_2
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->h()Z

    move-result v0

    if-nez v0, :cond_7

    .line 222
    :goto_6
    return-void

    .line 218
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->x()V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    sget-object v1, Ltm;->d:Ltm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a(Ltm;)Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 220
    new-instance v0, Ltd;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a(Ltd;Ljava/lang/String;)V

    goto :goto_6
.end method

.method public a(LsS;)V
    .registers 3
    .parameter

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lmx;

    if-eqz v0, :cond_a

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lmx;

    invoke-interface {p1, v0}, LsS;->a(Lmx;)V

    .line 338
    :goto_9
    return-void

    .line 336
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_9
.end method

.method public a(Ltd;)V
    .registers 2
    .parameter

    .prologue
    .line 160
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(Ltd;)V

    .line 161
    return-void
.end method

.method public a(Ltd;)Z
    .registers 5
    .parameter

    .prologue
    .line 182
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Ltd;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lte;

    invoke-interface {v0}, Lte;->b()V

    .line 184
    if-eqz p1, :cond_18

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    invoke-virtual {p1}, Ltd;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ltd;->a()Z

    move-result v2

    invoke-interface {v0, v1, v2}, LsR;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 189
    :goto_17
    return v0

    .line 188
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    invoke-interface {v0}, LsR;->a()V

    .line 189
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->h()Z

    move-result v0

    if-nez v0, :cond_7

    .line 211
    :goto_6
    return-void

    .line 207
    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->x()V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    sget-object v1, Ltm;->d:Ltm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a(Ltm;)Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 209
    new-instance v0, Ltd;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a(Ltd;)V

    goto :goto_6
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    const/4 v1, 0x1

    invoke-interface {v0, p1, v1}, LsR;->a(Ljava/lang/String;Z)Z

    .line 234
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    return-void
.end method

.method public b(Ltd;)V
    .registers 3
    .parameter

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->b()Z

    .line 245
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(Ltd;)V

    .line 246
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 406
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->c(Landroid/os/Bundle;)V

    .line 407
    const-string v0, "currentDiscussionHolder"

    iget v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 408
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->b()Z

    .line 251
    if-eqz p1, :cond_e

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    invoke-interface {v0, p1}, LsR;->a(Ljava/lang/String;)V

    .line 256
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    invoke-interface {v0}, LsR;->a()V

    .line 257
    return-void
.end method

.method public c(Ltd;)V
    .registers 4
    .parameter

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->x()V

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Ltm;

    move-result-object v0

    sget-object v1, Ltm;->c:Ltm;

    if-eq v0, v1, :cond_17

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    sget-object v1, Ltm;->c:Ltm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a(Ltm;)Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 316
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ltd;)V

    .line 317
    return-void
.end method

.method public g()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 92
    const-string v0, "DiscussionCoordinator"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsT;

    invoke-interface {v0}, LsT;->a()LsR;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lte;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lte;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    invoke-interface {v0, p0}, LsR;->a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b:I

    .line 98
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->y()V

    .line 99
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Ljava/util/Set;

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lmx;

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lma;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lma;

    .line 104
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Z

    if-eqz v0, :cond_51

    .line 106
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b(I)V

    .line 107
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(I)V

    .line 108
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Z

    .line 110
    :cond_51
    return-void
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsT;

    invoke-interface {v0}, LsT;->a()Z

    move-result v0

    if-nez v0, :cond_d

    .line 196
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->A()V

    .line 197
    const/4 v0, 0x0

    .line 199
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x1

    goto :goto_c
.end method

.method public i()V
    .registers 3

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lma;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lma;->b(Ljava/lang/Runnable;)Z

    .line 115
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->i()V

    .line 116
    return-void
.end method

.method public i()Z
    .registers 3

    .prologue
    .line 411
    iget v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 3
    .parameter

    .prologue
    .line 342
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 343
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->d()Z

    move-result v0

    if-nez v0, :cond_a

    .line 349
    :goto_9
    return-void

    .line 347
    :cond_a
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b:I

    .line 348
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->y()V

    goto :goto_9
.end method

.method public p()V
    .registers 3

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->j()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    sget-object v1, Ltm;->a:Ltm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a(Ltm;)Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 178
    :cond_10
    return-void
.end method

.method public q()V
    .registers 3

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->x()V

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    invoke-interface {v0}, LsR;->a()V

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    sget-object v1, Ltm;->b:Ltm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a(Ltm;)Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 229
    return-void
.end method

.method public r()V
    .registers 2

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->b()Z

    .line 240
    return-void
.end method

.method public s()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 264
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 271
    :goto_7
    return-void

    .line 267
    :cond_8
    const-string v0, "DiscussionCoordinator"

    const-string v1, "startFetchingDiscussions"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b(I)V

    .line 269
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lma;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lma;->a(Ljava/lang/Runnable;)Z

    goto :goto_7
.end method

.method public t()V
    .registers 3

    .prologue
    .line 291
    const-string v0, "DiscussionCoordinator"

    const-string v1, "showHideDiscussions"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->x()V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Ltm;

    move-result-object v0

    sget-object v1, Ltm;->a:Ltm;

    if-ne v0, v1, :cond_2c

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    sget-object v1, Ltm;->b:Ltm;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a(Ltm;)Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lmx;

    if-nez v0, :cond_26

    .line 298
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b(I)V

    .line 300
    :cond_26
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Lte;

    invoke-interface {v0}, Lte;->a()V

    .line 304
    :goto_2b
    return-void

    .line 302
    :cond_2c
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->p()V

    goto :goto_2b
.end method

.method public u()V
    .registers 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Ltd;

    if-eqz v0, :cond_9

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:Ltd;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(Ltd;)Z

    .line 328
    :cond_9
    return-void
.end method

.method public v()V
    .registers 2

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->h()Z

    move-result v0

    if-nez v0, :cond_7

    .line 422
    :goto_6
    return-void

    .line 421
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a:LsR;

    invoke-interface {v0}, LsR;->b()V

    goto :goto_6
.end method

.method public w()V
    .registers 2

    .prologue
    .line 522
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(I)V

    .line 523
    return-void
.end method
