.class public abstract Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "BaseDiscussionStateMachineFragment.java"


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ltm;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:I

.field public a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

.field public a:LsT;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 43
    return-void
.end method


# virtual methods
.method a(Ltm;)Landroid/util/Pair;
    .registers 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltm;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;",
            "Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Lo;

    move-result-object v0

    .line 164
    const-string v1, "BaseDiscussionStateMachineFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFragmentsForState "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Ltm;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " before pop backstack"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    invoke-virtual {p1}, Ltm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_76

    .line 166
    invoke-virtual {p1}, Ltm;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lo;->a(Ljava/lang/String;I)Z

    .line 174
    :cond_38
    :goto_38
    const-string v0, "BaseDiscussionStateMachineFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFragmentsForState "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Ltm;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " after pop backstack"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    sget-object v0, Ltl;->a:[I

    invoke-virtual {p1}, Ltm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_aa

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    move-result-object v1

    .line 193
    new-instance v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/AllDiscussionsStateMachineFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/AllDiscussionsStateMachineFragment;-><init>()V

    .line 196
    :goto_70
    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2

    .line 168
    :cond_76
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ltm;->d:Ltm;

    invoke-virtual {v2}, Ltm;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_38

    .line 172
    invoke-virtual {v0}, Lo;->b()Z

    goto :goto_38

    .line 179
    :pswitch_8a
    const/4 v1, 0x0

    .line 180
    new-instance v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/NoDiscussionsStateMachineFragment;-><init>()V

    goto :goto_70

    .line 183
    :pswitch_91
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    move-result-object v1

    .line 184
    new-instance v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/PagerDiscussionStateMachineFragment;-><init>()V

    goto :goto_70

    .line 187
    :pswitch_9d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    move-result-object v1

    .line 188
    new-instance v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/EditCommentStateMachineFragment;-><init>()V

    goto :goto_70

    .line 177
    nop

    :pswitch_data_aa
    .packed-switch 0x1
        :pswitch_8a
        :pswitch_91
        :pswitch_9d
    .end packed-switch
.end method

.method public a(Ltm;)Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;
    .registers 7
    .parameter

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->d()Z

    move-result v0

    if-nez v0, :cond_8

    .line 81
    const/4 v1, 0x0

    .line 107
    :goto_7
    return-object v1

    .line 83
    :cond_8
    const-string v0, "BaseDiscussionStateMachineFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "transitionTo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Lo;

    move-result-object v2

    .line 89
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a(Ltm;)Landroid/util/Pair;

    move-result-object v1

    .line 91
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;

    .line 92
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;

    .line 94
    invoke-virtual {v2}, Lo;->a()Lz;

    move-result-object v3

    .line 97
    if-eqz v0, :cond_3b

    .line 98
    sget v4, LsD;->discussion_holder_active:I

    invoke-virtual {v3, v4, v0}, Lz;->b(ILandroid/support/v4/app/Fragment;)Lz;

    .line 102
    :cond_3b
    sget v0, LsD;->discussion_state:I

    invoke-virtual {p1}, Ltm;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v1, v4}, Lz;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    .line 104
    invoke-virtual {p1}, Ltm;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lz;->a(Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 105
    invoke-virtual {v2}, Lo;->a()Z

    goto :goto_7
.end method

.method public abstract a()Ltm;
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    .line 135
    return-void
.end method

.method public g()V
    .registers 4

    .prologue
    .line 139
    const-string v0, "BaseDiscussionStateMachineFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Ltm;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:I

    .line 143
    return-void
.end method

.method public h()V
    .registers 4

    .prologue
    .line 147
    const-string v0, "BaseDiscussionStateMachineFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Ltm;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h()V

    .line 149
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 5
    .parameter

    .prologue
    .line 112
    const-string v0, "BaseDiscussionStateMachineFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a()Ltm;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->d()Z

    move-result v0

    if-nez v0, :cond_26

    .line 129
    :cond_25
    :goto_25
    return-void

    .line 118
    :cond_26
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_25

    iget v0, p0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_25

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Ltk;

    invoke-direct {v1, p0}, Ltk;-><init>(Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_25
.end method
