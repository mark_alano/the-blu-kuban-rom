.class public Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;
.super Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;
.source "AllDiscussionsFragment.java"


# instance fields
.field private a:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ltx;

.field private a:Lty;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;-><init>()V

    .line 65
    sget-object v0, Ltx;->a:Ltx;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Ltx;

    return-void
.end method

.method public static a()Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;
    .registers 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;-><init>()V

    .line 75
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;)Ljava/util/SortedSet;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Ljava/util/SortedSet;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;)Lty;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Lty;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;Ltx;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a(Ltx;)V

    return-void
.end method

.method private a(Ltx;)V
    .registers 3
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Ltx;

    if-ne v0, p1, :cond_5

    .line 104
    :goto_4
    return-void

    .line 102
    :cond_5
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Ltx;

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Lty;

    invoke-interface {v0, p1}, Lty;->a(Ltx;)V

    goto :goto_4
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Lty;

    invoke-interface {v0, p1}, Lty;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a()Ltm;
    .registers 2

    .prologue
    .line 80
    sget-object v0, Ltm;->b:Ltm;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Ltb;

    invoke-interface {v0}, Ltb;->a()V

    .line 159
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 108
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a(Landroid/os/Bundle;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Lty;

    if-nez v0, :cond_f

    .line 110
    new-instance v0, Ltz;

    invoke-direct {v0, p0}, Ltz;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Lty;

    .line 112
    :cond_f
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->d(Z)V

    .line 113
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->e(Z)V

    .line 114
    return-void
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 118
    sget v0, LsD;->menu_discussion:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 119
    sget v1, LsH;->discussion_close_comments:I

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 120
    return-void
.end method

.method a(Ljava/util/SortedSet;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 85
    const-string v0, "AllDiscussionsFragment"

    const-string v1, "Updating discussions"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Ljava/util/SortedSet;

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Landroid/os/Handler;

    new-instance v1, Ltw;

    invoke-direct {v1, p0}, Ltw;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 96
    return-void
.end method

.method public a(Ltd;)V
    .registers 3
    .parameter

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Ltb;

    invoke-interface {v0, p1}, Ltb;->a(Ltd;)V

    .line 152
    return-void
.end method

.method public g()V
    .registers 4

    .prologue
    .line 130
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->g()V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:LAQ;

    invoke-virtual {v0}, LAQ;->a()V

    .line 132
    sget-object v0, Ltx;->b:Ltx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a(Ltx;)V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LsD;->discussion_all_discussions_container:I

    invoke-static {v0, v1, v2}, LsY;->a(Landroid/view/View;Landroid/content/res/Resources;I)V

    .line 137
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 141
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->h()V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:LAQ;

    invoke-virtual {v0}, LAQ;->a()V

    .line 143
    return-void
.end method

.method public p()V
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a:Ltb;

    invoke-interface {v0}, Ltb;->p()V

    .line 166
    return-void
.end method
