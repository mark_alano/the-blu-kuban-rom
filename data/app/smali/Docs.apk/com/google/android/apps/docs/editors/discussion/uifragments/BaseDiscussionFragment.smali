.class public abstract Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "BaseDiscussionFragment.java"

# interfaces
.implements LsS;


# instance fields
.field a:LAQ;

.field final a:Landroid/os/Handler;

.field private final a:Ljava/lang/Runnable;

.field public a:Lma;

.field a:Lmx;

.field public a:LsT;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Ltb;

.field private a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Landroid/os/Handler;

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Z

    .line 48
    new-instance v0, LtD;

    invoke-direct {v0, p0}, LtD;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public a()LAQ;
    .registers 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LAQ;

    return-object v0
.end method

.method public a()Lmx;
    .registers 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Lmx;

    return-object v0
.end method

.method public abstract a()Ltm;
.end method

.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 111
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Z

    if-nez v0, :cond_15

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 113
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 114
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 116
    :cond_15
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()LAQ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LAQ;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Ltb;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lma;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Lma;

    .line 64
    return-void
.end method

.method public abstract a(Ljava/util/SortedSet;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation
.end method

.method public a(Lmx;)V
    .registers 2
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Lmx;

    .line 91
    return-void
.end method

.method public g()V
    .registers 3

    .prologue
    .line 68
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Lma;

    if-nez v0, :cond_1f

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()LAQ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LAQ;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Ltb;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lma;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Lma;

    .line 74
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Lma;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lma;->a(Ljava/lang/Runnable;)Z

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Lmx;

    if-nez v0, :cond_33

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(LsS;)V

    .line 78
    :cond_33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Z

    .line 79
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 83
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h()V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Z

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Lma;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lma;->b(Ljava/lang/Runnable;)Z

    .line 86
    return-void
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a:Z

    return v0
.end method
