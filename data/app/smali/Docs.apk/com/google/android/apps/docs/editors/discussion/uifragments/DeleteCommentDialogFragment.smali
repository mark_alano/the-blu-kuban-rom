.class public Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "DeleteCommentDialogFragment.java"


# instance fields
.field public a:LsT;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Bundle;Z)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 45
    const-string v0, "isDiscussion"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 46
    return-void
.end method

.method public static a(Lo;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 31
    invoke-static {v0, p2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;->a(Landroid/os/Bundle;Z)V

    .line 32
    new-instance v1, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;-><init>()V

    .line 33
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;->d(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {v1, p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 35
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 7
    .parameter

    .prologue
    const/high16 v4, 0x2

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "isDiscussion"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 52
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    const v2, 0x1080027

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 54
    if-eqz v0, :cond_57

    .line 55
    sget v0, LsH;->discussion_delete_discussion_title:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, LsH;->discussion_delete_discussion_text:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 61
    :goto_31
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, LsH;->discussion_delete_cancel:I

    new-instance v3, LtF;

    invoke-direct {v3, p0}, LtF;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, LsH;->discussion_delete_yes:I

    new-instance v3, LtE;

    invoke-direct {v3, p0}, LtE;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/DeleteCommentDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 80
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 83
    return-object v0

    .line 58
    :cond_57
    sget v0, LsH;->discussion_delete_comment_title:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, LsH;->discussion_delete_comment_text:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_31
.end method
