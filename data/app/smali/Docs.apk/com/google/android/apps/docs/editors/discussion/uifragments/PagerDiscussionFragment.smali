.class public Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;
.super Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;
.source "PagerDiscussionFragment.java"


# instance fields
.field private a:J

.field private a:Landroid/support/v4/view/ViewPager;

.field private a:Landroid/view/MenuItem;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ltd;

.field private a:Lub;

.field private a:Lug;

.field private a:Luh;

.field private a:Z

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ltd;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;-><init>()V

    .line 73
    sget-object v0, Luh;->a:Luh;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Luh;

    return-void
.end method

.method public static a()Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;
    .registers 1

    .prologue
    .line 98
    new-instance v0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;-><init>()V

    .line 99
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ljava/util/List;

    return-object v0
.end method

.method private a()V
    .registers 5

    .prologue
    .line 435
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:J

    .line 436
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/os/Handler;

    new-instance v1, Lue;

    invoke-direct {v1, p0}, Lue;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 444
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->p()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;Ltd;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b(Ltd;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;Ltd;Ltd;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ltd;Ltd;)V

    return-void
.end method

.method private a(Ltd;Ltd;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 378
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ltd;

    .line 379
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ltd;

    .line 380
    return-void
.end method

.method private a(Luh;)V
    .registers 6
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Luh;

    if-ne v0, p1, :cond_8

    .line 375
    :cond_7
    :goto_7
    return-void

    .line 360
    :cond_8
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Luh;

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->d:Landroid/view/View;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_7

    .line 363
    sget-object v0, Luf;->a:[I

    invoke-virtual {p1}, Luh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_34

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v3}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    goto :goto_7

    .line 365
    :pswitch_28
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    goto :goto_7

    .line 363
    nop

    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_28
    .end packed-switch
.end method

.method private a(Ljava/util/List;Ltd;)Z
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lmz;",
            ">;",
            "Ltd;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 230
    if-nez p1, :cond_5

    .line 248
    :cond_4
    :goto_4
    return v2

    :cond_5
    move v1, v2

    .line 233
    :goto_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 234
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 235
    invoke-interface {v0}, Lmz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ltd;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_26

    invoke-interface {v0}, Lmz;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ltd;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_49

    .line 237
    :cond_26
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ljava/util/List;

    if-eq v2, p1, :cond_31

    .line 238
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ljava/util/List;

    .line 239
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    invoke-virtual {v2}, Lub;->a()V

    .line 241
    :cond_31
    new-instance v2, Ltd;

    invoke-direct {v2, v0}, Ltd;-><init>(Lmz;)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b(Ltd;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 243
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ltd;Ltd;)V

    .line 244
    sget-object v0, Luh;->c:Luh;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Luh;)V

    move v2, v3

    .line 245
    goto :goto_4

    .line 233
    :cond_49
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6
.end method

.method private b(Ltd;)V
    .registers 3
    .parameter

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ltb;

    invoke-interface {v0, p1}, Ltb;->a(Ltd;)Z

    move-result v0

    .line 421
    if-eqz v0, :cond_c

    .line 422
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Z

    .line 427
    :goto_b
    return-void

    .line 424
    :cond_c
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Z

    .line 425
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()V

    goto :goto_b
.end method

.method private p()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 447
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 448
    iget-wide v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-ltz v0, :cond_34

    .line 449
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Z

    if-eqz v0, :cond_34

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->e()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 450
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, LsH;->discussion_non_anchored:I

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 452
    const/16 v1, 0x31

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LsB;->collaborator_padding_top:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v0, v1, v4, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 454
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 457
    :cond_34
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 301
    sget v0, LsF;->discussion_fragment_pager:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 303
    sget v0, LsD;->discussion_pager_loading:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->d:Landroid/view/View;

    .line 305
    sget v0, LsD;->discussion_pager_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(LaD;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 310
    return-object v1
.end method

.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ljava/util/List;

    return-object v0
.end method

.method public a()Ltd;
    .registers 4

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->a()I

    move-result v0

    .line 468
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 469
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_26

    .line 470
    new-instance v1, Ltd;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ljava/util/List;

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    invoke-direct {v1, v0}, Ltd;-><init>(Lmz;)V

    move-object v0, v1

    .line 474
    :goto_25
    return-object v0

    .line 471
    :cond_26
    const/4 v1, 0x1

    if-lt v0, v1, :cond_3a

    .line 472
    new-instance v1, Ltd;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ljava/util/List;

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    invoke-direct {v1, v0}, Ltd;-><init>(Lmz;)V

    move-object v0, v1

    goto :goto_25

    .line 474
    :cond_3a
    const/4 v0, 0x0

    goto :goto_25
.end method

.method public a()Ltm;
    .registers 2

    .prologue
    .line 104
    sget-object v0, Ltm;->c:Ltm;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 253
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->a(Landroid/os/Bundle;)V

    .line 254
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    if-nez v0, :cond_13

    .line 255
    new-instance v0, Lub;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->k()Z

    move-result v1

    invoke-direct {v0, p0, v1}, Lub;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    .line 257
    :cond_13
    invoke-static {p1}, Ltd;->a(Landroid/os/Bundle;)Ltd;

    move-result-object v0

    .line 258
    if-eqz v0, :cond_1b

    .line 259
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ltd;

    .line 261
    :cond_1b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lug;

    if-nez v0, :cond_27

    .line 262
    new-instance v0, Lug;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lug;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;Lue;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lug;

    .line 264
    :cond_27
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->d(Z)V

    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->k()Z

    move-result v0

    if-nez v0, :cond_34

    .line 268
    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->e(Z)V

    .line 272
    :goto_33
    return-void

    .line 270
    :cond_34
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->e(Z)V

    goto :goto_33
.end method

.method public a(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 277
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 278
    sget v0, LsG;->menu_phone_discussion_one_discussion:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 279
    sget v0, LsD;->menu_resolve_reopen:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/view/MenuItem;

    .line 280
    return-void
.end method

.method protected a(Ljava/util/SortedSet;)V
    .registers 12
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    const-string v0, "PagerDiscussionFragment"

    const-string v1, "Updating discussions"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()LsR;

    move-result-object v0

    invoke-interface {v0}, LsR;->a()Ljava/util/List;

    move-result-object v1

    .line 117
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 118
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 119
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 120
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 121
    sget-object v0, Lmz;->a:Lagv;

    invoke-static {p1, v0}, LajB;->a(Ljava/lang/Iterable;Lagv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_63

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 122
    invoke-interface {v0}, Lmz;->b()Ljava/lang/String;

    move-result-object v7

    .line 123
    if-eqz v7, :cond_47

    invoke-interface {v1, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_55

    .line 126
    :cond_47
    invoke-interface {v0}, Lmz;->d()Z

    move-result v7

    if-eqz v7, :cond_51

    .line 127
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2f

    .line 129
    :cond_51
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2f

    .line 133
    :cond_55
    invoke-interface {v0}, Lmz;->d()Z

    move-result v8

    if-eqz v8, :cond_5f

    .line 134
    invoke-interface {v5, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2f

    .line 136
    :cond_5f
    invoke-interface {v4, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2f

    .line 141
    :cond_63
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 142
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 143
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_71
    :goto_71
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 145
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmz;

    .line 146
    if-eqz v1, :cond_8e

    invoke-interface {v6, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_8e

    .line 147
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    :cond_8e
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 151
    if-eqz v0, :cond_71

    invoke-interface {v7, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_71

    .line 152
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_71

    .line 156
    :cond_a0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ljava/util/List;

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->c:Ljava/util/List;

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->c:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_cd

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lug;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Lbp;)V

    .line 167
    :cond_cd
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    if-eqz v0, :cond_e5

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ltd;

    if-eqz v0, :cond_e6

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->h()Z

    move-result v0

    if-nez v0, :cond_e6

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ltd;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ltd;)V

    .line 173
    :cond_e0
    :goto_e0
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    invoke-virtual {v0, p1}, Lub;->a(Ljava/util/SortedSet;)V

    .line 175
    :cond_e5
    return-void

    .line 170
    :cond_e6
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ltd;

    if-eqz v0, :cond_e0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->h()Z

    move-result v0

    if-nez v0, :cond_e0

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ltd;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ltd;)V

    goto :goto_e0
.end method

.method public a(LtX;)V
    .registers 3
    .parameter

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lma;

    if-nez v0, :cond_5

    .line 411
    :cond_4
    :goto_4
    return-void

    .line 407
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lma;

    invoke-interface {v0}, Lma;->a()Ljava/util/SortedSet;

    move-result-object v0

    .line 408
    if-eqz v0, :cond_4

    .line 409
    invoke-virtual {p1, v0}, LtX;->a(Ljava/util/SortedSet;)V

    goto :goto_4
.end method

.method public a(LtX;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/view/MenuItem;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    invoke-virtual {v0}, Lub;->a()LtX;

    move-result-object v0

    if-ne p1, v0, :cond_15

    .line 393
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/view/MenuItem;

    if-eqz p2, :cond_16

    sget v0, LsH;->discussion_reopen:I

    :goto_12
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 396
    :cond_15
    return-void

    .line 393
    :cond_16
    sget v0, LsH;->discussion_resolve:I

    goto :goto_12
.end method

.method public a(Ltd;)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 183
    if-nez p1, :cond_4

    .line 217
    :cond_3
    :goto_3
    return-void

    .line 186
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->h()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 188
    invoke-direct {p0, v3, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ltd;Ltd;)V

    goto :goto_3

    .line 191
    :cond_e
    const-string v0, "PagerDiscussionFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showOneDiscussion: discussion = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ltd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    invoke-virtual {p1}, Ltd;->a()Z

    move-result v0

    if-eqz v0, :cond_65

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ljava/util/List;

    .line 195
    :goto_32
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ljava/util/List;Ltd;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 202
    invoke-virtual {p1}, Ltd;->a()Z

    move-result v0

    if-eqz v0, :cond_68

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->c:Ljava/util/List;

    .line 203
    :goto_40
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ljava/util/List;Ltd;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 209
    invoke-direct {p0, v3, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ltd;Ltd;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->c:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lma;

    invoke-interface {v0}, Lma;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 214
    sget v0, LsH;->discussion_does_not_exist:I

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(II)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ltb;

    invoke-interface {v0}, Ltb;->p()V

    goto :goto_3

    .line 193
    :cond_65
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->c:Ljava/util/List;

    goto :goto_32

    .line 202
    :cond_68
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ljava/util/List;

    goto :goto_40
.end method

.method public a(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 284
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 285
    sget v2, LsD;->menu_comments:I

    if-ne v1, v2, :cond_f

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ltb;

    invoke-interface {v1}, Ltb;->q()V

    .line 295
    :goto_e
    return v0

    .line 288
    :cond_f
    sget v2, LsD;->menu_resolve_reopen:I

    if-ne v1, v2, :cond_1f

    .line 289
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    invoke-virtual {v1}, Lub;->a()LtX;

    move-result-object v1

    .line 290
    if-eqz v1, :cond_1f

    .line 291
    invoke-virtual {v1}, LtX;->b()V

    goto :goto_e

    .line 295
    :cond_1f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 349
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->c(Landroid/os/Bundle;)V

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ltd;

    invoke-static {p1, v0}, Ltd;->a(Landroid/os/Bundle;Ltd;)V

    .line 351
    return-void
.end method

.method public g()V
    .registers 4

    .prologue
    .line 315
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->g()V

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_d

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Lbp;)V

    .line 321
    :cond_d
    sget-object v0, Luh;->b:Luh;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Luh;)V

    .line 324
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LsD;->discussion_fragment_pager_container:I

    invoke-static {v0, v1, v2}, LsY;->a(Landroid/view/View;Landroid/content/res/Resources;I)V

    .line 326
    return-void
.end method

.method public h()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 330
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/BaseDiscussionFragment;->h()V

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ltd;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ltd;

    .line 334
    :goto_a
    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Ltd;Ltd;)V

    .line 337
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Ljava/util/List;

    .line 338
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ljava/util/List;

    .line 339
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->c:Ljava/util/List;

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    invoke-virtual {v0}, Lub;->a()V

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:Lub;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, v2}, Lub;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:LAQ;

    invoke-virtual {v0}, LAQ;->a()V

    .line 345
    return-void

    .line 333
    :cond_24
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->b:Ltd;

    goto :goto_a
.end method
