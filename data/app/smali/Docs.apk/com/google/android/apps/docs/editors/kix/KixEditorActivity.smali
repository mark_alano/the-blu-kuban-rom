.class public Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;
.super Lcom/google/android/apps/docs/editors/AbstractEditorActivity;
.source "KixEditorActivity.java"

# interfaces
.implements LsT;
.implements Lyl;
.implements LzO;


# instance fields
.field private a:LAQ;

.field public a:LAS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LBd;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LCL;

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LKt;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LLc;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LNe;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LNf;

.field public a:LPm;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LUL;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/net/Uri;

.field public a:Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

.field public a:Lcom/google/android/apps/docs/editors/kix/EditorStatusFragment;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

.field public a:Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

.field public a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lil;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Ljava/lang/Object;

.field public a:Ljava/lang/String;
    .annotation runtime Laon;
    .end annotation

    .annotation runtime LaqW;
        value = "DiscussionTrackerLabel"
    .end annotation
.end field

.field public a:Lkj;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LlX;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lma;

.field private a:Lth;

.field private a:Lti;

.field private a:Ltj;

.field public a:LxI;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lxt;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LyZ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lyz;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LzK;

.field private a:Lzc;

.field private a:Lzj;

.field private final a:Lzo;

.field private final b:Landroid/os/Handler;

.field public b:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final b:Ljava/lang/Object;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;-><init>()V

    .line 185
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Landroid/os/Handler;

    .line 200
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Ljava/lang/Object;

    .line 201
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/Object;

    .line 202
    new-instance v0, Lym;

    invoke-direct {v0, p0}, Lym;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzo;

    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->q:Z

    .line 222
    new-instance v0, Lyo;

    invoke-direct {v0, p0}, Lyo;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LNf;

    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter

    .prologue
    .line 827
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "mobilebasic"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Lcom/google/android/apps/docs/editors/kix/KixEditText;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 831
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 832
    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_d
    return-object v0

    :cond_e
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "disco"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_d
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)LzK;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LzK;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;LzK;)LzK;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LzK;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Lzj;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    return-object v0
.end method

.method private a(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 626
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 627
    invoke-virtual {p0, p2}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 628
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/String;

    sget-object v3, LfS;->a:LfS;

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/docs/doclist/dialogs/DocumentOpenerErrorDialogFragment;->a(Lo;Ljava/lang/String;Ljava/lang/String;LfS;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 630
    return-void
.end method

.method private a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V
    .registers 7
    .parameter

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LsC;->left_edge_fade:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 378
    const/4 v1, 0x0

    .line 379
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LsC;->right_edge_fade:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 380
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, LsC;->bottom_edge_fade:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 381
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setPaddingDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 382
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->i()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;ZZ)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a(ZZ)V

    return-void
.end method

.method private a(ZZ)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 388
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    if-eqz p1, :cond_10

    if-eqz p2, :cond_10

    move v0, v1

    :goto_8
    invoke-virtual {v2, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setCanComment(Z)V

    .line 389
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->j:Z

    if-nez v0, :cond_12

    .line 445
    :goto_f
    return-void

    .line 388
    :cond_10
    const/4 v0, 0x0

    goto :goto_8

    .line 393
    :cond_12
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->m:Z

    if-eqz v0, :cond_19

    .line 397
    iput-boolean p2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->l:Z

    goto :goto_f

    .line 400
    :cond_19
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->m:Z

    .line 401
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->k:Z

    .line 402
    iput-boolean p2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->l:Z

    .line 403
    if-nez p1, :cond_2e

    .line 404
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lti;

    invoke-virtual {v0, v1}, Lyz;->removeInitializationListener(LyH;)V

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lti;

    invoke-virtual {v0}, Lti;->a()V

    goto :goto_f

    .line 409
    :cond_2e
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LlX;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, LlX;->a(Ljava/lang/String;Ljava/lang/String;)Lma;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lma;

    .line 410
    new-instance v0, Ltj;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LeQ;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, Ltj;-><init>(LeQ;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Ltj;

    .line 412
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Ljava/lang/String;

    move-result-object v0

    .line 413
    if-eqz v0, :cond_55

    .line 414
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    invoke-virtual {v2, v0}, Lth;->b(Ljava/lang/String;)V

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Ltj;

    invoke-virtual {v0}, Ltj;->c()V

    .line 417
    :cond_55
    new-instance v0, LAQ;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LBd;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    invoke-direct {v0, v2, v3}, LAQ;-><init>(LBd;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LAQ;

    .line 419
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    const-string v2, "discussionCoordinator"

    invoke-virtual {v0, v2}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    if-nez v0, :cond_93

    .line 423
    new-instance v0, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    .line 424
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    const-string v3, "discussionCoordinator"

    invoke-virtual {v0, v2, v3}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 428
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Z

    .line 431
    :cond_93
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->n:Z

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    if-eqz v0, :cond_a0

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->n:Z

    invoke-virtual {v0, v1}, LCL;->b(Z)V

    .line 437
    :cond_a0
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyl;)V

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lth;)V

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lth;)V

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    invoke-virtual {v0, v1}, Lyz;->setDiscussionInsertListener(LyI;)V

    .line 444
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lti;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    invoke-virtual {v0, v1}, Lti;->a(Lth;)V

    goto/16 :goto_f
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->p:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->p:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->k()V

    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 848
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LZL;->d(Landroid/content/res/Resources;)Z

    move-result v0

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->q:Z

    return v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 85
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->r:Z

    return p1
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->n()V

    return-void
.end method

.method private c(Z)V
    .registers 5
    .parameter

    .prologue
    .line 875
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 876
    const-string v1, "IsUpAffordance"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 877
    new-instance v1, Lcom/google/android/apps/docs/editors/kix/UnsavedChangesDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/editors/kix/UnsavedChangesDialogFragment;-><init>()V

    .line 878
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/kix/UnsavedChangesDialogFragment;->d(Landroid/os/Bundle;)V

    .line 879
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/docs/editors/kix/UnsavedChangesDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 880
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->r:Z

    return v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Z
    .registers 2
    .parameter

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->o:Z

    return v0
.end method

.method private g()V
    .registers 3

    .prologue
    .line 448
    sget v0, LsD;->logo:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 449
    new-instance v1, Lyp;

    invoke-direct {v1, p0}, Lyp;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 458
    return-void
.end method

.method private h()V
    .registers 5

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->r:Z

    if-nez v0, :cond_f

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LNe;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    const-string v2, "writely"

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LNf;

    invoke-interface {v0, v1, v2, v3}, LNe;->a(Ljava/lang/String;Ljava/lang/String;LNf;)V

    .line 475
    :cond_f
    return-void
.end method

.method private i()V
    .registers 5

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKt;

    invoke-virtual {v1}, LKt;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setIsTestMode(Z)V

    .line 480
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->i:Z

    if-eqz v0, :cond_10c

    .line 481
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    const-string v2, "MenuHandler"

    invoke-virtual {v0, v1, v2}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 488
    :goto_22
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzc;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lzc;)V

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V

    .line 492
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzc;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;->a(Lzc;)V

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setCustomCursorPopup(LDY;)V

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setCustomSelectionMode(LER;)V

    .line 502
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LBd;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(LAY;)V

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    invoke-virtual {v0, v1}, Lyz;->addUpdateListener(LyU;)V

    .line 505
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    invoke-virtual {v0, v1}, Lyz;->addInitializationListener(LyH;)V

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    invoke-virtual {v0, v1}, Lyz;->addNetworkStatusListener(LyQ;)V

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    invoke-virtual {v0, v1}, Lyz;->addCollaboratorListener(LyF;)V

    .line 509
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-virtual {v0, v1}, Lyz;->addInitializationListener(LyH;)V

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, v1}, Lyz;->addInitializationListener(LyH;)V

    .line 511
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzc;

    invoke-virtual {v0, v1}, Lyz;->addInitializationListener(LyH;)V

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzc;

    invoke-virtual {v0, v1}, Lyz;->addUpdateListener(LyU;)V

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    new-instance v1, Lyq;

    invoke-direct {v1, p0}, Lyq;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    invoke-virtual {v0, v1}, Lyz;->addInitializationListener(LyH;)V

    .line 538
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->j()V

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    invoke-virtual {v0, v1}, Lyz;->setViewProvisionListener(LyX;)V

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(LzH;)V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(Lzj;)V

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    new-instance v1, Lyr;

    invoke-direct {v1, p0}, Lyr;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    invoke-virtual {v0, v1}, Lyz;->setErrorHandler(LyK;)V

    .line 581
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    sget v1, LsD;->kix_editor_view:I

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/collab/CollaboratorFragment;

    const-string v3, "collaboratorFragment"

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    sget v1, LsD;->kix_editor_view:I

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/EditorStatusFragment;

    const-string v3, "editorStatusFragment"

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    sget v1, LsD;->kix_editor_view:I

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    const-string v3, "selectionPopup"

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    sget v1, LsD;->kix_editor_view:I

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Lcom/google/android/apps/docs/editors/kix/popup/TextSelectionPopup;

    const-string v3, "pastePopup"

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    sget v1, LsD;->kix_editor_view:I

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;

    const-string v3, "savedStateFragment"

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    new-instance v1, Lyv;

    invoke-direct {v1, p0}, Lyv;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    invoke-virtual {v0, v1}, Lyz;->addNetworkStatusListener(LyQ;)V

    .line 608
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->o()V

    .line 610
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->m()V

    .line 611
    return-void

    .line 484
    :cond_10c
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    sget v1, LsD;->title_bar_container:I

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    const-string v3, "MenuHandler"

    invoke-virtual {v0, v1, v2, v3}, Lz;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    goto/16 :goto_22
.end method

.method private j()V
    .registers 3

    .prologue
    .line 614
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->j:Z

    if-eqz v0, :cond_15

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lti;

    invoke-virtual {v0, v1}, Lyz;->addInitializationListener(LyH;)V

    .line 616
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    new-instance v1, Lyw;

    invoke-direct {v1, p0}, Lyw;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    invoke-virtual {v0, v1}, Lyz;->setDocosMetadataListener(LyJ;)V

    .line 623
    :cond_15
    return-void
.end method

.method private k()V
    .registers 2

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;->h()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 684
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->c(Z)V

    .line 688
    :goto_c
    return-void

    .line 686
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->f()V

    goto :goto_c
.end method

.method private l()V
    .registers 4

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LzK;

    if-eqz v0, :cond_c

    .line 696
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LzK;

    invoke-virtual {v0}, LzK;->a()V

    .line 697
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LzK;

    .line 700
    :cond_c
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "This will modify (more like cripple) the content of your document.\nAre you sure?"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Yep"

    new-instance v2, Lyy;

    invoke-direct {v2, p0}, Lyy;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Nope"

    new-instance v2, Lyx;

    invoke-direct {v2, p0}, Lyx;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 718
    return-void
.end method

.method private m()V
    .registers 3

    .prologue
    .line 815
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Landroid/os/Handler;

    new-instance v1, Lyn;

    invoke-direct {v1, p0}, Lyn;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 824
    return-void
.end method

.method private n()V
    .registers 2

    .prologue
    .line 844
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->c()V

    .line 845
    return-void
.end method

.method private o()V
    .registers 6

    .prologue
    .line 853
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 854
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, LsB;->collaborator_padding_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 855
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, LsB;->kix_edit_bottom_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 856
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LsB;->kix_edit_left_padding_tablet:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 857
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, LsB;->kix_edit_right_padding_tablet:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 858
    sget v4, LsD;->editor:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v2, v0, v3, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 860
    :cond_37
    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 866
    invoke-static {}, LZL;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 867
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 868
    if-eqz v0, :cond_10

    .line 869
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 872
    :cond_10
    return-void
.end method


# virtual methods
.method public a()LAQ;
    .registers 2

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LAQ;

    return-object v0
.end method

.method public a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;
    .registers 2

    .prologue
    .line 913
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    return-object v0
.end method

.method public a()Lma;
    .registers 2

    .prologue
    .line 928
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lma;

    return-object v0
.end method

.method public a()LsR;
    .registers 2

    .prologue
    .line 923
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    return-object v0
.end method

.method public a()Lte;
    .registers 2

    .prologue
    .line 933
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Ltj;

    return-object v0
.end method

.method public a()Lyz;
    .registers 2

    .prologue
    .line 840
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 902
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->d()V

    .line 903
    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .registers 3
    .parameter

    .prologue
    .line 761
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lyz;

    move-result-object v0

    invoke-virtual {v0}, Lyz;->delete()V

    .line 762
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a(Ljava/lang/Throwable;)V

    .line 763
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 938
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->l:Z

    return v0
.end method

.method public a_(Z)V
    .registers 3
    .parameter

    .prologue
    .line 894
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 895
    if-eqz p1, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    if-eqz v0, :cond_10

    .line 896
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->p()V

    .line 898
    :cond_10
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 907
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    invoke-virtual {v0}, Lth;->a()V

    .line 908
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->e()V

    .line 909
    return-void
.end method

.method public b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 884
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 885
    if-eqz p1, :cond_b

    .line 886
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->f()V

    .line 890
    :goto_a
    return-void

    .line 888
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->finish()V

    goto :goto_a
.end method

.method public d()V
    .registers 3

    .prologue
    .line 636
    sget v0, LsH;->open_document_failed:I

    sget v1, LsH;->open_document_failed_expanded:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a(II)V

    .line 637
    return-void
.end method

.method public e()V
    .registers 3

    .prologue
    .line 643
    sget v0, LsH;->document_deleted:I

    sget v1, LsH;->document_deleted_expanded:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a(II)V

    .line 644
    return-void
.end method

.method f()V
    .registers 3

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lil;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    invoke-interface {v0, p0, v1}, Lil;->a(Landroid/app/Activity;Ljava/lang/String;)V

    .line 692
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 14
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 252
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 253
    const-string v0, "KixEditorActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Opening "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKS;

    const-string v3, "kixForcePathRenderSDKCutoff"

    const/16 v4, 0xb

    invoke-interface {v0, v3, v4}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, LDT;->a(I)V

    .line 259
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 262
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKt;

    invoke-virtual {v3, v0}, LKt;->a(Landroid/content/Intent;)V

    .line 264
    new-instance v3, LJX;

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKt;

    invoke-virtual {v4}, LKt;->a()Z

    move-result v4

    invoke-direct {v3, v0, v4}, LJX;-><init>(Landroid/content/Intent;Z)V

    .line 265
    invoke-virtual {v3, p0}, LJX;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    .line 266
    invoke-virtual {v3}, LJX;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/String;

    .line 267
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/String;

    if-nez v4, :cond_67

    .line 268
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKS;

    const-string v5, "kixDebugDocumentId"

    const-string v6, "1ZU-wer8Dh1bByaHDzjlfHr0dv1kGhEaF61so47qii88"

    invoke-interface {v4, v5, v6}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/String;

    .line 272
    :cond_67
    const-string v4, "document/d"

    iget-object v5, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LJX;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->c:Ljava/lang/String;

    .line 273
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->c:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Landroid/net/Uri;

    .line 275
    invoke-virtual {v3}, LJX;->a()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->o:Z

    .line 277
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKt;

    invoke-virtual {v4}, LKt;->a()Z

    move-result v4

    if-eqz v4, :cond_9a

    .line 279
    new-instance v4, LLf;

    iget-object v5, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LLc;

    iget-object v6, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    sget-object v7, Lfo;->d:Lfo;

    iget-object v7, v7, Lfo;->c:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v7}, LLf;-><init>(LLc;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 283
    :try_start_97
    invoke-virtual {v4}, Ljava/lang/Thread;->join()V
    :try_end_9a
    .catch Ljava/lang/InterruptedException; {:try_start_97 .. :try_end_9a} :catch_21b

    .line 289
    :cond_9a
    :goto_9a
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LeQ;

    const-string v5, "/kixEditor"

    invoke-virtual {v4, v5, v0}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LeQ;

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/Object;

    invoke-virtual {v0, v4}, LeQ;->a(Ljava/lang/Object;)V

    .line 292
    new-instance v0, Lzc;

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LPm;

    iget-object v5, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/String;

    invoke-virtual {v3}, LJX;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, p0, v4, v5, v6}, Lzc;-><init>(Landroid/content/Context;LPm;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzc;

    .line 294
    if-eqz p1, :cond_c9

    const-string v0, "editRecorded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c9

    .line 295
    const-string v0, "editRecorded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->p:Z

    .line 298
    :cond_c9
    invoke-static {}, LaaF;->a()Lfo;

    move-result-object v0

    .line 299
    sget-object v4, Lfo;->d:Lfo;

    invoke-virtual {v0, v4}, Lfo;->a(Lfo;)Z

    move-result v0

    if-nez v0, :cond_df

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKS;

    const-string v4, "kixShowDebugMenu"

    invoke-interface {v0, v4, v1}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_225

    :cond_df
    move v0, v2

    :goto_e0
    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->s:Z

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKS;

    const-string v4, "kixShowMobixButton"

    invoke-interface {v0, v4, v2}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->t:Z

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKt;

    invoke-virtual {v0}, LKt;->b()Z

    move-result v0

    if-nez v0, :cond_108

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lgl;

    sget-object v4, Lgi;->s:Lgi;

    invoke-interface {v0, v4}, Lgl;->a(Lgi;)Z

    move-result v0

    if-eqz v0, :cond_228

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKS;

    const-string v4, "enableKixEditorComments"

    invoke-interface {v0, v4, v2}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_228

    :cond_108
    :goto_108
    iput-boolean v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->j:Z

    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    const-string v2, "MenuHandler"

    invoke-virtual {v0, v2}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    if-nez v0, :cond_127

    .line 310
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->i:Z

    if-eqz v0, :cond_22b

    new-instance v0, Lcom/google/android/apps/docs/editors/kix/menu/ActionBarMenuHandler;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/kix/menu/ActionBarMenuHandler;-><init>()V

    :goto_125
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    .line 313
    :cond_127
    sget v0, LsF;->kix_editor:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->setContentView(I)V

    .line 315
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->i:Z

    if-nez v0, :cond_133

    .line 316
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->g()V

    .line 319
    :cond_133
    invoke-virtual {v3}, LJX;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->e:Ljava/lang/String;

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->e:Ljava/lang/String;

    if-eqz v0, :cond_153

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 322
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->i:Z

    if-nez v0, :cond_153

    .line 323
    sget v0, LsD;->title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    :cond_153
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LAS;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    invoke-virtual {v0, v2}, LAS;->a(LxZ;)V

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LAS;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LBd;

    invoke-virtual {v0, v2}, LAS;->a(LBd;)V

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKt;

    invoke-virtual {v0}, LKt;->a()Z

    move-result v0

    if-nez v0, :cond_185

    invoke-virtual {v3}, LJX;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_185

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-direct {v2}, Lcom/google/android/apps/docs/editors/SharingFragment;-><init>()V

    const-string v3, "SharingFragment"

    invoke-virtual {v0, v2, v3}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 335
    :cond_185
    sget v0, LsD;->editor:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lgl;

    sget-object v3, Lgi;->r:Lgi;

    invoke-interface {v2, v3}, Lgl;->a(Lgi;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setProfilingEnabled(Z)V

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V

    .line 339
    new-instance v7, LzQ;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->e:Ljava/lang/String;

    invoke-direct {v7, p0, v0, v2, v3}, LzQ;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->j:Z

    if-eqz v0, :cond_1ec

    .line 350
    new-instance v0, Lth;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-direct {v0, v2}, Lth;-><init>(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyj;)V

    .line 357
    new-instance v0, Lti;

    invoke-direct {v0}, Lti;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lti;

    .line 359
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->n:Z

    .line 360
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->m:Z

    .line 361
    if-eqz p1, :cond_1ec

    const-string v0, "discussionDocosEnabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1ec

    .line 362
    const-string v0, "discussionDocosEnabled"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->k:Z

    .line 363
    const-string v0, "discussionCanComment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->l:Z

    .line 364
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->k:Z

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->l:Z

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a(ZZ)V

    .line 368
    :cond_1ec
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lgl;

    sget-object v1, Lgi;->u:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v11

    .line 370
    new-instance v0, Lzj;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKS;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LAS;

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-boolean v5, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->o:Z

    iget-boolean v6, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->j:Z

    iget-object v8, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LxI;

    iget-object v9, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    invoke-virtual {v9}, Lyz;->getRectangleFactory()Lzi;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    invoke-direct/range {v0 .. v11}, Lzj;-><init>(Landroid/content/Context;LKS;LAS;Lcom/google/android/apps/docs/editors/kix/KixEditText;ZZLzP;LxI;Lzi;Lth;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzo;

    invoke-virtual {v0, v1}, Lzj;->a(Lzo;)V

    .line 374
    return-void

    .line 284
    :catch_21b
    move-exception v4

    .line 285
    const-string v5, "KixEditorActivity"

    const-string v6, "Waiting for client flag sync interrupted"

    invoke-static {v5, v6, v4}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_9a

    :cond_225
    move v0, v1

    .line 299
    goto/16 :goto_e0

    :cond_228
    move v2, v1

    .line 303
    goto/16 :goto_108

    .line 310
    :cond_22b
    new-instance v0, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;-><init>()V

    goto/16 :goto_125
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 649
    new-instance v0, LCL;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->s:Z

    invoke-direct {v0, p0, v1}, LCL;-><init>(Lcom/google/android/apps/docs/RoboFragmentActivity;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    .line 650
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->o:Z

    invoke-virtual {v0, v1}, LCL;->a(Z)V

    .line 651
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->n:Z

    invoke-virtual {v0, v1}, LCL;->b(Z)V

    .line 652
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->o:Z

    invoke-virtual {v0, v1}, LCL;->c(Z)V

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->t:Z

    invoke-virtual {v0, v1}, LCL;->d(Z)V

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    invoke-virtual {v0, p1}, LCL;->a(Landroid/view/Menu;)V

    .line 655
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 4

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 791
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lth;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b(Lyj;)V

    .line 792
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    invoke-virtual {v0}, Lzj;->b()V

    .line 793
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/Object;

    const-string v2, "/kixEditor"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 794
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 795
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    invoke-virtual {v0}, Lyz;->delete()V

    .line 796
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onDestroy()V

    .line 797
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 801
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LdL;

    invoke-interface {v1, p0}, LdL;->a(Landroid/content/Context;)V

    .line 802
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/docs/editors/kix/menu/MenuHandler;->a(ILandroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 811
    :goto_e
    return v0

    .line 805
    :cond_f
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1f

    .line 806
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/SavedStateFragment;->h()Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 807
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->c(Z)V

    goto :goto_e

    .line 811
    :cond_1f
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_e
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter

    .prologue
    .line 660
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 661
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 663
    sget v1, LsD;->menu_traffic_test:I

    if-ne v0, v1, :cond_12

    .line 664
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->l()V

    .line 679
    :cond_10
    :goto_10
    const/4 v0, 0x1

    :goto_11
    return v0

    .line 665
    :cond_12
    sget v1, LsD;->menu_webview_mode:I

    if-ne v0, v1, :cond_2a

    .line 666
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LyZ;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LyZ;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_10

    .line 667
    :cond_2a
    const v1, 0x102002c

    if-ne v0, v1, :cond_33

    .line 668
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->k()V

    goto :goto_10

    .line 669
    :cond_33
    sget v1, LsD;->menu_discussion:I

    if-ne v0, v1, :cond_41

    .line 670
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    if-eqz v0, :cond_10

    .line 671
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->t()V

    goto :goto_10

    .line 673
    :cond_41
    sget v1, LsD;->menu_edit:I

    if-ne v0, v1, :cond_49

    .line 674
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->n()V

    goto :goto_10

    .line 675
    :cond_49
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    if-eqz v0, :cond_55

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LCL;

    invoke-virtual {v0, p1}, LCL;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 676
    :cond_55
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_11
.end method

.method protected onPause()V
    .registers 4

    .prologue
    .line 767
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 768
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzo;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    if-eqz v0, :cond_14

    .line 769
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzo;

    invoke-virtual {v0, v1}, Lzj;->b(Lzo;)V

    .line 772
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LBd;

    if-eqz v0, :cond_1d

    .line 773
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LBd;

    invoke-virtual {v0}, LBd;->d()V

    .line 776
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    if-eqz v0, :cond_26

    .line 777
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    invoke-virtual {v0}, Lyz;->onPause()V

    .line 780
    :cond_26
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lxt;

    invoke-virtual {v0}, Lxt;->a()V

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Ljava/lang/Object;

    const-string v2, "kixActiveTime"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 783
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->q:Z

    .line 785
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onPause()V

    .line 786
    return-void
.end method

.method protected onResume()V
    .registers 4

    .prologue
    .line 722
    const-string v0, "KixEditorActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onResume()V

    .line 724
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->q:Z

    .line 725
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LeQ;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LeQ;->a(Ljava/lang/Object;)V

    .line 727
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    if-eqz v0, :cond_1d

    .line 728
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    invoke-virtual {v0}, Lyz;->onResume()V

    .line 731
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lxt;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lyz;

    invoke-virtual {v0, v1, v2}, Lxt;->a(Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;Lyz;)V

    .line 733
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LBd;

    if-eqz v0, :cond_2f

    .line 734
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LBd;

    invoke-virtual {v0}, LBd;->c()V

    .line 738
    :cond_2f
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    if-eqz v0, :cond_3a

    .line 739
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzj;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lzo;

    invoke-virtual {v0, v1}, Lzj;->a(Lzo;)V

    .line 742
    :cond_3a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKt;

    invoke-virtual {v0}, LKt;->a()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 744
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LNf;

    invoke-interface {v0}, LNf;->a()V

    .line 748
    :goto_47
    return-void

    .line 746
    :cond_48
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->h()V

    goto :goto_47
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 462
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 463
    const-string v0, "editRecorded"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 464
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->m:Z

    if-eqz v0, :cond_1c

    .line 466
    const-string v0, "discussionDocosEnabled"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 467
    const-string v0, "discussionCanComment"

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 469
    :cond_1c
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 2
    .parameter

    .prologue
    .line 752
    if-eqz p1, :cond_5

    .line 755
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->p()V

    .line 757
    :cond_5
    return-void
.end method
