.class public abstract Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;
.super Lcom/google/android/apps/docs/editors/kix/DecoratedEditText;
.source "ZoomableEditText.java"

# interfaces
.implements LzS;


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/kix/DecoratedEditText;-><init>(Landroid/content/Context;)V

    .line 21
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a:F

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/editors/kix/DecoratedEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a:F

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/kix/DecoratedEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    const/high16 v0, 0x3f80

    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a:F

    .line 36
    return-void
.end method

.method private a()LzT;
    .registers 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a:LEj;

    instance-of v0, v0, LzT;

    if-nez v0, :cond_d

    .line 79
    const-string v0, "ZoomableEditText"

    const-string v1, "Layout is not of type ZoomedLayout in a ZoomableEditText"

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a:LEj;

    check-cast v0, LzT;

    return-object v0
.end method


# virtual methods
.method public a()F
    .registers 2

    .prologue
    .line 50
    iget v0, p0, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a:F

    return v0
.end method

.method protected a(ILandroid/text/Layout$Alignment;I)LEj;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    new-instance v0, LDV;

    invoke-direct {v0}, LDV;-><init>()V

    .line 63
    invoke-super {p0, v0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/kix/DecoratedEditText;->a(LFg;ILandroid/text/Layout$Alignment;I)LEj;

    move-result-object v0

    .line 66
    new-instance v1, LzT;

    invoke-direct {v1, v0}, LzT;-><init>(LEj;)V

    .line 67
    iget v0, p0, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a:F

    invoke-virtual {v1, v0}, LzT;->a_(F)V

    .line 68
    iget v0, p0, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a:F

    invoke-virtual {v1, v0}, LzT;->b(F)V

    .line 69
    return-object v1
.end method

.method public a(F)V
    .registers 3
    .parameter

    .prologue
    .line 44
    iput p1, p0, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a:F

    .line 45
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a()LzT;

    move-result-object v0

    invoke-virtual {v0, p1}, LzT;->a_(F)V

    .line 46
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a()LzT;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a()F

    move-result v1

    invoke-virtual {v0, v1}, LzT;->b(F)V

    .line 58
    return-void
.end method
