.class public Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;
.super Landroid/widget/FrameLayout;
.source "FrameBoxView.java"

# interfaces
.implements LzZ;


# instance fields
.field private a:I

.field private final a:LDG;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDG",
            "<",
            "LzU;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LzX;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LDG;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LDG",
            "<",
            "LzU;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 29
    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:I

    .line 30
    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->b:I

    .line 31
    new-instance v0, LzX;

    invoke-direct {v0, p0}, LzX;-><init>(LzZ;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:LzX;

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:LDG;

    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a()V

    .line 40
    return-void
.end method

.method private a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:LDG;

    invoke-virtual {v0, p0}, LDG;->a(Ljava/lang/Object;)V

    .line 53
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->setClipChildren(Z)V

    .line 54
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->setClipToPadding(Z)V

    .line 55
    return-void
.end method


# virtual methods
.method public a()LDG;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDG",
            "<",
            "LzU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:LDG;

    return-object v0
.end method

.method public bridge synthetic a()Landroid/view/View;
    .registers 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a()Landroid/view/ViewGroup;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/view/ViewGroup;
    .registers 1

    .prologue
    .line 95
    return-object p0
.end method

.method public a()LzX;
    .registers 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:LzX;

    return-object v0
.end method

.method public a(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 116
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:LzX;

    invoke-virtual {v0, p1}, LzX;->a(Landroid/graphics/Canvas;)V

    .line 111
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:LzX;

    invoke-virtual {v0, p1}, LzX;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 70
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 72
    :cond_b
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 76
    invoke-static {p0, p1}, LzW;->a(Landroid/view/View;Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 77
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 79
    :cond_9
    return-void
.end method

.method protected onMeasure(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/high16 v2, 0x4000

    .line 63
    iget v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:I

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->b:I

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {p0, v0, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 65
    return-void
.end method

.method public setHeight(I)V
    .registers 2
    .parameter

    .prologue
    .line 89
    iput p1, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->b:I

    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->requestLayout()V

    .line 91
    return-void
.end method

.method public setIsClipped(Z)V
    .registers 3
    .parameter

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:LzX;

    invoke-virtual {v0, p1}, LzX;->a(Z)V

    .line 126
    return-void
.end method

.method public setMargin(IIII)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 100
    invoke-static {p0, p1, p2, p3, p4}, LzW;->a(Landroid/view/View;IIII)V

    .line 101
    return-void
.end method

.method public setWidth(I)V
    .registers 2
    .parameter

    .prologue
    .line 83
    iput p1, p0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->a:I

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->requestLayout()V

    .line 85
    return-void
.end method
