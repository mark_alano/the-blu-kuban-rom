.class public Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;
.super Landroid/view/View;
.source "TextBoxView.java"


# instance fields
.field private final a:LDk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDk",
            "<*",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private a:LEa;

.field private final a:Landroid/text/Spannable;

.field private final a:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;LDk;)V
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LDk",
            "<*",
            "LAx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LEa;

    .line 34
    invoke-virtual {p2}, LDk;->a()LDp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:Landroid/text/Spannable;

    .line 35
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:Landroid/text/TextPaint;

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LDk;

    .line 37
    return-void
.end method

.method private static a(II)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 55
    const/high16 v1, 0x4000

    if-ne v0, v1, :cond_d

    .line 56
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p0

    .line 60
    :cond_c
    :goto_c
    return p0

    .line 57
    :cond_d
    const/high16 v1, -0x8000

    if-ne v0, v1, :cond_c

    .line 58
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_c
.end method


# virtual methods
.method public a()LEj;
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LEa;

    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 42
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 44
    :cond_12
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 3
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LEa;

    if-eqz v0, :cond_9

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LEa;

    invoke-virtual {v0, p1}, LEa;->a(Landroid/graphics/Canvas;)V

    .line 51
    :cond_9
    return-void
.end method

.method protected onMeasure(II)V
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 68
    if-nez v0, :cond_40

    .line 70
    const/16 v4, 0x64

    .line 76
    :goto_8
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LEa;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LEa;

    invoke-virtual {v0}, LEa;->a()I

    move-result v0

    if-eq v4, v0, :cond_2a

    .line 77
    :cond_14
    new-instance v0, LEa;

    new-instance v1, LDV;

    invoke-direct {v1}, LDV;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:Landroid/text/Spannable;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f80

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, LEa;-><init>(LFg;Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LEa;

    .line 84
    :cond_2a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LEa;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a:LEa;

    invoke-virtual {v1}, LEa;->d()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LEa;->b(I)I

    move-result v0

    .line 87
    invoke-static {v0, p2}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a(II)I

    move-result v0

    .line 88
    invoke-virtual {p0, v4, v0}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->setMeasuredDimension(II)V

    .line 89
    return-void

    .line 73
    :cond_40
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    goto :goto_8
.end method
