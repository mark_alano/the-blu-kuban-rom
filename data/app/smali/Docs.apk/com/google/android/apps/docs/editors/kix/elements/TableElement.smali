.class public Lcom/google/android/apps/docs/editors/kix/elements/TableElement;
.super Ljava/lang/Object;
.source "TableElement.java"

# interfaces
.implements LAx;


# instance fields
.field private a:LCe;

.field private final a:LDA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDA",
            "<",
            "LAx;",
            "LzU;",
            ">;"
        }
    .end annotation
.end field

.field private a:LDI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDI",
            "<",
            "LzU;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LDb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDb",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LwL;

.field private final a:Lwp;

.field private final a:Lxu;

.field private a:LzU;

.field private final a:Lzd;


# direct methods
.method public constructor <init>(LDb;Lwp;LwL;Lzd;Lxu;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDb",
            "<",
            "LAx;",
            ">;",
            "Lwp;",
            "LwL;",
            "Lzd;",
            "Lxu;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LzU;

    .line 84
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LCe;

    .line 85
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDI;

    .line 89
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    invoke-virtual {v0, p0}, LDb;->a(Ljava/lang/Object;)V

    .line 91
    new-instance v0, LAI;

    invoke-direct {v0, p0}, LAI;-><init>(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    invoke-virtual {v0, p0}, LDA;->a(Ljava/lang/Object;)V

    .line 146
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lwp;

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lwp;

    invoke-interface {v0}, Lwp;->b()V

    .line 148
    iput-object p3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LwL;

    .line 149
    iput-object p4, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lzd;

    .line 150
    iput-object p5, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lxu;

    .line 151
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;)LCe;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LCe;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;)LDA;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;)LDI;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDI;

    return-object v0
.end method

.method private a()Ljava/lang/String;
    .registers 5

    .prologue
    const/16 v3, 0x20

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lxu;

    invoke-interface {v0}, Lxu;->a()Landroid/content/Context;

    move-result-object v0

    .line 242
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, LsH;->uneditable_table:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;)Lxu;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lxu;

    return-object v0
.end method


# virtual methods
.method public a()LDG;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LDG",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    return-object v0
.end method

.method public a(IILjava/util/List;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 236
    return-void
.end method

.method public a(Lwj;Lza;I)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 156
    const-wide/high16 v3, 0x408e

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lzd;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    invoke-interface {v0, v1}, Lzd;->a(I)Lvq;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_26

    .line 159
    invoke-interface {v0}, Lvq;->c()D

    move-result-wide v3

    invoke-interface {v0}, Lvq;->a()D

    move-result-wide v5

    sub-double/2addr v3, v5

    invoke-interface {v0}, Lvq;->b()D

    move-result-wide v0

    sub-double v0, v3, v0

    const-wide v3, 0x4001c71c71c71c72L

    mul-double/2addr v3, v0

    .line 163
    :cond_26
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lwp;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    invoke-virtual {v1}, LDb;->c()I

    move-result v1

    const-wide v5, 0x47efffffe0000000L

    iget-object v7, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LwL;

    move v8, v2

    invoke-interface/range {v0 .. v8}, Lwp;->a(IIDDLwL;Z)Lwl;

    .line 165
    const-string v0, "TableElement"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Table was "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    invoke-virtual {v3}, LDb;->a()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " now "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lwp;

    invoke-interface {v3}, Lwp;->a()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lwp;

    invoke-interface {v1}, Lwp;->a()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    invoke-virtual {v3}, LDb;->a()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {v0, v1}, LDb;->a(I)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    invoke-virtual {v0}, LDb;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0, v2}, Lwj;->a(II)V

    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a()Ljava/lang/String;

    move-result-object v0

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LCe;

    if-nez v1, :cond_fc

    .line 174
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lwp;

    invoke-interface {v1}, Lwp;->a()Ljava/lang/String;

    move-result-object v1

    .line 175
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lxu;

    invoke-interface {v3}, Lxu;->a()Lzs;

    move-result-object v3

    invoke-interface {v3, v1}, Lzs;->a(Ljava/lang/String;)LCJ;

    move-result-object v1

    invoke-interface {v1}, LCJ;->a()LzU;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LzU;

    .line 176
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LzU;

    invoke-interface {v3}, LzU;->a()LDG;

    move-result-object v3

    invoke-virtual {v1, v3}, LDA;->a(LDG;)V

    .line 178
    new-instance v1, LDI;

    invoke-direct {v1}, LDI;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDI;

    .line 179
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDI;

    invoke-virtual {v1, v3}, LDA;->a(LDG;)V

    .line 181
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDI;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LzU;

    invoke-virtual {v1, v3}, LDI;->a(Ljava/lang/Object;)V

    .line 182
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDI;

    invoke-virtual {v1, v0}, LDI;->a(Ljava/lang/CharSequence;)LDI;

    .line 183
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDI;

    const-string v3, "\n"

    invoke-virtual {v1, v3}, LDI;->a(Ljava/lang/CharSequence;)LDI;

    .line 184
    new-instance v1, LCe;

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LzU;

    invoke-interface {v3}, LzU;->a()Landroid/view/View;

    move-result-object v3

    invoke-direct {v1, v3}, LCe;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LCe;

    .line 185
    new-instance v1, Lcom/google/android/apps/docs/editors/kix/elements/TableElement$ReplacementSpanParent;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/docs/editors/kix/elements/TableElement$ReplacementSpanParent;-><init>(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;LAI;)V

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LzU;

    invoke-interface {v3}, LzU;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/docs/editors/kix/elements/TableElement$ReplacementSpanParent;->addView(Landroid/view/View;)V

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    new-instance v3, LAJ;

    invoke-direct {v3, p0}, LAJ;-><init>(Lcom/google/android/apps/docs/editors/kix/elements/TableElement;)V

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    invoke-virtual {v4}, LDA;->length()I

    move-result v4

    const/16 v5, 0x12

    invoke-virtual {v1, v3, v2, v4, v5}, LDA;->setSpan(Ljava/lang/Object;III)V

    .line 191
    :cond_fc
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lwp;

    invoke-interface {v1, v2}, Lwp;->a(I)V

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LzU;

    invoke-interface {v1}, LzU;->a()Landroid/view/View;

    move-result-object v1

    .line 193
    invoke-virtual {v1, v2, v2}, Landroid/view/View;->measure(II)V

    .line 194
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v1, v2, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LzU;

    invoke-interface {v1}, LzU;->a()LDG;

    move-result-object v1

    .line 198
    invoke-virtual {v1}, LDG;->length()I

    move-result v2

    .line 199
    invoke-virtual {v1}, LDG;->b()I

    move-result v1

    .line 202
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lxu;

    invoke-interface {v3}, Lxu;->a()Landroid/text/Editable;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LCe;

    add-int/2addr v2, v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v2

    const/16 v2, 0x21

    invoke-interface {v3, v4, v1, v0, v2}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    .line 204
    return-void
.end method

.method public a(Lza;)V
    .registers 4
    .parameter

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lwp;

    invoke-interface {v1}, Lwp;->a()I

    move-result v1

    neg-int v1, v1

    invoke-interface {p1, v0, v1}, Lza;->b(II)V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    invoke-virtual {v0}, LDA;->c()V

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:Lwp;

    invoke-interface {v0}, Lwp;->a()V

    .line 211
    return-void
.end method

.method public a(IILCh;)Z
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 230
    const/4 v0, 0x1

    return v0
.end method

.method public c(I)I
    .registers 3
    .parameter

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDA;

    invoke-virtual {v0}, LDA;->b()I

    move-result v0

    return v0
.end method

.method public d(I)I
    .registers 3
    .parameter

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement;->a:LDb;

    invoke-virtual {v0}, LDb;->c()I

    move-result v0

    return v0
.end method
