.class public Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;
.super Landroid/text/style/BackgroundColorSpan;
.source "DocosSpan.java"

# interfaces
.implements LDl;


# instance fields
.field private final a:I

.field private final a:Laji;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laji",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I[Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    .line 29
    const/16 v0, -0x100

    invoke-static {p1, v0}, LJN;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:I

    .line 30
    new-instance v0, Lajj;

    invoke-direct {v0}, Lajj;-><init>()V

    invoke-virtual {v0, p2}, Lajj;->a([Ljava/lang/Object;)Lajj;

    move-result-object v0

    invoke-virtual {v0}, Lajj;->a()Laji;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:Laji;

    .line 32
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:Ljava/util/Set;

    .line 33
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:Laji;

    return-object v0
.end method

.method public a(Ljava/lang/String;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:Laji;

    invoke-virtual {v0, p1}, Laji;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 53
    :goto_8
    return-void

    .line 48
    :cond_9
    if-eqz p2, :cond_11

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 51
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_8
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 3
    .parameter

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 65
    iget v0, p0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a:I

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    .line 69
    :goto_a
    return-void

    .line 67
    :cond_b
    invoke-super {p0, p1}, Landroid/text/style/BackgroundColorSpan;->updateDrawState(Landroid/text/TextPaint;)V

    goto :goto_a
.end method
