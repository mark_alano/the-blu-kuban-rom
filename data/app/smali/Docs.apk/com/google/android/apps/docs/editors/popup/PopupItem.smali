.class public Lcom/google/android/apps/docs/editors/popup/PopupItem;
.super Landroid/widget/LinearLayout;
.source "PopupItem.java"


# instance fields
.field private a:I

.field private a:Landroid/view/ViewGroup;

.field private a:Landroid/widget/ImageView;

.field private a:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-virtual {p0, v7}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->setOrientation(I)V

    .line 41
    invoke-virtual {p0, v8}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->setClickable(Z)V

    .line 44
    if-eqz p2, :cond_8d

    .line 47
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_8e

    .line 54
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 57
    invoke-virtual {v1, v7, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 58
    if-eqz v2, :cond_57

    .line 59
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, LsF;->popup_item_icon:I

    invoke-virtual {v0, v3, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/ImageView;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/ImageView;

    new-instance v3, Luj;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    const v5, 0x3f19999a

    const/high16 v6, -0x100

    invoke-direct {v3, v4, v2, v5, v6}, Luj;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;FI)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 65
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_52

    .line 67
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 70
    :cond_52
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->addView(Landroid/view/View;)V

    .line 74
    :cond_57
    invoke-virtual {v1, v8}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LEY;->a(Landroid/content/res/Resources;)Z

    move-result v0

    .line 77
    if-eqz v2, :cond_83

    if-nez v0, :cond_6b

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/ImageView;

    if-nez v0, :cond_83

    .line 78
    :cond_6b
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, LsF;->popup_item_title:I

    invoke-virtual {v0, v3, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/popup/PopupItem;->addView(Landroid/view/View;)V

    .line 85
    :cond_83
    const/4 v0, 0x2

    invoke-virtual {v1, v0, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:I

    .line 86
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 88
    :cond_8d
    return-void

    .line 47
    :array_8e
    .array-data 0x4
        0x2t 0x0t 0x1t 0x1t
        0xe1t 0x1t 0x1t 0x1t
        0x3bt 0x2t 0x1t 0x1t
        0x73t 0x2t 0x1t 0x1t
    .end array-data
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 115
    iget v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:I

    return v0
.end method

.method public a()Landroid/view/ViewGroup;
    .registers 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public a(I)V
    .registers 3
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    :cond_9
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public c()Z
    .registers 2

    .prologue
    .line 111
    iget v0, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:I

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public setNestedContentView(Landroid/view/ViewGroup;)V
    .registers 2
    .parameter

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/popup/PopupItem;->a:Landroid/view/ViewGroup;

    .line 124
    return-void
.end method
