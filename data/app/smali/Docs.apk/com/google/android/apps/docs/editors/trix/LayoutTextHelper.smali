.class public Lcom/google/android/apps/docs/editors/trix/LayoutTextHelper;
.super Ljava/lang/Object;
.source "LayoutTextHelper.java"


# annotations
.annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static layout(JF)[I
    .registers 7
    .parameter
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 38
    new-instance v0, LJf;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, LJf;-><init>(JZ)V

    .line 39
    new-instance v1, LGQ;

    float-to-double v2, p2

    invoke-direct {v1, v0, v2, v3}, LGQ;-><init>(LJf;D)V

    .line 40
    invoke-virtual {v1}, LGQ;->a()[I

    move-result-object v0

    return-object v0
.end method

.method public static measureHeight(JF)I
    .registers 10
    .parameter
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 22
    new-instance v2, LJf;

    invoke-direct {v2, p0, p1, v3}, LJf;-><init>(JZ)V

    .line 23
    invoke-virtual {v2}, LJf;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, LJf;->a()LIU;

    move-result-object v1

    invoke-virtual {v2}, LJf;->a()LJc;

    move-result-object v2

    const/4 v4, 0x0

    float-to-double v5, p2

    invoke-static/range {v0 .. v6}, LHQ;->a(Ljava/lang/String;LIU;LJc;ZLIT;D)LEW;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, LEW;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method
