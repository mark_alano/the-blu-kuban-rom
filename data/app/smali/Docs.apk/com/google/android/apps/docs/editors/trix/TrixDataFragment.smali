.class public Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "TrixDataFragment.java"

# interfaces
.implements LCM;
.implements LGY;


# instance fields
.field private a:I

.field public a:LHq;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LIP;

.field private a:LIQ;

.field private a:LJA;

.field private a:LJG;

.field private a:LJz;

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LNe;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LNf;

.field private final a:Landroid/os/Handler;

.field public a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LGZ;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LHa;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 68
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Landroid/os/Handler;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Ljava/util/List;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Ljava/util/List;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:I

    .line 80
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Z

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->c:Z

    .line 82
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->d:Z

    .line 83
    iput-boolean v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->e:Z

    .line 85
    new-instance v0, LGR;

    invoke-direct {v0, p0}, LGR;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LNf;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)I
    .registers 2
    .parameter

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LIP;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIP;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LJA;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJA;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LJG;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJG;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LJz;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJz;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 113
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;-><init>()V

    .line 115
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 116
    const-string v2, "accountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v2, "resourceId"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v2, "baseUrlPrefix"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const-string v2, "userCanEdit"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->d(Landroid/os/Bundle;)V

    .line 122
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)Ljava/util/List;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Ljava/util/List;

    return-object v0
.end method

.method private a(LIM;)V
    .registers 4
    .parameter

    .prologue
    .line 485
    invoke-virtual {p1}, LIM;->a()LJK;

    move-result-object v0

    .line 486
    if-eqz v0, :cond_14

    invoke-virtual {v0}, LJK;->a()I

    move-result v1

    if-lez v1, :cond_14

    .line 487
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LJK;->a(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->f(I)V

    .line 489
    :cond_14
    return-void
.end method

.method private a(LJA;)V
    .registers 4
    .parameter

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Landroid/os/Handler;

    new-instance v1, LGW;

    invoke-direct {v1, p0, p1}, LGW;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;LJA;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 427
    return-void
.end method

.method private a(LJF;LJD;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 324
    sget-object v0, LJF;->a:LJF;

    if-ne p1, v0, :cond_23

    .line 325
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_13
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_57

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHa;

    .line 326
    invoke-interface {v0, p2}, LHa;->a(LJD;)V

    goto :goto_13

    .line 328
    :cond_23
    sget-object v0, LJF;->c:LJF;

    if-ne p1, v0, :cond_3d

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_57

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHa;

    .line 330
    invoke-interface {v0, p2}, LHa;->b(LJD;)V

    goto :goto_2d

    .line 332
    :cond_3d
    sget-object v0, LJF;->b:LJF;

    if-ne p1, v0, :cond_57

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_47
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_57

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LHa;

    .line 334
    invoke-interface {v0, p2}, LHa;->c(LJD;)V

    goto :goto_47

    .line 337
    :cond_57
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->r()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->g(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;LJA;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(LJA;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;LJF;LJD;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(LJF;LJD;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;Lxl;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Lxl;)V

    return-void
.end method

.method private a(Lxl;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIP;

    if-eqz v0, :cond_c

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIP;

    invoke-virtual {v0}, LIP;->a()V

    .line 264
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIP;

    .line 267
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJG;

    if-eqz v0, :cond_17

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJG;

    invoke-virtual {v0}, LJG;->a()V

    .line 269
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJG;

    .line 272
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    if-eqz v0, :cond_22

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()V

    .line 274
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    .line 279
    :cond_22
    :try_start_22
    invoke-virtual {p1}, Lxl;->b()V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    .line 282
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->d:Ljava/lang/String;

    invoke-static {v1, v2, v0}, LIM;->a(Ljava/lang/String;Ljava/lang/String;Z)LIQ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()LIM;

    move-result-object v0

    .line 284
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LHq;

    invoke-virtual {v1}, LHq;->a()LKl;

    move-result-object v1

    invoke-static {v1}, LKc;->a(LKl;)LIR;

    move-result-object v1

    invoke-virtual {v0, v1}, LIM;->a(LIR;)V

    .line 285
    new-instance v1, LIP;

    new-instance v2, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    new-instance v3, LGT;

    invoke-direct {v3, p0}, LGT;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v1, v0, v2}, LIP;-><init>(LIM;LIR;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIP;

    .line 309
    new-instance v1, LJG;

    new-instance v2, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    new-instance v3, LGU;

    invoke-direct {v3, p0}, LGU;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v1, v0, v2}, LJG;-><init>(LIM;LIR;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJG;
    :try_end_6c
    .catchall {:try_start_22 .. :try_end_6c} :catchall_70

    .line 318
    invoke-virtual {p1}, Lxl;->c()V

    .line 320
    return-void

    .line 318
    :catchall_70
    move-exception v0

    invoke-virtual {p1}, Lxl;->c()V

    throw v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)Z
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->c:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LdL;

    return-object v0
.end method

.method private b(LJA;)V
    .registers 6
    .parameter

    .prologue
    .line 430
    invoke-virtual {p1}, LJA;->a()LJv;

    move-result-object v0

    invoke-virtual {v0}, LJv;->a()Z

    move-result v0

    .line 431
    if-eqz v0, :cond_e

    .line 433
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(LJA;)V

    .line 448
    :goto_d
    return-void

    .line 437
    :cond_e
    new-instance v0, LJz;

    invoke-virtual {p1}, LJA;->a()LJv;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/docs/editors/utils/CObjectReference;

    new-instance v3, LGX;

    invoke-direct {v3, p0, p1}, LGX;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;LJA;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/docs/editors/utils/CObjectReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1, v2}, LJz;-><init>(LJv;LIR;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJz;

    goto :goto_d
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->s()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)Z
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->t()V

    return-void
.end method

.method private g(I)V
    .registers 4
    .parameter

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 356
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->u()V

    .line 358
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:I

    if-ne p1, v0, :cond_19

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()LIM;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(LIM;)V

    .line 363
    :cond_19
    return-void
.end method

.method private h(I)V
    .registers 4
    .parameter

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Landroid/os/Handler;

    new-instance v1, LGV;

    invoke-direct {v1, p0, p1}, LGV;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 410
    return-void
.end method

.method private p()V
    .registers 5

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    if-eqz v0, :cond_2d

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    .line 238
    const-string v1, "TrixDataFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Application online state set to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v1}, LIQ;->a()LIM;

    move-result-object v1

    invoke-virtual {v1, v0}, LIM;->a(Z)V

    .line 241
    :cond_2d
    return-void
.end method

.method private q()V
    .registers 5

    .prologue
    .line 244
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Z

    if-nez v0, :cond_f

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LNe;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->e:Ljava/lang/String;

    const-string v2, "wise"

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LNf;

    invoke-interface {v0, v1, v2, v3}, LNe;->a(Ljava/lang/String;Ljava/lang/String;LNf;)V

    .line 247
    :cond_f
    return-void
.end method

.method private r()V
    .registers 5

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LHq;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->d:Ljava/lang/String;

    new-instance v3, LGS;

    invoke-direct {v3, p0}, LGS;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LHq;->a(Ljava/lang/String;Ljava/lang/String;LHs;)V

    .line 259
    return-void
.end method

.method private s()V
    .registers 3

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()LIM;

    move-result-object v0

    .line 343
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(LIM;)V

    .line 344
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->d:Z

    .line 345
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->w()V

    .line 346
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->x()V

    .line 347
    return-void
.end method

.method private t()V
    .registers 3

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 351
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->u()V

    .line 352
    return-void
.end method

.method private u()V
    .registers 3

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGZ;

    .line 391
    invoke-interface {v0}, LGZ;->g_()V

    goto :goto_6

    .line 393
    :cond_16
    return-void
.end method

.method private v()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJz;

    if-eqz v0, :cond_c

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJz;

    invoke-virtual {v0}, LJz;->a()V

    .line 515
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJz;

    .line 518
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJA;

    if-eqz v0, :cond_17

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJA;

    invoke-virtual {v0}, LJA;->a()V

    .line 522
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJA;

    .line 524
    :cond_17
    return-void
.end method

.method private w()V
    .registers 6

    .prologue
    .line 527
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()[LJD;

    move-result-object v1

    .line 528
    array-length v2, v1

    const/4 v0, 0x0

    :goto_6
    if-ge v0, v2, :cond_12

    aget-object v3, v1, v0

    .line 529
    sget-object v4, LJF;->a:LJF;

    invoke-direct {p0, v4, v3}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(LJF;LJD;)V

    .line 528
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 531
    :cond_12
    return-void
.end method

.method private x()V
    .registers 3

    .prologue
    .line 549
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Lgl;

    sget-object v1, Lgi;->x:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_f
    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->e:Z

    .line 551
    return-void

    .line 549
    :cond_12
    const/4 v0, 0x0

    goto :goto_f
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 481
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:I

    return v0
.end method

.method public a()LJA;
    .registers 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJA;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJA;

    invoke-virtual {v0}, LJA;->a()LJv;

    move-result-object v0

    invoke-virtual {v0}, LJv;->a()Z

    move-result v0

    if-nez v0, :cond_12

    .line 223
    :cond_10
    const/4 v0, 0x0

    .line 226
    :goto_11
    return-object v0

    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJA;

    goto :goto_11
.end method

.method public a()V
    .registers 1

    .prologue
    .line 367
    return-void
.end method

.method public a(ILjava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 375
    return-void
.end method

.method public a(LGZ;)V
    .registers 3
    .parameter

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    return-void
.end method

.method public a(LHa;)V
    .registers 3
    .parameter

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 132
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->e:Ljava/lang/String;

    .line 133
    const-string v1, "resourceId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->c:Ljava/lang/String;

    .line 134
    const-string v1, "baseUrlPrefix"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->d:Ljava/lang/String;

    .line 135
    const-string v1, "userCanEdit"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Z

    .line 136
    return-void
.end method

.method public a()[I
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 452
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    if-nez v1, :cond_6

    .line 467
    :cond_5
    return-object v0

    .line 456
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v1}, LIQ;->a()LIM;

    move-result-object v1

    invoke-virtual {v1}, LIM;->a()LJK;

    move-result-object v2

    .line 457
    if-eqz v2, :cond_5

    .line 461
    invoke-virtual {v2}, LJK;->a()I

    move-result v3

    .line 462
    invoke-virtual {v2}, LJK;->a()I

    move-result v0

    new-array v0, v0, [I

    .line 463
    const/4 v1, 0x0

    :goto_1d
    if-ge v1, v3, :cond_5

    .line 464
    invoke-virtual {v2, v1}, LJK;->a(I)I

    move-result v4

    aput v4, v0, v1

    .line 463
    add-int/lit8 v1, v1, 0x1

    goto :goto_1d
.end method

.method public a()[LJD;
    .registers 6

    .prologue
    .line 535
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->d:Z

    if-eqz v0, :cond_20

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()LIM;

    move-result-object v0

    invoke-virtual {v0}, LIM;->a()LJH;

    move-result-object v2

    .line 537
    invoke-virtual {v2}, LJH;->a()I

    move-result v3

    .line 538
    new-array v0, v3, [LJD;

    .line 539
    const/4 v1, 0x0

    :goto_15
    if-ge v1, v3, :cond_21

    .line 540
    invoke-virtual {v2, v1}, LJH;->a(I)LJD;

    move-result-object v4

    aput-object v4, v0, v1

    .line 539
    add-int/lit8 v1, v1, 0x1

    goto :goto_15

    .line 544
    :cond_20
    const/4 v0, 0x0

    :cond_21
    return-object v0
.end method

.method public b(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    if-nez v0, :cond_6

    .line 473
    const/4 v0, 0x0

    .line 476
    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()LIM;

    move-result-object v0

    invoke-virtual {v0, p1}, LIM;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method public b()V
    .registers 1

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->p()V

    .line 232
    return-void
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 371
    return-void
.end method

.method public b(LGZ;)V
    .registers 3
    .parameter

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 207
    return-void
.end method

.method public b(LHa;)V
    .registers 3
    .parameter

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 217
    return-void
.end method

.method public c(I)V
    .registers 2
    .parameter

    .prologue
    .line 379
    return-void
.end method

.method public d(I)V
    .registers 2
    .parameter

    .prologue
    .line 383
    return-void
.end method

.method public e(I)V
    .registers 2
    .parameter

    .prologue
    .line 387
    return-void
.end method

.method public f(I)V
    .registers 3
    .parameter

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    if-nez v0, :cond_5

    .line 509
    :cond_4
    :goto_4
    return-void

    .line 497
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()LIM;

    move-result-object v0

    invoke-virtual {v0, p1}, LIM;->a(I)LJA;

    move-result-object v0

    .line 498
    if-eqz v0, :cond_4

    .line 502
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->v()V

    .line 504
    iput p1, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:I

    .line 505
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJA;

    .line 507
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->h(I)V

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJA;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b(LJA;)V

    goto :goto_4
.end method

.method public g()V
    .registers 2

    .prologue
    .line 140
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->c:Z

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LHq;

    invoke-virtual {v0}, LHq;->b()V

    .line 145
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->p()V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;->a(LCM;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    if-eqz v0, :cond_20

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()LIM;

    move-result-object v0

    invoke-virtual {v0}, LIM;->c()V

    .line 151
    :cond_20
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->q()V

    .line 152
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->c:Z

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;->b(LCM;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    if-eqz v0, :cond_15

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()LIM;

    move-result-object v0

    invoke-virtual {v0}, LIM;->b()V

    .line 163
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LHq;

    invoke-virtual {v0}, LHq;->a()V

    .line 165
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h()V

    .line 166
    return-void
.end method

.method public h()Z
    .registers 2

    .prologue
    .line 555
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->e:Z

    return v0
.end method

.method public i()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 171
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->v()V

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIP;

    if-eqz v0, :cond_1b

    .line 174
    const-string v0, "TrixDataFragment"

    const-string v1, "Deleting application event listener..."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIP;

    invoke-virtual {v0}, LIP;->a()V

    .line 176
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIP;

    .line 179
    :cond_1b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJG;

    if-eqz v0, :cond_2d

    .line 180
    const-string v0, "TrixDataFragment"

    const-string v1, "Deleting user session event listener..."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJG;

    invoke-virtual {v0}, LJG;->a()V

    .line 182
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LJG;

    .line 185
    :cond_2d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    if-eqz v0, :cond_3f

    .line 186
    const-string v0, "TrixDataFragment"

    const-string v1, "Deleting application..."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    invoke-virtual {v0}, LIQ;->a()V

    .line 188
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LIQ;

    .line 191
    :cond_3f
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LHq;

    if-eqz v0, :cond_4a

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LHq;

    invoke-virtual {v0}, LHq;->c()V

    .line 193
    iput-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:LHq;

    .line 196
    :cond_4a
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->i()V

    .line 197
    return-void
.end method

.method public i()Z
    .registers 3

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a:Lgl;

    sget-object v1, Lgi;->y:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    return v0
.end method
