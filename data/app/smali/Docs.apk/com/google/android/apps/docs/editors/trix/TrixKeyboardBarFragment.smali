.class public Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "TrixKeyboardBarFragment.java"


# instance fields
.field private a:LGK;

.field private a:Landroid/view/inputmethod/InputMethodManager;

.field private a:Landroid/widget/ImageButton;

.field private a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

.field private b:Landroid/widget/ImageButton;

.field private c:Landroid/widget/ImageButton;

.field private d:Landroid/view/View;

.field private d:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)LGK;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:LGK;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->r()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->s()V

    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Landroid/widget/ImageButton;

    if-eqz v0, :cond_e

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Landroid/widget/ImageButton;

    new-instance v1, LHc;

    invoke-direct {v1, p0}, LHc;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->b:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1c

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->b:Landroid/widget/ImageButton;

    new-instance v1, LHd;

    invoke-direct {v1, p0}, LHd;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->c:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2a

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->c:Landroid/widget/ImageButton;

    new-instance v1, LHe;

    invoke-direct {v1, p0}, LHe;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    :cond_2a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/widget/ImageButton;

    if-eqz v0, :cond_38

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/widget/ImageButton;

    new-instance v1, LHf;

    invoke-direct {v1, p0}, LHf;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    :cond_38
    return-void
.end method

.method private q()V
    .registers 3

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    new-instance v1, LHg;

    invoke-direct {v1, p0}, LHg;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    new-instance v1, LHh;

    invoke-direct {v1, p0}, LHh;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->setPreImeListener(LIk;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    new-instance v1, LHi;

    invoke-direct {v1, p0}, LHi;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    new-instance v1, LHj;

    invoke-direct {v1, p0}, LHj;-><init>(Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->setSelectionChangedListener(LIl;)V

    .line 185
    return-void
.end method

.method private r()V
    .registers 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    const v1, 0xa0001

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->setInputType(I)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->setMaxLines(I)V

    .line 200
    return-void
.end method

.method private s()V
    .registers 3

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LKe;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 206
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->r()V

    .line 216
    :goto_d
    return-void

    .line 212
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    const v1, 0x20002

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->setInputType(I)V

    goto :goto_d
.end method

.method private t()V
    .registers 3

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LKe;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_31

    const/16 v0, 0x8

    .line 222
    :goto_c
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Landroid/widget/ImageButton;

    if-eqz v1, :cond_15

    .line 223
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 226
    :cond_15
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->b:Landroid/widget/ImageButton;

    if-eqz v1, :cond_1e

    .line 227
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 230
    :cond_1e
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->c:Landroid/widget/ImageButton;

    if-eqz v1, :cond_27

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->c:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 234
    :cond_27
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/widget/ImageButton;

    if-eqz v1, :cond_30

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 237
    :cond_30
    return-void

    .line 221
    :cond_31
    const/4 v0, 0x0

    goto :goto_c
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    sget v0, LsF;->trix_keyboard_bar:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/view/View;

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/view/View;

    sget v1, LsD;->cell_content_editor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/view/View;

    sget v1, LsD;->toggle_text_mode:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Landroid/widget/ImageButton;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/view/View;

    sget v1, LsD;->toggle_number_mode:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->b:Landroid/widget/ImageButton;

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/view/View;

    sget v1, LsD;->go_left_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->c:Landroid/widget/ImageButton;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/view/View;

    sget v1, LsD;->go_right_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/widget/ImageButton;

    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->r()V

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->p()V

    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->q()V

    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->t()V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public a()V
    .registers 4

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:LGK;

    if-eqz v0, :cond_9

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:LGK;

    invoke-interface {v0}, LGK;->d()V

    .line 265
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 266
    return-void
.end method

.method public a(Ljava/lang/CharSequence;LGK;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 250
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:LGK;

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->setSelection(I)V

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;->requestFocus()Z

    .line 254
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->t()V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixCellEditor;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 256
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 189
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->b(Landroid/os/Bundle;)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a:Landroid/view/inputmethod/InputMethodManager;

    .line 193
    return-void
.end method
