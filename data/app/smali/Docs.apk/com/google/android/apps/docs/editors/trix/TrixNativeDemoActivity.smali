.class public Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;
.super Lcom/google/android/apps/docs/editors/AbstractEditorActivity;
.source "TrixNativeDemoActivity.java"

# interfaces
.implements LGJ;
.implements LGZ;


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private a:LCL;

.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LKt;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;

.field private a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

.field private a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

.field private a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

.field public a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 44
    const-string v0, "TrixDemoActivity"

    sput-object v0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;-><init>()V

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Ljava/util/Map;

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->l:Z

    return-void
.end method

.method private d()V
    .registers 5

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    const-string v1, "TrixDataFragment"

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    if-nez v0, :cond_49

    .line 150
    sget-object v0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Ljava/lang/String;

    const-string v1, "Instantiating data fragment..."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->j:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    const-string v2, "TrixDataFragment"

    invoke-virtual {v0, v1, v2}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 159
    :goto_3a
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->a(LGZ;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Ljava/util/Map;

    const-class v1, LGY;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    return-void

    .line 156
    :cond_49
    sget-object v0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Ljava/lang/String;

    const-string v1, "Existing data fragment found"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3a
.end method

.method private e()V
    .registers 1

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->finish()V

    .line 195
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 244
    if-nez p2, :cond_b

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 246
    if-eqz v0, :cond_b

    .line 253
    :goto_a
    return-object v0

    :cond_b
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_a
.end method

.method public a()V
    .registers 2

    .prologue
    .line 273
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()V

    .line 274
    return-void
.end method

.method public a(ILJA;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->m:Z

    if-nez v0, :cond_23

    .line 232
    sget v0, LsD;->startup_loading_spinner:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-virtual {v0, v1}, Lz;->c(Landroid/support/v4/app/Fragment;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->m:Z

    .line 240
    :cond_23
    return-void
.end method

.method public a(Ljava/lang/CharSequence;LGK;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    .line 261
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-virtual {v0, v1}, Lz;->b(Landroid/support/v4/app/Fragment;)Lz;

    .line 262
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    invoke-virtual {v0, v1}, Lz;->c(Landroid/support/v4/app/Fragment;)Lz;

    .line 263
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lz;->a(Ljava/lang/String;)Lz;

    .line 264
    invoke-virtual {v0}, Lz;->a()I

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Z

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;->a(Ljava/lang/CharSequence;LGK;)V

    .line 268
    return-void
.end method

.method public b(I)V
    .registers 5
    .parameter

    .prologue
    .line 223
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;

    .line 224
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    sget v1, LsD;->spreadsheet_fragment_container:I

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/SpreadsheetFragment;

    invoke-virtual {v0, v1, v2}, Lz;->b(ILandroid/support/v4/app/Fragment;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 227
    return-void
.end method

.method public g_()V
    .registers 1

    .prologue
    .line 219
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    sget v0, LsF;->trix_native_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->setContentView(I)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LKt;

    invoke-virtual {v1, v0}, LKt;->a(Landroid/content/Intent;)V

    .line 89
    new-instance v1, LJX;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LKt;

    invoke-virtual {v2}, LKt;->a()Z

    move-result v2

    invoke-direct {v1, v0, v2}, LJX;-><init>(Landroid/content/Intent;Z)V

    .line 90
    invoke-virtual {v1, p0}, LJX;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->d:Ljava/lang/String;

    .line 91
    invoke-virtual {v1}, LJX;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->e:Ljava/lang/String;

    .line 93
    invoke-virtual {v1}, LJX;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->b:Ljava/lang/String;

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_3e

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LKS;

    const-string v2, "trixDebugDocumentId"

    const-string v3, "0AjnK2jXQOslKcjRzYmRhUzUtdXJsbEVmQWlWSjR1SlE"

    invoke-interface {v0, v2, v3}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->b:Ljava/lang/String;

    .line 100
    :cond_3e
    const-string v0, "spreadsheet"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LJX;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->c:Ljava/lang/String;

    .line 101
    invoke-virtual {v1}, LJX;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->j:Z

    .line 103
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->d()V

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    sget v2, LsD;->trix_sheet_bar_fragment:I

    invoke-virtual {v0, v2}, Lo;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a(LHz;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(LHG;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;

    const v2, 0x1020002

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    sget v2, LsD;->trix_keyboard_bar_fragment:I

    invoke-virtual {v0, v2}, Lo;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    .line 114
    new-instance v0, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;-><init>()V

    .line 115
    new-instance v2, Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;

    invoke-direct {v2}, Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;-><init>()V

    .line 116
    new-instance v3, Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;

    invoke-direct {v3}, Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;-><init>()V

    .line 118
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Ljava/util/Map;

    const-class v5, Lcom/google/android/apps/docs/editors/trix/popup/TrixCellSelectionPopup;

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Ljava/util/Map;

    const-class v5, Lcom/google/android/apps/docs/editors/trix/popup/TrixRowSelectionPopup;

    invoke-interface {v4, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Ljava/util/Map;

    const-class v5, Lcom/google/android/apps/docs/editors/trix/popup/TrixColumnSelectionPopup;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v4

    invoke-virtual {v4}, Lo;->a()Lz;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabMenuFragment;

    const-string v6, "sheetTabMenuFragment"

    invoke-virtual {v4, v5, v6}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v4

    const-string v5, "cellSelectionPopup"

    invoke-virtual {v4, v0, v5}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    const-string v4, "rowSelectionPopup"

    invoke-virtual {v0, v2, v4}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    const-string v2, "columnSelectionPopup"

    invoke-virtual {v0, v3, v2}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixKeyboardBarFragment;

    invoke-virtual {v0, v2}, Lz;->b(Landroid/support/v4/app/Fragment;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LKS;

    const-string v2, "trixShowWebviewButton"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->k:Z

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->e:Ljava/lang/String;

    if-eqz v0, :cond_e9

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 137
    :cond_e9
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LKt;

    invoke-virtual {v0}, LKt;->a()Z

    move-result v0

    if-nez v0, :cond_10d

    invoke-virtual {v1}, LJX;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_10d

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->a()Lz;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/docs/editors/SharingFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/editors/SharingFragment;-><init>()V

    const-string v2, "SharingFragment"

    invoke-virtual {v0, v1, v2}, Lz;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->a()I

    .line 143
    :cond_10d
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 166
    new-instance v0, LCL;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LCL;-><init>(Lcom/google/android/apps/docs/RoboFragmentActivity;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LCL;

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LCL;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->j:Z

    invoke-virtual {v0, v1}, LCL;->c(Z)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LCL;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->k:Z

    invoke-virtual {v0, v1}, LCL;->d(Z)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LCL;

    invoke-virtual {v0, p1}, LCL;->a(Landroid/view/Menu;)V

    .line 170
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/trix/TrixDataFragment;->b(LGZ;)V

    .line 214
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onDestroy()V

    .line 215
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 5
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 176
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 178
    sget v1, LsD;->menu_webview_mode:I

    if-ne v0, v1, :cond_22

    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->e:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/docs/doclist/documentopener/WebViewOpenActivity;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 182
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->startActivity(Landroid/content/Intent;)V

    .line 189
    :cond_20
    :goto_20
    const/4 v0, 0x1

    :goto_21
    return v0

    .line 183
    :cond_22
    const v1, 0x102002c

    if-ne v0, v1, :cond_2b

    .line 184
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->e()V

    goto :goto_20

    .line 185
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LCL;

    if-eqz v0, :cond_37

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->a:LCL;

    invoke-virtual {v0, p1}, LCL;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 186
    :cond_37
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_21
.end method

.method protected onPause()V
    .registers 2

    .prologue
    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->l:Z

    .line 209
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onPause()V

    .line 210
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 199
    invoke-super {p0}, Lcom/google/android/apps/docs/editors/AbstractEditorActivity;->onResume()V

    .line 201
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/TrixNativeDemoActivity;->l:Z

    .line 203
    return-void
.end method
