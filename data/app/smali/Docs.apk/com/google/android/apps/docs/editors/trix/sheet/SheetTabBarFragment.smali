.class public Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "SheetTabBarFragment.java"

# interfaces
.implements LGZ;
.implements LHz;


# instance fields
.field private a:I

.field private a:LGY;

.field private a:LHF;

.field private a:LHG;

.field private a:Landroid/content/Context;

.field private a:Landroid/widget/Button;

.field private a:Landroid/widget/HorizontalScrollView;

.field private a:Landroid/widget/LinearLayout$LayoutParams;

.field private a:Landroid/widget/LinearLayout;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LGY;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

.field private a:Z

.field private a:[I

.field private b:I

.field private b:Landroid/widget/LinearLayout$LayoutParams;

.field private c:I

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 77
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)I
    .registers 2
    .parameter

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b:I

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 39
    iput p1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:I

    return p1
.end method

.method private a([II)I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 454
    const/4 v0, 0x0

    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_c

    .line 455
    aget v1, p1, v0

    if-ne v1, p2, :cond_9

    .line 459
    :goto_8
    return v0

    .line 454
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 459
    :cond_c
    const/4 v0, -0x1

    goto :goto_8
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)LGY;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)LHG;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHG;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)Landroid/widget/HorizontalScrollView;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)Landroid/widget/LinearLayout;
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;
    .registers 4
    .parameter

    .prologue
    .line 509
    mul-int/lit8 v0, p1, 0x2

    .line 510
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;
    .registers 4
    .parameter

    .prologue
    .line 501
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x1

    .line 502
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    return-object v0
.end method

.method private a(IZLjava/util/List;)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;
    .registers 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;",
            ">;)",
            "Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;"
        }
    .end annotation

    .prologue
    .line 470
    if-eqz p3, :cond_8

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_27

    .line 471
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a(Landroid/content/Context;)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    move-result-object v0

    .line 476
    :goto_e
    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->setSheetId(I)V

    .line 477
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    invoke-interface {v1, p1}, LGY;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->setSheetName(Ljava/lang/String;)V

    .line 478
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHF;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 479
    invoke-virtual {v0, p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->setActive(Z)V

    .line 481
    if-eqz p2, :cond_26

    .line 482
    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    .line 485
    :cond_26
    return-object v0

    .line 473
    :cond_27
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    goto :goto_e
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->s()V

    return-void
.end method

.method private a(ZLcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 489
    invoke-virtual {p2, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->setActive(Z)V

    .line 490
    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    move-result-object v1

    if-eqz p1, :cond_22

    sget-object v0, LIw;->b:LIw;

    :goto_f
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setActive(LIw;)V

    .line 492
    invoke-virtual {p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->b()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    move-result-object v1

    if-eqz p1, :cond_25

    sget-object v0, LIw;->c:LIw;

    :goto_1e
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setActive(LIw;)V

    .line 494
    return-void

    .line 490
    :cond_22
    sget-object v0, LIw;->a:LIw;

    goto :goto_f

    .line 492
    :cond_25
    sget-object v0, LIw;->a:LIw;

    goto :goto_1e
.end method

.method private a([I)V
    .registers 12
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    invoke-interface {v0}, LGY;->a()I

    move-result v5

    .line 350
    const/4 v0, 0x0

    .line 351
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:[I

    if-eqz v2, :cond_26

    .line 352
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:[I

    array-length v0, v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 353
    :goto_16
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_25

    .line 354
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 353
    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    :cond_25
    move-object v0, v2

    .line 359
    :cond_26
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 362
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    iget-object v6, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/content/Context;

    sget-object v7, LIx;->a:LIx;

    invoke-direct {v3, v6, v7}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;-><init>(Landroid/content/Context;LIx;)V

    iget-object v6, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v2, v3, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v1

    .line 367
    :goto_3c
    array-length v3, p1

    if-ge v2, v3, :cond_86

    .line 369
    aget v6, p1, v2

    .line 371
    if-ne v6, v5, :cond_73

    move v3, v4

    :goto_44
    invoke-direct {p0, v6, v3, v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(IZLjava/util/List;)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    move-result-object v3

    .line 372
    invoke-virtual {v3, v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->setLogicalIndex(I)V

    .line 374
    iget-object v6, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v6, v3, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 377
    array-length v6, p1

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_75

    .line 378
    new-instance v6, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    iget-object v7, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/content/Context;

    sget-object v8, LIx;->b:LIx;

    invoke-direct {v6, v7, v8}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;-><init>(Landroid/content/Context;LIx;)V

    .line 379
    iget-object v7, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    iget-object v8, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v7, v6, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 387
    :goto_67
    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()Z

    move-result v6

    if-eqz v6, :cond_70

    .line 388
    invoke-direct {p0, v4, v3}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(ZLcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;)V

    .line 367
    :cond_70
    add-int/lit8 v2, v2, 0x1

    goto :goto_3c

    :cond_73
    move v3, v1

    .line 371
    goto :goto_44

    .line 383
    :cond_75
    iget-object v6, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    new-instance v7, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    iget-object v8, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/content/Context;

    sget-object v9, LIx;->c:LIx;

    invoke-direct {v7, v8, v9}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;-><init>(Landroid/content/Context;LIx;)V

    iget-object v8, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v6, v7, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_67

    .line 394
    :cond_86
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->r()V

    .line 397
    iput-boolean v4, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Z

    .line 401
    array-length v0, p1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:[I

    .line 402
    :goto_90
    array-length v0, p1

    if-ge v1, v0, :cond_9c

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:[I

    aget v2, p1, v1

    aput v2, v0, v1

    .line 402
    add-int/lit8 v1, v1, 0x1

    goto :goto_90

    .line 405
    :cond_9c
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)Z
    .registers 2
    .parameter

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Z

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Z

    return p1
.end method

.method private a([I)Z
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:[I

    if-eqz v0, :cond_c

    array-length v0, p1

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:[I

    array-length v3, v3

    if-eq v0, v3, :cond_e

    :cond_c
    move v1, v2

    .line 312
    :cond_d
    :goto_d
    return v1

    :cond_e
    move v0, v1

    .line 306
    :goto_f
    array-length v3, p1

    if-ge v0, v3, :cond_d

    .line 307
    aget v3, p1, v0

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:[I

    aget v4, v4, v0

    if-eq v3, v4, :cond_1c

    move v1, v2

    .line 308
    goto :goto_d

    .line 306
    :cond_1c
    add-int/lit8 v0, v0, 0x1

    goto :goto_f
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)I
    .registers 2
    .parameter

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:I

    return v0
.end method

.method private b(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;
    .registers 4
    .parameter

    .prologue
    .line 517
    mul-int/lit8 v0, p1, 0x2

    add-int/lit8 v0, v0, 0x2

    .line 518
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->r()V

    return-void
.end method

.method private b([I)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 417
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    invoke-interface {v1}, LGY;->a()I

    move-result v1

    .line 420
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()I

    move-result v2

    if-eq v1, v2, :cond_37

    .line 422
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a([II)I

    move-result v2

    .line 423
    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a([II)I

    move-result v1

    .line 425
    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    move-result-object v2

    .line 426
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    move-result-object v1

    .line 428
    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(ZLcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;)V

    .line 429
    invoke-direct {p0, v4, v1}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(ZLcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;)V

    .line 431
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    .line 432
    iput-boolean v4, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Z

    .line 433
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 440
    :cond_37
    :goto_37
    array-length v1, p1

    if-ge v0, v1, :cond_5a

    .line 442
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    aget v2, p1, v0

    invoke-interface {v1, v2}, LGY;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 443
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()Ljava/lang/String;

    move-result-object v2

    .line 444
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_57

    .line 445
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->setSheetName(Ljava/lang/String;)V

    .line 440
    :cond_57
    add-int/lit8 v0, v0, 0x1

    goto :goto_37

    .line 448
    :cond_5a
    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LHB;

    invoke-direct {v1, p0}, LHB;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 197
    return-void
.end method

.method private q()V
    .registers 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    new-instance v1, LHC;

    invoke-direct {v1, p0}, LHC;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->d:Landroid/view/View;

    new-instance v1, LHD;

    invoke-direct {v1, p0}, LHD;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 240
    return-void
.end method

.method private r()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v1}, Landroid/widget/LinearLayout;->measure(II)V

    .line 258
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b:I

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->c:I

    add-int/2addr v0, v1

    .line 260
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 262
    iget v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:I

    if-le v0, v2, :cond_2a

    .line 263
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:I

    sub-int/2addr v0, v3

    sub-int v0, v2, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 268
    :goto_29
    return-void

    .line 266
    :cond_2a
    const/4 v0, -0x2

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_29
.end method

.method private s()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_d

    .line 294
    :cond_c
    :goto_c
    return-void

    .line 282
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v1

    .line 285
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->b()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->getLeft()I

    move-result v2

    .line 286
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->b()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b(I)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->getRight()I

    move-result v3

    .line 288
    sub-int v4, v3, v1

    if-le v4, v0, :cond_45

    .line 289
    sub-int v1, v3, v1

    sub-int v0, v1, v0

    .line 290
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1, v0, v5}, Landroid/widget/HorizontalScrollView;->smoothScrollBy(II)V

    goto :goto_c

    .line 291
    :cond_45
    sub-int v0, v2, v1

    if-gez v0, :cond_c

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/HorizontalScrollView;

    sub-int v1, v2, v1

    invoke-virtual {v0, v1, v5}, Landroid/widget/HorizontalScrollView;->smoothScrollBy(II)V

    goto :goto_c
.end method

.method private t()V
    .registers 4

    .prologue
    .line 322
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    monitor-enter v1

    .line 324
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    invoke-interface {v0}, LGY;->a()[I

    move-result-object v0

    .line 325
    if-eqz v0, :cond_e

    array-length v2, v0

    if-nez v2, :cond_10

    .line 327
    :cond_e
    monitor-exit v1

    .line 338
    :goto_f
    return-void

    .line 331
    :cond_10
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a([I)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 332
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a([I)V

    .line 337
    :goto_19
    monitor-exit v1

    goto :goto_f

    :catchall_1b
    move-exception v0

    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_1b

    throw v0

    .line 335
    :cond_1e
    :try_start_1e
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b([I)V
    :try_end_21
    .catchall {:try_start_1e .. :try_end_21} :catchall_1b

    goto :goto_19
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 104
    sget v0, LsF;->trix_sheets_tab_bar:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->d:Landroid/view/View;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->d:Landroid/view/View;

    sget v1, LsD;->sheets_tab_scroll_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/HorizontalScrollView;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->d:Landroid/view/View;

    sget v1, LsD;->sheets_tab_bar:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout;

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->d:Landroid/view/View;

    sget v1, LsD;->sheets_tab_add_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/Button;

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public a(ILJA;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 566
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_f

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 569
    :cond_f
    return-void
.end method

.method public a(LHG;)V
    .registers 2
    .parameter

    .prologue
    .line 246
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHG;

    .line 247
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()I

    move-result v1

    invoke-interface {v0, v1, p1}, LGY;->a(ILjava/lang/String;)V

    .line 532
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHG;

    invoke-interface {v0}, LHG;->a()V

    .line 533
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()I

    move-result v1

    invoke-interface {v0, v1}, LGY;->b(I)V

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHG;

    invoke-interface {v0}, LHG;->a()V

    .line 527
    return-void
.end method

.method public b(I)V
    .registers 2
    .parameter

    .prologue
    .line 560
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->t()V

    .line 561
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->b(Landroid/os/Bundle;)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/content/Context;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/Button;

    new-instance v1, LHA;

    invoke-direct {v1, p0}, LHA;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    new-instance v0, LHF;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, LHF;-><init>(Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;LHA;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHF;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHG;

    invoke-interface {v0, p0}, LHG;->a(LHz;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 137
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    sget v2, LsB;->trix_sheets_tab_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sget v3, LsB;->trix_sheets_tab_height:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Landroid/widget/LinearLayout$LayoutParams;

    .line 139
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    sget v3, LsB;->trix_sheets_tab_height:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b:Landroid/widget/LinearLayout$LayoutParams;

    .line 143
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:I

    .line 144
    sget v1, LsB;->trix_sheets_tab_bar_left_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->b:I

    .line 145
    sget v1, LsB;->trix_sheets_tab_add_button_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->c:I

    .line 152
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_69

    .line 153
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->q()V

    .line 157
    :goto_68
    return-void

    .line 155
    :cond_69
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->p()V

    goto :goto_68
.end method

.method public c()V
    .registers 3

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()I

    move-result v1

    invoke-interface {v0, v1}, LGY;->c(I)V

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHG;

    invoke-interface {v0}, LHG;->a()V

    .line 539
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()I

    move-result v1

    invoke-interface {v0, v1}, LGY;->e(I)V

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHG;

    invoke-interface {v0}, LHG;->a()V

    .line 545
    return-void
.end method

.method public e()V
    .registers 3

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()I

    move-result v1

    invoke-interface {v0, v1}, LGY;->d(I)V

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LHG;

    invoke-interface {v0}, LHG;->a()V

    .line 551
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LGY;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    invoke-interface {v0, p0}, LGY;->a(LGZ;)V

    .line 165
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->t()V

    .line 166
    return-void
.end method

.method public g_()V
    .registers 1

    .prologue
    .line 555
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->t()V

    .line 556
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    invoke-interface {v0, p0}, LGY;->b(LGZ;)V

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/sheet/SheetTabBarFragment;->a:LGY;

    .line 173
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h()V

    .line 174
    return-void
.end method
