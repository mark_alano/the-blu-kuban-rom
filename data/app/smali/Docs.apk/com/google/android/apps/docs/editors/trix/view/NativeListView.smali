.class public Lcom/google/android/apps/docs/editors/trix/view/NativeListView;
.super Landroid/view/View;
.source "NativeListView.java"


# static fields
.field private static final a:D


# instance fields
.field private a:LIG;

.field private a:LJk;

.field private final a:Landroid/os/Handler;

.field private a:Z

.field private b:D

.field private c:D

.field private d:D

.field private e:D


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 46
    const-wide/high16 v0, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:Landroid/os/Handler;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:Z

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:Landroid/os/Handler;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:Z

    .line 70
    return-void
.end method

.method private a(ILandroid/util/SparseArray;)F
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;)F"
        }
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 272
    if-eqz v0, :cond_d

    .line 273
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 279
    :goto_c
    return v0

    .line 276
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    new-instance v1, LJo;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, LJo;-><init>(II)V

    invoke-virtual {v0, v1}, LJk;->a(LJo;)LJn;

    move-result-object v0

    .line 277
    invoke-virtual {v0}, LJn;->b()D

    move-result-wide v0

    double-to-float v0, v0

    .line 278
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_c
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;)LJk;
    .registers 2
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;)V
    .registers 1
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->b()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 9
    .parameter

    .prologue
    const-wide/16 v5, 0x0

    .line 134
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:Z

    if-eqz v0, :cond_9

    if-nez p1, :cond_9

    .line 167
    :goto_8
    return-void

    .line 139
    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:Z

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-virtual {v0}, LJk;->a()LJI;

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LIG;

    invoke-virtual {v0}, LIG;->a()LIL;

    move-result-object v0

    .line 143
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LIG;

    invoke-virtual {v1}, LIG;->a()LKj;

    move-result-object v1

    .line 144
    if-nez v1, :cond_28

    .line 145
    iput-wide v5, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->e:D

    iput-wide v5, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->c:D

    iput-wide v5, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->d:D

    iput-wide v5, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->b:D

    goto :goto_8

    .line 149
    :cond_28
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    new-instance v3, LJo;

    invoke-virtual {v1}, LKj;->a()I

    move-result v4

    invoke-virtual {v1}, LKj;->c()I

    move-result v1

    invoke-direct {v3, v4, v1}, LJo;-><init>(II)V

    invoke-virtual {v2, v3}, LJk;->a(LJo;)LJn;

    move-result-object v1

    .line 156
    invoke-virtual {v0}, LIL;->a()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 157
    invoke-virtual {v1}, LJn;->b()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->b:D

    .line 158
    invoke-virtual {v1}, LJn;->a()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->d:D

    .line 159
    iput-wide v5, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->c:D

    .line 160
    const-wide/high16 v0, 0x4034

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->e:D

    goto :goto_8

    .line 162
    :cond_54
    iput-wide v5, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->b:D

    .line 163
    const-wide/high16 v2, 0x4049

    iput-wide v2, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->d:D

    .line 164
    invoke-virtual {v1}, LJn;->b()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->c:D

    .line 165
    invoke-virtual {v1}, LJn;->a()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->e:D

    goto :goto_8
.end method

.method private b()V
    .registers 3

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:Z

    if-nez v0, :cond_5

    .line 131
    :goto_4
    return-void

    .line 121
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:Z

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:Landroid/os/Handler;

    new-instance v1, LIa;

    invoke-direct {v1, p0}, LIa;-><init>(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_4
.end method

.method private c()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 178
    new-instance v0, LJq;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, LJq;-><init>(IIII)V

    .line 179
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-virtual {v1, v0}, LJk;->a(LJq;)V

    .line 180
    invoke-virtual {v0}, LJq;->a()V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->invalidate()V

    .line 182
    return-void
.end method


# virtual methods
.method public a()I
    .registers 5

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->d:D

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-virtual {v2}, LJk;->a()D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public a()LJk;
    .registers 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LIG;

    if-eqz v0, :cond_e

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LIG;

    invoke-virtual {v0}, LIG;->a()V

    .line 75
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LIG;

    .line 76
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    .line 78
    :cond_e
    return-void
.end method

.method public a(LIG;)V
    .registers 10
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LIG;

    .line 86
    invoke-virtual {p1}, LIG;->a()LJk;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LIG;

    new-instance v1, LHZ;

    invoke-direct {v1, p0}, LHZ;-><init>(Lcom/google/android/apps/docs/editors/trix/view/NativeListView;)V

    invoke-virtual {v0, v1}, LIG;->a(LIK;)V

    .line 106
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(Z)V

    .line 108
    const-wide/high16 v0, 0x3ff0

    .line 109
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    new-instance v3, LJI;

    iget-wide v4, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->b:D

    iget-wide v6, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->c:D

    invoke-direct {v3, v4, v5, v6, v7}, LJI;-><init>(DD)V

    invoke-virtual {v2, v3}, LJk;->a(LJI;)V

    .line 110
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-virtual {v2, v0, v1}, LJk;->a(D)V

    .line 111
    iget-wide v2, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->b:D

    mul-double/2addr v2, v0

    double-to-int v2, v2

    iget-wide v3, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->c:D

    mul-double/2addr v0, v3

    double-to-int v0, v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->scrollTo(II)V

    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->c()V

    .line 114
    return-void
.end method

.method public b()I
    .registers 5

    .prologue
    .line 287
    iget-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->e:D

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-virtual {v2}, LJk;->a()D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 25
    .parameter

    .prologue
    .line 186
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    if-nez v3, :cond_7

    .line 268
    :cond_6
    return-void

    .line 190
    :cond_7
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(Z)V

    .line 192
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-virtual {v3}, LJk;->a()D

    move-result-wide v3

    double-to-float v15, v3

    .line 193
    const-wide/high16 v3, 0x4000

    float-to-double v5, v15

    invoke-static {v5, v6}, Ljava/lang/Math;->log(D)D

    move-result-wide v5

    sget-wide v7, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:D

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    double-to-float v0, v3

    move/from16 v16, v0

    .line 196
    div-float v17, v15, v16

    .line 198
    new-instance v18, Landroid/text/TextPaint;

    invoke-direct/range {v18 .. v18}, Landroid/text/TextPaint;-><init>()V

    .line 200
    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 202
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-virtual {v3}, LJk;->b()LJI;

    move-result-object v6

    .line 203
    new-instance v3, LJp;

    invoke-virtual {v6}, LJI;->a()D

    move-result-wide v4

    invoke-virtual {v6}, LJI;->b()D

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->getWidth()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v8, v15

    float-to-double v8, v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->getHeight()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v10, v15

    float-to-double v10, v10

    invoke-direct/range {v3 .. v11}, LJp;-><init>(DDDD)V

    .line 205
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-virtual {v4, v3}, LJk;->a(LJp;)LJo;

    move-result-object v3

    .line 208
    new-instance v19, Landroid/util/SparseArray;

    invoke-direct/range {v19 .. v19}, Landroid/util/SparseArray;-><init>()V

    .line 209
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 211
    new-instance v4, LJK;

    invoke-direct {v4}, LJK;-><init>()V

    .line 212
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-static {v5, v3, v4}, LJB;->a(LJk;LJo;LJK;)V

    .line 213
    invoke-static {v4}, Lcom/google/android/apps/docs/editors/utils/SwigUtils;->a(LJK;)[I

    move-result-object v21

    .line 214
    invoke-virtual {v4}, LJK;->a()V

    .line 216
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    invoke-virtual {v3}, LJk;->a()LJm;

    move-result-object v5

    .line 217
    const/4 v4, -0x1

    .line 218
    const/4 v3, -0x1

    .line 219
    invoke-virtual {v5}, LJm;->a()Z

    move-result v6

    if-eqz v6, :cond_198

    .line 220
    invoke-virtual {v5}, LJm;->a()LJg;

    move-result-object v3

    invoke-virtual {v3}, LJg;->a()LJo;

    move-result-object v3

    .line 221
    invoke-virtual {v3}, LJo;->a()I

    move-result v4

    .line 222
    invoke-virtual {v3}, LJo;->b()I

    move-result v3

    move v10, v3

    move v11, v4

    .line 225
    :goto_a5
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    const/4 v3, 0x0

    move v14, v3

    :goto_ac
    move/from16 v0, v22

    if-ge v14, v0, :cond_133

    aget v5, v21, v14

    .line 227
    if-lt v5, v11, :cond_e4

    if-ge v5, v10, :cond_e4

    .line 228
    sget-object v9, LHS;->b:LHS;

    .line 233
    :goto_b8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LIG;

    invoke-virtual {v3, v5}, LIG;->a(I)LHR;

    move-result-object v3

    .line 234
    if-nez v3, :cond_e7

    .line 235
    const-string v3, "NativeListView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cell content not found for list item ("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :goto_e0
    add-int/lit8 v3, v14, 0x1

    move v14, v3

    goto :goto_ac

    .line 230
    :cond_e4
    sget-object v9, LHS;->a:LHS;

    goto :goto_b8

    .line 240
    :cond_e7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LIG;

    invoke-virtual {v4}, LIG;->a()LIL;

    move-result-object v4

    invoke-virtual {v4}, LIL;->a()Z

    move-result v6

    .line 241
    if-eqz v6, :cond_126

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v5, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(ILandroid/util/SparseArray;)F

    move-result v4

    move v13, v4

    .line 242
    :goto_fe
    if-eqz v6, :cond_129

    const/4 v4, 0x0

    move v12, v4

    .line 243
    :goto_102
    mul-float v4, v13, v16

    mul-float v5, v12, v16

    move/from16 v6, v16

    move-object/from16 v7, p1

    move-object/from16 v8, v18

    invoke-virtual/range {v3 .. v9}, LHR;->a(FFFLandroid/graphics/Canvas;Landroid/graphics/Paint;LHS;)V

    .line 248
    new-instance v4, Landroid/graphics/RectF;

    invoke-virtual {v3}, LHR;->a()D

    move-result-wide v5

    double-to-float v5, v5

    add-float/2addr v5, v13

    invoke-virtual {v3}, LHR;->b()D

    move-result-wide v6

    double-to-float v3, v6

    add-float/2addr v3, v12

    invoke-direct {v4, v13, v12, v5, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_e0

    .line 241
    :cond_126
    const/4 v4, 0x0

    move v13, v4

    goto :goto_fe

    .line 242
    :cond_129
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v5, v1}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a(ILandroid/util/SparseArray;)F

    move-result v4

    move v12, v4

    goto :goto_102

    .line 252
    :cond_133
    const/high16 v3, 0x3f80

    div-float v3, v3, v17

    const/high16 v4, 0x3f80

    div-float v4, v4, v17

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->scale(FF)V

    .line 255
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 256
    const v3, -0x777778

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 257
    const/high16 v3, 0x3f80

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 259
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_159
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/RectF;

    .line 260
    iget v4, v3, Landroid/graphics/RectF;->left:F

    mul-float/2addr v4, v15

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v10

    .line 261
    iget v4, v3, Landroid/graphics/RectF;->right:F

    mul-float/2addr v4, v15

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v11

    .line 262
    iget v4, v3, Landroid/graphics/RectF;->top:F

    mul-float/2addr v4, v15

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 263
    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v3, v15

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v12

    .line 265
    int-to-float v4, v11

    int-to-float v5, v5

    int-to-float v6, v11

    int-to-float v7, v12

    move-object/from16 v3, p1

    move-object/from16 v8, v18

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 266
    int-to-float v4, v10

    int-to-float v5, v12

    int-to-float v6, v11

    int-to-float v7, v12

    move-object/from16 v3, p1

    move-object/from16 v8, v18

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_159

    :cond_198
    move v10, v3

    move v11, v4

    goto/16 :goto_a5
.end method

.method protected onLayout(ZIIII)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 171
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->a:LJk;

    if-eqz v0, :cond_a

    .line 173
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/NativeListView;->c()V

    .line 175
    :cond_a
    return-void
.end method
