.class public Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;
.super Landroid/view/ViewGroup;
.source "TrixNativeGridView.java"


# static fields
.field private static final a:D


# instance fields
.field private a:LHT;

.field private a:LIB;

.field private a:LJb;

.field private a:Landroid/graphics/Rect;

.field private final a:Landroid/os/Handler;

.field private a:Landroid/view/accessibility/AccessibilityManager;

.field private a:Landroid/widget/EditText;

.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/graphics/Point;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private a:Z

.field private b:D

.field private b:Z

.field private c:D

.field private d:D

.field private e:D


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 78
    const-wide/high16 v0, 0x4000

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const-wide/16 v1, 0x0

    .line 154
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 133
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/os/Handler;

    .line 135
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Z

    .line 136
    iput-wide v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b:D

    .line 137
    iput-wide v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->c:D

    .line 138
    iput-wide v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->d:D

    .line 139
    iput-wide v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->e:D

    .line 141
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/LinkedList;

    .line 145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/Map;

    .line 147
    iput-boolean v3, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b:Z

    .line 156
    invoke-virtual {p0, v3}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->setBackgroundColor(I)V

    .line 157
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->setFocusable(Z)V

    .line 158
    return-void
.end method

.method private a(I)F
    .registers 5
    .parameter

    .prologue
    .line 693
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    new-instance v1, LJo;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, LJo;-><init>(II)V

    invoke-virtual {v0, v1}, LJb;->b(LJo;)LJn;

    move-result-object v0

    .line 694
    invoke-virtual {v0}, LJn;->b()D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(ILandroid/util/SparseArray;)F
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;)F"
        }
    .end annotation

    .prologue
    .line 682
    invoke-virtual {p2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 683
    if-eqz v0, :cond_d

    .line 684
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 689
    :goto_c
    return v0

    .line 687
    :cond_d
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(I)F

    move-result v0

    .line 688
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_c
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LHT;
    .registers 2
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LHT;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LIB;
    .registers 2
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)LJb;
    .registers 2
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)Ljava/util/Map;
    .registers 2
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/Map;

    return-object v0
.end method

.method private a(LIu;)V
    .registers 13
    .parameter

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v0}, LJb;->a()D

    move-result-wide v0

    double-to-float v0, v0

    .line 643
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v1}, LJb;->b()LJI;

    move-result-object v1

    .line 644
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v2, v1}, LJb;->a(LJI;)LJJ;

    move-result-object v2

    .line 645
    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    new-instance v4, LJI;

    invoke-virtual {v1}, LJI;->a()D

    move-result-wide v5

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v0

    float-to-double v7, v7

    add-double/2addr v5, v7

    invoke-virtual {v1}, LJI;->b()D

    move-result-wide v7

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v0, v1, v0

    float-to-double v0, v0

    add-double/2addr v0, v7

    invoke-direct {v4, v5, v6, v0, v1}, LJI;-><init>(DD)V

    invoke-virtual {v3, v4}, LJb;->a(LJI;)LJJ;

    move-result-object v0

    .line 648
    invoke-virtual {v2}, LJJ;->a()I

    move-result v1

    .line 649
    invoke-virtual {v0}, LJJ;->a()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 650
    invoke-virtual {v2}, LJJ;->b()I

    move-result v2

    .line 651
    invoke-virtual {v0}, LJJ;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 653
    new-instance v4, LJq;

    sub-int/2addr v3, v1

    sub-int/2addr v0, v2

    invoke-direct {v4, v1, v2, v3, v0}, LJq;-><init>(IIII)V

    .line 655
    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    .line 656
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 658
    new-instance v0, LJK;

    invoke-direct {v0}, LJK;-><init>()V

    .line 659
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-static {v1, v4, v0}, LJB;->a(LJb;LJq;LJK;)V

    .line 660
    invoke-static {v0}, Lcom/google/android/apps/docs/editors/utils/SwigUtils;->a(LJK;)[I

    move-result-object v9

    .line 661
    invoke-virtual {v0}, LJK;->a()V

    .line 663
    array-length v0, v9

    div-int/lit8 v10, v0, 0x2

    .line 665
    const/4 v0, 0x0

    move v6, v0

    :goto_72
    if-ge v6, v10, :cond_bf

    .line 666
    mul-int/lit8 v0, v6, 0x2

    aget v2, v9, v0

    .line 667
    mul-int/lit8 v0, v6, 0x2

    add-int/lit8 v0, v0, 0x1

    aget v1, v9, v0

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    invoke-virtual {v0, v2, v1}, LIB;->a(II)LHR;

    move-result-object v5

    .line 670
    if-nez v5, :cond_b2

    .line 671
    const-string v0, "TrixGridView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cell content not found for cell ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    :goto_ae
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_72

    .line 675
    :cond_b2
    invoke-direct {p0, v2, v7}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(ILandroid/util/SparseArray;)F

    move-result v3

    .line 676
    invoke-direct {p0, v1, v8}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b(ILandroid/util/SparseArray;)F

    move-result v4

    move-object v0, p1

    .line 677
    invoke-interface/range {v0 .. v5}, LIu;->a(IIFFLHR;)V

    goto :goto_ae

    .line 679
    :cond_bf
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .registers 16
    .parameter

    .prologue
    const/4 v13, 0x0

    const/4 v4, -0x1

    const/high16 v12, 0x3f80

    .line 540
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    if-nez v0, :cond_9

    .line 638
    :cond_8
    return-void

    .line 544
    :cond_9
    invoke-direct {p0, v13}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Z)V

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v0}, LJb;->a()D

    move-result-wide v0

    double-to-float v8, v0

    .line 547
    const-wide/high16 v0, 0x4000

    float-to-double v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    sget-wide v5, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:D

    div-double/2addr v2, v5

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v5, v0

    .line 550
    div-float v11, v8, v5

    .line 552
    new-instance v7, Landroid/text/TextPaint;

    invoke-direct {v7}, Landroid/text/TextPaint;-><init>()V

    .line 554
    invoke-virtual {p1, v11, v11}, Landroid/graphics/Canvas;->scale(FF)V

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v0}, LJb;->a()LJl;

    move-result-object v0

    .line 560
    invoke-virtual {v0}, LJl;->a()Z

    move-result v1

    if-eqz v1, :cond_cb

    .line 561
    invoke-virtual {v0}, LJl;->a()LIX;

    move-result-object v0

    invoke-virtual {v0}, LIX;->a()LJq;

    move-result-object v0

    .line 562
    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v0}, LJq;->a()I

    move-result v1

    invoke-virtual {v0}, LJq;->b()I

    move-result v3

    invoke-virtual {v0}, LJq;->c()I

    move-result v6

    invoke-virtual {v0}, LJq;->d()I

    move-result v0

    invoke-direct {v2, v1, v3, v6, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 569
    :goto_59
    if-eqz v2, :cond_cd

    iget v3, v2, Landroid/graphics/Rect;->left:I

    .line 570
    :goto_5d
    if-eqz v2, :cond_61

    iget v4, v2, Landroid/graphics/Rect;->top:I

    .line 572
    :cond_61
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 573
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 575
    new-instance v0, LIs;

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v10}, LIs;-><init>(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;Landroid/graphics/Rect;IIFLandroid/graphics/Canvas;Landroid/graphics/Paint;FLjava/util/Map;Ljava/util/ArrayList;)V

    .line 611
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(LIu;)V

    .line 613
    div-float v0, v12, v11

    div-float v1, v12, v11

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 616
    invoke-virtual {v7, v13}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 617
    const v0, -0x333334

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 618
    invoke-virtual {v7, v12}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 620
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_cf

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LIt;

    .line 621
    invoke-static {v0}, LIt;->a(LIt;)Landroid/graphics/Rect;

    move-result-object v8

    .line 622
    invoke-virtual {v0}, LIt;->a()I

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 623
    iget v2, v8, Landroid/graphics/Rect;->right:I

    int-to-float v3, v2

    iget v2, v8, Landroid/graphics/Rect;->top:I

    int-to-float v4, v2

    iget v2, v8, Landroid/graphics/Rect;->right:I

    int-to-float v5, v2

    iget v2, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v2

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 624
    invoke-virtual {v0}, LIt;->b()I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 625
    iget v0, v8, Landroid/graphics/Rect;->left:I

    int-to-float v3, v0

    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    iget v0, v8, Landroid/graphics/Rect;->right:I

    int-to-float v5, v0

    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v0

    move-object v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_8c

    .line 564
    :cond_cb
    const/4 v2, 0x0

    goto :goto_59

    :cond_cd
    move v3, v4

    .line 569
    goto :goto_5d

    .line 628
    :cond_cf
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 629
    const/high16 v0, 0x4040

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 630
    invoke-interface {v9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_e1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 631
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 632
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 633
    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 636
    new-instance v0, Landroid/graphics/Rect;

    iget v3, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v3, v3, 0x1

    iget v4, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v4, v4, 0x1

    iget v5, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v5, v5, -0x1

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {v0, v3, v4, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p1, v0, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_e1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V
    .registers 1
    .parameter

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->d()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 7
    .parameter

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Z

    if-eqz v0, :cond_7

    if-nez p1, :cond_7

    .line 283
    :goto_6
    return-void

    .line 262
    :cond_7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Z

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v0}, LJb;->a()LJI;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    invoke-virtual {v0}, LIB;->a()LKj;

    move-result-object v0

    .line 268
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    invoke-virtual {v1}, LIB;->b()LKj;

    move-result-object v1

    .line 269
    if-eqz v0, :cond_1f

    if-nez v1, :cond_2a

    .line 270
    :cond_1f
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->e:D

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->d:D

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->c:D

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b:D

    goto :goto_6

    .line 274
    :cond_2a
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    new-instance v3, LJo;

    invoke-virtual {v0}, LKj;->a()I

    move-result v4

    invoke-virtual {v0}, LKj;->c()I

    move-result v0

    invoke-direct {v3, v4, v0}, LJo;-><init>(II)V

    invoke-virtual {v2, v3}, LJb;->b(LJo;)LJn;

    move-result-object v0

    .line 276
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    new-instance v3, LJo;

    invoke-virtual {v1}, LKj;->a()I

    move-result v4

    invoke-virtual {v1}, LKj;->c()I

    move-result v1

    invoke-direct {v3, v4, v1}, LJo;-><init>(II)V

    invoke-virtual {v2, v3}, LJb;->a(LJo;)LJn;

    move-result-object v1

    .line 279
    invoke-virtual {v1}, LJn;->b()D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->c:D

    .line 280
    invoke-virtual {v1}, LJn;->a()D

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->e:D

    .line 281
    invoke-virtual {v0}, LJn;->b()D

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b:D

    .line 282
    invoke-virtual {v0}, LJn;->a()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->d:D

    goto :goto_6
.end method

.method private b(I)F
    .registers 5
    .parameter

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    new-instance v1, LJo;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, LJo;-><init>(II)V

    invoke-virtual {v0, v1}, LJb;->a(LJo;)LJn;

    move-result-object v0

    .line 710
    invoke-virtual {v0}, LJn;->b()D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private b(ILandroid/util/SparseArray;)F
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;)F"
        }
    .end annotation

    .prologue
    .line 698
    invoke-virtual {p2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 699
    if-eqz v0, :cond_d

    .line 700
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 705
    :goto_c
    return v0

    .line 703
    :cond_d
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b(I)F

    move-result v0

    .line 704
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {p2, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_c
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V
    .registers 1
    .parameter

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->c()V

    return-void
.end method

.method private c()V
    .registers 1

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->invalidate()V

    .line 232
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->f()V

    .line 233
    return-void
.end method

.method private d()V
    .registers 3

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Z

    if-nez v0, :cond_5

    .line 254
    :goto_4
    return-void

    .line 243
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Z

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/os/Handler;

    new-instance v1, LIp;

    invoke-direct {v1, p0}, LIp;-><init>(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_4
.end method

.method private e()V
    .registers 3

    .prologue
    .line 289
    const/4 v0, 0x1

    invoke-static {p0, v0}, LaS;->a(Landroid/view/View;I)V

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/view/accessibility/AccessibilityManager;

    .line 293
    new-instance v0, LIq;

    invoke-direct {v0, p0}, LIq;-><init>(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    invoke-static {p0, v0}, LaS;->a(Landroid/view/View;Lag;)V

    .line 304
    return-void
.end method

.method private f()V
    .registers 3

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_27

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 312
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_12
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 313
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->removeView(Landroid/view/View;)V

    goto :goto_12

    .line 315
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 317
    :cond_27
    return-void
.end method

.method private g()V
    .registers 3

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v0}, LJb;->a()D

    move-result-wide v0

    double-to-float v0, v0

    .line 324
    new-instance v1, LIr;

    invoke-direct {v1, p0, v0}, LIr;-><init>(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;F)V

    .line 342
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(LIu;)V

    .line 343
    return-void
.end method

.method private h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 505
    new-instance v0, LJq;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, LJq;-><init>(IIII)V

    .line 506
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v1, v0}, LJb;->a(LJq;)V

    .line 507
    invoke-virtual {v0}, LJq;->a()V

    .line 508
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->invalidate()V

    .line 509
    return-void
.end method


# virtual methods
.method public a()D
    .registers 3

    .prologue
    .line 724
    iget-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->d:D

    return-wide v0
.end method

.method public a()I
    .registers 5

    .prologue
    .line 714
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Z)V

    .line 715
    iget-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->d:D

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v2}, LJb;->a()D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public a()LIB;
    .registers 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    return-object v0
.end method

.method public a(Landroid/graphics/Point;)LJJ;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 736
    new-instance v0, LJJ;

    iget v1, p1, Landroid/graphics/Point;->x:I

    iget v2, p1, Landroid/graphics/Point;->y:I

    invoke-direct {v0, v1, v2}, LJJ;-><init>(II)V

    .line 737
    new-instance v1, LJJ;

    invoke-direct {v1, v3, v3}, LJJ;-><init>(II)V

    .line 738
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v2, v0, v1}, LJb;->a(LJJ;LJJ;)Z

    .line 739
    return-object v1
.end method

.method public a()LJb;
    .registers 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    return-object v0
.end method

.method public a(II)LJq;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 743
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    new-instance v1, LJq;

    invoke-direct {v1, p1, p2, v2, v2}, LJq;-><init>(IIII)V

    invoke-virtual {v0, v1}, LJb;->a(LJq;)LJq;

    move-result-object v0

    return-object v0
.end method

.method public a()Landroid/graphics/Rect;
    .registers 2

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/graphics/Rect;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    if-eqz v0, :cond_e

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    invoke-virtual {v0}, LIB;->a()V

    .line 163
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    .line 164
    iput-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    .line 166
    :cond_e
    return-void
.end method

.method public a(LIB;)V
    .registers 10
    .parameter

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a()V

    .line 181
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    .line 182
    invoke-virtual {p1}, LIB;->a()LJb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    .line 183
    invoke-virtual {p1}, LIB;->a()LHT;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LHT;

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LIB;

    new-instance v1, LIo;

    invoke-direct {v1, p0}, LIo;-><init>(Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;)V

    invoke-virtual {v0, v1}, LIB;->a(LIF;)V

    .line 216
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Z)V

    .line 218
    const-wide/high16 v0, 0x3ff0

    .line 219
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    new-instance v3, LJI;

    iget-wide v4, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b:D

    iget-wide v6, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->c:D

    invoke-direct {v3, v4, v5, v6, v7}, LJI;-><init>(DD)V

    invoke-virtual {v2, v3}, LJb;->a(LJI;)V

    .line 220
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v2, v0, v1}, LJb;->a(D)V

    .line 221
    iget-wide v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b:D

    mul-double/2addr v2, v0

    double-to-int v2, v2

    iget-wide v3, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->c:D

    mul-double/2addr v0, v3

    double-to-int v0, v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->scrollTo(II)V

    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->h()V

    .line 224
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->e()V

    .line 225
    return-void
.end method

.method public a(LJJ;)V
    .registers 2
    .parameter

    .prologue
    .line 463
    return-void
.end method

.method public a(Landroid/graphics/Rect;LIW;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/16 v6, 0x12

    const/4 v5, 0x0

    .line 381
    iput-object p1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/graphics/Rect;

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    if-nez v0, :cond_19

    .line 384
    new-instance v0, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 389
    :cond_19
    invoke-virtual {p2}, LIW;->b()LIT;

    move-result-object v0

    .line 390
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, LIT;->d()S

    move-result v2

    invoke-virtual {v0}, LIT;->a()S

    move-result v3

    invoke-virtual {v0}, LIT;->b()S

    move-result v4

    invoke-virtual {v0}, LIT;->c()S

    move-result v0

    invoke-static {v2, v3, v4, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setBackgroundColor(I)V

    .line 394
    invoke-virtual {p2}, LIW;->a()LIT;

    move-result-object v0

    .line 395
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, LIT;->d()S

    move-result v2

    invoke-virtual {v0}, LIT;->a()S

    move-result v3

    invoke-virtual {v0}, LIT;->b()S

    move-result v4

    invoke-virtual {v0}, LIT;->c()S

    move-result v0

    invoke-static {v2, v3, v4, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {p2}, LIW;->a()LIU;

    move-result-object v1

    invoke-virtual {v1}, LIU;->a()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextSize(F)V

    .line 401
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {p2}, LIW;->a()LIU;

    move-result-object v1

    invoke-static {v1}, LHQ;->a(LIU;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 406
    invoke-virtual {p2}, LIW;->a()LJL;

    move-result-object v0

    .line 407
    invoke-virtual {p2}, LIW;->a()LJc;

    move-result-object v1

    .line 409
    sget-object v2, LJL;->a:LJL;

    if-ne v0, v2, :cond_d4

    .line 410
    const/16 v0, 0x30

    .line 417
    :goto_7b
    sget-object v2, LJc;->a:LJc;

    if-ne v1, v2, :cond_de

    .line 418
    const/4 v1, 0x3

    .line 425
    :goto_80
    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    or-int/2addr v0, v1

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setGravity(I)V

    .line 428
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 432
    invoke-virtual {p2}, LIW;->a()S

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_9b

    .line 433
    new-instance v1, Landroid/text/style/UnderlineSpan;

    invoke-direct {v1}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v0, v1, v5, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 438
    :cond_9b
    invoke-virtual {p2}, LIW;->a()S

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_ab

    .line 439
    new-instance v1, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v1}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {v0, v1, v5, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 442
    :cond_ab
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v1, v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_e6

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 454
    :goto_cd
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->forceLayout()V

    .line 455
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->requestLayout()V

    .line 456
    return-void

    .line 411
    :cond_d4
    sget-object v2, LJL;->c:LJL;

    if-ne v0, v2, :cond_db

    .line 412
    const/16 v0, 0x10

    goto :goto_7b

    .line 414
    :cond_db
    const/16 v0, 0x50

    goto :goto_7b

    .line 419
    :cond_de
    sget-object v2, LJc;->c:LJc;

    if-ne v1, v2, :cond_e4

    .line 420
    const/4 v1, 0x1

    goto :goto_80

    .line 422
    :cond_e4
    const/4 v1, 0x5

    goto :goto_80

    .line 449
    :cond_e6
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_cd
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 467
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setSelection(I)V

    .line 468
    return-void
.end method

.method public b()D
    .registers 3

    .prologue
    .line 728
    iget-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->e:D

    return-wide v0
.end method

.method public b()I
    .registers 5

    .prologue
    .line 719
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Z)V

    .line 720
    iget-wide v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->e:D

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    invoke-virtual {v2}, LJb;->a()D

    move-result-wide v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public b()V
    .registers 3

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->removeView(Landroid/view/View;)V

    .line 473
    return-void
.end method

.method public c()D
    .registers 7

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getWidth()I

    move-result v0

    int-to-double v0, v0

    iget-wide v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->d:D

    div-double/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getHeight()I

    move-result v2

    int-to-double v2, v2

    iget-wide v4, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->e:D

    div-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_d

    .line 350
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 376
    :goto_c
    return v0

    .line 353
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 354
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->g()V

    .line 358
    :cond_18
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 359
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Landroid/graphics/Point;)LJJ;

    move-result-object v0

    .line 360
    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/Map;

    new-instance v2, Landroid/graphics/Point;

    invoke-virtual {v0}, LJJ;->a()I

    move-result v3

    invoke-virtual {v0}, LJJ;->b()I

    move-result v0

    invoke-direct {v2, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 361
    if-eqz v0, :cond_71

    .line 363
    const/16 v1, 0x80

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 365
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;)V

    .line 367
    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 370
    const v1, 0x8000

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 371
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;)V

    .line 373
    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 376
    :cond_71
    const/4 v0, 0x1

    goto :goto_c
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 13
    .parameter

    .prologue
    const/4 v10, 0x1

    .line 513
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    .line 515
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b:Z

    if-eqz v0, :cond_34

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/LinkedList;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 517
    :goto_12
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_34

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long v3, v1, v3

    const-wide/16 v5, 0x3e8

    cmp-long v0, v3, v5

    if-lez v0, :cond_34

    .line 518
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    goto :goto_12

    .line 522
    :cond_34
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a(Landroid/graphics/Canvas;)V

    .line 524
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b:Z

    if-eqz v0, :cond_a8

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v10, :cond_a8

    .line 525
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    .line 526
    const/high16 v0, 0x4180

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 527
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getScrollX()I

    move-result v4

    add-int/2addr v4, v0

    .line 528
    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->getScrollY()I

    move-result v5

    add-int/2addr v5, v0

    .line 529
    const/4 v0, -0x1

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 530
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v6, v4, 0x50

    add-int/lit8 v7, v5, 0x1e

    invoke-direct {v0, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 531
    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 533
    const/high16 v0, -0x100

    invoke-virtual {v3, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 534
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    int-to-double v6, v0

    const-wide v8, 0x408f400000000000L

    mul-double/2addr v6, v8

    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v0, v1, v8

    long-to-double v0, v0

    div-double v0, v6, v0

    .line 535
    const-string v2, "FPS %02.1f"

    new-array v6, v10, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v4

    add-int/lit8 v2, v5, 0x14

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 537
    :cond_a8
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:LJb;

    if-eqz v0, :cond_7

    .line 493
    invoke-direct {p0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->h()V

    .line 497
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_29

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, -0x2

    iget-object v4, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->a:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/EditText;->layout(IIII)V

    .line 502
    :cond_29
    return-void
.end method

.method protected onMeasure(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 486
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->measureChildren(II)V

    .line 487
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 488
    return-void
.end method

.method public setShowDebugInfo(Z)V
    .registers 2
    .parameter

    .prologue
    .line 747
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixNativeGridView;->b:Z

    .line 748
    return-void
.end method
