.class public Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;
.super Landroid/view/View;
.source "TrixSheetEdgeView.java"


# instance fields
.field private a:LIx;


# direct methods
.method public constructor <init>(Landroid/content/Context;LIx;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->a:LIx;

    .line 41
    sget-object v0, LIw;->a:LIw;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setActive(LIw;)V

    .line 42
    return-void
.end method

.method private a(LIw;)V
    .registers 4
    .parameter

    .prologue
    .line 86
    sget-object v0, LIv;->b:[I

    invoke-virtual {p1}, LIw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1e

    .line 100
    :goto_b
    return-void

    .line 88
    :pswitch_c
    sget v0, LsC;->trix_sheet_inactive_inactive:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setBackgroundResource(I)V

    goto :goto_b

    .line 92
    :pswitch_12
    sget v0, LsC;->trix_sheet_inactive_active:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setBackgroundResource(I)V

    goto :goto_b

    .line 96
    :pswitch_18
    sget v0, LsC;->trix_sheet_active_inactive:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setBackgroundResource(I)V

    goto :goto_b

    .line 86
    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_c
        :pswitch_12
        :pswitch_18
    .end packed-switch
.end method

.method private a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 68
    if-eqz p1, :cond_8

    .line 69
    sget v0, LsC;->trix_sheet_active_left:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setBackgroundResource(I)V

    .line 73
    :goto_7
    return-void

    .line 71
    :cond_8
    sget v0, LsC;->trix_sheet_inactive_left:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setBackgroundResource(I)V

    goto :goto_7
.end method

.method private b(Z)V
    .registers 3
    .parameter

    .prologue
    .line 77
    if-eqz p1, :cond_8

    .line 78
    sget v0, LsC;->trix_sheet_active_right:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setBackgroundResource(I)V

    .line 82
    :goto_7
    return-void

    .line 80
    :cond_8
    sget v0, LsC;->trix_sheet_inactive_right:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->setBackgroundResource(I)V

    goto :goto_7
.end method


# virtual methods
.method public setActive(LIw;)V
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50
    sget-object v2, LIv;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->a:LIx;

    invoke-virtual {v3}, LIx;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_28

    .line 64
    :goto_f
    return-void

    .line 52
    :pswitch_10
    sget-object v2, LIw;->a:LIw;

    if-eq p1, v2, :cond_18

    :goto_14
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->a(Z)V

    goto :goto_f

    :cond_18
    move v0, v1

    goto :goto_14

    .line 56
    :pswitch_1a
    sget-object v2, LIw;->a:LIw;

    if-eq p1, v2, :cond_22

    :goto_1e
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->b(Z)V

    goto :goto_f

    :cond_22
    move v0, v1

    goto :goto_1e

    .line 60
    :pswitch_24
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetEdgeView;->a(LIw;)V

    goto :goto_f

    .line 50
    :pswitch_data_28
    .packed-switch 0x1
        :pswitch_10
        :pswitch_1a
        :pswitch_24
    .end packed-switch
.end method
