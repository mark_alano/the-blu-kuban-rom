.class public Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;
.super Landroid/widget/RelativeLayout;
.source "TrixSheetTabView.java"


# instance fields
.field private a:I

.field private a:Landroid/widget/TextView;

.field private a:Z

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;
    .registers 4
    .parameter

    .prologue
    .line 21
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, LsF;->trix_sheet_tab:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;

    .line 23
    invoke-direct {v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a()V

    .line 24
    return-object v0
.end method

.method private a()V
    .registers 2

    .prologue
    .line 41
    sget v0, LsD;->sheet_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a:Landroid/widget/TextView;

    .line 42
    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a:I

    return v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a:Z

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->b:I

    return v0
.end method

.method public setActive(Z)V
    .registers 2
    .parameter

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->setSelected(Z)V

    .line 63
    iput-boolean p1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a:Z

    .line 64
    return-void
.end method

.method public setLogicalIndex(I)V
    .registers 2
    .parameter

    .prologue
    .line 75
    iput p1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->b:I

    .line 76
    return-void
.end method

.method public setSheetId(I)V
    .registers 2
    .parameter

    .prologue
    .line 49
    iput p1, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a:I

    .line 50
    return-void
.end method

.method public setSheetName(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/editors/trix/view/TrixSheetTabView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 55
    return-void
.end method
