.class public interface abstract Lcom/google/android/apps/docs/editors/trix/viewmodel/CellContentFactory;
.super Ljava/lang/Object;
.source "CellContentFactory.java"


# annotations
.annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
.end annotation


# virtual methods
.method public abstract allocate(J)I
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation
.end method

.method public abstract free(I)V
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation
.end method
