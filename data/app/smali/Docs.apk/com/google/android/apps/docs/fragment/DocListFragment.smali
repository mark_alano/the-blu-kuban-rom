.class public Lcom/google/android/apps/docs/fragment/DocListFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "DocListFragment.java"

# interfaces
.implements LE;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements LiY;
.implements Liw;
.implements Ljb;
.implements LoQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/view/RoboFragment;",
        "LE",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "LiY;",
        "Liw;",
        "Ljb;",
        "LoQ;"
    }
.end annotation


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LLG;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LPm;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LUL;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LWQ;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/view/DocListView;

.field public a:Lev;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LiX;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Liv;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lja;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LlE;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llz;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Lrl;

.field private a:Z

.field private b:Z

.field private c:Ljava/lang/String;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 80
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 185
    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    .line 186
    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->b:Z

    .line 187
    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Z

    return-void
.end method

.method private a()LLH;
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 608
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lja;

    invoke-interface {v0}, Lja;->a()LiQ;

    move-result-object v0

    .line 609
    if-eqz v0, :cond_35

    .line 610
    invoke-interface {v0}, LiQ;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_21

    .line 611
    new-instance v0, LLH;

    sget-object v1, LmK;->v:LmK;

    invoke-virtual {v1}, LmK;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LmK;->v:LmK;

    invoke-virtual {v2}, LmK;->a()Lrl;

    move-result-object v2

    invoke-direct {v0, v1, v2, v3}, LLH;-><init>(Ljava/lang/String;Lrl;LLD;)V

    .line 621
    :goto_20
    return-object v0

    .line 614
    :cond_21
    invoke-interface {v0}, LiQ;->a()LmK;

    move-result-object v1

    .line 615
    if-eqz v1, :cond_35

    .line 616
    new-instance v0, LLH;

    invoke-virtual {v1}, LmK;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, LmK;->a()Lrl;

    move-result-object v1

    invoke-direct {v0, v2, v1, v3}, LLH;-><init>(Ljava/lang/String;Lrl;LLD;)V

    goto :goto_20

    .line 621
    :cond_35
    new-instance v0, LLH;

    const-string v1, "default"

    sget-object v2, Lrl;->b:Lrl;

    invoke-direct {v0, v1, v2, v3}, LLH;-><init>(Ljava/lang/String;Lrl;LLD;)V

    goto :goto_20
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/DocListFragment;)Lcom/google/android/apps/docs/view/DocListView;
    .registers 2
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    return-object v0
.end method

.method private a(Ljava/lang/String;Lrl;)Lrl;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 625
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    .line 626
    if-nez v0, :cond_9

    .line 645
    :cond_8
    :goto_8
    return-object p2

    .line 632
    :cond_9
    :try_start_9
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lev;

    invoke-interface {v1, v0}, Lev;->a(Ljava/lang/String;)Let;
    :try_end_e
    .catch Lew; {:try_start_9 .. :try_end_e} :catch_33

    move-result-object v0

    .line 638
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sorting-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Let;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 640
    sget-object v1, Lrl;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrl;

    .line 641
    if-eqz v0, :cond_8

    move-object p2, v0

    goto :goto_8

    .line 633
    :catch_33
    move-exception v0

    .line 634
    const-string v1, "DocListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while trying to load sorting preference - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lew;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8
.end method

.method public static a(Landroid/view/MenuItem;Llv;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 378
    invoke-virtual {p1}, Llv;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    sget v0, Len;->menu_move_folder:I

    :goto_8
    invoke-interface {p0, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 380
    return-void

    .line 378
    :cond_c
    sget v0, Len;->menu_move_file:I

    goto :goto_8
.end method

.method private a(Ljava/lang/String;Lrl;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    .line 650
    if-nez v0, :cond_9

    .line 663
    :goto_8
    return-void

    .line 655
    :cond_9
    :try_start_9
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lev;

    invoke-interface {v1, v0}, Lev;->a(Ljava/lang/String;)Let;

    move-result-object v0

    .line 656
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sorting-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lrl;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Let;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lev;

    invoke-interface {v1, v0}, Lev;->a(Let;)V
    :try_end_2e
    .catch Lew; {:try_start_9 .. :try_end_2e} :catch_2f

    goto :goto_8

    .line 659
    :catch_2f
    move-exception v0

    .line 660
    const-string v1, "DocListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while trying to save sorting preference - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lew;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8
.end method

.method private a(LkO;)V
    .registers 5
    .parameter

    .prologue
    .line 671
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1}, LkO;->a()LkY;

    move-result-object v1

    invoke-virtual {p1}, LkO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a(Landroid/content/Context;LkY;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 673
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Landroid/content/Intent;)V

    .line 674
    return-void
.end method

.method private b()Lrl;
    .registers 3

    .prologue
    .line 597
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()LLH;

    move-result-object v0

    .line 598
    invoke-static {v0}, LLH;->a(LLH;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, LLH;->a(LLH;)Lrl;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Ljava/lang/String;Lrl;)Lrl;

    move-result-object v0

    return-object v0
.end method

.method private c(Lrl;)V
    .registers 3
    .parameter

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_9

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/DocListView;->setSorting(Lrl;)V

    .line 590
    :cond_9
    return-void
.end method

.method private d(Lrl;)V
    .registers 3
    .parameter

    .prologue
    .line 602
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()LLH;

    move-result-object v0

    .line 604
    invoke-static {v0}, LLH;->a(LLH;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Ljava/lang/String;Lrl;)V

    .line 605
    return-void
.end method

.method private s()V
    .registers 4

    .prologue
    .line 700
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Z

    .line 701
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()Z

    move-result v0

    if-nez v0, :cond_14

    .line 710
    :cond_13
    :goto_13
    return-void

    .line 705
    :cond_14
    const-string v0, "DocListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "triggering doclist cursor fillData "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Z

    .line 707
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lja;

    invoke-interface {v0}, Lja;->a()LiQ;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 708
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lja;

    invoke-interface {v1}, Lja;->a()LiQ;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(LiQ;)V

    goto :goto_13
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()I

    move-result v0

    return v0
.end method

.method public a(ILandroid/os/Bundle;)LL;
    .registers 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "LL",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_25

    const/4 v0, 0x1

    :goto_e
    invoke-static {v0}, Lagu;->b(Z)V

    .line 732
    new-instance v0, LmI;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/view/DocListView;->a()LmJ;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, LmI;-><init>(Landroid/content/Context;Llf;LmJ;Ljava/lang/String;)V

    return-object v0

    .line 731
    :cond_25
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 226
    const-string v0, "DocListFragment"

    const-string v1, "in DLF.onCreateView"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    sget v0, Lej;->doc_list_view:I

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 229
    sget v0, Leh;->doc_list_view:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/DocListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-static {v0}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/view/DocListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/view/DocListView;->setTagName(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/view/DocListView;->setVisibility(I)V

    .line 238
    iget-boolean v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->p()V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Landroid/view/View;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Liv;

    invoke-interface {v0, p0}, Liv;->a(Liw;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lja;

    invoke-interface {v0, p0}, Lja;->a(Ljb;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LiX;

    invoke-interface {v0, p0}, LiX;->a(LiY;)V

    .line 247
    if-eqz p3, :cond_60

    .line 248
    const-string v0, "sortKind"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lrl;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lrl;

    .line 251
    :cond_60
    if-eqz v2, :cond_65

    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->q()V

    .line 255
    :cond_65
    return-object v1
.end method

.method public a(IZI)Landroid/view/animation/Animation;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 812
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 815
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lef;->navigation_panel_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 816
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v0

    .line 817
    const/16 v0, 0x2002

    if-ne p1, v0, :cond_3a

    .line 818
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->d:Z

    if-eqz v0, :cond_33

    .line 819
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v1, v2

    invoke-direct {v0, v1, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 833
    :goto_2d
    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 834
    :goto_32
    return-object v0

    .line 821
    :cond_33
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v1, v1

    invoke-direct {v0, v1, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_2d

    .line 823
    :cond_3a
    const/16 v0, 0x1001

    if-ne p1, v0, :cond_4f

    .line 824
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->d:Z

    if-eqz v0, :cond_49

    .line 825
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    neg-float v1, v2

    invoke-direct {v0, v3, v1, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_2d

    .line 827
    :cond_49
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v1, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_2d

    .line 830
    :cond_4f
    const/4 v0, 0x0

    goto :goto_32
.end method

.method public a()Lcom/google/android/apps/docs/view/DocListView;
    .registers 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    return-object v0
.end method

.method public a()Ljava/util/EnumSet;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lrl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lja;

    invoke-interface {v0}, Lja;->a()LiQ;

    move-result-object v0

    invoke-interface {v0}, LiQ;->a()LmK;

    move-result-object v0

    .line 473
    const-class v1, Lrl;

    invoke-static {v1}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 474
    if-eqz v0, :cond_29

    .line 475
    invoke-virtual {v0}, LmK;->a()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->retainAll(Ljava/util/Collection;)Z

    .line 480
    :goto_19
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 481
    invoke-virtual {v0}, Lfb;->a()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->retainAll(Ljava/util/Collection;)Z

    .line 483
    return-object v1

    .line 477
    :cond_29
    sget-object v0, LmK;->a:LmK;

    invoke-virtual {v0}, LmK;->a()Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/EnumSet;->retainAll(Ljava/util/Collection;)Z

    goto :goto_19
.end method

.method public a()Lrl;
    .registers 2

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-nez v0, :cond_7

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lrl;

    .line 467
    :goto_6
    return-object v0

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()Lrl;

    move-result-object v0

    goto :goto_6
.end method

.method public a()V
    .registers 1

    .prologue
    .line 690
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    .line 691
    return-void
.end method

.method public a(LL;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LL",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 745
    const-string v0, "DocListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onLoaderReset "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_34

    .line 747
    const-string v0, "DocListFragment"

    const-string v1, "calling doclistView.swapCursor(null)"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(Landroid/database/Cursor;)V

    .line 750
    :cond_34
    return-void
.end method

.method public a(LL;Landroid/database/Cursor;)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LL",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 737
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 738
    const-string v0, "DocListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in onLoadFinished "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/docs/view/DocListView;->a(Landroid/database/Cursor;)V

    .line 740
    return-void
.end method

.method public bridge synthetic a(LL;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 80
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(LL;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(LabA;)V
    .registers 5
    .parameter

    .prologue
    .line 506
    const-string v0, "DocListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in DLF.setSyncing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_23

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/DocListView;->setSyncStatus(LabA;)V

    .line 510
    :cond_23
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 759
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 760
    new-instance v0, LLD;

    invoke-direct {v0, p0}, LLD;-><init>(Lcom/google/android/apps/docs/fragment/DocListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LWQ;

    .line 788
    return-void
.end method

.method a(Landroid/view/ContextMenu;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Liv;

    invoke-interface {v1}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 317
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    invoke-interface {v1, v0, p2}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    .line 318
    if-nez v0, :cond_15

    .line 375
    :goto_14
    return-void

    .line 322
    :cond_15
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    .line 323
    sget v2, Lek;->menu_doclist_context:I

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 325
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llz;

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Llz;->a(Ljava/lang/String;Ljava/util/Set;)Llv;

    move-result-object v1

    .line 328
    sget v2, Leh;->menu_delete:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    .line 330
    sget-object v2, Llx;->a:Llx;

    invoke-virtual {v1, v2}, Llv;->a(Llx;)Z

    move-result v2

    if-nez v2, :cond_44

    .line 331
    sget v2, Leh;->menu_comments:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 333
    :cond_44
    sget-object v2, Llx;->h:Llx;

    invoke-virtual {v1, v2}, Llv;->a(Llx;)Z

    move-result v2

    if-nez v2, :cond_51

    .line 334
    sget v2, Leh;->menu_rename:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 336
    :cond_51
    sget-object v2, Llx;->b:Llx;

    invoke-virtual {v1, v2}, Llv;->a(Llx;)Z

    move-result v2

    if-nez v2, :cond_5e

    .line 337
    sget v2, Leh;->menu_delete:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 339
    :cond_5e
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LZL;->a(Landroid/content/res/Resources;)Z

    move-result v2

    if-nez v2, :cond_70

    sget-object v2, Llx;->c:Llx;

    invoke-virtual {v1, v2}, Llv;->a(Llx;)Z

    move-result v2

    if-nez v2, :cond_75

    .line 341
    :cond_70
    sget v2, Leh;->menu_sharing:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 343
    :cond_75
    sget-object v2, Llx;->d:Llx;

    invoke-virtual {v1, v2}, Llv;->a(Llx;)Z

    move-result v2

    if-nez v2, :cond_82

    .line 344
    sget v2, Leh;->menu_open_with:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 346
    :cond_82
    sget-object v2, Llx;->e:Llx;

    invoke-virtual {v1, v2}, Llv;->a(Llx;)Z

    move-result v2

    if-nez v2, :cond_8f

    .line 347
    sget v2, Leh;->menu_send:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 349
    :cond_8f
    sget-object v2, Llx;->g:Llx;

    invoke-virtual {v1, v2}, Llv;->a(Llx;)Z

    move-result v2

    if-nez v2, :cond_9c

    .line 350
    sget v2, Leh;->menu_send_link:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 352
    :cond_9c
    sget-object v2, Llx;->f:Llx;

    invoke-virtual {v1, v2}, Llv;->a(Llx;)Z

    move-result v2

    if-nez v2, :cond_a9

    .line 353
    sget v2, Leh;->menu_print:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->removeItem(I)V

    .line 355
    :cond_a9
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 356
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lgl;

    sget-object v4, Lgi;->i:Lgi;

    invoke-interface {v3, v4}, Lgl;->a(Lgi;)Z

    move-result v3

    if-eqz v3, :cond_fb

    sget-object v3, Llx;->i:Llx;

    invoke-virtual {v1, v3}, Llv;->a(Llx;)Z

    move-result v3

    if-eqz v3, :cond_fb

    .line 358
    sget v3, Leh;->menu_pin:I

    invoke-interface {p1, v3}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 359
    sget v4, Len;->menu_offline:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 360
    invoke-virtual {v0}, LkO;->h()Z

    move-result v4

    if-eqz v4, :cond_e1

    .line 361
    sget v4, Len;->menu_unpin:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 367
    :cond_e1
    :goto_e1
    sget-object v2, Llx;->j:Llx;

    invoke-virtual {v1, v2}, Llv;->a(Llx;)Z

    move-result v2

    if-eqz v2, :cond_101

    .line 368
    sget v2, Leh;->menu_move_to_folder:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 369
    invoke-static {v2, v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Landroid/view/MenuItem;Llv;)V

    .line 374
    :goto_f2
    invoke-virtual {v0}, LkO;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    goto/16 :goto_14

    .line 364
    :cond_fb
    sget v2, Leh;->menu_pin:I

    invoke-interface {p1, v2}, Landroid/view/ContextMenu;->removeItem(I)V

    goto :goto_e1

    .line 371
    :cond_101
    sget v1, Leh;->menu_move_to_folder:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->removeItem(I)V

    goto :goto_f2
.end method

.method public a(Lrl;)V
    .registers 2
    .parameter

    .prologue
    .line 839
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->d(Lrl;)V

    .line 840
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->b(Lrl;)V

    .line 841
    return-void
.end method

.method public a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 713
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_9

    .line 714
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/DocListView;->setSelectionMode(Z)V

    .line 716
    :cond_9
    return-void
.end method

.method public b()V
    .registers 3

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    .line 679
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v1, :cond_1c

    if-eqz v0, :cond_1c

    .line 680
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    invoke-interface {v1, v0}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 681
    if-eqz v0, :cond_1c

    .line 682
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/DocListView;->setAccount(LkB;)V

    .line 683
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->s()V

    .line 686
    :cond_1c
    return-void
.end method

.method public b(Lrl;)V
    .registers 3
    .parameter

    .prologue
    .line 492
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Lrl;

    move-result-object v0

    invoke-virtual {p1, v0}, Lrl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 499
    :goto_d
    return-void

    .line 497
    :cond_e
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->c(Lrl;)V

    .line 498
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->s()V

    goto :goto_d
.end method

.method public b(Z)V
    .registers 2
    .parameter

    .prologue
    .line 807
    iput-boolean p1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->d:Z

    .line 808
    return-void
.end method

.method public b(Landroid/view/MenuItem;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 385
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 390
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocListView;->a()Landroid/widget/ListView;

    move-result-object v1

    iget-object v3, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->targetView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    const/4 v3, -0x1

    if-ne v1, v3, :cond_21

    move v0, v2

    .line 456
    :goto_20
    return v0

    .line 395
    :cond_21
    iget-wide v3, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    invoke-interface {v0, v3, v4}, Llf;->a(J)Ljava/lang/String;

    move-result-object v5

    .line 397
    if-nez v5, :cond_34

    .line 398
    const-string v0, "DocListFragment"

    const-string v1, "Failed to retrieve the resourceId matching this contextmenu command."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 399
    goto :goto_20

    .line 402
    :cond_34
    const-string v0, "DocListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "in onContextItemSelected for "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const-string v0, "DocListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "selectedEntryResourceId = "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    iget-object v6, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Liv;

    invoke-interface {v6}, Liv;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    invoke-interface {v0, v1, v5}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v1

    .line 407
    if-nez v1, :cond_96

    .line 408
    const-string v0, "DocListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Entry does not exist anymore for resourceId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 409
    goto :goto_20

    .line 412
    :cond_96
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v6, Leh;->menu_comments:I

    if-ne v0, v6, :cond_ab

    .line 413
    const-string v0, "DocListFragment"

    const-string v2, "Discussions from context menu"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(LkO;)V

    .line 456
    :cond_a8
    :goto_a8
    const/4 v0, 0x1

    goto/16 :goto_20

    .line 415
    :cond_ab
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v6, Leh;->menu_sharing:I

    if-ne v0, v6, :cond_c4

    .line 416
    const-string v0, "DocListFragment"

    const-string v2, "Sharing from context menu"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1}, LkO;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a8

    .line 418
    :cond_c4
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v6, Leh;->menu_rename:I

    if-ne v0, v6, :cond_d9

    .line 419
    const-string v0, "DocListFragment"

    const-string v1, "Rename entry"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LLG;

    invoke-interface {v0, v5}, LLG;->c(Ljava/lang/String;)V

    goto :goto_a8

    .line 421
    :cond_d9
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v6, Leh;->menu_delete:I

    if-ne v0, v6, :cond_ee

    .line 422
    const-string v0, "DocListFragment"

    const-string v1, "Delete entry"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LLG;

    invoke-interface {v0, v5}, LLG;->b(Ljava/lang/String;)V

    goto :goto_a8

    .line 424
    :cond_ee
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v6, Leh;->menu_open_with:I

    if-ne v0, v6, :cond_105

    .line 425
    const-string v0, "DocListFragment"

    const-string v1, "Open with from context menu"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    sget-object v1, LfS;->b:LfS;

    invoke-virtual {v0, v3, v4, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(JLfS;)V

    goto :goto_a8

    .line 427
    :cond_105
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v6, Leh;->menu_send:I

    if-ne v0, v6, :cond_11c

    .line 428
    const-string v0, "DocListFragment"

    const-string v1, "Send from context menu"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    sget-object v1, LfS;->c:LfS;

    invoke-virtual {v0, v3, v4, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(JLfS;)V

    goto :goto_a8

    .line 430
    :cond_11c
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v3, Leh;->menu_send_link:I

    if-ne v0, v3, :cond_13e

    .line 431
    const-string v0, "DocListFragment"

    const-string v2, "Send Link from context menu"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    if-eqz v0, :cond_a8

    .line 434
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->a(LkO;)V

    goto/16 :goto_a8

    .line 436
    :cond_13e
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v3, Leh;->menu_print:I

    if-ne v0, v3, :cond_160

    .line 437
    const-string v0, "DocListFragment"

    const-string v2, "Print from context menu"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    if-eqz v0, :cond_a8

    .line 440
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->b(LkO;)V

    goto/16 :goto_a8

    .line 442
    :cond_160
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v3, Leh;->menu_pin:I

    if-ne v0, v3, :cond_181

    .line 443
    const-string v0, "DocListFragment"

    const-string v2, "Pin from context menu"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 446
    check-cast v0, LkM;

    .line 447
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/DocListView;->a(LkM;)V

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LLG;

    invoke-interface {v0, v5}, LLG;->d(Ljava/lang/String;)V

    .line 449
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    goto/16 :goto_a8

    .line 450
    :cond_181
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Leh;->menu_move_to_folder:I

    if-ne v0, v1, :cond_197

    .line 451
    const-string v0, "DocListFragment"

    const-string v1, "Move folder from context menu"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 452
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LLG;

    invoke-interface {v0, v5}, LLG;->a(Ljava/lang/String;)V

    goto/16 :goto_a8

    :cond_197
    move v0, v2

    .line 454
    goto/16 :goto_20
.end method

.method public b_()V
    .registers 3

    .prologue
    .line 720
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_16

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    const/4 v0, 0x0

    .line 724
    :goto_11
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/DocListView;->setSelectedResourceId(Ljava/lang/String;)V

    .line 726
    :cond_16
    return-void

    .line 722
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_11
.end method

.method public c()V
    .registers 2

    .prologue
    .line 695
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Z

    .line 696
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    .line 697
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 845
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->c(Landroid/os/Bundle;)V

    .line 846
    const-string v0, "sortKind"

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/view/DocListView;->a()Lrl;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 847
    return-void
.end method

.method public f()V
    .registers 1

    .prologue
    .line 754
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    .line 755
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 792
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 794
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->b()Lrl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->b(Lrl;)V

    .line 795
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->q()V

    .line 796
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LWQ;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncReceiver;->a(LWQ;)V

    .line 797
    return-void
.end method

.method public h()V
    .registers 2

    .prologue
    .line 801
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h()V

    .line 802
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LWQ;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncReceiver;->b(LWQ;)V

    .line 803
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->p()V

    .line 804
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 295
    instance-of v0, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    invoke-static {v0}, Lagu;->a(Z)V

    .line 296
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    iget-wide v1, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    invoke-interface {v0, v1, v2}, Llf;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 299
    if-nez v0, :cond_1b

    .line 304
    :goto_1a
    return-void

    .line 303
    :cond_1b
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a(Landroid/view/ContextMenu;Ljava/lang/String;)V

    goto :goto_1a
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Llf;

    invoke-interface {v0, p4, p5}, Llf;->a(J)Ljava/lang/String;

    move-result-object v1

    .line 262
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    .line 263
    sget v2, Leh;->preview_button:I

    if-ne v0, v2, :cond_29

    .line 264
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    if-eqz v0, :cond_28

    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/tablet/TabletDocListActivity;->e(Ljava/lang/String;)V

    .line 270
    :cond_28
    :goto_28
    return-void

    .line 267
    :cond_29
    sget v1, Leh;->doc_entry_row_root:I

    if-ne v0, v1, :cond_28

    .line 268
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    sget-object v1, LfS;->a:LfS;

    invoke-virtual {v0, p4, p5, v1}, Lcom/google/android/apps/docs/view/DocListView;->a(JLfS;)V

    goto :goto_28
.end method

.method public p()V
    .registers 4

    .prologue
    .line 516
    const-string v0, "DocListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in DLF.deActivate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    .line 518
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_26

    .line 519
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()V

    .line 521
    :cond_26
    return-void
.end method

.method public q()V
    .registers 4

    .prologue
    .line 540
    const-string v0, "DocListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in DLF.activate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    .line 542
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->r()V

    .line 543
    return-void
.end method

.method public r()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 559
    const-string v2, "DocListFragment"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "in attemptPopulateOrRefresh "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " isDelayed="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->b:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " isActive="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " queryHasChanged="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Z

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " is doclistView null="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-nez v0, :cond_58

    const/4 v0, 0x1

    :goto_44
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->b:Z

    if-nez v0, :cond_57

    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    if-nez v0, :cond_5a

    .line 584
    :cond_57
    :goto_57
    return-void

    :cond_58
    move v0, v1

    .line 559
    goto :goto_44

    .line 569
    :cond_5a
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Z

    if-eqz v0, :cond_75

    .line 570
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lrl;

    if-eqz v0, :cond_6d

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lrl;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->c(Lrl;)V

    .line 572
    iput-object v4, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lrl;

    .line 576
    :goto_69
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->s()V

    goto :goto_57

    .line 574
    :cond_6d
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->b()Lrl;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->c(Lrl;)V

    goto :goto_69

    .line 580
    :cond_75
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    if-eqz v0, :cond_57

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Lcom/google/android/apps/docs/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/DocListView;->a()LmJ;

    move-result-object v0

    if-eqz v0, :cond_57

    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->a:Z

    if-eqz v0, :cond_57

    .line 581
    const-string v0, "DocListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "triggering doclist cursor refresh "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/DocListFragment;->a()LD;

    move-result-object v0

    invoke-virtual {v0, v1, v4, p0}, LD;->a(ILandroid/os/Bundle;LE;)LL;

    goto :goto_57
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 667
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/DocListFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
