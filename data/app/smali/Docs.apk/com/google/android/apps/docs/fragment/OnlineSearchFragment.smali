.class public Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "OnlineSearchFragment.java"


# instance fields
.field public a:LSB;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZE;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LamZ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LamZ",
            "<",
            "LSF;",
            ">;"
        }
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 122
    invoke-static {}, LamZ;->a()LamZ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LamZ;

    .line 125
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->d(Z)V

    .line 126
    return-void
.end method

.method private a()LSC;
    .registers 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LSB;

    invoke-interface {v0}, LSB;->a()LSC;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)LSC;
    .registers 2
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()LSC;

    move-result-object v0

    return-object v0
.end method

.method private a()LamQ;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LamQ",
            "<",
            "LSF;",
            ">;"
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LamZ;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)LamQ;
    .registers 2
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()LamQ;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .registers 8
    .parameter

    .prologue
    .line 130
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:Llf;

    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 133
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->c()Ljava/lang/String;

    move-result-object v1

    .line 134
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LSB;

    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LZE;

    invoke-interface {v4}, LZE;->a()J

    move-result-wide v4

    invoke-interface {v2, v0, v3, v4, v5}, LSB;->a(LkB;Ljava/lang/String;J)LSF;

    move-result-object v0

    .line 135
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LamZ;

    invoke-virtual {v2, v0}, LamZ;->a(Ljava/lang/Object;)Z

    .line 137
    new-instance v0, Landroid/provider/SearchRecentSuggestions;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/docs/doclist/DocListAccountSuggestionProvider;->a:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-direct {v0, v2, v3, v4}, Landroid/provider/SearchRecentSuggestions;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 140
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method public i()V
    .registers 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/OnlineSearchFragment;->a:LSB;

    invoke-interface {v0}, LSB;->a()V

    .line 162
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->i()V

    .line 163
    return-void
.end method
