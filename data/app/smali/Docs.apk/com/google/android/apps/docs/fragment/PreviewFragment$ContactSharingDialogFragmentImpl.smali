.class public final Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;
.super Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;
.source "PreviewFragment.java"


# instance fields
.field private a:Lcom/google/android/apps/docs/fragment/PreviewFragment;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 135
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->a(Landroid/os/Bundle;)V

    .line 137
    if-eqz p1, :cond_17

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->a()Lo;

    move-result-object v0

    const-string v1, "parentFragment"

    invoke-virtual {v0, p1, v1}, Lo;->a(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/PreviewFragment;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    .line 142
    :cond_17
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    if-nez v0, :cond_25

    .line 143
    const-string v0, "PreviewFragment"

    const-string v1, "No parent fragment provided."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a()V

    .line 146
    :cond_25
    return-void
.end method

.method a(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    .line 131
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 150
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ContactSharingDialogFragment;->c(Landroid/os/Bundle;)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->a()Lo;

    move-result-object v0

    const-string v1, "parentFragment"

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-virtual {v0, p1, v1, v2}, Lo;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 154
    return-void
.end method

.method protected p()V
    .registers 3

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a:Lcom/google/android/apps/docs/fragment/PreviewFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V

    .line 160
    return-void
.end method
