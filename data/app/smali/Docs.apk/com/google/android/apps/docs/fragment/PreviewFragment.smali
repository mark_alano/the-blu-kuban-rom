.class public Lcom/google/android/apps/docs/fragment/PreviewFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "PreviewFragment.java"

# interfaces
.implements LTr;
.implements LiY;
.implements Liw;


# static fields
.field private static final a:I

.field private static b:I


# instance fields
.field public a:LLG;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LMs;

.field public a:LMt;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LTn;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LUq;

.field public a:LUr;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LVH;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LWY;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZj;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaZ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Labh;

.field private a:Landroid/graphics/Bitmap;

.field private a:Landroid/os/Handler;

.field private a:Landroid/support/v4/app/DialogFragment;

.field private a:Landroid/widget/CheckBox;

.field private a:Landroid/widget/ImageView;

.field a:Landroid/widget/LinearLayout;

.field private a:Landroid/widget/ProgressBar;

.field a:Landroid/widget/TextView;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Labh;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/view/ThumbnailView;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LiX;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Liv;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LjY;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/util/concurrent/Executor;

.field a:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Llz;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Z

.field private b:LUq;

.field private b:Landroid/widget/LinearLayout;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field private e:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 112
    sget v0, Leg;->ic_contact_list_picture:I

    sput v0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:I

    .line 967
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:I

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 114
    sget-object v0, LMs;->b:LMs;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LMs;

    .line 260
    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/support/v4/app/DialogFragment;

    .line 265
    invoke-static {v1}, LamF;->a(Ljava/lang/Object;)LamQ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Ljava/util/concurrent/Future;

    return-void
.end method

.method private a(LeH;Ljava/lang/String;)LUB;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 703
    new-instance v0, LeF;

    invoke-direct {v0}, LeF;-><init>()V

    invoke-virtual {v0, p1}, LeF;->a(LeH;)LeF;

    move-result-object v0

    invoke-virtual {v0, p2}, LeF;->b(Ljava/lang/String;)LeF;

    move-result-object v0

    invoke-virtual {v0}, LeF;->a()LeB;

    move-result-object v0

    .line 707
    new-instance v1, LUB;

    const/4 v2, 0x0

    new-instance v3, LUp;

    invoke-direct {v3, v0}, LUp;-><init>(LeB;)V

    invoke-direct {v1, v2, v3}, LUB;-><init>(LTG;LUp;)V

    return-object v1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LUq;)LUq;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/PreviewFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/PreviewFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private a(LUB;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LTL;)Landroid/view/View;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 712
    sget v1, Lej;->preview_fragment_sharing_entry:I

    invoke-virtual {p2, v1, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 715
    invoke-virtual {p1}, LUB;->a()LUp;

    move-result-object v1

    invoke-virtual {v1}, LUp;->a()LeI;

    move-result-object v1

    sget-object v2, LeI;->a:LeI;

    if-eq v1, v2, :cond_14

    const/4 v0, 0x1

    .line 717
    :cond_14
    if-eqz v0, :cond_1e

    .line 718
    new-instance v1, LMo;

    invoke-direct {v1, p0, p1}, LMo;-><init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;LUB;)V

    invoke-virtual {v4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 726
    :cond_1e
    invoke-virtual {v4, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 728
    sget v0, Leh;->share_name:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 729
    sget v1, Leh;->share_description:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 730
    sget v2, Leh;->share_role:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 731
    sget v3, Leh;->share_badge:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/QuickContactBadge;

    .line 733
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LUB;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 734
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b(LUB;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 735
    invoke-virtual {p1}, LUB;->a()LUp;

    move-result-object v0

    invoke-virtual {v0}, LUp;->a()LeG;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, LeG;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 736
    invoke-virtual {p1}, LUB;->a()LTG;

    move-result-object v0

    invoke-direct {p0, v3, p4, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Landroid/widget/QuickContactBadge;LTL;LTG;)V

    .line 738
    return-object v4
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 742
    sget v0, Lej;->preview_fragment_sharing_add_entry:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 743
    new-instance v1, LMp;

    invoke-direct {v1, p0}, LMp;-><init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 750
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 752
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/PreviewFragment;)Lcom/google/android/apps/docs/view/ThumbnailView;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LdL;

    return-object v0
.end method

.method private a(LUB;)Ljava/lang/String;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 789
    invoke-virtual {p1}, LUB;->a()LeB;

    move-result-object v0

    .line 790
    invoke-virtual {p1}, LUB;->a()LTG;

    move-result-object v2

    .line 794
    if-eqz v2, :cond_2d

    .line 795
    invoke-interface {v2}, LTG;->b()Ljava/lang/String;

    move-result-object v1

    .line 796
    invoke-interface {v2}, LTG;->a()Ljava/lang/String;

    move-result-object v0

    .line 798
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_72

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_72

    .line 800
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 801
    if-lez v1, :cond_2c

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 823
    :cond_2c
    :goto_2c
    return-object v0

    .line 804
    :cond_2d
    invoke-virtual {v0}, LeB;->a()LeK;

    move-result-object v1

    .line 805
    sget-object v2, LeK;->d:LeK;

    if-ne v1, v2, :cond_49

    .line 806
    invoke-virtual {v0}, LeB;->a()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 807
    sget v0, Len;->sharing_option_anyone_with_link:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2c

    .line 809
    :cond_42
    sget v0, Len;->sharing_option_anyone:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2c

    .line 811
    :cond_49
    sget-object v2, LeK;->c:LeK;

    if-ne v1, v2, :cond_6d

    .line 812
    invoke-virtual {v0}, LeB;->b()Ljava/lang/String;

    move-result-object v1

    .line 814
    invoke-virtual {v0}, LeB;->a()Z

    move-result v0

    if-eqz v0, :cond_62

    .line 815
    sget v0, Len;->sharing_option_anyone_from_with_link:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2c

    .line 817
    :cond_62
    sget v0, Len;->sharing_option_anyone_from:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2c

    .line 820
    :cond_6d
    invoke-virtual {v0}, LeB;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_2c

    :cond_72
    move-object v0, v1

    goto :goto_2c
.end method

.method private a(LkO;)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    .line 430
    invoke-virtual {p1}, LkO;->e()Ljava/lang/String;

    move-result-object v0

    .line 431
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 432
    invoke-virtual {p1}, LkO;->b()Ljava/util/Date;

    move-result-object v2

    .line 433
    sget v3, Len;->preview_general_info_modified:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v0, v4, v1

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 436
    return-object v0
.end method

.method private a()LkO;
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 893
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1b

    :cond_19
    move-object v0, v1

    .line 912
    :cond_1a
    :goto_1a
    return-object v0

    .line 898
    :cond_1b
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Llf;

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Liv;

    invoke-interface {v2}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v2

    .line 899
    if-nez v2, :cond_32

    .line 900
    const-string v0, "PreviewFragment"

    const-string v2, "Failed to load the account object"

    invoke-static {v0, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 901
    goto :goto_1a

    .line 905
    :cond_32
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    .line 906
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Llf;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v2, v0}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    .line 907
    if-nez v0, :cond_1a

    .line 908
    const-string v0, "PreviewFragment"

    const-string v2, "Failed to load the Entry"

    invoke-static {v0, v2}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 909
    goto :goto_1a
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LkO;
    .registers 2
    .parameter

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()LkO;

    move-result-object v0

    return-object v0
.end method

.method private a(LMs;)V
    .registers 2
    .parameter

    .prologue
    .line 590
    iput-object p1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LMs;

    .line 591
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->s()V

    .line 592
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 310
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 311
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 312
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 313
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    return-void
.end method

.method private a(Landroid/widget/QuickContactBadge;LTL;LTG;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 874
    if-eqz p3, :cond_23

    invoke-interface {p3}, LTG;->a()Ljava/lang/String;

    move-result-object v0

    .line 876
    :goto_6
    sget v1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:I

    invoke-virtual {p1, v1}, Landroid/widget/QuickContactBadge;->setImageResource(I)V

    .line 877
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 878
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/widget/QuickContactBadge;->assignContactFromEmail(Ljava/lang/String;Z)V

    .line 880
    :cond_15
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/widget/QuickContactBadge;->setMode(I)V

    .line 881
    if-eqz p3, :cond_22

    .line 882
    invoke-interface {p3}, LTG;->a()J

    move-result-wide v0

    invoke-virtual {p2, p1, v0, v1}, LTL;->a(Landroid/widget/ImageView;J)V

    .line 884
    :cond_22
    return-void

    .line 874
    :cond_23
    const/4 v0, 0x0

    goto :goto_6
.end method

.method private a(Landroid/widget/TextView;ILjava/util/Date;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 452
    if-eqz p3, :cond_25

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 453
    if-eqz p3, :cond_27

    .line 454
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 457
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 458
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 462
    :goto_24
    return-void

    .line 452
    :cond_25
    const/4 v0, 0x4

    goto :goto_4

    .line 460
    :cond_27
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_24
.end method

.method static synthetic a(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->t()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LMs;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LMs;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/fragment/PreviewFragment;LkO;Z)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b(LkO;Z)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 676
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 677
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    .line 678
    check-cast v0, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;->setNextFocusLeftEnabled(Z)V

    goto :goto_5

    .line 680
    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4f

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/CheckBox;

    move-object v1, v0

    .line 681
    :goto_25
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_53

    .line 682
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    .line 683
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 684
    invoke-static {p1}, LajB;->b(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/fragment/CustomFocusRelativeLayout;->setNextFocusDownEnabled(Z)V

    .line 688
    :goto_4e
    return-void

    .line 680
    :cond_4f
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    move-object v1, v0

    goto :goto_25

    .line 686
    :cond_53
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    goto :goto_4e
.end method

.method private static a(Ljava/util/List;Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 691
    invoke-virtual {p1}, Landroid/view/View;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 692
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 694
    :cond_9
    return-void
.end method

.method private a(LkO;)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 443
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lgl;

    sget-object v2, Lgi;->i:Lgi;

    invoke-interface {v1, v2}, Lgl;->a(Lgi;)Z

    move-result v1

    if-eqz v1, :cond_28

    invoke-virtual {p1}, LkO;->i()Z

    move-result v1

    if-eqz v1, :cond_28

    const/4 v1, 0x1

    .line 444
    :goto_12
    if-eqz v1, :cond_2a

    .line 445
    :goto_14
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 446
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 447
    invoke-virtual {p1}, LkO;->g()Z

    move-result v0

    .line 448
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 449
    return-void

    :cond_28
    move v1, v0

    .line 443
    goto :goto_12

    .line 444
    :cond_2a
    const/16 v0, 0x8

    goto :goto_14
.end method

.method private a(LkO;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->clearAnimation()V

    .line 472
    if-eqz p2, :cond_b

    .line 473
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/graphics/Bitmap;

    .line 475
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/docs/view/ThumbnailView;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/ThumbnailView;->setEntry(LkO;)V

    .line 478
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LkO;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 479
    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b(LkO;Z)V

    .line 485
    :goto_1e
    return-void

    .line 480
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2d

    if-nez p2, :cond_2d

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/view/ThumbnailView;->setImage(Landroid/graphics/Bitmap;Z)V

    goto :goto_1e

    .line 483
    :cond_2d
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b(LkO;)V

    goto :goto_1e
.end method

.method private a(LkO;)Z
    .registers 3
    .parameter

    .prologue
    .line 536
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_14

    if-eqz p1, :cond_14

    invoke-virtual {p1}, LkO;->o()Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LdL;

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 970
    sget v0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:I

    .line 971
    sget v1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:I

    .line 972
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddCollaboratorTextDialogFragment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(LUB;)Ljava/lang/String;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 834
    invoke-virtual {p1}, LUB;->a()LeB;

    move-result-object v0

    .line 837
    sget-object v1, LMr;->a:[I

    invoke-virtual {v0}, LeB;->a()LeK;

    move-result-object v2

    invoke-virtual {v2}, LeK;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_6a

    .line 864
    sget v0, Len;->sharing_option_unknown:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 867
    :goto_1b
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LUB;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 868
    const-string v0, ""

    .line 870
    :cond_27
    return-object v0

    .line 839
    :pswitch_28
    invoke-virtual {v0}, LeB;->a()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 840
    sget v0, Len;->sharing_option_anyone_with_link_description:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1b

    .line 842
    :cond_35
    sget v0, Len;->sharing_option_anyone_description:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1b

    .line 846
    :pswitch_3c
    invoke-virtual {v0}, LeB;->b()Ljava/lang/String;

    move-result-object v1

    .line 848
    invoke-virtual {v0}, LeB;->a()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 849
    sget v0, Len;->sharing_option_anyone_from_with_link_description:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1b

    .line 851
    :cond_51
    sget v0, Len;->sharing_option_anyone_from_description:I

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v1, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1b

    .line 856
    :pswitch_5c
    invoke-virtual {p1}, LUB;->a()LTG;

    move-result-object v0

    .line 857
    if-eqz v0, :cond_67

    .line 858
    invoke-interface {v0}, LTG;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1b

    .line 860
    :cond_67
    const-string v0, ""

    goto :goto_1b

    .line 837
    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_28
        :pswitch_3c
        :pswitch_5c
        :pswitch_5c
    .end packed-switch
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->r()V

    return-void
.end method

.method private b(LkO;)V
    .registers 7
    .parameter

    .prologue
    .line 497
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->q()V

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->a()V

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labh;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Labh;

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Labh;

    invoke-virtual {p1}, LkO;->a()LkY;

    move-result-object v1

    new-instance v2, LZw;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/view/ThumbnailView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v4}, Lcom/google/android/apps/docs/view/ThumbnailView;->getMeasuredHeight()I

    move-result v4

    invoke-direct {v2, v3, v4}, LZw;-><init>(II)V

    new-instance v3, LMk;

    invoke-direct {v3, p0, p1}, LMk;-><init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;LkO;)V

    invoke-interface {v0, v1, v2, v3}, Labh;->a(LkY;LZw;Labi;)V

    .line 526
    return-void
.end method

.method private b(LkO;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 488
    if-nez p1, :cond_3

    .line 494
    :goto_2
    return-void

    .line 491
    :cond_3
    invoke-virtual {p1}, LkO;->a()LkP;

    move-result-object v0

    invoke-virtual {v0}, LkP;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LkO;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LkO;->d()Z

    move-result v2

    invoke-static {v0, v1, v2}, LkO;->b(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    .line 493
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/docs/view/ThumbnailView;->setIcon(IZ)V

    goto :goto_2
.end method

.method private b(Z)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 394
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()LkO;

    move-result-object v1

    .line 395
    if-nez v1, :cond_a

    .line 396
    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    .line 427
    :goto_9
    return-void

    .line 400
    :cond_a
    if-eqz p1, :cond_17

    .line 401
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LeQ;

    const-string v3, "tabletDoclist"

    const-string v4, "previewEntryEvent"

    invoke-virtual {v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    .line 406
    :cond_17
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->c:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LkO;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    invoke-virtual {v1}, LkO;->b()Ljava/lang/Long;

    move-result-object v2

    .line 409
    if-nez v2, :cond_69

    .line 411
    :goto_26
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->d:Landroid/widget/TextView;

    sget v3, Len;->preview_general_info_modified_by_me:I

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Landroid/widget/TextView;ILjava/util/Date;)V

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->e:Landroid/widget/TextView;

    sget v2, Len;->preview_general_info_opened_by_me:I

    invoke-virtual {v1}, LkO;->c()Ljava/util/Date;

    move-result-object v3

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Landroid/widget/TextView;ILjava/util/Date;)V

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, LkO;->a()LkP;

    move-result-object v2

    invoke-virtual {v2}, LkP;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, LkO;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LkO;->d()Z

    move-result v4

    invoke-static {v2, v3, v4}, LkO;->a(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, LkO;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LkO;)V

    .line 423
    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LkO;Z)V

    .line 424
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->r()V

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/ThumbnailView;->requestFocus()Z

    goto :goto_9

    .line 409
    :cond_69
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    goto :goto_26
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic g(Lcom/google/android/apps/docs/fragment/PreviewFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LdL;

    return-object v0
.end method

.method private h()Z
    .registers 2

    .prologue
    .line 887
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()LkO;

    move-result-object v0

    .line 889
    if-eqz v0, :cond_e

    invoke-virtual {v0}, LkO;->k()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private i()Z
    .registers 2

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private q()V
    .registers 2

    .prologue
    .line 529
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Labh;

    if-eqz v0, :cond_c

    .line 530
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Labh;

    invoke-interface {v0}, Labh;->a()V

    .line 531
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Labh;

    .line 533
    :cond_c
    return-void
.end method

.method private r()V
    .registers 5

    .prologue
    .line 540
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->u()V

    .line 542
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()LkO;

    move-result-object v1

    .line 543
    if-nez v1, :cond_a

    .line 587
    :goto_9
    return-void

    .line 547
    :cond_a
    sget-object v0, LMs;->c:LMs;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LMs;)V

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v2

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 552
    invoke-virtual {v1}, LkO;->c()Ljava/lang/String;

    move-result-object v1

    .line 554
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LTn;

    invoke-interface {v3, v2, v0, v1}, LTn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LamQ;

    move-result-object v1

    .line 557
    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Ljava/util/concurrent/Future;

    .line 558
    new-instance v2, LMn;

    invoke-direct {v2, p0, v0}, LMn;-><init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;Ljava/lang/String;)V

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    goto :goto_9
.end method

.method private s()V
    .registers 11

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 595
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:LUq;

    invoke-static {v0, v3}, Lagp;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_bc

    move v0, v1

    .line 597
    :goto_f
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    iput-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:LUq;

    .line 599
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->h()Z

    move-result v3

    if-nez v3, :cond_bf

    move v3, v1

    .line 600
    :goto_1a
    iget-object v5, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    if-nez v5, :cond_c2

    const/4 v5, 0x0

    .line 602
    :goto_1f
    if-nez v5, :cond_127

    .line 603
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    move-object v5, v3

    move v6, v1

    .line 608
    :goto_27
    if-eqz v0, :cond_2e

    .line 609
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 612
    :cond_2e
    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LMs;

    sget-object v7, LMs;->b:LMs;

    if-eq v3, v7, :cond_3e

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LMs;

    sget-object v7, LMs;->c:LMs;

    if-ne v3, v7, :cond_ca

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    if-eqz v3, :cond_ca

    :cond_3e
    move v3, v1

    .line 615
    :goto_3f
    if-eqz v3, :cond_ef

    if-eqz v0, :cond_ef

    .line 616
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 618
    new-instance v7, LTL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v8, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:I

    invoke-direct {v7, v1, v8}, LTL;-><init>(Landroid/content/Context;I)V

    .line 621
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 623
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    if-eqz v1, :cond_9d

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    invoke-interface {v1}, LUq;->a()LeH;

    move-result-object v1

    if-eqz v1, :cond_9d

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    invoke-interface {v1}, LUq;->a()LeH;

    move-result-object v1

    sget-object v9, LeH;->a:LeH;

    if-eq v1, v9, :cond_9d

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    invoke-interface {v1}, LUq;->a()LeH;

    move-result-object v1

    sget-object v9, LeH;->n:LeH;

    if-eq v1, v9, :cond_9d

    .line 627
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    invoke-interface {v1}, LUq;->a()LeH;

    move-result-object v1

    iget-object v9, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    invoke-interface {v9}, LUq;->a()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v1, v9}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LeH;Ljava/lang/String;)LUB;

    move-result-object v1

    .line 630
    iget-object v9, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0, v9, v7}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LUB;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LTL;)Landroid/view/View;

    move-result-object v1

    .line 632
    iget-object v9, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 633
    invoke-static {v8, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Ljava/util/List;Landroid/view/View;)V

    .line 636
    :cond_9d
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_a1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_cd

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LUB;

    .line 637
    iget-object v9, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0, v9, v7}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LUB;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;LTL;)Landroid/view/View;

    move-result-object v1

    .line 639
    iget-object v9, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 640
    invoke-static {v8, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Ljava/util/List;Landroid/view/View;)V

    goto :goto_a1

    :cond_bc
    move v0, v2

    .line 595
    goto/16 :goto_f

    :cond_bf
    move v3, v2

    .line 599
    goto/16 :goto_1a

    .line 600
    :cond_c2
    iget-object v5, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    invoke-interface {v5}, LUq;->a()Ljava/util/List;

    move-result-object v5

    goto/16 :goto_1f

    :cond_ca
    move v3, v2

    .line 612
    goto/16 :goto_3f

    .line 643
    :cond_cd
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    if-eqz v1, :cond_e7

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    invoke-interface {v1}, LUq;->b()Z

    move-result v1

    if-eqz v1, :cond_e7

    .line 645
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 646
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 647
    invoke-static {v8, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Ljava/util/List;Landroid/view/View;)V

    .line 650
    :cond_e7
    invoke-direct {p0, v8}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Ljava/util/List;)V

    .line 651
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LjY;

    invoke-interface {v0}, LjY;->c()V

    .line 654
    :cond_ef
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LMs;

    sget-object v5, LMs;->c:LMs;

    if-ne v0, v5, :cond_11a

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    if-nez v0, :cond_11a

    move v0, v2

    :goto_fc
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 657
    const/4 v0, -0x1

    .line 658
    if-eqz v3, :cond_11c

    if-eqz v6, :cond_11c

    .line 659
    sget v0, Len;->sharing_list_may_not_be_completed:I

    .line 664
    :cond_106
    :goto_106
    if-ltz v0, :cond_10d

    .line 665
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 667
    :cond_10d
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:Landroid/widget/TextView;

    if-ltz v0, :cond_125

    :goto_111
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 672
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->requestLayout()V

    .line 673
    return-void

    :cond_11a
    move v0, v4

    .line 654
    goto :goto_fc

    .line 660
    :cond_11c
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LMs;

    sget-object v3, LMs;->a:LMs;

    if-ne v1, v3, :cond_106

    .line 661
    sget v0, Len;->sharing_list_offline:I

    goto :goto_106

    :cond_125
    move v2, v4

    .line 667
    goto :goto_111

    :cond_127
    move v6, v3

    goto/16 :goto_27
.end method

.method private t()V
    .registers 4

    .prologue
    .line 921
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    .line 922
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    .line 923
    sget-object v1, LMs;->c:LMs;

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(LMs;)V

    .line 924
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->u()V

    .line 925
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUr;

    invoke-interface {v1, v0}, LUr;->a(LUq;)LamQ;

    move-result-object v0

    .line 927
    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Ljava/util/concurrent/Future;

    .line 928
    new-instance v1, LMq;

    invoke-direct {v1, p0}, LMq;-><init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V

    .line 947
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    .line 948
    return-void
.end method

.method private u()V
    .registers 3

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_16

    .line 1040
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1042
    :cond_16
    return-void
.end method


# virtual methods
.method public a()LUq;
    .registers 2

    .prologue
    .line 959
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 271
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:LUq;

    .line 272
    sget v0, Lej;->preview_fragment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 274
    sget v0, Leh;->doc_icon:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/ImageView;

    .line 275
    sget v0, Leh;->thumbnail:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/ThumbnailView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Lcom/google/android/apps/docs/view/ThumbnailView;

    .line 276
    sget v0, Leh;->share_list_progress_bar:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/ProgressBar;

    .line 277
    sget v0, Leh;->share_list_warning:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:Landroid/widget/TextView;

    .line 278
    sget v0, Leh;->last_modified:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->c:Landroid/widget/TextView;

    .line 279
    sget v0, Leh;->last_modified_by_me:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->d:Landroid/widget/TextView;

    .line 280
    sget v0, Leh;->last_opened_by_me:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->e:Landroid/widget/TextView;

    .line 281
    sget v0, Leh;->preview_close_button:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->d:Landroid/view/View;

    .line 282
    sget v0, Leh;->pin:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b:Landroid/widget/LinearLayout;

    .line 283
    sget v0, Leh;->pin_checkbox:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/CheckBox;

    .line 284
    sget v0, Leh;->title:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/TextView;

    .line 285
    sget v0, Leh;->title_container:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->e:Landroid/view/View;

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->e:Landroid/view/View;

    new-instance v2, LMh;

    invoke-direct {v2, p0}, LMh;-><init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 300
    sget v0, Leh;->share_list:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/LinearLayout;

    .line 302
    sget v0, Leh;->offline_title:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Landroid/view/View;I)V

    .line 303
    sget v0, Leh;->sharing_title:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Landroid/view/View;I)V

    .line 304
    sget v0, Leh;->general_info_title:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a(Landroid/view/View;I)V

    .line 306
    return-object v1
.end method

.method public a(IZI)Landroid/view/animation/Animation;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1000
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 1003
    iget-boolean v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Z

    if-eqz v0, :cond_29

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    move v1, v0

    .line 1006
    :goto_1a
    const/16 v0, 0x1001

    if-ne p1, v0, :cond_35

    .line 1007
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v1, v2, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1014
    :goto_23
    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1015
    :goto_28
    return-object v0

    .line 1003
    :cond_29
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lef;->preview_panel_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    move v1, v0

    goto :goto_1a

    .line 1008
    :cond_35
    const/16 v0, 0x2002

    if-ne p1, v0, :cond_3f

    .line 1009
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v2, v1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_23

    .line 1011
    :cond_3f
    const/4 v0, 0x0

    goto :goto_28
.end method

.method public a()V
    .registers 1

    .prologue
    .line 964
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->t()V

    .line 965
    return-void
.end method

.method public a(LUB;)V
    .registers 8
    .parameter

    .prologue
    .line 761
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 762
    invoke-virtual {p1}, LUB;->a()LTG;

    move-result-object v2

    .line 763
    if-nez v2, :cond_c

    .line 779
    :cond_b
    :goto_b
    return-void

    .line 766
    :cond_c
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->i()Z

    move-result v0

    if-nez v0, :cond_b

    .line 767
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Liv;

    invoke-interface {v0}, Liv;->a()Ljava/lang/String;

    move-result-object v1

    .line 768
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LiX;

    invoke-interface {v0}, LiX;->a()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, LajB;->a(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 769
    invoke-static {v1, v0}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v1

    .line 770
    invoke-interface {v2}, LTG;->a()Ljava/lang/String;

    move-result-object v5

    .line 771
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    .line 772
    new-instance v0, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;

    invoke-direct {v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;-><init>()V

    .line 773
    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V

    .line 774
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LZj;

    iget-object v3, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Llf;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a(LkY;LZj;Llf;Landroid/content/Context;Ljava/lang/String;)V

    .line 776
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->a()Lo;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/fragment/PreviewFragment$ContactSharingDialogFragmentImpl;->a(Lo;Ljava/lang/String;)V

    .line 777
    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/support/v4/app/DialogFragment;

    goto :goto_b
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 1020
    const-string v0, "PreviewFragment"

    const-string v1, "in onCreate"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 1022
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/os/Handler;

    .line 1023
    new-instance v0, Laat;

    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/os/Handler;

    invoke-direct {v0, v1}, Laat;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Ljava/util/concurrent/Executor;

    .line 1024
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->d(Z)V

    .line 1025
    return-void
.end method

.method public a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 995
    iput-boolean p1, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Z

    .line 996
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 359
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b(Z)V

    .line 360
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 318
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->b(Landroid/os/Bundle;)V

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->d:Landroid/view/View;

    new-instance v1, LMi;

    invoke-direct {v1, p0}, LMi;-><init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LiX;

    invoke-interface {v0, p0}, LiX;->a(LiY;)V

    .line 329
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Liv;

    invoke-interface {v0, p0}, Liv;->a(Liw;)V

    .line 331
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b(Z)V

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/CheckBox;

    new-instance v1, LMj;

    invoke-direct {v1, p0}, LMj;-><init>(Lcom/google/android/apps/docs/fragment/PreviewFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/widget/CheckBox;

    sget v1, Leg;->state_selector_background:I

    invoke-static {v0, v1}, Labr;->a(Landroid/view/View;I)V

    .line 355
    return-void
.end method

.method public b_()V
    .registers 2

    .prologue
    .line 364
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b(Z)V

    .line 365
    return-void
.end method

.method public f()V
    .registers 2

    .prologue
    .line 369
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b(Z)V

    .line 370
    return-void
.end method

.method public g()V
    .registers 3

    .prologue
    .line 977
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->g()V

    .line 978
    const-string v0, "PreviewFragment"

    const-string v1, "in onResume"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 981
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LMs;

    sget-object v1, LMs;->a:LMs;

    if-ne v0, v1, :cond_13

    .line 982
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->r()V

    .line 984
    :cond_13
    return-void
.end method

.method public h()V
    .registers 3

    .prologue
    .line 988
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 989
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->u()V

    .line 990
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/support/v4/app/DialogFragment;

    .line 991
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h()V

    .line 992
    return-void
.end method

.method public j_()V
    .registers 3

    .prologue
    .line 1029
    const-string v0, "PreviewFragment"

    const-string v1, "in onDestroyView"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1030
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->q()V

    .line 1031
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->j_()V

    .line 1032
    return-void
.end method

.method public p()V
    .registers 4

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:LUq;

    invoke-interface {v0}, LUq;->a()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_d

    .line 387
    :cond_c
    :goto_c
    return-void

    .line 381
    :cond_d
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->i()Z

    move-result v0

    if-nez v0, :cond_c

    .line 382
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->a()Lo;

    move-result-object v0

    .line 383
    invoke-static {}, Lcom/google/android/apps/docs/fragment/PreviewFragment;->b()Ljava/lang/String;

    move-result-object v1

    .line 384
    new-instance v2, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-direct {v2}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/support/v4/app/DialogFragment;

    .line 385
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/PreviewFragment;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(Lo;Ljava/lang/String;)V

    goto :goto_c
.end method
