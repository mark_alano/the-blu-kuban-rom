.class public abstract Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;
.super Lcom/google/android/apps/docs/view/RoboFragment;
.source "ViewSwitcherFragment.java"


# instance fields
.field private a:I

.field private a:[I

.field protected a:[Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/RoboFragment;-><init>()V

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    return-void
.end method

.method protected static a(Landroid/os/Bundle;[II)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    const-string v0, "listOfIdsForEachPanel"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 32
    const-string v0, "visiblePanel"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 33
    return-void
.end method

.method private b()Landroid/view/View;
    .registers 6

    .prologue
    const/4 v3, 0x0

    .line 104
    const/4 v1, 0x0

    move v2, v3

    .line 105
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    array-length v0, v0

    if-ge v2, v0, :cond_1f

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    aget-object v0, v0, v2

    .line 107
    iget v4, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    if-ne v2, v4, :cond_18

    .line 109
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 105
    :goto_13
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_3

    .line 111
    :cond_18
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    goto :goto_13

    .line 115
    :cond_1f
    return-object v1
.end method

.method private e(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 64
    const-string v0, "visiblePanel"

    invoke-static {p1, v0}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->b(I)V

    .line 65
    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[I

    array-length v0, v0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    .line 96
    const/4 v0, 0x0

    :goto_8
    iget-object v1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    array-length v1, v1

    if-ge v0, v1, :cond_23

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 98
    invoke-static {v1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v2, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    aput-object v1, v2, v0

    .line 96
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 101
    :cond_23
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->a(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 55
    const-string v1, "listOfIdsForEachPanel"

    invoke-static {v0, v1}, LZz;->a(Landroid/os/Bundle;Ljava/lang/String;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[I

    .line 58
    iget v1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_19

    .line 59
    if-eqz p1, :cond_1a

    :goto_16
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->e(Landroid/os/Bundle;)V

    .line 61
    :cond_19
    return-void

    :cond_1a
    move-object p1, v0

    .line 59
    goto :goto_16
.end method

.method protected b(I)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    const-string v0, "ViewSwitcherFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "in setVisiblePanel "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    if-ltz p1, :cond_38

    move v0, v1

    :goto_1d
    invoke-static {v0}, Lagu;->a(Z)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[I

    array-length v0, v0

    if-ge p1, v0, :cond_3a

    :goto_25
    invoke-static {v1}, Lagu;->a(Z)V

    .line 85
    iput p1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:[Landroid/view/View;

    if-eqz v0, :cond_37

    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->b()Landroid/view/View;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_37

    .line 89
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 92
    :cond_37
    return-void

    :cond_38
    move v0, v2

    .line 83
    goto :goto_1d

    :cond_3a
    move v1, v2

    .line 84
    goto :goto_25
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/view/RoboFragment;->c(Landroid/os/Bundle;)V

    .line 77
    const-string v0, "visiblePanel"

    iget v1, p0, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 78
    return-void
.end method

.method public h_()V
    .registers 1

    .prologue
    .line 69
    invoke-super {p0}, Lcom/google/android/apps/docs/view/RoboFragment;->h_()V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->a()V

    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/docs/fragment/ViewSwitcherFragment;->b()Landroid/view/View;

    .line 72
    return-void
.end method
