.class public Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "OpenUrlActivityDelegate.java"

# interfaces
.implements Low;


# instance fields
.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LPm;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LVH;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LWY;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/net/Uri;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field public a:LkK;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LnE;

.field public a:LnF;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:[Landroid/accounts/Account;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 83
    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Ljava/lang/String;

    .line 85
    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    .line 89
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->b:Landroid/os/Handler;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static a([Landroid/accounts/Account;[BLjava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 147
    :try_start_0
    const-string v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 149
    array-length v2, p0

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_47

    aget-object v3, p0, v0

    .line 150
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 151
    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    .line 152
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 153
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v4

    .line 154
    invoke-static {p1, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-eqz v4, :cond_3c

    .line 155
    iget-object v0, v3, Landroid/accounts/Account;->name:Ljava/lang/String;
    :try_end_3b
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_3b} :catch_3f

    .line 161
    :goto_3b
    return-object v0

    .line 149
    :cond_3c
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 158
    :catch_3f
    move-exception v0

    .line 159
    const-string v0, "OpenUrlActivity"

    const-string v1, "MD5 digest algorithm not found"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    :cond_47
    const/4 v0, 0x0

    goto :goto_3b
.end method

.method private a(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    .line 202
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->startActivityForResult(Landroid/content/Intent;I)V

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Len;->opening_in_app_drivev2:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 206
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Landroid/content/Intent;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Landroid/content/Intent;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LkK;

    invoke-virtual {v0, p1}, LkK;->b(Ljava/lang/String;)LkB;

    move-result-object v4

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LkK;

    invoke-virtual {v0, v4, p2}, LkK;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 213
    invoke-direct {p0, p3}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Landroid/content/Intent;)V

    .line 260
    :goto_11
    return-void

    .line 216
    :cond_12
    invoke-static {p2}, LVI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 217
    new-instance v0, LPh;

    move-object v1, p0

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LPh;-><init>(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;Ljava/lang/String;Ljava/lang/String;LkB;Landroid/content/Intent;)V

    invoke-virtual {v0}, LPh;->start()V

    goto :goto_11
.end method

.method private a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LZM;

    invoke-interface {v0, p1, p2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 264
    const-string v0, "OpenUrlActivity"

    invoke-static {v0, p1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->b:Landroid/os/Handler;

    new-instance v1, LPk;

    invoke-direct {v1, p0}, LPk;-><init>(Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 271
    return-void
.end method

.method private f()V
    .registers 7

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LPm;

    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    invoke-interface {v0, p0, v1}, LPm;->a(Landroid/content/Context;Landroid/net/Uri;)LPp;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, LPp;->a()Landroid/content/Intent;

    move-result-object v1

    .line 189
    const-string v2, "uri"

    iget-object v3, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    const-string v2, "accountName"

    iget-object v3, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 191
    const-string v2, "forceApplicationSelectionDialog"

    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "forceApplicationSelectionDialog"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 193
    invoke-virtual {v0}, LPp;->a()Ljava/lang/String;

    move-result-object v0

    .line 194
    if-eqz v0, :cond_3a

    .line 195
    iget-object v2, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 199
    :goto_39
    return-void

    .line 197
    :cond_3a
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Landroid/content/Intent;)V

    goto :goto_39
.end method

.method private g()V
    .registers 2

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a()Lo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(Lo;)V

    .line 275
    return-void
.end method


# virtual methods
.method public a(Landroid/accounts/Account;)V
    .registers 3
    .parameter

    .prologue
    .line 289
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Ljava/lang/String;

    .line 290
    invoke-direct {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->f()V

    .line 291
    return-void
.end method

.method public l_()V
    .registers 1

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->finish()V

    .line 296
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 280
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 281
    if-eqz p1, :cond_11

    .line 282
    const-string v0, "OpenUrlActivity"

    const-string v1, "Invalid request code in activity result."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->finish()V

    .line 285
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 93
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LnF;

    invoke-interface {v0, p0, v4}, LnF;->a(Landroid/app/Activity;I)LnE;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LnE;

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LeQ;

    invoke-virtual {v1}, LeQ;->a()V

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LeQ;

    const-string v2, "/urlOpen"

    invoke-virtual {v1, v2, v0}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 100
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_45

    .line 101
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 135
    :goto_44
    return-void

    .line 105
    :cond_45
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Landroid/net/Uri;

    if-nez v1, :cond_55

    .line 107
    const-string v0, "URL is not specified."

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_44

    .line 111
    :cond_55
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Ljava/lang/String;

    if-nez v1, :cond_8d

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LME;

    invoke-interface {v1}, LME;->a()[Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    .line 114
    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    if-eqz v1, :cond_72

    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    array-length v1, v1

    if-ge v1, v5, :cond_80

    .line 116
    :cond_72
    invoke-virtual {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Len;->google_account_missing_all_apps:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_44

    .line 119
    :cond_80
    iget-object v1, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    array-length v1, v1

    if-ne v1, v5, :cond_95

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    aget-object v0, v0, v4

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Ljava/lang/String;

    .line 130
    :cond_8d
    :goto_8d
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Ljava/lang/String;

    if-nez v0, :cond_ae

    .line 131
    invoke-direct {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->g()V

    goto :goto_44

    .line 122
    :cond_95
    const-string v1, "digest"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 123
    const-string v2, "salt"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    if-eqz v1, :cond_8d

    if-eqz v0, :cond_8d

    .line 125
    iget-object v2, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:[Landroid/accounts/Account;

    invoke-static {v2, v1, v0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a([Landroid/accounts/Account;[BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:Ljava/lang/String;

    goto :goto_8d

    .line 133
    :cond_ae
    invoke-direct {p0}, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->f()V

    goto :goto_44
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LnE;

    invoke-interface {v0, p1}, LnE;->a(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 168
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onDestroy()V

    .line 169
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/docs/openurl/OpenUrlActivityDelegate;->a:LnE;

    invoke-interface {v0, p1, p2}, LnE;->a(ILandroid/app/Dialog;)V

    .line 181
    return-void
.end method
