.class public Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;
.super Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;
.source "GridViewSlidePickerFragment.java"


# instance fields
.field private a:I

.field private a:Landroid/widget/FrameLayout;

.field a:Landroid/widget/GridView;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;
    .registers 1

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;-><init>()V

    return-object v0
.end method

.method private p()V
    .registers 3

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    new-instance v1, LQE;

    invoke-direct {v1, p0}, LQE;-><init>(Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 145
    return-void
.end method

.method private q()V
    .registers 3

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lei;->punch_grid_view_slide_picker_columns:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:I

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    iget v1, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 151
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-interface {v0, v3}, LdL;->a(Landroid/content/Context;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    if-nez v0, :cond_3f

    .line 53
    sget v0, Lej;->punch_grid_view_slide_picker:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    sget v3, Leh;->grid_view:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 58
    instance-of v3, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_37

    .line 59
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    sget-object v3, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 62
    :cond_37
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    if-eqz v0, :cond_75

    move v0, v1

    :goto_3c
    invoke-static {v0}, Lagu;->b(Z)V

    .line 65
    :cond_3f
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->p()V

    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->q()V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    if-nez v0, :cond_77

    :goto_49
    invoke-static {v1}, Lagu;->b(Z)V

    .line 70
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    new-instance v1, LQD;

    invoke-direct {v1, p0}, LQD;-><init>(Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    return-object v0

    :cond_75
    move v0, v2

    .line 62
    goto :goto_3c

    :cond_77
    move v1, v2

    .line 69
    goto :goto_49
.end method

.method protected a(Landroid/widget/ListAdapter;)V
    .registers 3
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 107
    return-void
.end method

.method protected b(I)V
    .registers 3
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    invoke-virtual {v0, p1}, Landroid/widget/GridView;->setSelection(I)V

    .line 112
    return-void
.end method

.method public d(I)V
    .registers 5
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/GridView;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 124
    if-nez v0, :cond_25

    .line 125
    const-string v0, "GridViewSlidePickerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Aborting onThumbnailAvailable. Could not find view for slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :goto_24
    return-void

    .line 129
    :cond_25
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:LRc;

    invoke-virtual {v1, v0, p1}, LRc;->a(Landroid/view/View;I)V

    goto :goto_24
.end method

.method public j_()V
    .registers 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    .line 91
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->j_()V

    .line 92
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 118
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/GridViewSlidePickerFragment;->q()V

    .line 119
    return-void
.end method
