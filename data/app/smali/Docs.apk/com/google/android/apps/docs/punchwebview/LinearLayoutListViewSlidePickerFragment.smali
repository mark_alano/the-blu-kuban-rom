.class public Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;
.super Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;
.source "LinearLayoutListViewSlidePickerFragment.java"


# static fields
.field static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LabX;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Landroid/widget/FrameLayout;

.field a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

.field private d:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Ljava/util/Map;

    .line 40
    sget-object v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, LabX;->a:LabX;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Ljava/util/Map;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, LabX;->b:LabX;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;-><init>()V

    return-void
.end method

.method private a()LabX;
    .registers 5

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    .line 128
    sget-object v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LabX;

    .line 129
    if-nez v0, :cond_31

    .line 130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Configuration has unknown orientation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 134
    :cond_31
    return-object v0
.end method

.method public static a()Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;
    .registers 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;-><init>()V

    return-object v0
.end method

.method private p()V
    .registers 3

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()LabX;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->setOrientation(LabX;)V

    .line 114
    return-void
.end method

.method private q()V
    .registers 3

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    new-instance v1, LQJ;

    invoke-direct {v1, p0}, LQJ;-><init>(Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->setOnScrollListener(LabW;)V

    .line 124
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    const/4 v2, 0x0

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-interface {v0, v3}, LdL;->a(Landroid/content/Context;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    if-nez v0, :cond_2c

    .line 59
    sget v0, Lej;->punch_list_view_slide_picker:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    sget v3, Leh;->list_view:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/LinearLayoutListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    if-eqz v0, :cond_62

    move v0, v1

    :goto_29
    invoke-static {v0}, Lagu;->b(Z)V

    .line 64
    :cond_2c
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->q()V

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->p()V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    if-nez v0, :cond_64

    :goto_36
    invoke-static {v1}, Lagu;->b(Z)V

    .line 69
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    new-instance v1, LQI;

    invoke-direct {v1, p0}, LQI;-><init>(Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->setOnItemClickListener(LabV;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    return-object v0

    :cond_62
    move v0, v2

    .line 61
    goto :goto_29

    :cond_64
    move v1, v2

    .line 68
    goto :goto_36
.end method

.method protected a(Landroid/widget/ListAdapter;)V
    .registers 3
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 96
    return-void
.end method

.method protected b(I)V
    .registers 3
    .parameter

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(I)V

    .line 108
    return-void
.end method

.method public d(I)V
    .registers 5
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Lcom/google/android/apps/docs/view/LinearLayoutListView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/view/LinearLayoutListView;->a(I)Landroid/view/View;

    move-result-object v0

    .line 140
    if-nez v0, :cond_21

    .line 141
    const-string v0, "LinearLayoutListViewSlidePickerFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Aborting onThumbnailAvailable. Could not find view for slideIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :goto_20
    return-void

    .line 145
    :cond_21
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:LRc;

    invoke-virtual {v1, v0, p1}, LRc;->a(Landroid/view/View;I)V

    goto :goto_20
.end method

.method public j_()V
    .registers 3

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->a:Landroid/widget/FrameLayout;

    .line 90
    invoke-super {p0}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->j_()V

    .line 91
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/punchwebview/BaseSlidePickerFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 102
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/LinearLayoutListViewSlidePickerFragment;->p()V

    .line 103
    return-void
.end method
