.class public Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;
.super Landroid/widget/RelativeLayout;
.source "PunchPresentationView.java"


# instance fields
.field public a:LQR;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LQk;

.field private a:LQl;

.field public a:LRC;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/view/ViewGroup;

.field a:Landroid/view/animation/Animation;

.field private a:Landroid/webkit/WebView;

.field private a:Landroid/widget/ImageView;

.field public a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LRb;",
            ">;"
        }
    .end annotation
.end field

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private b:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 93
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 94
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a()V

    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 99
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a()V

    .line 100
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 104
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a()V

    .line 105
    return-void
.end method

.method private final a()V
    .registers 3

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)Laoo;

    move-result-object v0

    invoke-interface {v0, p0}, Laoo;->a(Ljava/lang/Object;)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lej;->central_slide_view:I

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 111
    sget v0, Leh;->swipe_next_arrow:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/widget/ImageView;

    .line 112
    sget v0, Leh;->swipe_previous_arrow:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->b:Landroid/widget/ImageView;

    .line 113
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lec;->fade_in:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/animation/Animation;

    .line 114
    sget v0, Leh;->punch_web_view_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/ViewGroup;

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Len;->punch_no_title_slide_content_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Ljava/lang/String;

    .line 116
    return-void
.end method

.method private a(LRE;)V
    .registers 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LRC;

    invoke-interface {v0}, LRC;->a()Z

    move-result v0

    .line 195
    if-eqz v0, :cond_25

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 205
    :cond_10
    :goto_10
    sget-object v0, LRE;->c:LRE;

    if-ne p1, v0, :cond_40

    .line 206
    :goto_14
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->b:Landroid/widget/ImageView;

    if-eqz v1, :cond_42

    :goto_18
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 207
    if-eqz v1, :cond_24

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 210
    :cond_24
    return-void

    .line 198
    :cond_25
    sget-object v0, LRE;->b:LRE;

    if-ne p1, v0, :cond_3c

    move v0, v1

    .line 199
    :goto_2a
    iget-object v5, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_3e

    move v3, v2

    :goto_2f
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    if-eqz v0, :cond_10

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_10

    :cond_3c
    move v0, v2

    .line 198
    goto :goto_2a

    :cond_3e
    move v3, v4

    .line 199
    goto :goto_2f

    :cond_40
    move v1, v2

    .line 205
    goto :goto_14

    :cond_42
    move v2, v4

    .line 206
    goto :goto_18
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V
    .registers 1
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->c()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;LRE;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a(LRE;)V

    return-void
.end method

.method private b()V
    .registers 8

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/webkit/WebView;

    if-eqz v0, :cond_1f

    .line 131
    iget-object v6, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LRC;

    invoke-interface {v0}, LRC;->a()LRy;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQR;

    iget-object v3, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LRC;

    iget-object v4, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Laoz;

    iget-object v5, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LeQ;

    invoke-virtual/range {v0 .. v5}, LRy;->a(Landroid/content/Context;LQR;LRC;Laoz;LeQ;)Landroid/view/View$OnTouchListener;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 134
    :cond_1f
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V
    .registers 1
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->b()V

    return-void
.end method

.method private c()V
    .registers 4

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQR;

    invoke-interface {v0}, LQR;->c()I

    move-result v0

    if-nez v0, :cond_9

    .line 166
    :cond_8
    :goto_8
    return-void

    .line 157
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQR;

    invoke-interface {v0}, LQR;->d()I

    move-result v0

    .line 158
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQR;

    invoke-interface {v1, v0}, LQR;->a(I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQR;

    invoke-interface {v1, v0}, LQR;->b(I)Ljava/lang/String;

    move-result-object v1

    .line 160
    if-eqz v1, :cond_25

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3f

    :cond_25
    const/4 v0, 0x1

    .line 161
    :goto_26
    iget-object v2, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/ViewGroup;

    if-eqz v0, :cond_41

    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Ljava/lang/String;

    :goto_2c
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    goto :goto_8

    .line 160
    :cond_3f
    const/4 v0, 0x0

    goto :goto_26

    :cond_41
    move-object v0, v1

    .line 161
    goto :goto_2c
.end method

.method private d()V
    .registers 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQk;

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 170
    new-instance v0, LQZ;

    invoke-direct {v0, p0}, LQZ;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQk;

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQR;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQk;

    invoke-interface {v0, v1}, LQR;->a(LQS;)V

    .line 185
    return-void

    .line 169
    :cond_17
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private e()V
    .registers 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQk;

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQR;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQk;

    invoke-interface {v0, v1}, LQR;->b(LQS;)V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQk;

    .line 191
    return-void

    .line 188
    :cond_13
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private f()V
    .registers 3

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQl;

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 214
    new-instance v0, LRa;

    invoke-direct {v0, p0}, LRa;-><init>(Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQl;

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LRC;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQl;

    invoke-interface {v0, v1}, LRC;->a(LRD;)V

    .line 226
    return-void

    .line 213
    :cond_17
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private g()V
    .registers 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQl;

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LRC;

    iget-object v1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQl;

    invoke-interface {v0, v1}, LRC;->b(LRD;)V

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:LQl;

    .line 232
    return-void

    .line 229
    :cond_13
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .registers 2

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 147
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 148
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->d()V

    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->f()V

    .line 150
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->e()V

    .line 140
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->g()V

    .line 141
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 142
    return-void
.end method

.method public setWebView(Landroid/webkit/WebView;)V
    .registers 2
    .parameter

    .prologue
    .line 124
    iput-object p1, p0, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->a:Landroid/webkit/WebView;

    .line 126
    invoke-direct {p0}, Lcom/google/android/apps/docs/punchwebview/PunchPresentationView;->b()V

    .line 127
    return-void
.end method
