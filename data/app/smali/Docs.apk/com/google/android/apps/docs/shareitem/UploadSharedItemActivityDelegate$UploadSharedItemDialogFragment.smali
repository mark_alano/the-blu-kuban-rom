.class public Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "UploadSharedItemActivityDelegate.java"


# instance fields
.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    return-void
.end method

.method private a()Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;
    .registers 2

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;
    .registers 2
    .parameter

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a:LdL;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 107
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    move-result-object v1

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v2

    .line 110
    const-string v0, "layout_inflater"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 112
    invoke-static {v1, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    .line 114
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 115
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 116
    invoke-static {v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->b:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    invoke-virtual {v0}, Lfb;->a()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 118
    const v0, 0x104000a

    new-instance v1, LTj;

    invoke-direct {v1, p0}, LTj;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 125
    const/high16 v0, 0x104

    new-instance v1, LTk;

    invoke-direct {v1, p0}, LTk;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 133
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 134
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 135
    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 4
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 141
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a()Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V

    .line 142
    return-void
.end method
