.class public Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;
.super Lcom/google/android/apps/docs/app/BaseDialogActivity;
.source "UploadSharedItemActivityDelegate.java"


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LSJ;

.field private a:LSP;

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZj;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZv;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LaaD;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/content/ServiceConnection;

.field private a:Landroid/widget/Button;

.field private a:Landroid/widget/CheckBox;

.field private a:Landroid/widget/EditText;

.field private a:Landroid/widget/ImageView;

.field private a:Landroid/widget/LinearLayout;

.field private a:Landroid/widget/Spinner;

.field private a:Landroid/widget/TextView;

.field public a:Lev;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LSG;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LoL;

.field public a:LrR;
    .annotation runtime Laon;
    .end annotation
.end field

.field private b:Landroid/widget/TextView;

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public c:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "LZS;",
            ">;"
        }
    .end annotation
.end field

.field public d:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;-><init>()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/content/ServiceConnection;

    .line 819
    return-void
.end method

.method private a()LSJ;
    .registers 6

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSJ;

    if-nez v0, :cond_38

    .line 366
    new-instance v0, LSW;

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSP;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LZv;

    invoke-direct {v0, v1, v2, v3}, LSW;-><init>(LSP;Landroid/content/ContentResolver;LZv;)V

    .line 368
    new-instance v1, LSR;

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LaaD;

    iget-object v3, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Laoz;

    invoke-direct {v1, p0, v2, v3}, LSR;-><init>(Landroid/app/Activity;LaaD;Laoz;)V

    .line 369
    new-instance v2, LSQ;

    iget-object v3, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LKS;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LSQ;-><init>(LKS;Landroid/content/ContentResolver;)V

    .line 372
    new-instance v3, LSJ;

    invoke-direct {v3}, LSJ;-><init>()V

    invoke-virtual {v3, v0}, LSJ;->a(LSW;)LSJ;

    move-result-object v0

    invoke-virtual {v0, v1}, LSJ;->a(LSR;)LSJ;

    move-result-object v0

    invoke-virtual {v0, v2}, LSJ;->a(LSQ;)LSJ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSJ;

    .line 375
    :cond_38
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSJ;

    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .registers 4
    .parameter

    .prologue
    .line 231
    sget v0, Lej;->upload_shared_item_activity:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 232
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/view/View;)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Landroid/content/Intent;)V

    .line 234
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/view/LayoutInflater;)Landroid/view/View;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 898
    if-nez v0, :cond_22

    .line 899
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 900
    const-string v2, "accountName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 901
    if-eqz v2, :cond_22

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 902
    const-string v0, "collectionResourceId"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 905
    :cond_22
    if-nez v0, :cond_28

    .line 906
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 908
    :cond_28
    return-object v0
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 356
    if-nez p1, :cond_e

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSP;

    const-string v1, "_display_name"

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p2, v1, v2}, LSP;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 360
    :cond_e
    return-object p1
.end method

.method private a()Ljava/util/List;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 555
    invoke-static {}, LajX;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LME;

    invoke-interface {v0}, LME;->a()[Landroid/accounts/Account;

    move-result-object v2

    .line 557
    array-length v3, v2

    const/4 v0, 0x0

    :goto_c
    if-ge v0, v3, :cond_32

    aget-object v4, v2, v0

    .line 558
    iget-object v5, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LZj;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v5, v6}, LZj;->a(Ljava/lang/String;)LZi;

    move-result-object v5

    .line 559
    invoke-direct {p0, v5}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LZi;)Z

    move-result v6

    .line 560
    invoke-virtual {v5}, LZi;->a()Z

    move-result v7

    if-nez v7, :cond_2f

    invoke-virtual {v5}, LZi;->b()Z

    move-result v5

    if-nez v5, :cond_2a

    if-eqz v6, :cond_2f

    .line 561
    :cond_2a
    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 557
    :cond_2f
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 565
    :cond_32
    return-object v1
.end method

.method private a(Landroid/content/Intent;)Ljava/util/List;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "LSG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 298
    invoke-static {}, LajX;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 300
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 301
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 302
    const-string v3, "evaluateForOcr"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 305
    const-string v4, "android.intent.action.SEND"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_71

    .line 306
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_54

    .line 307
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 308
    const-string v2, "docListTitle"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 310
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()LSJ;

    move-result-object v4

    .line 311
    invoke-virtual {v4, v0}, LSJ;->a(Landroid/net/Uri;)LSJ;

    move-result-object v0

    invoke-virtual {v0, v2}, LSJ;->a(Ljava/lang/String;)LSJ;

    move-result-object v0

    invoke-virtual {v0, v3}, LSJ;->a(Z)LSJ;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LSJ;->b(Ljava/lang/String;)LSJ;

    .line 314
    invoke-virtual {v4}, LSJ;->a()LSH;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 333
    :cond_53
    :goto_53
    return-object v1

    .line 315
    :cond_54
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 316
    const-string v0, "docListTitle"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 317
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 318
    new-instance v3, LSK;

    invoke-direct {v3, v0, v2, p0}, LSK;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Landroid/content/Context;)V

    .line 319
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_53

    .line 321
    :cond_71
    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 322
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 323
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()LSJ;

    move-result-object v2

    .line 324
    invoke-virtual {v2, v3}, LSJ;->a(Z)LSJ;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LSJ;->b(Ljava/lang/String;)LSJ;

    .line 326
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_92
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_53

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 327
    const/4 v4, 0x0

    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 328
    invoke-virtual {v2, v0}, LSJ;->a(Landroid/net/Uri;)LSJ;

    move-result-object v0

    invoke-virtual {v0, v4}, LSJ;->a(Ljava/lang/String;)LSJ;

    .line 329
    invoke-virtual {v2}, LSJ;->a()LSH;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_92
.end method

.method private a(Z)Ljava/util/List;
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "LrK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 629
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_64

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSG;

    .line 633
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_5f

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    if-eqz v1, :cond_5f

    .line 635
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 641
    :goto_2e
    new-instance v4, LrM;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c:Laoz;

    invoke-direct {v4, v5, v6}, LrM;-><init>(Landroid/content/ContentResolver;Laoz;)V

    invoke-virtual {v4, v1}, LrM;->a(Ljava/lang/String;)LrM;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, LrM;->c(Ljava/lang/String;)LrM;

    move-result-object v1

    invoke-virtual {v1, p1}, LrM;->a(Z)LrM;

    move-result-object v1

    iget-boolean v4, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->k:Z

    invoke-virtual {v1, v4}, LrM;->b(Z)LrM;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LrM;->d(Ljava/lang/String;)LrM;

    move-result-object v1

    .line 648
    invoke-interface {v0, v1}, LSG;->a(LrM;)LrK;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 638
    :cond_5f
    invoke-interface {v0}, LSG;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_2e

    .line 650
    :cond_64
    return-object v2
.end method

.method private a(ILjava/lang/String;)V
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 698
    if-eqz p2, :cond_b

    const-string v0, "root"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 700
    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c()Ljava/lang/String;

    move-result-object v0

    .line 712
    :goto_f
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lel;->upload_toast_message:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 714
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 715
    invoke-static {v1, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 716
    return-void

    .line 702
    :cond_31
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 704
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Llf;

    invoke-interface {v1, v0, p2}, Llf;->a(LkB;Ljava/lang/String;)LkH;

    move-result-object v0

    .line 705
    if-nez v0, :cond_46

    .line 706
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_f

    .line 708
    :cond_46
    invoke-virtual {v0}, LkH;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_f
.end method

.method private a(Landroid/content/Intent;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 252
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 253
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 254
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 255
    new-instance v1, LSP;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v1, v2}, LSP;-><init>(Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LSP;

    .line 256
    invoke-static {}, LajX;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    .line 257
    const-string v1, "evaluateForOcr"

    invoke-virtual {p1, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 260
    const-string v1, "android.intent.action.SEND"

    const-string v2, "android.intent.action.SEND_MULTIPLE"

    invoke-static {v1, v2}, Lajm;->a(Ljava/lang/Object;Ljava/lang/Object;)Lajm;

    move-result-object v1

    .line 261
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4c

    .line 262
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c(Ljava/lang/String;)V

    .line 295
    :goto_4b
    return-void

    .line 266
    :cond_4c
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7d

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7d

    .line 267
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 268
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LKS;

    const-string v3, "maxExtraTextLength"

    const v4, 0xf4240

    invoke-interface {v2, v3, v4}, LKS;->a(Ljava/lang/String;I)I

    move-result v2

    if-le v1, v2, :cond_7d

    .line 270
    sget v0, Len;->notification_extra_text_is_too_long:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(I)V

    goto :goto_4b

    .line 275
    :cond_7d
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    .line 276
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a2

    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No files requested to be uploaded: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->c(Ljava/lang/String;)V

    goto :goto_4b

    .line 281
    :cond_a2
    const-string v0, "convertDocument"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    .line 282
    const-string v0, "deleteAfterUpload"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->k:Z

    .line 283
    const-string v0, "showConversionOption"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->i:Z

    .line 285
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    .line 286
    const-string v0, "collectionResourceId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 287
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 288
    if-eqz v0, :cond_db

    .line 289
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Z)Ljava/util/List;

    move-result-object v0

    .line 290
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/util/List;)V

    goto/16 :goto_4b

    .line 292
    :cond_db
    new-instance v0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;

    invoke-direct {v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;-><init>()V

    .line 293
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()Lo;

    move-result-object v1

    const-string v2, "UploadSharedItemDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate$UploadSharedItemDialogFragment;->a(Lo;Ljava/lang/String;)V

    goto/16 :goto_4b
.end method

.method private a(Landroid/net/Uri;)V
    .registers 5
    .parameter

    .prologue
    .line 429
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->k:Z

    if-eqz v0, :cond_36

    if-eqz p1, :cond_36

    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 430
    const-string v0, "UploadSharedItemActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cleaning up file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 432
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 434
    :cond_36
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 3
    .parameter

    .prologue
    .line 238
    sget v0, Leh;->upload_textview_document_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/TextView;

    .line 239
    sget v0, Leh;->upload_multiple_listview_document_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Landroid/widget/TextView;

    .line 240
    sget v0, Leh;->upload_edittext_document_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    .line 241
    sget v0, Leh;->upload_image_preview:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    .line 242
    sget v0, Leh;->upload_doclist_convert:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    .line 243
    sget v0, Leh;->upload_spinner_account:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    .line 244
    sget v0, Leh;->upload_folder:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Button;

    .line 245
    sget v0, Leh;->upload_conversion_options_layout:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/LinearLayout;

    .line 248
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->e()Z

    .line 249
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V
    .registers 1
    .parameter

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->f()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->d(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LZj;

    invoke-interface {v0, p1}, LZj;->a(Ljava/lang/String;)LZi;

    move-result-object v3

    .line 494
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->i:Z

    if-eqz v0, :cond_2b

    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LZi;)Z

    move-result v0

    if-eqz v0, :cond_2b

    move v0, v1

    .line 495
    :goto_13
    iget-object v4, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2d

    :goto_17
    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 498
    invoke-virtual {v3}, LZi;->b()Z

    move-result v0

    .line 499
    if-nez v0, :cond_25

    .line 500
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    invoke-virtual {v2, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 502
    :cond_25
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 503
    return-void

    :cond_2b
    move v0, v2

    .line 494
    goto :goto_13

    .line 495
    :cond_2d
    const/16 v2, 0x8

    goto :goto_17
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 893
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 894
    return-void
.end method

.method private declared-synchronized a(Ljava/util/List;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LrK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 659
    monitor-enter p0

    :try_start_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LrK;

    .line 660
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LrK;)Z
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_47

    move-result v0

    if-eqz v0, :cond_5

    .line 691
    :goto_17
    monitor-exit p0

    return-void

    .line 665
    :cond_19
    :try_start_19
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 666
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 668
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LrR;

    new-instance v2, LTh;

    invoke-direct {v2, p0, p1}, LTh;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/util/List;)V

    invoke-interface {v1, p0, v2}, LrR;->a(Landroid/content/Context;LZA;)Landroid/content/ServiceConnection;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/content/ServiceConnection;

    .line 685
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/content/ServiceConnection;

    if-nez v1, :cond_3f

    .line 686
    sget v1, Len;->upload_queue_failed_to_start:I

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 690
    :cond_3f
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(ILjava/lang/String;)V
    :try_end_46
    .catchall {:try_start_19 .. :try_end_46} :catchall_47

    goto :goto_17

    .line 659
    :catchall_47
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Ljava/util/List;I)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 569
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-static {p0}, LnL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    const v2, 0x1090008

    invoke-direct {v1, v0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 572
    const v0, 0x1090009

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 574
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 575
    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_16

    .line 577
    :cond_26
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    new-instance v2, LTg;

    invoke-direct {v2, p0, v1}, LTg;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Landroid/widget/ArrayAdapter;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 592
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 593
    return-void
.end method

.method private a(LZi;)Z
    .registers 4
    .parameter

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSG;

    .line 507
    invoke-interface {v0}, LSG;->b()Ljava/lang/String;

    move-result-object v0

    .line 508
    invoke-virtual {p1, v0}, LZi;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 509
    const/4 v0, 0x0

    .line 512
    :goto_1d
    return v0

    :cond_1e
    const/4 v0, 0x1

    goto :goto_1d
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 337
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "forceShowDialog"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 349
    :cond_d
    :goto_d
    return v0

    .line 340
    :cond_e
    if-eqz p1, :cond_d

    if-eqz p2, :cond_d

    .line 343
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LME;

    invoke-interface {v1}, LME;->a()[Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1, p1}, LMG;->a([Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_d

    .line 347
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Llf;

    invoke-interface {v1, p1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v1

    .line 348
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Llf;

    invoke-interface {v2, v1, p2}, Llf;->a(LkB;Ljava/lang/String;)LkH;

    move-result-object v1

    .line 349
    if-eqz v1, :cond_d

    const/4 v0, 0x1

    goto :goto_d
.end method

.method private a(LrK;)Z
    .registers 13
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 738
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LZj;

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, LZj;->a(Ljava/lang/String;)LZi;

    move-result-object v5

    .line 742
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_72

    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_72

    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 748
    :goto_1c
    invoke-virtual {p1}, LrK;->b()I

    move-result v1

    int-to-long v6, v1

    .line 749
    const-wide/16 v1, 0x0

    .line 753
    if-eqz v0, :cond_31

    invoke-virtual {p1}, LrK;->b()Ljava/lang/String;

    move-result-object v0

    const-string v8, "image/"

    invoke-virtual {v0, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_75

    .line 754
    :cond_31
    sget-object v0, LkP;->j:LkP;

    invoke-virtual {v5, v0}, LZi;->a(LkP;)J

    move-result-wide v1

    .line 768
    :cond_37
    cmp-long v0, v6, v1

    if-lez v0, :cond_9b

    .line 769
    invoke-virtual {p1}, LrK;->a()Ljava/lang/String;

    move-result-object v0

    .line 770
    invoke-static {p0, v6, v7}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 771
    invoke-static {p0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 772
    sget v2, Len;->file_too_large_for_upload_drivev2:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 773
    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v4

    aput-object v5, v6, v3

    const/4 v0, 0x2

    aput-object v1, v6, v0

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 775
    sget v1, Len;->file_too_large_for_upload_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 776
    sget v2, Len;->file_too_large_for_upload_okbtn:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 778
    invoke-static {v0, v1, v2}, LnY;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 779
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LoL;

    invoke-interface {v1, v0}, LoL;->a(Landroid/os/Bundle;)V

    move v0, v3

    .line 783
    :goto_71
    return v0

    .line 742
    :cond_72
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    goto :goto_1c

    .line 759
    :cond_75
    invoke-virtual {p1}, LrK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, LZi;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 761
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_81
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 762
    invoke-static {v0}, LkO;->a(Ljava/lang/String;)LkP;

    move-result-object v0

    invoke-virtual {v5, v0}, LZi;->a(LkP;)J

    move-result-wide v9

    .line 764
    invoke-static {v9, v10, v1, v2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    move-wide v1, v0

    .line 765
    goto :goto_81

    :cond_9b
    move v0, v4

    .line 783
    goto :goto_71
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 437
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    if-eqz v0, :cond_b

    sget v0, Len;->upload_shared_item_title_convert_drivev2:I

    :goto_6
    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_b
    sget v0, Len;->upload_shared_item_title_drivev2:I

    goto :goto_6
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 912
    .line 914
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Lev;

    invoke-interface {v1, p1}, Lev;->a(Ljava/lang/String;)Let;

    move-result-object v1

    .line 915
    const-string v2, "lastUploadCollection"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Let;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_d
    .catch Lew; {:try_start_1 .. :try_end_d} :catch_f

    move-result-object v0

    .line 920
    :goto_e
    return-object v0

    .line 917
    :catch_f
    move-exception v1

    .line 918
    const-string v2, "UploadSharedItemActivity"

    const-string v3, "Failed to lookup the last collection to upload the file to."

    invoke-static {v2, v3, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_e
.end method

.method private b(I)V
    .registers 5
    .parameter

    .prologue
    .line 800
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 801
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LZM;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 802
    const-string v1, "UploadSharedItemActivity"

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 804
    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .registers 6
    .parameter

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_4d

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 456
    :goto_16
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->h()V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_5f

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSG;

    .line 459
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-interface {v0}, LSG;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 461
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Labr;->a(Landroid/widget/EditText;Landroid/app/Dialog;)V

    .line 462
    invoke-interface {v0}, LSG;->a()V

    .line 471
    :goto_3b
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Button;

    new-instance v1, LTf;

    invoke-direct {v1, p0}, LTf;-><init>(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 483
    return-void

    .line 451
    :cond_4d
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 452
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/TextView;

    sget v1, Len;->upload_multiple_document_titles:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_16

    .line 464
    :cond_5f
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_91

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSG;

    .line 466
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, LSG;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6a

    .line 468
    :cond_91
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3b
.end method

.method static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;)V
    .registers 1
    .parameter

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->g()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 8
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 596
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Llf;

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 598
    if-eqz p1, :cond_49

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Llf;

    invoke-interface {v1, v0, p1}, Llf;->a(LkB;Ljava/lang/String;)LkH;

    move-result-object v0

    move-object v1, v0

    .line 604
    :goto_12
    if-eqz v1, :cond_1c

    const-string v0, "root"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 605
    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->d:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 606
    invoke-virtual {v0}, Lfb;->b()LmK;

    move-result-object v1

    .line 607
    const-string p1, "root"

    .line 608
    invoke-virtual {v1}, LmK;->a()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 609
    invoke-virtual {v0}, Lfb;->e()I

    move-result v0

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    .line 616
    :goto_39
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 618
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 619
    return-void

    .line 598
    :cond_49
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_12

    .line 611
    :cond_4c
    invoke-virtual {v1}, LkH;->c()Ljava/lang/String;

    move-result-object v0

    .line 612
    invoke-virtual {v1}, LkH;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, LkH;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, LkH;->d()Z

    move-result v1

    invoke-static {v2, v3, v1}, LkO;->a(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    goto :goto_39
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 925
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Lev;

    invoke-interface {v0, p1}, Lev;->a(Ljava/lang/String;)Let;

    move-result-object v0

    .line 926
    const-string v1, "lastUploadCollection"

    invoke-interface {v0, v1, p2}, Let;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Lev;

    invoke-interface {v1, v0}, Lev;->a(Let;)V
    :try_end_10
    .catch Lew; {:try_start_0 .. :try_end_10} :catch_11

    .line 931
    :goto_10
    return-void

    .line 928
    :catch_11
    move-exception v0

    .line 929
    const-string v1, "UploadSharedItemActivity"

    const-string v2, "Failed to save the last collection to upload the file to."

    invoke-static {v1, v2, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_10
.end method

.method private c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->d:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 720
    invoke-virtual {v0}, Lfb;->b()LmK;

    move-result-object v0

    .line 721
    invoke-virtual {v0}, LmK;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 722
    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 810
    const-string v0, "UploadSharedItemActivity"

    invoke-static {v0, p1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 812
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 934
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 935
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Ljava/lang/String;)V

    .line 936
    return-void
.end method

.method private e()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 535
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a()Ljava/util/List;

    move-result-object v1

    .line 536
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 537
    sget v1, Len;->no_account_support_this_upload:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(I)V

    .line 538
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 551
    :goto_13
    return v0

    .line 542
    :cond_14
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    if-nez v2, :cond_20

    .line 543
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LME;

    invoke-interface {v2}, LME;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    .line 545
    :cond_20
    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 546
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/lang/String;)V

    .line 548
    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/util/List;I)V

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->d(Ljava/lang/String;)V

    .line 551
    const/4 v0, 0x1

    goto :goto_13
.end method

.method private f()V
    .registers 2

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 418
    :goto_e
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Z)Ljava/util/List;

    move-result-object v0

    .line 419
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Ljava/util/List;)V

    .line 420
    return-void

    .line 415
    :cond_16
    iget-boolean v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->j:Z

    goto :goto_e
.end method

.method private g()V
    .registers 3

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 424
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/net/Uri;)V

    .line 425
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    .line 426
    return-void
.end method

.method private h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 517
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1f

    .line 518
    const/4 v0, 0x0

    .line 525
    :goto_b
    if-eqz v0, :cond_40

    .line 526
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 527
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 528
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 532
    :goto_1e
    return-void

    .line 520
    :cond_1f
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LSG;

    .line 521
    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v2, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 523
    invoke-interface {v0, v1}, LSG;->a(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_b

    .line 530
    :cond_40
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1e
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    return-object v0
.end method

.method public e()V
    .registers 2

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/content/Intent;)V

    .line 228
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 398
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 400
    const/4 v0, 0x1

    if-ne p1, v0, :cond_18

    .line 401
    const/4 v0, -0x1

    if-ne p2, v0, :cond_17

    .line 402
    const-string v0, "resourceId"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->b(Ljava/lang/String;)V

    .line 410
    :cond_17
    :goto_17
    return-void

    .line 405
    :cond_18
    if-eqz p1, :cond_21

    .line 406
    const-string v0, "UploadSharedItemActivity"

    const-string v1, "Invalid request code in activity result."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :cond_21
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->finish()V

    goto :goto_17
.end method

.method public onBackPressed()V
    .registers 3

    .prologue
    .line 727
    invoke-virtual {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 728
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(Landroid/net/Uri;)V

    .line 729
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onBackPressed()V

    .line 730
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 210
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 212
    if-eqz p1, :cond_23

    .line 213
    const-string v0, "accountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    .line 216
    const-string v0, "AccountCollectionMap"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 218
    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    .line 222
    :goto_17
    new-instance v0, LnY;

    invoke-direct {v0}, LnY;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a(LnB;)LoL;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:LoL;

    .line 223
    return-void

    .line 220
    :cond_23
    invoke-static {}, Lalp;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    goto :goto_17
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 788
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_9

    .line 789
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->unbindService(Landroid/content/ServiceConnection;)V

    .line 791
    :cond_9
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onDestroy()V

    .line 792
    return-void
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 380
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onResume()V

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Landroid/widget/Spinner;

    if-eqz v0, :cond_a

    .line 383
    invoke-direct {p0}, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->e()Z

    .line 385
    :cond_a
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 389
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 391
    const-string v0, "accountName"

    iget-object v1, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const-string v1, "AccountCollectionMap"

    iget-object v0, p0, Lcom/google/android/apps/docs/shareitem/UploadSharedItemActivityDelegate;->a:Ljava/util/Map;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 394
    return-void
.end method
