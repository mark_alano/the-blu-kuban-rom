.class public Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "AddCollaboratorTextDialogFragment.java"

# interfaces
.implements LTC;
.implements Landroid/text/TextWatcher;


# static fields
.field private static final a:LeG;

.field private static m:I


# instance fields
.field public a:LKS;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LTI;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LTr;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LUj;

.field public a:LUq;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZj;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Laji;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laji",
            "<",
            "LTP;",
            ">;"
        }
    .end annotation
.end field

.field private a:Landroid/app/AlertDialog;

.field private final a:Landroid/os/Handler;

.field a:Landroid/widget/Button;

.field private a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field private a:Landroid/widget/MultiAutoCompleteTextView;

.field private a:LdG;

.field private a:Lgq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgq",
            "<",
            "LUq;",
            ">;"
        }
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:[Ljava/lang/String;

.field b:Landroid/widget/Button;

.field public b:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 70
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->m:I

    .line 89
    sget-object v0, LeG;->b:LeG;

    sput-object v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LeG;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 91
    new-instance v0, LUj;

    invoke-direct {v0}, LUj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUj;

    .line 144
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/os/Handler;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)Landroid/app/AlertDialog;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/os/Handler;

    return-object v0
.end method

.method private a(Landroid/app/AlertDialog$Builder;Landroid/content/Context;)Landroid/widget/ListView;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 285
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 287
    sget v1, Lej;->add_collaborator:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 289
    sget v0, Leh;->sharing_options:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 290
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 291
    invoke-direct {p0, p2, v3}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v1

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 292
    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x109000f

    invoke-direct {v3, p2, v4, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 295
    invoke-virtual {p1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 296
    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)Landroid/widget/MultiAutoCompleteTextView;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;LdG;)LdG;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdG;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdL;

    return-object v0
.end method

.method private a()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 361
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 362
    const/4 v0, 0x0

    :goto_10
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_36

    .line 364
    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v3, v1, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v3

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 365
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2d

    .line 366
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    :cond_2d
    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v3, v1, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenEnd(Ljava/lang/CharSequence;I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 369
    :cond_36
    return-object v2
.end method

.method private a(Landroid/content/Context;Z)Ljava/util/List;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Laji;

    invoke-static {v0}, LajX;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 301
    if-nez p2, :cond_d

    .line 302
    sget-object v1, LTP;->d:LTP;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 304
    :cond_d
    invoke-static {p1, v0}, LTP;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic a(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-static {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/app/AlertDialog$Builder;)V
    .registers 4
    .parameter

    .prologue
    .line 251
    const/high16 v0, 0x104

    new-instance v1, LTt;

    invoke-direct {v1, p0}, LTt;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 262
    sget v0, Len;->add_collaborator_accept:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 263
    return-void
.end method

.method private a(Landroid/app/AlertDialog;Landroid/content/Context;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 266
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/Button;

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/Button;

    new-instance v1, LTu;

    invoke-direct {v1, p0, p2}, LTu;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 276
    const/4 v0, -0x2

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b:Landroid/widget/Button;

    .line 277
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->q()V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/app/AlertDialog;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Landroid/app/AlertDialog;Landroid/content/Context;)V

    return-void
.end method

.method private a(Ljava/util/List;)Z
    .registers 12
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 401
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 402
    :goto_9
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_42

    .line 403
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 407
    const-string v5, "<"

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 408
    const-string v6, ">"

    invoke-virtual {v0, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    .line 409
    if-eq v5, v7, :cond_2d

    if-eq v6, v7, :cond_2d

    if-le v6, v5, :cond_2d

    .line 410
    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 413
    :cond_2d
    new-instance v5, Lamn;

    invoke-direct {v5, v0, v3}, Lamn;-><init>(Ljava/lang/String;Z)V

    .line 414
    invoke-virtual {v5}, Lamn;->a()Z

    move-result v5

    if-nez v5, :cond_3b

    .line 415
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    :cond_3b
    invoke-interface {p1, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 402
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 421
    :cond_42
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_6f

    .line 422
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/content/res/Resources;

    move-result-object v1

    sget v5, Lel;->add_collaborators_invalid_contact_address:I

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    new-array v7, v3, [Ljava/lang/Object;

    const-string v8, ", "

    invoke-interface {v4}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v1, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 429
    :cond_6f
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_76

    move v2, v3

    :cond_76
    return v2
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)[Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdL;

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 631
    sget v0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->m:I

    .line 632
    sget v1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->m:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->m:I

    .line 633
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddCollaboratorTextDialogFragment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/util/List;[Ljava/lang/String;)Ljava/util/List;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LTG;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LTG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 581
    new-instance v1, Ljava/util/HashSet;

    array-length v0, p1

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 582
    array-length v2, p1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_12

    aget-object v3, p1, v0

    .line 583
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 582
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 585
    :cond_12
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 586
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1b
    :goto_1b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTG;

    .line 587
    invoke-interface {v0}, LTG;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1b

    .line 588
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1b

    .line 591
    :cond_35
    return-object v2
.end method

.method private b(Landroid/app/AlertDialog;Landroid/content/Context;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/app/AlertDialog;

    sget v1, Leh;->text_view:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/MultiAutoCompleteTextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0, p0}, Landroid/widget/MultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 310
    new-instance v0, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;

    invoke-direct {v0}, Landroid/widget/MultiAutoCompleteTextView$CommaTokenizer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    .line 313
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->r()V

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    new-instance v1, LTv;

    invoke-direct {v1, p0, p2}, LTv;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/MultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v2}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/MultiAutoCompleteTextView;->setSelection(II)V

    .line 328
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/app/AlertDialog;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b(Landroid/app/AlertDialog;Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdL;

    return-object v0
.end method

.method private c()Ljava/lang/String;
    .registers 3

    .prologue
    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "confirmSharingDialog"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)LdL;
    .registers 2
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdL;

    return-object v0
.end method

.method private p()V
    .registers 3

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/MultiAutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/MultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_13

    const/4 v0, 0x1

    .line 351
    :goto_d
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 352
    return-void

    .line 350
    :cond_13
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private q()V
    .registers 7

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 380
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_b

    .line 389
    :cond_a
    :goto_a
    return-void

    .line 384
    :cond_b
    invoke-direct {p0, v3}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 385
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->a()Lo;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUq;

    invoke-interface {v4}, LUq;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUq;

    invoke-interface {v5}, LUq;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/sharingactivity/ConfirmSharingDialogFragment;->a(Lo;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a
.end method

.method private r()V
    .registers 4

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LKS;

    const-string v1, "enableMultiTokenCollaboratorSuggestions"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 436
    if-eqz v0, :cond_f

    .line 437
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->s()V

    .line 441
    :goto_e
    return-void

    .line 439
    :cond_f
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->t()V

    goto :goto_e
.end method

.method private s()V
    .registers 3

    .prologue
    .line 447
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in createAdapterAsync"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdG;

    if-eqz v0, :cond_c

    .line 506
    :goto_b
    return-void

    .line 452
    :cond_c
    new-instance v0, LTw;

    invoke-direct {v0, p0}, LTw;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdG;

    .line 505
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdG;

    invoke-virtual {v0}, LdG;->start()V

    goto :goto_b
.end method

.method private t()V
    .registers 3

    .prologue
    .line 512
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in createAdapterAsync"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdG;

    if-eqz v0, :cond_c

    .line 556
    :goto_b
    return-void

    .line 517
    :cond_c
    new-instance v0, LTy;

    invoke-direct {v0, p0}, LTy;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdG;

    .line 555
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdG;

    invoke-virtual {v0}, LdG;->start()V

    goto :goto_b
.end method

.method private u()V
    .registers 3

    .prologue
    .line 559
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in killCreateAdapterThread"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdG;

    .line 561
    if-eqz v0, :cond_11

    .line 562
    invoke-virtual {v0}, LdG;->a()V

    .line 564
    :try_start_e
    invoke-virtual {v0}, LdG;->join()V
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_11} :catch_12

    .line 569
    :cond_11
    :goto_11
    return-void

    .line 565
    :catch_12
    move-exception v0

    goto :goto_11
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 7
    .parameter

    .prologue
    .line 197
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in onCreateDialog"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUq;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUq;

    invoke-interface {v0}, LUq;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUq;

    invoke-interface {v0}, LUq;->a()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2d

    .line 202
    :cond_24
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "Early exit in onCreateView"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    const/4 v0, 0x0

    .line 235
    :goto_2c
    return-object v0

    .line 206
    :cond_2d
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 208
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 209
    sget v2, Len;->add_collaborators:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 210
    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Landroid/app/AlertDialog$Builder;)V

    .line 212
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a(Landroid/app/AlertDialog$Builder;Landroid/content/Context;)Landroid/widget/ListView;

    move-result-object v2

    .line 214
    sget-object v3, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LeG;

    invoke-static {v3}, LTP;->a(LeG;)LTP;

    move-result-object v3

    .line 215
    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Laji;

    invoke-virtual {v4, v3}, Laji;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 216
    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUj;

    invoke-virtual {v4, v3}, LUj;->a(I)V

    .line 217
    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUj;

    invoke-virtual {v3, v2}, LUj;->a(Landroid/widget/ListView;)V

    .line 219
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/app/AlertDialog;

    .line 224
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/app/AlertDialog;

    new-instance v2, LTs;

    invoke-direct {v2, p0, v0}, LTs;-><init>(Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Landroid/app/AlertDialog;

    goto :goto_2c
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 151
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in onCreate"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(Landroid/os/Bundle;)V

    .line 154
    new-instance v0, Lgq;

    const-class v1, LUq;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->b:Laoz;

    invoke-direct {v0, v1, v2}, Lgq;-><init>(Ljava/lang/Class;Laoz;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lgq;

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lgq;

    invoke-virtual {v0}, Lgq;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUq;

    .line 158
    invoke-interface {v0}, LUq;->e()Ljava/lang/String;

    move-result-object v1

    .line 159
    invoke-interface {v0}, LUq;->c()Ljava/lang/String;

    move-result-object v2

    .line 160
    invoke-static {v1, v2}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v1

    .line 161
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LZj;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Llf;

    invoke-static {v1, v2, v3}, LTP;->a(LkY;LZj;Llf;)Laji;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Laji;

    .line 164
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->d(Z)V

    .line 166
    if-eqz v0, :cond_45

    invoke-interface {v0}, LUq;->b()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_45

    invoke-interface {v0}, LUq;->a()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_5c

    .line 169
    :cond_45
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()V

    .line 171
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Lo;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/view/RoboDialogFragment;

    .line 173
    if-eqz v0, :cond_5b

    .line 174
    invoke-virtual {v0}, Lcom/google/android/apps/docs/view/RoboDialogFragment;->a()V

    .line 189
    :cond_5b
    :goto_5b
    return-void

    .line 181
    :cond_5c
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 182
    invoke-interface {v0}, LUq;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_69
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_81

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUB;

    .line 183
    invoke-virtual {v0}, LUB;->a()LUp;

    move-result-object v0

    invoke-virtual {v0}, LUp;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_69

    .line 186
    :cond_81
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:[Ljava/lang/String;

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUj;

    invoke-virtual {v0, p1}, LUj;->a(Landroid/os/Bundle;)V

    goto :goto_5b
.end method

.method public a(Ljava/util/List;)V
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUj;

    invoke-virtual {v0}, LUj;->a()I

    move-result v0

    .line 597
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Laji;

    invoke-virtual {v1}, Laji;->size()I

    move-result v1

    invoke-static {v0, v1}, Lagu;->a(II)I

    .line 600
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Laji;

    invoke-virtual {v1, v0}, Laji;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LTP;

    .line 603
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:Lgq;

    invoke-virtual {v1}, Lgq;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LUq;

    .line 604
    invoke-interface {v1}, LUq;->c()Ljava/lang/String;

    move-result-object v2

    .line 606
    invoke-virtual {v0}, LTP;->a()LeG;

    move-result-object v3

    .line 608
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 609
    new-instance v5, LeF;

    invoke-direct {v5}, LeF;-><init>()V

    invoke-virtual {v5, v0}, LeF;->b(Ljava/lang/String;)LeF;

    move-result-object v0

    invoke-virtual {v0, v2}, LeF;->a(Ljava/lang/String;)LeF;

    move-result-object v0

    invoke-virtual {v0, v3}, LeF;->a(LeG;)LeF;

    move-result-object v0

    sget-object v5, LeK;->a:LeK;

    invoke-virtual {v0, v5}, LeF;->a(LeK;)LeF;

    move-result-object v0

    invoke-virtual {v0}, LeF;->a()LeB;

    move-result-object v0

    .line 614
    invoke-interface {v1, v0}, LUq;->a(LeB;)V

    goto :goto_2b

    .line 617
    :cond_56
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LTr;

    invoke-interface {v0}, LTr;->a()V

    .line 619
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()V

    .line 620
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .parameter

    .prologue
    .line 332
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 336
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 240
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->c(Landroid/os/Bundle;)V

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUj;

    invoke-virtual {v0, p1}, LUj;->b(Landroid/os/Bundle;)V

    .line 242
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 246
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->g()V

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LUj;

    invoke-virtual {v0}, LUj;->a()V

    .line 248
    return-void
.end method

.method public i()V
    .registers 3

    .prologue
    .line 624
    const-string v0, "AddCollaboratorTextDialogFragment"

    const-string v1, "in onDestroy"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 626
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->u()V

    .line 627
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->i()V

    .line 628
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 341
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;->p()V

    .line 342
    return-void
.end method
