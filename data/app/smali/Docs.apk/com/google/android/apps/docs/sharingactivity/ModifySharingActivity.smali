.class public Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;
.super Lcom/google/android/apps/docs/app/BaseActivity;
.source "ModifySharingActivity.java"

# interfaces
.implements LTr;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static a:I


# instance fields
.field private a:LTL;

.field public a:LTn;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LUB;

.field private a:LUi;

.field private a:LUq;

.field public a:LUr;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LUy;

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZj;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Landroid/os/Parcelable;

.field private a:Landroid/support/v4/app/DialogFragment;

.field public a:Landroid/view/View;

.field a:Landroid/widget/ListView;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:Lgl;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LUB;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/concurrent/Executor;

.field private a:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field

.field public a:Llf;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:[LeH;

.field private a:[Ljava/lang/String;

.field private b:Landroid/os/Handler;

.field b:Landroid/view/View;

.field private b:Ljava/lang/String;

.field c:Landroid/view/View;

.field private c:Ljava/lang/String;

.field d:Landroid/view/View;

.field e:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 622
    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:I

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseActivity;-><init>()V

    .line 136
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    .line 138
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUy;

    .line 153
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()[LeH;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[LeH;

    .line 166
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUB;

    .line 174
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    .line 182
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    .line 186
    invoke-static {v1}, LamF;->a(Ljava/lang/Object;)LamQ;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;LUi;)LUi;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;LUq;)LUq;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 555
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 556
    const-string v1, "resourceId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 557
    const-string v1, "documentTitle"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 558
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 559
    return-object v0
.end method

.method private a(ILjava/lang/Throwable;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LZM;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 551
    return-void
.end method

.method private a(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 308
    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    if-ne p1, v0, :cond_21

    move v0, v1

    :goto_a
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 309
    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    if-ne p1, v0, :cond_23

    move v0, v1

    :goto_14
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/view/View;

    if-ne p1, v3, :cond_25

    :goto_1d
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 311
    return-void

    :cond_21
    move v0, v2

    .line 308
    goto :goto_a

    :cond_23
    move v0, v2

    .line 309
    goto :goto_14

    :cond_25
    move v1, v2

    .line 310
    goto :goto_1d
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->j()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c(I)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;ILjava/lang/Throwable;)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(ILjava/lang/Throwable;)V

    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;Landroid/view/View;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    return-void
.end method

.method private a(LUB;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 365
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    sget-object v2, LUi;->c:LUi;

    if-ne v1, v2, :cond_2f

    .line 366
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v1}, LUq;->b()Z

    move-result v1

    if-nez v1, :cond_15

    .line 367
    sget v1, Len;->sharing_cannot_change:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->d(I)V

    .line 377
    :goto_14
    return v0

    .line 369
    :cond_15
    if-nez p1, :cond_1d

    .line 370
    sget v1, Len;->sharing_cannot_change_option:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->d(I)V

    goto :goto_14

    .line 372
    :cond_1d
    invoke-virtual {p1}, LUB;->a()LUp;

    move-result-object v1

    invoke-virtual {v1}, LUp;->a()LeI;

    move-result-object v1

    sget-object v2, LeI;->a:LeI;

    if-ne v1, v2, :cond_2f

    .line 373
    sget v1, Len;->sharing_cannot_change_owner:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->d(I)V

    goto :goto_14

    .line 377
    :cond_2f
    const/4 v0, 0x0

    goto :goto_14
.end method

.method private final a()[LeH;
    .registers 3

    .prologue
    .line 156
    const-class v0, LeH;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 157
    sget-object v1, LeH;->n:LeH;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 158
    const/4 v1, 0x0

    new-array v1, v1, [LeH;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LeH;

    return-object v0
.end method

.method private a()[Ljava/lang/String;
    .registers 6

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    if-nez v0, :cond_23

    .line 514
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[LeH;

    array-length v1, v0

    .line 515
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    .line 516
    const/4 v0, 0x0

    :goto_c
    if-ge v0, v1, :cond_23

    .line 517
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[LeH;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v4}, LUq;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p0, v4}, LeH;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 516
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 521
    :cond_23
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[Ljava/lang/String;

    return-object v0
.end method

.method public static b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 625
    sget v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:I

    .line 626
    sget v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:I

    .line 627
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AddCollaboratorTextDialogFragment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(I)V
    .registers 4
    .parameter

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->e:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_13

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->e:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    .line 316
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 320
    :goto_d
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    .line 321
    return-void

    .line 318
    :cond_13
    const-string v0, "ModifySharingActivity"

    const-string v1, "Invalid empty list message view"

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->g()V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->d(I)V

    return-void
.end method

.method private c(I)V
    .registers 4
    .parameter

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[LeH;

    aget-object v1, v1, p1

    invoke-interface {v0, v1}, LUq;->a(LeH;)V

    .line 509
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->j()V

    .line 510
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->h()V

    return-void
.end method

.method private d(I)V
    .registers 5
    .parameter

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LZM;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 547
    return-void
.end method

.method private e()Z
    .registers 2

    .prologue
    .line 631
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private f()V
    .registers 3

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    sget-object v1, LUi;->c:LUi;

    if-ne v0, v1, :cond_11

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v0}, LUq;->a()V

    .line 257
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->g()V

    .line 258
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->j()V

    .line 260
    :cond_11
    return-void
.end method

.method private g()V
    .registers 6

    .prologue
    .line 263
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v0}, LUq;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUB;

    .line 265
    invoke-virtual {v0}, LUB;->a()LUp;

    move-result-object v3

    invoke-virtual {v3}, LUp;->a()LeK;

    move-result-object v3

    sget-object v4, LeK;->b:LeK;

    if-eq v3, v4, :cond_33

    invoke-virtual {v0}, LUB;->a()LUp;

    move-result-object v3

    invoke-virtual {v3}, LUp;->a()LeK;

    move-result-object v3

    sget-object v4, LeK;->a:LeK;

    if-ne v3, v4, :cond_f

    .line 267
    :cond_33
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_f

    .line 270
    :cond_37
    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    .line 271
    return-void
.end method

.method private h()V
    .registers 5

    .prologue
    .line 274
    sget-object v0, LUi;->a:LUi;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    .line 275
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->p()V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LTn;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, LTn;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LamQ;

    move-result-object v0

    .line 278
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    .line 279
    new-instance v1, LUf;

    invoke-direct {v1, p0}, LUf;-><init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 304
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    .line 305
    return-void
.end method

.method private i()V
    .registers 7

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 326
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v0}, LUq;->a()Z

    move-result v3

    .line 327
    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Landroid/view/View;

    if-eqz v3, :cond_27

    move v0, v1

    :goto_e
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->d:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    sget-object v5, LUi;->c:LUi;

    if-ne v4, v5, :cond_29

    iget-object v4, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v4}, LUq;->b()Z

    move-result v4

    if-eqz v4, :cond_29

    if-nez v3, :cond_29

    :goto_23
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 331
    return-void

    :cond_27
    move v0, v2

    .line 327
    goto :goto_e

    :cond_29
    move v1, v2

    .line 328
    goto :goto_23
.end method

.method private j()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 334
    const-string v0, "ModifySharingActivity"

    const-string v1, "Populating adapter"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    sget-object v1, LUi;->c:LUi;

    if-eq v0, v1, :cond_14

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    .line 362
    :goto_13
    return-void

    .line 340
    :cond_14
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->o()V

    .line 342
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->i()V

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 344
    sget v0, Len;->empty_sharing_list:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b(I)V

    goto :goto_13

    .line 347
    :cond_28
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    sget-object v1, LUi;->c:LUi;

    if-ne v0, v1, :cond_65

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    invoke-static {}, LUy;->a()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v0}, LUq;->a()LeH;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v1}, LUq;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, LeH;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 355
    new-instance v0, LUy;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LTL;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LUy;-><init>(Landroid/content/Context;Ljava/util/List;LTL;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUy;

    .line 360
    :goto_58
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUy;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_13

    .line 358
    :cond_65
    new-instance v0, LUy;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LTL;

    const/4 v4, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LUy;-><init>(Landroid/content/Context;Ljava/util/List;LTL;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUy;

    goto :goto_58
.end method

.method private k()V
    .registers 7

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->o()V

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUB;

    if-nez v0, :cond_f

    .line 384
    const-string v0, "ModifySharingActivity"

    const-string v1, "Selected item in contact sharing dialog is not defined."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_e
    :goto_e
    return-void

    .line 386
    :cond_f
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->e()Z

    move-result v0

    if-nez v0, :cond_e

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Ljava/lang/String;

    invoke-static {v0, v1}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v1

    .line 388
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUB;

    invoke-virtual {v0}, LUB;->a()LTG;

    move-result-object v0

    invoke-interface {v0}, LTG;->a()Ljava/lang/String;

    move-result-object v5

    .line 389
    new-instance v0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;

    invoke-direct {v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;-><init>()V

    .line 390
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LZj;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Llf;

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;->a(LkY;LZj;Llf;Landroid/content/Context;Ljava/lang/String;)V

    .line 391
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()Lo;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$ContactSharingDialogFragmentImpl;->a(Lo;Ljava/lang/String;)V

    .line 392
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    goto :goto_e
.end method

.method private l()V
    .registers 6

    .prologue
    .line 398
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->o()V

    .line 400
    const/4 v0, 0x0

    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[LeH;

    array-length v1, v1

    if-ge v0, v1, :cond_36

    .line 401
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v1}, LUq;->a()LeH;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:[LeH;

    aget-object v2, v2, v0

    if-ne v1, v2, :cond_37

    .line 403
    new-instance v1, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;

    invoke-direct {v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity$SharingOptionsDialogFragment;-><init>()V

    .line 404
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 405
    sget v3, Len;->dialog_sharing_options:I

    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()[Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a(Landroid/os/Bundle;I[Ljava/lang/String;)V

    .line 407
    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->d(Landroid/os/Bundle;)V

    .line 409
    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->c(I)V

    .line 410
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()Lo;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 415
    :cond_36
    return-void

    .line 400
    :cond_37
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method private m()V
    .registers 4

    .prologue
    .line 453
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->e()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 454
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a()Lo;

    move-result-object v0

    .line 455
    invoke-static {}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b()Ljava/lang/String;

    move-result-object v1

    .line 456
    new-instance v2, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;

    invoke-direct {v2}, Lcom/google/android/apps/docs/sharingactivity/AddCollaboratorTextDialogFragment;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    .line 457
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v2, v0, v1}, Landroid/support/v4/app/DialogFragment;->a(Lo;Ljava/lang/String;)V

    .line 459
    :cond_1a
    return-void
.end method

.method private n()V
    .registers 4

    .prologue
    .line 479
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->p()V

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUr;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v0, v1}, LUr;->a(LUq;)LamQ;

    move-result-object v0

    .line 481
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    .line 482
    new-instance v1, LUg;

    invoke-direct {v1, p0}, LUg;-><init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 504
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    .line 505
    return-void
.end method

.method private o()V
    .registers 3

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Lgl;

    sget-object v1, Lgi;->h:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 594
    :cond_a
    :goto_a
    return-void

    .line 589
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_a

    .line 592
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v1}, LUq;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    goto :goto_a
.end method

.method private p()V
    .registers 3

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_16

    .line 636
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 638
    :cond_16
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/Object;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 569
    const-class v2, LUq;

    if-ne p1, v2, :cond_10

    .line 570
    if-nez p2, :cond_e

    :goto_8
    invoke-static {v0}, Lagu;->a(Z)V

    .line 572
    iget-object p0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    .line 580
    :goto_d
    return-object p0

    :cond_e
    move v0, v1

    .line 570
    goto :goto_8

    .line 574
    :cond_10
    const-class v2, LTr;

    if-ne p1, v2, :cond_1c

    .line 575
    if-nez p2, :cond_1a

    :goto_16
    invoke-static {v0}, Lagu;->a(Z)V

    goto :goto_d

    :cond_1a
    move v0, v1

    goto :goto_16

    .line 580
    :cond_1c
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/docs/app/BaseActivity;->a(Ljava/lang/Class;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    goto :goto_d
.end method

.method public a()V
    .registers 4

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->p()V

    .line 599
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUr;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    invoke-interface {v0, v1}, LUr;->a(LUq;)LamQ;

    move-result-object v0

    .line 600
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Future;

    .line 601
    new-instance v1, LUh;

    invoke-direct {v1, p0}, LUh;-><init>(Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;)V

    .line 619
    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LamF;->a(LamQ;LamE;Ljava/util/concurrent/Executor;)V

    .line 620
    return-void
.end method

.method protected d()V
    .registers 2

    .prologue
    .line 228
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->d()V

    .line 229
    sget v0, Leh;->loading_sharing_list:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    .line 230
    sget v0, Leh;->empty_sharing_list:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/view/View;

    .line 231
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    .line 232
    sget v0, Leh;->save_sharing_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Landroid/view/View;

    .line 233
    sget v0, Leh;->add_collaborators_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->d:Landroid/view/View;

    .line 234
    sget v0, Leh;->empty_list_message:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->e:Landroid/view/View;

    .line 235
    return-void
.end method

.method public onAddClicked(Landroid/view/View;)V
    .registers 3
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 449
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->m()V

    .line 450
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 193
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 194
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/os/Handler;

    .line 195
    new-instance v0, Laat;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Landroid/os/Handler;

    invoke-direct {v0, v1}, Laat;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/util/concurrent/Executor;

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LeQ;

    invoke-virtual {v1}, LeQ;->a()V

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LeQ;

    const-string v2, "/sharing"

    invoke-virtual {v1, v2, v0}, LeQ;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 203
    sget v1, Lej;->sharing_list:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->setContentView(I)V

    .line 205
    new-instance v1, LTL;

    sget v2, LUy;->a:I

    invoke-direct {v1, p0, v2}, LTL;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LTL;

    .line 206
    const-string v1, "resourceId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Ljava/lang/String;

    .line 207
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Ljava/lang/String;

    if-nez v1, :cond_48

    .line 208
    const-string v0, "ModifySharingActivity"

    const-string v1, "No resource ID for the document is specified. Exiting."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->finish()V

    .line 224
    :goto_47
    return-void

    .line 212
    :cond_48
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/lang/String;

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/lang/String;

    if-nez v1, :cond_5f

    .line 214
    const-string v0, "ModifySharingActivity"

    const-string v1, "No account for the document access is specified. Exiting."

    invoke-static {v0, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->finish()V

    goto :goto_47

    .line 218
    :cond_5f
    const-string v1, "documentTitle"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Ljava/lang/String;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_8b

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 222
    :cond_8b
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUr;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, LUr;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LUq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUq;

    .line 223
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->h()V

    goto :goto_47
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 542
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onDestroy()V

    .line 543
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUB;

    .line 421
    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(LUB;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 431
    :goto_13
    return-void

    .line 424
    :cond_14
    if-nez v0, :cond_1a

    .line 425
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->l()V

    goto :goto_13

    .line 427
    :cond_1a
    const-string v1, "ModifySharingActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Clicked: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, LUB;->a()LTG;

    move-result-object v3

    invoke-interface {v3}, LTG;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUB;

    .line 429
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->k()V

    goto :goto_13
.end method

.method protected onPause()V
    .registers 4

    .prologue
    .line 526
    const-string v0, "ModifySharingActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause in state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    invoke-virtual {v2}, LUi;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 528
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LeQ;

    const-string v1, "/sharing"

    invoke-virtual {v0, p0, v1}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 532
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LTL;

    invoke-virtual {v0}, LTL;->a()V

    .line 533
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->p()V

    .line 534
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/support/v4/app/DialogFragment;

    .line 535
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onPause()V

    .line 536
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 463
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 464
    const-string v0, "listViewState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    .line 465
    return-void
.end method

.method protected onResume()V
    .registers 4

    .prologue
    .line 239
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseActivity;->onResume()V

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 241
    const-string v1, "ModifySharingActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume in state "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    if-nez v0, :cond_50

    const-string v0, "NULL"

    :goto_1b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LeQ;

    invoke-virtual {v0, p0}, LeQ;->a(Ljava/lang/Object;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LTL;

    invoke-virtual {v0}, LTL;->d()V

    .line 245
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->f()V

    .line 246
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->o()V

    .line 247
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->j()V

    .line 248
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4f

    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    if-eqz v0, :cond_4f

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 250
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/os/Parcelable;

    .line 252
    :cond_4f
    return-void

    .line 241
    :cond_50
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    invoke-virtual {v0}, LUi;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1b
.end method

.method public onSaveClicked(Landroid/view/View;)V
    .registers 4
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 439
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 440
    sget-object v0, LUi;->b:LUi;

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:LUi;

    .line 441
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a(Landroid/view/View;)V

    .line 442
    invoke-direct {p0}, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->n()V

    .line 443
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 469
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_16

    .line 471
    const-string v0, "listViewState"

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/ModifySharingActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 473
    :cond_16
    return-void
.end method
