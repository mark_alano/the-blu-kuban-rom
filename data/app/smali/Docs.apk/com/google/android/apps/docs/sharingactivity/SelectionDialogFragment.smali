.class public abstract Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;
.super Lcom/google/android/apps/docs/app/BaseDialogFragment;
.source "SelectionDialogFragment.java"

# interfaces
.implements LUl;


# instance fields
.field private final a:LUj;

.field private a:[Ljava/lang/String;

.field private m:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;-><init>()V

    .line 28
    new-instance v0, LUj;

    invoke-direct {v0}, LUj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:LUj;

    return-void
.end method

.method public static a(Landroid/os/Bundle;I[Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    const-string v0, "titleId"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 50
    const-string v0, "choiceItems"

    invoke-virtual {p0, v0, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 51
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LnL;->a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->m:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    iget-object v2, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:LUj;

    iget-object v3, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:LdL;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v2, p0, v3, v4}, LUj;->a(LUl;LdL;Landroid/app/Activity;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x104

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:LUj;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v1, v2}, LUj;->a(Landroid/widget/ListView;)V

    .line 75
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "titleId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->m:I

    .line 58
    invoke-virtual {p0}, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "choiceItems"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:[Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:LUj;

    invoke-virtual {v0, p1}, LUj;->a(Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method public c(I)V
    .registers 3
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:LUj;

    invoke-virtual {v0, p1}, LUj;->a(I)V

    .line 92
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->c(Landroid/os/Bundle;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:LUj;

    invoke-virtual {v0, p1}, LUj;->b(Landroid/os/Bundle;)V

    .line 82
    return-void
.end method

.method public g()V
    .registers 2

    .prologue
    .line 86
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogFragment;->g()V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/docs/sharingactivity/SelectionDialogFragment;->a:LUj;

    invoke-virtual {v0}, LUj;->a()V

    .line 88
    return-void
.end method
