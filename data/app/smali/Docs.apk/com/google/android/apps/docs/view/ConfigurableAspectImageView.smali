.class public Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;
.super Landroid/widget/ImageView;
.source "ConfigurableAspectImageView.java"


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const v0, 0x3faa9fbe

    iput v0, p0, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;->a:F

    .line 33
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;->a:F

    invoke-static {v0, p1, p2}, Labr;->a(FII)Landroid/util/Pair;

    move-result-object v1

    .line 49
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;->setMeasuredDimension(II)V

    .line 50
    return-void
.end method

.method public setAspect(F)V
    .registers 2
    .parameter

    .prologue
    .line 39
    iput p1, p0, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;->a:F

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/ConfigurableAspectImageView;->requestLayout()V

    .line 41
    return-void
.end method
