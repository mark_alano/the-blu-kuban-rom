.class public Lcom/google/android/apps/docs/view/SpeakerNotesContent;
.super Landroid/widget/LinearLayout;
.source "SpeakerNotesContent.java"


# instance fields
.field public a:LQR;
    .annotation runtime Laon;
    .end annotation
.end field

.field private a:LQk;

.field private a:Lacm;

.field public final a:Landroid/content/Context;

.field private a:Landroid/view/View;

.field private a:Landroid/webkit/WebView;

.field private a:Landroid/widget/ImageView;

.field private a:Landroid/widget/TextView;

.field a:Ljava/lang/String;

.field b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 74
    iput-object p1, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/content/Context;

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a()V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 85
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    iput-object p1, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/content/Context;

    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a()V

    .line 88
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/SpeakerNotesContent;)Lacm;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Lacm;

    return-object v0
.end method

.method private a()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/content/Context;

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)Laoo;

    move-result-object v0

    invoke-interface {v0, p0}, Laoo;->a(Ljava/lang/Object;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/content/Context;

    sget v1, Lej;->speaker_note_content:I

    invoke-static {v0, v1, p0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 108
    sget v0, Leh;->speaker_notes:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    .line 109
    sget v0, Leh;->no_notes:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/widget/TextView;

    .line 110
    sget v0, Leh;->speaker_notes_close_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/widget/ImageView;

    .line 111
    sget v0, Leh;->speaker_notes_wrapper:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/view/View;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lee;->punch_grey_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    .line 114
    invoke-static {}, LZL;->b()Z

    move-result v0

    if-eqz v0, :cond_52

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/webkit/WebView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 118
    :cond_52
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 120
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setLightTouchEnabled(Z)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LZL;->d(Landroid/content/res/Resources;)Z

    move-result v1

    if-eqz v1, :cond_70

    .line 123
    invoke-static {}, LZL;->d()Z

    move-result v1

    if-eqz v1, :cond_81

    .line 124
    const/16 v1, 0x96

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    .line 130
    :cond_70
    :goto_70
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/widget/ImageView;

    new-instance v1, Lack;

    invoke-direct {v1, p0}, Lack;-><init>(Lcom/google/android/apps/docs/view/SpeakerNotesContent;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->c()V

    .line 142
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->b()V

    .line 143
    return-void

    .line 126
    :cond_81
    sget-object v1, Landroid/webkit/WebSettings$TextSize;->LARGER:Landroid/webkit/WebSettings$TextSize;

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setTextSize(Landroid/webkit/WebSettings$TextSize;)V

    goto :goto_70
.end method

.method public static synthetic a(Lcom/google/android/apps/docs/view/SpeakerNotesContent;)V
    .registers 1
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->b()V

    return-void
.end method

.method private b()V
    .registers 9

    .prologue
    const/4 v5, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQR;

    invoke-interface {v0}, LQR;->c()I

    move-result v0

    if-eqz v0, :cond_1a

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQR;

    invoke-interface {v0}, LQR;->d()I

    move-result v0

    .line 148
    iget-object v1, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQR;

    invoke-interface {v1, v0}, LQR;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Ljava/lang/String;

    .line 151
    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Ljava/lang/String;

    if-nez v0, :cond_36

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->b:Ljava/lang/String;

    if-eqz v0, :cond_2b

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    const-string v1, "about:blank"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 154
    iput-object v5, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->b:Ljava/lang/String;

    .line 157
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    :goto_35
    return-void

    .line 160
    :cond_36
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_60

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    const-string v1, "fake-url"

    iget-object v2, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->b:Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 167
    :cond_60
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_35
.end method

.method private c()V
    .registers 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQk;

    if-nez v0, :cond_17

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 181
    new-instance v0, Lacl;

    invoke-direct {v0, p0}, Lacl;-><init>(Lcom/google/android/apps/docs/view/SpeakerNotesContent;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQk;

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQR;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQk;

    invoke-interface {v0, v1}, LQR;->a(LQS;)V

    .line 199
    return-void

    .line 180
    :cond_17
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private d()V
    .registers 3

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQk;

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_5
    invoke-static {v0}, Lagu;->b(Z)V

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQR;

    iget-object v1, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:LQk;

    invoke-interface {v0, v1}, LQR;->b(LQS;)V

    .line 204
    return-void

    .line 202
    :cond_10
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .registers 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/content/Context;

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 175
    invoke-direct {p0}, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->d()V

    .line 176
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 177
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/content/Context;

    invoke-static {v0}, LdY;->a(Landroid/content/Context;)V

    .line 100
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 102
    return-void
.end method

.method public setListener(Lacm;)V
    .registers 2
    .parameter

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/apps/docs/view/SpeakerNotesContent;->a:Lacm;

    .line 95
    return-void
.end method
