.class public Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;
.super Lcom/google/android/apps/docs/app/BaseDialogActivity;
.source "WidgetConfigureActivity.java"

# interfaces
.implements Low;


# static fields
.field private static final a:I


# instance fields
.field public a:LME;
    .annotation runtime Laon;
    .end annotation
.end field

.field public a:LZM;
    .annotation runtime Laon;
    .end annotation
.end field

.field private final a:LacP;

.field public a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 37
    invoke-static {}, LacG;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:I

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;-><init>()V

    .line 46
    new-instance v0, LacP;

    invoke-direct {v0, p0}, LacP;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LacP;

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->b:I

    return-void
.end method

.method static a(Landroid/content/Context;[Landroid/accounts/Account;Ljava/lang/String;)Landroid/widget/RemoteViews;
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 101
    array-length v3, p1

    .line 102
    invoke-static {p1, p2}, LMG;->a([Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    .line 105
    if-gez v0, :cond_4a

    .line 106
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v3, Lej;->app_widget_broken:I

    invoke-direct {v0, v1, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 107
    if-nez p2, :cond_24

    .line 108
    sget v1, Leh;->widget_broken_title:I

    sget v2, Len;->widget_configuration_missing:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 137
    :goto_23
    return-object v0

    .line 111
    :cond_24
    sget v1, Len;->widget_account_missing:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 112
    new-array v3, v7, [Ljava/lang/Object;

    aput-object p2, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 113
    sget v3, Leh;->widget_broken_title:I

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 114
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.settings.SYNC_SETTINGS"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 115
    sget v3, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:I

    invoke-static {p0, v3, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 117
    sget v2, Leh;->widget_broken_title:I

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto :goto_23

    .line 120
    :cond_4a
    if-nez p2, :cond_54

    .line 121
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "null accountName"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 123
    :cond_54
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v4, Lej;->app_widget:I

    invoke-direct {v0, v1, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 124
    invoke-static {}, LacG;->values()[LacG;

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_65
    if-ge v1, v5, :cond_6f

    aget-object v6, v4, v1

    .line 125
    invoke-virtual {v6, v0, p0, p2}, LacG;->a(Landroid/widget/RemoteViews;Landroid/content/Context;Ljava/lang/String;)V

    .line 124
    add-int/lit8 v1, v1, 0x1

    goto :goto_65

    .line 127
    :cond_6f
    if-le v3, v7, :cond_81

    .line 128
    sget v1, Leh;->widget_account_name:I

    invoke-virtual {v0, v1, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 129
    sget v1, Leh;->widget_app_title_only:I

    invoke-virtual {v0, v1, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 130
    sget v1, Leh;->widget_app_title_account_container:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_23

    .line 132
    :cond_81
    sget v1, Leh;->widget_account_name:I

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 133
    sget v1, Leh;->widget_app_title_only:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 134
    sget v1, Leh;->widget_app_title_account_container:I

    invoke-virtual {v0, v1, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_23
.end method

.method public static a(Landroid/appwidget/AppWidgetManager;ILandroid/content/Context;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 142
    const-string v0, "WidgetConfigure"

    const-string v1, "Configuring AppWidget %d [%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-static {p2}, LZn;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 144
    invoke-static {p2, v0, p3}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a(Landroid/content/Context;[Landroid/accounts/Account;Ljava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 145
    invoke-virtual {p0, p1, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 146
    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-virtual {p0, v1}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->setResult(I)V

    .line 85
    const-string v0, "appWidgetId"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 88
    if-nez v0, :cond_10

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->finish()V

    .line 94
    :goto_f
    return-void

    .line 91
    :cond_10
    iput v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->b:I

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a()Lo;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PickAccountDialogFragment;->a(Lo;)V

    goto :goto_f
.end method


# virtual methods
.method public a(Landroid/accounts/Account;)V
    .registers 5
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LeQ;

    const-string v1, "widget"

    const-string v2, "addWidgetInstance"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 152
    iget v1, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->b:I

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LacP;

    invoke-virtual {v2, v1, v0}, LacP;->a(ILjava/lang/String;)V

    .line 155
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    invoke-static {v2, v1, p0, v0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a(Landroid/appwidget/AppWidgetManager;ILandroid/content/Context;Ljava/lang/String;)V

    .line 156
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 157
    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 158
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->setResult(ILandroid/content/Intent;)V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->finish()V

    .line 160
    return-void
.end method

.method public l_()V
    .registers 2

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->setResult(I)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->finish()V

    .line 166
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->a()V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a(Landroid/content/Intent;)V

    .line 53
    return-void
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LeQ;

    invoke-virtual {v0}, LeQ;->b()V

    .line 58
    invoke-super {p0}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onDestroy()V

    .line 59
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 3
    .parameter

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a:LdL;

    invoke-interface {v0, p0}, LdL;->a(Landroid/content/Context;)V

    .line 65
    invoke-virtual {p0, p1}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->setIntent(Landroid/content/Intent;)V

    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->a(Landroid/content/Intent;)V

    .line 67
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 72
    const-string v0, "appWidgetId"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->b:I

    .line 74
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/google/android/apps/docs/app/BaseDialogActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 79
    const-string v0, "appWidgetId"

    iget v1, p0, Lcom/google/android/apps/docs/widget/WidgetConfigureActivity;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    return-void
.end method
