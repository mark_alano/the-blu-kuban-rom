.class public Lcom/google/api/services/discussions/Discussions$Authors$Get;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "authors/me"


# instance fields
.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$Authors;


# direct methods
.method constructor <init>(Lcom/google/api/services/discussions/Discussions$Authors;)V
    .registers 6
    .parameter

    .prologue
    .line 1623
    iput-object p1, p0, Lcom/google/api/services/discussions/Discussions$Authors$Get;->this$1:Lcom/google/api/services/discussions/Discussions$Authors;

    .line 1624
    iget-object v0, p1, Lcom/google/api/services/discussions/Discussions$Authors;->this$0:Lcom/google/api/services/discussions/Discussions;

    sget-object v1, Laeh;->b:Laeh;

    const-string v2, "authors/me"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/api/services/discussions/DiscussionsRequest;-><init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1625
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Author;
    .registers 3

    .prologue
    .line 1634
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Authors$Get;->a()Laen;

    move-result-object v1

    .line 1635
    const-class v0, Lcom/google/api/services/discussions/model/Author;

    invoke-virtual {v1, v0}, Laen;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Author;

    .line 1637
    invoke-virtual {v1}, Laen;->a()Laee;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Author;->a(Laee;)V

    .line 1638
    return-object v0
.end method
