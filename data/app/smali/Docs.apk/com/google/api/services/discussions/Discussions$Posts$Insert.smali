.class public Lcom/google/api/services/discussions/Discussions$Posts$Insert;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "targets/{targetId}/discussions/{discussionId}"


# instance fields
.field private discussionId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private targetId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$Posts;


# direct methods
.method constructor <init>(Lcom/google/api/services/discussions/Discussions$Posts;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/discussions/model/Post;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 777
    iput-object p1, p0, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->this$1:Lcom/google/api/services/discussions/Discussions$Posts;

    .line 778
    iget-object v0, p1, Lcom/google/api/services/discussions/Discussions$Posts;->this$0:Lcom/google/api/services/discussions/Discussions;

    sget-object v1, Laeh;->f:Laeh;

    const-string v2, "targets/{targetId}/discussions/{discussionId}"

    invoke-direct {p0, v0, v1, v2, p4}, Lcom/google/api/services/discussions/DiscussionsRequest;-><init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V

    .line 779
    const-string v0, "Required parameter targetId must be specified."

    invoke-static {p2, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->targetId:Ljava/lang/String;

    .line 780
    const-string v0, "Required parameter discussionId must be specified."

    invoke-static {p3, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->discussionId:Ljava/lang/String;

    .line 781
    invoke-static {p4}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 782
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Post;
    .registers 3

    .prologue
    .line 791
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->a()Laen;

    move-result-object v1

    .line 792
    const-class v0, Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v1, v0}, Laen;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Post;

    .line 794
    invoke-virtual {v1}, Laen;->a()Laee;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->a(Laee;)V

    .line 795
    return-object v0
.end method
