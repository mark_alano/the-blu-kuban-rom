.class public Lcom/google/api/services/discussions/Discussions$Posts$Remove;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "targets/{targetId}/discussions/{discussionId}/posts/{postId}"


# instance fields
.field private discussionId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private postId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private targetId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$Posts;


# direct methods
.method constructor <init>(Lcom/google/api/services/discussions/Discussions$Posts;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1333
    iput-object p1, p0, Lcom/google/api/services/discussions/Discussions$Posts$Remove;->this$1:Lcom/google/api/services/discussions/Discussions$Posts;

    .line 1334
    iget-object v0, p1, Lcom/google/api/services/discussions/Discussions$Posts;->this$0:Lcom/google/api/services/discussions/Discussions;

    sget-object v1, Laeh;->a:Laeh;

    const-string v2, "targets/{targetId}/discussions/{discussionId}/posts/{postId}"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/api/services/discussions/DiscussionsRequest;-><init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1335
    const-string v0, "Required parameter targetId must be specified."

    invoke-static {p2, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$Posts$Remove;->targetId:Ljava/lang/String;

    .line 1336
    const-string v0, "Required parameter discussionId must be specified."

    invoke-static {p3, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$Posts$Remove;->discussionId:Ljava/lang/String;

    .line 1337
    const-string v0, "Required parameter postId must be specified."

    invoke-static {p4, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$Posts$Remove;->postId:Ljava/lang/String;

    .line 1338
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 1346
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Posts$Remove;->a()Laen;

    move-result-object v0

    .line 1347
    invoke-virtual {v0}, Laen;->a()V

    .line 1348
    return-void
.end method
