.class public Lcom/google/api/services/discussions/Discussions$Posts$Update;
.super Lcom/google/api/services/discussions/DiscussionsRequest;
.source "Discussions.java"


# static fields
.field private static final REST_PATH:Ljava/lang/String; = "targets/{targetId}/discussions/{discussionId}/posts/{postId}"


# instance fields
.field private discussionId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private postId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private targetId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field final synthetic this$1:Lcom/google/api/services/discussions/Discussions$Posts;


# direct methods
.method constructor <init>(Lcom/google/api/services/discussions/Discussions$Posts;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/discussions/model/Post;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1195
    iput-object p1, p0, Lcom/google/api/services/discussions/Discussions$Posts$Update;->this$1:Lcom/google/api/services/discussions/Discussions$Posts;

    .line 1196
    iget-object v0, p1, Lcom/google/api/services/discussions/Discussions$Posts;->this$0:Lcom/google/api/services/discussions/Discussions;

    sget-object v1, Laeh;->e:Laeh;

    const-string v2, "targets/{targetId}/discussions/{discussionId}/posts/{postId}"

    invoke-direct {p0, v0, v1, v2, p5}, Lcom/google/api/services/discussions/DiscussionsRequest;-><init>(LaeG;Laeh;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1197
    const-string v0, "Required parameter targetId must be specified."

    invoke-static {p2, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$Posts$Update;->targetId:Ljava/lang/String;

    .line 1198
    const-string v0, "Required parameter discussionId must be specified."

    invoke-static {p3, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$Posts$Update;->discussionId:Ljava/lang/String;

    .line 1199
    const-string v0, "Required parameter postId must be specified."

    invoke-static {p4, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/api/services/discussions/Discussions$Posts$Update;->postId:Ljava/lang/String;

    .line 1200
    invoke-static {p5}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1201
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Post;
    .registers 3

    .prologue
    .line 1210
    invoke-virtual {p0}, Lcom/google/api/services/discussions/Discussions$Posts$Update;->a()Laen;

    move-result-object v1

    .line 1211
    const-class v0, Lcom/google/api/services/discussions/model/Post;

    invoke-virtual {v1, v0}, Laen;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Post;

    .line 1213
    invoke-virtual {v1}, Laen;->a()Laee;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->a(Laee;)V

    .line 1214
    return-object v0
.end method
