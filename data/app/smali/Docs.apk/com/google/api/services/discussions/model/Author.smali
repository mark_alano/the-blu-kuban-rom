.class public final Lcom/google/api/services/discussions/model/Author;
.super LaeN;
.source "Author.java"


# instance fields
.field private displayName:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private image:Lcom/google/api/services/discussions/model/Author$Image;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private responseHeaders:Laee;

.field private url:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 31
    invoke-direct {p0}, LaeN;-><init>()V

    .line 178
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Author$Image;
    .registers 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Author;->image:Lcom/google/api/services/discussions/model/Author$Image;

    return-object v0
.end method

.method public a(Laee;)V
    .registers 2
    .parameter

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Author;->responseHeaders:Laee;

    .line 165
    return-void
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Author;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Author;->id:Ljava/lang/String;

    return-object v0
.end method
