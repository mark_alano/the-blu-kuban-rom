.class public final Lcom/google/api/services/discussions/model/Discussion$Actor;
.super LaeN;
.source "Discussion.java"


# instance fields
.field private displayName:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private image:Lcom/google/api/services/discussions/model/Discussion$Actor$Image;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private url:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 557
    invoke-direct {p0}, LaeN;-><init>()V

    .line 658
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Discussion$Actor$Image;
    .registers 2

    .prologue
    .line 609
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion$Actor;->image:Lcom/google/api/services/discussions/model/Discussion$Actor$Image;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion$Actor;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 643
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion$Actor;->id:Ljava/lang/String;

    return-object v0
.end method
