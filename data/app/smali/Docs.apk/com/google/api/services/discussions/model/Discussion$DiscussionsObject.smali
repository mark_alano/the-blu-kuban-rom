.class public final Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
.super LaeN;
.source "Discussion.java"


# instance fields
.field private anchorId:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private content:Lcom/google/api/services/discussions/model/MimedcontentJson;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private context:Lcom/google/api/services/discussions/model/MimedcontentJson;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private deleted:Ljava/lang/Boolean;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private objectType:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private originalContent:Lcom/google/api/services/discussions/model/MimedcontentJson;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private replies:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject$Replies;
    .annotation runtime Lafu;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 274
    invoke-direct {p0}, LaeN;-><init>()V

    .line 465
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject$Replies;
    .registers 2

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->replies:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject$Replies;

    return-object v0
.end method

.method public a(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .registers 2
    .parameter

    .prologue
    .line 363
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->originalContent:Lcom/google/api/services/discussions/model/MimedcontentJson;

    .line 364
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .registers 2
    .parameter

    .prologue
    .line 380
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->deleted:Ljava/lang/Boolean;

    .line 381
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .registers 2
    .parameter

    .prologue
    .line 344
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->anchorId:Ljava/lang/String;

    .line 345
    return-object p0
.end method

.method public a()Lcom/google/api/services/discussions/model/MimedcontentJson;
    .registers 2

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->originalContent:Lcom/google/api/services/discussions/model/MimedcontentJson;

    return-object v0
.end method

.method public a()Ljava/lang/Boolean;
    .registers 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->deleted:Ljava/lang/Boolean;

    return-object v0
.end method

.method public b(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .registers 2
    .parameter

    .prologue
    .line 401
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->content:Lcom/google/api/services/discussions/model/MimedcontentJson;

    .line 402
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .registers 2
    .parameter

    .prologue
    .line 458
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->objectType:Ljava/lang/String;

    .line 459
    return-object p0
.end method

.method public b()Lcom/google/api/services/discussions/model/MimedcontentJson;
    .registers 2

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->content:Lcom/google/api/services/discussions/model/MimedcontentJson;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->anchorId:Ljava/lang/String;

    return-object v0
.end method

.method public c(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .registers 2
    .parameter

    .prologue
    .line 424
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->context:Lcom/google/api/services/discussions/model/MimedcontentJson;

    .line 425
    return-object p0
.end method

.method public c()Lcom/google/api/services/discussions/model/MimedcontentJson;
    .registers 2

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->context:Lcom/google/api/services/discussions/model/MimedcontentJson;

    return-object v0
.end method
