.class public final Lcom/google/api/services/discussions/model/Discussion;
.super LaeN;
.source "Discussion.java"


# instance fields
.field private actor:Lcom/google/api/services/discussions/model/Discussion$Actor;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private discussionsObject:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .annotation runtime Lafu;
        a = "object"
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private labels:Ljava/util/List;
    .annotation runtime Lafu;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private published:Lafo;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private responseHeaders:Laee;

.field private target:Lcom/google/api/services/discussions/model/Discussion$Target;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private updated:Lafo;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private verb:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, LaeN;-><init>()V

    .line 557
    return-void
.end method


# virtual methods
.method public a()Lafo;
    .registers 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion;->updated:Lafo;

    return-object v0
.end method

.method public a()Lcom/google/api/services/discussions/model/Discussion$Actor;
    .registers 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion;->actor:Lcom/google/api/services/discussions/model/Discussion$Actor;

    return-object v0
.end method

.method public a()Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;
    .registers 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion;->discussionsObject:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    return-object v0
.end method

.method public a(Lafo;)Lcom/google/api/services/discussions/model/Discussion;
    .registers 2
    .parameter

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion;->updated:Lafo;

    .line 178
    return-object p0
.end method

.method public a(Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;)Lcom/google/api/services/discussions/model/Discussion;
    .registers 2
    .parameter

    .prologue
    .line 126
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion;->discussionsObject:Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    .line 127
    return-object p0
.end method

.method public a(Lcom/google/api/services/discussions/model/Discussion$Target;)Lcom/google/api/services/discussions/model/Discussion;
    .registers 2
    .parameter

    .prologue
    .line 143
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion;->target:Lcom/google/api/services/discussions/model/Discussion$Target;

    .line 144
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Discussion;
    .registers 2
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion;->kind:Ljava/lang/String;

    .line 110
    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/google/api/services/discussions/model/Discussion;
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/api/services/discussions/model/Discussion;"
        }
    .end annotation

    .prologue
    .line 160
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion;->labels:Ljava/util/List;

    .line 161
    return-object p0
.end method

.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion;->labels:Ljava/util/List;

    return-object v0
.end method

.method public a(Laee;)V
    .registers 2
    .parameter

    .prologue
    .line 260
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion;->responseHeaders:Laee;

    .line 261
    return-void
.end method

.method public b()Lafo;
    .registers 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion;->published:Lafo;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Discussion;
    .registers 2
    .parameter

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Discussion;->verb:Ljava/lang/String;

    .line 212
    return-object p0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Discussion;->id:Ljava/lang/String;

    return-object v0
.end method
