.class public final Lcom/google/api/services/discussions/model/Post$DiscussionsObject;
.super LaeN;
.source "Post.java"


# instance fields
.field private content:Lcom/google/api/services/discussions/model/MimedcontentJson;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private objectType:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private originalContent:Lcom/google/api/services/discussions/model/MimedcontentJson;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private replies:Lcom/google/api/services/discussions/model/Post$DiscussionsObject$Replies;
    .annotation runtime Lafu;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 339
    invoke-direct {p0}, LaeN;-><init>()V

    .line 449
    return-void
.end method


# virtual methods
.method public a()Lcom/google/api/services/discussions/model/MimedcontentJson;
    .registers 2

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->content:Lcom/google/api/services/discussions/model/MimedcontentJson;

    return-object v0
.end method

.method public a(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Post$DiscussionsObject;
    .registers 2
    .parameter

    .prologue
    .line 389
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->content:Lcom/google/api/services/discussions/model/MimedcontentJson;

    .line 390
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post$DiscussionsObject;
    .registers 2
    .parameter

    .prologue
    .line 442
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->objectType:Ljava/lang/String;

    .line 443
    return-object p0
.end method

.method public b()Lcom/google/api/services/discussions/model/MimedcontentJson;
    .registers 2

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->originalContent:Lcom/google/api/services/discussions/model/MimedcontentJson;

    return-object v0
.end method

.method public b(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Post$DiscussionsObject;
    .registers 2
    .parameter

    .prologue
    .line 408
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->originalContent:Lcom/google/api/services/discussions/model/MimedcontentJson;

    .line 409
    return-object p0
.end method
