.class public final Lcom/google/api/services/discussions/model/Post;
.super LaeN;
.source "Post.java"


# instance fields
.field private action:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private actor:Lcom/google/api/services/discussions/model/Author;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private deleted:Ljava/lang/Boolean;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private discussionsObject:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;
    .annotation runtime Lafu;
        a = "object"
    .end annotation
.end field

.field private id:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private published:Lafo;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private responseHeaders:Laee;

.field private target:Lcom/google/api/services/discussions/model/Post$Target;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private updated:Lafo;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private verb:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 32
    invoke-direct {p0}, LaeN;-><init>()V

    .line 339
    return-void
.end method


# virtual methods
.method public a()Lafo;
    .registers 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Post;->updated:Lafo;

    return-object v0
.end method

.method public a()Lcom/google/api/services/discussions/model/Author;
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Post;->actor:Lcom/google/api/services/discussions/model/Author;

    return-object v0
.end method

.method public a()Lcom/google/api/services/discussions/model/Post$DiscussionsObject;
    .registers 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Post;->discussionsObject:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    return-object v0
.end method

.method public a(Lcom/google/api/services/discussions/model/Post$DiscussionsObject;)Lcom/google/api/services/discussions/model/Post;
    .registers 2
    .parameter

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post;->discussionsObject:Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    .line 172
    return-object p0
.end method

.method public a(Lcom/google/api/services/discussions/model/Post$Target;)Lcom/google/api/services/discussions/model/Post;
    .registers 2
    .parameter

    .prologue
    .line 137
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post;->target:Lcom/google/api/services/discussions/model/Post$Target;

    .line 138
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lcom/google/api/services/discussions/model/Post;
    .registers 2
    .parameter

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post;->deleted:Ljava/lang/Boolean;

    .line 155
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;
    .registers 2
    .parameter

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post;->kind:Ljava/lang/String;

    .line 121
    return-object p0
.end method

.method public a()Ljava/lang/Boolean;
    .registers 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Post;->deleted:Ljava/lang/Boolean;

    return-object v0
.end method

.method public a(Laee;)V
    .registers 2
    .parameter

    .prologue
    .line 294
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post;->responseHeaders:Laee;

    .line 295
    return-void
.end method

.method public b()Lafo;
    .registers 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Post;->published:Lafo;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;
    .registers 2
    .parameter

    .prologue
    .line 224
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post;->verb:Ljava/lang/String;

    .line 225
    return-object p0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Post;->action:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;
    .registers 2
    .parameter

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post;->action:Ljava/lang/String;

    .line 263
    return-object p0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/api/services/discussions/model/Post;->id:Ljava/lang/String;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;
    .registers 2
    .parameter

    .prologue
    .line 279
    iput-object p1, p0, Lcom/google/api/services/discussions/model/Post;->id:Ljava/lang/String;

    .line 280
    return-object p0
.end method
