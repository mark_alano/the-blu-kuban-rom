.class public final Lcom/google/api/services/discussions/model/PostFeed;
.super LaeN;
.source "PostFeed.java"


# instance fields
.field private id:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private items:Ljava/util/List;
    .annotation runtime Lafu;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/discussions/model/Post;",
            ">;"
        }
    .end annotation
.end field

.field private kind:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private nextPageToken:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field

.field private responseHeaders:Laee;

.field private title:Ljava/lang/String;
    .annotation runtime Lafu;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 50
    const-class v0, Lcom/google/api/services/discussions/model/Post;

    invoke-static {v0}, Lafj;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 51
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 31
    invoke-direct {p0}, LaeN;-><init>()V

    return-void
.end method
