.class public Lct;
.super Ljava/lang/Object;
.source "GAServiceManager.java"

# interfaces
.implements Ldf;


# static fields
.field private static a:Lct;

.field private static final a:Ljava/lang/Object;


# instance fields
.field private a:I

.field private a:Landroid/content/Context;

.field private a:Landroid/os/Handler;

.field private a:Lcg;

.field private a:Lch;

.field private volatile a:Lci;

.field private a:Z

.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lct;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/16 v0, 0x708

    iput v0, p0, Lct;->a:I

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lct;->a:Z

    .line 42
    new-instance v0, Lcu;

    invoke-direct {v0, p0}, Lcu;-><init>(Lct;)V

    iput-object v0, p0, Lct;->a:Lch;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lct;->b:Z

    .line 64
    return-void
.end method

.method static synthetic a(Lct;)I
    .registers 2
    .parameter

    .prologue
    .line 20
    iget v0, p0, Lct;->a:I

    return v0
.end method

.method static synthetic a(Lct;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 20
    iget-object v0, p0, Lct;->a:Landroid/os/Handler;

    return-object v0
.end method

.method public static a()Lct;
    .registers 1

    .prologue
    .line 57
    sget-object v0, Lct;->a:Lct;

    if-nez v0, :cond_b

    .line 58
    new-instance v0, Lct;

    invoke-direct {v0}, Lct;-><init>()V

    sput-object v0, Lct;->a:Lct;

    .line 60
    :cond_b
    sget-object v0, Lct;->a:Lct;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 20
    sget-object v0, Lct;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lct;)Z
    .registers 2
    .parameter

    .prologue
    .line 20
    iget-boolean v0, p0, Lct;->b:Z

    return v0
.end method

.method private b()V
    .registers 5

    .prologue
    .line 76
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lct;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcv;

    invoke-direct {v2, p0}, Lcv;-><init>(Lct;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lct;->a:Landroid/os/Handler;

    .line 92
    iget v0, p0, Lct;->a:I

    if-lez v0, :cond_29

    .line 93
    iget-object v0, p0, Lct;->a:Landroid/os/Handler;

    iget-object v1, p0, Lct;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lct;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lct;->a:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 96
    :cond_29
    return-void
.end method


# virtual methods
.method declared-synchronized a()Lcg;
    .registers 4

    .prologue
    .line 139
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lct;->a:Lcg;

    if-nez v0, :cond_1f

    .line 140
    iget-object v0, p0, Lct;->a:Landroid/content/Context;

    if-nez v0, :cond_14

    .line 143
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cant get a store unless we have a context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_11

    .line 139
    :catchall_11
    move-exception v0

    monitor-exit p0

    throw v0

    .line 145
    :cond_14
    :try_start_14
    new-instance v0, Ldc;

    iget-object v1, p0, Lct;->a:Lch;

    iget-object v2, p0, Lct;->a:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Ldc;-><init>(Lch;Landroid/content/Context;)V

    iput-object v0, p0, Lct;->a:Lcg;

    .line 147
    :cond_1f
    iget-object v0, p0, Lct;->a:Landroid/os/Handler;

    if-nez v0, :cond_26

    .line 149
    invoke-direct {p0}, Lct;->b()V

    .line 151
    :cond_26
    iget-object v0, p0, Lct;->a:Lcg;
    :try_end_28
    .catchall {:try_start_14 .. :try_end_28} :catchall_11

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized a()V
    .registers 2

    .prologue
    .line 158
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lct;->a:Lci;

    if-nez v0, :cond_f

    .line 159
    const-string v0, "dispatch call queued.  Need to call GAServiceManager.getInstance().initializea()."

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    .line 160
    const/4 v0, 0x1

    iput-boolean v0, p0, Lct;->a:Z
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_15

    .line 165
    :goto_d
    monitor-exit p0

    return-void

    .line 164
    :cond_f
    :try_start_f
    iget-object v0, p0, Lct;->a:Lci;

    invoke-interface {v0}, Lci;->a()V
    :try_end_14
    .catchall {:try_start_f .. :try_end_14} :catchall_15

    goto :goto_d

    .line 158
    :catchall_15
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .registers 6
    .parameter

    .prologue
    .line 169
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lct;->a:Landroid/os/Handler;

    if-nez v0, :cond_e

    .line 170
    const-string v0, "Need to call initializea() and be in fallback mode to start dispatch."

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    .line 171
    iput p1, p0, Lct;->a:I
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_38

    .line 182
    :cond_c
    :goto_c
    monitor-exit p0

    return-void

    .line 174
    :cond_e
    :try_start_e
    iget-boolean v0, p0, Lct;->b:Z

    if-nez v0, :cond_1e

    iget v0, p0, Lct;->a:I

    if-lez v0, :cond_1e

    .line 175
    iget-object v0, p0, Lct;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lct;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 177
    :cond_1e
    iput p1, p0, Lct;->a:I

    .line 178
    if-lez p1, :cond_c

    iget-boolean v0, p0, Lct;->b:Z

    if-nez v0, :cond_c

    .line 179
    iget-object v0, p0, Lct;->a:Landroid/os/Handler;

    iget-object v1, p0, Lct;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lct;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    mul-int/lit16 v2, p1, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_37
    .catchall {:try_start_e .. :try_end_37} :catchall_38

    goto :goto_c

    .line 169
    :catchall_38
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Landroid/content/Context;Lci;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 107
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lct;->a:Landroid/content/Context;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_1b

    if-eqz v0, :cond_7

    .line 118
    :cond_5
    :goto_5
    monitor-exit p0

    return-void

    .line 110
    :cond_7
    :try_start_7
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lct;->a:Landroid/content/Context;

    .line 112
    iget-object v0, p0, Lct;->a:Lci;

    if-nez v0, :cond_5

    .line 113
    iput-object p2, p0, Lct;->a:Lci;

    .line 114
    iget-boolean v0, p0, Lct;->a:Z

    if-eqz v0, :cond_5

    .line 115
    invoke-interface {p2}, Lci;->a()V
    :try_end_1a
    .catchall {:try_start_7 .. :try_end_1a} :catchall_1b

    goto :goto_5

    .line 107
    :catchall_1b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Z)V
    .registers 6
    .parameter

    .prologue
    .line 188
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lct;->b:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_4b

    if-ne v0, p1, :cond_7

    .line 200
    :goto_5
    monitor-exit p0

    return-void

    .line 191
    :cond_7
    if-eqz p1, :cond_15

    :try_start_9
    iget v0, p0, Lct;->a:I

    if-lez v0, :cond_15

    .line 192
    iget-object v0, p0, Lct;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lct;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 194
    :cond_15
    if-nez p1, :cond_2e

    iget v0, p0, Lct;->a:I

    if-lez v0, :cond_2e

    .line 195
    iget-object v0, p0, Lct;->a:Landroid/os/Handler;

    iget-object v1, p0, Lct;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lct;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lct;->a:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 198
    :cond_2e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PowerSaveMode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_4e

    const-string v0, "initiated."

    :goto_3d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LcT;->e(Ljava/lang/String;)I

    .line 199
    iput-boolean p1, p0, Lct;->b:Z
    :try_end_4a
    .catchall {:try_start_9 .. :try_end_4a} :catchall_4b

    goto :goto_5

    .line 188
    :catchall_4b
    move-exception v0

    monitor-exit p0

    throw v0

    .line 198
    :cond_4e
    :try_start_4e
    const-string v0, "terminated."
    :try_end_50
    .catchall {:try_start_4e .. :try_end_50} :catchall_4b

    goto :goto_3d
.end method
