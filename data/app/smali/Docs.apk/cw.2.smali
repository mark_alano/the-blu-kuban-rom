.class Lcw;
.super Ljava/lang/Object;
.source "GAServiceProxy.java"

# interfaces
.implements Lce;
.implements Lcf;
.implements Ldg;


# instance fields
.field private volatile a:I

.field private volatile a:J

.field private final a:Landroid/content/Context;

.field private volatile a:LcA;

.field private volatile a:Lcb;

.field private a:Lcg;

.field private final a:Lci;

.field private a:Lck;

.field private final a:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LcD;",
            ">;"
        }
    .end annotation
.end field

.field private volatile a:Ljava/util/Timer;

.field private a:Z

.field private b:J

.field private b:Lcg;

.field private volatile b:Ljava/util/Timer;

.field private b:Z

.field private volatile c:Ljava/util/Timer;


# direct methods
.method constructor <init>(Landroid/content/Context;Lci;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcw;-><init>(Landroid/content/Context;Lci;Lcg;)V

    .line 83
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lci;Lcg;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcw;->a:Ljava/util/Queue;

    .line 64
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lcw;->b:J

    .line 68
    iput-object p3, p0, Lcw;->b:Lcg;

    .line 69
    iput-object p1, p0, Lcw;->a:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcw;->a:Lci;

    .line 71
    new-instance v0, Lcx;

    invoke-direct {v0, p0}, Lcx;-><init>(Lcw;)V

    iput-object v0, p0, Lcw;->a:Lck;

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcw;->a:I

    .line 78
    sget-object v0, LcA;->g:LcA;

    iput-object v0, p0, Lcw;->a:LcA;

    .line 79
    return-void
.end method

.method static synthetic a(Lcw;)J
    .registers 3
    .parameter

    .prologue
    .line 27
    iget-wide v0, p0, Lcw;->a:J

    return-wide v0
.end method

.method static synthetic a(Lcw;)LcA;
    .registers 2
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcw;->a:LcA;

    return-object v0
.end method

.method static synthetic a(Lcw;)Lck;
    .registers 2
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcw;->a:Lck;

    return-object v0
.end method

.method static synthetic a(Lcw;)Ljava/util/Queue;
    .registers 2
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcw;->a:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic a(Lcw;)Ljava/util/Timer;
    .registers 2
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcw;->c:Ljava/util/Timer;

    return-object v0
.end method

.method private a(Ljava/util/Timer;)Ljava/util/Timer;
    .registers 3
    .parameter

    .prologue
    .line 141
    if-eqz p1, :cond_5

    .line 142
    invoke-virtual {p1}, Ljava/util/Timer;->cancel()V

    .line 144
    :cond_5
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic a(Lcw;)V
    .registers 1
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Lcw;->g()V

    return-void
.end method

.method static synthetic b(Lcw;)J
    .registers 3
    .parameter

    .prologue
    .line 27
    iget-wide v0, p0, Lcw;->b:J

    return-wide v0
.end method

.method static synthetic b(Lcw;)V
    .registers 1
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Lcw;->i()V

    return-void
.end method

.method static synthetic c(Lcw;)V
    .registers 1
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Lcw;->j()V

    return-void
.end method

.method static synthetic d(Lcw;)V
    .registers 1
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Lcw;->k()V

    return-void
.end method

.method private f()V
    .registers 2

    .prologue
    .line 148
    iget-object v0, p0, Lcw;->a:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lcw;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lcw;->a:Ljava/util/Timer;

    .line 149
    iget-object v0, p0, Lcw;->b:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lcw;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lcw;->b:Ljava/util/Timer;

    .line 150
    iget-object v0, p0, Lcw;->c:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lcw;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lcw;->c:Ljava/util/Timer;

    .line 151
    return-void
.end method

.method private declared-synchronized g()V
    .registers 8

    .prologue
    .line 194
    monitor-enter p0

    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    iget-object v2, p0, Lcw;->a:Lci;

    invoke-interface {v2}, Lci;->a()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_21

    .line 195
    iget-object v1, p0, Lcw;->a:Lci;

    invoke-interface {v1}, Lci;->a()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v1

    new-instance v2, Lcy;

    invoke-direct {v2, p0}, Lcy;-><init>(Lcw;)V

    invoke-virtual {v1, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_1f
    .catchall {:try_start_1 .. :try_end_1f} :catchall_63

    .line 239
    :cond_1f
    :goto_1f
    monitor-exit p0

    return-void

    .line 203
    :cond_21
    :try_start_21
    iget-boolean v1, p0, Lcw;->b:Z

    if-eqz v1, :cond_28

    .line 204
    invoke-virtual {p0}, Lcw;->d()V

    .line 206
    :cond_28
    sget-object v1, Lcz;->a:[I

    iget-object v2, p0, Lcw;->a:LcA;

    invoke-virtual {v2}, LcA;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_bc

    goto :goto_1f

    .line 208
    :goto_36
    :pswitch_36
    iget-object v1, p0, Lcw;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_66

    .line 209
    iget-object v1, p0, Lcw;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LcD;

    move-object v6, v0

    .line 210
    const-string v1, "Sending hit to store"

    invoke-static {v1}, LcT;->e(Ljava/lang/String;)I

    .line 211
    iget-object v1, p0, Lcw;->a:Lcg;

    invoke-virtual {v6}, LcD;->a()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v6}, LcD;->a()J

    move-result-wide v3

    invoke-virtual {v6}, LcD;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, LcD;->a()Ljava/util/List;

    move-result-object v6

    invoke-interface/range {v1 .. v6}, Lcg;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/Collection;)V
    :try_end_62
    .catchall {:try_start_21 .. :try_end_62} :catchall_63

    goto :goto_36

    .line 194
    :catchall_63
    move-exception v1

    monitor-exit p0

    throw v1

    .line 215
    :cond_66
    :try_start_66
    iget-boolean v1, p0, Lcw;->a:Z

    if-eqz v1, :cond_1f

    .line 216
    invoke-direct {p0}, Lcw;->h()V

    goto :goto_1f

    .line 220
    :goto_6e
    :pswitch_6e
    iget-object v1, p0, Lcw;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a0

    .line 221
    iget-object v1, p0, Lcw;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LcD;

    move-object v6, v0

    .line 222
    const-string v1, "Sending hit to service"

    invoke-static {v1}, LcT;->e(Ljava/lang/String;)I

    .line 223
    iget-object v1, p0, Lcw;->a:Lcb;

    invoke-virtual {v6}, LcD;->a()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v6}, LcD;->a()J

    move-result-wide v3

    invoke-virtual {v6}, LcD;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, LcD;->a()Ljava/util/List;

    move-result-object v6

    invoke-interface/range {v1 .. v6}, Lcb;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    .line 225
    iget-object v1, p0, Lcw;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_6e

    .line 227
    :cond_a0
    iget-object v1, p0, Lcw;->a:Lck;

    invoke-interface {v1}, Lck;->a()J

    move-result-wide v1

    iput-wide v1, p0, Lcw;->a:J

    goto/16 :goto_1f

    .line 230
    :pswitch_aa
    const-string v1, "Need to reconnect"

    invoke-static {v1}, LcT;->e(Ljava/lang/String;)I

    .line 231
    iget-object v1, p0, Lcw;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1f

    .line 232
    invoke-direct {p0}, Lcw;->j()V
    :try_end_ba
    .catchall {:try_start_66 .. :try_end_ba} :catchall_63

    goto/16 :goto_1f

    .line 206
    :pswitch_data_bc
    .packed-switch 0x1
        :pswitch_36
        :pswitch_6e
        :pswitch_aa
    .end packed-switch
.end method

.method private h()V
    .registers 2

    .prologue
    .line 245
    iget-object v0, p0, Lcw;->a:Lcg;

    invoke-interface {v0}, Lcg;->b()V

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcw;->a:Z

    .line 247
    return-void
.end method

.method private declared-synchronized i()V
    .registers 4

    .prologue
    .line 253
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcw;->a:LcA;

    sget-object v1, LcA;->c:LcA;
    :try_end_5
    .catchall {:try_start_1 .. :try_end_5} :catchall_21

    if-ne v0, v1, :cond_9

    .line 268
    :goto_7
    monitor-exit p0

    return-void

    .line 257
    :cond_9
    :try_start_9
    invoke-direct {p0}, Lcw;->f()V

    .line 258
    const-string v0, "falling back to local store"

    invoke-static {v0}, LcT;->e(Ljava/lang/String;)I

    .line 259
    iget-object v0, p0, Lcw;->b:Lcg;

    if-eqz v0, :cond_24

    .line 260
    iget-object v0, p0, Lcw;->b:Lcg;

    iput-object v0, p0, Lcw;->a:Lcg;

    .line 266
    :goto_19
    sget-object v0, LcA;->c:LcA;

    iput-object v0, p0, Lcw;->a:LcA;

    .line 267
    invoke-direct {p0}, Lcw;->g()V
    :try_end_20
    .catchall {:try_start_9 .. :try_end_20} :catchall_21

    goto :goto_7

    .line 253
    :catchall_21
    move-exception v0

    monitor-exit p0

    throw v0

    .line 262
    :cond_24
    :try_start_24
    invoke-static {}, Lct;->a()Lct;

    move-result-object v0

    .line 263
    iget-object v1, p0, Lcw;->a:Landroid/content/Context;

    iget-object v2, p0, Lcw;->a:Lci;

    invoke-virtual {v0, v1, v2}, Lct;->a(Landroid/content/Context;Lci;)V

    .line 264
    invoke-virtual {v0}, Lct;->a()Lcg;

    move-result-object v0

    iput-object v0, p0, Lcw;->a:Lcg;
    :try_end_35
    .catchall {:try_start_24 .. :try_end_35} :catchall_21

    goto :goto_19
.end method

.method private declared-synchronized j()V
    .registers 5

    .prologue
    .line 271
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcw;->a:Lcb;

    if-eqz v0, :cond_49

    iget-object v0, p0, Lcw;->a:LcA;

    sget-object v1, LcA;->c:LcA;
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_46

    if-eq v0, v1, :cond_49

    .line 273
    :try_start_b
    iget v0, p0, Lcw;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcw;->a:I

    .line 274
    iget-object v0, p0, Lcw;->b:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lcw;->a(Ljava/util/Timer;)Ljava/util/Timer;

    .line 275
    sget-object v0, LcA;->a:LcA;

    iput-object v0, p0, Lcw;->a:LcA;

    .line 276
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Failed Connect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcw;->b:Ljava/util/Timer;

    .line 277
    iget-object v0, p0, Lcw;->b:Ljava/util/Timer;

    new-instance v1, LcC;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LcC;-><init>(Lcw;Lcx;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 278
    const-string v0, "connecting to Analytics service"

    invoke-static {v0}, LcT;->e(Ljava/lang/String;)I

    .line 279
    iget-object v0, p0, Lcw;->a:Lcb;

    invoke-interface {v0}, Lcb;->b()V
    :try_end_3a
    .catchall {:try_start_b .. :try_end_3a} :catchall_46
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_3a} :catch_3c

    .line 288
    :goto_3a
    monitor-exit p0

    return-void

    .line 280
    :catch_3c
    move-exception v0

    .line 281
    :try_start_3d
    const-string v0, "security exception on connectToService"

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    .line 282
    invoke-direct {p0}, Lcw;->i()V
    :try_end_45
    .catchall {:try_start_3d .. :try_end_45} :catchall_46

    goto :goto_3a

    .line 271
    :catchall_46
    move-exception v0

    monitor-exit p0

    throw v0

    .line 285
    :cond_49
    :try_start_49
    const-string v0, "client not initialized."

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    .line 286
    invoke-direct {p0}, Lcw;->i()V
    :try_end_51
    .catchall {:try_start_49 .. :try_end_51} :catchall_46

    goto :goto_3a
.end method

.method private declared-synchronized k()V
    .registers 3

    .prologue
    .line 291
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcw;->a:Lcb;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcw;->a:LcA;

    sget-object v1, LcA;->b:LcA;

    if-ne v0, v1, :cond_14

    .line 292
    sget-object v0, LcA;->f:LcA;

    iput-object v0, p0, Lcw;->a:LcA;

    .line 293
    iget-object v0, p0, Lcw;->a:Lcb;

    invoke-interface {v0}, Lcb;->c()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_16

    .line 295
    :cond_14
    monitor-exit p0

    return-void

    .line 291
    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private l()V
    .registers 5

    .prologue
    .line 338
    iget-object v0, p0, Lcw;->a:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lcw;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lcw;->a:Ljava/util/Timer;

    .line 339
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Service Reconnect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcw;->a:Ljava/util/Timer;

    .line 340
    iget-object v0, p0, Lcw;->a:Ljava/util/Timer;

    new-instance v1, LcE;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LcE;-><init>(Lcw;Lcx;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 341
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 5

    .prologue
    .line 299
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcw;->b:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lcw;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lcw;->b:Ljava/util/Timer;

    .line 300
    const/4 v0, 0x0

    iput v0, p0, Lcw;->a:I

    .line 301
    const-string v0, "Connected to service"

    invoke-static {v0}, LcT;->e(Ljava/lang/String;)I

    .line 302
    sget-object v0, LcA;->b:LcA;

    iput-object v0, p0, Lcw;->a:LcA;

    .line 303
    invoke-direct {p0}, Lcw;->g()V

    .line 304
    iget-object v0, p0, Lcw;->c:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lcw;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lcw;->c:Ljava/util/Timer;

    .line 305
    new-instance v0, Ljava/util/Timer;

    const-string v1, "disconnect check"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcw;->c:Ljava/util/Timer;

    .line 306
    iget-object v0, p0, Lcw;->c:Ljava/util/Timer;

    new-instance v1, LcB;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LcB;-><init>(Lcw;Lcx;)V

    iget-wide v2, p0, Lcw;->b:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_36
    .catchall {:try_start_1 .. :try_end_36} :catchall_38

    .line 307
    monitor-exit p0

    return-void

    .line 299
    :catchall_38
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(ILandroid/content/Intent;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 328
    monitor-enter p0

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Connection to service failed "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LcT;->h(Ljava/lang/String;)I

    .line 329
    sget-object v0, LcA;->e:LcA;

    iput-object v0, p0, Lcw;->a:LcA;

    .line 330
    iget v0, p0, Lcw;->a:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_25

    .line 331
    invoke-direct {p0}, Lcw;->l()V
    :try_end_23
    .catchall {:try_start_1 .. :try_end_23} :catchall_29

    .line 335
    :goto_23
    monitor-exit p0

    return-void

    .line 333
    :cond_25
    :try_start_25
    invoke-direct {p0}, Lcw;->i()V
    :try_end_28
    .catchall {:try_start_25 .. :try_end_28} :catchall_29

    goto :goto_23

    .line 328
    :catchall_29
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/analytics/internal/Command;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    const-string v0, "putHit called"

    invoke-static {v0}, LcT;->e(Ljava/lang/String;)I

    .line 97
    iget-object v6, p0, Lcw;->a:Ljava/util/Queue;

    new-instance v0, LcD;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LcD;-><init>(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-direct {p0}, Lcw;->g()V

    .line 99
    return-void
.end method

.method public declared-synchronized b()V
    .registers 3

    .prologue
    .line 311
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcw;->a:LcA;

    sget-object v1, LcA;->f:LcA;

    if-ne v0, v1, :cond_15

    .line 312
    const-string v0, "Disconnected from service"

    invoke-static {v0}, LcT;->e(Ljava/lang/String;)I

    .line 313
    invoke-direct {p0}, Lcw;->f()V

    .line 314
    sget-object v0, LcA;->g:LcA;

    iput-object v0, p0, Lcw;->a:LcA;
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_27

    .line 324
    :goto_13
    monitor-exit p0

    return-void

    .line 316
    :cond_15
    :try_start_15
    const-string v0, "Unexpected disconnect."

    invoke-static {v0}, LcT;->e(Ljava/lang/String;)I

    .line 317
    sget-object v0, LcA;->e:LcA;

    iput-object v0, p0, Lcw;->a:LcA;

    .line 318
    iget v0, p0, Lcw;->a:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_2a

    .line 319
    invoke-direct {p0}, Lcw;->l()V
    :try_end_26
    .catchall {:try_start_15 .. :try_end_26} :catchall_27

    goto :goto_13

    .line 311
    :catchall_27
    move-exception v0

    monitor-exit p0

    throw v0

    .line 321
    :cond_2a
    :try_start_2a
    invoke-direct {p0}, Lcw;->i()V
    :try_end_2d
    .catchall {:try_start_2a .. :try_end_2d} :catchall_27

    goto :goto_13
.end method

.method public c()V
    .registers 3

    .prologue
    .line 103
    sget-object v0, Lcz;->a:[I

    iget-object v1, p0, Lcw;->a:LcA;

    invoke-virtual {v1}, LcA;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_16

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcw;->a:Z

    .line 113
    :goto_10
    :pswitch_10
    return-void

    .line 105
    :pswitch_11
    invoke-direct {p0}, Lcw;->h()V

    goto :goto_10

    .line 103
    nop

    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
    .end packed-switch
.end method

.method public d()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 117
    const-string v0, "clearHits called"

    invoke-static {v0}, LcT;->d(Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcw;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 119
    sget-object v0, Lcz;->a:[I

    iget-object v1, p0, Lcw;->a:LcA;

    invoke-virtual {v1}, LcA;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2c

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcw;->b:Z

    .line 132
    :goto_1b
    return-void

    .line 121
    :pswitch_1c
    iget-object v0, p0, Lcw;->a:Lcg;

    invoke-interface {v0}, Lcg;->a()V

    .line 122
    iput-boolean v2, p0, Lcw;->b:Z

    goto :goto_1b

    .line 125
    :pswitch_24
    iget-object v0, p0, Lcw;->a:Lcb;

    invoke-interface {v0}, Lcb;->a()V

    .line 126
    iput-boolean v2, p0, Lcw;->b:Z

    goto :goto_1b

    .line 119
    :pswitch_data_2c
    .packed-switch 0x1
        :pswitch_1c
        :pswitch_24
    .end packed-switch
.end method

.method public e()V
    .registers 3

    .prologue
    .line 159
    iget-object v0, p0, Lcw;->a:Lcb;

    if-eqz v0, :cond_5

    .line 164
    :goto_4
    return-void

    .line 162
    :cond_5
    new-instance v0, Lcc;

    iget-object v1, p0, Lcw;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p0}, Lcc;-><init>(Landroid/content/Context;Lce;Lcf;)V

    iput-object v0, p0, Lcw;->a:Lcb;

    .line 163
    invoke-direct {p0}, Lcw;->j()V

    goto :goto_4
.end method
