.class public LdH;
.super Laoe;
.source "ContextModule.java"


# instance fields
.field private final a:Landroid/app/Application;


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .registers 2
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Laoe;-><init>()V

    .line 38
    iput-object p1, p0, LdH;->a:Landroid/app/Application;

    .line 39
    return-void
.end method

.method static synthetic a(LdH;)Landroid/app/Application;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, LdH;->a:Landroid/app/Application;

    return-object v0
.end method


# virtual methods
.method protected a()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 43
    const-class v0, Landroid/content/Context;

    invoke-virtual {p0, v0}, LdH;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LdK;

    invoke-interface {v0, v1}, LaoM;->b(Ljava/lang/Class;)LaoR;

    .line 44
    const-class v0, LdL;

    invoke-virtual {p0, v0}, LdH;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LdM;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-interface {v0, v1}, LaoR;->a(Ljava/lang/Class;)V

    .line 46
    iget-object v0, p0, LdH;->a:Landroid/app/Application;

    if-eqz v0, :cond_61

    .line 47
    new-instance v0, LdM;

    iget-object v1, p0, LdH;->a:Landroid/app/Application;

    invoke-direct {v0, v1}, LdM;-><init>(Landroid/content/Context;)V

    .line 48
    const-class v1, LdM;

    invoke-virtual {p0, v1}, LdH;->a(Ljava/lang/Class;)LaoM;

    move-result-object v1

    invoke-static {v0}, LarT;->a(Ljava/lang/Object;)Laoz;

    move-result-object v0

    invoke-interface {v1, v0}, LaoM;->a(Laoz;)LaoR;

    .line 49
    const-class v0, Landroid/app/Application;

    invoke-virtual {p0, v0}, LdH;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    iget-object v1, p0, LdH;->a:Landroid/app/Application;

    invoke-static {v1}, LarT;->a(Ljava/lang/Object;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 55
    :goto_44
    const-class v0, Landroid/app/NotificationManager;

    invoke-virtual {p0, v0}, LdH;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    new-instance v1, LdI;

    invoke-direct {v1, p0}, LdI;-><init>(LdH;)V

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 63
    const-class v0, Landroid/content/res/AssetManager;

    invoke-virtual {p0, v0}, LdH;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    new-instance v1, LdJ;

    invoke-direct {v1, p0}, LdJ;-><init>(LdH;)V

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 69
    return-void

    .line 51
    :cond_61
    const-class v0, LdM;

    invoke-virtual {p0, v0}, LdH;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    invoke-static {v2}, LarT;->a(Ljava/lang/Object;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    .line 52
    const-class v0, Landroid/app/Application;

    invoke-virtual {p0, v0}, LdH;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    invoke-static {v2}, LarT;->a(Ljava/lang/Object;)Laoz;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Laoz;)LaoR;

    goto :goto_44
.end method
