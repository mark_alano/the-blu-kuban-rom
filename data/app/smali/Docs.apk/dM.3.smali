.class public LdM;
.super Ljava/lang/Object;
.source "ContextScopeImpl.java"

# interfaces
.implements LdL;


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private final a:LagY;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LagY",
            "<",
            "Landroid/content/Context;",
            "LagO",
            "<",
            "Laop",
            "<*>;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final a:Landroid/content/Context;

.field private final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LdM;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {}, LagP;->a()LagP;

    move-result-object v0

    invoke-virtual {v0}, LagP;->b()LagP;

    move-result-object v0

    new-instance v1, LdN;

    invoke-direct {v1, p0}, LdN;-><init>(LdM;)V

    invoke-virtual {v0, v1}, LagP;->a(LagV;)LagY;

    move-result-object v0

    iput-object v0, p0, LdM;->a:LagY;

    .line 38
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, LdM;->a:Ljava/lang/ThreadLocal;

    .line 43
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LdM;->a:Landroid/content/Context;

    .line 44
    return-void
.end method

.method private a()LagO;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LagO",
            "<",
            "Laop",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    invoke-virtual {p0}, LdM;->a()Landroid/content/Context;

    move-result-object v1

    .line 91
    :try_start_4
    iget-object v0, p0, LdM;->a:LagY;

    invoke-interface {v0, v1}, LagY;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LagO;
    :try_end_c
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_c} :catch_d

    .line 97
    return-object v0

    .line 92
    :catch_d
    move-exception v0

    .line 93
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected error when trying to get a scopedObjectMap for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method static synthetic a(LdM;)LagO;
    .registers 2
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, LdM;->a()LagO;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 25
    sget-object v0, LdM;->a:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method a()Landroid/content/Context;
    .registers 4

    .prologue
    .line 104
    iget-object v0, p0, LdM;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 105
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Thread "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no reference to a Context (probably a missing scope.enter inside a new/foreign"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Thread)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Thread "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has an invalid reference to a Context (probably a missing scope.enter or the Context"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has been garbage-collected)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    return-object v0
.end method

.method public a(Laop;Laoz;)Laoz;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laop",
            "<TT;>;",
            "Laoz",
            "<TT;>;)",
            "Laoz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 48
    new-instance v0, LdO;

    invoke-direct {v0, p0, p2}, LdO;-><init>(LdM;Laoz;)V

    .line 61
    new-instance v1, LdP;

    invoke-direct {v1, p0, p1, v0}, LdP;-><init>(LdM;Laop;Ljava/util/concurrent/Callable;)V

    return-object v1
.end method

.method public a(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, LdM;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 135
    :goto_8
    return-void

    .line 134
    :cond_9
    iget-object v0, p0, LdM;->a:Ljava/lang/ThreadLocal;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    goto :goto_8
.end method

.method b()Landroid/content/Context;
    .registers 3

    .prologue
    .line 121
    iget-object v0, p0, LdM;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 122
    if-eqz v0, :cond_17

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_17

    .line 123
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 125
    :goto_16
    return-object v0

    :cond_17
    iget-object v0, p0, LdM;->a:Landroid/content/Context;

    goto :goto_16
.end method

.method public b(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, LdM;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 150
    :cond_8
    :goto_8
    return-void

    .line 143
    :cond_9
    iget-object v0, p0, LdM;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 144
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 145
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 146
    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 147
    iget-object v0, p0, LdM;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    goto :goto_8
.end method

.method public c(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 155
    return-void
.end method
