.class public final LdR;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LdL;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LdK;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LdE;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LdF;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LdM;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 35
    iput-object p1, p0, LdR;->a:LYD;

    .line 36
    const-class v0, LdL;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, LdR;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LdR;->a:LZb;

    .line 39
    const-class v0, LdK;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LdR;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LdR;->b:LZb;

    .line 42
    const-class v0, LdE;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LdR;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LdR;->c:LZb;

    .line 45
    const-class v0, LdF;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LdR;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LdR;->d:LZb;

    .line 48
    const-class v0, LdM;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, LdR;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LdR;->e:LZb;

    .line 51
    return-void
.end method

.method static synthetic a(LdR;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LdR;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 138
    const-class v0, LdK;

    new-instance v1, LdS;

    invoke-direct {v1, p0}, LdS;-><init>(LdR;)V

    invoke-virtual {p0, v0, v1}, LdR;->a(Ljava/lang/Class;Laou;)V

    .line 146
    const-class v0, Lcom/google/android/apps/docs/DocsApplication;

    new-instance v1, LdT;

    invoke-direct {v1, p0}, LdT;-><init>(LdR;)V

    invoke-virtual {p0, v0, v1}, LdR;->a(Ljava/lang/Class;Laou;)V

    .line 154
    const-class v0, LdL;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LdR;->a:LZb;

    invoke-virtual {p0, v0, v1}, LdR;->a(Laop;LZb;)V

    .line 155
    const-class v0, LdK;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LdR;->b:LZb;

    invoke-virtual {p0, v0, v1}, LdR;->a(Laop;LZb;)V

    .line 156
    const-class v0, LdE;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LdR;->c:LZb;

    invoke-virtual {p0, v0, v1}, LdR;->a(Laop;LZb;)V

    .line 157
    const-class v0, LdF;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LdR;->d:LZb;

    invoke-virtual {p0, v0, v1}, LdR;->a(Laop;LZb;)V

    .line 158
    const-class v0, LdM;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LdR;->e:LZb;

    invoke-virtual {p0, v0, v1}, LdR;->a(Laop;LZb;)V

    .line 159
    iget-object v0, p0, LdR;->a:LZb;

    iget-object v1, p0, LdR;->a:LYD;

    iget-object v1, v1, LYD;->a:LdR;

    iget-object v1, v1, LdR;->e:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 161
    iget-object v0, p0, LdR;->b:LZb;

    new-instance v1, LdU;

    invoke-direct {v1, p0}, LdU;-><init>(LdR;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 172
    iget-object v0, p0, LdR;->c:LZb;

    new-instance v1, LdV;

    invoke-direct {v1, p0}, LdV;-><init>(LdR;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 186
    iget-object v0, p0, LdR;->d:LZb;

    new-instance v1, LdW;

    invoke-direct {v1, p0}, LdW;-><init>(LdR;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 200
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/DocsApplication;)V
    .registers 3
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llg;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:Llg;

    .line 73
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKZ;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LKZ;

    .line 79
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LaaJ;

    .line 85
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKU;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LKU;

    .line 91
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LME;

    .line 97
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/DocsApplication;->a:LKS;

    .line 103
    return-void
.end method

.method public a(LdK;)V
    .registers 3
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LdR;

    iget-object v0, v0, LdR;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LdM;

    iput-object v0, p1, LdK;->a:LdM;

    .line 63
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 204
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LdR;

    invoke-virtual {v0}, LdR;->c()V

    .line 206
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LdR;

    invoke-virtual {v0}, LdR;->d()V

    .line 208
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    sput-object v0, LdX;->a:Laoz;

    .line 112
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LdR;

    iget-object v0, v0, LdR;->a:LZb;

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    sput-object v0, LdX;->b:Laoz;

    .line 118
    return-void
.end method

.method public d()V
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    sput-object v0, Les;->a:Laoz;

    .line 127
    iget-object v0, p0, LdR;->a:LYD;

    iget-object v0, v0, LYD;->a:LdR;

    iget-object v0, v0, LdR;->a:LZb;

    invoke-static {v0}, LdR;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    sput-object v0, Les;->b:Laoz;

    .line 133
    return-void
.end method
