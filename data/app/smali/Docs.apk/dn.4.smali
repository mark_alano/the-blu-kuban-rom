.class public Ldn;
.super Ljava/lang/Object;
.source "Configuration.java"


# instance fields
.field private a:I

.field private a:Ldu;

.field private final a:Ljava/lang/String;

.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private final b:Ljava/lang/String;

.field private c:I

.field private c:Ljava/lang/String;

.field private d:I

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>()V
    .registers 7

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "Android %s"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldn;->a:Ljava/lang/String;

    .line 26
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    iput-object v0, p0, Ldn;->b:Ljava/lang/String;

    .line 27
    iput v5, p0, Ldn;->a:I

    .line 28
    const-string v0, "_s"

    iput-object v0, p0, Ldn;->c:Ljava/lang/String;

    .line 29
    const-string v0, "http://csi.gstatic.com/csi"

    iput-object v0, p0, Ldn;->d:Ljava/lang/String;

    .line 30
    const/16 v0, 0x10

    iput v0, p0, Ldn;->b:I

    .line 31
    iput v5, p0, Ldn;->c:I

    .line 32
    iput v4, p0, Ldn;->d:I

    .line 33
    iput v4, p0, Ldn;->e:I

    .line 34
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Ldn;->a:Ljava/util/Map;

    return-void
.end method

.method private a(ILjava/lang/String;)I
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 45
    if-gtz p1, :cond_14

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "%s must be greater than 0."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_14
    return p1
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 71
    iget v0, p0, Ldn;->a:I

    return v0
.end method

.method public a(I)Ldn;
    .registers 3
    .parameter

    .prologue
    .line 183
    const-string v0, "batchSize"

    invoke-direct {p0, p1, v0}, Ldn;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Ldn;->e:I

    .line 184
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ldn;
    .registers 2
    .parameter

    .prologue
    .line 95
    iput-object p1, p0, Ldn;->c:Ljava/lang/String;

    .line 96
    return-object p0
.end method

.method public a()Ldu;
    .registers 2

    .prologue
    .line 215
    iget-object v0, p0, Ldn;->a:Ldu;

    if-nez v0, :cond_b

    .line 216
    new-instance v0, Ldw;

    invoke-direct {v0}, Ldw;-><init>()V

    iput-object v0, p0, Ldn;->a:Ldu;

    .line 219
    :cond_b
    iget-object v0, p0, Ldn;->a:Ldu;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Ldn;->c:Ljava/lang/String;

    return-object v0
.end method

.method public a()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    iget-object v0, p0, Ldn;->a:Ljava/util/Map;

    return-object v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 120
    iget v0, p0, Ldn;->b:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 103
    iget-object v0, p0, Ldn;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 138
    iget v0, p0, Ldn;->c:I

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 156
    iget v0, p0, Ldn;->d:I

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 174
    iget v0, p0, Ldn;->e:I

    return v0
.end method
