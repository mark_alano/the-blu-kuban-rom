.class public Ldt;
.super Ljava/lang/Object;
.source "ReporterFactory.java"


# static fields
.field private static a:Ldr;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized a()Ldq;
    .registers 2

    .prologue
    .line 44
    const-class v1, Ldt;

    monitor-enter v1

    :try_start_3
    sget-object v0, Ldt;->a:Ldr;

    if-nez v0, :cond_f

    .line 45
    new-instance v0, Ldn;

    invoke-direct {v0}, Ldn;-><init>()V

    invoke-static {v0}, Ldt;->b(Ldn;)V

    .line 47
    :cond_f
    sget-object v0, Ldt;->a:Ldr;
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_13

    monitor-exit v1

    return-object v0

    .line 44
    :catchall_13
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ldn;)V
    .registers 3
    .parameter

    .prologue
    .line 58
    if-nez p0, :cond_a

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "configuration can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_a
    invoke-static {p0}, Ldt;->b(Ldn;)V

    .line 62
    return-void
.end method

.method private static declared-synchronized b(Ldn;)V
    .registers 10
    .parameter

    .prologue
    .line 23
    const-class v8, Ldt;

    monitor-enter v8

    :try_start_3
    new-instance v0, Ldr;

    invoke-virtual {p0}, Ldn;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ldn;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Ldn;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Ldn;->a()Ldu;

    move-result-object v4

    invoke-virtual {p0}, Ldn;->b()I

    move-result v5

    invoke-virtual {p0}, Ldn;->c()I

    move-result v6

    invoke-virtual {p0}, Ldn;->d()I

    move-result v7

    invoke-direct/range {v0 .. v7}, Ldr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ldu;III)V

    sput-object v0, Ldt;->a:Ldr;

    .line 32
    sget-object v0, Ldt;->a:Ldr;

    invoke-virtual {p0}, Ldn;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Ldr;->a(I)V

    .line 33
    invoke-virtual {p0}, Ldn;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_60

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 34
    sget-object v3, Ldt;->a:Ldr;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Ldr;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5c
    .catchall {:try_start_3 .. :try_end_5c} :catchall_5d

    goto :goto_3f

    .line 23
    :catchall_5d
    move-exception v0

    monitor-exit v8

    throw v0

    .line 36
    :cond_60
    monitor-exit v8

    return-void
.end method
