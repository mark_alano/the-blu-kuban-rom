.class public final enum LeG;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LeG;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LeG;

.field private static final synthetic a:[LeG;

.field public static final enum b:LeG;

.field public static final enum c:LeG;

.field public static final enum d:LeG;

.field public static final enum e:LeG;

.field public static final enum f:LeG;

.field public static final enum g:LeG;


# instance fields
.field private final a:I

.field private final a:LeI;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v2, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 116
    new-instance v0, LeG;

    const-string v1, "OWNER"

    sget-object v3, LeI;->a:LeI;

    invoke-direct {v0, v1, v7, v3}, LeG;-><init>(Ljava/lang/String;ILeI;)V

    sput-object v0, LeG;->a:LeG;

    .line 117
    new-instance v0, LeG;

    const-string v1, "WRITER"

    sget-object v3, LeI;->b:LeI;

    invoke-direct {v0, v1, v8, v3}, LeG;-><init>(Ljava/lang/String;ILeI;)V

    sput-object v0, LeG;->b:LeG;

    .line 118
    new-instance v0, LeG;

    const-string v1, "COMMENTER"

    sget v3, Len;->sharing_role_commenter:I

    sget-object v4, LeI;->c:LeI;

    sget-object v5, LeD;->a:LeD;

    new-array v6, v7, [LeD;

    invoke-direct/range {v0 .. v6}, LeG;-><init>(Ljava/lang/String;IILeI;LeD;[LeD;)V

    sput-object v0, LeG;->c:LeG;

    .line 119
    new-instance v0, LeG;

    const-string v1, "READER"

    sget-object v3, LeI;->c:LeI;

    invoke-direct {v0, v1, v9, v3}, LeG;-><init>(Ljava/lang/String;ILeI;)V

    sput-object v0, LeG;->d:LeG;

    .line 120
    new-instance v0, LeG;

    const-string v1, "NONE"

    sget-object v3, LeI;->d:LeI;

    invoke-direct {v0, v1, v10, v3}, LeG;-><init>(Ljava/lang/String;ILeI;)V

    sput-object v0, LeG;->e:LeG;

    .line 121
    new-instance v0, LeG;

    const-string v1, "NOACCESS"

    const/4 v3, 0x5

    sget-object v4, LeI;->e:LeI;

    invoke-direct {v0, v1, v3, v4}, LeG;-><init>(Ljava/lang/String;ILeI;)V

    sput-object v0, LeG;->f:LeG;

    .line 122
    new-instance v0, LeG;

    const-string v1, "UNKNOWN"

    const/4 v3, 0x6

    sget-object v4, LeI;->f:LeI;

    invoke-direct {v0, v1, v3, v4}, LeG;-><init>(Ljava/lang/String;ILeI;)V

    sput-object v0, LeG;->g:LeG;

    .line 115
    const/4 v0, 0x7

    new-array v0, v0, [LeG;

    sget-object v1, LeG;->a:LeG;

    aput-object v1, v0, v7

    sget-object v1, LeG;->b:LeG;

    aput-object v1, v0, v8

    sget-object v1, LeG;->c:LeG;

    aput-object v1, v0, v2

    sget-object v1, LeG;->d:LeG;

    aput-object v1, v0, v9

    sget-object v1, LeG;->e:LeG;

    aput-object v1, v0, v10

    const/4 v1, 0x5

    sget-object v2, LeG;->f:LeG;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LeG;->g:LeG;

    aput-object v2, v0, v1

    sput-object v0, LeG;->a:[LeG;

    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;IILeI;LeD;[LeD;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LeI;",
            "LeD;",
            "[",
            "LeD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-static {p5, p6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, LeG;-><init>(Ljava/lang/String;IILeI;Ljava/util/Set;)V

    .line 140
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILeI;Ljava/util/Set;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LeI;",
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 129
    iput p3, p0, LeG;->a:I

    .line 130
    iput-object p4, p0, LeG;->a:LeI;

    .line 131
    invoke-static {p5}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LeG;->a:Ljava/util/Set;

    .line 132
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILeI;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LeI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 135
    invoke-static {p3}, LeI;->a(LeI;)I

    move-result v3

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, LeG;-><init>(Ljava/lang/String;IILeI;Ljava/util/Set;)V

    .line 136
    return-void
.end method

.method public static a(LeI;Ljava/util/Set;)LeG;
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LeI;",
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;)",
            "LeG;"
        }
    .end annotation

    .prologue
    .line 165
    invoke-static {}, LeG;->values()[LeG;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_20

    aget-object v0, v2, v1

    .line 166
    iget-object v4, v0, LeG;->a:LeI;

    invoke-virtual {v4, p0}, LeI;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    iget-object v4, v0, LeG;->a:Ljava/util/Set;

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 174
    :goto_1b
    return-object v0

    .line 165
    :cond_1c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 171
    :cond_20
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 172
    sget-object v0, LeG;->g:LeG;

    goto :goto_1b

    .line 174
    :cond_29
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {p0, v0}, LeG;->a(LeI;Ljava/util/Set;)LeG;

    move-result-object v0

    goto :goto_1b
.end method

.method public static varargs a(LeI;[LeD;)LeG;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 159
    const-class v0, LeD;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 160
    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 161
    invoke-static {p0, v0}, LeG;->a(LeI;Ljava/util/Set;)LeG;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LeG;
    .registers 2
    .parameter

    .prologue
    .line 115
    const-class v0, LeG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LeG;

    return-object v0
.end method

.method public static values()[LeG;
    .registers 1

    .prologue
    .line 115
    sget-object v0, LeG;->a:[LeG;

    invoke-virtual {v0}, [LeG;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LeG;

    return-object v0
.end method


# virtual methods
.method public a()LeI;
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, LeG;->a:LeI;

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 155
    iget v0, p0, LeG;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/Set;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "LeD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, LeG;->a:Ljava/util/Set;

    return-object v0
.end method
