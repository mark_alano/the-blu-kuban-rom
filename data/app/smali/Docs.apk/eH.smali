.class public final enum LeH;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LeH;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LeH;

.field private static final synthetic a:[LeH;

.field public static final enum b:LeH;

.field public static final enum c:LeH;

.field public static final enum d:LeH;

.field public static final enum e:LeH;

.field public static final enum f:LeH;

.field public static final enum g:LeH;

.field public static final enum h:LeH;

.field public static final enum i:LeH;

.field public static final enum j:LeH;

.field public static final enum k:LeH;

.field public static final enum l:LeH;

.field public static final enum m:LeH;

.field public static final enum n:LeH;


# instance fields
.field private final a:I

.field private final a:LeG;

.field private final a:LeK;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .registers 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 223
    new-instance v0, LeH;

    const-string v1, "PRIVATE"

    sget-object v3, LeG;->g:LeG;

    sget-object v4, LeK;->e:LeK;

    sget v6, Len;->sharing_option_private:I

    move v5, v2

    invoke-direct/range {v0 .. v6}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v0, LeH;->a:LeH;

    .line 225
    new-instance v3, LeH;

    const-string v4, "ANYONE_CAN_EDIT"

    sget-object v6, LeG;->b:LeG;

    sget-object v7, LeK;->d:LeK;

    sget v9, Len;->sharing_option_anyone_can_edit:I

    move v5, v10

    move v8, v2

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->b:LeH;

    .line 227
    new-instance v3, LeH;

    const-string v4, "ANYONE_WITH_LINK_CAN_EDIT"

    sget-object v6, LeG;->b:LeG;

    sget-object v7, LeK;->d:LeK;

    sget v9, Len;->sharing_option_anyone_with_link_can_edit:I

    move v5, v11

    move v8, v10

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->c:LeH;

    .line 229
    new-instance v3, LeH;

    const-string v4, "ANYONE_CAN_COMMENT"

    sget-object v6, LeG;->c:LeG;

    sget-object v7, LeK;->d:LeK;

    sget v9, Len;->sharing_option_anyone_can_comment:I

    move v5, v12

    move v8, v2

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->d:LeH;

    .line 231
    new-instance v3, LeH;

    const-string v4, "ANYONE_WITH_LINK_CAN_COMMENT"

    sget-object v6, LeG;->c:LeG;

    sget-object v7, LeK;->d:LeK;

    sget v9, Len;->sharing_option_anyone_with_link_can_comment:I

    move v5, v13

    move v8, v10

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->e:LeH;

    .line 233
    new-instance v3, LeH;

    const-string v4, "ANYONE_CAN_VIEW"

    const/4 v5, 0x5

    sget-object v6, LeG;->d:LeG;

    sget-object v7, LeK;->d:LeK;

    sget v9, Len;->sharing_option_anyone_can_view:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->f:LeH;

    .line 235
    new-instance v3, LeH;

    const-string v4, "ANYONE_WITH_LINK_CAN_VIEW"

    const/4 v5, 0x6

    sget-object v6, LeG;->d:LeG;

    sget-object v7, LeK;->d:LeK;

    sget v9, Len;->sharing_option_anyone_with_link_can_view:I

    move v8, v10

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->g:LeH;

    .line 238
    new-instance v3, LeH;

    const-string v4, "ANYONE_FROM_CAN_EDIT"

    const/4 v5, 0x7

    sget-object v6, LeG;->b:LeG;

    sget-object v7, LeK;->c:LeK;

    sget v9, Len;->sharing_option_anyone_from_can_edit:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->h:LeH;

    .line 240
    new-instance v3, LeH;

    const-string v4, "ANYONE_FROM_WITH_LINK_CAN_EDIT"

    const/16 v5, 0x8

    sget-object v6, LeG;->b:LeG;

    sget-object v7, LeK;->c:LeK;

    sget v9, Len;->sharing_option_anyone_from_with_link_can_edit:I

    move v8, v10

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->i:LeH;

    .line 242
    new-instance v3, LeH;

    const-string v4, "ANYONE_FROM_CAN_COMMENT"

    const/16 v5, 0x9

    sget-object v6, LeG;->c:LeG;

    sget-object v7, LeK;->c:LeK;

    sget v9, Len;->sharing_option_anyone_from_can_comment:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->j:LeH;

    .line 244
    new-instance v3, LeH;

    const-string v4, "ANYONE_FROM_WITH_LINK_CAN_COMMENT"

    const/16 v5, 0xa

    sget-object v6, LeG;->c:LeG;

    sget-object v7, LeK;->c:LeK;

    sget v9, Len;->sharing_option_anyone_from_with_link_can_comment:I

    move v8, v10

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->k:LeH;

    .line 246
    new-instance v3, LeH;

    const-string v4, "ANYONE_FROM_CAN_VIEW"

    const/16 v5, 0xb

    sget-object v6, LeG;->d:LeG;

    sget-object v7, LeK;->c:LeK;

    sget v9, Len;->sharing_option_anyone_from_can_view:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->l:LeH;

    .line 248
    new-instance v3, LeH;

    const-string v4, "ANYONE_FROM_WITH_LINK_CAN_VIEW"

    const/16 v5, 0xc

    sget-object v6, LeG;->d:LeG;

    sget-object v7, LeK;->c:LeK;

    sget v9, Len;->sharing_option_anyone_from_with_link_can_view:I

    move v8, v10

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->m:LeH;

    .line 250
    new-instance v3, LeH;

    const-string v4, "UNKNOWN"

    const/16 v5, 0xd

    sget-object v6, LeG;->g:LeG;

    sget-object v7, LeK;->e:LeK;

    sget v9, Len;->sharing_option_unknown:I

    move v8, v2

    invoke-direct/range {v3 .. v9}, LeH;-><init>(Ljava/lang/String;ILeG;LeK;ZI)V

    sput-object v3, LeH;->n:LeH;

    .line 222
    const/16 v0, 0xe

    new-array v0, v0, [LeH;

    sget-object v1, LeH;->a:LeH;

    aput-object v1, v0, v2

    sget-object v1, LeH;->b:LeH;

    aput-object v1, v0, v10

    sget-object v1, LeH;->c:LeH;

    aput-object v1, v0, v11

    sget-object v1, LeH;->d:LeH;

    aput-object v1, v0, v12

    sget-object v1, LeH;->e:LeH;

    aput-object v1, v0, v13

    const/4 v1, 0x5

    sget-object v2, LeH;->f:LeH;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LeH;->g:LeH;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LeH;->h:LeH;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LeH;->i:LeH;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LeH;->j:LeH;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LeH;->k:LeH;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LeH;->l:LeH;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LeH;->m:LeH;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LeH;->n:LeH;

    aput-object v2, v0, v1

    sput-object v0, LeH;->a:[LeH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILeG;LeK;ZI)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LeG;",
            "LeK;",
            "ZI)V"
        }
    .end annotation

    .prologue
    .line 261
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 262
    iput-object p3, p0, LeH;->a:LeG;

    .line 263
    iput-object p4, p0, LeH;->a:LeK;

    .line 264
    iput-boolean p5, p0, LeH;->a:Z

    .line 265
    iput p6, p0, LeH;->a:I

    .line 266
    return-void
.end method

.method public static a(LeG;LeK;Z)LeH;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 291
    sget-object v0, LeG;->g:LeG;

    invoke-virtual {p0, v0}, LeG;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 292
    invoke-static {}, LeH;->values()[LeH;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_f
    if-ge v1, v3, :cond_2c

    aget-object v0, v2, v1

    .line 293
    iget-object v4, v0, LeH;->a:LeG;

    invoke-virtual {v4, p0}, LeG;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_28

    iget-object v4, v0, LeH;->a:LeK;

    invoke-virtual {v4, p1}, LeK;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_28

    iget-boolean v4, v0, LeH;->a:Z

    if-ne v4, p2, :cond_28

    .line 300
    :goto_27
    return-object v0

    .line 292
    :cond_28
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_f

    .line 300
    :cond_2c
    sget-object v0, LeH;->n:LeH;

    goto :goto_27
.end method

.method public static valueOf(Ljava/lang/String;)LeH;
    .registers 2
    .parameter

    .prologue
    .line 222
    const-class v0, LeH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LeH;

    return-object v0
.end method

.method public static values()[LeH;
    .registers 1

    .prologue
    .line 222
    sget-object v0, LeH;->a:[LeH;

    invoke-virtual {v0}, [LeH;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LeH;

    return-object v0
.end method


# virtual methods
.method public a()LeG;
    .registers 2

    .prologue
    .line 269
    iget-object v0, p0, LeH;->a:LeG;

    return-object v0
.end method

.method public a()LeK;
    .registers 2

    .prologue
    .line 273
    iget-object v0, p0, LeH;->a:LeK;

    return-object v0
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 281
    iget-object v0, p0, LeH;->a:LeK;

    sget-object v1, LeK;->c:LeK;

    invoke-virtual {v0, v1}, LeK;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 282
    iget v0, p0, LeH;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 284
    :goto_1a
    return-object v0

    :cond_1b
    iget v0, p0, LeH;->a:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1a
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 277
    iget-boolean v0, p0, LeH;->a:Z

    return v0
.end method
