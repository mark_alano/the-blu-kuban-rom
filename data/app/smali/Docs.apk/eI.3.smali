.class public enum LeI;
.super Ljava/lang/Enum;
.source "AclType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LeI;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LeI;

.field private static final synthetic a:[LeI;

.field public static final enum b:LeI;

.field public static final enum c:LeI;

.field public static final enum d:LeI;

.field public static final enum e:LeI;

.field public static final enum f:LeI;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 26
    new-instance v0, LeI;

    const-string v1, "OWNER"

    const-string v2, "owner"

    sget v3, Len;->sharing_role_owner:I

    invoke-direct {v0, v1, v5, v2, v3}, LeI;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LeI;->a:LeI;

    .line 27
    new-instance v0, LeI;

    const-string v1, "WRITER"

    const-string v2, "writer"

    sget v3, Len;->sharing_role_writer:I

    invoke-direct {v0, v1, v6, v2, v3}, LeI;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LeI;->b:LeI;

    .line 28
    new-instance v0, LeI;

    const-string v1, "READER"

    const-string v2, "reader"

    sget v3, Len;->sharing_role_reader:I

    invoke-direct {v0, v1, v7, v2, v3}, LeI;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LeI;->c:LeI;

    .line 29
    new-instance v0, LeI;

    const-string v1, "NONE"

    const-string v2, "none"

    sget v3, Len;->sharing_role_unknown:I

    invoke-direct {v0, v1, v8, v2, v3}, LeI;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LeI;->d:LeI;

    .line 30
    new-instance v0, LeI;

    const-string v1, "NOACCESS"

    const-string v2, "noaccess"

    sget v3, Len;->sharing_role_no_access:I

    invoke-direct {v0, v1, v9, v2, v3}, LeI;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LeI;->e:LeI;

    .line 31
    new-instance v0, LeJ;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x5

    const/4 v3, 0x0

    sget v4, Len;->sharing_role_unknown:I

    invoke-direct {v0, v1, v2, v3, v4}, LeJ;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LeI;->f:LeI;

    .line 25
    const/4 v0, 0x6

    new-array v0, v0, [LeI;

    sget-object v1, LeI;->a:LeI;

    aput-object v1, v0, v5

    sget-object v1, LeI;->b:LeI;

    aput-object v1, v0, v6

    sget-object v1, LeI;->c:LeI;

    aput-object v1, v0, v7

    sget-object v1, LeI;->d:LeI;

    aput-object v1, v0, v8

    sget-object v1, LeI;->e:LeI;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LeI;->f:LeI;

    aput-object v2, v0, v1

    sput-object v0, LeI;->a:[LeI;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput-object p3, p0, LeI;->a:Ljava/lang/String;

    .line 42
    iput p4, p0, LeI;->a:I

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;ILeC;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, LeI;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    return-void
.end method

.method static synthetic a(LeI;)I
    .registers 2
    .parameter

    .prologue
    .line 25
    iget v0, p0, LeI;->a:I

    return v0
.end method

.method public static a(Ljava/lang/String;)LeI;
    .registers 7
    .parameter

    .prologue
    .line 62
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-static {}, LeI;->values()[LeI;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_b
    if-ge v1, v4, :cond_1c

    aget-object v0, v3, v1

    .line 64
    iget-object v5, v0, LeI;->a:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 68
    :goto_17
    return-object v0

    .line 63
    :cond_18
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_b

    .line 68
    :cond_1c
    sget-object v0, LeI;->f:LeI;

    goto :goto_17
.end method

.method public static valueOf(Ljava/lang/String;)LeI;
    .registers 2
    .parameter

    .prologue
    .line 25
    const-class v0, LeI;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LeI;

    return-object v0
.end method

.method public static values()[LeI;
    .registers 1

    .prologue
    .line 25
    sget-object v0, LeI;->a:[LeI;

    invoke-virtual {v0}, [LeI;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LeI;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 55
    iget-object v0, p0, LeI;->a:Ljava/lang/String;

    return-object v0
.end method
