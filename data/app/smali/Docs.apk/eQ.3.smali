.class public LeQ;
.super Ljava/lang/Object;
.source "Tracker.java"


# annotations
.annotation runtime LaoJ;
.end annotation


# instance fields
.field private a:I

.field private final a:LeV;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laoz;Ljava/lang/String;Lgl;LKS;)V
    .registers 7
    .parameter
    .end parameter
    .parameter
        .annotation runtime LeT;
        .end annotation
    .end parameter
    .parameter
    .end parameter
    .parameter
    .end parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Ljava/lang/String;",
            "Lgl;",
            "LKS;",
            ")V"
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    const/4 v0, 0x0

    iput v0, p0, LeQ;->a:I

    .line 141
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LeQ;->a:Ljava/util/Map;

    .line 147
    sget-object v0, Lgi;->l:Lgi;

    invoke-interface {p3, v0}, Lgl;->a(Lgi;)Z

    move-result v1

    .line 148
    invoke-interface {p1}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 150
    if-nez v1, :cond_21

    if-nez v0, :cond_2a

    .line 151
    :cond_21
    new-instance v0, LeU;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LeU;-><init>(LeR;)V

    iput-object v0, p0, LeQ;->a:LeV;

    .line 155
    :goto_29
    return-void

    .line 153
    :cond_2a
    new-instance v1, LeS;

    invoke-direct {v1, v0, p2, p4}, LeS;-><init>(Landroid/content/Context;Ljava/lang/String;LKS;)V

    iput-object v1, p0, LeQ;->a:LeV;

    goto :goto_29
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 3

    .prologue
    .line 162
    monitor-enter p0

    :try_start_1
    iget v0, p0, LeQ;->a:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LeQ;->a:I

    if-nez v0, :cond_e

    .line 163
    iget-object v0, p0, LeQ;->a:LeV;

    invoke-interface {v0}, LeV;->a()V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_10

    .line 165
    :cond_e
    monitor-exit p0

    return-void

    .line 162
    :catchall_10
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/Object;)V
    .registers 5
    .parameter

    .prologue
    .line 181
    iget-object v0, p0, LeQ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    :goto_9
    invoke-static {v0}, Lagu;->b(Z)V

    .line 182
    iget-object v0, p0, LeQ;->a:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    return-void

    .line 181
    :cond_1a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 189
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 196
    iget-object v0, p0, LeQ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 197
    if-eqz v0, :cond_22

    const/4 v1, 0x1

    :goto_b
    invoke-static {v1}, Lagu;->b(Z)V

    .line 198
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long v0, v1, v3

    .line 199
    const-string v2, "timeSpan"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v2, p2, p3, v0}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 200
    return-void

    .line 197
    :cond_22
    const/4 v1, 0x0

    goto :goto_b
.end method

.method public a(Ljava/lang/String;Landroid/content/Intent;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 244
    if-eqz p2, :cond_1b

    .line 245
    const-string v0, "referrer"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 246
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 247
    iget-object v1, p0, LeQ;->a:LeV;

    const-string v2, "referredFrom"

    const/4 v3, 0x0

    invoke-interface {v1, p1, v2, v0, v3}, LeV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 248
    iget-object v1, p0, LeQ;->a:LeV;

    invoke-interface {v1, v0}, LeV;->b(Ljava/lang/String;)V

    .line 251
    :cond_1b
    iget-object v0, p0, LeQ;->a:LeV;

    invoke-interface {v0, p1}, LeV;->a(Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 216
    invoke-virtual {p0, p1, p2, v0, v0}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 217
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 225
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 226
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 233
    iget-object v0, p0, LeQ;->a:LeV;

    invoke-interface {v0, p1, p2, p3, p4}, LeV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 234
    return-void
.end method

.method public declared-synchronized b()V
    .registers 2

    .prologue
    .line 172
    monitor-enter p0

    :try_start_1
    iget v0, p0, LeQ;->a:I

    if-lez v0, :cond_16

    const/4 v0, 0x1

    :goto_6
    invoke-static {v0}, Lagu;->b(Z)V

    .line 173
    iget v0, p0, LeQ;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LeQ;->a:I

    .line 174
    iget-object v0, p0, LeQ;->a:LeV;

    invoke-interface {v0}, LeV;->b()V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_18

    .line 175
    monitor-exit p0

    return-void

    .line 172
    :cond_16
    const/4 v0, 0x0

    goto :goto_6

    :catchall_18
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Ljava/lang/Object;)V
    .registers 3
    .parameter

    .prologue
    .line 206
    iget-object v0, p0, LeQ;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 207
    if-eqz v0, :cond_f

    const/4 v0, 0x1

    :goto_b
    invoke-static {v0}, Lagu;->b(Z)V

    .line 208
    return-void

    .line 207
    :cond_f
    const/4 v0, 0x0

    goto :goto_b
.end method
