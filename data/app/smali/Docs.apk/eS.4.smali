.class LeS;
.super Ljava/lang/Object;
.source "Tracker.java"

# interfaces
.implements LeV;


# static fields
.field private static a:Ldj;


# instance fields
.field private final a:LKS;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LKS;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p3, p0, LeS;->a:LKS;

    .line 84
    sget-object v0, LeS;->a:Ldj;

    if-nez v0, :cond_2c

    .line 85
    invoke-static {p1}, LcK;->a(Landroid/content/Context;)LcK;

    move-result-object v0

    .line 86
    invoke-virtual {v0, p2}, LcK;->a(Ljava/lang/String;)Ldj;

    move-result-object v0

    sput-object v0, LeS;->a:Ldj;

    .line 90
    :try_start_13
    sget v0, Len;->app_name_drivev2:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_18
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_13 .. :try_end_18} :catch_28

    move-result-object v0

    .line 95
    :goto_19
    invoke-static {}, LaaF;->a()Ljava/lang/String;

    move-result-object v1

    .line 96
    sget-object v2, LeS;->a:Ldj;

    invoke-interface {v2, v0}, Ldj;->a(Ljava/lang/String;)V

    .line 97
    sget-object v0, LeS;->a:Ldj;

    invoke-interface {v0, v1}, Ldj;->b(Ljava/lang/String;)V

    .line 101
    :goto_27
    return-void

    .line 91
    :catch_28
    move-exception v0

    .line 92
    const-string v0, "unknown"

    goto :goto_19

    .line 99
    :cond_2c
    sget-object v0, LeS;->a:Ldj;

    invoke-interface {v0}, Ldj;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lagu;->b(Z)V

    goto :goto_27
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 105
    iget-object v0, p0, LeS;->a:LKS;

    const-string v1, "googleAnalyticsDispatchPeriodS"

    const/16 v2, 0x3c

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    .line 107
    invoke-static {}, Lct;->a()Lct;

    move-result-object v1

    invoke-virtual {v1, v0}, Lct;->a(I)V

    .line 108
    sget-object v0, LeS;->a:Ldj;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ldj;->a(Z)V

    .line 109
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 123
    sget-object v0, LeS;->a:Ldj;

    invoke-interface {v0, p1}, Ldj;->c(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 118
    sget-object v0, LeS;->a:Ldj;

    invoke-interface {v0, p1, p2, p3, p4}, Ldj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 119
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 113
    invoke-static {}, Lct;->a()Lct;

    move-result-object v0

    invoke-virtual {v0}, Lct;->a()V

    .line 114
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 128
    sget-object v0, LeS;->a:Ldj;

    invoke-interface {v0, p1}, Ldj;->d(Ljava/lang/String;)V

    .line 129
    return-void
.end method
