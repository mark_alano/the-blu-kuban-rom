.class public final Len;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final about_dialog:I = 0x7f08004b

.field public static final add_collaborator_accept:I = 0x7f080290

.field public static final add_collaborators:I = 0x7f0800cf

.field public static final add_collaborators_default:I = 0x7f080291

.field public static final add_collaborators_description:I = 0x7f08028f

.field public static final alignment_center:I = 0x7f08008b

.field public static final alignment_justified:I = 0x7f08008d

.field public static final alignment_left:I = 0x7f08008a

.field public static final alignment_right:I = 0x7f08008c

.field public static final alphabet_set:I = 0x7f0800d0

.field public static final app_docs_name:I = 0x7f0800d1

.field public static final app_drive_name:I = 0x7f0800d2

.field public static final app_name:I = 0x7f080048

.field public static final app_name_drive:I = 0x7f080263

.field public static final app_name_drivev2:I = 0x7f080274

.field public static final app_name_kix_editor:I = 0x7f080049

.field public static final app_name_trix_editor:I = 0x7f08004a

.field public static final ask_confirmation_for_document_deletion:I = 0x7f0800d3

.field public static final ask_confirmation_for_document_deletion_multiple:I = 0x7f0800d4

.field public static final authentication_error:I = 0x7f0800d5

.field public static final back:I = 0x7f0800cd

.field public static final bold:I = 0x7f08007c

.field public static final bulleted_list:I = 0x7f08007f

.field public static final button_add_from_contacts:I = 0x7f0800d6

.field public static final button_comment:I = 0x7f0800d7

.field public static final button_create_contact:I = 0x7f0800d8

.field public static final button_retry:I = 0x7f0800d9

.field public static final button_save_sharing:I = 0x7f0800da

.field public static final button_showall:I = 0x7f0800db

.field public static final button_yes:I = 0x7f0800dc

.field public static final camera_ocr_blur_title:I = 0x7f0800e2

.field public static final camera_ocr_blur_warning:I = 0x7f0800e3

.field public static final camera_ocr_default_name:I = 0x7f0800df

.field public static final camera_ocr_error_capture:I = 0x7f0800dd

.field public static final camera_ocr_evaluating_message:I = 0x7f0800e0

.field public static final camera_ocr_evaluating_skip_button:I = 0x7f0800e1

.field public static final camera_ocr_no_sdcard:I = 0x7f0800de

.field public static final camera_ocr_warning_continue:I = 0x7f0800e4

.field public static final camera_ocr_warning_retake:I = 0x7f0800e5

.field public static final can_not_be_deleted:I = 0x7f08008e

.field public static final cannot_edit:I = 0x7f08005f

.field public static final clear_cache:I = 0x7f0800e6

.field public static final clear_cache_cleared_message:I = 0x7f0800e7

.field public static final clear_cache_message:I = 0x7f0800e8

.field public static final close_application:I = 0x7f080055

.field public static final color_describer:I = 0x7f08008f

.field public static final comment:I = 0x7f080058

.field public static final comment_action_delete:I = 0x7f0800ea

.field public static final comment_action_edit:I = 0x7f0800e9

.field public static final comment_document_hint:I = 0x7f0800eb

.field public static final comment_document_reply_hint:I = 0x7f0800ec

.field public static final comment_document_reply_reopen_hint:I = 0x7f0800ed

.field public static final comment_error:I = 0x7f0800ee

.field public static final comment_loading_document:I = 0x7f0800f3

.field public static final comment_loading_thread:I = 0x7f0800f4

.field public static final comment_markresolved:I = 0x7f0800f0

.field public static final comment_none:I = 0x7f0800f2

.field public static final comment_post_error:I = 0x7f0800ef

.field public static final comment_reopen:I = 0x7f0800f8

.field public static final comment_resolved:I = 0x7f0800f1

.field public static final comment_resolved_username:I = 0x7f0800f7

.field public static final comment_working:I = 0x7f0800f5

.field public static final contact_sharing_commenter:I = 0x7f0800f9

.field public static final contact_sharing_no_access:I = 0x7f0800fa

.field public static final contact_sharing_owner:I = 0x7f0800fb

.field public static final contact_sharing_reader:I = 0x7f0800fc

.field public static final contact_sharing_writer:I = 0x7f0800fd

.field public static final content_sync_question:I = 0x7f0800fe

.field public static final copy:I = 0x7f080054

.field public static final create_new_doc_title:I = 0x7f0800ff

.field public static final create_new_error_document:I = 0x7f080100

.field public static final create_new_error_folder:I = 0x7f080101

.field public static final create_new_error_spreadsheet:I = 0x7f080102

.field public static final create_new_folder:I = 0x7f080103

.field public static final create_new_from_camera:I = 0x7f080104

.field public static final create_new_from_upload:I = 0x7f080105

.field public static final create_new_kix_doc:I = 0x7f080107

.field public static final create_new_ocr_doc:I = 0x7f080106

.field public static final create_new_trix_doc:I = 0x7f080109

.field public static final create_shortcut_label:I = 0x7f080108

.field public static final creating_document:I = 0x7f08010a

.field public static final creating_folder:I = 0x7f08010b

.field public static final creating_spreadsheet:I = 0x7f08010c

.field public static final cut:I = 0x7f080053

.field public static final decrease_indent:I = 0x7f080082

.field public static final decrypting_progress_message:I = 0x7f08010d

.field public static final default_new_folder_title:I = 0x7f08010e

.field public static final default_new_kix_title:I = 0x7f08010f

.field public static final default_new_trix_title:I = 0x7f080110

.field public static final default_scope_contact_name:I = 0x7f080111

.field public static final delete_collection:I = 0x7f080112

.field public static final delete_collection_drive:I = 0x7f080264

.field public static final delete_document:I = 0x7f080113

.field public static final delete_multiple:I = 0x7f080114

.field public static final dialog_confirm_sharing:I = 0x7f080115

.field public static final dialog_confirm_sharing_message:I = 0x7f080116

.field public static final dialog_confirm_sharing_message_multiple:I = 0x7f080117

.field public static final dialog_contact_sharing:I = 0x7f080118

.field public static final dialog_select:I = 0x7f08011a

.field public static final dialog_select_a_document:I = 0x7f08011b

.field public static final dialog_select_a_folder:I = 0x7f08011c

.field public static final dialog_select_an_item:I = 0x7f08011d

.field public static final dialog_select_navigate_up_to:I = 0x7f08011e

.field public static final dialog_sharing_options:I = 0x7f080119

.field public static final discussion_cancel:I = 0x7f0800a2

.field public static final discussion_cant_comment:I = 0x7f080098

.field public static final discussion_cant_edit_comment:I = 0x7f080097

.field public static final discussion_close:I = 0x7f08009e

.field public static final discussion_close_comments:I = 0x7f08009a

.field public static final discussion_collapsed_replies:I = 0x7f080095

.field public static final discussion_comment_on_this_document:I = 0x7f080092

.field public static final discussion_comments:I = 0x7f08009b

.field public static final discussion_contact_picture:I = 0x7f080094

.field public static final discussion_delete_cancel:I = 0x7f0800b1

.field public static final discussion_delete_comment_text:I = 0x7f0800ad

.field public static final discussion_delete_comment_title:I = 0x7f0800ac

.field public static final discussion_delete_discussion_text:I = 0x7f0800af

.field public static final discussion_delete_discussion_title:I = 0x7f0800ae

.field public static final discussion_delete_yes:I = 0x7f0800b0

.field public static final discussion_does_not_exist:I = 0x7f0800aa

.field public static final discussion_error:I = 0x7f080096

.field public static final discussion_longer_comment:I = 0x7f0800ab

.field public static final discussion_marked_as_resolved:I = 0x7f0800a6

.field public static final discussion_me:I = 0x7f0800a5

.field public static final discussion_menu:I = 0x7f080091

.field public static final discussion_new_comment:I = 0x7f0800a1

.field public static final discussion_no_comments:I = 0x7f080093

.field public static final discussion_non_anchored:I = 0x7f0800a9

.field public static final discussion_re_opened:I = 0x7f0800a7

.field public static final discussion_reopen:I = 0x7f08009d

.field public static final discussion_reply:I = 0x7f0800a4

.field public static final discussion_reply_to_reopen_this_comment:I = 0x7f0800a0

.field public static final discussion_reply_to_this_comment:I = 0x7f08009f

.field public static final discussion_resolve:I = 0x7f08009c

.field public static final discussion_resolved:I = 0x7f0800a8

.field public static final discussion_save:I = 0x7f0800a3

.field public static final discussion_view_all_comments:I = 0x7f080099

.field public static final doclist_preview_close_button_content_description:I = 0x7f08011f

.field public static final doclist_preview_content_description:I = 0x7f080120

.field public static final doclist_quickactions_content_description:I = 0x7f080121

.field public static final doclist_star_cb_content_description:I = 0x7f080122

.field public static final doclist_starred_state:I = 0x7f080123

.field public static final doclist_unstarred_state:I = 0x7f080124

.field public static final document_deleted:I = 0x7f080050

.field public static final document_deleted_expanded:I = 0x7f080051

.field public static final document_preparing_to_open_progress:I = 0x7f080125

.field public static final document_preparing_to_send_progress:I = 0x7f080126

.field public static final document_view_button_back_to_doclist:I = 0x7f080127

.field public static final domain_scope_contact_name:I = 0x7f080128

.field public static final done:I = 0x7f08006d

.field public static final download_bytes_format:I = 0x7f080129

.field public static final download_bytes_format_short:I = 0x7f08012a

.field public static final download_kilobytes_format:I = 0x7f08012b

.field public static final download_kilobytes_format_short:I = 0x7f08012c

.field public static final download_megabytes_format:I = 0x7f08012d

.field public static final download_megabytes_format_short:I = 0x7f08012e

.field public static final drive_invitation_error_drivev2:I = 0x7f080275

.field public static final drive_welcome_message_drivev2:I = 0x7f080276

.field public static final drive_welcome_title_drivev2:I = 0x7f080277

.field public static final edit:I = 0x7f080057

.field public static final edit_menu:I = 0x7f080090

.field public static final empty_contact_list:I = 0x7f080130

.field public static final empty_doclist:I = 0x7f080131

.field public static final empty_sharing_list:I = 0x7f080132

.field public static final enter_edit_mode:I = 0x7f080060

.field public static final enter_new_name:I = 0x7f080133

.field public static final error_access_denied_html:I = 0x7f080134

.field public static final error_document_not_available:I = 0x7f080138

.field public static final error_failed_to_create_account:I = 0x7f08013d

.field public static final error_internal_error_html:I = 0x7f080135

.field public static final error_network_error_html:I = 0x7f080136

.field public static final error_no_viewer_available:I = 0x7f080137

.field public static final error_opening_document:I = 0x7f08013a

.field public static final error_opening_document_for_html:I = 0x7f08013b

.field public static final error_page_title:I = 0x7f08013c

.field public static final error_report_button_close:I = 0x7f080076

.field public static final error_report_button_reload:I = 0x7f080075

.field public static final error_report_button_report:I = 0x7f080074

.field public static final error_report_description:I = 0x7f080073

.field public static final error_report_title:I = 0x7f080072

.field public static final error_sync:I = 0x7f08013e

.field public static final error_video_not_available:I = 0x7f080139

.field public static final exporting_document:I = 0x7f08013f

.field public static final fast_scroll_time_grouper_earlier:I = 0x7f080140

.field public static final fast_scroll_time_grouper_in_year:I = 0x7f080141

.field public static final fast_scroll_time_grouper_today:I = 0x7f080142

.field public static final fast_scroll_time_grouper_yesterday:I = 0x7f080143

.field public static final fast_scroll_title_grouper_collections:I = 0x7f080144

.field public static final fast_scroll_title_grouper_collections_drive:I = 0x7f080265

.field public static final file_too_large_for_upload_drivev2:I = 0x7f080278

.field public static final file_too_large_for_upload_okbtn:I = 0x7f080145

.field public static final file_too_large_for_upload_title:I = 0x7f080146

.field public static final font_picker_font:I = 0x7f080078

.field public static final font_picker_size:I = 0x7f080079

.field public static final gallery_select_error:I = 0x7f080147

.field public static final getting_authentication_information:I = 0x7f08014e

.field public static final gf_anonymous:I = 0x7f080006

.field public static final gf_app_name:I = 0x7f080002

.field public static final gf_back:I = 0x7f080003

.field public static final gf_build_view:I = 0x7f08001c

.field public static final gf_choose_an_account:I = 0x7f080007

.field public static final gf_crash_header:I = 0x7f08003c

.field public static final gf_error_report_board:I = 0x7f080030

.field public static final gf_error_report_brand:I = 0x7f080031

.field public static final gf_error_report_build_id:I = 0x7f080028

.field public static final gf_error_report_build_type:I = 0x7f080029

.field public static final gf_error_report_codename:I = 0x7f08002f

.field public static final gf_error_report_description:I = 0x7f080026

.field public static final gf_error_report_device:I = 0x7f080027

.field public static final gf_error_report_incremental:I = 0x7f08002e

.field public static final gf_error_report_installed_packages:I = 0x7f080033

.field public static final gf_error_report_installer_package_name:I = 0x7f080023

.field public static final gf_error_report_model:I = 0x7f08002a

.field public static final gf_error_report_package_name:I = 0x7f080022

.field public static final gf_error_report_package_version:I = 0x7f080020

.field public static final gf_error_report_package_version_name:I = 0x7f080021

.field public static final gf_error_report_process_name:I = 0x7f080024

.field public static final gf_error_report_product:I = 0x7f08002b

.field public static final gf_error_report_release:I = 0x7f08002d

.field public static final gf_error_report_running_apps:I = 0x7f080034

.field public static final gf_error_report_running_service_details:I = 0x7f080037

.field public static final gf_error_report_sdk_version:I = 0x7f08002c

.field public static final gf_error_report_system:I = 0x7f08001f

.field public static final gf_error_report_system_app:I = 0x7f080036

.field public static final gf_error_report_system_log:I = 0x7f080035

.field public static final gf_error_report_time:I = 0x7f080025

.field public static final gf_error_report_user_accounts:I = 0x7f080032

.field public static final gf_exception_class_name:I = 0x7f08003e

.field public static final gf_exception_message:I = 0x7f080043

.field public static final gf_feedback:I = 0x7f080004

.field public static final gf_include_screenshot:I = 0x7f080010

.field public static final gf_include_system_data:I = 0x7f08000f

.field public static final gf_network_data:I = 0x7f08001d

.field public static final gf_network_name:I = 0x7f08001e

.field public static final gf_no:I = 0x7f08003b

.field public static final gf_no_data:I = 0x7f08000d

.field public static final gf_optional_description:I = 0x7f080017

.field public static final gf_preview:I = 0x7f08000a

.field public static final gf_preview_feedback:I = 0x7f08000c

.field public static final gf_privacy:I = 0x7f08000e

.field public static final gf_privacy_policy:I = 0x7f08001a

.field public static final gf_privacy_text:I = 0x7f08001b

.field public static final gf_receiver_host:I = 0x7f080045

.field public static final gf_receiver_path:I = 0x7f080047

.field public static final gf_receiver_port:I = 0x7f080046

.field public static final gf_receiver_transport_scheme:I = 0x7f080044

.field public static final gf_report_being_sent:I = 0x7f080014

.field public static final gf_report_feedback:I = 0x7f080008

.field public static final gf_report_queued:I = 0x7f080016

.field public static final gf_report_sent_failure:I = 0x7f080015

.field public static final gf_screenshot_preview:I = 0x7f080009

.field public static final gf_send:I = 0x7f08000b

.field public static final gf_should_submit_anonymously:I = 0x7f080039

.field public static final gf_should_submit_on_empty_description:I = 0x7f080038

.field public static final gf_stack_trace:I = 0x7f08003d

.field public static final gf_sys_logs:I = 0x7f080019

.field public static final gf_system_log:I = 0x7f080018

.field public static final gf_this_will_help:I = 0x7f080011

.field public static final gf_this_will_help_screenshot:I = 0x7f080012

.field public static final gf_throw_class_name:I = 0x7f080041

.field public static final gf_throw_file_name:I = 0x7f08003f

.field public static final gf_throw_line_number:I = 0x7f080040

.field public static final gf_throw_method_name:I = 0x7f080042

.field public static final gf_unknown_app:I = 0x7f080013

.field public static final gf_user_account:I = 0x7f080005

.field public static final gf_yes:I = 0x7f08003a

.field public static final google_account_missing_all_apps:I = 0x7f080266

.field public static final google_account_needed_all_apps:I = 0x7f080267

.field public static final home_activity:I = 0x7f08014f

.field public static final home_collections:I = 0x7f080150

.field public static final home_documents:I = 0x7f080151

.field public static final home_images:I = 0x7f080152

.field public static final home_more_choices:I = 0x7f080153

.field public static final home_my_drive:I = 0x7f080154

.field public static final home_owned_by_me:I = 0x7f080155

.field public static final home_pinned:I = 0x7f080156

.field public static final home_shared_with_me:I = 0x7f080157

.field public static final home_starred:I = 0x7f080158

.field public static final increase_indent:I = 0x7f080081

.field public static final install_docs_app:I = 0x7f08006c

.field public static final intro_dialog_text:I = 0x7f08015a

.field public static final intro_dialog_text_drive:I = 0x7f080269

.field public static final intro_dialog_title:I = 0x7f080159

.field public static final intro_dialog_title_drive:I = 0x7f080268

.field public static final italic:I = 0x7f08007d

.field public static final keyboard:I = 0x7f080088

.field public static final loading:I = 0x7f08015b

.field public static final loading_document:I = 0x7f08015d

.field public static final loading_printers:I = 0x7f08015c

.field public static final logo_title:I = 0x7f08015e

.field public static final logo_title_drive:I = 0x7f08026a

.field public static final menu_account_settings:I = 0x7f08015f

.field public static final menu_action_star:I = 0x7f080160

.field public static final menu_action_unstar:I = 0x7f080161

.field public static final menu_add_collaborator:I = 0x7f080059

.field public static final menu_comments:I = 0x7f080162

.field public static final menu_create_new_doc:I = 0x7f080163

.field public static final menu_create_new_from_upload:I = 0x7f080164

.field public static final menu_delete:I = 0x7f080165

.field public static final menu_edit:I = 0x7f080166

.field public static final menu_email:I = 0x7f080167

.field public static final menu_filter_by:I = 0x7f080168

.field public static final menu_full_screen:I = 0x7f080169

.field public static final menu_help:I = 0x7f080171

.field public static final menu_hide_live_editing:I = 0x7f08016a

.field public static final menu_info:I = 0x7f08016b

.field public static final menu_mobix:I = 0x7f08005b

.field public static final menu_more:I = 0x7f08016c

.field public static final menu_move_file:I = 0x7f08016d

.field public static final menu_move_folder:I = 0x7f08016e

.field public static final menu_my_drive:I = 0x7f08016f

.field public static final menu_offline:I = 0x7f080170

.field public static final menu_open_with:I = 0x7f080172

.field public static final menu_pin:I = 0x7f080173

.field public static final menu_pin_mode:I = 0x7f080174

.field public static final menu_print:I = 0x7f08017b

.field public static final menu_quick_hints:I = 0x7f080175

.field public static final menu_reading_mode:I = 0x7f08005c

.field public static final menu_refresh:I = 0x7f080176

.field public static final menu_rename:I = 0x7f080177

.field public static final menu_resolve:I = 0x7f0800f6

.field public static final menu_search:I = 0x7f080178

.field public static final menu_send:I = 0x7f080179

.field public static final menu_send_feedback:I = 0x7f08005a

.field public static final menu_send_link:I = 0x7f08017a

.field public static final menu_settings:I = 0x7f08017c

.field public static final menu_share_collaborators:I = 0x7f080182

.field public static final menu_shared_with_me:I = 0x7f08017d

.field public static final menu_sharing:I = 0x7f08017e

.field public static final menu_show_activity:I = 0x7f08026b

.field public static final menu_show_all:I = 0x7f08017f

.field public static final menu_show_collection:I = 0x7f080181

.field public static final menu_show_drawing:I = 0x7f080180

.field public static final menu_show_folder:I = 0x7f08026c

.field public static final menu_show_home:I = 0x7f080183

.field public static final menu_show_kix:I = 0x7f080184

.field public static final menu_show_live_editing:I = 0x7f080185

.field public static final menu_show_movie:I = 0x7f080186

.field public static final menu_show_owned_by_me:I = 0x7f080187

.field public static final menu_show_pdf:I = 0x7f080188

.field public static final menu_show_picture:I = 0x7f080189

.field public static final menu_show_pinned:I = 0x7f08018a

.field public static final menu_show_punch:I = 0x7f08018b

.field public static final menu_show_recent:I = 0x7f08018c

.field public static final menu_show_recently_edited_by_me:I = 0x7f08018e

.field public static final menu_show_recently_opened_by_me:I = 0x7f08018d

.field public static final menu_show_starred:I = 0x7f08018f

.field public static final menu_show_trash:I = 0x7f080190

.field public static final menu_show_trix:I = 0x7f080191

.field public static final menu_sort_by:I = 0x7f080192

.field public static final menu_sort_last_modified:I = 0x7f080193

.field public static final menu_sort_recently_edited:I = 0x7f080194

.field public static final menu_sort_recently_opened:I = 0x7f080195

.field public static final menu_sort_title:I = 0x7f080196

.field public static final menu_star:I = 0x7f080197

.field public static final menu_switch_account:I = 0x7f080198

.field public static final menu_unpin:I = 0x7f080199

.field public static final menubar_fonts:I = 0x7f080077

.field public static final message_loading:I = 0x7f08005d

.field public static final migration_checkout_drive_message_drivev2:I = 0x7f080279

.field public static final migration_checkout_drive_negative_button_drivev2:I = 0x7f08027a

.field public static final migration_checkout_drive_positive_button_drivev2:I = 0x7f08027b

.field public static final migration_checkout_drive_title_drivev2:I = 0x7f08027c

.field public static final move:I = 0x7f08019a

.field public static final move_confirm_dialog_title:I = 0x7f08019b

.field public static final move_multi_parent_file:I = 0x7f08019c

.field public static final move_multi_parent_file_drive:I = 0x7f08026d

.field public static final move_multi_parent_folder:I = 0x7f08019d

.field public static final move_multi_parent_folder_drive:I = 0x7f08026e

.field public static final move_shared_to_shared:I = 0x7f08019e

.field public static final move_shared_to_unshared:I = 0x7f08019f

.field public static final move_toast_no_source_folder:I = 0x7f0801a0

.field public static final move_toast_with_source_folder:I = 0x7f0801a1

.field public static final move_unshared_to_shared:I = 0x7f0801a2

.field public static final multiple_collaborator_viewing:I = 0x7f08004d

.field public static final navigation_all_items:I = 0x7f08028e

.field public static final navigation_filter:I = 0x7f080292

.field public static final navigation_home:I = 0x7f080293

.field public static final navigation_my_collections:I = 0x7f080294

.field public static final navigation_pinned:I = 0x7f080295

.field public static final navigation_search_results:I = 0x7f080296

.field public static final navigation_shared_collections:I = 0x7f080297

.field public static final new_account:I = 0x7f0801a3

.field public static final new_folder_title:I = 0x7f0801a4

.field public static final new_kix_title:I = 0x7f0801a5

.field public static final new_trix_title:I = 0x7f0801a6

.field public static final no_account_support_this_upload:I = 0x7f0801a7

.field public static final notification_extra_text_is_too_long:I = 0x7f0801a8

.field public static final notification_welcome_to_drive:I = 0x7f08027d

.field public static final numbered_list:I = 0x7f080080

.field public static final ocr_image_description_drivev2:I = 0x7f08027e

.field public static final ocr_image_title_drivev2:I = 0x7f08027f

.field public static final offline:I = 0x7f08005e

.field public static final older:I = 0x7f0801a9

.field public static final one_collaborator_viewing:I = 0x7f08004c

.field public static final open_document_failed:I = 0x7f08004e

.field public static final open_document_failed_expanded:I = 0x7f08004f

.field public static final open_link:I = 0x7f080056

.field public static final open_pinned_version:I = 0x7f0801aa

.field public static final open_url_authentication_error:I = 0x7f0801ab

.field public static final open_url_io_error:I = 0x7f0801ac

.field public static final open_with_dialog_title:I = 0x7f0801ad

.field public static final open_with_native_drive_app_item_subtitle:I = 0x7f0801ae

.field public static final open_with_web_app_item_subtitle:I = 0x7f0801af

.field public static final opened_document:I = 0x7f0801b0

.field public static final opening_document:I = 0x7f0801b1

.field public static final opening_in_app_drivev2:I = 0x7f080280

.field public static final operation_on_starring_failed:I = 0x7f0801b3

.field public static final operation_on_starring_failed_as_offline:I = 0x7f0801b2

.field public static final operation_retry_error:I = 0x7f0801b6

.field public static final operation_retry_exceeded_message:I = 0x7f0801b7

.field public static final operation_retry_exceeded_retry:I = 0x7f0801b8

.field public static final operation_sync_error:I = 0x7f0801b4

.field public static final operation_sync_network_error:I = 0x7f0801b5

.field public static final ouch_button_close:I = 0x7f0801b9

.field public static final ouch_button_close_alert:I = 0x7f0801ba

.field public static final ouch_button_report:I = 0x7f0801bb

.field public static final ouch_msg_sync_error_drivev2:I = 0x7f080281

.field public static final ouch_msg_unhandled_exception_drive:I = 0x7f08026f

.field public static final ouch_msg_unhandled_exception_drivev2:I = 0x7f080282

.field public static final ouch_title_sawwrie:I = 0x7f0801bc

.field public static final owned_by_me:I = 0x7f0801bd

.field public static final page_counter_format:I = 0x7f08012f

.field public static final paste:I = 0x7f080061

.field public static final pasteDisabled:I = 0x7f080001

.field public static final perm_sync_status:I = 0x7f0801c0

.field public static final perm_sync_status_desc:I = 0x7f0801c1

.field public static final permsearch:I = 0x7f0801be

.field public static final permsearch_desc:I = 0x7f0801bf

.field public static final pin_checkbox_label:I = 0x7f0801c2

.field public static final pin_encryption_continue:I = 0x7f0801c5

.field public static final pin_encryption_message:I = 0x7f0801c7

.field public static final pin_encryption_title:I = 0x7f0801c6

.field public static final pin_error_external_storage_not_ready:I = 0x7f0801c3

.field public static final pin_external_storage_not_ready:I = 0x7f0801c8

.field public static final pin_free_storage:I = 0x7f0801c4

.field public static final pin_mode_done:I = 0x7f0801c9

.field public static final pin_not_available:I = 0x7f0801cb

.field public static final pin_notification_sync_fail:I = 0x7f0801ca

.field public static final pin_offline:I = 0x7f0801cc

.field public static final pin_out_of_date:I = 0x7f0801cd

.field public static final pin_pending:I = 0x7f0801ce

.field public static final pin_progress_known:I = 0x7f0801d2

.field public static final pin_status:I = 0x7f0801d3

.field public static final pin_sync_broadband_warning_do_not_show_again:I = 0x7f0801cf

.field public static final pin_sync_broadband_warning_message:I = 0x7f0801d0

.field public static final pin_sync_broadband_warning_update_files:I = 0x7f0801d1

.field public static final pin_up_to_date:I = 0x7f0801d4

.field public static final pin_update:I = 0x7f0801d5

.field public static final pin_waiting:I = 0x7f0801d6

.field public static final pin_warning_external_storage_not_ready:I = 0x7f0801d7

.field public static final pin_warning_external_storage_not_ready_ok:I = 0x7f0801d8

.field public static final prefs_about:I = 0x7f0801db

.field public static final prefs_about_category_title_all_apps:I = 0x7f080270

.field public static final prefs_additional_filters:I = 0x7f0801d9

.field public static final prefs_additional_filters_summary:I = 0x7f0801da

.field public static final prefs_cache_category_title:I = 0x7f0801dc

.field public static final prefs_cache_size:I = 0x7f0801dd

.field public static final prefs_cache_size_choice_format_string:I = 0x7f0801de

.field public static final prefs_cache_size_summary_format_string:I = 0x7f0801df

.field public static final prefs_clear_cache_summary:I = 0x7f0801e0

.field public static final prefs_enable_pin_encryption_summary:I = 0x7f0801e1

.field public static final prefs_enable_pin_encryption_title:I = 0x7f0801e2

.field public static final prefs_encryption_category:I = 0x7f0801e3

.field public static final prefs_legal:I = 0x7f0801ea

.field public static final prefs_notify_category_title:I = 0x7f0801e4

.field public static final prefs_notify_newdoc:I = 0x7f0801eb

.field public static final prefs_notify_newdoc_summary:I = 0x7f0801ec

.field public static final prefs_offline_category:I = 0x7f0801ed

.field public static final prefs_offline_data_usage_reminder_summary:I = 0x7f0801ee

.field public static final prefs_offline_data_usage_reminder_title:I = 0x7f0801f6

.field public static final prefs_pinned_files_auto_sync_options_always:I = 0x7f0801ef

.field public static final prefs_pinned_files_auto_sync_options_never:I = 0x7f0801f0

.field public static final prefs_pinned_files_auto_sync_options_wifi:I = 0x7f0801f1

.field public static final prefs_pinned_files_auto_sync_summary_always:I = 0x7f0801f2

.field public static final prefs_pinned_files_auto_sync_summary_disable:I = 0x7f0801f3

.field public static final prefs_pinned_files_auto_sync_summary_wifi:I = 0x7f0801f4

.field public static final prefs_pinned_files_auto_sync_title:I = 0x7f0801f5

.field public static final prefs_storage_add_summary:I = 0x7f0801e5

.field public static final prefs_storage_add_title:I = 0x7f0801e6

.field public static final prefs_storage_category_title:I = 0x7f0801e7

.field public static final prefs_streaming_decryption_summary:I = 0x7f0801e9

.field public static final prefs_streaming_decryption_title:I = 0x7f0801e8

.field public static final prefs_title_all_apps:I = 0x7f080271

.field public static final prefs_title_drivev2:I = 0x7f080283

.field public static final preview_general_info_modified:I = 0x7f080149

.field public static final preview_general_info_modified_by_me:I = 0x7f08014a

.field public static final preview_general_info_opened_by_me:I = 0x7f08014b

.field public static final preview_general_info_title:I = 0x7f080148

.field public static final preview_offline_title:I = 0x7f08014c

.field public static final preview_sharing_title:I = 0x7f08014d

.field public static final print_error:I = 0x7f080262

.field public static final punch_approximate_transitions_notification:I = 0x7f0801f7

.field public static final punch_drag_knob_button_content_description_close_slide_picker:I = 0x7f0801f8

.field public static final punch_drag_knob_button_content_description_open_slide_picker:I = 0x7f0801f9

.field public static final punch_missing_features_notification:I = 0x7f0801fa

.field public static final punch_no_title_slide_content_description:I = 0x7f0801fc

.field public static final punch_open_failed:I = 0x7f0801fd

.field public static final punch_open_failed_expanded:I = 0x7f0801fe

.field public static final punch_speaker_notes_absent:I = 0x7f0801fb

.field public static final punch_speaker_notes_close_button_content_description:I = 0x7f0801ff

.field public static final punch_speaker_notes_present_content_description:I = 0x7f080235

.field public static final punch_which_slide_is_displayed:I = 0x7f080200

.field public static final redo:I = 0x7f08007b

.field public static final rename_collection:I = 0x7f080201

.field public static final rename_collection_drive:I = 0x7f080272

.field public static final rename_document:I = 0x7f080202

.field public static final saved:I = 0x7f080063

.field public static final saving:I = 0x7f080062

.field public static final search_description_drivev2:I = 0x7f080284

.field public static final search_hint:I = 0x7f080203

.field public static final search_in_progress:I = 0x7f080204

.field public static final search_showing_local_results_only:I = 0x7f080205

.field public static final selectAll:I = 0x7f080052

.field public static final select_account:I = 0x7f080206

.field public static final share:I = 0x7f080207

.field public static final shared_with_me:I = 0x7f080208

.field public static final sharing_activity_title:I = 0x7f080209

.field public static final sharing_cannot_change:I = 0x7f08020a

.field public static final sharing_cannot_change_option:I = 0x7f08020b

.field public static final sharing_cannot_change_owner:I = 0x7f08020c

.field public static final sharing_error:I = 0x7f08020d

.field public static final sharing_list_may_not_be_completed:I = 0x7f08020e

.field public static final sharing_list_offline:I = 0x7f08020f

.field public static final sharing_message_saved:I = 0x7f080210

.field public static final sharing_message_saved_no_list:I = 0x7f080211

.field public static final sharing_message_saving:I = 0x7f080212

.field public static final sharing_message_unable_to_change:I = 0x7f080213

.field public static final sharing_option_anyone:I = 0x7f080214

.field public static final sharing_option_anyone_can_comment:I = 0x7f080215

.field public static final sharing_option_anyone_can_edit:I = 0x7f080216

.field public static final sharing_option_anyone_can_view:I = 0x7f080217

.field public static final sharing_option_anyone_description:I = 0x7f080218

.field public static final sharing_option_anyone_from:I = 0x7f080219

.field public static final sharing_option_anyone_from_can_comment:I = 0x7f08021a

.field public static final sharing_option_anyone_from_can_edit:I = 0x7f08021b

.field public static final sharing_option_anyone_from_can_view:I = 0x7f08021c

.field public static final sharing_option_anyone_from_description:I = 0x7f08021d

.field public static final sharing_option_anyone_from_with_link:I = 0x7f080223

.field public static final sharing_option_anyone_from_with_link_can_comment:I = 0x7f080224

.field public static final sharing_option_anyone_from_with_link_can_edit:I = 0x7f080225

.field public static final sharing_option_anyone_from_with_link_can_view:I = 0x7f080226

.field public static final sharing_option_anyone_from_with_link_description:I = 0x7f080227

.field public static final sharing_option_anyone_with_link:I = 0x7f08021e

.field public static final sharing_option_anyone_with_link_can_comment:I = 0x7f08021f

.field public static final sharing_option_anyone_with_link_can_edit:I = 0x7f080220

.field public static final sharing_option_anyone_with_link_can_view:I = 0x7f080221

.field public static final sharing_option_anyone_with_link_description:I = 0x7f080222

.field public static final sharing_option_private:I = 0x7f080228

.field public static final sharing_option_unknown:I = 0x7f080229

.field public static final sharing_role_commenter:I = 0x7f08022a

.field public static final sharing_role_no_access:I = 0x7f08022b

.field public static final sharing_role_owner:I = 0x7f08022c

.field public static final sharing_role_reader:I = 0x7f08022d

.field public static final sharing_role_unknown:I = 0x7f08022e

.field public static final sharing_role_writer:I = 0x7f08022f

.field public static final slider_title_all_items:I = 0x7f080230

.field public static final slider_title_home:I = 0x7f080231

.field public static final slider_title_owned_by_me:I = 0x7f080232

.field public static final slider_title_pinned:I = 0x7f080233

.field public static final slider_title_starred:I = 0x7f080234

.field public static final status_deleting:I = 0x7f080236

.field public static final status_syncing:I = 0x7f080237

.field public static final sync_more:I = 0x7f080238

.field public static final sync_more_before_template:I = 0x7f080239

.field public static final sync_more_error:I = 0x7f08023b

.field public static final sync_more_in_progress:I = 0x7f08023a

.field public static final sync_new_docs:I = 0x7f08023e

.field public static final sync_waiting:I = 0x7f08023c

.field public static final sync_waiting_subtitle:I = 0x7f08023d

.field public static final terms_and_privacy:I = 0x7f08023f

.field public static final terms_of_service:I = 0x7f080240

.field public static final terms_of_service_accept:I = 0x7f080241

.field public static final terms_of_service_decline:I = 0x7f080242

.field public static final terms_of_service_dialog_text_all_apps:I = 0x7f080273

.field public static final terms_of_service_url_drivev2:I = 0x7f080285

.field public static final textSelectionCABTitle:I = 0x7f080000

.field public static final text_alignment:I = 0x7f080089

.field public static final text_background_color:I = 0x7f080084

.field public static final text_color:I = 0x7f080083

.field public static final text_font_family:I = 0x7f080086

.field public static final text_font_size:I = 0x7f080087

.field public static final text_font_style:I = 0x7f080085

.field public static final this_month:I = 0x7f080243

.field public static final this_week:I = 0x7f080244

.field public static final this_year:I = 0x7f080245

.field public static final thumbnail_open:I = 0x7f080246

.field public static final title_grouper_files:I = 0x7f080286

.field public static final titlebar_documents:I = 0x7f080247

.field public static final titlebar_media:I = 0x7f080248

.field public static final today:I = 0x7f080249

.field public static final trix_clear_content:I = 0x7f0800c7

.field public static final trix_delete:I = 0x7f0800c8

.field public static final trix_insert:I = 0x7f0800c4

.field public static final trix_insert_above:I = 0x7f0800cb

.field public static final trix_insert_below:I = 0x7f0800cc

.field public static final trix_insert_left:I = 0x7f0800c9

.field public static final trix_insert_right:I = 0x7f0800ca

.field public static final trix_keyboard_bar_go_left:I = 0x7f0800b5

.field public static final trix_keyboard_bar_go_right:I = 0x7f0800b4

.field public static final trix_keyboard_bar_toggle_number_mode:I = 0x7f0800b3

.field public static final trix_keyboard_bar_toggle_text_mode:I = 0x7f0800b2

.field public static final trix_loading:I = 0x7f0800ce

.field public static final trix_sheets_default_title:I = 0x7f0800b6

.field public static final trix_sheets_tab_add_description:I = 0x7f0800c0

.field public static final trix_sheets_tab_add_symbol:I = 0x7f0800bf

.field public static final trix_sheets_tab_menu_cancel:I = 0x7f0800be

.field public static final trix_sheets_tab_menu_change_name:I = 0x7f0800bc

.field public static final trix_sheets_tab_menu_close:I = 0x7f0800c2

.field public static final trix_sheets_tab_menu_delete:I = 0x7f0800b7

.field public static final trix_sheets_tab_menu_description:I = 0x7f0800c1

.field public static final trix_sheets_tab_menu_duplicate:I = 0x7f0800b8

.field public static final trix_sheets_tab_menu_move_left:I = 0x7f0800bb

.field public static final trix_sheets_tab_menu_move_right:I = 0x7f0800ba

.field public static final trix_sheets_tab_menu_ok:I = 0x7f0800bd

.field public static final trix_sheets_tab_menu_rename:I = 0x7f0800b9

.field public static final trix_sort:I = 0x7f0800c3

.field public static final trix_sort_ascending:I = 0x7f0800c5

.field public static final trix_sort_descending:I = 0x7f0800c6

.field public static final underline:I = 0x7f08007e

.field public static final undo:I = 0x7f08007a

.field public static final uneditable_drawing:I = 0x7f08006f

.field public static final uneditable_equation:I = 0x7f080070

.field public static final uneditable_item:I = 0x7f080071

.field public static final uneditable_table:I = 0x7f08006e

.field public static final unsaved_dialog_cancel:I = 0x7f08006b

.field public static final unsaved_dialog_discard:I = 0x7f08006a

.field public static final unsaved_dialog_message:I = 0x7f080069

.field public static final unsaved_dialog_title:I = 0x7f080068

.field public static final upload_account:I = 0x7f08024a

.field public static final upload_cancel:I = 0x7f08024b

.field public static final upload_conversion_options:I = 0x7f08024c

.field public static final upload_convert_image_drivev2:I = 0x7f080287

.field public static final upload_document_title:I = 0x7f08024d

.field public static final upload_folder:I = 0x7f08024e

.field public static final upload_list_item_canceled:I = 0x7f08024f

.field public static final upload_list_item_completed:I = 0x7f080250

.field public static final upload_list_item_failed:I = 0x7f080251

.field public static final upload_multiple_document_titles:I = 0x7f080252

.field public static final upload_notification_failure_ticker:I = 0x7f080253

.field public static final upload_notification_failure_title:I = 0x7f080254

.field public static final upload_notification_started_ticker_drivev2:I = 0x7f080288

.field public static final upload_notification_started_title_drivev2:I = 0x7f080289

.field public static final upload_notification_success_ticker:I = 0x7f080255

.field public static final upload_notification_success_title_drivev2:I = 0x7f08028a

.field public static final upload_ok:I = 0x7f080256

.field public static final upload_queue_activity_cancel_upload:I = 0x7f080257

.field public static final upload_queue_failed_to_start:I = 0x7f080258

.field public static final upload_queue_list_empty:I = 0x7f080259

.field public static final upload_queue_title_bar:I = 0x7f08025a

.field public static final upload_select_account:I = 0x7f08025b

.field public static final upload_shared_item_title_convert_drivev2:I = 0x7f08028c

.field public static final upload_shared_item_title_drivev2:I = 0x7f08028b

.field public static final upload_untitled_file_title:I = 0x7f08025c

.field public static final version_too_old:I = 0x7f080065

.field public static final version_too_old_close:I = 0x7f080066

.field public static final version_too_old_drivev2:I = 0x7f08028d

.field public static final version_too_old_title:I = 0x7f080064

.field public static final version_too_old_upgrade:I = 0x7f080067

.field public static final widget_account_missing:I = 0x7f08025d

.field public static final widget_configuration_missing:I = 0x7f08025e

.field public static final widget_initializing:I = 0x7f08025f

.field public static final write_access_denied:I = 0x7f080260

.field public static final yesterday:I = 0x7f080261


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
