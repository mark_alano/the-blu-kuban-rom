.class public LfA;
.super Ljava/lang/Object;
.source "DefaultTitleBarListener.java"

# interfaces
.implements LNa;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final a:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 18
    const-class v0, LfA;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LfA;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .registers 2
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, LfA;->a:Landroid/app/Activity;

    .line 24
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 37
    iget-object v0, p0, LfA;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onSearchRequested()Z

    .line 38
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 28
    if-nez p1, :cond_a

    .line 29
    sget-object v0, LfA;->a:Ljava/lang/String;

    const-string v1, "null accountName"

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    :goto_9
    return-void

    .line 31
    :cond_a
    iget-object v0, p0, LfA;->a:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_9
.end method

.method public b()V
    .registers 1

    .prologue
    .line 43
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, LfA;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;

    if-nez v0, :cond_c

    iget-object v0, p0, LfA;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/docs/app/tablet/TabletHomeActivity;

    if-eqz v0, :cond_d

    .line 53
    :cond_c
    :goto_c
    return-void

    .line 50
    :cond_d
    iget-object v0, p0, LfA;->a:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 52
    iget-object v1, p0, LfA;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_c
.end method

.method public c(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 61
    invoke-virtual {p0, p1}, LfA;->b(Ljava/lang/String;)V

    .line 62
    return-void
.end method
