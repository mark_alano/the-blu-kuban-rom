.class public enum LfS;
.super Ljava/lang/Enum;
.source "DocumentOpenMethod.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LfS;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LfS;

.field private static final synthetic a:[LfS;

.field public static final enum b:LfS;

.field public static final enum c:LfS;


# instance fields
.field public final a:I

.field private final a:LUK;

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, LfS;

    const-string v1, "OPEN"

    sget-object v3, LUK;->a:LUK;

    const-string v4, "android.intent.action.VIEW"

    sget v5, Len;->document_preparing_to_open_progress:I

    invoke-direct/range {v0 .. v5}, LfS;-><init>(Ljava/lang/String;ILUK;Ljava/lang/String;I)V

    sput-object v0, LfS;->a:LfS;

    .line 22
    new-instance v3, LfS;

    const-string v4, "OPEN_WITH"

    sget-object v6, LUK;->b:LUK;

    const-string v7, "android.intent.action.VIEW"

    sget v8, Len;->document_preparing_to_open_progress:I

    move v5, v9

    invoke-direct/range {v3 .. v8}, LfS;-><init>(Ljava/lang/String;ILUK;Ljava/lang/String;I)V

    sput-object v3, LfS;->b:LfS;

    .line 23
    new-instance v3, LfT;

    const-string v4, "SEND"

    sget-object v6, LUK;->b:LUK;

    const-string v7, "android.intent.action.SEND"

    sget v8, Len;->document_preparing_to_send_progress:I

    move v5, v10

    invoke-direct/range {v3 .. v8}, LfT;-><init>(Ljava/lang/String;ILUK;Ljava/lang/String;I)V

    sput-object v3, LfS;->c:LfS;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [LfS;

    sget-object v1, LfS;->a:LfS;

    aput-object v1, v0, v2

    sget-object v1, LfS;->b:LfS;

    aput-object v1, v0, v9

    sget-object v1, LfS;->c:LfS;

    aput-object v1, v0, v10

    sput-object v0, LfS;->a:[LfS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILUK;Ljava/lang/String;I)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LUK;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput-object p3, p0, LfS;->a:LUK;

    .line 54
    iput-object p4, p0, LfS;->a:Ljava/lang/String;

    .line 55
    iput p5, p0, LfS;->a:I

    .line 56
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILUK;Ljava/lang/String;ILfT;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct/range {p0 .. p5}, LfS;-><init>(Ljava/lang/String;ILUK;Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LfS;
    .registers 2
    .parameter

    .prologue
    .line 20
    const-class v0, LfS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LfS;

    return-object v0
.end method

.method public static values()[LfS;
    .registers 1

    .prologue
    .line 20
    sget-object v0, LfS;->a:[LfS;

    invoke-virtual {v0}, [LfS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LfS;

    return-object v0
.end method


# virtual methods
.method public final a(LkP;)LUK;
    .registers 4
    .parameter

    .prologue
    .line 81
    sget-object v0, LkP;->a:LkP;

    sget-object v1, LkP;->d:LkP;

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 82
    iget-object v0, p0, LfS;->a:LUK;

    .line 84
    :goto_10
    return-object v0

    :cond_11
    sget-object v0, LUK;->a:LUK;

    goto :goto_10
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LfS;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 74
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    invoke-virtual {p0, v0, p1}, LfS;->a(Landroid/content/Intent;Landroid/net/Uri;)V

    .line 76
    const/high16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 77
    return-object v0
.end method

.method public a(Landroid/content/Intent;Landroid/net/Uri;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    return-void
.end method
