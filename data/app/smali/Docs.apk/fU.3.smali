.class public LfU;
.super Ljava/lang/Object;
.source "DocumentOpenerActivity.java"

# interfaces
.implements LamE;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LamE",
        "<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

.field final synthetic a:LkB;

.field final synthetic a:LkM;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;LkB;LkM;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 291
    iput-object p1, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    iput-object p2, p0, LfU;->a:LkB;

    iput-object p3, p0, LfU;->a:LkM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Boolean;)V
    .registers 6
    .parameter

    .prologue
    .line 308
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 309
    iget-object v0, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;)V

    .line 310
    iget-object v0, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    iget-object v1, p0, LfU;->a:LkB;

    iget-object v2, p0, LfU;->a:LkM;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;LkB;LkO;)Z

    .line 311
    iget-object v0, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LeQ;

    const-string v1, "docList"

    const-string v2, "documentOpened"

    iget-object v3, p0, LfU;->a:LkM;

    invoke-virtual {v3}, LkM;->a()LkP;

    move-result-object v3

    invoke-virtual {v3}, LkP;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    :goto_29
    return-void

    .line 315
    :cond_2a
    iget-object v0, p0, LfU;->a:LkM;

    invoke-virtual {v0}, LkM;->g()Z

    move-result v0

    if-eqz v0, :cond_44

    iget-object v0, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LaaZ;

    invoke-interface {v0}, LaaZ;->a()Z

    move-result v0

    if-nez v0, :cond_44

    .line 316
    iget-object v0, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    sget-object v1, Lpc;->e:Lpc;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Lpc;)V

    goto :goto_29

    .line 318
    :cond_44
    iget-object v0, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    sget-object v1, Lpc;->c:Lpc;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;Lpc;)V

    goto :goto_29
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 291
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, LfU;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method public a(Ljava/lang/Throwable;)V
    .registers 4
    .parameter

    .prologue
    .line 294
    const-string v0, "DocumentOpenerActivity"

    const-string v1, "Abort open document"

    invoke-static {v0, v1, p1}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 295
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-eqz v0, :cond_11

    .line 297
    iget-object v0, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    .line 304
    :goto_10
    return-void

    .line 298
    :cond_11
    instance-of v0, p1, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_1b

    .line 300
    iget-object v0, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->finish()V

    goto :goto_10

    .line 302
    :cond_1b
    iget-object v0, p0, LfU;->a:Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    sget-object v1, Lpc;->h:Lpc;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_10
.end method
