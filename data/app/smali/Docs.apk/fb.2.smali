.class public abstract enum Lfb;
.super Ljava/lang/Enum;
.source "ApplicationMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lfb;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lfb;

.field private static final synthetic a:[Lfb;

.field public static final enum b:Lfb;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LmK;

.field private final b:I

.field private final b:LmK;

.field private final c:I

.field private final d:I

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .registers 15

    .prologue
    const/4 v14, 0x2

    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 19
    new-instance v0, Lfc;

    const-string v1, "DOCS"

    const-class v3, Lcom/google/android/apps/docs/app/NewMainProxyActivity;

    sget v4, Leg;->launcher_docs_icon:I

    sget-object v6, LmK;->b:LmK;

    sget-object v7, LmK;->a:LmK;

    sget v8, Leg;->top_bar_search:I

    sget v9, Len;->app_docs_name:I

    sget v10, Leg;->ic_type_folder:I

    invoke-direct/range {v0 .. v10}, Lfc;-><init>(Ljava/lang/String;ILjava/lang/Class;IILmK;LmK;III)V

    sput-object v0, Lfb;->a:Lfb;

    .line 27
    new-instance v3, Lfd;

    const-string v4, "DRIVE"

    const-class v6, Lcom/google/android/apps/docs/app/MainDriveProxyActivity;

    sget v7, Leg;->launcher_drive_icon:I

    sget-object v9, LmK;->v:LmK;

    sget-object v10, LmK;->v:LmK;

    const v11, 0x108004f

    sget v12, Len;->app_drive_name:I

    sget v13, Leg;->ic_drive_my_drive:I

    move v8, v14

    invoke-direct/range {v3 .. v13}, Lfd;-><init>(Ljava/lang/String;ILjava/lang/Class;IILmK;LmK;III)V

    sput-object v3, Lfb;->b:Lfb;

    .line 18
    new-array v0, v14, [Lfb;

    sget-object v1, Lfb;->a:Lfb;

    aput-object v1, v0, v2

    sget-object v1, Lfb;->b:Lfb;

    aput-object v1, v0, v5

    sput-object v0, Lfb;->a:[Lfb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;IILmK;LmK;III)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;II",
            "LmK;",
            "LmK;",
            "III)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput-object p3, p0, Lfb;->a:Ljava/lang/Class;

    .line 49
    iput p4, p0, Lfb;->a:I

    .line 50
    iput p5, p0, Lfb;->b:I

    .line 51
    iput-object p6, p0, Lfb;->a:LmK;

    .line 52
    iput-object p7, p0, Lfb;->b:LmK;

    .line 53
    iput p8, p0, Lfb;->c:I

    .line 54
    iput p9, p0, Lfb;->d:I

    .line 55
    iput p10, p0, Lfb;->e:I

    .line 56
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/Class;IILmK;LmK;IIILfc;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct/range {p0 .. p10}, Lfb;-><init>(Ljava/lang/String;ILjava/lang/Class;IILmK;LmK;III)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfb;
    .registers 2
    .parameter

    .prologue
    .line 18
    const-class v0, Lfb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfb;

    return-object v0
.end method

.method public static values()[Lfb;
    .registers 1

    .prologue
    .line 18
    sget-object v0, Lfb;->a:[Lfb;

    invoke-virtual {v0}, [Lfb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfb;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 73
    iget v0, p0, Lfb;->a:I

    return v0
.end method

.method public abstract a()Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lrl;",
            ">;"
        }
    .end annotation
.end method

.method public a()LmK;
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lfb;->a:LmK;

    return-object v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 82
    iget v0, p0, Lfb;->b:I

    return v0
.end method

.method public b()LmK;
    .registers 2

    .prologue
    .line 110
    iget-object v0, p0, Lfb;->b:LmK;

    return-object v0
.end method

.method public c()I
    .registers 2

    .prologue
    .line 96
    iget v0, p0, Lfb;->c:I

    return v0
.end method

.method public d()I
    .registers 2

    .prologue
    .line 103
    iget v0, p0, Lfb;->d:I

    return v0
.end method

.method public e()I
    .registers 2

    .prologue
    .line 117
    iget v0, p0, Lfb;->e:I

    return v0
.end method
