.class public Lfg;
.super Ljava/lang/Object;
.source "ApplicationStringHelper.java"


# instance fields
.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laoz;)V
    .registers 3
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Lfb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lfh;

    invoke-direct {v0, p0}, Lfh;-><init>(Lfg;)V

    iput-object v0, p0, Lfg;->a:Ljava/util/Map;

    .line 22
    iput-object p1, p0, Lfg;->a:Laoz;

    .line 23
    return-void
.end method


# virtual methods
.method public a(I)I
    .registers 4
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lfg;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 53
    sget-object v1, Lfb;->b:Lfb;

    if-ne v0, v1, :cond_1e

    .line 54
    iget-object v0, p0, Lfg;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 55
    if-eqz v0, :cond_1e

    .line 56
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 60
    :cond_1e
    return p1
.end method
