.class Lfz;
.super Ljava/lang/Object;
.source "CreateNewDocActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lfw;


# direct methods
.method constructor <init>(Lfw;)V
    .registers 2
    .parameter

    .prologue
    .line 288
    iput-object p1, p0, Lfz;->a:Lfw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    .prologue
    .line 291
    iget-object v0, p0, Lfz;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ProgressDialog;

    .line 292
    if-eqz v0, :cond_17

    .line 293
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 294
    iget-object v0, p0, Lfz;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 297
    :cond_17
    iget-object v0, p0, Lfz;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Landroid/app/Activity;

    invoke-static {v0}, LZJ;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 298
    iget-object v0, p0, Lfz;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    iget-object v1, p0, Lfz;->a:Lfw;

    iget-object v1, v1, Lfw;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment$SimpleEntryCreator;)V

    .line 302
    :goto_2c
    return-void

    .line 300
    :cond_2d
    iget-object v0, p0, Lfz;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_2c
.end method
