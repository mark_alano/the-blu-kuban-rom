.class public final Lgr;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lin;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lil;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lgl;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lgb;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LeW;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lgd;",
            ">;"
        }
    .end annotation
.end field

.field public g:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field public h:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lfe;",
            ">;"
        }
    .end annotation
.end field

.field public i:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lim;",
            ">;"
        }
    .end annotation
.end field

.field public j:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LeX;",
            ">;"
        }
    .end annotation
.end field

.field public k:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lgo;",
            ">;"
        }
    .end annotation
.end field

.field public l:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lgc;",
            ">;"
        }
    .end annotation
.end field

.field public m:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lhx;",
            ">;"
        }
    .end annotation
.end field

.field public n:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Liq;",
            ">;"
        }
    .end annotation
.end field

.field public o:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lfg;",
            ">;"
        }
    .end annotation
.end field

.field public p:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lfo;",
            ">;"
        }
    .end annotation
.end field

.field public q:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<[",
            "Lii;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 47
    iput-object p1, p0, Lgr;->a:LYD;

    .line 48
    const-class v0, Lin;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->a:LZb;

    .line 51
    const-class v0, Lil;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->b:LZb;

    .line 54
    const-class v0, Lgl;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->c:LZb;

    .line 57
    const-class v0, Lgb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->d:LZb;

    .line 60
    const-class v0, LeW;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->e:LZb;

    .line 63
    const-class v0, Lgd;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->f:LZb;

    .line 66
    const-class v0, Lfb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->g:LZb;

    .line 69
    const-class v0, Lfe;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->h:LZb;

    .line 72
    const-class v0, Lim;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->i:LZb;

    .line 75
    const-class v0, LeX;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->j:LZb;

    .line 78
    const-class v0, Lgo;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->k:LZb;

    .line 81
    const-class v0, Lgc;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->l:LZb;

    .line 84
    const-class v0, Lhx;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->m:LZb;

    .line 87
    const-class v0, Liq;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->n:LZb;

    .line 90
    const-class v0, Lfg;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->o:LZb;

    .line 93
    const-class v0, Lfo;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->p:LZb;

    .line 96
    const-class v0, [Lii;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v2}, Lgr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lgr;->q:LZb;

    .line 99
    return-void
.end method

.method static synthetic a(Lgr;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lgr;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 950
    const-class v0, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;

    new-instance v1, Lgs;

    invoke-direct {v1, p0}, Lgs;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 958
    const-class v0, Lcom/google/android/apps/docs/app/CreateNewDocActivity;

    new-instance v1, LgD;

    invoke-direct {v1, p0}, LgD;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 966
    const-class v0, Lcom/google/android/apps/docs/app/MoveEntryActivity;

    new-instance v1, LgO;

    invoke-direct {v1, p0}, LgO;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 974
    const-class v0, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;

    new-instance v1, LgZ;

    invoke-direct {v1, p0}, LgZ;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 982
    const-class v0, Lcom/google/android/apps/docs/app/OcrCameraActivity;

    new-instance v1, Lha;

    invoke-direct {v1, p0}, Lha;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 990
    const-class v0, Lcom/google/android/apps/docs/app/PickEntryActivity;

    new-instance v1, Lhb;

    invoke-direct {v1, p0}, Lhb;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 998
    const-class v0, Lfe;

    new-instance v1, Lhc;

    invoke-direct {v1, p0}, Lhc;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1006
    const-class v0, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;

    new-instance v1, Lhd;

    invoke-direct {v1, p0}, Lhd;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1014
    const-class v0, Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;

    new-instance v1, Lhe;

    invoke-direct {v1, p0}, Lhe;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1022
    const-class v0, Lgq;

    new-instance v1, Lgt;

    invoke-direct {v1, p0}, Lgt;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1030
    const-class v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;

    new-instance v1, Lgu;

    invoke-direct {v1, p0}, Lgu;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1038
    const-class v0, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;

    new-instance v1, Lgv;

    invoke-direct {v1, p0}, Lgv;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1046
    const-class v0, Lcom/google/android/apps/docs/app/RetriesExceededActivity;

    new-instance v1, Lgw;

    invoke-direct {v1, p0}, Lgw;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1054
    const-class v0, Lcom/google/android/apps/docs/app/BaseActivity;

    new-instance v1, Lgx;

    invoke-direct {v1, p0}, Lgx;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1062
    const-class v0, Lcom/google/android/apps/docs/app/AccountsActivity;

    new-instance v1, Lgy;

    invoke-direct {v1, p0}, Lgy;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1070
    const-class v0, Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;

    new-instance v1, Lgz;

    invoke-direct {v1, p0}, Lgz;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1078
    const-class v0, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;

    new-instance v1, LgA;

    invoke-direct {v1, p0}, LgA;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1086
    const-class v0, Lcom/google/android/apps/docs/app/MainDriveProxyActivity;

    new-instance v1, LgB;

    invoke-direct {v1, p0}, LgB;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1094
    const-class v0, Lcom/google/android/apps/docs/app/CheckStatusActivity;

    new-instance v1, LgC;

    invoke-direct {v1, p0}, LgC;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1102
    const-class v0, Liq;

    new-instance v1, LgE;

    invoke-direct {v1, p0}, LgE;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1110
    const-class v0, Lcom/google/android/apps/docs/app/TestRoboFragmentActivity;

    new-instance v1, LgF;

    invoke-direct {v1, p0}, LgF;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1118
    const-class v0, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;

    new-instance v1, LgG;

    invoke-direct {v1, p0}, LgG;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1126
    const-class v0, Lcom/google/android/apps/docs/app/NewMainProxyActivity;

    new-instance v1, LgH;

    invoke-direct {v1, p0}, LgH;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1134
    const-class v0, Lgc;

    new-instance v1, LgI;

    invoke-direct {v1, p0}, LgI;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1142
    const-class v0, Lcom/google/android/apps/docs/app/BaseDialogActivity;

    new-instance v1, LgJ;

    invoke-direct {v1, p0}, LgJ;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1150
    const-class v0, Lcom/google/android/apps/docs/app/BaseDialogFragment;

    new-instance v1, LgK;

    invoke-direct {v1, p0}, LgK;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1158
    const-class v0, Lcom/google/android/apps/docs/app/CommentStreamActivity;

    new-instance v1, LgL;

    invoke-direct {v1, p0}, LgL;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1166
    const-class v0, Lcom/google/android/apps/docs/app/AccountListeningActivity;

    new-instance v1, LgM;

    invoke-direct {v1, p0}, LgM;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1174
    const-class v0, Lcom/google/android/apps/docs/app/ZippedKixOpenActivity;

    new-instance v1, LgN;

    invoke-direct {v1, p0}, LgN;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1182
    const-class v0, Lcom/google/android/apps/docs/app/InvitationActivity;

    new-instance v1, LgP;

    invoke-direct {v1, p0}, LgP;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1190
    const-class v0, Lcom/google/android/apps/docs/app/CreateShortcutActivity;

    new-instance v1, LgQ;

    invoke-direct {v1, p0}, LgQ;-><init>(Lgr;)V

    invoke-virtual {p0, v0, v1}, Lgr;->a(Ljava/lang/Class;Laou;)V

    .line 1198
    const-class v0, Lin;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->a:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1199
    const-class v0, Lil;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->b:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1200
    const-class v0, Lgl;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->c:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1201
    const-class v0, Lgb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->d:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1202
    const-class v0, LeW;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->e:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1203
    const-class v0, Lgd;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->f:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1204
    const-class v0, Lfb;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->g:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1205
    const-class v0, Lfe;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->h:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1206
    const-class v0, Lim;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->i:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1207
    const-class v0, LeX;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->j:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1208
    const-class v0, Lgo;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->k:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1209
    const-class v0, Lgc;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->l:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1210
    const-class v0, Lhx;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->m:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1211
    const-class v0, Liq;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->n:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1212
    const-class v0, Lfg;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->o:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1213
    const-class v0, Lfo;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->p:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1214
    const-class v0, [Lii;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lgr;->q:LZb;

    invoke-virtual {p0, v0, v1}, Lgr;->a(Laop;LZb;)V

    .line 1215
    iget-object v0, p0, Lgr;->a:LZb;

    iget-object v1, p0, Lgr;->a:LYD;

    iget-object v1, v1, LYD;->a:Lgr;

    iget-object v1, v1, Lgr;->n:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1217
    iget-object v0, p0, Lgr;->b:LZb;

    iget-object v1, p0, Lgr;->a:LYD;

    iget-object v1, v1, LYD;->a:Lgr;

    iget-object v1, v1, Lgr;->i:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1219
    iget-object v0, p0, Lgr;->c:LZb;

    iget-object v1, p0, Lgr;->a:LYD;

    iget-object v1, v1, LYD;->a:Lgr;

    iget-object v1, v1, Lgr;->k:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1221
    iget-object v0, p0, Lgr;->d:LZb;

    iget-object v1, p0, Lgr;->a:LYD;

    iget-object v1, v1, LYD;->a:Lgr;

    iget-object v1, v1, Lgr;->l:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1223
    iget-object v0, p0, Lgr;->e:LZb;

    iget-object v1, p0, Lgr;->a:LYD;

    iget-object v1, v1, LYD;->a:Lgr;

    iget-object v1, v1, Lgr;->j:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1225
    iget-object v0, p0, Lgr;->f:LZb;

    iget-object v1, p0, Lgr;->a:LYD;

    iget-object v1, v1, LYD;->a:Lju;

    iget-object v1, v1, Lju;->a:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1227
    iget-object v0, p0, Lgr;->g:LZb;

    iget-object v1, p0, Lgr;->a:LYD;

    iget-object v1, v1, LYD;->a:Lgr;

    iget-object v1, v1, Lgr;->h:LZb;

    invoke-static {v1}, Lgr;->a(Laoz;)Laoz;

    move-result-object v1

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1230
    iget-object v0, p0, Lgr;->h:LZb;

    new-instance v1, LgR;

    invoke-direct {v1, p0}, LgR;-><init>(Lgr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1241
    iget-object v0, p0, Lgr;->i:LZb;

    new-instance v1, LgS;

    invoke-direct {v1, p0}, LgS;-><init>(Lgr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1250
    iget-object v0, p0, Lgr;->j:LZb;

    new-instance v1, LgT;

    invoke-direct {v1, p0}, LgT;-><init>(Lgr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1264
    iget-object v0, p0, Lgr;->k:LZb;

    new-instance v1, LgU;

    invoke-direct {v1, p0}, LgU;-><init>(Lgr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1283
    iget-object v0, p0, Lgr;->l:LZb;

    new-instance v1, LgV;

    invoke-direct {v1, p0}, LgV;-><init>(Lgr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1294
    iget-object v0, p0, Lgr;->m:LZb;

    new-instance v1, LgW;

    invoke-direct {v1, p0}, LgW;-><init>(Lgr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1333
    iget-object v0, p0, Lgr;->n:LZb;

    new-instance v1, LgX;

    invoke-direct {v1, p0}, LgX;-><init>(Lgr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1354
    iget-object v0, p0, Lgr;->o:LZb;

    new-instance v1, LgY;

    invoke-direct {v1, p0}, LgY;-><init>(Lgr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 1368
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 493
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 495
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/AccountListeningActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 873
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogActivity;)V

    .line 875
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/AccountsActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 469
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 471
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LeQ;

    .line 477
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LME;

    .line 483
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/AccountsActivity;->a:LZM;

    .line 489
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/BaseActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 441
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->o:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfg;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/BaseActivity;->a:Lfg;

    .line 447
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeW;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/BaseActivity;->a:LeW;

    .line 453
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LMQ;

    iget-object v0, v0, LMQ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMO;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/BaseActivity;->a:LMO;

    .line 459
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/BaseActivity;->a:Laoz;

    .line 465
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/BaseDialogActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 803
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 805
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    iget-object v0, v0, Lob;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LnF;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/BaseDialogActivity;->a:LnF;

    .line 811
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 815
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->o:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfg;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:Lfg;

    .line 821
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:Laoz;

    .line 827
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LMQ;

    iget-object v0, v0, LMQ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMO;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/BaseDialogFragment;->a:LMO;

    .line 833
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/CheckStatusActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 587
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 589
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgb;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CheckStatusActivity;->a:Lgb;

    .line 595
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZl;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CheckStatusActivity;->a:LZl;

    .line 601
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->h:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfe;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CheckStatusActivity;->a:Lfe;

    .line 607
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->s:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWM;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CheckStatusActivity;->a:LWM;

    .line 613
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CheckStatusActivity;->a:LME;

    .line 619
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZj;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CheckStatusActivity;->a:LZj;

    .line 625
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/CommentStreamActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 837
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogActivity;)V

    .line 839
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Llf;

    .line 845
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lms;

    iget-object v0, v0, Lms;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlX;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LlX;

    .line 851
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Ld;

    iget-object v0, v0, Ld;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CommentStreamActivity;->b:Landroid/os/Handler;

    .line 857
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:LeQ;

    .line 863
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CommentStreamActivity;->a:Lgl;

    .line 869
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 119
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LZM;

    .line 125
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LVM;

    iget-object v0, v0, LVM;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LVK;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LVK;

    .line 131
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LeQ;

    .line 137
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:Llf;

    .line 143
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LWY;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CreateNewDocActivity;->a:LWY;

    .line 149
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/CreateShortcutActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 939
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/CreateShortcutActivity;->a:Llf;

    .line 945
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/DocsPreferencesActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 499
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Laoz;

    .line 505
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->o:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfg;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Lfg;

    .line 511
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LaaJ;

    .line 517
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Llf;

    .line 523
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LNl;

    iget-object v0, v0, LNl;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNe;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LNe;

    .line 529
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:Lgl;

    .line 535
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LVc;

    iget-object v0, v0, LVc;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUL;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LUL;

    .line 541
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lob;

    iget-object v0, v0, Lob;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LnF;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LnF;

    .line 547
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LMQ;

    iget-object v0, v0, LMQ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LMO;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LMO;

    .line 553
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LKS;

    .line 559
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LOU;

    iget-object v0, v0, LOU;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LOS;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LOS;

    .line 565
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZj;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LZj;

    .line 571
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocsPreferencesActivity;->a:LME;

    .line 577
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/DocumentOpenerActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 645
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 647
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lpu;

    iget-object v0, v0, Lpu;->x:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LpX;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LpX;

    .line 653
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lgl;

    .line 659
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LXu;

    iget-object v0, v0, LXu;->e:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LXS;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LXS;

    .line 665
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LeQ;

    .line 671
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LVc;

    iget-object v0, v0, LVc;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUL;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LUL;

    .line 677
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lpu;

    iget-object v0, v0, Lpu;->r:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpk;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lpk;

    .line 683
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlE;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LlE;

    .line 689
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LZM;

    .line 695
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lpu;

    iget-object v0, v0, Lpu;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LoZ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->b:LoZ;

    .line 701
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lpu;

    iget-object v0, v0, Lpu;->u:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqb;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Lqb;

    .line 707
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lpu;

    iget-object v0, v0, Lpu;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LoZ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LoZ;

    .line 713
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->i:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaZ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:LaaZ;

    .line 719
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/DocumentOpenerActivity;->a:Llf;

    .line 725
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/ErrorNotificationActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 107
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZR;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/ErrorNotificationActivity;->a:LZR;

    .line 113
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/HomeScreenActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 339
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogActivity;)V

    .line 341
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljo;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Ljo;

    .line 347
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LeQ;

    .line 353
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lkz;

    iget-object v0, v0, Lkz;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lky;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Lky;

    .line 359
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZl;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LZl;

    .line 365
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Llf;

    .line 371
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiG;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LiG;

    .line 377
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LZM;

    .line 383
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->q:LZb;

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->b:Laoz;

    .line 389
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LaaJ;

    .line 395
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lez;

    iget-object v0, v0, Lez;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lev;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:Lev;

    .line 401
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LME;

    .line 407
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/InvitationActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 885
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 887
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LZM;

    .line 893
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LPe;

    iget-object v0, v0, LPe;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPm;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LPm;

    .line 899
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LME;

    .line 905
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgb;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Lgb;

    .line 911
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LNl;

    iget-object v0, v0, LNl;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LNe;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LNe;

    .line 917
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LKS;

    .line 923
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/InvitationActivity;->a:Ljava/lang/Class;

    .line 929
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZl;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/InvitationActivity;->a:LZl;

    .line 935
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 299
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 301
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LVc;

    iget-object v0, v0, LVc;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LUL;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;->a:LUL;

    .line 307
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->g:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZS;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;->a:LZS;

    .line 313
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;->a:Llf;

    .line 319
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LaaJ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;->a:LaaJ;

    .line 325
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/MainDriveProxyActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 581
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V

    .line 583
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/MoveEntryActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 155
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:Llf;

    .line 161
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LZM;

    .line 167
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlE;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/MoveEntryActivity;->a:LlE;

    .line 173
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/NewMainProxyActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 729
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->m:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhx;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Lhx;

    .line 735
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LZM;

    .line 741
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Ljava/lang/Class;

    .line 747
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LMJ;

    iget-object v0, v0, LMJ;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LME;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LME;

    .line 753
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:LeQ;

    .line 759
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->h:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfe;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/NewMainProxyActivity;->a:Lfe;

    .line 765
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/OcrCameraActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 231
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 233
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a:LeQ;

    .line 239
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 271
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->g:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiT;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiT;

    .line 277
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lje;

    iget-object v0, v0, Lje;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiG;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LiG;

    .line 283
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LatG;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:LatG;

    .line 289
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llb;

    iget-object v0, v0, Llb;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llf;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/PickEntryActivity$PickEntryDialogFragment;->a:Llf;

    .line 295
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/PickEntryActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 243
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/AbstractModalDialogActivity;)V

    .line 245
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/RetriesExceededActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 423
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 425
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LOU;

    iget-object v0, v0, LOU;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPa;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a:LPa;

    .line 431
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Llm;

    iget-object v0, v0, Llm;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LlE;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/RetriesExceededActivity;->a:LlE;

    .line 437
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/TestRoboFragmentActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 639
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseActivity;)V

    .line 641
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/BaseDialogFragment;)V

    .line 179
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->h:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->m:I

    .line 185
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->g:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->o:I

    .line 191
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->k:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZM;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:LZM;

    .line 197
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:LKS;

    .line 203
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lin;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->a:Lin;

    .line 209
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->i:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->c:Ljava/lang/String;

    .line 215
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->j:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->p:I

    .line 221
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->f:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, Lcom/google/android/apps/docs/app/VersionCheckDialogFragment;->n:I

    .line 227
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/ZippedKixOpenActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 879
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;)V

    .line 881
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 411
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    invoke-virtual {v0, p1}, Lgr;->a(Lcom/google/android/apps/docs/app/LocalFileOpenerActivity;)V

    .line 413
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lcom/google/android/apps/docs/app/ZippedTrixOpenActivity;->a:LKS;

    .line 419
    return-void
.end method

.method public a(Lfe;)V
    .registers 3
    .parameter

    .prologue
    .line 249
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgb;

    iput-object v0, p1, Lfe;->a:Lgb;

    .line 255
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lfe;->a:Lgl;

    .line 261
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lfe;->a:Laoz;

    .line 267
    return-void
.end method

.method public a(Lgc;)V
    .registers 3
    .parameter

    .prologue
    .line 769
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LLi;

    iget-object v0, v0, LLi;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKS;

    iput-object v0, p1, Lgc;->a:LKS;

    .line 775
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LZX;

    iget-object v0, v0, LZX;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZj;

    iput-object v0, p1, Lgc;->a:LZj;

    .line 781
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lez;

    iget-object v0, v0, Lez;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lev;

    iput-object v0, p1, Lgc;->a:Lev;

    .line 787
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->c:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgl;

    iput-object v0, p1, Lgc;->a:Lgl;

    .line 793
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lgc;->a:Laoz;

    .line 799
    return-void
.end method

.method public a(Lgq;)V
    .registers 3
    .parameter

    .prologue
    .line 329
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:Lb;

    iget-object v0, v0, Lb;->a:LZb;

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lgq;->a:Laoz;

    .line 335
    return-void
.end method

.method public a(Liq;)V
    .registers 3
    .parameter

    .prologue
    .line 629
    iget-object v0, p0, Lgr;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->d:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p1, Liq;->a:Ljava/lang/String;

    .line 635
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 1372
    return-void
.end method
