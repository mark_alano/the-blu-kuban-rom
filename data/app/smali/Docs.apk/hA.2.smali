.class public final enum LhA;
.super Ljava/lang/Enum;
.source "MainProxyLogic.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LhA;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LhA;

.field private static final synthetic a:[LhA;

.field public static final enum b:LhA;

.field public static final enum c:LhA;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 114
    new-instance v0, LhA;

    const-string v1, "DOCS"

    invoke-direct {v0, v1, v2}, LhA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhA;->a:LhA;

    new-instance v0, LhA;

    const-string v1, "DRIVE"

    invoke-direct {v0, v1, v3}, LhA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhA;->b:LhA;

    .line 118
    new-instance v0, LhA;

    const-string v1, "DRIVE_ENABLED_FLAG"

    invoke-direct {v0, v1, v4}, LhA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhA;->c:LhA;

    .line 113
    const/4 v0, 0x3

    new-array v0, v0, [LhA;

    sget-object v1, LhA;->a:LhA;

    aput-object v1, v0, v2

    sget-object v1, LhA;->b:LhA;

    aput-object v1, v0, v3

    sget-object v1, LhA;->c:LhA;

    aput-object v1, v0, v4

    sput-object v0, LhA;->a:[LhA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LhA;
    .registers 2
    .parameter

    .prologue
    .line 113
    const-class v0, LhA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LhA;

    return-object v0
.end method

.method public static values()[LhA;
    .registers 1

    .prologue
    .line 113
    sget-object v0, LhA;->a:[LhA;

    invoke-virtual {v0}, [LhA;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LhA;

    return-object v0
.end method
