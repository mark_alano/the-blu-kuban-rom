.class public final enum LhE;
.super Ljava/lang/Enum;
.source "MainProxyLogic.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LhE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LhE;

.field private static final synthetic a:[LhE;

.field public static final enum b:LhE;

.field public static final enum c:LhE;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, LhE;

    const-string v1, "CHECK_DRIVE_ENABLE_STATUS"

    invoke-direct {v0, v1, v2}, LhE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhE;->a:LhE;

    .line 64
    new-instance v0, LhE;

    const-string v1, "INVITATION_FLOW"

    invoke-direct {v0, v1, v3}, LhE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhE;->b:LhE;

    .line 69
    new-instance v0, LhE;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v4}, LhE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhE;->c:LhE;

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [LhE;

    sget-object v1, LhE;->a:LhE;

    aput-object v1, v0, v2

    sget-object v1, LhE;->b:LhE;

    aput-object v1, v0, v3

    sget-object v1, LhE;->c:LhE;

    aput-object v1, v0, v4

    sput-object v0, LhE;->a:[LhE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LhE;
    .registers 2
    .parameter

    .prologue
    .line 62
    const-class v0, LhE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LhE;

    return-object v0
.end method

.method public static values()[LhE;
    .registers 1

    .prologue
    .line 62
    sget-object v0, LhE;->a:[LhE;

    invoke-virtual {v0}, [LhE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LhE;

    return-object v0
.end method
