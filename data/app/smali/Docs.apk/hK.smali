.class public abstract enum LhK;
.super Ljava/lang/Enum;
.source "MoveEntryActivity.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LhK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LhK;

.field private static final synthetic a:[LhK;

.field public static final enum b:LhK;

.field public static final enum c:LhK;

.field public static final enum d:LhK;

.field public static final enum e:LhK;

.field public static final enum f:LhK;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    new-instance v0, LhL;

    const-string v1, "VALIDATE_SELECTED_ENTRY"

    invoke-direct {v0, v1, v3}, LhL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhK;->a:LhK;

    .line 57
    new-instance v0, LhM;

    const-string v1, "LAUNCH_PICK_ENTRY_DIALOG"

    invoke-direct {v0, v1, v4}, LhM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhK;->b:LhK;

    .line 63
    new-instance v0, LhN;

    const-string v1, "SELECTING_TARGET"

    invoke-direct {v0, v1, v5}, LhN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhK;->c:LhK;

    .line 70
    new-instance v0, LhO;

    const-string v1, "WARNING_DIALOG"

    invoke-direct {v0, v1, v6}, LhO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhK;->d:LhK;

    .line 76
    new-instance v0, LhP;

    const-string v1, "PERFORM_MOVE"

    invoke-direct {v0, v1, v7}, LhP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhK;->e:LhK;

    .line 82
    new-instance v0, LhQ;

    const-string v1, "FINISH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LhQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LhK;->f:LhK;

    .line 50
    const/4 v0, 0x6

    new-array v0, v0, [LhK;

    sget-object v1, LhK;->a:LhK;

    aput-object v1, v0, v3

    sget-object v1, LhK;->b:LhK;

    aput-object v1, v0, v4

    sget-object v1, LhK;->c:LhK;

    aput-object v1, v0, v5

    sget-object v1, LhK;->d:LhK;

    aput-object v1, v0, v6

    sget-object v1, LhK;->e:LhK;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LhK;->f:LhK;

    aput-object v2, v0, v1

    sput-object v0, LhK;->a:[LhK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILhF;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, LhK;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LhK;
    .registers 2
    .parameter

    .prologue
    .line 50
    const-class v0, LhK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LhK;

    return-object v0
.end method

.method public static values()[LhK;
    .registers 1

    .prologue
    .line 50
    sget-object v0, LhK;->a:[LhK;

    invoke-virtual {v0}, [LhK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LhK;

    return-object v0
.end method


# virtual methods
.method abstract a(Lcom/google/android/apps/docs/app/MoveEntryActivity;)LhK;
.end method
