.class public LhY;
.super LbT;
.source "PickEntryActivity.java"


# instance fields
.field private a:Ljava/lang/String;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LkP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/Set;)V
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "LkP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    sget v0, Lej;->pick_entry_dialog_row:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, LbT;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 96
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, LhY;->a:Ljava/util/Set;

    .line 97
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 175
    invoke-virtual {p0, p2}, LhY;->a(Ljava/lang/String;)V

    .line 176
    invoke-virtual {p0, p1}, LhY;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 177
    return-void
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 14
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 101
    invoke-static {p2}, LdY;->a(Landroid/content/Context;)V

    .line 107
    invoke-static {p3}, LPW;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LkO;->a(Ljava/lang/String;)LkP;

    move-result-object v5

    .line 108
    invoke-static {p3}, LPW;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 109
    sget-object v1, LPX;->j:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1, p3}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 110
    invoke-virtual {v5}, LkP;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0, v1}, LkO;->a(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    .line 111
    sget v0, Leh;->doc_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 113
    sget-object v2, LkP;->h:LkP;

    invoke-virtual {v2, v5}, LkP;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3f

    iget-object v2, p0, LhY;->a:Ljava/util/Set;

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c8

    :cond_3f
    move v2, v4

    .line 116
    :goto_40
    if-eqz v2, :cond_cb

    .line 117
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 126
    :goto_45
    sget-object v0, LPX;->a:LPX;

    invoke-virtual {v0}, LPX;->a()LPI;

    move-result-object v0

    invoke-virtual {v0, p3}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 127
    sget v0, Leh;->title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 128
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 132
    sget v1, Leh;->entry_details:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 133
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 134
    sget-object v6, LkP;->h:LkP;

    invoke-virtual {v5, v6}, LkP;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e6

    .line 135
    sget-object v5, LPX;->f:LPX;

    invoke-virtual {v5}, LPX;->a()LPI;

    move-result-object v5

    invoke-virtual {v5, p3}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    .line 137
    new-instance v6, Ljava/util/Date;

    sget-object v7, LPX;->e:LPX;

    invoke-virtual {v7}, LPX;->a()LPI;

    move-result-object v7

    invoke-virtual {v7, p3}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-direct {v6, v7, v8}, Ljava/util/Date;-><init>(J)V

    .line 139
    invoke-static {p2}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    .line 141
    sget v8, Len;->preview_general_info_modified:I

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v7, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v9, v3

    aput-object v5, v9, v4

    invoke-virtual {p2, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 145
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 152
    :goto_a8
    sget-object v1, LPX;->n:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1, p3}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 153
    iget-object v3, p0, LhY;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_ec

    .line 156
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 157
    sget v0, Lee;->list_entry_activated:I

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 163
    :goto_c4
    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 164
    return-void

    :cond_c8
    move v2, v3

    .line 113
    goto/16 :goto_40

    .line 119
    :cond_cb
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v6, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 120
    invoke-static {v1}, LZu;->a(Landroid/graphics/Bitmap;)LZu;

    move-result-object v1

    const/16 v6, 0x64

    invoke-virtual {v1, v6}, LZu;->b(I)LZu;

    move-result-object v1

    invoke-virtual {v1}, LZu;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 122
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_45

    .line 148
    :cond_e6
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_a8

    .line 159
    :cond_ec
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 160
    const v0, 0x106000d

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_c4
.end method

.method public a(Ljava/lang/String;)V
    .registers 2
    .parameter

    .prologue
    .line 170
    iput-object p1, p0, LhY;->a:Ljava/lang/String;

    .line 171
    invoke-virtual {p0}, LhY;->notifyDataSetChanged()V

    .line 172
    return-void
.end method
