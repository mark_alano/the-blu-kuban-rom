.class public Lhi;
.super Ljava/lang/Object;
.source "HomeScreenActivity.java"

# interfaces
.implements LMF;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/app/HomeScreenActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/app/HomeScreenActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 656
    iput-object p1, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 672
    iget-object v0, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    iget-object v1, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 673
    iget-object v0, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LZM;

    iget-object v1, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    sget v2, Len;->google_account_missing_all_apps:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 674
    iget-object v0, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->finish()V

    .line 675
    return-void
.end method

.method public a(Ljava/lang/Exception;)V
    .registers 5
    .parameter

    .prologue
    .line 665
    iget-object v0, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    iget-object v1, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 666
    iget-object v0, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LZM;

    iget-object v1, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    sget v2, Len;->google_account_missing_all_apps:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LZM;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 667
    iget-object v0, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->finish()V

    .line 668
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 659
    iget-object v0, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a:LdL;

    iget-object v1, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 660
    iget-object v0, p0, Lhi;->a:Lcom/google/android/apps/docs/app/HomeScreenActivity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/docs/app/HomeScreenActivity;->a(Lcom/google/android/apps/docs/app/HomeScreenActivity;Ljava/lang/String;Z)V

    .line 661
    return-void
.end method
