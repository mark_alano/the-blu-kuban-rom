.class public Lhx;
.super Ljava/lang/Object;
.source "MainProxyLogic.java"


# instance fields
.field private final a:LME;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lev;

.field private final a:Lfe;

.field private final a:Lgb;

.field private final a:Lgl;

.field a:Z

.field b:Z

.field c:Z


# direct methods
.method public constructor <init>(Laoz;Lgb;LME;Lfe;Lev;Lgl;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lgb;",
            "LME;",
            "Lfe;",
            "Lev;",
            "Lgl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    iput-object p1, p0, Lhx;->a:Laoz;

    .line 194
    iput-object p2, p0, Lhx;->a:Lgb;

    .line 195
    iput-object p3, p0, Lhx;->a:LME;

    .line 196
    iput-object p4, p0, Lhx;->a:Lfe;

    .line 197
    iput-object p5, p0, Lhx;->a:Lev;

    .line 198
    iput-object p6, p0, Lhx;->a:Lgl;

    .line 199
    return-void
.end method

.method private a(Ljava/lang/String;LhA;)Ljava/lang/String;
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 374
    if-nez p1, :cond_26

    .line 375
    iget-object v0, p0, Lhx;->a:LME;

    invoke-interface {v0}, LME;->a()Ljava/lang/String;

    move-result-object v0

    .line 377
    :goto_9
    if-nez v0, :cond_20

    .line 379
    sget-object v0, LhA;->b:LhA;

    if-ne p2, v0, :cond_24

    .line 380
    iget-object v0, p0, Lhx;->a:LME;

    invoke-static {v0}, LMG;->a(LME;)Landroid/accounts/Account;

    move-result-object v0

    .line 382
    :goto_15
    if-nez v0, :cond_1d

    .line 383
    iget-object v0, p0, Lhx;->a:LME;

    invoke-static {v0}, LMG;->b(LME;)Landroid/accounts/Account;

    move-result-object v0

    .line 385
    :cond_1d
    if-nez v0, :cond_21

    move-object v0, v1

    .line 392
    :cond_20
    :goto_20
    return-object v0

    .line 389
    :cond_21
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_20

    :cond_24
    move-object v0, v1

    goto :goto_15

    :cond_26
    move-object v0, p1

    goto :goto_9
.end method

.method private a(Ljava/lang/String;Lfb;LhB;Lhz;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 397
    invoke-direct {p0, p1, p2}, Lhx;->a(Ljava/lang/String;Lfb;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 398
    invoke-interface {p4, p1, p3}, Lhz;->a(Ljava/lang/String;LhB;)V

    .line 402
    :goto_9
    return-void

    .line 400
    :cond_a
    invoke-interface {p4, p1, p2, p3}, Lhz;->a(Ljava/lang/String;Lfb;LhB;)V

    goto :goto_9
.end method

.method private a(Ljava/lang/String;LhA;Lhz;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 263
    sget-object v0, LhA;->c:LhA;

    if-ne p2, v0, :cond_1d

    .line 264
    iget-object v0, p0, Lhx;->a:Lgb;

    invoke-interface {v0, p1}, Lgb;->b(Ljava/lang/String;)Z

    move-result v0

    .line 265
    if-eqz v0, :cond_16

    sget-object v0, LhA;->b:LhA;

    .line 268
    :goto_e
    sget-object v1, LhA;->a:LhA;

    if-ne v0, v1, :cond_19

    .line 269
    invoke-direct {p0, p1, p3}, Lhx;->a(Ljava/lang/String;Lhz;)V

    .line 273
    :goto_15
    return-void

    .line 265
    :cond_16
    sget-object v0, LhA;->a:LhA;

    goto :goto_e

    .line 271
    :cond_19
    invoke-direct {p0, p1, p3}, Lhx;->b(Ljava/lang/String;Lhz;)V

    goto :goto_15

    :cond_1d
    move-object v0, p2

    goto :goto_e
.end method

.method private a(Ljava/lang/String;Lhz;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 276
    iget-object v0, p0, Lhx;->a:Lgb;

    invoke-interface {v0, p1}, Lgb;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 277
    invoke-interface {p2, p1}, Lhz;->b(Ljava/lang/String;)V

    .line 289
    :goto_b
    return-void

    .line 278
    :cond_c
    iget-object v0, p0, Lhx;->a:Lgb;

    invoke-interface {v0, p1}, Lgb;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 279
    invoke-direct {p0, p1}, Lhx;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 280
    sget-object v0, Lfb;->b:Lfb;

    sget-object v1, LhB;->c:LhB;

    invoke-direct {p0, p1, v0, v1, p2}, Lhx;->a(Ljava/lang/String;Lfb;LhB;Lhz;)V

    goto :goto_b

    .line 283
    :cond_22
    sget-object v0, Lfb;->b:Lfb;

    sget-object v1, LhB;->a:LhB;

    invoke-direct {p0, p1, v0, v1, p2}, Lhx;->a(Ljava/lang/String;Lfb;LhB;Lhz;)V

    goto :goto_b

    .line 287
    :cond_2a
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lhx;->a(Ljava/lang/String;Lhz;Z)V

    goto :goto_b
.end method

.method private a(Ljava/lang/String;Lhz;LhD;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 308
    sget-object v0, LhD;->a:LhD;

    if-eq p3, v0, :cond_8

    sget-object v0, LhD;->b:LhD;

    if-ne p3, v0, :cond_18

    :cond_8
    const/4 v0, 0x1

    :goto_9
    invoke-static {v0}, Lagu;->a(Z)V

    .line 311
    iget-object v0, p0, Lhx;->a:Lgb;

    invoke-interface {v0, p1}, Lgb;->b(Ljava/lang/String;)Z

    move-result v0

    .line 312
    if-eqz v0, :cond_1a

    .line 313
    invoke-direct {p0, p1, p2}, Lhx;->d(Ljava/lang/String;Lhz;)V

    .line 317
    :goto_17
    return-void

    .line 308
    :cond_18
    const/4 v0, 0x0

    goto :goto_9

    .line 315
    :cond_1a
    invoke-interface {p2, p1}, Lhz;->a(Ljava/lang/String;)V

    goto :goto_17
.end method

.method private a(Ljava/lang/String;Lhz;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 352
    iget-object v0, p0, Lhx;->a:Lgb;

    invoke-interface {v0, p1}, Lgb;->c(Ljava/lang/String;)Z

    move-result v0

    .line 354
    invoke-direct {p0, p1}, Lhx;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    if-nez v0, :cond_18

    if-nez p3, :cond_18

    .line 355
    sget-object v0, Lfb;->a:Lfb;

    sget-object v1, LhB;->d:LhB;

    invoke-direct {p0, p1, v0, v1, p2}, Lhx;->a(Ljava/lang/String;Lfb;LhB;Lhz;)V

    .line 360
    :goto_17
    return-void

    .line 358
    :cond_18
    sget-object v0, Lfb;->a:Lfb;

    sget-object v1, LhB;->a:LhB;

    invoke-direct {p0, p1, v0, v1, p2}, Lhx;->a(Ljava/lang/String;Lfb;LhB;Lhz;)V

    goto :goto_17
.end method

.method private a(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 405
    iget-object v0, p0, Lhx;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 408
    if-nez v0, :cond_d

    .line 409
    iget-boolean v0, p0, Lhx;->a:Z

    .line 411
    :goto_c
    return v0

    :cond_d
    const-string v0, "FirstTimeDrive"

    invoke-direct {p0, p1, v0}, Lhx;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_c
.end method

.method private a(Ljava/lang/String;Lfb;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 438
    invoke-direct {p0}, Lhx;->c()Z

    move-result v0

    if-nez v0, :cond_20

    iget-object v0, p0, Lhx;->a:LME;

    invoke-interface {v0}, LME;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    iget-object v0, p0, Lhx;->a:Lfe;

    invoke-virtual {v0, p1}, Lfe;->a(Ljava/lang/String;)Lfb;

    move-result-object v0

    invoke-virtual {p2, v0}, Lfb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 426
    .line 429
    :try_start_1
    iget-object v1, p0, Lhx;->a:Lev;

    invoke-interface {v1, p1}, Lev;->a(Ljava/lang/String;)Let;

    move-result-object v1

    .line 430
    const/4 v2, 0x1

    invoke-interface {v1, p2, v2}, Let;->a(Ljava/lang/String;Z)Z
    :try_end_b
    .catch Lew; {:try_start_1 .. :try_end_b} :catch_d

    move-result v0

    .line 433
    :goto_c
    return v0

    .line 431
    :catch_d
    move-exception v1

    .line 432
    const-string v1, "MainProxyLogic"

    const-string v2, "Failed loading account preference"

    invoke-static {v1, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c
.end method

.method private b(Ljava/lang/String;LhA;Lhz;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 331
    iget-object v0, p0, Lhx;->a:Lgb;

    invoke-interface {v0, p1}, Lgb;->b(Ljava/lang/String;)Z

    move-result v0

    .line 333
    if-eqz v0, :cond_14

    sget-object v0, LhA;->b:LhA;

    if-ne p2, v0, :cond_14

    .line 334
    sget-object v0, Lfb;->b:Lfb;

    sget-object v1, LhB;->a:LhB;

    invoke-direct {p0, p1, v0, v1, p3}, Lhx;->a(Ljava/lang/String;Lfb;LhB;Lhz;)V

    .line 338
    :goto_13
    return-void

    .line 336
    :cond_14
    sget-object v0, Lfb;->a:Lfb;

    sget-object v1, LhB;->a:LhB;

    invoke-direct {p0, p1, v0, v1, p3}, Lhx;->a(Ljava/lang/String;Lfb;LhB;Lhz;)V

    goto :goto_13
.end method

.method private b(Ljava/lang/String;Lhz;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 292
    iget-object v0, p0, Lhx;->a:Lgb;

    invoke-interface {v0, p1}, Lgb;->b(Ljava/lang/String;)Z

    move-result v0

    .line 293
    if-eqz v0, :cond_1e

    .line 294
    invoke-direct {p0, p1}, Lhx;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 295
    sget-object v0, Lfb;->b:Lfb;

    sget-object v1, LhB;->c:LhB;

    invoke-direct {p0, p1, v0, v1, p2}, Lhx;->a(Ljava/lang/String;Lfb;LhB;Lhz;)V

    .line 304
    :goto_15
    return-void

    .line 298
    :cond_16
    sget-object v0, Lfb;->b:Lfb;

    sget-object v1, LhB;->a:LhB;

    invoke-direct {p0, p1, v0, v1, p2}, Lhx;->a(Ljava/lang/String;Lfb;LhB;Lhz;)V

    goto :goto_15

    .line 302
    :cond_1e
    invoke-interface {p2, p1}, Lhz;->b(Ljava/lang/String;)V

    goto :goto_15
.end method

.method private b(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 416
    iget-object v0, p0, Lhx;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 418
    if-nez v0, :cond_d

    .line 419
    iget-boolean v0, p0, Lhx;->b:Z

    .line 421
    :goto_c
    return v0

    :cond_d
    const-string v0, "FirstTimeDocs"

    invoke-direct {p0, p1, v0}, Lhx;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_c
.end method

.method private c(Ljava/lang/String;Lhz;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 320
    iget-object v0, p0, Lhx;->a:Lgb;

    invoke-interface {v0, p1}, Lgb;->b(Ljava/lang/String;)Z

    move-result v0

    .line 322
    if-eqz v0, :cond_c

    .line 323
    invoke-direct {p0, p1, p2}, Lhx;->d(Ljava/lang/String;Lhz;)V

    .line 327
    :goto_b
    return-void

    .line 325
    :cond_c
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lhx;->a(Ljava/lang/String;Lhz;Z)V

    goto :goto_b
.end method

.method private c()Z
    .registers 3

    .prologue
    .line 447
    iget-object v0, p0, Lhx;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 449
    if-eqz v0, :cond_15

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_15

    .line 450
    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v0

    .line 452
    :goto_14
    return v0

    :cond_15
    iget-boolean v0, p0, Lhx;->c:Z

    goto :goto_14
.end method

.method private d(Ljava/lang/String;Lhz;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 342
    invoke-direct {p0, p1}, Lhx;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 343
    sget-object v0, Lfb;->b:Lfb;

    sget-object v1, LhB;->c:LhB;

    invoke-interface {p2, p1, v0, v1}, Lhz;->a(Ljava/lang/String;Lfb;LhB;)V

    .line 349
    :goto_d
    return-void

    .line 346
    :cond_e
    sget-object v0, Lfb;->b:Lfb;

    sget-object v1, LhB;->a:LhB;

    invoke-interface {p2, p1, v0, v1}, Lhz;->a(Ljava/lang/String;Lfb;LhB;)V

    goto :goto_d
.end method


# virtual methods
.method public a(Ljava/lang/String;LhA;LhE;LhD;Lhz;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 235
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    invoke-static {p5}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    invoke-direct {p0, p1, p2}, Lhx;->a(Ljava/lang/String;LhA;)Ljava/lang/String;

    move-result-object v0

    .line 239
    if-nez v0, :cond_10

    .line 240
    invoke-interface {p5}, Lhz;->a_()V

    .line 259
    :goto_f
    return-void

    .line 241
    :cond_10
    invoke-virtual {p0}, Lhx;->a()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 242
    const/4 v1, 0x0

    invoke-direct {p0, v0, p5, v1}, Lhx;->a(Ljava/lang/String;Lhz;Z)V

    goto :goto_f

    .line 243
    :cond_1b
    invoke-virtual {p0}, Lhx;->b()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 244
    invoke-direct {p0, v0, p2, p5}, Lhx;->b(Ljava/lang/String;LhA;Lhz;)V

    goto :goto_f

    .line 247
    :cond_25
    sget-object v1, Lhy;->a:[I

    invoke-virtual {p3}, LhE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_3c

    .line 255
    invoke-direct {p0, v0, p2, p5}, Lhx;->a(Ljava/lang/String;LhA;Lhz;)V

    goto :goto_f

    .line 249
    :pswitch_34
    invoke-direct {p0, v0, p5, p4}, Lhx;->a(Ljava/lang/String;Lhz;LhD;)V

    goto :goto_f

    .line 252
    :pswitch_38
    invoke-direct {p0, v0, p5}, Lhx;->c(Ljava/lang/String;Lhz;)V

    goto :goto_f

    .line 247
    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_34
        :pswitch_38
    .end packed-switch
.end method

.method a()Z
    .registers 3

    .prologue
    .line 364
    iget-object v0, p0, Lhx;->a:Lgl;

    sget-object v1, Lgi;->c:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    return v0
.end method

.method b()Z
    .registers 3

    .prologue
    .line 369
    invoke-virtual {p0}, Lhx;->a()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lhx;->a:Lgl;

    sget-object v1, Lgi;->d:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method
