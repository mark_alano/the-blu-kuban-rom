.class public LiI;
.super Ljava/lang/Object;
.source "CriterionFactoryImpl.java"

# interfaces
.implements LiG;


# instance fields
.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lfg;

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LiP;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljt;

.field private final a:Llf;


# direct methods
.method public constructor <init>(Llf;Laoz;Lfg;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Llf;",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Lfg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, LiJ;

    invoke-direct {v0, p0}, LiJ;-><init>(LiI;)V

    iput-object v0, p0, LiI;->a:Ljava/util/Map;

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, LiI;->a:Ljt;

    .line 111
    iput-object p1, p0, LiI;->a:Llf;

    .line 112
    iput-object p2, p0, LiI;->a:Laoz;

    .line 113
    iput-object p3, p0, LiI;->a:Lfg;

    .line 114
    return-void
.end method

.method static synthetic a(LiI;)Lfg;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, LiI;->a:Lfg;

    return-object v0
.end method


# virtual methods
.method public a()LiE;
    .registers 7

    .prologue
    .line 146
    iget-object v0, p0, LiI;->a:Ljt;

    if-nez v0, :cond_1d

    .line 147
    invoke-static {}, LPW;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lnh;->a(Ljava/lang/String;Ljava/util/Collection;)Lnh;

    move-result-object v4

    .line 150
    new-instance v0, Ljt;

    const-string v1, "notInTrash"

    sget-object v2, LWr;->a:LWr;

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljt;-><init>(Ljava/lang/String;LWr;Ljava/lang/String;Lnh;Z)V

    iput-object v0, p0, LiI;->a:Ljt;

    .line 154
    :cond_1d
    iget-object v0, p0, LiI;->a:Ljt;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)LiE;
    .registers 7
    .parameter

    .prologue
    .line 159
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    const-string v0, "kind"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 162
    if-nez v1, :cond_13

    .line 163
    new-instance v0, LiH;

    const-string v1, "Criterion bundle is missing \"kind\" key."

    invoke-direct {v0, v1}, LiH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :cond_13
    iget-object v0, p0, LiI;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LiP;

    .line 168
    if-nez v0, :cond_3c

    .line 169
    new-instance v0, LiH;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Criterion bundle has invalid kind \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LiH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_3c
    :try_start_3c
    iget-object v2, p0, LiI;->a:Laoz;

    iget-object v3, p0, LiI;->a:Llf;

    invoke-interface {v0, p1, v2, v3}, LiP;->a(Landroid/os/Bundle;Laoz;Llf;)LiE;
    :try_end_43
    .catch LiF; {:try_start_3c .. :try_end_43} :catch_65

    move-result-object v0

    .line 181
    if-nez v0, :cond_85

    .line 182
    new-instance v0, LiH;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to deserialize criterion from bundle with kind \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LiH;-><init>(Ljava/lang/String;)V

    throw v0

    .line 176
    :catch_65
    move-exception v0

    .line 177
    new-instance v2, LiH;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to deserialize criterion from bundle with kind \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1, v0}, LiH;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 185
    :cond_85
    return-object v0
.end method

.method public a(Ljava/lang/String;)LiE;
    .registers 4
    .parameter

    .prologue
    .line 141
    new-instance v0, Liu;

    iget-object v1, p0, LiI;->a:Llf;

    invoke-direct {v0, p1, v1}, Liu;-><init>(Ljava/lang/String;Llf;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;LSF;)LiE;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 135
    new-instance v0, Ljs;

    invoke-virtual {p2}, LSF;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, LSF;->a()Lnh;

    move-result-object v2

    iget-object v3, p0, LiI;->a:Laoz;

    invoke-direct {v0, v1, p1, v2, v3}, Ljs;-><init>(Ljava/lang/String;Ljava/lang/String;Lnh;Laoz;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)LiE;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 118
    new-instance v0, LiD;

    iget-object v1, p0, LiI;->a:Llf;

    invoke-direct {v0, p1, v1, p2}, LiD;-><init>(Ljava/lang/String;Llf;Ljava/lang/String;)V

    return-object v0
.end method

.method public a(LmK;Ljava/lang/String;)LiE;
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 123
    new-instance v0, LiW;

    iget-object v3, p0, LiI;->a:Laoz;

    iget-object v4, p0, LiI;->a:Llf;

    iget-object v5, p0, LiI;->a:Lfg;

    move-object v1, p1

    move-object v6, p2

    move v7, v2

    invoke-direct/range {v0 .. v7}, LiW;-><init>(LmK;ZLaoz;Llf;Lfg;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public b(LmK;Ljava/lang/String;)LiE;
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 129
    new-instance v0, LiW;

    const/4 v2, 0x0

    iget-object v3, p0, LiI;->a:Laoz;

    iget-object v4, p0, LiI;->a:Llf;

    iget-object v5, p0, LiI;->a:Lfg;

    const/4 v7, 0x1

    move-object v1, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v7}, LiW;-><init>(LmK;ZLaoz;Llf;Lfg;Ljava/lang/String;Z)V

    return-object v0
.end method
