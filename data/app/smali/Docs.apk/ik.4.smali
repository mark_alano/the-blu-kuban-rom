.class public final enum Lik;
.super Ljava/lang/Enum;
.source "ShortcutDefinition.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lik;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lik;

.field private static final synthetic a:[Lik;

.field public static final enum b:Lik;

.field public static final enum c:Lik;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 158
    new-instance v0, Lik;

    const-string v1, "MAIN_LIST"

    invoke-direct {v0, v1, v2}, Lik;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lik;->a:Lik;

    .line 161
    new-instance v0, Lik;

    const-string v1, "ADDITIONAL_ACTION"

    invoke-direct {v0, v1, v3}, Lik;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lik;->b:Lik;

    .line 164
    new-instance v0, Lik;

    const-string v1, "DRIVE_ADDITIONAL_FILTERS"

    invoke-direct {v0, v1, v4}, Lik;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lik;->c:Lik;

    .line 156
    const/4 v0, 0x3

    new-array v0, v0, [Lik;

    sget-object v1, Lik;->a:Lik;

    aput-object v1, v0, v2

    sget-object v1, Lik;->b:Lik;

    aput-object v1, v0, v3

    sget-object v1, Lik;->c:Lik;

    aput-object v1, v0, v4

    sput-object v0, Lik;->a:[Lik;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 156
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lik;
    .registers 2
    .parameter

    .prologue
    .line 156
    const-class v0, Lik;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lik;

    return-object v0
.end method

.method public static values()[Lik;
    .registers 1

    .prologue
    .line 156
    sget-object v0, Lik;->a:[Lik;

    invoke-virtual {v0}, [Lik;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lik;

    return-object v0
.end method
