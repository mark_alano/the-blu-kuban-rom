.class public final enum LkQ;
.super Ljava/lang/Enum;
.source "Entry.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LkQ;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LkQ;

.field private static final synthetic a:[LkQ;

.field public static final enum b:LkQ;

.field public static final enum c:LkQ;

.field public static final enum d:LkQ;

.field public static final enum e:LkQ;

.field public static final enum f:LkQ;


# instance fields
.field private final a:I

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .registers 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 61
    new-instance v0, LkQ;

    const-string v1, "ZIP"

    sget v3, Leg;->ic_type_zip:I

    sget v4, Leg;->ic_type_zip_big:I

    new-instance v5, LkR;

    invoke-direct {v5}, LkR;-><init>()V

    invoke-direct/range {v0 .. v5}, LkQ;-><init>(Ljava/lang/String;IIILjava/util/List;)V

    sput-object v0, LkQ;->a:LkQ;

    .line 70
    new-instance v3, LkQ;

    const-string v4, "PICTURE"

    sget v6, Leg;->ic_type_image:I

    sget v7, Leg;->ic_type_image_big:I

    new-instance v8, LkS;

    invoke-direct {v8}, LkS;-><init>()V

    move v5, v9

    invoke-direct/range {v3 .. v8}, LkQ;-><init>(Ljava/lang/String;IIILjava/util/List;)V

    sput-object v3, LkQ;->b:LkQ;

    .line 95
    new-instance v3, LkQ;

    const-string v4, "MOVIE"

    sget v6, Leg;->ic_type_video:I

    sget v7, Leg;->ic_type_video_big:I

    new-instance v8, LkT;

    invoke-direct {v8}, LkT;-><init>()V

    move v5, v10

    invoke-direct/range {v3 .. v8}, LkQ;-><init>(Ljava/lang/String;IIILjava/util/List;)V

    sput-object v3, LkQ;->c:LkQ;

    .line 113
    new-instance v3, LkQ;

    const-string v4, "MSWORD"

    sget v6, Leg;->ic_type_word:I

    sget v7, Leg;->ic_type_word_big:I

    new-instance v8, LkU;

    invoke-direct {v8}, LkU;-><init>()V

    move v5, v11

    invoke-direct/range {v3 .. v8}, LkQ;-><init>(Ljava/lang/String;IIILjava/util/List;)V

    sput-object v3, LkQ;->d:LkQ;

    .line 121
    new-instance v3, LkQ;

    const-string v4, "MSEXCEL"

    sget v6, Leg;->ic_type_excel:I

    sget v7, Leg;->ic_type_excel_big:I

    new-instance v8, LkV;

    invoke-direct {v8}, LkV;-><init>()V

    move v5, v12

    invoke-direct/range {v3 .. v8}, LkQ;-><init>(Ljava/lang/String;IIILjava/util/List;)V

    sput-object v3, LkQ;->e:LkQ;

    .line 131
    new-instance v3, LkQ;

    const-string v4, "MSPOWERPOINT"

    const/4 v5, 0x5

    sget v6, Leg;->ic_type_powerpoint:I

    sget v7, Leg;->ic_type_powerpoint_big:I

    new-instance v8, LkW;

    invoke-direct {v8}, LkW;-><init>()V

    invoke-direct/range {v3 .. v8}, LkQ;-><init>(Ljava/lang/String;IIILjava/util/List;)V

    sput-object v3, LkQ;->f:LkQ;

    .line 60
    const/4 v0, 0x6

    new-array v0, v0, [LkQ;

    sget-object v1, LkQ;->a:LkQ;

    aput-object v1, v0, v2

    sget-object v1, LkQ;->b:LkQ;

    aput-object v1, v0, v9

    sget-object v1, LkQ;->c:LkQ;

    aput-object v1, v0, v10

    sget-object v1, LkQ;->d:LkQ;

    aput-object v1, v0, v11

    sget-object v1, LkQ;->e:LkQ;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, LkQ;->f:LkQ;

    aput-object v2, v0, v1

    sput-object v0, LkQ;->a:[LkQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILjava/util/List;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 148
    iput p3, p0, LkQ;->a:I

    .line 149
    iput p4, p0, LkQ;->b:I

    .line 150
    iput-object p5, p0, LkQ;->a:Ljava/util/List;

    .line 151
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LkQ;
    .registers 2
    .parameter

    .prologue
    .line 60
    const-class v0, LkQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LkQ;

    return-object v0
.end method

.method public static values()[LkQ;
    .registers 1

    .prologue
    .line 60
    sget-object v0, LkQ;->a:[LkQ;

    invoke-virtual {v0}, [LkQ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LkQ;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 154
    iget v0, p0, LkQ;->a:I

    return v0
.end method

.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, LkQ;->a:Ljava/util/List;

    return-object v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 158
    iget v0, p0, LkQ;->b:I

    return v0
.end method
