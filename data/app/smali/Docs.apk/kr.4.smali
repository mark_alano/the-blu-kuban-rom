.class public final Lkr;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lkf;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lkj;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lkh;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lkk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 34
    iput-object p1, p0, Lkr;->a:LYD;

    .line 35
    const-class v0, Lkf;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lkr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lkr;->a:LZb;

    .line 38
    const-class v0, Lkj;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lkr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lkr;->b:LZb;

    .line 41
    const-class v0, Lkh;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lkr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lkr;->c:LZb;

    .line 44
    const-class v0, Lkk;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lkr;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lkr;->d:LZb;

    .line 47
    return-void
.end method

.method static synthetic a(Lkr;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lkr;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lkr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lkr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lkr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lkr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lkr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lkr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lkr;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 54
    const-class v0, Lkf;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lkr;->a:LZb;

    invoke-virtual {p0, v0, v1}, Lkr;->a(Laop;LZb;)V

    .line 55
    const-class v0, Lkj;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lkr;->b:LZb;

    invoke-virtual {p0, v0, v1}, Lkr;->a(Laop;LZb;)V

    .line 56
    const-class v0, Lkh;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lkr;->c:LZb;

    invoke-virtual {p0, v0, v1}, Lkr;->a(Laop;LZb;)V

    .line 57
    const-class v0, Lkk;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lkr;->d:LZb;

    invoke-virtual {p0, v0, v1}, Lkr;->a(Laop;LZb;)V

    .line 58
    iget-object v0, p0, Lkr;->a:LZb;

    iget-object v1, p0, Lkr;->a:LYD;

    iget-object v1, v1, LYD;->a:Lkr;

    iget-object v1, v1, Lkr;->c:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 60
    iget-object v0, p0, Lkr;->b:LZb;

    iget-object v1, p0, Lkr;->a:LYD;

    iget-object v1, v1, LYD;->a:Lkr;

    iget-object v1, v1, Lkr;->d:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 62
    iget-object v0, p0, Lkr;->c:LZb;

    new-instance v1, Lks;

    invoke-direct {v1, p0}, Lks;-><init>(Lkr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 91
    iget-object v0, p0, Lkr;->d:LZb;

    new-instance v1, Lkt;

    invoke-direct {v1, p0}, Lkt;-><init>(Lkr;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 115
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 119
    return-void
.end method
