.class public LlA;
.super LlD;
.source "NameOp.java"


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LkO;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 33
    const-string v0, "rename"

    invoke-direct {p0, p1, v0}, LlD;-><init>(LkO;Ljava/lang/String;)V

    .line 34
    iput-object p2, p0, LlA;->c:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static a(LkO;Lorg/json/JSONObject;)LlA;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 38
    new-instance v0, LlA;

    const-string v1, "nameValue"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LlA;-><init>(LkO;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(Llf;LkO;)LlB;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 49
    new-instance v0, LlA;

    invoke-virtual {p2}, LkO;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p2, v1}, LlA;-><init>(LkO;Ljava/lang/String;)V

    .line 50
    iget-object v1, p0, LlA;->c:Ljava/lang/String;

    invoke-virtual {p2, v1}, LkO;->c(Ljava/lang/String;)V

    .line 51
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .registers 4

    .prologue
    .line 56
    invoke-super {p0}, LlD;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 57
    const-string v1, "operationName"

    const-string v2, "rename"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 58
    const-string v1, "nameValue"

    iget-object v2, p0, LlA;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 60
    return-object v0
.end method

.method public a(LVV;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    invoke-virtual {p0, p1}, LlA;->b(LVV;)V

    .line 44
    iget-object v0, p0, LlA;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, LVV;->v(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 65
    instance-of v1, p1, LlA;

    if-nez v1, :cond_6

    .line 69
    :cond_5
    :goto_5
    return v0

    .line 68
    :cond_6
    check-cast p1, LlA;

    .line 69
    invoke-virtual {p0, p1}, LlA;->a(LlC;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, LlA;->c:Ljava/lang/String;

    iget-object v2, p1, LlA;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 74
    invoke-virtual {p0}, LlA;->b()I

    move-result v0

    iget-object v1, p0, LlA;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    mul-int/lit8 v1, v1, 0x11

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 79
    const-string v0, "NameOp[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LlA;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LlA;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
