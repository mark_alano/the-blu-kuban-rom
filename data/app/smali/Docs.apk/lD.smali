.class public abstract LlD;
.super LlC;
.source "OperationOnDocEntry.java"


# direct methods
.method public constructor <init>(LkO;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, LlC;-><init>(LkO;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method protected abstract a(LVV;)V
.end method

.method public a(LlK;LlR;)Z
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-virtual {p2}, LlR;->a()LVF;

    move-result-object v1

    invoke-interface {v1, p1}, LVF;->a(LlK;)LVE;

    move-result-object v1

    .line 26
    iget-object v2, p0, LlD;->b:Ljava/lang/String;

    iget-object v3, p0, LlD;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LVE;->a(Ljava/lang/String;Ljava/lang/String;)LVV;

    move-result-object v2

    .line 27
    if-nez v2, :cond_14

    .line 42
    :cond_13
    :goto_13
    return v0

    .line 33
    :cond_14
    invoke-virtual {p0, v2}, LlD;->a(LVV;)V

    .line 34
    invoke-virtual {v2}, LVV;->p()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_56

    .line 35
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://docs.google.com/feeds/default/private/full/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, LVV;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, LVV;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 37
    invoke-virtual {v2, v3}, LVV;->q(Ljava/lang/String;)V

    .line 41
    :cond_56
    iget-object v3, p0, LlD;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, LVE;->a(LasT;Ljava/lang/String;)LVV;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_13

    const/4 v0, 0x1

    goto :goto_13
.end method

.method protected final b(LVV;)V
    .registers 3
    .parameter

    .prologue
    .line 55
    invoke-virtual {p0}, LlD;->a()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lla;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-virtual {p1, v0}, LVV;->w(Ljava/lang/String;)V

    .line 57
    return-void
.end method
