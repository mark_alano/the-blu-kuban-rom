.class public LlJ;
.super Ljava/lang/Object;
.source "OperationQueueRateLimiterFactory.java"

# interfaces
.implements LaaX;


# instance fields
.field private final a:LKS;


# direct methods
.method public constructor <init>(LKS;)V
    .registers 2
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, LlJ;->a:LKS;

    .line 28
    return-void
.end method


# virtual methods
.method public a()LaaW;
    .registers 9

    .prologue
    .line 32
    new-instance v0, LZO;

    iget-object v1, p0, LlJ;->a:LKS;

    const-string v2, "multiOperationQueueMinWait"

    const/16 v3, 0xc8

    invoke-interface {v1, v2, v3}, LKS;->a(Ljava/lang/String;I)I

    move-result v1

    int-to-long v1, v1

    iget-object v3, p0, LlJ;->a:LKS;

    const-string v4, "multiOperationQueueWaitGrowthFactor"

    const-wide/high16 v5, 0x3ff8

    invoke-interface {v3, v4, v5, v6}, LKS;->a(Ljava/lang/String;D)D

    move-result-wide v3

    iget-object v5, p0, LlJ;->a:LKS;

    const-string v6, "multiOperationQueueMaxWait"

    const/16 v7, 0x2710

    invoke-interface {v5, v6, v7}, LKS;->a(Ljava/lang/String;I)I

    move-result v5

    int-to-long v5, v5

    invoke-direct/range {v0 .. v6}, LZO;-><init>(JDJ)V

    return-object v0
.end method
