.class public LlT;
.super LlD;
.source "StarredOp.java"


# instance fields
.field private final a:Z


# direct methods
.method public constructor <init>(LkO;)V
    .registers 3
    .parameter

    .prologue
    .line 43
    invoke-virtual {p1}, LkO;->c()Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_7
    invoke-direct {p0, p1, v0}, LlT;-><init>(LkO;Z)V

    .line 44
    return-void

    .line 43
    :cond_b
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public constructor <init>(LkO;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 32
    const-string v0, "starred"

    invoke-direct {p0, p1, v0}, LlD;-><init>(LkO;Ljava/lang/String;)V

    .line 33
    iput-boolean p2, p0, LlT;->a:Z

    .line 34
    return-void
.end method

.method public static a(LkO;Lorg/json/JSONObject;)LlT;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 47
    new-instance v0, LlT;

    const-string v1, "starValue"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-direct {v0, p0, v1}, LlT;-><init>(LkO;Z)V

    return-object v0
.end method


# virtual methods
.method public a(Llf;LkO;)LlB;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 58
    new-instance v0, LlT;

    invoke-virtual {p2}, LkO;->c()Z

    move-result v1

    invoke-direct {v0, p2, v1}, LlT;-><init>(LkO;Z)V

    .line 59
    iget-boolean v1, p0, LlT;->a:Z

    invoke-virtual {p2, v1}, LkO;->c(Z)V

    .line 60
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .registers 4

    .prologue
    .line 65
    invoke-super {p0}, LlD;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 66
    const-string v1, "operationName"

    const-string v2, "starred"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 67
    const-string v1, "starValue"

    iget-boolean v2, p0, LlT;->a:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 69
    return-object v0
.end method

.method public a(LVV;)V
    .registers 3
    .parameter

    .prologue
    .line 52
    invoke-virtual {p0, p1}, LlT;->b(LVV;)V

    .line 53
    iget-boolean v0, p0, LlT;->a:Z

    invoke-virtual {p1, v0}, LVV;->a(Z)V

    .line 54
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 74
    instance-of v1, p1, LlT;

    if-nez v1, :cond_6

    .line 78
    :cond_5
    :goto_5
    return v0

    .line 77
    :cond_6
    check-cast p1, LlT;

    .line 78
    invoke-virtual {p0, p1}, LlT;->a(LlC;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, LlT;->a:Z

    iget-boolean v2, p1, LlT;->a:Z

    if-ne v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 83
    invoke-virtual {p0}, LlT;->b()I

    move-result v1

    iget-boolean v0, p0, LlT;->a:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_9
    add-int/2addr v0, v1

    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 88
    const-string v0, "StarredOp[%s, %s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, LlT;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, LlT;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
