.class public LlU;
.super Ljava/lang/Object;
.source "WapiServerRequestIssuer.java"

# interfaces
.implements LlS;


# instance fields
.field private final a:LVH;


# direct methods
.method public constructor <init>(LVH;)V
    .registers 2
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, LlU;->a:LVH;

    .line 32
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 154
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 156
    const-string v1, "remove-behavior"

    const-string v2, "drive_v1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 157
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 158
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(LkY;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 93
    iget-object v0, p1, LkY;->b:Ljava/lang/String;

    .line 94
    const-string v1, "root"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_25

    .line 95
    invoke-static {v0}, LVI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "contents"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 100
    :goto_24
    return-object v0

    .line 98
    :cond_25
    iget-object v0, p0, LlU;->a:LVH;

    invoke-interface {v0, p1}, LVH;->a(LkY;)Ljava/lang/String;

    move-result-object v0

    goto :goto_24
.end method

.method private a(LlK;Ljava/lang/Exception;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 162
    instance-of v0, p2, LasH;

    if-eqz v0, :cond_2b

    .line 163
    const/4 v0, 0x1

    invoke-interface {p1, v0, p2}, LlK;->a(ILjava/lang/Throwable;)V

    .line 171
    :goto_8
    const-string v0, "WapiServerRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyOnServer failed for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    return-void

    .line 164
    :cond_2b
    instance-of v0, p2, Latc;

    if-eqz v0, :cond_34

    .line 165
    const/4 v0, 0x2

    invoke-interface {p1, v0, p2}, LlK;->a(ILjava/lang/Throwable;)V

    goto :goto_8

    .line 166
    :cond_34
    instance-of v0, p2, Ljava/io/IOException;

    if-eqz v0, :cond_3d

    .line 167
    const/4 v0, 0x3

    invoke-interface {p1, v0, p2}, LlK;->a(ILjava/lang/Throwable;)V

    goto :goto_8

    .line 169
    :cond_3d
    const/4 v0, 0x4

    invoke-interface {p1, v0, p2}, LlK;->a(ILjava/lang/Throwable;)V

    goto :goto_8
.end method

.method private a(LkY;Ljava/lang/String;Ljava/lang/String;ZLlK;)Z
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 48
    iget-object v1, p1, LkY;->a:Ljava/lang/String;

    .line 49
    iget-object v2, p1, LkY;->b:Ljava/lang/String;

    .line 52
    :try_start_5
    invoke-static {v1, p2}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v3

    invoke-direct {p0, v3}, LlU;->a(LkY;)Ljava/lang/String;

    move-result-object v3

    .line 54
    if-nez v3, :cond_28

    .line 55
    const-string v1, "WapiServerRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "contentSource is null for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :goto_27
    return v0

    .line 59
    :cond_28
    if-eqz p4, :cond_44

    .line 60
    iget-object v4, p0, LlU;->a:LVH;

    invoke-static {v2}, LVI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2, v1}, LVH;->a(Ljava/lang/String;Ljava/lang/String;)LVV;

    move-result-object v2

    .line 62
    new-instance v4, LVT;

    invoke-virtual {v2}, LasT;->r()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2}, LVT;-><init>(Ljava/lang/String;)V

    .line 63
    iget-object v2, p0, LlU;->a:LVH;

    invoke-interface {v2, v3, v1, v4}, LVH;->a(Ljava/lang/String;Ljava/lang/String;LasT;)LasT;

    .line 83
    :goto_42
    const/4 v0, 0x1

    goto :goto_27

    .line 65
    :cond_44
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 66
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    .line 67
    invoke-virtual {v3, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 68
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 69
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 71
    iget-object v3, p0, LlU;->a:LVH;

    const-string v4, "*"

    invoke-interface {v3, v2, v1, v4}, LVH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5e
    .catch Latc; {:try_start_5 .. :try_end_5e} :catch_5f
    .catch LasH; {:try_start_5 .. :try_end_5e} :catch_64
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5e} :catch_69

    goto :goto_42

    .line 73
    :catch_5f
    move-exception v1

    .line 74
    invoke-direct {p0, p5, v1}, LlU;->a(LlK;Ljava/lang/Exception;)V

    goto :goto_27

    .line 76
    :catch_64
    move-exception v1

    .line 77
    invoke-direct {p0, p5, v1}, LlU;->a(LlK;Ljava/lang/Exception;)V

    goto :goto_27

    .line 79
    :catch_69
    move-exception v1

    .line 80
    invoke-direct {p0, p5, v1}, LlU;->a(LlK;Ljava/lang/Exception;)V

    goto :goto_27
.end method


# virtual methods
.method public a(LkY;Ljava/lang/String;Ljava/lang/String;LlK;)Z
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LlU;->a(LkY;Ljava/lang/String;Ljava/lang/String;ZLlK;)Z

    move-result v0

    return v0
.end method

.method public a(LkY;Ljava/lang/String;LlK;)Z
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LlU;->a(LkY;Ljava/lang/String;Ljava/lang/String;ZLlK;)Z

    move-result v0

    return v0
.end method

.method public b(LkY;Ljava/lang/String;Ljava/lang/String;LlK;)Z
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 106
    iget-object v2, p1, LkY;->a:Ljava/lang/String;

    .line 107
    iget-object v1, p1, LkY;->b:Ljava/lang/String;

    .line 111
    if-eqz p2, :cond_36

    .line 113
    :try_start_7
    iget-object v3, p0, LlU;->a:LVH;

    invoke-static {v2, p2}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v4

    invoke-interface {v3, v4}, LVH;->a(LkY;)Ljava/lang/String;

    move-result-object v3

    .line 115
    if-nez v3, :cond_18

    .line 116
    const/4 v1, 0x0

    invoke-direct {p0, p4, v1}, LlU;->a(LlK;Ljava/lang/Exception;)V

    .line 146
    :goto_17
    return v0

    .line 119
    :cond_18
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 120
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    .line 121
    invoke-virtual {v3, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 122
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 123
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 134
    :cond_2b
    :goto_2b
    invoke-direct {p0, v1}, LlU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    iget-object v3, p0, LlU;->a:LVH;

    invoke-interface {v3, v1, v2, p3}, LVH;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const/4 v0, 0x1

    goto :goto_17

    .line 125
    :cond_36
    iget-object v3, p0, LlU;->a:LVH;

    invoke-static {v1}, LVI;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v2}, LVH;->a(Ljava/lang/String;Ljava/lang/String;)LVV;

    move-result-object v3

    .line 128
    invoke-virtual {v3}, LVV;->p()Ljava/lang/String;

    move-result-object v1

    .line 129
    if-nez v1, :cond_2b

    .line 130
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "https://docs.google.com/feeds/default/private/full/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, LVV;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, LVV;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7b
    .catch Latc; {:try_start_7 .. :try_end_7b} :catch_7d
    .catch LasH; {:try_start_7 .. :try_end_7b} :catch_82
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7b} :catch_87

    move-result-object v1

    goto :goto_2b

    .line 136
    :catch_7d
    move-exception v1

    .line 137
    invoke-direct {p0, p4, v1}, LlU;->a(LlK;Ljava/lang/Exception;)V

    goto :goto_17

    .line 139
    :catch_82
    move-exception v1

    .line 140
    invoke-direct {p0, p4, v1}, LlU;->a(LlK;Ljava/lang/Exception;)V

    goto :goto_17

    .line 142
    :catch_87
    move-exception v1

    .line 143
    invoke-direct {p0, p4, v1}, LlU;->a(LlK;Ljava/lang/Exception;)V

    goto :goto_17
.end method
