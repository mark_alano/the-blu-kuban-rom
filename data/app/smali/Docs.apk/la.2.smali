.class public Lla;
.super Ljava/lang/Object;
.source "GDataConverterImpl.java"

# interfaces
.implements LkZ;


# static fields
.field private static final a:Lla;


# instance fields
.field private final a:Llk;

.field private final b:Llk;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 20
    new-instance v0, Lla;

    invoke-direct {v0}, Lla;-><init>()V

    sput-object v0, Lla;->a:Lla;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Llk;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

    invoke-direct {v0, v1}, Llk;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lla;->a:Llk;

    .line 27
    new-instance v0, Llk;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSSz"

    invoke-direct {v0, v1}, Llk;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lla;->b:Llk;

    .line 28
    iget-object v0, p0, Lla;->a:Llk;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Llk;->a(Ljava/util/TimeZone;)V

    .line 29
    iget-object v0, p0, Lla;->b:Llk;

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Llk;->a(Ljava/util/TimeZone;)V

    .line 30
    return-void
.end method

.method public static b(Ljava/util/Date;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lla;->a:Lla;

    iget-object v1, v1, Lla;->a:Llk;

    invoke-virtual {v1, p0}, Llk;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Z"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/Date;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lla;->a:Llk;

    invoke-virtual {v1, p1}, Llk;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "z"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/util/Date;
    .registers 3
    .parameter

    .prologue
    .line 128
    if-nez p1, :cond_4

    .line 129
    const/4 v0, 0x0

    .line 133
    :goto_3
    return-object v0

    .line 131
    :cond_4
    const-string v0, "z"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "Z"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    :cond_14
    iget-object v0, p0, Lla;->a:Llk;

    .line 133
    :goto_16
    invoke-virtual {v0, p1}, Llk;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto :goto_3

    .line 131
    :cond_1b
    iget-object v0, p0, Lla;->b:Llk;

    goto :goto_16
.end method

.method public a(LVV;LkH;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 116
    invoke-virtual {p0, p1, p2}, Lla;->a(LVV;LkO;)V

    .line 117
    return-void
.end method

.method public a(LVV;LkM;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 121
    invoke-virtual {p0, p1, p2}, Lla;->a(LVV;LkO;)V

    .line 122
    invoke-virtual {p1}, LVV;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LkM;->a(Ljava/lang/String;)V

    .line 123
    invoke-virtual {p1}, LVV;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LkM;->h(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public a(LVV;LkO;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 42
    invoke-virtual {p2}, LkO;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, LVV;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lagu;->a(Z)V

    .line 44
    invoke-virtual {p1}, LVV;->v()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lla;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 45
    invoke-virtual {p1}, LVV;->h()Ljava/lang/String;

    move-result-object v5

    .line 46
    instance-of v0, p2, LkM;

    if-eqz v0, :cond_3d

    move-object v0, p2

    .line 47
    check-cast v0, LkM;

    .line 52
    if-eqz v5, :cond_c4

    .line 53
    invoke-virtual {v0}, LkM;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 57
    :goto_2e
    if-nez v4, :cond_3a

    .line 58
    sget-object v4, LUK;->a:LUK;

    invoke-virtual {v0, v3, v4}, LkM;->a(ZLUK;)V

    .line 59
    sget-object v4, LUK;->b:LUK;

    invoke-virtual {v0, v3, v4}, LkM;->a(ZLUK;)V

    .line 61
    :cond_3a
    invoke-virtual {v0, v5}, LkM;->b(Ljava/lang/String;)V

    .line 63
    :cond_3d
    invoke-virtual {p2, v1}, LkO;->b(Ljava/util/Date;)V

    .line 65
    invoke-virtual {p1}, LVV;->s()Ljava/lang/String;

    move-result-object v0

    .line 67
    if-nez v0, :cond_ce

    move-object v0, v1

    :goto_47
    invoke-virtual {p2, v0}, LkO;->a(Ljava/util/Date;)V

    .line 72
    invoke-virtual {p1}, LVV;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lla;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 73
    invoke-virtual {p2}, LkO;->c()Ljava/util/Date;

    move-result-object v0

    .line 74
    if-nez v0, :cond_d4

    move-object v0, v1

    .line 82
    :cond_59
    :goto_59
    invoke-virtual {p2, v0}, LkO;->c(Ljava/util/Date;)V

    .line 84
    invoke-virtual {p1}, LVV;->c()Ljava/lang/String;

    move-result-object v0

    .line 85
    if-nez v0, :cond_df

    move-object v0, v2

    :goto_63
    invoke-virtual {p2, v0}, LkO;->a(Ljava/lang/Long;)V

    .line 88
    invoke-virtual {p1}, LVV;->d()Ljava/lang/String;

    move-result-object v0

    .line 89
    if-nez v0, :cond_ed

    :goto_6c
    invoke-virtual {p2, v2}, LkO;->b(Ljava/lang/Long;)V

    .line 92
    invoke-virtual {p1}, LVV;->f()Ljava/lang/String;

    move-result-object v0

    .line 93
    if-nez v0, :cond_77

    .line 94
    const-string v0, ""

    .line 96
    :cond_77
    invoke-virtual {p2, v0}, LkO;->e(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p1}, LVV;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LkO;->f(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p1}, LVV;->y()Ljava/lang/String;

    move-result-object v0

    .line 99
    if-nez v0, :cond_89

    .line 100
    const-string v0, ""

    .line 102
    :cond_89
    invoke-virtual {p2, v0}, LkO;->d(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p1}, LVV;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, LkO;->i(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p2, v0}, LkO;->g(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p1}, LVV;->a()Z

    move-result v0

    invoke-virtual {p2, v0}, LkO;->c(Z)V

    .line 106
    invoke-virtual {p1}, LVV;->b()Z

    move-result v0

    invoke-virtual {p2, v0}, LkO;->d(Z)V

    .line 107
    invoke-virtual {p1}, LVV;->c()Z

    move-result v0

    invoke-virtual {p2, v0}, LkO;->e(Z)V

    .line 108
    invoke-virtual {p1}, LVV;->e()Z

    move-result v0

    invoke-virtual {p2, v0}, LkO;->f(Z)V

    .line 109
    invoke-virtual {p1}, LVV;->u()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LkO;->c(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p1}, LVV;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_fb

    const/4 v0, 0x1

    :goto_c0
    invoke-virtual {p2, v0}, LkO;->h(Z)V

    .line 111
    return-void

    .line 55
    :cond_c4
    invoke-virtual {p2}, LkO;->b()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/Date;->equals(Ljava/lang/Object;)Z

    move-result v4

    goto/16 :goto_2e

    .line 67
    :cond_ce
    invoke-virtual {p0, v0}, Lla;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto/16 :goto_47

    .line 77
    :cond_d4
    if-eqz v1, :cond_59

    .line 79
    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-nez v4, :cond_59

    move-object v0, v1

    goto/16 :goto_59

    .line 85
    :cond_df
    invoke-virtual {p0, v0}, Lla;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto/16 :goto_63

    .line 89
    :cond_ed
    invoke-virtual {p0, v0}, Lla;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto/16 :goto_6c

    :cond_fb
    move v0, v3

    .line 110
    goto :goto_c0
.end method
