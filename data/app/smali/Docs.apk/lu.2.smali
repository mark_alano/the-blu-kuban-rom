.class public Llu;
.super LlD;
.source "MaybeFailingOperation.java"


# instance fields
.field private a:I

.field a:Z


# direct methods
.method public constructor <init>(LkO;IZ)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 40
    const-string v0, "opMayFail"

    invoke-direct {p0, p1, v0}, LlD;-><init>(LkO;Ljava/lang/String;)V

    .line 41
    iput p2, p0, Llu;->a:I

    .line 42
    iput-boolean p3, p0, Llu;->a:Z

    .line 43
    return-void
.end method

.method public static a(LkO;Lorg/json/JSONObject;)Llu;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 46
    new-instance v0, Llu;

    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "success"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Llu;-><init>(LkO;IZ)V

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 51
    iget v0, p0, Llu;->a:I

    return v0
.end method

.method public a(Llf;LkO;)LlB;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 65
    new-instance v0, Llu;

    iget v1, p0, Llu;->a:I

    iget-boolean v2, p0, Llu;->a:Z

    invoke-direct {v0, p2, v1, v2}, Llu;-><init>(LkO;IZ)V

    .line 66
    return-object v0
.end method

.method public a()Lorg/json/JSONObject;
    .registers 4

    .prologue
    .line 76
    invoke-super {p0}, LlD;->a()Lorg/json/JSONObject;

    move-result-object v0

    .line 77
    const-string v1, "operationName"

    const-string v2, "opMayFail"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 78
    const-string v1, "id"

    iget v2, p0, Llu;->a:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 79
    const-string v1, "success"

    iget-boolean v2, p0, Llu;->a:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 81
    return-object v0
.end method

.method public a(LVV;)V
    .registers 2
    .parameter

    .prologue
    .line 60
    return-void
.end method

.method public a(LlK;LlR;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 71
    iget-boolean v0, p0, Llu;->a:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 86
    instance-of v1, p1, Llu;

    if-nez v1, :cond_6

    .line 90
    :cond_5
    :goto_5
    return v0

    .line 89
    :cond_6
    check-cast p1, Llu;

    .line 90
    invoke-virtual {p0, p1}, Llu;->a(LlC;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget v1, p0, Llu;->a:I

    invoke-virtual {p1}, Llu;->a()I

    move-result v2

    if-ne v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 95
    invoke-virtual {p0}, Llu;->b()I

    move-result v0

    iget v1, p0, Llu;->a:I

    mul-int/lit8 v1, v1, 0x11

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 100
    const-string v0, "OpMayFail[%d, %b, %s]"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Llu;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Llu;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Llu;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
