.class public Llv;
.super Ljava/lang/Object;
.source "MenuItemsState.java"


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Llx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Z

.field private final b:Z

.field private final c:Z


# direct methods
.method private constructor <init>(Ljava/util/Set;ZZZ)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Llx;",
            ">;ZZZ)V"
        }
    .end annotation

    .prologue
    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    iput-object p1, p0, Llv;->a:Ljava/util/Set;

    .line 279
    iput-boolean p2, p0, Llv;->a:Z

    .line 280
    iput-boolean p3, p0, Llv;->b:Z

    .line 281
    iput-boolean p4, p0, Llv;->c:Z

    .line 282
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Set;ZZZLlw;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Llv;-><init>(Ljava/util/Set;ZZZ)V

    return-void
.end method

.method public static a(LkO;Lgl;LKS;)Ljava/lang/String;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 117
    sget-object v0, Lgi;->j:Lgi;

    invoke-interface {p1, v0}, Lgl;->a(Lgi;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 119
    instance-of v0, p0, LkM;

    if-eqz v0, :cond_13

    .line 120
    check-cast p0, LkM;

    invoke-virtual {p0}, LkM;->a()Ljava/lang/String;

    move-result-object v0

    .line 136
    :goto_12
    return-object v0

    .line 121
    :cond_13
    instance-of v0, p0, LkH;

    if-eqz v0, :cond_2e

    .line 122
    const-string v0, "folderSendLinkUrlPattern"

    const-string v1, "https://docs.google.com/folder/d/%1$s/edit"

    invoke-interface {p2, v0, v1}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, LkO;->i()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_12

    .line 136
    :cond_2e
    const/4 v0, 0x0

    goto :goto_12
.end method

.method static a(ZZZZZZZZZZZZZZZ)Llv;
    .registers 19
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 250
    if-nez p3, :cond_4

    if-nez p6, :cond_82

    :cond_4
    const/4 v1, 0x1

    :goto_5
    invoke-static {v1}, Lagu;->a(Z)V

    .line 251
    new-instance v2, Lly;

    const/4 v1, 0x0

    invoke-direct {v2, v1}, Lly;-><init>(Llw;)V

    .line 252
    sget-object v3, Llx;->a:Llx;

    if-eqz p0, :cond_84

    if-eqz p4, :cond_84

    if-eqz p9, :cond_84

    const/4 v1, 0x1

    :goto_17
    invoke-virtual {v2, v3, v1}, Lly;->a(Llx;Z)Lly;

    .line 254
    sget-object v3, Llx;->b:Llx;

    if-eqz p0, :cond_86

    if-eqz p1, :cond_86

    if-nez p8, :cond_86

    const/4 v1, 0x1

    :goto_23
    invoke-virtual {v2, v3, v1}, Lly;->a(Llx;Z)Lly;

    .line 255
    sget-object v3, Llx;->c:Llx;

    if-eqz p0, :cond_88

    if-eqz p2, :cond_88

    const/4 v1, 0x1

    :goto_2d
    invoke-virtual {v2, v3, v1}, Lly;->a(Llx;Z)Lly;

    .line 256
    sget-object v3, Llx;->d:Llx;

    if-eqz p3, :cond_8a

    if-nez p0, :cond_38

    if-eqz p5, :cond_8a

    :cond_38
    const/4 v1, 0x1

    :goto_39
    invoke-virtual {v2, v3, v1}, Lly;->a(Llx;Z)Lly;

    .line 257
    sget-object v3, Llx;->e:Llx;

    if-eqz p3, :cond_8c

    if-nez p0, :cond_44

    if-eqz p5, :cond_8c

    :cond_44
    const/4 v1, 0x1

    :goto_45
    invoke-virtual {v2, v3, v1}, Lly;->a(Llx;Z)Lly;

    .line 258
    sget-object v3, Llx;->g:Llx;

    if-eqz p11, :cond_8e

    if-nez p8, :cond_8e

    const/4 v1, 0x1

    :goto_4f
    invoke-virtual {v2, v3, v1}, Lly;->a(Llx;Z)Lly;

    .line 259
    sget-object v3, Llx;->h:Llx;

    if-eqz p1, :cond_90

    if-eqz p2, :cond_90

    const/4 v1, 0x1

    :goto_59
    invoke-virtual {v2, v3, v1}, Lly;->a(Llx;Z)Lly;

    .line 261
    sget-object v1, Llx;->i:Llx;

    invoke-virtual {v2, v1, p6}, Lly;->a(Llx;Z)Lly;

    .line 262
    sget-object v1, Llx;->j:Llx;

    move/from16 v0, p12

    invoke-virtual {v2, v1, v0}, Lly;->a(Llx;Z)Lly;

    .line 263
    sget-object v3, Llx;->f:Llx;

    if-eqz p14, :cond_92

    if-eqz p0, :cond_92

    const/4 v1, 0x1

    :goto_6f
    invoke-virtual {v2, v3, v1}, Lly;->a(Llx;Z)Lly;

    .line 265
    invoke-virtual {v2, p10}, Lly;->a(Z)Lly;

    .line 266
    invoke-virtual {v2, p7}, Lly;->b(Z)Lly;

    .line 267
    move/from16 v0, p13

    invoke-virtual {v2, v0}, Lly;->c(Z)Lly;

    .line 268
    invoke-virtual {v2}, Lly;->a()Llv;

    move-result-object v1

    return-object v1

    .line 250
    :cond_82
    const/4 v1, 0x0

    goto :goto_5

    .line 252
    :cond_84
    const/4 v1, 0x0

    goto :goto_17

    .line 254
    :cond_86
    const/4 v1, 0x0

    goto :goto_23

    .line 255
    :cond_88
    const/4 v1, 0x0

    goto :goto_2d

    .line 256
    :cond_8a
    const/4 v1, 0x0

    goto :goto_39

    .line 257
    :cond_8c
    const/4 v1, 0x0

    goto :goto_45

    .line 258
    :cond_8e
    const/4 v1, 0x0

    goto :goto_4f

    .line 259
    :cond_90
    const/4 v1, 0x0

    goto :goto_59

    .line 263
    :cond_92
    const/4 v1, 0x0

    goto :goto_6f
.end method


# virtual methods
.method public a()Z
    .registers 2

    .prologue
    .line 289
    iget-boolean v0, p0, Llv;->b:Z

    return v0
.end method

.method public a(Llx;)Z
    .registers 3
    .parameter

    .prologue
    .line 297
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    iget-object v0, p0, Llv;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 293
    iget-boolean v0, p0, Llv;->c:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 303
    if-ne p0, p1, :cond_5

    .line 309
    :cond_4
    :goto_4
    return v0

    .line 305
    :cond_5
    instance-of v2, p1, Llv;

    if-nez v2, :cond_b

    move v0, v1

    .line 306
    goto :goto_4

    .line 308
    :cond_b
    check-cast p1, Llv;

    .line 309
    iget-boolean v2, p0, Llv;->a:Z

    iget-boolean v3, p1, Llv;->a:Z

    if-ne v2, v3, :cond_29

    iget-boolean v2, p0, Llv;->b:Z

    iget-boolean v3, p1, Llv;->b:Z

    if-ne v2, v3, :cond_29

    iget-boolean v2, p0, Llv;->c:Z

    iget-boolean v3, p1, Llv;->c:Z

    if-ne v2, v3, :cond_29

    iget-object v2, p0, Llv;->a:Ljava/util/Set;

    iget-object v3, p1, Llv;->a:Ljava/util/Set;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_29
    move v0, v1

    goto :goto_4
.end method

.method public hashCode()I
    .registers 4

    .prologue
    .line 318
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Llv;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Llv;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Llv;->c:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Llv;->a:Ljava/util/Set;

    aput-object v2, v0, v1

    invoke-static {v0}, Lagp;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 323
    const-string v0, "MenuItemsState[enabledMenuItems=%s, isOwner=%s, isPinned=%s, isCollection=%s]"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Llv;->a:Ljava/util/Set;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Llv;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, Llv;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, Llv;->c:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
