.class public Llz;
.super Ljava/lang/Object;
.source "MenuItemsState.java"


# instance fields
.field private final a:LKS;

.field private final a:LPm;

.field private final a:LUL;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lgl;

.field private final a:LlE;

.field private final a:Llf;

.field private final b:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laoz;Llf;LlE;LPm;LUL;Lgl;LKS;Laoz;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "Llf;",
            "LlE;",
            "LPm;",
            "LUL;",
            "Lgl;",
            "LKS;",
            "Laoz",
            "<",
            "Lfb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-object p1, p0, Llz;->a:Laoz;

    .line 162
    iput-object p2, p0, Llz;->a:Llf;

    .line 163
    iput-object p3, p0, Llz;->a:LlE;

    .line 164
    iput-object p4, p0, Llz;->a:LPm;

    .line 165
    iput-object p5, p0, Llz;->a:LUL;

    .line 166
    iput-object p6, p0, Llz;->a:Lgl;

    .line 167
    iput-object p7, p0, Llz;->a:LKS;

    .line 168
    iput-object p8, p0, Llz;->b:Laoz;

    .line 169
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/util/Set;)Llv;
    .registers 20
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Llv;"
        }
    .end annotation

    .prologue
    .line 172
    invoke-static/range {p2 .. p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    move-object/from16 v0, p0

    iget-object v1, v0, Llz;->a:Laoz;

    invoke-interface {v1}, Laoz;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/content/Context;

    .line 175
    move-object/from16 v0, p0

    iget-object v1, v0, Llz;->a:Llf;

    move-object/from16 v0, p1

    invoke-interface {v1, v0}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v3

    .line 178
    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v4, 0x1

    if-ne v1, v4, :cond_10e

    .line 181
    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 182
    if-nez v3, :cond_f3

    if-eqz v1, :cond_f3

    const/4 v14, 0x0

    .line 185
    :goto_2e
    if-eqz v14, :cond_10e

    .line 186
    invoke-static {v2}, LZJ;->a(Landroid/content/Context;)Z

    move-result v1

    .line 187
    move-object/from16 v0, p0

    iget-object v3, v0, Llz;->a:LlE;

    invoke-static {v3, v2}, LlN;->a(LlE;Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_fd

    const/4 v2, 0x1

    .line 190
    :goto_3f
    invoke-virtual {v14}, LkO;->k()Z

    move-result v3

    .line 191
    invoke-virtual {v14}, LkO;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_100

    const/4 v4, 0x1

    .line 195
    :goto_4a
    move-object/from16 v0, p0

    iget-object v5, v0, Llz;->a:LPm;

    invoke-virtual {v14, v5}, LkO;->a(LPm;)Z

    move-result v5

    if-eqz v5, :cond_103

    move-object/from16 v0, p0

    iget-object v5, v0, Llz;->a:Lgl;

    sget-object v6, Lgi;->b:Lgi;

    invoke-interface {v5, v6}, Lgl;->a(Lgi;)Z

    move-result v5

    if-eqz v5, :cond_103

    move-object/from16 v0, p0

    iget-object v5, v0, Llz;->a:LKS;

    const-string v6, "enableDocumentlistComments"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_103

    const/4 v10, 0x1

    .line 198
    :goto_6e
    invoke-virtual {v14}, LkO;->f()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 199
    sget-object v5, LfS;->c:LfS;

    invoke-virtual {v14}, LkO;->a()LkP;

    move-result-object v6

    invoke-virtual {v5, v6}, LfS;->a(LkP;)LUK;

    move-result-object v6

    .line 200
    instance-of v5, v14, LkM;

    if-eqz v5, :cond_106

    move-object/from16 v0, p0

    iget-object v7, v0, Llz;->a:LUL;

    move-object v5, v14

    check-cast v5, LkM;

    invoke-interface {v7, v5, v6}, LUL;->c(LkM;LUK;)Z

    move-result v5

    if-eqz v5, :cond_106

    const/4 v6, 0x1

    .line 203
    :goto_94
    invoke-virtual {v14}, LkO;->i()Z

    move-result v7

    .line 204
    invoke-virtual {v14}, LkO;->g()Z

    move-result v8

    .line 205
    invoke-virtual {v14}, LkO;->f()Z

    move-result v9

    .line 206
    move-object/from16 v0, p0

    iget-object v5, v0, Llz;->a:Lgl;

    move-object/from16 v0, p0

    iget-object v12, v0, Llz;->a:LKS;

    invoke-static {v14, v5, v12}, Llv;->a(LkO;Lgl;LKS;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_108

    const/4 v12, 0x1

    .line 208
    :goto_af
    invoke-static {v14}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(LkO;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_10a

    move-object/from16 v0, p0

    iget-object v5, v0, Llz;->a:Lgl;

    sget-object v13, Lgi;->k:Lgi;

    invoke-interface {v5, v13}, Lgl;->a(Lgi;)Z

    move-result v5

    if-eqz v5, :cond_10a

    const/4 v15, 0x1

    .line 210
    :goto_c2
    move-object/from16 v0, p0

    iget-object v5, v0, Llz;->b:Laoz;

    invoke-interface {v5}, Laoz;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lfb;

    .line 211
    move-object/from16 v0, p0

    iget-object v13, v0, Llz;->a:Lgl;

    sget-object v16, Lgi;->C:Lgi;

    move-object/from16 v0, v16

    invoke-interface {v13, v0}, Lgl;->a(Lgi;)Z

    move-result v13

    if-eqz v13, :cond_10c

    sget-object v13, Lfb;->b:Lfb;

    invoke-virtual {v5, v13}, Lfb;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10c

    const/4 v13, 0x1

    .line 213
    :goto_e3
    invoke-virtual {v14}, LkO;->a()LkP;

    move-result-object v5

    sget-object v14, LkP;->h:LkP;

    invoke-virtual {v5, v14}, LkP;->equals(Ljava/lang/Object;)Z

    move-result v14

    move v5, v3

    .line 215
    invoke-static/range {v1 .. v15}, Llv;->a(ZZZZZZZZZZZZZZZ)Llv;

    move-result-object v1

    .line 236
    :goto_f2
    return-object v1

    .line 182
    :cond_f3
    move-object/from16 v0, p0

    iget-object v4, v0, Llz;->a:Llf;

    invoke-interface {v4, v3, v1}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v14

    goto/16 :goto_2e

    .line 187
    :cond_fd
    const/4 v2, 0x0

    goto/16 :goto_3f

    .line 191
    :cond_100
    const/4 v4, 0x0

    goto/16 :goto_4a

    .line 195
    :cond_103
    const/4 v10, 0x0

    goto/16 :goto_6e

    .line 200
    :cond_106
    const/4 v6, 0x0

    goto :goto_94

    .line 206
    :cond_108
    const/4 v12, 0x0

    goto :goto_af

    .line 208
    :cond_10a
    const/4 v15, 0x0

    goto :goto_c2

    .line 211
    :cond_10c
    const/4 v13, 0x0

    goto :goto_e3

    .line 223
    :cond_10e
    const/4 v2, 0x1

    .line 224
    if-eqz v3, :cond_147

    .line 225
    invoke-interface/range {p2 .. p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_115
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_147

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 226
    move-object/from16 v0, p0

    iget-object v5, v0, Llz;->a:Llf;

    invoke-interface {v5, v3, v1}, Llf;->a(LkB;Ljava/lang/String;)LkO;

    move-result-object v1

    .line 227
    if-eqz v1, :cond_115

    invoke-virtual {v1}, LkO;->f()Z

    move-result v1

    if-nez v1, :cond_115

    .line 228
    const/4 v1, 0x0

    .line 234
    :goto_132
    new-instance v2, Lly;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lly;-><init>(Llw;)V

    .line 235
    sget-object v3, Llx;->b:Llx;

    if-nez v1, :cond_145

    const/4 v1, 0x1

    :goto_13d
    invoke-virtual {v2, v3, v1}, Lly;->a(Llx;Z)Lly;

    .line 236
    invoke-virtual {v2}, Lly;->a()Llv;

    move-result-object v1

    goto :goto_f2

    .line 235
    :cond_145
    const/4 v1, 0x0

    goto :goto_13d

    :cond_147
    move v1, v2

    goto :goto_132
.end method
