.class public LmR;
.super Ljava/lang/Object;
.source "EntryViewBinder.java"

# interfaces
.implements LbW;


# instance fields
.field private a:I

.field private final a:LKS;

.field private final a:LUL;

.field private a:LWX;

.field private final a:LaaY;

.field private final a:LabB;

.field private a:Landroid/database/Cursor;

.field private final a:Lgl;

.field private final a:Ljava/lang/String;

.field private a:LkY;

.field private final a:Llf;

.field private final a:LmX;

.field public final a:LmY;

.field private final a:LqT;

.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Z

.field private final f:Z

.field private final g:Z

.field private final h:Z


# direct methods
.method public constructor <init>(Llf;LUL;LaaY;LmY;LmX;LqT;Ljava/lang/String;LKS;LabB;ZZLgl;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, LmR;->a:Landroid/database/Cursor;

    .line 160
    const/4 v0, -0x1

    iput v0, p0, LmR;->a:I

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, LmR;->a:Z

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, LmR;->b:Z

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, LmR;->c:Z

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, LmR;->a:LWX;

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, LmR;->d:Z

    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, LmR;->e:Z

    .line 185
    iput-object p1, p0, LmR;->a:Llf;

    .line 186
    iput-object p2, p0, LmR;->a:LUL;

    .line 187
    iput-object p4, p0, LmR;->a:LmY;

    .line 188
    iput-object p5, p0, LmR;->a:LmX;

    .line 189
    iput-object p6, p0, LmR;->a:LqT;

    .line 190
    iput-object p7, p0, LmR;->a:Ljava/lang/String;

    .line 191
    iput-object p3, p0, LmR;->a:LaaY;

    .line 192
    iput-object p8, p0, LmR;->a:LKS;

    .line 193
    iput-object p9, p0, LmR;->a:LabB;

    .line 194
    const-string v0, "enableMultiSelect"

    const/4 v1, 0x0

    invoke-interface {p8, v0, v1}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LmR;->f:Z

    .line 196
    iput-boolean p10, p0, LmR;->g:Z

    .line 197
    iput-boolean p11, p0, LmR;->h:Z

    .line 198
    iput-object p12, p0, LmR;->a:Lgl;

    .line 199
    return-void
.end method

.method static synthetic a(LmR;)LmX;
    .registers 2
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, LmR;->a:LmX;

    return-object v0
.end method

.method private a(Landroid/database/Cursor;)V
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 202
    iget-object v0, p0, LmR;->a:Landroid/database/Cursor;

    if-ne v0, p1, :cond_e

    iget v0, p0, LmR;->a:I

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    if-eq v0, v3, :cond_87

    .line 203
    :cond_e
    iput-object p1, p0, LmR;->a:Landroid/database/Cursor;

    .line 204
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iput v0, p0, LmR;->a:I

    .line 205
    invoke-static {p1}, LPW;->c(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 206
    sget-object v0, LPX;->u:LPX;

    invoke-virtual {v0}, LPX;->a()LPI;

    move-result-object v0

    invoke-virtual {v0, p1}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LmR;->a:Z

    .line 207
    iget-object v0, p0, LmR;->a:Ljava/lang/String;

    invoke-static {v0, v3}, LkY;->a(Ljava/lang/String;Ljava/lang/String;)LkY;

    move-result-object v0

    iput-object v0, p0, LmR;->a:LkY;

    .line 208
    iget-object v0, p0, LmR;->a:LkY;

    invoke-static {v0}, Lcom/google/android/apps/docs/sync/syncadapter/ContentSyncReceiver;->a(LkY;)LWX;

    move-result-object v0

    iput-object v0, p0, LmR;->a:LWX;

    .line 209
    iget-object v0, p0, LmR;->a:LWX;

    if-eqz v0, :cond_88

    iget-object v0, p0, LmR;->a:LWX;

    invoke-virtual {v0}, LWX;->a()LWW;

    move-result-object v0

    sget-object v4, LWW;->a:LWW;

    if-ne v0, v4, :cond_88

    move v0, v1

    :goto_49
    iput-boolean v0, p0, LmR;->b:Z

    .line 210
    iget-object v0, p0, LmR;->a:LWX;

    if-eqz v0, :cond_8a

    iget-object v0, p0, LmR;->a:LWX;

    invoke-virtual {v0}, LWX;->a()LWW;

    move-result-object v0

    sget-object v4, LWW;->b:LWW;

    if-ne v0, v4, :cond_8a

    move v0, v1

    :goto_5a
    iput-boolean v0, p0, LmR;->c:Z

    .line 212
    iget-object v0, p0, LmR;->a:Llf;

    iget-object v4, p0, LmR;->a:Ljava/lang/String;

    invoke-interface {v0, v4}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 213
    iget-object v4, p0, LmR;->a:Llf;

    invoke-interface {v4, v0, v3}, Llf;->a(LkB;Ljava/lang/String;)LkM;

    move-result-object v3

    .line 214
    if-eqz v3, :cond_8c

    iget-object v0, p0, LmR;->a:LUL;

    sget-object v4, LUK;->a:LUK;

    invoke-interface {v0, v3, v4}, LUL;->a(LkM;LUK;)Z

    move-result v0

    if-eqz v0, :cond_8c

    move v0, v1

    :goto_77
    iput-boolean v0, p0, LmR;->d:Z

    .line 216
    if-eqz v3, :cond_8e

    iget-object v0, p0, LmR;->a:LUL;

    sget-object v4, LUK;->a:LUK;

    invoke-interface {v0, v3, v4}, LUL;->b(LkM;LUK;)Z

    move-result v0

    if-eqz v0, :cond_8e

    :goto_85
    iput-boolean v1, p0, LmR;->e:Z

    .line 219
    :cond_87
    return-void

    :cond_88
    move v0, v2

    .line 209
    goto :goto_49

    :cond_8a
    move v0, v2

    .line 210
    goto :goto_5a

    :cond_8c
    move v0, v2

    .line 214
    goto :goto_77

    :cond_8e
    move v1, v2

    .line 216
    goto :goto_85
.end method

.method private a(Landroid/widget/TextView;Z)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 403
    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 405
    if-eqz p2, :cond_12

    .line 406
    const/4 v1, 0x5

    sget v2, Leh;->title:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 410
    :goto_e
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 411
    return-void

    .line 408
    :cond_12
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_e
.end method


# virtual methods
.method public a(Landroid/view/View;Landroid/database/Cursor;I)Z
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 223
    invoke-direct {p0, p2}, LmR;->a(Landroid/database/Cursor;)V

    .line 224
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 226
    iget-object v0, p0, LmR;->a:LabB;

    invoke-interface {v0}, LabB;->b()Z

    move-result v0

    if-nez v0, :cond_54

    move v0, v2

    .line 228
    :goto_14
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 229
    sget v6, Leh;->title:I

    if-ne v1, v6, :cond_5f

    .line 230
    check-cast p1, Landroid/widget/TextView;

    .line 231
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    iget-object v0, p0, LmR;->a:Lgl;

    sget-object v1, Lgi;->o:Lgi;

    invoke-interface {v0, v1}, Lgl;->a(Lgi;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 233
    sget-object v0, LPX;->e:LPX;

    invoke-virtual {v0}, LPX;->a()LPI;

    move-result-object v0

    invoke-virtual {v0, p2}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 235
    sget-object v4, LPX;->h:LPX;

    invoke-virtual {v4}, LPX;->a()LPI;

    move-result-object v4

    invoke-virtual {v4, p2}, LPI;->a(Landroid/database/Cursor;)J

    move-result-wide v4

    .line 237
    cmp-long v0, v4, v0

    if-gez v0, :cond_4c

    move v3, v2

    .line 240
    :cond_4c
    if-eqz v3, :cond_56

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    :goto_50
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 395
    :cond_53
    :goto_53
    return v2

    :cond_54
    move v0, v3

    .line 226
    goto :goto_14

    .line 240
    :cond_56
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    goto :goto_50

    .line 242
    :cond_59
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_53

    .line 245
    :cond_5f
    sget v6, Leh;->star_cb:I

    if-ne v1, v6, :cond_90

    move-object v0, p1

    .line 247
    check-cast v0, Landroid/widget/ToggleButton;

    .line 248
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_8a

    move v1, v2

    :goto_6d
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 249
    iget-object v1, p0, LmR;->a:LKS;

    const-string v4, "canChangeStarring"

    invoke-interface {v1, v4, v2}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_8c

    .line 250
    invoke-virtual {p1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 254
    invoke-static {p2}, LPW;->c(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 255
    new-instance v3, LmS;

    invoke-direct {v3, p0, v1}, LmS;-><init>(LmR;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_53

    :cond_8a
    move v1, v3

    .line 248
    goto :goto_6d

    .line 263
    :cond_8c
    invoke-virtual {p1, v3}, Landroid/view/View;->setClickable(Z)V

    goto :goto_53

    .line 266
    :cond_90
    sget v6, Leh;->group_title:I

    if-ne v1, v6, :cond_e4

    .line 267
    iget-object v0, p0, LmR;->a:LqT;

    invoke-virtual {v0}, LqT;->a()Lri;

    move-result-object v6

    .line 268
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v1, p1

    .line 269
    check-cast v1, Landroid/widget/TextView;

    .line 271
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v7

    sget v8, Leh;->group_title_container:I

    if-ne v7, v8, :cond_26c

    .line 273
    sget v4, Leh;->group_padding:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object p1, v0

    move-object v0, v4

    .line 275
    :goto_b3
    invoke-interface {v6}, Lri;->a()Z

    move-result v4

    if-eqz v4, :cond_be

    .line 276
    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    move v2, v3

    .line 277
    goto :goto_53

    .line 279
    :cond_be
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 280
    if-eqz v0, :cond_cd

    .line 281
    invoke-interface {p2}, Landroid/database/Cursor;->isFirst()Z

    move-result v4

    if-eqz v4, :cond_ca

    move v3, v5

    :cond_ca
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 284
    :cond_cd
    invoke-interface {v6}, Lri;->a()Ljava/lang/String;

    move-result-object v0

    .line 285
    if-nez v0, :cond_df

    .line 286
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {v6}, Lri;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 288
    :cond_df
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_53

    .line 290
    :cond_e4
    sget v6, Leh;->date:I

    if-ne v1, v6, :cond_10b

    .line 291
    iget-boolean v0, p0, LmR;->g:Z

    if-eqz v0, :cond_fe

    :goto_ec
    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 292
    check-cast p1, Landroid/widget/TextView;

    .line 296
    invoke-interface {p2, p3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_100

    .line 297
    const-string v0, ""

    .line 302
    :goto_f9
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_53

    :cond_fe
    move v5, v3

    .line 291
    goto :goto_ec

    .line 299
    :cond_100
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 300
    iget-object v3, p0, LmR;->a:LaaY;

    invoke-virtual {v3, v0, v1}, LaaY;->a(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_f9

    .line 304
    :cond_10b
    sget v6, Leh;->last_modifier_name:I

    if-ne v1, v6, :cond_12a

    .line 305
    iget-boolean v0, p0, LmR;->h:Z

    .line 307
    if-eqz v0, :cond_11a

    .line 308
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 309
    if-nez v4, :cond_11a

    move v0, v3

    .line 313
    :cond_11a
    if-eqz v0, :cond_128

    :goto_11c
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 314
    if-eqz v0, :cond_53

    .line 315
    check-cast p1, Landroid/widget/TextView;

    .line 316
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_53

    :cond_128
    move v3, v5

    .line 313
    goto :goto_11c

    .line 319
    :cond_12a
    sget v6, Leh;->preview_button:I

    if-ne v1, v6, :cond_137

    .line 320
    if-eqz v0, :cond_135

    :goto_130
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_53

    :cond_135
    move v3, v5

    goto :goto_130

    .line 322
    :cond_137
    sget v0, Leh;->selected_checkbox:I

    if-ne v1, v0, :cond_165

    .line 323
    iget-boolean v0, p0, LmR;->f:Z

    if-eqz v0, :cond_53

    .line 326
    iget-object v0, p0, LmR;->a:LabB;

    invoke-interface {v0}, LabB;->b()Z

    move-result v0

    if-eqz v0, :cond_163

    :goto_147
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 327
    check-cast p1, Landroid/widget/ToggleButton;

    .line 328
    iget-object v0, p0, LmR;->a:LkY;

    iget-object v0, v0, LkY;->b:Ljava/lang/String;

    .line 329
    iget-object v1, p0, LmR;->a:LmX;

    invoke-interface {v1, v0}, LmX;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 331
    new-instance v1, LmT;

    invoke-direct {v1, p0, v0}, LmT;-><init>(LmR;Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_53

    :cond_163
    move v3, v5

    .line 326
    goto :goto_147

    .line 338
    :cond_165
    sget v0, Leh;->pin_status:I

    if-ne v1, v0, :cond_1a3

    move-object v0, p1

    .line 339
    check-cast v0, Landroid/widget/TextView;

    .line 341
    iget-boolean v1, p0, LmR;->g:Z

    if-eqz v1, :cond_18d

    .line 342
    iget-boolean v1, p0, LmR;->e:Z

    if-nez v1, :cond_183

    .line 343
    sget v1, Len;->pin_not_available:I

    .line 358
    :goto_176
    iget-boolean v4, p0, LmR;->g:Z

    invoke-direct {p0, v0, v4}, LmR;->a(Landroid/widget/TextView;Z)V

    .line 359
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 360
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_53

    .line 344
    :cond_183
    iget-boolean v1, p0, LmR;->d:Z

    if-nez v1, :cond_18a

    .line 345
    sget v1, Len;->pin_out_of_date:I

    goto :goto_176

    .line 347
    :cond_18a
    sget v1, Len;->pin_up_to_date:I

    goto :goto_176

    .line 350
    :cond_18d
    iget-boolean v1, p0, LmR;->a:Z

    if-eqz v1, :cond_198

    iget-boolean v1, p0, LmR;->e:Z

    if-eqz v1, :cond_198

    .line 351
    sget v1, Len;->pin_offline:I

    goto :goto_176

    .line 353
    :cond_198
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_53

    .line 362
    :cond_1a3
    sget v0, Leh;->pin_update:I

    if-ne v1, v0, :cond_1c8

    .line 363
    iget-boolean v0, p0, LmR;->g:Z

    if-eqz v0, :cond_1c6

    iget-boolean v0, p0, LmR;->d:Z

    if-nez v0, :cond_1c6

    iget-boolean v0, p0, LmR;->b:Z

    if-nez v0, :cond_1c6

    iget-boolean v0, p0, LmR;->c:Z

    if-nez v0, :cond_1c6

    :goto_1b7
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 365
    new-instance v0, LmZ;

    iget-object v1, p0, LmR;->a:LkY;

    invoke-direct {v0, v1}, LmZ;-><init>(LkY;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_53

    :cond_1c6
    move v3, v5

    .line 363
    goto :goto_1b7

    .line 367
    :cond_1c8
    sget v0, Leh;->pin_waiting:I

    if-ne v1, v0, :cond_1f5

    .line 368
    iget-boolean v0, p0, LmR;->g:Z

    if-eqz v0, :cond_1f3

    iget-boolean v0, p0, LmR;->b:Z

    if-eqz v0, :cond_1f3

    :goto_1d4
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 369
    iget-boolean v0, p0, LmR;->g:Z

    if-eqz v0, :cond_53

    .line 370
    new-instance v0, LmU;

    iget-object v1, p0, LmR;->a:LkY;

    invoke-direct {v0, v1}, LmU;-><init>(LkY;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    new-instance v0, LmV;

    sget-object v1, LmW;->a:LmW;

    iget-object v3, p0, LmR;->a:LkY;

    invoke-direct {v0, v1, v3}, LmV;-><init>(LmW;LkY;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_53

    :cond_1f3
    move v3, v5

    .line 368
    goto :goto_1d4

    .line 374
    :cond_1f5
    sget v0, Leh;->pin_progress:I

    if-ne v1, v0, :cond_232

    .line 375
    iget-boolean v0, p0, LmR;->g:Z

    if-eqz v0, :cond_230

    iget-boolean v0, p0, LmR;->c:Z

    if-eqz v0, :cond_230

    :goto_201
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 376
    iget-boolean v0, p0, LmR;->g:Z

    if-eqz v0, :cond_21e

    iget-boolean v0, p0, LmR;->c:Z

    if-eqz v0, :cond_21e

    move-object v0, p1

    .line 377
    check-cast v0, Lcom/google/android/apps/docs/view/ProgressButton;

    iget-object v1, p0, LmR;->a:LWX;

    invoke-static {v4, v0, v1}, LaaH;->a(Lcom/google/android/apps/docs/view/CustomButton;Lcom/google/android/apps/docs/view/ProgressButton;LWX;)V

    .line 378
    new-instance v0, LmU;

    iget-object v1, p0, LmR;->a:LkY;

    invoke-direct {v0, v1}, LmU;-><init>(LkY;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 380
    :cond_21e
    iget-boolean v0, p0, LmR;->g:Z

    if-eqz v0, :cond_53

    .line 381
    new-instance v0, LmV;

    sget-object v1, LmW;->b:LmW;

    iget-object v3, p0, LmR;->a:LkY;

    invoke-direct {v0, v1, v3}, LmV;-><init>(LmW;LkY;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_53

    :cond_230
    move v3, v5

    .line 375
    goto :goto_201

    .line 384
    :cond_232
    sget v0, Leh;->pin_filler:I

    if-ne v1, v0, :cond_244

    .line 385
    iget-boolean v0, p0, LmR;->g:Z

    if-eqz v0, :cond_23e

    iget-boolean v0, p0, LmR;->d:Z

    if-eqz v0, :cond_23f

    :cond_23e
    move v3, v5

    :cond_23f
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_53

    .line 387
    :cond_244
    sget v0, Leh;->doc_icon:I

    if-ne v1, v0, :cond_269

    .line 388
    check-cast p1, Landroid/widget/ImageView;

    .line 389
    invoke-static {p2}, LPW;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 390
    invoke-static {p2}, LPW;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 391
    sget-object v3, LPX;->j:LPX;

    invoke-virtual {v3}, LPX;->a()LPI;

    move-result-object v3

    invoke-virtual {v3, p2}, LPI;->a(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 392
    invoke-static {v0, v1, v3}, LkO;->a(Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_53

    :cond_269
    move v2, v3

    .line 395
    goto/16 :goto_53

    :cond_26c
    move-object v0, v4

    goto/16 :goto_b3
.end method
