.class public final enum LmW;
.super Ljava/lang/Enum;
.source "EntryViewBinder.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LmW;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LmW;

.field private static final synthetic a:[LmW;

.field public static final enum b:LmW;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    new-instance v0, LmW;

    const-string v1, "WAITING"

    invoke-direct {v0, v1, v2}, LmW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LmW;->a:LmW;

    .line 94
    new-instance v0, LmW;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v3}, LmW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LmW;->b:LmW;

    .line 92
    const/4 v0, 0x2

    new-array v0, v0, [LmW;

    sget-object v1, LmW;->a:LmW;

    aput-object v1, v0, v2

    sget-object v1, LmW;->b:LmW;

    aput-object v1, v0, v3

    sput-object v0, LmW;->a:[LmW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LmW;
    .registers 2
    .parameter

    .prologue
    .line 92
    const-class v0, LmW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LmW;

    return-object v0
.end method

.method public static values()[LmW;
    .registers 1

    .prologue
    .line 92
    sget-object v0, LmW;->a:[LmW;

    invoke-virtual {v0}, [LmW;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LmW;

    return-object v0
.end method
