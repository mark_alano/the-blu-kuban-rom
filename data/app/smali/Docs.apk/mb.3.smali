.class public Lmb;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements Lma;


# instance fields
.field private final a:LNe;

.field private a:Lafo;

.field private final a:Landroid/os/Handler;

.field private a:Lcom/google/api/services/discussions/Discussions;

.field private final a:LeQ;

.field private final a:Ljava/lang/Object;

.field private final a:Ljava/lang/String;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lmq;",
            ">;"
        }
    .end annotation
.end field

.field private a:LlW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LlW",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lmo;

.field private a:Z

.field private final b:Ljava/lang/Object;

.field private final b:Ljava/lang/String;

.field private b:LlW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LlW",
            "<",
            "Lmx;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Object;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LNe;LeQ;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lmb;->a:Landroid/os/Handler;

    .line 104
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmb;->a:Ljava/lang/Object;

    .line 112
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lmb;->a:Ljava/util/Set;

    .line 115
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmb;->b:Ljava/lang/Object;

    .line 136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmb;->c:Ljava/lang/Object;

    .line 137
    sget-object v0, Lmo;->a:Lmo;

    iput-object v0, p0, Lmb;->a:Lmo;

    .line 143
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lmb;->d:Ljava/lang/Object;

    .line 151
    iput-object p1, p0, Lmb;->a:Ljava/lang/String;

    .line 152
    iput-object p2, p0, Lmb;->b:Ljava/lang/String;

    .line 153
    iput-object p3, p0, Lmb;->a:LNe;

    .line 154
    iput-object p4, p0, Lmb;->a:LeQ;

    .line 155
    iput-object p5, p0, Lmb;->c:Ljava/lang/String;

    .line 156
    iput-object v1, p0, Lmb;->a:LlW;

    .line 157
    iput-object v1, p0, Lmb;->a:Lafo;

    .line 158
    iput-object v1, p0, Lmb;->b:LlW;

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmb;->a:Z

    .line 160
    return-void
.end method

.method static synthetic a(Lmb;)LNe;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->a:LNe;

    return-object v0
.end method

.method static synthetic a(Lmb;Lafo;)Lafo;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lmb;->a:Lafo;

    return-object p1
.end method

.method static synthetic a(Lmb;)Lcom/google/api/services/discussions/Discussions;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->a:Lcom/google/api/services/discussions/Discussions;

    return-object v0
.end method

.method static synthetic a(Lmb;)LeQ;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->a:LeQ;

    return-object v0
.end method

.method static synthetic a(Lmb;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private a(Ljava/lang/String;I)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 253
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p2, :cond_11

    .line 254
    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 256
    :cond_11
    return-object p1
.end method

.method static synthetic a(Lmb;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lmb;)Ljava/util/TreeSet;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->a:Ljava/util/TreeSet;

    return-object v0
.end method

.method private a(Ljava/lang/String;Lafo;)LlV;
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lafo;",
            ")",
            "LlV",
            "<",
            "Lcom/google/api/services/discussions/model/DiscussionFeed;",
            ">;"
        }
    .end annotation

    .prologue
    .line 537
    new-instance v0, LlW;

    invoke-direct {v0}, LlW;-><init>()V

    .line 538
    new-instance v1, Lmg;

    invoke-direct {v1, p0, v0}, Lmg;-><init>(Lmb;LlW;)V

    invoke-direct {p0, v0, p1, p2, v1}, Lmb;->a(LlW;Ljava/lang/String;Lafo;Ljava/lang/Runnable;)V

    .line 555
    return-object v0
.end method

.method static synthetic a(Lmb;)LlW;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->b:LlW;

    return-object v0
.end method

.method static synthetic a(Lmb;LlW;)LlW;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lmb;->b:LlW;

    return-object p1
.end method

.method static synthetic a(Lmb;)Lmo;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->a:Lmo;

    return-object v0
.end method

.method static synthetic a(Lmb;Lmo;)Lmo;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lmb;->a:Lmo;

    return-object p1
.end method

.method private a(Lmz;)Lmq;
    .registers 3
    .parameter

    .prologue
    .line 308
    instance-of v0, p1, Lmq;

    if-eqz v0, :cond_7

    .line 309
    check-cast p1, Lmq;

    .line 311
    :goto_6
    return-object p1

    :cond_7
    invoke-interface {p1}, Lmz;->a()Lmz;

    move-result-object v0

    check-cast v0, Lmq;

    move-object p1, v0

    goto :goto_6
.end method

.method private declared-synchronized a()V
    .registers 5

    .prologue
    .line 300
    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lmb;->a:Ljava/util/Set;

    monitor-enter v1
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_1f

    .line 301
    :try_start_4
    iget-object v0, p0, Lmb;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 302
    iget-object v3, p0, Lmb;->a:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_a

    .line 304
    :catchall_1c
    move-exception v0

    monitor-exit v1
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_1c

    :try_start_1e
    throw v0
    :try_end_1f
    .catchall {:try_start_1e .. :try_end_1f} :catchall_1f

    .line 300
    :catchall_1f
    move-exception v0

    monitor-exit p0

    throw v0

    .line 304
    :cond_22
    :try_start_22
    monitor-exit v1
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_1c

    .line 305
    monitor-exit p0

    return-void
.end method

.method private a(LadQ;)V
    .registers 4
    .parameter

    .prologue
    .line 325
    const-string v0, "DiscussionSessionApi"

    const-string v1, "checkAuthToken"

    invoke-static {v0, v1, p1}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 326
    invoke-virtual {p1}, LadQ;->a()I

    move-result v0

    const/16 v1, 0x191

    if-lt v0, v1, :cond_12

    .line 329
    invoke-direct {p0}, Lmb;->b()V

    .line 331
    :cond_12
    return-void
.end method

.method private a(Ljava/lang/String;Lafo;Lafo;LlW;Ljava/util/TreeSet;I)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lafo;",
            "Lafo;",
            "LlW",
            "<",
            "Ljava/lang/Void;",
            ">;",
            "Ljava/util/TreeSet",
            "<",
            "Lmq;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 397
    invoke-direct {p0, p1, p2}, Lmb;->a(Ljava/lang/String;Lafo;)LlV;

    move-result-object v3

    .line 398
    iget-object v8, p0, Lmb;->a:Landroid/os/Handler;

    new-instance v0, Lmf;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p5

    move-object v5, p2

    move-object v6, p4

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lmf;-><init>(Lmb;Lafo;LlV;Ljava/util/TreeSet;Lafo;LlW;I)V

    invoke-interface {v3, v8, v0}, LlV;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 491
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lafo;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 167
    const-string v0, "can\'t refresh API with null OAuth token"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v1, p0, Lmb;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 169
    :try_start_8
    invoke-static {}, LadL;->a()Laeq;

    move-result-object v0

    .line 171
    new-instance v2, LaeW;

    invoke-direct {v2}, LaeW;-><init>()V

    invoke-static {v0, v2}, Lcom/google/api/services/discussions/Discussions;->a(Laeq;LaeP;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    new-instance v2, Lmn;

    invoke-direct {v2, p1, p2, p3}, Lmn;-><init>(Ljava/lang/String;Ljava/lang/String;Lafo;)V

    invoke-virtual {v0, v2}, Lcom/google/api/services/discussions/Discussions$Builder;->a(LaeL;)Lcom/google/api/services/discussions/Discussions$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions$Builder;->a()Lcom/google/api/services/discussions/Discussions;

    move-result-object v0

    iput-object v0, p0, Lmb;->a:Lcom/google/api/services/discussions/Discussions;

    .line 175
    monitor-exit v1

    .line 176
    return-void

    .line 175
    :catchall_26
    move-exception v0

    monitor-exit v1
    :try_end_28
    .catchall {:try_start_8 .. :try_end_28} :catchall_26

    throw v0
.end method

.method private a(Ljava/util/SortedSet;Z)V
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmq;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 500
    iget-object v1, p0, Lmb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 501
    :try_start_3
    iget-object v0, p0, Lmb;->a:Ljava/util/TreeSet;

    if-nez v0, :cond_15

    .line 503
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0, p1}, Ljava/util/TreeSet;-><init>(Ljava/util/SortedSet;)V

    iput-object v0, p0, Lmb;->a:Ljava/util/TreeSet;

    .line 520
    :goto_e
    if-eqz p2, :cond_13

    .line 521
    invoke-direct {p0}, Lmb;->a()V

    .line 523
    :cond_13
    monitor-exit v1

    .line 524
    return-void

    .line 506
    :cond_15
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 507
    invoke-interface {p1}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmq;

    .line 508
    invoke-virtual {v0}, Lmq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1e

    .line 523
    :catchall_32
    move-exception v0

    monitor-exit v1
    :try_end_34
    .catchall {:try_start_3 .. :try_end_34} :catchall_32

    throw v0

    .line 510
    :cond_35
    :try_start_35
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 511
    iget-object v0, p0, Lmb;->a:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_40
    :goto_40
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmq;

    .line 513
    invoke-virtual {v0}, Lmq;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_40

    .line 514
    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_40

    .line 517
    :cond_5a
    iget-object v0, p0, Lmb;->a:Ljava/util/TreeSet;

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->removeAll(Ljava/util/Collection;)Z

    .line 518
    iget-object v0, p0, Lmb;->a:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z
    :try_end_64
    .catchall {:try_start_35 .. :try_end_64} :catchall_32

    goto :goto_e
.end method

.method private a(LlW;Ljava/lang/Runnable;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LlW",
            "<*>;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 185
    invoke-direct {p0, p1, v0, v0, p2}, Lmb;->a(LlW;Ljava/lang/String;Lafo;Ljava/lang/Runnable;)V

    .line 186
    return-void
.end method

.method private a(LlW;Ljava/lang/String;Lafo;Ljava/lang/Runnable;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LlW",
            "<*>;",
            "Ljava/lang/String;",
            "Lafo;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 199
    const-string v0, "Must be given a future to fail if we can\'t get token"

    invoke-static {p1, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    const-string v0, "Task must be non-null"

    invoke-static {p4, v0}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    new-instance v0, Lmc;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lmc;-><init>(Lmb;Ljava/lang/String;Lafo;Ljava/lang/Runnable;LlW;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lmc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 243
    return-void
.end method

.method static synthetic a(Lmb;)V
    .registers 1
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Lmb;->b()V

    return-void
.end method

.method static synthetic a(Lmb;LadQ;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lmb;->a(LadQ;)V

    return-void
.end method

.method static synthetic a(Lmb;Ljava/lang/String;Lafo;Lafo;LlW;Ljava/util/TreeSet;I)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct/range {p0 .. p6}, Lmb;->a(Ljava/lang/String;Lafo;Lafo;LlW;Ljava/util/TreeSet;I)V

    return-void
.end method

.method static synthetic a(Lmb;Ljava/lang/String;Ljava/lang/String;Lafo;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lmb;->a(Ljava/lang/String;Ljava/lang/String;Lafo;)V

    return-void
.end method

.method static synthetic a(Lmb;Ljava/util/SortedSet;Z)V
    .registers 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lmb;->a(Ljava/util/SortedSet;Z)V

    return-void
.end method

.method static synthetic a(Lmb;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 54
    iput-boolean p1, p0, Lmb;->a:Z

    return p1
.end method

.method static synthetic b(Lmb;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->c:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lmb;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->a:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    .line 337
    const-string v0, "DiscussionSessionApi"

    const-string v1, "invalidateToken"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :try_start_7
    iget-object v0, p0, Lmb;->a:LNe;

    iget-object v1, p0, Lmb;->b:Ljava/lang/String;

    const-string v2, "oauth2:https://www.googleapis.com/auth/discussions"

    invoke-interface {v0, v1, v2}, LNe;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_10
    .catch Landroid/accounts/AuthenticatorException; {:try_start_7 .. :try_end_10} :catch_11

    .line 343
    :goto_10
    return-void

    .line 340
    :catch_11
    move-exception v0

    .line 341
    const-string v1, "DiscussionSessionApi"

    const-string v2, "Can\'t invalidate discussion token"

    invoke-static {v1, v2, v0}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_10
.end method

.method static synthetic b(Lmb;)V
    .registers 1
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Lmb;->c()V

    return-void
.end method

.method static synthetic c(Lmb;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lmb;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->c:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .registers 9

    .prologue
    const/4 v0, 0x1

    .line 363
    iget-object v7, p0, Lmb;->c:Ljava/lang/Object;

    monitor-enter v7

    .line 364
    :try_start_4
    iget-object v1, p0, Lmb;->a:Lmo;

    sget-object v2, Lmo;->c:Lmo;

    if-ne v1, v2, :cond_33

    :goto_a
    const-string v1, "restartRefresh called in wrong state"

    invoke-static {v0, v1}, Lagu;->a(ZLjava/lang/Object;)V

    .line 369
    iget-object v0, p0, Lmb;->a:LeQ;

    iget-object v1, p0, Lmb;->a:LlW;

    invoke-virtual {v0, v1}, LeQ;->b(Ljava/lang/Object;)V

    .line 370
    iget-object v0, p0, Lmb;->a:LeQ;

    iget-object v1, p0, Lmb;->a:LlW;

    invoke-virtual {v0, v1}, LeQ;->a(Ljava/lang/Object;)V

    .line 371
    sget-object v0, Lmo;->b:Lmo;

    iput-object v0, p0, Lmb;->a:Lmo;

    .line 372
    const/4 v1, 0x0

    iget-object v2, p0, Lmb;->a:Lafo;

    const/4 v3, 0x0

    iget-object v4, p0, Lmb;->a:LlW;

    new-instance v5, Ljava/util/TreeSet;

    invoke-direct {v5}, Ljava/util/TreeSet;-><init>()V

    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmb;->a(Ljava/lang/String;Lafo;Lafo;LlW;Ljava/util/TreeSet;I)V

    .line 374
    monitor-exit v7

    .line 375
    return-void

    .line 364
    :cond_33
    const/4 v0, 0x0

    goto :goto_a

    .line 374
    :catchall_35
    move-exception v0

    monitor-exit v7
    :try_end_37
    .catchall {:try_start_4 .. :try_end_37} :catchall_35

    throw v0
.end method

.method static synthetic c(Lmb;)V
    .registers 1
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Lmb;->a()V

    return-void
.end method

.method static synthetic d(Lmb;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lmb;->d:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/util/SortedSet;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561
    iget-object v1, p0, Lmb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 562
    :try_start_3
    iget-object v0, p0, Lmb;->a:Ljava/util/TreeSet;

    if-eqz v0, :cond_14

    .line 563
    new-instance v0, Ljava/util/TreeSet;

    iget-object v2, p0, Lmb;->a:Ljava/util/TreeSet;

    invoke-direct {v0, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v0

    monitor-exit v1

    .line 565
    :goto_13
    return-object v0

    :cond_14
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_13

    .line 567
    :catchall_17
    move-exception v0

    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    throw v0
.end method

.method public a()LlV;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LlV",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 347
    iget-object v7, p0, Lmb;->c:Ljava/lang/Object;

    monitor-enter v7

    .line 348
    :try_start_3
    iget-object v0, p0, Lmb;->a:Lmo;

    sget-object v1, Lmo;->a:Lmo;

    if-ne v0, v1, :cond_2b

    .line 350
    new-instance v0, LlW;

    invoke-direct {v0}, LlW;-><init>()V

    iput-object v0, p0, Lmb;->a:LlW;

    .line 351
    iget-object v0, p0, Lmb;->a:LeQ;

    iget-object v1, p0, Lmb;->a:LlW;

    invoke-virtual {v0, v1}, LeQ;->a(Ljava/lang/Object;)V

    .line 352
    sget-object v0, Lmo;->b:Lmo;

    iput-object v0, p0, Lmb;->a:Lmo;

    .line 353
    const/4 v1, 0x0

    iget-object v2, p0, Lmb;->a:Lafo;

    const/4 v3, 0x0

    iget-object v4, p0, Lmb;->a:LlW;

    new-instance v5, Ljava/util/TreeSet;

    invoke-direct {v5}, Ljava/util/TreeSet;-><init>()V

    const/4 v6, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmb;->a(Ljava/lang/String;Lafo;Lafo;LlW;Ljava/util/TreeSet;I)V

    .line 358
    :cond_2b
    iget-object v0, p0, Lmb;->a:LlW;

    monitor-exit v7

    return-object v0

    .line 359
    :catchall_2f
    move-exception v0

    monitor-exit v7
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    throw v0
.end method

.method public a(Landroid/net/Uri;)LlV;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LlV",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 931
    new-instance v0, LlW;

    invoke-direct {v0}, LlW;-><init>()V

    .line 932
    if-nez p1, :cond_12

    .line 933
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "null Uri"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LlW;->a(Ljava/lang/Throwable;)V

    .line 964
    :goto_11
    return-object v0

    .line 937
    :cond_12
    new-instance v1, Lmd;

    invoke-direct {v1, p0, p1, v0}, Lmd;-><init>(Lmb;Landroid/net/Uri;LlW;)V

    invoke-direct {p0, v0, v1}, Lmb;->a(LlW;Ljava/lang/Runnable;)V

    goto :goto_11
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LlV;
    .registers 10
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 580
    const/16 v0, 0xfa0

    invoke-direct {p0, p3, v0}, Lmb;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 581
    const/16 v0, 0x800

    invoke-direct {p0, p1, v0}, Lmb;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 582
    new-instance v5, LlW;

    invoke-direct {v5}, LlW;-><init>()V

    .line 583
    new-instance v0, Lmh;

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lmh;-><init>(Lmb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LlW;)V

    invoke-direct {p0, v5, v0}, Lmb;->a(LlW;Ljava/lang/Runnable;)V

    .line 632
    return-object v5
.end method

.method public a(Lmz;)LlV;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 821
    new-instance v0, LlW;

    invoke-direct {v0}, LlW;-><init>()V

    .line 822
    check-cast p1, Lmq;

    .line 823
    invoke-virtual {p1}, Lmq;->a()Ljava/lang/String;

    move-result-object v1

    .line 825
    new-instance v2, Lml;

    invoke-direct {v2, p0, v1, v0, p1}, Lml;-><init>(Lmb;Ljava/lang/String;LlW;Lmq;)V

    invoke-direct {p0, v0, v2}, Lmb;->a(LlW;Ljava/lang/Runnable;)V

    .line 871
    return-object v0
.end method

.method public a(Lmz;Ljava/lang/String;)LlV;
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            "Ljava/lang/String;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 637
    const/16 v0, 0x800

    invoke-direct {p0, p2, v0}, Lmb;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 639
    new-instance v5, LlW;

    invoke-direct {v5}, LlW;-><init>()V

    move-object v4, p1

    .line 640
    check-cast v4, Lmq;

    .line 641
    invoke-virtual {v4}, Lmq;->a()Ljava/lang/String;

    move-result-object v3

    .line 643
    new-instance v0, Lmi;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lmi;-><init>(Lmb;Ljava/lang/String;Ljava/lang/String;Lmq;LlW;)V

    invoke-direct {p0, v5, v0}, Lmb;->a(LlW;Ljava/lang/Runnable;)V

    .line 693
    return-object v5
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 572
    iget-object v1, p0, Lmb;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 573
    :try_start_3
    iget-boolean v0, p0, Lmb;->a:Z

    monitor-exit v1

    return v0

    .line 574
    :catchall_7
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    throw v0
.end method

.method public a(Ljava/lang/Runnable;)Z
    .registers 5
    .parameter

    .prologue
    .line 275
    iget-object v1, p0, Lmb;->a:Ljava/util/Set;

    monitor-enter v1

    .line 276
    :try_start_3
    iget-object v0, p0, Lmb;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 277
    monitor-exit v1
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_18

    .line 278
    iget-object v1, p0, Lmb;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 279
    :try_start_d
    iget-object v2, p0, Lmb;->a:Ljava/util/TreeSet;

    if-eqz v2, :cond_1b

    .line 280
    iget-object v2, p0, Lmb;->a:Landroid/os/Handler;

    invoke-virtual {v2, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 284
    :goto_16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_d .. :try_end_17} :catchall_1f

    .line 286
    return v0

    .line 277
    :catchall_18
    move-exception v0

    :try_start_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    throw v0

    .line 282
    :cond_1b
    :try_start_1b
    invoke-virtual {p0}, Lmb;->a()LlV;

    goto :goto_16

    .line 284
    :catchall_1f
    move-exception v0

    monitor-exit v1
    :try_end_21
    .catchall {:try_start_1b .. :try_end_21} :catchall_1f

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 268
    iget-object v0, p0, Lmb;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lmb;->b:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    .line 269
    :goto_11
    return v0

    .line 268
    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public b()LlV;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LlV",
            "<",
            "Lmx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 969
    iget-object v1, p0, Lmb;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 970
    :try_start_3
    iget-object v0, p0, Lmb;->b:LlW;

    if-eqz v0, :cond_b

    .line 971
    iget-object v0, p0, Lmb;->b:LlW;

    monitor-exit v1

    .line 993
    :goto_a
    return-object v0

    .line 973
    :cond_b
    new-instance v0, LlW;

    invoke-direct {v0}, LlW;-><init>()V

    iput-object v0, p0, Lmb;->b:LlW;

    .line 974
    iget-object v0, p0, Lmb;->b:LlW;

    new-instance v2, Lme;

    invoke-direct {v2, p0}, Lme;-><init>(Lmb;)V

    invoke-direct {p0, v0, v2}, Lmb;->a(LlW;Ljava/lang/Runnable;)V

    .line 993
    iget-object v0, p0, Lmb;->b:LlW;

    monitor-exit v1

    goto :goto_a

    .line 994
    :catchall_20
    move-exception v0

    monitor-exit v1
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_20

    throw v0
.end method

.method public b(Lmz;)LlV;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 876
    new-instance v0, LlW;

    invoke-direct {v0}, LlW;-><init>()V

    .line 877
    check-cast p1, Lmq;

    .line 878
    invoke-virtual {p1}, Lmq;->a()Ljava/lang/String;

    move-result-object v1

    .line 880
    new-instance v2, Lmm;

    invoke-direct {v2, p0, v1, v0, p1}, Lmm;-><init>(Lmb;Ljava/lang/String;LlW;Lmq;)V

    invoke-direct {p0, v0, v2}, Lmb;->a(LlW;Ljava/lang/Runnable;)V

    .line 926
    return-object v0
.end method

.method public b(Lmz;Ljava/lang/String;)LlV;
    .registers 9
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            "Ljava/lang/String;",
            ")",
            "LlV",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 698
    const/16 v0, 0x800

    invoke-direct {p0, p2, v0}, Lmb;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 700
    new-instance v5, LlW;

    invoke-direct {v5}, LlW;-><init>()V

    .line 701
    invoke-direct {p0, p1}, Lmb;->a(Lmz;)Lmq;

    move-result-object v4

    .line 703
    new-instance v0, Lmj;

    move-object v1, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lmj;-><init>(Lmb;Ljava/lang/String;Lmz;Lmq;LlW;)V

    invoke-direct {p0, v5, v0}, Lmb;->a(LlW;Ljava/lang/Runnable;)V

    .line 763
    return-object v5
.end method

.method public b(Ljava/lang/Runnable;)Z
    .registers 4
    .parameter

    .prologue
    .line 291
    iget-object v1, p0, Lmb;->a:Ljava/util/Set;

    monitor-enter v1

    .line 292
    :try_start_3
    iget-object v0, p0, Lmb;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 293
    :catchall_b
    move-exception v0

    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_b

    throw v0
.end method

.method public c(Lmz;)LlV;
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lmz;",
            ")",
            "LlV",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 768
    new-instance v0, LlW;

    invoke-direct {v0}, LlW;-><init>()V

    .line 769
    invoke-direct {p0, p1}, Lmb;->a(Lmz;)Lmq;

    move-result-object v1

    .line 771
    new-instance v2, Lmk;

    invoke-direct {v2, p0, v1, p1, v0}, Lmk;-><init>(Lmb;Lmq;Lmz;LlW;)V

    invoke-direct {p0, v0, v2}, Lmb;->a(LlW;Ljava/lang/Runnable;)V

    .line 816
    return-object v0
.end method
