.class Lmd;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/net/Uri;

.field final synthetic a:LlW;

.field final synthetic a:Lmb;


# direct methods
.method constructor <init>(Lmb;Landroid/net/Uri;LlW;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 937
    iput-object p1, p0, Lmd;->a:Lmb;

    iput-object p2, p0, Lmd;->a:Landroid/net/Uri;

    iput-object p3, p0, Lmd;->a:LlW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    .prologue
    .line 944
    :try_start_0
    iget-object v0, p0, Lmd;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 945
    new-instance v1, Ljava/net/URL;

    const-string v2, "https://"

    const-string v3, "http://"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 947
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/URLConnection;->setUseCaches(Z)V

    .line 948
    invoke-virtual {v0}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_1e
    .catch LadQ; {:try_start_0 .. :try_end_1e} :catch_33
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_1e} :catch_3f

    move-result-object v1

    .line 950
    :try_start_1f
    const-string v0, "src"

    invoke-static {v1, v0}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_24
    .catchall {:try_start_1f .. :try_end_24} :catchall_2e

    move-result-object v0

    .line 952
    :try_start_25
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 954
    iget-object v1, p0, Lmd;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Object;)V

    .line 961
    :goto_2d
    return-void

    .line 952
    :catchall_2e
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_33
    .catch LadQ; {:try_start_25 .. :try_end_33} :catch_33
    .catch Ljava/io/IOException; {:try_start_25 .. :try_end_33} :catch_3f

    .line 955
    :catch_33
    move-exception v0

    .line 956
    iget-object v1, p0, Lmd;->a:Lmb;

    invoke-static {v1, v0}, Lmb;->a(Lmb;LadQ;)V

    .line 957
    iget-object v1, p0, Lmd;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_2d

    .line 958
    :catch_3f
    move-exception v0

    .line 959
    iget-object v1, p0, Lmd;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_2d
.end method
