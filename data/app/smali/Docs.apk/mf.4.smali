.class Lmf;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic a:Lafo;

.field final synthetic a:Ljava/util/TreeSet;

.field final synthetic a:LlV;

.field final synthetic a:LlW;

.field final synthetic a:Lmb;

.field final synthetic b:Lafo;


# direct methods
.method constructor <init>(Lmb;Lafo;LlV;Ljava/util/TreeSet;Lafo;LlW;I)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 399
    iput-object p1, p0, Lmf;->a:Lmb;

    iput-object p2, p0, Lmf;->a:Lafo;

    iput-object p3, p0, Lmf;->a:LlV;

    iput-object p4, p0, Lmf;->a:Ljava/util/TreeSet;

    iput-object p5, p0, Lmf;->b:Lafo;

    iput-object p6, p0, Lmf;->a:LlW;

    iput p7, p0, Lmf;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 402
    .line 403
    iget-object v3, p0, Lmf;->a:Lafo;

    .line 405
    :try_start_4
    iget-object v0, p0, Lmf;->a:LlV;

    invoke-interface {v0}, LlV;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/DiscussionFeed;

    .line 406
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/DiscussionFeed;->b()Ljava/lang/String;

    move-result-object v1

    .line 407
    if-nez v3, :cond_16

    .line 410
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/DiscussionFeed;->a()Lafo;

    move-result-object v3

    .line 413
    :cond_16
    if-eqz v0, :cond_56

    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/DiscussionFeed;->a()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_56

    .line 414
    invoke-virtual {v0}, Lcom/google/api/services/discussions/model/DiscussionFeed;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_26
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/discussions/model/Discussion;

    .line 415
    iget-object v4, p0, Lmf;->a:Ljava/util/TreeSet;

    new-instance v5, Lmq;

    invoke-direct {v5, v0}, Lmq;-><init>(Lcom/google/api/services/discussions/model/Discussion;)V

    invoke-virtual {v4, v5}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_3c
    .catch Lmp; {:try_start_4 .. :try_end_3c} :catch_3d

    goto :goto_26

    .line 434
    :catch_3d
    move-exception v0

    .line 435
    iget-object v1, p0, Lmf;->a:Lmb;

    invoke-static {v1}, Lmb;->b(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 436
    :try_start_45
    iget-object v2, p0, Lmf;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Lmo;

    move-result-object v2

    sget-object v3, Lmo;->c:Lmo;

    if-ne v2, v3, :cond_ad

    .line 438
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->b(Lmb;)V

    .line 454
    :goto_54
    monitor-exit v1
    :try_end_55
    .catchall {:try_start_45 .. :try_end_55} :catchall_118

    .line 489
    :goto_55
    return-void

    .line 418
    :cond_56
    :try_start_56
    const-string v0, "DiscussionSessionApi"

    const-string v2, "Discussion stream %s does not exist; treating as empty"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lmf;->a:Lmb;

    invoke-static {v6}, Lmb;->b(Lmb;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    :cond_6d
    if-eqz v1, :cond_1fb

    .line 423
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->b(Lmb;)Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9
    :try_end_76
    .catch Lmp; {:try_start_56 .. :try_end_76} :catch_3d

    .line 424
    :try_start_76
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->b(Lmb;)Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lmo;->c:Lmo;

    if-ne v0, v2, :cond_9a

    .line 426
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->b(Lmb;)V

    .line 431
    :goto_85
    monitor-exit v9
    :try_end_86
    .catchall {:try_start_76 .. :try_end_86} :catchall_aa

    move v0, v7

    .line 457
    :goto_87
    if-nez v0, :cond_13b

    .line 460
    iget-object v0, p0, Lmf;->a:Lmb;

    new-instance v1, Ljava/util/TreeSet;

    iget-object v2, p0, Lmf;->a:Ljava/util/TreeSet;

    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/SortedSet;)V

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lmb;->a(Lmb;Ljava/util/SortedSet;Z)V

    goto :goto_55

    .line 428
    :cond_9a
    :try_start_9a
    iget-object v0, p0, Lmf;->a:Lmb;

    iget-object v2, p0, Lmf;->b:Lafo;

    iget-object v4, p0, Lmf;->a:LlW;

    iget-object v5, p0, Lmf;->a:Ljava/util/TreeSet;

    iget v6, p0, Lmf;->a:I

    add-int/lit8 v6, v6, 0x1

    invoke-static/range {v0 .. v6}, Lmb;->a(Lmb;Ljava/lang/String;Lafo;Lafo;LlW;Ljava/util/TreeSet;I)V

    goto :goto_85

    .line 431
    :catchall_aa
    move-exception v0

    monitor-exit v9
    :try_end_ac
    .catchall {:try_start_9a .. :try_end_ac} :catchall_aa

    :try_start_ac
    throw v0
    :try_end_ad
    .catch Lmp; {:try_start_ac .. :try_end_ad} :catch_3d

    .line 439
    :cond_ad
    :try_start_ad
    iget-object v2, p0, Lmf;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Lmo;

    move-result-object v2

    sget-object v3, Lmo;->b:Lmo;

    if-ne v2, v3, :cond_11b

    .line 441
    iget-object v2, p0, Lmf;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)LeQ;

    move-result-object v2

    iget-object v3, p0, Lmf;->a:LlW;

    const-string v4, "discussionListAllDurationError"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p0, Lmf;->a:I

    add-int/lit8 v6, v6, -0x1

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Pages "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lmf;->a:Lmb;

    invoke-static {v6}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-object v2, p0, Lmf;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)LeQ;

    move-result-object v2

    const-string v3, "discussion"

    const-string v4, "discussionListError"

    iget-object v5, p0, Lmf;->a:Lmb;

    invoke-static {v5}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v5

    iget v6, p0, Lmf;->a:I

    add-int/lit8 v6, v6, -0x1

    int-to-long v6, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 447
    iget-object v2, p0, Lmf;->a:LlW;

    invoke-virtual {v2, v0}, LlW;->a(Ljava/lang/Throwable;)V

    .line 448
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->c(Lmb;)V

    .line 450
    iget-object v0, p0, Lmf;->a:Lmb;

    sget-object v2, Lmo;->a:Lmo;

    invoke-static {v0, v2}, Lmb;->a(Lmb;Lmo;)Lmo;

    goto/16 :goto_54

    .line 454
    :catchall_118
    move-exception v0

    monitor-exit v1
    :try_end_11a
    .catchall {:try_start_ad .. :try_end_11a} :catchall_118

    throw v0

    .line 452
    :cond_11b
    :try_start_11b
    const-string v0, "DiscussionSessionApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected state after finishing refresh (failure): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmf;->a:Lmb;

    invoke-static {v3}, Lmb;->a(Lmb;)Lmo;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_139
    .catchall {:try_start_11b .. :try_end_139} :catchall_118

    goto/16 :goto_54

    .line 463
    :cond_13b
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->b(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 464
    :try_start_142
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Lmo;

    move-result-object v0

    sget-object v2, Lmo;->c:Lmo;

    if-ne v0, v2, :cond_157

    .line 466
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->b(Lmb;)V

    .line 487
    :goto_151
    monitor-exit v1

    goto/16 :goto_55

    :catchall_154
    move-exception v0

    monitor-exit v1
    :try_end_156
    .catchall {:try_start_142 .. :try_end_156} :catchall_154

    throw v0

    .line 467
    :cond_157
    :try_start_157
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Lmo;

    move-result-object v0

    sget-object v2, Lmo;->b:Lmo;

    if-ne v0, v2, :cond_1db

    .line 468
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0, v3}, Lmb;->a(Lmb;Lafo;)Lafo;

    .line 471
    iget-object v0, p0, Lmf;->a:Lmb;

    new-instance v2, Ljava/util/TreeSet;

    iget-object v3, p0, Lmf;->a:Ljava/util/TreeSet;

    invoke-direct {v2, v3}, Ljava/util/TreeSet;-><init>(Ljava/util/SortedSet;)V

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableSortedSet(Ljava/util/SortedSet;)Ljava/util/SortedSet;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lmb;->a(Lmb;Ljava/util/SortedSet;Z)V

    .line 473
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)LeQ;

    move-result-object v0

    iget-object v2, p0, Lmf;->a:LlW;

    const-string v3, "discussionListAllDurationOk"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lmf;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Pages "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lmf;->a:Lmb;

    invoke-static {v5}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 476
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)LeQ;

    move-result-object v0

    const-string v2, "discussion"

    const-string v3, "discussionListOk"

    iget-object v4, p0, Lmf;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lmf;->a:I

    int-to-long v5, v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 479
    iget-object v0, p0, Lmf;->a:LlW;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LlW;->a(Ljava/lang/Object;)V

    .line 480
    iget-object v0, p0, Lmf;->a:Lmb;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lmb;->a(Lmb;Z)Z

    .line 481
    iget-object v0, p0, Lmf;->a:Lmb;

    invoke-static {v0}, Lmb;->c(Lmb;)V

    .line 483
    iget-object v0, p0, Lmf;->a:Lmb;

    sget-object v2, Lmo;->a:Lmo;

    invoke-static {v0, v2}, Lmb;->a(Lmb;Lmo;)Lmo;

    goto/16 :goto_151

    .line 485
    :cond_1db
    const-string v0, "DiscussionSessionApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected state after finishing refresh (success): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lmf;->a:Lmb;

    invoke-static {v3}, Lmb;->a(Lmb;)Lmo;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1f9
    .catchall {:try_start_157 .. :try_end_1f9} :catchall_154

    goto/16 :goto_151

    :cond_1fb
    move v0, v8

    goto/16 :goto_87
.end method
