.class Lmh;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic a:LlW;

.field final synthetic a:Lmb;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;


# direct methods
.method constructor <init>(Lmb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LlW;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 583
    iput-object p1, p0, Lmh;->a:Lmb;

    iput-object p2, p0, Lmh;->a:Ljava/lang/String;

    iput-object p3, p0, Lmh;->b:Ljava/lang/String;

    iput-object p4, p0, Lmh;->c:Ljava/lang/String;

    iput-object p5, p0, Lmh;->a:LlW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 586
    new-instance v0, Lcom/google/api/services/discussions/model/Discussion;

    invoke-direct {v0}, Lcom/google/api/services/discussions/model/Discussion;-><init>()V

    .line 587
    new-instance v1, Lcom/google/api/services/discussions/model/MimedcontentJson;

    invoke-direct {v1}, Lcom/google/api/services/discussions/model/MimedcontentJson;-><init>()V

    iget-object v2, p0, Lmh;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v1

    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/MimedcontentJson;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v1

    .line 589
    new-instance v2, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    invoke-direct {v2}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v1

    const-string v2, "discussion"

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    move-result-object v1

    .line 592
    iget-object v2, p0, Lmh;->b:Ljava/lang/String;

    if-eqz v2, :cond_42

    .line 593
    iget-object v2, p0, Lmh;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    .line 594
    new-instance v2, Lcom/google/api/services/discussions/model/MimedcontentJson;

    invoke-direct {v2}, Lcom/google/api/services/discussions/model/MimedcontentJson;-><init>()V

    iget-object v3, p0, Lmh;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v2

    const-string v3, "text/plain"

    invoke-virtual {v2, v3}, Lcom/google/api/services/discussions/model/MimedcontentJson;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v2

    .line 596
    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;->c(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;

    .line 598
    :cond_42
    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Discussion;->a(Lcom/google/api/services/discussions/model/Discussion$DiscussionsObject;)Lcom/google/api/services/discussions/model/Discussion;

    .line 599
    const-string v1, "discussions#discussion"

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Discussion;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Discussion;

    .line 600
    const-string v1, "discuss"

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Discussion;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Discussion;

    .line 601
    new-instance v1, Lcom/google/api/services/discussions/model/Discussion$Target;

    invoke-direct {v1}, Lcom/google/api/services/discussions/model/Discussion$Target;-><init>()V

    iget-object v2, p0, Lmh;->a:Lmb;

    invoke-static {v2}, Lmb;->b(Lmb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/Discussion$Target;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Discussion$Target;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Discussion;->a(Lcom/google/api/services/discussions/model/Discussion$Target;)Lcom/google/api/services/discussions/model/Discussion;

    .line 603
    :try_start_61
    iget-object v1, p0, Lmh;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)Lcom/google/api/services/discussions/Discussions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/services/discussions/Discussions;->a()Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;

    move-result-object v1

    iget-object v2, p0, Lmh;->a:Lmb;

    invoke-static {v2}, Lmb;->b(Lmb;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;->a(Ljava/lang/String;Lcom/google/api/services/discussions/model/Discussion;)Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Insert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Insert;->a()Lcom/google/api/services/discussions/model/Discussion;

    move-result-object v0

    .line 604
    new-instance v1, Lmq;

    invoke-direct {v1, v0}, Lmq;-><init>(Lcom/google/api/services/discussions/model/Discussion;)V

    .line 607
    iget-object v0, p0, Lmh;->a:Lmb;

    invoke-static {v0}, Lmb;->c(Lmb;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_85
    .catch LadQ; {:try_start_61 .. :try_end_85} :catch_c9
    .catch Ljava/io/IOException; {:try_start_61 .. :try_end_85} :catch_eb

    .line 608
    :try_start_85
    iget-object v0, p0, Lmh;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Ljava/util/TreeSet;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 609
    monitor-exit v2
    :try_end_8f
    .catchall {:try_start_85 .. :try_end_8f} :catchall_c6

    .line 610
    :try_start_8f
    iget-object v0, p0, Lmh;->a:Lmb;

    invoke-static {v0}, Lmb;->b(Lmb;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_96
    .catch LadQ; {:try_start_8f .. :try_end_96} :catch_c9
    .catch Ljava/io/IOException; {:try_start_8f .. :try_end_96} :catch_eb

    .line 611
    :try_start_96
    iget-object v0, p0, Lmh;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Lmo;

    move-result-object v0

    sget-object v3, Lmo;->b:Lmo;

    if-ne v0, v3, :cond_a7

    .line 613
    iget-object v0, p0, Lmh;->a:Lmb;

    sget-object v3, Lmo;->c:Lmo;

    invoke-static {v0, v3}, Lmb;->a(Lmb;Lmo;)Lmo;

    .line 615
    :cond_a7
    monitor-exit v2
    :try_end_a8
    .catchall {:try_start_96 .. :try_end_a8} :catchall_e8

    .line 616
    :try_start_a8
    iget-object v0, p0, Lmh;->a:Lmb;

    invoke-static {v0}, Lmb;->c(Lmb;)V

    .line 617
    iget-object v0, p0, Lmh;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)LeQ;

    move-result-object v0

    const-string v2, "discussion"

    const-string v3, "discussionCreationOk"

    iget-object v4, p0, Lmh;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    iget-object v0, p0, Lmh;->a:LlW;

    invoke-virtual {v0, v1}, LlW;->a(Ljava/lang/Object;)V
    :try_end_c5
    .catch LadQ; {:try_start_a8 .. :try_end_c5} :catch_c9
    .catch Ljava/io/IOException; {:try_start_a8 .. :try_end_c5} :catch_eb

    .line 630
    :goto_c5
    return-void

    .line 609
    :catchall_c6
    move-exception v0

    :try_start_c7
    monitor-exit v2
    :try_end_c8
    .catchall {:try_start_c7 .. :try_end_c8} :catchall_c6

    :try_start_c8
    throw v0
    :try_end_c9
    .catch LadQ; {:try_start_c8 .. :try_end_c9} :catch_c9
    .catch Ljava/io/IOException; {:try_start_c8 .. :try_end_c9} :catch_eb

    .line 620
    :catch_c9
    move-exception v0

    .line 621
    iget-object v1, p0, Lmh;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionCreationError"

    iget-object v4, p0, Lmh;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    iget-object v1, p0, Lmh;->a:Lmb;

    invoke-static {v1, v0}, Lmb;->a(Lmb;LadQ;)V

    .line 624
    iget-object v1, p0, Lmh;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_c5

    .line 615
    :catchall_e8
    move-exception v0

    :try_start_e9
    monitor-exit v2
    :try_end_ea
    .catchall {:try_start_e9 .. :try_end_ea} :catchall_e8

    :try_start_ea
    throw v0
    :try_end_eb
    .catch LadQ; {:try_start_ea .. :try_end_eb} :catch_c9
    .catch Ljava/io/IOException; {:try_start_ea .. :try_end_eb} :catch_eb

    .line 625
    :catch_eb
    move-exception v0

    .line 626
    iget-object v1, p0, Lmh;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionCreationError"

    iget-object v4, p0, Lmh;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    iget-object v1, p0, Lmh;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_c5
.end method
