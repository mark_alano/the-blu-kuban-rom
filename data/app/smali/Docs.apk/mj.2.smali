.class Lmj;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic a:LlW;

.field final synthetic a:Lmb;

.field final synthetic a:Lmq;

.field final synthetic a:Lmz;


# direct methods
.method constructor <init>(Lmb;Ljava/lang/String;Lmz;Lmq;LlW;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 703
    iput-object p1, p0, Lmj;->a:Lmb;

    iput-object p2, p0, Lmj;->a:Ljava/lang/String;

    iput-object p3, p0, Lmj;->a:Lmz;

    iput-object p4, p0, Lmj;->a:Lmq;

    iput-object p5, p0, Lmj;->a:LlW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 708
    new-instance v0, Lcom/google/api/services/discussions/model/MimedcontentJson;

    invoke-direct {v0}, Lcom/google/api/services/discussions/model/MimedcontentJson;-><init>()V

    iget-object v1, p0, Lmj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/MimedcontentJson;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/MimedcontentJson;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/MimedcontentJson;

    move-result-object v0

    .line 712
    :try_start_11
    new-instance v1, Lcom/google/api/services/discussions/model/Post;

    invoke-direct {v1}, Lcom/google/api/services/discussions/model/Post;-><init>()V

    .line 713
    new-instance v2, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    invoke-direct {v2}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->a(Lcom/google/api/services/discussions/model/MimedcontentJson;)Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    move-result-object v0

    const-string v2, "post"

    invoke-virtual {v0, v2}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    move-result-object v0

    .line 715
    invoke-virtual {v1, v0}, Lcom/google/api/services/discussions/model/Post;->a(Lcom/google/api/services/discussions/model/Post$DiscussionsObject;)Lcom/google/api/services/discussions/model/Post;

    .line 716
    iget-object v0, p0, Lmj;->a:Lmz;

    invoke-interface {v0}, Lmz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/api/services/discussions/model/Post;->d(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 717
    const-string v0, "discussions#post"

    invoke-virtual {v1, v0}, Lcom/google/api/services/discussions/model/Post;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 718
    const-string v0, "post"

    invoke-virtual {v1, v0}, Lcom/google/api/services/discussions/model/Post;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 719
    iget-object v0, p0, Lmj;->a:Lmz;

    invoke-interface {v0}, Lmz;->b()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 722
    const-string v0, "reopen"

    invoke-virtual {v1, v0}, Lcom/google/api/services/discussions/model/Post;->c(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 724
    :cond_48
    new-instance v0, Lcom/google/api/services/discussions/model/Post$Target;

    invoke-direct {v0}, Lcom/google/api/services/discussions/model/Post$Target;-><init>()V

    iget-object v2, p0, Lmj;->a:Lmq;

    invoke-virtual {v2}, Lmq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/api/services/discussions/model/Post$Target;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post$Target;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/api/services/discussions/model/Post;->a(Lcom/google/api/services/discussions/model/Post$Target;)Lcom/google/api/services/discussions/model/Post;

    .line 725
    iget-object v0, p0, Lmj;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Lcom/google/api/services/discussions/Discussions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions;->a()Lcom/google/api/services/discussions/Discussions$Posts;

    move-result-object v0

    iget-object v2, p0, Lmj;->a:Lmb;

    invoke-static {v2}, Lmb;->b(Lmb;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lmj;->a:Lmq;

    invoke-virtual {v3}, Lmq;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lmj;->a:Lmz;

    invoke-interface {v4}, Lmz;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/google/api/services/discussions/Discussions$Posts;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/discussions/model/Post;)Lcom/google/api/services/discussions/Discussions$Posts$Update;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions$Posts$Update;->a()Lcom/google/api/services/discussions/model/Post;

    move-result-object v0

    .line 728
    iget-object v1, p0, Lmj;->a:Lmq;

    invoke-static {v1, v0}, Lmq;->a(Lmq;Lcom/google/api/services/discussions/model/Post;)Landroid/util/Pair;

    move-result-object v1

    .line 730
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lmz;

    .line 731
    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lmq;
    :try_end_8c
    .catch LadQ; {:try_start_11 .. :try_end_8c} :catch_e2
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_8c} :catch_101

    .line 745
    iget-object v2, p0, Lmj;->a:Lmb;

    invoke-static {v2}, Lmb;->c(Lmb;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 746
    :try_start_93
    iget-object v3, p0, Lmj;->a:Lmb;

    invoke-static {v3}, Lmb;->a(Lmb;)Ljava/util/TreeSet;

    move-result-object v3

    iget-object v4, p0, Lmj;->a:Lmq;

    invoke-virtual {v3, v4}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_aa

    .line 747
    iget-object v3, p0, Lmj;->a:Lmb;

    invoke-static {v3}, Lmb;->a(Lmb;)Ljava/util/TreeSet;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 749
    :cond_aa
    iget-object v1, p0, Lmj;->a:Lmb;

    invoke-static {v1}, Lmb;->c(Lmb;)V

    .line 750
    monitor-exit v2
    :try_end_b0
    .catchall {:try_start_93 .. :try_end_b0} :catchall_11b

    .line 751
    iget-object v1, p0, Lmj;->a:Lmb;

    invoke-static {v1}, Lmb;->b(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 752
    :try_start_b7
    iget-object v2, p0, Lmj;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Lmo;

    move-result-object v2

    sget-object v3, Lmo;->b:Lmo;

    if-ne v2, v3, :cond_c8

    .line 754
    iget-object v2, p0, Lmj;->a:Lmb;

    sget-object v3, Lmo;->c:Lmo;

    invoke-static {v2, v3}, Lmb;->a(Lmb;Lmo;)Lmo;

    .line 756
    :cond_c8
    monitor-exit v1
    :try_end_c9
    .catchall {:try_start_b7 .. :try_end_c9} :catchall_11e

    .line 757
    iget-object v1, p0, Lmj;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionEditOk"

    iget-object v4, p0, Lmj;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    iget-object v1, p0, Lmj;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Object;)V

    .line 760
    :goto_e1
    return-void

    .line 732
    :catch_e2
    move-exception v0

    .line 733
    iget-object v1, p0, Lmj;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionEditError"

    iget-object v4, p0, Lmj;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    iget-object v1, p0, Lmj;->a:Lmb;

    invoke-static {v1, v0}, Lmb;->a(Lmb;LadQ;)V

    .line 736
    iget-object v1, p0, Lmj;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_e1

    .line 738
    :catch_101
    move-exception v0

    .line 739
    iget-object v1, p0, Lmj;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionEditError"

    iget-object v4, p0, Lmj;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    iget-object v1, p0, Lmj;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_e1

    .line 750
    :catchall_11b
    move-exception v0

    :try_start_11c
    monitor-exit v2
    :try_end_11d
    .catchall {:try_start_11c .. :try_end_11d} :catchall_11b

    throw v0

    .line 756
    :catchall_11e
    move-exception v0

    :try_start_11f
    monitor-exit v1
    :try_end_120
    .catchall {:try_start_11f .. :try_end_120} :catchall_11e

    throw v0
.end method
