.class Lmk;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LlW;

.field final synthetic a:Lmb;

.field final synthetic a:Lmq;

.field final synthetic a:Lmz;


# direct methods
.method constructor <init>(Lmb;Lmq;Lmz;LlW;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 771
    iput-object p1, p0, Lmk;->a:Lmb;

    iput-object p2, p0, Lmk;->a:Lmq;

    iput-object p3, p0, Lmk;->a:Lmz;

    iput-object p4, p0, Lmk;->a:LlW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 775
    :try_start_0
    iget-object v0, p0, Lmk;->a:Lmq;

    iget-object v1, p0, Lmk;->a:Lmz;

    if-eq v0, v1, :cond_8c

    .line 777
    iget-object v0, p0, Lmk;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Lcom/google/api/services/discussions/Discussions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions;->a()Lcom/google/api/services/discussions/Discussions$Posts;

    move-result-object v0

    iget-object v1, p0, Lmk;->a:Lmb;

    invoke-static {v1}, Lmb;->b(Lmb;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmk;->a:Lmq;

    invoke-virtual {v2}, Lmq;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lmk;->a:Lmz;

    invoke-interface {v3}, Lmz;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/api/services/discussions/Discussions$Posts;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$Posts$Remove;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions$Posts$Remove;->a()V
    :try_end_29
    .catch LadQ; {:try_start_0 .. :try_end_29} :catch_aa
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_29} :catch_c9

    .line 795
    :goto_29
    iget-object v0, p0, Lmk;->a:Lmb;

    invoke-static {v0}, Lmb;->c(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 796
    :try_start_30
    iget-object v0, p0, Lmk;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Ljava/util/TreeSet;

    move-result-object v0

    iget-object v2, p0, Lmk;->a:Lmq;

    invoke-virtual {v0, v2}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 798
    iget-object v0, p0, Lmk;->a:Lmq;

    iget-object v2, p0, Lmk;->a:Lmz;

    invoke-interface {v2}, Lmz;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lmq;->a(Lmq;Ljava/lang/String;)Lmq;

    move-result-object v0

    .line 800
    iget-object v2, p0, Lmk;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Ljava/util/TreeSet;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 802
    :cond_53
    iget-object v0, p0, Lmk;->a:Lmb;

    invoke-static {v0}, Lmb;->c(Lmb;)V

    .line 803
    monitor-exit v1
    :try_end_59
    .catchall {:try_start_30 .. :try_end_59} :catchall_e3

    .line 804
    iget-object v0, p0, Lmk;->a:Lmb;

    invoke-static {v0}, Lmb;->b(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 805
    :try_start_60
    iget-object v0, p0, Lmk;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Lmo;

    move-result-object v0

    sget-object v2, Lmo;->b:Lmo;

    if-ne v0, v2, :cond_71

    .line 807
    iget-object v0, p0, Lmk;->a:Lmb;

    sget-object v2, Lmo;->c:Lmo;

    invoke-static {v0, v2}, Lmb;->a(Lmb;Lmo;)Lmo;

    .line 809
    :cond_71
    monitor-exit v1
    :try_end_72
    .catchall {:try_start_60 .. :try_end_72} :catchall_e6

    .line 810
    iget-object v0, p0, Lmk;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)LeQ;

    move-result-object v0

    const-string v1, "discussion"

    const-string v2, "discussionDeleteOk"

    iget-object v3, p0, Lmk;->a:Lmb;

    invoke-static {v3}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    iget-object v0, p0, Lmk;->a:LlW;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LlW;->a(Ljava/lang/Object;)V

    .line 813
    :goto_8b
    return-void

    .line 780
    :cond_8c
    :try_start_8c
    iget-object v0, p0, Lmk;->a:Lmb;

    invoke-static {v0}, Lmb;->a(Lmb;)Lcom/google/api/services/discussions/Discussions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions;->a()Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;

    move-result-object v0

    iget-object v1, p0, Lmk;->a:Lmb;

    invoke-static {v1}, Lmb;->b(Lmb;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lmk;->a:Lmq;

    invoke-virtual {v2}, Lmq;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions$DiscussionsOperations$Remove;->a()V
    :try_end_a9
    .catch LadQ; {:try_start_8c .. :try_end_a9} :catch_aa
    .catch Ljava/io/IOException; {:try_start_8c .. :try_end_a9} :catch_c9

    goto :goto_29

    .line 782
    :catch_aa
    move-exception v0

    .line 783
    iget-object v1, p0, Lmk;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionDeleteError"

    iget-object v4, p0, Lmk;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    iget-object v1, p0, Lmk;->a:Lmb;

    invoke-static {v1, v0}, Lmb;->a(Lmb;LadQ;)V

    .line 786
    iget-object v1, p0, Lmk;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_8b

    .line 788
    :catch_c9
    move-exception v0

    .line 789
    iget-object v1, p0, Lmk;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionDeleteError"

    iget-object v4, p0, Lmk;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    iget-object v1, p0, Lmk;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_8b

    .line 803
    :catchall_e3
    move-exception v0

    :try_start_e4
    monitor-exit v1
    :try_end_e5
    .catchall {:try_start_e4 .. :try_end_e5} :catchall_e3

    throw v0

    .line 809
    :catchall_e6
    move-exception v0

    :try_start_e7
    monitor-exit v1
    :try_end_e8
    .catchall {:try_start_e7 .. :try_end_e8} :catchall_e6

    throw v0
.end method
