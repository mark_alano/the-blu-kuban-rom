.class Lmm;
.super Ljava/lang/Object;
.source "DiscussionSessionApi.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic a:LlW;

.field final synthetic a:Lmb;

.field final synthetic a:Lmq;


# direct methods
.method constructor <init>(Lmb;Ljava/lang/String;LlW;Lmq;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 880
    iput-object p1, p0, Lmm;->a:Lmb;

    iput-object p2, p0, Lmm;->a:Ljava/lang/String;

    iput-object p3, p0, Lmm;->a:LlW;

    iput-object p4, p0, Lmm;->a:Lmq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    .prologue
    .line 883
    new-instance v0, Lcom/google/api/services/discussions/model/Post;

    invoke-direct {v0}, Lcom/google/api/services/discussions/model/Post;-><init>()V

    .line 884
    new-instance v1, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    invoke-direct {v1}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;-><init>()V

    const-string v2, "post"

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/Post$DiscussionsObject;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post$DiscussionsObject;

    move-result-object v1

    .line 886
    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->a(Lcom/google/api/services/discussions/model/Post$DiscussionsObject;)Lcom/google/api/services/discussions/model/Post;

    .line 887
    const-string v1, "reopen"

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->c(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 888
    const-string v1, "discussions#post"

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 889
    const-string v1, "post"

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->b(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post;

    .line 890
    new-instance v1, Lcom/google/api/services/discussions/model/Post$Target;

    invoke-direct {v1}, Lcom/google/api/services/discussions/model/Post$Target;-><init>()V

    iget-object v2, p0, Lmm;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/api/services/discussions/model/Post$Target;->a(Ljava/lang/String;)Lcom/google/api/services/discussions/model/Post$Target;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/discussions/model/Post;->a(Lcom/google/api/services/discussions/model/Post$Target;)Lcom/google/api/services/discussions/model/Post;

    .line 894
    :try_start_30
    iget-object v1, p0, Lmm;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)Lcom/google/api/services/discussions/Discussions;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/services/discussions/Discussions;->a()Lcom/google/api/services/discussions/Discussions$Posts;

    move-result-object v1

    iget-object v2, p0, Lmm;->a:Lmb;

    invoke-static {v2}, Lmb;->b(Lmb;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lmm;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/api/services/discussions/Discussions$Posts;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/api/services/discussions/model/Post;)Lcom/google/api/services/discussions/Discussions$Posts$Insert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/services/discussions/Discussions$Posts$Insert;->a()Lcom/google/api/services/discussions/model/Post;
    :try_end_49
    .catch LadQ; {:try_start_30 .. :try_end_49} :catch_aa
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_49} :catch_c9

    move-result-object v0

    .line 908
    iget-object v1, p0, Lmm;->a:Lmq;

    invoke-static {v1, v0}, Lmq;->a(Lmq;Lcom/google/api/services/discussions/model/Post;)Landroid/util/Pair;

    move-result-object v0

    .line 909
    iget-object v1, p0, Lmm;->a:Lmb;

    invoke-static {v1}, Lmb;->c(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 910
    :try_start_57
    iget-object v2, p0, Lmm;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Ljava/util/TreeSet;

    move-result-object v2

    iget-object v3, p0, Lmm;->a:Lmq;

    invoke-virtual {v2, v3}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_70

    .line 911
    iget-object v2, p0, Lmm;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Ljava/util/TreeSet;

    move-result-object v2

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 913
    :cond_70
    iget-object v2, p0, Lmm;->a:Lmb;

    invoke-static {v2}, Lmb;->c(Lmb;)V

    .line 914
    monitor-exit v1
    :try_end_76
    .catchall {:try_start_57 .. :try_end_76} :catchall_e3

    .line 915
    iget-object v1, p0, Lmm;->a:Lmb;

    invoke-static {v1}, Lmb;->b(Lmb;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 916
    :try_start_7d
    iget-object v2, p0, Lmm;->a:Lmb;

    invoke-static {v2}, Lmb;->a(Lmb;)Lmo;

    move-result-object v2

    sget-object v3, Lmo;->b:Lmo;

    if-ne v2, v3, :cond_8e

    .line 918
    iget-object v2, p0, Lmm;->a:Lmb;

    sget-object v3, Lmo;->c:Lmo;

    invoke-static {v2, v3}, Lmb;->a(Lmb;Lmo;)Lmo;

    .line 920
    :cond_8e
    monitor-exit v1
    :try_end_8f
    .catchall {:try_start_7d .. :try_end_8f} :catchall_e6

    .line 921
    iget-object v1, p0, Lmm;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionReopenOk"

    iget-object v4, p0, Lmm;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 923
    iget-object v1, p0, Lmm;->a:LlW;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Object;)V

    .line 924
    :goto_a9
    return-void

    .line 895
    :catch_aa
    move-exception v0

    .line 896
    iget-object v1, p0, Lmm;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionReopenError"

    iget-object v4, p0, Lmm;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    iget-object v1, p0, Lmm;->a:Lmb;

    invoke-static {v1, v0}, Lmb;->a(Lmb;LadQ;)V

    .line 899
    iget-object v1, p0, Lmm;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_a9

    .line 901
    :catch_c9
    move-exception v0

    .line 902
    iget-object v1, p0, Lmm;->a:Lmb;

    invoke-static {v1}, Lmb;->a(Lmb;)LeQ;

    move-result-object v1

    const-string v2, "discussion"

    const-string v3, "discussionReopenError"

    iget-object v4, p0, Lmm;->a:Lmb;

    invoke-static {v4}, Lmb;->c(Lmb;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    iget-object v1, p0, Lmm;->a:LlW;

    invoke-virtual {v1, v0}, LlW;->a(Ljava/lang/Throwable;)V

    goto :goto_a9

    .line 914
    :catchall_e3
    move-exception v0

    :try_start_e4
    monitor-exit v1
    :try_end_e5
    .catchall {:try_start_e4 .. :try_end_e5} :catchall_e3

    throw v0

    .line 920
    :catchall_e6
    move-exception v0

    :try_start_e7
    monitor-exit v1
    :try_end_e8
    .catchall {:try_start_e7 .. :try_end_e8} :catchall_e6

    throw v0
.end method
