.class final enum Lmo;
.super Ljava/lang/Enum;
.source "DiscussionSessionApi.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lmo;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lmo;

.field private static final synthetic a:[Lmo;

.field public static final enum b:Lmo;

.field public static final enum c:Lmo;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 131
    new-instance v0, Lmo;

    const-string v1, "READY"

    invoke-direct {v0, v1, v2}, Lmo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmo;->a:Lmo;

    .line 132
    new-instance v0, Lmo;

    const-string v1, "REFRESHING"

    invoke-direct {v0, v1, v3}, Lmo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmo;->b:Lmo;

    .line 133
    new-instance v0, Lmo;

    const-string v1, "INVALIDATE_REFRESH"

    invoke-direct {v0, v1, v4}, Lmo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lmo;->c:Lmo;

    .line 130
    const/4 v0, 0x3

    new-array v0, v0, [Lmo;

    sget-object v1, Lmo;->a:Lmo;

    aput-object v1, v0, v2

    sget-object v1, Lmo;->b:Lmo;

    aput-object v1, v0, v3

    sget-object v1, Lmo;->c:Lmo;

    aput-object v1, v0, v4

    sput-object v0, Lmo;->a:[Lmo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lmo;
    .registers 2
    .parameter

    .prologue
    .line 130
    const-class v0, Lmo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lmo;

    return-object v0
.end method

.method public static values()[Lmo;
    .registers 1

    .prologue
    .line 130
    sget-object v0, Lmo;->a:[Lmo;

    invoke-virtual {v0}, [Lmo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lmo;

    return-object v0
.end method
