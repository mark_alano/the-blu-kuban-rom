.class public final Lms;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lky;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LlX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 32
    iput-object p1, p0, Lms;->a:LYD;

    .line 33
    const-class v0, LlZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lms;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lms;->a:LZb;

    .line 36
    const-class v0, LlX;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    const-class v1, LaoJ;

    invoke-static {v0, v1}, Lms;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lms;->b:LZb;

    .line 39
    return-void
.end method

.method static synthetic a(Lms;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lms;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lms;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lms;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 62
    const-class v0, LlX;

    new-instance v1, Lmt;

    invoke-direct {v1, p0}, Lmt;-><init>(Lms;)V

    invoke-virtual {p0, v0, v1}, Lms;->a(Ljava/lang/Class;Laou;)V

    .line 70
    const-class v0, LlZ;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lms;->a:LZb;

    invoke-virtual {p0, v0, v1}, Lms;->a(Laop;LZb;)V

    .line 71
    const-class v0, LlX;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lms;->b:LZb;

    invoke-virtual {p0, v0, v1}, Lms;->a(Laop;LZb;)V

    .line 72
    iget-object v0, p0, Lms;->a:LZb;

    new-instance v1, Lmu;

    invoke-direct {v1, p0}, Lmu;-><init>(Lms;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 86
    iget-object v0, p0, Lms;->b:LZb;

    new-instance v1, Lmv;

    invoke-direct {v1, p0}, Lmv;-><init>(Lms;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 102
    return-void
.end method

.method public a(LlX;)V
    .registers 3
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lms;->a:LYD;

    iget-object v0, v0, LYD;->a:LYE;

    iget-object v0, v0, LYE;->b:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lms;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p1, LlX;->a:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lms;->a:LYD;

    iget-object v0, v0, LYD;->a:LeO;

    iget-object v0, v0, LeO;->a:LZb;

    invoke-virtual {v0}, LZb;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lms;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LeQ;

    iput-object v0, p1, LlX;->a:LeQ;

    .line 57
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 106
    return-void
.end method
