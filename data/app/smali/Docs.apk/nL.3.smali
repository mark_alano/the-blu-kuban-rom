.class public LnL;
.super Ljava/lang/Object;
.source "DialogUtility.java"


# direct methods
.method public static a(Landroid/content/Context;)Landroid/app/AlertDialog$Builder;
    .registers 3
    .parameter

    .prologue
    .line 106
    invoke-static {p0}, LnL;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 107
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Context;
    .registers 3
    .parameter

    .prologue
    .line 98
    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Leo;->CakemixTheme_Dialog:I

    invoke-direct {v0, p0, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 65
    const-string v0, "accountName"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lagu;->a(Z)V

    .line 66
    const-string v0, "accountName"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 70
    const-string v0, "accountName"

    invoke-virtual {p0, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void
.end method
