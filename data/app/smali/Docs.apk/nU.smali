.class public LnU;
.super Ljava/lang/Object;
.source "EditTitleDialogFragment.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, LnU;->a:Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 93
    const-string v1, "EditTitleDialogFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "actionId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    if-nez p2, :cond_21

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-eqz v1, :cond_27

    :cond_21
    const/4 v1, 0x6

    if-eq p2, v1, :cond_27

    const/4 v1, 0x5

    if-ne p2, v1, :cond_3f

    .line 99
    :cond_27
    iget-object v1, p0, LnU;->a:Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 100
    iget-object v2, p0, LnU;->a:Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;

    invoke-static {v2}, Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;->a(Lcom/google/android/apps/docs/doclist/dialogs/EditTitleDialogFragment;)LnN;

    move-result-object v2

    invoke-interface {v2, v1}, LnN;->a(Ljava/lang/String;)V

    .line 109
    :cond_3e
    :goto_3e
    return v0

    .line 105
    :cond_3f
    if-nez p2, :cond_47

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-eq v1, v0, :cond_3e

    .line 109
    :cond_47
    const/4 v0, 0x0

    goto :goto_3e
.end method
