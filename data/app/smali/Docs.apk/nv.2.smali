.class public Lnv;
.super Ljava/lang/Object;
.source "CreateEntryDialogFragment.java"

# interfaces
.implements Lnw;


# instance fields
.field final synthetic a:Landroid/support/v4/app/FragmentActivity;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;Landroid/support/v4/app/FragmentActivity;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 308
    iput-object p1, p0, Lnv;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    iput-object p2, p0, Lnv;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 322
    sget v0, Len;->create_new_from_camera:I

    return v0
.end method

.method public a(Lcom/google/android/apps/docs/app/CreateNewDocActivity;)V
    .registers 4
    .parameter

    .prologue
    .line 311
    iget-object v0, p0, Lnv;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lnv;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    invoke-static {v1}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->b(Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 313
    iget-object v1, p0, Lnv;->a:Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/doclist/dialogs/CreateEntryDialogFragment;->a()V

    .line 317
    iget-object v1, p0, Lnv;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 318
    return-void
.end method

.method public a(Landroid/content/Context;)Z
    .registers 3
    .parameter

    .prologue
    .line 332
    invoke-static {p1}, Lcom/google/android/apps/docs/app/OcrCameraActivity;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 327
    sget v0, Leg;->ic_create_new_from_camera:I

    return v0
.end method
