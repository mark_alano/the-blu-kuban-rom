.class public final LoB;
.super Lqf;
.source "PrintDialogActivity.java"


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;Landroid/content/Context;Lqd;Ljava/lang/String;LKS;Ljava/lang/Class;LPm;Landroid/content/SharedPreferences;LNe;Landroid/os/Handler;)V
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lqd;",
            "Ljava/lang/String;",
            "LKS;",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;",
            "LPm;",
            "Landroid/content/SharedPreferences;",
            "LNe;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 321
    iput-object p1, p0, LoB;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    .line 322
    invoke-direct/range {v0 .. v9}, Lqf;-><init>(Landroid/content/Context;Lqd;Ljava/lang/String;LKS;Ljava/lang/Class;LPm;Landroid/content/SharedPreferences;LNe;Landroid/os/Handler;)V

    .line 331
    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 335
    invoke-super {p0, p1, p2}, Lqf;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 336
    const-string v0, "PrintDialogActivity"

    const-string v1, "Finished page loading of %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    iget-object v0, p0, LoB;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 340
    const-string v0, "javascript:window.addEventListener(\'message\',function(evt){window.AndroidPrintDialog.onPostMessage(evt.data)}, false)"

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 343
    iget-object v0, p0, LoB;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v0, v3}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;Z)V

    .line 345
    :cond_2a
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 350
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LoY;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 351
    iget-object v2, p0, LoB;->a:Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;

    invoke-static {v2}, Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;->a(Lcom/google/android/apps/docs/doclist/dialogs/PrintDialogActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LoY;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 352
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 353
    const-string v1, "PrintDialogActivity"

    const-string v2, "In shouldOverrideUrlLoading with url %s and returning false"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    :goto_2d
    return v0

    .line 357
    :cond_2e
    const-string v1, "PrintDialogActivity"

    const-string v2, "In shouldOverrideUrlLoading with url %s and calling super"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p2, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-super {p0, p1, p2}, Lqf;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    goto :goto_2d
.end method
