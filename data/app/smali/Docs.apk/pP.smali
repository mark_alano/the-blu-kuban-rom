.class LpP;
.super Ljava/lang/Object;
.source "GellyInjectorStore.java"

# interfaces
.implements Laoz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Laoz",
        "<",
        "Lcom/google/android/apps/docs/doclist/documentopener/VideoDocumentOpener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lpu;


# direct methods
.method constructor <init>(Lpu;)V
    .registers 2
    .parameter

    .prologue
    .line 484
    iput-object p1, p0, LpP;->a:Lpu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/apps/docs/doclist/documentopener/VideoDocumentOpener;
    .registers 14

    .prologue
    .line 487
    new-instance v0, Lcom/google/android/apps/docs/doclist/documentopener/VideoDocumentOpener;

    iget-object v1, p0, LpP;->a:Lpu;

    invoke-static {v1}, Lpu;->a(Lpu;)LYD;

    move-result-object v1

    iget-object v1, v1, LYD;->a:Lb;

    iget-object v1, v1, Lb;->a:LZb;

    invoke-virtual {v1}, LZb;->a()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lpu;->M(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iget-object v2, p0, LpP;->a:Lpu;

    invoke-static {v2}, Lpu;->a(Lpu;)LYD;

    move-result-object v2

    iget-object v2, v2, LYD;->a:LXu;

    iget-object v2, v2, LXu;->b:LZb;

    invoke-virtual {v2}, LZb;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lpu;->N(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LXP;

    iget-object v3, p0, LpP;->a:Lpu;

    invoke-static {v3}, Lpu;->a(Lpu;)LYD;

    move-result-object v3

    iget-object v3, v3, LYD;->a:Llb;

    iget-object v3, v3, Llb;->c:LZb;

    invoke-virtual {v3}, LZb;->a()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lpu;->O(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Llf;

    iget-object v4, p0, LpP;->a:Lpu;

    invoke-static {v4}, Lpu;->a(Lpu;)LYD;

    move-result-object v4

    iget-object v4, v4, LYD;->a:LeO;

    iget-object v4, v4, LeO;->a:LZb;

    invoke-virtual {v4}, LZb;->a()Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Lpu;->P(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LeQ;

    iget-object v5, p0, LpP;->a:Lpu;

    invoke-static {v5}, Lpu;->a(Lpu;)LYD;

    move-result-object v5

    iget-object v5, v5, LYD;->a:LVM;

    iget-object v5, v5, LVM;->b:LZb;

    invoke-virtual {v5}, LZb;->a()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Lpu;->Q(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LVH;

    iget-object v6, p0, LpP;->a:Lpu;

    invoke-static {v6}, Lpu;->a(Lpu;)LYD;

    move-result-object v6

    iget-object v6, v6, LYD;->a:Lpu;

    iget-object v6, v6, Lpu;->c:LZb;

    invoke-virtual {v6}, LZb;->a()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Lpu;->R(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LoZ;

    iget-object v7, p0, LpP;->a:Lpu;

    invoke-static {v7}, Lpu;->a(Lpu;)LYD;

    move-result-object v7

    iget-object v7, v7, LYD;->a:LVc;

    iget-object v7, v7, LVc;->b:LZb;

    invoke-virtual {v7}, LZb;->a()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v7}, Lpu;->S(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LUL;

    iget-object v8, p0, LpP;->a:Lpu;

    invoke-static {v8}, Lpu;->a(Lpu;)LYD;

    move-result-object v8

    iget-object v8, v8, LYD;->a:Lpu;

    iget-object v8, v8, Lpu;->b:LZb;

    invoke-virtual {v8}, LZb;->a()Ljava/lang/Object;

    move-result-object v8

    invoke-static {v8}, Lpu;->T(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    iget-object v9, p0, LpP;->a:Lpu;

    invoke-static {v9}, Lpu;->a(Lpu;)LYD;

    move-result-object v9

    iget-object v9, v9, LYD;->a:LqA;

    iget-object v9, v9, LqA;->d:LZb;

    invoke-virtual {v9}, LZb;->a()Ljava/lang/Object;

    move-result-object v9

    invoke-static {v9}, Lpu;->U(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lqv;

    iget-object v10, p0, LpP;->a:Lpu;

    invoke-static {v10}, Lpu;->a(Lpu;)LYD;

    move-result-object v10

    iget-object v10, v10, LYD;->a:LqA;

    iget-object v10, v10, LqA;->c:LZb;

    invoke-virtual {v10}, LZb;->a()Ljava/lang/Object;

    move-result-object v10

    invoke-static {v10}, Lpu;->V(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lqr;

    iget-object v11, p0, LpP;->a:Lpu;

    invoke-static {v11}, Lpu;->a(Lpu;)LYD;

    move-result-object v11

    iget-object v11, v11, LYD;->a:LXu;

    iget-object v11, v11, LXu;->f:LZb;

    invoke-virtual {v11}, LZb;->a()Ljava/lang/Object;

    move-result-object v11

    invoke-static {v11}, Lpu;->W(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, LYf;

    iget-object v12, p0, LpP;->a:Lpu;

    invoke-static {v12}, Lpu;->a(Lpu;)LYD;

    move-result-object v12

    iget-object v12, v12, LYD;->a:Lgr;

    iget-object v12, v12, Lgr;->c:LZb;

    invoke-virtual {v12}, LZb;->a()Ljava/lang/Object;

    move-result-object v12

    invoke-static {v12}, Lpu;->X(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lgl;

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/docs/doclist/documentopener/VideoDocumentOpener;-><init>(Landroid/content/Context;LXP;Llf;LeQ;LVH;LoZ;LUL;Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;Lqv;Lqr;LYf;Lgl;)V

    .line 549
    return-object v0
.end method

.method public bridge synthetic a()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 484
    invoke-virtual {p0}, LpP;->a()Lcom/google/android/apps/docs/doclist/documentopener/VideoDocumentOpener;

    move-result-object v0

    return-object v0
.end method
