.class public LpW;
.super Ljava/lang/Object;
.source "LocalFileIntentOpener.java"

# interfaces
.implements Lnm;


# instance fields
.field private final a:LUM;

.field private a:LaaS;

.field private final a:Landroid/os/Bundle;

.field final synthetic a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

.field private final a:Ljava/lang/String;

.field private final a:Lpa;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;Lpa;LUM;Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, LpW;->a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p3, p0, LpW;->a:LUM;

    .line 65
    iput-object p2, p0, LpW;->a:Lpa;

    .line 66
    iput-object p4, p0, LpW;->a:Ljava/lang/String;

    .line 67
    iput-object p5, p0, LpW;->a:Landroid/os/Bundle;

    .line 68
    return-void
.end method


# virtual methods
.method public a(I)I
    .registers 8
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, LpW;->a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    iget-object v1, p0, LpW;->a:Lpa;

    iget-object v2, p0, LpW;->a:LUM;

    iget-object v3, p0, LpW;->a:Landroid/os/Bundle;

    iget-object v4, p0, LpW;->a:Ljava/lang/String;

    iget-object v5, p0, LpW;->a:LaaS;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;Lpa;LUM;Landroid/os/Bundle;Ljava/lang/String;LaaS;)V

    .line 73
    const/4 v0, -0x1

    return v0
.end method

.method public a()Ljava/lang/String;
    .registers 5

    .prologue
    .line 78
    iget-object v0, p0, LpW;->a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Len;->opening_document:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 79
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LpW;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaaS;)V
    .registers 2
    .parameter

    .prologue
    .line 89
    iput-object p1, p0, LpW;->a:LaaS;

    .line 90
    return-void
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 84
    iget-object v0, p0, LpW;->a:LUM;

    invoke-interface {v0}, LUM;->a()Ljavax/crypto/SecretKey;

    move-result-object v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, LpW;->a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    iget-object v1, p0, LpW;->a:LUM;

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;LUM;)Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, LpW;->a:Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    invoke-static {v0}, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;->a(Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;)Z

    move-result v0

    if-nez v0, :cond_1c

    const/4 v0, 0x1

    :goto_1b
    return v0

    :cond_1c
    const/4 v0, 0x0

    goto :goto_1b
.end method
