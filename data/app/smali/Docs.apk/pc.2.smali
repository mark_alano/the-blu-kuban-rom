.class public final enum Lpc;
.super Ljava/lang/Enum;
.source "DocumentOpenerError.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lpc;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lpc;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum a:Lpc;

.field private static final synthetic a:[Lpc;

.field public static final enum b:Lpc;

.field public static final enum c:Lpc;

.field public static final enum d:Lpc;

.field public static final enum e:Lpc;

.field public static final enum f:Lpc;

.field public static final enum g:Lpc;

.field public static final enum h:Lpc;


# instance fields
.field private final a:I

.field private final a:Ljava/lang/Integer;

.field private final a:Z


# direct methods
.method static constructor <clinit>()V
    .registers 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lpc;

    const-string v1, "USER_INTERRUPTED"

    const/4 v4, 0x0

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lpc;-><init>(Ljava/lang/String;IILjava/lang/Integer;Z)V

    sput-object v0, Lpc;->a:Lpc;

    .line 23
    new-instance v4, Lpc;

    const-string v5, "DOCUMENT_UNAVAILABLE"

    const/4 v7, -0x1

    sget v0, Len;->error_document_not_available:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move v6, v3

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lpc;-><init>(Ljava/lang/String;IILjava/lang/Integer;Z)V

    sput-object v4, Lpc;->b:Lpc;

    .line 26
    new-instance v4, Lpc;

    const-string v5, "VIEWER_UNAVAILABLE"

    const/4 v7, -0x2

    sget v0, Len;->error_no_viewer_available:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move v6, v10

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lpc;-><init>(Ljava/lang/String;IILjava/lang/Integer;Z)V

    sput-object v4, Lpc;->c:Lpc;

    .line 29
    new-instance v4, Lpc;

    const-string v5, "VIDEO_UNAVAILABLE"

    const/4 v7, -0x3

    sget v0, Len;->error_video_not_available:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move v6, v11

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lpc;-><init>(Ljava/lang/String;IILjava/lang/Integer;Z)V

    sput-object v4, Lpc;->d:Lpc;

    .line 32
    new-instance v4, Lpc;

    const-string v5, "EXTERNAL_STORAGE_NOT_READY"

    const/4 v7, -0x4

    sget v0, Len;->pin_error_external_storage_not_ready:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move v6, v12

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lpc;-><init>(Ljava/lang/String;IILjava/lang/Integer;Z)V

    sput-object v4, Lpc;->e:Lpc;

    .line 35
    new-instance v4, Lpc;

    const-string v5, "AUTHENTICATION_FAILURE"

    const/4 v6, 0x5

    const/16 v7, -0x65

    sget v0, Len;->error_access_denied_html:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lpc;-><init>(Ljava/lang/String;IILjava/lang/Integer;Z)V

    sput-object v4, Lpc;->f:Lpc;

    .line 38
    new-instance v4, Lpc;

    const-string v5, "CONNECTION_FAILURE"

    const/4 v6, 0x6

    const/16 v7, -0x66

    sget v0, Len;->error_network_error_html:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lpc;-><init>(Ljava/lang/String;IILjava/lang/Integer;Z)V

    sput-object v4, Lpc;->g:Lpc;

    .line 41
    new-instance v4, Lpc;

    const-string v5, "UNKNOWN_INTERNAL"

    const/4 v6, 0x7

    const/16 v7, -0xc8

    sget v0, Len;->error_internal_error_html:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    move v9, v2

    invoke-direct/range {v4 .. v9}, Lpc;-><init>(Ljava/lang/String;IILjava/lang/Integer;Z)V

    sput-object v4, Lpc;->h:Lpc;

    .line 17
    const/16 v0, 0x8

    new-array v0, v0, [Lpc;

    sget-object v1, Lpc;->a:Lpc;

    aput-object v1, v0, v2

    sget-object v1, Lpc;->b:Lpc;

    aput-object v1, v0, v3

    sget-object v1, Lpc;->c:Lpc;

    aput-object v1, v0, v10

    sget-object v1, Lpc;->d:Lpc;

    aput-object v1, v0, v11

    sget-object v1, Lpc;->e:Lpc;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v3, Lpc;->f:Lpc;

    aput-object v3, v0, v1

    const/4 v1, 0x6

    sget-object v3, Lpc;->g:Lpc;

    aput-object v3, v0, v1

    const/4 v1, 0x7

    sget-object v3, Lpc;->h:Lpc;

    aput-object v3, v0, v1

    sput-object v0, Lpc;->a:[Lpc;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lpc;->a:Ljava/util/Map;

    .line 47
    invoke-static {}, Lpc;->values()[Lpc;

    move-result-object v0

    array-length v1, v0

    :goto_c6
    if-ge v2, v1, :cond_da

    aget-object v3, v0, v2

    .line 48
    sget-object v4, Lpc;->a:Ljava/util/Map;

    invoke-virtual {v3}, Lpc;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    add-int/lit8 v2, v2, 0x1

    goto :goto_c6

    .line 50
    :cond_da
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/Integer;Z)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Integer;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 74
    iput p3, p0, Lpc;->a:I

    .line 75
    iput-object p4, p0, Lpc;->a:Ljava/lang/Integer;

    .line 76
    iput-boolean p5, p0, Lpc;->a:Z

    .line 77
    return-void
.end method

.method public static a(I)Lpc;
    .registers 5
    .parameter

    .prologue
    .line 56
    sget-object v0, Lpc;->a:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpc;

    .line 57
    if-eqz v0, :cond_26

    const/4 v1, 0x1

    :goto_f
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error code not recognized: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lagu;->a(ZLjava/lang/Object;)V

    .line 59
    return-object v0

    .line 57
    :cond_26
    const/4 v1, 0x0

    goto :goto_f
.end method

.method public static valueOf(Ljava/lang/String;)Lpc;
    .registers 2
    .parameter

    .prologue
    .line 17
    const-class v0, Lpc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lpc;

    return-object v0
.end method

.method public static values()[Lpc;
    .registers 1

    .prologue
    .line 17
    sget-object v0, Lpc;->a:[Lpc;

    invoke-virtual {v0}, [Lpc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lpc;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 84
    iget v0, p0, Lpc;->a:I

    return v0
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 91
    iget-object v0, p0, Lpc;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public b()I
    .registers 4

    .prologue
    .line 106
    iget-object v0, p0, Lpc;->a:Ljava/lang/Integer;

    if-nez v0, :cond_1d

    .line 107
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not reportable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_1d
    iget-object v0, p0, Lpc;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 98
    iget-boolean v0, p0, Lpc;->a:Z

    return v0
.end method
