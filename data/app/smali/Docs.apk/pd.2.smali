.class public Lpd;
.super Laoe;
.source "DocumentOpenerModule.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 21
    invoke-direct {p0}, Laoe;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()V
    .registers 3

    .prologue
    .line 25
    const-class v0, LoZ;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-string v1, "DefaultLocal"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/annotation/Annotation;)LaoQ;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/doclist/documentopener/LocalFileIntentOpener;

    invoke-interface {v0, v1}, LaoQ;->a(Ljava/lang/Class;)LaoR;

    .line 27
    const-class v0, LoZ;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-string v1, "DefaultRemote"

    invoke-static {v1}, LaqY;->a(Ljava/lang/String;)LaqW;

    move-result-object v1

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/annotation/Annotation;)LaoQ;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/doclist/documentopener/HtmlDocumentOpener;

    invoke-interface {v0, v1}, LaoQ;->a(Ljava/lang/Class;)LaoR;

    .line 29
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpener;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 30
    const-class v0, Lqv;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lqy;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 31
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 32
    const-class v0, LqN;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, LqO;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 34
    const-class v0, Lqr;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lqs;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 38
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/GViewDocumentOpener;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/doclist/documentopener/NativeGViewDocumentOpener;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 43
    const-class v0, Lqb;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    sget-object v1, LaoE;->a:LaoC;

    invoke-interface {v0, v1}, LaoM;->a(LaoC;)V

    .line 44
    const-class v0, LpY;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    sget-object v1, LaoE;->a:LaoC;

    invoke-interface {v0, v1}, LaoM;->a(LaoC;)V

    .line 45
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreator;

    invoke-virtual {p0, v0}, Lpd;->a(Ljava/lang/Class;)LaoM;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/docs/doclist/documentopener/FileOpenerIntentCreatorImpl;

    invoke-interface {v0, v1}, LaoM;->a(Ljava/lang/Class;)LaoR;

    .line 46
    return-void
.end method
