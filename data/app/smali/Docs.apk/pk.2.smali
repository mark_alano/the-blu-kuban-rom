.class public Lpk;
.super LoX;
.source "ExportOpenerSelector.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LoX",
        "<",
        "LkP;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Laoz;Laoz;)V
    .registers 4
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileUnknownDocumentOpener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-static {p1, p2}, Lpk;->a(Laoz;Laoz;)Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, LoX;-><init>(Ljava/util/Map;)V

    .line 25
    return-void
.end method

.method static a(Laoz;Laoz;)Ljava/util/Map;
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/PdfExportDocumentOpener;",
            ">;",
            "Laoz",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileUnknownDocumentOpener;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "LkP;",
            "Laoz",
            "<+",
            "LoZ;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 35
    sget-object v1, LkP;->c:LkP;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    sget-object v1, LkP;->d:LkP;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    sget-object v1, LkP;->a:LkP;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    sget-object v1, LkP;->j:LkP;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    sget-object v1, LkP;->e:LkP;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    sget-object v1, LkP;->k:LkP;

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v1, LkP;->b:LkP;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    return-object v0
.end method
