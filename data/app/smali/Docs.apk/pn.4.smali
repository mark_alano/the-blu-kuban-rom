.class public abstract enum Lpn;
.super Ljava/lang/Enum;
.source "FileOpenerIntentCreatorImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lpn;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lpn;

.field private static final synthetic a:[Lpn;

.field public static final enum b:Lpn;

.field public static final enum c:Lpn;

.field public static final enum d:Lpn;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 81
    new-instance v0, Lpo;

    const-string v1, "URI_WITH_MIME_TYPE"

    invoke-direct {v0, v1, v2}, Lpo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lpn;->a:Lpn;

    .line 88
    new-instance v0, Lpp;

    const-string v1, "URI_WITHOUT_MIME_TYPE"

    invoke-direct {v0, v1, v3}, Lpp;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lpn;->b:Lpn;

    .line 95
    new-instance v0, Lpq;

    const-string v1, "EXTENSION_WITH_MIME_TYPE"

    invoke-direct {v0, v1, v4}, Lpq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lpn;->c:Lpn;

    .line 102
    new-instance v0, Lpr;

    const-string v1, "EXTENSION_WITHOUT_MIME_TYPE"

    invoke-direct {v0, v1, v5}, Lpr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lpn;->d:Lpn;

    .line 80
    const/4 v0, 0x4

    new-array v0, v0, [Lpn;

    sget-object v1, Lpn;->a:Lpn;

    aput-object v1, v0, v2

    sget-object v1, Lpn;->b:Lpn;

    aput-object v1, v0, v3

    sget-object v1, Lpn;->c:Lpn;

    aput-object v1, v0, v4

    sget-object v1, Lpn;->d:Lpn;

    aput-object v1, v0, v5

    sput-object v0, Lpn;->a:[Lpn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILpm;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lpn;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lpn;
    .registers 2
    .parameter

    .prologue
    .line 80
    const-class v0, Lpn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lpn;

    return-object v0
.end method

.method public static values()[Lpn;
    .registers 1

    .prologue
    .line 80
    sget-object v0, Lpn;->a:[Lpn;

    invoke-virtual {v0}, [Lpn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lpn;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;LfS;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Lpl;
.end method
