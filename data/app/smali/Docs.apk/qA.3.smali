.class public final LqA;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LqN;",
            ">;"
        }
    .end annotation
.end field

.field public c:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lqr;",
            ">;"
        }
    .end annotation
.end field

.field public d:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lqv;",
            ">;"
        }
    .end annotation
.end field

.field public e:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LqO;",
            ">;"
        }
    .end annotation
.end field

.field public f:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lqs;",
            ">;"
        }
    .end annotation
.end field

.field public g:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lqy;",
            ">;"
        }
    .end annotation
.end field

.field public h:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 38
    iput-object p1, p0, LqA;->a:LYD;

    .line 39
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LqA;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LqA;->a:LZb;

    .line 42
    const-class v0, LqN;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LqA;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LqA;->b:LZb;

    .line 45
    const-class v0, Lqr;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LqA;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LqA;->c:LZb;

    .line 48
    const-class v0, Lqv;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LqA;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LqA;->d:LZb;

    .line 51
    const-class v0, LqO;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LqA;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LqA;->e:LZb;

    .line 54
    const-class v0, Lqs;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LqA;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LqA;->f:LZb;

    .line 57
    const-class v0, Lqy;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LqA;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LqA;->g:LZb;

    .line 60
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, LqA;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, LqA;->h:LZb;

    .line 63
    return-void
.end method

.method static synthetic a(LqA;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, LqA;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, LqA;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 70
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpener;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LqA;->a:LZb;

    invoke-virtual {p0, v0, v1}, LqA;->a(Laop;LZb;)V

    .line 71
    const-class v0, LqN;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LqA;->b:LZb;

    invoke-virtual {p0, v0, v1}, LqA;->a(Laop;LZb;)V

    .line 72
    const-class v0, Lqr;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LqA;->c:LZb;

    invoke-virtual {p0, v0, v1}, LqA;->a(Laop;LZb;)V

    .line 73
    const-class v0, Lqv;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LqA;->d:LZb;

    invoke-virtual {p0, v0, v1}, LqA;->a(Laop;LZb;)V

    .line 74
    const-class v0, LqO;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LqA;->e:LZb;

    invoke-virtual {p0, v0, v1}, LqA;->a(Laop;LZb;)V

    .line 75
    const-class v0, Lqs;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LqA;->f:LZb;

    invoke-virtual {p0, v0, v1}, LqA;->a(Laop;LZb;)V

    .line 76
    const-class v0, Lqy;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LqA;->g:LZb;

    invoke-virtual {p0, v0, v1}, LqA;->a(Laop;LZb;)V

    .line 77
    const-class v0, Lcom/google/android/apps/docs/doclist/documentopener/driveapp/ThirdPartyDocumentOpenerImpl;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, LqA;->h:LZb;

    invoke-virtual {p0, v0, v1}, LqA;->a(Laop;LZb;)V

    .line 78
    iget-object v0, p0, LqA;->a:LZb;

    iget-object v1, p0, LqA;->a:LYD;

    iget-object v1, v1, LYD;->a:LqA;

    iget-object v1, v1, LqA;->h:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 80
    iget-object v0, p0, LqA;->b:LZb;

    iget-object v1, p0, LqA;->a:LYD;

    iget-object v1, v1, LYD;->a:LqA;

    iget-object v1, v1, LqA;->e:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 82
    iget-object v0, p0, LqA;->c:LZb;

    iget-object v1, p0, LqA;->a:LYD;

    iget-object v1, v1, LYD;->a:LqA;

    iget-object v1, v1, LqA;->f:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 84
    iget-object v0, p0, LqA;->d:LZb;

    iget-object v1, p0, LqA;->a:LYD;

    iget-object v1, v1, LYD;->a:LqA;

    iget-object v1, v1, LqA;->g:LZb;

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 86
    iget-object v0, p0, LqA;->e:LZb;

    new-instance v1, LqB;

    invoke-direct {v1, p0}, LqB;-><init>(LqA;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 120
    iget-object v0, p0, LqA;->f:LZb;

    new-instance v1, LqC;

    invoke-direct {v1, p0}, LqC;-><init>(LqA;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 134
    iget-object v0, p0, LqA;->g:LZb;

    new-instance v1, LqD;

    invoke-direct {v1, p0}, LqD;-><init>(LqA;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 148
    iget-object v0, p0, LqA;->h:LZb;

    new-instance v1, LqE;

    invoke-direct {v1, p0}, LqE;-><init>(LqA;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 187
    return-void
.end method

.method public b()V
    .registers 1

    .prologue
    .line 191
    return-void
.end method
