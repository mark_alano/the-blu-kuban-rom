.class final enum LqU;
.super Ljava/lang/Enum;
.source "EntriesGrouper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LqU;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LqU;

.field private static final synthetic a:[LqU;

.field public static final enum b:LqU;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, LqU;

    const-string v1, "ASC"

    invoke-direct {v0, v1, v2}, LqU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LqU;->a:LqU;

    .line 25
    new-instance v0, LqU;

    const-string v1, "DESC"

    invoke-direct {v0, v1, v3}, LqU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LqU;->b:LqU;

    .line 22
    const/4 v0, 0x2

    new-array v0, v0, [LqU;

    sget-object v1, LqU;->a:LqU;

    aput-object v1, v0, v2

    sget-object v1, LqU;->b:LqU;

    aput-object v1, v0, v3

    sput-object v0, LqU;->a:[LqU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LqU;
    .registers 2
    .parameter

    .prologue
    .line 22
    const-class v0, LqU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LqU;

    return-object v0
.end method

.method public static values()[LqU;
    .registers 1

    .prologue
    .line 22
    sget-object v0, LqU;->a:[LqU;

    invoke-virtual {v0}, [LqU;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LqU;

    return-object v0
.end method
