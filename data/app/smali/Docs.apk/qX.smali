.class public LqX;
.super Ljava/lang/Object;
.source "FastScroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:J

.field final synthetic a:LqW;

.field b:J


# direct methods
.method public constructor <init>(LqW;)V
    .registers 2
    .parameter

    .prologue
    .line 527
    iput-object p1, p0, LqX;->a:LqW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method a()I
    .registers 9

    .prologue
    const-wide/16 v6, 0xd0

    .line 541
    iget-object v0, p0, LqX;->a:LqW;

    invoke-virtual {v0}, LqW;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_e

    .line 542
    const/16 v0, 0xd0

    .line 551
    :goto_d
    return v0

    .line 545
    :cond_e
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 546
    iget-wide v2, p0, LqX;->a:J

    iget-wide v4, p0, LqX;->b:J

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_1d

    .line 547
    const/4 v0, 0x0

    goto :goto_d

    .line 549
    :cond_1d
    iget-wide v2, p0, LqX;->a:J

    sub-long/2addr v0, v2

    mul-long/2addr v0, v6

    iget-wide v2, p0, LqX;->b:J

    div-long/2addr v0, v2

    sub-long v0, v6, v0

    long-to-int v0, v0

    goto :goto_d
.end method

.method a()V
    .registers 3

    .prologue
    .line 535
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, LqX;->b:J

    .line 536
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LqX;->a:J

    .line 537
    iget-object v0, p0, LqX;->a:LqW;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, LqW;->a(I)V

    .line 538
    return-void
.end method

.method public run()V
    .registers 3

    .prologue
    .line 556
    iget-object v0, p0, LqX;->a:LqW;

    invoke-virtual {v0}, LqW;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_d

    .line 557
    invoke-virtual {p0}, LqX;->a()V

    .line 566
    :goto_c
    return-void

    .line 561
    :cond_d
    invoke-virtual {p0}, LqX;->a()I

    move-result v0

    if-lez v0, :cond_1d

    .line 562
    iget-object v0, p0, LqX;->a:LqW;

    invoke-static {v0}, LqW;->a(LqW;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_c

    .line 564
    :cond_1d
    iget-object v0, p0, LqX;->a:LqW;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LqW;->a(I)V

    goto :goto_c
.end method
