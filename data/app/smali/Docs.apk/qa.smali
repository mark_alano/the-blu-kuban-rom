.class Lqa;
.super LWN;
.source "ProgressSyncMonitor.java"


# instance fields
.field private final a:LaaS;

.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;LaaS;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, LWN;-><init>()V

    .line 30
    sget v0, Len;->download_bytes_format:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqa;->a:Ljava/lang/String;

    .line 31
    sget v0, Len;->download_bytes_format_short:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqa;->b:Ljava/lang/String;

    .line 32
    sget v0, Len;->download_kilobytes_format:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqa;->c:Ljava/lang/String;

    .line 33
    sget v0, Len;->download_kilobytes_format_short:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqa;->d:Ljava/lang/String;

    .line 34
    sget v0, Len;->download_megabytes_format:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqa;->e:Ljava/lang/String;

    .line 35
    sget v0, Len;->download_megabytes_format_short:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lqa;->f:Ljava/lang/String;

    .line 36
    iput-object p2, p0, Lqa;->a:LaaS;

    .line 37
    return-void
.end method

.method private a(J)Ljava/lang/String;
    .registers 11
    .parameter

    .prologue
    const-wide/high16 v6, 0x4130

    const-wide/high16 v4, 0x4090

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 66
    long-to-double v0, p1

    cmpg-double v0, v0, v4

    if-gez v0, :cond_1a

    .line 67
    iget-object v0, p0, Lqa;->b:Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_19
    return-object v0

    .line 68
    :cond_1a
    long-to-double v0, p1

    cmpg-double v0, v0, v6

    if-gez v0, :cond_2e

    .line 69
    iget-object v0, p0, Lqa;->d:Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v4, v5}, Lqa;->a(JD)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_19

    .line 71
    :cond_2e
    iget-object v0, p0, Lqa;->f:Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v6, v7}, Lqa;->a(JD)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_19
.end method

.method private a(JD)Ljava/lang/String;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 76
    .line 77
    long-to-double v0, p1

    const-wide/high16 v2, 0x4024

    mul-double/2addr v2, p3

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1a

    .line 78
    const/4 v0, 0x2

    .line 85
    :goto_9
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v1

    .line 86
    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 87
    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 88
    long-to-double v2, p1

    div-double/2addr v2, p3

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 79
    :cond_1a
    long-to-double v0, p1

    const-wide/high16 v2, 0x4059

    mul-double/2addr v2, p3

    cmpg-double v0, v0, v2

    if-gez v0, :cond_24

    .line 80
    const/4 v0, 0x1

    goto :goto_9

    .line 82
    :cond_24
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private a(JJ)Ljava/lang/String;
    .registers 16
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-wide/high16 v6, 0x4130

    const-wide/high16 v4, 0x4090

    .line 53
    const-wide/high16 v0, 0x4059

    long-to-double v2, p1

    mul-double/2addr v0, v2

    long-to-double v2, p3

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 54
    long-to-double v1, p3

    cmpg-double v1, v1, v4

    if-gez v1, :cond_2f

    .line 55
    iget-object v1, p0, Lqa;->a:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 60
    :goto_2e
    return-object v0

    .line 56
    :cond_2f
    long-to-double v1, p3

    cmpg-double v1, v1, v6

    if-gez v1, :cond_50

    .line 57
    iget-object v1, p0, Lqa;->c:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v4, v5}, Lqa;->a(JD)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-direct {p0, p3, p4, v4, v5}, Lqa;->a(JD)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2e

    .line 60
    :cond_50
    iget-object v1, p0, Lqa;->e:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0, p1, p2, v6, v7}, Lqa;->a(JD)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-direct {p0, p3, p4, v6, v7}, Lqa;->a(JD)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v10

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2e
.end method


# virtual methods
.method public a(JJ)V
    .registers 11
    .parameter
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lqa;->a:LaaS;

    if-nez v0, :cond_5

    .line 50
    :goto_4
    return-void

    .line 45
    :cond_5
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_17

    invoke-direct {p0, p1, p2, p3, p4}, Lqa;->a(JJ)Ljava/lang/String;

    move-result-object v5

    .line 49
    :goto_f
    iget-object v0, p0, Lqa;->a:LaaS;

    move-wide v1, p1

    move-wide v3, p3

    invoke-interface/range {v0 .. v5}, LaaS;->a(JJLjava/lang/String;)V

    goto :goto_4

    .line 45
    :cond_17
    invoke-direct {p0, p1, p2}, Lqa;->a(J)Ljava/lang/String;

    move-result-object v5

    goto :goto_f
.end method
