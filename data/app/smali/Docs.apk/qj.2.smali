.class public final Lqj;
.super Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;
.source "VideoDocumentOpener.java"


# instance fields
.field final synthetic a:LYf;


# direct methods
.method public constructor <init>(Landroid/content/Context;LXP;Llf;LeQ;LVH;LoZ;LUL;LYf;)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    iput-object p8, p0, Lqj;->a:LYf;

    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/docs/doclist/documentopener/DownloadFileDocumentOpenerImpl;-><init>(Landroid/content/Context;LXP;Llf;LeQ;LVH;LoZ;LUL;)V

    return-void
.end method


# virtual methods
.method public a(LkM;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lqj;->a:LYf;

    invoke-interface {v0}, LYf;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method a(LkY;Lpa;)Ljava/lang/String;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 63
    .line 65
    :try_start_1
    iget-object v0, p0, Lqj;->a:LYf;

    invoke-interface {v0, p1}, LYf;->a(LkY;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_6} :catch_10
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_6} :catch_21
    .catch LNt; {:try_start_1 .. :try_end_6} :catch_32
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_6} :catch_43
    .catch LNv; {:try_start_1 .. :try_end_6} :catch_54

    move-result-object v0

    .line 66
    if-nez v0, :cond_f

    .line 67
    :try_start_9
    sget-object v1, Lpc;->d:Lpc;

    const/4 v2, 0x0

    invoke-interface {p2, v1, v2}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_f} :catch_6d
    .catch Landroid/accounts/AuthenticatorException; {:try_start_9 .. :try_end_f} :catch_6b
    .catch LNt; {:try_start_9 .. :try_end_f} :catch_69
    .catch Ljava/net/URISyntaxException; {:try_start_9 .. :try_end_f} :catch_67
    .catch LNv; {:try_start_9 .. :try_end_f} :catch_65

    .line 85
    :cond_f
    :goto_f
    return-object v0

    .line 69
    :catch_10
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 70
    :goto_14
    const-string v2, "VideoDocumentOpener"

    const-string v3, "Network error while retrieving video URL."

    invoke-static {v2, v3, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 71
    sget-object v2, Lpc;->g:Lpc;

    invoke-interface {p2, v2, v1}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_f

    .line 72
    :catch_21
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 73
    :goto_25
    const-string v2, "VideoDocumentOpener"

    const-string v3, "Authentication error while retrieving video URL."

    invoke-static {v2, v3, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 74
    sget-object v2, Lpc;->f:Lpc;

    invoke-interface {p2, v2, v1}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_f

    .line 75
    :catch_32
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 76
    :goto_36
    const-string v2, "VideoDocumentOpener"

    const-string v3, "Credential error while retrieving video URL."

    invoke-static {v2, v3, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 77
    sget-object v2, Lpc;->f:Lpc;

    invoke-interface {p2, v2, v1}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_f

    .line 78
    :catch_43
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 79
    :goto_47
    const-string v2, "VideoDocumentOpener"

    const-string v3, "Failed to construct video info URL."

    invoke-static {v2, v3, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 80
    sget-object v2, Lpc;->h:Lpc;

    invoke-interface {p2, v2, v1}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_f

    .line 81
    :catch_54
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    .line 82
    :goto_58
    const-string v2, "VideoDocumentOpener"

    const-string v3, "Too many redirects while retrieving video info URL."

    invoke-static {v2, v3, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 83
    sget-object v2, Lpc;->h:Lpc;

    invoke-interface {p2, v2, v1}, Lpa;->a(Lpc;Ljava/lang/Throwable;)V

    goto :goto_f

    .line 81
    :catch_65
    move-exception v1

    goto :goto_58

    .line 78
    :catch_67
    move-exception v1

    goto :goto_47

    .line 75
    :catch_69
    move-exception v1

    goto :goto_36

    .line 72
    :catch_6b
    move-exception v1

    goto :goto_25

    .line 69
    :catch_6d
    move-exception v1

    goto :goto_14
.end method
