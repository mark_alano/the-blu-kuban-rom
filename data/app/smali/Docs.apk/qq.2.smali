.class Lqq;
.super Ljava/lang/Object;
.source "AbstractDriveAppOpenerOption.java"

# interfaces
.implements Lnm;


# instance fields
.field private a:Landroid/content/Intent;

.field final synthetic a:Ljava/lang/String;

.field final synthetic a:LkM;

.field final synthetic a:Lpa;

.field final synthetic a:Lqp;


# direct methods
.method constructor <init>(Lqp;Ljava/lang/String;Lpa;LkM;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, Lqq;->a:Lqp;

    iput-object p2, p0, Lqq;->a:Ljava/lang/String;

    iput-object p3, p0, Lqq;->a:Lpa;

    iput-object p4, p0, Lqq;->a:LkM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)I
    .registers 7
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 63
    if-nez p1, :cond_22

    .line 64
    iget-object v1, p0, Lqq;->a:Lqp;

    iget-object v1, v1, Lqp;->a:Lqr;

    iget-object v2, p0, Lqq;->a:Lpa;

    iget-object v3, p0, Lqq;->a:Lqp;

    iget-object v3, v3, Lqp;->a:Ljava/lang/String;

    iget-object v4, p0, Lqq;->a:LkM;

    invoke-interface {v1, v2, v3, v4}, Lqr;->a(Lpa;Ljava/lang/String;LkM;)Landroid/net/Uri;

    move-result-object v1

    .line 65
    if-nez v1, :cond_16

    .line 73
    :goto_15
    return v0

    .line 68
    :cond_16
    iget-object v0, p0, Lqq;->a:Lqp;

    iget-object v2, p0, Lqq;->a:LkM;

    invoke-virtual {v0, v2, v1}, Lqp;->a(LkM;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lqq;->a:Landroid/content/Intent;

    .line 69
    const/4 v0, 0x1

    goto :goto_15

    .line 71
    :cond_22
    iget-object v1, p0, Lqq;->a:Landroid/content/Intent;

    invoke-static {v1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v1, p0, Lqq;->a:Lpa;

    iget-object v2, p0, Lqq;->a:Landroid/content/Intent;

    invoke-interface {v1, v2}, Lpa;->a(Landroid/content/Intent;)V

    goto :goto_15
.end method

.method public a()Ljava/lang/String;
    .registers 5

    .prologue
    .line 53
    iget-object v0, p0, Lqq;->a:Lqp;

    iget-object v0, v0, Lqp;->a:Landroid/content/Context;

    sget v1, Len;->opening_document:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lqq;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(LaaS;)V
    .registers 2
    .parameter

    .prologue
    .line 49
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method
