.class public LrK;
.super Ljava/lang/Object;
.source "ItemToUpload.java"


# instance fields
.field private a:I

.field private a:Laay;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<+",
            "LZS;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private a:LrN;

.field private a:Z

.field private b:Ljava/lang/String;

.field private b:Z

.field private c:Ljava/lang/String;

.field private c:Z


# direct methods
.method constructor <init>(Laoz;)V
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<+",
            "LZS;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    iput-boolean v0, p0, LrK;->c:Z

    .line 216
    iput-object p1, p0, LrK;->a:Laoz;

    .line 217
    iput v0, p0, LrK;->a:I

    .line 218
    return-void
.end method

.method static synthetic a(LrK;)I
    .registers 2
    .parameter

    .prologue
    .line 40
    iget v0, p0, LrK;->a:I

    return v0
.end method

.method static synthetic a(LrK;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput p1, p0, LrK;->a:I

    return p1
.end method

.method static synthetic a(LrK;)Laay;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, LrK;->a:Laay;

    return-object v0
.end method

.method static synthetic a(LrK;Laay;)Laay;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, LrK;->a:Laay;

    return-object p1
.end method

.method static synthetic a(LrK;)Laoz;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, LrK;->a:Laoz;

    return-object v0
.end method

.method static synthetic a(LrK;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, LrK;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(LrK;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, LrK;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(LrK;)LrN;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, LrK;->a:LrN;

    return-object v0
.end method

.method static synthetic a(LrK;LrN;)LrN;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, LrK;->a:LrN;

    return-object p1
.end method

.method static synthetic a(LrK;)Z
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-boolean v0, p0, LrK;->b:Z

    return v0
.end method

.method static synthetic a(LrK;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-boolean p1, p0, LrK;->a:Z

    return p1
.end method

.method static synthetic b(LrK;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, LrK;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(LrK;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, LrK;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(LrK;)Z
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-boolean v0, p0, LrK;->a:Z

    return v0
.end method

.method static synthetic b(LrK;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-boolean p1, p0, LrK;->b:Z

    return p1
.end method

.method static synthetic c(LrK;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, LrK;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(LrK;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, LrK;->c:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 351
    iget v0, p0, LrK;->a:I

    return v0
.end method

.method public a()Laay;
    .registers 2

    .prologue
    .line 331
    iget-object v0, p0, LrK;->a:Laay;

    return-object v0
.end method

.method public a(Ljava/io/InputStream;)Laay;
    .registers 6
    .parameter

    .prologue
    .line 423
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    :try_start_3
    new-instance v1, Laay;

    invoke-static {}, LVp;->a()LVo;

    move-result-object v0

    invoke-interface {v0}, LVo;->a()Ljava/io/File;

    move-result-object v0

    invoke-direct {v1, v0}, Laay;-><init>(Ljava/io/File;)V

    .line 429
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Laay;->a()Ljava/io/File;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 430
    new-instance v3, Lru;

    new-instance v0, LrL;

    invoke-direct {v0, p0}, LrL;-><init>(LrK;)V

    invoke-direct {v3, p1, v0}, Lru;-><init>(Ljava/io/InputStream;Lrv;)V

    .line 438
    iget-object v0, p0, LrK;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LZS;

    invoke-interface {v0, v3, v2}, LZS;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_2e
    .catch Lrw; {:try_start_3 .. :try_end_2e} :catch_2f
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_2e} :catch_31

    .line 445
    return-object v1

    .line 439
    :catch_2f
    move-exception v0

    .line 440
    throw v0

    .line 441
    :catch_31
    move-exception v0

    .line 442
    new-instance v1, LrT;

    const-string v2, "Error while creating temp file for uploading."

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a()Ljava/io/File;
    .registers 2

    .prologue
    .line 324
    iget-object v0, p0, LrK;->a:Laay;

    if-nez v0, :cond_6

    .line 325
    const/4 v0, 0x0

    .line 327
    :goto_5
    return-object v0

    :cond_6
    iget-object v0, p0, LrK;->a:Laay;

    invoke-virtual {v0}, Laay;->a()Ljava/io/File;

    move-result-object v0

    goto :goto_5
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 320
    iget-object v0, p0, LrK;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 355
    iget-object v0, p0, LrK;->a:LrN;

    invoke-interface {v0, p1}, LrN;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()LrK;
    .registers 3

    .prologue
    .line 398
    invoke-virtual {p0}, LrK;->a()Laay;

    move-result-object v0

    if-nez v0, :cond_13

    .line 399
    iget-object v0, p0, LrK;->a:LrN;

    invoke-interface {v0}, LrN;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 400
    invoke-virtual {p0, v0}, LrK;->a(Ljava/io/InputStream;)Laay;

    move-result-object v0

    .line 401
    if-nez v0, :cond_14

    .line 402
    const/4 p0, 0x0

    .line 411
    :cond_13
    :goto_13
    return-object p0

    .line 404
    :cond_14
    new-instance v1, LrM;

    invoke-direct {v1, p0}, LrM;-><init>(LrK;)V

    invoke-virtual {v1, v0}, LrM;->a(Laay;)LrM;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LrM;->b(Z)LrM;

    move-result-object v0

    invoke-virtual {v0}, LrM;->a()LrK;

    move-result-object v0

    .line 408
    invoke-virtual {p0}, LrK;->a()V

    move-object p0, v0

    .line 409
    goto :goto_13
.end method

.method public a()Lsl;
    .registers 7

    .prologue
    .line 315
    iget-object v0, p0, LrK;->a:LrN;

    iget-object v1, p0, LrK;->a:Ljava/lang/String;

    invoke-virtual {p0}, LrK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, LrK;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, LrK;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, LrK;->a()Z

    move-result v5

    invoke-interface/range {v0 .. v5}, LrN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lsl;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 383
    iget-object v0, p0, LrK;->a:Laay;

    if-eqz v0, :cond_d

    iget-boolean v0, p0, LrK;->b:Z

    if-eqz v0, :cond_d

    .line 384
    iget-object v0, p0, LrK;->a:Laay;

    invoke-virtual {v0}, Laay;->a()Z

    .line 386
    :cond_d
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .registers 3
    .parameter

    .prologue
    .line 458
    iget-object v0, p0, LrK;->a:LrN;

    invoke-interface {v0, p1}, LrN;->a(Landroid/content/Intent;)V

    .line 459
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 343
    iget-boolean v0, p0, LrK;->a:Z

    return v0
.end method

.method public b()I
    .registers 2

    .prologue
    .line 359
    iget-object v0, p0, LrK;->a:LrN;

    invoke-interface {v0}, LrN;->a()I

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .prologue
    .line 335
    iget-object v0, p0, LrK;->a:LrN;

    invoke-interface {v0}, LrN;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b()V
    .registers 2

    .prologue
    .line 450
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, LrK;->c:Z
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_6

    .line 451
    monitor-exit p0

    return-void

    .line 450
    :catchall_6
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .registers 2

    .prologue
    .line 347
    iget-boolean v0, p0, LrK;->b:Z

    return v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 339
    iget-object v0, p0, LrK;->b:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized c()Z
    .registers 2

    .prologue
    .line 454
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, LrK;->c:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Ljava/lang/String;
    .registers 2

    .prologue
    .line 363
    iget-object v0, p0, LrK;->c:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 368
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Upload Item"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 369
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " TITLE="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LrK;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " MIME="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LrK;->a:LrN;

    invoke-interface {v2}, LrN;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 371
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ACCOUNTNAME="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LrK;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " CONVERT="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LrK;->a:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " DELETEAFTERUPLOAD="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, LrK;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ORIENTATION="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, LrK;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
