.class public LrY;
.super Ljava/lang/Object;
.source "UploadProgressNotifier.java"

# interfaces
.implements Lsn;


# instance fields
.field private a:J

.field private final a:LME;

.field private final a:LPa;

.field private a:Landroid/app/Notification;

.field private final a:Landroid/app/PendingIntent;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/content/res/Resources;

.field private final a:Landroid/widget/RemoteViews;

.field private final a:Ljava/lang/String;

.field private final a:Llf;

.field private b:J

.field private final b:Landroid/app/PendingIntent;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/app/PendingIntent;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;LrK;LPa;Ljava/lang/String;Llf;LME;Landroid/content/Context;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LrY;->a:J

    .line 48
    iput-object p1, p0, LrY;->a:Landroid/app/PendingIntent;

    .line 49
    iput-object p2, p0, LrY;->b:Landroid/app/PendingIntent;

    .line 50
    iput-object p3, p0, LrY;->c:Landroid/app/PendingIntent;

    .line 51
    invoke-virtual {p9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LrY;->a:Landroid/content/res/Resources;

    .line 52
    iget-object v0, p0, LrY;->a:Landroid/content/res/Resources;

    sget v1, Len;->upload_notification_started_ticker_drivev2:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LrY;->a:Ljava/lang/String;

    .line 53
    invoke-virtual {p4}, LrK;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LrY;->b:Ljava/lang/String;

    .line 54
    iput-object p5, p0, LrY;->a:LPa;

    .line 55
    iput-object p6, p0, LrY;->d:Ljava/lang/String;

    .line 56
    iput-object p7, p0, LrY;->a:Llf;

    .line 57
    iput-object p8, p0, LrY;->a:LME;

    .line 58
    iput-object p9, p0, LrY;->a:Landroid/content/Context;

    .line 59
    invoke-virtual {p4, p6}, LrK;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LrY;->c:Ljava/lang/String;

    .line 60
    iget-object v0, p0, LrY;->b:Ljava/lang/String;

    invoke-static {p9, v0}, LrY;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v0

    iput-object v0, p0, LrY;->a:Landroid/widget/RemoteViews;

    .line 61
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/widget/RemoteViews;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 69
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget v2, Lej;->progress_notification:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 71
    sget v1, Leh;->notification_title:I

    sget v2, Len;->upload_notification_started_title_drivev2:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 73
    sget v1, Leh;->notification_progressbar:I

    const/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v4, v4}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 74
    sget v1, Leh;->notification_icon:I

    sget v2, Leg;->notification_icon:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 75
    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 6

    .prologue
    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, LrY;->b:J

    .line 81
    new-instance v0, Landroid/app/Notification;

    const v1, 0x1080088

    iget-object v2, p0, LrY;->a:Ljava/lang/String;

    iget-wide v3, p0, LrY;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    iput-object v0, p0, LrY;->a:Landroid/app/Notification;

    .line 83
    iget-object v0, p0, LrY;->a:Landroid/app/Notification;

    iget-object v1, p0, LrY;->a:Landroid/widget/RemoteViews;

    iput-object v1, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 84
    iget-object v0, p0, LrY;->a:Landroid/app/Notification;

    iget-object v1, p0, LrY;->b:Landroid/app/PendingIntent;

    iput-object v1, v0, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 85
    iget-object v0, p0, LrY;->a:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 86
    iget-object v0, p0, LrY;->a:LPa;

    const/4 v1, 0x1

    iget-object v2, p0, LrY;->a:Landroid/app/Notification;

    invoke-interface {v0, v1, v2}, LPa;->a(ILandroid/app/Notification;)V

    .line 87
    return-void
.end method

.method public a(JJ)V
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 123
    iget-object v0, p0, LrY;->a:Landroid/app/Notification;

    if-eqz v0, :cond_38

    move v0, v1

    :goto_7
    invoke-static {v0}, Lagu;->b(Z)V

    .line 124
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    .line 125
    iget-wide v5, p0, LrY;->a:J

    sub-long v5, v3, v5

    const-wide/16 v7, 0x3e8

    cmp-long v0, v5, v7

    if-lez v0, :cond_37

    .line 126
    iput-wide v3, p0, LrY;->a:J

    .line 127
    iget-object v0, p0, LrY;->a:Landroid/widget/RemoteViews;

    sget v3, Leh;->notification_progressbar:I

    const/16 v4, 0x64

    const-wide/16 v5, 0x64

    mul-long/2addr v5, p1

    div-long/2addr v5, p3

    long-to-int v5, v5

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 129
    iget-object v0, p0, LrY;->a:Landroid/app/Notification;

    iget v2, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0xa

    iput v2, v0, Landroid/app/Notification;->flags:I

    .line 131
    iget-object v0, p0, LrY;->a:LPa;

    iget-object v2, p0, LrY;->a:Landroid/app/Notification;

    invoke-interface {v0, v1, v2}, LPa;->a(ILandroid/app/Notification;)V

    .line 133
    :cond_37
    return-void

    :cond_38
    move v0, v2

    .line 123
    goto :goto_7
.end method

.method public a(LrT;)V
    .registers 7
    .parameter

    .prologue
    .line 108
    new-instance v0, Landroid/app/Notification;

    const v1, 0x1080078

    iget-object v2, p0, LrY;->a:Landroid/content/res/Resources;

    sget v3, Len;->upload_notification_failure_ticker:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, LrY;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 110
    iget-object v1, p0, LrY;->a:Landroid/content/Context;

    iget-object v2, p0, LrY;->a:Landroid/content/res/Resources;

    sget v3, Len;->upload_notification_failure_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LrY;->b:Ljava/lang/String;

    iget-object v4, p0, LrY;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 112
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 113
    iget-object v1, p0, LrY;->a:LPa;

    iget-object v2, p0, LrY;->c:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3, v0}, LPa;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 114
    return-void
.end method

.method public b()V
    .registers 6

    .prologue
    .line 91
    new-instance v0, Landroid/app/Notification;

    const v1, 0x1080089

    iget-object v2, p0, LrY;->a:Landroid/content/res/Resources;

    sget v3, Len;->upload_notification_success_ticker:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, LrY;->b:J

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 93
    iget-object v1, p0, LrY;->a:Landroid/content/Context;

    iget-object v2, p0, LrY;->a:Landroid/content/res/Resources;

    sget v3, Len;->upload_notification_success_title_drivev2:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LrY;->b:Ljava/lang/String;

    iget-object v4, p0, LrY;->a:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 96
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 97
    iget-object v1, p0, LrY;->a:LPa;

    iget-object v2, p0, LrY;->c:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3, v0}, LPa;->a(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 100
    iget-object v0, p0, LrY;->a:Llf;

    iget-object v1, p0, LrY;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Llf;->b(Ljava/lang/String;)LkB;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_40

    .line 102
    iget-object v1, p0, LrY;->a:LME;

    invoke-interface {v1, v0}, LME;->a(LkB;)V

    .line 104
    :cond_40
    return-void
.end method

.method public c()V
    .registers 4

    .prologue
    .line 118
    iget-object v0, p0, LrY;->a:LPa;

    iget-object v1, p0, LrY;->c:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LPa;->a(Ljava/lang/String;I)V

    .line 119
    return-void
.end method
