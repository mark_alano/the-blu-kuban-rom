.class public Lra;
.super LqT;
.source "FoldersThenTitleGrouper.java"


# static fields
.field private static final a:Lri;


# instance fields
.field private final a:Landroid/content/Context;

.field private a:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<[",
            "Lnc;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field a:Laoz;
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Lfb;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lfg;

.field private final a:Ljava/text/Collator;

.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 56
    sget-object v0, Lrr;->a:Lrr;

    sput-object v0, Lra;->a:Lri;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lfg;)V
    .registers 5
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 66
    sget v0, Len;->alphabet_set:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lra;-><init>(Landroid/content/Context;Lfg;Ljava/lang/String;Ljava/util/Locale;)V

    .line 68
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lfg;Ljava/lang/String;Ljava/util/Locale;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    sget-object v0, LPX;->a:LPX;

    invoke-virtual {v0}, LPX;->a()LPI;

    move-result-object v0

    invoke-virtual {v0}, LPI;->a()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LqU;->a:LqU;

    invoke-direct {p0, v0, v1}, LqT;-><init>(Ljava/lang/String;LqU;)V

    .line 80
    iput-object p1, p0, Lra;->a:Landroid/content/Context;

    .line 81
    iput-object p3, p0, Lra;->b:Ljava/lang/String;

    .line 84
    invoke-static {p4}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lra;->a:Ljava/text/Collator;

    .line 85
    iget-object v0, p0, Lra;->a:Ljava/text/Collator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 86
    iput-object p2, p0, Lra;->a:Lfg;

    .line 87
    return-void
.end method

.method private a(Landroid/database/Cursor;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 199
    iget-object v0, p0, Lra;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfb;

    .line 201
    invoke-static {p1}, LPW;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-static {v1}, LkO;->a(Ljava/lang/String;)LkP;

    move-result-object v1

    .line 203
    sget-object v2, LkP;->h:LkP;

    if-ne v1, v2, :cond_19

    .line 204
    invoke-direct {p0}, Lra;->d()Ljava/lang/String;

    move-result-object v0

    .line 210
    :goto_18
    return-object v0

    .line 206
    :cond_19
    sget-object v1, Lfb;->b:Lfb;

    if-ne v0, v1, :cond_26

    .line 207
    iget-object v0, p0, Lra;->a:Landroid/content/Context;

    sget v1, Len;->title_grouper_files:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_18

    .line 209
    :cond_26
    invoke-virtual {p0}, Lra;->c()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 210
    invoke-virtual {p0, v0}, Lra;->a(Ljava/lang/String;)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    goto :goto_18
.end method

.method private d()Ljava/lang/String;
    .registers 4

    .prologue
    .line 216
    iget-object v0, p0, Lra;->a:Landroid/content/Context;

    iget-object v1, p0, Lra;->a:Lfg;

    sget v2, Len;->fast_scroll_title_grouper_collections:I

    invoke-virtual {v1, v2}, Lfg;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method a(Ljava/lang/String;)C
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 144
    invoke-virtual {p0}, Lra;->a()Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/String;

    .line 146
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lra;->a:Ljava/text/Collator;

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v1

    .line 148
    if-ltz v1, :cond_1c

    .line 149
    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 156
    :goto_1b
    return v0

    .line 152
    :cond_1c
    const/4 v2, -0x1

    if-ne v1, v2, :cond_22

    .line 153
    const/16 v0, 0x20

    goto :goto_1b

    .line 156
    :cond_22
    add-int/lit8 v1, v1, 0x1

    neg-int v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    goto :goto_1b
.end method

.method public a()I
    .registers 2

    .prologue
    .line 228
    const/16 v0, 0x1a

    return v0
.end method

.method a()Landroid/util/Pair;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<[",
            "Lnc;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lra;->a:Landroid/util/Pair;

    if-nez v0, :cond_7c

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lra;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 96
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4, v1, v0}, Ljava/lang/String;->codePointCount(II)I

    move-result v0

    new-array v5, v0, [Ljava/lang/String;

    move v0, v1

    move v3, v1

    .line 99
    :goto_26
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v3, v2, :cond_43

    .line 100
    invoke-virtual {v4, v3}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v6

    .line 101
    add-int/lit8 v2, v0, 0x1

    add-int v7, v3, v6

    invoke-virtual {v4, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v0

    .line 102
    add-int v0, v3, v6

    move v3, v0

    move v0, v2

    .line 103
    goto :goto_26

    .line 105
    :cond_43
    iget-object v0, p0, Lra;->a:Ljava/text/Collator;

    invoke-static {v5, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 109
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 110
    new-instance v0, Lnc;

    invoke-direct {p0}, Lra;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const-string v6, ""

    invoke-direct {v0, v3, v4, v6}, Lnc;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    array-length v3, v5

    move v0, v1

    :goto_5e
    if-ge v0, v3, :cond_6d

    aget-object v4, v5, v0

    .line 112
    new-instance v6, Lnc;

    invoke-direct {v6, v4, v1, v4}, Lnc;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_5e

    .line 114
    :cond_6d
    new-array v0, v1, [Lnc;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lnc;

    .line 119
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v0, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v1, p0, Lra;->a:Landroid/util/Pair;

    .line 121
    :cond_7c
    iget-object v0, p0, Lra;->a:Landroid/util/Pair;

    return-object v0
.end method

.method public a()Landroid/widget/SectionIndexer;
    .registers 6

    .prologue
    .line 222
    new-instance v1, Lna;

    iget-object v2, p0, Lra;->a:Landroid/database/Cursor;

    iget-object v3, p0, Lra;->a:Ljava/text/Collator;

    invoke-virtual {p0}, Lra;->c()I

    move-result v4

    invoke-virtual {p0}, Lra;->a()Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [Lnc;

    invoke-direct {v1, v2, v3, v4, v0}, Lna;-><init>(Landroid/database/Cursor;Ljava/text/Collator;I[Lnc;)V

    return-object v1
.end method

.method public a()Lri;
    .registers 4

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 164
    iget-object v1, p0, Lra;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v1

    if-eqz v1, :cond_23

    .line 165
    iget-object v0, p0, Lra;->a:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lra;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 166
    iget-object v1, p0, Lra;->a:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 173
    :goto_14
    iget-object v1, p0, Lra;->a:Landroid/database/Cursor;

    invoke-direct {p0, v1}, Lra;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 177
    sget-object v0, Lra;->a:Lri;

    .line 180
    :goto_22
    return-object v0

    .line 168
    :cond_23
    iget-object v1, p0, Lra;->a:Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_14

    .line 180
    :cond_2a
    new-instance v0, Lrb;

    invoke-direct {v0, p0, v1}, Lrb;-><init>(Lra;Ljava/lang/String;)V

    goto :goto_22
.end method

.method public b()I
    .registers 2

    .prologue
    .line 233
    const/16 v0, 0x12c

    return v0
.end method

.method protected b()Ljava/lang/String;
    .registers 3

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LPX;->p:LPX;

    invoke-virtual {v1}, LPX;->a()LPI;

    move-result-object v1

    invoke-virtual {v1}, LPI;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <> \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LkP;->h:LkP;

    invoke-virtual {v1}, LkP;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "upper(trim("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lra;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")) COLLATE LOCALIZED,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " trim("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lra;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") COLLATE LOCALIZED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
