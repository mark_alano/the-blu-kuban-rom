.class public final Lrc;
.super LYY;
.source "GellyInjectorStore.java"


# instance fields
.field private a:LYD;

.field public a:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "Lra;",
            ">;"
        }
    .end annotation
.end field

.field public b:LZb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LZb",
            "<",
            "LqS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LYD;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1}, LYY;-><init>(LYL;)V

    .line 32
    iput-object p1, p0, Lrc;->a:LYD;

    .line 33
    const-class v0, Lra;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lrc;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lrc;->a:LZb;

    .line 36
    const-class v0, LqS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    invoke-static {v0, v1}, Lrc;->a(Laop;Ljava/lang/Class;)LZb;

    move-result-object v0

    iput-object v0, p0, Lrc;->b:LZb;

    .line 39
    return-void
.end method

.method static synthetic a(Lrc;)LYD;
    .registers 2
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lrc;->a:LYD;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lrc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lrc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lrc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 23
    invoke-static {p0}, Lrc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 71
    const-class v0, Lra;

    new-instance v1, Lrd;

    invoke-direct {v1, p0}, Lrd;-><init>(Lrc;)V

    invoke-virtual {p0, v0, v1}, Lrc;->a(Ljava/lang/Class;Laou;)V

    .line 79
    const-class v0, Lra;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lrc;->a:LZb;

    invoke-virtual {p0, v0, v1}, Lrc;->a(Laop;LZb;)V

    .line 80
    const-class v0, LqS;

    invoke-static {v0}, Laop;->a(Ljava/lang/Class;)Laop;

    move-result-object v0

    iget-object v1, p0, Lrc;->b:LZb;

    invoke-virtual {p0, v0, v1}, Lrc;->a(Laop;LZb;)V

    .line 81
    iget-object v0, p0, Lrc;->a:LZb;

    new-instance v1, Lre;

    invoke-direct {v1, p0}, Lre;-><init>(Lrc;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 102
    iget-object v0, p0, Lrc;->b:LZb;

    new-instance v1, Lrf;

    invoke-direct {v1, p0}, Lrf;-><init>(Lrc;)V

    invoke-virtual {v0, v1}, LZb;->a(Laoz;)V

    .line 121
    return-void
.end method

.method public a(Lra;)V
    .registers 3
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lrc;->a:LYD;

    iget-object v0, v0, LYD;->a:Lgr;

    iget-object v0, v0, Lgr;->g:LZb;

    invoke-static {v0}, Lrc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iput-object v0, p1, Lra;->a:Laoz;

    .line 51
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, Lrc;->a:LYD;

    iget-object v0, v0, LYD;->a:Lrc;

    invoke-virtual {v0}, Lrc;->c()V

    .line 127
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 54
    iget-object v0, p0, Lrc;->a:LYD;

    iget-object v0, v0, LYD;->a:Lrc;

    iget-object v0, v0, Lrc;->a:LZb;

    invoke-static {v0}, Lrc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laoz;

    iget-object v1, p0, Lrc;->a:LYD;

    iget-object v1, v1, LYD;->a:Lrc;

    iget-object v1, v1, Lrc;->b:LZb;

    invoke-static {v1}, Lrc;->aN(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laoz;

    invoke-static {v0, v1}, LqV;->injectFactories(Laoz;Laoz;)V

    .line 66
    return-void
.end method
