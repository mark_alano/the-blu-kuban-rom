.class public abstract Lrh;
.super Landroid/database/DataSetObserver;
.source "GenericSectionIndexer.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/database/DataSetObserver;",
        "Landroid/widget/SectionIndexer;"
    }
.end annotation


# instance fields
.field protected a:I

.field protected a:Landroid/database/Cursor;

.field private final a:Landroid/util/SparseIntArray;

.field private final a:Z

.field private final a:[Lrg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lrg",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;I[Lrg;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "I[",
            "Lrg",
            "<TT;>;Z)V"
        }
    .end annotation

    .prologue
    .line 99
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 100
    iput-boolean p4, p0, Lrh;->a:Z

    .line 101
    iput-object p1, p0, Lrh;->a:Landroid/database/Cursor;

    .line 102
    iput p2, p0, Lrh;->a:I

    .line 104
    array-length v0, p3

    iput v0, p0, Lrh;->b:I

    .line 105
    iput-object p3, p0, Lrh;->a:[Lrg;

    .line 109
    new-instance v0, Landroid/util/SparseIntArray;

    iget v1, p0, Lrh;->b:I

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lrh;->a:Landroid/util/SparseIntArray;

    .line 110
    if-eqz p1, :cond_1c

    .line 111
    invoke-interface {p1, p0}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 116
    :cond_1c
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)I"
        }
    .end annotation

    .prologue
    .line 156
    invoke-virtual {p0}, Lrh;->a()Ljava/util/Comparator;

    move-result-object v0

    .line 157
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 158
    iget-boolean v1, p0, Lrh;->a:Z

    if-eqz v1, :cond_d

    neg-int v0, v0

    :cond_d
    return v0
.end method

.method protected abstract a(Landroid/database/Cursor;I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "I)TT;"
        }
    .end annotation
.end method

.method protected abstract a()Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<TT;>;"
        }
    .end annotation
.end method

.method public getPositionForSection(I)I
    .registers 12
    .parameter

    .prologue
    const/4 v0, 0x0

    const/high16 v8, -0x8000

    .line 186
    iget-object v4, p0, Lrh;->a:Landroid/util/SparseIntArray;

    .line 187
    iget-object v5, p0, Lrh;->a:Landroid/database/Cursor;

    .line 189
    if-eqz v5, :cond_d

    iget-object v1, p0, Lrh;->a:[Lrg;

    if-nez v1, :cond_e

    .line 290
    :cond_d
    :goto_d
    return v0

    .line 194
    :cond_e
    if-lez p1, :cond_d

    .line 197
    iget v1, p0, Lrh;->b:I

    if-lt p1, v1, :cond_18

    .line 198
    iget v1, p0, Lrh;->b:I

    add-int/lit8 p1, v1, -0x1

    .line 201
    :cond_18
    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    .line 203
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 211
    iget-object v1, p0, Lrh;->a:[Lrg;

    aget-object v1, v1, p1

    invoke-interface {v1}, Lrg;->a()Ljava/lang/Object;

    move-result-object v7

    .line 214
    invoke-virtual {v4, p1, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    if-eq v8, v1, :cond_7f

    .line 218
    if-gez v1, :cond_5d

    .line 219
    neg-int v2, v1

    .line 228
    :goto_31
    if-lez p1, :cond_3f

    .line 230
    add-int/lit8 v1, p1, -0x1

    .line 231
    invoke-virtual {v4, v1, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    .line 232
    if-eq v1, v8, :cond_3f

    .line 233
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 239
    :cond_3f
    add-int v1, v2, v0

    div-int/lit8 v1, v1, 0x2

    move v9, v2

    move v2, v0

    move v0, v9

    .line 241
    :goto_46
    if-ge v1, v0, :cond_55

    .line 243
    invoke-interface {v5, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 245
    iget v8, p0, Lrh;->a:I

    invoke-virtual {p0, v5, v8}, Lrh;->a(Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v8

    .line 246
    if-nez v8, :cond_62

    .line 247
    if-nez v1, :cond_5f

    .line 288
    :cond_55
    :goto_55
    invoke-virtual {v4, p1, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 289
    invoke-interface {v5, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    move v0, v1

    .line 290
    goto :goto_d

    :cond_5d
    move v0, v1

    .line 223
    goto :goto_d

    .line 250
    :cond_5f
    add-int/lit8 v1, v1, -0x1

    .line 251
    goto :goto_46

    .line 254
    :cond_62
    invoke-virtual {p0, v8, v7}, Lrh;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v8

    .line 255
    if-eqz v8, :cond_7a

    .line 267
    if-gez v8, :cond_70

    .line 268
    add-int/lit8 v1, v1, 0x1

    .line 269
    if-lt v1, v3, :cond_72

    move v1, v3

    .line 271
    goto :goto_55

    :cond_70
    move v0, v1

    move v1, v2

    .line 286
    :cond_72
    :goto_72
    add-int v2, v1, v0

    div-int/lit8 v2, v2, 0x2

    move v9, v2

    move v2, v1

    move v1, v9

    .line 287
    goto :goto_46

    .line 278
    :cond_7a
    if-eq v2, v1, :cond_55

    move v0, v1

    move v1, v2

    .line 283
    goto :goto_72

    :cond_7f
    move v2, v3

    goto :goto_31
.end method

.method public getSectionForPosition(I)I
    .registers 5
    .parameter

    .prologue
    .line 299
    iget-object v0, p0, Lrh;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 300
    iget-object v1, p0, Lrh;->a:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 302
    iget-object v1, p0, Lrh;->a:Landroid/database/Cursor;

    iget v2, p0, Lrh;->a:I

    invoke-virtual {p0, v1, v2}, Lrh;->a(Landroid/database/Cursor;I)Ljava/lang/Object;

    move-result-object v1

    .line 303
    iget-object v2, p0, Lrh;->a:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 306
    iget v0, p0, Lrh;->b:I

    add-int/lit8 v0, v0, -0x1

    :goto_1c
    if-ltz v0, :cond_30

    .line 309
    iget-object v2, p0, Lrh;->a:[Lrg;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lrg;->a()Ljava/lang/Object;

    move-result-object v2

    .line 310
    invoke-virtual {p0, v2, v1}, Lrh;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gtz v2, :cond_2d

    .line 314
    :goto_2c
    return v0

    .line 306
    :cond_2d
    add-int/lit8 v0, v0, -0x1

    goto :goto_1c

    .line 314
    :cond_30
    const/4 v0, 0x0

    goto :goto_2c
.end method

.method public getSections()[Ljava/lang/Object;
    .registers 2

    .prologue
    .line 134
    iget-object v0, p0, Lrh;->a:[Lrg;

    return-object v0
.end method

.method public onChanged()V
    .registers 2

    .prologue
    .line 322
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 323
    iget-object v0, p0, Lrh;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 324
    return-void
.end method

.method public onInvalidated()V
    .registers 2

    .prologue
    .line 331
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    .line 332
    iget-object v0, p0, Lrh;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 333
    return-void
.end method
