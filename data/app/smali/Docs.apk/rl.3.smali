.class public abstract enum Lrl;
.super Ljava/lang/Enum;
.source "SortKind.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lrl;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lrl;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum a:Lrl;

.field private static final synthetic a:[Lrl;

.field public static final enum b:Lrl;

.field public static final enum c:Lrl;

.field public static final enum d:Lrl;

.field public static final enum e:Lrl;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lrm;

    const-string v1, "FOLDERS_THEN_TITLE"

    sget v2, Len;->menu_sort_title:I

    invoke-direct {v0, v1, v3, v2}, Lrm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrl;->a:Lrl;

    .line 27
    new-instance v0, Lrn;

    const-string v1, "LAST_MODIFIED"

    sget v2, Len;->menu_sort_last_modified:I

    invoke-direct {v0, v1, v4, v2}, Lrn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrl;->b:Lrl;

    .line 34
    new-instance v0, Lro;

    const-string v1, "OPENED_BY_ME_DATE"

    sget v2, Len;->menu_sort_recently_opened:I

    invoke-direct {v0, v1, v5, v2}, Lro;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrl;->c:Lrl;

    .line 41
    new-instance v0, Lrp;

    const-string v1, "EDITED_BY_ME_DATE"

    sget v2, Len;->menu_sort_recently_edited:I

    invoke-direct {v0, v1, v6, v2}, Lrp;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrl;->d:Lrl;

    .line 48
    new-instance v0, Lrq;

    const-string v1, "SHARED_WITH_ME_DATE"

    sget v2, Len;->menu_shared_with_me:I

    invoke-direct {v0, v1, v7, v2}, Lrq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lrl;->e:Lrl;

    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [Lrl;

    sget-object v1, Lrl;->a:Lrl;

    aput-object v1, v0, v3

    sget-object v1, Lrl;->b:Lrl;

    aput-object v1, v0, v4

    sget-object v1, Lrl;->c:Lrl;

    aput-object v1, v0, v5

    sget-object v1, Lrl;->d:Lrl;

    aput-object v1, v0, v6

    sget-object v1, Lrl;->e:Lrl;

    aput-object v1, v0, v7

    sput-object v0, Lrl;->a:[Lrl;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lrl;->a:Ljava/util/Map;

    .line 59
    const-class v0, Lrl;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_66
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrl;

    .line 60
    sget-object v2, Lrl;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lrl;->name()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_66

    .line 62
    :cond_7c
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 67
    iput p3, p0, Lrl;->a:I

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILrm;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lrl;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lrl;
    .registers 2
    .parameter

    .prologue
    .line 20
    const-class v0, Lrl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lrl;

    return-object v0
.end method

.method public static values()[Lrl;
    .registers 1

    .prologue
    .line 20
    sget-object v0, Lrl;->a:[Lrl;

    invoke-virtual {v0}, [Lrl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lrl;

    return-object v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 71
    iget v0, p0, Lrl;->a:I

    return v0
.end method

.method public abstract a()LqT;
.end method
