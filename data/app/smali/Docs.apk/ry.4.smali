.class public Lry;
.super Ljava/lang/Object;
.source "DocsUploaderImpl.java"

# interfaces
.implements Lrx;


# instance fields
.field private final a:LKS;

.field private final a:LNj;

.field private final a:LrU;

.field a:LrV;


# direct methods
.method public constructor <init>(LNj;LrU;LKS;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .prologue
    .line 538
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 539
    iput-object p1, p0, Lry;->a:LNj;

    .line 540
    iput-object p2, p0, Lry;->a:LrU;

    .line 541
    iput-object p3, p0, Lry;->a:LKS;

    .line 542
    return-void
.end method

.method static synthetic a(Lry;)LKS;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lry;->a:LKS;

    return-object v0
.end method

.method static synthetic a(Lry;)LNj;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lry;->a:LNj;

    return-object v0
.end method

.method private a(Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 553
    :try_start_0
    iget-object v0, p0, Lry;->a:LNj;

    invoke-interface {v0, p1, p2}, LNj;->a(Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_5
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_5} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_5} :catch_29
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_5} :catch_4b
    .catch LNt; {:try_start_0 .. :try_end_5} :catch_6d

    move-result-object v0

    return-object v0

    .line 554
    :catch_7
    move-exception v0

    .line 555
    new-instance v1, LrT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Client protocol error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 556
    :catch_29
    move-exception v0

    .line 557
    new-instance v1, LrW;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error in transmission: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LrW;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 559
    :catch_4b
    move-exception v0

    .line 560
    new-instance v1, LrT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Authentication problem: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 561
    :catch_6d
    move-exception v0

    .line 562
    new-instance v1, LrT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Authentication problem: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Lry;Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lry;->a(Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lry;)LrU;
    .registers 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lry;->a:LrU;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lsm;Lsn;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 579
    new-instance v0, Lrz;

    invoke-direct {v0, p0, p1, p2, p3}, Lrz;-><init>(Lry;Ljava/lang/String;Lsm;Lsn;)V

    iput-object v0, p0, Lry;->a:LrV;

    .line 580
    iget-object v0, p0, Lry;->a:LrV;

    invoke-interface {v0}, LrV;->a()V

    .line 581
    return-void
.end method
