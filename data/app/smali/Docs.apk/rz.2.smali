.class public Lrz;
.super Ljava/lang/Object;
.source "DocsUploaderImpl.java"

# interfaces
.implements LrV;


# instance fields
.field private final a:I

.field private a:J

.field private final a:LZO;

.field private a:Ljava/io/RandomAccessFile;

.field private final a:Ljava/lang/String;

.field final a:Ljava/util/regex/Pattern;

.field a:LrK;

.field final synthetic a:Lry;

.field private final a:Lsm;

.field private final a:Lsn;


# direct methods
.method constructor <init>(Lry;Ljava/lang/String;Lsm;Lsn;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 131
    iput-object p1, p0, Lrz;->a:Lry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    const-string v0, "(?:bytes? *=? *)?(\\d+)-(-?\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lrz;->a:Ljava/util/regex/Pattern;

    .line 132
    iput-object p2, p0, Lrz;->a:Ljava/lang/String;

    .line 133
    iput-object p3, p0, Lrz;->a:Lsm;

    .line 134
    invoke-virtual {p3}, Lsm;->a()LrK;

    move-result-object v0

    iput-object v0, p0, Lrz;->a:LrK;

    .line 135
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lrz;->a:J

    .line 136
    iput-object p4, p0, Lrz;->a:Lsn;

    .line 141
    invoke-static {p1}, Lry;->a(Lry;)LKS;

    move-result-object v1

    monitor-enter v1

    .line 142
    :try_start_22
    invoke-static {p1}, Lry;->a(Lry;)LKS;

    move-result-object v0

    const-string v2, "maxUploadChunkRetryCount1"

    const/4 v3, 0x4

    invoke-interface {v0, v2, v3}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lrz;->a:I

    .line 144
    const-wide v2, 0x408f400000000000L

    invoke-static {p1}, Lry;->a(Lry;)LKS;

    move-result-object v0

    const-string v4, "initialChunkBackOff"

    const-wide/high16 v5, 0x3ff0

    invoke-interface {v0, v4, v5, v6}, LKS;->a(Ljava/lang/String;D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-long v2, v2

    .line 146
    invoke-static {p1}, Lry;->a(Lry;)LKS;

    move-result-object v0

    const-string v4, "chunkBackoffGrowthFactor"

    const-wide/high16 v5, 0x4000

    invoke-interface {v0, v4, v5, v6}, LKS;->a(Ljava/lang/String;D)D

    move-result-wide v4

    .line 148
    new-instance v0, LZO;

    invoke-direct {v0, v2, v3, v4, v5}, LZO;-><init>(JD)V

    iput-object v0, p0, Lrz;->a:LZO;

    .line 150
    monitor-exit v1

    .line 151
    return-void

    .line 150
    :catchall_57
    move-exception v0

    monitor-exit v1
    :try_end_59
    .catchall {:try_start_22 .. :try_end_59} :catchall_57

    throw v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter

    .prologue
    .line 515
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 517
    :try_start_b
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_13
    if-ge v0, v3, :cond_31

    aget-byte v4, v2, v0

    .line 518
    const-string v5, "%"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 519
    and-int/lit16 v4, v4, 0xff

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_25
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_b .. :try_end_25} :catch_28

    .line 517
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 521
    :catch_28
    move-exception v0

    .line 522
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "UTF-8 should always be supported"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 524
    :cond_31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 525
    return-object v0
.end method

.method private a(Ljava/lang/String;)Lrt;
    .registers 8
    .parameter

    .prologue
    .line 502
    iget-object v0, p0, Lrz;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 503
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_14

    .line 504
    new-instance v0, LrT;

    const-string v1, "Unable to upload file: invalid byte range returned by server."

    invoke-direct {v0, v1}, LrT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 507
    :cond_14
    new-instance v1, Lrt;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lrt;-><init>(JJ)V

    return-object v1
.end method

.method private a()Z
    .registers 3

    .prologue
    .line 314
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lrz;->a:J

    .line 315
    invoke-direct {p0}, Lrz;->c()Z

    move-result v0

    .line 316
    return v0
.end method

.method private a(Lorg/apache/http/client/methods/HttpUriRequest;)Z
    .registers 7
    .parameter

    .prologue
    .line 352
    iget-object v0, p0, Lrz;->a:Lry;

    iget-object v1, p0, Lrz;->a:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lry;->a(Lry;Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 354
    :try_start_8
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 355
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 356
    iget-object v3, p0, Lrz;->a:Lry;

    invoke-static {v3}, Lry;->a(Lry;)LNj;

    move-result-object v3

    invoke-interface {v3}, LNj;->a()V

    .line 357
    sparse-switch v2, :sswitch_data_86

    .line 377
    new-instance v0, LrT;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to upload item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to upload "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lrz;->a:LrK;

    invoke-virtual {v2}, LrK;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LrT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_53
    .catchall {:try_start_8 .. :try_end_53} :catchall_53

    .line 382
    :catchall_53
    move-exception v0

    iget-object v1, p0, Lrz;->a:Lry;

    invoke-static {v1}, Lry;->a(Lry;)LNj;

    move-result-object v1

    invoke-interface {v1}, LNj;->b()V

    throw v0

    .line 361
    :sswitch_5e
    :try_start_5e
    const-string v0, "DocsUploaderImpl"

    const-string v1, "Server reports upload complete."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_65
    .catchall {:try_start_5e .. :try_end_65} :catchall_53

    .line 364
    const/4 v0, 0x1

    .line 382
    iget-object v1, p0, Lrz;->a:Lry;

    invoke-static {v1}, Lry;->a(Lry;)LNj;

    move-result-object v1

    invoke-interface {v1}, LNj;->b()V

    :goto_6f
    return v0

    .line 368
    :sswitch_70
    :try_start_70
    invoke-virtual {p0, v0}, Lrz;->a(Lorg/apache/http/HttpResponse;)V
    :try_end_73
    .catchall {:try_start_70 .. :try_end_73} :catchall_53

    .line 369
    const/4 v0, 0x0

    .line 382
    iget-object v1, p0, Lrz;->a:Lry;

    invoke-static {v1}, Lry;->a(Lry;)LNj;

    move-result-object v1

    invoke-interface {v1}, LNj;->b()V

    goto :goto_6f

    .line 373
    :sswitch_7e
    :try_start_7e
    new-instance v0, LrW;

    const-string v1, "GData Service unavailable."

    invoke-direct {v0, v1}, LrW;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_86
    .catchall {:try_start_7e .. :try_end_86} :catchall_53

    .line 357
    :sswitch_data_86
    .sparse-switch
        0xc9 -> :sswitch_5e
        0x134 -> :sswitch_70
        0x1f7 -> :sswitch_7e
    .end sparse-switch
.end method

.method private b()V
    .registers 2

    .prologue
    .line 251
    iget-object v0, p0, Lrz;->a:Lsm;

    invoke-virtual {v0}, Lsm;->b()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 252
    new-instance v0, Lrw;

    invoke-direct {v0}, Lrw;-><init>()V

    throw v0

    .line 254
    :cond_e
    return-void
.end method

.method private b()Z
    .registers 2

    .prologue
    .line 327
    invoke-virtual {p0}, Lrz;->a()Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 328
    invoke-direct {p0, v0}, Lrz;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Z

    move-result v0

    return v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 257
    iget-object v0, p0, Lrz;->a:LZO;

    invoke-virtual {v0}, LZO;->a()I

    move-result v0

    iget v1, p0, Lrz;->a:I

    if-gt v0, v1, :cond_1b

    const/4 v0, 0x1

    :goto_b
    const-string v1, "backoff called after too many failures."

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 260
    :try_start_10
    iget-object v0, p0, Lrz;->a:LZO;

    invoke-virtual {v0}, LZO;->b()V
    :try_end_15
    .catchall {:try_start_10 .. :try_end_15} :catchall_2b
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_15} :catch_1d

    .line 264
    iget-object v0, p0, Lrz;->a:LZO;

    invoke-virtual {v0}, LZO;->c()V

    .line 266
    :goto_1a
    return-void

    .line 257
    :cond_1b
    const/4 v0, 0x0

    goto :goto_b

    .line 261
    :catch_1d
    move-exception v0

    .line 262
    :try_start_1e
    const-string v0, "DocsUploaderImpl"

    const-string v1, "Backoff sleep interrupted."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_25
    .catchall {:try_start_1e .. :try_end_25} :catchall_2b

    .line 264
    iget-object v0, p0, Lrz;->a:LZO;

    invoke-virtual {v0}, LZO;->c()V

    goto :goto_1a

    :catchall_2b
    move-exception v0

    iget-object v1, p0, Lrz;->a:LZO;

    invoke-virtual {v1}, LZO;->c()V

    throw v0
.end method

.method private c()Z
    .registers 2

    .prologue
    .line 340
    invoke-virtual {p0}, Lrz;->b()Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 341
    invoke-direct {p0, v0}, Lrz;->a(Lorg/apache/http/client/methods/HttpUriRequest;)Z

    move-result v0

    return v0
.end method

.method private d()V
    .registers 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 281
    iget-object v2, p0, Lrz;->a:LrK;

    invoke-virtual {v2}, LrK;->a()Z

    move-result v3

    .line 282
    if-eqz v3, :cond_1d

    iget-object v2, p0, Lrz;->a:LrK;

    invoke-virtual {v2}, LrK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    const-string v4, "image/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    move v0, v1

    .line 283
    :cond_1d
    const-string v2, "https://docs.google.com/feeds/upload/create-session/default/private/full"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 284
    iget-object v4, p0, Lrz;->a:LrK;

    invoke-virtual {v4}, LrK;->d()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_39

    .line 285
    iget-object v4, p0, Lrz;->a:LrK;

    invoke-virtual {v4}, LrK;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, LWB;->a(Ljava/lang/String;Z)LWB;

    move-result-object v4

    invoke-virtual {v4, v2}, LWB;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 289
    :cond_39
    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "convert"

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "ocr"

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 291
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lrz;->a(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 294
    :try_start_5d
    iget-object v2, p0, Lrz;->a:Lry;

    iget-object v3, p0, Lrz;->a:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lry;->a(Lry;Ljava/lang/String;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 295
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    .line 296
    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 297
    const/16 v4, 0xc8

    if-eq v3, v4, :cond_bc

    .line 298
    new-instance v0, LrT;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to upload item: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to upload "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lrz;->a:LrK;

    invoke-virtual {v3}, LrK;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LrT;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a8
    .catchall {:try_start_5d .. :try_end_a8} :catchall_a8

    .line 308
    :catchall_a8
    move-exception v0

    iget-object v1, p0, Lrz;->a:Lry;

    invoke-static {v1}, Lry;->a(Lry;)LNj;

    move-result-object v1

    invoke-interface {v1}, LNj;->a()V

    .line 309
    iget-object v1, p0, Lrz;->a:Lry;

    invoke-static {v1}, Lry;->a(Lry;)LNj;

    move-result-object v1

    invoke-interface {v1}, LNj;->b()V

    throw v0

    .line 302
    :cond_bc
    :try_start_bc
    const-string v2, "Location"

    invoke-interface {v0, v2}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v0

    .line 303
    if-eqz v0, :cond_c7

    array-length v2, v0

    if-eq v2, v1, :cond_cf

    .line 304
    :cond_c7
    new-instance v0, LrT;

    const-string v1, "Unable to upload item: Server upload URI invalid."

    invoke-direct {v0, v1}, LrT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :cond_cf
    iget-object v1, p0, Lrz;->a:Lsm;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lsm;->a(Ljava/lang/String;)V
    :try_end_db
    .catchall {:try_start_bc .. :try_end_db} :catchall_a8

    .line 308
    iget-object v0, p0, Lrz;->a:Lry;

    invoke-static {v0}, Lry;->a(Lry;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->a()V

    .line 309
    iget-object v0, p0, Lrz;->a:Lry;

    invoke-static {v0}, Lry;->a(Lry;)LNj;

    move-result-object v0

    invoke-interface {v0}, LNj;->b()V

    .line 311
    return-void
.end method


# virtual methods
.method a()Lorg/apache/http/client/methods/HttpUriRequest;
    .registers 13

    .prologue
    .line 409
    :try_start_0
    new-instance v7, Lorg/apache/http/client/methods/HttpPut;

    new-instance v0, Ljava/net/URI;

    iget-object v1, p0, Lrz;->a:Lsm;

    invoke-virtual {v1}, Lsm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v7, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/net/URI;)V
    :try_end_10
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_10} :catch_ab

    .line 414
    const-string v0, "Content-Type"

    iget-object v1, p0, Lrz;->a:LrK;

    invoke-virtual {v1}, LrK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string v0, "GData-Version"

    const-string v1, "3.0"

    invoke-virtual {v7, v0, v1}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    const-string v0, "X-Upload-Content-Type"

    iget-object v1, p0, Lrz;->a:LrK;

    invoke-virtual {v1}, LrK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    iget-object v0, p0, Lrz;->a:LrK;

    invoke-virtual {v0}, LrK;->a()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iget-wide v2, p0, Lrz;->a:J

    sub-long/2addr v0, v2

    .line 420
    const-wide/32 v2, 0x40000

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    .line 421
    const-string v0, "Content-Range"

    const-string v1, "bytes %d-%d/%d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lrz;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lrz;->a:J

    add-long/2addr v4, v8

    const-wide/16 v10, 0x1

    sub-long/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lrz;->a:LrK;

    invoke-virtual {v4}, LrK;->a()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    :try_start_76
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v0, p0, Lrz;->a:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_81
    .catch Ljava/io/IOException; {:try_start_76 .. :try_end_81} :catch_cb

    .line 430
    new-instance v0, LYt;

    new-instance v2, LrX;

    iget-object v3, p0, Lrz;->a:Lsn;

    invoke-direct {v2, v3}, LrX;-><init>(Lsn;)V

    iget-object v3, p0, Lrz;->a:LrK;

    invoke-virtual {v3}, LrK;->a()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    long-to-int v3, v3

    int-to-long v3, v3

    iget-wide v5, p0, Lrz;->a:J

    long-to-int v5, v5

    int-to-long v5, v5

    invoke-direct/range {v0 .. v6}, LYt;-><init>(Ljava/io/InputStream;LXM;JJ)V

    .line 433
    iget-wide v1, p0, Lrz;->a:J

    add-long/2addr v1, v8

    iput-wide v1, p0, Lrz;->a:J

    .line 434
    new-instance v1, Lorg/apache/http/entity/InputStreamEntity;

    invoke-direct {v1, v0, v8, v9}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 435
    invoke-virtual {v7, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 437
    return-object v7

    .line 410
    :catch_ab
    move-exception v0

    .line 411
    new-instance v1, LrT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lrz;->a:Lsm;

    invoke-virtual {v3}, Lsm;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 427
    :catch_cb
    move-exception v0

    .line 428
    new-instance v1, LrT;

    const-string v2, "Unable to read input file"

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method a(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .registers 6
    .parameter

    .prologue
    .line 389
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V
    :try_end_a
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_a} :catch_4a

    .line 394
    const-string v1, "GData-Version"

    const-string v2, "3.0"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    iget-object v1, p0, Lrz;->a:LrK;

    invoke-virtual {v1}, LrK;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lrz;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 396
    const-string v2, "Slug"

    invoke-virtual {v0, v2, v1}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const-string v1, "Content-Type"

    iget-object v2, p0, Lrz;->a:LrK;

    invoke-virtual {v2}, LrK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    const-string v1, "X-Upload-Content-Type"

    iget-object v2, p0, Lrz;->a:LrK;

    invoke-virtual {v2}, LrK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 400
    const-string v1, "X-Upload-Content-Length"

    iget-object v2, p0, Lrz;->a:LrK;

    invoke-virtual {v2}, LrK;->a()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    return-object v0

    .line 390
    :catch_4a
    move-exception v0

    .line 391
    new-instance v1, LrT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a()V
    .registers 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 156
    :try_start_2
    const-string v0, "DocsUploaderImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Uploading as "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lrz;->a:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lrz;->a:LrK;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v0, p0, Lrz;->a:LrK;

    invoke-virtual {v0}, LrK;->a()LrK;

    move-result-object v0

    iput-object v0, p0, Lrz;->a:LrK;

    .line 161
    invoke-direct {p0}, Lrz;->b()V

    .line 163
    iget-object v0, p0, Lrz;->a:Lry;

    invoke-static {v0}, Lry;->a(Lry;)LrU;

    move-result-object v0

    iget-object v1, p0, Lrz;->a:LrK;

    invoke-interface {v0, v1}, LrU;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LrK;

    iput-object v0, p0, Lrz;->a:LrK;
    :try_end_43
    .catchall {:try_start_2 .. :try_end_43} :catchall_164
    .catch Lrw; {:try_start_2 .. :try_end_43} :catch_c8
    .catch LrT; {:try_start_2 .. :try_end_43} :catch_122

    .line 166
    :try_start_43
    new-instance v0, Ljava/io/RandomAccessFile;

    iget-object v1, p0, Lrz;->a:LrK;

    invoke-virtual {v1}, LrK;->a()Ljava/io/File;

    move-result-object v1

    const-string v4, "r"

    invoke-direct {v0, v1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lrz;->a:Ljava/io/RandomAccessFile;
    :try_end_52
    .catchall {:try_start_43 .. :try_end_52} :catchall_164
    .catch Ljava/io/IOException; {:try_start_43 .. :try_end_52} :catch_bf
    .catch Lrw; {:try_start_43 .. :try_end_52} :catch_c8
    .catch LrT; {:try_start_43 .. :try_end_52} :catch_122

    .line 170
    :try_start_52
    iget-object v0, p0, Lrz;->a:Lsn;

    invoke-interface {v0}, Lsn;->a()V

    .line 177
    iget-object v0, p0, Lrz;->a:Lsm;

    invoke-virtual {v0}, Lsm;->a()Ljava/lang/String;
    :try_end_5c
    .catchall {:try_start_52 .. :try_end_5c} :catchall_164
    .catch Lrw; {:try_start_52 .. :try_end_5c} :catch_c8
    .catch LrT; {:try_start_52 .. :try_end_5c} :catch_122

    move-result-object v0

    if-eqz v0, :cond_18b

    .line 179
    :try_start_5f
    invoke-direct {p0}, Lrz;->a()Z
    :try_end_62
    .catchall {:try_start_5f .. :try_end_62} :catchall_164
    .catch LrT; {:try_start_5f .. :try_end_62} :catch_d4
    .catch Lrw; {:try_start_5f .. :try_end_62} :catch_c8

    move-result v1

    .line 180
    :try_start_63
    const-string v0, "DocsUploaderImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Upload resuming - complete="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bytesSent="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lrz;->a:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_87
    .catchall {:try_start_63 .. :try_end_87} :catchall_164
    .catch LrT; {:try_start_63 .. :try_end_87} :catch_188
    .catch Lrw; {:try_start_63 .. :try_end_87} :catch_c8

    move v0, v3

    .line 189
    :goto_88
    if-nez v0, :cond_94

    .line 190
    :try_start_8a
    const-string v0, "DocsUploaderImpl"

    const-string v4, "Beginning resumable upload"

    invoke-static {v0, v4}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-direct {p0}, Lrz;->d()V

    .line 195
    :cond_94
    iget-object v0, p0, Lrz;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V

    .line 196
    :goto_99
    if-nez v1, :cond_16e

    .line 197
    invoke-direct {p0}, Lrz;->b()V
    :try_end_9e
    .catchall {:try_start_8a .. :try_end_9e} :catchall_164
    .catch Lrw; {:try_start_8a .. :try_end_9e} :catch_c8
    .catch LrT; {:try_start_8a .. :try_end_9e} :catch_122

    .line 201
    :try_start_9e
    invoke-direct {p0}, Lrz;->b()Z
    :try_end_a1
    .catchall {:try_start_9e .. :try_end_a1} :catchall_164
    .catch LrW; {:try_start_9e .. :try_end_a1} :catch_f0
    .catch Lrw; {:try_start_9e .. :try_end_a1} :catch_c8
    .catch LrT; {:try_start_9e .. :try_end_a1} :catch_122

    move-result v1

    .line 203
    :try_start_a2
    iget-object v0, p0, Lrz;->a:LZO;

    invoke-virtual {v0}, LZO;->a()V
    :try_end_a7
    .catchall {:try_start_a2 .. :try_end_a7} :catchall_164
    .catch LrW; {:try_start_a2 .. :try_end_a7} :catch_183
    .catch Lrw; {:try_start_a2 .. :try_end_a7} :catch_c8
    .catch LrT; {:try_start_a2 .. :try_end_a7} :catch_122

    move v4, v1

    move v1, v2

    .line 212
    :cond_a9
    :goto_a9
    if-eqz v1, :cond_16b

    .line 213
    :try_start_ab
    invoke-direct {p0}, Lrz;->b()V

    .line 215
    const-wide/16 v5, -0x1

    iput-wide v5, p0, Lrz;->a:J

    .line 216
    invoke-direct {p0}, Lrz;->c()V

    .line 218
    invoke-direct {p0}, Lrz;->b()V
    :try_end_b8
    .catchall {:try_start_ab .. :try_end_b8} :catchall_164
    .catch Lrw; {:try_start_ab .. :try_end_b8} :catch_c8
    .catch LrT; {:try_start_ab .. :try_end_b8} :catch_122

    .line 220
    :try_start_b8
    invoke-direct {p0}, Lrz;->c()Z
    :try_end_bb
    .catchall {:try_start_b8 .. :try_end_bb} :catchall_164
    .catch LrW; {:try_start_b8 .. :try_end_bb} :catch_134
    .catch Lrw; {:try_start_b8 .. :try_end_bb} :catch_c8
    .catch LrT; {:try_start_b8 .. :try_end_bb} :catch_122

    move-result v1

    move v4, v1

    move v1, v2

    .line 228
    goto :goto_a9

    .line 167
    :catch_bf
    move-exception v0

    .line 168
    :try_start_c0
    new-instance v1, LrT;

    const-string v2, "Unable to upload file: "

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_c8
    .catchall {:try_start_c0 .. :try_end_c8} :catchall_164
    .catch Lrw; {:try_start_c0 .. :try_end_c8} :catch_c8
    .catch LrT; {:try_start_c0 .. :try_end_c8} :catch_122

    .line 236
    :catch_c8
    move-exception v0

    .line 237
    :try_start_c9
    iget-object v0, p0, Lrz;->a:Lsn;

    invoke-interface {v0}, Lsn;->c()V
    :try_end_ce
    .catchall {:try_start_c9 .. :try_end_ce} :catchall_164

    .line 243
    iget-object v0, p0, Lrz;->a:LrK;

    invoke-virtual {v0}, LrK;->a()V

    .line 245
    :goto_d3
    return-void

    .line 183
    :catch_d4
    move-exception v0

    move v1, v2

    .line 185
    :goto_d6
    :try_start_d6
    const-string v4, "DocsUploaderImpl"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Upload resumption failed, will begin again: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    goto :goto_88

    .line 204
    :catch_f0
    move-exception v0

    move v4, v1

    move v1, v3

    .line 205
    :goto_f3
    const-string v5, "DocsUploaderImpl"

    const-string v6, "Retrying chunk - failures %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lrz;->a:LZO;

    invoke-virtual {v9}, LZO;->a()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v5, p0, Lrz;->a:LZO;

    invoke-virtual {v5}, LZO;->a()I

    move-result v5

    iget v6, p0, Lrz;->a:I

    if-lt v5, v6, :cond_a9

    .line 208
    new-instance v1, LrT;

    const-string v2, "Error uploading after multiple retries."

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_122
    .catchall {:try_start_d6 .. :try_end_122} :catchall_164
    .catch Lrw; {:try_start_d6 .. :try_end_122} :catch_c8
    .catch LrT; {:try_start_d6 .. :try_end_122} :catch_122

    .line 238
    :catch_122
    move-exception v0

    .line 239
    :try_start_123
    iget-object v1, p0, Lrz;->a:Lsm;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lsm;->a(Z)V

    .line 240
    iget-object v1, p0, Lrz;->a:Lsn;

    invoke-interface {v1, v0}, Lsn;->a(LrT;)V
    :try_end_12e
    .catchall {:try_start_123 .. :try_end_12e} :catchall_164

    .line 243
    iget-object v0, p0, Lrz;->a:LrK;

    invoke-virtual {v0}, LrK;->a()V

    goto :goto_d3

    .line 222
    :catch_134
    move-exception v0

    .line 223
    :try_start_135
    const-string v5, "DocsUploaderImpl"

    const-string v6, "Upload status request failed - failures %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lrz;->a:LZO;

    invoke-virtual {v9}, LZO;->a()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iget-object v5, p0, Lrz;->a:LZO;

    invoke-virtual {v5}, LZO;->a()I

    move-result v5

    iget v6, p0, Lrz;->a:I

    if-lt v5, v6, :cond_a9

    .line 226
    new-instance v1, LrT;

    const-string v2, "Error uploading after multiple retries."

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_164
    .catchall {:try_start_135 .. :try_end_164} :catchall_164
    .catch Lrw; {:try_start_135 .. :try_end_164} :catch_c8
    .catch LrT; {:try_start_135 .. :try_end_164} :catch_122

    .line 243
    :catchall_164
    move-exception v0

    iget-object v1, p0, Lrz;->a:LrK;

    invoke-virtual {v1}, LrK;->a()V

    throw v0

    :cond_16b
    move v1, v4

    .line 230
    goto/16 :goto_99

    .line 232
    :cond_16e
    :try_start_16e
    invoke-direct {p0}, Lrz;->b()V

    .line 234
    iget-object v0, p0, Lrz;->a:Lsm;

    const/16 v1, 0x64

    iput v1, v0, Lsm;->a:I

    .line 235
    iget-object v0, p0, Lrz;->a:Lsn;

    invoke-interface {v0}, Lsn;->b()V
    :try_end_17c
    .catchall {:try_start_16e .. :try_end_17c} :catchall_164
    .catch Lrw; {:try_start_16e .. :try_end_17c} :catch_c8
    .catch LrT; {:try_start_16e .. :try_end_17c} :catch_122

    .line 243
    iget-object v0, p0, Lrz;->a:LrK;

    invoke-virtual {v0}, LrK;->a()V

    goto/16 :goto_d3

    .line 204
    :catch_183
    move-exception v0

    move v4, v1

    move v1, v2

    goto/16 :goto_f3

    .line 183
    :catch_188
    move-exception v0

    goto/16 :goto_d6

    :cond_18b
    move v1, v2

    move v0, v2

    goto/16 :goto_88
.end method

.method a(Lorg/apache/http/HttpResponse;)V
    .registers 14
    .parameter

    .prologue
    .line 459
    const-string v0, "Location"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_8
    if-ge v0, v2, :cond_18

    aget-object v3, v1, v0

    .line 460
    iget-object v4, p0, Lrz;->a:Lsm;

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lsm;->a(Ljava/lang/String;)V

    .line 459
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 464
    :cond_18
    const-string v0, "Range"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 465
    const-string v0, "DocsUploaderImpl"

    const-string v1, "Resumable upload response missing range header - attempting to proceed."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    :cond_27
    const-string v0, "Range"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_30
    if-ge v1, v3, :cond_e7

    aget-object v0, v2, v1

    .line 468
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lrz;->a(Ljava/lang/String;)Lrt;

    move-result-object v4

    .line 469
    const-string v0, "DocsUploaderImpl"

    const-string v5, "Resumable upload range = %d-%d (expected %d)"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-wide v8, v4, Lrt;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, v4, Lrt;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-wide v8, p0, Lrz;->a:J

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    iget-wide v5, v4, Lrt;->a:J

    const-wide/16 v7, 0x0

    cmp-long v0, v5, v7

    if-eqz v0, :cond_78

    .line 474
    new-instance v0, LrT;

    const-string v1, "Unable to upload item: Bytes lost in transmission."

    invoke-direct {v0, v1}, LrT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 477
    :cond_78
    iget-wide v5, p0, Lrz;->a:J

    const-wide/16 v7, -0x1

    cmp-long v0, v5, v7

    if-eqz v0, :cond_d7

    iget-wide v5, v4, Lrt;->b:J

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    iget-wide v7, p0, Lrz;->a:J

    cmp-long v0, v5, v7

    if-eqz v0, :cond_d7

    const/4 v0, 0x1

    .line 478
    :goto_8c
    if-eqz v0, :cond_ae

    .line 479
    const-string v5, "DocsUploaderImpl"

    const-string v6, "Upload server byterange mismatch: we sent %d bytes, server acks %d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-wide v9, p0, Lrz;->a:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-wide v9, v4, Lrt;->b:J

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_ae
    iget-wide v4, v4, Lrt;->b:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lrz;->a:J

    .line 485
    :try_start_b5
    iget-object v4, p0, Lrz;->a:Ljava/io/RandomAccessFile;

    iget-wide v5, p0, Lrz;->a:J

    invoke-virtual {v4, v5, v6}, Ljava/io/RandomAccessFile;->seek(J)V
    :try_end_bc
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_bc} :catch_d9

    .line 489
    if-eqz v0, :cond_e2

    .line 491
    iget-object v0, p0, Lrz;->a:Lsn;

    iget-wide v1, p0, Lrz;->a:J

    iget-object v3, p0, Lrz;->a:LrK;

    invoke-virtual {v3}, LrK;->a()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-interface {v0, v1, v2, v3, v4}, Lsn;->a(JJ)V

    .line 492
    new-instance v0, LrW;

    const-string v1, "Unable to upload item: Bytes lost in transmission."

    invoke-direct {v0, v1}, LrW;-><init>(Ljava/lang/String;)V

    throw v0

    .line 477
    :cond_d7
    const/4 v0, 0x0

    goto :goto_8c

    .line 486
    :catch_d9
    move-exception v0

    .line 487
    new-instance v1, LrT;

    const-string v2, "Error resending file data"

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 467
    :cond_e2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_30

    .line 495
    :cond_e7
    return-void
.end method

.method b()Lorg/apache/http/client/methods/HttpUriRequest;
    .registers 6

    .prologue
    .line 443
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    new-instance v1, Ljava/net/URI;

    iget-object v2, p0, Lrz;->a:Lsm;

    invoke-virtual {v2}, Lsm;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/net/URI;)V
    :try_end_10
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_10} :catch_49

    .line 448
    const-string v1, "GData-Version"

    const-string v2, "3.0"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    const-string v1, "Content-Range"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bytes */"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lrz;->a:LrK;

    invoke-virtual {v3}, LrK;->a()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    const/4 v2, 0x0

    new-array v2, v2, [B

    invoke-direct {v1, v2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 454
    return-object v0

    .line 444
    :catch_49
    move-exception v0

    .line 445
    new-instance v1, LrT;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lrz;->a:Lsm;

    invoke-virtual {v3}, Lsm;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LrT;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
