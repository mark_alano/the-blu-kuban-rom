.class public LsV;
.super LdX;
.source "DiscussionCoordinator.java"


# instance fields
.field final synthetic a:I

.field final synthetic a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

.field final synthetic a:LlV;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;LlV;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 493
    iput-object p1, p0, LsV;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    iput-object p2, p0, LsV;->a:LlV;

    iput p3, p0, LsV;->a:I

    invoke-direct {p0}, LdX;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 4

    .prologue
    .line 497
    :try_start_0
    iget-object v0, p0, LsV;->a:LlV;

    invoke-interface {v0}, LlV;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmx;

    .line 498
    if-eqz v0, :cond_3d

    .line 499
    iget-object v1, p0, LsV;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-static {v1, v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;Lmx;)Lmx;

    .line 500
    iget-object v1, p0, LsV;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-static {v1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_19
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LsS;

    .line 501
    invoke-interface {v1, v0}, LsS;->a(Lmx;)V
    :try_end_28
    .catch Lmp; {:try_start_0 .. :try_end_28} :catch_29

    goto :goto_19

    .line 506
    :catch_29
    move-exception v0

    .line 507
    iget v1, p0, LsV;->a:I

    const/4 v2, 0x3

    if-lt v1, v2, :cond_48

    .line 508
    const-string v1, "DiscussionCoordinator"

    invoke-virtual {v0}, Lmp;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;)V

    .line 514
    :cond_3d
    :goto_3d
    return-void

    .line 504
    :cond_3e
    :try_start_3e
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->clear()V
    :try_end_47
    .catch Lmp; {:try_start_3e .. :try_end_47} :catch_29

    goto :goto_3d

    .line 511
    :cond_48
    iget-object v0, p0, LsV;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    iget v1, p0, LsV;->a:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;I)V

    goto :goto_3d
.end method
