.class public Lsa;
.super Ljava/lang/Object;
.source "UploadQueueActivity.java"

# interfaces
.implements LZA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LZA",
        "<",
        "Lcom/google/android/apps/docs/docsuploader/UploadQueueService;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;)V
    .registers 8
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a(Lsf;)V

    .line 75
    iget-object v0, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    iput-object p1, v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    .line 77
    iget-object v0, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a()LrZ;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;LrZ;)LrZ;

    .line 78
    const-string v0, "UploadQueueActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UploadList: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    invoke-static {v2}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;)LrZ;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    new-instance v1, Lse;

    iget-object v2, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    iget-object v3, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    sget v4, Lej;->upload_queue_list_item:I

    iget-object v5, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    invoke-static {v5}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;)LrZ;

    move-result-object v5

    invoke-virtual {v5}, LrZ;->a()Ljava/util/List;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lse;-><init>(Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->a:Lse;

    .line 83
    iget-object v0, p0, Lsa;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;

    new-instance v1, Lsb;

    invoke-direct {v1, p0}, Lsb;-><init>(Lsa;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/docsuploader/UploadQueueActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 90
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 71
    check-cast p1, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    invoke-virtual {p0, p1}, Lsa;->a(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;)V

    return-void
.end method
