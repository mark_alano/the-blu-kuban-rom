.class public Lsk;
.super Ljava/lang/Object;
.source "UploadQueueService.java"

# interfaces
.implements Lsn;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

.field a:Lsm;

.field a:Lsn;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/docsuploader/UploadQueueService;Lsn;Lsm;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 162
    iput-object p1, p0, Lsk;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 163
    iput-object p2, p0, Lsk;->a:Lsn;

    .line 164
    iput-object p3, p0, Lsk;->a:Lsm;

    .line 165
    return-void
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 174
    iget-object v0, p0, Lsk;->a:Lsn;

    invoke-interface {v0}, Lsn;->a()V

    .line 175
    return-void
.end method

.method public a(JJ)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 189
    iget-object v0, p0, Lsk;->a:Lsn;

    invoke-interface {v0, p1, p2, p3, p4}, Lsn;->a(JJ)V

    .line 190
    iget-object v0, p0, Lsk;->a:Lsm;

    const-wide/16 v1, 0x64

    mul-long/2addr v1, p1

    div-long/2addr v1, p3

    long-to-int v1, v1

    iput v1, v0, Lsm;->a:I

    .line 192
    iget-object v0, p0, Lsk;->a:Lcom/google/android/apps/docs/docsuploader/UploadQueueService;

    iget-object v0, v0, Lcom/google/android/apps/docs/docsuploader/UploadQueueService;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsf;

    .line 193
    invoke-interface {v0}, Lsf;->f()V

    goto :goto_16

    .line 195
    :cond_26
    return-void
.end method

.method public a(LrT;)V
    .registers 3
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, Lsk;->a:Lsn;

    invoke-interface {v0, p1}, Lsn;->a(LrT;)V

    .line 170
    return-void
.end method

.method public b()V
    .registers 2

    .prologue
    .line 179
    iget-object v0, p0, Lsk;->a:Lsn;

    invoke-interface {v0}, Lsn;->b()V

    .line 180
    return-void
.end method

.method public c()V
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Lsk;->a:Lsn;

    invoke-interface {v0}, Lsn;->c()V

    .line 185
    return-void
.end method
