.class public Lsm;
.super Ljava/lang/Object;
.source "UploadStatus.java"


# instance fields
.field a:I

.field private a:Ljava/lang/String;

.field final a:LrK;

.field private final a:Lsl;

.field public final a:Z

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(LrK;)V
    .registers 5
    .parameter

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-virtual {p1}, LrK;->b()Z

    move-result v1

    invoke-virtual {p1}, LrK;->a()Lsl;

    move-result-object v2

    invoke-direct {p0, p1, v0, v1, v2}, Lsm;-><init>(LrK;IZLsl;)V

    .line 36
    return-void
.end method

.method public constructor <init>(LrK;IZLsl;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lsm;->a:LrK;

    .line 27
    iput p2, p0, Lsm;->a:I

    .line 28
    iput-boolean p3, p0, Lsm;->a:Z

    .line 29
    iput-object p4, p0, Lsm;->a:Lsl;

    .line 30
    iput-boolean v0, p0, Lsm;->b:Z

    .line 31
    iput-boolean v0, p0, Lsm;->c:Z

    .line 32
    return-void
.end method


# virtual methods
.method public declared-synchronized a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 79
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lsm;->a:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-object v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()LrK;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lsm;->a:LrK;

    return-object v0
.end method

.method public a()Lsl;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Lsm;->a:Lsl;

    return-object v0
.end method

.method declared-synchronized a()V
    .registers 2

    .prologue
    .line 62
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lsm;->b:Z

    .line 63
    iget-object v0, p0, Lsm;->a:LrK;

    invoke-virtual {v0}, LrK;->b()V
    :try_end_9
    .catchall {:try_start_2 .. :try_end_9} :catchall_b

    .line 64
    monitor-exit p0

    return-void

    .line 62
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 83
    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lsm;->a:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 84
    monitor-exit p0

    return-void

    .line 83
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 75
    monitor-enter p0

    :try_start_1
    iput-boolean p1, p0, Lsm;->c:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    .line 76
    monitor-exit p0

    return-void

    .line 75
    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a()Z
    .registers 3

    .prologue
    .line 42
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lsm;->c:Z

    if-nez v0, :cond_12

    iget-boolean v0, p0, Lsm;->b:Z

    if-nez v0, :cond_12

    iget v0, p0, Lsm;->a:I
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_14

    const/16 v1, 0x64

    if-ge v0, v1, :cond_12

    const/4 v0, 0x1

    :goto_10
    monitor-exit p0

    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_10

    :catchall_14
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b()Z
    .registers 2

    .prologue
    .line 53
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lsm;->b:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized c()Z
    .registers 2

    .prologue
    .line 67
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lsm;->c:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return v0

    :catchall_5
    move-exception v0

    monitor-exit p0

    throw v0
.end method
