.class LtB;
.super Ljava/lang/Object;
.source "AllDiscussionsViewManager.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Ltz;


# direct methods
.method constructor <init>(Ltz;)V
    .registers 2
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, LtB;->a:Ltz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, LtB;->a:Ltz;

    invoke-static {v0}, Ltz;->a(Ltz;)Ltv;

    move-result-object v0

    invoke-virtual {v0}, Ltv;->getCount()I

    move-result v0

    if-lt p3, v0, :cond_d

    .line 55
    :goto_c
    return-void

    .line 52
    :cond_d
    iget-object v0, p0, LtB;->a:Ltz;

    invoke-static {v0}, Ltz;->a(Ltz;)Ltv;

    move-result-object v0

    invoke-virtual {v0, p3}, Ltv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 53
    new-instance v1, Ltd;

    invoke-direct {v1, v0}, Ltd;-><init>(Lmz;)V

    .line 54
    iget-object v0, p0, LtB;->a:Ltz;

    invoke-static {v0}, Ltz;->a(Ltz;)Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/AllDiscussionsFragment;->a(Ltd;)V

    goto :goto_c
.end method
