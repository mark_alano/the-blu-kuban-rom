.class public LtM;
.super Ljava/lang/Object;
.source "EditCommentViewManager.java"

# interfaces
.implements LtL;


# instance fields
.field private final a:Landroid/text/TextWatcher;

.field private final a:Landroid/view/View$OnClickListener;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/EditText;

.field private a:Landroid/widget/ImageView;

.field private a:Landroid/widget/TextView;

.field private final a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

.field private a:Z

.field private b:Landroid/view/View;

.field private b:Z

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;)V
    .registers 3
    .parameter

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, LtN;

    invoke-direct {v0, p0}, LtN;-><init>(LtM;)V

    iput-object v0, p0, LtM;->a:Landroid/text/TextWatcher;

    .line 53
    new-instance v0, LtO;

    invoke-direct {v0, p0}, LtO;-><init>(LtM;)V

    iput-object v0, p0, LtM;->a:Landroid/view/View$OnClickListener;

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, LtM;->a:Z

    .line 81
    iput-object p1, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    .line 82
    return-void
.end method

.method static synthetic a(LtM;)Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 96
    sget v0, LsD;->comment_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, LtM;->a:Landroid/widget/EditText;

    .line 97
    sget v0, LsD;->discussion_loading_spinner_edit_comment_fragment:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LtM;->a:Landroid/view/View;

    .line 98
    sget v0, LsD;->contact_picture:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LtM;->a:Landroid/widget/ImageView;

    .line 99
    sget v0, LsD;->comment_author_date:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LtM;->a:Landroid/widget/TextView;

    .line 100
    sget v0, LsD;->action_edit_cancel:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LtM;->c:Landroid/view/View;

    .line 101
    sget v0, LsD;->action_edit_save:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LtM;->b:Landroid/view/View;

    .line 102
    sget v0, LsD;->action_edit_trash:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LtM;->d:Landroid/view/View;

    .line 103
    iget-object v0, p0, LtM;->c:Landroid/view/View;

    iget-object v1, p0, LtM;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v0, p0, LtM;->b:Landroid/view/View;

    iget-object v1, p0, LtM;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v0, p0, LtM;->a:Landroid/widget/EditText;

    iget-object v1, p0, LtM;->a:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, LtM;->a:Z

    .line 107
    return-void
.end method

.method static synthetic a(LtM;)V
    .registers 1
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, LtM;->h()V

    return-void
.end method

.method static synthetic a(LtM;)Z
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-boolean v0, p0, LtM;->b:Z

    return v0
.end method

.method private b(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 129
    if-nez p1, :cond_6

    .line 152
    :goto_5
    return-void

    .line 133
    :cond_6
    invoke-direct {p0, p1}, LtM;->a(Landroid/view/View;)V

    .line 134
    sget-object v0, LtP;->a:[I

    iget-object v1, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()LtK;

    move-result-object v1

    invoke-virtual {v1}, LtK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_56

    .line 146
    iget-object v0, p0, LtM;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    sget v1, LsH;->discussion_save:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 147
    iget-object v0, p0, LtM;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 149
    :goto_28
    invoke-direct {p0}, LtM;->g()V

    .line 150
    invoke-direct {p0}, LtM;->h()V

    .line 151
    iput-boolean v2, p0, LtM;->b:Z

    goto :goto_5

    .line 136
    :pswitch_31
    iget-object v0, p0, LtM;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    sget v1, LsH;->discussion_reply:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 137
    iget-object v0, p0, LtM;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_28

    .line 140
    :pswitch_40
    iget-object v0, p0, LtM;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    sget v1, LsH;->discussion_save:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 141
    iget-object v0, p0, LtM;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 142
    iget-object v0, p0, LtM;->d:Landroid/view/View;

    iget-object v1, p0, LtM;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_28

    .line 134
    :pswitch_data_56
    .packed-switch 0x1
        :pswitch_31
        :pswitch_40
    .end packed-switch
.end method

.method private g()V
    .registers 8

    .prologue
    const/4 v4, 0x1

    const/16 v6, 0x21

    const/4 v5, 0x0

    .line 190
    iget-boolean v0, p0, LtM;->a:Z

    if-nez v0, :cond_9

    .line 223
    :goto_8
    return-void

    .line 193
    :cond_9
    iget-object v0, p0, LtM;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 196
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 198
    iget-object v2, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Lmx;

    move-result-object v2

    .line 200
    if-eqz v2, :cond_7a

    .line 201
    invoke-interface {v2}, Lmx;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 202
    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    invoke-virtual {v1, v3, v5, v4, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 205
    invoke-interface {v2}, Lmx;->a()Landroid/net/Uri;

    move-result-object v2

    .line 206
    iget-object v3, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()LAQ;

    move-result-object v3

    iget-object v4, p0, LtM;->a:Landroid/widget/ImageView;

    invoke-virtual {v3, v4, v2}, LAQ;->a(Landroid/widget/ImageView;Landroid/net/Uri;)Z

    .line 214
    :goto_3e
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 215
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 216
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const v3, -0x777778

    invoke-direct {v0, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v2, v0, v5, v3, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 220
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 221
    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 222
    iget-object v1, p0, LtM;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_8

    .line 208
    :cond_7a
    sget v2, LsH;->discussion_me:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 209
    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    invoke-virtual {v1, v2, v5, v3, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_3e
.end method

.method private h()V
    .registers 3

    .prologue
    .line 231
    iget-object v0, p0, LtM;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_13

    const/4 v0, 0x1

    .line 232
    :goto_d
    iget-object v1, p0, LtM;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 233
    return-void

    .line 231
    :cond_13
    const/4 v0, 0x0

    goto :goto_d
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Z)Landroid/view/View;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 87
    sget v0, LsF;->discussion_fragment_edit_comment:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 88
    invoke-direct {p0, v0}, LtM;->a(Landroid/view/View;)V

    .line 89
    if-eqz p2, :cond_f

    .line 90
    invoke-direct {p0, v0}, LtM;->b(Landroid/view/View;)V

    .line 92
    :cond_f
    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 125
    iget-object v0, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, LtM;->b(Landroid/view/View;)V

    .line 126
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 116
    iget-boolean v0, p0, LtM;->a:Z

    if-nez v0, :cond_5

    .line 121
    :goto_4
    return-void

    .line 119
    :cond_5
    iget-object v0, p0, LtM;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 120
    invoke-virtual {p0}, LtM;->a()V

    goto :goto_4
.end method

.method public b()V
    .registers 1

    .prologue
    .line 111
    invoke-direct {p0}, LtM;->g()V

    .line 112
    return-void
.end method

.method public c()V
    .registers 4

    .prologue
    .line 156
    iget-object v0, p0, LtM;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_29

    iget-object v0, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->h()Z

    move-result v0

    if-nez v0, :cond_29

    .line 157
    iget-object v0, p0, LtM;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 158
    iget-object v0, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 160
    iget-object v1, p0, LtM;->a:Landroid/widget/EditText;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 162
    :cond_29
    return-void
.end method

.method public d()V
    .registers 4

    .prologue
    .line 166
    iget-object v0, p0, LtM;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_28

    iget-object v0, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->h()Z

    move-result v0

    if-nez v0, :cond_28

    .line 167
    iget-object v0, p0, LtM;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 169
    iget-object v1, p0, LtM;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 171
    :cond_28
    return-void
.end method

.method public e()V
    .registers 3

    .prologue
    .line 175
    iget-boolean v0, p0, LtM;->a:Z

    if-eqz v0, :cond_a

    .line 176
    iget-object v0, p0, LtM;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 178
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, LtM;->b:Z

    .line 179
    return-void
.end method

.method public f()V
    .registers 3

    .prologue
    .line 183
    iget-boolean v0, p0, LtM;->a:Z

    if-eqz v0, :cond_b

    .line 184
    iget-object v0, p0, LtM;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 186
    :cond_b
    const/4 v0, 0x0

    iput-boolean v0, p0, LtM;->b:Z

    .line 187
    return-void
.end method
