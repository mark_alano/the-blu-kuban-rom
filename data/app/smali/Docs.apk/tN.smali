.class LtN;
.super Ljava/lang/Object;
.source "EditCommentViewManager.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:LtM;


# direct methods
.method constructor <init>(LtM;)V
    .registers 2
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, LtN;->a:LtM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, LtN;->a:LtM;

    invoke-static {v0}, LtM;->a(LtM;)Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    move-result-object v0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->b(I)V

    .line 42
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, LtN;->a:LtM;

    invoke-static {v0}, LtM;->a(LtM;)V

    .line 50
    return-void
.end method
