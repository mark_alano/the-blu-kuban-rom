.class LtO;
.super Ljava/lang/Object;
.source "EditCommentViewManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:LtM;


# direct methods
.method constructor <init>(LtM;)V
    .registers 2
    .parameter

    .prologue
    .line 53
    iput-object p1, p0, LtO;->a:LtM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, LtO;->a:LtM;

    invoke-static {v0}, LtM;->a(LtM;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 67
    :cond_8
    :goto_8
    return-void

    .line 60
    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->action_edit_cancel:I

    if-ne v0, v1, :cond_1b

    .line 61
    iget-object v0, p0, LtO;->a:LtM;

    invoke-static {v0}, LtM;->a(LtM;)Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->a()V

    goto :goto_8

    .line 62
    :cond_1b
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->action_edit_trash:I

    if-ne v0, v1, :cond_2d

    .line 63
    iget-object v0, p0, LtO;->a:LtM;

    invoke-static {v0}, LtM;->a(LtM;)Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->p()V

    goto :goto_8

    .line 64
    :cond_2d
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->action_edit_save:I

    if-ne v0, v1, :cond_8

    .line 65
    iget-object v0, p0, LtO;->a:LtM;

    invoke-static {v0}, LtM;->a(LtM;)Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/EditCommentFragment;->s()V

    goto :goto_8
.end method
