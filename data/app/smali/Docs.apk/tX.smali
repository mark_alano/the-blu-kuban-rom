.class public LtX;
.super Ljava/lang/Object;
.source "OneDiscussionPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final a:LAQ;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/ListView;

.field private a:Landroid/widget/TextView;

.field private final a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

.field private final a:Lma;

.field private a:Lmz;

.field private final a:LtW;

.field private final a:Ltb;

.field private a:Ltd;

.field private a:Lua;

.field private a:Z

.field private b:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    sget-object v0, Lua;->a:Lua;

    iput-object v0, p0, LtX;->a:Lua;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, LtX;->a:Z

    .line 83
    iput-object p1, p0, LtX;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    .line 84
    iget-object v0, p1, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    move-result-object v0

    iput-object v0, p0, LtX;->a:Ltb;

    .line 85
    iget-object v0, p1, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()LAQ;

    move-result-object v0

    iput-object v0, p0, LtX;->a:LAQ;

    .line 86
    iget-object v0, p1, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lma;

    move-result-object v0

    iput-object v0, p0, LtX;->a:Lma;

    .line 87
    iget-object v0, p1, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a:LsT;

    invoke-interface {v0}, LsT;->a()Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 89
    new-instance v1, LtW;

    invoke-direct {v1, v0, p0}, LtW;-><init>(Landroid/content/Context;Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v1, p0, LtX;->a:LtW;

    .line 90
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-direct {p0, v0, p2}, LtX;->a(Landroid/view/LayoutInflater;Z)V

    .line 91
    return-void
.end method

.method static synthetic a(LtX;)Ltb;
    .registers 2
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, LtX;->a:Ltb;

    return-object v0
.end method

.method private a(IIIZ)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, LtX;->b:Landroid/view/View;

    iget-object v1, p0, LtX;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 223
    iget-object v0, p0, LtX;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 224
    iget-object v0, p0, LtX;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 226
    iget-object v0, p0, LtX;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    invoke-virtual {v0, p0, p4}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(LtX;Z)V

    .line 227
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x8

    .line 251
    sget v0, LsF;->discussion_fragment_one_discussion:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LtX;->c:Landroid/view/View;

    .line 253
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, LtX;->a:Landroid/widget/ListView;

    .line 254
    iget-object v0, p0, LtX;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 256
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    sget v1, LsD;->discussion_loading_spinner_one_discussion_fragment:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LtX;->a:Landroid/view/View;

    .line 257
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    sget v1, LsD;->discussion_one_discussion_fragment_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LtX;->b:Landroid/view/View;

    .line 258
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    sget v1, LsD;->action_resolve:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LtX;->a:Landroid/widget/TextView;

    .line 259
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    sget v1, LsD;->action_one_discussion_reply:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LtX;->b:Landroid/widget/TextView;

    .line 262
    iget-object v0, p0, LtX;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    sget v1, LsD;->discussion_fragment_close_phone:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 265
    iget-object v1, p0, LtX;->c:Landroid/view/View;

    sget v2, LsD;->discussion_one_discussion_fragment_header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 267
    if-eqz p2, :cond_82

    .line 270
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    sget v1, LsD;->action_close:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 271
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    sget v1, LsD;->action_resolve:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 273
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    sget v1, LsD;->action_comments:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 275
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    :goto_81
    return-void

    .line 278
    :cond_82
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 279
    iget-object v1, p0, LtX;->c:Landroid/view/View;

    sget v2, LsD;->discussion_one_discussion_fragment_header_separator:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 281
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 284
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 285
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    sget v1, LsD;->discussion_fragment_close_icon_phone:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 287
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_81
.end method

.method static synthetic a(LtX;)V
    .registers 1
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, LtX;->f()V

    return-void
.end method

.method static synthetic a(LtX;Lua;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, LtX;->a(Lua;)V

    return-void
.end method

.method private a(Lua;)V
    .registers 6
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 230
    iget-object v0, p0, LtX;->a:Lua;

    if-ne v0, p1, :cond_8

    .line 248
    :goto_7
    return-void

    .line 233
    :cond_8
    iput-object p1, p0, LtX;->a:Lua;

    .line 234
    sget-object v0, LtZ;->a:[I

    invoke-virtual {p1}, Lua;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_36

    .line 245
    iget-object v0, p0, LtX;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 246
    iget-object v0, p0, LtX;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_7

    .line 236
    :pswitch_20
    iget-object v0, p0, LtX;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v0, p0, LtX;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_7

    .line 240
    :pswitch_2b
    iget-object v0, p0, LtX;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 241
    iget-object v0, p0, LtX;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_7

    .line 234
    :pswitch_data_36
    .packed-switch 0x1
        :pswitch_20
        :pswitch_2b
    .end packed-switch
.end method

.method static synthetic a(LtX;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 37
    iput-boolean p1, p0, LtX;->a:Z

    return p1
.end method

.method private b(Ljava/util/SortedSet;)V
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    iget-object v0, p0, LtX;->a:Ltd;

    if-nez v0, :cond_7

    .line 153
    :cond_6
    :goto_6
    return-void

    .line 133
    :cond_7
    sget-object v0, Lmz;->a:Lagv;

    invoke-static {p1, v0}, LajB;->a(Ljava/lang/Iterable;Lagv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_11
    :goto_11
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 134
    invoke-interface {v0}, Lmz;->a()Ljava/lang/String;

    move-result-object v4

    .line 135
    invoke-interface {v0}, Lmz;->b()Ljava/lang/String;

    move-result-object v5

    .line 136
    iget-object v6, p0, LtX;->a:Ltd;

    invoke-virtual {v6, v4}, Ltd;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_51

    .line 137
    iput-object v0, p0, LtX;->a:Lmz;

    .line 140
    iget-object v3, p0, LtX;->a:Ltd;

    invoke-virtual {v3}, Ltd;->b()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_6

    if-eqz v5, :cond_6

    .line 141
    invoke-interface {v0}, Lmz;->d()Z

    move-result v0

    if-nez v0, :cond_4f

    move v0, v1

    .line 142
    :goto_40
    new-instance v1, Ltd;

    invoke-direct {v1, v4, v5, v0}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v1, p0, LtX;->a:Ltd;

    .line 143
    iget-object v0, p0, LtX;->a:Ltb;

    iget-object v1, p0, LtX;->a:Ltd;

    invoke-interface {v0, v1}, Ltb;->a(Ltd;)Z

    goto :goto_6

    :cond_4f
    move v0, v2

    .line 141
    goto :goto_40

    .line 147
    :cond_51
    iget-object v6, p0, LtX;->a:Ltd;

    invoke-virtual {v6, v5}, Ltd;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 148
    iput-object v0, p0, LtX;->a:Lmz;

    .line 149
    invoke-interface {v0}, Lmz;->d()Z

    move-result v0

    if-nez v0, :cond_6a

    move v0, v1

    .line 150
    :goto_62
    new-instance v6, Ltd;

    invoke-direct {v6, v4, v5, v0}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v6, p0, LtX;->a:Ltd;

    goto :goto_11

    :cond_6a
    move v0, v2

    .line 149
    goto :goto_62
.end method

.method private c()V
    .registers 4

    .prologue
    .line 159
    iget-object v0, p0, LtX;->a:Landroid/widget/ListView;

    if-nez v0, :cond_5

    .line 190
    :goto_4
    return-void

    .line 164
    :cond_5
    iget-object v0, p0, LtX;->a:Lmz;

    if-eqz v0, :cond_3d

    const/4 v0, 0x1

    :goto_a
    const-string v1, "showOneDiscussion with entry == null"

    invoke-static {v0, v1}, Lagu;->b(ZLjava/lang/Object;)V

    .line 166
    iget-object v0, p0, LtX;->a:LtW;

    invoke-virtual {v0}, LtW;->clear()V

    .line 167
    iget-object v0, p0, LtX;->a:LtW;

    iget-object v1, p0, LtX;->a:Lmz;

    invoke-virtual {v0, v1}, LtW;->add(Ljava/lang/Object;)V

    .line 168
    iget-object v0, p0, LtX;->a:Lmz;

    invoke-interface {v0}, Lmz;->a()Ljava/util/Collection;

    move-result-object v0

    .line 169
    sget-object v1, Lmz;->a:Lagv;

    invoke-static {v0, v1}, LajB;->a(Ljava/lang/Iterable;Lagv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 170
    iget-object v2, p0, LtX;->a:LtW;

    invoke-virtual {v2, v0}, LtW;->add(Ljava/lang/Object;)V

    goto :goto_2b

    .line 164
    :cond_3d
    const/4 v0, 0x0

    goto :goto_a

    .line 173
    :cond_3f
    iget-object v0, p0, LtX;->a:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_4e

    .line 174
    iget-object v0, p0, LtX;->a:Landroid/widget/ListView;

    iget-object v1, p0, LtX;->a:LtW;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 177
    :cond_4e
    iget-object v0, p0, LtX;->a:Lua;

    sget-object v1, Lua;->d:Lua;

    if-eq v0, v1, :cond_59

    .line 179
    sget-object v0, Lua;->c:Lua;

    invoke-direct {p0, v0}, LtX;->a(Lua;)V

    .line 182
    :cond_59
    invoke-virtual {p0}, LtX;->a()V

    .line 185
    iget-object v0, p0, LtX;->a:LtW;

    invoke-virtual {v0}, LtW;->a()Z

    move-result v0

    if-nez v0, :cond_6b

    .line 186
    iget-object v0, p0, LtX;->a:LtW;

    iget-object v1, p0, LtX;->a:LAQ;

    invoke-virtual {v0, v1}, LtW;->a(LAQ;)V

    .line 189
    :cond_6b
    iget-object v0, p0, LtX;->a:LtW;

    invoke-virtual {v0}, LtW;->notifyDataSetChanged()V

    goto :goto_4
.end method

.method private d()V
    .registers 5

    .prologue
    .line 210
    sget v0, LsA;->discussion_resolved_docos_background:I

    sget v1, LsH;->discussion_reopen:I

    sget v2, LsH;->discussion_reply_to_reopen_this_comment:I

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v2, v3}, LtX;->a(IIIZ)V

    .line 212
    return-void
.end method

.method private e()V
    .registers 5

    .prologue
    .line 215
    sget v0, LsA;->discussion_active_docos_background:I

    sget v1, LsH;->discussion_resolve:I

    sget v2, LsH;->discussion_reply_to_this_comment:I

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, LtX;->a(IIIZ)V

    .line 217
    return-void
.end method

.method private f()V
    .registers 4

    .prologue
    .line 406
    iget-object v0, p0, LtX;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    sget v1, LsH;->discussion_error:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(II)V

    .line 407
    return-void
.end method

.method private g()V
    .registers 4

    .prologue
    .line 410
    iget-object v0, p0, LtX;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    sget v1, LsH;->discussion_cant_edit_comment:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(II)V

    .line 411
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .registers 2

    .prologue
    .line 295
    iget-object v0, p0, LtX;->c:Landroid/view/View;

    return-object v0
.end method

.method a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, LtX;->a:Ltd;

    invoke-virtual {v0}, Ltd;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 197
    iget-object v0, p0, LtX;->a:Lmz;

    if-nez v0, :cond_5

    .line 207
    :goto_4
    return-void

    .line 200
    :cond_5
    iget-object v0, p0, LtX;->a:Lmz;

    invoke-interface {v0}, Lmz;->d()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 202
    invoke-direct {p0}, LtX;->d()V

    goto :goto_4

    .line 205
    :cond_11
    invoke-direct {p0}, LtX;->e()V

    goto :goto_4
.end method

.method public a(Ljava/util/SortedSet;)V
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    const-string v0, "OneDiscussionPage"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateDiscussion: discussion = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LtX;->a:Ltd;

    invoke-virtual {v2}, Ltd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-direct {p0, p1}, LtX;->b(Ljava/util/SortedSet;)V

    .line 117
    iget-object v0, p0, LtX;->a:Lmz;

    if-eqz v0, :cond_29

    .line 118
    invoke-direct {p0}, LtX;->c()V

    .line 127
    :cond_28
    :goto_28
    return-void

    .line 121
    :cond_29
    iget-object v0, p0, LtX;->a:Ltd;

    if-eqz v0, :cond_28

    .line 123
    invoke-direct {p0}, LtX;->f()V

    .line 124
    iget-object v0, p0, LtX;->a:Ltb;

    invoke-interface {v0}, Ltb;->q()V

    goto :goto_28
.end method

.method public a(Ltd;)V
    .registers 3
    .parameter

    .prologue
    .line 99
    iput-object p1, p0, LtX;->a:Ltd;

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, LtX;->a:Lmz;

    .line 101
    iget-object v0, p0, LtX;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a(LtX;)V

    .line 102
    return-void
.end method

.method public b()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 359
    iget-boolean v0, p0, LtX;->a:Z

    if-eqz v0, :cond_6

    .line 403
    :cond_5
    :goto_5
    return-void

    .line 362
    :cond_6
    iget-object v0, p0, LtX;->a:Ltb;

    invoke-interface {v0}, Ltb;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 365
    iput-boolean v4, p0, LtX;->a:Z

    .line 366
    iget-object v0, p0, LtX;->a:Lmz;

    if-nez v0, :cond_18

    .line 367
    invoke-direct {p0}, LtX;->f()V

    goto :goto_5

    .line 372
    :cond_18
    iget-object v0, p0, LtX;->a:Lmz;

    invoke-interface {v0}, Lmz;->d()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 373
    iget-object v0, p0, LtX;->a:Lma;

    iget-object v1, p0, LtX;->a:Lmz;

    invoke-interface {v0, v1}, Lma;->b(Lmz;)LlV;

    move-result-object v1

    .line 375
    new-instance v0, Ltd;

    iget-object v2, p0, LtX;->a:Lmz;

    invoke-interface {v2}, Lmz;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LtX;->a:Lmz;

    invoke-interface {v3}, Lmz;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3, v4}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 382
    :goto_39
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, LtY;

    invoke-direct {v3, p0, v1, v0}, LtY;-><init>(LtX;LlV;Ltd;)V

    invoke-interface {v1, v2, v3}, LlV;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 402
    sget-object v0, Lua;->d:Lua;

    invoke-direct {p0, v0}, LtX;->a(Lua;)V

    goto :goto_5

    .line 377
    :cond_4c
    iget-object v0, p0, LtX;->a:Lma;

    iget-object v1, p0, LtX;->a:Lmz;

    invoke-interface {v0, v1}, Lma;->a(Lmz;)LlV;

    move-result-object v1

    .line 380
    iget-object v0, p0, LtX;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Ltd;

    move-result-object v0

    goto :goto_39
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 342
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->action_comments:I

    if-ne v0, v1, :cond_e

    .line 343
    iget-object v0, p0, LtX;->a:Ltb;

    invoke-interface {v0}, Ltb;->q()V

    .line 352
    :cond_d
    :goto_d
    return-void

    .line 344
    :cond_e
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->action_resolve:I

    if-ne v0, v1, :cond_1a

    .line 345
    invoke-virtual {p0}, LtX;->b()V

    goto :goto_d

    .line 346
    :cond_1a
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->action_close:I

    if-eq v0, v1, :cond_2a

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->discussion_fragment_close_icon_phone:I

    if-ne v0, v1, :cond_30

    .line 348
    :cond_2a
    iget-object v0, p0, LtX;->a:Ltb;

    invoke-interface {v0}, Ltb;->p()V

    goto :goto_d

    .line 349
    :cond_30
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, LsD;->action_one_discussion_reply:I

    if-ne v0, v1, :cond_d

    .line 350
    iget-object v0, p0, LtX;->a:Ltb;

    iget-object v1, p0, LtX;->a:Ltd;

    invoke-virtual {v1}, Ltd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ltb;->b(Ljava/lang/String;)V

    goto :goto_d
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 300
    iget-object v0, p0, LtX;->a:LtW;

    invoke-virtual {v0}, LtW;->getCount()I

    move-result v0

    if-lt p3, v0, :cond_9

    .line 338
    :goto_8
    return-void

    .line 304
    :cond_9
    iget-object v0, p0, LtX;->a:LtW;

    invoke-virtual {v0, p3}, LtW;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 306
    iget-object v1, p0, LtX;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Lmx;

    move-result-object v2

    .line 307
    invoke-interface {v0}, Lmz;->a()Lmx;

    move-result-object v1

    invoke-interface {v1}, Lmx;->a()Z

    move-result v1

    if-eqz v1, :cond_25

    .line 309
    invoke-direct {p0}, LtX;->g()V

    goto :goto_8

    .line 312
    :cond_25
    if-eqz v2, :cond_3d

    invoke-interface {v0}, Lmz;->a()Lmx;

    move-result-object v1

    invoke-interface {v1}, Lmx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2}, Lmx;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3d

    .line 314
    invoke-direct {p0}, LtX;->g()V

    goto :goto_8

    .line 318
    :cond_3d
    invoke-interface {v0}, Lmz;->a()Ljava/lang/String;

    move-result-object v3

    .line 319
    invoke-interface {v0}, Lmz;->d()Ljava/lang/String;

    move-result-object v1

    .line 321
    if-nez v1, :cond_49

    .line 322
    const-string v1, ""

    .line 324
    :cond_49
    invoke-interface {v0}, Lmz;->a()Z

    move-result v4

    if-nez v4, :cond_5d

    invoke-interface {v0}, Lmz;->b()Z

    move-result v0

    if-eqz v0, :cond_61

    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 327
    :cond_5d
    invoke-direct {p0}, LtX;->g()V

    goto :goto_8

    .line 331
    :cond_61
    if-nez v2, :cond_67

    .line 333
    invoke-direct {p0}, LtX;->f()V

    goto :goto_8

    .line 336
    :cond_67
    iget-object v0, p0, LtX;->a:Ltb;

    invoke-interface {v0, v3, v1}, Ltb;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method
