.class public Lth;
.super Ljava/lang/Object;
.source "KixDiscussionAnchorManager.java"

# interfaces
.implements LsR;
.implements LyI;
.implements Lyj;


# instance fields
.field private a:LDA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lalt;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lalt",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

.field private final a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LBV;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ltd;

.field private a:LvZ;

.field private a:LwF;

.field private a:Lyb;

.field private a:Z

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/kix/KixEditText;)V
    .registers 3
    .parameter

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lth;->a:Ljava/util/List;

    .line 71
    iput-object p1, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lth;->b:Ljava/util/List;

    .line 73
    invoke-static {}, Laja;->a()Laja;

    move-result-object v0

    iput-object v0, p0, Lth;->a:Lalt;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lth;->a:Ljava/util/Set;

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lth;->a:Z

    .line 76
    return-void
.end method

.method private a(I)I
    .registers 4
    .parameter

    .prologue
    .line 295
    new-instance v0, Lyc;

    iget-object v1, p0, Lth;->a:LDA;

    invoke-direct {v0, v1, p1}, Lyc;-><init>(LDA;I)V

    .line 297
    iget-object v1, p0, Lth;->a:Lyb;

    invoke-interface {v1, v0}, Lyb;->a(Lyc;)Lyd;

    move-result-object v0

    .line 299
    iget v0, v0, Lyd;->b:F

    float-to-int v0, v0

    add-int/lit8 v0, v0, -0x14

    .line 300
    return v0
.end method

.method private a(II)Ljava/lang/String;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 345
    iget-object v0, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBV;

    .line 346
    iget-object v2, p0, Lth;->a:LDA;

    invoke-virtual {v2, v0}, LDA;->a(Ljava/lang/Object;)I

    move-result v2

    if-gt v2, p1, :cond_7

    iget-object v2, p0, Lth;->a:LDA;

    invoke-virtual {v2, v0}, LDA;->b(Ljava/lang/Object;)I

    move-result v2

    if-lt v2, p2, :cond_7

    .line 348
    invoke-virtual {v0}, LBV;->a()Ljava/lang/String;

    move-result-object v1

    .line 367
    :goto_27
    return-object v1

    .line 351
    :cond_28
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 352
    iget-object v0, p0, Lth;->a:LDA;

    const-class v4, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    move v2, p1

    move v3, p2

    invoke-virtual/range {v0 .. v5}, LDA;->a(Ljava/util/List;IILjava/lang/Class;Ljava/lang/Object;)V

    .line 353
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_79

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    .line 354
    iget-object v1, p0, Lth;->a:LDA;

    invoke-virtual {v1, v0}, LDA;->a(Ljava/lang/Object;)I

    move-result v1

    if-gt v1, p1, :cond_3a

    iget-object v1, p0, Lth;->a:LDA;

    invoke-virtual {v1, v0}, LDA;->b(Ljava/lang/Object;)I

    move-result v1

    if-lt v1, p2, :cond_3a

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a()Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 357
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a()Ljava/util/List;

    move-result-object v1

    .line 358
    if-eqz v1, :cond_3a

    .line 359
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_66
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 360
    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_66

    goto :goto_27

    :cond_79
    move-object v1, v5

    .line 367
    goto :goto_27
.end method

.method private a(Ljava/lang/String;)Ljava/util/List;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "LKj;",
            ">;"
        }
    .end annotation

    .prologue
    .line 379
    iget-object v0, p0, Lth;->a:Lalt;

    invoke-interface {v0, p1}, Lalt;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    .line 380
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    if-nez v1, :cond_e

    .line 381
    const/4 v0, 0x0

    .line 393
    :goto_d
    return-object v0

    .line 383
    :cond_e
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 384
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_17
    :goto_17
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    .line 385
    iget-object v3, p0, Lth;->a:LDA;

    invoke-virtual {v3, v0}, LDA;->a(Ljava/lang/Object;)I

    move-result v3

    .line 386
    iget-object v4, p0, Lth;->a:LDA;

    invoke-virtual {v4, v0}, LDA;->b(Ljava/lang/Object;)I

    move-result v0

    .line 387
    if-ltz v3, :cond_17

    if-ltz v0, :cond_17

    .line 388
    new-instance v4, LKj;

    invoke-direct {v4, v3, v0}, LKj;-><init>(II)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_17

    .line 391
    :cond_3c
    invoke-static {v1}, LKj;->a(Ljava/util/List;)V

    .line 392
    invoke-static {v1}, LKj;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_d
.end method

.method private a(I)V
    .registers 3
    .parameter

    .prologue
    .line 330
    const/4 v0, 0x0

    iput-boolean v0, p0, Lth;->a:Z

    .line 331
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setSelection(I)V

    .line 332
    const/4 v0, 0x1

    iput-boolean v0, p0, Lth;->a:Z

    .line 333
    return-void
.end method

.method private b()Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 444
    iget-object v0, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_b

    .line 445
    const/4 v0, 0x0

    .line 453
    :goto_a
    return-object v0

    .line 449
    :cond_b
    iget-object v0, p0, Lth;->a:LDA;

    iget-object v1, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, LDA;->a(Ljava/lang/Object;)I

    move-result v0

    .line 450
    iget-object v1, p0, Lth;->a:LDA;

    iget-object v2, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, LDA;->b(Ljava/lang/Object;)I

    move-result v1

    .line 451
    sub-int v2, v1, v0

    new-array v2, v2, [C

    .line 452
    iget-object v3, p0, Lth;->a:LDA;

    invoke-virtual {v3, v0, v1, v2, v4}, LDA;->getChars(II[CI)V

    .line 453
    invoke-static {v2}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    move-result-object v0

    goto :goto_a
.end method

.method private d(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 438
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lth;->a(Ljava/lang/String;Z)Z

    .line 439
    invoke-direct {p0}, Lth;->b()Ljava/lang/String;

    move-result-object v0

    .line 440
    iget-object v1, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    return-void
.end method

.method private e()V
    .registers 11

    .prologue
    const/4 v1, 0x0

    .line 200
    const-string v0, "KixDiscussionAnchorManager"

    const-string v2, "updateSortedDiscussions"

    invoke-static {v0, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    iget-object v0, p0, Lth;->a:LwF;

    if-nez v0, :cond_14

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lth;->b:Ljava/util/List;

    .line 221
    :goto_13
    return-void

    .line 205
    :cond_14
    iget-object v0, p0, Lth;->a:LwF;

    invoke-interface {v0}, LwF;->a()LuV;

    move-result-object v3

    .line 206
    invoke-interface {v3}, LuV;->b()V

    .line 207
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lth;->b:Ljava/util/List;

    .line 209
    :try_start_24
    iget-object v0, p0, Lth;->a:LwF;

    invoke-interface {v0}, LwF;->a()[I

    move-result-object v4

    .line 210
    array-length v5, v4

    move v2, v1

    :goto_2c
    if-ge v2, v5, :cond_54

    aget v0, v4, v2

    .line 211
    iget-object v6, p0, Lth;->a:LwF;

    invoke-interface {v6, v0}, LwF;->a(I)Lvo;

    move-result-object v0

    invoke-interface {v0}, Lvo;->a()[Ljava/lang/String;

    move-result-object v6

    .line 212
    array-length v7, v6

    move v0, v1

    :goto_3c
    if-ge v0, v7, :cond_50

    aget-object v8, v6, v0

    .line 213
    iget-object v9, p0, Lth;->b:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4d

    .line 214
    iget-object v9, p0, Lth;->b:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4d
    .catchall {:try_start_24 .. :try_end_4d} :catchall_58

    .line 212
    :cond_4d
    add-int/lit8 v0, v0, 0x1

    goto :goto_3c

    .line 210
    :cond_50
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2c

    .line 219
    :cond_54
    invoke-interface {v3}, LuV;->c()V

    goto :goto_13

    :catchall_58
    move-exception v0

    invoke-interface {v3}, LuV;->c()V

    throw v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 3

    .prologue
    .line 324
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v0

    .line 325
    iget-object v1, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v1

    .line 326
    invoke-direct {p0, v0, v1}, Lth;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    iget-object v0, p0, Lth;->b:Ljava/util/List;

    return-object v0
.end method

.method public a()V
    .registers 4

    .prologue
    .line 305
    iget-object v0, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 313
    :goto_8
    return-void

    .line 308
    :cond_9
    iget-object v0, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBV;

    .line 309
    iget-object v2, p0, Lth;->a:LDA;

    invoke-virtual {v2, v0}, LDA;->removeSpan(Ljava/lang/Object;)V

    goto :goto_f

    .line 311
    :cond_21
    iget-object v0, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 312
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->postInvalidate()V

    goto :goto_8
.end method

.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()Z

    move-result v0

    if-eqz v0, :cond_36

    iget-boolean v0, p0, Lth;->a:Z

    if-eqz v0, :cond_36

    .line 93
    invoke-direct {p0, p1, p2}, Lth;->a(II)Ljava/lang/String;

    move-result-object v1

    .line 95
    iget-object v0, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_37

    iget-object v0, p0, Lth;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBV;

    invoke-virtual {v0}, LBV;->a()Ljava/lang/String;

    move-result-object v0

    .line 97
    :goto_25
    if-eqz v1, :cond_36

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_36

    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    if-eqz v0, :cond_36

    .line 99
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a(Ljava/lang/String;)V

    .line 102
    :cond_36
    return-void

    .line 95
    :cond_37
    const/4 v0, 0x0

    goto :goto_25
.end method

.method public a(LDA;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    iput-object p1, p0, Lth;->a:LDA;

    .line 80
    new-instance v0, Lye;

    iget-object v1, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-direct {v0, v1, p1}, Lye;-><init>(Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;LDA;)V

    iput-object v0, p0, Lth;->a:Lyb;

    .line 81
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;)V
    .registers 2
    .parameter

    .prologue
    .line 191
    iput-object p1, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    .line 192
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;)V
    .registers 5
    .parameter

    .prologue
    .line 131
    const-string v0, "KixDiscussionAnchorManager"

    const-string v1, "onDocosSpanApplied"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 133
    iget-object v2, p0, Lth;->a:Lalt;

    invoke-interface {v2, v0, p1}, Lalt;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_f

    .line 135
    :cond_21
    iget-object v0, p0, Lth;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 136
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 414
    iget-object v0, p0, Lth;->a:LwF;

    invoke-interface {v0}, LwF;->a()LuV;

    move-result-object v1

    .line 415
    invoke-interface {v1}, LuV;->b()V

    .line 417
    :try_start_9
    iget-object v0, p0, Lth;->a:LvZ;

    invoke-interface {v0, p1}, LvZ;->e(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_12

    .line 419
    invoke-interface {v1}, LuV;->c()V

    .line 421
    return-void

    .line 419
    :catchall_12
    move-exception v0

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public a(Ljava/util/Set;Z)V
    .registers 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 225
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 226
    iget-object v1, p0, Lth;->a:Lalt;

    invoke-interface {v1, v0}, Lalt;->b(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;

    .line 227
    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a(Ljava/lang/String;Z)V

    goto :goto_1a

    .line 232
    :cond_2a
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->g()V

    .line 233
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->postInvalidate()V

    .line 234
    return-void
.end method

.method public a(LwF;LvZ;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    if-nez v0, :cond_5

    .line 123
    :cond_4
    :goto_4
    return-void

    .line 114
    :cond_5
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LwF;

    iput-object v0, p0, Lth;->a:LwF;

    .line 115
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvZ;

    iput-object v0, p0, Lth;->a:LvZ;

    .line 116
    invoke-direct {p0}, Lth;->e()V

    .line 118
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->s()V

    .line 120
    iget-object v0, p0, Lth;->a:Ltd;

    if-eqz v0, :cond_4

    .line 121
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    iget-object v1, p0, Lth;->a:Ltd;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->c(Ltd;)V

    goto :goto_4
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 398
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v0

    iget-object v1, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v1

    if-eq v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public a(Ljava/lang/String;Z)Z
    .registers 14
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    if-eqz p1, :cond_b5

    .line 239
    invoke-direct {p0, p1}, Lth;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 240
    if-eqz v4, :cond_b1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_b1

    .line 241
    iget-object v0, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_16
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LBV;

    .line 242
    iget-object v5, p0, Lth;->a:LDA;

    invoke-virtual {v5, v0}, LDA;->removeSpan(Ljava/lang/Object;)V

    goto :goto_16

    .line 244
    :cond_28
    iget-object v0, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 246
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_32
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKj;

    .line 247
    new-instance v6, LBV;

    invoke-direct {v6, p1, p2}, LBV;-><init>(Ljava/lang/String;Z)V

    .line 248
    iget-object v7, p0, Lth;->a:Ljava/util/List;

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    iget-object v7, p0, Lth;->a:LDA;

    invoke-virtual {v0}, LKj;->a()I

    move-result v8

    invoke-virtual {v0}, LKj;->b()I

    move-result v9

    const/16 v10, 0x21

    invoke-virtual {v7, v6, v8, v9, v10}, LDA;->setSpan(Ljava/lang/Object;III)V

    .line 252
    iget-object v6, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v6}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v6

    invoke-virtual {v0}, LKj;->a()I

    move-result v7

    if-lt v6, v7, :cond_c7

    iget-object v6, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v6}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v6

    invoke-virtual {v0}, LKj;->b()I

    move-result v0

    if-gt v6, v0, :cond_c7

    move v0, v3

    :goto_70
    move v1, v0

    .line 256
    goto :goto_32

    .line 257
    :cond_72
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKj;

    invoke-virtual {v0}, LKj;->a()I

    move-result v5

    .line 258
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKj;

    invoke-virtual {v0}, LKj;->b()I

    move-result v0

    .line 261
    iget-object v2, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b()Landroid/text/Editable;

    move-result-object v2

    const-class v4, LCe;

    invoke-interface {v2, v5, v0, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LCe;

    .line 263
    array-length v0, v0

    if-nez v0, :cond_a7

    .line 266
    if-nez v1, :cond_9c

    .line 267
    invoke-direct {p0, v5}, Lth;->a(I)V

    .line 269
    :cond_9c
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->i()V

    .line 276
    :goto_a1
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->postInvalidate()V

    .line 291
    :cond_a6
    :goto_a6
    return v3

    .line 273
    :cond_a7
    invoke-direct {p0, v5}, Lth;->a(I)I

    move-result v0

    .line 274
    iget-object v1, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(I)V

    goto :goto_a1

    .line 278
    :cond_b1
    iget-object v0, p0, Lth;->a:LwF;

    if-eqz v0, :cond_a6

    .line 286
    :cond_b5
    invoke-virtual {p0}, Lth;->a()V

    .line 288
    invoke-direct {p0, v2}, Lth;->a(I)V

    .line 289
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->i()V

    .line 290
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->postInvalidate()V

    move v3, v2

    .line 291
    goto :goto_a6

    :cond_c7
    move v0, v1

    goto :goto_70
.end method

.method public b()V
    .registers 3

    .prologue
    .line 403
    iget-object v0, p0, Lth;->a:LwF;

    invoke-interface {v0}, LwF;->a()LuV;

    move-result-object v1

    .line 404
    invoke-interface {v1}, LuV;->b()V

    .line 406
    :try_start_9
    iget-object v0, p0, Lth;->a:LvZ;

    invoke-interface {v0}, LvZ;->k()V
    :try_end_e
    .catchall {:try_start_9 .. :try_end_e} :catchall_12

    .line 408
    invoke-interface {v1}, LuV;->c()V

    .line 410
    return-void

    .line 408
    :catchall_12
    move-exception v0

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public b(Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;)V
    .registers 5
    .parameter

    .prologue
    .line 144
    const-string v0, "KixDiscussionAnchorManager"

    const-string v1, "onDocosSpanRemoved"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    invoke-virtual {p1}, Lcom/google/android/apps/docs/editors/kix/spans/DocosSpan;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 146
    iget-object v2, p0, Lth;->a:Lalt;

    invoke-interface {v2, v0, p1}, Lalt;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_f

    .line 148
    :cond_21
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 85
    if-eqz p1, :cond_a

    .line 86
    new-instance v0, Ltd;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Ltd;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lth;->a:Ltd;

    .line 88
    :cond_a
    return-void
.end method

.method public c()V
    .registers 6

    .prologue
    .line 154
    const-string v0, "KixDiscussionAnchorManager"

    const-string v1, "onRedrawFinished"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    if-nez v0, :cond_11

    .line 157
    iget-object v0, p0, Lth;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 187
    :goto_10
    return-void

    .line 160
    :cond_11
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->a()Ljava/util/SortedSet;

    move-result-object v0

    .line 161
    if-nez v0, :cond_1f

    .line 164
    iget-object v0, p0, Lth;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_10

    .line 167
    :cond_1f
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 168
    sget-object v2, Lmz;->a:Lagv;

    invoke-static {v0, v2}, LajB;->a(Ljava/lang/Iterable;Lagv;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2e
    :goto_2e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 169
    invoke-interface {v0}, Lmz;->b()Ljava/lang/String;

    move-result-object v3

    .line 170
    if-eqz v3, :cond_2e

    invoke-interface {v0}, Lmz;->d()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 172
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2e

    .line 175
    :cond_4a
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 176
    iget-object v0, p0, Lth;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_55
    :goto_55
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 177
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_55

    .line 178
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_55

    .line 181
    :cond_6b
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0}, Lth;->a(Ljava/util/Set;Z)V

    .line 182
    iget-object v0, p0, Lth;->a:Ljava/lang/String;

    if-eqz v0, :cond_85

    iget-object v0, p0, Lth;->a:Ljava/util/Set;

    iget-object v1, p0, Lth;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_85

    .line 183
    iget-object v0, p0, Lth;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lth;->d(Ljava/lang/String;)V

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lth;->a:Ljava/lang/String;

    .line 186
    :cond_85
    iget-object v0, p0, Lth;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_10
.end method

.method public c(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 428
    invoke-direct {p0, p1}, Lth;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 430
    invoke-direct {p0, p1}, Lth;->d(Ljava/lang/String;)V

    .line 435
    :goto_9
    return-void

    .line 433
    :cond_a
    iput-object p1, p0, Lth;->a:Ljava/lang/String;

    goto :goto_9
.end method

.method public d()V
    .registers 2

    .prologue
    .line 459
    invoke-direct {p0}, Lth;->e()V

    .line 460
    iget-object v0, p0, Lth;->a:Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/DiscussionCoordinator;->w()V

    .line 461
    return-void
.end method
