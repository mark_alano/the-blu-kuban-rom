.class public final enum Ltm;
.super Ljava/lang/Enum;
.source "BaseDiscussionStateMachineFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltm;

.field private static final synthetic a:[Ltm;

.field public static final enum b:Ltm;

.field public static final enum c:Ltm;

.field public static final enum d:Ltm;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 44
    new-instance v0, Ltm;

    const-string v1, "NO_DISCUSSION"

    const-string v2, "noDiscussionStateMachineFragment"

    invoke-direct {v0, v1, v3, v2}, Ltm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ltm;->a:Ltm;

    .line 45
    new-instance v0, Ltm;

    const-string v1, "ALL"

    const-string v2, "allDiscussionsStateMachineFragment"

    invoke-direct {v0, v1, v4, v2}, Ltm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ltm;->b:Ltm;

    .line 46
    new-instance v0, Ltm;

    const-string v1, "PAGER"

    const-string v2, "pagerDiscussionStateMachineFragment"

    invoke-direct {v0, v1, v5, v2}, Ltm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ltm;->c:Ltm;

    .line 47
    new-instance v0, Ltm;

    const-string v1, "EDIT"

    const-string v2, "editCommentStateMachineFragment"

    invoke-direct {v0, v1, v6, v2}, Ltm;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Ltm;->d:Ltm;

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [Ltm;

    sget-object v1, Ltm;->a:Ltm;

    aput-object v1, v0, v3

    sget-object v1, Ltm;->b:Ltm;

    aput-object v1, v0, v4

    sget-object v1, Ltm;->c:Ltm;

    aput-object v1, v0, v5

    sget-object v1, Ltm;->d:Ltm;

    aput-object v1, v0, v6

    sput-object v0, Ltm;->a:[Ltm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput-object p3, p0, Ltm;->a:Ljava/lang/String;

    .line 53
    sget-object v0, Lcom/google/android/apps/docs/editors/discussion/statefragments/BaseDiscussionStateMachineFragment;->a:Ljava/util/Map;

    invoke-interface {v0, p3, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltm;
    .registers 2
    .parameter

    .prologue
    .line 43
    const-class v0, Ltm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltm;

    return-object v0
.end method

.method public static values()[Ltm;
    .registers 1

    .prologue
    .line 43
    sget-object v0, Ltm;->a:[Ltm;

    invoke-virtual {v0}, [Ltm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltm;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Ltm;->a:Ljava/lang/String;

    return-object v0
.end method
