.class Ltv;
.super Landroid/widget/ArrayAdapter;
.source "AllDiscussionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/util/List",
        "<",
        "Lmz;",
        ">;>;"
    }
.end annotation


# instance fields
.field private a:I

.field private a:LAQ;

.field private final a:Landroid/text/SpannableStringBuilder;

.field private final a:Landroid/view/View$OnClickListener;

.field private a:Landroid/view/View;

.field private a:Landroid/widget/TextView;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lmz;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 58
    sget v0, LsF;->discussion_list_element_all_discussions:I

    sget v1, LsD;->comment_container_collapsed_text:I

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 35
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    iput-object v0, p0, Ltv;->a:Landroid/text/SpannableStringBuilder;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Ltv;->a:LAQ;

    .line 60
    invoke-static {p2}, LsY;->a(Landroid/widget/AdapterView$OnItemClickListener;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Ltv;->a:Landroid/view/View$OnClickListener;

    .line 61
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 142
    sget v0, LsD;->discussion_all_discussion_single_discussion_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv;->a:Landroid/view/View;

    .line 144
    sget v0, LsD;->discussion_is_resolved:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv;->a:Landroid/widget/TextView;

    .line 145
    sget v0, LsD;->comment_container_first:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv;->b:Landroid/view/View;

    .line 146
    sget v0, LsD;->comment_container_collapsed_replies:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv;->c:Landroid/view/View;

    .line 148
    sget v0, LsD;->comment_container_collapsed_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ltv;->b:Landroid/widget/TextView;

    .line 150
    sget v0, LsD;->comment_separator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv;->d:Landroid/view/View;

    .line 151
    sget v0, LsD;->comment_container_last:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ltv;->e:Landroid/view/View;

    .line 153
    invoke-virtual {p0, p2}, Ltv;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Ltv;->a:Ljava/util/List;

    .line 154
    iget-object v0, p0, Ltv;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Ltv;->a:I

    .line 155
    return-void
.end method

.method private a(Landroid/content/Context;)Z
    .registers 8
    .parameter

    .prologue
    const/16 v5, 0xd

    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 163
    iget-object v0, p0, Ltv;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    invoke-interface {v0}, Lmz;->d()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 164
    const/4 v0, 0x1

    .line 165
    iget-object v1, p0, Ltv;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LsC;->discussion_all_discussions_discussion_resolved_background:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 167
    iget-object v1, p0, Ltv;->c:Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LsC;->discussion_all_discussions_collapsed_replies_resolved_background:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 177
    :goto_32
    iget-object v1, p0, Ltv;->a:Landroid/view/View;

    invoke-virtual {v1, v5, v4, v5, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 178
    return v0

    .line 171
    :cond_38
    iget-object v0, p0, Ltv;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LsC;->discussion_all_discussions_discussion_open_background:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 173
    iget-object v0, p0, Ltv;->c:Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, LsC;->discussion_all_discussions_collapsed_replies_open_background:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move v0, v1

    goto :goto_32
.end method


# virtual methods
.method public a(LAQ;)V
    .registers 2
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Ltv;->a:LAQ;

    .line 65
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Ltv;->a:LAQ;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 15
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 73
    if-nez p2, :cond_1c

    .line 74
    const-string v0, "AllDiscussionsAdapter"

    const-string v1, "Discussion View is null"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, LsF;->discussion_list_element_all_discussions:I

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 79
    :cond_1c
    invoke-direct {p0, p2, p1}, Ltv;->a(Landroid/view/View;I)V

    .line 81
    sget v0, LsD;->adapter_position_tag:I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 82
    iget-object v0, p0, Ltv;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Ltv;->a(Landroid/content/Context;)Z

    move-result v8

    .line 87
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ltv;->a:LAQ;

    iget-object v2, p0, Ltv;->b:Landroid/view/View;

    iget-object v3, p0, Ltv;->a:Landroid/view/View$OnClickListener;

    iget-object v5, p0, Ltv;->a:Ljava/util/List;

    invoke-interface {v5, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmz;

    move v7, p1

    invoke-static/range {v0 .. v7}, LsY;->a(Landroid/content/Context;LAQ;Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lmz;ZI)V

    .line 89
    iget-object v0, p0, Ltv;->b:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 91
    iget v0, p0, Ltv;->a:I

    if-le v0, v6, :cond_108

    .line 93
    if-eqz v8, :cond_b3

    .line 95
    iget-object v0, p0, Ltv;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->clear()V

    .line 96
    iget-object v0, p0, Ltv;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Ltv;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, LsH;->discussion_resolved:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 97
    iget-object v0, p0, Ltv;->a:Landroid/text/SpannableStringBuilder;

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    iget-object v2, p0, Ltv;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/16 v3, 0x21

    invoke-virtual {v0, v1, v9, v2, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 99
    iget-object v0, p0, Ltv;->a:Landroid/widget/TextView;

    iget-object v1, p0, Ltv;->a:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v0, p0, Ltv;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Ltv;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, LsH;->discussion_collapsed_replies:I

    new-array v3, v6, [Ljava/lang/Object;

    iget v4, p0, Ltv;->a:I

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Ltv;->c:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Ltv;->d:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Ltv;->e:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 131
    :goto_b2
    return-object p2

    .line 108
    :cond_b3
    iget-object v0, p0, Ltv;->d:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 109
    iget v0, p0, Ltv;->a:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_102

    .line 111
    iget-object v0, p0, Ltv;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Ltv;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, LsH;->discussion_collapsed_replies:I

    new-array v3, v6, [Ljava/lang/Object;

    iget v5, p0, Ltv;->a:I

    add-int/lit8 v5, v5, -0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Ltv;->c:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 119
    :goto_dd
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ltv;->a:LAQ;

    iget-object v2, p0, Ltv;->e:Landroid/view/View;

    iget-object v3, p0, Ltv;->a:Landroid/view/View$OnClickListener;

    iget-object v5, p0, Ltv;->a:Ljava/util/List;

    iget v7, p0, Ltv;->a:I

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lmz;

    move v7, p1

    invoke-static/range {v0 .. v7}, LsY;->a(Landroid/content/Context;LAQ;Landroid/view/View;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lmz;ZI)V

    .line 121
    iget-object v0, p0, Ltv;->e:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Ltv;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_b2

    .line 116
    :cond_102
    iget-object v0, p0, Ltv;->c:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_dd

    .line 127
    :cond_108
    iget-object v0, p0, Ltv;->d:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Ltv;->c:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Ltv;->e:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_b2
.end method
