.class public final enum Ltx;
.super Ljava/lang/Enum;
.source "AllDiscussionsFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Ltx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Ltx;

.field private static final synthetic a:[Ltx;

.field public static final enum b:Ltx;

.field public static final enum c:Ltx;

.field public static final enum d:Ltx;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v0, Ltx;

    const-string v1, "NOT_INITIALIZED"

    invoke-direct {v0, v1, v2}, Ltx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltx;->a:Ltx;

    .line 37
    new-instance v0, Ltx;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Ltx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltx;->b:Ltx;

    .line 38
    new-instance v0, Ltx;

    const-string v1, "NO_COMMENTS"

    invoke-direct {v0, v1, v4}, Ltx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltx;->c:Ltx;

    .line 39
    new-instance v0, Ltx;

    const-string v1, "LIST"

    invoke-direct {v0, v1, v5}, Ltx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ltx;->d:Ltx;

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [Ltx;

    sget-object v1, Ltx;->a:Ltx;

    aput-object v1, v0, v2

    sget-object v1, Ltx;->b:Ltx;

    aput-object v1, v0, v3

    sget-object v1, Ltx;->c:Ltx;

    aput-object v1, v0, v4

    sget-object v1, Ltx;->d:Ltx;

    aput-object v1, v0, v5

    sput-object v0, Ltx;->a:[Ltx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ltx;
    .registers 2
    .parameter

    .prologue
    .line 35
    const-class v0, Ltx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ltx;

    return-object v0
.end method

.method public static values()[Ltx;
    .registers 1

    .prologue
    .line 35
    sget-object v0, Ltx;->a:[Ltx;

    invoke-virtual {v0}, [Ltx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ltx;

    return-object v0
.end method
