.class final enum LuC;
.super Ljava/lang/Enum;
.source "FontPicker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LuC;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LuC;

.field private static final synthetic a:[LuC;

.field public static final enum b:LuC;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 89
    new-instance v0, LuC;

    const-string v1, "FONT_NAME"

    invoke-direct {v0, v1, v2}, LuC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LuC;->a:LuC;

    .line 90
    new-instance v0, LuC;

    const-string v1, "FONT_SIZE"

    invoke-direct {v0, v1, v3}, LuC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LuC;->b:LuC;

    .line 88
    const/4 v0, 0x2

    new-array v0, v0, [LuC;

    sget-object v1, LuC;->a:LuC;

    aput-object v1, v0, v2

    sget-object v1, LuC;->b:LuC;

    aput-object v1, v0, v3

    sput-object v0, LuC;->a:[LuC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LuC;
    .registers 2
    .parameter

    .prologue
    .line 88
    const-class v0, LuC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LuC;

    return-object v0
.end method

.method public static values()[LuC;
    .registers 1

    .prologue
    .line 88
    sget-object v0, LuC;->a:[LuC;

    invoke-virtual {v0}, [LuC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LuC;

    return-object v0
.end method
