.class public Lub;
.super LaD;
.source "PagerDiscussionAdapter.java"


# instance fields
.field private final a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

.field private a:LtX;

.field private final a:Lud;

.field private final a:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;Z)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, LaD;-><init>()V

    .line 42
    iput-object p1, p0, Lub;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    .line 43
    new-instance v0, Lud;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lud;-><init>(Lub;Luc;)V

    iput-object v0, p0, Lub;->a:Lud;

    .line 44
    iput-boolean p2, p0, Lub;->a:Z

    .line 45
    return-void
.end method

.method static synthetic a(Lub;)Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lub;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    return-object v0
.end method

.method private a(I)LtX;
    .registers 5
    .parameter

    .prologue
    .line 142
    iget-object v0, p0, Lub;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Ljava/util/List;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_e

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lt p1, v1, :cond_10

    .line 144
    :cond_e
    const/4 v0, 0x0

    .line 151
    :goto_f
    return-object v0

    .line 146
    :cond_10
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    .line 149
    iget-object v1, p0, Lub;->a:Lud;

    invoke-interface {v0}, Lmz;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lud;->a(Lud;Ljava/lang/String;)LtX;

    move-result-object v1

    .line 150
    new-instance v2, Ltd;

    invoke-direct {v2, v0}, Ltd;-><init>(Lmz;)V

    invoke-virtual {v1, v2}, LtX;->a(Ltd;)V

    move-object v0, v1

    .line 151
    goto :goto_f
.end method

.method static synthetic a(Lub;)Z
    .registers 2
    .parameter

    .prologue
    .line 25
    iget-boolean v0, p0, Lub;->a:Z

    return v0
.end method


# virtual methods
.method public a()I
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lub;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Ljava/util/List;

    move-result-object v0

    .line 50
    if-nez v0, :cond_a

    const/4 v0, 0x0

    :goto_9
    return v0

    :cond_a
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_9
.end method

.method public a(Ljava/lang/Object;)I
    .registers 7
    .parameter

    .prologue
    const/4 v2, -0x2

    .line 83
    iget-object v0, p0, Lub;->a:Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;->a()Ljava/util/List;

    move-result-object v3

    .line 84
    check-cast p1, LtX;

    .line 85
    invoke-virtual {p1}, LtX;->a()Ljava/lang/String;

    move-result-object v4

    .line 86
    if-eqz v4, :cond_11

    if-nez v3, :cond_13

    :cond_11
    move v1, v2

    .line 94
    :cond_12
    :goto_12
    return v1

    .line 89
    :cond_13
    const/4 v0, 0x0

    move v1, v0

    :goto_15
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2f

    .line 90
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmz;

    invoke-interface {v0}, Lmz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 89
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_15

    :cond_2f
    move v1, v2

    .line 94
    goto :goto_12
.end method

.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p2}, Lub;->a(I)LtX;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, LtX;->a()Landroid/view/View;

    move-result-object v2

    .line 58
    if-eqz v2, :cond_19

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 60
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 62
    :cond_19
    if-eqz v2, :cond_1e

    .line 63
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 65
    :cond_1e
    return-object v1
.end method

.method public a()LtX;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lub;->a:LtX;

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    check-cast p3, LtX;

    .line 71
    invoke-virtual {p3}, LtX;->a()Landroid/view/View;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_b

    .line 73
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 76
    :cond_b
    iget-object v0, p0, Lub;->a:Lud;

    invoke-virtual {p3}, LtX;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lud;->a(Lud;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public a(Ljava/util/SortedSet;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/SortedSet",
            "<",
            "Lmz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lub;->a:Lud;

    invoke-static {v0}, Lud;->a(Lud;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtX;

    .line 123
    invoke-virtual {v0, p1}, LtX;->a(Ljava/util/SortedSet;)V

    goto :goto_a

    .line 125
    :cond_1a
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 99
    check-cast p2, LtX;

    invoke-virtual {p2}, LtX;->a()Landroid/view/View;

    move-result-object v1

    .line 100
    if-eqz v1, :cond_c

    .line 101
    if-ne v1, p1, :cond_c

    const/4 v0, 0x1

    .line 103
    :cond_c
    return v0
.end method

.method public b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    check-cast p3, LtX;

    .line 110
    if-eqz p3, :cond_7

    .line 111
    invoke-virtual {p3}, LtX;->a()V

    .line 113
    :cond_7
    iput-object p3, p0, Lub;->a:LtX;

    .line 114
    return-void
.end method
