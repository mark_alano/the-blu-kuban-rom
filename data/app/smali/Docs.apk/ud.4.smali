.class Lud;
.super Ljava/lang/Object;
.source "PagerDiscussionAdapter.java"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LtX;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic a:Lub;


# direct methods
.method private constructor <init>(Lub;)V
    .registers 3
    .parameter

    .prologue
    .line 157
    iput-object p1, p0, Lud;->a:Lub;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lud;->a:Ljava/util/Map;

    return-void
.end method

.method synthetic constructor <init>(Lub;Luc;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lud;-><init>(Lub;)V

    return-void
.end method

.method private a()Ljava/util/Collection;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "LtX;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lud;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lud;)Ljava/util/Collection;
    .registers 2
    .parameter

    .prologue
    .line 157
    invoke-direct {p0}, Lud;->a()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)LtX;
    .registers 5
    .parameter

    .prologue
    .line 165
    iget-object v0, p0, Lud;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LtX;

    .line 166
    if-nez v0, :cond_20

    .line 167
    new-instance v0, LtX;

    iget-object v1, p0, Lud;->a:Lub;

    invoke-static {v1}, Lub;->a(Lub;)Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;

    move-result-object v1

    iget-object v2, p0, Lud;->a:Lub;

    invoke-static {v2}, Lub;->a(Lub;)Z

    move-result v2

    invoke-direct {v0, v1, v2}, LtX;-><init>(Lcom/google/android/apps/docs/editors/discussion/uifragments/PagerDiscussionFragment;Z)V

    .line 168
    iget-object v1, p0, Lud;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    :cond_20
    return-object v0
.end method

.method static synthetic a(Lud;Ljava/lang/String;)LtX;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lud;->a(Ljava/lang/String;)LtX;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lud;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    return-void
.end method

.method static synthetic a(Lud;Ljava/lang/String;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lud;->a(Ljava/lang/String;)V

    return-void
.end method
