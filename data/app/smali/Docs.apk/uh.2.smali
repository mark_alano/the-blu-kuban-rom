.class public final enum Luh;
.super Ljava/lang/Enum;
.source "PagerDiscussionFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Luh;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Luh;

.field private static final synthetic a:[Luh;

.field public static final enum b:Luh;

.field public static final enum c:Luh;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    new-instance v0, Luh;

    const-string v1, "NOT_INITIALIZED"

    invoke-direct {v0, v1, v2}, Luh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luh;->a:Luh;

    .line 62
    new-instance v0, Luh;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, Luh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luh;->b:Luh;

    .line 63
    new-instance v0, Luh;

    const-string v1, "PAGE"

    invoke-direct {v0, v1, v4}, Luh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Luh;->c:Luh;

    .line 60
    const/4 v0, 0x3

    new-array v0, v0, [Luh;

    sget-object v1, Luh;->a:Luh;

    aput-object v1, v0, v2

    sget-object v1, Luh;->b:Luh;

    aput-object v1, v0, v3

    sget-object v1, Luh;->c:Luh;

    aput-object v1, v0, v4

    sput-object v0, Luh;->a:[Luh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Luh;
    .registers 2
    .parameter

    .prologue
    .line 60
    const-class v0, Luh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Luh;

    return-object v0
.end method

.method public static values()[Luh;
    .registers 1

    .prologue
    .line 60
    sget-object v0, Luh;->a:[Luh;

    invoke-virtual {v0}, [Luh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Luh;

    return-object v0
.end method
