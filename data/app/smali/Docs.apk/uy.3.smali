.class Luy;
.super Ljava/lang/Object;
.source "FontPicker.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic a:Luw;


# direct methods
.method constructor <init>(Luw;Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 224
    iput-object p1, p0, Luy;->a:Luw;

    iput-object p2, p0, Luy;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 227
    sget v0, LsD;->font_family:I

    if-ne p2, v0, :cond_13

    .line 228
    iget-object v0, p0, Luy;->a:Luw;

    sget-object v1, LuC;->a:LuC;

    invoke-static {v0, v1}, Luw;->a(Luw;LuC;)LuC;

    .line 229
    iget-object v0, p0, Luy;->a:Luw;

    iget-object v1, p0, Luy;->a:Landroid/view/View;

    invoke-static {v0, v1}, Luw;->a(Luw;Landroid/view/View;)V

    .line 236
    :goto_12
    return-void

    .line 230
    :cond_13
    sget v0, LsD;->font_size:I

    if-ne p2, v0, :cond_26

    .line 231
    iget-object v0, p0, Luy;->a:Luw;

    sget-object v1, LuC;->b:LuC;

    invoke-static {v0, v1}, Luw;->a(Luw;LuC;)LuC;

    .line 232
    iget-object v0, p0, Luy;->a:Luw;

    iget-object v1, p0, Luy;->a:Landroid/view/View;

    invoke-static {v0, v1}, Luw;->b(Luw;Landroid/view/View;)V

    goto :goto_12

    .line 234
    :cond_26
    const-string v0, "FontPicker"

    const-string v1, "Unknown font mode entered."

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_12
.end method
