.class public LvE;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements LvB;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "LvB;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2507
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 2508
    return-void
.end method

.method static a(Lvw;J)LvE;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 2503
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, LvE;

    invoke-direct {v0, p0, p1, p2}, LvE;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()LvZ;
    .registers 4

    .prologue
    .line 2569
    invoke-virtual {p0}, LvE;->a()J

    move-result-wide v0

    .line 2570
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(J)J

    move-result-wide v1

    .line 2573
    iget-object v0, p0, LvE;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lwa;->a(Lvw;J)Lwa;

    move-result-object v0

    return-object v0
.end method

.method public a()LwF;
    .registers 4

    .prologue
    .line 2535
    invoke-virtual {p0}, LvE;->a()J

    move-result-wide v0

    .line 2536
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(J)J

    move-result-wide v1

    .line 2539
    iget-object v0, p0, LvE;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, LwG;->a(Lvw;J)LwG;

    move-result-object v0

    return-object v0
.end method

.method public a()LwY;
    .registers 4

    .prologue
    .line 2586
    invoke-virtual {p0}, LvE;->a()J

    move-result-wide v0

    .line 2587
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->e(J)J

    move-result-wide v1

    .line 2590
    iget-object v0, p0, LvE;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, LwZ;->a(Lvw;J)LwZ;

    move-result-object v0

    return-object v0
.end method

.method public a()Lxa;
    .registers 4

    .prologue
    .line 2552
    invoke-virtual {p0}, LvE;->a()J

    move-result-wide v0

    .line 2553
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(J)J

    move-result-wide v1

    .line 2556
    iget-object v0, p0, LvE;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lxb;->a(Lvw;J)Lxb;

    move-result-object v0

    return-object v0
.end method

.method public a(I)V
    .registers 4
    .parameter

    .prologue
    .line 2631
    invoke-virtual {p0}, LvE;->a()J

    move-result-wide v0

    .line 2632
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JI)V

    .line 2633
    return-void
.end method

.method public c()V
    .registers 3

    .prologue
    .line 2521
    invoke-virtual {p0}, LvE;->a()J

    move-result-wide v0

    .line 2522
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(J)V

    .line 2523
    return-void
.end method

.method public d()V
    .registers 3

    .prologue
    .line 2603
    invoke-virtual {p0}, LvE;->a()J

    move-result-wide v0

    .line 2604
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(J)V

    .line 2605
    return-void
.end method

.method public e()V
    .registers 3

    .prologue
    .line 2617
    invoke-virtual {p0}, LvE;->a()J

    move-result-wide v0

    .line 2618
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(J)V

    .line 2619
    return-void
.end method
