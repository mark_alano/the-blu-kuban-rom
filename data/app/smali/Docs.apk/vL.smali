.class public LvL;
.super Ljava/lang/Object;
.source "Kix.java"

# interfaces
.implements Lcom/google/android/apps/docs/editors/jsvm/JSCallback;


# instance fields
.field private a:LvK;

.field protected a:Lvw;


# direct methods
.method public constructor <init>(Lvw;LvK;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 5753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5754
    iput-object p1, p0, LvL;->a:Lvw;

    .line 5755
    iput-object p2, p0, LvL;->a:LvK;

    .line 5756
    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 5787
    iget-object v0, p0, LvL;->a:LvK;

    invoke-interface {v0}, LvK;->a()Ljava/lang/String;

    move-result-object v0

    .line 5790
    return-object v0
.end method

.method public setBackgroundColor(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 5775
    iget-object v0, p0, LvL;->a:LvK;

    invoke-interface {v0, p1}, LvK;->a(Ljava/lang/String;)V

    .line 5776
    return-void
.end method

.method public setBorderColor(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 5783
    iget-object v0, p0, LvL;->a:LvK;

    invoke-interface {v0, p1}, LvK;->b(Ljava/lang/String;)V

    .line 5784
    return-void
.end method

.method public setBorderWidth(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5779
    iget-object v0, p0, LvL;->a:LvK;

    invoke-interface {v0, p1, p2, p3, p4}, LvK;->c(IIII)V

    .line 5780
    return-void
.end method

.method public setHeight(I)V
    .registers 3
    .parameter

    .prologue
    .line 5763
    iget-object v0, p0, LvL;->a:LvK;

    invoke-interface {v0, p1}, LvK;->b(I)V

    .line 5764
    return-void
.end method

.method public setMargin(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5767
    iget-object v0, p0, LvL;->a:LvK;

    invoke-interface {v0, p1, p2, p3, p4}, LvK;->a(IIII)V

    .line 5768
    return-void
.end method

.method public setPadding(IIII)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 5771
    iget-object v0, p0, LvL;->a:LvK;

    invoke-interface {v0, p1, p2, p3, p4}, LvK;->b(IIII)V

    .line 5772
    return-void
.end method

.method public setWidth(I)V
    .registers 3
    .parameter

    .prologue
    .line 5759
    iget-object v0, p0, LvL;->a:LvK;

    invoke-interface {v0, p1}, LvK;->a(I)V

    .line 5760
    return-void
.end method
