.class public Lvc;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements Lvb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "Lvb;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1899
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 1900
    return-void
.end method

.method static a(Lvw;J)Lvc;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1895
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, Lvc;

    invoke-direct {v0, p0, p1, p2}, Lvc;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a(D)I
    .registers 5
    .parameter

    .prologue
    .line 1972
    invoke-virtual {p0}, Lvc;->a()J

    move-result-wide v0

    .line 1973
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JD)I

    move-result v0

    .line 1976
    return v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LvC;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1913
    invoke-virtual {p0}, Lvc;->a()J

    move-result-wide v0

    .line 1914
    invoke-static {v0, v1, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v1

    .line 1917
    iget-object v0, p0, Lvc;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, LvD;->a(Lvw;J)LvD;

    move-result-object v0

    return-object v0
.end method

.method public a(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1958
    invoke-virtual {p0}, Lvc;->a()J

    move-result-wide v0

    .line 1959
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JII)V

    .line 1960
    return-void
.end method

.method public a(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1944
    invoke-virtual {p0}, Lvc;->a()J

    move-result-wide v0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    .line 1945
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1946
    return-void
.end method
