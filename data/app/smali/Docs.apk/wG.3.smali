.class public LwG;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements LwF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "LwF;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4061
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 4062
    return-void
.end method

.method static a(Lvw;J)LwG;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 4057
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, LwG;

    invoke-direct {v0, p0, p1, p2}, LwG;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()I
    .registers 3

    .prologue
    .line 4075
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4076
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->i(J)I

    move-result v0

    .line 4079
    return v0
.end method

.method public a(I)I
    .registers 4
    .parameter

    .prologue
    .line 4313
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4314
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JI)I

    move-result v0

    .line 4317
    return v0
.end method

.method public a(Ljava/lang/String;)I
    .registers 4
    .parameter

    .prologue
    .line 4449
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4450
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JLjava/lang/String;)I

    move-result v0

    .line 4453
    return v0
.end method

.method public a(I)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 4330
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4331
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JI)Ljava/lang/String;

    move-result-object v0

    .line 4334
    return-object v0
.end method

.method public a(II)Ljava/lang/String;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 4092
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4093
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JII)Ljava/lang/String;

    move-result-object v0

    .line 4096
    return-object v0
.end method

.method public a(I)Lvo;
    .registers 5
    .parameter

    .prologue
    .line 4194
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4195
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(JI)J

    move-result-wide v1

    .line 4198
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lvp;->a(Lvw;J)Lvp;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lvq;
    .registers 5
    .parameter

    .prologue
    .line 4517
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4518
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->g(JI)J

    move-result-wide v1

    .line 4521
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lvr;->a(Lvw;J)Lvr;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lvs;
    .registers 5
    .parameter

    .prologue
    .line 4398
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4399
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->f(JI)J

    move-result-wide v1

    .line 4402
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lvt;->a(Lvw;J)Lvt;

    move-result-object v0

    return-object v0
.end method

.method public a(II)Lvs;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 4415
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4416
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JII)J

    move-result-wide v1

    .line 4419
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lvt;->a(Lvw;J)Lvt;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lxc;
    .registers 5
    .parameter

    .prologue
    .line 4296
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4297
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(JI)J

    move-result-wide v1

    .line 4300
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lxd;->a(Lvw;J)Lxd;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lxg;
    .registers 5
    .parameter

    .prologue
    .line 4262
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4263
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)J

    move-result-wide v1

    .line 4266
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lxh;->a(Lvw;J)Lxh;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lxi;
    .registers 5
    .parameter

    .prologue
    .line 4143
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4144
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JI)J

    move-result-wide v1

    .line 4147
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lxj;->a(Lvw;J)Lxj;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .registers 5

    .prologue
    .line 4116
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->e(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4117
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v1

    const/16 v3, 0x53

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->e(Lvw;Z)Z

    .line 4120
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->f(Lvw;Z)Z

    .line 4122
    :cond_23
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->f(Lvw;)Z

    move-result v0

    return v0
.end method

.method public a()[I
    .registers 3

    .prologue
    .line 4160
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4161
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(J)[I

    move-result-object v0

    .line 4164
    return-object v0
.end method

.method public a(II)[I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 4126
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4127
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JII)[I

    move-result-object v0

    .line 4130
    return-object v0
.end method

.method public a(I)[Lvu;
    .registers 5
    .parameter

    .prologue
    .line 4364
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4365
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(JI)[J

    move-result-object v0

    .line 4368
    new-instance v1, LwH;

    invoke-direct {v1, p0}, LwH;-><init>(LwG;)V

    const-class v2, Lvu;

    invoke-virtual {p0, v1, v2, v0}, LwG;->a(LuW;Ljava/lang/Class;[J)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lvu;

    return-object v0
.end method

.method public b(I)I
    .registers 4
    .parameter

    .prologue
    .line 4381
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4382
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(JI)I

    move-result v0

    .line 4385
    return v0
.end method

.method public b(I)Lxg;
    .registers 5
    .parameter

    .prologue
    .line 4347
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4348
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->e(JI)J

    move-result-wide v1

    .line 4351
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0, v1, v2}, Lxh;->a(Lvw;J)Lxh;

    move-result-object v0

    return-object v0
.end method

.method public b()Z
    .registers 5

    .prologue
    .line 4133
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->g(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4134
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v1

    const/16 v3, 0x54

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->g(Lvw;Z)Z

    .line 4137
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->h(Lvw;Z)Z

    .line 4139
    :cond_23
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->h(Lvw;)Z

    move-result v0

    return v0
.end method

.method public b()[I
    .registers 3

    .prologue
    .line 4228
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4229
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(J)[I

    move-result-object v0

    .line 4232
    return-object v0
.end method

.method public b(II)[I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 4177
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4178
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->b(JII)[I

    move-result-object v0

    .line 4181
    return-object v0
.end method

.method public c(I)I
    .registers 4
    .parameter

    .prologue
    .line 4432
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4433
    invoke-static {v0, v1, p1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)I

    move-result v0

    .line 4436
    return v0
.end method

.method public c()Z
    .registers 5

    .prologue
    .line 4167
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->i(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4168
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v1

    const/16 v3, 0x56

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->i(Lvw;Z)Z

    .line 4171
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->j(Lvw;Z)Z

    .line 4173
    :cond_23
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->j(Lvw;)Z

    move-result v0

    return v0
.end method

.method public c()[I
    .registers 3

    .prologue
    .line 4279
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4280
    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(J)[I

    move-result-object v0

    .line 4283
    return-object v0
.end method

.method public c(II)[I
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 4245
    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v0

    .line 4246
    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JII)[I

    move-result-object v0

    .line 4249
    return-object v0
.end method

.method public d()Z
    .registers 5

    .prologue
    .line 4184
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->k(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4185
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v1

    const/16 v3, 0x57

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->k(Lvw;Z)Z

    .line 4188
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->l(Lvw;Z)Z

    .line 4190
    :cond_23
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->l(Lvw;)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .registers 5

    .prologue
    .line 4235
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->m(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4236
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v1

    const/16 v3, 0x5a

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->m(Lvw;Z)Z

    .line 4239
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->n(Lvw;Z)Z

    .line 4241
    :cond_23
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->n(Lvw;)Z

    move-result v0

    return v0
.end method

.method public f()Z
    .registers 5

    .prologue
    .line 4303
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->o(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4304
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v1

    const/16 v3, 0x5e

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->o(Lvw;Z)Z

    .line 4307
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->p(Lvw;Z)Z

    .line 4309
    :cond_23
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->p(Lvw;)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .registers 5

    .prologue
    .line 4405
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->q(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4406
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v1

    const/16 v3, 0x64

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->q(Lvw;Z)Z

    .line 4409
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->r(Lvw;Z)Z

    .line 4411
    :cond_23
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->r(Lvw;)Z

    move-result v0

    return v0
.end method

.method public h()Z
    .registers 5

    .prologue
    .line 4439
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->s(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4440
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v1

    const/16 v3, 0x66

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->s(Lvw;Z)Z

    .line 4443
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->t(Lvw;Z)Z

    .line 4445
    :cond_23
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->t(Lvw;)Z

    move-result v0

    return v0
.end method

.method public i()Z
    .registers 5

    .prologue
    .line 4507
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->u(Lvw;)Z

    move-result v0

    if-nez v0, :cond_23

    .line 4508
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-virtual {p0}, LwG;->a()J

    move-result-wide v1

    const/16 v3, 0x6a

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->c(JI)Z

    move-result v1

    invoke-static {v0, v1}, Lvw;->u(Lvw;Z)Z

    .line 4511
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lvw;->v(Lvw;Z)Z

    .line 4513
    :cond_23
    iget-object v0, p0, LwG;->a:LuV;

    check-cast v0, Lvw;

    invoke-static {v0}, Lvw;->v(Lvw;)Z

    move-result v0

    return v0
.end method
