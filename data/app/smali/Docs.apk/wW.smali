.class public LwW;
.super Ljava/lang/Object;
.source "Kix.java"

# interfaces
.implements Lcom/google/android/apps/docs/editors/jsvm/JSCallback;


# instance fields
.field protected a:Lvw;

.field private a:LwV;


# direct methods
.method public constructor <init>(Lvw;LwV;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 6516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6517
    iput-object p1, p0, LwW;->a:Lvw;

    .line 6518
    iput-object p2, p0, LwW;->a:LwV;

    .line 6519
    return-void
.end method


# virtual methods
.method public provideCellContentRenderer()J
    .registers 3

    .prologue
    .line 6543
    iget-object v0, p0, LwW;->a:LwV;

    invoke-interface {v0}, LwV;->b()LvV;

    move-result-object v0

    .line 6546
    invoke-static {v0}, LuZ;->a(LuY;)J

    move-result-wide v0

    return-wide v0
.end method

.method public provideCellRenderer()J
    .registers 3

    .prologue
    .line 6536
    iget-object v0, p0, LwW;->a:LwV;

    invoke-interface {v0}, LwV;->a()LvN;

    move-result-object v0

    .line 6539
    invoke-static {v0}, LuZ;->a(LuY;)J

    move-result-wide v0

    return-wide v0
.end method

.method public provideRowRenderer()J
    .registers 3

    .prologue
    .line 6529
    iget-object v0, p0, LwW;->a:LwV;

    invoke-interface {v0}, LwV;->a()LwQ;

    move-result-object v0

    .line 6532
    invoke-static {v0}, LuZ;->a(LuY;)J

    move-result-wide v0

    return-wide v0
.end method

.method public provideTableRenderer()J
    .registers 3

    .prologue
    .line 6522
    iget-object v0, p0, LwW;->a:LwV;

    invoke-interface {v0}, LwV;->a()LvV;

    move-result-object v0

    .line 6525
    invoke-static {v0}, LuZ;->a(LuY;)J

    move-result-wide v0

    return-wide v0
.end method
