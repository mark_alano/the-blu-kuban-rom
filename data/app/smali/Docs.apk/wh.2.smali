.class public Lwh;
.super Ljava/lang/Object;
.source "Kix.java"

# interfaces
.implements Lcom/google/android/apps/docs/editors/jsvm/JSCallback;


# instance fields
.field protected a:Lvw;

.field private a:Lwg;


# direct methods
.method public constructor <init>(Lvw;Lwg;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 6003
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6004
    iput-object p1, p0, Lwh;->a:Lvw;

    .line 6005
    iput-object p2, p0, Lwh;->a:Lwg;

    .line 6006
    return-void
.end method


# virtual methods
.method public changeSelection(IIZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 6009
    iget-object v0, p0, Lwh;->a:Lwg;

    invoke-interface {v0, p1, p2, p3, p4}, Lwg;->a(IIZZ)V

    .line 6010
    return-void
.end method

.method public reset()V
    .registers 2

    .prologue
    .line 6037
    iget-object v0, p0, Lwh;->a:Lwg;

    invoke-interface {v0}, Lwg;->d()V

    .line 6038
    return-void
.end method

.method public setDocumentDeleted()V
    .registers 2

    .prologue
    .line 6013
    iget-object v0, p0, Lwh;->a:Lwg;

    invoke-interface {v0}, Lwg;->a()V

    .line 6014
    return-void
.end method

.method public setImageUrl(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 6025
    iget-object v0, p0, Lwh;->a:Lwg;

    invoke-interface {v0, p1, p2}, Lwg;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 6026
    return-void
.end method

.method public setModelLoadComplete()V
    .registers 2

    .prologue
    .line 6017
    iget-object v0, p0, Lwh;->a:Lwg;

    invoke-interface {v0}, Lwg;->b()V

    .line 6018
    return-void
.end method

.method public setModelLoadFailed(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 6021
    iget-object v0, p0, Lwh;->a:Lwg;

    invoke-interface {v0, p1}, Lwg;->a(Ljava/lang/String;)V

    .line 6022
    return-void
.end method

.method public setSaveState(I)V
    .registers 3
    .parameter

    .prologue
    .line 6041
    iget-object v0, p0, Lwh;->a:Lwg;

    invoke-interface {v0, p1}, Lwg;->a(I)V

    .line 6042
    return-void
.end method

.method public updateModel()V
    .registers 2

    .prologue
    .line 6029
    iget-object v0, p0, Lwh;->a:Lwg;

    invoke-interface {v0}, Lwg;->c()V

    .line 6030
    return-void
.end method

.method public updateModel2(Z)V
    .registers 3
    .parameter

    .prologue
    .line 6033
    iget-object v0, p0, Lwh;->a:Lwg;

    invoke-interface {v0, p1}, Lwg;->a(Z)V

    .line 6034
    return-void
.end method
