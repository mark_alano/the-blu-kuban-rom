.class public Lwr;
.super Ljava/lang/Object;
.source "Kix.java"

# interfaces
.implements Lcom/google/android/apps/docs/editors/jsvm/JSCallback;


# instance fields
.field protected a:Lvw;

.field private a:Lwq;


# direct methods
.method public constructor <init>(Lvw;Lwq;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 6155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6156
    iput-object p1, p0, Lwr;->a:Lvw;

    .line 6157
    iput-object p2, p0, Lwr;->a:Lwq;

    .line 6158
    return-void
.end method


# virtual methods
.method public getContentBoundingBoxForSpacerIndex(I)J
    .registers 4
    .parameter

    .prologue
    .line 6200
    iget-object v0, p0, Lwr;->a:Lwq;

    invoke-interface {v0, p1}, Lwq;->a(I)Lxe;

    move-result-object v0

    .line 6203
    invoke-static {v0}, LuZ;->a(LuY;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getHeight(I)D
    .registers 4
    .parameter

    .prologue
    .line 6168
    iget-object v0, p0, Lwr;->a:Lwq;

    invoke-interface {v0, p1}, Lwq;->a(I)D

    move-result-wide v0

    .line 6171
    return-wide v0
.end method

.method public getNativeRendererId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 6186
    iget-object v0, p0, Lwr;->a:Lwq;

    invoke-interface {v0}, Lwq;->a()Ljava/lang/String;

    move-result-object v0

    .line 6189
    return-object v0
.end method

.method public getNumSpacers()I
    .registers 2

    .prologue
    .line 6175
    iget-object v0, p0, Lwr;->a:Lwq;

    invoke-interface {v0}, Lwq;->a()I

    move-result v0

    .line 6178
    return v0
.end method

.method public getSpacerIndexForCoordinates(IDD)I
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 6193
    iget-object v0, p0, Lwr;->a:Lwq;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lwq;->a(IDD)I

    move-result v0

    .line 6196
    return v0
.end method

.method public layout(IIDDJZ)J
    .registers 21
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 6161
    iget-object v2, p0, Lwr;->a:Lwq;

    iget-object v3, p0, Lwr;->a:Lvw;

    move-wide/from16 v0, p7

    invoke-static {v3, v0, v1}, LwL;->a(Lvw;J)LwL;

    move-result-object v9

    move v3, p1

    move v4, p2

    move-wide v5, p3

    move-wide/from16 v7, p5

    move/from16 v10, p9

    invoke-interface/range {v2 .. v10}, Lwq;->a(IIDDLwL;Z)Lwl;

    move-result-object v2

    .line 6164
    invoke-static {v2}, LuZ;->a(LuY;)J

    move-result-wide v2

    return-wide v2
.end method

.method public render(I)V
    .registers 3
    .parameter

    .prologue
    .line 6182
    iget-object v0, p0, Lwr;->a:Lwq;

    invoke-interface {v0, p1}, Lwq;->a(I)V

    .line 6183
    return-void
.end method
