.class public Lwz;
.super Ljava/lang/Object;
.source "Kix.java"

# interfaces
.implements Lcom/google/android/apps/docs/editors/jsvm/JSCallback;


# instance fields
.field protected a:Lvw;

.field private a:Lwy;


# direct methods
.method public constructor <init>(Lvw;Lwy;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 6287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6288
    iput-object p1, p0, Lwz;->a:Lvw;

    .line 6289
    iput-object p2, p0, Lwz;->a:Lwy;

    .line 6290
    return-void
.end method


# virtual methods
.method public getParagraphViewProvider()J
    .registers 3

    .prologue
    .line 6300
    iget-object v0, p0, Lwz;->a:Lwy;

    invoke-interface {v0}, Lwy;->b()Lwt;

    move-result-object v0

    .line 6303
    invoke-static {v0}, LuZ;->a(LuY;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTextBoxViewProvider()J
    .registers 3

    .prologue
    .line 6293
    iget-object v0, p0, Lwz;->a:Lwy;

    invoke-interface {v0}, Lwy;->a()Lwt;

    move-result-object v0

    .line 6296
    invoke-static {v0}, LuZ;->a(LuY;)J

    move-result-wide v0

    return-wide v0
.end method
