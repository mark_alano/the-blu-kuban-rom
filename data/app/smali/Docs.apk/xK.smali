.class final enum LxK;
.super Ljava/lang/Enum;
.source "FontManagerImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LxK;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:LxK;

.field private static final synthetic a:[LxK;

.field public static final enum b:LxK;

.field public static final enum c:LxK;

.field public static final enum d:LxK;

.field public static final enum e:LxK;


# instance fields
.field private final a:Landroid/graphics/Typeface;

.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 37
    new-instance v0, LxK;

    const-string v1, "ARIAL"

    const-string v2, "Arial"

    sget-object v3, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-direct {v0, v1, v4, v2, v3}, LxK;-><init>(Ljava/lang/String;ILjava/lang/String;Landroid/graphics/Typeface;)V

    sput-object v0, LxK;->a:LxK;

    .line 38
    new-instance v0, LxK;

    const-string v1, "COURIER_NEW"

    const-string v2, "Courier New"

    sget-object v3, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-direct {v0, v1, v5, v2, v3}, LxK;-><init>(Ljava/lang/String;ILjava/lang/String;Landroid/graphics/Typeface;)V

    sput-object v0, LxK;->b:LxK;

    .line 39
    new-instance v0, LxK;

    const-string v1, "DROID_SANS"

    const-string v2, "Droid Sans"

    sget-object v3, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-direct {v0, v1, v6, v2, v3}, LxK;-><init>(Ljava/lang/String;ILjava/lang/String;Landroid/graphics/Typeface;)V

    sput-object v0, LxK;->c:LxK;

    .line 40
    new-instance v0, LxK;

    const-string v1, "DROID_SERIF"

    const-string v2, "Droid Serif"

    sget-object v3, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-direct {v0, v1, v7, v2, v3}, LxK;-><init>(Ljava/lang/String;ILjava/lang/String;Landroid/graphics/Typeface;)V

    sput-object v0, LxK;->d:LxK;

    .line 41
    new-instance v0, LxK;

    const-string v1, "TIMES_NEW_ROMAN"

    const-string v2, "Times New Roman"

    sget-object v3, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-direct {v0, v1, v8, v2, v3}, LxK;-><init>(Ljava/lang/String;ILjava/lang/String;Landroid/graphics/Typeface;)V

    sput-object v0, LxK;->e:LxK;

    .line 36
    const/4 v0, 0x5

    new-array v0, v0, [LxK;

    sget-object v1, LxK;->a:LxK;

    aput-object v1, v0, v4

    sget-object v1, LxK;->b:LxK;

    aput-object v1, v0, v5

    sget-object v1, LxK;->c:LxK;

    aput-object v1, v0, v6

    sget-object v1, LxK;->d:LxK;

    aput-object v1, v0, v7

    sget-object v1, LxK;->e:LxK;

    aput-object v1, v0, v8

    sput-object v0, LxK;->a:[LxK;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Landroid/graphics/Typeface;)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/graphics/Typeface;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iput-object p3, p0, LxK;->a:Ljava/lang/String;

    .line 56
    iput-object p4, p0, LxK;->a:Landroid/graphics/Typeface;

    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LxK;
    .registers 2
    .parameter

    .prologue
    .line 36
    const-class v0, LxK;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LxK;

    return-object v0
.end method

.method public static values()[LxK;
    .registers 1

    .prologue
    .line 36
    sget-object v0, LxK;->a:[LxK;

    invoke-virtual {v0}, [LxK;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LxK;

    return-object v0
.end method


# virtual methods
.method public a()Landroid/graphics/Typeface;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, LxK;->a:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 47
    iget-object v0, p0, LxK;->a:Ljava/lang/String;

    return-object v0
.end method
