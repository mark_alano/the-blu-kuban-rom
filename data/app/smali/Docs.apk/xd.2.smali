.class public Lxd;
.super Lcom/google/android/apps/docs/editors/jsvm/JSObject;
.source "Kix.java"

# interfaces
.implements Lxc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/docs/editors/jsvm/JSObject",
        "<",
        "Lvw;",
        ">;",
        "Lxc;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lvw;J)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 4910
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/docs/editors/jsvm/JSObject;-><init>(LuV;J)V

    .line 4911
    return-void
.end method

.method static a(Lvw;J)Lxd;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 4906
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_c

    new-instance v0, Lxd;

    invoke-direct {v0, p0, p1, p2}, Lxd;-><init>(Lvw;J)V

    :goto_b
    return-object v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method


# virtual methods
.method public a()D
    .registers 3

    .prologue
    .line 4940
    invoke-virtual {p0}, Lxd;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->f(J)D

    move-result-wide v0

    .line 4942
    return-wide v0
.end method

.method public a()I
    .registers 3

    .prologue
    .line 4916
    invoke-virtual {p0}, Lxd;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->j(J)I

    move-result v0

    .line 4918
    return v0
.end method

.method public a()Z
    .registers 3

    .prologue
    .line 4932
    invoke-virtual {p0}, Lxd;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->d(J)Z

    move-result v0

    .line 4934
    return v0
.end method

.method public b()D
    .registers 3

    .prologue
    .line 4948
    invoke-virtual {p0}, Lxd;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->g(J)D

    move-result-wide v0

    .line 4950
    return-wide v0
.end method

.method public b()I
    .registers 3

    .prologue
    .line 4924
    invoke-virtual {p0}, Lxd;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->k(J)I

    move-result v0

    .line 4926
    return v0
.end method

.method public c()D
    .registers 3

    .prologue
    .line 4956
    invoke-virtual {p0}, Lxd;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->h(J)D

    move-result-wide v0

    .line 4958
    return-wide v0
.end method

.method public d()D
    .registers 3

    .prologue
    .line 4964
    invoke-virtual {p0}, Lxd;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->i(J)D

    move-result-wide v0

    .line 4966
    return-wide v0
.end method

.method public e()D
    .registers 3

    .prologue
    .line 4972
    invoke-virtual {p0}, Lxd;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->j(J)D

    move-result-wide v0

    .line 4974
    return-wide v0
.end method
