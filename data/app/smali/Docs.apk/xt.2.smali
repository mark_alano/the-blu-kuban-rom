.class public Lxt;
.super Ljava/lang/Object;
.source "ConnectivityChangeListener.java"

# interfaces
.implements LCM;


# instance fields
.field private a:I

.field private final a:LKS;

.field private a:LZK;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

.field private a:Lyz;

.field private b:I


# direct methods
.method public constructor <init>(LKS;Laoz;)V
    .registers 5
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LKS;",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-object v0, LZK;->c:LZK;

    iput-object v0, p0, Lxt;->a:LZK;

    .line 34
    iput-object p1, p0, Lxt;->a:LKS;

    .line 35
    iput-object p2, p0, Lxt;->a:Laoz;

    .line 37
    const-string v0, "kixMobileMutationBatchInterval"

    const/16 v1, 0xbb8

    invoke-interface {p1, v0, v1}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lxt;->a:I

    .line 39
    const-string v0, "kixWifiMutationBatchInterval"

    const/16 v1, 0xc8

    invoke-interface {p1, v0, v1}, LKS;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lxt;->b:I

    .line 41
    return-void
.end method

.method private a(LZK;)V
    .registers 5
    .parameter

    .prologue
    .line 44
    sget-object v0, LZK;->b:LZK;

    if-ne p1, v0, :cond_15

    iget v0, p0, Lxt;->b:I

    .line 47
    :goto_6
    iget-object v1, p0, Lxt;->a:LZK;

    sget-object v2, LZK;->a:LZK;

    if-ne v1, v2, :cond_18

    const/4 v1, 0x1

    .line 49
    :goto_d
    iget-object v2, p0, Lxt;->a:Lyz;

    invoke-virtual {v2, v0, v1}, Lyz;->setMutationBatchInterval(IZ)V

    .line 50
    iput-object p1, p0, Lxt;->a:LZK;

    .line 51
    return-void

    .line 44
    :cond_15
    iget v0, p0, Lxt;->a:I

    goto :goto_6

    .line 47
    :cond_18
    const/4 v1, 0x0

    goto :goto_d
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 72
    iget-object v0, p0, Lxt;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    if-eqz v0, :cond_c

    .line 73
    iget-object v0, p0, Lxt;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;->b(LCM;)V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lxt;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    .line 76
    :cond_c
    return-void
.end method

.method public a(Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;Lyz;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lxt;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    if-eqz v0, :cond_5

    .line 66
    :goto_4
    return-void

    .line 61
    :cond_5
    iput-object p1, p0, Lxt;->a:Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;

    .line 62
    iput-object p2, p0, Lxt;->a:Lyz;

    .line 64
    invoke-virtual {p1, p0}, Lcom/google/android/apps/docs/editors/net/ConnectivityChangeReceiver;->a(LCM;)V

    .line 65
    iget-object v0, p0, Lxt;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LZJ;->a(Landroid/content/Context;)LZK;

    move-result-object v0

    invoke-direct {p0, v0}, Lxt;->a(LZK;)V

    goto :goto_4
.end method

.method public b()V
    .registers 3

    .prologue
    .line 80
    iget-object v0, p0, Lxt;->a:Laoz;

    invoke-interface {v0}, Laoz;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, LZJ;->a(Landroid/content/Context;)LZK;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lxt;->a:LZK;

    if-ne v0, v1, :cond_11

    .line 86
    :goto_10
    return-void

    .line 85
    :cond_11
    invoke-direct {p0, v0}, Lxt;->a(LZK;)V

    goto :goto_10
.end method
