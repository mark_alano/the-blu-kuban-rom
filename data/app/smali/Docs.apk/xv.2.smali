.class public Lxv;
.super Ljava/lang/Object;
.source "DisplayContextImpl.java"

# interfaces
.implements Lxu;


# instance fields
.field private final a:F

.field private final a:LAS;

.field private final a:LCA;

.field private final a:Landroid/content/Context;

.field private final a:Landroid/text/Editable;

.field private final a:Lth;

.field private final a:LxI;

.field private final a:Lxs;

.field private final a:LzP;

.field private final a:Lzi;

.field private final a:Z

.field private b:F

.field private final c:F

.field private final d:F


# direct methods
.method public constructor <init>(Landroid/content/Context;LAS;Landroid/text/Editable;FFFZLzP;LxI;Lzi;Lth;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lxv;->a:Landroid/content/Context;

    .line 46
    iput-object p3, p0, Lxv;->a:Landroid/text/Editable;

    .line 47
    iput-object p2, p0, Lxv;->a:LAS;

    .line 48
    iput p5, p0, Lxv;->c:F

    .line 49
    iput p6, p0, Lxv;->d:F

    .line 50
    iput-boolean p7, p0, Lxv;->a:Z

    .line 51
    iput-object p8, p0, Lxv;->a:LzP;

    .line 52
    iput-object p9, p0, Lxv;->a:LxI;

    .line 53
    new-instance v0, Lxs;

    invoke-direct {v0}, Lxs;-><init>()V

    iput-object v0, p0, Lxv;->a:Lxs;

    .line 54
    iput-object p11, p0, Lxv;->a:Lth;

    .line 55
    new-instance v0, LCB;

    invoke-direct {v0}, LCB;-><init>()V

    iput-object v0, p0, Lxv;->a:LCA;

    .line 56
    iput-object p10, p0, Lxv;->a:Lzi;

    .line 61
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 62
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 63
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 64
    iget v0, v1, Landroid/util/DisplayMetrics;->xdpi:F

    iget v1, v1, Landroid/util/DisplayMetrics;->ydpi:F

    add-float/2addr v0, v1

    const/high16 v1, 0x4000

    div-float/2addr v0, v1

    const/high16 v1, 0x4290

    div-float/2addr v0, v1

    iput v0, p0, Lxv;->a:F

    .line 65
    iput p4, p0, Lxv;->b:F

    .line 66
    return-void
.end method

.method private a(F)F
    .registers 4
    .parameter

    .prologue
    .line 94
    iget v0, p0, Lxv;->d:F

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lxv;->c:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public a()F
    .registers 3

    .prologue
    .line 75
    iget v0, p0, Lxv;->a:F

    iget v1, p0, Lxv;->b:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public a()LAS;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lxv;->a:LAS;

    return-object v0
.end method

.method public a()LCA;
    .registers 2

    .prologue
    .line 124
    iget-object v0, p0, Lxv;->a:LCA;

    return-object v0
.end method

.method public a()Landroid/content/Context;
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lxv;->a:Landroid/content/Context;

    return-object v0
.end method

.method public a()Landroid/text/Editable;
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lxv;->a:Landroid/text/Editable;

    return-object v0
.end method

.method public a()Lth;
    .registers 2

    .prologue
    .line 129
    iget-object v0, p0, Lxv;->a:Lth;

    return-object v0
.end method

.method public a()LxI;
    .registers 2

    .prologue
    .line 114
    iget-object v0, p0, Lxv;->a:LxI;

    return-object v0
.end method

.method public a()LzP;
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, Lxv;->a:LzP;

    return-object v0
.end method

.method public a()Lzi;
    .registers 2

    .prologue
    .line 134
    iget-object v0, p0, Lxv;->a:Lzi;

    return-object v0
.end method

.method public a()Lzs;
    .registers 2

    .prologue
    .line 119
    iget-object v0, p0, Lxv;->a:Lxs;

    return-object v0
.end method

.method public a(F)V
    .registers 3
    .parameter

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lxv;->a(F)F

    move-result v0

    iput v0, p0, Lxv;->b:F

    .line 91
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 104
    iget-boolean v0, p0, Lxv;->a:Z

    return v0
.end method

.method public b()F
    .registers 2

    .prologue
    .line 99
    iget v0, p0, Lxv;->b:F

    return v0
.end method
