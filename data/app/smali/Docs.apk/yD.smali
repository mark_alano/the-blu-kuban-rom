.class LyD;
.super Ljava/lang/Object;
.source "KixJSVM.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:LyB;


# direct methods
.method constructor <init>(LyB;)V
    .registers 2
    .parameter

    .prologue
    .line 481
    iput-object p1, p0, LyD;->a:LyB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    .prologue
    .line 484
    iget-object v0, p0, LyD;->a:LyB;

    iget-object v0, v0, LyB;->a:Lyz;

    #getter for: Lyz;->b:Z
    invoke-static {v0}, Lyz;->access$800(Lyz;)Z

    move-result v0

    if-nez v0, :cond_2c

    .line 485
    const-string v0, "Model"

    const-string v1, "Failed loading the JS Binary"

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    iget-object v0, p0, LyD;->a:LyB;

    iget-object v0, v0, LyB;->a:Lyz;

    #getter for: Lyz;->a:LeQ;
    invoke-static {v0}, Lyz;->access$1300(Lyz;)LeQ;

    move-result-object v0

    const-string v1, "kixEditor"

    const-string v2, "kixJsLoadFailure"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    iget-object v0, p0, LyD;->a:LyB;

    iget-object v0, v0, LyB;->a:Lyz;

    #getter for: Lyz;->a:LyK;
    invoke-static {v0}, Lyz;->access$1700(Lyz;)LyK;

    move-result-object v0

    invoke-interface {v0}, LyK;->a()V

    .line 492
    :goto_2b
    return-void

    .line 490
    :cond_2c
    const-string v0, "Model"

    const-string v1, "Ignoring JS fetching error as JSVM already deleted."

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2b
.end method
