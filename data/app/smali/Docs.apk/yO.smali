.class LyO;
.super Lvf;
.source "KixJSVM.java"


# instance fields
.field final synthetic a:Lyz;


# direct methods
.method private constructor <init>(Lyz;)V
    .registers 2
    .parameter

    .prologue
    .line 1148
    iput-object p1, p0, LyO;->a:Lyz;

    invoke-direct {p0}, Lvf;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lyz;LyA;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 1148
    invoke-direct {p0, p1}, LyO;-><init>(Lyz;)V

    return-void
.end method


# virtual methods
.method public a()V
    .registers 3

    .prologue
    .line 1160
    const-string v0, "Model"

    const-string v1, "Discussions sync"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    iget-object v0, p0, LyO;->a:Lyz;

    #getter for: Lyz;->a:LyI;
    invoke-static {v0}, Lyz;->access$2100(Lyz;)LyI;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 1162
    iget-object v0, p0, LyO;->a:Lyz;

    #getter for: Lyz;->a:LyI;
    invoke-static {v0}, Lyz;->access$2100(Lyz;)LyI;

    move-result-object v0

    invoke-interface {v0}, LyI;->d()V

    .line 1164
    :cond_18
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 1152
    const-string v0, "Model"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Discussions createDoco "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1153
    iget-object v0, p0, LyO;->a:Lyz;

    #getter for: Lyz;->a:LyI;
    invoke-static {v0}, Lyz;->access$2100(Lyz;)LyI;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 1154
    iget-object v0, p0, LyO;->a:Lyz;

    #getter for: Lyz;->a:LyI;
    invoke-static {v0}, Lyz;->access$2100(Lyz;)LyI;

    move-result-object v0

    invoke-interface {v0, p1}, LyI;->c(Ljava/lang/String;)V

    .line 1156
    :cond_29
    return-void
.end method

.method public a(ZZ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1168
    const-string v0, "Model"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDocosMetadata "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 1169
    iget-object v0, p0, LyO;->a:Lyz;

    #getter for: Lyz;->a:LyJ;
    invoke-static {v0}, Lyz;->access$2200(Lyz;)LyJ;

    move-result-object v0

    if-eqz v0, :cond_33

    .line 1170
    iget-object v0, p0, LyO;->a:Lyz;

    #getter for: Lyz;->a:LyJ;
    invoke-static {v0}, Lyz;->access$2200(Lyz;)LyJ;

    move-result-object v0

    invoke-interface {v0, p1, p2}, LyJ;->a(ZZ)V

    .line 1172
    :cond_33
    return-void
.end method
