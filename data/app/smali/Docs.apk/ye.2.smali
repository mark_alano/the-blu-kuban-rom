.class public Lye;
.super Ljava/lang/Object;
.source "IndexConverterImpl.java"

# interfaces
.implements Lyb;


# instance fields
.field private final a:LDA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;LDA;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;",
            "LDA",
            "<",
            "LAx;",
            "LAx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lye;->a:Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;

    .line 39
    iput-object p2, p0, Lye;->a:LDA;

    .line 40
    return-void
.end method

.method private a(I)Lyd;
    .registers 4
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lye;->a:Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;

    iget-object v1, p0, Lye;->a:Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a()LEj;

    move-result-object v1

    invoke-direct {p0, v0, v1, p1}, Lye;->a(Landroid/view/View;LEj;I)Lyd;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;LEj;I)Lyd;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 139
    invoke-interface {p2, p3}, LEj;->a(I)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v1

    sub-int/2addr v0, v1

    .line 140
    invoke-interface {p2, p3}, LEj;->g(I)I

    move-result v1

    invoke-interface {p2, v1}, LEj;->b(I)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v2

    sub-int/2addr v1, v2

    .line 142
    new-instance v2, Lyd;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-direct {v2, v0, v1}, Lyd;-><init>(FF)V

    return-object v2
.end method

.method private a(Lyd;)Lyd;
    .registers 6
    .parameter

    .prologue
    .line 129
    new-instance v0, Lyd;

    iget v1, p1, Lyd;->a:F

    iget-object v2, p0, Lye;->a:Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p1, Lyd;->b:F

    iget-object v3, p0, Lye;->a:Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->getScrollY()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lyd;-><init>(FF)V

    return-object v0
.end method


# virtual methods
.method public a(Lyc;)Lyd;
    .registers 9
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 92
    iget-object v0, p1, Lyc;->a:LDA;

    .line 94
    iget-object v1, p1, Lyc;->a:LDA;

    iget-object v2, p0, Lye;->a:LDA;

    if-ne v1, v2, :cond_14

    .line 95
    iget v0, p1, Lyc;->a:I

    invoke-direct {p0, v0}, Lye;->a(I)Lyd;

    move-result-object v0

    invoke-direct {p0, v0}, Lye;->a(Lyd;)Lyd;

    move-result-object v0

    .line 125
    :goto_13
    return-object v0

    .line 98
    :cond_14
    invoke-virtual {v0}, LDA;->a()Ljava/lang/Object;

    move-result-object v0

    .line 100
    instance-of v1, v0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;

    if-eqz v1, :cond_a8

    .line 102
    check-cast v0, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/boxview/FrameBoxView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;

    .line 103
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->a()LEj;

    move-result-object v1

    iget v2, p1, Lyc;->a:I

    invoke-direct {p0, v0, v1, v2}, Lye;->a(Landroid/view/View;LEj;I)Lyd;

    move-result-object v1

    .line 105
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/boxview/TextBoxView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 106
    :goto_33
    instance-of v2, v0, LzU;

    if-eqz v2, :cond_58

    .line 107
    check-cast v0, LzU;

    invoke-interface {v0}, LzU;->a()Landroid/view/View;

    move-result-object v0

    .line 108
    new-instance v2, Lyd;

    iget v3, v1, Lyd;->a:F

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget v1, v1, Lyd;->b:F

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v1, v4

    invoke-direct {v2, v3, v1}, Lyd;-><init>(FF)V

    .line 109
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v1, v2

    .line 110
    goto :goto_33

    .line 111
    :cond_58
    instance-of v2, v0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement$ReplacementSpanParent;

    if-eqz v2, :cond_89

    .line 112
    check-cast v0, Lcom/google/android/apps/docs/editors/kix/elements/TableElement$ReplacementSpanParent;

    .line 113
    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/elements/TableElement$ReplacementSpanParent;->a()I

    move-result v2

    invoke-direct {p0, v2}, Lye;->a(I)Lyd;

    move-result-object v2

    .line 114
    iget-object v3, p0, Lye;->a:Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/kix/ZoomableEditText;->a()F

    move-result v3

    .line 115
    new-instance v4, Lyd;

    iget v5, v2, Lyd;->a:F

    iget v6, v1, Lyd;->a:F

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/elements/TableElement$ReplacementSpanParent;->b()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v6, v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v5

    iget v2, v2, Lyd;->b:F

    iget v1, v1, Lyd;->b:F

    mul-float/2addr v1, v3

    add-float/2addr v1, v2

    invoke-direct {v4, v0, v1}, Lyd;-><init>(FF)V

    .line 118
    invoke-direct {p0, v4}, Lye;->a(Lyd;)Lyd;

    move-result-object v0

    goto :goto_13

    .line 120
    :cond_89
    const-string v0, "IndexConverterImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to trace the replacement span from a TextBoxView at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :goto_a1
    new-instance v0, Lyd;

    invoke-direct {v0, v5, v5}, Lyd;-><init>(FF)V

    goto/16 :goto_13

    .line 122
    :cond_a8
    const-string v0, "IndexConverterImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "The display index group is not owned by a TextBoxView at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a1
.end method
