.class public final enum Lyk;
.super Ljava/lang/Enum;
.source "KixEditText.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lyk;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lyk;

.field private static final synthetic a:[Lyk;

.field public static final enum b:Lyk;

.field public static final enum c:Lyk;

.field public static final enum d:Lyk;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 140
    new-instance v0, Lyk;

    const-string v1, "NOT_INITIALIZED"

    invoke-direct {v0, v1, v2}, Lyk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyk;->a:Lyk;

    .line 141
    new-instance v0, Lyk;

    const-string v1, "VIEW"

    invoke-direct {v0, v1, v3}, Lyk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyk;->b:Lyk;

    .line 142
    new-instance v0, Lyk;

    const-string v1, "COMMENT"

    invoke-direct {v0, v1, v4}, Lyk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyk;->c:Lyk;

    .line 143
    new-instance v0, Lyk;

    const-string v1, "EDIT"

    invoke-direct {v0, v1, v5}, Lyk;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lyk;->d:Lyk;

    .line 139
    const/4 v0, 0x4

    new-array v0, v0, [Lyk;

    sget-object v1, Lyk;->a:Lyk;

    aput-object v1, v0, v2

    sget-object v1, Lyk;->b:Lyk;

    aput-object v1, v0, v3

    sget-object v1, Lyk;->c:Lyk;

    aput-object v1, v0, v4

    sget-object v1, Lyk;->d:Lyk;

    aput-object v1, v0, v5

    sput-object v0, Lyk;->a:[Lyk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lyk;
    .registers 2
    .parameter

    .prologue
    .line 139
    const-class v0, Lyk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lyk;

    return-object v0
.end method

.method public static values()[Lyk;
    .registers 1

    .prologue
    .line 139
    sget-object v0, Lyk;->a:[Lyk;

    invoke-virtual {v0}, [Lyk;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lyk;

    return-object v0
.end method
