.class public Lyq;
.super Ljava/lang/Object;
.source "KixEditorActivity.java"

# interfaces
.implements LyH;


# instance fields
.field final synthetic a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 514
    iput-object p1, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(LwF;)V
    .registers 5
    .parameter

    .prologue
    .line 517
    iget-object v0, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LdL;

    iget-object v1, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 518
    new-instance v0, Lke;

    iget-object v1, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    iget-object v1, v1, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:Lkj;

    iget-object v2, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    iget-object v2, v2, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LUL;

    invoke-direct {v0, v1, v2}, Lke;-><init>(Lkj;LUL;)V

    invoke-virtual {v0}, Lke;->start()V

    .line 519
    iget-object v0, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LKt;

    sget-object v1, LKu;->a:LKu;

    invoke-virtual {v0, v1}, LKt;->a(LKu;)V

    .line 521
    iget-object v0, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Lzj;

    move-result-object v0

    invoke-virtual {v0}, Lzj;->l()V

    .line 522
    iget-object v0, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "editMode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 523
    iget-object v0, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)Lcom/google/android/apps/docs/editors/kix/KixEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a()Z

    move-result v0

    if-nez v0, :cond_4b

    .line 526
    iget-object v0, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    invoke-static {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->c(Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;)V

    .line 529
    :cond_4b
    return-void
.end method

.method public a(LwF;LvZ;Lxa;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 533
    iget-object v0, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    iget-object v0, v0, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->a:LdL;

    iget-object v1, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    invoke-interface {v0, v1}, LdL;->a(Landroid/content/Context;)V

    .line 534
    iget-object v0, p0, Lyq;->a:Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;

    sget v1, LsD;->loading_spinner:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 535
    return-void
.end method
