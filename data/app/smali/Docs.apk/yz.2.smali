.class public Lyz;
.super Ljava/lang/Object;
.source "KixJSVM.java"

# interfaces
.implements LCW;
.implements LKb;
.implements LuU;
.implements LxZ;


# instance fields
.field private a:I

.field private final a:LJY;

.field private final a:LKS;

.field private final a:LKl;

.field private final a:LKt;

.field private final a:LNS;

.field private final a:LNj;

.field private final a:LZS;

.field private final a:Landroid/os/Handler;

.field private final a:Laoz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LeQ;

.field private final a:Ljava/lang/Object;

.field private a:Ljava/lang/String;

.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LyH;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LCQ;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LuN;

.field private a:LvB;

.field private a:LvZ;

.field private a:Lvb;

.field private a:Lvw;

.field private a:Lvy;

.field private a:Lvz;

.field private a:LwF;

.field private a:LwL;

.field private a:Lxa;

.field private a:LyG;

.field private a:LyI;

.field private a:LyJ;

.field private a:LyK;

.field private a:LyL;

.field private final a:LyM;

.field private final a:LyN;

.field private final a:LyO;

.field private final a:LyP;

.field private a:LyR;

.field private a:LyX;

.field private a:Lya;

.field private a:Lzi;

.field private a:Z

.field private final b:Ljava/lang/Object;

.field private b:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LyU;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LyF;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LyQ;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Z


# direct methods
.method public constructor <init>(Laoz;LNj;LKS;LuN;LZS;LKt;LeQ;LNS;)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation runtime Laon;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laoz",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LNj;",
            "LKS;",
            "LuN;",
            "LZS;",
            "LKt;",
            "LeQ;",
            "LNS;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 432
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    new-instance v0, LyM;

    invoke-direct {v0, p0, v1}, LyM;-><init>(Lyz;LyA;)V

    iput-object v0, p0, Lyz;->a:LyM;

    .line 347
    new-instance v0, LyN;

    invoke-direct {v0, p0, v1}, LyN;-><init>(Lyz;LyA;)V

    iput-object v0, p0, Lyz;->a:LyN;

    .line 349
    new-instance v0, LyP;

    invoke-direct {v0, p0, v1}, LyP;-><init>(Lyz;LyA;)V

    iput-object v0, p0, Lyz;->a:LyP;

    .line 351
    new-instance v0, LyO;

    invoke-direct {v0, p0, v1}, LyO;-><init>(Lyz;LyA;)V

    iput-object v0, p0, Lyz;->a:LyO;

    .line 364
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lyz;->a:Ljava/util/List;

    .line 366
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lyz;->b:Ljava/util/List;

    .line 369
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lyz;->c:Ljava/util/List;

    .line 371
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lyz;->d:Ljava/util/List;

    .line 375
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lyz;->a:Landroid/os/Handler;

    .line 376
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lyz;->a:Ljava/util/Map;

    .line 378
    const/4 v0, 0x0

    iput-boolean v0, p0, Lyz;->a:Z

    .line 379
    const/16 v0, 0xbb8

    iput v0, p0, Lyz;->a:I

    .line 380
    iput-object v1, p0, Lyz;->a:LyX;

    .line 381
    iput-object v1, p0, Lyz;->a:LyL;

    .line 382
    iput-object v1, p0, Lyz;->a:Lvz;

    .line 383
    iput-object v1, p0, Lyz;->a:LwL;

    .line 384
    iput-object v1, p0, Lyz;->a:LyR;

    .line 385
    iput-object v1, p0, Lyz;->a:Lvy;

    .line 386
    new-instance v0, LyA;

    invoke-direct {v0, p0}, LyA;-><init>(Lyz;)V

    iput-object v0, p0, Lyz;->a:Lzi;

    .line 396
    const/4 v0, 0x1

    iput-boolean v0, p0, Lyz;->c:Z

    .line 409
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lyz;->a:Ljava/lang/Object;

    .line 410
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lyz;->b:Ljava/lang/Object;

    .line 412
    invoke-static {}, LaaF;->a()Lfo;

    move-result-object v0

    sget-object v1, Lfo;->d:Lfo;

    invoke-virtual {v0, v1}, Lfo;->a(Lfo;)Z

    move-result v0

    iput-boolean v0, p0, Lyz;->d:Z

    .line 433
    iput-object p1, p0, Lyz;->a:Laoz;

    .line 434
    iput-object p2, p0, Lyz;->a:LNj;

    .line 435
    iput-object p3, p0, Lyz;->a:LKS;

    .line 436
    iput-object p4, p0, Lyz;->a:LuN;

    .line 437
    iput-object p5, p0, Lyz;->a:LZS;

    .line 438
    iput-object p8, p0, Lyz;->a:LNS;

    .line 439
    iput-object p6, p0, Lyz;->a:LKt;

    .line 440
    iput-object p7, p0, Lyz;->a:LeQ;

    .line 443
    new-instance v0, LKl;

    iget-object v1, p0, Lyz;->a:Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v2

    sget-object v3, LKq;->b:LKq;

    invoke-direct {v0, v1, v2, v3}, LKl;-><init>(Landroid/os/Handler;Landroid/os/MessageQueue;LKq;)V

    iput-object v0, p0, Lyz;->a:LKl;

    .line 445
    new-instance v0, LJY;

    iget-object v1, p0, Lyz;->a:LKl;

    invoke-direct {v0, p0, v1}, LJY;-><init>(LKb;LKl;)V

    iput-object v0, p0, Lyz;->a:LJY;

    .line 446
    return-void
.end method

.method static synthetic access$000(Lyz;)Lvw;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:Lvw;

    return-object v0
.end method

.method static synthetic access$002(Lyz;Lvw;)Lvw;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lyz;->a:Lvw;

    return-object p1
.end method

.method static synthetic access$100(Lyz;)LyL;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:LyL;

    return-object v0
.end method

.method static synthetic access$1000(Lyz;)LKt;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:LKt;

    return-object v0
.end method

.method static synthetic access$1100(Lyz;)LKS;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:LKS;

    return-object v0
.end method

.method static synthetic access$1200(Lyz;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1300(Lyz;)LeQ;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:LeQ;

    return-object v0
.end method

.method static synthetic access$1400(Lyz;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1500(Lyz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3, p4}, Lyz;->reallyOpenDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$1600(Lyz;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lyz;)LyK;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:LyK;

    return-object v0
.end method

.method static synthetic access$200(Lyz;)LyR;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:LyR;

    return-object v0
.end method

.method static synthetic access$2000(Lyz;)V
    .registers 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lyz;->onReset()V

    return-void
.end method

.method static synthetic access$2100(Lyz;)LyI;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:LyI;

    return-object v0
.end method

.method static synthetic access$2200(Lyz;)LyJ;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:LyJ;

    return-object v0
.end method

.method static synthetic access$700(Lyz;)Lvy;
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lyz;->a:Lvy;

    return-object v0
.end method

.method static synthetic access$800(Lyz;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lyz;->b:Z

    return v0
.end method

.method static synthetic access$900(Lyz;)Z
    .registers 2
    .parameter

    .prologue
    .line 65
    iget-boolean v0, p0, Lyz;->d:Z

    return v0
.end method

.method private fetchJsFromAppCache(Ljava/lang/String;Ljava/lang/String;LuO;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 539
    iget-object v0, p0, Lyz;->a:LKS;

    const-string v1, "kixJsManifestUrlFormat"

    const-string v2, "https://docs.google.com/document/d/%s/native/androidmanifest"

    invoke-interface {v0, v1, v2}, LKS;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 541
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 543
    invoke-static {v0}, LCN;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_54

    .line 544
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {p5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, LCN;->a(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    move-object v3, v0

    .line 547
    :goto_3b
    iget-object v0, p0, Lyz;->a:LuN;

    iget-object v1, p0, Lyz;->a:LKt;

    invoke-virtual {v1}, LKt;->a()Z

    move-result v1

    if-eqz v1, :cond_52

    const/4 v1, 0x0

    :goto_46
    const-string v2, "kix_mobile"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, p4

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, LuN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LuO;)V

    .line 549
    return-void

    :cond_52
    move-object v1, p1

    .line 547
    goto :goto_46

    :cond_54
    move-object v3, v0

    goto :goto_3b
.end method

.method private onReset()V
    .registers 3

    .prologue
    .line 848
    iget-object v0, p0, Lyz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyU;

    .line 849
    invoke-interface {v0}, LyU;->a()V

    goto :goto_6

    .line 851
    :cond_16
    return-void
.end method

.method private reallyOpenDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 554
    iput-object p1, p0, Lyz;->a:Ljava/lang/String;

    .line 555
    iput-object p3, p0, Lyz;->b:Ljava/lang/String;

    .line 557
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->b()V

    .line 559
    :try_start_9
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->a()Lvy;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:Lvy;

    .line 561
    iget-object v0, p0, Lyz;->a:Lvy;

    invoke-virtual {v0}, Lvy;->b()V

    .line 563
    iget-object v0, p0, Lyz;->a:Lvy;

    invoke-virtual {v0}, Lvy;->a()Lvb;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:Lvb;

    .line 564
    iget-object v0, p0, Lyz;->a:Lvb;

    invoke-interface {v0}, Lvb;->b()V

    .line 566
    iget-object v0, p0, Lyz;->a:Lvb;

    invoke-interface {v0, p2, p1, p3}, Lvb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LvC;

    move-result-object v0

    .line 569
    invoke-interface {v0, p4}, LvC;->a(Z)V

    .line 570
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LvC;->b(Z)V

    .line 572
    iget-object v1, p0, Lyz;->a:Lvw;

    iget-object v2, p0, Lyz;->a:LyM;

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(Lvw;LvG;)LvI;

    move-result-object v1

    .line 574
    iget-object v2, p0, Lyz;->a:Lvw;

    iget-object v3, p0, Lyz;->a:LyN;

    invoke-static {v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(Lvw;LvS;)LvU;

    move-result-object v2

    .line 576
    iget-object v3, p0, Lyz;->a:Lvw;

    iget-object v4, p0, Lyz;->a:LyP;

    invoke-static {v3, v4}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(Lvw;Lwg;)Lwi;

    move-result-object v3

    .line 578
    iget-object v4, p0, Lyz;->a:Lvw;

    iget-object v5, p0, Lyz;->a:LyO;

    invoke-static {v4, v5}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(Lvw;Lwc;)Lwe;

    move-result-object v4

    .line 581
    invoke-interface {v0, v1}, LvC;->a(LvI;)V

    .line 582
    invoke-interface {v0, v2}, LvC;->a(LvU;)V

    .line 583
    invoke-interface {v0, v3}, LvC;->a(Lwi;)V

    .line 584
    invoke-interface {v0}, LvC;->a()Z

    move-result v1

    if-eqz v1, :cond_62

    .line 587
    invoke-interface {v0, v4}, LvC;->a(Lwe;)V

    .line 590
    :cond_62
    iget-object v1, p0, Lyz;->a:LyX;

    if-eqz v1, :cond_81

    .line 591
    iget-object v1, p0, Lyz;->a:Lvw;

    new-instance v2, LyV;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, LyV;-><init>(Lyz;LyA;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(Lvw;Lwy;)LwA;

    move-result-object v1

    .line 593
    iget-object v2, p0, Lyz;->a:Lvw;

    new-instance v3, LyS;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, LyS;-><init>(Lyz;LyA;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(Lvw;LwN;)LwP;

    move-result-object v2

    .line 596
    invoke-interface {v0, v1, v2}, LvC;->a(LwA;LwP;)V

    .line 599
    :cond_81
    invoke-interface {v0}, LvC;->a()LvB;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:LvB;

    .line 600
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->b()V

    .line 602
    iget-object v0, p0, Lyz;->a:LvB;

    iget v1, p0, Lyz;->a:I

    invoke-interface {v0, v1}, LvB;->a(I)V

    .line 603
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->c()V
    :try_end_98
    .catchall {:try_start_9 .. :try_end_98} :catchall_9e

    .line 605
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->c()V

    .line 607
    return-void

    .line 605
    :catchall_9e
    move-exception v0

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    throw v0
.end method

.method private shouldLoadJsFromAppCache()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 519
    iget-boolean v1, p0, Lyz;->d:Z

    if-nez v1, :cond_16

    invoke-direct {p0}, Lyz;->shouldUseFallbackJsInThisVersion()Z

    move-result v1

    if-nez v1, :cond_16

    iget-object v1, p0, Lyz;->a:LKS;

    const-string v2, "kixEnableJsDownload"

    invoke-interface {v1, v2, v0}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_16

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method private shouldUseFallbackJsInThisVersion()Z
    .registers 5

    .prologue
    .line 528
    invoke-static {}, LaaF;->a()I

    move-result v0

    .line 529
    iget-object v1, p0, Lyz;->a:LKS;

    const-string v2, "kixMinDocsVersionToUseLocalJs"

    const v3, 0x7fffffff

    invoke-interface {v1, v2, v3}, LKS;->a(Ljava/lang/String;I)I

    move-result v1

    .line 531
    if-lt v0, v1, :cond_13

    const/4 v0, 0x1

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method


# virtual methods
.method public abortHttpRequest(I)V
    .registers 4
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 778
    iget-object v0, p0, Lyz;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCQ;

    .line 779
    if-eqz v0, :cond_11

    .line 780
    invoke-virtual {v0}, LCQ;->a()V

    .line 782
    :cond_11
    iget-object v0, p0, Lyz;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 783
    return-void
.end method

.method public addCollaboratorListener(LyF;)V
    .registers 3
    .parameter

    .prologue
    .line 900
    iget-object v0, p0, Lyz;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 901
    return-void
.end method

.method public addInitializationListener(LyH;)V
    .registers 3
    .parameter

    .prologue
    .line 874
    iget-object v0, p0, Lyz;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 875
    return-void
.end method

.method public addNetworkStatusListener(LyQ;)V
    .registers 3
    .parameter

    .prologue
    .line 908
    iget-object v0, p0, Lyz;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 909
    return-void
.end method

.method public addUpdateListener(LyU;)V
    .registers 3
    .parameter

    .prologue
    .line 882
    iget-object v0, p0, Lyz;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 883
    return-void
.end method

.method public delete()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 680
    const-string v0, "Model"

    const-string v1, "JSVM deleted."

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    iput-boolean v3, p0, Lyz;->b:Z

    .line 682
    iput-boolean v3, p0, Lyz;->a:Z

    .line 684
    iget-object v0, p0, Lyz;->a:Lvw;

    if-nez v0, :cond_12

    .line 743
    :goto_11
    return-void

    .line 688
    :cond_12
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->b()V

    .line 690
    :try_start_17
    iget-object v0, p0, Lyz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LCQ;

    .line 691
    invoke-virtual {v0}, LCQ;->a()V
    :try_end_30
    .catchall {:try_start_17 .. :try_end_30} :catchall_31

    goto :goto_21

    .line 737
    :catchall_31
    move-exception v0

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    throw v0

    .line 693
    :cond_38
    :try_start_38
    iget-object v0, p0, Lyz;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 695
    iget-object v0, p0, Lyz;->a:LJY;

    invoke-virtual {v0}, LJY;->a()V

    .line 697
    iget-object v0, p0, Lyz;->a:LwF;

    if-eqz v0, :cond_4e

    .line 698
    iget-object v0, p0, Lyz;->a:LwF;

    invoke-interface {v0}, LwF;->a()V

    .line 699
    const/4 v0, 0x0

    iput-object v0, p0, Lyz;->a:LwF;

    .line 702
    :cond_4e
    iget-object v0, p0, Lyz;->a:LvZ;

    if-eqz v0, :cond_5a

    .line 703
    iget-object v0, p0, Lyz;->a:LvZ;

    invoke-interface {v0}, LvZ;->a()V

    .line 704
    const/4 v0, 0x0

    iput-object v0, p0, Lyz;->a:LvZ;

    .line 707
    :cond_5a
    iget-object v0, p0, Lyz;->a:LwL;

    if-eqz v0, :cond_66

    .line 708
    iget-object v0, p0, Lyz;->a:LwL;

    invoke-virtual {v0}, LwL;->a()V

    .line 709
    const/4 v0, 0x0

    iput-object v0, p0, Lyz;->a:LwL;

    .line 712
    :cond_66
    iget-object v0, p0, Lyz;->a:Lvz;

    if-eqz v0, :cond_72

    .line 713
    iget-object v0, p0, Lyz;->a:Lvz;

    invoke-interface {v0}, Lvz;->a()V

    .line 714
    const/4 v0, 0x0

    iput-object v0, p0, Lyz;->a:Lvz;

    .line 717
    :cond_72
    iget-object v0, p0, Lyz;->a:Lxa;

    if-eqz v0, :cond_7e

    .line 718
    iget-object v0, p0, Lyz;->a:Lxa;

    invoke-interface {v0}, Lxa;->a()V

    .line 719
    const/4 v0, 0x0

    iput-object v0, p0, Lyz;->a:Lxa;

    .line 722
    :cond_7e
    iget-object v0, p0, Lyz;->a:LvB;

    if-eqz v0, :cond_8a

    .line 723
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->a()V

    .line 724
    const/4 v0, 0x0

    iput-object v0, p0, Lyz;->a:LvB;

    .line 727
    :cond_8a
    iget-object v0, p0, Lyz;->a:Lvb;

    if-eqz v0, :cond_96

    .line 728
    iget-object v0, p0, Lyz;->a:Lvb;

    invoke-interface {v0}, Lvb;->a()V

    .line 729
    const/4 v0, 0x0

    iput-object v0, p0, Lyz;->a:Lvb;

    .line 732
    :cond_96
    iget-object v0, p0, Lyz;->a:Lvy;

    if-eqz v0, :cond_a2

    .line 733
    iget-object v0, p0, Lyz;->a:Lvy;

    invoke-virtual {v0}, Lvy;->a()V

    .line 734
    const/4 v0, 0x0

    iput-object v0, p0, Lyz;->a:Lvy;
    :try_end_a2
    .catchall {:try_start_38 .. :try_end_a2} :catchall_31

    .line 737
    :cond_a2
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->c()V

    .line 740
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->a()V

    .line 741
    iput-object v2, p0, Lyz;->a:Lvw;

    .line 742
    iput-object v2, p0, Lyz;->a:Lzi;

    goto/16 :goto_11
.end method

.method public fetchImageUrl(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 934
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->b()V

    .line 936
    :try_start_5
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->a()LwY;

    move-result-object v0

    invoke-interface {v0, p1}, LwY;->a(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_5 .. :try_end_e} :catchall_14

    .line 938
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->c()V

    .line 940
    return-void

    .line 938
    :catchall_14
    move-exception v0

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    throw v0
.end method

.method public getController()LvZ;
    .registers 2

    .prologue
    .line 1003
    iget-object v0, p0, Lyz;->a:LvZ;

    return-object v0
.end method

.method public getJsLayoutViewProvider()Lvz;
    .registers 2

    .prologue
    .line 1010
    iget-object v0, p0, Lyz;->a:Lvz;

    return-object v0
.end method

.method public getModel()LwF;
    .registers 2

    .prologue
    .line 996
    iget-object v0, p0, Lyz;->a:LwF;

    return-object v0
.end method

.method public getRectangleFactory()Lzi;
    .registers 2

    .prologue
    .line 1014
    iget-object v0, p0, Lyz;->a:Lzi;

    return-object v0
.end method

.method public getTime()D
    .registers 3
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 805
    iget-object v0, p0, Lyz;->a:LJY;

    invoke-virtual {v0}, LJY;->a()D

    move-result-wide v0

    return-wide v0
.end method

.method public invokeTimer(D)I
    .registers 6
    .parameter

    .prologue
    .line 815
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->b()V

    .line 817
    :try_start_5
    iget-object v0, p0, Lyz;->a:Lvb;

    invoke-virtual {p0}, Lyz;->getTime()D

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lvb;->a(D)I
    :try_end_e
    .catchall {:try_start_5 .. :try_end_e} :catchall_15

    move-result v0

    .line 819
    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    return v0

    :catchall_15
    move-exception v0

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    throw v0
.end method

.method public onCollaboratorChat(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1048
    iget-object v0, p0, Lyz;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyF;

    .line 1049
    invoke-interface {v0, p1, p2}, LyF;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1051
    :cond_16
    return-void
.end method

.method public onCollaboratorCursorMove(Ljava/lang/String;II)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1055
    iget-object v0, p0, Lyz;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyF;

    .line 1056
    invoke-interface {v0, p1, p2, p3}, LyF;->a(Ljava/lang/String;II)V

    goto :goto_6

    .line 1058
    :cond_16
    return-void
.end method

.method public onCollaboratorJoin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1034
    iget-object v0, p0, Lyz;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyF;

    .line 1035
    invoke-interface {v0, p1, p2, p3}, LyF;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 1037
    :cond_16
    return-void
.end method

.method public onCollaboratorLeave(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 1041
    iget-object v0, p0, Lyz;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyF;

    .line 1042
    invoke-interface {v0, p1}, LyF;->a(Ljava/lang/String;)V

    goto :goto_6

    .line 1044
    :cond_16
    return-void
.end method

.method public onDataReceived(ILCX;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 748
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->b()V

    .line 750
    :try_start_5
    iget-object v0, p0, Lyz;->a:Lvb;

    invoke-virtual {p2}, LCX;->ordinal()I

    move-result v2

    move v1, p1

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lvb;->a(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_13
    .catchall {:try_start_5 .. :try_end_13} :catchall_19

    .line 753
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->c()V

    .line 755
    return-void

    .line 753
    :catchall_19
    move-exception v0

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    throw v0
.end method

.method public onDocumentSaveStateChanged(I)V
    .registers 3
    .parameter

    .prologue
    .line 1061
    iget-object v0, p0, Lyz;->a:LyG;

    if-eqz v0, :cond_9

    .line 1062
    iget-object v0, p0, Lyz;->a:LyG;

    invoke-interface {v0, p1}, LyG;->a(I)V

    .line 1064
    :cond_9
    return-void
.end method

.method public onDocumentUpdated()V
    .registers 6

    .prologue
    .line 950
    iget-object v0, p0, Lyz;->a:LvZ;

    if-nez v0, :cond_11

    .line 951
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->a()LvZ;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:LvZ;

    .line 952
    iget-object v0, p0, Lyz;->a:LvZ;

    invoke-interface {v0}, LvZ;->b()V

    .line 955
    :cond_11
    iget-object v0, p0, Lyz;->a:Lxa;

    if-nez v0, :cond_22

    .line 956
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->a()Lxa;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:Lxa;

    .line 957
    iget-object v0, p0, Lyz;->a:Lxa;

    invoke-interface {v0}, Lxa;->b()V

    .line 960
    :cond_22
    iget-object v0, p0, Lyz;->a:Lvz;

    if-nez v0, :cond_3b

    iget-object v0, p0, Lyz;->a:Lxa;

    invoke-interface {v0}, Lxa;->a()Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 961
    iget-object v0, p0, Lyz;->a:Lxa;

    invoke-interface {v0}, Lxa;->a()Lvz;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:Lvz;

    .line 962
    iget-object v0, p0, Lyz;->a:Lvz;

    invoke-interface {v0}, Lvz;->b()V

    .line 965
    :cond_3b
    iget-object v0, p0, Lyz;->a:LwL;

    if-nez v0, :cond_51

    .line 966
    iget-object v0, p0, Lyz;->a:Lvw;

    new-instance v1, LyE;

    invoke-direct {v1, p0}, LyE;-><init>(Lyz;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/docs/editors/jsvm/Kix;->a(Lvw;LwJ;)LwL;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:LwL;

    .line 967
    iget-object v0, p0, Lyz;->a:LwL;

    invoke-virtual {v0}, LwL;->b()V

    .line 970
    :cond_51
    iget-object v0, p0, Lyz;->a:LwF;

    if-nez v0, :cond_9c

    .line 971
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->a()LwF;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:LwF;

    .line 972
    iget-object v0, p0, Lyz;->a:LwF;

    invoke-interface {v0}, LwF;->b()V

    .line 974
    iget-object v0, p0, Lyz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_68
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyH;

    .line 975
    iget-object v2, p0, Lyz;->a:LwF;

    iget-object v3, p0, Lyz;->a:LvZ;

    iget-object v4, p0, Lyz;->a:Lxa;

    invoke-interface {v0, v2, v3, v4}, LyH;->a(LwF;LvZ;Lxa;)V

    goto :goto_68

    .line 978
    :cond_7e
    iget-object v0, p0, Lyz;->a:LyX;

    if-eqz v0, :cond_9c

    iget-object v0, p0, Lyz;->a:Lvz;

    if-eqz v0, :cond_9c

    .line 979
    iget-object v0, p0, Lyz;->a:LyX;

    iget-object v1, p0, Lyz;->a:Lvw;

    iget-object v2, p0, Lyz;->a:Lvz;

    invoke-interface {v0, v1, v2}, LyX;->a(Lvw;Lvz;)LyL;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:LyL;

    .line 980
    iget-object v0, p0, Lyz;->a:LyX;

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-interface {v0, v1}, LyX;->a(Lvw;)LyR;

    move-result-object v0

    iput-object v0, p0, Lyz;->a:LyR;

    .line 984
    :cond_9c
    const-string v0, "Model"

    const-string v1, "OnDocumentUpdated"

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    iget-object v0, p0, Lyz;->a:Lxa;

    const-string v1, ""

    invoke-interface {v0, v1}, Lxa;->a(Ljava/lang/String;)Lwj;

    move-result-object v1

    .line 988
    iget-object v0, p0, Lyz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_b1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyU;

    .line 989
    iget-object v3, p0, Lyz;->a:LwL;

    invoke-interface {v0, v1, v3}, LyU;->a(Lwj;LwL;)V

    goto :goto_b1

    .line 992
    :cond_c3
    iget-object v0, p0, Lyz;->a:LKl;

    sget-object v1, LKq;->a:LKq;

    invoke-virtual {v0, v1}, LKl;->a(LKq;)V

    .line 993
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 855
    const-string v0, "Model"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    iget-object v0, p0, Lyz;->a:LyK;

    if-eqz v0, :cond_21

    .line 857
    iget-object v0, p0, Lyz;->a:LyK;

    invoke-interface {v0, p1}, LyK;->a(Ljava/lang/String;)V

    .line 859
    :cond_21
    return-void
.end method

.method public onImageUrlFetched(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 1019
    const-string v0, "Model"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fetched image. Id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " url: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    iget-object v0, p0, Lyz;->a:Lya;

    if-eqz v0, :cond_2d

    .line 1021
    iget-object v0, p0, Lyz;->a:Lya;

    iget-object v1, p0, Lyz;->a:Ljava/lang/String;

    invoke-interface {v0, p1, p2, v1}, Lya;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1023
    :cond_2d
    return-void
.end method

.method public onModelLoadComplete()V
    .registers 4

    .prologue
    .line 825
    iget-object v0, p0, Lyz;->a:LeQ;

    iget-object v1, p0, Lyz;->b:Ljava/lang/Object;

    const-string v2, "kixLoadTime"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 827
    iget-object v0, p0, Lyz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyH;

    .line 828
    iget-object v2, p0, Lyz;->a:LwF;

    invoke-interface {v0, v2}, LyH;->a(LwF;)V

    goto :goto_f

    .line 833
    :cond_21
    iget-object v0, p0, Lyz;->a:LKl;

    sget-object v1, LKq;->b:LKq;

    invoke-virtual {v0, v1}, LKl;->a(LKq;)V

    .line 834
    return-void
.end method

.method public onModelLoadFailed(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 838
    iget-object v0, p0, Lyz;->a:LeQ;

    iget-object v1, p0, Lyz;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LeQ;->b(Ljava/lang/Object;)V

    .line 842
    iget-object v0, p0, Lyz;->a:LyK;

    if-eqz v0, :cond_10

    .line 843
    iget-object v0, p0, Lyz;->a:LyK;

    invoke-interface {v0}, LyK;->a()V

    .line 845
    :cond_10
    return-void
.end method

.method public onNetworkStatus(ZLjava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 863
    const-string v0, "Model"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNetworkStatus: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    iget-boolean v0, p0, Lyz;->c:Z

    if-eq v0, p1, :cond_3e

    .line 865
    iput-boolean p1, p0, Lyz;->c:Z

    .line 867
    iget-object v0, p0, Lyz;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyQ;

    .line 868
    invoke-interface {v0, p1, p2}, LyQ;->a(ZLjava/lang/String;)V

    goto :goto_2e

    .line 871
    :cond_3e
    return-void
.end method

.method public onPause()V
    .registers 3

    .prologue
    .line 633
    iget-boolean v0, p0, Lyz;->a:Z

    if-eqz v0, :cond_5

    .line 651
    :cond_4
    :goto_4
    return-void

    .line 637
    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Lyz;->a:Z

    .line 638
    iget-object v0, p0, Lyz;->a:LvB;

    if-eqz v0, :cond_4

    .line 643
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->b()V

    .line 645
    :try_start_11
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->d()V
    :try_end_16
    .catchall {:try_start_11 .. :try_end_16} :catchall_21

    .line 647
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->c()V

    .line 650
    iget-object v0, p0, Lyz;->a:LKl;

    invoke-virtual {v0}, LKl;->b()V

    goto :goto_4

    .line 647
    :catchall_21
    move-exception v0

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    throw v0
.end method

.method public onRequestCompleted(I)V
    .registers 4
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 772
    iget-object v0, p0, Lyz;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 773
    return-void
.end method

.method public onRequestFailed(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 759
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->b()V

    .line 761
    :try_start_5
    iget-object v0, p0, Lyz;->a:Lvb;

    invoke-interface {v0, p1, p2}, Lvb;->a(II)V

    .line 762
    iget-object v0, p0, Lyz;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 763
    const-string v0, "Model"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTP request failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2b
    .catchall {:try_start_5 .. :try_end_2b} :catchall_31

    .line 765
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->c()V

    .line 767
    return-void

    .line 765
    :catchall_31
    move-exception v0

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    throw v0
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 654
    iget-boolean v0, p0, Lyz;->a:Z

    if-nez v0, :cond_5

    .line 673
    :cond_4
    :goto_4
    return-void

    .line 658
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lyz;->a:Z

    .line 660
    iget-object v0, p0, Lyz;->a:LvB;

    if-eqz v0, :cond_4

    .line 665
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->b()V

    .line 667
    :try_start_11
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->e()V
    :try_end_16
    .catchall {:try_start_11 .. :try_end_16} :catchall_21

    .line 669
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->c()V

    .line 672
    iget-object v0, p0, Lyz;->a:LKl;

    invoke-virtual {v0}, LKl;->a()V

    goto :goto_4

    .line 669
    :catchall_21
    move-exception v0

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    throw v0
.end method

.method public onSelectionChanged(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 1027
    iget-object v0, p0, Lyz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LyU;

    .line 1028
    invoke-interface {v0, p2, p1}, LyU;->b(II)V

    goto :goto_6

    .line 1030
    :cond_16
    return-void
.end method

.method public openDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 455
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 456
    new-instance v0, LyB;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, LyB;-><init>(Lyz;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 497
    iget-object v1, p0, Lyz;->a:LeQ;

    iget-object v2, p0, Lyz;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2}, LeQ;->a(Ljava/lang/Object;)V

    .line 499
    const-string v5, "kix_mobilenative_android.js"

    .line 500
    invoke-direct {p0}, Lyz;->shouldLoadJsFromAppCache()Z

    move-result v1

    if-eqz v1, :cond_26

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v0

    move-object v6, p3

    .line 501
    invoke-direct/range {v1 .. v6}, Lyz;->fetchJsFromAppCache(Ljava/lang/String;Ljava/lang/String;LuO;Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    :goto_25
    return-void

    .line 505
    :cond_26
    :try_start_26
    iget-object v2, p0, Lyz;->a:LZS;

    iget-object v1, p0, Lyz;->a:Laoz;

    invoke-interface {v1}, Laoz;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    invoke-interface {v2, v1}, LZS;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LuO;->a([BLjava/lang/String;)V
    :try_end_40
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_40} :catch_41

    goto :goto_25

    .line 507
    :catch_41
    move-exception v1

    .line 508
    const-string v2, "Model"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed reading from file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fallback to download from the web."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Laaz;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v0

    move-object v6, p3

    .line 510
    invoke-direct/range {v1 .. v6}, Lyz;->fetchJsFromAppCache(Ljava/lang/String;Ljava/lang/String;LuO;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_25
.end method

.method public removeCollaboratorListener(LyF;)V
    .registers 3
    .parameter

    .prologue
    .line 904
    iget-object v0, p0, Lyz;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 905
    return-void
.end method

.method public removeInitializationListener(LyH;)V
    .registers 3
    .parameter

    .prologue
    .line 878
    iget-object v0, p0, Lyz;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 879
    return-void
.end method

.method public removeNetworkStatusListener(LyQ;)V
    .registers 3
    .parameter

    .prologue
    .line 912
    iget-object v0, p0, Lyz;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 913
    return-void
.end method

.method public removeUpdateListener(LyU;)V
    .registers 3
    .parameter

    .prologue
    .line 886
    iget-object v0, p0, Lyz;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 887
    return-void
.end method

.method public sendHttpRequest(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation build Lcom/google/android/apps/docs/KeepAfterProguard;
    .end annotation

    .prologue
    .line 789
    const-string v0, "Model"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sending HTTP request "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    :try_start_18
    new-instance v0, LCQ;

    iget-object v1, p0, Lyz;->a:LNj;

    const-string v2, "writely"

    iget-object v3, p0, Lyz;->a:LNS;

    iget-object v4, p0, Lyz;->a:Ljava/lang/String;

    iget-object v6, p0, Lyz;->a:LKl;

    iget-object v7, p0, Lyz;->a:LKt;

    move-object v5, p0

    invoke-direct/range {v0 .. v7}, LCQ;-><init>(LNj;Ljava/lang/String;LNS;Ljava/lang/String;LCW;LKl;LKt;)V

    .line 795
    iget-object v1, p0, Lyz;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    iget-object v1, p0, Lyz;->b:Ljava/lang/String;

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, LCQ;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    :try_end_3e
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_3e} :catch_3f

    .line 800
    :goto_3e
    return-void

    .line 797
    :catch_3f
    move-exception v0

    .line 798
    const-string v1, "Model"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SendHttpRequest: Exception "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Laaz;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3e
.end method

.method public setDiscussionInsertListener(LyI;)V
    .registers 2
    .parameter

    .prologue
    .line 920
    iput-object p1, p0, Lyz;->a:LyI;

    .line 921
    return-void
.end method

.method public setDocosMetadataListener(LyJ;)V
    .registers 2
    .parameter

    .prologue
    .line 916
    iput-object p1, p0, Lyz;->a:LyJ;

    .line 917
    return-void
.end method

.method public setDocumentSaveStateListener(LyG;)V
    .registers 2
    .parameter

    .prologue
    .line 896
    iput-object p1, p0, Lyz;->a:LyG;

    .line 897
    return-void
.end method

.method public setErrorHandler(LyK;)V
    .registers 2
    .parameter

    .prologue
    .line 943
    iput-object p1, p0, Lyz;->a:LyK;

    .line 944
    return-void
.end method

.method public setImageListener(Lya;)V
    .registers 2
    .parameter

    .prologue
    .line 892
    iput-object p1, p0, Lyz;->a:Lya;

    .line 893
    return-void
.end method

.method public setMutationBatchInterval(IZ)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 613
    iput p1, p0, Lyz;->a:I

    .line 614
    iget-object v0, p0, Lyz;->a:LvB;

    if-nez v0, :cond_7

    .line 630
    :goto_6
    return-void

    .line 619
    :cond_7
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->b()V

    .line 621
    :try_start_c
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0, p1}, LvB;->a(I)V

    .line 622
    if-eqz p2, :cond_21

    iget-boolean v0, p0, Lyz;->a:Z

    if-nez v0, :cond_21

    .line 624
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->d()V

    .line 625
    iget-object v0, p0, Lyz;->a:LvB;

    invoke-interface {v0}, LvB;->e()V
    :try_end_21
    .catchall {:try_start_c .. :try_end_21} :catchall_27

    .line 628
    :cond_21
    iget-object v0, p0, Lyz;->a:Lvw;

    invoke-virtual {v0}, Lvw;->c()V

    goto :goto_6

    :catchall_27
    move-exception v0

    iget-object v1, p0, Lyz;->a:Lvw;

    invoke-virtual {v1}, Lvw;->c()V

    throw v0
.end method

.method public setViewProvisionListener(LyX;)V
    .registers 2
    .parameter

    .prologue
    .line 928
    iput-object p1, p0, Lyz;->a:LyX;

    .line 929
    return-void
.end method

.method public startTimer(I)V
    .registers 3
    .parameter

    .prologue
    .line 810
    iget-object v0, p0, Lyz;->a:LJY;

    invoke-virtual {v0, p1}, LJY;->a(I)V

    .line 811
    return-void
.end method
