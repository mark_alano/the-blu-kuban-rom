.class LzY;
.super Ljava/lang/Object;
.source "DrawHelper.java"

# interfaces
.implements LAa;


# instance fields
.field final synthetic a:Landroid/graphics/Canvas;

.field a:Landroid/graphics/Matrix;

.field a:Landroid/graphics/Rect;

.field final synthetic a:LzX;


# direct methods
.method constructor <init>(LzX;Landroid/graphics/Canvas;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 92
    iput-object p1, p0, LzY;->a:LzX;

    iput-object p2, p0, LzY;->a:Landroid/graphics/Canvas;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iget-object v0, p0, LzY;->a:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, LzY;->a:Landroid/graphics/Matrix;

    .line 94
    iget-object v0, p0, LzY;->a:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, LzY;->a:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;)V
    .registers 4
    .parameter

    .prologue
    .line 98
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 99
    iget-object v0, p0, LzY;->a:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 100
    iget-object v0, p0, LzY;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 101
    iget-object v0, p0, LzY;->a:LzX;

    invoke-static {v0}, LzX;->a(LzX;)LzZ;

    move-result-object v0

    invoke-interface {v0, p1}, LzZ;->a(Landroid/graphics/Canvas;)V

    .line 102
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 105
    iget-object v0, p0, LzY;->a:LzX;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LzX;->a(LzX;Ljava/util/Stack;)V

    .line 106
    return-void
.end method
