.class public Lzc;
.super Ljava/lang/Object;
.source "LinkNavigator.java"

# interfaces
.implements LyH;
.implements LyU;


# instance fields
.field private final a:LPm;

.field private final a:Landroid/content/Context;

.field private final a:Ljava/lang/String;

.field private a:LvZ;

.field private a:LwF;

.field private a:Z

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LPm;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lzc;->a:Landroid/content/Context;

    .line 49
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LPm;

    iput-object v0, p0, Lzc;->a:LPm;

    .line 50
    invoke-static {p3}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lzc;->a:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lzc;->b:Ljava/lang/String;

    .line 52
    if-eqz p4, :cond_23

    const/4 v0, 0x1

    :goto_20
    iput-boolean v0, p0, Lzc;->a:Z

    .line 53
    return-void

    .line 52
    :cond_23
    const/4 v0, 0x0

    goto :goto_20
.end method


# virtual methods
.method public a()V
    .registers 2

    .prologue
    .line 145
    const/4 v0, 0x0

    iput-boolean v0, p0, Lzc;->a:Z

    .line 146
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 6
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lzc;->a:LwF;

    const-string v1, "Model is not initialized."

    invoke-static {v0, v1}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v0, p0, Lzc;->a:LvZ;

    const-string v1, "Controller is not initialized."

    invoke-static {v0, v1}, Lagu;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    const-string v0, "LinkNavigator"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Navigate to URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const-string v0, "#"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 83
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lzc;->a(Ljava/lang/String;)Z

    .line 105
    :goto_36
    return-void

    .line 86
    :cond_37
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lzc;->a:LPm;

    iget-object v2, p0, Lzc;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LPm;->a(Landroid/content/Context;Landroid/net/Uri;)LPp;

    move-result-object v1

    .line 88
    invoke-virtual {v1}, LPp;->a()Ljava/lang/String;

    move-result-object v2

    .line 89
    invoke-virtual {v1}, LPp;->a()LkP;

    move-result-object v1

    sget-object v3, LkP;->a:LkP;

    if-ne v1, v3, :cond_69

    iget-object v1, p0, Lzc;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_69

    .line 90
    invoke-virtual {v0}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_61

    .line 92
    invoke-virtual {p0, v0}, Lzc;->a(Ljava/lang/String;)Z

    goto :goto_36

    .line 96
    :cond_61
    const-string v0, "LinkNavigator"

    const-string v1, "Navigation to the same document, but the internal link is unknown."

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_36

    .line 99
    :cond_69
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 101
    :try_start_70
    iget-object v0, p0, Lzc;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_75
    .catch Landroid/content/ActivityNotFoundException; {:try_start_70 .. :try_end_75} :catch_76

    goto :goto_36

    .line 102
    :catch_76
    move-exception v0

    goto :goto_36
.end method

.method public a(LwF;)V
    .registers 4
    .parameter

    .prologue
    .line 63
    iget-boolean v0, p0, Lzc;->a:Z

    if-eqz v0, :cond_10

    .line 64
    iget-object v0, p0, Lzc;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lzc;->a(Ljava/lang/String;)Z

    .line 65
    const-string v0, "LinkNavigator"

    const-string v1, "navigating"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_10
    const/4 v0, 0x0

    iput-boolean v0, p0, Lzc;->a:Z

    .line 68
    return-void
.end method

.method public a(LwF;LvZ;Lxa;)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-static {p1}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LwF;

    iput-object v0, p0, Lzc;->a:LwF;

    .line 58
    invoke-static {p2}, Lagu;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LvZ;

    iput-object v0, p0, Lzc;->a:LvZ;

    .line 59
    return-void
.end method

.method public a(Lwj;LwL;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 133
    iget-boolean v0, p0, Lzc;->a:Z

    if-eqz v0, :cond_f

    .line 134
    iget-object v0, p0, Lzc;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lzc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    :goto_d
    iput-boolean v0, p0, Lzc;->a:Z

    .line 136
    :cond_f
    return-void

    .line 134
    :cond_10
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public a(Ljava/lang/String;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 114
    const-string v1, "LinkNavigator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Navigate to internal link: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    iget-object v1, p0, Lzc;->a:LwF;

    invoke-interface {v1}, LwF;->a()LuV;

    move-result-object v1

    .line 116
    invoke-interface {v1}, LuV;->b()V

    .line 118
    :try_start_22
    iget-object v2, p0, Lzc;->a:LwF;

    invoke-interface {v2}, LwF;->h()Z

    move-result v2

    if-eqz v2, :cond_3e

    .line 119
    iget-object v2, p0, Lzc;->a:LwF;

    invoke-interface {v2, p1}, LwF;->a(Ljava/lang/String;)I

    move-result v2

    .line 120
    const/4 v3, -0x1

    if-eq v2, v3, :cond_3e

    .line 121
    iget-object v0, p0, Lzc;->a:LvZ;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LvZ;->a(IZ)V
    :try_end_39
    .catchall {:try_start_22 .. :try_end_39} :catchall_42

    .line 122
    const/4 v0, 0x1

    .line 126
    invoke-interface {v1}, LuV;->c()V

    .line 128
    :goto_3d
    return v0

    .line 126
    :cond_3e
    invoke-interface {v1}, LuV;->c()V

    goto :goto_3d

    :catchall_42
    move-exception v0

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public b(II)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 141
    return-void
.end method
