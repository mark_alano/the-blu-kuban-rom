.class public Lzg;
.super LMD;
.source "OpticalZoomManager.java"

# interfaces
.implements LzR;


# instance fields
.field private final a:F

.field private final a:LMB;

.field private final a:Landroid/os/Handler;

.field private final a:Ljava/lang/Runnable;

.field private a:LzS;

.field private a:Z

.field private final b:F


# direct methods
.method public constructor <init>(Landroid/content/Context;FF)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, LMD;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lzg;->a:Z

    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lzg;->a:Landroid/os/Handler;

    .line 31
    new-instance v0, Lzh;

    invoke-direct {v0, p0}, Lzh;-><init>(Lzg;)V

    iput-object v0, p0, Lzg;->a:Ljava/lang/Runnable;

    .line 39
    iput p2, p0, Lzg;->a:F

    .line 40
    iput p3, p0, Lzg;->b:F

    .line 41
    new-instance v0, LMB;

    invoke-direct {v0, p1, p0}, LMB;-><init>(Landroid/content/Context;LMC;)V

    iput-object v0, p0, Lzg;->a:LMB;

    .line 42
    return-void
.end method

.method private a(FFF)F
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 125
    const/high16 v0, 0x3f80

    sub-float v0, p1, v0

    mul-float/2addr v0, p2

    mul-float v1, p3, p1

    add-float/2addr v0, v1

    return v0
.end method

.method static synthetic a(Lzg;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 18
    iput-boolean p1, p0, Lzg;->a:Z

    return p1
.end method


# virtual methods
.method public a(LMB;)V
    .registers 6
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, Lzg;->a:LzS;

    if-eqz v0, :cond_11

    .line 106
    iget-object v0, p0, Lzg;->a:LzS;

    invoke-virtual {p1}, LMB;->a()F

    move-result v1

    invoke-virtual {p1}, LMB;->b()F

    move-result v2

    invoke-interface {v0, v1, v2}, LzS;->b(FF)V

    .line 109
    :cond_11
    iget-object v0, p0, Lzg;->a:Landroid/os/Handler;

    iget-object v1, p0, Lzg;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 110
    iget-object v0, p0, Lzg;->a:Landroid/os/Handler;

    iget-object v1, p0, Lzg;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 111
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lzg;->a:LMB;

    invoke-virtual {v0, p1}, LMB;->a(Landroid/view/MotionEvent;)Z

    .line 47
    return-void
.end method

.method public a(LzS;)V
    .registers 2
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lzg;->a:LzS;

    .line 52
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 56
    iget-boolean v0, p0, Lzg;->a:Z

    return v0
.end method

.method public a(LMB;)Z
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 61
    iget-object v1, p0, Lzg;->a:LzS;

    if-nez v1, :cond_7

    .line 62
    const/4 v0, 0x0

    .line 71
    :goto_6
    return v0

    .line 65
    :cond_7
    iget-object v1, p0, Lzg;->a:Landroid/os/Handler;

    iget-object v2, p0, Lzg;->a:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 66
    iput-boolean v0, p0, Lzg;->a:Z

    .line 69
    iget-object v1, p0, Lzg;->a:LzS;

    invoke-virtual {p1}, LMB;->a()F

    move-result v2

    invoke-virtual {p1}, LMB;->b()F

    move-result v3

    invoke-interface {v1, v2, v3}, LzS;->a(FF)V

    goto :goto_6
.end method

.method public b(LMB;)Z
    .registers 8
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lzg;->a:LzS;

    if-nez v0, :cond_6

    .line 77
    const/4 v0, 0x0

    .line 100
    :goto_5
    return v0

    .line 80
    :cond_6
    invoke-virtual {p1}, LMB;->c()F

    move-result v0

    .line 81
    iget-object v1, p0, Lzg;->a:LzS;

    invoke-interface {v1}, LzS;->a()F

    move-result v2

    .line 82
    mul-float v1, v2, v0

    .line 85
    iget v0, p0, Lzg;->a:F

    cmpg-float v0, v1, v0

    if-gez v0, :cond_49

    .line 86
    iget v1, p0, Lzg;->a:F

    .line 91
    :cond_1a
    :goto_1a
    div-float v0, v1, v2

    .line 93
    invoke-virtual {p1}, LMB;->a()F

    move-result v4

    .line 94
    invoke-virtual {p1}, LMB;->b()F

    move-result v5

    .line 96
    iget-object v2, p0, Lzg;->a:LzS;

    invoke-interface {v2}, LzS;->a()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {p0, v0, v4, v2}, Lzg;->a(FFF)F

    move-result v2

    .line 97
    iget-object v3, p0, Lzg;->a:LzS;

    invoke-interface {v3}, LzS;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getScrollY()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {p0, v0, v5, v3}, Lzg;->a(FFF)F

    move-result v3

    .line 99
    iget-object v0, p0, Lzg;->a:LzS;

    invoke-interface/range {v0 .. v5}, LzS;->a(FFFFF)V

    .line 100
    const/4 v0, 0x1

    goto :goto_5

    .line 87
    :cond_49
    iget v0, p0, Lzg;->b:F

    cmpl-float v0, v1, v0

    if-lez v0, :cond_1a

    .line 88
    iget v1, p0, Lzg;->b:F

    goto :goto_1a
.end method
