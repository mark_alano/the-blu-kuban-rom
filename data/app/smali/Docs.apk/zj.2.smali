.class public Lzj;
.super Ljava/lang/Object;
.source "RedrawManager.java"

# interfaces
.implements LDu;
.implements LyF;
.implements LyH;
.implements LyQ;
.implements LyU;
.implements LyX;
.implements Lyj;
.implements LzH;
.implements Lzr;


# static fields
.field static a:LAh;
    .annotation runtime Laon;
    .end annotation
.end field

.field static a:LAo;
    .annotation runtime Laon;
    .end annotation
.end field

.field static a:LeQ;
    .annotation runtime Laon;
    .end annotation
.end field

.field private static final a:Ljava/lang/Object;


# instance fields
.field private final a:I

.field private a:LAx;

.field private a:LDa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDa",
            "<",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LDj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LDj",
            "<",
            "LAx;",
            "LAx;",
            ">;"
        }
    .end annotation
.end field

.field private final a:LDp;

.field private final a:Lcom/google/android/apps/docs/editors/kix/CursorTracker;

.field private final a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lzo;",
            ">;"
        }
    .end annotation
.end field

.field private final a:Lth;

.field private a:LuV;

.field private a:LvZ;

.field private a:LwF;

.field private a:Lxa;

.field private final a:Lxu;

.field private a:Lzd;

.field private final a:Z

.field private final b:Z

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 82
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lzj;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LKS;LAS;Lcom/google/android/apps/docs/editors/kix/KixEditText;ZZLzP;LxI;Lzi;Lth;Z)V
    .registers 25
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v1, 0x0

    iput-object v1, p0, Lzj;->a:LvZ;

    .line 91
    const/4 v1, 0x0

    iput-object v1, p0, Lzj;->a:LwF;

    .line 92
    const/4 v1, 0x0

    iput-object v1, p0, Lzj;->a:Lxa;

    .line 93
    const/4 v1, 0x0

    iput-object v1, p0, Lzj;->a:Lzd;

    .line 94
    const/4 v1, 0x0

    iput-object v1, p0, Lzj;->a:LuV;

    .line 101
    const/4 v1, 0x0

    iput-object v1, p0, Lzj;->a:LAx;

    .line 108
    const/4 v1, 0x1

    iput-boolean v1, p0, Lzj;->c:Z

    .line 111
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lzj;->a:Ljava/util/Set;

    .line 135
    const-string v1, "kixEnableResetHack"

    const/4 v2, 0x1

    invoke-interface {p2, v1, v2}, LKS;->a(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lzj;->a:Z

    .line 136
    move-object/from16 v0, p4

    iput-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    .line 137
    move/from16 v0, p5

    iput-boolean v0, p0, Lzj;->b:Z

    .line 138
    new-instance v1, Lzk;

    invoke-direct {v1, p0}, Lzk;-><init>(Lzj;)V

    iput-object v1, p0, Lzj;->a:LDj;

    .line 149
    new-instance v1, Lcom/google/android/apps/docs/editors/kix/CursorTracker;

    iget-object v2, p0, Lzj;->a:LDj;

    invoke-direct {v1, v2}, Lcom/google/android/apps/docs/editors/kix/CursorTracker;-><init>(Landroid/text/Spannable;)V

    iput-object v1, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/CursorTracker;

    .line 150
    iget-object v1, p0, Lzj;->a:LDj;

    invoke-virtual {v1}, LDj;->a()LDp;

    move-result-object v1

    iput-object v1, p0, Lzj;->a:LDp;

    .line 151
    iget-object v1, p0, Lzj;->a:LDp;

    invoke-virtual {v1, p0}, LDp;->a(LDu;)V

    .line 152
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lzj;->a(Landroid/content/res/Resources;LKS;)F

    move-result v5

    .line 154
    const-string v1, "kixMinZoom"

    const-wide/high16 v2, 0x3ff0

    invoke-interface {p2, v1, v2, v3}, LKS;->a(Ljava/lang/String;D)D

    move-result-wide v1

    double-to-float v6, v1

    .line 156
    const-string v1, "kixMaxZoom"

    const-wide/high16 v2, 0x4008

    invoke-interface {p2, v1, v2, v3}, LKS;->a(Ljava/lang/String;D)D

    move-result-wide v1

    double-to-float v7, v1

    .line 158
    new-instance v1, Lxv;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lzj;->a:LDp;

    move-object/from16 v3, p3

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    invoke-direct/range {v1 .. v12}, Lxv;-><init>(Landroid/content/Context;LAS;Landroid/text/Editable;FFFZLzP;LxI;Lzi;Lth;)V

    iput-object v1, p0, Lzj;->a:Lxu;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move/from16 v8, p11

    .line 161
    invoke-direct/range {v3 .. v8}, Lzj;->a(Landroid/content/Context;LKS;FFZ)V

    .line 162
    move-object/from16 v0, p10

    iput-object v0, p0, Lzj;->a:Lth;

    .line 163
    if-eqz p10, :cond_95

    .line 164
    iget-object v1, p0, Lzj;->a:Lth;

    iget-object v2, p0, Lzj;->a:LDj;

    invoke-virtual {v1, v2}, Lth;->a(LDA;)V

    .line 171
    :cond_95
    const-string v1, "kixPageContentWidth"

    const/16 v2, 0x264

    invoke-interface {p2, v1, v2}, LKS;->a(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lzj;->a:I

    .line 175
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setTextIsSelectable(Z)V

    .line 177
    new-instance v1, Lzl;

    invoke-direct {v1, p0}, Lzl;-><init>(Lzj;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setEditableFactory(Landroid/text/Editable$Factory;)V

    .line 190
    return-void
.end method

.method private a(Landroid/content/res/Resources;LKS;)F
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 193
    invoke-static {p1}, LZL;->b(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 194
    const-string v0, "kixDefaultSevenInchTabletZoom"

    const-wide v1, 0x3fe99999a0000000L

    invoke-interface {p2, v0, v1, v2}, LKS;->a(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 200
    :goto_12
    return v0

    .line 196
    :cond_13
    invoke-static {p1}, LZL;->d(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 197
    const-string v0, "kixDefaultTabletZoom"

    const-wide/high16 v1, 0x3ff0

    invoke-interface {p2, v0, v1, v2}, LKS;->a(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_12

    .line 200
    :cond_23
    const-string v0, "kixDefaultPhoneZoom"

    const-wide v1, 0x3fe570a3e0000000L

    invoke-interface {p2, v0, v1, v2}, LKS;->a(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_12
.end method

.method static synthetic a(Lzj;)LDp;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lzj;->a:LDp;

    return-object v0
.end method

.method static synthetic a(Lzj;)Lxa;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lzj;->a:Lxa;

    return-object v0
.end method

.method static synthetic a(Lzj;)Lxu;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lzj;->a:Lxu;

    return-object v0
.end method

.method static synthetic a(Lzj;)Lzd;
    .registers 2
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lzj;->a:Lzd;

    return-object v0
.end method

.method private a(Landroid/content/Context;LKS;FFZ)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 209
    if-eqz p5, :cond_12

    .line 210
    new-instance v0, Lzg;

    invoke-direct {v0, p1, p3, p4}, Lzg;-><init>(Landroid/content/Context;FF)V

    .line 217
    :goto_7
    iget-object v1, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-interface {v0, v1}, LzR;->a(LzS;)V

    .line 218
    iget-object v1, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setZoomManager(LzR;)V

    .line 219
    return-void

    .line 212
    :cond_12
    new-instance v0, Lzq;

    iget-object v1, p0, Lzj;->a:Lxu;

    invoke-direct {v0, v1}, Lzq;-><init>(Lxu;)V

    .line 213
    invoke-virtual {v0, p0}, Lzq;->a(Lzr;)V

    goto :goto_7
.end method

.method private a(Ljava/lang/CharSequence;II)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    .line 375
    iget-object v0, p0, Lzj;->a:LDp;

    sget-object v1, Lzj;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, LDp;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 376
    iget-object v0, p0, Lzj;->a:LDp;

    sget-object v2, Lzj;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2}, LDp;->getSpanEnd(Ljava/lang/Object;)I

    move-result v2

    .line 377
    iget-object v0, p0, Lzj;->a:LDp;

    sget-object v3, Lzj;->a:Ljava/lang/Object;

    invoke-virtual {v0, v3}, LDp;->removeSpan(Ljava/lang/Object;)V

    .line 381
    if-le v2, v1, :cond_3d

    if-ltz v1, :cond_3d

    .line 382
    instance-of v0, p1, Landroid/text/Spanned;

    if-eqz v0, :cond_3d

    .line 383
    check-cast p1, Landroid/text/Spanned;

    .line 384
    const-class v0, Ljava/lang/Object;

    invoke-interface {p1, p2, p3, v0}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    .line 385
    array-length v4, v3

    const/4 v0, 0x0

    :goto_29
    if-ge v0, v4, :cond_3d

    aget-object v5, v3, v0

    .line 386
    invoke-interface {p1, v5}, Landroid/text/Spanned;->getSpanFlags(Ljava/lang/Object;)I

    move-result v6

    .line 387
    and-int/lit16 v7, v6, 0x100

    if-lez v7, :cond_3a

    .line 388
    iget-object v7, p0, Lzj;->a:LDp;

    invoke-virtual {v7, v5, v1, v2, v6}, LDp;->setSpan(Ljava/lang/Object;III)V

    .line 385
    :cond_3a
    add-int/lit8 v0, v0, 0x1

    goto :goto_29

    .line 393
    :cond_3d
    return-void
.end method

.method private b(IILjava/lang/CharSequence;II)V
    .registers 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 233
    invoke-interface {p3, p4, p5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    .line 236
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 237
    iget-object v4, p0, Lzj;->a:LAx;

    invoke-interface {v4, p1, p2, v0}, LAx;->a(IILjava/util/List;)V

    .line 238
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_17

    .line 331
    :goto_16
    return-void

    .line 241
    :cond_17
    invoke-static {v0}, LKj;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 243
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v4, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKj;

    .line 244
    iget-object v5, p0, Lzj;->a:LAx;

    invoke-virtual {v0}, LKj;->a()I

    move-result v6

    invoke-interface {v5, v6}, LAx;->c(I)I

    move-result v5

    .line 245
    iget-object v6, p0, Lzj;->a:LAx;

    invoke-virtual {v0}, LKj;->b()I

    move-result v7

    invoke-interface {v6, v7}, LAx;->c(I)I

    move-result v6

    .line 249
    iget-object v7, p0, Lzj;->a:LuV;

    invoke-interface {v7}, LuV;->b()V

    .line 251
    if-gt v5, p1, :cond_109

    if-lt v6, p2, :cond_109

    .line 254
    :try_start_44
    const-string v7, "\n"

    invoke-virtual {p3, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d8

    iget-object v7, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v7}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v7

    if-ne p2, v7, :cond_d8

    iget-object v7, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v7}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v7

    if-ne p1, v7, :cond_d8

    .line 261
    iget-object v2, p0, Lzj;->a:LvZ;

    iget v7, p0, Lzj;->a:I

    invoke-interface {v2, v7}, LvZ;->a(I)V

    .line 283
    :goto_63
    if-nez v1, :cond_123

    .line 286
    iget-object v1, p0, Lzj;->a:LDp;

    invoke-virtual {v1, v5, v6}, LDp;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    .line 288
    invoke-static {v3, v1}, LagE;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    .line 289
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {v3, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    .line 290
    add-int/2addr v1, v5

    .line 291
    iget-object v3, p0, Lzj;->a:LAx;

    invoke-interface {v3, v1}, LAx;->d(I)I

    move-result v3

    .line 292
    invoke-virtual {v0}, LKj;->b()I

    move-result v0

    .line 297
    invoke-direct {p0, p1, v1}, Lzj;->c(II)V

    .line 311
    invoke-direct {p0, v5, v6}, Lzj;->d(II)V

    .line 314
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-gtz v1, :cond_94

    if-ge v3, v0, :cond_9f

    .line 315
    :cond_94
    iget-object v1, p0, Lzj;->a:LvZ;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3, v0, v2}, LvZ;->a(IILjava/lang/String;)V

    .line 318
    :cond_9f
    invoke-direct {p0, p3, p4, p5}, Lzj;->a(Ljava/lang/CharSequence;II)V

    .line 321
    invoke-static {v4}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 322
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a9
    :goto_a9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_123

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LKj;

    .line 323
    invoke-virtual {v0}, LKj;->a()I

    move-result v2

    invoke-virtual {v0}, LKj;->b()I

    move-result v3

    if-ge v2, v3, :cond_a9

    .line 324
    iget-object v2, p0, Lzj;->a:LvZ;

    invoke-virtual {v0}, LKj;->a()I

    move-result v3

    invoke-virtual {v0}, LKj;->b()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const-string v4, ""

    invoke-interface {v2, v3, v0, v4}, LvZ;->a(IILjava/lang/String;)V
    :try_end_d0
    .catchall {:try_start_44 .. :try_end_d0} :catchall_d1

    goto :goto_a9

    .line 329
    :catchall_d1
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0

    .line 263
    :cond_d8
    :try_start_d8
    const-string v7, ""

    invoke-virtual {p3, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_120

    iget-object v7, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v7}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v7

    if-ne p2, v7, :cond_120

    iget-object v7, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v7}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v7

    if-eq p1, v7, :cond_102

    add-int/lit8 v7, p2, -0x1

    if-ne p1, v7, :cond_120

    iget-object v7, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v7}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v7

    iget-object v8, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v8}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v8

    if-ne v7, v8, :cond_120

    .line 272
    :cond_102
    iget-object v2, p0, Lzj;->a:LvZ;

    invoke-interface {v2}, LvZ;->c()V

    goto/16 :goto_63

    .line 277
    :cond_109
    iget-object v1, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v7, LsH;->can_not_be_deleted:I

    const/4 v8, 0x1

    invoke-static {v1, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    .line 279
    const/16 v7, 0x11

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v1, v7, v8, v9}, Landroid/widget/Toast;->setGravity(III)V

    .line 280
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_120
    .catchall {:try_start_d8 .. :try_end_120} :catchall_d1

    :cond_120
    move v1, v2

    goto/16 :goto_63

    .line 329
    :cond_123
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    goto/16 :goto_16
.end method

.method private c(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 339
    iget-object v1, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v3

    .line 340
    iget-object v1, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v2

    .line 341
    const/4 v1, 0x0

    .line 342
    if-lt v3, p1, :cond_14

    if-ge v3, p2, :cond_14

    move v1, v0

    move v3, p2

    .line 346
    :cond_14
    if-lt v2, p1, :cond_20

    if-ge v2, p2, :cond_20

    .line 350
    :goto_18
    if-eqz v0, :cond_1f

    .line 351
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, v3, p2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setSelection(II)V

    .line 353
    :cond_1f
    return-void

    :cond_20
    move v0, v1

    move p2, v2

    goto :goto_18
.end method

.method private d(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 364
    iget-object v0, p0, Lzj;->a:LDp;

    sget-object v1, Lzj;->a:Ljava/lang/Object;

    const/16 v2, 0x12

    invoke-virtual {v0, v1, p1, p2, v2}, LDp;->setSpan(Ljava/lang/Object;III)V

    .line 366
    return-void
.end method

.method private n()V
    .registers 2

    .prologue
    .line 483
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->a(Lyj;)V

    .line 484
    return-void
.end method

.method private o()V
    .registers 2

    .prologue
    .line 490
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b(Lyj;)V

    .line 491
    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 535
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->n()V

    .line 536
    iget-object v0, p0, Lzj;->a:LDp;

    invoke-virtual {v0}, LDp;->a()V

    .line 538
    :try_start_a
    invoke-direct {p0}, Lzj;->o()V

    .line 539
    iget-object v0, p0, Lzj;->a:LAx;

    new-instance v1, Lzm;

    invoke-direct {v1, p0}, Lzm;-><init>(Lzj;)V

    invoke-interface {v0, v1}, LAx;->a(Lza;)V
    :try_end_17
    .catchall {:try_start_a .. :try_end_17} :catchall_25

    .line 548
    iget-object v0, p0, Lzj;->a:LDp;

    invoke-virtual {v0}, LDp;->b()V

    .line 549
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()V

    .line 550
    invoke-direct {p0}, Lzj;->n()V

    .line 552
    return-void

    .line 548
    :catchall_25
    move-exception v0

    iget-object v1, p0, Lzj;->a:LDp;

    invoke-virtual {v1}, LDp;->b()V

    .line 549
    iget-object v1, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()V

    .line 550
    invoke-direct {p0}, Lzj;->n()V

    throw v0
.end method

.method private q()V
    .registers 6

    .prologue
    .line 555
    invoke-direct {p0}, Lzj;->p()V

    .line 557
    new-instance v0, LAH;

    iget-object v1, p0, Lzj;->a:LDa;

    invoke-virtual {v1}, LDa;->a()LDb;

    move-result-object v1

    sget-object v2, Lzj;->a:LAo;

    iget-object v3, p0, Lzj;->a:Lzd;

    iget-object v4, p0, Lzj;->a:Lxu;

    invoke-direct {v0, v1, v2, v3, v4}, LAH;-><init>(LDb;LAb;Lzd;Lxu;)V

    iput-object v0, p0, Lzj;->a:LAx;

    .line 558
    iget-object v0, p0, Lzj;->a:LDj;

    iget-object v1, p0, Lzj;->a:LAx;

    invoke-interface {v1}, LAx;->a()LDG;

    move-result-object v1

    invoke-virtual {v0, v1}, LDj;->a(LDG;)V

    .line 559
    return-void
.end method

.method private r()V
    .registers 3

    .prologue
    .line 856
    iget-object v0, p0, Lzj;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzo;

    .line 857
    invoke-interface {v0}, Lzo;->a()V

    goto :goto_6

    .line 859
    :cond_16
    return-void
.end method


# virtual methods
.method public a()LCh;
    .registers 3

    .prologue
    .line 618
    iget-object v0, p0, Lzj;->a:LwF;

    if-nez v0, :cond_6

    .line 619
    const/4 v0, 0x0

    .line 626
    :goto_5
    return-object v0

    .line 622
    :cond_6
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 624
    :try_start_b
    new-instance v0, LCh;

    iget-object v1, p0, Lzj;->a:Lxa;

    invoke-interface {v1}, Lxa;->a()Lvm;

    move-result-object v1

    invoke-direct {v0, v1}, LCh;-><init>(Lvm;)V
    :try_end_16
    .catchall {:try_start_b .. :try_end_16} :catchall_1c

    .line 626
    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    goto :goto_5

    :catchall_1c
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public a(II)LCh;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 632
    iget-object v1, p0, Lzj;->a:LAx;

    if-nez v1, :cond_6

    .line 660
    :goto_5
    return-object v0

    .line 636
    :cond_6
    new-instance v0, LCh;

    invoke-direct {v0}, LCh;-><init>()V

    .line 637
    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->b()V

    .line 643
    :try_start_10
    iget-object v1, p0, Lzj;->a:LAx;

    invoke-interface {v1, p2}, LAx;->d(I)I

    move-result v1

    .line 644
    new-instance v2, LCw;

    iget-object v3, p0, Lzj;->a:LwF;

    add-int/lit8 v4, v1, -0x1

    invoke-interface {v3, v4}, LwF;->a(I)Lxg;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lzj;->a:Lxu;

    invoke-direct {v2, v3, v4, v5}, LCw;-><init>(Lxg;Lvo;Lxu;)V

    .line 646
    invoke-virtual {v0, v2}, LCh;->a(LCw;)Z

    .line 649
    new-instance v2, LCq;

    iget-object v3, p0, Lzj;->a:LwF;

    invoke-interface {v3, v1}, LwF;->a(I)Lxc;

    move-result-object v3

    iget-object v4, p0, Lzj;->a:Lxu;

    iget-object v5, p0, Lzj;->a:LwF;

    invoke-interface {v5, v1}, LwF;->a(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lzj;->a:LwF;

    invoke-interface {v6, v1}, LwF;->b(I)Lxg;

    move-result-object v1

    invoke-direct {v2, v3, v4, v5, v1}, LCq;-><init>(Lxc;Lxu;Ljava/lang/String;Lxg;)V

    .line 652
    invoke-virtual {v0, v2}, LCh;->a(LCq;)Z

    .line 655
    iget-object v1, p0, Lzj;->a:LAx;

    invoke-interface {v1, p1, p2, v0}, LAx;->a(IILCh;)Z
    :try_end_4a
    .catchall {:try_start_10 .. :try_end_4a} :catchall_50

    .line 657
    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    goto :goto_5

    :catchall_50
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public a(Lvw;Lvz;)LyL;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 872
    sget-object v0, Lzj;->a:LAo;

    invoke-virtual {v0, p2}, LAo;->a(Lvz;)V

    .line 874
    new-instance v0, Lzn;

    invoke-direct {v0, p0, p1}, Lzn;-><init>(Lzj;Lvw;)V

    return-object v0
.end method

.method public a(Lvw;)LyR;
    .registers 4
    .parameter

    .prologue
    .line 884
    new-instance v0, Lzt;

    iget-object v1, p0, Lzj;->a:Lxu;

    invoke-direct {v0, v1, p1}, Lzt;-><init>(Lxu;Lvw;)V

    return-object v0
.end method

.method public a()V
    .registers 2

    .prologue
    .line 529
    iget-boolean v0, p0, Lzj;->a:Z

    if-nez v0, :cond_7

    .line 530
    invoke-direct {p0}, Lzj;->q()V

    .line 532
    :cond_7
    return-void
.end method

.method public a(I)V
    .registers 5
    .parameter

    .prologue
    .line 779
    const-string v0, "RedrawManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setParagraphAlignment "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 782
    :try_start_1d
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 783
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0, p1}, LvZ;->b(I)V
    :try_end_27
    .catchall {:try_start_1d .. :try_end_27} :catchall_2d

    .line 785
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 787
    return-void

    .line 785
    :catchall_2d
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 397
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 399
    :try_start_5
    iget-object v0, p0, Lzj;->a:LAx;

    invoke-interface {v0, p1}, LAx;->d(I)I

    move-result v0

    .line 400
    iget-object v1, p0, Lzj;->a:LAx;

    invoke-interface {v1, p2}, LAx;->d(I)I

    move-result v1

    .line 401
    if-ne p1, p2, :cond_1f

    .line 402
    iget-object v1, p0, Lzj;->a:LvZ;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LvZ;->a(IZ)V
    :try_end_19
    .catchall {:try_start_5 .. :try_end_19} :catchall_25

    .line 409
    :goto_19
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 411
    return-void

    .line 406
    :cond_1f
    :try_start_1f
    iget-object v2, p0, Lzj;->a:LvZ;

    invoke-interface {v2, v0, v1}, LvZ;->a(II)V
    :try_end_24
    .catchall {:try_start_1f .. :try_end_24} :catchall_25

    goto :goto_19

    .line 409
    :catchall_25
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public a(IILjava/lang/CharSequence;II)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 223
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->b()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 224
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 225
    invoke-direct/range {p0 .. p5}, Lzj;->b(IILjava/lang/CharSequence;II)V

    .line 226
    invoke-direct {p0}, Lzj;->r()V

    .line 230
    :goto_13
    return-void

    .line 228
    :cond_14
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, LsH;->enter_edit_mode:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_13
.end method

.method public a(Ljava/lang/Integer;)V
    .registers 5
    .parameter

    .prologue
    .line 802
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 804
    :try_start_5
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 805
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-double v1, v1

    invoke-interface {v0, v1, v2}, LvZ;->a(D)V
    :try_end_14
    .catchall {:try_start_5 .. :try_end_14} :catchall_1a

    .line 807
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 809
    return-void

    .line 807
    :catchall_1a
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 575
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/CursorTracker;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a(Ljava/lang/String;)V

    .line 576
    const-string v0, "RedrawManager"

    const-string v1, "Cursor removed"

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    return-void
.end method

.method public a(Ljava/lang/String;II)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 586
    iget-object v0, p0, Lzj;->a:LAx;

    invoke-interface {v0, p3}, LAx;->c(I)I

    move-result v0

    .line 587
    iget-object v1, p0, Lzj;->a:LAx;

    invoke-interface {v1, p2}, LAx;->c(I)I

    move-result v1

    .line 589
    iget-object v2, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/CursorTracker;

    invoke-virtual {v2, p1, v1, v0}, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a(Ljava/lang/String;II)V

    .line 590
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 582
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 563
    const v0, -0x7f3f3f01

    .line 565
    :try_start_3
    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_6} :catch_d

    move-result v0

    .line 570
    :goto_7
    iget-object v1, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/CursorTracker;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/apps/docs/editors/kix/CursorTracker;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 571
    return-void

    .line 566
    :catch_d
    move-exception v1

    .line 567
    const-string v1, "RedrawManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid color "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7
.end method

.method public a(LwF;)V
    .registers 2
    .parameter

    .prologue
    .line 824
    return-void
.end method

.method public a(LwF;LvZ;Lxa;)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 495
    iput-object p1, p0, Lzj;->a:LwF;

    .line 496
    iput-object p2, p0, Lzj;->a:LvZ;

    .line 497
    iput-object p3, p0, Lzj;->a:Lxa;

    .line 498
    invoke-interface {p1}, LwF;->a()LuV;

    move-result-object v0

    iput-object v0, p0, Lzj;->a:LuV;

    .line 499
    new-instance v0, Lze;

    invoke-direct {v0, p1}, Lze;-><init>(LwF;)V

    iput-object v0, p0, Lzj;->a:Lzd;

    .line 500
    new-instance v0, LDa;

    invoke-direct {v0}, LDa;-><init>()V

    iput-object v0, p0, Lzj;->a:LDa;

    .line 501
    invoke-interface {p3}, Lxa;->a()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 502
    sget-object v0, Lzj;->a:LAo;

    invoke-interface {p3}, Lxa;->a()Lvz;

    move-result-object v1

    invoke-virtual {v0, v1}, LAo;->a(Lvz;)V

    .line 504
    :cond_29
    new-instance v0, LAH;

    iget-object v1, p0, Lzj;->a:LDa;

    invoke-virtual {v1}, LDa;->a()LDb;

    move-result-object v1

    sget-object v2, Lzj;->a:LAo;

    iget-object v3, p0, Lzj;->a:Lzd;

    iget-object v4, p0, Lzj;->a:Lxu;

    invoke-direct {v0, v1, v2, v3, v4}, LAH;-><init>(LDb;LAb;Lzd;Lxu;)V

    iput-object v0, p0, Lzj;->a:LAx;

    .line 505
    iget-object v0, p0, Lzj;->a:LDj;

    iget-object v1, p0, Lzj;->a:LAx;

    invoke-interface {v1}, LAx;->a()LDG;

    move-result-object v1

    invoke-virtual {v0, v1}, LDj;->a(LDG;)V

    .line 506
    invoke-direct {p0}, Lzj;->o()V

    .line 507
    invoke-direct {p0}, Lzj;->n()V

    .line 508
    return-void
.end method

.method public a(Lwj;LwL;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 436
    iget-object v0, p0, Lzj;->a:Lzd;

    invoke-interface {v0}, Lzd;->a()V

    .line 442
    iget-boolean v0, p0, Lzj;->a:Z

    if-eqz v0, :cond_1b

    invoke-static {p1}, Lzp;->a(Lwj;)Lzp;

    move-result-object v0

    sget-object v1, Lzp;->c:Lzp;

    if-ne v0, v1, :cond_1b

    invoke-interface {p1}, Lwj;->b()I

    move-result v0

    if-nez v0, :cond_1b

    .line 445
    invoke-direct {p0}, Lzj;->q()V

    .line 449
    :cond_1b
    sget-object v0, Lzj;->a:LAo;

    invoke-virtual {v0, p2}, LAo;->a(LwL;)V

    .line 451
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->n()V

    .line 452
    iget-object v0, p0, Lzj;->a:LDp;

    invoke-virtual {v0}, LDp;->a()V

    .line 454
    :try_start_2a
    invoke-direct {p0}, Lzj;->o()V

    .line 458
    invoke-static {p1}, Lzp;->a(Lwj;)Lzp;

    move-result-object v0

    sget-object v1, Lzp;->a:Lzp;

    if-eq v0, v1, :cond_51

    .line 459
    iget-object v0, p0, Lzj;->a:LwF;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LwF;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 460
    iget-object v1, p0, Lzj;->a:LAx;

    new-instance v2, Lzb;

    invoke-direct {v2, p1}, Lzb;-><init>(Lwj;)V

    invoke-interface {v1, p1, v2, v0}, LAx;->a(Lwj;Lza;I)V

    .line 461
    iget-object v0, p0, Lzj;->a:Lth;

    if-eqz v0, :cond_51

    .line 462
    iget-object v0, p0, Lzj;->a:Lth;

    invoke-virtual {v0}, Lth;->c()V
    :try_end_51
    .catchall {:try_start_2a .. :try_end_51} :catchall_71

    .line 466
    :cond_51
    iget-object v0, p0, Lzj;->a:LDp;

    invoke-virtual {v0}, LDp;->b()V

    .line 467
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()V

    .line 468
    invoke-direct {p0}, Lzj;->n()V

    .line 471
    iget-boolean v0, p0, Lzj;->c:Z

    if-eqz v0, :cond_70

    .line 473
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setSelection(I)V

    .line 474
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->sendAccessibilityEvent(I)V

    .line 475
    iput-boolean v3, p0, Lzj;->c:Z

    .line 477
    :cond_70
    return-void

    .line 466
    :catchall_71
    move-exception v0

    iget-object v1, p0, Lzj;->a:LDp;

    invoke-virtual {v1}, LDp;->b()V

    .line 467
    iget-object v1, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()V

    .line 468
    invoke-direct {p0}, Lzj;->n()V

    throw v0
.end method

.method public a(Lzo;)V
    .registers 3
    .parameter

    .prologue
    .line 844
    iget-object v0, p0, Lzj;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 845
    return-void
.end method

.method public a(ZLjava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 813
    if-eqz p1, :cond_9

    .line 814
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setEditable(Z)V

    .line 819
    :goto_8
    return-void

    .line 817
    :cond_9
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-boolean v1, p0, Lzj;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setEditable(Z)V

    goto :goto_8
.end method

.method public b()V
    .registers 3

    .prologue
    .line 515
    iget-object v0, p0, Lzj;->a:LuV;

    if-eqz v0, :cond_11

    .line 516
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 518
    :try_start_9
    invoke-direct {p0}, Lzj;->p()V
    :try_end_c
    .catchall {:try_start_9 .. :try_end_c} :catchall_12

    .line 520
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 523
    :cond_11
    return-void

    .line 520
    :catchall_12
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public b(II)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 417
    iget-object v0, p0, Lzj;->a:LAx;

    invoke-interface {v0, p2}, LAx;->c(I)I

    move-result v0

    .line 418
    iget-object v1, p0, Lzj;->a:LAx;

    invoke-interface {v1, p1}, LAx;->c(I)I

    move-result v1

    .line 419
    iget-object v2, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v2}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->o()I

    move-result v2

    .line 420
    iget-object v3, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v3}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->p()I

    move-result v3

    .line 421
    if-ne v0, v2, :cond_1c

    if-eq v1, v3, :cond_62

    .line 422
    :cond_1c
    const-string v4, "RedrawManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Model updated selection. Start: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " End: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Old start: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " Old end: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Laaz;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    :try_start_52
    invoke-direct {p0}, Lzj;->o()V

    .line 426
    iget-object v2, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setSelection(II)V

    .line 427
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->i()V
    :try_end_5f
    .catchall {:try_start_52 .. :try_end_5f} :catchall_63

    .line 429
    invoke-direct {p0}, Lzj;->n()V

    .line 432
    :cond_62
    return-void

    .line 429
    :catchall_63
    move-exception v0

    invoke-direct {p0}, Lzj;->n()V

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 749
    if-nez p1, :cond_3

    .line 760
    :goto_2
    return-void

    .line 752
    :cond_3
    const-string v0, "RedrawManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ApplyBackgroundColor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 755
    :try_start_20
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 756
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0, p1}, LvZ;->c(Ljava/lang/String;)V
    :try_end_2a
    .catchall {:try_start_20 .. :try_end_2a} :catchall_30

    .line 758
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    goto :goto_2

    :catchall_30
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public b(Lzo;)V
    .registers 3
    .parameter

    .prologue
    .line 852
    iget-object v0, p0, Lzj;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 853
    return-void
.end method

.method public c()V
    .registers 4

    .prologue
    .line 594
    sget-object v0, Lzj;->a:LeQ;

    const-string v1, "kixEditor"

    const-string v2, "kixUndo"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 597
    :try_start_e
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 598
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0}, LvZ;->d()V
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_1e

    .line 600
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 602
    return-void

    .line 600
    :catchall_1e
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public c(Ljava/lang/String;)V
    .registers 5
    .parameter

    .prologue
    .line 764
    if-nez p1, :cond_3

    .line 775
    :goto_2
    return-void

    .line 767
    :cond_3
    const-string v0, "RedrawManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ApplyForegroundColor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Laaz;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 770
    :try_start_20
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 771
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0, p1}, LvZ;->d(Ljava/lang/String;)V
    :try_end_2a
    .catchall {:try_start_20 .. :try_end_2a} :catchall_30

    .line 773
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    goto :goto_2

    :catchall_30
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public d()V
    .registers 4

    .prologue
    .line 606
    sget-object v0, Lzj;->a:LeQ;

    const-string v1, "kixEditor"

    const-string v2, "kixRedo"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 609
    :try_start_e
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 610
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0}, LvZ;->e()V
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_1e

    .line 612
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 614
    return-void

    .line 612
    :catchall_1e
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public d(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 791
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 793
    :try_start_5
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 794
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0, p1}, LvZ;->b(Ljava/lang/String;)V
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_15

    .line 796
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 798
    return-void

    .line 796
    :catchall_15
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public e()V
    .registers 4

    .prologue
    .line 665
    sget-object v0, Lzj;->a:LeQ;

    const-string v1, "kixEditor"

    const-string v2, "kixBold"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 668
    :try_start_e
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 669
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0}, LvZ;->f()V
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_21

    .line 671
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 673
    invoke-direct {p0}, Lzj;->r()V

    .line 674
    return-void

    .line 671
    :catchall_21
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public f()V
    .registers 4

    .prologue
    .line 678
    sget-object v0, Lzj;->a:LeQ;

    const-string v1, "kixEditor"

    const-string v2, "kixItalic"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 681
    :try_start_e
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 682
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0}, LvZ;->g()V
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_21

    .line 684
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 686
    invoke-direct {p0}, Lzj;->r()V

    .line 687
    return-void

    .line 684
    :catchall_21
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public g()V
    .registers 4

    .prologue
    .line 691
    sget-object v0, Lzj;->a:LeQ;

    const-string v1, "kixEditor"

    const-string v2, "kixUnderline"

    invoke-virtual {v0, v1, v2}, LeQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 695
    :try_start_e
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 696
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0}, LvZ;->h()V
    :try_end_18
    .catchall {:try_start_e .. :try_end_18} :catchall_21

    .line 698
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 700
    invoke-direct {p0}, Lzj;->r()V

    .line 701
    return-void

    .line 698
    :catchall_21
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public h()V
    .registers 3

    .prologue
    .line 705
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 707
    :try_start_5
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 708
    iget-object v0, p0, Lzj;->a:LvZ;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LvZ;->a(Z)V
    :try_end_10
    .catchall {:try_start_5 .. :try_end_10} :catchall_16

    .line 710
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 712
    return-void

    .line 710
    :catchall_16
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public i()V
    .registers 3

    .prologue
    .line 716
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 718
    :try_start_5
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 719
    iget-object v0, p0, Lzj;->a:LvZ;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LvZ;->a(Z)V
    :try_end_10
    .catchall {:try_start_5 .. :try_end_10} :catchall_16

    .line 721
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 723
    return-void

    .line 721
    :catchall_16
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public j()V
    .registers 3

    .prologue
    .line 727
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 729
    :try_start_5
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 730
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0}, LvZ;->i()V
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_15

    .line 732
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 734
    return-void

    .line 732
    :catchall_15
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public k()V
    .registers 3

    .prologue
    .line 738
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->b()V

    .line 740
    :try_start_5
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    invoke-virtual {v0}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->h()V

    .line 741
    iget-object v0, p0, Lzj;->a:LvZ;

    invoke-interface {v0}, LvZ;->j()V
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_15

    .line 743
    iget-object v0, p0, Lzj;->a:LuV;

    invoke-interface {v0}, LuV;->c()V

    .line 745
    return-void

    .line 743
    :catchall_15
    move-exception v0

    iget-object v1, p0, Lzj;->a:LuV;

    invoke-interface {v1}, LuV;->c()V

    throw v0
.end method

.method public l()V
    .registers 3

    .prologue
    .line 836
    iget-object v0, p0, Lzj;->a:Lcom/google/android/apps/docs/editors/kix/KixEditText;

    iget-boolean v1, p0, Lzj;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/docs/editors/kix/KixEditText;->setEditable(Z)V

    .line 837
    return-void
.end method

.method public m()V
    .registers 2

    .prologue
    .line 863
    iget-object v0, p0, Lzj;->a:LDp;

    invoke-virtual {v0}, LDp;->c()V

    .line 864
    return-void
.end method
