.class public Lzq;
.super LMD;
.source "ReflowZoomManager.java"

# interfaces
.implements LzR;


# instance fields
.field private a:F

.field private final a:LMB;

.field private a:Landroid/widget/ImageView;

.field private final a:Lxu;

.field private a:LzS;

.field private a:Lzr;

.field private a:Z

.field private b:F

.field private c:F


# direct methods
.method public constructor <init>(Lxu;)V
    .registers 4
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, LMD;-><init>()V

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lzq;->a:Z

    .line 48
    new-instance v0, LMB;

    invoke-interface {p1}, Lxu;->a()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LMB;-><init>(Landroid/content/Context;LMC;)V

    iput-object v0, p0, Lzq;->a:LMB;

    .line 49
    iput-object p1, p0, Lzq;->a:Lxu;

    .line 50
    return-void
.end method


# virtual methods
.method public a(LMB;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 133
    iget-object v0, p0, Lzq;->a:LzS;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lzq;->a:Lzr;

    if-nez v0, :cond_a

    .line 153
    :cond_9
    :goto_9
    return-void

    .line 136
    :cond_a
    iget-object v0, p0, Lzq;->a:LzS;

    invoke-interface {v0}, LzS;->a()Landroid/view/View;

    move-result-object v1

    .line 139
    iget-object v0, p0, Lzq;->a:LzS;

    iget v2, p0, Lzq;->b:F

    iget v3, p0, Lzq;->c:F

    invoke-interface {v0, v2, v3}, LzS;->b(FF)V

    .line 142
    iget-object v0, p0, Lzq;->a:Lzr;

    invoke-interface {v0}, Lzr;->m()V

    .line 145
    iget-object v0, p0, Lzq;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 146
    iget-object v0, p0, Lzq;->a:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 148
    iget-object v2, p0, Lzq;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 151
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 152
    iput-boolean v4, p0, Lzq;->a:Z

    goto :goto_9
.end method

.method public a(Landroid/view/MotionEvent;)V
    .registers 3
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lzq;->a:LMB;

    invoke-virtual {v0, p1}, LMB;->a(Landroid/view/MotionEvent;)Z

    .line 55
    return-void
.end method

.method public a(LzS;)V
    .registers 2
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lzq;->a:LzS;

    .line 70
    return-void
.end method

.method public a(Lzr;)V
    .registers 2
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lzq;->a:Lzr;

    .line 65
    return-void
.end method

.method public a()Z
    .registers 2

    .prologue
    .line 157
    iget-boolean v0, p0, Lzq;->a:Z

    return v0
.end method

.method public a(LMB;)Z
    .registers 9
    .parameter

    .prologue
    const/4 v6, -0x2

    const/4 v1, 0x1

    .line 74
    iget-object v0, p0, Lzq;->a:LzS;

    if-nez v0, :cond_8

    .line 75
    const/4 v0, 0x0

    .line 104
    :goto_7
    return v0

    .line 78
    :cond_8
    iput-boolean v1, p0, Lzq;->a:Z

    .line 81
    iget-object v0, p0, Lzq;->a:LzS;

    invoke-virtual {p1}, LMB;->a()F

    move-result v2

    invoke-virtual {p1}, LMB;->b()F

    move-result v3

    invoke-interface {v0, v2, v3}, LzS;->a(FF)V

    .line 82
    iget-object v0, p0, Lzq;->a:LzS;

    invoke-interface {v0}, LzS;->a()Landroid/view/View;

    move-result-object v2

    .line 85
    invoke-virtual {v2}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v0

    .line 86
    invoke-virtual {v2, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 87
    invoke-virtual {v2}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 88
    invoke-virtual {v2, v0}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 91
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 92
    new-instance v4, Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lzq;->a:Landroid/widget/ImageView;

    .line 93
    iget-object v4, p0, Lzq;->a:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 94
    iget-object v3, p0, Lzq;->a:Landroid/widget/ImageView;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 95
    iget-object v3, p0, Lzq;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3, v6, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 98
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lzq;->a:Lxu;

    invoke-interface {v0}, Lxu;->b()F

    move-result v0

    iput v0, p0, Lzq;->a:F

    .line 101
    invoke-virtual {p1}, LMB;->a()F

    move-result v0

    iput v0, p0, Lzq;->b:F

    .line 102
    invoke-virtual {p1}, LMB;->b()F

    move-result v0

    iput v0, p0, Lzq;->c:F

    move v0, v1

    .line 104
    goto :goto_7
.end method

.method public b(LMB;)Z
    .registers 12
    .parameter

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 109
    iget-object v0, p0, Lzq;->a:Lzr;

    if-nez v0, :cond_7

    .line 128
    :goto_6
    return v5

    .line 112
    :cond_7
    iget-object v0, p0, Lzq;->a:Lxu;

    invoke-interface {v0}, Lxu;->b()F

    move-result v0

    .line 113
    iget v1, p0, Lzq;->a:F

    div-float v1, v0, v1

    .line 114
    iget-object v2, p0, Lzq;->a:Lxu;

    invoke-virtual {p1}, LMB;->c()F

    move-result v3

    mul-float/2addr v0, v3

    invoke-interface {v2, v0}, Lxu;->a(F)V

    .line 115
    iget-object v0, p0, Lzq;->a:Lxu;

    invoke-interface {v0}, Lxu;->b()F

    move-result v0

    iget v2, p0, Lzq;->a:F

    div-float v2, v0, v2

    .line 119
    cmpl-float v0, v2, v1

    if-eqz v0, :cond_42

    .line 121
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget v6, p0, Lzq;->b:F

    iget v8, p0, Lzq;->c:F

    move v3, v1

    move v4, v2

    move v7, v5

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 124
    invoke-virtual {v0, v9}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 125
    const-wide/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 126
    iget-object v1, p0, Lzq;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_42
    move v5, v9

    .line 128
    goto :goto_6
.end method
