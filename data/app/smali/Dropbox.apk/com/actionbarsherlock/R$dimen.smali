.class public final Lcom/actionbarsherlock/R$dimen;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final abs__action_bar_default_height:I = 0x7f090001

.field public static final abs__action_bar_icon_vertical_padding:I = 0x7f090002

.field public static final abs__action_bar_subtitle_bottom_margin:I = 0x7f090006

.field public static final abs__action_bar_subtitle_text_size:I = 0x7f090004

.field public static final abs__action_bar_subtitle_top_margin:I = 0x7f090005

.field public static final abs__action_bar_title_text_size:I = 0x7f090003

.field public static final abs__action_button_min_width:I = 0x7f090007

.field public static final abs__alert_dialog_title_height:I = 0x7f090008

.field public static final abs__config_prefDialogWidth:I = 0x7f090000

.field public static final abs__dialog_min_width_major:I = 0x7f090009

.field public static final abs__dialog_min_width_minor:I = 0x7f09000a

.field public static final actionBarTabPadding:I = 0x7f09004b

.field public static final action_button_min_width:I = 0x7f09000b

.field public static final authOuterMargin:I = 0x7f090048

.field public static final buttonTextSize:I = 0x7f090053

.field public static final cameraUploadBottomEdgeMargin:I = 0x7f090050

.field public static final cameraUploadPaddingSides:I = 0x7f09004c

.field public static final cameraUploadPaddingVertical:I = 0x7f09004d

.field public static final cameraUploadProgressSpacing:I = 0x7f090051

.field public static final cameraUploadSettingsWarningTextSize:I = 0x7f090056

.field public static final cameraUploadTextSpacing:I = 0x7f09004e

.field public static final cameraUploadThumbBottomMargin:I = 0x7f090052

.field public static final cameraUploadTopEdgeMargin:I = 0x7f09004f

.field public static final emptyListLayoutPadding:I = 0x7f09002f

.field public static final emptyPageOuterPadding:I = 0x7f090047

.field public static final fileChooserButtonLeft:I = 0x7f09001c

.field public static final fileChooserButtonRight:I = 0x7f09001d

.field public static final fileChooserButtonsPadding:I = 0x7f09001b

.field public static final fileChooserPaddingBottom:I = 0x7f090021

.field public static final fileChooserPaddingLeft:I = 0x7f09001f

.field public static final fileChooserPaddingRight:I = 0x7f09001e

.field public static final fileChooserPaddingTop:I = 0x7f090020

.field public static final fileChooserTitleText:I = 0x7f090022

.field public static final filelistEmptyPadding:I = 0x7f09002e

.field public static final filelistIconHalfMargin:I = 0x7f090027

.field public static final filelistIconOverlayBottom:I = 0x7f09002a

.field public static final filelistIconOverlayRight:I = 0x7f090029

.field public static final filelistIconRight:I = 0x7f090028

.field public static final filelistIconSize:I = 0x7f090024

.field public static final filelistModifiedLeft:I = 0x7f090060

.field public static final filelistPadding:I = 0x7f090023

.field public static final filelistTextMedium:I = 0x7f09002c

.field public static final filelistTextSmall:I = 0x7f09002d

.field public static final filelistThumbIconOverlayBottom:I = 0x7f09002b

.field public static final filelistThumbailFrameSize:I = 0x7f090026

.field public static final filelistThumbnailSize:I = 0x7f090025

.field public static final firstPicBottom:I = 0x7f090032

.field public static final firstPicLeft:I = 0x7f090034

.field public static final firstPicRight:I = 0x7f090035

.field public static final firstPicTop:I = 0x7f090033

.field public static final folderPickerButtonLowerMargin:I = 0x7f09004a

.field public static final folderPickerIconToTextMargin:I = 0x7f090049

.field public static final galleryPickerItemSize:I = 0x7f090030

.field public static final galleryViewText:I = 0x7f090031

.field public static final gallery_video_icon_padding:I = 0x7f090055

.field public static final itemCameraUploadInfoLeftMargin:I = 0x7f09005a

.field public static final itemCameraUploadInfoTopMargin:I = 0x7f09005b

.field public static final itemCameraUploadThumbLeftPadding:I = 0x7f090059

.field public static final linkButtonButtomMarginTop:I = 0x7f090046

.field public static final linkButtonTextSize:I = 0x7f090045

.field public static final loginButtonPadding:I = 0x7f090018

.field public static final loginChoiceButtonPadding:I = 0x7f09000c

.field public static final loginChoiceTextBottom:I = 0x7f09000d

.field public static final loginChoiceTextLeft:I = 0x7f09000f

.field public static final loginChoiceTextRight:I = 0x7f090010

.field public static final loginChoiceTextTop:I = 0x7f09000e

.field public static final loginLogoBottom:I = 0x7f09005d

.field public static final loginLogoHeight:I = 0x7f09005c

.field public static final loginLogoTop:I = 0x7f09005e

.field public static final loginTextBottom:I = 0x7f090011

.field public static final loginTextBottomClose:I = 0x7f090012

.field public static final loginTextEntryLeft:I = 0x7f090016

.field public static final loginTextLeft:I = 0x7f090015

.field public static final loginTextLeftClose:I = 0x7f09005f

.field public static final loginTextRight:I = 0x7f090017

.field public static final loginTextTop:I = 0x7f090013

.field public static final loginTextVeryTop:I = 0x7f090014

.field public static final password_keyboard_key_height:I = 0x7f090019

.field public static final password_keyboard_key_spacing:I = 0x7f09001a

.field public static final separatorPadding:I = 0x7f090057

.field public static final separatorPaddingLeft:I = 0x7f090058

.field public static final status_video_icon_padding:I = 0x7f090054

.field public static final titleBarSidePadding:I = 0x7f090043

.field public static final titleBarTextVerticalPadding:I = 0x7f090044

.field public static final tourBoxPadding:I = 0x7f090040

.field public static final tourBoxText:I = 0x7f090041

.field public static final tourCameraSettingsImageBottomMargin:I = 0x7f09003c

.field public static final tourCameraSettingsMargin:I = 0x7f09003b

.field public static final tourMargin:I = 0x7f09003a

.field public static final tourMaxPicHeight:I = 0x7f090042

.field public static final tourPageButtonPadding:I = 0x7f090039

.field public static final tourSectionMargin:I = 0x7f09003f

.field public static final tourSubsectionMargin:I = 0x7f09003d

.field public static final tourTextLineSpacingExtra:I = 0x7f090038

.field public static final tourTextSize:I = 0x7f090036

.field public static final tourTextSizeSmaller:I = 0x7f090037

.field public static final tourTitleTextSpacing:I = 0x7f09003e


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
