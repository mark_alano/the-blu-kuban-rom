.class public final Lcom/actionbarsherlock/R$id;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final abs__action_bar:I = 0x7f060022

.field public static final abs__action_bar_container:I = 0x7f060021

.field public static final abs__action_bar_subtitle:I = 0x7f060011

.field public static final abs__action_bar_title:I = 0x7f060010

.field public static final abs__action_context_bar:I = 0x7f060023

.field public static final abs__action_menu_divider:I = 0x7f06000c

.field public static final abs__action_menu_presenter:I = 0x7f06000d

.field public static final abs__action_mode_bar:I = 0x7f060026

.field public static final abs__action_mode_bar_stub:I = 0x7f060025

.field public static final abs__action_mode_close_button:I = 0x7f060014

.field public static final abs__activity_chooser_view_content:I = 0x7f060015

.field public static final abs__checkbox:I = 0x7f06001e

.field public static final abs__content:I = 0x7f06001d

.field public static final abs__default_activity_button:I = 0x7f060018

.field public static final abs__expand_activities_button:I = 0x7f060016

.field public static final abs__home:I = 0x7f06000a

.field public static final abs__icon:I = 0x7f06001a

.field public static final abs__image:I = 0x7f060017

.field public static final abs__imageButton:I = 0x7f060012

.field public static final abs__list_item:I = 0x7f060019

.field public static final abs__progress_circular:I = 0x7f06000e

.field public static final abs__progress_horizontal:I = 0x7f06000f

.field public static final abs__radio:I = 0x7f060020

.field public static final abs__shortcut:I = 0x7f06001f

.field public static final abs__split_action_bar:I = 0x7f060024

.field public static final abs__textButton:I = 0x7f060013

.field public static final abs__title:I = 0x7f06001b

.field public static final abs__titleDivider:I = 0x7f06001c

.field public static final abs__up:I = 0x7f06000b

.field public static final accept_button:I = 0x7f0600ff

.field public static final app_info:I = 0x7f060029

.field public static final arrow_bar:I = 0x7f06008f

.field public static final auth_layout:I = 0x7f060028

.field public static final auth_progress_layout:I = 0x7f060027

.field public static final back_button:I = 0x7f0600fe

.field public static final bottom_bar:I = 0x7f06002f

.field public static final bottom_bar_cancel_button:I = 0x7f060030

.field public static final bottom_bar_ok_button:I = 0x7f060031

.field public static final bottom_chrome:I = 0x7f0600c3

.field public static final button_allow:I = 0x7f06002b

.field public static final button_bar:I = 0x7f06004a

.field public static final button_cancel:I = 0x7f06002d

.field public static final button_container:I = 0x7f060049

.field public static final button_deny:I = 0x7f06002a

.field public static final button_retry:I = 0x7f06002e

.field public static final camera_toggle_elts:I = 0x7f060058

.field public static final camera_up_image:I = 0x7f06004d

.field public static final camera_upload_details_thumb_container:I = 0x7f06003d

.field public static final camera_upload_details_thumb_image:I = 0x7f06003f

.field public static final camera_upload_settings_explanation:I = 0x7f06004c

.field public static final camera_uploads_turn_on_button:I = 0x7f060048

.field public static final chooser_title:I = 0x7f060075

.field public static final connection_options:I = 0x7f06004f

.field public static final connection_wifi_3g:I = 0x7f060053

.field public static final connection_wifi_3g_layout:I = 0x7f060052

.field public static final connection_wifi_only:I = 0x7f060051

.field public static final connection_wifi_only_layout:I = 0x7f060050

.field public static final custom_page_stub:I = 0x7f060091

.field public static final data_warning:I = 0x7f060054

.field public static final dev_settings:I = 0x7f0600ba

.field public static final dev_settings_done:I = 0x7f06005f

.field public static final dev_settings_user:I = 0x7f06005d

.field public static final dev_settings_user_radio_button:I = 0x7f06005e

.field public static final dev_settings_vm_radio_button:I = 0x7f06005c

.field public static final dev_settings_vm_server:I = 0x7f06005b

.field public static final disableHome:I = 0x7f060009

.field public static final download_cancel_button:I = 0x7f060067

.field public static final download_icon:I = 0x7f060064

.field public static final download_message:I = 0x7f060065

.field public static final download_progress:I = 0x7f060066

.field public static final dropbox_list:I = 0x7f06006f

.field public static final empty_folder_text:I = 0x7f0600a8

.field public static final empty_image:I = 0x7f060047

.field public static final enter_twofactor_code_additional_help:I = 0x7f06006c

.field public static final enter_twofactor_code_didnt_receive_desc:I = 0x7f060061

.field public static final enter_twofactor_code_didnt_receive_need_help_button:I = 0x7f060063

.field public static final enter_twofactor_code_didnt_receive_send_new_button:I = 0x7f060062

.field public static final enter_twofactor_code_didnt_receive_title:I = 0x7f060060

.field public static final enter_twofactor_code_input:I = 0x7f06006a

.field public static final enter_twofactor_code_leadin:I = 0x7f060069

.field public static final enter_twofactor_code_submit:I = 0x7f06006b

.field public static final enter_twofactor_code_title:I = 0x7f060068

.field public static final error:I = 0x7f0600f7

.field public static final favorites_info:I = 0x7f06006e

.field public static final favorites_info_button:I = 0x7f060073

.field public static final favorites_refresh:I = 0x7f060071

.field public static final favorites_status:I = 0x7f060070

.field public static final ffwd:I = 0x7f0600c6

.field public static final filelist_empty_container:I = 0x7f060072

.field public static final filelist_empty_layout:I = 0x7f060079

.field public static final filelist_empty_progress:I = 0x7f06007a

.field public static final filelist_empty_text:I = 0x7f06007b

.field public static final filelist_group_header:I = 0x7f0600a5

.field public static final filelist_icon:I = 0x7f0600a1

.field public static final filelist_include:I = 0x7f060076

.field public static final filelist_name:I = 0x7f06009a

.field public static final filelist_name_subtext:I = 0x7f0600a3

.field public static final filelist_quickaction_button:I = 0x7f06009c

.field public static final filelist_row:I = 0x7f0600a2

.field public static final filelist_status:I = 0x7f06009d

.field public static final filelist_status_progressbar:I = 0x7f06009b

.field public static final filelist_sync:I = 0x7f0600a4

.field public static final filelist_thumb_frame:I = 0x7f06009e

.field public static final filelist_thumbnail:I = 0x7f0600a0

.field public static final filelist_thumbnail_img_container:I = 0x7f06009f

.field public static final first_logo_bottom_pic:I = 0x7f0600b7

.field public static final first_logo_pic:I = 0x7f0600b6

.field public static final forgot_password_email:I = 0x7f06007d

.field public static final forgot_password_leadin:I = 0x7f06007c

.field public static final forgot_password_submit:I = 0x7f06007e

.field public static final form_elts_container:I = 0x7f06004b

.field public static final frag_container:I = 0x7f06007f

.field public static final frag_container_A:I = 0x7f0600b5

.field public static final frag_container_B:I = 0x7f0600f9

.field public static final from:I = 0x7f060100

.field public static final gallery_button_container:I = 0x7f060089

.field public static final gallery_delete_button:I = 0x7f06008d

.field public static final gallery_export_button:I = 0x7f06008c

.field public static final gallery_favorite_button:I = 0x7f06008a

.field public static final gallery_item_checkbox:I = 0x7f060083

.field public static final gallery_item_icon:I = 0x7f060081

.field public static final gallery_item_video_info:I = 0x7f060041

.field public static final gallery_progress_a:I = 0x7f060087

.field public static final gallery_progress_b:I = 0x7f060088

.field public static final gallery_share_button:I = 0x7f06008b

.field public static final gallery_title_container:I = 0x7f060085

.field public static final gallery_titler:I = 0x7f060086

.field public static final gallery_view:I = 0x7f060084

.field public static final gallery_zoom_container:I = 0x7f0600f8

.field public static final generic_page:I = 0x7f060092

.field public static final get_more_space:I = 0x7f06003c

.field public static final glow_overlay:I = 0x7f060082

.field public static final grid_empty_progress:I = 0x7f060045

.field public static final homeAsUp:I = 0x7f060006

.field public static final icon:I = 0x7f0600af

.field public static final initial_spinner:I = 0x7f060033

.field public static final intent_picker_icon:I = 0x7f060097

.field public static final intent_picker_text1:I = 0x7f060098

.field public static final intent_picker_text2:I = 0x7f060099

.field public static final layout:I = 0x7f06008e

.field public static final layout_root:I = 0x7f0600d6

.field public static final lfb_list:I = 0x7f0600a7

.field public static final listMode:I = 0x7f060002

.field public static final loaded_view:I = 0x7f060046

.field public static final loading:I = 0x7f0600fd

.field public static final loading_view:I = 0x7f060044

.field public static final localitem_checkbox:I = 0x7f0600ae

.field public static final localitem_icon:I = 0x7f0600ab

.field public static final localitem_info:I = 0x7f0600ad

.field public static final localitem_list_row:I = 0x7f060080

.field public static final localitem_name:I = 0x7f0600ac

.field public static final localitem_thumb_frame:I = 0x7f0600a9

.field public static final localitem_thumbnail:I = 0x7f0600aa

.field public static final lock_code_prompt:I = 0x7f0600b0

.field public static final lock_digit_1:I = 0x7f0600b1

.field public static final lock_digit_2:I = 0x7f0600b2

.field public static final lock_digit_3:I = 0x7f0600b3

.field public static final lock_digit_4:I = 0x7f0600b4

.field public static final login:I = 0x7f0600b8

.field public static final login_email:I = 0x7f0600bc

.field public static final login_email_prompt:I = 0x7f0600bb

.field public static final login_forgot_password:I = 0x7f0600c1

.field public static final login_new_account:I = 0x7f0600b9

.field public static final login_password:I = 0x7f0600be

.field public static final login_password_prompt:I = 0x7f0600bd

.field public static final login_screen_switch_to_create:I = 0x7f0600c0

.field public static final login_screen_switch_to_login:I = 0x7f0600d5

.field public static final login_submit:I = 0x7f0600bf

.field public static final main_container:I = 0x7f060057

.field public static final mediacontroller_progress:I = 0x7f0600c9

.field public static final new_account_email:I = 0x7f0600d0

.field public static final new_account_email_prompt:I = 0x7f0600cf

.field public static final new_account_first_name:I = 0x7f0600cc

.field public static final new_account_first_name_prompt:I = 0x7f0600cb

.field public static final new_account_last_name:I = 0x7f0600ce

.field public static final new_account_last_name_prompt:I = 0x7f0600cd

.field public static final new_account_password:I = 0x7f0600d2

.field public static final new_account_password_prompt:I = 0x7f0600d1

.field public static final new_account_submit:I = 0x7f0600d3

.field public static final new_account_video:I = 0x7f0600d4

.field public static final new_folder_button:I = 0x7f060078

.field public static final new_folder_name:I = 0x7f0600d7

.field public static final new_folder_progress:I = 0x7f0600d8

.field public static final new_folder_progress_text:I = 0x7f0600d9

.field public static final next:I = 0x7f0600c7

.field public static final normal:I = 0x7f060001

.field public static final overlay_image:I = 0x7f060038

.field public static final page_container:I = 0x7f060035

.field public static final pause:I = 0x7f0600c2

.field public static final pin0:I = 0x7f0600e6

.field public static final pin1:I = 0x7f0600df

.field public static final pin2:I = 0x7f0600e3

.field public static final pin3:I = 0x7f0600e7

.field public static final pin4:I = 0x7f0600e0

.field public static final pin5:I = 0x7f0600e4

.field public static final pin6:I = 0x7f0600e8

.field public static final pin7:I = 0x7f0600e1

.field public static final pin8:I = 0x7f0600e5

.field public static final pin9:I = 0x7f0600e9

.field public static final pin_blank:I = 0x7f0600e2

.field public static final pin_del:I = 0x7f0600ea

.field public static final prev:I = 0x7f0600c4

.field public static final progress:I = 0x7f0600f5

.field public static final progress_bar:I = 0x7f060039

.field public static final progress_text:I = 0x7f0600dd

.field public static final quickaction_cancel_button:I = 0x7f0600ec

.field public static final quickaction_clear_button:I = 0x7f0600ed

.field public static final quickaction_container:I = 0x7f0600eb

.field public static final quickaction_delete_button:I = 0x7f0600ee

.field public static final quickaction_export_button:I = 0x7f0600ef

.field public static final quickaction_favorite_button:I = 0x7f0600f0

.field public static final quickaction_more_button:I = 0x7f0600f1

.field public static final quickaction_rename_button:I = 0x7f0600f2

.field public static final quickaction_share_button:I = 0x7f0600f3

.field public static final quickaction_view_in_folder:I = 0x7f0600f4

.field public static final retry_layout:I = 0x7f06002c

.field public static final rew:I = 0x7f0600c5

.field public static final selection_status_text:I = 0x7f0600de

.field public static final showCustom:I = 0x7f060008

.field public static final showHome:I = 0x7f060005

.field public static final showTitle:I = 0x7f060007

.field public static final simple_status_text:I = 0x7f060034

.field public static final status_text:I = 0x7f06003b

.field public static final status_title:I = 0x7f06003a

.field public static final tabMode:I = 0x7f060003

.field public static final temp:I = 0x7f0600f6

.field public static final text:I = 0x7f0600dc

.field public static final text_edit_text:I = 0x7f0600fa

.field public static final thumb_container:I = 0x7f060037

.field public static final thumb_container_empty_view:I = 0x7f060036

.field public static final thumbnail_w_frame:I = 0x7f06003e

.field public static final time:I = 0x7f0600ca

.field public static final time_current:I = 0x7f0600c8

.field public static final title:I = 0x7f0600db

.field public static final title_bar:I = 0x7f06006d

.field public static final title_bar_text:I = 0x7f060077

.field public static final to:I = 0x7f060101

.field public static final toggleable_form_elts:I = 0x7f06004e

.field public static final togglebutton:I = 0x7f060032

.field public static final top_bar:I = 0x7f060074

.field public static final top_row:I = 0x7f0600da

.field public static final tour_image:I = 0x7f060093

.field public static final tour_next:I = 0x7f060090

.field public static final tour_text:I = 0x7f060095

.field public static final tour_title:I = 0x7f060094

.field public static final turn_on_camera_upload_button:I = 0x7f060059

.field public static final turn_on_camera_upload_text:I = 0x7f06005a

.field public static final up_folder_text:I = 0x7f0600a6

.field public static final upload_existing:I = 0x7f060056

.field public static final uploads_info_button:I = 0x7f0600fb

.field public static final useLogo:I = 0x7f060004

.field public static final video_icon:I = 0x7f060042

.field public static final video_info_container_parent:I = 0x7f060040

.field public static final video_length:I = 0x7f060043

.field public static final video_view:I = 0x7f0600fc

.field public static final webview:I = 0x7f060096

.field public static final wifi_only_extras:I = 0x7f060055

.field public static final wrap_content:I = 0x7f060000


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 1279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
