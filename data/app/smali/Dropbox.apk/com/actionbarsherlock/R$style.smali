.class public final Lcom/actionbarsherlock/R$style;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final Animation_QuickActions:I = 0x7f0c0059

.field public static final BottomBar:I = 0x7f0c006f

.field public static final BottomBarContainer:I = 0x7f0c0072

.field public static final Checkbox:I = 0x7f0c006c

.field public static final CheckboxLight:I = 0x7f0c006d

.field public static final DialogWindowTitle_Sherlock:I = 0x7f0c0033

.field public static final DialogWindowTitle_Sherlock_Light:I = 0x7f0c0034

.field public static final GalleryButton:I = 0x7f0c005d

.field public static final GalleryChromeText:I = 0x7f0c005c

.field public static final LoginInput:I = 0x7f0c0070

.field public static final NotificationText:I = 0x7f0c0073

.field public static final NotificationTitle:I = 0x7f0c0074

.field public static final QuickactionButton:I = 0x7f0c005b

.field public static final RadioButton:I = 0x7f0c006e

.field public static final Sherlock___TextAppearance_Small:I = 0x7f0c0047

.field public static final Sherlock___Theme:I = 0x7f0c004a

.field public static final Sherlock___Theme_DarkActionBar:I = 0x7f0c004c

.field public static final Sherlock___Theme_Dialog:I = 0x7f0c004d

.field public static final Sherlock___Theme_Light:I = 0x7f0c004b

.field public static final Sherlock___Widget_ActionBar:I = 0x7f0c0001

.field public static final Sherlock___Widget_ActionMode:I = 0x7f0c0016

.field public static final Sherlock___Widget_ActivityChooserView:I = 0x7f0c001e

.field public static final Sherlock___Widget_Holo_DropDownItem:I = 0x7f0c0029

.field public static final Sherlock___Widget_Holo_ListView:I = 0x7f0c0026

.field public static final Sherlock___Widget_Holo_Spinner:I = 0x7f0c0023

.field public static final SingleCharacterPasscodeBox:I = 0x7f0c0058

.field public static final StatusProgressBar:I = 0x7f0c005a

.field public static final TabletLoginOrientation:I = 0x7f0c0075

.field public static final TextAppearance_Sherlock_DialogWindowTitle:I = 0x7f0c0045

.field public static final TextAppearance_Sherlock_Light_DialogWindowTitle:I = 0x7f0c0046

.field public static final TextAppearance_Sherlock_Light_Small:I = 0x7f0c0049

.field public static final TextAppearance_Sherlock_Light_Widget_PopupMenu_Large:I = 0x7f0c0040

.field public static final TextAppearance_Sherlock_Light_Widget_PopupMenu_Small:I = 0x7f0c0042

.field public static final TextAppearance_Sherlock_Small:I = 0x7f0c0048

.field public static final TextAppearance_Sherlock_Widget_ActionBar_Menu:I = 0x7f0c0035

.field public static final TextAppearance_Sherlock_Widget_ActionBar_Subtitle:I = 0x7f0c0038

.field public static final TextAppearance_Sherlock_Widget_ActionBar_Subtitle_Inverse:I = 0x7f0c0039

.field public static final TextAppearance_Sherlock_Widget_ActionBar_Title:I = 0x7f0c0036

.field public static final TextAppearance_Sherlock_Widget_ActionBar_Title_Inverse:I = 0x7f0c0037

.field public static final TextAppearance_Sherlock_Widget_ActionMode_Subtitle:I = 0x7f0c003c

.field public static final TextAppearance_Sherlock_Widget_ActionMode_Subtitle_Inverse:I = 0x7f0c003d

.field public static final TextAppearance_Sherlock_Widget_ActionMode_Title:I = 0x7f0c003a

.field public static final TextAppearance_Sherlock_Widget_ActionMode_Title_Inverse:I = 0x7f0c003b

.field public static final TextAppearance_Sherlock_Widget_DropDownItem:I = 0x7f0c0044

.field public static final TextAppearance_Sherlock_Widget_PopupMenu:I = 0x7f0c003e

.field public static final TextAppearance_Sherlock_Widget_PopupMenu_Large:I = 0x7f0c003f

.field public static final TextAppearance_Sherlock_Widget_PopupMenu_Small:I = 0x7f0c0041

.field public static final TextAppearance_Sherlock_Widget_TextView_SpinnerItem:I = 0x7f0c0043

.field public static final Theme:I = 0x7f0c0076

.field public static final Theme_Dialog:I = 0x7f0c0080

.field public static final Theme_Light:I = 0x7f0c0077

.field public static final Theme_Light_NoActionBar:I = 0x7f0c007f

.field public static final Theme_NoActionBar:I = 0x7f0c007e

.field public static final Theme_NoActionBar_Fullscreen:I = 0x7f0c0083

.field public static final Theme_NoBackground:I = 0x7f0c0082

.field public static final Theme_Sherlock:I = 0x7f0c004e

.field public static final Theme_Sherlock_Dialog:I = 0x7f0c0056

.field public static final Theme_Sherlock_ForceOverflow:I = 0x7f0c0053

.field public static final Theme_Sherlock_Light:I = 0x7f0c004f

.field public static final Theme_Sherlock_Light_DarkActionBar:I = 0x7f0c0050

.field public static final Theme_Sherlock_Light_DarkActionBar_ForceOverflow:I = 0x7f0c0055

.field public static final Theme_Sherlock_Light_Dialog:I = 0x7f0c0057

.field public static final Theme_Sherlock_Light_ForceOverflow:I = 0x7f0c0054

.field public static final Theme_Sherlock_Light_NoActionBar:I = 0x7f0c0052

.field public static final Theme_Sherlock_NoActionBar:I = 0x7f0c0051

.field public static final Theme_Translucent_NoActionBar:I = 0x7f0c0081

.field public static final TourButton:I = 0x7f0c005e

.field public static final TourButtonLeft:I = 0x7f0c005f

.field public static final TourButtonRight:I = 0x7f0c0060

.field public static final UploadButton:I = 0x7f0c0071

.field public static final Widget:I = 0x7f0c0000

.field public static final Widget_Holo_Light_ActionButton_Overflow:I = 0x7f0c007d

.field public static final Widget_Sherlock_ActionBar:I = 0x7f0c0002

.field public static final Widget_Sherlock_ActionBar_Solid:I = 0x7f0c0003

.field public static final Widget_Sherlock_ActionBar_TabBar:I = 0x7f0c000a

.field public static final Widget_Sherlock_ActionBar_TabText:I = 0x7f0c000d

.field public static final Widget_Sherlock_ActionBar_TabView:I = 0x7f0c0007

.field public static final Widget_Sherlock_ActionButton:I = 0x7f0c0010

.field public static final Widget_Sherlock_ActionButton_CloseMode:I = 0x7f0c0012

.field public static final Widget_Sherlock_ActionButton_Overflow:I = 0x7f0c0014

.field public static final Widget_Sherlock_ActionMode:I = 0x7f0c0017

.field public static final Widget_Sherlock_ActivityChooserView:I = 0x7f0c001f

.field public static final Widget_Sherlock_Button_Small:I = 0x7f0c0021

.field public static final Widget_Sherlock_DropDownItem_Spinner:I = 0x7f0c002a

.field public static final Widget_Sherlock_Light_ActionBar:I = 0x7f0c0004

.field public static final Widget_Sherlock_Light_ActionBar_Solid:I = 0x7f0c0005

.field public static final Widget_Sherlock_Light_ActionBar_Solid_Inverse:I = 0x7f0c0006

.field public static final Widget_Sherlock_Light_ActionBar_TabBar:I = 0x7f0c000b

.field public static final Widget_Sherlock_Light_ActionBar_TabBar_Inverse:I = 0x7f0c000c

.field public static final Widget_Sherlock_Light_ActionBar_TabText:I = 0x7f0c000e

.field public static final Widget_Sherlock_Light_ActionBar_TabText_Inverse:I = 0x7f0c000f

.field public static final Widget_Sherlock_Light_ActionBar_TabView:I = 0x7f0c0008

.field public static final Widget_Sherlock_Light_ActionBar_TabView_Inverse:I = 0x7f0c0009

.field public static final Widget_Sherlock_Light_ActionButton:I = 0x7f0c0011

.field public static final Widget_Sherlock_Light_ActionButton_CloseMode:I = 0x7f0c0013

.field public static final Widget_Sherlock_Light_ActionButton_Overflow:I = 0x7f0c0015

.field public static final Widget_Sherlock_Light_ActionMode:I = 0x7f0c0018

.field public static final Widget_Sherlock_Light_ActionMode_Inverse:I = 0x7f0c0019

.field public static final Widget_Sherlock_Light_ActivityChooserView:I = 0x7f0c0020

.field public static final Widget_Sherlock_Light_Button_Small:I = 0x7f0c0022

.field public static final Widget_Sherlock_Light_DropDownItem_Spinner:I = 0x7f0c002b

.field public static final Widget_Sherlock_Light_ListPopupWindow:I = 0x7f0c001b

.field public static final Widget_Sherlock_Light_ListView_DropDown:I = 0x7f0c0028

.field public static final Widget_Sherlock_Light_PopupMenu:I = 0x7f0c001d

.field public static final Widget_Sherlock_Light_PopupWindow_ActionMode:I = 0x7f0c002d

.field public static final Widget_Sherlock_Light_ProgressBar:I = 0x7f0c002f

.field public static final Widget_Sherlock_Light_ProgressBar_Horizontal:I = 0x7f0c0031

.field public static final Widget_Sherlock_Light_Spinner_DropDown_ActionBar:I = 0x7f0c0025

.field public static final Widget_Sherlock_ListPopupWindow:I = 0x7f0c001a

.field public static final Widget_Sherlock_ListView_DropDown:I = 0x7f0c0027

.field public static final Widget_Sherlock_PopupMenu:I = 0x7f0c001c

.field public static final Widget_Sherlock_PopupWindow_ActionMode:I = 0x7f0c002c

.field public static final Widget_Sherlock_ProgressBar:I = 0x7f0c002e

.field public static final Widget_Sherlock_ProgressBar_Horizontal:I = 0x7f0c0030

.field public static final Widget_Sherlock_Spinner_DropDown_ActionBar:I = 0x7f0c0024

.field public static final Widget_Sherlock_TextView_SpinnerItem:I = 0x7f0c0032

.field public static final bigBoldBlueText:I = 0x7f0c0061

.field public static final bigishBoldBlueText:I = 0x7f0c0064

.field public static final cameraUploadMainText:I = 0x7f0c0066

.field public static final cameraUploadStatusText:I = 0x7f0c0068

.field public static final cameraUploadSubText:I = 0x7f0c0067

.field public static final dropboxLightActionBarStyle:I = 0x7f0c0078

.field public static final dropboxLightActionBarTabBarStyle:I = 0x7f0c007b

.field public static final dropboxLightActionBarTabStyle:I = 0x7f0c007a

.field public static final dropboxLightActionBarTabTextStyle:I = 0x7f0c007c

.field public static final dropboxTitleTextStyle:I = 0x7f0c0079

.field public static final favoritesDisabled:I = 0x7f0c006b

.field public static final favoritesText:I = 0x7f0c006a

.field public static final mediumBoldBlueText:I = 0x7f0c0062

.field public static final smallGrayText:I = 0x7f0c0065

.field public static final smallishGrayText:I = 0x7f0c0069

.field public static final tourBoldBlueText:I = 0x7f0c0063


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 5494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
