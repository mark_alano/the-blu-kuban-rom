.class public Lcom/dropbox/android/activity/CameraUploadGridFragment;
.super Lcom/dropbox/android/activity/lock/SweetListFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/y;
.implements Ldbxyzptlk/g/D;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/Button;

.field private volatile l:Lcom/dropbox/android/activity/s;

.field private m:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 36
    const-class v0, Lcom/dropbox/android/activity/CameraUploadGridFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/SweetListFragment;-><init>()V

    .line 46
    sget-object v0, Lcom/dropbox/android/activity/s;->a:Lcom/dropbox/android/activity/s;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->l:Lcom/dropbox/android/activity/s;

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->m:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/CameraUploadGridFragment;Lcom/dropbox/android/activity/s;)Lcom/dropbox/android/activity/s;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->l:Lcom/dropbox/android/activity/s;

    return-object p1
.end method

.method private a()V
    .registers 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 142
    iget-object v3, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->l:Lcom/dropbox/android/activity/s;

    sget-object v4, Lcom/dropbox/android/activity/s;->a:Lcom/dropbox/android/activity/s;

    if-ne v0, v4, :cond_3b

    move v0, v1

    :goto_c
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 143
    iget-object v3, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->l:Lcom/dropbox/android/activity/s;

    sget-object v4, Lcom/dropbox/android/activity/s;->a:Lcom/dropbox/android/activity/s;

    if-ne v0, v4, :cond_3d

    move v0, v2

    :goto_18
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 145
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->g()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 147
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->b:Landroid/widget/TextView;

    const v1, 0x7f0b011a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 148
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->c:Landroid/widget/TextView;

    const v1, 0x7f0b011c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 149
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->k:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 155
    :goto_3a
    return-void

    :cond_3b
    move v0, v2

    .line 142
    goto :goto_c

    :cond_3d
    move v0, v1

    .line 143
    goto :goto_18

    .line 151
    :cond_3f
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->b:Landroid/widget/TextView;

    const v2, 0x7f0b00fa

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->c:Landroid/widget/TextView;

    const v2, 0x7f0b01b6

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 153
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->k:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3a
.end method

.method static synthetic a(Lcom/dropbox/android/activity/CameraUploadGridFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->a()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Ldbxyzptlk/a/d;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 193
    new-instance v0, Ldbxyzptlk/a/c;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/provider/CameraUploadsProvider;->c:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/a/c;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Ldbxyzptlk/a/d;)V
    .registers 4
    .parameter

    .prologue
    .line 224
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/aD;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(Landroid/database/Cursor;)V

    .line 225
    return-void
.end method

.method public final a(Ldbxyzptlk/a/d;Landroid/database/Cursor;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 206
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/aD;->a(Landroid/database/Cursor;)V

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->f:Lcom/dropbox/android/util/aM;

    if-eqz v0, :cond_1f

    .line 212
    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->f:Lcom/dropbox/android/util/aM;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aM;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/SweetListView;->setDelayedPositionFromTop(I)V

    .line 213
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->f:Lcom/dropbox/android/util/aM;

    .line 216
    :cond_1f
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->a()V

    .line 217
    return-void
.end method

.method public final bridge synthetic a(Ldbxyzptlk/a/d;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 33
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->a(Ldbxyzptlk/a/d;Landroid/database/Cursor;)V

    return-void
.end method

.method public final a(Ldbxyzptlk/g/F;)V
    .registers 4
    .parameter

    .prologue
    .line 229
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->m:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/activity/q;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/activity/q;-><init>(Lcom/dropbox/android/activity/CameraUploadGridFragment;Ldbxyzptlk/g/F;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 260
    return-void
.end method

.method protected final a(Lcom/dropbox/android/widget/SweetListView;Landroid/view/View;IJ)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 179
    instance-of v0, p2, Lcom/dropbox/android/widget/CameraUploadItemView;

    if-nez v0, :cond_d

    const v0, 0x7f0600a5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_f

    .line 181
    :cond_d
    const/4 v0, 0x1

    .line 183
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 88
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getLoaderManager()Landroid/support/v4/app/x;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/x;->a(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Ldbxyzptlk/a/d;

    .line 89
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setSweetAdapter(Lcom/dropbox/android/widget/az;)V

    .line 90
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->g:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    iget-object v1, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->h:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/SweetListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 92
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 93
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->a()V

    .line 94
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/g/B;->a(Landroid/content/Context;)Ldbxyzptlk/g/B;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/D;)V

    .line 95
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 2
    .parameter

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onAttach(Landroid/app/Activity;)V

    .line 52
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->a(Landroid/os/Bundle;)V

    .line 67
    new-instance v0, Lcom/dropbox/android/widget/aD;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/aD;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    .line 70
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 106
    const v0, 0x7f03001d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 107
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/SweetListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    .line 108
    const v0, 0x7f06003a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->b:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f06003b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->c:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f060046

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->i:Landroid/view/View;

    .line 111
    const v0, 0x7f060044

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->j:Landroid/view/View;

    .line 113
    const v0, 0x7f060048

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->k:Landroid/widget/Button;

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->k:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/p;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/p;-><init>(Lcom/dropbox/android/activity/CameraUploadGridFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    return-object v1
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 134
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onDestroy()V

    .line 135
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    if-eqz v0, :cond_f

    .line 136
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/aD;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(Landroid/database/Cursor;)V

    .line 138
    :cond_f
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/g/B;->a(Landroid/content/Context;)Ldbxyzptlk/g/B;

    move-result-object v0

    invoke-virtual {v0, p0}, Ldbxyzptlk/g/B;->b(Ldbxyzptlk/g/D;)V

    .line 139
    return-void
.end method

.method public onDestroyView()V
    .registers 2

    .prologue
    .line 99
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onDestroyView()V

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->d:Lcom/dropbox/android/widget/SweetListView;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->unregisterForContextMenu(Landroid/view/View;)V

    .line 101
    return-void
.end method

.method public onDetach()V
    .registers 3

    .prologue
    .line 56
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onDetach()V

    .line 57
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    if-eqz v0, :cond_19

    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 58
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/aD;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(Landroid/database/Cursor;)V

    .line 60
    :cond_19
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/aD;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aD;->c()Landroid/database/Cursor;

    move-result-object v0

    .line 160
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 161
    invoke-static {v0}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v1

    .line 162
    sget-object v2, Lcom/dropbox/android/activity/r;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/provider/P;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_3e

    .line 175
    :goto_1a
    return-void

    .line 164
    :pswitch_1b
    invoke-static {v0}, Lcom/dropbox/android/provider/K;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 165
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/provider/CameraUploadsProvider;->b:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-static {v1, v0, v2, v3}, Lcom/dropbox/android/util/bd;->a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/net/Uri;Z)V

    goto :goto_1a

    .line 168
    :pswitch_2a
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/dropbox/android/activity/CameraUploadDetailsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 169
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1a

    .line 162
    nop

    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_2a
    .end packed-switch
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->a()V

    .line 128
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onResume()V

    .line 129
    invoke-virtual {p0}, Lcom/dropbox/android/activity/CameraUploadGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/g/B;->a(Landroid/content/Context;)Ldbxyzptlk/g/B;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/g/B;->a()V

    .line 130
    return-void
.end method

.method public onStart()V
    .registers 3

    .prologue
    .line 74
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onStart()V

    .line 75
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/aD;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(Z)V

    .line 76
    return-void
.end method

.method public onStop()V
    .registers 3

    .prologue
    .line 80
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/SweetListFragment;->onStop()V

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/activity/CameraUploadGridFragment;->e:Lcom/dropbox/android/widget/az;

    check-cast v0, Lcom/dropbox/android/widget/aD;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/aD;->a(Z)V

    .line 82
    return-void
.end method
