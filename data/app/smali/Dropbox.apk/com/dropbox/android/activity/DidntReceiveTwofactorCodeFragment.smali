.class public Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;
.super Lcom/dropbox/android/activity/base/BaseFragment;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/activity/C;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragment;-><init>()V

    .line 22
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;)Lcom/dropbox/android/activity/C;
    .registers 2
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->b:Lcom/dropbox/android/activity/C;

    return-object v0
.end method

.method public static a()Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;
    .registers 1

    .prologue
    .line 29
    new-instance v0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .registers 6
    .parameter

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 36
    :try_start_3
    move-object v0, p1

    check-cast v0, Lcom/dropbox/android/activity/C;

    move-object v1, v0

    iput-object v1, p0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->b:Lcom/dropbox/android/activity/C;
    :try_end_9
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_9} :catch_a

    .line 40
    return-void

    .line 37
    :catch_a
    move-exception v1

    .line 38
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement DidntReceiveTwofactorCodeFragment.Callback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    const v0, 0x7f030022

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 53
    const v0, 0x7f060062

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 54
    new-instance v2, Lcom/dropbox/android/activity/A;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/A;-><init>(Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const v0, 0x7f060063

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 64
    new-instance v2, Lcom/dropbox/android/activity/B;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/B;-><init>(Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-object v1
.end method

.method public onDetach()V
    .registers 2

    .prologue
    .line 44
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onDetach()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/DidntReceiveTwofactorCodeFragment;->b:Lcom/dropbox/android/activity/C;

    .line 46
    return-void
.end method
