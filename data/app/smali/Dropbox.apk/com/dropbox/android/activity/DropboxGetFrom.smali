.class public Lcom/dropbox/android/activity/DropboxGetFrom;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 21
    const-class v0, Lcom/dropbox/android/activity/DropboxGetFrom;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/DropboxGetFrom;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 27
    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/DropboxGetFrom;->requestWindowFeature(I)Z

    .line 28
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/DropboxGetFrom;->setContentView(I)V

    .line 33
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v0

    if-nez v0, :cond_25

    .line 35
    const v0, 0x7f0b004b

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 36
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->finish()V

    .line 53
    :goto_24
    return-void

    .line 40
    :cond_25
    if-nez p1, :cond_4d

    .line 42
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/dropbox/android/activity/GetFromFragment;

    invoke-direct {v1}, Lcom/dropbox/android/activity/GetFromFragment;-><init>()V

    .line 44
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/dropbox/android/activity/GetFromFragment;->a(Landroid/net/Uri;Z)V

    .line 45
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v0

    .line 46
    const v2, 0x7f06007f

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/u;

    .line 47
    invoke-virtual {v0}, Landroid/support/v4/app/u;->b()I

    .line 50
    :cond_4d
    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v0

    .line 51
    invoke-static {}, Lcom/dropbox/android/util/i;->N()Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "request.mime.type"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/DropboxGetFrom;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "caller"

    if-eqz v0, :cond_73

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v0

    :goto_6b
    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    goto :goto_24

    :cond_73
    const/4 v0, 0x0

    goto :goto_6b
.end method
