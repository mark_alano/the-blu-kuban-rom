.class public Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;
.super Lcom/dropbox/android/activity/base/BaseFragment;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/activity/Q;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragment;-><init>()V

    .line 32
    return-void
.end method

.method public static a()Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;
    .registers 1

    .prologue
    .line 42
    new-instance v0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;-><init>()V

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)Lcom/dropbox/android/activity/Q;
    .registers 2
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Lcom/dropbox/android/activity/Q;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    .line 126
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    .line 128
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1b

    invoke-static {v0}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2f

    .line 131
    :cond_1b
    invoke-virtual {p0}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0b01a9

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 140
    :goto_2e
    return-void

    .line 139
    :cond_2f
    iget-object v1, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Lcom/dropbox/android/activity/Q;

    invoke-interface {v1, v0}, Lcom/dropbox/android/activity/Q;->a(Ljava/lang/String;)V

    goto :goto_2e
.end method

.method static synthetic b(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b()V

    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .registers 6
    .parameter

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 49
    :try_start_3
    move-object v0, p1

    check-cast v0, Lcom/dropbox/android/activity/Q;

    move-object v1, v0

    iput-object v1, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Lcom/dropbox/android/activity/Q;
    :try_end_9
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_9} :catch_a

    .line 53
    return-void

    .line 50
    :catch_a
    move-exception v1

    .line 51
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement EnterTwofactorCodeFragmentCallback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    .line 64
    const v0, 0x7f030025

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 66
    const v0, 0x7f060069

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->c:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/a;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    const v0, 0x7f06006a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->d:Landroid/widget/EditText;

    .line 70
    iget-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->d:Landroid/widget/EditText;

    new-instance v2, Lcom/dropbox/android/activity/M;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/M;-><init>(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 83
    const v0, 0x7f06006b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 84
    new-instance v2, Lcom/dropbox/android/activity/N;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/N;-><init>(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    const v0, 0x7f06006c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 95
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/l/m;->I()Ldbxyzptlk/l/t;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/l/t;->e()Ldbxyzptlk/l/s;

    move-result-object v2

    .line 97
    sget-object v3, Ldbxyzptlk/l/s;->b:Ldbxyzptlk/l/s;

    invoke-virtual {v3, v2}, Ldbxyzptlk/l/s;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_72

    .line 99
    const v2, 0x7f0b01b0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 100
    new-instance v2, Lcom/dropbox/android/activity/O;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/O;-><init>(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    :goto_71
    return-object v1

    .line 110
    :cond_72
    const v2, 0x7f0b01a8

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 111
    new-instance v2, Lcom/dropbox/android/activity/P;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/P;-><init>(Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_71
.end method

.method public onDetach()V
    .registers 2

    .prologue
    .line 57
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onDetach()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/EnterTwofactorCodeFragment;->b:Lcom/dropbox/android/activity/Q;

    .line 59
    return-void
.end method
