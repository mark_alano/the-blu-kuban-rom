.class public final Lcom/dropbox/android/activity/F;
.super Landroid/content/res/Resources;
.source "panda.py"


# instance fields
.field a:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .registers 6
    .parameter

    .prologue
    .line 360
    invoke-virtual {p1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    .line 357
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/activity/F;->a:I

    .line 361
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "action_bar_embed_tabs"

    const-string v2, "bool"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/F;->a:I

    .line 362
    return-void
.end method


# virtual methods
.method public final getBoolean(I)Z
    .registers 3
    .parameter

    .prologue
    .line 366
    iget v0, p0, Lcom/dropbox/android/activity/F;->a:I

    if-ne p1, v0, :cond_6

    .line 367
    const/4 v0, 0x1

    .line 369
    :goto_5
    return v0

    :cond_6
    invoke-super {p0, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    goto :goto_5
.end method
