.class public Lcom/dropbox/android/activity/GalleryPickerActivity;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/bl;
.implements Lcom/dropbox/android/util/e;
.implements Ldbxyzptlk/g/I;


# static fields
.field static final synthetic a:Z

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 23
    const-class v0, Lcom/dropbox/android/activity/GalleryPickerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/dropbox/android/activity/GalleryPickerActivity;->a:Z

    .line 26
    const-class v0, Lcom/dropbox/android/activity/GalleryPickerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/GalleryPickerActivity;->b:Ljava/lang/String;

    return-void

    .line 23
    :cond_14
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/GalleryPickerActivity;->c:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/util/f;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 77
    iget-boolean v2, p0, Lcom/dropbox/android/activity/GalleryPickerActivity;->c:Z

    if-nez v2, :cond_11

    .line 78
    sget-object v2, Lcom/dropbox/android/activity/am;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/util/f;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_34

    .line 94
    :cond_11
    :goto_11
    :pswitch_11
    return-void

    .line 84
    :pswitch_12
    iput-boolean v0, p0, Lcom/dropbox/android/activity/GalleryPickerActivity;->c:Z

    .line 85
    new-instance v2, Ldbxyzptlk/g/G;

    sget-object v3, Lcom/dropbox/android/util/f;->b:Lcom/dropbox/android/util/f;

    if-ne v3, p1, :cond_32

    :goto_1a
    invoke-direct {v2, p0, v0, p2, p3}, Ldbxyzptlk/g/G;-><init>(Landroid/content/Context;ZLjava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V

    .line 90
    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Ldbxyzptlk/g/G;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 91
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {p3}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/m;->a(Ljava/lang/String;)V

    goto :goto_11

    :cond_32
    move v0, v1

    .line 85
    goto :goto_1a

    .line 78
    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_12
    .end packed-switch
.end method

.method public final a(Ljava/util/List;)V
    .registers 4
    .parameter

    .prologue
    .line 99
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 100
    new-instance v1, Landroid/content/Intent;

    const-string v0, "com.dropbox.android.file_added"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 102
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/activity/GalleryPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 104
    :cond_1b
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->finish()V

    .line 105
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .registers 4
    .parameter

    .prologue
    .line 69
    sget-boolean v0, Lcom/dropbox/android/activity/GalleryPickerActivity;->a:Z

    if-nez v0, :cond_12

    if-eqz p1, :cond_c

    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_c
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 70
    :cond_12
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 71
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    invoke-static {p0, p0, p1, v1}, Lcom/dropbox/android/util/a;->a(Lcom/dropbox/android/util/e;Landroid/content/Context;Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)Z

    .line 72
    return-void
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->setResult(I)V

    .line 64
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->finish()V

    .line 65
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 40
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Launched without an upload path extra"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_17
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->setContentView(I)V

    .line 45
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    const-string v1, "GALLERY_PICKER"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/GalleryPickerFragment;

    .line 47
    if-nez v0, :cond_47

    .line 48
    new-instance v0, Lcom/dropbox/android/activity/GalleryPickerFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/GalleryPickerFragment;-><init>()V

    .line 49
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/GalleryPickerFragment;->setRetainInstance(Z)V

    .line 51
    invoke-virtual {p0}, Lcom/dropbox/android/activity/GalleryPickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v1

    .line 52
    const v2, 0x7f06007f

    const-string v3, "GALLERY_PICKER"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/u;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/u;

    .line 53
    invoke-virtual {v1}, Landroid/support/v4/app/u;->b()I

    .line 57
    :cond_47
    invoke-virtual {v0, p0}, Lcom/dropbox/android/activity/GalleryPickerFragment;->a(Lcom/dropbox/android/activity/bl;)V

    .line 58
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 3
    .parameter

    .prologue
    .line 111
    invoke-static {p0}, Lcom/dropbox/android/util/a;->a(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
