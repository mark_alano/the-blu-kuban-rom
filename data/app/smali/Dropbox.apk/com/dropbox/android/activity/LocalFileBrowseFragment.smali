.class public Lcom/dropbox/android/activity/LocalFileBrowseFragment;
.super Lcom/dropbox/android/activity/base/BaseFragment;
.source "panda.py"

# interfaces
.implements Landroid/support/v4/app/y;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field protected final a:Ljava/util/HashSet;

.field private c:Lcom/dropbox/android/util/aq;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ListView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ImageButton;

.field private h:Lcom/dropbox/android/widget/ar;

.field private i:Lcom/dropbox/android/activity/aE;

.field private j:Landroid/os/Bundle;

.field private k:Lcom/dropbox/android/activity/aF;

.field private final l:Landroid/widget/AdapterView$OnItemClickListener;

.field private m:Landroid/widget/Button;

.field private n:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 43
    const-class v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragment;-><init>()V

    .line 69
    sget-object v0, Lcom/dropbox/android/activity/aE;->a:Lcom/dropbox/android/activity/aE;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Lcom/dropbox/android/activity/aE;

    .line 94
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/util/HashSet;

    .line 111
    new-instance v0, Lcom/dropbox/android/activity/ay;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/ay;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->l:Landroid/widget/AdapterView$OnItemClickListener;

    .line 41
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 270
    if-nez p1, :cond_7

    .line 271
    iget-object p1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Landroid/os/Bundle;

    .line 272
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Landroid/os/Bundle;

    .line 275
    :cond_7
    if-eqz p1, :cond_1d

    .line 277
    const-string v0, "key_Mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 279
    :try_start_11
    const-string v0, "key_Mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/aE;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/aE;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Lcom/dropbox/android/activity/aE;
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_1d} :catch_1e

    .line 288
    :cond_1d
    :goto_1d
    return-void

    .line 280
    :catch_1e
    move-exception v0

    .line 281
    sget-object v0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b:Ljava/lang/String;

    const-string v1, "Invalid browse mode, or browse mode extra not specified"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    sget-object v0, Lcom/dropbox/android/activity/aE;->a:Lcom/dropbox/android/activity/aE;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Lcom/dropbox/android/activity/aE;

    goto :goto_1d

    .line 285
    :cond_2b
    sget-object v0, Lcom/dropbox/android/activity/aE;->a:Lcom/dropbox/android/activity/aE;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Lcom/dropbox/android/activity/aE;

    goto :goto_1d
.end method

.method private a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 297
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/widget/ar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ar;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 298
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 300
    invoke-static {v0}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v1

    .line 301
    sget-object v2, Lcom/dropbox/android/activity/aD;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/provider/P;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_b8

    .line 318
    const-string v1, "is_dir"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 319
    if-eqz v1, :cond_84

    .line 320
    const-string v1, "uri"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 321
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Landroid/net/Uri;)V

    .line 335
    :goto_39
    return-void

    .line 303
    :pswitch_3a
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->b()Lcom/dropbox/android/util/ap;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/util/ap;->a:Landroid/net/Uri;

    .line 304
    invoke-static {v0}, Lcom/dropbox/android/provider/FileSystemProvider;->c(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 306
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    invoke-virtual {v1}, Lcom/dropbox/android/util/aq;->d()I

    move-result v1

    .line 308
    const/4 v2, 0x2

    if-lt v1, v2, :cond_72

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v2, v1}, Lcom/dropbox/android/util/aq;->a(I)Lcom/dropbox/android/util/ap;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/android/util/ap;->a:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_72

    .line 309
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->c()Lcom/dropbox/android/util/ap;

    .line 315
    :goto_64
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/x;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/x;->b(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Ldbxyzptlk/a/d;

    goto :goto_39

    .line 311
    :cond_72
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/aq;->a(Landroid/widget/ListView;)V

    .line 312
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    new-instance v2, Lcom/dropbox/android/util/ap;

    invoke-direct {v2, v0}, Lcom/dropbox/android/util/ap;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/util/ap;)Lcom/dropbox/android/util/ap;

    goto :goto_64

    .line 323
    :cond_84
    new-instance v1, Ljava/io/File;

    const-string v2, "path"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 324
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b1

    .line 325
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 330
    :goto_a4
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/widget/ar;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, p2, v2, v0}, Lcom/dropbox/android/widget/ar;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 331
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i()V

    goto :goto_39

    .line 327
    :cond_b1
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_a4

    .line 301
    nop

    :pswitch_data_b8
    .packed-switch 0x1
        :pswitch_3a
    .end packed-switch
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/LocalFileBrowseFragment;Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct/range {p0 .. p5}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Landroid/widget/ListView;Landroid/view/View;IJ)V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)Lcom/dropbox/android/util/aq;
    .registers 2
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)Lcom/dropbox/android/activity/aF;
    .registers 2
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/aF;

    return-object v0
.end method

.method private d()V
    .registers 4

    .prologue
    .line 99
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->b()Lcom/dropbox/android/util/ap;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/util/ap;->a:Landroid/net/Uri;

    .line 100
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 105
    invoke-static {v0}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/net/Uri;)Z

    move-result v0

    .line 106
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 107
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2d

    const/4 v0, 0x0

    :goto_29
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 109
    :cond_2c
    return-void

    .line 107
    :cond_2d
    const/4 v0, 0x4

    goto :goto_29
.end method

.method static synthetic d(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h()V

    return-void
.end method

.method private e()Landroid/net/Uri;
    .registers 3

    .prologue
    .line 217
    sget-object v0, Lcom/dropbox/android/activity/aE;->b:Lcom/dropbox/android/activity/aE;

    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Lcom/dropbox/android/activity/aE;

    if-ne v0, v1, :cond_18

    .line 218
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->e()Landroid/net/Uri;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_18

    .line 221
    :try_start_10
    invoke-static {v0}, Lcom/dropbox/android/provider/FileSystemProvider;->a(Landroid/net/Uri;)Z
    :try_end_13
    .catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_13} :catch_17

    move-result v1

    if-eqz v1, :cond_18

    .line 229
    :goto_16
    return-object v0

    .line 224
    :catch_17
    move-exception v0

    .line 229
    :cond_18
    invoke-static {}, Lcom/dropbox/android/provider/FileSystemProvider;->a()Landroid/net/Uri;

    move-result-object v0

    goto :goto_16
.end method

.method static synthetic e(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V
    .registers 1
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g()V

    return-void
.end method

.method private final f()V
    .registers 4

    .prologue
    .line 233
    new-instance v0, Lcom/dropbox/android/util/aq;

    invoke-direct {v0}, Lcom/dropbox/android/util/aq;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    .line 234
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    new-instance v1, Lcom/dropbox/android/util/ap;

    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/ap;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/util/ap;)Lcom/dropbox/android/util/ap;

    .line 235
    return-void
.end method

.method private g()V
    .registers 4

    .prologue
    .line 244
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/aF;

    if-eqz v0, :cond_1b

    .line 245
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 247
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/aF;

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/util/HashSet;

    invoke-interface {v1, v0, v2}, Lcom/dropbox/android/activity/aF;->a(Landroid/net/Uri;Ljava/util/Set;)V

    .line 249
    :cond_1b
    return-void
.end method

.method private h()V
    .registers 3

    .prologue
    .line 263
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/aF;

    if-eqz v0, :cond_11

    .line 264
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/aF;

    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    invoke-virtual {v1}, Lcom/dropbox/android/util/aq;->b()Lcom/dropbox/android/util/ap;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/android/util/ap;->a:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/dropbox/android/activity/aF;->a(Landroid/net/Uri;)V

    .line 266
    :cond_11
    return-void
.end method

.method private i()V
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 338
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c()Z

    move-result v2

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3f

    :cond_11
    :goto_11
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 339
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c()Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 340
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "UPLOAD_PATH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 342
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    invoke-static {v1, v0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;I)Ljava/lang/String;

    move-result-object v0

    .line 343
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 345
    :cond_3e
    return-void

    .line 338
    :cond_3f
    const/4 v0, 0x0

    goto :goto_11
.end method

.method private j()V
    .registers 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/aF;

    if-eqz v0, :cond_9

    .line 349
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/aF;

    invoke-interface {v0}, Lcom/dropbox/android/activity/aF;->g_()V

    .line 351
    :cond_9
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Ldbxyzptlk/a/d;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 355
    new-instance v0, Lcom/dropbox/android/filemanager/H;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    invoke-virtual {v2}, Lcom/dropbox/android/util/aq;->b()Lcom/dropbox/android/util/ap;

    move-result-object v2

    iget-object v2, v2, Lcom/dropbox/android/util/ap;->a:Landroid/net/Uri;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/filemanager/H;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/net/Uri;)V
    .registers 5
    .parameter

    .prologue
    .line 238
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aq;->a(Landroid/widget/ListView;)V

    .line 239
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    new-instance v1, Lcom/dropbox/android/util/ap;

    invoke-direct {v1, p1}, Lcom/dropbox/android/util/ap;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aq;->a(Lcom/dropbox/android/util/ap;)Lcom/dropbox/android/util/ap;

    .line 240
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/x;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/x;->b(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Ldbxyzptlk/a/d;

    .line 241
    return-void
.end method

.method public final a(Ldbxyzptlk/a/d;)V
    .registers 4
    .parameter

    .prologue
    .line 408
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/widget/ar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ar;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 409
    return-void
.end method

.method public final a(Ldbxyzptlk/a/d;Landroid/database/Cursor;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/16 v4, -0x3e7

    .line 368
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/widget/ar;

    invoke-virtual {v0, p2}, Lcom/dropbox/android/widget/ar;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 369
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d()V

    .line 371
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/widget/ar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/ar;->c()Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 375
    invoke-static {}, Lcom/dropbox/android/util/af;->c()Z

    move-result v0

    if-nez v0, :cond_42

    invoke-static {}, Lcom/dropbox/android/util/af;->d()Z

    move-result v0

    if-nez v0, :cond_42

    .line 376
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f:Landroid/widget/TextView;

    const v1, 0x7f0b0044

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 380
    :goto_27
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 386
    :goto_2c
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    invoke-virtual {v0}, Lcom/dropbox/android/util/aq;->b()Lcom/dropbox/android/util/ap;

    move-result-object v0

    .line 387
    iget v1, v0, Lcom/dropbox/android/util/ap;->b:I

    if-ltz v1, :cond_53

    .line 388
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/ListView;

    iget v2, v0, Lcom/dropbox/android/util/ap;->b:I

    iget v3, v0, Lcom/dropbox/android/util/ap;->c:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 394
    iput v4, v0, Lcom/dropbox/android/util/ap;->b:I

    .line 401
    :cond_41
    :goto_41
    return-void

    .line 378
    :cond_42
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f:Landroid/widget/TextView;

    const v1, 0x7f0b003e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_27

    .line 382
    :cond_4b
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2c

    .line 395
    :cond_53
    iget v1, v0, Lcom/dropbox/android/util/ap;->b:I

    if-eq v1, v4, :cond_41

    .line 398
    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 399
    iput v4, v0, Lcom/dropbox/android/util/ap;->b:I

    goto :goto_41
.end method

.method public final bridge synthetic a(Ldbxyzptlk/a/d;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 41
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Ldbxyzptlk/a/d;Landroid/database/Cursor;)V

    return-void
.end method

.method public final a()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 252
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    invoke-virtual {v2}, Lcom/dropbox/android/util/aq;->d()I

    move-result v2

    if-gt v2, v1, :cond_b

    .line 259
    :goto_a
    return v0

    .line 256
    :cond_b
    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    invoke-virtual {v2}, Lcom/dropbox/android/util/aq;->c()Lcom/dropbox/android/util/ap;

    .line 257
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/x;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, p0}, Landroid/support/v4/app/x;->b(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Ldbxyzptlk/a/d;

    move v0, v1

    .line 259
    goto :goto_a
.end method

.method protected final b()Z
    .registers 3

    .prologue
    .line 414
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Lcom/dropbox/android/activity/aE;

    sget-object v1, Lcom/dropbox/android/activity/aE;->b:Lcom/dropbox/android/activity/aE;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method protected final c()Z
    .registers 3

    .prologue
    .line 418
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Lcom/dropbox/android/activity/aE;

    sget-object v1, Lcom/dropbox/android/activity/aE;->a:Lcom/dropbox/android/activity/aE;

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .registers 5
    .parameter

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 124
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d()V

    .line 128
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/x;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/x;->a(ILandroid/os/Bundle;Landroid/support/v4/app/y;)Ldbxyzptlk/a/d;

    .line 129
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 6
    .parameter

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 79
    :try_start_3
    move-object v0, p1

    check-cast v0, Lcom/dropbox/android/activity/aF;

    move-object v1, v0

    iput-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/aF;
    :try_end_9
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_9} :catch_a

    .line 83
    return-void

    .line 80
    :catch_a
    move-exception v1

    .line 81
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement LocalFileBrowseCallback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 135
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->j:Landroid/os/Bundle;

    .line 137
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a(Landroid/os/Bundle;)V

    .line 139
    new-instance v0, Lcom/dropbox/android/widget/ar;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b()Z

    move-result v4

    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c()Z

    move-result v5

    iget-object v6, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->a:Ljava/util/HashSet;

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/widget/ar;-><init>(Landroid/content/Context;Landroid/database/Cursor;IZZLjava/util/Set;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/widget/ar;

    .line 147
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f()V

    .line 148
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 153
    const v0, 0x7f030041

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 155
    const v0, 0x7f060078

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Landroid/widget/ImageButton;

    .line 157
    const v0, 0x7f0600a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/ListView;

    .line 158
    const v0, 0x7f060077

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d:Landroid/widget/TextView;

    .line 159
    const v0, 0x7f0600a8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->f:Landroid/widget/TextView;

    .line 161
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->h:Lcom/dropbox/android/widget/ar;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 162
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->l:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 164
    const v0, 0x7f060030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 165
    const v2, 0x7f0b0018

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 166
    new-instance v2, Lcom/dropbox/android/activity/az;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/az;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    const v0, 0x7f060031

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    .line 176
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 177
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->g:Landroid/widget/ImageButton;

    new-instance v2, Lcom/dropbox/android/activity/aA;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aA;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    const v2, 0x7f0b00cf

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 185
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/aB;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aB;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    :goto_8b
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->d()V

    .line 207
    return-object v1

    .line 192
    :cond_8f
    const v0, 0x7f0600de

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->n:Landroid/widget/TextView;

    .line 193
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 195
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    const v2, 0x7f0b00d0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 196
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->m:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/aC;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/aC;-><init>(Lcom/dropbox/android/activity/LocalFileBrowseFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    invoke-direct {p0}, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i()V

    goto :goto_8b
.end method

.method public onDetach()V
    .registers 2

    .prologue
    .line 87
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onDetach()V

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->k:Lcom/dropbox/android/activity/aF;

    .line 89
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 292
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 293
    const-string v0, "key_Mode"

    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->i:Lcom/dropbox/android/activity/aE;

    invoke-virtual {v1}, Lcom/dropbox/android/activity/aE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    return-void
.end method

.method public onStop()V
    .registers 3

    .prologue
    .line 212
    iget-object v0, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->c:Lcom/dropbox/android/util/aq;

    iget-object v1, p0, Lcom/dropbox/android/activity/LocalFileBrowseFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/util/aq;->a(Landroid/widget/ListView;)V

    .line 213
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onStop()V

    .line 214
    return-void
.end method
