.class public Lcom/dropbox/android/activity/LoginBrandFragment;
.super Lcom/dropbox/android/activity/base/BaseFragment;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field protected b:Lcom/dropbox/android/activity/aP;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/LoginBrandFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_FRAG_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/LoginBrandFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragment;-><init>()V

    .line 23
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .registers 6
    .parameter

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 46
    :try_start_3
    move-object v0, p1

    check-cast v0, Lcom/dropbox/android/activity/aP;

    move-object v1, v0

    iput-object v1, p0, Lcom/dropbox/android/activity/LoginBrandFragment;->b:Lcom/dropbox/android/activity/aP;
    :try_end_9
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_9} :catch_a

    .line 50
    return-void

    .line 47
    :catch_a
    move-exception v1

    .line 48
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement LoginBrandFragmentCallback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x8

    .line 62
    invoke-super {p0, p3}, Lcom/dropbox/android/activity/base/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f030045

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 65
    const v0, 0x7f0600b6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 66
    invoke-static {}, Lcom/dropbox/android/util/ab;->a()Z

    move-result v1

    if-eqz v1, :cond_61

    .line 67
    const v1, 0x7f02018f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 68
    new-instance v1, Lcom/dropbox/android/activity/aM;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/aM;-><init>(Lcom/dropbox/android/activity/LoginBrandFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    :goto_2a
    const v0, 0x7f0600b8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 79
    const v1, 0x7f0600b9

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 80
    invoke-virtual {p0}, Lcom/dropbox/android/activity/LoginBrandFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v3

    if-eqz v3, :cond_68

    .line 81
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 82
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 104
    :goto_4c
    const v0, 0x7f0600ba

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 116
    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_79

    .line 117
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 75
    :cond_61
    const v1, 0x7f020191

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2a

    .line 85
    :cond_68
    new-instance v3, Lcom/dropbox/android/activity/aN;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/aN;-><init>(Lcom/dropbox/android/activity/LoginBrandFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    new-instance v0, Lcom/dropbox/android/activity/aO;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/aO;-><init>(Lcom/dropbox/android/activity/LoginBrandFragment;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4c

    .line 120
    :cond_79
    return-object v2
.end method

.method public onDetach()V
    .registers 2

    .prologue
    .line 54
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onDetach()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/LoginBrandFragment;->b:Lcom/dropbox/android/activity/aP;

    .line 56
    return-void
.end method
