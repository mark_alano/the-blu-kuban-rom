.class public Lcom/dropbox/android/activity/TourActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/activity/cl;


# instance fields
.field private a:[Lcom/dropbox/android/activity/bT;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;[Lcom/dropbox/android/activity/bT;)Landroid/content/Intent;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 26
    array-length v0, p1

    new-array v1, v0, [Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    :goto_4
    array-length v2, p1

    if-ge v0, v2, :cond_12

    .line 28
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/dropbox/android/activity/bT;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 31
    :cond_12
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/dropbox/android/activity/TourActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/l/m;->t()Z

    move-result v2

    if-nez v2, :cond_29

    .line 33
    const-string v2, "INTRO_TOUR"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 35
    :cond_29
    const-string v2, "EXTRA_TOUR_PAGES"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    return-object v0
.end method

.method private a(IZ)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 83
    iget-object v0, p0, Lcom/dropbox/android/activity/TourActivity;->a:[Lcom/dropbox/android/activity/bT;

    aget-object v0, v0, p1

    invoke-static {v0, p1}, Lcom/dropbox/android/activity/TourPageFragment;->a(Lcom/dropbox/android/activity/bT;I)Lcom/dropbox/android/activity/TourPageFragment;

    move-result-object v0

    .line 84
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v1

    .line 85
    const v2, 0x7f06007f

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/u;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/u;

    .line 86
    if-eqz p2, :cond_1c

    .line 87
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/u;->a(Ljava/lang/String;)Landroid/support/v4/app/u;

    .line 89
    :cond_1c
    invoke-virtual {v1}, Landroid/support/v4/app/u;->b()I

    .line 90
    return-void
.end method


# virtual methods
.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 73
    add-int/lit8 v0, p1, 0x1

    .line 74
    iget-object v1, p0, Lcom/dropbox/android/activity/TourActivity;->a:[Lcom/dropbox/android/activity/bT;

    array-length v1, v1

    if-ge v0, v1, :cond_c

    .line 75
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/dropbox/android/activity/TourActivity;->a(IZ)V

    .line 80
    :goto_b
    return-void

    .line 77
    :cond_c
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->setResult(I)V

    .line 78
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->finish()V

    goto :goto_b
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 42
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "EXTRA_TOUR_PAGES"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 45
    if-nez v2, :cond_19

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TourActivity expects an extra with the pages it should show."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_19
    array-length v0, v2

    new-array v0, v0, [Lcom/dropbox/android/activity/bT;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourActivity;->a:[Lcom/dropbox/android/activity/bT;

    move v0, v1

    .line 50
    :goto_1f
    array-length v3, v2

    if-ge v0, v3, :cond_2f

    .line 51
    iget-object v3, p0, Lcom/dropbox/android/activity/TourActivity;->a:[Lcom/dropbox/android/activity/bT;

    aget-object v4, v2, v0

    invoke-static {v4}, Lcom/dropbox/android/activity/bT;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/bT;

    move-result-object v4

    aput-object v4, v3, v0

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_1f

    .line 54
    :cond_2f
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-nez v0, :cond_3d

    .line 56
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->setRequestedOrientation(I)V

    .line 59
    :cond_3d
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    .line 60
    const v2, 0x7f020172

    invoke-virtual {v0, v2}, Lcom/actionbarsherlock/app/ActionBar;->setIcon(I)V

    .line 61
    invoke-virtual {v0, v5}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 62
    invoke-virtual {v0, v5}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 64
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/TourActivity;->setContentView(I)V

    .line 66
    if-nez p1, :cond_58

    .line 67
    invoke-direct {p0, v1, v1}, Lcom/dropbox/android/activity/TourActivity;->a(IZ)V

    .line 69
    :cond_58
    return-void
.end method
