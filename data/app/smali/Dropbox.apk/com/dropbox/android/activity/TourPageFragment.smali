.class public Lcom/dropbox/android/activity/TourPageFragment;
.super Lcom/dropbox/android/activity/base/BaseFragment;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/activity/cl;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/Button;

.field private f:Landroid/view/View;

.field private g:Landroid/view/ViewStub;

.field private h:Lcom/dropbox/android/activity/ck;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseFragment;-><init>()V

    .line 131
    return-void
.end method

.method public static a(Lcom/dropbox/android/activity/bT;I)Lcom/dropbox/android/activity/TourPageFragment;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 56
    new-instance v0, Lcom/dropbox/android/activity/TourPageFragment;

    invoke-direct {v0}, Lcom/dropbox/android/activity/TourPageFragment;-><init>()V

    .line 58
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v2, "ARG_PAGE"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/bT;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v2, "ARG_INDEX"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 61
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/TourPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 63
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/TourPageFragment;)Lcom/dropbox/android/activity/cl;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->a:Lcom/dropbox/android/activity/cl;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/widget/Button;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->e:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/view/ViewStub;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->g:Landroid/view/ViewStub;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->i:Landroid/view/View;

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->j:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/widget/Button;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->k:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic k(Lcom/dropbox/android/activity/TourPageFragment;)Landroid/widget/Button;
    .registers 2
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->l:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .registers 6
    .parameter

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onAttach(Landroid/app/Activity;)V

    .line 74
    :try_start_3
    move-object v0, p1

    check-cast v0, Lcom/dropbox/android/activity/cl;

    move-object v1, v0

    iput-object v1, p0, Lcom/dropbox/android/activity/TourPageFragment;->a:Lcom/dropbox/android/activity/cl;
    :try_end_9
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_9} :catch_a

    .line 78
    return-void

    .line 75
    :catch_a
    move-exception v1

    .line 76
    new-instance v1, Ljava/lang/ClassCastException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " must implement TourPageFragmentCallback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 97
    const v0, 0x7f030030

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 99
    const v0, 0x7f060094

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->b:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f060095

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->c:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f060093

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->d:Landroid/widget/ImageView;

    .line 102
    const v0, 0x7f060090

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->e:Landroid/widget/Button;

    .line 103
    const v0, 0x7f060092

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->f:Landroid/view/View;

    .line 104
    const v0, 0x7f060091

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->g:Landroid/view/ViewStub;

    .line 105
    const v0, 0x7f06008f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->i:Landroid/view/View;

    .line 106
    const v0, 0x7f06004a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->j:Landroid/view/View;

    .line 107
    const v0, 0x7f060031

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->k:Landroid/widget/Button;

    .line 108
    const v0, 0x7f060030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->l:Landroid/widget/Button;

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->e:Landroid/widget/Button;

    new-instance v2, Lcom/dropbox/android/activity/bS;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/bS;-><init>(Lcom/dropbox/android/activity/TourPageFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    invoke-virtual {p0}, Lcom/dropbox/android/activity/TourPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 120
    if-eqz v0, :cond_90

    const-string v2, "ARG_PAGE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_90

    const-string v2, "ARG_INDEX"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_98

    .line 121
    :cond_90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "TourPageFragment expects an arg with the page to show and an arg with that page\'s index in the tour."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_98
    const-string v2, "ARG_PAGE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/bT;->valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/bT;

    move-result-object v0

    .line 125
    invoke-static {}, Lcom/dropbox/android/util/i;->a()Lcom/dropbox/android/util/s;

    move-result-object v2

    const-string v3, "page"

    invoke-virtual {v0}, Lcom/dropbox/android/activity/bT;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/s;->c()V

    .line 126
    invoke-virtual {v0, p0, p3}, Lcom/dropbox/android/activity/bT;->a(Lcom/dropbox/android/activity/TourPageFragment;Landroid/os/Bundle;)Lcom/dropbox/android/activity/ck;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->h:Lcom/dropbox/android/activity/ck;

    .line 128
    return-object v1
.end method

.method public onDetach()V
    .registers 2

    .prologue
    .line 82
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseFragment;->onDetach()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->a:Lcom/dropbox/android/activity/cl;

    .line 84
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 3
    .parameter

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 89
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->h:Lcom/dropbox/android/activity/ck;

    if-eqz v0, :cond_c

    .line 90
    iget-object v0, p0, Lcom/dropbox/android/activity/TourPageFragment;->h:Lcom/dropbox/android/activity/ck;

    invoke-interface {v0, p1}, Lcom/dropbox/android/activity/ck;->a(Landroid/os/Bundle;)V

    .line 92
    :cond_c
    return-void
.end method
