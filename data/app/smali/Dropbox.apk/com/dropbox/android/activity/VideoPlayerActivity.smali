.class public Lcom/dropbox/android/activity/VideoPlayerActivity;
.super Lcom/dropbox/android/activity/base/BaseActivity;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/u;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/net/Uri;

.field private c:Lcom/dropbox/android/widget/DbxVideoView;

.field private d:Lcom/dropbox/android/widget/DbxMediaController;

.field private f:Landroid/widget/ProgressBar;

.field private g:Lcom/dropbox/android/activity/cy;

.field private h:I

.field private i:Z

.field private final j:Landroid/os/Handler;

.field private final k:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 48
    const-class v0, Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Lcom/dropbox/android/activity/base/BaseActivity;-><init>()V

    .line 74
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    .line 76
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    .line 77
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->d:Lcom/dropbox/android/widget/DbxMediaController;

    .line 78
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Landroid/widget/ProgressBar;

    .line 81
    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Lcom/dropbox/android/activity/cy;

    .line 89
    iput v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:I

    .line 95
    iput-boolean v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:Z

    .line 98
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Landroid/os/Handler;

    .line 111
    new-instance v0, Lcom/dropbox/android/activity/co;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/co;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Ljava/lang/Runnable;

    .line 413
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxVideoView;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    return-object v0
.end method

.method private a(I)V
    .registers 5
    .parameter

    .prologue
    .line 105
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting duration to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    if-eqz v0, :cond_21

    .line 107
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->setDuration(I)V

    .line 109
    :cond_21
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/VideoPlayerActivity;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/VideoPlayerActivity;)I
    .registers 2
    .parameter

    .prologue
    .line 46
    iget v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:I

    return v0
.end method

.method static synthetic b()Ljava/lang/String;
    .registers 1

    .prologue
    .line 46
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/widget/ProgressBar;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 323
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 324
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 325
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    const-string v1, "Showing spinner."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 327
    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/activity/VideoPlayerActivity;)Ljava/lang/Runnable;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Ljava/lang/Runnable;

    return-object v0
.end method

.method private d()V
    .registers 3

    .prologue
    .line 330
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Landroid/os/Handler;

    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 331
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 332
    return-void
.end method

.method static synthetic e(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->j:Landroid/os/Handler;

    return-object v0
.end method

.method private e()V
    .registers 3

    .prologue
    .line 403
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/dropbox/android/util/bj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 404
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->setSystemUiVisibility(I)V

    .line 406
    :cond_e
    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/activity/VideoPlayerActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->e()V

    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/VideoPlayerActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->d()V

    return-void
.end method

.method static synthetic h(Lcom/dropbox/android/activity/VideoPlayerActivity;)V
    .registers 1
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->c()V

    return-void
.end method

.method static synthetic i(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxMediaController;
    .registers 2
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->d:Lcom/dropbox/android/widget/DbxMediaController;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 149
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 151
    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 152
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    .line 154
    const v0, 0x7f030066

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->setContentView(I)V

    .line 156
    const v0, 0x7f0600fd

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Landroid/widget/ProgressBar;

    .line 157
    const v0, 0x7f0600fc

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/DbxVideoView;

    iput-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    .line 160
    const-string v0, "EXTRA_CONTAINER"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    const-string v2, "EXTRA_PROGRESS_URL"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 166
    if-eqz v2, :cond_145

    .line 167
    iput-boolean v4, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:Z

    .line 172
    :goto_3b
    iget-boolean v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:Z

    if-eqz v1, :cond_44

    .line 173
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v1, v4}, Lcom/dropbox/android/widget/DbxVideoView;->setAllowSeek(Z)V

    .line 175
    :cond_44
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v3, Lcom/dropbox/android/activity/cq;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/cq;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/dropbox/android/widget/DbxVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 188
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v3, Lcom/dropbox/android/activity/cr;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/cr;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/dropbox/android/widget/DbxVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 202
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v3, Lcom/dropbox/android/activity/cs;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/cs;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    invoke-virtual {v1, v3}, Lcom/dropbox/android/widget/DbxVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 211
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v3, Lcom/dropbox/android/activity/ct;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/ct;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/dropbox/android/widget/DbxVideoView;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 221
    new-instance v1, Lcom/dropbox/android/widget/DbxMediaController;

    invoke-direct {v1, p0, v5, p0, v2}, Lcom/dropbox/android/widget/DbxMediaController;-><init>(Landroid/content/Context;ZLcom/dropbox/android/widget/u;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->d:Lcom/dropbox/android/widget/DbxMediaController;

    .line 223
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->d:Lcom/dropbox/android/widget/DbxMediaController;

    new-instance v2, Lcom/dropbox/android/activity/cu;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cu;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/DbxMediaController;->setOnHideListener(Lcom/dropbox/android/widget/w;)V

    .line 230
    iget-boolean v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:Z

    if-eqz v1, :cond_8b

    .line 231
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->d:Lcom/dropbox/android/widget/DbxMediaController;

    new-instance v2, Lcom/dropbox/android/activity/cv;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cv;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/DbxMediaController;->setOnUserSeekListener(Lcom/dropbox/android/widget/y;)V

    .line 249
    :cond_8b
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->d:Lcom/dropbox/android/widget/DbxMediaController;

    new-instance v2, Lcom/dropbox/android/activity/cw;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/cw;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/DbxMediaController;->setOnPlayPauseListener(Lcom/dropbox/android/widget/x;)V

    .line 270
    new-instance v1, Lcom/dropbox/android/activity/cx;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/cx;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V

    .line 277
    const/16 v2, 0xe

    invoke-static {v2}, Lcom/dropbox/android/util/bj;->a(I)Z

    move-result v2

    if-eqz v2, :cond_ac

    .line 278
    iget-object v2, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    new-instance v3, Lcom/dropbox/android/activity/cp;

    invoke-direct {v3, p0, v1}, Lcom/dropbox/android/activity/cp;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v3}, Lcom/dropbox/android/widget/DbxVideoView;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 296
    :cond_ac
    invoke-virtual {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "EXTRA_METADATA_URL"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 300
    if-eqz v1, :cond_c6

    .line 301
    new-instance v2, Lcom/dropbox/android/activity/cy;

    invoke-direct {v2, p0, v1}, Lcom/dropbox/android/activity/cy;-><init>(Lcom/dropbox/android/activity/VideoPlayerActivity;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Lcom/dropbox/android/activity/cy;

    .line 302
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Lcom/dropbox/android/activity/cy;

    new-array v2, v5, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/cy;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 305
    :cond_c6
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v2, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->d:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/DbxVideoView;->setMediaController(Lcom/dropbox/android/widget/DbxMediaController;)V

    .line 306
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->d:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v1}, Lcom/dropbox/android/widget/DbxMediaController;->f()V

    .line 308
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 309
    sget-object v1, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uri = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v2, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/DbxVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 311
    invoke-static {}, Lcom/dropbox/android/util/i;->w()Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "host"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->b:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "can_seek"

    iget-boolean v3, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:Z

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "container"

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 314
    if-eqz p1, :cond_144

    iget-boolean v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:Z

    if-eqz v0, :cond_144

    .line 315
    const-string v0, "START_POSITION"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:I

    .line 317
    :cond_144
    return-void

    .line 169
    :cond_145
    const-string v3, "EXTRA_CAN_SEEK"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:Z

    goto/16 :goto_3b
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 385
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onDestroy()V

    .line 386
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Lcom/dropbox/android/activity/cy;

    if-eqz v0, :cond_12

    .line 389
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Lcom/dropbox/android/activity/cy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/cy;->cancel(Z)Z

    .line 390
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->g:Lcom/dropbox/android/activity/cy;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/cy;->a()V

    .line 392
    :cond_12
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->j()V

    .line 393
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->d:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->e()V

    .line 394
    return-void
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 353
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onPause()V

    .line 355
    iget-boolean v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->i:Z

    if-eqz v0, :cond_f

    .line 356
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->d()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:I

    .line 358
    :cond_f
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->l()V

    .line 359
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->d()V

    .line 360
    return-void
.end method

.method public onResume()V
    .registers 4

    .prologue
    .line 364
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onResume()V

    .line 365
    iget v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:I

    if-lez v0, :cond_3c

    .line 366
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resuming at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    iget v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:I

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->a(I)V

    .line 368
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->m()V

    .line 373
    :goto_2d
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->k()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 374
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->c()V

    .line 379
    :goto_38
    invoke-direct {p0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->e()V

    .line 380
    return-void

    .line 370
    :cond_3c
    iget-object v0, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->c:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->a()V

    goto :goto_2d

    .line 376
    :cond_42
    sget-object v0, Lcom/dropbox/android/activity/VideoPlayerActivity;->a:Ljava/lang/String;

    const-string v1, "Paused, no spinner."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_38
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 398
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/base/BaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 399
    const-string v0, "START_POSITION"

    iget v1, p0, Lcom/dropbox/android/activity/VideoPlayerActivity;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 400
    return-void
.end method

.method public onStart()V
    .registers 5

    .prologue
    .line 336
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onStart()V

    .line 337
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/dropbox/android/util/bj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 338
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 341
    :cond_19
    return-void
.end method

.method public onStop()V
    .registers 3

    .prologue
    .line 345
    invoke-super {p0}, Lcom/dropbox/android/activity/base/BaseActivity;->onStop()V

    .line 346
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/dropbox/android/util/bj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 347
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 349
    :cond_17
    return-void
.end method
