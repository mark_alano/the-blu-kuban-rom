.class public Lcom/dropbox/android/activity/WebViewActivity;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/webkit/WebView;

.field private c:Landroid/widget/Button;

.field private d:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z

.field private final h:Lcom/dropbox/android/util/g;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 32
    const-class v0, Lcom/dropbox/android/activity/WebViewActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/WebViewActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 41
    iput-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->c:Landroid/widget/Button;

    .line 42
    iput-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->d:Ljava/lang/String;

    .line 43
    iput-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->f:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->g:Z

    .line 46
    new-instance v0, Lcom/dropbox/android/util/g;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/g;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->h:Lcom/dropbox/android/util/g;

    .line 104
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/WebViewActivity;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/WebViewActivity;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/dropbox/android/activity/WebViewActivity;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/activity/WebViewActivity;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/WebViewActivity;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 249
    if-eqz p1, :cond_a

    const/4 v0, -0x1

    :goto_3
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/WebViewActivity;->setResult(I)V

    .line 250
    invoke-virtual {p0}, Lcom/dropbox/android/activity/WebViewActivity;->finish()V

    .line 251
    return-void

    .line 249
    :cond_a
    const/4 v0, 0x0

    goto :goto_3
.end method

.method static synthetic b(Lcom/dropbox/android/activity/WebViewActivity;)Landroid/widget/Button;
    .registers 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->c:Landroid/widget/Button;

    return-object v0
.end method

.method private d()Landroid/webkit/WebViewClient;
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 123
    invoke-static {}, Lcom/dropbox/android/util/bj;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    new-instance v0, Lcom/dropbox/android/activity/cD;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/activity/cD;-><init>(Lcom/dropbox/android/activity/WebViewActivity;Lcom/dropbox/android/activity/cz;)V

    :goto_c
    return-object v0

    :cond_d
    new-instance v0, Lcom/dropbox/android/activity/cC;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/activity/cC;-><init>(Lcom/dropbox/android/activity/WebViewActivity;Lcom/dropbox/android/activity/cz;)V

    goto :goto_c
.end method


# virtual methods
.method public a()Z
    .registers 2

    .prologue
    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method protected a_()Z
    .registers 2

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 145
    invoke-virtual {p0}, Lcom/dropbox/android/activity/WebViewActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 147
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 149
    invoke-virtual {p0}, Lcom/dropbox/android/activity/WebViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 150
    const-string v0, "BUTTONS"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 151
    iput-boolean v4, p0, Lcom/dropbox/android/activity/WebViewActivity;->g:Z

    .line 154
    :cond_1a
    if-eqz p1, :cond_24

    .line 155
    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->d:Ljava/lang/String;

    .line 158
    :cond_24
    iget-boolean v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->g:Z

    if-eqz v0, :cond_b9

    .line 159
    const v0, 0x7f030067

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/WebViewActivity;->setContentView(I)V

    .line 165
    :goto_2e
    invoke-virtual {p0, v4}, Lcom/dropbox/android/activity/WebViewActivity;->setProgressBarVisibility(Z)V

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->d:Ljava/lang/String;

    if-nez v0, :cond_d1

    .line 170
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 171
    if-eqz v1, :cond_c8

    .line 172
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    iget-object v3, p0, Lcom/dropbox/android/activity/WebViewActivity;->h:Lcom/dropbox/android/util/g;

    invoke-virtual {v3, v1}, Lcom/dropbox/android/util/g;->a(Landroid/net/Uri;)V

    :goto_44
    move-object v1, v0

    .line 185
    :goto_45
    const-string v0, "TITLE"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d6

    .line 186
    const-string v0, "TITLE"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->f:Ljava/lang/String;

    .line 191
    :goto_55
    const v0, 0x7f060096

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/WebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->b:Landroid/webkit/WebView;

    .line 192
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 195
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->b:Landroid/webkit/WebView;

    new-instance v2, Lcom/dropbox/android/activity/cz;

    invoke-direct {v2, p0, p0}, Lcom/dropbox/android/activity/cz;-><init>(Lcom/dropbox/android/activity/WebViewActivity;Landroid/app/Activity;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 204
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->b:Landroid/webkit/WebView;

    invoke-direct {p0}, Lcom/dropbox/android/activity/WebViewActivity;->d()Landroid/webkit/WebViewClient;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 206
    if-eqz v1, :cond_dc

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 211
    :goto_83
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/WebViewActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 213
    iget-boolean v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->g:Z

    if-eqz v0, :cond_b8

    .line 214
    const v0, 0x7f0600fe

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/WebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/cA;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/cA;-><init>(Lcom/dropbox/android/activity/WebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    const v0, 0x7f0600ff

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/WebViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->c:Landroid/widget/Button;

    .line 221
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 222
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->c:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/cB;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/cB;-><init>(Lcom/dropbox/android/activity/WebViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 229
    :cond_b8
    return-void

    .line 161
    :cond_b9
    const v0, 0x7f030031

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/WebViewActivity;->setContentView(I)V

    .line 162
    invoke-virtual {p0}, Lcom/dropbox/android/activity/WebViewActivity;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/actionbarsherlock/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto/16 :goto_2e

    .line 179
    :cond_c8
    const v0, 0x7f0b000b

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/WebViewActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_44

    .line 182
    :cond_d1
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->d:Ljava/lang/String;

    move-object v1, v0

    goto/16 :goto_45

    .line 188
    :cond_d6
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->f:Ljava/lang/String;

    goto/16 :goto_55

    .line 209
    :cond_dc
    invoke-virtual {p0}, Lcom/dropbox/android/activity/WebViewActivity;->finish()V

    goto :goto_83
.end method

.method protected onDestroy()V
    .registers 2

    .prologue
    .line 233
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onDestroy()V

    .line 234
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->h:Lcom/dropbox/android/util/g;

    invoke-virtual {v0}, Lcom/dropbox/android/util/g;->a()V

    .line 235
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 239
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1e

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1e

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 240
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 241
    iget-object v0, p0, Lcom/dropbox/android/activity/WebViewActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 242
    const/4 v0, 0x1

    .line 245
    :goto_1d
    return v0

    :cond_1e
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1d
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 255
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_e

    .line 256
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/WebViewActivity;->a(Z)V

    .line 257
    const/4 v0, 0x1

    .line 259
    :cond_e
    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter

    .prologue
    .line 139
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 140
    const-string v0, "url"

    iget-object v1, p0, Lcom/dropbox/android/activity/WebViewActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    return-void
.end method
