.class public abstract enum Lcom/dropbox/android/activity/aW;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/activity/aW;

.field public static final enum b:Lcom/dropbox/android/activity/aW;

.field public static final enum c:Lcom/dropbox/android/activity/aW;

.field public static final enum d:Lcom/dropbox/android/activity/aW;

.field public static final enum e:Lcom/dropbox/android/activity/aW;

.field private static final f:Ljava/util/concurrent/ConcurrentHashMap;

.field private static final synthetic h:[Lcom/dropbox/android/activity/aW;


# instance fields
.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 167
    new-instance v0, Lcom/dropbox/android/activity/aX;

    const-string v1, "NEW_ACCT_PROGRESS"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/activity/aX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/aW;->a:Lcom/dropbox/android/activity/aW;

    .line 177
    new-instance v0, Lcom/dropbox/android/activity/aY;

    const-string v1, "LOGIN_PROGRESS"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/activity/aY;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/aW;->b:Lcom/dropbox/android/activity/aW;

    .line 187
    new-instance v0, Lcom/dropbox/android/activity/aZ;

    const-string v1, "REQUEST_PASSWORD_RESET_PROGRESS"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/activity/aZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/aW;->c:Lcom/dropbox/android/activity/aW;

    .line 194
    new-instance v0, Lcom/dropbox/android/activity/ba;

    const-string v1, "REQUEST_RESEND_TWOFACTOR_CODE"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/activity/ba;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/aW;->d:Lcom/dropbox/android/activity/aW;

    .line 204
    new-instance v0, Lcom/dropbox/android/activity/bb;

    const-string v1, "VERIFY_CODE_PROGRESS"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/activity/bb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/activity/aW;->e:Lcom/dropbox/android/activity/aW;

    .line 162
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dropbox/android/activity/aW;

    sget-object v1, Lcom/dropbox/android/activity/aW;->a:Lcom/dropbox/android/activity/aW;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/activity/aW;->b:Lcom/dropbox/android/activity/aW;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/activity/aW;->c:Lcom/dropbox/android/activity/aW;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/activity/aW;->d:Lcom/dropbox/android/activity/aW;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/activity/aW;->e:Lcom/dropbox/android/activity/aW;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/activity/aW;->h:[Lcom/dropbox/android/activity/aW;

    .line 212
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/activity/aW;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 219
    const-class v0, Lcom/dropbox/android/activity/aW;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_76

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/aW;

    .line 220
    sget-object v2, Lcom/dropbox/android/activity/aW;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/aW;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5c

    .line 222
    :cond_76
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 224
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 225
    invoke-virtual {p0}, Lcom/dropbox/android/activity/aW;->ordinal()I

    move-result v0

    add-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/dropbox/android/activity/aW;->g:I

    .line 226
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/activity/aV;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 162
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/activity/aW;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/dropbox/android/activity/aW;
    .registers 3
    .parameter

    .prologue
    .line 233
    sget-object v0, Lcom/dropbox/android/activity/aW;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/aW;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/activity/aW;
    .registers 2
    .parameter

    .prologue
    .line 162
    const-class v0, Lcom/dropbox/android/activity/aW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/aW;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/activity/aW;
    .registers 1

    .prologue
    .line 162
    sget-object v0, Lcom/dropbox/android/activity/aW;->h:[Lcom/dropbox/android/activity/aW;

    invoke-virtual {v0}, [Lcom/dropbox/android/activity/aW;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/activity/aW;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 229
    iget v0, p0, Lcom/dropbox/android/activity/aW;->g:I

    return v0
.end method

.method abstract a(Lcom/dropbox/android/activity/LoginOrNewAcctActivity;)Ljava/lang/String;
.end method

.method final b(Lcom/dropbox/android/activity/LoginOrNewAcctActivity;)Landroid/app/Dialog;
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 239
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 240
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 241
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 243
    invoke-virtual {p0, p1}, Lcom/dropbox/android/activity/aW;->a(Lcom/dropbox/android/activity/LoginOrNewAcctActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 245
    return-object v0
.end method
