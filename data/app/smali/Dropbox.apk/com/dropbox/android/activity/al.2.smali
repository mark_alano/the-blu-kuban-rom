.class final Lcom/dropbox/android/activity/al;
.super Ldbxyzptlk/g/c;
.source "panda.py"


# instance fields
.field a:Lcom/dropbox/android/filemanager/LocalEntry;

.field final synthetic b:Lcom/dropbox/android/activity/ak;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/ak;Landroid/content/Context;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 343
    iput-object p1, p0, Lcom/dropbox/android/activity/al;->b:Lcom/dropbox/android/activity/ak;

    invoke-direct {p0, p2}, Ldbxyzptlk/g/c;-><init>(Landroid/content/Context;)V

    .line 344
    iget-object v0, p0, Lcom/dropbox/android/activity/al;->b:Lcom/dropbox/android/activity/ak;

    iget-object v0, v0, Lcom/dropbox/android/activity/ak;->a:Lcom/dropbox/android/activity/GalleryActivity;

    iget-object v0, v0, Lcom/dropbox/android/activity/GalleryActivity;->a:Lcom/dropbox/android/widget/GalleryView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/GalleryView;->b()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 348
    invoke-static {}, Lcom/dropbox/android/activity/GalleryActivity;->f()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Showing delete progress dialog."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    iget-object v0, p0, Lcom/dropbox/android/activity/al;->b:Lcom/dropbox/android/activity/ak;

    iget-object v0, v0, Lcom/dropbox/android/activity/ak;->a:Lcom/dropbox/android/activity/GalleryActivity;

    const/16 v1, 0x3b7

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->showDialog(I)V

    .line 350
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 372
    iget-object v0, p0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_30

    const v0, 0x7f0b009a

    .line 373
    :goto_a
    iget-object v1, p0, Lcom/dropbox/android/activity/al;->b:Lcom/dropbox/android/activity/ak;

    iget-object v1, v1, Lcom/dropbox/android/activity/ak;->a:Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 375
    invoke-static {p1, v0, v4}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 378
    :try_start_26
    iget-object v0, p0, Lcom/dropbox/android/activity/al;->b:Lcom/dropbox/android/activity/ak;

    iget-object v0, v0, Lcom/dropbox/android/activity/ak;->a:Lcom/dropbox/android/activity/GalleryActivity;

    const/16 v1, 0x3b7

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->dismissDialog(I)V
    :try_end_2f
    .catch Ljava/lang/IllegalArgumentException; {:try_start_26 .. :try_end_2f} :catch_34

    .line 382
    :goto_2f
    return-void

    .line 372
    :cond_30
    const v0, 0x7f0b0096

    goto :goto_a

    .line 379
    :catch_34
    move-exception v0

    goto :goto_2f
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 343
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/al;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 358
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/activity/al;->b:Lcom/dropbox/android/activity/ak;

    iget-object v0, v0, Lcom/dropbox/android/activity/ak;->a:Lcom/dropbox/android/activity/GalleryActivity;

    const/16 v1, 0x3b7

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/GalleryActivity;->dismissDialog(I)V
    :try_end_9
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_9} :catch_24

    .line 363
    :goto_9
    instance-of v0, p1, Lcom/dropbox/android/activity/GalleryActivity;

    if-eqz v0, :cond_13

    move-object v0, p1

    .line 364
    check-cast v0, Lcom/dropbox/android/activity/GalleryActivity;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/GalleryActivity;->d()V

    .line 367
    :cond_13
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p0, Lcom/dropbox/android/activity/al;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/dropbox/android/provider/z;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 368
    return-void

    .line 359
    :catch_24
    move-exception v0

    goto :goto_9
.end method
