.class final Lcom/dropbox/android/activity/as;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/S;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 273
    iput-object p1, p0, Lcom/dropbox/android/activity/as;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/provider/P;Landroid/database/Cursor;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 282
    sget-object v0, Lcom/dropbox/android/activity/au;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/provider/P;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_30

    .line 298
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected item type in this fragement"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :pswitch_13
    invoke-static {p2}, Lcom/dropbox/android/provider/K;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 285
    iget-boolean v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v1, :cond_23

    .line 286
    iget-object v1, p0, Lcom/dropbox/android/activity/as;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->c(Ljava/lang/String;)V

    .line 300
    :goto_22
    :pswitch_22
    return-void

    .line 288
    :cond_23
    iget-object v1, p0, Lcom/dropbox/android/activity/as;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_22

    .line 292
    :pswitch_29
    iget-object v0, p0, Lcom/dropbox/android/activity/as;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->h()V

    goto :goto_22

    .line 282
    nop

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_13
        :pswitch_29
        :pswitch_22
    .end packed-switch
.end method

.method public final a(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 276
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, p2}, Landroid/view/ViewParent;->showContextMenuForChild(Landroid/view/View;)Z

    .line 277
    const/4 v0, 0x1

    return v0
.end method
