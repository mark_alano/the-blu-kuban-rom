.class public Lcom/dropbox/android/activity/auth/DropboxAuth;
.super Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Z


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/Button;

.field private j:Landroid/widget/Button;

.field private k:Landroid/webkit/WebView;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 53
    const-class v0, Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->b:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;-><init>()V

    .line 404
    return-void
.end method

.method private static a(Landroid/content/Intent;Ldbxyzptlk/r/o;)Landroid/content/Intent;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 339
    const-string v0, "ACCESS_TOKEN"

    iget-object v1, p1, Ldbxyzptlk/r/o;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 340
    const-string v0, "ACCESS_SECRET"

    iget-object v1, p1, Ldbxyzptlk/r/o;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    const-string v0, "UID"

    iget-wide v1, p1, Ldbxyzptlk/r/o;->a:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    return-object p0
.end method

.method private static a(Ljava/lang/String;)Landroid/content/Intent;
    .registers 3
    .parameter

    .prologue
    .line 321
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 324
    const-class v1, Lcom/dropbox/client2/android/AuthActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 326
    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->l:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/activity/auth/DropboxAuth;Ldbxyzptlk/r/o;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Ldbxyzptlk/r/o;)V

    return-void
.end method

.method private a(Ldbxyzptlk/r/o;)V
    .registers 3
    .parameter

    .prologue
    .line 298
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->f:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 299
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 300
    invoke-static {v0, p1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Landroid/content/Intent;Ldbxyzptlk/r/o;)Landroid/content/Intent;

    move-result-object v0

    .line 302
    :try_start_e
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->startActivity(Landroid/content/Intent;)V

    .line 303
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->finish()V
    :try_end_14
    .catch Landroid/content/ActivityNotFoundException; {:try_start_e .. :try_end_14} :catch_15

    .line 318
    :goto_14
    return-void

    .line 305
    :catch_15
    move-exception v0

    .line 310
    :cond_16
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->k()Landroid/content/Intent;

    move-result-object v0

    .line 311
    invoke-static {v0, p1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Landroid/content/Intent;Ldbxyzptlk/r/o;)Landroid/content/Intent;

    move-result-object v0

    .line 313
    :try_start_1e
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->startActivity(Landroid/content/Intent;)V
    :try_end_21
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1e .. :try_end_21} :catch_25

    .line 317
    :goto_21
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->finish()V

    goto :goto_14

    .line 314
    :catch_25
    move-exception v0

    goto :goto_21
.end method

.method static synthetic b(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->n:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->m:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->f()V

    return-void
.end method

.method public static d()Z
    .registers 1

    .prologue
    .line 231
    sget-boolean v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->b:Z

    return v0
.end method

.method private e()Ljava/lang/String;
    .registers 5

    .prologue
    .line 235
    const-string v1, "/app_info"

    .line 236
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "k"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->c:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "s"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    iget-object v3, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->d:Ljava/lang/String;

    aput-object v3, v2, v0

    .line 240
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 241
    iget-object v0, v0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v0}, Ldbxyzptlk/r/i;->a()Ldbxyzptlk/q/m;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/r/E;

    invoke-virtual {v0}, Ldbxyzptlk/r/E;->i()Ljava/lang/String;

    move-result-object v0

    const-string v3, "r2"

    invoke-static {v0, v3, v1, v2}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->g()V

    return-void
.end method

.method private f()V
    .registers 3

    .prologue
    .line 247
    new-instance v0, Lcom/dropbox/android/activity/auth/j;

    invoke-direct {v0, p0, p0}, Lcom/dropbox/android/activity/auth/j;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;Landroid/content/Context;)V

    .line 248
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/j;->a(I)V

    .line 249
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/j;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 250
    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->h()V

    return-void
.end method

.method static synthetic g(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/webkit/WebView;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->k:Landroid/webkit/WebView;

    return-object v0
.end method

.method private g()V
    .registers 1

    .prologue
    .line 253
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->j()V

    .line 254
    return-void
.end method

.method private h()V
    .registers 6

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 257
    new-instance v0, Lcom/dropbox/android/activity/auth/i;

    invoke-direct {v0, p0, p0}, Lcom/dropbox/android/activity/auth/i;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;Landroid/content/Context;)V

    .line 258
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/i;->a(I)V

    .line 259
    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/i;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 261
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->n:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->m:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->l:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 264
    return-void
.end method

.method static synthetic h(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->i()V

    return-void
.end method

.method static synthetic i(Lcom/dropbox/android/activity/auth/DropboxAuth;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->c:Ljava/lang/String;

    return-object v0
.end method

.method private i()V
    .registers 4

    .prologue
    .line 271
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 272
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 273
    const v1, 0x7f0b018f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 274
    const v1, 0x7f0b0190

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 275
    const v1, 0x7f0b0019

    new-instance v2, Lcom/dropbox/android/activity/auth/g;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/auth/g;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 282
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 283
    return-void
.end method

.method private j()V
    .registers 4

    .prologue
    .line 286
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "db-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/error"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 289
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 290
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 292
    :try_start_39
    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->startActivity(Landroid/content/Intent;)V
    :try_end_3c
    .catch Landroid/content/ActivityNotFoundException; {:try_start_39 .. :try_end_3c} :catch_40

    .line 294
    :goto_3c
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->finish()V

    .line 295
    return-void

    .line 293
    :catch_40
    move-exception v0

    goto :goto_3c
.end method

.method static synthetic j(Lcom/dropbox/android/activity/auth/DropboxAuth;)V
    .registers 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->m()V

    return-void
.end method

.method private k()Landroid/content/Intent;
    .registers 4

    .prologue
    .line 330
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 331
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "db-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/connect"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 333
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 334
    const/high16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 335
    return-object v0
.end method

.method private m()V
    .registers 4

    .prologue
    .line 346
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 347
    const v1, 0x7f0b018f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 348
    const v1, 0x7f0b0195

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 349
    const v1, 0x7f0b0019

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 350
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 351
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 74
    invoke-super {p0, p1}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v0

    if-nez v0, :cond_26

    .line 79
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/LoginOrNewAcctActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 80
    const-string v1, "com.dropbox.activity.extra.NEXT_INTENT"

    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 83
    invoke-virtual {p0, v0, v3}, Lcom/dropbox/android/activity/auth/DropboxAuth;->startActivityForResult(Landroid/content/Intent;I)V

    .line 85
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->finish()V

    .line 203
    :goto_25
    return-void

    .line 89
    :cond_26
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getSupportActionBar()Lcom/actionbarsherlock/app/ActionBar;

    move-result-object v0

    const v1, 0x7f020172

    invoke-virtual {v0, v1}, Lcom/actionbarsherlock/app/ActionBar;->setIcon(I)V

    .line 91
    const v0, 0x7f030014

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->setContentView(I)V

    .line 93
    sput-boolean v4, Lcom/dropbox/android/activity/auth/DropboxAuth;->b:Z

    .line 95
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 96
    const-string v1, "CONSUMER_KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->c:Ljava/lang/String;

    .line 97
    const-string v1, "CONSUMER_SIG"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->d:Ljava/lang/String;

    .line 98
    const-string v1, "CALLING_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->f:Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->c:Ljava/lang/String;

    if-eqz v0, :cond_5c

    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->d:Ljava/lang/String;

    if-nez v0, :cond_67

    .line 101
    :cond_5c
    sget-object v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    const-string v1, "App trying to authenticate without setting a consumer key or sig."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->i()V

    goto :goto_25

    .line 106
    :cond_67
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "db-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/test"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 111
    invoke-virtual {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_b4

    .line 113
    sget-object v0, Lcom/dropbox/android/activity/auth/DropboxAuth;->a:Ljava/lang/String;

    const-string v1, "App trying to authenticate has not set up proper intent filter."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->i()V

    goto/16 :goto_25

    .line 118
    :cond_b4
    const v0, 0x7f06002b

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->g:Landroid/widget/Button;

    .line 119
    const v0, 0x7f06002a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->h:Landroid/widget/Button;

    .line 120
    const v0, 0x7f06002d

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->i:Landroid/widget/Button;

    .line 121
    const v0, 0x7f06002e

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->j:Landroid/widget/Button;

    .line 122
    const v0, 0x7f060029

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->k:Landroid/webkit/WebView;

    .line 123
    const v0, 0x7f060027

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->l:Landroid/view/View;

    .line 124
    const v0, 0x7f060028

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->m:Landroid/view/View;

    .line 125
    const v0, 0x7f06002c

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->n:Landroid/view/View;

    .line 127
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->k:Landroid/webkit/WebView;

    new-instance v1, Lcom/dropbox/android/activity/auth/a;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/a;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 168
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->k:Landroid/webkit/WebView;

    new-instance v1, Lcom/dropbox/android/activity/auth/b;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/b;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->g:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/auth/c;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/c;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->h:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/auth/d;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/d;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->i:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/auth/e;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/e;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/DropboxAuth;->j:Landroid/widget/Button;

    new-instance v1, Lcom/dropbox/android/activity/auth/f;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/auth/f;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_25
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 217
    packed-switch p1, :pswitch_data_1c

    .line 226
    const/4 v0, 0x0

    :goto_5
    return-object v0

    .line 219
    :pswitch_6
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 220
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 221
    const v1, 0x7f0b0191

    invoke-virtual {p0, v1}, Lcom/dropbox/android/activity/auth/DropboxAuth;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 222
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 223
    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_5

    .line 217
    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch
.end method

.method protected onResume()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    .line 207
    invoke-super {p0}, Lcom/dropbox/android/activity/lock/LockableBetterDefaultActivity;->onResume()V

    .line 209
    new-instance v0, Lcom/dropbox/android/activity/auth/i;

    invoke-direct {v0, p0, p0}, Lcom/dropbox/android/activity/auth/i;-><init>(Lcom/dropbox/android/activity/auth/DropboxAuth;Landroid/content/Context;)V

    .line 210
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/i;->a(I)V

    .line 211
    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/auth/i;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 212
    return-void
.end method
