.class final Lcom/dropbox/android/activity/auth/i;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/auth/DropboxAuth;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/auth/DropboxAuth;Landroid/content/Context;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 362
    iput-object p1, p0, Lcom/dropbox/android/activity/auth/i;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    .line 363
    invoke-direct {p0, p2}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 364
    return-void
.end method


# virtual methods
.method protected final varargs a(Landroid/content/Context;[Ljava/lang/String;)Lcom/dropbox/android/activity/auth/h;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 371
    aget-object v0, p2, v5

    .line 372
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v1}, Ldbxyzptlk/r/i;->a()Ldbxyzptlk/q/m;

    move-result-object v1

    .line 373
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 374
    invoke-interface {v1, v2}, Ldbxyzptlk/q/m;->a(Lorg/apache/http/HttpRequest;)V

    .line 375
    invoke-static {v1, v2}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/q/m;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 376
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 378
    new-instance v2, Lcom/dropbox/android/activity/auth/h;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/dropbox/android/activity/auth/h;-><init>(Lcom/dropbox/android/activity/auth/a;)V

    .line 379
    iput-object v0, v2, Lcom/dropbox/android/activity/auth/h;->a:Ljava/lang/String;

    .line 380
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/B/d;->b(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/dropbox/android/activity/auth/h;->b:Ljava/lang/String;

    .line 381
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    const-string v3, ";"

    const/4 v4, 0x2

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v5

    iput-object v0, v2, Lcom/dropbox/android/activity/auth/h;->c:Ljava/lang/String;

    .line 382
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/dropbox/android/activity/auth/h;->d:Ljava/lang/String;

    .line 383
    return-object v2
.end method

.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 360
    check-cast p2, [Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/auth/i;->a(Landroid/content/Context;[Ljava/lang/String;)Lcom/dropbox/android/activity/auth/h;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Lcom/dropbox/android/activity/auth/h;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 388
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/i;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->g(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/webkit/WebView;

    move-result-object v0

    iget-object v1, p2, Lcom/dropbox/android/activity/auth/h;->a:Ljava/lang/String;

    iget-object v2, p2, Lcom/dropbox/android/activity/auth/h;->b:Ljava/lang/String;

    iget-object v3, p2, Lcom/dropbox/android/activity/auth/h;->c:Ljava/lang/String;

    iget-object v4, p2, Lcom/dropbox/android/activity/auth/h;->d:Ljava/lang/String;

    iget-object v5, p2, Lcom/dropbox/android/activity/auth/h;->a:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x8

    .line 393
    instance-of v0, p2, Ldbxyzptlk/o/i;

    if-eqz v0, :cond_14

    check-cast p2, Ldbxyzptlk/o/i;

    iget v0, p2, Ldbxyzptlk/o/i;->b:I

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_14

    .line 394
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/i;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->h(Lcom/dropbox/android/activity/auth/DropboxAuth;)V

    .line 400
    :goto_13
    return-void

    .line 396
    :cond_14
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/i;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->a(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 397
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/i;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->c(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 398
    iget-object v0, p0, Lcom/dropbox/android/activity/auth/i;->a:Lcom/dropbox/android/activity/auth/DropboxAuth;

    invoke-static {v0}, Lcom/dropbox/android/activity/auth/DropboxAuth;->b(Lcom/dropbox/android/activity/auth/DropboxAuth;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_13
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 360
    check-cast p2, Lcom/dropbox/android/activity/auth/h;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/auth/i;->a(Landroid/content/Context;Lcom/dropbox/android/activity/auth/h;)V

    return-void
.end method
