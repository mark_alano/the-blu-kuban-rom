.class final Lcom/dropbox/android/activity/aw;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

.field private b:Z

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method private constructor <init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 535
    iput-object p1, p0, Lcom/dropbox/android/activity/aw;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537
    iput-boolean v0, p0, Lcom/dropbox/android/activity/aw;->b:Z

    .line 538
    iput v0, p0, Lcom/dropbox/android/activity/aw;->c:I

    .line 539
    iput v0, p0, Lcom/dropbox/android/activity/aw;->d:I

    .line 540
    iput v0, p0, Lcom/dropbox/android/activity/aw;->e:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;Lcom/dropbox/android/activity/as;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 535
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/aw;-><init>(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;)V

    return-void
.end method

.method private a()V
    .registers 5

    .prologue
    .line 557
    iget-object v0, p0, Lcom/dropbox/android/activity/aw;->a:Lcom/dropbox/android/activity/HierarchicalBrowserFragment;

    iget v1, p0, Lcom/dropbox/android/activity/aw;->c:I

    iget v2, p0, Lcom/dropbox/android/activity/aw;->d:I

    iget v3, p0, Lcom/dropbox/android/activity/aw;->e:I

    invoke-static {v0, v1, v2, v3}, Lcom/dropbox/android/activity/HierarchicalBrowserFragment;->a(Lcom/dropbox/android/activity/HierarchicalBrowserFragment;III)V

    .line 558
    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 545
    iget v0, p0, Lcom/dropbox/android/activity/aw;->c:I

    if-ne p2, v0, :cond_c

    iget v0, p0, Lcom/dropbox/android/activity/aw;->d:I

    if-ne p3, v0, :cond_c

    iget v0, p0, Lcom/dropbox/android/activity/aw;->e:I

    if-eq p4, v0, :cond_1d

    :cond_c
    const/4 v0, 0x1

    .line 547
    :goto_d
    iput p2, p0, Lcom/dropbox/android/activity/aw;->c:I

    .line 548
    iput p3, p0, Lcom/dropbox/android/activity/aw;->d:I

    .line 549
    iput p4, p0, Lcom/dropbox/android/activity/aw;->e:I

    .line 551
    iget-boolean v1, p0, Lcom/dropbox/android/activity/aw;->b:Z

    if-nez v1, :cond_1c

    if-eqz v0, :cond_1c

    .line 552
    invoke-direct {p0}, Lcom/dropbox/android/activity/aw;->a()V

    .line 554
    :cond_1c
    return-void

    .line 545
    :cond_1d
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 562
    iget-boolean v1, p0, Lcom/dropbox/android/activity/aw;->b:Z

    .line 563
    const/4 v0, 0x2

    if-ne p2, v0, :cond_12

    const/4 v0, 0x1

    :goto_6
    iput-boolean v0, p0, Lcom/dropbox/android/activity/aw;->b:Z

    .line 564
    if-eqz v1, :cond_11

    iget-boolean v0, p0, Lcom/dropbox/android/activity/aw;->b:Z

    if-nez v0, :cond_11

    .line 566
    invoke-direct {p0}, Lcom/dropbox/android/activity/aw;->a()V

    .line 568
    :cond_11
    return-void

    .line 563
    :cond_12
    const/4 v0, 0x0

    goto :goto_6
.end method
