.class public final Lcom/dropbox/android/activity/base/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final e:Lcom/dropbox/android/activity/base/f;


# instance fields
.field private final a:Lcom/dropbox/android/activity/base/e;

.field private final b:Landroid/app/Activity;

.field private c:Z

.field private d:Z

.field private final f:Ljava/util/Queue;

.field private g:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 32
    new-instance v0, Lcom/dropbox/android/activity/base/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/dropbox/android/activity/base/f;-><init>(Lcom/dropbox/android/activity/base/b;)V

    sput-object v0, Lcom/dropbox/android/activity/base/a;->e:Lcom/dropbox/android/activity/base/f;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/activity/base/e;)V
    .registers 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->c:Z

    .line 30
    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->d:Z

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/a;->f:Ljava/util/Queue;

    .line 84
    new-instance v0, Lcom/dropbox/android/activity/base/b;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/base/b;-><init>(Lcom/dropbox/android/activity/base/a;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/base/a;->g:Landroid/content/BroadcastReceiver;

    .line 105
    iput-object p1, p0, Lcom/dropbox/android/activity/base/a;->a:Lcom/dropbox/android/activity/base/e;

    .line 106
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->a:Lcom/dropbox/android/activity/base/e;

    invoke-interface {v0}, Lcom/dropbox/android/activity/base/e;->l()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    .line 107
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/base/a;)Lcom/dropbox/android/activity/base/e;
    .registers 2
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->a:Lcom/dropbox/android/activity/base/e;

    return-object v0
.end method

.method private a(I)V
    .registers 6
    .parameter

    .prologue
    .line 131
    const-string v0, "network_state"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "count"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    .line 132
    new-instance v1, Lcom/dropbox/android/activity/base/d;

    invoke-direct {v1, p0, v0}, Lcom/dropbox/android/activity/base/d;-><init>(Lcom/dropbox/android/activity/base/a;Lcom/dropbox/android/util/s;)V

    invoke-virtual {v1}, Lcom/dropbox/android/activity/base/d;->start()V

    .line 138
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/activity/base/a;)Landroid/app/Activity;
    .registers 2
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .registers 4

    .prologue
    .line 110
    const-string v0, "create"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 111
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0}, Ldbxyzptlk/i/a;->a(Landroid/content/Context;)Ldbxyzptlk/i/a;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Ldbxyzptlk/i/a;->a(Landroid/app/Activity;)V

    .line 112
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.dropbox.android.filemanager.ApiManager.ACTION_UNLINKED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 113
    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v1}, Ldbxyzptlk/a/g;->a(Landroid/content/Context;)Ldbxyzptlk/a/g;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/base/a;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Ldbxyzptlk/a/g;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 114
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)Z
    .registers 4
    .parameter

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/dropbox/android/activity/base/a;->h()Z

    move-result v0

    .line 198
    if-eqz v0, :cond_a

    .line 199
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 203
    :goto_9
    return v0

    .line 201
    :cond_a
    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->f:Ljava/util/Queue;

    invoke-interface {v1, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_9
.end method

.method public final b()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    .line 117
    sget-object v0, Lcom/dropbox/android/activity/base/a;->e:Lcom/dropbox/android/activity/base/f;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/base/f;->b()I

    move-result v0

    .line 118
    const-string v1, "start"

    iget-object v2, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v1, v2}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "count"

    int-to-long v3, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v1

    .line 119
    if-ne v0, v5, :cond_23

    .line 120
    const-string v2, "time.in.background.ms"

    sget-object v3, Lcom/dropbox/android/activity/base/a;->e:Lcom/dropbox/android/activity/base/f;

    invoke-virtual {v3}, Lcom/dropbox/android/activity/base/f;->c()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 122
    :cond_23
    invoke-virtual {v1}, Lcom/dropbox/android/util/s;->c()V

    .line 123
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/base/a;->a(I)V

    .line 124
    iput-boolean v5, p0, Lcom/dropbox/android/activity/base/a;->c:Z

    .line 126
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    sget-object v1, Ldbxyzptlk/m/e;->a:Ldbxyzptlk/m/e;

    invoke-static {v0, v1}, Ldbxyzptlk/m/a;->a(Landroid/content/Context;Ldbxyzptlk/m/e;)V

    .line 127
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 141
    const-string v0, "resume"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0}, Ldbxyzptlk/i/a;->a(Landroid/content/Context;)Ldbxyzptlk/i/a;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Ldbxyzptlk/i/a;->c(Landroid/app/Activity;)V

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->d:Z

    .line 145
    :goto_19
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2d

    .line 146
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_19

    .line 151
    :cond_2d
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->a:Lcom/dropbox/android/activity/base/e;

    invoke-interface {v0}, Lcom/dropbox/android/activity/base/e;->a()Z

    move-result v0

    if-nez v0, :cond_44

    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->c()Z

    move-result v0

    if-nez v0, :cond_44

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 154
    :cond_44
    return-void
.end method

.method public final d()V
    .registers 5

    .prologue
    .line 157
    const-string v0, "stop"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "count"

    sget-object v2, Lcom/dropbox/android/activity/base/a;->e:Lcom/dropbox/android/activity/base/f;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/base/f;->a()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->c:Z

    .line 159
    return-void
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 162
    const-string v0, "pause"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->d:Z

    .line 164
    return-void
.end method

.method public final f()V
    .registers 3

    .prologue
    .line 167
    const-string v0, "destroy"

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 168
    invoke-static {}, Lcom/dropbox/android/util/i;->am()V

    .line 169
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0}, Ldbxyzptlk/i/a;->a(Landroid/content/Context;)Ldbxyzptlk/i/a;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Ldbxyzptlk/i/a;->b(Landroid/app/Activity;)V

    .line 170
    iget-object v0, p0, Lcom/dropbox/android/activity/base/a;->b:Landroid/app/Activity;

    invoke-static {v0}, Ldbxyzptlk/a/g;->a(Landroid/content/Context;)Ldbxyzptlk/a/g;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/base/a;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Ldbxyzptlk/a/g;->a(Landroid/content/BroadcastReceiver;)V

    .line 171
    return-void
.end method

.method public final g()Z
    .registers 2

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->c:Z

    return v0
.end method

.method protected final h()Z
    .registers 2

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/dropbox/android/activity/base/a;->d:Z

    return v0
.end method
