.class final Lcom/dropbox/android/activity/bm;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Ldbxyzptlk/l/m;

.field final synthetic b:Lcom/dropbox/android/activity/PrefsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/l/m;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 105
    iput-object p1, p0, Lcom/dropbox/android/activity/bm;->b:Lcom/dropbox/android/activity/PrefsActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/bm;->a:Ldbxyzptlk/l/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 108
    iget-object v0, p0, Lcom/dropbox/android/activity/bm;->a:Ldbxyzptlk/l/m;

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->g()Z

    move-result v0

    if-eqz v0, :cond_31

    .line 110
    iget-object v0, p0, Lcom/dropbox/android/activity/bm;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-static {v3, v0}, Lcom/dropbox/android/service/CameraUploadService;->a(ZLandroid/content/Context;)V

    .line 111
    invoke-static {}, Lcom/dropbox/android/util/i;->V()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 112
    iget-object v0, p0, Lcom/dropbox/android/activity/bm;->b:Lcom/dropbox/android/activity/PrefsActivity;

    iget-object v1, p0, Lcom/dropbox/android/activity/bm;->a:Ldbxyzptlk/l/m;

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->a(Lcom/dropbox/android/activity/PrefsActivity;Ldbxyzptlk/l/m;)V

    .line 119
    :goto_1d
    invoke-static {}, Lcom/dropbox/android/util/i;->j()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "cameraupload.enabled"

    iget-object v2, p0, Lcom/dropbox/android/activity/bm;->a:Ldbxyzptlk/l/m;

    invoke-virtual {v2}, Ldbxyzptlk/l/m;->g()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 120
    return v4

    .line 115
    :cond_31
    iget-object v0, p0, Lcom/dropbox/android/activity/bm;->b:Lcom/dropbox/android/activity/PrefsActivity;

    new-array v1, v4, [Lcom/dropbox/android/activity/bT;

    sget-object v2, Lcom/dropbox/android/activity/bT;->e:Lcom/dropbox/android/activity/bT;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/dropbox/android/activity/TourActivity;->a(Landroid/content/Context;[Lcom/dropbox/android/activity/bT;)Landroid/content/Intent;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/dropbox/android/activity/bm;->b:Lcom/dropbox/android/activity/PrefsActivity;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/PrefsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1d
.end method
