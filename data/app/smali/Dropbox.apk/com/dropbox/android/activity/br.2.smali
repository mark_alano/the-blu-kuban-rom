.class final Lcom/dropbox/android/activity/br;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Landroid/widget/EditText;

.field final synthetic e:Lcom/dropbox/android/activity/PrefsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/PrefsActivity;Landroid/widget/Button;Landroid/view/View;Landroid/view/View;Landroid/widget/EditText;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 478
    iput-object p1, p0, Lcom/dropbox/android/activity/br;->e:Lcom/dropbox/android/activity/PrefsActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/br;->a:Landroid/widget/Button;

    iput-object p3, p0, Lcom/dropbox/android/activity/br;->b:Landroid/view/View;

    iput-object p4, p0, Lcom/dropbox/android/activity/br;->c:Landroid/view/View;

    iput-object p5, p0, Lcom/dropbox/android/activity/br;->d:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 10
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 481
    iget-object v0, p0, Lcom/dropbox/android/activity/br;->a:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    .line 482
    iget-object v0, p0, Lcom/dropbox/android/activity/br;->b:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 483
    iget-object v0, p0, Lcom/dropbox/android/activity/br;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 484
    iget-object v0, p0, Lcom/dropbox/android/activity/br;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 485
    new-instance v0, Lcom/dropbox/android/activity/bs;

    iget-object v1, p0, Lcom/dropbox/android/activity/br;->e:Lcom/dropbox/android/activity/PrefsActivity;

    iget-object v2, p0, Lcom/dropbox/android/activity/br;->e:Lcom/dropbox/android/activity/PrefsActivity;

    iget-object v3, p0, Lcom/dropbox/android/activity/br;->a:Landroid/widget/Button;

    iget-object v4, p0, Lcom/dropbox/android/activity/br;->b:Landroid/view/View;

    iget-object v5, p0, Lcom/dropbox/android/activity/br;->c:Landroid/view/View;

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/activity/bs;-><init>(Lcom/dropbox/android/activity/PrefsActivity;Landroid/content/Context;Landroid/widget/Button;Landroid/view/View;Landroid/view/View;)V

    .line 486
    invoke-virtual {v0}, Lcom/dropbox/android/activity/bs;->f()V

    .line 487
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    aput-object v6, v1, v7

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/bs;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 488
    invoke-static {}, Lcom/dropbox/android/util/i;->ad()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 489
    return-void
.end method
