.class final Lcom/dropbox/android/activity/bs;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Lcom/dropbox/android/activity/PrefsActivity;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/activity/PrefsActivity;Landroid/content/Context;Landroid/widget/Button;Landroid/view/View;Landroid/view/View;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 436
    iput-object p1, p0, Lcom/dropbox/android/activity/bs;->d:Lcom/dropbox/android/activity/PrefsActivity;

    iput-object p3, p0, Lcom/dropbox/android/activity/bs;->a:Landroid/widget/Button;

    iput-object p4, p0, Lcom/dropbox/android/activity/bs;->b:Landroid/view/View;

    iput-object p5, p0, Lcom/dropbox/android/activity/bs;->c:Landroid/view/View;

    .line 437
    invoke-direct {p0, p2}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 438
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 435
    check-cast p2, [Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/bs;->a(Landroid/content/Context;[Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs a(Landroid/content/Context;[Ljava/lang/String;)Ljava/lang/Void;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 442
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v4

    .line 443
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    const-string v1, "android"

    iget-object v2, v4, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    iget-object v3, v4, Ldbxyzptlk/j/g;->d:Ljava/lang/String;

    iget-object v4, v4, Ldbxyzptlk/j/g;->e:Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v5, p2, v5

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 456
    iget-object v0, p0, Lcom/dropbox/android/activity/bs;->a:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 457
    iget-object v0, p0, Lcom/dropbox/android/activity/bs;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 458
    iget-object v0, p0, Lcom/dropbox/android/activity/bs;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 459
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 435
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/bs;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 450
    iget-object v0, p0, Lcom/dropbox/android/activity/bs;->d:Lcom/dropbox/android/activity/PrefsActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/PrefsActivity;->dismissDialog(I)V

    .line 451
    const v0, 0x7f0b0141

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 452
    return-void
.end method
