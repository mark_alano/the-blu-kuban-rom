.class final Lcom/dropbox/android/activity/co;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/VideoPlayerActivity;

.field private b:I


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/VideoPlayerActivity;)V
    .registers 3
    .parameter

    .prologue
    .line 111
    iput-object p1, p0, Lcom/dropbox/android/activity/co;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/activity/co;->b:I

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 131
    iget-object v0, p0, Lcom/dropbox/android/activity/co;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->a(Lcom/dropbox/android/activity/VideoPlayerActivity;)Lcom/dropbox/android/widget/DbxVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->d()I

    move-result v0

    .line 134
    iget v1, p0, Lcom/dropbox/android/activity/co;->b:I

    if-le v0, v1, :cond_32

    iget v1, p0, Lcom/dropbox/android/activity/co;->b:I

    if-ltz v1, :cond_32

    iget-object v1, p0, Lcom/dropbox/android/activity/co;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->b(Lcom/dropbox/android/activity/VideoPlayerActivity;)I

    move-result v1

    if-eq v0, v1, :cond_32

    .line 135
    invoke-static {}, Lcom/dropbox/android/activity/VideoPlayerActivity;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "We are playing, hiding the spinner."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v0, p0, Lcom/dropbox/android/activity/co;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->c(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 137
    const/4 v0, -0x1

    iput v0, p0, Lcom/dropbox/android/activity/co;->b:I

    .line 143
    :goto_31
    return-void

    .line 140
    :cond_32
    iput v0, p0, Lcom/dropbox/android/activity/co;->b:I

    .line 141
    iget-object v0, p0, Lcom/dropbox/android/activity/co;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v0}, Lcom/dropbox/android/activity/VideoPlayerActivity;->e(Lcom/dropbox/android/activity/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/activity/co;->a:Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-static {v1}, Lcom/dropbox/android/activity/VideoPlayerActivity;->d(Lcom/dropbox/android/activity/VideoPlayerActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_31
.end method
