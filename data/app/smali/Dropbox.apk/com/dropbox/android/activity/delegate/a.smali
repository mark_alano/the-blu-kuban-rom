.class public final Lcom/dropbox/android/activity/delegate/a;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/activity/delegate/a;->a:Z

    .line 75
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Lcom/actionbarsherlock/view/MenuItem;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 93
    invoke-interface {p2}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v0

    invoke-static {v0}, Lcom/dropbox/android/activity/delegate/c;->a(I)Lcom/dropbox/android/activity/delegate/c;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_f

    .line 95
    invoke-virtual {v0, p1}, Lcom/dropbox/android/activity/delegate/c;->a(Landroid/app/Activity;)V

    .line 96
    const/4 v0, 0x1

    .line 98
    :goto_e
    return v0

    :cond_f
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final a(Lcom/actionbarsherlock/view/Menu;Z)Z
    .registers 8
    .parameter
    .parameter

    .prologue
    const/high16 v4, 0x1

    const/4 v3, 0x0

    .line 81
    sget-object v0, Lcom/dropbox/android/activity/delegate/c;->b:Lcom/dropbox/android/activity/delegate/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/delegate/c;->a()I

    move-result v0

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0084

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v4, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x1080040

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 84
    sget-object v0, Lcom/dropbox/android/activity/delegate/c;->a:Lcom/dropbox/android/activity/delegate/c;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/delegate/c;->a()I

    move-result v0

    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0083

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v4, v1}, Lcom/actionbarsherlock/view/Menu;->add(IIILjava/lang/CharSequence;)Lcom/actionbarsherlock/view/MenuItem;

    move-result-object v0

    const v1, 0x1080049

    invoke-interface {v0, v1}, Lcom/actionbarsherlock/view/MenuItem;->setIcon(I)Lcom/actionbarsherlock/view/MenuItem;

    .line 87
    const/4 v0, 0x1

    return v0
.end method
