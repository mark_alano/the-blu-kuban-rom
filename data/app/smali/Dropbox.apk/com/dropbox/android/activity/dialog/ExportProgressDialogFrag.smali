.class public final Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;
.super Lcom/dropbox/android/util/DialogFragmentBase;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:Ldbxyzptlk/g/h;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    .line 35
    return-void
.end method

.method private constructor <init>(Ldbxyzptlk/g/h;)V
    .registers 3
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/g/h;

    .line 26
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->setRetainInstance(Z)V

    .line 27
    return-void
.end method

.method public static a(Ldbxyzptlk/g/h;)Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;
    .registers 2
    .parameter

    .prologue
    .line 21
    new-instance v0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;-><init>(Ldbxyzptlk/g/h;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 81
    sget-object v0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .registers 3
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/g/h;

    invoke-virtual {v0}, Ldbxyzptlk/g/h;->c()V

    .line 66
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/g/h;

    if-nez v0, :cond_a

    .line 45
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->setShowsDialog(Z)V

    .line 46
    const/4 v0, 0x0

    .line 50
    :goto_9
    return-object v0

    .line 48
    :cond_a
    new-instance v0, Lcom/dropbox/android/widget/I;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/g/h;

    invoke-virtual {v2}, Ldbxyzptlk/g/h;->b()Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/widget/I;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 49
    iget-object v1, p0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->b:Ldbxyzptlk/g/h;

    invoke-virtual {v1, v0}, Ldbxyzptlk/g/h;->a(Lcom/dropbox/android/widget/I;)V

    goto :goto_9
.end method

.method public final onDestroyView()V
    .registers 3

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 57
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 60
    :cond_14
    invoke-super {p0}, Lcom/dropbox/android/util/DialogFragmentBase;->onDestroyView()V

    .line 61
    return-void
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 76
    sget-object v0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 70
    sget-object v0, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 72
    return-void
.end method
