.class public Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;
.super Lcom/dropbox/android/util/DialogFragmentBase;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field b:Ldbxyzptlk/g/x;

.field private c:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Landroid/text/TextWatcher;

    .line 58
    new-instance v0, Lcom/dropbox/android/activity/dialog/c;

    invoke-direct {v0, p0}, Lcom/dropbox/android/activity/dialog/c;-><init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->b:Ldbxyzptlk/g/x;

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Ldbxyzptlk/g/y;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a(Ldbxyzptlk/g/y;)I

    move-result v0

    return v0
.end method

.method private a(Ldbxyzptlk/g/y;)I
    .registers 4
    .parameter

    .prologue
    .line 47
    sget-object v0, Lcom/dropbox/android/activity/dialog/g;->a:[I

    invoke-virtual {p1}, Ldbxyzptlk/g/y;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_18

    .line 54
    const v0, 0x7f0b00a0

    :goto_e
    return v0

    .line 49
    :pswitch_f
    const v0, 0x7f0b009f

    goto :goto_e

    .line 51
    :pswitch_13
    const v0, 0x7f0b009e

    goto :goto_e

    .line 47
    nop

    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_f
        :pswitch_13
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;
    .registers 3
    .parameter

    .prologue
    .line 39
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 40
    const-string v1, "ARG_PARENT_DIR"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    new-instance v1, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-direct {v1}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;-><init>()V

    .line 42
    invoke-virtual {v1, v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 43
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 115
    sget-object v0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 125
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 126
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 127
    const v1, 0x7f0b009b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 128
    const v1, 0x7f0b009c

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 129
    const v1, 0x7f0b0018

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 130
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030049

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 131
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onStart()V
    .registers 10

    .prologue
    const/4 v8, 0x4

    const/4 v7, -0x1

    .line 136
    invoke-super {p0}, Lcom/dropbox/android/util/DialogFragmentBase;->onStart()V

    .line 138
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_PARENT_DIR"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 139
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    check-cast v4, Landroid/app/AlertDialog;

    .line 142
    invoke-virtual {v4}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 143
    const v0, 0x7f0600d7

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 144
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Landroid/text/TextWatcher;

    if-eqz v0, :cond_32

    .line 145
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Landroid/text/TextWatcher;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Landroid/text/TextWatcher;

    .line 148
    :cond_32
    const v0, 0x7f0600d8

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ProgressBar;

    .line 149
    invoke-virtual {v5, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 150
    const v0, 0x7f0600d9

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 151
    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    new-instance v0, Lcom/dropbox/android/activity/dialog/d;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/activity/dialog/d;-><init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Landroid/widget/EditText;Ljava/lang/String;Landroid/app/AlertDialog;Landroid/widget/ProgressBar;Landroid/widget/TextView;)V

    .line 174
    invoke-virtual {v4}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0b0018

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/dropbox/android/activity/dialog/e;

    invoke-direct {v3, p0}, Lcom/dropbox/android/activity/dialog/e;-><init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;)V

    invoke-virtual {v4, v7, v1, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 185
    invoke-virtual {v4, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    invoke-virtual {v4, v7}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v2}, Landroid/widget/EditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_91

    const/4 v0, 0x1

    :goto_81
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 188
    new-instance v0, Lcom/dropbox/android/activity/dialog/f;

    invoke-direct {v0, p0, v4, v6}, Lcom/dropbox/android/activity/dialog/f;-><init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Landroid/app/AlertDialog;Landroid/widget/TextView;)V

    iput-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Landroid/text/TextWatcher;

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->c:Landroid/text/TextWatcher;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 212
    return-void

    .line 187
    :cond_91
    const/4 v0, 0x0

    goto :goto_81
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 110
    sget-object v0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 104
    sget-object v0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 106
    return-void
.end method
