.class public Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;
.super Lcom/dropbox/android/util/DialogFragmentBase;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    return-void
.end method

.method public static a()Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;
    .registers 1

    .prologue
    .line 19
    new-instance v0, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;-><init>()V

    return-object v0
.end method

.method public static b(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 51
    sget-object v0, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_12

    .line 53
    invoke-virtual {p0}, Landroid/support/v4/app/j;->a()Landroid/support/v4/app/u;

    move-result-object v1

    .line 54
    invoke-virtual {v1, v0}, Landroid/support/v4/app/u;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/u;

    .line 55
    invoke-virtual {v1}, Landroid/support/v4/app/u;->c()I

    .line 57
    :cond_12
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 35
    sget-object v0, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .parameter

    .prologue
    .line 44
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 45
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 46
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0b00a9

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 47
    return-object v0
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 30
    sget-object v0, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 24
    sget-object v0, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 26
    return-void
.end method
