.class public Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;
.super Lcom/dropbox/android/util/DialogFragmentBase;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 19
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;Z)Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 25
    new-instance v0, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;-><init>()V

    .line 26
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->setArguments(Landroid/os/Bundle;)V

    .line 27
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_LAUNCH_INTENT"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 28
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "ARG_UPDATE_REQUIRED"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 29
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 45
    sget-object v0, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_LAUNCH_INTENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 56
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 57
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_UPDATE_REQUIRED"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_41

    .line 58
    const v2, 0x7f0b0197

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 59
    const v2, 0x7f0b0199

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 61
    const v2, 0x7f0b019a

    new-instance v3, Lcom/dropbox/android/activity/dialog/n;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/dialog/n;-><init>(Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;Landroid/content/Intent;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 68
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 88
    :goto_3c
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 70
    :cond_41
    const v2, 0x7f0b0196

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 71
    const v2, 0x7f0b0198

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 73
    const v2, 0x7f0b019b

    new-instance v3, Lcom/dropbox/android/activity/dialog/o;

    invoke-direct {v3, p0, v0}, Lcom/dropbox/android/activity/dialog/o;-><init>(Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;Landroid/content/Intent;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    const v0, 0x7f0b019c

    new-instance v2, Lcom/dropbox/android/activity/dialog/p;

    invoke-direct {v2, p0}, Lcom/dropbox/android/activity/dialog/p;-><init>(Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_3c
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 40
    sget-object v0, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 34
    sget-object v0, Lcom/dropbox/android/activity/dialog/UpdateDialogFrag;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 36
    return-void
.end method
