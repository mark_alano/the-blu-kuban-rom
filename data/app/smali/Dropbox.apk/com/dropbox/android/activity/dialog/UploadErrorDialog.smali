.class public Lcom/dropbox/android/activity/dialog/UploadErrorDialog;
.super Lcom/dropbox/android/util/DialogFragmentBase;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_TAG"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/dropbox/android/util/DialogFragmentBase;-><init>()V

    .line 52
    return-void
.end method

.method private static a(ZLcom/dropbox/android/taskqueue/m;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 39
    sget-object v0, Lcom/dropbox/android/activity/dialog/s;->a:[I

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/m;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_38

    .line 52
    if-eqz p0, :cond_33

    const v0, 0x7f0b00cc

    :goto_10
    return v0

    .line 41
    :pswitch_11
    if-eqz p0, :cond_1b

    .line 42
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "We swallow camera upload storage exceptions and should not hit this."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_1b
    const v0, 0x7f0b00ca

    goto :goto_10

    .line 46
    :pswitch_1f
    if-eqz p0, :cond_25

    const v0, 0x7f0b00ce

    goto :goto_10

    :cond_25
    const v0, 0x7f0b00c9

    goto :goto_10

    .line 50
    :pswitch_29
    if-eqz p0, :cond_2f

    const v0, 0x7f0b00cd

    goto :goto_10

    :cond_2f
    const v0, 0x7f0b00c8

    goto :goto_10

    .line 52
    :cond_33
    const v0, 0x7f0b00c7

    goto :goto_10

    .line 39
    nop

    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_11
        :pswitch_1f
        :pswitch_29
        :pswitch_29
        :pswitch_29
    .end packed-switch
.end method

.method public static a(ZLcom/dropbox/android/taskqueue/m;Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/UploadErrorDialog;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 29
    new-instance v0, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;

    invoke-direct {v0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;-><init>()V

    .line 30
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 31
    const-string v2, "ARG_CAMERA_UPLOAD"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 32
    const-string v2, "ARG_STATUS"

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/m;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v2, "ARG_FILENAME"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->setArguments(Landroid/os/Bundle;)V

    .line 35
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/j;)V
    .registers 3
    .parameter

    .prologue
    .line 92
    sget-object v0, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 93
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 7
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 58
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ARG_CAMERA_UPLOAD"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 59
    const-class v0, Lcom/dropbox/android/taskqueue/m;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_STATUS"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dropbox/android/taskqueue/m;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/m;

    .line 60
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ARG_FILENAME"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-static {v1, v0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a(ZLcom/dropbox/android/taskqueue/m;)I

    move-result v0

    .line 63
    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v2, v3, v4

    invoke-static {v0, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 66
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 67
    if-eqz v1, :cond_67

    const v0, 0x7f0b00cb

    :goto_4e
    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 68
    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 69
    const v0, 0x7f0b0019

    new-instance v1, Lcom/dropbox/android/activity/dialog/r;

    invoke-direct {v1, p0}, Lcom/dropbox/android/activity/dialog/r;-><init>(Lcom/dropbox/android/activity/dialog/UploadErrorDialog;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 75
    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 76
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 67
    :cond_67
    const v0, 0x7f0b00c6

    goto :goto_4e
.end method

.method public final show(Landroid/support/v4/app/u;Ljava/lang/String;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 87
    sget-object v0, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/u;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final show(Landroid/support/v4/app/j;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 81
    sget-object v0, Lcom/dropbox/android/activity/dialog/UploadErrorDialog;->a:Ljava/lang/String;

    invoke-static {p2, v0}, Lcom/dropbox/android/util/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    invoke-super {p0, p1, p2}, Lcom/dropbox/android/util/DialogFragmentBase;->show(Landroid/support/v4/app/j;Ljava/lang/String;)V

    .line 83
    return-void
.end method
