.class final Lcom/dropbox/android/activity/dialog/c;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/g/x;


# instance fields
.field final synthetic a:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;)V
    .registers 2
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/c;->a:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/support/v4/app/DialogFragment;)Landroid/app/AlertDialog;
    .registers 3
    .parameter

    .prologue
    .line 61
    invoke-virtual {p1}, Landroid/support/v4/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    return-object v0
.end method

.method private a(Landroid/support/v4/app/FragmentActivity;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;
    .registers 4
    .parameter

    .prologue
    .line 65
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/FragmentActivity;Ldbxyzptlk/g/y;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/c;->a(Landroid/support/v4/app/FragmentActivity;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/dialog/c;->a(Landroid/support/v4/app/DialogFragment;)Landroid/app/AlertDialog;

    move-result-object v1

    .line 71
    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    .line 72
    const v0, 0x7f0600d8

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 73
    const v3, 0x7f0600d9

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 75
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 76
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/c;->a:Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    invoke-static {v0, p2}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->a(Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;Ldbxyzptlk/g/y;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 78
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f08002b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    return-void
.end method

.method public final a(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/dropbox/android/activity/dialog/c;->a(Landroid/support/v4/app/FragmentActivity;)Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;

    move-result-object v0

    .line 86
    invoke-direct {p0, v0}, Lcom/dropbox/android/activity/dialog/c;->a(Landroid/support/v4/app/DialogFragment;)Landroid/app/AlertDialog;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 90
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    instance-of v1, v1, Lcom/dropbox/android/activity/ax;

    if-eqz v1, :cond_1d

    .line 91
    invoke-virtual {v0}, Lcom/dropbox/android/activity/dialog/NewFolderDialogFrag;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/activity/ax;

    .line 97
    :goto_19
    invoke-interface {v0, p2}, Lcom/dropbox/android/activity/ax;->c(Ljava/lang/String;)V

    .line 98
    return-void

    .line 92
    :cond_1d
    instance-of v0, p1, Lcom/dropbox/android/activity/ax;

    if-eqz v0, :cond_25

    .line 93
    check-cast p1, Lcom/dropbox/android/activity/ax;

    move-object v0, p1

    goto :goto_19

    .line 95
    :cond_25
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Target fragment or activity must be of type DropboxHierarchicalDirBrowserInterface"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
