.class final Lcom/dropbox/android/activity/dialog/l;
.super Ldbxyzptlk/g/J;
.source "panda.py"


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/LocalEntry;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/dropbox/android/activity/dialog/k;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/dialog/k;Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 147
    iput-object p1, p0, Lcom/dropbox/android/activity/dialog/l;->c:Lcom/dropbox/android/activity/dialog/k;

    iput-object p5, p0, Lcom/dropbox/android/activity/dialog/l;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iput-object p6, p0, Lcom/dropbox/android/activity/dialog/l;->b:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4}, Ldbxyzptlk/g/J;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 157
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v1, p2, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v1

    .line 158
    invoke-static {p1, v1}, Lcom/dropbox/android/provider/z;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 161
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/l;->b:Ljava/lang/String;

    if-eqz v0, :cond_35

    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/l;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/dropbox/android/widget/quickactions/e;

    if-eqz v0, :cond_35

    .line 162
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    iget-object v2, p0, Lcom/dropbox/android/activity/dialog/l;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/support/v4/app/j;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/quickactions/e;

    .line 168
    :goto_31
    invoke-interface {v0, v1}, Lcom/dropbox/android/widget/quickactions/e;->a(Landroid/net/Uri;)V

    .line 169
    return-void

    .line 163
    :cond_35
    instance-of v0, p1, Lcom/dropbox/android/widget/quickactions/e;

    if-eqz v0, :cond_3d

    .line 164
    check-cast p1, Lcom/dropbox/android/widget/quickactions/e;

    move-object v0, p1

    goto :goto_31

    .line 166
    :cond_3d
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Target fragment or activity must be of type RenameCallback"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, Lcom/dropbox/android/activity/dialog/l;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_16

    const v0, 0x7f0b00a7

    .line 151
    :goto_9
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/dropbox/android/util/aZ;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 153
    return-void

    .line 150
    :cond_16
    const v0, 0x7f0b00a4

    goto :goto_9
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 147
    check-cast p2, Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/activity/dialog/l;->a(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    return-void
.end method
