.class final Lcom/dropbox/android/activity/k;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/T;

.field final synthetic b:I

.field final synthetic c:Lcom/dropbox/android/activity/j;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/j;Lcom/dropbox/android/filemanager/T;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 532
    iput-object p1, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/j;

    iput-object p2, p0, Lcom/dropbox/android/activity/k;->a:Lcom/dropbox/android/filemanager/T;

    iput p3, p0, Lcom/dropbox/android/activity/k;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 9

    .prologue
    const/4 v7, 0x1

    .line 535
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/j;

    iget-object v0, v0, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v0}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_17

    .line 537
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->a:Lcom/dropbox/android/filemanager/T;

    if-eqz v0, :cond_16

    .line 538
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->a:Lcom/dropbox/android/filemanager/T;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 558
    :cond_16
    :goto_16
    return-void

    .line 542
    :cond_17
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/j;

    invoke-static {v0}, Lcom/dropbox/android/activity/j;->a(Lcom/dropbox/android/activity/j;)Ljava/util/Map;

    move-result-object v0

    iget v1, p0, Lcom/dropbox/android/activity/k;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 543
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/j;

    invoke-static {v0}, Lcom/dropbox/android/activity/j;->b(Lcom/dropbox/android/activity/j;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v6

    .line 545
    if-ltz v6, :cond_16

    .line 551
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->a:Lcom/dropbox/android/filemanager/T;

    if-eqz v0, :cond_46

    .line 552
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/j;

    iget-object v2, p0, Lcom/dropbox/android/activity/k;->a:Lcom/dropbox/android/filemanager/T;

    invoke-static {v0, v1, v2, v6, v7}, Lcom/dropbox/android/activity/j;->a(Lcom/dropbox/android/activity/j;Ljava/lang/String;Lcom/dropbox/android/filemanager/T;IZ)V

    .line 557
    :goto_40
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/j;

    invoke-static {v0}, Lcom/dropbox/android/activity/j;->c(Lcom/dropbox/android/activity/j;)V

    goto :goto_16

    .line 554
    :cond_46
    iget-object v0, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/j;

    iget-object v2, p0, Lcom/dropbox/android/activity/k;->c:Lcom/dropbox/android/activity/j;

    iget-object v2, v2, Lcom/dropbox/android/activity/j;->c:Lcom/dropbox/android/activity/CameraUploadDetailsFragment;

    invoke-virtual {v2}, Lcom/dropbox/android/activity/CameraUploadDetailsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020182

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/activity/j;->a(Lcom/dropbox/android/activity/j;Ljava/lang/String;Landroid/graphics/drawable/Drawable;ZJIZ)V

    goto :goto_40
.end method
