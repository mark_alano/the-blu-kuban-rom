.class final Lcom/dropbox/android/activity/v;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:[Landroid/view/View;

.field final synthetic b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;


# direct methods
.method constructor <init>(Lcom/dropbox/android/activity/CameraUploadSettingsActivity;[Landroid/view/View;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 172
    iput-object p1, p0, Lcom/dropbox/android/activity/v;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    iput-object p2, p0, Lcom/dropbox/android/activity/v;->a:[Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 176
    iget-object v1, p0, Lcom/dropbox/android/activity/v;->a:[Landroid/view/View;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v2, :cond_e

    aget-object v3, v1, v0

    .line 177
    invoke-virtual {v3, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 176
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 179
    :cond_e
    iget-object v0, p0, Lcom/dropbox/android/activity/v;->b:Lcom/dropbox/android/activity/CameraUploadSettingsActivity;

    const v1, 0x7f06004e

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/CameraUploadSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p2, :cond_1f

    const/high16 v0, 0x3f80

    :goto_1b
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 180
    return-void

    .line 179
    :cond_1f
    const/high16 v0, 0x3f00

    goto :goto_1b
.end method
