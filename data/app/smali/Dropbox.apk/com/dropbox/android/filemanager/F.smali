.class public Lcom/dropbox/android/filemanager/F;
.super Lcom/dropbox/android/util/aL;
.source "panda.py"


# instance fields
.field private final t:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 27
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/util/aL;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    iput-object p3, p0, Lcom/dropbox/android/filemanager/F;->t:Ljava/lang/String;

    .line 29
    return-void
.end method

.method private c(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 5
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/dropbox/android/filemanager/F;->t:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/provider/O;->b(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/dropbox/android/provider/L;

    const-string v2, "_up_folder"

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/provider/L;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 69
    new-instance v1, Landroid/database/MergeCursor;

    invoke-direct {v1, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v1
.end method


# virtual methods
.method public final synthetic d()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/F;->f()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/database/Cursor;
    .registers 7

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/F;->j()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/filemanager/F;->g:Landroid/net/Uri;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/F;->h:[Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/F;->i:Ljava/lang/String;

    iget-object v4, p0, Lcom/dropbox/android/filemanager/F;->j:[Ljava/lang/String;

    iget-object v5, p0, Lcom/dropbox/android/filemanager/F;->k:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 36
    if-eqz v1, :cond_79

    .line 37
    new-instance v0, Ldbxyzptlk/k/h;

    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/x;->b()Ldbxyzptlk/k/g;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/k/h;-><init>(Ldbxyzptlk/k/g;Landroid/database/Cursor;)V

    .line 41
    :goto_28
    iget-object v1, p0, Lcom/dropbox/android/filemanager/F;->g:Landroid/net/Uri;

    invoke-static {v1}, Lcom/dropbox/android/provider/DropboxProvider;->a(Landroid/net/Uri;)Lcom/dropbox/android/provider/m;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/provider/m;->b:Lcom/dropbox/android/provider/m;

    if-ne v1, v2, :cond_69

    .line 42
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/F;->g:Landroid/net/Uri;

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Landroid/net/Uri;)V

    .line 43
    new-instance v2, Lcom/dropbox/android/provider/L;

    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/x;->k()Lcom/dropbox/android/taskqueue/H;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/dropbox/android/taskqueue/H;->a(Lcom/dropbox/android/util/DropboxPath;)Landroid/database/Cursor;

    move-result-object v1

    const-string v3, "_upload_in_progress"

    invoke-direct {v2, v1, v3}, Lcom/dropbox/android/provider/L;-><init>(Landroid/database/Cursor;Ljava/lang/String;)V

    .line 46
    new-instance v1, Lcom/dropbox/android/provider/H;

    new-instance v3, Lcom/dropbox/android/provider/M;

    invoke-direct {v3}, Lcom/dropbox/android/provider/M;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/provider/H;-><init>(Landroid/database/Cursor;Ljava/util/Comparator;)V

    .line 48
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/database/Cursor;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object v1, v2, v0

    .line 49
    new-instance v0, Lcom/dropbox/android/provider/t;

    new-instance v1, Lcom/dropbox/android/provider/n;

    invoke-direct {v1}, Lcom/dropbox/android/provider/n;-><init>()V

    invoke-direct {v0, v2, v1}, Lcom/dropbox/android/provider/t;-><init>([Landroid/database/Cursor;Ljava/util/Comparator;)V

    .line 53
    :cond_69
    iget-object v1, p0, Lcom/dropbox/android/filemanager/F;->t:Ljava/lang/String;

    if-eqz v1, :cond_71

    .line 54
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/F;->c(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 57
    :cond_71
    if-eqz v0, :cond_78

    .line 58
    iget-object v1, p0, Lcom/dropbox/android/filemanager/F;->f:Ldbxyzptlk/a/e;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/filemanager/F;->a(Landroid/database/Cursor;Landroid/database/ContentObserver;)V

    .line 61
    :cond_78
    return-object v0

    :cond_79
    move-object v0, v1

    goto :goto_28
.end method
