.class public abstract enum Lcom/dropbox/android/filemanager/W;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/filemanager/W;

.field public static final enum b:Lcom/dropbox/android/filemanager/W;

.field public static final enum c:Lcom/dropbox/android/filemanager/W;

.field public static final enum d:Lcom/dropbox/android/filemanager/W;

.field public static final enum e:Lcom/dropbox/android/filemanager/W;

.field private static final synthetic f:[Lcom/dropbox/android/filemanager/W;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lcom/dropbox/android/filemanager/X;

    const-string v1, "NO_REMOTE_DATA"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/filemanager/X;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/W;->a:Lcom/dropbox/android/filemanager/W;

    .line 17
    new-instance v0, Lcom/dropbox/android/filemanager/Y;

    const-string v1, "METADATA_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/filemanager/Y;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/W;->b:Lcom/dropbox/android/filemanager/W;

    .line 29
    new-instance v0, Lcom/dropbox/android/filemanager/Z;

    const-string v1, "METADATA_AND_UPLOAD"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/filemanager/Z;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/W;->c:Lcom/dropbox/android/filemanager/W;

    .line 42
    new-instance v0, Lcom/dropbox/android/filemanager/aa;

    const-string v1, "METADATA_AND_UPLOAD_SYNC_FAVORITES"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/filemanager/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/W;->d:Lcom/dropbox/android/filemanager/W;

    .line 54
    new-instance v0, Lcom/dropbox/android/filemanager/ab;

    const-string v1, "FULL_SYNC"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/filemanager/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/filemanager/W;->e:Lcom/dropbox/android/filemanager/W;

    .line 3
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dropbox/android/filemanager/W;

    sget-object v1, Lcom/dropbox/android/filemanager/W;->a:Lcom/dropbox/android/filemanager/W;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/filemanager/W;->b:Lcom/dropbox/android/filemanager/W;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/filemanager/W;->c:Lcom/dropbox/android/filemanager/W;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/filemanager/W;->d:Lcom/dropbox/android/filemanager/W;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/filemanager/W;->e:Lcom/dropbox/android/filemanager/W;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dropbox/android/filemanager/W;->f:[Lcom/dropbox/android/filemanager/W;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/filemanager/X;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/filemanager/W;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/filemanager/W;
    .registers 2
    .parameter

    .prologue
    .line 3
    const-class v0, Lcom/dropbox/android/filemanager/W;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/W;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/filemanager/W;
    .registers 1

    .prologue
    .line 3
    sget-object v0, Lcom/dropbox/android/filemanager/W;->f:[Lcom/dropbox/android/filemanager/W;

    invoke-virtual {v0}, [Lcom/dropbox/android/filemanager/W;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/filemanager/W;

    return-object v0
.end method


# virtual methods
.method abstract a(Lcom/dropbox/android/filemanager/LocalEntry;)Z
.end method
