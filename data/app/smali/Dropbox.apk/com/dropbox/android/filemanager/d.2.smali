.class public Lcom/dropbox/android/filemanager/d;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/R;


# static fields
.field private static final h:I


# instance fields
.field protected a:Ljava/util/HashMap;

.field protected b:Lcom/dropbox/android/util/H;

.field protected final c:Landroid/os/Handler;

.field protected d:Lcom/dropbox/android/filemanager/f;

.field protected e:Z

.field protected f:Z

.field protected final g:I

.field private i:Ljava/util/HashMap;

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 27
    invoke-static {}, Lcom/dropbox/android/util/bc;->f()I

    move-result v0

    sput v0, Lcom/dropbox/android/filemanager/d;->h:I

    .line 28
    return-void
.end method

.method public constructor <init>(ILcom/dropbox/android/filemanager/f;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/d;->c:Landroid/os/Handler;

    .line 57
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/d;->e:Z

    .line 59
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/d;->f:Z

    .line 62
    iput v1, p0, Lcom/dropbox/android/filemanager/d;->j:I

    .line 67
    new-instance v0, Lcom/dropbox/android/util/H;

    sget v1, Lcom/dropbox/android/filemanager/d;->h:I

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/H;-><init>(II)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    .line 68
    iput-object p2, p0, Lcom/dropbox/android/filemanager/d;->d:Lcom/dropbox/android/filemanager/f;

    .line 69
    iput p3, p0, Lcom/dropbox/android/filemanager/d;->g:I

    .line 70
    return-void
.end method

.method public constructor <init>(ILcom/dropbox/android/filemanager/f;Lcom/dropbox/android/filemanager/d;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/d;->c:Landroid/os/Handler;

    .line 57
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/d;->e:Z

    .line 59
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/d;->f:Z

    .line 62
    iput v1, p0, Lcom/dropbox/android/filemanager/d;->j:I

    .line 73
    iput-object p2, p0, Lcom/dropbox/android/filemanager/d;->d:Lcom/dropbox/android/filemanager/f;

    .line 74
    new-instance v0, Lcom/dropbox/android/util/H;

    sget v1, Lcom/dropbox/android/filemanager/d;->h:I

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/H;-><init>(II)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    .line 75
    iget v0, p3, Lcom/dropbox/android/filemanager/d;->g:I

    iput v0, p0, Lcom/dropbox/android/filemanager/d;->g:I

    .line 76
    iget-object v0, p3, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    .line 77
    const/4 v0, 0x0

    iput-object v0, p3, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    .line 78
    return-void
.end method

.method private a(Ljava/lang/String;I)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 108
    iget v1, p0, Lcom/dropbox/android/filemanager/d;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dropbox/android/filemanager/d;->j:I

    .line 109
    iget-object v1, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    if-eqz v1, :cond_2e

    .line 110
    iget-object v1, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 112
    iget-object v1, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/E;

    invoke-virtual {v1, p2, v0}, Lcom/dropbox/android/util/H;->a(ILcom/dropbox/android/util/E;)V

    .line 113
    const/4 v0, 0x1

    .line 115
    :cond_21
    iget v1, p0, Lcom/dropbox/android/filemanager/d;->j:I

    iget-object v2, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-le v1, v2, :cond_2e

    .line 116
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/d;->c()V

    .line 119
    :cond_2e
    return v0
.end method

.method private c()V
    .registers 3

    .prologue
    .line 130
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    if-eqz v0, :cond_21

    .line 131
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/E;

    .line 132
    invoke-virtual {v0}, Lcom/dropbox/android/util/E;->b()V

    goto :goto_e

    .line 134
    :cond_1e
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    .line 136
    :cond_21
    return-void
.end method


# virtual methods
.method public final a(ILcom/dropbox/android/filemanager/h;)Lcom/dropbox/android/util/E;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 151
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/H;->d(I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/H;->c(I)Lcom/dropbox/android/util/E;

    move-result-object v0

    .line 160
    :goto_e
    return-object v0

    .line 154
    :cond_f
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->d:Lcom/dropbox/android/filemanager/f;

    invoke-interface {v0, p1}, Lcom/dropbox/android/filemanager/f;->a(I)Lcom/dropbox/android/filemanager/g;

    move-result-object v0

    .line 155
    if-eqz v0, :cond_29

    iget-object v0, v0, Lcom/dropbox/android/filemanager/g;->a:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/dropbox/android/filemanager/d;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_29

    const/4 v0, 0x1

    .line 156
    :goto_20
    if-eqz v0, :cond_2b

    .line 157
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/H;->c(I)Lcom/dropbox/android/util/E;

    move-result-object v0

    goto :goto_e

    .line 155
    :cond_29
    const/4 v0, 0x0

    goto :goto_20

    .line 159
    :cond_2b
    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/filemanager/d;->b(ILcom/dropbox/android/filemanager/h;)V

    .line 160
    const/4 v0, 0x0

    goto :goto_e
.end method

.method public final a()V
    .registers 6

    .prologue
    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/filemanager/d;->f:Z

    .line 96
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/d;->c()V

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0}, Lcom/dropbox/android/util/H;->c()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 98
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    .line 99
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 100
    iget-object v3, p0, Lcom/dropbox/android/filemanager/d;->i:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/dropbox/android/filemanager/d;->d:Lcom/dropbox/android/filemanager/f;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v4, v1}, Lcom/dropbox/android/filemanager/f;->a(I)Lcom/dropbox/android/filemanager/g;

    move-result-object v1

    iget-object v1, v1, Lcom/dropbox/android/filemanager/g;->a:Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1f

    .line 103
    :cond_47
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0}, Lcom/dropbox/android/util/H;->a()V

    .line 104
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/H;->b(I)V

    .line 140
    return-void
.end method

.method public final a(II)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/util/H;->a(II)V

    .line 145
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/d;->b()V

    .line 146
    return-void
.end method

.method public final a(ILcom/dropbox/android/filemanager/T;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 218
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->c:Landroid/os/Handler;

    new-instance v1, Lcom/dropbox/android/filemanager/e;

    invoke-direct {v1, p0, p2, p1}, Lcom/dropbox/android/filemanager/e;-><init>(Lcom/dropbox/android/filemanager/d;Lcom/dropbox/android/filemanager/T;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 241
    return-void
.end method

.method protected final b()V
    .registers 8

    .prologue
    const/4 v1, 0x1

    .line 187
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/d;->e:Z

    if-nez v0, :cond_2d

    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/d;->f:Z

    if-nez v0, :cond_2d

    .line 188
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0}, Lcom/dropbox/android/util/H;->b()I

    move-result v4

    .line 190
    const/4 v0, -0x1

    if-eq v4, v0, :cond_2d

    .line 191
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->d:Lcom/dropbox/android/filemanager/f;

    invoke-interface {v0, v4}, Lcom/dropbox/android/filemanager/f;->a(I)Lcom/dropbox/android/filemanager/g;

    move-result-object v3

    .line 192
    if-eqz v3, :cond_41

    iget-object v0, v3, Lcom/dropbox/android/filemanager/g;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_41

    .line 194
    iget-object v0, v3, Lcom/dropbox/android/filemanager/g;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v4}, Lcom/dropbox/android/filemanager/d;->a(Ljava/lang/String;I)Z

    move-result v0

    .line 195
    if-eqz v0, :cond_2e

    .line 196
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/d;->b()V

    .line 214
    :cond_2d
    :goto_2d
    return-void

    .line 199
    :cond_2e
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/d;->e:Z

    .line 200
    invoke-static {}, Lcom/dropbox/android/filemanager/I;->a()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    iget-object v1, v3, Lcom/dropbox/android/filemanager/g;->a:Ljava/lang/String;

    iget-object v2, v3, Lcom/dropbox/android/filemanager/g;->b:Ljava/lang/String;

    iget-boolean v3, v3, Lcom/dropbox/android/filemanager/g;->c:Z

    iget v5, p0, Lcom/dropbox/android/filemanager/d;->g:I

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;Ljava/lang/String;ZIILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/S;

    goto :goto_2d

    .line 203
    :cond_41
    if-eqz v3, :cond_5d

    iget-object v0, v3, Lcom/dropbox/android/filemanager/g;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5d

    .line 205
    iput-boolean v1, p0, Lcom/dropbox/android/filemanager/d;->e:Z

    .line 206
    invoke-static {}, Lcom/dropbox/android/filemanager/I;->a()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    iget-object v1, v3, Lcom/dropbox/android/filemanager/g;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/filemanager/d;->g:I

    invoke-virtual {v0, v1, v4, v2, p0}, Lcom/dropbox/android/filemanager/I;->b(Landroid/net/Uri;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/S;

    goto :goto_2d

    .line 208
    :cond_5d
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    invoke-virtual {v0, v4}, Lcom/dropbox/android/util/H;->b(I)V

    .line 210
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/d;->b()V

    goto :goto_2d
.end method

.method protected final b(ILcom/dropbox/android/filemanager/h;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 166
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 168
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    :goto_1b
    return-void

    .line 170
    :cond_1c
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 171
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v1, p0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1b
.end method

.method public final c(ILcom/dropbox/android/filemanager/h;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 179
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 180
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    :cond_2a
    return-void
.end method
