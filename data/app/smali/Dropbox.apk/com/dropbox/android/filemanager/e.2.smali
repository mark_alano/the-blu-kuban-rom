.class final Lcom/dropbox/android/filemanager/e;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/dropbox/android/filemanager/T;

.field final synthetic b:I

.field final synthetic c:Lcom/dropbox/android/filemanager/d;


# direct methods
.method constructor <init>(Lcom/dropbox/android/filemanager/d;Lcom/dropbox/android/filemanager/T;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 218
    iput-object p1, p0, Lcom/dropbox/android/filemanager/e;->c:Lcom/dropbox/android/filemanager/d;

    iput-object p2, p0, Lcom/dropbox/android/filemanager/e;->a:Lcom/dropbox/android/filemanager/T;

    iput p3, p0, Lcom/dropbox/android/filemanager/e;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 5

    .prologue
    .line 221
    const/4 v0, 0x0

    .line 222
    iget-object v1, p0, Lcom/dropbox/android/filemanager/e;->a:Lcom/dropbox/android/filemanager/T;

    if-nez v1, :cond_45

    .line 223
    iget-object v1, p0, Lcom/dropbox/android/filemanager/e;->c:Lcom/dropbox/android/filemanager/d;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    iget v2, p0, Lcom/dropbox/android/filemanager/e;->b:I

    invoke-virtual {v1, v2}, Lcom/dropbox/android/util/H;->b(I)V

    move-object v1, v0

    .line 228
    :goto_f
    iget-object v0, p0, Lcom/dropbox/android/filemanager/e;->c:Lcom/dropbox/android/filemanager/d;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    iget v2, p0, Lcom/dropbox/android/filemanager/e;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_66

    .line 229
    iget-object v0, p0, Lcom/dropbox/android/filemanager/e;->c:Lcom/dropbox/android/filemanager/d;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    iget v2, p0, Lcom/dropbox/android/filemanager/e;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_33
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/h;

    .line 230
    iget v3, p0, Lcom/dropbox/android/filemanager/e;->b:I

    invoke-interface {v0, v3, v1}, Lcom/dropbox/android/filemanager/h;->a(ILcom/dropbox/android/util/E;)V

    goto :goto_33

    .line 225
    :cond_45
    new-instance v0, Lcom/dropbox/android/util/E;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/e;->a:Lcom/dropbox/android/filemanager/T;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/E;-><init>(Landroid/graphics/Bitmap;)V

    .line 226
    iget-object v1, p0, Lcom/dropbox/android/filemanager/e;->c:Lcom/dropbox/android/filemanager/d;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/d;->b:Lcom/dropbox/android/util/H;

    iget v2, p0, Lcom/dropbox/android/filemanager/e;->b:I

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/H;->a(ILcom/dropbox/android/util/E;)V

    move-object v1, v0

    goto :goto_f

    .line 232
    :cond_59
    iget-object v0, p0, Lcom/dropbox/android/filemanager/e;->c:Lcom/dropbox/android/filemanager/d;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/d;->a:Ljava/util/HashMap;

    iget v2, p0, Lcom/dropbox/android/filemanager/e;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    :cond_66
    if-eqz v1, :cond_6b

    .line 235
    invoke-virtual {v1}, Lcom/dropbox/android/util/E;->b()V

    .line 237
    :cond_6b
    iget-object v0, p0, Lcom/dropbox/android/filemanager/e;->c:Lcom/dropbox/android/filemanager/d;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/dropbox/android/filemanager/d;->e:Z

    .line 238
    iget-object v0, p0, Lcom/dropbox/android/filemanager/e;->c:Lcom/dropbox/android/filemanager/d;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/d;->b()V

    .line 239
    return-void
.end method
