.class public Lcom/dropbox/android/filemanager/u;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 39
    const-class v0, Lcom/dropbox/android/filemanager/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/u;->a:Ljava/lang/String;

    .line 80
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "path"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "hash"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "revision"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_revision"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "local_modified"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "is_dir"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "local_hash"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "is_favorite"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "bytes"

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/filemanager/u;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method private static a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;
    .registers 7
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 95
    new-instance v3, Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v3}, Lcom/dropbox/android/filemanager/LocalEntry;-><init>()V

    .line 96
    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    .line 97
    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->i:Ljava/lang/String;

    .line 98
    const/4 v0, 0x3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    .line 99
    const/4 v0, 0x4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->c:Ljava/lang/String;

    .line 100
    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->a:Ljava/lang/String;

    .line 101
    const/4 v0, 0x6

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/dropbox/android/filemanager/LocalEntry;->e:J

    .line 102
    const/4 v0, 0x7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_55

    move v0, v1

    :goto_38
    iput-boolean v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    .line 103
    const/16 v0, 0x8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->d:Ljava/lang/String;

    .line 104
    const/16 v0, 0x9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_57

    :goto_4a
    iput-boolean v1, v3, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    .line 105
    const/16 v0, 0xa

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v3, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    .line 106
    return-object v3

    :cond_55
    move v0, v2

    .line 102
    goto :goto_38

    :cond_57
    move v1, v2

    .line 104
    goto :goto_4a
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 157
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 159
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_22

    .line 160
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 163
    :cond_22
    const-string v1, "canon_path = ?"

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {p0, v1, v2}, Lcom/dropbox/android/filemanager/u;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedList;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-nez v1, :cond_34

    .line 165
    const/4 v0, 0x0

    .line 167
    :goto_33
    return-object v0

    :cond_34
    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/filemanager/LocalEntry;

    goto :goto_33
.end method

.method public static a(Ljava/lang/String;Lcom/dropbox/android/filemanager/w;)Lcom/dropbox/android/filemanager/v;
    .registers 12
    .parameter
    .parameter

    .prologue
    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    .line 257
    invoke-static {p0}, Lcom/dropbox/android/util/aT;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_51

    iget-object v0, p1, Lcom/dropbox/android/filemanager/w;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/aT;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_51

    .line 258
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 260
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_51

    iget-wide v6, p1, Lcom/dropbox/android/filemanager/w;->c:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_51

    iget-wide v6, p1, Lcom/dropbox/android/filemanager/w;->c:J

    cmp-long v1, v6, v8

    if-lez v1, :cond_51

    .line 261
    invoke-static {v0}, Lcom/dropbox/android/util/af;->c(Ljava/io/File;)Ljava/lang/String;

    move-result-object v1

    .line 262
    iget-object v0, p1, Lcom/dropbox/android/filemanager/w;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/aT;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_48

    iget-object v0, p1, Lcom/dropbox/android/filemanager/w;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 263
    new-instance v0, Lcom/dropbox/android/filemanager/v;

    iget-object v4, p1, Lcom/dropbox/android/filemanager/w;->d:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/v;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    .line 268
    :goto_47
    return-object v0

    .line 265
    :cond_48
    new-instance v0, Lcom/dropbox/android/filemanager/v;

    iget-object v4, p1, Lcom/dropbox/android/filemanager/w;->d:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/v;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    goto :goto_47

    .line 268
    :cond_51
    new-instance v0, Lcom/dropbox/android/filemanager/v;

    const/4 v1, 0x0

    iget-object v4, p1, Lcom/dropbox/android/filemanager/w;->d:Ljava/lang/String;

    move-wide v2, v8

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/filemanager/v;-><init>(Ljava/lang/String;JLjava/lang/String;Z)V

    goto :goto_47
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/android/util/DropboxPath;Ljava/lang/String;Lcom/dropbox/android/provider/A;)Ldbxyzptlk/n/k;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    const/16 v8, 0x11

    .line 174
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v7

    .line 178
    :try_start_b
    iget-object v0, v7, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    const/16 v2, 0x2710

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Ldbxyzptlk/n/k;

    move-result-object v0

    .line 179
    if-eqz p3, :cond_1d

    .line 180
    const/16 v2, 0x8

    invoke-virtual {p3, p0, v2}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V
    :try_end_1d
    .catch Ldbxyzptlk/o/d; {:try_start_b .. :try_end_1d} :catch_1e
    .catch Ldbxyzptlk/o/f; {:try_start_b .. :try_end_1d} :catch_2d
    .catch Ldbxyzptlk/o/j; {:try_start_b .. :try_end_1d} :catch_44
    .catch Ldbxyzptlk/o/i; {:try_start_b .. :try_end_1d} :catch_57
    .catch Ldbxyzptlk/o/a; {:try_start_b .. :try_end_1d} :catch_a7

    .line 226
    :cond_1d
    :goto_1d
    return-object v0

    .line 183
    :catch_1e
    move-exception v0

    .line 184
    if-eqz p3, :cond_24

    .line 185
    invoke-virtual {p3, p0, v8}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V

    .line 187
    :cond_24
    sget-object v1, Lcom/dropbox/android/filemanager/u;->a:Ljava/lang/String;

    const-string v2, "IOException when contacting server. Probably networking failure."

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2b
    :goto_2b
    move-object v0, v6

    .line 226
    goto :goto_1d

    .line 188
    :catch_2d
    move-exception v0

    .line 189
    sget-object v1, Lcom/dropbox/android/filemanager/u;->a:Ljava/lang/String;

    const-string v2, "Not able to communicate directly with server"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 190
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 191
    if-eqz p3, :cond_2b

    .line 192
    invoke-virtual {p3, p0, v8}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V

    goto :goto_2b

    .line 194
    :catch_44
    move-exception v0

    .line 196
    if-eqz p3, :cond_4c

    .line 197
    const/16 v1, 0x10

    invoke-virtual {p3, p0, v1}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V

    .line 199
    :cond_4c
    sget-object v1, Lcom/dropbox/android/filemanager/u;->a:Ljava/lang/String;

    const-string v2, "Unauthorized token, unlinking account: "

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 200
    invoke-virtual {v7}, Lcom/dropbox/android/filemanager/a;->e()Z

    goto :goto_2b

    .line 201
    :catch_57
    move-exception v0

    .line 202
    iget v2, v0, Ldbxyzptlk/o/i;->b:I

    const/16 v3, 0x130

    if-ne v2, v3, :cond_67

    .line 204
    if-eqz p3, :cond_65

    .line 205
    const/16 v0, 0xff

    invoke-virtual {p3, p0, v0}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V

    :cond_65
    move-object v0, v6

    .line 207
    goto :goto_1d

    .line 208
    :cond_67
    iget v2, v0, Ldbxyzptlk/o/i;->b:I

    const/16 v3, 0x194

    if-ne v2, v3, :cond_91

    .line 209
    sget-object v0, Lcom/dropbox/android/filemanager/u;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Folder not found "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    if-eqz p3, :cond_2b

    .line 212
    const/16 v0, 0x13

    invoke-virtual {p3, p0, v0}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V

    goto :goto_2b

    .line 215
    :cond_91
    sget-object v1, Lcom/dropbox/android/filemanager/u;->a:Ljava/lang/String;

    const-string v2, "Strange metadata error: "

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 216
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 217
    if-eqz p3, :cond_2b

    .line 218
    invoke-virtual {p3, p0, v8}, Lcom/dropbox/android/provider/A;->a(Landroid/content/Context;I)V

    goto :goto_2b

    .line 221
    :catch_a7
    move-exception v0

    .line 223
    sget-object v1, Lcom/dropbox/android/filemanager/u;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Strange metadata error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    goto/16 :goto_2b
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/ArrayList;
    .registers 10
    .parameter

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 115
    const-string v1, "dropbox"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "path"

    aput-object v0, v2, v8

    const-string v3, "local_revision NOT NULL"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 124
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 125
    const/4 v2, -0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 126
    :goto_22
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_39

    .line 127
    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/at;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_22

    .line 129
    :cond_39
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 130
    return-object v1
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedList;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 231
    const-string v1, "dropbox"

    sget-object v2, Lcom/dropbox/android/filemanager/u;->b:[Ljava/lang/String;

    const-string v7, "is_dir DESC, _display_name COLLATE NOCASE"

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 234
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 235
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_2c

    .line 236
    const/4 v2, -0x1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 240
    :goto_1e
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 241
    invoke-static {v0}, Lcom/dropbox/android/filemanager/u;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    .line 242
    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1e

    .line 245
    :cond_2c
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 246
    return-object v1
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;Z)Ljava/util/List;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 135
    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 138
    const-string v1, "canon_parent_path = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {p0, v1, v2}, Lcom/dropbox/android/filemanager/u;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedList;

    move-result-object v0

    .line 139
    if-eqz p2, :cond_1f

    .line 141
    invoke-static {p0, p1}, Lcom/dropbox/android/filemanager/u;->a(Landroid/database/sqlite/SQLiteDatabase;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v1

    .line 142
    if-eqz v1, :cond_1f

    .line 143
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_1f
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/dropbox/android/util/V;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 296
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    iget-object v0, p2, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    .line 303
    :try_start_8
    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/at;

    move-result-object v0

    .line 304
    invoke-virtual {v0}, Lcom/dropbox/android/util/at;->a()Ljava/io/File;

    move-result-object v0

    .line 305
    if-eqz v0, :cond_1c

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 306
    const/4 v2, 0x0

    invoke-static {p0, v0, v2}, Lcom/dropbox/android/util/af;->a(Landroid/content/Context;Ljava/io/File;Ljava/util/HashSet;)V

    .line 310
    :cond_1c
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/util/DropboxPath;)V

    .line 314
    invoke-virtual {p1}, Lcom/dropbox/android/util/V;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 317
    const-string v2, "canon_path = ?"

    .line 318
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 319
    const-string v4, "dropbox"

    invoke-virtual {v0, v4, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 320
    if-eq v0, v6, :cond_6d

    .line 321
    sget-object v2, Lcom/dropbox/android/filemanager/u;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error deleting entry in directory sync, not one: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " for "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6d
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_6d} :catch_6e

    .line 328
    :cond_6d
    :goto_6d
    return-void

    .line 323
    :catch_6e
    move-exception v0

    .line 325
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    invoke-virtual {v2, v0}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;)V

    .line 326
    sget-object v2, Lcom/dropbox/android/filemanager/u;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception in deleteLocalEntryAndMedia() with path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6d
.end method

.method static a(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 6
    .parameter

    .prologue
    .line 332
    iget-boolean v0, p0, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    if-eqz v0, :cond_26

    .line 334
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    .line 335
    sget-object v1, Lcom/dropbox/android/taskqueue/v;->b:Lcom/dropbox/android/taskqueue/v;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    invoke-static {}, Lcom/dropbox/android/util/bc;->e()Ldbxyzptlk/n/o;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/taskqueue/v;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V

    .line 336
    sget-object v1, Lcom/dropbox/android/taskqueue/v;->a:Lcom/dropbox/android/taskqueue/v;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/LocalEntry;->r:Ljava/lang/String;

    invoke-static {}, Lcom/dropbox/android/util/bc;->g()Ldbxyzptlk/n/o;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/taskqueue/v;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V

    .line 338
    :cond_26
    return-void
.end method

.method public static b(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/LinkedList;
    .registers 3
    .parameter

    .prologue
    .line 151
    const-string v0, "is_favorite = 1"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/filemanager/u;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method
