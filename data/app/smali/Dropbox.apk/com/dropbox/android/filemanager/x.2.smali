.class public Lcom/dropbox/android/filemanager/x;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Lcom/dropbox/android/filemanager/x;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Landroid/os/Handler;

.field private final e:Lcom/dropbox/android/provider/h;

.field private final f:Lcom/dropbox/android/util/ah;

.field private final g:Ljava/lang/Object;

.field private volatile h:Lcom/dropbox/android/taskqueue/H;

.field private i:Lcom/dropbox/android/util/DropboxPath;

.field private j:Lcom/dropbox/android/taskqueue/p;

.field private k:Lcom/dropbox/android/taskqueue/i;

.field private l:Lcom/dropbox/android/taskqueue/q;

.field private m:Ljava/lang/Thread;

.field private final n:Ldbxyzptlk/k/g;

.field private final o:Lcom/dropbox/android/filemanager/D;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 83
    const-class v0, Lcom/dropbox/android/filemanager/x;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    .line 88
    const/4 v0, 0x0

    sput-object v0, Lcom/dropbox/android/filemanager/x;->b:Lcom/dropbox/android/filemanager/x;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->g:Ljava/lang/Object;

    .line 99
    iput-object v1, p0, Lcom/dropbox/android/filemanager/x;->i:Lcom/dropbox/android/util/DropboxPath;

    .line 108
    new-instance v0, Ldbxyzptlk/k/g;

    invoke-direct {v0}, Ldbxyzptlk/k/g;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->n:Ldbxyzptlk/k/g;

    .line 109
    new-instance v0, Lcom/dropbox/android/filemanager/D;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/filemanager/D;-><init>(Lcom/dropbox/android/filemanager/x;Lcom/dropbox/android/filemanager/y;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->o:Lcom/dropbox/android/filemanager/D;

    .line 112
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    .line 113
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->d:Landroid/os/Handler;

    .line 114
    invoke-static {}, Lcom/dropbox/android/provider/h;->a()Lcom/dropbox/android/provider/h;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    .line 116
    new-instance v0, Lcom/dropbox/android/util/ah;

    invoke-direct {v0}, Lcom/dropbox/android/util/ah;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->f:Lcom/dropbox/android/util/ah;

    .line 118
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/x;->p()V

    .line 122
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Start UploadQueue Timer"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/dropbox/android/filemanager/y;

    invoke-direct {v1, p0}, Lcom/dropbox/android/filemanager/y;-><init>(Lcom/dropbox/android/filemanager/x;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 129
    return-void
.end method

.method private a(Landroid/net/Uri;)Landroid/net/Uri;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 722
    iget-object v1, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-static {p1, v1}, Lcom/dropbox/android/util/ae;->a(Landroid/net/Uri;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 754
    :cond_9
    :goto_9
    return-object v0

    .line 726
    :cond_a
    :try_start_a
    iget-object v1, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "r"

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    .line 727
    if-eqz v1, :cond_9

    .line 728
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    .line 731
    invoke-static {p1}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 732
    invoke-static {}, Lcom/dropbox/android/util/af;->b()Ljava/io/File;

    move-result-object v3

    .line 733
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_29} :catch_40
    .catch Ljava/lang/NullPointerException; {:try_start_a .. :try_end_29} :catch_48
    .catch Ljava/lang/SecurityException; {:try_start_a .. :try_end_29} :catch_4a

    .line 737
    :try_start_29
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2e
    .catchall {:try_start_29 .. :try_end_2e} :catchall_42
    .catch Ljava/io/IOException; {:try_start_29 .. :try_end_2e} :catch_3a

    .line 738
    :try_start_2e
    invoke-static {v1, v2}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 739
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_34
    .catchall {:try_start_2e .. :try_end_34} :catchall_4c
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_34} :catch_4e

    move-result-object v1

    .line 743
    :try_start_35
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    move-object v0, v1

    goto :goto_9

    .line 740
    :catch_3a
    move-exception v1

    move-object v1, v0

    .line 743
    :goto_3c
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    goto :goto_9

    .line 747
    :catch_40
    move-exception v1

    goto :goto_9

    .line 743
    :catchall_42
    move-exception v1

    move-object v2, v0

    :goto_44
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    throw v1
    :try_end_48
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_48} :catch_40
    .catch Ljava/lang/NullPointerException; {:try_start_35 .. :try_end_48} :catch_48
    .catch Ljava/lang/SecurityException; {:try_start_35 .. :try_end_48} :catch_4a

    .line 749
    :catch_48
    move-exception v1

    goto :goto_9

    .line 753
    :catch_4a
    move-exception v1

    goto :goto_9

    .line 743
    :catchall_4c
    move-exception v1

    goto :goto_44

    .line 740
    :catch_4e
    move-exception v1

    move-object v1, v2

    goto :goto_3c
.end method

.method public static a()Lcom/dropbox/android/filemanager/x;
    .registers 1

    .prologue
    .line 140
    sget-object v0, Lcom/dropbox/android/filemanager/x;->b:Lcom/dropbox/android/filemanager/x;

    if-nez v0, :cond_a

    .line 141
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 143
    :cond_a
    sget-object v0, Lcom/dropbox/android/filemanager/x;->b:Lcom/dropbox/android/filemanager/x;

    return-object v0
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/x;Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/util/DropboxPath;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/dropbox/android/filemanager/x;->i:Lcom/dropbox/android/util/DropboxPath;

    return-object p1
.end method

.method static synthetic a(Lcom/dropbox/android/filemanager/x;)Lcom/dropbox/android/util/ah;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->f:Lcom/dropbox/android/util/ah;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 132
    sget-object v0, Lcom/dropbox/android/filemanager/x;->b:Lcom/dropbox/android/filemanager/x;

    if-nez v0, :cond_c

    .line 133
    new-instance v0, Lcom/dropbox/android/filemanager/x;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/x;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/dropbox/android/filemanager/x;->b:Lcom/dropbox/android/filemanager/x;

    .line 137
    return-void

    .line 135
    :cond_c
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private a(Ljava/util/HashSet;)V
    .registers 4
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->j:Lcom/dropbox/android/taskqueue/p;

    if-eqz v0, :cond_9

    .line 206
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->j:Lcom/dropbox/android/taskqueue/p;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/p;->b()V

    .line 209
    :cond_9
    invoke-static {}, Lcom/dropbox/android/util/af;->j()Ljava/io/File;

    move-result-object v0

    .line 211
    iget-object v1, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/dropbox/android/util/af;->a(Landroid/content/Context;Ljava/io/File;Ljava/util/HashSet;)V

    .line 213
    invoke-static {}, Lcom/dropbox/android/util/af;->l()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/af;->a(Ljava/io/File;)V

    .line 216
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->l:Lcom/dropbox/android/taskqueue/q;

    if-eqz v0, :cond_22

    .line 217
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->l:Lcom/dropbox/android/taskqueue/q;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/taskqueue/q;->a(Ljava/util/HashSet;)V

    .line 219
    :cond_22
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/filemanager/x;)Ldbxyzptlk/k/g;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->n:Ldbxyzptlk/k/g;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/filemanager/x;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/filemanager/x;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 82
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private e(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 4
    .parameter

    .prologue
    .line 883
    new-instance v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 884
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->i()Lcom/dropbox/android/taskqueue/p;

    move-result-object v1

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DownloadTask;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/p;->b(Ljava/lang/String;)Z

    .line 885
    return-void
.end method

.method private p()V
    .registers 3

    .prologue
    .line 153
    new-instance v0, Lcom/dropbox/android/filemanager/z;

    invoke-direct {v0, p0}, Lcom/dropbox/android/filemanager/z;-><init>(Lcom/dropbox/android/filemanager/x;)V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->m:Ljava/lang/Thread;

    .line 163
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->m:Ljava/lang/Thread;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 164
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->m:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 165
    return-void
.end method

.method private q()Ljava/util/HashSet;
    .registers 11

    .prologue
    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 230
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 234
    const-string v1, "dropbox"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "canon_path"

    aput-object v3, v2, v9

    const-string v3, "is_favorite= 1"

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 236
    :goto_1f
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_32

    .line 237
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1f

    .line 239
    :cond_32
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 241
    return-object v8
.end method

.method private r()V
    .registers 3

    .prologue
    .line 249
    const/4 v0, 0x0

    move v1, v0

    .line 253
    :goto_2
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->m:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_1a
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_7} :catch_16

    .line 260
    if-eqz v1, :cond_10

    .line 261
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 264
    :cond_10
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->f:Lcom/dropbox/android/util/ah;

    invoke-virtual {v0}, Lcom/dropbox/android/util/ah;->a()V

    .line 265
    return-void

    .line 255
    :catch_16
    move-exception v0

    .line 256
    const/4 v0, 0x1

    move v1, v0

    .line 257
    goto :goto_2

    .line 260
    :catchall_1a
    move-exception v0

    if-eqz v1, :cond_24

    .line 261
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_24
    throw v0
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)Lcom/dropbox/android/filemanager/LocalEntry;
    .registers 16
    .parameter
    .parameter

    .prologue
    const/4 v12, 0x1

    .line 395
    new-instance v1, Ldbxyzptlk/k/e;

    invoke-direct {v1}, Ldbxyzptlk/k/e;-><init>()V

    .line 396
    new-instance v2, Ldbxyzptlk/k/i;

    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v0, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v0}, Ldbxyzptlk/k/i;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    .line 397
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->n:Ldbxyzptlk/k/g;

    invoke-virtual {v0, v2, v1}, Ldbxyzptlk/k/g;->a(Ldbxyzptlk/k/i;Ldbxyzptlk/k/f;)V

    .line 400
    :try_start_17
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v0, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 401
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 403
    sget-object v3, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Renaming "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v3

    .line 406
    iget-object v3, v3, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    iget-object v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/n/k;

    move-result-object v3

    .line 408
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    .line 411
    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7c

    .line 412
    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 415
    :cond_7c
    new-instance v4, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v4, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/at;

    move-result-object v4

    .line 416
    invoke-virtual {v4}, Lcom/dropbox/android/util/at;->a()Ljava/io/File;

    move-result-object v4

    .line 417
    new-instance v5, Lcom/dropbox/android/util/DropboxPath;

    iget-object v6, v3, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/at;

    move-result-object v5

    .line 419
    iget-object v6, p0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    invoke-virtual {v6}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_99
    .catchall {:try_start_17 .. :try_end_99} :catchall_189

    move-result-object v6

    .line 421
    :try_start_9a
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 423
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v0}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 426
    const-string v8, "canon_path = ?"

    .line 427
    invoke-static {v3}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ldbxyzptlk/n/k;)Landroid/content/ContentValues;

    move-result-object v9

    .line 428
    if-eqz v4, :cond_c2

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_c2

    .line 429
    const-string v10, "_data"

    invoke-virtual {v5}, Lcom/dropbox/android/util/at;->a()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :cond_c2
    const-string v10, "dropbox"

    invoke-virtual {v6, v10, v9, v8, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 433
    if-eq v7, v12, :cond_f0

    .line 434
    sget-object v8, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error moving entry, not one: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " for "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :cond_f0
    iget-boolean v7, p1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v7, :cond_190

    .line 443
    if-eqz v4, :cond_102

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_102

    .line 444
    iget-object v5, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    const/4 v7, 0x0

    invoke-static {v5, v4, v7}, Lcom/dropbox/android/util/af;->a(Landroid/content/Context;Ljava/io/File;Ljava/util/HashSet;)V

    .line 447
    :cond_102
    iget-object v4, p0, Lcom/dropbox/android/filemanager/x;->l:Lcom/dropbox/android/taskqueue/q;

    new-instance v5, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v5, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/util/DropboxPath;)V

    .line 453
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/dropbox/android/provider/h;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 455
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 457
    iget-object v5, v3, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-static {v5}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 458
    const-string v7, "update dropbox set canon_path= ? || substr(canon_path,?), path= ? || substr(path,?), canon_parent_path= ? || substr(canon_parent_path,?), parent_path=? || substr(parent_path,?)  where canon_parent_path LIKE ? ESCAPE \'\\\'"

    .line 464
    const/16 v8, 0x9

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v8, v10

    const/4 v10, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v10

    const/4 v10, 0x2

    iget-object v11, v3, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    aput-object v11, v8, v10

    const/4 v10, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v10

    const/4 v10, 0x4

    aput-object v5, v8, v10

    const/4 v5, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v5

    const/4 v5, 0x6

    iget-object v3, v3, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    aput-object v3, v8, v5

    const/4 v3, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v3

    const/16 v0, 0x8

    aput-object v4, v8, v0
    :try_end_165
    .catchall {:try_start_9a .. :try_end_165} :catchall_184

    .line 471
    :try_start_165
    invoke-virtual {v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_168
    .catchall {:try_start_165 .. :try_end_168} :catchall_184
    .catch Ljava/lang/Exception; {:try_start_165 .. :try_end_168} :catch_17f

    .line 476
    :goto_168
    :try_start_168
    invoke-static {}, Lcom/dropbox/android/util/i;->Z()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 488
    :goto_16f
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 490
    invoke-static {p1, v9}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/ContentValues;)Lcom/dropbox/android/filemanager/LocalEntry;
    :try_end_175
    .catchall {:try_start_168 .. :try_end_175} :catchall_184

    move-result-object v0

    .line 492
    :try_start_176
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_179
    .catchall {:try_start_176 .. :try_end_179} :catchall_189

    .line 496
    iget-object v3, p0, Lcom/dropbox/android/filemanager/x;->n:Ldbxyzptlk/k/g;

    invoke-virtual {v3, v2, v1}, Ldbxyzptlk/k/g;->b(Ldbxyzptlk/k/i;Ldbxyzptlk/k/f;)V

    return-object v0

    .line 472
    :catch_17f
    move-exception v0

    .line 473
    :try_start_180
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_183
    .catchall {:try_start_180 .. :try_end_183} :catchall_184

    goto :goto_168

    .line 492
    :catchall_184
    move-exception v0

    :try_start_185
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_189
    .catchall {:try_start_185 .. :try_end_189} :catchall_189

    .line 496
    :catchall_189
    move-exception v0

    iget-object v3, p0, Lcom/dropbox/android/filemanager/x;->n:Ldbxyzptlk/k/g;

    invoke-virtual {v3, v2, v1}, Ldbxyzptlk/k/g;->b(Ldbxyzptlk/k/i;Ldbxyzptlk/k/f;)V

    throw v0

    .line 478
    :cond_190
    :try_start_190
    invoke-virtual {v5}, Lcom/dropbox/android/util/at;->a()Ljava/io/File;

    move-result-object v0

    .line 481
    if-eqz v4, :cond_19f

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_19f

    .line 482
    invoke-virtual {v4, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 484
    :cond_19f
    const-string v0, "rename"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 486
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->l:Lcom/dropbox/android/taskqueue/q;

    iget-object v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    iget-object v3, v3, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Lcom/dropbox/android/taskqueue/q;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1b1
    .catchall {:try_start_190 .. :try_end_1b1} :catchall_184

    goto :goto_16f
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;)Lcom/dropbox/android/filemanager/LocalEntry;
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 850
    const-string v3, "canon_path = ?"

    .line 851
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 853
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 857
    :try_start_17
    const-string v1, "dropbox"

    sget-object v2, Lcom/dropbox/android/provider/K;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_21
    .catchall {:try_start_17 .. :try_end_21} :catchall_3b

    move-result-object v1

    .line 860
    if-eqz v1, :cond_34

    :try_start_24
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 861
    invoke-static {v1}, Lcom/dropbox/android/provider/K;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;
    :try_end_2d
    .catchall {:try_start_24 .. :try_end_2d} :catchall_42

    move-result-object v0

    .line 865
    if-eqz v1, :cond_33

    .line 866
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 869
    :cond_33
    :goto_33
    return-object v0

    .line 865
    :cond_34
    if-eqz v1, :cond_39

    .line 866
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_39
    move-object v0, v8

    .line 869
    goto :goto_33

    .line 865
    :catchall_3b
    move-exception v0

    :goto_3c
    if-eqz v8, :cond_41

    .line 866
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_41
    throw v0

    .line 865
    :catchall_42
    move-exception v0

    move-object v8, v1

    goto :goto_3c
.end method

.method public final a(Ljava/lang/String;)Lcom/dropbox/android/taskqueue/m;
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 501
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v1, p1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    .line 502
    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v0

    .line 506
    :try_start_a
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v2

    .line 507
    iget-object v2, v2, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-virtual {v2, v0}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;)Ldbxyzptlk/n/k;
    :try_end_13
    .catch Ldbxyzptlk/o/d; {:try_start_a .. :try_end_13} :catch_34
    .catch Ldbxyzptlk/o/i; {:try_start_a .. :try_end_13} :catch_38
    .catch Ldbxyzptlk/o/a; {:try_start_a .. :try_end_13} :catch_75

    move-result-object v0

    .line 528
    invoke-static {v0}, Lcom/dropbox/android/filemanager/LocalEntry;->a(Ldbxyzptlk/n/k;)Landroid/content/ContentValues;

    move-result-object v0

    .line 530
    iget-object v2, p0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 533
    :try_start_1e
    const-string v3, "dropbox"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_24
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_1e .. :try_end_24} :catch_82
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_24} :catch_b4

    .line 544
    :cond_24
    :goto_24
    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v0

    .line 545
    iget-object v1, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/dropbox/android/provider/z;->a(Landroid/content/Context;Landroid/net/Uri;)V

    .line 547
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->b:Lcom/dropbox/android/taskqueue/m;

    :goto_33
    return-object v0

    .line 508
    :catch_34
    move-exception v0

    .line 509
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->c:Lcom/dropbox/android/taskqueue/m;

    goto :goto_33

    .line 510
    :catch_38
    move-exception v0

    .line 511
    iget-object v1, v0, Ldbxyzptlk/o/i;->a:Ldbxyzptlk/o/c;

    iget-object v1, v1, Ldbxyzptlk/o/c;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_68

    .line 512
    sget-object v1, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error creating folder: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Ldbxyzptlk/o/i;->a:Ldbxyzptlk/o/c;

    iget-object v3, v3, Ldbxyzptlk/o/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    :goto_5f
    iget v0, v0, Ldbxyzptlk/o/i;->b:I

    const/16 v1, 0x193

    if-ne v0, v1, :cond_72

    .line 518
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->n:Lcom/dropbox/android/taskqueue/m;

    goto :goto_33

    .line 515
    :cond_68
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    goto :goto_5f

    .line 520
    :cond_72
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->m:Lcom/dropbox/android/taskqueue/m;

    goto :goto_33

    .line 522
    :catch_75
    move-exception v0

    .line 524
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 525
    sget-object v0, Lcom/dropbox/android/taskqueue/m;->m:Lcom/dropbox/android/taskqueue/m;

    goto :goto_33

    .line 534
    :catch_82
    move-exception v3

    .line 535
    const-string v3, "dropbox"

    invoke-virtual {v2, v3, v0, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 536
    const/4 v2, 0x1

    if-eq v0, v2, :cond_24

    .line 537
    sget-object v2, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Insert fell back to update.  Odd result, with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " rows changed.  path was: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_24

    .line 539
    :catch_b4
    move-exception v0

    .line 540
    sget-object v2, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    const-string v3, "Crazy error in insert"

    invoke-static {v2, v3, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 541
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    invoke-virtual {v2, v0}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_24
.end method

.method public final a(Ljava/io/File;Ljava/io/File;Z)Ljava/io/File;
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 581
    new-instance v1, Landroid/os/StatFs;

    invoke-virtual {p2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 583
    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/af;->b(Ljava/io/File;)Z

    move-result v3

    if-nez v3, :cond_36

    .line 584
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to create export destination path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 587
    :cond_36
    invoke-virtual {p2}, Ljava/io/File;->createNewFile()Z

    move-result v3

    if-nez v3, :cond_40

    if-nez p3, :cond_40

    move-object p2, v0

    .line 602
    :goto_3f
    return-object p2

    .line 591
    :cond_40
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-long v5, v1

    mul-long/2addr v3, v5

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-lez v1, :cond_65

    const/4 v1, 0x1

    .line 592
    :goto_54
    if-eqz v1, :cond_67

    .line 593
    invoke-static {p1, p2}, Lcom/dropbox/android/util/af;->a(Ljava/io/File;Ljava/io/File;)V

    .line 601
    :goto_59
    new-instance v1, Lcom/dropbox/android/util/ax;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-virtual {p2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4, v0, v2}, Lcom/dropbox/android/util/ax;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_3f

    :cond_65
    move v1, v2

    .line 591
    goto :goto_54

    .line 596
    :cond_67
    sget-object v1, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    const-string v3, "Not enough free space to copy, trying a move"

    invoke-static {v1, v3}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    invoke-virtual {p1, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    goto :goto_59
.end method

.method public final a(J)V
    .registers 4
    .parameter

    .prologue
    .line 877
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->k()Lcom/dropbox/android/taskqueue/H;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/android/taskqueue/H;->a(J)V

    .line 879
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->l()V

    .line 880
    return-void
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;Ljava/io/File;Z)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 613
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->i()Lcom/dropbox/android/taskqueue/p;

    move-result-object v0

    .line 614
    if-eqz v0, :cond_10

    .line 615
    new-instance v1, Lcom/dropbox/android/taskqueue/ExportTask;

    iget-object v2, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/dropbox/android/taskqueue/ExportTask;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/io/File;Z)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/p;->b(Lcom/dropbox/android/taskqueue/k;)V

    .line 617
    :cond_10
    return-void
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;Z)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 551
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 552
    const-string v1, "is_favorite"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 554
    const-string v1, "canon_path = ?"

    .line 555
    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-static {v4}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 557
    iget-object v3, p0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    invoke-virtual {v3}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 558
    const-string v4, "dropbox"

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 559
    if-eq v0, v5, :cond_65

    .line 560
    sget-object v1, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Updated "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " rows when setting favorite, instead of just 1"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    :goto_48
    if-eqz p2, :cond_78

    .line 569
    new-instance v0, Lcom/dropbox/android/taskqueue/DownloadTask;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 570
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->i()Lcom/dropbox/android/taskqueue/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/p;->b(Lcom/dropbox/android/taskqueue/k;)V

    .line 571
    invoke-static {p1}, Lcom/dropbox/android/filemanager/u;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 572
    const-string v0, "favorite"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 577
    :goto_64
    return-void

    .line 563
    :cond_65
    iput-boolean p2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    .line 564
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/provider/z;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_48

    .line 575
    :cond_78
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/x;->e(Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_64
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Landroid/net/Uri;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 674
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/dropbox/android/filemanager/x;->a(Lcom/dropbox/android/util/DropboxPath;Landroid/net/Uri;ZZ)V

    .line 675
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Landroid/net/Uri;ZZ)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 679
    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p3, p4}, Lcom/dropbox/android/filemanager/x;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/Collection;ZZ)V

    .line 680
    return-void
.end method

.method public final a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/Collection;ZZ)V
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 683
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 684
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5c

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 691
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 692
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    .line 693
    iget-object v3, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v1, v2, v4}, Landroid/content/Context;->checkUriPermission(Landroid/net/Uri;III)I

    move-result v1

    .line 694
    if-nez v1, :cond_64

    .line 695
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/x;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 696
    if-eqz v1, :cond_64

    move-object v3, v1

    .line 701
    :goto_31
    invoke-static {v3}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 703
    if-eqz p4, :cond_4b

    .line 704
    new-instance v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 708
    :goto_47
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_d

    .line 706
    :cond_4b
    new-instance v0, Lcom/dropbox/android/taskqueue/UploadTask;

    iget-object v1, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/UploadTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_47

    .line 710
    :cond_5c
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->k()Lcom/dropbox/android/taskqueue/H;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/util/List;)V

    .line 711
    return-void

    :cond_64
    move-object v3, v0

    goto :goto_31
.end method

.method public final a(Lcom/dropbox/android/util/at;)V
    .registers 19
    .parameter

    .prologue
    .line 763
    const-string v4, "canon_path = ?"

    .line 764
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/dropbox/android/util/at;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/provider/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    .line 768
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/h;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 770
    const/4 v14, 0x0

    .line 771
    const-wide/16 v12, 0x0

    .line 772
    const-string v11, ""

    .line 773
    const-string v10, ""

    .line 775
    const/4 v9, 0x0

    .line 777
    :try_start_24
    const-string v2, "dropbox"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "_data"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string v7, "local_modified"

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string v7, "local_hash"

    aput-object v7, v3, v6

    const/4 v6, 0x3

    const-string v7, "local_revision"

    aput-object v7, v3, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_43
    .catchall {:try_start_24 .. :try_end_43} :catchall_97

    move-result-object v6

    .line 785
    if-eqz v6, :cond_16b

    :try_start_46
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_16b

    .line 786
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 787
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    .line 788
    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 789
    const/4 v1, 0x3

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_5f
    .catchall {:try_start_46 .. :try_end_5f} :catchall_167

    move-result-object v1

    move-object v15, v1

    move-object/from16 v16, v2

    move-wide v1, v7

    move-object v7, v15

    move-object/from16 v8, v16

    .line 792
    :goto_67
    if-eqz v6, :cond_6c

    .line 793
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 797
    :cond_6c
    if-nez v3, :cond_a3

    .line 801
    invoke-virtual/range {p1 .. p1}, Lcom/dropbox/android/util/at;->b()Ljava/io/File;

    move-result-object v3

    .line 802
    const-wide/16 v1, 0x1

    .line 803
    if-nez v3, :cond_9f

    .line 805
    sget-object v1, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t update, since couldn\'t get a new file for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/dropbox/android/util/at;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/dropbox/android/util/ae;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    :cond_96
    :goto_96
    return-void

    .line 792
    :catchall_97
    move-exception v1

    move-object v2, v9

    :goto_99
    if-eqz v2, :cond_9e

    .line 793
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_9e
    throw v1

    .line 808
    :cond_9f
    invoke-virtual {v3}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v3

    .line 811
    :cond_a3
    new-instance v6, Lcom/dropbox/android/filemanager/w;

    invoke-direct {v6, v8, v1, v2, v7}, Lcom/dropbox/android/filemanager/w;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-static {v3, v6}, Lcom/dropbox/android/filemanager/u;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/w;)Lcom/dropbox/android/filemanager/v;

    move-result-object v6

    .line 813
    iget-boolean v7, v6, Lcom/dropbox/android/filemanager/v;->a:Z

    if-eqz v7, :cond_f9

    .line 814
    sget-object v1, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "File changed, uploading: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    new-instance v1, Lcom/dropbox/android/taskqueue/UploadTask;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Lcom/dropbox/android/util/at;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->e()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/util/DropboxPath;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/dropbox/android/util/at;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/dropbox/android/util/at;->c()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/dropbox/android/taskqueue/UploadTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 822
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/filemanager/x;->k()Lcom/dropbox/android/taskqueue/H;

    move-result-object v2

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->e()Ldbxyzptlk/k/i;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/dropbox/android/taskqueue/H;->a(Ldbxyzptlk/k/i;)V

    .line 823
    invoke-virtual/range {p0 .. p0}, Lcom/dropbox/android/filemanager/x;->k()Lcom/dropbox/android/taskqueue/H;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/dropbox/android/taskqueue/H;->a(Lcom/dropbox/android/taskqueue/DbTask;)V

    goto :goto_96

    .line 824
    :cond_f9
    iget-wide v7, v6, Lcom/dropbox/android/filemanager/v;->c:J

    cmp-long v1, v1, v7

    if-eqz v1, :cond_96

    .line 827
    sget-object v1, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "File unchanged: "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    iget-wide v1, v6, Lcom/dropbox/android/filemanager/v;->c:J

    const-wide/16 v7, 0x0

    cmp-long v1, v1, v7

    if-lez v1, :cond_96

    .line 830
    sget-object v1, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    const-string v2, "File didn\'t really change, updating local modified time"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 832
    const-string v2, "local_modified"

    iget-wide v6, v6, Lcom/dropbox/android/filemanager/v;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 834
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    invoke-virtual {v2}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 836
    const-string v3, "dropbox"

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 837
    const/4 v2, 0x1

    if-eq v1, v2, :cond_96

    .line 838
    sget-object v2, Lcom/dropbox/android/filemanager/x;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " rows modified in uploadNewVersion update operation, unexpected!"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_96

    .line 792
    :catchall_167
    move-exception v1

    move-object v2, v6

    goto/16 :goto_99

    :cond_16b
    move-object v7, v10

    move-object v8, v11

    move-wide v1, v12

    move-object v3, v14

    goto/16 :goto_67
.end method

.method public final a(Lcom/dropbox/android/filemanager/LocalEntry;)Z
    .registers 7
    .parameter

    .prologue
    .line 367
    new-instance v1, Ldbxyzptlk/k/b;

    invoke-direct {v1}, Ldbxyzptlk/k/b;-><init>()V

    .line 368
    new-instance v2, Ldbxyzptlk/k/i;

    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v0, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v0}, Ldbxyzptlk/k/i;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    .line 369
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->n:Ldbxyzptlk/k/g;

    invoke-virtual {v0, v2, v1}, Ldbxyzptlk/k/g;->a(Ldbxyzptlk/k/i;Ldbxyzptlk/k/f;)V

    .line 371
    :try_start_16
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 372
    iget-object v0, v0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ldbxyzptlk/r/i;->b(Ljava/lang/String;)V

    .line 373
    invoke-direct {p0, p1}, Lcom/dropbox/android/filemanager/x;->e(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 374
    new-instance v3, Lcom/dropbox/android/util/V;

    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    invoke-direct {v3, v0}, Lcom/dropbox/android/util/V;-><init>(Lcom/dropbox/android/provider/h;)V
    :try_end_2b
    .catchall {:try_start_16 .. :try_end_2b} :catchall_4e

    .line 376
    :try_start_2b
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-static {v0, v3, p1}, Lcom/dropbox/android/filemanager/u;->a(Landroid/content/Context;Lcom/dropbox/android/util/V;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 377
    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->b()V
    :try_end_33
    .catchall {:try_start_2b .. :try_end_33} :catchall_49

    .line 379
    :try_start_33
    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->c()V

    .line 381
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->l:Lcom/dropbox/android/taskqueue/q;

    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    iget-object v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/dropbox/android/taskqueue/q;->a(Lcom/dropbox/android/util/DropboxPath;)V
    :try_end_42
    .catchall {:try_start_33 .. :try_end_42} :catchall_4e

    .line 383
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->n:Ldbxyzptlk/k/g;

    invoke-virtual {v0, v2, v1}, Ldbxyzptlk/k/g;->b(Ldbxyzptlk/k/i;Ldbxyzptlk/k/f;)V

    .line 385
    const/4 v0, 0x1

    return v0

    .line 379
    :catchall_49
    move-exception v0

    :try_start_4a
    invoke-virtual {v3}, Lcom/dropbox/android/util/V;->c()V

    throw v0
    :try_end_4e
    .catchall {:try_start_4a .. :try_end_4e} :catchall_4e

    .line 383
    :catchall_4e
    move-exception v0

    iget-object v3, p0, Lcom/dropbox/android/filemanager/x;->n:Ldbxyzptlk/k/g;

    invoke-virtual {v3, v2, v1}, Ldbxyzptlk/k/g;->b(Ldbxyzptlk/k/i;Ldbxyzptlk/k/f;)V

    throw v0
.end method

.method public final b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/k/f;
    .registers 6
    .parameter

    .prologue
    .line 892
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->b()Ldbxyzptlk/k/g;

    move-result-object v0

    new-instance v1, Ldbxyzptlk/k/i;

    new-instance v2, Lcom/dropbox/android/util/DropboxPath;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ldbxyzptlk/k/i;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v0, v1}, Ldbxyzptlk/k/g;->a(Ldbxyzptlk/k/i;)Ldbxyzptlk/k/f;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ldbxyzptlk/k/g;
    .registers 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->n:Ldbxyzptlk/k/g;

    return-object v0
.end method

.method public final c()V
    .registers 2

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/x;->r()V

    .line 172
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/x;->q()Ljava/util/HashSet;

    move-result-object v0

    .line 173
    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/x;->a(Ljava/util/HashSet;)V

    .line 175
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/x;->p()V

    .line 176
    return-void
.end method

.method public final c(Lcom/dropbox/android/filemanager/LocalEntry;)Z
    .registers 3
    .parameter

    .prologue
    .line 897
    invoke-virtual {p0, p1}, Lcom/dropbox/android/filemanager/x;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/k/f;

    move-result-object v0

    .line 898
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ldbxyzptlk/k/f;->a()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final d()V
    .registers 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->h:Lcom/dropbox/android/taskqueue/H;

    if-eqz v0, :cond_9

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->h:Lcom/dropbox/android/taskqueue/H;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/H;->a()V

    .line 184
    :cond_9
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->j:Lcom/dropbox/android/taskqueue/p;

    if-eqz v0, :cond_12

    .line 185
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->j:Lcom/dropbox/android/taskqueue/p;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/p;->b()V

    .line 188
    :cond_12
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->k:Lcom/dropbox/android/taskqueue/i;

    if-eqz v0, :cond_1b

    .line 189
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->k:Lcom/dropbox/android/taskqueue/i;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/i;->b()V

    .line 191
    :cond_1b
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/x;->r()V

    .line 192
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->o:Lcom/dropbox/android/filemanager/D;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/D;->a()V

    .line 195
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/x;->a(Ljava/util/HashSet;)V

    .line 196
    return-void
.end method

.method public final d(Lcom/dropbox/android/filemanager/LocalEntry;)Z
    .registers 3
    .parameter

    .prologue
    .line 903
    invoke-virtual {p0, p1}, Lcom/dropbox/android/filemanager/x;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/k/f;

    move-result-object v0

    .line 904
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Ldbxyzptlk/k/f;->b()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final e()V
    .registers 2

    .prologue
    .line 200
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/filemanager/x;->a(Ljava/util/HashSet;)V

    .line 201
    return-void
.end method

.method public final f()J
    .registers 6

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/dropbox/android/filemanager/x;->q()Ljava/util/HashSet;

    move-result-object v0

    .line 224
    invoke-static {}, Lcom/dropbox/android/util/af;->j()Ljava/io/File;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/dropbox/android/util/af;->a(Ljava/io/File;Ljava/util/Set;)J

    move-result-wide v1

    .line 225
    invoke-static {v0}, Lcom/dropbox/android/util/af;->a(Ljava/util/Set;)J

    move-result-wide v3

    .line 226
    add-long v0, v1, v3

    return-wide v0
.end method

.method public final g()Lcom/dropbox/android/taskqueue/i;
    .registers 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->k:Lcom/dropbox/android/taskqueue/i;

    if-nez v0, :cond_b

    .line 270
    new-instance v0, Lcom/dropbox/android/taskqueue/i;

    invoke-direct {v0}, Lcom/dropbox/android/taskqueue/i;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->k:Lcom/dropbox/android/taskqueue/i;

    .line 273
    :cond_b
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->k:Lcom/dropbox/android/taskqueue/i;

    return-object v0
.end method

.method public final h()Lcom/dropbox/android/taskqueue/q;
    .registers 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->l:Lcom/dropbox/android/taskqueue/q;

    if-nez v0, :cond_b

    .line 279
    new-instance v0, Lcom/dropbox/android/taskqueue/q;

    invoke-direct {v0}, Lcom/dropbox/android/taskqueue/q;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->l:Lcom/dropbox/android/taskqueue/q;

    .line 281
    :cond_b
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->l:Lcom/dropbox/android/taskqueue/q;

    return-object v0
.end method

.method public final i()Lcom/dropbox/android/taskqueue/p;
    .registers 5

    .prologue
    .line 286
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->j:Lcom/dropbox/android/taskqueue/p;

    if-nez v0, :cond_1a

    .line 287
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 288
    new-instance v1, Lcom/dropbox/android/filemanager/A;

    invoke-direct {v1, p0}, Lcom/dropbox/android/filemanager/A;-><init>(Lcom/dropbox/android/filemanager/x;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    new-instance v1, Lcom/dropbox/android/taskqueue/p;

    const/4 v2, 0x1

    const/4 v3, 0x4

    invoke-direct {v1, v2, v3, v0}, Lcom/dropbox/android/taskqueue/p;-><init>(IILjava/util/List;)V

    iput-object v1, p0, Lcom/dropbox/android/filemanager/x;->j:Lcom/dropbox/android/taskqueue/p;

    .line 314
    :cond_1a
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->j:Lcom/dropbox/android/taskqueue/p;

    return-object v0
.end method

.method protected final j()Lcom/dropbox/android/taskqueue/H;
    .registers 6

    .prologue
    .line 320
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Lcom/dropbox/android/taskqueue/UploadTask;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    aput-object v2, v0, v1

    .line 325
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 326
    new-instance v2, Lcom/dropbox/android/filemanager/C;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/dropbox/android/filemanager/C;-><init>(Lcom/dropbox/android/filemanager/x;Lcom/dropbox/android/filemanager/y;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    iget-object v2, p0, Lcom/dropbox/android/filemanager/x;->o:Lcom/dropbox/android/filemanager/D;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 329
    new-instance v2, Lcom/dropbox/android/taskqueue/H;

    iget-object v3, p0, Lcom/dropbox/android/filemanager/x;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/dropbox/android/filemanager/x;->e:Lcom/dropbox/android/provider/h;

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/dropbox/android/taskqueue/H;-><init>(Landroid/content/Context;Lcom/dropbox/android/provider/h;[Ljava/lang/Class;Ljava/util/List;)V

    return-object v2
.end method

.method public final k()Lcom/dropbox/android/taskqueue/H;
    .registers 3

    .prologue
    .line 336
    iget-object v1, p0, Lcom/dropbox/android/filemanager/x;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 337
    :try_start_3
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->h:Lcom/dropbox/android/taskqueue/H;

    if-eqz v0, :cond_b

    .line 338
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->h:Lcom/dropbox/android/taskqueue/H;

    monitor-exit v1

    .line 341
    :goto_a
    return-object v0

    .line 340
    :cond_b
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->j()Lcom/dropbox/android/taskqueue/H;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/filemanager/x;->h:Lcom/dropbox/android/taskqueue/H;

    .line 341
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->h:Lcom/dropbox/android/taskqueue/H;

    monitor-exit v1

    goto :goto_a

    .line 342
    :catchall_15
    move-exception v0

    monitor-exit v1
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_15

    throw v0
.end method

.method public final l()V
    .registers 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->h:Lcom/dropbox/android/taskqueue/H;

    if-eqz v0, :cond_9

    .line 352
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->h:Lcom/dropbox/android/taskqueue/H;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/H;->b()V

    .line 354
    :cond_9
    return-void
.end method

.method public final m()Lcom/dropbox/android/util/ah;
    .registers 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->f:Lcom/dropbox/android/util/ah;

    return-object v0
.end method

.method public final n()V
    .registers 3

    .prologue
    .line 888
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->k()Lcom/dropbox/android/taskqueue/H;

    move-result-object v0

    const-class v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/lang/Class;)V

    .line 889
    return-void
.end method

.method public final o()Lcom/dropbox/android/util/DropboxPath;
    .registers 2

    .prologue
    .line 936
    invoke-virtual {p0}, Lcom/dropbox/android/filemanager/x;->k()Lcom/dropbox/android/taskqueue/H;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/H;->d()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v0

    .line 937
    if-eqz v0, :cond_b

    :goto_a
    return-object v0

    :cond_b
    iget-object v0, p0, Lcom/dropbox/android/filemanager/x;->i:Lcom/dropbox/android/util/DropboxPath;

    goto :goto_a
.end method
