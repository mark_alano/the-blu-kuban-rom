.class public final Lcom/dropbox/android/h;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final abs__ab_bottom_solid_dark_holo:I = 0x7f020000

.field public static final abs__ab_bottom_solid_inverse_holo:I = 0x7f020001

.field public static final abs__ab_bottom_solid_light_holo:I = 0x7f020002

.field public static final abs__ab_bottom_transparent_dark_holo:I = 0x7f020003

.field public static final abs__ab_bottom_transparent_light_holo:I = 0x7f020004

.field public static final abs__ab_share_pack_holo_dark:I = 0x7f020005

.field public static final abs__ab_share_pack_holo_light:I = 0x7f020006

.field public static final abs__ab_solid_dark_holo:I = 0x7f020007

.field public static final abs__ab_solid_light_holo:I = 0x7f020008

.field public static final abs__ab_solid_shadow_holo:I = 0x7f020009

.field public static final abs__ab_stacked_solid_dark_holo:I = 0x7f02000a

.field public static final abs__ab_stacked_solid_light_holo:I = 0x7f02000b

.field public static final abs__ab_stacked_transparent_dark_holo:I = 0x7f02000c

.field public static final abs__ab_stacked_transparent_light_holo:I = 0x7f02000d

.field public static final abs__ab_transparent_dark_holo:I = 0x7f02000e

.field public static final abs__ab_transparent_light_holo:I = 0x7f02000f

.field public static final abs__activated_background_holo_dark:I = 0x7f020010

.field public static final abs__activated_background_holo_light:I = 0x7f020011

.field public static final abs__btn_cab_done_default_holo_dark:I = 0x7f020012

.field public static final abs__btn_cab_done_default_holo_light:I = 0x7f020013

.field public static final abs__btn_cab_done_focused_holo_dark:I = 0x7f020014

.field public static final abs__btn_cab_done_focused_holo_light:I = 0x7f020015

.field public static final abs__btn_cab_done_holo_dark:I = 0x7f020016

.field public static final abs__btn_cab_done_holo_light:I = 0x7f020017

.field public static final abs__btn_cab_done_pressed_holo_dark:I = 0x7f020018

.field public static final abs__btn_cab_done_pressed_holo_light:I = 0x7f020019

.field public static final abs__cab_background_bottom_holo_dark:I = 0x7f02001a

.field public static final abs__cab_background_bottom_holo_light:I = 0x7f02001b

.field public static final abs__cab_background_top_holo_dark:I = 0x7f02001c

.field public static final abs__cab_background_top_holo_light:I = 0x7f02001d

.field public static final abs__dialog_full_holo_dark:I = 0x7f02001e

.field public static final abs__dialog_full_holo_light:I = 0x7f02001f

.field public static final abs__ic_ab_back_holo_dark:I = 0x7f020020

.field public static final abs__ic_ab_back_holo_light:I = 0x7f020021

.field public static final abs__ic_cab_done_holo_dark:I = 0x7f020022

.field public static final abs__ic_cab_done_holo_light:I = 0x7f020023

.field public static final abs__ic_menu_moreoverflow_holo_dark:I = 0x7f020024

.field public static final abs__ic_menu_moreoverflow_holo_light:I = 0x7f020025

.field public static final abs__ic_menu_moreoverflow_normal_holo_dark:I = 0x7f020026

.field public static final abs__ic_menu_moreoverflow_normal_holo_light:I = 0x7f020027

.field public static final abs__ic_menu_share_holo_dark:I = 0x7f020028

.field public static final abs__ic_menu_share_holo_light:I = 0x7f020029

.field public static final abs__item_background_holo_dark:I = 0x7f02002a

.field public static final abs__item_background_holo_light:I = 0x7f02002b

.field public static final abs__list_activated_holo:I = 0x7f02002c

.field public static final abs__list_divider_holo_dark:I = 0x7f02002d

.field public static final abs__list_divider_holo_light:I = 0x7f02002e

.field public static final abs__list_focused_holo:I = 0x7f02002f

.field public static final abs__list_longpressed_holo:I = 0x7f020030

.field public static final abs__list_pressed_holo_dark:I = 0x7f020031

.field public static final abs__list_pressed_holo_light:I = 0x7f020032

.field public static final abs__list_selector_background_transition_holo_dark:I = 0x7f020033

.field public static final abs__list_selector_background_transition_holo_light:I = 0x7f020034

.field public static final abs__list_selector_disabled_holo_dark:I = 0x7f020035

.field public static final abs__list_selector_disabled_holo_light:I = 0x7f020036

.field public static final abs__list_selector_holo_dark:I = 0x7f020037

.field public static final abs__list_selector_holo_light:I = 0x7f020038

.field public static final abs__menu_dropdown_panel_holo_dark:I = 0x7f020039

.field public static final abs__menu_dropdown_panel_holo_light:I = 0x7f02003a

.field public static final abs__progress_bg_holo_dark:I = 0x7f02003b

.field public static final abs__progress_bg_holo_light:I = 0x7f02003c

.field public static final abs__progress_horizontal_holo_dark:I = 0x7f02003d

.field public static final abs__progress_horizontal_holo_light:I = 0x7f02003e

.field public static final abs__progress_medium_holo:I = 0x7f02003f

.field public static final abs__progress_primary_holo_dark:I = 0x7f020040

.field public static final abs__progress_primary_holo_light:I = 0x7f020041

.field public static final abs__progress_secondary_holo_dark:I = 0x7f020042

.field public static final abs__progress_secondary_holo_light:I = 0x7f020043

.field public static final abs__spinner_48_inner_holo:I = 0x7f020044

.field public static final abs__spinner_48_outer_holo:I = 0x7f020045

.field public static final abs__spinner_ab_default_holo_dark:I = 0x7f020046

.field public static final abs__spinner_ab_default_holo_light:I = 0x7f020047

.field public static final abs__spinner_ab_disabled_holo_dark:I = 0x7f020048

.field public static final abs__spinner_ab_disabled_holo_light:I = 0x7f020049

.field public static final abs__spinner_ab_focused_holo_dark:I = 0x7f02004a

.field public static final abs__spinner_ab_focused_holo_light:I = 0x7f02004b

.field public static final abs__spinner_ab_holo_dark:I = 0x7f02004c

.field public static final abs__spinner_ab_holo_light:I = 0x7f02004d

.field public static final abs__spinner_ab_pressed_holo_dark:I = 0x7f02004e

.field public static final abs__spinner_ab_pressed_holo_light:I = 0x7f02004f

.field public static final abs__tab_indicator_ab_holo:I = 0x7f020050

.field public static final abs__tab_selected_focused_holo:I = 0x7f020051

.field public static final abs__tab_selected_holo:I = 0x7f020052

.field public static final abs__tab_selected_pressed_holo:I = 0x7f020053

.field public static final abs__tab_unselected_pressed_holo:I = 0x7f020054

.field public static final actionbar_bg:I = 0x7f020055

.field public static final add_folder:I = 0x7f020056

.field public static final add_folder_light:I = 0x7f020057

.field public static final all_done:I = 0x7f020058

.field public static final bar_dark_bottom:I = 0x7f020059

.field public static final bar_dark_top:I = 0x7f02005a

.field public static final bar_silver_bottom:I = 0x7f02005b

.field public static final bar_silver_top:I = 0x7f02005c

.field public static final battery:I = 0x7f02005d

.field public static final bg_vidcontrol:I = 0x7f02005e

.field public static final black:I = 0x7f02005f

.field public static final blue_arrow_left:I = 0x7f020060

.field public static final blue_arrow_right:I = 0x7f020061

.field public static final btn_blue_arrow_left_focus:I = 0x7f020062

.field public static final btn_blue_arrow_left_focused:I = 0x7f020063

.field public static final btn_blue_arrow_left_focused_pressed:I = 0x7f020064

.field public static final btn_blue_arrow_left_normal:I = 0x7f020065

.field public static final btn_blue_arrow_left_pressed:I = 0x7f020066

.field public static final btn_blue_arrow_left_pressedfocused:I = 0x7f020067

.field public static final btn_blue_arrow_right_focused:I = 0x7f020068

.field public static final btn_blue_arrow_right_focused_pressed:I = 0x7f020069

.field public static final btn_blue_arrow_right_normal:I = 0x7f02006a

.field public static final btn_blue_arrow_right_pressed:I = 0x7f02006b

.field public static final btn_blue_focused:I = 0x7f02006c

.field public static final btn_blue_focused_pressed:I = 0x7f02006d

.field public static final btn_blue_normal:I = 0x7f02006e

.field public static final btn_blue_pressed:I = 0x7f02006f

.field public static final btn_camera_upload:I = 0x7f020070

.field public static final btn_check:I = 0x7f020071

.field public static final btn_check_label_background:I = 0x7f020072

.field public static final btn_check_light:I = 0x7f020073

.field public static final btn_check_light_off:I = 0x7f020074

.field public static final btn_check_light_off_pressed:I = 0x7f020075

.field public static final btn_check_light_off_selected:I = 0x7f020076

.field public static final btn_check_light_on:I = 0x7f020077

.field public static final btn_check_light_on_pressed:I = 0x7f020078

.field public static final btn_check_light_on_selected:I = 0x7f020079

.field public static final btn_check_off:I = 0x7f02007a

.field public static final btn_check_off_pressed:I = 0x7f02007b

.field public static final btn_check_off_selected:I = 0x7f02007c

.field public static final btn_check_on:I = 0x7f02007d

.field public static final btn_check_on_pressed:I = 0x7f02007e

.field public static final btn_check_on_selected:I = 0x7f02007f

.field public static final btn_dark:I = 0x7f020080

.field public static final btn_radio:I = 0x7f020081

.field public static final btn_radio_label_background:I = 0x7f020082

.field public static final btn_radio_off:I = 0x7f020083

.field public static final btn_radio_off_pressed:I = 0x7f020084

.field public static final btn_radio_off_selected:I = 0x7f020085

.field public static final btn_radio_on:I = 0x7f020086

.field public static final btn_radio_on_pressed:I = 0x7f020087

.field public static final btn_radio_on_selected:I = 0x7f020088

.field public static final button_blue:I = 0x7f020089

.field public static final button_dark_gray:I = 0x7f02008a

.field public static final button_dark_gray_disabled:I = 0x7f02008b

.field public static final button_dark_gray_focused:I = 0x7f02008c

.field public static final button_dark_gray_pressed:I = 0x7f02008d

.field public static final button_gray:I = 0x7f02008e

.field public static final button_gray_disabled:I = 0x7f02008f

.field public static final button_gray_focused:I = 0x7f020090

.field public static final button_gray_pressed:I = 0x7f020091

.field public static final button_green:I = 0x7f020092

.field public static final button_green_disabled:I = 0x7f020093

.field public static final button_green_focused:I = 0x7f020094

.field public static final button_green_pressed:I = 0x7f020095

.field public static final button_red:I = 0x7f020096

.field public static final button_red_disabled:I = 0x7f020097

.field public static final button_red_focused:I = 0x7f020098

.field public static final button_red_pressed:I = 0x7f020099

.field public static final button_update:I = 0x7f02009a

.field public static final button_update_icon:I = 0x7f02009b

.field public static final camera_upload_done:I = 0x7f02009c

.field public static final camera_upload_done_2x:I = 0x7f02009d

.field public static final camera_upload_item_bg_color:I = 0x7f02009e

.field public static final camera_upload_item_bottom_line:I = 0x7f02009f

.field public static final camera_uploads_icon:I = 0x7f0200a0

.field public static final camera_uploads_icon_focused:I = 0x7f0200a1

.field public static final camera_uploads_icon_pressed:I = 0x7f0200a2

.field public static final connect_icon_loading:I = 0x7f0200a3

.field public static final connect_icon_unknown:I = 0x7f0200a4

.field public static final copy_to_clipboard:I = 0x7f0200a5

.field public static final cu_promo:I = 0x7f0200a6

.field public static final db_browser_list_item:I = 0x7f0200a7

.field public static final divider_horizontal_dark:I = 0x7f0200a8

.field public static final export:I = 0x7f0200a9

.field public static final export_focused:I = 0x7f0200aa

.field public static final export_pressed:I = 0x7f0200ab

.field public static final fastscroll_thumb_default_holo_blue:I = 0x7f0200ac

.field public static final fastscroll_thumb_default_holo_white:I = 0x7f0200ad

.field public static final fastscroll_thumb_holo:I = 0x7f0200ae

.field public static final fastscroll_thumb_pressed_holo_blue:I = 0x7f0200af

.field public static final fastscroll_thumb_pressed_holo_white:I = 0x7f0200b0

.field public static final favs_hug_2x:I = 0x7f0200b1

.field public static final filelist_highlight:I = 0x7f0200b2

.field public static final folder:I = 0x7f0200b3

.field public static final folder_42:I = 0x7f0200b4

.field public static final folder_4x:I = 0x7f0200b5

.field public static final folder_app:I = 0x7f0200b6

.field public static final folder_app_4x:I = 0x7f0200b7

.field public static final folder_camera:I = 0x7f0200b8

.field public static final folder_camera_4x:I = 0x7f0200b9

.field public static final folder_name_bar_bg:I = 0x7f0200ba

.field public static final folder_photos:I = 0x7f0200bb

.field public static final folder_photos_4x:I = 0x7f0200bc

.field public static final folder_public:I = 0x7f0200bd

.field public static final folder_public_4x:I = 0x7f0200be

.field public static final folder_share:I = 0x7f0200bf

.field public static final folder_star:I = 0x7f0200c0

.field public static final folder_star_4x:I = 0x7f0200c1

.field public static final folder_up:I = 0x7f0200c2

.field public static final folder_user:I = 0x7f0200c3

.field public static final folder_user_4x:I = 0x7f0200c4

.field public static final folderbox:I = 0x7f0200c5

.field public static final gallery_item_checked:I = 0x7f0200c6

.field public static final gallery_item_unchecked:I = 0x7f0200c7

.field public static final gradient_gray_noisy_topstroke:I = 0x7f0200c8

.field public static final gradientbgimg:I = 0x7f0200c9

.field public static final gray_button:I = 0x7f0200ca

.field public static final green_button:I = 0x7f0200cb

.field public static final html:I = 0x7f0200cc

.field public static final hugbox_2x:I = 0x7f0200cd

.field public static final ic_ab_back_holo_dark:I = 0x7f0200ce

.field public static final ic_cancel:I = 0x7f0200cf

.field public static final ic_clear:I = 0x7f0200d0

.field public static final ic_delete:I = 0x7f0200d1

.field public static final ic_dialog_menu_generic:I = 0x7f0200d2

.field public static final ic_export:I = 0x7f0200d3

.field public static final ic_favorite:I = 0x7f0200d4

.field public static final ic_menu_compose:I = 0x7f0200d5

.field public static final ic_menu_new_folder:I = 0x7f0200d6

.field public static final ic_menu_refresh:I = 0x7f0200d7

.field public static final ic_more:I = 0x7f0200d8

.field public static final ic_open:I = 0x7f0200d9

.field public static final ic_rename:I = 0x7f0200da

.field public static final ic_share:I = 0x7f0200db

.field public static final icon:I = 0x7f0200dc

.field public static final image_transition:I = 0x7f0200dd

.field public static final info:I = 0x7f0200de

.field public static final info_button:I = 0x7f0200df

.field public static final info_pressed:I = 0x7f0200e0

.field public static final info_selected:I = 0x7f0200e1

.field public static final internet:I = 0x7f0200e2

.field public static final keynote:I = 0x7f0200e3

.field public static final keynote_4x:I = 0x7f0200e4

.field public static final line:I = 0x7f0200e5

.field public static final link:I = 0x7f0200e6

.field public static final link_button:I = 0x7f0200e7

.field public static final link_focused:I = 0x7f0200e8

.field public static final link_pressed:I = 0x7f0200e9

.field public static final list_divider_holo_light:I = 0x7f0200ea

.field public static final local_browser_list_item:I = 0x7f0200eb

.field public static final more:I = 0x7f0200ec

.field public static final more_focused:I = 0x7f0200ed

.field public static final more_pressed:I = 0x7f0200ee

.field public static final nessie:I = 0x7f0200ef

.field public static final noise:I = 0x7f0200f0

.field public static final nook_spinner_bg:I = 0x7f0200f1

.field public static final notification:I = 0x7f0200f2

.field public static final numbers:I = 0x7f0200f3

.field public static final numbers_4x:I = 0x7f0200f4

.field public static final open:I = 0x7f0200f5

.field public static final open_focused:I = 0x7f0200f6

.field public static final open_pressed:I = 0x7f0200f7

.field public static final package_icon:I = 0x7f0200f8

.field public static final package_icon_4x:I = 0x7f0200f9

.field public static final page_white:I = 0x7f0200fa

.field public static final page_white_4x:I = 0x7f0200fb

.field public static final page_white_acrobat:I = 0x7f0200fc

.field public static final page_white_acrobat_4x:I = 0x7f0200fd

.field public static final page_white_actionscript:I = 0x7f0200fe

.field public static final page_white_actionscript_4x:I = 0x7f0200ff

.field public static final page_white_c:I = 0x7f020100

.field public static final page_white_c_4x:I = 0x7f020101

.field public static final page_white_code:I = 0x7f020102

.field public static final page_white_code_4x:I = 0x7f020103

.field public static final page_white_compressed:I = 0x7f020104

.field public static final page_white_compressed_4x:I = 0x7f020105

.field public static final page_white_cplusplus:I = 0x7f020106

.field public static final page_white_cplusplus_4x:I = 0x7f020107

.field public static final page_white_csharp:I = 0x7f020108

.field public static final page_white_csharp_4x:I = 0x7f020109

.field public static final page_white_cup:I = 0x7f02010a

.field public static final page_white_cup_4x:I = 0x7f02010b

.field public static final page_white_dvd:I = 0x7f02010c

.field public static final page_white_dvd_4x:I = 0x7f02010d

.field public static final page_white_excel:I = 0x7f02010e

.field public static final page_white_excel_4x:I = 0x7f02010f

.field public static final page_white_film:I = 0x7f020110

.field public static final page_white_film_4x:I = 0x7f020111

.field public static final page_white_flash:I = 0x7f020112

.field public static final page_white_flash_4x:I = 0x7f020113

.field public static final page_white_gear:I = 0x7f020114

.field public static final page_white_gear_4x:I = 0x7f020115

.field public static final page_white_h:I = 0x7f020116

.field public static final page_white_h_4x:I = 0x7f020117

.field public static final page_white_java:I = 0x7f020118

.field public static final page_white_js:I = 0x7f020119

.field public static final page_white_js_4x:I = 0x7f02011a

.field public static final page_white_paint:I = 0x7f02011b

.field public static final page_white_paint_4x:I = 0x7f02011c

.field public static final page_white_php:I = 0x7f02011d

.field public static final page_white_php_4x:I = 0x7f02011e

.field public static final page_white_picture:I = 0x7f02011f

.field public static final page_white_picture_4x:I = 0x7f020120

.field public static final page_white_powerpoint:I = 0x7f020121

.field public static final page_white_powerpoint_4x:I = 0x7f020122

.field public static final page_white_py:I = 0x7f020123

.field public static final page_white_py_4x:I = 0x7f020124

.field public static final page_white_ruby:I = 0x7f020125

.field public static final page_white_ruby_4x:I = 0x7f020126

.field public static final page_white_sound:I = 0x7f020127

.field public static final page_white_sound_4x:I = 0x7f020128

.field public static final page_white_text:I = 0x7f020129

.field public static final page_white_text_4x:I = 0x7f02012a

.field public static final page_white_tux:I = 0x7f02012b

.field public static final page_white_tux_4x:I = 0x7f02012c

.field public static final page_white_vector:I = 0x7f02012d

.field public static final page_white_vector_4x:I = 0x7f02012e

.field public static final page_white_visualstudio:I = 0x7f02012f

.field public static final page_white_visualstudio_4x:I = 0x7f020130

.field public static final page_white_word:I = 0x7f020131

.field public static final page_white_word_4x:I = 0x7f020132

.field public static final pages:I = 0x7f020133

.field public static final pages_4x:I = 0x7f020134

.field public static final pencil:I = 0x7f020135

.field public static final pencil_focused:I = 0x7f020136

.field public static final pencil_pressed:I = 0x7f020137

.field public static final photo_check:I = 0x7f020138

.field public static final photo_close:I = 0x7f020139

.field public static final photo_collections:I = 0x7f02013a

.field public static final photo_delete:I = 0x7f02013b

.field public static final photo_frame:I = 0x7f02013c

.field public static final photo_link:I = 0x7f02013d

.field public static final photo_share:I = 0x7f02013e

.field public static final progress_bg_holo_light:I = 0x7f02013f

.field public static final progress_horizontal_holo_light:I = 0x7f020140

.field public static final progress_primary_holo_light:I = 0x7f020141

.field public static final progress_secondary_holo_light:I = 0x7f020142

.field public static final progressbar_indeterminate_holo1:I = 0x7f020143

.field public static final progressbar_indeterminate_holo2:I = 0x7f020144

.field public static final progressbar_indeterminate_holo3:I = 0x7f020145

.field public static final progressbar_indeterminate_holo4:I = 0x7f020146

.field public static final progressbar_indeterminate_holo5:I = 0x7f020147

.field public static final progressbar_indeterminate_holo6:I = 0x7f020148

.field public static final progressbar_indeterminate_holo7:I = 0x7f020149

.field public static final progressbar_indeterminate_holo8:I = 0x7f02014a

.field public static final qa_cancel:I = 0x7f02014b

.field public static final qa_cancel_focused:I = 0x7f02014c

.field public static final qa_cancel_pressed:I = 0x7f02014d

.field public static final qa_clear:I = 0x7f02014e

.field public static final qa_clear_focused:I = 0x7f02014f

.field public static final qa_clear_pressed:I = 0x7f020150

.field public static final quickaction_arrow:I = 0x7f020151

.field public static final quickaction_arrow_active:I = 0x7f020152

.field public static final quickaction_arrow_disabled:I = 0x7f020153

.field public static final quickaction_arrow_inactive:I = 0x7f020154

.field public static final quickaction_arrow_pressed:I = 0x7f020155

.field public static final quickactionsoverlay:I = 0x7f020156

.field public static final quickactionsoverlay_opaque:I = 0x7f020157

.field public static final quickactionsoverlay_up:I = 0x7f020158

.field public static final quickactionsoverlay_up_opaque:I = 0x7f020159

.field public static final red_button:I = 0x7f02015a

.field public static final right_chevron:I = 0x7f02015b

.field public static final sdcard:I = 0x7f02015c

.field public static final silver_bottom_bar:I = 0x7f02015d

.field public static final small_star:I = 0x7f02015e

.field public static final small_star_grey:I = 0x7f02015f

.field public static final space:I = 0x7f020160

.field public static final star_empty:I = 0x7f020161

.field public static final star_empty_focused:I = 0x7f020162

.field public static final star_empty_pressed:I = 0x7f020163

.field public static final star_full:I = 0x7f020164

.field public static final star_full_focused:I = 0x7f020165

.field public static final star_full_pressed:I = 0x7f020166

.field public static final sym_keyboard_delete:I = 0x7f020167

.field public static final sym_keyboard_num0:I = 0x7f020168

.field public static final sym_keyboard_num1:I = 0x7f020169

.field public static final sym_keyboard_num2:I = 0x7f02016a

.field public static final sym_keyboard_num3:I = 0x7f02016b

.field public static final sym_keyboard_num4:I = 0x7f02016c

.field public static final sym_keyboard_num5:I = 0x7f02016d

.field public static final sym_keyboard_num6:I = 0x7f02016e

.field public static final sym_keyboard_num7:I = 0x7f02016f

.field public static final sym_keyboard_num8:I = 0x7f020170

.field public static final sym_keyboard_num9:I = 0x7f020171

.field public static final tab_dropbox:I = 0x7f020172

.field public static final tab_dropbox_inactive:I = 0x7f020173

.field public static final tab_focused_holo:I = 0x7f020174

.field public static final tab_indicator:I = 0x7f020175

.field public static final tab_more:I = 0x7f020176

.field public static final tab_notifications:I = 0x7f020177

.field public static final tab_photos:I = 0x7f020178

.field public static final tab_pressed_holo_dark:I = 0x7f020179

.field public static final tab_search:I = 0x7f02017a

.field public static final tab_selected_focused_holo:I = 0x7f02017b

.field public static final tab_selected_holo:I = 0x7f02017c

.field public static final tab_selected_pressed_holo:I = 0x7f02017d

.field public static final tab_star:I = 0x7f02017e

.field public static final text_entry:I = 0x7f02017f

.field public static final textb:I = 0x7f020180

.field public static final thumb_outline:I = 0x7f020181

.field public static final thumbnail_loading:I = 0x7f020182

.field public static final tour_splash:I = 0x7f020183

.field public static final trash:I = 0x7f020184

.field public static final trash_focused:I = 0x7f020185

.field public static final trash_pressed:I = 0x7f020186

.field public static final update_button:I = 0x7f020187

.field public static final update_button_disabled:I = 0x7f020188

.field public static final update_button_pressed:I = 0x7f020189

.field public static final update_button_selected:I = 0x7f02018a

.field public static final update_icon:I = 0x7f02018b

.field public static final update_icon_disabled:I = 0x7f02018c

.field public static final uploads_bg_2x:I = 0x7f02018d

.field public static final video_icon:I = 0x7f02018e

.field public static final welcome_logo:I = 0x7f02018f

.field public static final welcome_logo_name:I = 0x7f020190

.field public static final welcome_logo_no_play:I = 0x7f020191

.field public static final white_dropbox:I = 0x7f020192

.field public static final white_glow_on_press:I = 0x7f020193

.field public static final white_left_arrow:I = 0x7f020194

.field public static final white_with_border_bg:I = 0x7f020195

.field public static final wifi:I = 0x7f020196


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
