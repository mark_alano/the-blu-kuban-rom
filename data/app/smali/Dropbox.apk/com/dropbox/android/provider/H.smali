.class public final Lcom/dropbox/android/provider/H;
.super Landroid/database/AbstractCursor;
.source "panda.py"


# instance fields
.field private a:Landroid/database/DataSetObserver;

.field private final b:Landroid/database/Cursor;

.field private final c:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;Ljava/util/Comparator;)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 13
    new-instance v0, Lcom/dropbox/android/provider/I;

    invoke-direct {v0, p0}, Lcom/dropbox/android/provider/I;-><init>(Lcom/dropbox/android/provider/H;)V

    iput-object v0, p0, Lcom/dropbox/android/provider/H;->a:Landroid/database/DataSetObserver;

    .line 29
    iput-object p1, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    .line 33
    new-instance v1, Lcom/dropbox/android/provider/J;

    invoke-direct {v1, p0, p2}, Lcom/dropbox/android/provider/J;-><init>(Lcom/dropbox/android/provider/H;Ljava/util/Comparator;)V

    .line 41
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/dropbox/android/provider/H;->c:[Ljava/lang/Integer;

    .line 42
    const/4 v0, 0x0

    :goto_1c
    iget-object v2, p0, Lcom/dropbox/android/provider/H;->c:[Ljava/lang/Integer;

    array-length v2, v2

    if-ge v0, v2, :cond_2c

    .line 43
    iget-object v2, p0, Lcom/dropbox/android/provider/H;->c:[Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 45
    :cond_2c
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->c:[Ljava/lang/Integer;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 47
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_3c

    .line 48
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/provider/H;->a:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 50
    :cond_3c
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/provider/H;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 12
    iput p1, p0, Lcom/dropbox/android/provider/H;->mPos:I

    return p1
.end method

.method static synthetic a(Lcom/dropbox/android/provider/H;)Landroid/database/Cursor;
    .registers 2
    .parameter

    .prologue
    .line 12
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/provider/H;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 12
    iput p1, p0, Lcom/dropbox/android/provider/H;->mPos:I

    return p1
.end method


# virtual methods
.method public final close()V
    .registers 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_9

    .line 131
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 133
    :cond_9
    return-void
.end method

.method public final deactivate()V
    .registers 3

    .prologue
    .line 125
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "deactivate not supported by Sort, this is deprecated anyway"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getBlob(I)[B
    .registers 3
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .registers 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_b

    .line 117
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 119
    :goto_a
    return-object v0

    :cond_b
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_a
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_a
    return v0

    :cond_b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public final getDouble(I)D
    .registers 4
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getFloat(I)F
    .registers 3
    .parameter

    .prologue
    .line 90
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final getInt(I)I
    .registers 3
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getLong(I)J
    .registers 4
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getShort(I)S
    .registers 3
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 70
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getType(I)I
    .registers 3
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public final isNull(I)Z
    .registers 3
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public final onMove(II)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    if-nez v0, :cond_6

    .line 60
    const/4 v0, 0x0

    .line 64
    :goto_5
    return v0

    .line 61
    :cond_6
    const/4 v0, -0x1

    if-le p2, v0, :cond_11

    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lt p2, v0, :cond_18

    .line 62
    :cond_11
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    goto :goto_5

    .line 64
    :cond_18
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/provider/H;->c:[Ljava/lang/Integer;

    aget-object v1, v1, p2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    goto :goto_5
.end method

.method public final registerContentObserver(Landroid/database/ContentObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_9

    .line 138
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 140
    :cond_9
    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 151
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_9

    .line 152
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 154
    :cond_9
    return-void
.end method

.method public final requery()Z
    .registers 3

    .prologue
    .line 165
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "requery not supported by MergeSortedCursor, this is deprecated anyway"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final unregisterContentObserver(Landroid/database/ContentObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_9

    .line 145
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 147
    :cond_9
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    if-eqz v0, :cond_9

    .line 159
    iget-object v0, p0, Lcom/dropbox/android/provider/H;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 161
    :cond_9
    return-void
.end method
