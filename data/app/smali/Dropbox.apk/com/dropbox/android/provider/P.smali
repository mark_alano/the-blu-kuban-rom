.class public final enum Lcom/dropbox/android/provider/P;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/provider/P;

.field public static final enum b:Lcom/dropbox/android/provider/P;

.field public static final enum c:Lcom/dropbox/android/provider/P;

.field public static final enum d:Lcom/dropbox/android/provider/P;

.field public static final enum e:Lcom/dropbox/android/provider/P;

.field public static final enum f:Lcom/dropbox/android/provider/P;

.field private static final synthetic h:[Lcom/dropbox/android/provider/P;


# instance fields
.field private final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    new-instance v0, Lcom/dropbox/android/provider/P;

    const-string v1, "DROPBOX_ENTRY"

    const-string v2, "DropboxEntry"

    invoke-direct {v0, v1, v4, v2}, Lcom/dropbox/android/provider/P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/P;->a:Lcom/dropbox/android/provider/P;

    .line 36
    new-instance v0, Lcom/dropbox/android/provider/P;

    const-string v1, "UP_FOLDER"

    const-string v2, "_up_folder"

    invoke-direct {v0, v1, v5, v2}, Lcom/dropbox/android/provider/P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/P;->b:Lcom/dropbox/android/provider/P;

    .line 37
    new-instance v0, Lcom/dropbox/android/provider/P;

    const-string v1, "IN_PROGRESS_UPLOAD"

    const-string v2, "_upload_in_progress"

    invoke-direct {v0, v1, v6, v2}, Lcom/dropbox/android/provider/P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/P;->c:Lcom/dropbox/android/provider/P;

    .line 38
    new-instance v0, Lcom/dropbox/android/provider/P;

    const-string v1, "CAMERA_UPLOAD_STATUS"

    const-string v2, "_camera_upload_status_item"

    invoke-direct {v0, v1, v7, v2}, Lcom/dropbox/android/provider/P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/P;->d:Lcom/dropbox/android/provider/P;

    .line 39
    new-instance v0, Lcom/dropbox/android/provider/P;

    const-string v1, "SEPARATOR"

    const-string v2, "_separator"

    invoke-direct {v0, v1, v8, v2}, Lcom/dropbox/android/provider/P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/P;->e:Lcom/dropbox/android/provider/P;

    .line 40
    new-instance v0, Lcom/dropbox/android/provider/P;

    const-string v1, "TURN_ON"

    const/4 v2, 0x5

    const-string v3, "turn_camera_upload_on"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/provider/P;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/P;->f:Lcom/dropbox/android/provider/P;

    .line 34
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dropbox/android/provider/P;

    sget-object v1, Lcom/dropbox/android/provider/P;->a:Lcom/dropbox/android/provider/P;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/provider/P;->b:Lcom/dropbox/android/provider/P;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/provider/P;->c:Lcom/dropbox/android/provider/P;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/provider/P;->d:Lcom/dropbox/android/provider/P;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dropbox/android/provider/P;->e:Lcom/dropbox/android/provider/P;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/provider/P;->f:Lcom/dropbox/android/provider/P;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/provider/P;->h:[Lcom/dropbox/android/provider/P;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput-object p3, p0, Lcom/dropbox/android/provider/P;->g:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;
    .registers 7
    .parameter

    .prologue
    .line 50
    const-string v0, "_cursor_type_tag"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 51
    const/4 v1, -0x1

    if-ne v0, v1, :cond_c

    .line 53
    sget-object v0, Lcom/dropbox/android/provider/P;->a:Lcom/dropbox/android/provider/P;

    .line 58
    :cond_b
    return-object v0

    .line 55
    :cond_c
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 56
    invoke-static {}, Lcom/dropbox/android/provider/P;->values()[Lcom/dropbox/android/provider/P;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_17
    if-ge v1, v4, :cond_27

    aget-object v0, v3, v1

    .line 57
    iget-object v5, v0, Lcom/dropbox/android/provider/P;->g:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_b

    .line 56
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_17

    .line 61
    :cond_27
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type of tagged cursor entry for FileListCursorAdapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/provider/P;
    .registers 2
    .parameter

    .prologue
    .line 34
    const-class v0, Lcom/dropbox/android/provider/P;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/provider/P;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/provider/P;
    .registers 1

    .prologue
    .line 34
    sget-object v0, Lcom/dropbox/android/provider/P;->h:[Lcom/dropbox/android/provider/P;

    invoke-virtual {v0}, [Lcom/dropbox/android/provider/P;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/provider/P;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/dropbox/android/provider/P;->g:Ljava/lang/String;

    return-object v0
.end method
