.class public final Lcom/dropbox/android/provider/n;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(ZLandroid/database/Cursor;)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 44
    if-eqz p0, :cond_15

    .line 45
    const-string v0, "local_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 46
    invoke-static {v0}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 49
    :goto_14
    return-object v0

    :cond_15
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_14
.end method

.method private static a(Landroid/database/Cursor;)Z
    .registers 3
    .parameter

    .prologue
    .line 34
    const-string v0, "_cursor_type_tag"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 35
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1b

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/P;->c:Lcom/dropbox/android/provider/P;

    invoke-virtual {v1}, Lcom/dropbox/android/provider/P;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    const/4 v0, 0x1

    :goto_1a
    return v0

    :cond_1b
    const/4 v0, 0x0

    goto :goto_1a
.end method

.method private static b(Landroid/database/Cursor;)Z
    .registers 3
    .parameter

    .prologue
    .line 39
    const-string v0, "is_dir"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 40
    const/4 v1, -0x1

    if-eq v0, v1, :cond_11

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Landroid/database/Cursor;)I
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 18
    invoke-static {p1}, Lcom/dropbox/android/provider/n;->a(Landroid/database/Cursor;)Z

    move-result v3

    .line 19
    invoke-static {p2}, Lcom/dropbox/android/provider/n;->a(Landroid/database/Cursor;)Z

    move-result v4

    .line 20
    if-nez v3, :cond_22

    invoke-static {p1}, Lcom/dropbox/android/provider/n;->b(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_22

    move v2, v0

    .line 21
    :goto_13
    if-nez v4, :cond_1c

    invoke-static {p2}, Lcom/dropbox/android/provider/n;->b(Landroid/database/Cursor;)Z

    move-result v5

    if-eqz v5, :cond_1c

    move v1, v0

    .line 22
    :cond_1c
    if-eqz v2, :cond_24

    if-nez v1, :cond_24

    .line 23
    const/4 v0, -0x1

    .line 27
    :cond_21
    :goto_21
    return v0

    :cond_22
    move v2, v1

    .line 20
    goto :goto_13

    .line 24
    :cond_24
    if-eqz v1, :cond_28

    if-eqz v2, :cond_21

    .line 27
    :cond_28
    invoke-static {v3, p1}, Lcom/dropbox/android/provider/n;->a(ZLandroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, p2}, Lcom/dropbox/android/provider/n;->a(ZLandroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/aT;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_21
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 14
    check-cast p1, Landroid/database/Cursor;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/dropbox/android/provider/n;->a(Landroid/database/Cursor;Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method
