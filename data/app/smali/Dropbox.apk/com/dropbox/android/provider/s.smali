.class final Lcom/dropbox/android/provider/s;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/text/DateFormat;


# instance fields
.field private final b:Ljava/util/List;

.field private final c:Ljava/util/List;

.field private final d:Ljava/util/Calendar;

.field private e:Ljava/util/Calendar;

.field private f:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 58
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMMM yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/provider/s;->a:Ljava/text/DateFormat;

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/s;->b:Ljava/util/List;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/provider/s;->c:Ljava/util/List;

    .line 65
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/Calendar;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/provider/s;->f:Ljava/util/Calendar;

    .line 70
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/provider/s;->d:Ljava/util/Calendar;

    .line 71
    invoke-direct {p0}, Lcom/dropbox/android/provider/s;->a()V

    .line 72
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/provider/s;->f:Ljava/util/Calendar;

    .line 73
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->f:Ljava/util/Calendar;

    const-wide v1, 0x7fffffffffffffffL

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 74
    return-void
.end method

.method private a(J)Ljava/lang/String;
    .registers 13
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v1, 0x1

    .line 78
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/Calendar;

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v4, p0, Lcom/dropbox/android/provider/s;->f:Ljava/util/Calendar;

    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v0, v4, :cond_4e

    iget-object v0, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v4, p0, Lcom/dropbox/android/provider/s;->f:Ljava/util/Calendar;

    invoke-virtual {v4, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    if-ne v0, v4, :cond_4e

    move v0, v1

    .line 82
    :goto_26
    iget-object v4, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/Calendar;

    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v5, p0, Lcom/dropbox/android/provider/s;->d:Ljava/util/Calendar;

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_43

    iget-object v4, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/Calendar;

    invoke-virtual {v4, v1}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v5, p0, Lcom/dropbox/android/provider/s;->d:Ljava/util/Calendar;

    invoke-virtual {v5, v1}, Ljava/util/Calendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_43

    move v2, v1

    .line 87
    :cond_43
    if-eqz v0, :cond_50

    if-nez v2, :cond_50

    move-object v0, v3

    .line 103
    :goto_48
    iget-object v1, p0, Lcom/dropbox/android/provider/s;->f:Ljava/util/Calendar;

    invoke-virtual {v1, p1, p2}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 104
    return-object v0

    :cond_4e
    move v0, v2

    .line 80
    goto :goto_26

    .line 89
    :cond_50
    if-nez v2, :cond_63

    if-nez v0, :cond_63

    .line 91
    sget-object v0, Lcom/dropbox/android/provider/s;->a:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/dropbox/android/provider/s;->e:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_48

    .line 94
    :cond_61
    add-int/lit8 v1, v1, 0x1

    :cond_63
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a0

    .line 95
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->b:Ljava/util/List;

    add-int/lit8 v2, v1, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->f:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    cmp-long v0, v8, v6

    if-ltz v0, :cond_61

    cmp-long v0, v6, p1

    if-lez v0, :cond_61

    cmp-long v0, p1, v4

    if-ltz v0, :cond_61

    .line 98
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_48

    :cond_a0
    move-object v0, v3

    goto :goto_48
.end method

.method static synthetic a(Lcom/dropbox/android/provider/s;J)Ljava/lang/String;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/provider/s;->a(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .registers 11

    .prologue
    const/4 v9, 0x7

    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v6, -0x1

    .line 114
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 115
    invoke-static {}, Lcom/dropbox/android/provider/r;->a()Ljava/util/Calendar;

    move-result-object v1

    .line 116
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/dropbox/android/provider/r;->a(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    .line 118
    const-wide v3, 0x7fffffffffffffffL

    const/4 v5, 0x0

    invoke-direct {p0, v3, v4, v5}, Lcom/dropbox/android/provider/s;->a(JLjava/lang/String;)V

    .line 121
    invoke-static {}, Lcom/dropbox/android/provider/r;->a()Ljava/util/Calendar;

    move-result-object v3

    .line 122
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    const v5, 0x7f0b0134

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/dropbox/android/provider/s;->a(JLjava/lang/String;)V

    .line 125
    invoke-static {}, Lcom/dropbox/android/provider/r;->a()Ljava/util/Calendar;

    move-result-object v3

    .line 126
    invoke-virtual {v3, v8, v6}, Ljava/util/Calendar;->add(II)V

    .line 127
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 128
    cmp-long v5, v3, v1

    if-ltz v5, :cond_50

    .line 129
    const v5, 0x7f0b0135

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/dropbox/android/provider/s;->a(JLjava/lang/String;)V

    .line 133
    :cond_50
    invoke-static {}, Lcom/dropbox/android/provider/r;->a()Ljava/util/Calendar;

    move-result-object v3

    .line 134
    invoke-virtual {v3, v9, v7}, Ljava/util/Calendar;->set(II)V

    .line 135
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 136
    cmp-long v5, v3, v1

    if-ltz v5, :cond_69

    .line 137
    const v5, 0x7f0b0136

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/dropbox/android/provider/s;->a(JLjava/lang/String;)V

    .line 141
    :cond_69
    invoke-static {}, Lcom/dropbox/android/provider/r;->a()Ljava/util/Calendar;

    move-result-object v3

    .line 142
    invoke-virtual {v3, v9, v7}, Ljava/util/Calendar;->set(II)V

    .line 143
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v6}, Ljava/util/Calendar;->add(II)V

    .line 144
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    .line 145
    cmp-long v1, v3, v1

    if-ltz v1, :cond_86

    .line 146
    const v1, 0x7f0b0137

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v3, v4, v1}, Lcom/dropbox/android/provider/s;->a(JLjava/lang/String;)V

    .line 149
    :cond_86
    invoke-static {}, Lcom/dropbox/android/provider/r;->a()Ljava/util/Calendar;

    move-result-object v1

    .line 150
    const/4 v2, 0x1

    invoke-virtual {v1, v8, v2}, Ljava/util/Calendar;->set(II)V

    .line 151
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    .line 152
    const v3, 0x7f0b0138

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/dropbox/android/provider/s;->a(JLjava/lang/String;)V

    .line 153
    return-void
.end method

.method private a(JLjava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_21

    iget-object v0, p0, Lcom/dropbox/android/provider/s;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/dropbox/android/provider/s;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_21

    .line 164
    :goto_20
    return-void

    .line 162
    :cond_21
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->b:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v0, p0, Lcom/dropbox/android/provider/s;->c:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_20
.end method
