.class public final Lcom/dropbox/android/provider/t;
.super Landroid/database/AbstractCursor;
.source "panda.py"


# instance fields
.field private final a:Landroid/database/DataSetObserver;

.field private final b:I

.field private final c:Ljava/util/ArrayList;

.field private final d:[Landroid/database/Cursor;


# direct methods
.method public constructor <init>([Landroid/database/Cursor;Ljava/util/Comparator;)V
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 24
    new-instance v0, Lcom/dropbox/android/provider/u;

    invoke-direct {v0, p0}, Lcom/dropbox/android/provider/u;-><init>(Lcom/dropbox/android/provider/t;)V

    iput-object v0, p0, Lcom/dropbox/android/provider/t;->a:Landroid/database/DataSetObserver;

    .line 41
    iput-object p1, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    .line 44
    array-length v3, p1

    move v1, v2

    move v0, v2

    :goto_10
    if-ge v1, v3, :cond_1e

    aget-object v4, p1, v1

    .line 45
    if-eqz v4, :cond_1b

    .line 46
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v4

    add-int/2addr v0, v4

    .line 44
    :cond_1b
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 49
    :cond_1e
    iput v0, p0, Lcom/dropbox/android/provider/t;->b:I

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->b:I

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    .line 52
    new-instance v3, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 53
    array-length v1, p1

    move v0, v2

    :goto_31
    if-ge v0, v1, :cond_61

    aget-object v4, p1, v0

    .line 54
    if-eqz v4, :cond_40

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_40

    .line 55
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    :cond_40
    add-int/lit8 v0, v0, 0x1

    goto :goto_31

    .line 69
    :cond_43
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    new-instance v4, Landroid/util/Pair;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 71
    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-eqz v0, :cond_61

    .line 72
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 59
    :cond_61
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8c

    .line 60
    const/4 v1, 0x0

    .line 61
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_82

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 62
    if-eqz v1, :cond_80

    invoke-interface {p2, v1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v5

    if-lez v5, :cond_a0

    :cond_80
    :goto_80
    move-object v1, v0

    .line 63
    goto :goto_6c

    .line 66
    :cond_82
    if-nez v1, :cond_43

    .line 67
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Canidate should never be null.  PC LOAD LETTER!?!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_8c
    iget-object v1, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    array-length v3, v1

    move v0, v2

    :goto_90
    if-ge v0, v3, :cond_9f

    aget-object v2, v1, v0

    .line 78
    if-nez v2, :cond_99

    .line 77
    :goto_96
    add-int/lit8 v0, v0, 0x1

    goto :goto_90

    .line 80
    :cond_99
    iget-object v4, p0, Lcom/dropbox/android/provider/t;->a:Landroid/database/DataSetObserver;

    invoke-interface {v2, v4}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_96

    .line 82
    :cond_9f
    return-void

    :cond_a0
    move-object v0, v1

    goto :goto_80
.end method

.method static synthetic a(Lcom/dropbox/android/provider/t;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 22
    iput p1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/provider/t;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 22
    iput p1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    return p1
.end method


# virtual methods
.method public final close()V
    .registers 4

    .prologue
    .line 179
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 180
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v1, :cond_17

    .line 181
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-nez v2, :cond_f

    .line 180
    :goto_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 184
    :cond_f
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_c

    .line 186
    :cond_17
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 187
    return-void
.end method

.method public final deactivate()V
    .registers 3

    .prologue
    .line 174
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "deactivate not supported by MergeSortedCursor, this is deprecated anyway"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getBlob(I)[B
    .registers 4
    .parameter

    .prologue
    .line 152
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 157
    iget v0, p0, Lcom/dropbox/android/provider/t;->mPos:I

    if-ltz v0, :cond_1e

    iget v0, p0, Lcom/dropbox/android/provider/t;->mPos:I

    iget v2, p0, Lcom/dropbox/android/provider/t;->b:I

    if-ge v0, v2, :cond_1e

    .line 158
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 168
    :goto_1d
    return-object v0

    .line 162
    :cond_1e
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    array-length v3, v2

    move v0, v1

    :goto_22
    if-ge v0, v3, :cond_30

    aget-object v4, v2, v0

    .line 163
    if-eqz v4, :cond_2d

    .line 164
    invoke-interface {v4}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    goto :goto_1d

    .line 162
    :cond_2d
    add-int/lit8 v0, v0, 0x1

    goto :goto_22

    .line 168
    :cond_30
    new-array v0, v1, [Ljava/lang/String;

    goto :goto_1d
.end method

.method public final getCount()I
    .registers 2

    .prologue
    .line 87
    iget v0, p0, Lcom/dropbox/android/provider/t;->b:I

    return v0
.end method

.method public final getDouble(I)D
    .registers 4
    .parameter

    .prologue
    .line 136
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getFloat(I)F
    .registers 4
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final getInt(I)I
    .registers 4
    .parameter

    .prologue
    .line 121
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getLong(I)J
    .registers 4
    .parameter

    .prologue
    .line 126
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getShort(I)S
    .registers 4
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getType(I)I
    .registers 4
    .parameter

    .prologue
    .line 142
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public final isNull(I)Z
    .registers 4
    .parameter

    .prologue
    .line 147
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    iget v1, p0, Lcom/dropbox/android/provider/t;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public final onMove(II)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 93
    const/4 v1, -0x1

    if-ge p2, v1, :cond_5

    .line 105
    :cond_4
    :goto_4
    return v0

    .line 95
    :cond_5
    iget v1, p0, Lcom/dropbox/android/provider/t;->b:I

    if-gt p2, v1, :cond_4

    .line 98
    if-ltz p2, :cond_26

    iget v0, p0, Lcom/dropbox/android/provider/t;->b:I

    if-ge p2, v0, :cond_26

    .line 102
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 103
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/database/Cursor;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 105
    :cond_26
    const/4 v0, 0x1

    goto :goto_4
.end method

.method public final registerContentObserver(Landroid/database/ContentObserver;)V
    .registers 5
    .parameter

    .prologue
    .line 191
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 192
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v1, :cond_16

    .line 193
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_13

    .line 194
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 192
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 197
    :cond_16
    return-void
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 5
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 211
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v1, :cond_16

    .line 212
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_13

    .line 213
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 211
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 216
    :cond_16
    return-void
.end method

.method public final requery()Z
    .registers 3

    .prologue
    .line 230
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "requery not supported by MergeSortedCursor, this is deprecated anyway"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final unregisterContentObserver(Landroid/database/ContentObserver;)V
    .registers 5
    .parameter

    .prologue
    .line 200
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 201
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v1, :cond_16

    .line 202
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_13

    .line 203
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 201
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 206
    :cond_16
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 5
    .parameter

    .prologue
    .line 220
    iget-object v0, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    array-length v1, v0

    .line 221
    const/4 v0, 0x0

    :goto_4
    if-ge v0, v1, :cond_16

    .line 222
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_13

    .line 223
    iget-object v2, p0, Lcom/dropbox/android/provider/t;->d:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 221
    :cond_13
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 226
    :cond_16
    return-void
.end method
