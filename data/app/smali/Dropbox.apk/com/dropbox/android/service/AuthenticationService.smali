.class public Lcom/dropbox/android/service/AuthenticationService;
.super Landroid/app/Service;
.source "panda.py"


# instance fields
.field private a:Ldbxyzptlk/h/a;

.field private b:Landroid/accounts/OnAccountsUpdateListener;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/service/AuthenticationService;->a:Ldbxyzptlk/h/a;

    invoke-virtual {v0}, Ldbxyzptlk/h/a;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .registers 5

    .prologue
    .line 21
    const-string v0, "create"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 22
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 24
    new-instance v0, Ldbxyzptlk/h/a;

    invoke-direct {v0, p0}, Ldbxyzptlk/h/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dropbox/android/service/AuthenticationService;->a:Ldbxyzptlk/h/a;

    .line 26
    new-instance v0, Lcom/dropbox/android/service/a;

    invoke-direct {v0, p0}, Lcom/dropbox/android/service/a;-><init>(Lcom/dropbox/android/service/AuthenticationService;)V

    iput-object v0, p0, Lcom/dropbox/android/service/AuthenticationService;->b:Landroid/accounts/OnAccountsUpdateListener;

    .line 40
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/dropbox/android/service/AuthenticationService;->b:Landroid/accounts/OnAccountsUpdateListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 42
    return-void
.end method

.method public onDestroy()V
    .registers 3

    .prologue
    .line 46
    const-string v0, "destroy"

    invoke-static {v0, p0}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 48
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/service/AuthenticationService;->b:Landroid/accounts/OnAccountsUpdateListener;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    .line 49
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 50
    return-void
.end method
