.class public abstract Lcom/dropbox/android/service/C;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Ljava/util/HashSet;


# direct methods
.method public constructor <init>()V
    .registers 6

    .prologue
    .line 453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/service/C;->a:Ljava/util/HashSet;

    .line 454
    invoke-virtual {p0}, Lcom/dropbox/android/service/C;->a()Ljava/lang/String;

    move-result-object v0

    .line 455
    if-eqz v0, :cond_24

    .line 456
    const-string v1, "\\|"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 457
    array-length v2, v1

    const/4 v0, 0x0

    :goto_18
    if-ge v0, v2, :cond_24

    aget-object v3, v1, v0

    .line 458
    iget-object v4, p0, Lcom/dropbox/android/service/C;->a:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 457
    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    .line 461
    :cond_24
    return-void
.end method

.method private b()V
    .registers 3

    .prologue
    .line 488
    const-string v0, "|"

    iget-object v1, p0, Lcom/dropbox/android/service/C;->a:Ljava/util/HashSet;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 489
    invoke-virtual {p0, v0}, Lcom/dropbox/android/service/C;->a(Ljava/lang/String;)V

    .line 490
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/String;
.end method

.method protected abstract a(Ljava/lang/String;)V
.end method

.method public final b(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 464
    iget-object v0, p0, Lcom/dropbox/android/service/C;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 465
    if-eqz v0, :cond_b

    .line 466
    invoke-direct {p0}, Lcom/dropbox/android/service/C;->b()V

    .line 468
    :cond_b
    return v0
.end method

.method public final c(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 472
    iget-object v0, p0, Lcom/dropbox/android/service/C;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 473
    if-eqz v0, :cond_b

    .line 474
    invoke-direct {p0}, Lcom/dropbox/android/service/C;->b()V

    .line 476
    :cond_b
    return v0
.end method

.method public final d(Ljava/lang/String;)Z
    .registers 3
    .parameter

    .prologue
    .line 480
    iget-object v0, p0, Lcom/dropbox/android/service/C;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
