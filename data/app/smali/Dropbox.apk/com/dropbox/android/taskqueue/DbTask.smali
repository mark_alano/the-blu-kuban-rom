.class public abstract Lcom/dropbox/android/taskqueue/DbTask;
.super Lcom/dropbox/android/taskqueue/k;
.source "panda.py"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/k;-><init>()V

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dropbox/android/taskqueue/DbTask;->a:J

    .line 17
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/DbTask;->i()Lcom/dropbox/android/taskqueue/l;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/l;)V

    .line 18
    return-void
.end method

.method private i()Lcom/dropbox/android/taskqueue/l;
    .registers 2

    .prologue
    .line 21
    new-instance v0, Lcom/dropbox/android/taskqueue/f;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/f;-><init>(Lcom/dropbox/android/taskqueue/DbTask;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/dropbox/android/taskqueue/DbTask;)I
    .registers 5
    .parameter

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/DbTask;->a:J

    long-to-int v0, v0

    iget-wide v1, p1, Lcom/dropbox/android/taskqueue/DbTask;->a:J

    long-to-int v1, v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/m;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 91
    return-void
.end method

.method public final b(J)V
    .registers 3
    .parameter

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/dropbox/android/taskqueue/DbTask;->a:J

    .line 79
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .parameter

    .prologue
    .line 14
    check-cast p1, Lcom/dropbox/android/taskqueue/DbTask;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/taskqueue/DbTask;->a(Lcom/dropbox/android/taskqueue/DbTask;)I

    move-result v0

    return v0
.end method

.method public abstract e()Ldbxyzptlk/k/i;
.end method

.method protected abstract f()J
.end method

.method public abstract h()Landroid/net/Uri;
.end method

.method public abstract j()Ljava/lang/String;
.end method

.method public abstract k()Ljava/lang/String;
.end method

.method public final l()J
    .registers 3

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/dropbox/android/taskqueue/DbTask;->a:J

    return-wide v0
.end method

.method public final m()I
    .registers 2

    .prologue
    .line 86
    const/16 v0, 0x14

    return v0
.end method
