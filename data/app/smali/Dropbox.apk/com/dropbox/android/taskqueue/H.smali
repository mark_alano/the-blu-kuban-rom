.class public Lcom/dropbox/android/taskqueue/H;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final i:Ljava/lang/String;


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/List;

.field private final c:Ljava/util/TreeSet;

.field private d:Ljava/util/Timer;

.field private e:I

.field private f:Z

.field private g:Lcom/dropbox/android/taskqueue/T;

.field private final h:I

.field private final j:Lcom/dropbox/android/provider/h;

.field private final k:Landroid/database/ContentObservable;

.field private final l:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 798
    const-class v0, Lcom/dropbox/android/taskqueue/H;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/taskqueue/H;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/provider/h;[Ljava/lang/Class;Ljava/util/List;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    .line 110
    iput-object v2, p0, Lcom/dropbox/android/taskqueue/H;->d:Ljava/util/Timer;

    .line 111
    iput v1, p0, Lcom/dropbox/android/taskqueue/H;->e:I

    .line 112
    iput-boolean v1, p0, Lcom/dropbox/android/taskqueue/H;->f:Z

    .line 113
    iput-object v2, p0, Lcom/dropbox/android/taskqueue/H;->g:Lcom/dropbox/android/taskqueue/T;

    .line 470
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/taskqueue/H;->h:I

    .line 800
    new-instance v0, Landroid/database/ContentObservable;

    invoke-direct {v0}, Landroid/database/ContentObservable;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    .line 128
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, Lcom/dropbox/android/taskqueue/W;

    invoke-direct {v1, p3}, Lcom/dropbox/android/taskqueue/W;-><init>([Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    .line 129
    iput-object p2, p0, Lcom/dropbox/android/taskqueue/H;->j:Lcom/dropbox/android/provider/h;

    .line 130
    iput-object p1, p0, Lcom/dropbox/android/taskqueue/H;->l:Landroid/content/Context;

    .line 131
    iput-object p4, p0, Lcom/dropbox/android/taskqueue/H;->a:Ljava/util/List;

    .line 133
    new-instance v0, Lcom/dropbox/android/util/z;

    new-instance v1, Lcom/dropbox/android/taskqueue/I;

    invoke-direct {v1, p0}, Lcom/dropbox/android/taskqueue/I;-><init>(Lcom/dropbox/android/taskqueue/H;)V

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/util/z;-><init>(Landroid/content/Context;Lcom/dropbox/android/util/C;)V

    .line 143
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->f()V

    .line 145
    return-void
.end method

.method private static a(Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;ILandroid/database/MatrixCursor;)I
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 602
    .line 603
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_6
    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 604
    instance-of v4, v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    if-eqz v4, :cond_6

    .line 607
    check-cast v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    .line 608
    if-eqz p1, :cond_24

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->i()Lcom/dropbox/android/util/DropboxPath;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/dropbox/android/util/DropboxPath;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5e

    .line 609
    :cond_24
    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    add-int v5, p2, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->l()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->h()Landroid/net/Uri;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->j()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->s()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->u()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 616
    invoke-virtual {p3, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 617
    add-int/lit8 v0, v1, 0x1

    :goto_5b
    move v1, v0

    .line 619
    goto :goto_6

    .line 620
    :cond_5d
    return v1

    :cond_5e
    move v0, v1

    goto :goto_5b
.end method

.method private declared-synchronized a(Lcom/dropbox/android/util/aN;)Lcom/dropbox/android/taskqueue/DbTask;
    .registers 5
    .parameter

    .prologue
    .line 300
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 301
    invoke-interface {p1, v0}, Lcom/dropbox/android/util/aN;->a(Ljava/lang/Object;)Z
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_36

    move-result v2

    if-eqz v2, :cond_7

    .line 311
    :goto_19
    monitor-exit p0

    return-object v0

    .line 306
    :cond_1b
    :try_start_1b
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_21
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 307
    invoke-interface {p1, v0}, Lcom/dropbox/android/util/aN;->a(Ljava/lang/Object;)Z
    :try_end_30
    .catchall {:try_start_1b .. :try_end_30} :catchall_36

    move-result v2

    if-eqz v2, :cond_21

    goto :goto_19

    .line 311
    :cond_34
    const/4 v0, 0x0

    goto :goto_19

    .line 300
    :catchall_36
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(ILjava/lang/Class;)Ljava/util/List;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 866
    monitor-enter p0

    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 867
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 868
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 869
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 870
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I
    :try_end_28
    .catchall {:try_start_1 .. :try_end_28} :catchall_30

    move-result v0

    if-lt v0, p1, :cond_c

    move-object v0, v1

    .line 875
    :goto_2c
    monitor-exit p0

    return-object v0

    :cond_2e
    move-object v0, v1

    goto :goto_2c

    .line 866
    :catchall_30
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/H;)V
    .registers 1
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->i()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/H;Ljava/lang/Thread;Lcom/dropbox/android/taskqueue/DbTask;Lcom/dropbox/android/taskqueue/m;)V
    .registers 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/lang/Thread;Lcom/dropbox/android/taskqueue/DbTask;Lcom/dropbox/android/taskqueue/m;)V

    return-void
.end method

.method private a(Lcom/dropbox/android/taskqueue/k;)V
    .registers 3
    .parameter

    .prologue
    .line 167
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/util/Collection;)V

    .line 168
    return-void
.end method

.method private a(Ljava/lang/Class;JLjava/lang/String;Z)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 149
    :try_start_0
    const-string v0, "restore"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Landroid/content/Context;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p1, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 150
    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/dropbox/android/taskqueue/H;->l:Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 151
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->a(Lcom/dropbox/android/taskqueue/k;)V

    .line 152
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->b(Lcom/dropbox/android/taskqueue/DbTask;)V
    :try_end_37
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_37} :catch_38
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_37} :catch_41
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_37} :catch_4a
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_37} :catch_53
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_37} :catch_5c

    .line 164
    return-void

    .line 153
    :catch_38
    move-exception v0

    .line 154
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "DB Restore code failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 155
    :catch_41
    move-exception v0

    .line 156
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "DB Restore code failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 157
    :catch_4a
    move-exception v0

    .line 158
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "DB Restore code failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 159
    :catch_53
    move-exception v0

    .line 160
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "DB Restore code failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 161
    :catch_5c
    move-exception v0

    .line 162
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "DB Restore code failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Ljava/lang/String;JLjava/lang/String;Z)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 201
    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    move-object v0, p0

    move-wide v2, p2

    move-object v4, p4

    move v5, p5

    .line 202
    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/lang/Class;JLjava/lang/String;Z)V
    :try_end_b
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_b} :catch_c

    .line 206
    return-void

    .line 203
    :catch_c
    move-exception v0

    .line 204
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "DB Restore code failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Ljava/lang/Thread;Lcom/dropbox/android/taskqueue/DbTask;Lcom/dropbox/android/taskqueue/m;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 539
    monitor-enter p0

    .line 540
    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 541
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Very unexpected, a task that finished executing, was not in our list"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :catchall_11
    move-exception v0

    monitor-exit p0
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_11

    throw v0

    .line 545
    :cond_14
    :try_start_14
    invoke-virtual {p3}, Lcom/dropbox/android/taskqueue/m;->b()Lcom/dropbox/android/taskqueue/n;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/n;->a:Lcom/dropbox/android/taskqueue/n;

    if-ne v0, v1, :cond_56

    .line 546
    invoke-direct {p0, p2}, Lcom/dropbox/android/taskqueue/H;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 547
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->l:Landroid/content/Context;

    invoke-virtual {p2, v0, p3}, Lcom/dropbox/android/taskqueue/DbTask;->a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/m;)V

    .line 548
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/taskqueue/H;->e:I

    .line 564
    :goto_27
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    .line 566
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->i()V

    .line 568
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->g()Z

    move-result v0

    if-eqz v0, :cond_46

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->g:Lcom/dropbox/android/taskqueue/T;

    if-eqz v0, :cond_46

    .line 569
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/H;->g:Lcom/dropbox/android/taskqueue/T;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/u;->b(Lcom/dropbox/android/service/w;)V

    .line 570
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/H;->g:Lcom/dropbox/android/taskqueue/T;

    .line 572
    :cond_46
    monitor-exit p0
    :try_end_47
    .catchall {:try_start_14 .. :try_end_47} :catchall_11

    .line 574
    invoke-virtual {p3}, Lcom/dropbox/android/taskqueue/m;->b()Lcom/dropbox/android/taskqueue/n;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/n;->a:Lcom/dropbox/android/taskqueue/n;

    if-ne v0, v1, :cond_52

    .line 575
    invoke-direct {p0, p2}, Lcom/dropbox/android/taskqueue/H;->b(Lcom/dropbox/android/taskqueue/k;)V

    .line 577
    :cond_52
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->e()V

    .line 578
    return-void

    .line 550
    :cond_56
    :try_start_56
    invoke-virtual {p3}, Lcom/dropbox/android/taskqueue/m;->a()Z

    move-result v0

    if-eqz v0, :cond_8e

    .line 551
    invoke-direct {p0, p2}, Lcom/dropbox/android/taskqueue/H;->b(Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 552
    iget v0, p0, Lcom/dropbox/android/taskqueue/H;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/taskqueue/H;->e:I

    .line 553
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/H;->f:Z

    .line 554
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/H;->d:Ljava/util/Timer;

    .line 555
    iget v0, p0, Lcom/dropbox/android/taskqueue/H;->e:I

    int-to-long v0, v0

    const-wide/16 v2, 0xf

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 556
    const-wide/16 v2, 0x3a98

    const-wide/high16 v4, 0x4000

    long-to-double v0, v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-long v0, v0

    mul-long/2addr v0, v2

    .line 557
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/H;->d:Ljava/util/Timer;

    new-instance v3, Lcom/dropbox/android/taskqueue/P;

    invoke-direct {v3, p0}, Lcom/dropbox/android/taskqueue/P;-><init>(Lcom/dropbox/android/taskqueue/H;)V

    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_27

    .line 559
    :cond_8e
    invoke-direct {p0, p2}, Lcom/dropbox/android/taskqueue/H;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 560
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->l:Landroid/content/Context;

    invoke-virtual {p2, v0, p3}, Lcom/dropbox/android/taskqueue/DbTask;->a(Landroid/content/Context;Lcom/dropbox/android/taskqueue/m;)V
    :try_end_96
    .catchall {:try_start_56 .. :try_end_96} :catchall_11

    goto :goto_27
.end method

.method private a(Ljava/util/Collection;)V
    .registers 6
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->a:Ljava/util/List;

    if-eqz v0, :cond_2a

    .line 174
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/V;

    .line 175
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/taskqueue/k;

    .line 176
    invoke-interface {v0, p0, v1}, Lcom/dropbox/android/taskqueue/V;->a(Lcom/dropbox/android/taskqueue/H;Lcom/dropbox/android/taskqueue/k;)V

    goto :goto_1a

    .line 180
    :cond_2a
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/taskqueue/H;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/dropbox/android/taskqueue/H;->f:Z

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/taskqueue/H;)Landroid/database/ContentObservable;
    .registers 2
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    return-object v0
.end method

.method private declared-synchronized b(Lcom/dropbox/android/taskqueue/DbTask;)V
    .registers 4
    .parameter

    .prologue
    .line 289
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0, p1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 290
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    .line 292
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->g:Lcom/dropbox/android/taskqueue/T;

    if-nez v0, :cond_21

    .line 293
    new-instance v0, Lcom/dropbox/android/taskqueue/T;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/taskqueue/T;-><init>(Lcom/dropbox/android/taskqueue/H;Lcom/dropbox/android/taskqueue/I;)V

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/H;->g:Lcom/dropbox/android/taskqueue/T;

    .line 294
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/H;->g:Lcom/dropbox/android/taskqueue/T;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/service/u;->a(Lcom/dropbox/android/service/w;)V
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    .line 296
    :cond_21
    monitor-exit p0

    return-void

    .line 289
    :catchall_23
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b(Lcom/dropbox/android/taskqueue/k;)V
    .registers 4
    .parameter

    .prologue
    .line 183
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->a:Ljava/util/List;

    if-eqz v0, :cond_1a

    .line 184
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/V;

    .line 185
    invoke-interface {v0, p0, p1}, Lcom/dropbox/android/taskqueue/V;->b(Lcom/dropbox/android/taskqueue/H;Lcom/dropbox/android/taskqueue/k;)V

    goto :goto_a

    .line 188
    :cond_1a
    return-void
.end method

.method private b(Lcom/dropbox/android/util/aN;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 376
    sget-object v0, Lcom/dropbox/android/taskqueue/H;->i:Ljava/lang/String;

    const-string v2, "canceled task"

    invoke-static {v0, v2}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    monitor-enter p0

    .line 380
    :try_start_9
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 381
    invoke-interface {p1, v0}, Lcom/dropbox/android/util/aN;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 382
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->b()V

    .line 383
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    .line 384
    monitor-exit p0

    .line 406
    :cond_2b
    :goto_2b
    return-void

    .line 388
    :cond_2c
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 389
    :cond_32
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_61

    .line 390
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 391
    invoke-interface {p1, v0}, Lcom/dropbox/android/util/aN;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 392
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->b()V

    .line 393
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->r()Lcom/dropbox/android/taskqueue/m;

    .line 394
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 395
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 396
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    .line 397
    const/4 v0, 0x1

    .line 401
    :goto_57
    monitor-exit p0
    :try_end_58
    .catchall {:try_start_9 .. :try_end_58} :catchall_5e

    .line 403
    if-eqz v0, :cond_2b

    .line 404
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->e()V

    goto :goto_2b

    .line 401
    :catchall_5e
    move-exception v0

    :try_start_5f
    monitor-exit p0
    :try_end_60
    .catchall {:try_start_5f .. :try_end_60} :catchall_5e

    throw v0

    :cond_61
    move v0, v1

    goto :goto_57
.end method

.method private declared-synchronized b(Ljava/util/Collection;)V
    .registers 12
    .parameter

    .prologue
    .line 804
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->j:Lcom/dropbox/android/provider/h;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_6
    .catchall {:try_start_1 .. :try_end_6} :catchall_4c

    move-result-object v1

    .line 806
    :try_start_7
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 808
    new-instance v2, Landroid/database/DatabaseUtils$InsertHelper;

    const-string v0, "pending_uploads"

    invoke-direct {v2, v1, v0}, Landroid/database/DatabaseUtils$InsertHelper;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 809
    sget-object v0, Lcom/dropbox/android/provider/f;->b:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 810
    sget-object v0, Lcom/dropbox/android/provider/f;->c:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/database/DatabaseUtils$InsertHelper;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 812
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_25
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_94

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 813
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    .line 814
    invoke-static {v6}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4f

    .line 815
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "passed class must have a canonical name for restoration"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_47
    .catchall {:try_start_7 .. :try_end_47} :catchall_47

    .line 835
    :catchall_47
    move-exception v0

    :try_start_48
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_4c
    .catchall {:try_start_48 .. :try_end_4c} :catchall_4c

    .line 804
    :catchall_4c
    move-exception v0

    monitor-exit p0

    throw v0

    .line 819
    :cond_4f
    :try_start_4f
    sget-object v7, Lcom/dropbox/android/taskqueue/H;->i:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Task "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " adding to task DB"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    invoke-virtual {v2}, Landroid/database/DatabaseUtils$InsertHelper;->prepareForInsert()V

    .line 821
    invoke-virtual {v2, v3, v6}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    .line 822
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v6}, Landroid/database/DatabaseUtils$InsertHelper;->bind(ILjava/lang/String;)V

    .line 823
    invoke-virtual {v2}, Landroid/database/DatabaseUtils$InsertHelper;->execute()J

    move-result-wide v6

    .line 824
    const-wide/16 v8, -0x1

    cmp-long v8, v6, v8

    if-nez v8, :cond_90

    .line 825
    sget-object v0, Lcom/dropbox/android/taskqueue/H;->i:Ljava/lang/String;

    const-string v6, "Error inserting upload entry into db!"

    invoke-static {v0, v6}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_25

    .line 827
    :cond_90
    invoke-virtual {v0, v6, v7}, Lcom/dropbox/android/taskqueue/DbTask;->b(J)V

    goto :goto_25

    .line 831
    :cond_94
    invoke-virtual {v2}, Landroid/database/DatabaseUtils$InsertHelper;->close()V

    .line 833
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_9a
    .catchall {:try_start_4f .. :try_end_9a} :catchall_47

    .line 835
    :try_start_9a
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_9d
    .catchall {:try_start_9a .. :try_end_9d} :catchall_4c

    .line 837
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized c(Ljava/lang/Class;)Lcom/dropbox/android/taskqueue/DbTask;
    .registers 5
    .parameter

    .prologue
    .line 852
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 853
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_3e

    move-result v2

    if-eqz v2, :cond_7

    .line 862
    :goto_1d
    monitor-exit p0

    return-object v0

    .line 857
    :cond_1f
    :try_start_1f
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_25
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 858
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_38
    .catchall {:try_start_1f .. :try_end_38} :catchall_3e

    move-result v2

    if-eqz v2, :cond_25

    goto :goto_1d

    .line 862
    :cond_3c
    const/4 v0, 0x0

    goto :goto_1d

    .line 852
    :catchall_3e
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c(Lcom/dropbox/android/taskqueue/DbTask;)V
    .registers 8
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 841
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->j:Lcom/dropbox/android/provider/h;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 842
    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/DbTask;->l()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 843
    const-string v2, "pending_uploads"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dropbox/android/provider/f;->a:Lcom/dropbox/android/provider/c;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 845
    if-eq v0, v5, :cond_49

    .line 846
    sget-object v1, Lcom/dropbox/android/taskqueue/H;->i:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error deleting task entry from db table pending_uploads , deleted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 849
    :cond_49
    return-void
.end method

.method private e()V
    .registers 3

    .prologue
    .line 191
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->a:Ljava/util/List;

    if-eqz v0, :cond_1a

    .line 192
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/V;

    .line 193
    invoke-interface {v0, p0}, Lcom/dropbox/android/taskqueue/V;->a(Lcom/dropbox/android/taskqueue/H;)V

    goto :goto_a

    .line 196
    :cond_1a
    return-void
.end method

.method private f()V
    .registers 9

    .prologue
    const/4 v2, 0x0

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->j:Lcom/dropbox/android/provider/h;

    invoke-virtual {v0}, Lcom/dropbox/android/provider/h;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 210
    const-string v1, "pending_uploads"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 212
    :goto_12
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_43

    .line 213
    sget-object v0, Lcom/dropbox/android/provider/f;->a:Lcom/dropbox/android/provider/c;

    iget-object v0, v0, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 214
    sget-object v1, Lcom/dropbox/android/provider/f;->b:Lcom/dropbox/android/provider/c;

    iget-object v1, v1, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 215
    sget-object v2, Lcom/dropbox/android/provider/f;->c:Lcom/dropbox/android/provider/c;

    iget-object v2, v2, Lcom/dropbox/android/provider/c;->b:Ljava/lang/String;

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 217
    int-to-long v2, v0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/lang/String;JLjava/lang/String;Z)V

    goto :goto_12

    .line 219
    :cond_43
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 221
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->i()V

    .line 222
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->e()V

    .line 223
    return-void
.end method

.method private declared-synchronized g()Z
    .registers 2

    .prologue
    .line 449
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_16

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_12
    monitor-exit p0

    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_12

    :catchall_16
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()Lcom/dropbox/android/taskqueue/DbTask;
    .registers 4

    .prologue
    .line 473
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 474
    :cond_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 475
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 476
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->p()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 477
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_1c
    .catchall {:try_start_1 .. :try_end_1c} :catchall_20

    .line 481
    :goto_1c
    monitor-exit p0

    return-object v0

    :cond_1e
    const/4 v0, 0x0

    goto :goto_1c

    .line 473
    :catchall_20
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized i()V
    .registers 7

    .prologue
    .line 514
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/dropbox/android/taskqueue/H;->f:Z

    if-nez v0, :cond_4f

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_4f

    .line 515
    const-class v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/taskqueue/H;->b(Ljava/lang/Class;)I

    move-result v0

    .line 516
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v1

    .line 517
    invoke-static {}, Lcom/dropbox/android/util/i;->G()Lcom/dropbox/android/util/s;

    move-result-object v2

    const-string v3, "num.camera.tasks"

    int-to-long v4, v0

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v2, "num.all.tasks"

    int-to-long v3, v1

    invoke-virtual {v0, v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->b()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 520
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->h()Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v0

    .line 521
    if-eqz v0, :cond_4f

    .line 522
    new-instance v1, Lcom/dropbox/android/taskqueue/R;

    new-instance v2, Lcom/dropbox/android/taskqueue/N;

    invoke-direct {v2, p0}, Lcom/dropbox/android/taskqueue/N;-><init>(Lcom/dropbox/android/taskqueue/H;)V

    invoke-direct {v1, v2, v0}, Lcom/dropbox/android/taskqueue/R;-><init>(Lcom/dropbox/android/taskqueue/S;Lcom/dropbox/android/taskqueue/DbTask;)V

    .line 530
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Lcom/dropbox/android/taskqueue/R;->setPriority(I)V

    .line 532
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/R;->start()V

    .line 535
    :cond_4f
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->dispatchChange(Z)V
    :try_end_55
    .catchall {:try_start_1 .. :try_end_55} :catchall_57

    .line 536
    monitor-exit p0

    return-void

    .line 514
    :catchall_57
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/dropbox/android/util/DropboxPath;)Landroid/database/Cursor;
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 627
    monitor-enter p0

    const/4 v0, 0x6

    :try_start_3
    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "local_uri"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "intended_db_path"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "destination_filename"

    aput-object v2, v0, v1

    .line 629
    new-instance v1, Landroid/database/MatrixCursor;

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 631
    const/4 v0, 0x0

    .line 634
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-static {v2, p1, v0, v1}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;ILandroid/database/MatrixCursor;)I

    move-result v0

    add-int/2addr v0, v3

    .line 635
    iget-object v2, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-static {v2, p1, v0, v1}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/util/Collection;Lcom/dropbox/android/util/DropboxPath;ILandroid/database/MatrixCursor;)I

    .line 637
    new-instance v0, Lcom/dropbox/android/provider/v;

    invoke-direct {v0, v1}, Lcom/dropbox/android/provider/v;-><init>(Landroid/database/Cursor;)V

    .line 638
    iget-object v1, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/v;->a(Landroid/database/ContentObservable;)V
    :try_end_3f
    .catchall {:try_start_3 .. :try_end_3f} :catchall_41

    .line 640
    monitor-exit p0

    return-object v0

    .line 627
    :catchall_41
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)Landroid/database/Cursor;
    .registers 20
    .parameter

    .prologue
    .line 667
    monitor-enter p0

    const/16 v1, 0x9

    :try_start_3
    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "id"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "camera_upload_status"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "camera_upload_initial_scan"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "camera_upload_num_remaining"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "camera_upload_local_uri"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "camera_upload_local_mime_type"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "camera_upload_file_path"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "camera_upload_pending_paths_json"

    aput-object v3, v1, v2

    .line 676
    new-instance v11, Landroid/database/MatrixCursor;

    invoke-direct {v11, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 678
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v12

    .line 679
    invoke-virtual {v12}, Ldbxyzptlk/l/m;->g()Z

    move-result v1

    if-eqz v1, :cond_1cb

    .line 680
    sget-object v8, Lcom/dropbox/android/taskqueue/Q;->f:Lcom/dropbox/android/taskqueue/Q;

    .line 681
    const/4 v9, 0x0

    .line 682
    const-class v1, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/H;->b(Ljava/lang/Class;)I

    move-result v13

    .line 683
    const/4 v7, 0x0

    .line 684
    const/4 v6, 0x0

    .line 685
    const/4 v5, 0x0

    .line 686
    const-wide/16 v3, -0x1

    .line 687
    const/4 v2, 0x0

    .line 689
    invoke-virtual {v12}, Ldbxyzptlk/l/m;->j()Ldbxyzptlk/l/q;

    move-result-object v1

    sget-object v10, Ldbxyzptlk/l/q;->c:Ldbxyzptlk/l/q;

    if-ne v1, v10, :cond_6d

    .line 690
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/u;->b()Lcom/dropbox/android/service/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/service/x;->a()Z

    move-result v1

    if-eqz v1, :cond_10b

    .line 691
    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->d:Lcom/dropbox/android/taskqueue/Q;

    .line 695
    :goto_6b
    const/4 v9, 0x1

    move-object v8, v1

    .line 698
    :cond_6d
    if-lez v13, :cond_d3

    .line 699
    if-nez v9, :cond_1e8

    .line 700
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_79
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1e8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/taskqueue/DbTask;

    .line 701
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v14

    const-class v15, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    invoke-virtual {v14, v15}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_79

    .line 703
    sget-object v8, Lcom/dropbox/android/taskqueue/Q;->g:Lcom/dropbox/android/taskqueue/Q;

    .line 704
    const/4 v2, 0x1

    .line 705
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->l()J

    move-result-wide v3

    move v10, v2

    move-object v2, v1

    move-object v1, v8

    .line 712
    :goto_9b
    if-nez v2, :cond_1e5

    .line 714
    const-class v2, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/dropbox/android/taskqueue/H;->c(Ljava/lang/Class;)Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v3

    .line 715
    invoke-virtual {v3}, Lcom/dropbox/android/taskqueue/DbTask;->l()J

    move-result-wide v8

    .line 717
    if-nez v10, :cond_1e0

    .line 718
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_10f

    .line 720
    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->d:Lcom/dropbox/android/taskqueue/Q;

    move-object v2, v3

    move-wide v3, v8

    move-object v8, v1

    .line 755
    :goto_ba
    if-eqz v2, :cond_d3

    .line 756
    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/DbTask;->h()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 757
    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/DbTask;->j()Ljava/lang/String;

    move-result-object v5

    .line 758
    move-object v0, v2

    check-cast v0, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/CameraUploadTask;->i()Ljava/lang/String;

    move-result-object v1

    move-object v7, v6

    move-object v6, v5

    move-object v5, v1

    .line 762
    :cond_d3
    const/16 v1, 0x8

    const-class v9, Lcom/dropbox/android/taskqueue/CameraUploadTask;

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v9}, Lcom/dropbox/android/taskqueue/H;->a(ILjava/lang/Class;)Ljava/util/List;

    move-result-object v1

    .line 763
    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9}, Lorg/json/JSONArray;-><init>()V

    .line 764
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_e6
    :goto_e6
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_173

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/taskqueue/DbTask;

    .line 768
    if-eqz v2, :cond_100

    invoke-virtual {v2}, Lcom/dropbox/android/taskqueue/DbTask;->l()J

    move-result-wide v14

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->l()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-eqz v14, :cond_e6

    .line 769
    :cond_100
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->h()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v9, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_107
    .catchall {:try_start_3 .. :try_end_107} :catchall_108

    goto :goto_e6

    .line 667
    :catchall_108
    move-exception v1

    monitor-exit p0

    throw v1

    .line 693
    :cond_10b
    :try_start_10b
    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->b:Lcom/dropbox/android/taskqueue/Q;

    goto/16 :goto_6b

    .line 723
    :cond_10f
    new-instance v1, Ldbxyzptlk/k/i;

    invoke-direct {v1, v8, v9}, Ldbxyzptlk/k/i;-><init>(J)V

    .line 724
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/x;->b()Ldbxyzptlk/k/g;

    move-result-object v2

    invoke-virtual {v2, v1}, Ldbxyzptlk/k/g;->a(Ldbxyzptlk/k/i;)Ldbxyzptlk/k/f;

    move-result-object v2

    .line 726
    instance-of v1, v2, Ldbxyzptlk/k/j;

    if-eqz v1, :cond_141

    move-object v0, v2

    check-cast v0, Ldbxyzptlk/k/j;

    move-object v1, v0

    invoke-virtual {v1}, Ldbxyzptlk/k/j;->e()Lcom/dropbox/android/taskqueue/m;

    move-result-object v1

    sget-object v4, Lcom/dropbox/android/taskqueue/m;->k:Lcom/dropbox/android/taskqueue/m;

    if-eq v1, v4, :cond_13a

    check-cast v2, Ldbxyzptlk/k/j;

    invoke-virtual {v2}, Ldbxyzptlk/k/j;->e()Lcom/dropbox/android/taskqueue/m;

    move-result-object v1

    sget-object v2, Lcom/dropbox/android/taskqueue/m;->l:Lcom/dropbox/android/taskqueue/m;

    if-ne v1, v2, :cond_141

    .line 729
    :cond_13a
    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->h:Lcom/dropbox/android/taskqueue/Q;

    move-object v2, v3

    move-wide v3, v8

    move-object v8, v1

    goto/16 :goto_ba

    .line 732
    :cond_141
    invoke-virtual {v3}, Lcom/dropbox/android/taskqueue/DbTask;->j_()Lcom/dropbox/android/taskqueue/o;

    move-result-object v1

    .line 733
    sget-object v2, Lcom/dropbox/android/taskqueue/O;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/o;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1ec

    .line 747
    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->d:Lcom/dropbox/android/taskqueue/Q;

    move-object v2, v3

    move-wide v3, v8

    move-object v8, v1

    goto/16 :goto_ba

    .line 735
    :pswitch_157
    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->b:Lcom/dropbox/android/taskqueue/Q;

    move-object v2, v3

    move-wide v3, v8

    move-object v8, v1

    .line 736
    goto/16 :goto_ba

    .line 738
    :pswitch_15e
    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->a:Lcom/dropbox/android/taskqueue/Q;

    move-object v2, v3

    move-wide v3, v8

    move-object v8, v1

    .line 739
    goto/16 :goto_ba

    .line 741
    :pswitch_165
    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->c:Lcom/dropbox/android/taskqueue/Q;

    move-object v2, v3

    move-wide v3, v8

    move-object v8, v1

    .line 742
    goto/16 :goto_ba

    .line 744
    :pswitch_16c
    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->e:Lcom/dropbox/android/taskqueue/Q;

    move-object v2, v3

    move-wide v3, v8

    move-object v8, v1

    .line 745
    goto/16 :goto_ba

    .line 773
    :cond_173
    invoke-virtual {v12}, Ldbxyzptlk/l/m;->i()Z

    move-result v1

    if-eqz v1, :cond_1d9

    invoke-virtual {v12}, Ldbxyzptlk/l/m;->s()Z

    move-result v1

    if-nez v1, :cond_1d9

    const/4 v1, 0x1

    move v2, v1

    .line 774
    :goto_181
    if-eqz p1, :cond_1dc

    sget-object v1, Lcom/dropbox/android/taskqueue/Q;->f:Lcom/dropbox/android/taskqueue/Q;

    if-ne v8, v1, :cond_1dc

    if-nez v2, :cond_1dc

    const/4 v1, 0x1

    .line 777
    :goto_18a
    if-nez v1, :cond_1cb

    .line 778
    const/16 v1, 0x9

    new-array v10, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v1

    const/4 v1, 0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v10, v1

    const/4 v1, 0x2

    invoke-virtual {v8}, Lcom/dropbox/android/taskqueue/Q;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v1

    const/4 v3, 0x3

    if-eqz v2, :cond_1de

    const/4 v1, 0x1

    :goto_1aa
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v10, v3

    const/4 v1, 0x4

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v10, v1

    const/4 v1, 0x5

    aput-object v7, v10, v1

    const/4 v1, 0x6

    aput-object v6, v10, v1

    const/4 v1, 0x7

    aput-object v5, v10, v1

    const/16 v1, 0x8

    invoke-virtual {v9}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v1

    invoke-virtual {v11, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 792
    :cond_1cb
    new-instance v1, Lcom/dropbox/android/provider/v;

    invoke-direct {v1, v11}, Lcom/dropbox/android/provider/v;-><init>(Landroid/database/Cursor;)V

    .line 793
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    invoke-virtual {v1, v2}, Lcom/dropbox/android/provider/v;->a(Landroid/database/ContentObservable;)V
    :try_end_1d7
    .catchall {:try_start_10b .. :try_end_1d7} :catchall_108

    .line 795
    monitor-exit p0

    return-object v1

    .line 773
    :cond_1d9
    const/4 v1, 0x0

    move v2, v1

    goto :goto_181

    .line 774
    :cond_1dc
    const/4 v1, 0x0

    goto :goto_18a

    .line 778
    :cond_1de
    const/4 v1, 0x0

    goto :goto_1aa

    :cond_1e0
    move-object v2, v3

    move-wide v3, v8

    move-object v8, v1

    goto/16 :goto_ba

    :cond_1e5
    move-object v8, v1

    goto/16 :goto_ba

    :cond_1e8
    move v10, v9

    move-object v1, v8

    goto/16 :goto_9b

    .line 733
    :pswitch_data_1ec
    .packed-switch 0x1
        :pswitch_157
        :pswitch_15e
        :pswitch_165
        :pswitch_16c
    .end packed-switch
.end method

.method public final a()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 410
    monitor-enter p0

    .line 411
    :try_start_2
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 413
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->d:Ljava/util/Timer;

    if-eqz v0, :cond_1c

    .line 414
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/taskqueue/H;->f:Z

    .line 415
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->d:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/taskqueue/H;->d:Ljava/util/Timer;

    .line 421
    :cond_1c
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 422
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_27
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 423
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->b()V

    goto :goto_27

    .line 434
    :catchall_37
    move-exception v0

    monitor-exit p0
    :try_end_39
    .catchall {:try_start_2 .. :try_end_39} :catchall_37

    throw v0

    .line 426
    :cond_3a
    :try_start_3a
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 427
    :goto_40
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_59

    .line 428
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 429
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->b()V

    .line 430
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->r()Lcom/dropbox/android/taskqueue/m;

    .line 431
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 432
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_40

    .line 434
    :cond_59
    monitor-exit p0
    :try_end_5a
    .catchall {:try_start_3a .. :try_end_5a} :catchall_37

    .line 440
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 441
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    goto :goto_5e

    .line 444
    :cond_6e
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->e()V

    .line 445
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    invoke-virtual {v0, v3}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    .line 446
    return-void
.end method

.method public final declared-synchronized a(J)V
    .registers 4
    .parameter

    .prologue
    .line 367
    monitor-enter p0

    :try_start_1
    new-instance v0, Lcom/dropbox/android/taskqueue/L;

    invoke-direct {v0, p0, p1, p2}, Lcom/dropbox/android/taskqueue/L;-><init>(Lcom/dropbox/android/taskqueue/H;J)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->b(Lcom/dropbox/android/util/aN;)V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    .line 373
    monitor-exit p0

    return-void

    .line 367
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/dropbox/android/taskqueue/DbTask;)V
    .registers 3
    .parameter

    .prologue
    .line 226
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/taskqueue/H;->a(Lcom/dropbox/android/taskqueue/DbTask;Z)V

    .line 227
    return-void
.end method

.method public final a(Lcom/dropbox/android/taskqueue/DbTask;Z)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 230
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/util/List;Z)V

    .line 231
    return-void
.end method

.method public final declared-synchronized a(Ldbxyzptlk/k/i;)V
    .registers 3
    .parameter

    .prologue
    .line 354
    monitor-enter p0

    :try_start_1
    new-instance v0, Lcom/dropbox/android/taskqueue/K;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/taskqueue/K;-><init>(Lcom/dropbox/android/taskqueue/H;Ldbxyzptlk/k/i;)V

    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->b(Lcom/dropbox/android/util/aN;)V
    :try_end_9
    .catchall {:try_start_1 .. :try_end_9} :catchall_b

    .line 360
    monitor-exit p0

    return-void

    .line 354
    :catchall_b
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/Class;)V
    .registers 6
    .parameter

    .prologue
    .line 316
    monitor-enter p0

    .line 317
    :try_start_1
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 319
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_12
    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 320
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 321
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->b()V

    goto :goto_12

    .line 335
    :catchall_2c
    move-exception v0

    monitor-exit p0
    :try_end_2e
    .catchall {:try_start_1 .. :try_end_2e} :catchall_2c

    throw v0

    .line 325
    :cond_2f
    :try_start_2f
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 326
    :cond_35
    :goto_35
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_58

    .line 327
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 328
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 329
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->b()V

    .line 330
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->r()Lcom/dropbox/android/taskqueue/m;

    .line 331
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 332
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_35

    .line 335
    :cond_58
    monitor-exit p0
    :try_end_59
    .catchall {:try_start_2f .. :try_end_59} :catchall_2c

    .line 341
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 342
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->c(Lcom/dropbox/android/taskqueue/DbTask;)V

    goto :goto_5d

    .line 345
    :cond_6d
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->e()V

    .line 346
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    .line 347
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .registers 3
    .parameter

    .prologue
    .line 234
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/util/List;Z)V

    .line 235
    return-void
.end method

.method public final a(Ljava/util/List;Z)V
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 238
    monitor-enter p0

    .line 241
    if-eqz p2, :cond_51

    .line 243
    :try_start_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 244
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_10
    :goto_10
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 245
    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/DbTask;->d()Ljava/lang/String;

    move-result-object v5

    .line 246
    new-instance v1, Lcom/dropbox/android/taskqueue/J;

    invoke-direct {v1, p0, v5}, Lcom/dropbox/android/taskqueue/J;-><init>(Lcom/dropbox/android/taskqueue/H;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/dropbox/android/taskqueue/H;->a(Lcom/dropbox/android/util/aN;)Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v1

    .line 252
    if-nez v1, :cond_10

    .line 254
    const/4 v3, 0x0

    .line 255
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_30
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dropbox/android/taskqueue/DbTask;

    .line 256
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DbTask;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 257
    const/4 v1, 0x1

    .line 261
    :goto_47
    if-nez v1, :cond_10

    .line 262
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_10

    .line 278
    :catchall_4d
    move-exception v0

    monitor-exit p0
    :try_end_4f
    .catchall {:try_start_3 .. :try_end_4f} :catchall_4d

    throw v0

    :cond_50
    move-object p1, v2

    .line 270
    :cond_51
    :try_start_51
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/H;->b(Ljava/util/Collection;)V

    .line 272
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_58
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_71

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 273
    const-string v2, "enqueue"

    invoke-static {v2, v0}, Lcom/dropbox/android/util/i;->b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/k;)Lcom/dropbox/android/util/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/s;->c()V

    .line 274
    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->b(Lcom/dropbox/android/taskqueue/DbTask;)V

    goto :goto_58

    .line 277
    :cond_71
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->i()V

    .line 278
    monitor-exit p0
    :try_end_75
    .catchall {:try_start_51 .. :try_end_75} :catchall_4d

    .line 284
    invoke-direct {p0, p1}, Lcom/dropbox/android/taskqueue/H;->a(Ljava/util/Collection;)V

    .line 285
    invoke-direct {p0}, Lcom/dropbox/android/taskqueue/H;->e()V

    .line 286
    return-void

    :cond_7c
    move v1, v3

    goto :goto_47
.end method

.method public final declared-synchronized b(Ljava/lang/Class;)I
    .registers 5
    .parameter

    .prologue
    .line 453
    monitor-enter p0

    const/4 v1, 0x0

    .line 455
    :try_start_2
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_8
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 456
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_49

    .line 457
    add-int/lit8 v0, v1, 0x1

    :goto_20
    move v1, v0

    goto :goto_8

    .line 461
    :cond_22
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->c:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_28
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_42

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/DbTask;

    .line 462
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_3b
    .catchall {:try_start_2 .. :try_end_3b} :catchall_44

    move-result v0

    if-eqz v0, :cond_47

    .line 463
    add-int/lit8 v0, v1, 0x1

    :goto_40
    move v1, v0

    goto :goto_28

    .line 467
    :cond_42
    monitor-exit p0

    return v1

    .line 453
    :catchall_44
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_47
    move v0, v1

    goto :goto_40

    :cond_49
    move v0, v1

    goto :goto_20
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 495
    new-instance v0, Lcom/dropbox/android/taskqueue/M;

    invoke-direct {v0, p0}, Lcom/dropbox/android/taskqueue/M;-><init>(Lcom/dropbox/android/taskqueue/H;)V

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/M;->start()V

    .line 505
    return-void
.end method

.method public final c()V
    .registers 3

    .prologue
    .line 509
    iget-object v0, p0, Lcom/dropbox/android/taskqueue/H;->k:Landroid/database/ContentObservable;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    .line 510
    return-void
.end method

.method public final declared-synchronized d()Lcom/dropbox/android/util/DropboxPath;
    .registers 3

    .prologue
    .line 645
    monitor-enter p0

    :try_start_1
    const-class v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    invoke-direct {p0, v0}, Lcom/dropbox/android/taskqueue/H;->c(Ljava/lang/Class;)Lcom/dropbox/android/taskqueue/DbTask;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/taskqueue/UserImportUploadTask;

    .line 646
    if-eqz v0, :cond_17

    .line 647
    new-instance v1, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/taskqueue/UserImportUploadTask;->s()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V
    :try_end_14
    .catchall {:try_start_1 .. :try_end_14} :catchall_19

    move-object v0, v1

    .line 650
    :goto_15
    monitor-exit p0

    return-object v0

    :cond_17
    const/4 v0, 0x0

    goto :goto_15

    .line 645
    :catchall_19
    move-exception v0

    monitor-exit p0

    throw v0
.end method
