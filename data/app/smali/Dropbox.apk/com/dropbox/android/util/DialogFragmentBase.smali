.class public Lcom/dropbox/android/util/DialogFragmentBase;
.super Lcom/actionbarsherlock/app/SherlockDialogFragment;
.source "panda.py"


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onDestroyView()V
    .registers 3

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/dropbox/android/util/DialogFragmentBase;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-virtual {p0}, Lcom/dropbox/android/util/DialogFragmentBase;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 11
    invoke-virtual {p0}, Lcom/dropbox/android/util/DialogFragmentBase;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 13
    :cond_14
    invoke-super {p0}, Lcom/actionbarsherlock/app/SherlockDialogFragment;->onDestroyView()V

    .line 14
    return-void
.end method
