.class public final Lcom/dropbox/android/util/Y;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final d:Ljava/util/ArrayList;


# instance fields
.field private b:Ljava/lang/String;

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 23
    const-class v0, Lcom/dropbox/android/util/Y;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/Y;->a:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/Y;->d:Ljava/util/ArrayList;

    .line 159
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/util/Y;->b:Ljava/lang/String;

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/util/Y;->c:I

    .line 162
    invoke-direct {p0}, Lcom/dropbox/android/util/Y;->a()V

    .line 163
    return-void
.end method

.method private a()V
    .registers 4

    .prologue
    .line 225
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/util/Y;->b()Ljava/io/File;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ldbxyzptlk/B/b;->a(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_31

    .line 227
    const-string v1, ":"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    .line 228
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 229
    sget-object v2, Lcom/dropbox/android/util/Y;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_32

    .line 230
    sget-object v0, Lcom/dropbox/android/util/Y;->a:Ljava/lang/String;

    const-string v1, "Saved VM index doesn\'t exist!"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_31
    :goto_31
    return-void

    .line 233
    :cond_32
    iput v1, p0, Lcom/dropbox/android/util/Y;->c:I

    .line 234
    const/4 v2, 0x1

    aget-object v2, v0, v2

    .line 235
    sget-object v0, Lcom/dropbox/android/util/Y;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/Z;

    invoke-virtual {v0}, Lcom/dropbox/android/util/Z;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_53

    .line 236
    sget-object v0, Lcom/dropbox/android/util/Y;->a:Ljava/lang/String;

    const-string v1, "Saved user doesn\'t exist!"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_31

    .line 241
    :catch_51
    move-exception v0

    goto :goto_31

    .line 239
    :cond_53
    iput-object v2, p0, Lcom/dropbox/android/util/Y;->b:Ljava/lang/String;
    :try_end_55
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_55} :catch_51

    goto :goto_31
.end method

.method private static b()Ljava/io/File;
    .registers 3

    .prologue
    .line 245
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    const-string v1, "dev_settings"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    .line 246
    new-instance v1, Ljava/io/File;

    const-string v2, ".state"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method
