.class public abstract enum Lcom/dropbox/android/util/aA;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/util/aA;

.field private static final synthetic b:[Lcom/dropbox/android/util/aA;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    .line 215
    new-instance v0, Lcom/dropbox/android/util/aB;

    const-string v1, "UPLOAD_FAILED"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aB;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aA;->a:Lcom/dropbox/android/util/aA;

    .line 214
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dropbox/android/util/aA;

    sget-object v1, Lcom/dropbox/android/util/aA;->a:Lcom/dropbox/android/util/aA;

    aput-object v1, v0, v2

    sput-object v0, Lcom/dropbox/android/util/aA;->b:[Lcom/dropbox/android/util/aA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 214
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/util/az;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 214
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/util/aA;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/aA;
    .registers 2
    .parameter

    .prologue
    .line 214
    const-class v0, Lcom/dropbox/android/util/aA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/aA;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/aA;
    .registers 1

    .prologue
    .line 214
    sget-object v0, Lcom/dropbox/android/util/aA;->b:[Lcom/dropbox/android/util/aA;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/aA;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/aA;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;
.end method
