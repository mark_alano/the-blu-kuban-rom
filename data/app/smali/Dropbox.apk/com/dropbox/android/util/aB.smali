.class final enum Lcom/dropbox/android/util/aB;
.super Lcom/dropbox/android/util/aA;
.source "panda.py"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/util/aA;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/az;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;
    .registers 9
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 219
    if-eqz p2, :cond_13

    const-string v0, "ARG_FILENAME"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "ARG_STATUS"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 220
    :cond_13
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Upload failed notification needs args."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_1b
    const-string v0, "ACTION_UPLOAD_FAILURE_DLG"

    invoke-static {p1, v0}, Lcom/dropbox/android/activity/DropboxBrowser;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 224
    const-string v0, "EXTRA_FILENAME"

    const-string v2, "ARG_FILENAME"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    const-string v0, "EXTRA_STATUS"

    const-string v2, "ARG_STATUS"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 226
    const-string v0, "ARG_INTENDED_FOLDER"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 227
    const-string v0, "ARG_INTENDED_FOLDER"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 230
    :cond_4e
    invoke-static {p1, v1}, Lcom/dropbox/android/util/ay;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 232
    const v1, 0x7f0b01c7

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "ARG_FILENAME"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 236
    new-instance v2, Landroid/support/v4/app/D;

    invoke-direct {v2, p1}, Landroid/support/v4/app/D;-><init>(Landroid/content/Context;)V

    .line 238
    const v3, 0x7f0200f2

    invoke-virtual {v2, v3}, Landroid/support/v4/app/D;->a(I)Landroid/support/v4/app/D;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/D;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/D;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v1

    const v2, 0x7f0b01c8

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/D;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/D;->a(J)Landroid/support/v4/app/D;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/D;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/support/v4/app/D;->b(Z)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/D;->a()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method
