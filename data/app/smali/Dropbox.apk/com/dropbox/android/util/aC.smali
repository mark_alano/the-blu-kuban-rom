.class public abstract enum Lcom/dropbox/android/util/aC;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/util/aC;

.field public static final enum b:Lcom/dropbox/android/util/aC;

.field public static final enum c:Lcom/dropbox/android/util/aC;

.field public static final enum d:Lcom/dropbox/android/util/aC;

.field public static final enum e:Lcom/dropbox/android/util/aC;

.field public static final enum f:Lcom/dropbox/android/util/aC;

.field public static final enum g:Lcom/dropbox/android/util/aC;

.field public static final enum h:Lcom/dropbox/android/util/aC;

.field private static final synthetic i:[Lcom/dropbox/android/util/aC;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 28
    new-instance v0, Lcom/dropbox/android/util/aD;

    const-string v1, "CAMERA_UPLOAD_PROMO"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/util/aD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aC;->a:Lcom/dropbox/android/util/aC;

    .line 48
    new-instance v0, Lcom/dropbox/android/util/aE;

    const-string v1, "CAMERA_UPLOAD_STOPPED_QUOTA"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/util/aE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aC;->b:Lcom/dropbox/android/util/aC;

    .line 67
    new-instance v0, Lcom/dropbox/android/util/aF;

    const-string v1, "UPLOAD_STOPPED_QUOTA"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/util/aF;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aC;->c:Lcom/dropbox/android/util/aC;

    .line 86
    new-instance v0, Lcom/dropbox/android/util/aG;

    const-string v1, "CAMERA_UPLOAD_PAUSED_BATTERY"

    invoke-direct {v0, v1, v6}, Lcom/dropbox/android/util/aG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aC;->d:Lcom/dropbox/android/util/aC;

    .line 106
    new-instance v0, Lcom/dropbox/android/util/aH;

    const-string v1, "CAMERA_UPLOAD_BACKLOG_FINISHED"

    invoke-direct {v0, v1, v7}, Lcom/dropbox/android/util/aH;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aC;->e:Lcom/dropbox/android/util/aC;

    .line 126
    new-instance v0, Lcom/dropbox/android/util/aI;

    const-string v1, "CAMERA_UPLOAD_FIRST_BUNCH_FINISHED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aI;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aC;->f:Lcom/dropbox/android/util/aC;

    .line 146
    new-instance v0, Lcom/dropbox/android/util/aJ;

    const-string v1, "UPLOAD_PROGRESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aC;->g:Lcom/dropbox/android/util/aC;

    .line 178
    new-instance v0, Lcom/dropbox/android/util/aK;

    const-string v1, "UPLOAD_DONE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/aK;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/aC;->h:Lcom/dropbox/android/util/aC;

    .line 27
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/dropbox/android/util/aC;

    sget-object v1, Lcom/dropbox/android/util/aC;->a:Lcom/dropbox/android/util/aC;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/util/aC;->b:Lcom/dropbox/android/util/aC;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/util/aC;->c:Lcom/dropbox/android/util/aC;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dropbox/android/util/aC;->d:Lcom/dropbox/android/util/aC;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dropbox/android/util/aC;->e:Lcom/dropbox/android/util/aC;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dropbox/android/util/aC;->f:Lcom/dropbox/android/util/aC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dropbox/android/util/aC;->g:Lcom/dropbox/android/util/aC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dropbox/android/util/aC;->h:Lcom/dropbox/android/util/aC;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dropbox/android/util/aC;->i:[Lcom/dropbox/android/util/aC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/dropbox/android/util/az;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/util/aC;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/aC;
    .registers 2
    .parameter

    .prologue
    .line 27
    const-class v0, Lcom/dropbox/android/util/aC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/aC;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/aC;
    .registers 1

    .prologue
    .line 27
    sget-object v0, Lcom/dropbox/android/util/aC;->i:[Lcom/dropbox/android/util/aC;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/aC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/aC;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;
.end method
