.class final enum Lcom/dropbox/android/util/aD;
.super Lcom/dropbox/android/util/aC;
.source "panda.py"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/util/aC;-><init>(Ljava/lang/String;ILcom/dropbox/android/util/az;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/app/Notification;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 31
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/DropboxBrowser;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    const-string v1, "EXTRA_CAME_FROM_PROMO"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 33
    invoke-static {p1, v0}, Lcom/dropbox/android/util/ay;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 35
    new-instance v1, Landroid/support/v4/app/D;

    invoke-direct {v1, p1}, Landroid/support/v4/app/D;-><init>(Landroid/content/Context;)V

    .line 37
    const v2, 0x7f0200f2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/D;->a(I)Landroid/support/v4/app/D;

    move-result-object v1

    const v2, 0x7f0b01b6

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/D;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v1

    const v2, 0x7f0b01b7

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/D;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v1

    const v2, 0x7f0b01b8

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/D;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/D;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/D;->a(J)Landroid/support/v4/app/D;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/D;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/D;->b(Z)Landroid/support/v4/app/D;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/D;->a()Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method
