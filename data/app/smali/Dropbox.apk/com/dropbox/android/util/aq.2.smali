.class public final Lcom/dropbox/android/util/aq;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final a:Ljava/util/Stack;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/util/aq;->a:Ljava/util/Stack;

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/dropbox/android/util/ap;
    .registers 3
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/dropbox/android/util/aq;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/ap;

    return-object v0
.end method

.method public final a(Lcom/dropbox/android/util/ap;)Lcom/dropbox/android/util/ap;
    .registers 3
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/dropbox/android/util/aq;->a:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/ap;

    return-object v0
.end method

.method public final declared-synchronized a(Landroid/widget/ListView;)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 59
    monitor-enter p0

    :try_start_2
    invoke-virtual {p0}, Lcom/dropbox/android/util/aq;->a()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 60
    invoke-virtual {p1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 61
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 62
    if-nez v2, :cond_1d

    .line 64
    :goto_13
    invoke-virtual {p0}, Lcom/dropbox/android/util/aq;->b()Lcom/dropbox/android/util/ap;

    move-result-object v2

    .line 65
    iput v1, v2, Lcom/dropbox/android/util/ap;->b:I

    .line 66
    iput v0, v2, Lcom/dropbox/android/util/ap;->c:I
    :try_end_1b
    .catchall {:try_start_2 .. :try_end_1b} :catchall_22

    .line 68
    :cond_1b
    monitor-exit p0

    return-void

    .line 62
    :cond_1d
    :try_start_1d
    invoke-virtual {v2}, Landroid/view/View;->getTop()I
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_22

    move-result v0

    goto :goto_13

    .line 59
    :catchall_22
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/dropbox/android/widget/DropboxItemListView;)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 47
    monitor-enter p0

    :try_start_2
    invoke-virtual {p0}, Lcom/dropbox/android/util/aq;->a()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 48
    invoke-virtual {p1}, Lcom/dropbox/android/widget/DropboxItemListView;->c()I

    move-result v1

    .line 49
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/dropbox/android/widget/DropboxItemListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 50
    if-nez v2, :cond_1d

    .line 52
    :goto_13
    invoke-virtual {p0}, Lcom/dropbox/android/util/aq;->b()Lcom/dropbox/android/util/ap;

    move-result-object v2

    .line 53
    iput v1, v2, Lcom/dropbox/android/util/ap;->b:I

    .line 54
    iput v0, v2, Lcom/dropbox/android/util/ap;->c:I
    :try_end_1b
    .catchall {:try_start_2 .. :try_end_1b} :catchall_22

    .line 56
    :cond_1b
    monitor-exit p0

    return-void

    .line 50
    :cond_1d
    :try_start_1d
    invoke-virtual {v2}, Landroid/view/View;->getTop()I
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_22

    move-result v0

    goto :goto_13

    .line 47
    :catchall_22
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/dropbox/android/util/aq;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/dropbox/android/util/ap;
    .registers 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/util/aq;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/ap;

    return-object v0
.end method

.method public final c()Lcom/dropbox/android/util/ap;
    .registers 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/util/aq;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/ap;

    return-object v0
.end method

.method public final d()I
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/dropbox/android/util/aq;->a:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    return v0
.end method
