.class public final Lcom/dropbox/android/util/bd;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field protected static a:Ljava/util/Map;

.field private static b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const v3, 0x7f0b007b

    .line 52
    const-class v0, Lcom/dropbox/android/util/bd;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/bd;->b:Ljava/lang/String;

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    .line 98
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->a:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b0079

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->b:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b007a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->c:Lcom/dropbox/android/taskqueue/m;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->d:Lcom/dropbox/android/taskqueue/m;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->e:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b007c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->g:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b007d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->h:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b007e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->j:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b007f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->k:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b0050

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->l:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b0125

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->m:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b0081

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->n:Lcom/dropbox/android/taskqueue/m;

    const v2, 0x7f0b0082

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Date;)J
    .registers 3
    .parameter

    .prologue
    .line 76
    if-eqz p0, :cond_e

    .line 77
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 78
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 79
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    .line 81
    :goto_d
    return-wide v0

    :cond_e
    const-wide/16 v0, 0x0

    goto :goto_d
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/n/k;)Lcom/dropbox/android/util/bh;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 307
    iget-object v0, p2, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->n(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 308
    const-string v0, "video/mp4"

    .line 309
    const-string v0, "http://dl.dropbox.com/mock_streaming_file.mp4"

    .line 310
    new-instance v0, Lcom/dropbox/android/util/bh;

    const-string v1, "http://dl.dropbox.com/mock_streaming_file.mp4"

    const-string v2, "video/mp4"

    invoke-static {p0, p1, v1, v2}, Lcom/dropbox/android/util/bd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    const-string v2, "http://dl.dropbox.com/mock_streaming_file.mp4"

    const-string v3, "video/mp4"

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bh;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 321
    :goto_1e
    return-object v0

    .line 314
    :cond_1f
    iget-object v0, p2, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 315
    const-string v0, "http://dl.dropbox.com/mock_streaming_file"

    .line 316
    new-instance v0, Lcom/dropbox/android/util/bh;

    const-string v1, "http://dl.dropbox.com/mock_streaming_file"

    iget-object v2, p2, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    invoke-static {p0, p1, v1, v2}, Lcom/dropbox/android/util/bd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    const-string v2, "http://dl.dropbox.com/mock_streaming_file"

    iget-object v3, p2, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/util/bh;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1e

    .line 321
    :cond_3b
    new-instance v0, Lcom/dropbox/android/util/bh;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2, v2}, Lcom/dropbox/android/util/bh;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1e
.end method

.method public static a(J)Ljava/lang/String;
    .registers 11
    .parameter

    .prologue
    const-wide/32 v7, 0x36ee80

    const-wide/16 v5, 0x3c

    .line 67
    const-string v0, "%02d:%02d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-wide/32 v3, 0xea60

    div-long v3, p0, v3

    rem-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-wide/16 v3, 0x3e8

    div-long v3, p0, v3

    rem-long/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 69
    div-long v1, p0, v7

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_48

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    div-long v2, p0, v7

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    :cond_48
    return-object v0
.end method

.method public static a(Landroid/content/Context;IJ)Ljava/lang/String;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const-wide/16 v0, 0x0

    .line 86
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 87
    sub-long/2addr v2, p2

    .line 88
    cmp-long v4, v2, v0

    if-gez v4, :cond_10

    .line 92
    :goto_b
    invoke-static {p0, v0, v1, p1}, Lcom/dropbox/android/util/aj;->b(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_10
    move-wide v0, v2

    goto :goto_b
.end method

.method public static a(Lcom/dropbox/android/taskqueue/m;Landroid/content/Context;)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 113
    if-nez p0, :cond_4

    .line 114
    const/4 v0, 0x0

    .line 116
    :goto_3
    return-object v0

    :cond_4
    sget-object v0, Lcom/dropbox/android/util/bd;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;
    .registers 3
    .parameter

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/dropbox/android/util/DropboxPath;->c()Ljava/lang/String;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_12

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 60
    :cond_12
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b001a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    :cond_1d
    return-object v0
.end method

.method public static a(Lcom/dropbox/android/util/DropboxPath;I)Ljava/lang/String;
    .registers 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 374
    invoke-static {p0}, Lcom/dropbox/android/util/bd;->a(Lcom/dropbox/android/util/DropboxPath;)Ljava/lang/String;

    move-result-object v0

    .line 376
    if-nez p1, :cond_1c

    .line 377
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00d4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 385
    :goto_1b
    return-object v0

    .line 380
    :cond_1c
    if-ne p1, v3, :cond_32

    .line 381
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00d5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1b

    .line 385
    :cond_32
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00d6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1b
.end method

.method static synthetic a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-static {p0, p1}, Lcom/dropbox/android/util/bd;->b(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V

    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/net/Uri;Z)V
    .registers 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 261
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    .line 263
    if-eqz p3, :cond_29

    invoke-static {p1}, Lcom/dropbox/android/util/ae;->a(Ldbxyzptlk/n/k;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 265
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/GalleryActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 266
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 267
    const-string v1, "SELECTED_PATH"

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 291
    :goto_28
    return-void

    .line 270
    :cond_29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 271
    const-string v3, "android.intent.action.VIEW"

    iget-object v4, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {p0, v3, v0, v4}, Lcom/dropbox/android/util/bd;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_58

    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_58

    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7f

    :cond_58
    move v0, v2

    .line 274
    :goto_59
    iget-object v3, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-static {v3}, Lcom/dropbox/android/util/bd;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_63

    if-nez v0, :cond_81

    .line 276
    :cond_63
    :goto_63
    if-eqz v2, :cond_83

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p0, v1, p1}, Lcom/dropbox/android/util/bd;->a(Landroid/content/Context;Ljava/lang/String;Ldbxyzptlk/n/k;)Lcom/dropbox/android/util/bh;

    move-result-object v1

    .line 278
    :goto_6b
    if-eqz v2, :cond_85

    iget-boolean v2, v1, Lcom/dropbox/android/util/bh;->a:Z

    if-eqz v2, :cond_85

    .line 279
    const-string v0, "android.intent.action.VIEW"

    iget-object v2, v1, Lcom/dropbox/android/util/bh;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v1, v1, Lcom/dropbox/android/util/bh;->c:Ljava/lang/String;

    invoke-static {p0, p1, v0, v2, v1}, Lcom/dropbox/android/util/bd;->a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_28

    :cond_7f
    move v0, v1

    .line 271
    goto :goto_59

    :cond_81
    move v2, v1

    .line 274
    goto :goto_63

    .line 276
    :cond_83
    const/4 v1, 0x0

    goto :goto_6b

    .line 280
    :cond_85
    if-eqz v0, :cond_8d

    .line 281
    const-string v0, "android.intent.action.VIEW"

    invoke-static {p0, p1, v0}, Lcom/dropbox/android/util/bd;->a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)V

    goto :goto_28

    .line 283
    :cond_8d
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 284
    const-string v0, "text/plain"

    iput-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    .line 285
    const-string v0, "android.intent.action.VIEW"

    invoke-static {p0, p1, v0}, Lcom/dropbox/android/util/bd;->a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)V

    goto :goto_28

    .line 287
    :cond_9f
    invoke-static {p0, p1}, Lcom/dropbox/android/util/bd;->b(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto :goto_28
.end method

.method private static a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 367
    new-instance v0, Ldbxyzptlk/g/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Ldbxyzptlk/g/d;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;)V

    .line 368
    invoke-virtual {v0}, Ldbxyzptlk/g/d;->f()V

    .line 369
    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Ldbxyzptlk/g/h;)Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/activity/dialog/ExportProgressDialogFrag;->a(Landroid/support/v4/app/j;)V

    .line 370
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Ldbxyzptlk/g/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 371
    return-void
.end method

.method private static a(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 221
    invoke-static {p4}, Lcom/dropbox/android/util/ae;->n(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 222
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/dropbox/android/activity/VideoPlayerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 223
    new-instance v1, Ldbxyzptlk/g/P;

    sget-object v2, Ldbxyzptlk/g/S;->b:Ldbxyzptlk/g/S;

    invoke-direct {v1, p0, p1, v0, v2}, Ldbxyzptlk/g/P;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;Ldbxyzptlk/g/S;)V

    .line 224
    new-instance v0, Lcom/dropbox/android/util/be;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/be;-><init>(Landroid/support/v4/app/FragmentActivity;)V

    invoke-virtual {v1, v0}, Ldbxyzptlk/g/P;->a(Ldbxyzptlk/g/R;)V

    .line 230
    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Ldbxyzptlk/g/P;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 252
    :goto_22
    return-void

    .line 232
    :cond_23
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 233
    invoke-virtual {v0, p3, p4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    const v1, 0x7f0b008b

    invoke-virtual {p0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 236
    new-instance v2, Lcom/dropbox/android/widget/ag;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/content/Intent;

    aput-object v0, v3, v4

    const/4 v0, 0x0

    invoke-direct {v2, p0, v1, v3, v0}, Lcom/dropbox/android/widget/ag;-><init>(Landroid/content/Context;Ljava/lang/String;[Landroid/content/Intent;[Landroid/content/Intent;)V

    .line 238
    new-instance v0, Lcom/dropbox/android/util/bf;

    invoke-direct {v0, p0, p1}, Lcom/dropbox/android/util/bf;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v0}, Lcom/dropbox/android/widget/ag;->a(Lcom/dropbox/android/widget/aj;)V

    .line 250
    invoke-virtual {v2}, Lcom/dropbox/android/widget/ag;->a()V

    goto :goto_22
.end method

.method private static a(II)Z
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 122
    if-eqz p0, :cond_5

    if-nez p1, :cond_6

    .line 126
    :cond_5
    :goto_5
    return v0

    .line 125
    :cond_6
    int-to-float v1, p1

    int-to-float v2, p0

    div-float/2addr v1, v2

    .line 126
    const/high16 v2, 0x4040

    cmpl-float v2, v1, v2

    if-gez v2, :cond_16

    const v2, 0x3eaaaaab

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_5

    :cond_16
    const/4 v0, 0x1

    goto :goto_5
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 333
    if-nez p1, :cond_4

    .line 353
    :goto_3
    return v1

    .line 336
    :cond_4
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 337
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 338
    const/high16 v2, 0x1000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 339
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 342
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v3, 0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 346
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_26
    :goto_26
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 347
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 348
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 349
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_26

    .line 353
    :cond_40
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_49

    const/4 v0, 0x1

    :goto_47
    move v1, v0

    goto :goto_3

    :cond_49
    move v0, v1

    goto :goto_47
.end method

.method public static a(Landroid/graphics/Bitmap;)Z
    .registers 3
    .parameter

    .prologue
    .line 141
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/util/bd;->a(II)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)Z
    .registers 4
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 131
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 132
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 133
    if-eq v0, v2, :cond_d

    if-ne v1, v2, :cond_f

    .line 134
    :cond_d
    const/4 v0, 0x0

    .line 136
    :goto_e
    return v0

    :cond_f
    invoke-static {v0, v1}, Lcom/dropbox/android/util/bd;->a(II)Z

    move-result v0

    goto :goto_e
.end method

.method public static a(Ljava/lang/String;)Z
    .registers 2
    .parameter

    .prologue
    .line 357
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->f()Lcom/dropbox/android/util/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/at;->b()Ljava/io/File;

    move-result-object v0

    .line 358
    if-eqz v0, :cond_14

    .line 359
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    .line 361
    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private static b(Landroid/support/v4/app/FragmentActivity;Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 326
    const-string v0, "no.viewer"

    invoke-static {v0, p1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 327
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->a(Ljava/lang/String;)Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/NoViewerDialogFrag;->a(Landroid/support/v4/app/j;)V

    .line 328
    return-void
.end method
