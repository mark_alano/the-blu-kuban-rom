.class public Lcom/dropbox/android/util/bg;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static a:Ljava/lang/String;


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 146
    const-class v0, Lcom/dropbox/android/util/bg;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/bg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/util/bg;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/dropbox/android/activity/ax;ILandroid/net/Uri;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 151
    invoke-static {}, Lcom/dropbox/android/a;->a()Landroid/content/Context;

    move-result-object v0

    .line 153
    sparse-switch p2, :sswitch_data_8a

    .line 211
    sget-object v0, Lcom/dropbox/android/util/bg;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message received from DropboxProvider: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :goto_21
    :sswitch_21
    return-void

    .line 155
    :sswitch_22
    iput-boolean v1, p0, Lcom/dropbox/android/util/bg;->b:Z

    .line 156
    const v1, 0x7f0b003a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-interface {p1, v0}, Lcom/dropbox/android/activity/ax;->e(Ljava/lang/String;)V

    goto :goto_21

    .line 161
    :sswitch_2f
    iput-boolean v1, p0, Lcom/dropbox/android/util/bg;->b:Z

    .line 162
    const v1, 0x7f0b003e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 163
    invoke-interface {p1, v0}, Lcom/dropbox/android/activity/ax;->d(Ljava/lang/String;)V

    .line 164
    invoke-interface {p1, v2}, Lcom/dropbox/android/activity/ax;->a(Z)V

    goto :goto_21

    .line 168
    :sswitch_3f
    iput-boolean v1, p0, Lcom/dropbox/android/util/bg;->b:Z

    .line 169
    invoke-interface {p1, v2}, Lcom/dropbox/android/activity/ax;->a(Z)V

    goto :goto_21

    .line 173
    :sswitch_45
    iput-boolean v2, p0, Lcom/dropbox/android/util/bg;->b:Z

    .line 174
    const v1, 0x7f0b003b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-interface {p1, v0}, Lcom/dropbox/android/activity/ax;->e(Ljava/lang/String;)V

    goto :goto_21

    .line 183
    :sswitch_52
    const v1, 0x7f0b0041

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/dropbox/android/activity/ax;->d(Ljava/lang/String;)V

    goto :goto_21

    .line 189
    :sswitch_5d
    iget-boolean v1, p0, Lcom/dropbox/android/util/bg;->b:Z

    if-eqz v1, :cond_6c

    .line 190
    const v1, 0x7f0b003c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 194
    :goto_68
    invoke-interface {p1, v0}, Lcom/dropbox/android/activity/ax;->d(Ljava/lang/String;)V

    goto :goto_21

    .line 192
    :cond_6c
    const v1, 0x7f0b003d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_68

    .line 198
    :sswitch_74
    const v1, 0x7f0b003f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-interface {p1, v0}, Lcom/dropbox/android/activity/ax;->d(Ljava/lang/String;)V

    goto :goto_21

    .line 203
    :sswitch_7f
    const v1, 0x7f0b0042

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 204
    invoke-interface {p1, v0}, Lcom/dropbox/android/activity/ax;->d(Ljava/lang/String;)V

    goto :goto_21

    .line 153
    :sswitch_data_8a
    .sparse-switch
        0x0 -> :sswitch_22
        0x1 -> :sswitch_2f
        0x2 -> :sswitch_3f
        0x3 -> :sswitch_45
        0x8 -> :sswitch_21
        0x10 -> :sswitch_52
        0x11 -> :sswitch_5d
        0x12 -> :sswitch_74
        0x13 -> :sswitch_7f
        0xff -> :sswitch_21
    .end sparse-switch
.end method
