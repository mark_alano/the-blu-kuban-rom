.class public final enum Lcom/dropbox/android/util/f;
.super Ljava/lang/Enum;
.source "panda.py"


# static fields
.field public static final enum a:Lcom/dropbox/android/util/f;

.field public static final enum b:Lcom/dropbox/android/util/f;

.field public static final enum c:Lcom/dropbox/android/util/f;

.field public static final enum d:Lcom/dropbox/android/util/f;

.field private static final synthetic e:[Lcom/dropbox/android/util/f;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 124
    new-instance v0, Lcom/dropbox/android/util/f;

    const-string v1, "NO_CONFLICTS"

    invoke-direct {v0, v1, v2}, Lcom/dropbox/android/util/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/f;->a:Lcom/dropbox/android/util/f;

    .line 125
    new-instance v0, Lcom/dropbox/android/util/f;

    const-string v1, "OVERWRITE"

    invoke-direct {v0, v1, v3}, Lcom/dropbox/android/util/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/f;->b:Lcom/dropbox/android/util/f;

    .line 126
    new-instance v0, Lcom/dropbox/android/util/f;

    const-string v1, "UPLOAD_NEW_ONLY"

    invoke-direct {v0, v1, v4}, Lcom/dropbox/android/util/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/f;->c:Lcom/dropbox/android/util/f;

    .line 127
    new-instance v0, Lcom/dropbox/android/util/f;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v5}, Lcom/dropbox/android/util/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/android/util/f;->d:Lcom/dropbox/android/util/f;

    .line 123
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dropbox/android/util/f;

    sget-object v1, Lcom/dropbox/android/util/f;->a:Lcom/dropbox/android/util/f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dropbox/android/util/f;->b:Lcom/dropbox/android/util/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dropbox/android/util/f;->c:Lcom/dropbox/android/util/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dropbox/android/util/f;->d:Lcom/dropbox/android/util/f;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dropbox/android/util/f;->e:[Lcom/dropbox/android/util/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/android/util/f;
    .registers 2
    .parameter

    .prologue
    .line 123
    const-class v0, Lcom/dropbox/android/util/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/util/f;

    return-object v0
.end method

.method public static values()[Lcom/dropbox/android/util/f;
    .registers 1

    .prologue
    .line 123
    sget-object v0, Lcom/dropbox/android/util/f;->e:[Lcom/dropbox/android/util/f;

    invoke-virtual {v0}, [Lcom/dropbox/android/util/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/util/f;

    return-object v0
.end method
