.class public Lcom/dropbox/android/util/i;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/util/TimerTask;

.field private static final c:Ljava/lang/Object;

.field private static d:Ljava/io/File;

.field private static e:Ljava/io/BufferedWriter;

.field private static f:Ljava/io/File;

.field private static volatile g:Z

.field private static final h:Ljava/util/concurrent/atomic/AtomicLong;

.field private static final i:Ljava/util/Timer;

.field private static j:I

.field private static final k:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    const-class v0, Lcom/dropbox/android/util/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    .line 73
    sput-object v1, Lcom/dropbox/android/util/i;->b:Ljava/util/TimerTask;

    .line 603
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/i;->c:Ljava/lang/Object;

    .line 609
    sput-object v1, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    .line 614
    sput-object v1, Lcom/dropbox/android/util/i;->f:Ljava/io/File;

    .line 621
    sput-boolean v2, Lcom/dropbox/android/util/i;->g:Z

    .line 626
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/i;->h:Ljava/util/concurrent/atomic/AtomicLong;

    .line 742
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/i;->i:Ljava/util/Timer;

    .line 798
    sput v2, Lcom/dropbox/android/util/i;->j:I

    .line 1297
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/i;->k:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1013
    return-void
.end method

.method public static A()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 207
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "video.info"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static B()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 215
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "video.size.mismatch"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static C()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 223
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "video.seek.limited"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static D()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 231
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "need.dotless.intent"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static E()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 235
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "uncaught.exception"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static F()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 239
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "upload.queue.bump"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static G()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 243
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "upload.queue.schedule"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static H()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 247
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "camera.gallery.refresh"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static I()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 251
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "unknown.file.extension"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static J()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 255
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "gallery.showing.image.set"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static K()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 259
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "gallery.new.image.shown"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static L()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 263
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "gallery.pinch.start"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static M()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 267
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "gallery.pinch.end"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static N()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 271
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "get.content.request"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static O()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 275
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "get.content.result"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static P()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 285
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "app.link"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static Q()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 289
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "app.unlink"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static R()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 293
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "login.twofactor.prompted"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static S()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 297
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "login.twofactor.success"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static T()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 301
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "login.twofactor.didntreceive"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static U()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 306
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "cu.turned_on"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static V()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 310
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "cu.turned_off"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static W()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 314
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "cu.skipped"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static X()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 318
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "share_link.generate"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static Y()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 325
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "image.view"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static Z()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 329
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "folder.rename"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 89
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "tour.view"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/content/BroadcastReceiver;Landroid/content/Intent;)Lcom/dropbox/android/util/s;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 496
    const/4 v0, 0x0

    .line 497
    if-eqz p1, :cond_7

    .line 498
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 500
    :cond_7
    new-instance v1, Lcom/dropbox/android/util/s;

    const-string v2, "broadcast.received"

    invoke-direct {v1, v2}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    const-string v2, "class"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "intent.action"

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/app/Activity;)Lcom/dropbox/android/util/s;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 466
    new-instance v0, Lcom/dropbox/android/util/s;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "act."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    .line 467
    const-string v1, "create"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_36

    const-string v1, "network_state"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    .line 468
    :cond_36
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 469
    if-eqz v1, :cond_47

    .line 470
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 471
    if-eqz v1, :cond_47

    .line 472
    const-string v2, "intent.action"

    invoke-virtual {v0, v2, v1}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 475
    :cond_47
    invoke-virtual {p1}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    .line 476
    if-eqz v1, :cond_56

    .line 477
    const-string v2, "caller"

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 480
    :cond_56
    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/app/Service;)Lcom/dropbox/android/util/s;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 484
    new-instance v0, Lcom/dropbox/android/util/s;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "service."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/support/v4/app/Fragment;)Lcom/dropbox/android/util/s;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 488
    new-instance v0, Lcom/dropbox/android/util/s;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "frag."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/s;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 435
    new-instance v0, Lcom/dropbox/android/util/s;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    const-string v1, "mime"

    iget-object v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "extension"

    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->t(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "size"

    iget-wide v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    .line 438
    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/k;)Lcom/dropbox/android/util/s;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 449
    new-instance v0, Lcom/dropbox/android/util/s;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "download."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    const-string v1, "id"

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/k;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 507
    new-instance v0, Lcom/dropbox/android/util/s;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notification."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    const-string v1, "notification"

    invoke-virtual {v0, v1, p1}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/io/BufferedReader;)Ljava/util/Map;
    .registers 5
    .parameter

    .prologue
    .line 1266
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 1268
    new-instance v2, Ldbxyzptlk/E/b;

    invoke-direct {v2}, Ldbxyzptlk/E/b;-><init>()V

    .line 1270
    :cond_a
    :goto_a
    invoke-virtual {p0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 1271
    if-nez v0, :cond_11

    .line 1288
    return-object v1

    .line 1275
    :cond_11
    :try_start_11
    invoke-virtual {v2, v0}, Ldbxyzptlk/E/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1276
    instance-of v3, v0, Ljava/util/Map;

    if-eqz v3, :cond_a

    .line 1278
    check-cast v0, Ljava/util/Map;

    .line 1279
    const-string v3, "event"

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1280
    if-eqz v0, :cond_a

    .line 1281
    invoke-static {v1, v0}, Lcom/dropbox/android/util/i;->a(Ljava/util/Map;Ljava/lang/String;)V
    :try_end_28
    .catch Ldbxyzptlk/E/c; {:try_start_11 .. :try_end_28} :catch_29

    goto :goto_a

    .line 1284
    :catch_29
    move-exception v0

    .line 1285
    const-string v0, "skipped.log.parse.exception"

    invoke-static {v1, v0}, Lcom/dropbox/android/util/i;->a(Ljava/util/Map;Ljava/lang/String;)V

    goto :goto_a
.end method

.method public static a(J)V
    .registers 4
    .parameter

    .prologue
    .line 725
    sget-boolean v0, Lcom/dropbox/android/util/i;->g:Z

    if-nez v0, :cond_5

    .line 737
    :goto_4
    return-void

    .line 728
    :cond_5
    invoke-static {}, Lcom/dropbox/android/util/i;->av()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 729
    sget-object v0, Lcom/dropbox/android/util/i;->i:Ljava/util/Timer;

    new-instance v1, Lcom/dropbox/android/util/m;

    invoke-direct {v1}, Lcom/dropbox/android/util/m;-><init>()V

    invoke-virtual {v0, v1, p0, p1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_4
.end method

.method public static a(Lcom/dropbox/android/util/r;)V
    .registers 7
    .parameter

    .prologue
    const/16 v3, 0xa

    .line 805
    sget-boolean v0, Lcom/dropbox/android/util/i;->g:Z

    if-nez v0, :cond_c

    .line 806
    sget-object v0, Lcom/dropbox/android/util/i;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    .line 841
    :cond_b
    :goto_b
    return-void

    .line 820
    :cond_c
    :try_start_c
    invoke-interface {p0}, Lcom/dropbox/android/util/r;->a()Ljava/lang/String;

    move-result-object v0

    .line 821
    if-eqz v0, :cond_b

    .line 822
    sget-object v1, Lcom/dropbox/android/util/i;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_15} :catch_48

    .line 823
    :try_start_15
    sget-object v2, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 824
    sget-object v0, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(I)V

    .line 825
    sget v0, Lcom/dropbox/android/util/i;->j:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/dropbox/android/util/i;->j:I

    .line 826
    sget v0, Lcom/dropbox/android/util/i;->j:I

    if-lt v0, v3, :cond_43

    .line 827
    const/4 v0, 0x0

    sput v0, Lcom/dropbox/android/util/i;->j:I

    .line 828
    sget-object v0, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    .line 829
    sget-object v0, Lcom/dropbox/android/util/i;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v4, 0x80000

    cmp-long v0, v2, v4

    if-lez v0, :cond_43

    .line 830
    invoke-static {}, Lcom/dropbox/android/util/i;->aA()V

    .line 833
    :cond_43
    monitor-exit v1

    goto :goto_b

    :catchall_45
    move-exception v0

    monitor-exit v1
    :try_end_47
    .catchall {:try_start_15 .. :try_end_47} :catchall_45

    :try_start_47
    throw v0
    :try_end_48
    .catch Ljava/io/IOException; {:try_start_47 .. :try_end_48} :catch_48

    .line 838
    :catch_48
    move-exception v0

    .line 839
    sget-object v1, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v2, "logEvent"

    invoke-static {v1, v2, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_b
.end method

.method private static a(Ldbxyzptlk/j/g;)V
    .registers 6
    .parameter

    .prologue
    .line 747
    sget-object v1, Lcom/dropbox/android/util/i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 749
    :try_start_3
    sget-object v0, Lcom/dropbox/android/util/i;->d:Ljava/io/File;

    invoke-static {v0}, Lcom/dropbox/android/util/P;->a(Ljava/io/File;)Ljava/io/OutputStream;

    move-result-object v0

    .line 750
    if-nez v0, :cond_14

    .line 751
    sget-object v0, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v2, "Couldn\'t create stream."

    invoke-static {v0, v2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_50
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_12} :catch_53

    .line 752
    :try_start_12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_12 .. :try_end_13} :catchall_50

    .line 785
    :goto_13
    return-void

    .line 754
    :cond_14
    :try_start_14
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/OutputStreamWriter;

    invoke-direct {v3, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v0, 0x2000

    invoke-direct {v2, v3, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    sput-object v2, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    .line 762
    new-instance v0, Lcom/dropbox/android/util/t;

    invoke-direct {v0, p0}, Lcom/dropbox/android/util/t;-><init>(Ldbxyzptlk/j/g;)V

    .line 763
    sget-boolean v2, Lcom/dropbox/android/util/i;->g:Z

    if-nez v2, :cond_36

    .line 764
    const-string v2, "early.event.count"

    sget-object v3, Lcom/dropbox/android/util/i;->h:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 766
    :cond_36
    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->a()Ljava/lang/String;

    move-result-object v0

    .line 767
    sget-object v2, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    invoke-virtual {v2, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 768
    sget-object v0, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Ljava/io/BufferedWriter;->write(I)V

    .line 769
    sget-object v0, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    .line 773
    const/4 v0, 0x1

    sput-boolean v0, Lcom/dropbox/android/util/i;->g:Z
    :try_end_4e
    .catchall {:try_start_14 .. :try_end_4e} :catchall_50
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_4e} :catch_53

    .line 784
    :goto_4e
    :try_start_4e
    monitor-exit v1

    goto :goto_13

    :catchall_50
    move-exception v0

    monitor-exit v1
    :try_end_52
    .catchall {:try_start_4e .. :try_end_52} :catchall_50

    throw v0

    .line 774
    :catch_53
    move-exception v0

    .line 777
    :try_start_54
    sget-object v2, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initWriter, sInitialized = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/dropbox/android/util/i;->g:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 778
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v2, v0, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V
    :try_end_77
    .catchall {:try_start_54 .. :try_end_77} :catchall_50

    goto :goto_4e
.end method

.method public static a(Ljava/io/File;)V
    .registers 7
    .parameter

    .prologue
    .line 634
    sget-object v1, Lcom/dropbox/android/util/i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 635
    :try_start_3
    sget-object v0, Lcom/dropbox/android/util/i;->d:Ljava/io/File;

    if-eqz v0, :cond_10

    .line 636
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 643
    :catchall_d
    move-exception v0

    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    throw v0

    .line 638
    :cond_10
    :try_start_10
    invoke-virtual {p0}, Ljava/io/File;->mkdirs()Z

    .line 639
    new-instance v0, Ljava/io/File;

    const-string v2, "dbl.dbl"

    invoke-direct {v0, p0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v0, Lcom/dropbox/android/util/i;->d:Ljava/io/File;

    .line 640
    sput-object p0, Lcom/dropbox/android/util/i;->f:Ljava/io/File;

    .line 642
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/i;->a(Ldbxyzptlk/j/g;)V

    .line 643
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_10 .. :try_end_2a} :catchall_d

    .line 645
    sget-boolean v0, Lcom/dropbox/android/util/i;->g:Z

    if-nez v0, :cond_2f

    .line 671
    :goto_2e
    return-void

    .line 651
    :cond_2f
    invoke-static {}, Lcom/dropbox/android/util/i;->aB()V

    .line 653
    invoke-static {}, Lcom/dropbox/android/util/i;->aD()V

    .line 663
    sget-object v0, Lcom/dropbox/android/util/i;->i:Ljava/util/Timer;

    new-instance v1, Lcom/dropbox/android/util/j;

    invoke-direct {v1}, Lcom/dropbox/android/util/j;-><init>()V

    const-wide/16 v2, 0x1388

    const-wide/32 v4, 0x1808580

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_2e
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 1233
    const/4 v1, 0x1

    .line 1234
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1235
    if-eqz v0, :cond_16

    .line 1236
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    .line 1238
    :goto_e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1239
    return-void

    :cond_16
    move v0, v1

    goto :goto_e
.end method

.method private static a(Ljava/io/File;Ljava/util/Map;)Z
    .registers 11
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1180
    invoke-static {p0}, Lcom/dropbox/android/util/i;->c(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 1181
    if-nez v0, :cond_9

    move v0, v2

    .line 1227
    :goto_8
    return v0

    .line 1189
    :cond_9
    :try_start_9
    const-string v1, "skipped"

    const-string v3, "tmp"

    invoke-virtual {p0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-static {v1, v3, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_14} :catch_24

    move-result-object v3

    .line 1197
    :try_start_15
    invoke-static {v3}, Lcom/dropbox/android/util/P;->a(Ljava/io/File;)Ljava/io/OutputStream;

    move-result-object v1

    .line 1198
    if-nez v1, :cond_27

    .line 1199
    sget-object v0, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v1, "Couldn\'t create stream."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 1200
    goto :goto_8

    .line 1190
    :catch_24
    move-exception v0

    move v0, v2

    .line 1191
    goto :goto_8

    .line 1202
    :cond_27
    new-instance v4, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    invoke-direct {v5, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v1, 0x2000

    invoke-direct {v4, v5, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    .line 1203
    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1204
    const/16 v0, 0xa

    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 1207
    invoke-static {}, Lcom/dropbox/android/util/i;->au()Lcom/dropbox/android/util/s;

    move-result-object v5

    .line 1208
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_47
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_75

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1209
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v7, v1

    invoke-virtual {v5, v0, v7, v8}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;
    :try_end_61
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_61} :catch_62

    goto :goto_47

    .line 1215
    :catch_62
    move-exception v0

    .line 1216
    sget-object v1, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v3, "failedToReplaceContents"

    invoke-static {v1, v3}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    move v0, v2

    .line 1218
    goto :goto_8

    .line 1211
    :cond_75
    :try_start_75
    invoke-virtual {v5}, Lcom/dropbox/android/util/s;->a()Ljava/lang/String;

    move-result-object v0

    .line 1212
    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 1213
    const/16 v0, 0xa

    invoke-virtual {v4, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 1214
    invoke-virtual {v4}, Ljava/io/BufferedWriter;->close()V
    :try_end_84
    .catch Ljava/io/IOException; {:try_start_75 .. :try_end_84} :catch_62

    .line 1222
    invoke-virtual {v3, p0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 1223
    const/4 v0, 0x1

    goto/16 :goto_8

    .line 1225
    :cond_8d
    sget-object v0, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v1, "failedToRenameTempFile"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move v0, v2

    .line 1227
    goto/16 :goto_8
.end method

.method private static a(Ljava/util/Map;)Z
    .registers 10
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 1137
    .line 1140
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "broadcast.received"

    aput-object v1, v0, v3

    const-string v1, "upload.start"

    aput-object v1, v0, v6

    const/4 v1, 0x2

    const-string v2, "upload.enqueue"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "upload.cancel"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "open.log"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "cameraupload.scan.finished"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "cache.cleanup.remove"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "camera.upload.fullscan.event"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "control.change"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "media.playbackstate.change"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "upload.queue.schedule"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "battery.level"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "download.start"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "reachability.change"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "upload.queue.bump"

    aput-object v2, v0, v1

    .line 1145
    new-instance v7, Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1150
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v3

    move v4, v3

    move v5, v3

    :cond_6a
    :goto_6a
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_96

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1151
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1152
    add-int/2addr v5, v1

    .line 1153
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6a

    .line 1156
    if-le v1, v4, :cond_91

    move v0, v4

    :goto_8e
    move v2, v0

    move v4, v1

    .line 1162
    goto :goto_6a

    .line 1159
    :cond_91
    if-le v1, v2, :cond_b0

    move v0, v2

    move v1, v4

    .line 1160
    goto :goto_8e

    .line 1164
    :cond_96
    int-to-double v0, v5

    const-wide v7, 0x406f400000000000L

    cmpl-double v0, v0, v7

    if-ltz v0, :cond_ae

    add-int v0, v4, v2

    div-int/2addr v0, v5

    int-to-double v0, v0

    const-wide v4, 0x3fefae147ae147aeL

    cmpl-double v0, v0, v4

    if-lez v0, :cond_ae

    .line 1168
    :goto_ad
    return v3

    :cond_ae
    move v3, v6

    goto :goto_ad

    :cond_b0
    move v0, v2

    move v1, v4

    goto :goto_8e
.end method

.method private static aA()V
    .registers 4

    .prologue
    .line 687
    sget-object v0, Lcom/dropbox/android/util/i;->i:Ljava/util/Timer;

    new-instance v1, Lcom/dropbox/android/util/k;

    invoke-direct {v1}, Lcom/dropbox/android/util/k;-><init>()V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 701
    return-void
.end method

.method private static declared-synchronized aB()V
    .registers 14

    .prologue
    const-wide/32 v12, 0x15180

    const-wide/16 v10, 0x3e8

    .line 867
    const-class v4, Lcom/dropbox/android/util/i;

    monitor-enter v4

    :try_start_8
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v5

    .line 868
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v6, v0, v2

    .line 869
    invoke-virtual {v5}, Ldbxyzptlk/l/m;->F()J

    move-result-wide v0

    .line 871
    sget-object v2, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "nextRotation: "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_5b

    .line 875
    add-long v0, v6, v12

    .line 876
    invoke-virtual {v5, v0, v1}, Ldbxyzptlk/l/m;->b(J)V

    .line 877
    sget-object v2, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "no scheduled rotation, scheduling it for "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sub-long v8, v0, v6

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " seconds from now."

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    :cond_5b
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/i;->b(Ldbxyzptlk/j/g;)Ljava/lang/String;

    move-result-object v3

    .line 881
    invoke-virtual {v5}, Ldbxyzptlk/l/m;->G()Ljava/lang/String;

    move-result-object v2

    .line 882
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7d

    .line 883
    invoke-virtual {v5, v3}, Ldbxyzptlk/l/m;->h(Ljava/lang/String;)V

    .line 886
    const-string v5, ""

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7d

    move-object v2, v3

    .line 891
    :cond_7d
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c8

    const/4 v2, 0x1

    .line 893
    :goto_84
    cmp-long v5, v6, v0

    if-gez v5, :cond_8a

    if-eqz v2, :cond_ba

    .line 894
    :cond_8a
    sget-object v0, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Version changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    invoke-static {}, Lcom/dropbox/android/util/i;->aw()Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 896
    invoke-static {}, Lcom/dropbox/android/util/i;->aC()V

    .line 897
    sget-object v0, Lcom/dropbox/android/util/i;->i:Ljava/util/Timer;

    new-instance v1, Lcom/dropbox/android/util/n;

    invoke-direct {v1}, Lcom/dropbox/android/util/n;-><init>()V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 905
    add-long v0, v6, v12

    .line 909
    :cond_ba
    sub-long/2addr v0, v6

    mul-long/2addr v0, v10

    .line 910
    sget-object v2, Lcom/dropbox/android/util/i;->i:Ljava/util/Timer;

    new-instance v3, Lcom/dropbox/android/util/o;

    invoke-direct {v3}, Lcom/dropbox/android/util/o;-><init>()V

    invoke-virtual {v2, v3, v0, v1}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_c6
    .catchall {:try_start_8 .. :try_end_c6} :catchall_ca

    .line 918
    monitor-exit v4

    return-void

    .line 891
    :cond_c8
    const/4 v2, 0x0

    goto :goto_84

    .line 867
    :catchall_ca
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method private static aC()V
    .registers 9

    .prologue
    .line 944
    sget-object v0, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v1, "Rotating."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    const/4 v1, 0x0

    .line 947
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v3

    .line 949
    sget-object v4, Lcom/dropbox/android/util/i;->c:Ljava/lang/Object;

    monitor-enter v4

    .line 951
    :try_start_f
    sget-object v0, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_14
    .catchall {:try_start_f .. :try_end_14} :catchall_76
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_14} :catch_6d

    .line 956
    :goto_14
    :try_start_14
    sget-object v0, Lcom/dropbox/android/util/i;->d:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v0, v5, v7

    if-nez v0, :cond_79

    .line 957
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    new-instance v2, Ljava/lang/Throwable;

    const-string v5, "Zero-length file."

    invoke-direct {v2, v5}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sget-object v5, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v0, v2, v5}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 958
    sget-object v0, Lcom/dropbox/android/util/i;->d:Ljava/io/File;

    invoke-static {v0}, Lcom/dropbox/android/util/i;->g(Ljava/io/File;)Z

    move-result v0

    move v2, v0

    .line 974
    :goto_37
    if-eqz v2, :cond_c2

    .line 975
    invoke-static {}, Lcom/dropbox/android/util/i;->aE()V

    .line 976
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v0

    .line 977
    invoke-static {v0}, Lcom/dropbox/android/util/i;->a(Ldbxyzptlk/j/g;)V

    .line 979
    :goto_47
    monitor-exit v4
    :try_end_48
    .catchall {:try_start_14 .. :try_end_48} :catchall_76

    .line 981
    if-eqz v2, :cond_5e

    .line 983
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/dropbox/android/util/p;

    invoke-direct {v2}, Lcom/dropbox/android/util/p;-><init>()V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 991
    invoke-static {v0}, Lcom/dropbox/android/util/i;->b(Ldbxyzptlk/j/g;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ldbxyzptlk/l/m;->h(Ljava/lang/String;)V

    .line 994
    :cond_5e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    .line 995
    const-wide/32 v4, 0x15180

    add-long/2addr v0, v4

    invoke-virtual {v3, v0, v1}, Ldbxyzptlk/l/m;->b(J)V

    .line 996
    return-void

    .line 952
    :catch_6d
    move-exception v0

    .line 953
    :try_start_6e
    sget-object v2, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v5, "rotateLog"

    invoke-static {v2, v5, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_14

    .line 979
    :catchall_76
    move-exception v0

    monitor-exit v4
    :try_end_78
    .catchall {:try_start_6e .. :try_end_78} :catchall_76

    throw v0

    .line 966
    :cond_79
    :try_start_79
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/dropbox/android/util/i;->f:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dbup-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".dbu"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v2, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 967
    sget-object v2, Lcom/dropbox/android/util/i;->d:Ljava/io/File;

    invoke-virtual {v2, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_be

    .line 968
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    new-instance v2, Ljava/lang/Throwable;

    const-string v5, "Failed to rename, deleting instead"

    invoke-direct {v2, v5}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sget-object v5, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v0, v2, v5}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 969
    sget-object v0, Lcom/dropbox/android/util/i;->d:Ljava/io/File;

    invoke-static {v0}, Lcom/dropbox/android/util/i;->f(Ljava/io/File;)Z
    :try_end_ba
    .catchall {:try_start_79 .. :try_end_ba} :catchall_76

    move-result v0

    move v2, v0

    goto/16 :goto_37

    .line 971
    :cond_be
    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_37

    :cond_c2
    move-object v0, v1

    goto :goto_47
.end method

.method private static aD()V
    .registers 2

    .prologue
    .line 1066
    invoke-static {}, Ldbxyzptlk/l/a;->a()Ldbxyzptlk/l/a;

    move-result-object v0

    .line 1067
    invoke-virtual {v0}, Ldbxyzptlk/l/a;->k()Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_13

    invoke-virtual {v0}, Ldbxyzptlk/l/a;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_13

    .line 1068
    invoke-static {}, Lcom/dropbox/android/util/i;->an()V

    .line 1070
    :cond_13
    return-void
.end method

.method private static aE()V
    .registers 2

    .prologue
    .line 1073
    invoke-static {}, Ldbxyzptlk/l/a;->a()Ldbxyzptlk/l/a;

    move-result-object v0

    .line 1074
    invoke-virtual {v0}, Ldbxyzptlk/l/a;->k()Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_13

    .line 1075
    invoke-virtual {v0}, Ldbxyzptlk/l/a;->c()I

    move-result v1

    .line 1076
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/a;->a(I)V

    .line 1078
    :cond_13
    return-void
.end method

.method private static aF()V
    .registers 23

    .prologue
    .line 1342
    invoke-static {}, Lcom/dropbox/android/util/T;->a()V

    .line 1343
    invoke-static {}, Lcom/dropbox/android/util/i;->aG()Z

    move-result v2

    if-nez v2, :cond_a

    .line 1421
    :goto_9
    return-void

    .line 1346
    :cond_a
    sget-object v20, Lcom/dropbox/android/util/i;->k:Ljava/lang/Object;

    monitor-enter v20

    .line 1347
    :try_start_d
    sget-object v2, Lcom/dropbox/android/util/i;->f:Ljava/io/File;

    invoke-static {v2}, Lcom/dropbox/android/util/i;->b(Ljava/io/File;)[Ljava/io/File;

    move-result-object v21

    .line 1348
    sget-object v2, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Found "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v21

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " matching files."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1349
    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v22, v0

    const/4 v2, 0x0

    move/from16 v19, v2

    :goto_3c
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_154

    aget-object v3, v21, v19

    .line 1350
    invoke-static {v3}, Lcom/dropbox/android/util/i;->d(Ljava/io/File;)Ldbxyzptlk/j/g;

    move-result-object v2

    .line 1351
    if-nez v2, :cond_15b

    .line 1352
    sget-object v2, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v4, "Couldn\'t parse header, using current UserInfo."

    invoke-static {v2, v4}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 1357
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v2

    move-object/from16 v18, v2

    .line 1362
    :goto_5b
    invoke-static {v3}, Lcom/dropbox/android/util/i;->e(Ljava/io/File;)Ljava/util/Map;

    move-result-object v2

    .line 1363
    if-eqz v2, :cond_72

    invoke-static {v2}, Lcom/dropbox/android/util/i;->a(Ljava/util/Map;)Z

    move-result v4

    if-nez v4, :cond_72

    .line 1364
    invoke-static {v3, v2}, Lcom/dropbox/android/util/i;->a(Ljava/io/File;Ljava/util/Map;)Z

    move-result v2

    .line 1365
    if-nez v2, :cond_72

    .line 1349
    :cond_6d
    :goto_6d
    add-int/lit8 v2, v19, 0x1

    move/from16 v19, v2

    goto :goto_3c

    .line 1370
    :cond_72
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "dbup-"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_157

    .line 1371
    invoke-static {v3}, Lcom/dropbox/android/util/i;->h(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    .line 1372
    if-eqz v2, :cond_157

    move-object/from16 v17, v2

    .line 1376
    :goto_86
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "gzdbup-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    .line 1379
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v4, 0x400000

    cmp-long v2, v2, v4

    if-lez v2, :cond_b3

    .line 1380
    invoke-static {}, Lcom/dropbox/android/util/i;->ay()Lcom/dropbox/android/util/s;

    move-result-object v2

    const-string v3, "size"

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/s;->c()V

    .line 1381
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/i;->g(Ljava/io/File;)Z

    goto :goto_6d

    .line 1420
    :catchall_b0
    move-exception v2

    monitor-exit v20
    :try_end_b2
    .catchall {:try_start_d .. :try_end_b2} :catchall_b0

    throw v2

    .line 1388
    :cond_b3
    :try_start_b3
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v10, v2, v4
    :try_end_bb
    .catchall {:try_start_b3 .. :try_end_bb} :catchall_b0

    .line 1389
    const/4 v2, 0x0

    move/from16 v16, v2

    :goto_be
    const/4 v2, 0x2

    move/from16 v0, v16

    if-ge v0, v2, :cond_6d

    .line 1390
    const/4 v12, 0x0

    .line 1392
    :try_start_c4
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/P;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v12

    .line 1393
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v2

    iget-object v2, v2, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    const-string v3, "android"

    move-object/from16 v0, v18

    iget-object v4, v0, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    sget-object v5, Ldbxyzptlk/j/e;->d:Ldbxyzptlk/j/e;

    move-object/from16 v0, v18

    iget-object v6, v0, Ldbxyzptlk/j/g;->h:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v7, v0, Ldbxyzptlk/j/g;->f:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v8, v0, Ldbxyzptlk/j/g;->d:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v9, v0, Ldbxyzptlk/j/g;->e:Ljava/lang/String;

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v13

    invoke-virtual/range {v2 .. v15}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/j/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/io/InputStream;JZ)V

    .line 1397
    invoke-static {}, Lcom/dropbox/android/util/i;->ax()Lcom/dropbox/android/util/s;

    move-result-object v2

    const-string v3, "size"

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v2

    const-string v3, "attempt"

    move/from16 v0, v16

    int-to-long v4, v0

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v2

    const-string v3, "gzipped"

    invoke-virtual {v2, v3, v15}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dropbox/android/util/s;->c()V

    .line 1398
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/i;->g(Ljava/io/File;)Z
    :try_end_11a
    .catchall {:try_start_c4 .. :try_end_11a} :catchall_14f
    .catch Ldbxyzptlk/o/d; {:try_start_c4 .. :try_end_11a} :catch_11f
    .catch Ldbxyzptlk/o/a; {:try_start_c4 .. :try_end_11a} :catch_12f
    .catch Ljava/io/IOException; {:try_start_c4 .. :try_end_11a} :catch_13f

    .line 1416
    :try_start_11a
    invoke-static {v12}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V
    :try_end_11d
    .catchall {:try_start_11a .. :try_end_11d} :catchall_b0

    goto/16 :goto_6d

    .line 1400
    :catch_11f
    move-exception v2

    .line 1402
    :try_start_120
    sget-object v3, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v4, "Failed to upload"

    invoke-static {v3, v4, v2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_127
    .catchall {:try_start_120 .. :try_end_127} :catchall_14f

    .line 1416
    :try_start_127
    invoke-static {v12}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V
    :try_end_12a
    .catchall {:try_start_127 .. :try_end_12a} :catchall_b0

    .line 1389
    add-int/lit8 v2, v16, 0x1

    move/from16 v16, v2

    goto :goto_be

    .line 1403
    :catch_12f
    move-exception v2

    .line 1406
    :try_start_130
    sget-object v3, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v4, "Failed to upload"

    invoke-static {v3, v4, v2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1407
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/i;->g(Ljava/io/File;)Z
    :try_end_13a
    .catchall {:try_start_130 .. :try_end_13a} :catchall_14f

    .line 1416
    :try_start_13a
    invoke-static {v12}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V
    :try_end_13d
    .catchall {:try_start_13a .. :try_end_13d} :catchall_b0

    goto/16 :goto_6d

    .line 1409
    :catch_13f
    move-exception v2

    .line 1412
    :try_start_140
    sget-object v3, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v4, "Failed to upload"

    invoke-static {v3, v4, v2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1413
    invoke-static/range {v17 .. v17}, Lcom/dropbox/android/util/i;->g(Ljava/io/File;)Z
    :try_end_14a
    .catchall {:try_start_140 .. :try_end_14a} :catchall_14f

    .line 1416
    :try_start_14a
    invoke-static {v12}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    goto/16 :goto_6d

    :catchall_14f
    move-exception v2

    invoke-static {v12}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    throw v2

    .line 1420
    :cond_154
    monitor-exit v20
    :try_end_155
    .catchall {:try_start_14a .. :try_end_155} :catchall_b0

    goto/16 :goto_9

    :cond_157
    move-object/from16 v17, v3

    goto/16 :goto_86

    :cond_15b
    move-object/from16 v18, v2

    goto/16 :goto_5b
.end method

.method private static aG()Z
    .registers 2

    .prologue
    .line 1480
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/u;->b()Lcom/dropbox/android/service/x;

    move-result-object v0

    .line 1481
    invoke-virtual {v0}, Lcom/dropbox/android/service/x;->a()Z

    move-result v1

    if-eqz v1, :cond_16

    invoke-virtual {v0}, Lcom/dropbox/android/service/x;->d()Z

    move-result v0

    if-nez v0, :cond_16

    const/4 v0, 0x1

    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method

.method public static aa()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 337
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "tell_friends_about_dropbox"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ab()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 345
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "help.view_TOS"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ac()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 353
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "help.view_privacy"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ad()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 361
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "help.send_feedback"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ae()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 368
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "new_text_file"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static af()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 372
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "help.start_video"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ag()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 379
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "edit_existing_text_file"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ah()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 383
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "refresh_file_browser"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ai()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 397
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "password.reset.sent"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static aj()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 404
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "sdk.provider.query"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static ak()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 416
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "app.create"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static declared-synchronized al()V
    .registers 5

    .prologue
    .line 708
    const-class v1, Lcom/dropbox/android/util/i;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/dropbox/android/util/i;->b:Ljava/util/TimerTask;

    if-eqz v0, :cond_11

    .line 709
    sget-object v0, Lcom/dropbox/android/util/i;->b:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 710
    sget-object v0, Lcom/dropbox/android/util/i;->i:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 712
    :cond_11
    new-instance v0, Lcom/dropbox/android/util/l;

    invoke-direct {v0}, Lcom/dropbox/android/util/l;-><init>()V

    sput-object v0, Lcom/dropbox/android/util/i;->b:Ljava/util/TimerTask;

    .line 718
    sget-object v0, Lcom/dropbox/android/util/i;->i:Ljava/util/Timer;

    sget-object v2, Lcom/dropbox/android/util/i;->b:Ljava/util/TimerTask;

    const-wide/32 v3, 0x927c0

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_24

    .line 719
    monitor-exit v1

    return-void

    .line 708
    :catchall_24
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static am()V
    .registers 4

    .prologue
    .line 847
    sget-boolean v0, Lcom/dropbox/android/util/i;->g:Z

    if-nez v0, :cond_5

    .line 861
    :goto_4
    return-void

    .line 850
    :cond_5
    sget-object v1, Lcom/dropbox/android/util/i;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 852
    :try_start_8
    sget-object v0, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;
    :try_end_a
    .catchall {:try_start_8 .. :try_end_a} :catchall_e
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_a} :catch_1b

    if-nez v0, :cond_11

    .line 853
    :try_start_c
    monitor-exit v1

    goto :goto_4

    .line 860
    :catchall_e
    move-exception v0

    monitor-exit v1
    :try_end_10
    .catchall {:try_start_c .. :try_end_10} :catchall_e

    throw v0

    .line 855
    :cond_11
    :try_start_11
    sget-object v0, Lcom/dropbox/android/util/i;->e:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    .line 856
    const/4 v0, 0x0

    sput v0, Lcom/dropbox/android/util/i;->j:I
    :try_end_19
    .catchall {:try_start_11 .. :try_end_19} :catchall_e
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_19} :catch_1b

    .line 860
    :goto_19
    :try_start_19
    monitor-exit v1

    goto :goto_4

    .line 857
    :catch_1b
    move-exception v0

    .line 858
    sget-object v2, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    const-string v3, "flushLog"

    invoke-static {v2, v3, v0}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_23
    .catchall {:try_start_19 .. :try_end_23} :catchall_e

    goto :goto_19
.end method

.method public static an()V
    .registers 3

    .prologue
    .line 1046
    invoke-static {}, Ldbxyzptlk/l/a;->a()Ldbxyzptlk/l/a;

    move-result-object v1

    .line 1047
    const/4 v0, 0x0

    .line 1048
    invoke-virtual {v1}, Ldbxyzptlk/l/a;->k()Landroid/accounts/Account;

    move-result-object v2

    if-eqz v2, :cond_1a

    .line 1051
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1052
    invoke-virtual {v1, v0}, Ldbxyzptlk/l/a;->a(Ljava/lang/String;)V

    .line 1053
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ldbxyzptlk/l/a;->a(I)V

    .line 1055
    :cond_1a
    invoke-static {}, Lcom/dropbox/android/util/i;->az()Lcom/dropbox/android/util/s;

    move-result-object v1

    const-string v2, "series.uuid"

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 1056
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(J)V

    .line 1057
    return-void
.end method

.method static synthetic ao()V
    .registers 0

    .prologue
    .line 61
    invoke-static {}, Lcom/dropbox/android/util/i;->aF()V

    return-void
.end method

.method static synthetic ap()Ljava/lang/Object;
    .registers 1

    .prologue
    .line 61
    sget-object v0, Lcom/dropbox/android/util/i;->c:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic aq()Ljava/io/File;
    .registers 1

    .prologue
    .line 61
    sget-object v0, Lcom/dropbox/android/util/i;->d:Ljava/io/File;

    return-object v0
.end method

.method static synthetic ar()Lcom/dropbox/android/util/s;
    .registers 1

    .prologue
    .line 61
    invoke-static {}, Lcom/dropbox/android/util/i;->aw()Lcom/dropbox/android/util/s;

    move-result-object v0

    return-object v0
.end method

.method static synthetic as()V
    .registers 0

    .prologue
    .line 61
    invoke-static {}, Lcom/dropbox/android/util/i;->aC()V

    return-void
.end method

.method static synthetic at()V
    .registers 0

    .prologue
    .line 61
    invoke-static {}, Lcom/dropbox/android/util/i;->aB()V

    return-void
.end method

.method private static au()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 77
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "skipped.log"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static av()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 389
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "rotation.forced"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static aw()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 393
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "rotation.scheduled"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static ax()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 408
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "log.up"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static ay()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 412
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "deleted.big.log"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static az()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 428
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "logseries.newUUID"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static b()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 93
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "current.settings.state"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;Lcom/dropbox/android/taskqueue/k;)Lcom/dropbox/android/util/s;
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 458
    new-instance v0, Lcom/dropbox/android/util/s;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "upload."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    const-string v1, "id"

    invoke-virtual {p1}, Lcom/dropbox/android/taskqueue/k;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "class"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ldbxyzptlk/j/g;)Ljava/lang/String;
    .registers 5
    .parameter

    .prologue
    .line 1490
    const-string v0, "|"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Ldbxyzptlk/j/g;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Ldbxyzptlk/j/g;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Ldbxyzptlk/j/g;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Ldbxyzptlk/j/g;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Ldbxyzptlk/j/g;->g:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Ldbxyzptlk/j/g;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/io/File;)[Ljava/io/File;
    .registers 2
    .parameter

    .prologue
    .line 999
    new-instance v0, Lcom/dropbox/android/util/q;

    invoke-direct {v0}, Lcom/dropbox/android/util/q;-><init>()V

    .line 1005
    invoke-virtual {p0, v0}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 97
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "camera.upload.tour.btn"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static c(Ljava/io/File;)Ljava/lang/String;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1081
    .line 1083
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-static {p0}, Lcom/dropbox/android/util/i;->i(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_1d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_f} :catch_17

    .line 1084
    :try_start_f
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_25
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_12} :catch_27

    move-result-object v0

    .line 1088
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    :goto_16
    return-object v0

    .line 1085
    :catch_17
    move-exception v1

    move-object v1, v0

    .line 1088
    :goto_19
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    goto :goto_16

    :catchall_1d
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_21
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    throw v0

    :catchall_25
    move-exception v0

    goto :goto_21

    .line 1085
    :catch_27
    move-exception v2

    goto :goto_19
.end method

.method public static d()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 101
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "quickaction.btn.click"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static d(Ljava/io/File;)Ldbxyzptlk/j/g;
    .registers 9
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1097
    invoke-static {p0}, Lcom/dropbox/android/util/i;->c(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 1098
    if-nez v0, :cond_a

    move-object v0, v2

    .line 1123
    :goto_9
    return-object v0

    .line 1102
    :cond_a
    :try_start_a
    new-instance v3, Ldbxyzptlk/E/b;

    invoke-direct {v3}, Ldbxyzptlk/E/b;-><init>()V

    invoke-virtual {v3, v0}, Ldbxyzptlk/E/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1103
    instance-of v3, v0, Ljava/util/Map;

    if-eqz v3, :cond_97

    .line 1104
    new-instance v3, Ldbxyzptlk/j/g;

    invoke-direct {v3}, Ldbxyzptlk/j/g;-><init>()V

    .line 1106
    check-cast v0, Ljava/util/Map;

    .line 1107
    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "APP_VERSION"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "USER_ID"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "DEVICE_ID"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "PHONE_MODEL"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "ANDROID_VERSION"

    aput-object v6, v4, v5

    .line 1108
    array-length v5, v4

    :goto_3b
    if-ge v1, v5, :cond_62

    aget-object v6, v4, v1

    .line 1109
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_5f

    .line 1110
    sget-object v0, Lcom/dropbox/android/util/i;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Required header field missing or null: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 1111
    goto :goto_9

    .line 1108
    :cond_5f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3b

    .line 1114
    :cond_62
    const-string v1, "APP_VERSION"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v3, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    .line 1115
    const-string v1, "USER_ID"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v3, Ldbxyzptlk/j/g;->h:Ljava/lang/String;

    .line 1116
    const-string v1, "DEVICE_ID"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v3, Ldbxyzptlk/j/g;->f:Ljava/lang/String;

    .line 1117
    const-string v1, "PHONE_MODEL"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v3, Ldbxyzptlk/j/g;->d:Ljava/lang/String;

    .line 1118
    const-string v1, "ANDROID_VERSION"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Ldbxyzptlk/j/g;->e:Ljava/lang/String;
    :try_end_94
    .catch Ldbxyzptlk/E/c; {:try_start_a .. :try_end_94} :catch_9a

    move-object v0, v3

    .line 1119
    goto/16 :goto_9

    :cond_97
    move-object v0, v2

    .line 1121
    goto/16 :goto_9

    .line 1122
    :catch_9a
    move-exception v0

    move-object v0, v2

    .line 1123
    goto/16 :goto_9
.end method

.method public static e()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 105
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "quickaction.show"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static e(Ljava/io/File;)Ljava/util/Map;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1247
    .line 1250
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-static {p0}, Lcom/dropbox/android/util/i;->i(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_1d
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_f} :catch_17

    .line 1251
    :try_start_f
    invoke-static {v1}, Lcom/dropbox/android/util/i;->a(Ljava/io/BufferedReader;)Ljava/util/Map;
    :try_end_12
    .catchall {:try_start_f .. :try_end_12} :catchall_25
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_12} :catch_27

    move-result-object v0

    .line 1255
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    :goto_16
    return-object v0

    .line 1252
    :catch_17
    move-exception v1

    move-object v1, v0

    .line 1255
    :goto_19
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    goto :goto_16

    :catchall_1d
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_21
    invoke-static {v1}, Ldbxyzptlk/B/d;->a(Ljava/io/Reader;)V

    throw v0

    :catchall_25
    move-exception v0

    goto :goto_21

    .line 1252
    :catch_27
    move-exception v2

    goto :goto_19
.end method

.method public static f()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 109
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "quickaction.dismiss"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static f(Ljava/io/File;)Z
    .registers 3
    .parameter

    .prologue
    .line 1306
    invoke-static {p0}, Lcom/dropbox/android/util/i;->g(Ljava/io/File;)Z

    move-result v0

    .line 1307
    if-nez v0, :cond_9

    .line 1308
    const/4 v1, 0x0

    sput-boolean v1, Lcom/dropbox/android/util/i;->g:Z

    .line 1310
    :cond_9
    return v0
.end method

.method public static g()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 113
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "custom.intent.chooser"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static g(Ljava/io/File;)Z
    .registers 6
    .parameter

    .prologue
    .line 1319
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1320
    const/4 v0, 0x1

    .line 1327
    :cond_7
    :goto_7
    return v0

    .line 1322
    :cond_8
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 1323
    if-nez v0, :cond_7

    .line 1324
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    new-instance v2, Ljava/lang/Throwable;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to delete "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v2, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    goto :goto_7
.end method

.method public static h()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 117
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "cameraupload.scan.finished"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static h(Ljava/io/File;)Ljava/io/File;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1432
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1433
    const-string v2, "dbup-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 1434
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Wrong prefix."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1437
    :cond_15
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gzdbup-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "dbup-"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1438
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    invoke-static {v1}, Lcom/dropbox/android/util/i;->g(Ljava/io/File;)Z

    move-result v2

    .line 1441
    if-nez v2, :cond_42

    .line 1468
    :goto_41
    return-object v0

    .line 1448
    :cond_42
    :try_start_42
    invoke-static {p0}, Lcom/dropbox/android/util/P;->b(Ljava/io/File;)Ljava/io/InputStream;
    :try_end_45
    .catchall {:try_start_42 .. :try_end_45} :catchall_7d
    .catch Ljava/io/IOException; {:try_start_42 .. :try_end_45} :catch_70

    move-result-object v3

    .line 1449
    :try_start_46
    new-instance v2, Ljava/io/FileOutputStream;

    const/4 v4, 0x0

    invoke-direct {v2, v1, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    invoke-static {v2}, Lcom/dropbox/android/util/P;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v4

    .line 1450
    new-instance v2, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v2, v4}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_55
    .catchall {:try_start_46 .. :try_end_55} :catchall_88
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_55} :catch_8e

    .line 1451
    :try_start_55
    invoke-static {v3, v2}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)I

    .line 1453
    invoke-virtual {p0}, Ljava/io/File;->delete()Z
    :try_end_5b
    .catchall {:try_start_55 .. :try_end_5b} :catchall_8c
    .catch Ljava/io/IOException; {:try_start_55 .. :try_end_5b} :catch_91

    move-result v4

    if-eqz v4, :cond_66

    .line 1464
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 1465
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    move-object v0, v1

    goto :goto_41

    .line 1458
    :cond_66
    :try_start_66
    invoke-static {v1}, Lcom/dropbox/android/util/i;->g(Ljava/io/File;)Z
    :try_end_69
    .catchall {:try_start_66 .. :try_end_69} :catchall_8c
    .catch Ljava/io/IOException; {:try_start_66 .. :try_end_69} :catch_91

    .line 1464
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 1465
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    goto :goto_41

    .line 1460
    :catch_70
    move-exception v2

    move-object v2, v0

    move-object v3, v0

    .line 1462
    :goto_73
    :try_start_73
    invoke-static {v1}, Lcom/dropbox/android/util/i;->g(Ljava/io/File;)Z
    :try_end_76
    .catchall {:try_start_73 .. :try_end_76} :catchall_8c

    .line 1464
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 1465
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    goto :goto_41

    .line 1464
    :catchall_7d
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    :goto_81
    invoke-static {v3}, Ldbxyzptlk/B/d;->a(Ljava/io/InputStream;)V

    .line 1465
    invoke-static {v2}, Ldbxyzptlk/B/d;->a(Ljava/io/OutputStream;)V

    throw v0

    .line 1464
    :catchall_88
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_81

    :catchall_8c
    move-exception v0

    goto :goto_81

    .line 1460
    :catch_8e
    move-exception v2

    move-object v2, v0

    goto :goto_73

    :catch_91
    move-exception v4

    goto :goto_73
.end method

.method public static i()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 121
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "userid.change"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static i(Ljava/io/File;)Ljava/io/InputStream;
    .registers 3
    .parameter

    .prologue
    .line 1472
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gzdbup-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1473
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v0}, Lcom/dropbox/android/util/P;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v0

    .line 1475
    :goto_1a
    return-object v0

    :cond_1b
    invoke-static {p0}, Lcom/dropbox/android/util/P;->b(Ljava/io/File;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_1a
.end method

.method public static j()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 125
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "pref.changed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static k()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 129
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "cameraupload.tour.pref.changed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static l()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 133
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "cameraupload.tour.pref.final.value"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static m()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 137
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "block.scan"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static n()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 141
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "textedit.open"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static o()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 145
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "textedit.save"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static p()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 149
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "user.clear.cache"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static q()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 164
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "user.unlink"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static r()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 171
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "user.unlink.done"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static s()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 175
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "unlink.broadcast"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static t()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 179
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "battery.level"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static u()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 183
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "camera.upload.command"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static v()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 187
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "camera.upload.fullscan.event"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static w()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 191
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "video.start"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static x()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 195
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "video.prepared"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static y()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 199
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "video.completed"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static z()Lcom/dropbox/android/util/s;
    .registers 2

    .prologue
    .line 203
    new-instance v0, Lcom/dropbox/android/util/s;

    const-string v1, "video.error"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
