.class final Lcom/dropbox/android/util/t;
.super Lcom/dropbox/android/util/s;
.source "panda.py"


# direct methods
.method public constructor <init>(Ldbxyzptlk/j/g;)V
    .registers 6
    .parameter

    .prologue
    .line 1016
    const-string v0, "open.log"

    invoke-direct {p0, v0}, Lcom/dropbox/android/util/s;-><init>(Ljava/lang/String;)V

    .line 1017
    const-string v0, "APP_VERSION"

    iget-object v1, p1, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 1018
    const-string v0, "USER_ID"

    iget-object v1, p1, Ldbxyzptlk/j/g;->h:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 1019
    const-string v0, "DEVICE_ID"

    iget-object v1, p1, Ldbxyzptlk/j/g;->f:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 1020
    const-string v0, "PHONE_MODEL"

    iget-object v1, p1, Ldbxyzptlk/j/g;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 1021
    const-string v0, "ANDROID_VERSION"

    iget-object v1, p1, Ldbxyzptlk/j/g;->e:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 1022
    const-string v0, "MANUFACTURER"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 1024
    invoke-static {}, Ldbxyzptlk/l/a;->a()Ldbxyzptlk/l/a;

    move-result-object v2

    .line 1025
    const/4 v1, 0x0

    .line 1026
    const/4 v0, -0x1

    .line 1027
    invoke-virtual {v2}, Ldbxyzptlk/l/a;->k()Landroid/accounts/Account;

    move-result-object v3

    if-eqz v3, :cond_43

    .line 1028
    invoke-virtual {v2}, Ldbxyzptlk/l/a;->b()Ljava/lang/String;

    move-result-object v1

    .line 1029
    invoke-virtual {v2}, Ldbxyzptlk/l/a;->c()I

    move-result v0

    .line 1031
    :cond_43
    const-string v2, "LOG_SERIES_UUID"

    invoke-virtual {p0, v2, v1}, Lcom/dropbox/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 1032
    const-string v1, "LOG_SEQUENCE_NUMBER"

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/dropbox/android/util/t;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 1034
    const-string v0, "LOCALE"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/dropbox/android/util/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 1038
    return-void
.end method
