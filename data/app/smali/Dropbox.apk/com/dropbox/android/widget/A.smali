.class final Lcom/dropbox/android/widget/A;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# instance fields
.field final synthetic a:Lcom/dropbox/android/widget/DbxVideoView;


# direct methods
.method constructor <init>(Lcom/dropbox/android/widget/DbxVideoView;)V
    .registers 2
    .parameter

    .prologue
    .line 330
    iput-object p1, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, -0x1

    .line 333
    invoke-static {}, Lcom/dropbox/android/widget/DbxVideoView;->o()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaPlayer says the size is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v1, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1, p1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;Landroid/media/MediaPlayer;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;I)I

    .line 335
    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    iget-object v1, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1, p1}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;Landroid/media/MediaPlayer;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;I)I

    .line 337
    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    if-eqz v0, :cond_60

    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    if-eqz v0, :cond_60

    .line 338
    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v2}, Lcom/dropbox/android/widget/DbxVideoView;->b(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 342
    :cond_60
    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->c(Lcom/dropbox/android/widget/DbxVideoView;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 343
    :try_start_67
    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->d(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    if-eq v0, v3, :cond_dc

    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->e(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    if-eq v0, v3, :cond_dc

    .line 344
    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->d(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    if-ne v0, p2, :cond_87

    iget-object v0, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxVideoView;->e(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v0

    if-eq v0, p3, :cond_dc

    .line 345
    :cond_87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 346
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v3}, Lcom/dropbox/android/widget/DbxVideoView;->d(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/A;->a:Lcom/dropbox/android/widget/DbxVideoView;

    invoke-static {v3}, Lcom/dropbox/android/widget/DbxVideoView;->e(Lcom/dropbox/android/widget/DbxVideoView;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 347
    invoke-static {}, Lcom/dropbox/android/util/i;->B()Lcom/dropbox/android/util/s;

    move-result-object v3

    const-string v4, "device"

    invoke-virtual {v3, v4, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v3, "server"

    invoke-virtual {v0, v3, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 350
    :cond_dc
    monitor-exit v1

    .line 351
    return-void

    .line 350
    :catchall_de
    move-exception v0

    monitor-exit v1
    :try_end_e0
    .catchall {:try_start_67 .. :try_end_e0} :catchall_de

    throw v0
.end method
