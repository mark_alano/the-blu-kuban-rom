.class public Lcom/dropbox/android/widget/CameraUploadItemView;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# instance fields
.field protected a:Z

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ProgressBar;

.field private d:Landroid/widget/FrameLayout;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Ldbxyzptlk/k/f;

.field private k:I

.field private final l:Lcom/dropbox/android/widget/l;

.field private m:Landroid/os/Handler;

.field private n:Lcom/dropbox/android/widget/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->k:I

    .line 47
    new-instance v0, Lcom/dropbox/android/widget/l;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/l;-><init>(Lcom/dropbox/android/widget/CameraUploadItemView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->l:Lcom/dropbox/android/widget/l;

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->m:Landroid/os/Handler;

    .line 49
    new-instance v0, Lcom/dropbox/android/widget/j;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/j;-><init>(Lcom/dropbox/android/widget/CameraUploadItemView;Lcom/dropbox/android/widget/i;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->n:Lcom/dropbox/android/widget/j;

    .line 54
    const v0, 0x7f03003b

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 55
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/CameraUploadItemView;->addView(Landroid/view/View;)V

    .line 57
    const v0, 0x7f06009e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    .line 58
    const v0, 0x7f06009f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    .line 59
    const v0, 0x7f0600a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    .line 60
    const v0, 0x7f060042

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->g:Landroid/widget/ImageView;

    .line 61
    const v0, 0x7f06009a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->h:Landroid/widget/TextView;

    .line 62
    const v0, 0x7f0600a3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->i:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f06009d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f06009b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->c:Landroid/widget/ProgressBar;

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/CameraUploadItemView;)Ldbxyzptlk/k/f;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/k/f;

    return-object v0
.end method

.method private a()V
    .registers 4

    .prologue
    const/16 v2, 0x8

    .line 68
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/k/f;

    if-eqz v0, :cond_10

    .line 69
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/k/f;

    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->l:Lcom/dropbox/android/widget/l;

    invoke-virtual {v0, v1}, Ldbxyzptlk/k/f;->unregisterObserver(Ljava/lang/Object;)V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/k/f;

    .line 73
    :cond_10
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 74
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    .line 75
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 76
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    const v1, 0x7f020182

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 78
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 80
    return-void
.end method

.method private a(Lcom/dropbox/android/widget/h;)V
    .registers 12
    .parameter

    .prologue
    const/4 v5, 0x3

    const/4 v0, 0x0

    const/16 v4, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 95
    invoke-static {}, Lcom/dropbox/android/filemanager/I;->a()Lcom/dropbox/android/filemanager/I;

    move-result-object v1

    .line 96
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->d()Ljava/lang/String;

    move-result-object v3

    .line 97
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/dropbox/android/util/ae;->i(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->a:Z

    .line 98
    if-eqz v3, :cond_bd

    .line 100
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->b()Z

    move-result v2

    if-eqz v2, :cond_a6

    .line 103
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->a()Ljava/lang/String;

    move-result-object v2

    .line 104
    iget v4, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->k:I

    invoke-virtual {v1, v2, v4, v5, v0}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/T;

    move-result-object v2

    .line 106
    if-eqz v2, :cond_2e

    iget-object v0, v2, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    .line 107
    :cond_2e
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 108
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    move v2, v8

    .line 119
    :goto_3e
    iget v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->k:I

    iget-object v4, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->n:Lcom/dropbox/android/widget/j;

    invoke-virtual {v1, v3, v0, v5, v4}, Lcom/dropbox/android/filemanager/I;->a(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/T;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_64

    .line 122
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, v0, Lcom/dropbox/android/filemanager/T;->a:Landroid/graphics/Bitmap;

    invoke-direct {v1, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 123
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-boolean v3, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->a:Z

    iget-object v4, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->g:Landroid/widget/ImageView;

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/widget/al;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 138
    :cond_64
    :goto_64
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->h:Landroid/widget/TextView;

    const v1, 0x7f0b0118

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 140
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->g()Lcom/dropbox/android/taskqueue/Q;

    move-result-object v1

    .line 143
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->h()Z

    move-result v0

    if-nez v0, :cond_93

    .line 144
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->f()I

    move-result v0

    .line 145
    if-lez v0, :cond_93

    .line 146
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 148
    if-ne v0, v8, :cond_e1

    .line 149
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0128

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 154
    :goto_8e
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->i:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    :cond_93
    sget-object v0, Lcom/dropbox/android/widget/i;->a:[I

    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/Q;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_16c

    .line 197
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected camera upload tracker state"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_a6
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->c()Z

    move-result v2

    if-eqz v2, :cond_169

    .line 113
    iget-object v2, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 114
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    move v2, v8

    .line 116
    goto :goto_3e

    .line 128
    :cond_bd
    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 129
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    const v1, 0x7f0200a6

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 134
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_64

    .line 151
    :cond_e1
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0129

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v9

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_8e

    .line 162
    :pswitch_f9
    const v0, 0x7f0b0051

    .line 199
    :goto_fc
    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 200
    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 201
    :goto_106
    return-void

    .line 165
    :pswitch_107
    const v0, 0x7f0b0120

    .line 166
    goto :goto_fc

    .line 168
    :pswitch_10b
    const v0, 0x7f0b011f

    .line 169
    goto :goto_fc

    .line 171
    :pswitch_10f
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/z;->a(Landroid/content/Context;)Lcom/dropbox/android/util/D;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/D;->b()Z

    move-result v0

    if-eqz v0, :cond_121

    .line 172
    const v0, 0x7f0b0124

    goto :goto_fc

    .line 174
    :cond_121
    const v0, 0x7f0b0123

    .line 176
    goto :goto_fc

    .line 179
    :pswitch_125
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->i()Ldbxyzptlk/k/i;

    move-result-object v0

    .line 180
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/x;->b()Ldbxyzptlk/k/g;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldbxyzptlk/k/g;->a(Ldbxyzptlk/k/i;)Ldbxyzptlk/k/f;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/k/f;

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/k/f;

    if-eqz v0, :cond_142

    .line 182
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/k/f;

    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->l:Lcom/dropbox/android/widget/l;

    invoke-virtual {v0, v1}, Ldbxyzptlk/k/f;->registerObserver(Landroid/database/ContentObserver;)V

    .line 184
    :cond_142
    invoke-virtual {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->j:Ldbxyzptlk/k/f;

    iget-object v3, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->c:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    move v2, v8

    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/widget/al;->a(Landroid/content/Context;Ldbxyzptlk/k/f;ZLandroid/widget/ProgressBar;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    goto :goto_106

    .line 188
    :pswitch_153
    const v0, 0x7f0b004e

    .line 189
    goto :goto_fc

    .line 191
    :pswitch_157
    invoke-interface {p1}, Lcom/dropbox/android/widget/h;->h()Z

    move-result v0

    if-nez v0, :cond_165

    .line 192
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "we should be initial scan, as non-initial state is supressed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_165
    const v0, 0x7f0b0122

    .line 195
    goto :goto_fc

    :cond_169
    move v2, v9

    goto/16 :goto_3e

    .line 160
    :pswitch_data_16c
    .packed-switch 0x1
        :pswitch_f9
        :pswitch_107
        :pswitch_10b
        :pswitch_10f
        :pswitch_125
        :pswitch_125
        :pswitch_153
        :pswitch_157
    .end packed-switch
.end method

.method static synthetic b(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ProgressBar;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->c:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/CameraUploadItemView;)I
    .registers 2
    .parameter

    .prologue
    .line 31
    iget v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->k:I

    return v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->d:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->f:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->g:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/widget/CameraUploadItemView;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->m:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Lcom/dropbox/android/widget/h;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->a:Z

    .line 85
    invoke-direct {p0}, Lcom/dropbox/android/widget/CameraUploadItemView;->a()V

    .line 88
    iget v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/CameraUploadItemView;->k:I

    .line 90
    invoke-direct {p0, p2}, Lcom/dropbox/android/widget/CameraUploadItemView;->a(Lcom/dropbox/android/widget/h;)V

    .line 91
    return-void
.end method
