.class public Lcom/dropbox/android/widget/DbxMediaController;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private A:Lcom/dropbox/android/widget/w;

.field private final B:Lcom/dropbox/android/widget/u;

.field private C:Lcom/dropbox/android/widget/y;

.field private final D:Ljava/lang/String;

.field private E:I

.field private F:Landroid/view/View$OnLayoutChangeListener;

.field private G:Landroid/view/View$OnTouchListener;

.field private H:Landroid/os/Handler;

.field private I:Landroid/view/View$OnClickListener;

.field private J:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private K:Landroid/view/View$OnClickListener;

.field private L:Landroid/view/View$OnClickListener;

.field private M:Lcom/dropbox/android/widget/x;

.field a:Ljava/lang/StringBuilder;

.field b:Ljava/util/Formatter;

.field private d:Lcom/dropbox/android/widget/v;

.field private e:Landroid/content/Context;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/view/WindowManager;

.field private i:Landroid/view/Window;

.field private j:Landroid/view/View;

.field private k:Landroid/view/WindowManager$LayoutParams;

.field private l:Landroid/widget/ProgressBar;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Landroid/view/View$OnClickListener;

.field private u:Landroid/view/View$OnClickListener;

.field private v:Landroid/widget/ImageButton;

.field private w:Landroid/widget/ImageButton;

.field private x:Landroid/widget/ImageButton;

.field private y:Landroid/widget/ImageButton;

.field private z:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 88
    const-class v0, Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/DbxMediaController;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/dropbox/android/widget/u;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 175
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 123
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->C:Lcom/dropbox/android/widget/y;

    .line 134
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    .line 254
    new-instance v0, Lcom/dropbox/android/widget/m;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/m;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->F:Landroid/view/View$OnLayoutChangeListener;

    .line 267
    new-instance v0, Lcom/dropbox/android/widget/n;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/n;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->G:Landroid/view/View$OnTouchListener;

    .line 476
    new-instance v0, Lcom/dropbox/android/widget/p;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/p;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    .line 648
    new-instance v0, Lcom/dropbox/android/widget/q;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/q;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/view/View$OnClickListener;

    .line 714
    new-instance v0, Lcom/dropbox/android/widget/r;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/r;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->J:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 809
    new-instance v0, Lcom/dropbox/android/widget/s;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/s;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->K:Landroid/view/View$OnClickListener;

    .line 821
    new-instance v0, Lcom/dropbox/android/widget/t;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/t;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->L:Landroid/view/View$OnClickListener;

    .line 176
    iput-object p3, p0, Lcom/dropbox/android/widget/DbxMediaController;->B:Lcom/dropbox/android/widget/u;

    .line 177
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    .line 178
    iput-boolean p2, p0, Lcom/dropbox/android/widget/DbxMediaController;->q:Z

    .line 179
    iput-object p4, p0, Lcom/dropbox/android/widget/DbxMediaController;->D:Ljava/lang/String;

    .line 180
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->D:Ljava/lang/String;

    if-eqz v0, :cond_4c

    .line 181
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 183
    :cond_4c
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->i()V

    .line 184
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->h()V

    .line 185
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/view/Window;
    .registers 6
    .parameter

    .prologue
    .line 190
    :try_start_0
    const-string v0, "com.android.internal.policy.PolicyManager"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 191
    const-string v1, "makeNewWindow"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 192
    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Window;
    :try_end_21
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_21} :catch_22
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_21} :catch_29
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_21} :catch_30
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_21} :catch_37

    return-object v0

    .line 193
    :catch_22
    move-exception v0

    .line 194
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 195
    :catch_29
    move-exception v0

    .line 196
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 197
    :catch_30
    move-exception v0

    .line 198
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 199
    :catch_37
    move-exception v0

    .line 200
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Landroid/view/View;)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    const/16 v2, 0x8

    .line 324
    const v0, 0x7f0600c2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    .line 325
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1e

    .line 326
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 327
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->I:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    :cond_1e
    const v0, 0x7f0600c6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    .line 331
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    if-eqz v0, :cond_42

    .line 332
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->L:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_42

    .line 334
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->q:Z

    if-eqz v0, :cond_ff

    move v0, v1

    :goto_3f
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 338
    :cond_42
    const v0, 0x7f0600c5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    .line 339
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    if-eqz v0, :cond_65

    .line 340
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->K:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_65

    .line 342
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->q:Z

    if-eqz v3, :cond_102

    :goto_62
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 347
    :cond_65
    const v0, 0x7f0600c7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    .line 348
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz v0, :cond_81

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_81

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->s:Z

    if-nez v0, :cond_81

    .line 349
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 351
    :cond_81
    const v0, 0x7f0600c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    .line 352
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz v0, :cond_9d

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_9d

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->s:Z

    if-nez v0, :cond_9d

    .line 353
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 356
    :cond_9d
    const v0, 0x7f0600c9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    .line 357
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_c2

    .line 358
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    instance-of v0, v0, Landroid/widget/SeekBar;

    if-eqz v0, :cond_bb

    .line 359
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    check-cast v0, Landroid/widget/SeekBar;

    .line 360
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->J:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 362
    :cond_bb
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 365
    :cond_c2
    const v0, 0x7f0600c3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/dropbox/android/widget/o;

    invoke-direct {v1, p0}, Lcom/dropbox/android/widget/o;-><init>(Lcom/dropbox/android/widget/DbxMediaController;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 374
    const v0, 0x7f0600ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->m:Landroid/widget/TextView;

    .line 375
    const v0, 0x7f0600c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->n:Landroid/widget/TextView;

    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->a:Ljava/lang/StringBuilder;

    .line 377
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->a:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->b:Ljava/util/Formatter;

    .line 379
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->p()V

    .line 380
    return-void

    :cond_ff
    move v0, v2

    .line 334
    goto/16 :goto_3f

    :cond_102
    move v1, v2

    .line 342
    goto/16 :goto_62
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxMediaController;)V
    .registers 1
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->j()V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxMediaController;I)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxMediaController;->c(I)V

    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxMediaController;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->p:Z

    return p1
.end method

.method private b(I)Ljava/lang/String;
    .registers 11
    .parameter

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 513
    div-int/lit16 v0, p1, 0x3e8

    .line 515
    rem-int/lit8 v1, v0, 0x3c

    .line 516
    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v2, v2, 0x3c

    .line 517
    div-int/lit16 v0, v0, 0xe10

    .line 519
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 520
    if-lez v0, :cond_36

    .line 521
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->b:Ljava/util/Formatter;

    const-string v4, "%d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-virtual {v3, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 523
    :goto_35
    return-object v0

    :cond_36
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->b:Ljava/util/Formatter;

    const-string v3, "%02d:%02d"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v7

    invoke-virtual {v0, v3, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_35
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxMediaController;I)Ljava/lang/String;
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxMediaController;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxMediaController;)Z
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    return v0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/view/View;
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    return-object v0
.end method

.method private c(I)V
    .registers 4
    .parameter

    .prologue
    .line 687
    mul-int/lit8 v0, p1, 0xa

    .line 688
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->o()Z

    move-result v1

    if-eqz v1, :cond_e

    iget v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    if-le v0, v1, :cond_e

    .line 689
    iput v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    .line 691
    :cond_e
    return-void
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/view/WindowManager$LayoutParams;
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/view/WindowManager;
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/DbxMediaController;)I
    .registers 2
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->l()I

    move-result v0

    return v0
.end method

.method static synthetic g()Ljava/lang/String;
    .registers 1

    .prologue
    .line 86
    sget-object v0, Lcom/dropbox/android/widget/DbxMediaController;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/DbxMediaController;)Z
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->p:Z

    return v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/DbxMediaController;)Lcom/dropbox/android/widget/v;
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    return-object v0
.end method

.method private h()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 205
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    .line 206
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(Landroid/content/Context;)Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    invoke-virtual {v0, v1, v3, v3}, Landroid/view/Window;->setWindowManager(Landroid/view/WindowManager;Landroid/os/IBinder;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    invoke-virtual {v0, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->G:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 211
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    invoke-virtual {v0, p0}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    .line 212
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    const v1, 0x106000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 216
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->i:Landroid/view/Window;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setVolumeControlStream(I)V

    .line 218
    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/DbxMediaController;->setFocusable(Z)V

    .line 219
    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/DbxMediaController;->setFocusableInTouchMode(Z)V

    .line 220
    const/high16 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->setDescendantFocusability(I)V

    .line 221
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->requestFocus()Z

    .line 222
    return-void
.end method

.method private i()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 228
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    .line 229
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    .line 230
    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 231
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 232
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 233
    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 234
    const/16 v1, 0x3e8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 235
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v2, 0x820020

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 238
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 239
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 240
    return-void
.end method

.method static synthetic i(Lcom/dropbox/android/widget/DbxMediaController;)Z
    .registers 2
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->o()Z

    move-result v0

    return v0
.end method

.method private j()V
    .registers 4

    .prologue
    .line 245
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 246
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 248
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    .line 249
    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 250
    const/4 v2, 0x1

    aget v0, v0, v2

    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 251
    return-void
.end method

.method static synthetic j(Lcom/dropbox/android/widget/DbxMediaController;)V
    .registers 1
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->n()V

    return-void
.end method

.method static synthetic k(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    return-object v0
.end method

.method private k()V
    .registers 3

    .prologue
    .line 396
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->g()Z

    move-result v0

    if-nez v0, :cond_12

    .line 397
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 399
    :cond_12
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    if-eqz v0, :cond_24

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->h()Z

    move-result v0

    if-nez v0, :cond_24

    .line 400
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 402
    :cond_24
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->i()Z

    move-result v0

    if-nez v0, :cond_36

    .line 403
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 407
    :cond_36
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->i()Z

    move-result v0

    if-nez v0, :cond_4c

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->h()Z

    move-result v0

    if-nez v0, :cond_4c

    .line 408
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setEnabled(Z)V
    :try_end_4c
    .catch Ljava/lang/IncompatibleClassChangeError; {:try_start_0 .. :try_end_4c} :catch_4d

    .line 416
    :cond_4c
    :goto_4c
    return-void

    .line 410
    :catch_4d
    move-exception v0

    goto :goto_4c
.end method

.method private l()I
    .registers 7

    .prologue
    .line 535
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->p:Z

    if-eqz v0, :cond_a

    .line 536
    :cond_8
    const/4 v0, 0x0

    .line 566
    :cond_9
    :goto_9
    return v0

    .line 538
    :cond_a
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->d()I

    move-result v0

    .line 539
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v1}, Lcom/dropbox/android/widget/v;->c()I

    move-result v1

    .line 541
    int-to-long v2, v1

    const-wide/16 v4, 0x7530

    cmp-long v2, v2, v4

    if-gtz v2, :cond_22

    .line 542
    const/16 v2, 0x64

    invoke-direct {p0, v2}, Lcom/dropbox/android/widget/DbxMediaController;->c(I)V

    .line 545
    :cond_22
    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_51

    .line 546
    if-lez v1, :cond_44

    .line 548
    const-wide/16 v2, 0x3e8

    int-to-long v4, v0

    mul-long/2addr v2, v4

    int-to-long v4, v1

    div-long/2addr v2, v4

    .line 549
    iget-object v4, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    long-to-int v5, v2

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 550
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->o()Z

    move-result v4

    if-eqz v4, :cond_44

    iget v4, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    int-to-long v4, v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_44

    .line 551
    long-to-int v2, v2

    iput v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    .line 554
    :cond_44
    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v2}, Lcom/dropbox/android/widget/v;->f()I

    move-result v2

    .line 558
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    mul-int/lit8 v2, v2, 0xa

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 561
    :cond_51
    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->m:Landroid/widget/TextView;

    if-eqz v2, :cond_5e

    .line 562
    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->m:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 563
    :cond_5e
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->n:Landroid/widget/TextView;

    if-eqz v1, :cond_9

    .line 564
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->n:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_9
.end method

.method static synthetic l(Lcom/dropbox/android/widget/DbxMediaController;)I
    .registers 2
    .parameter

    .prologue
    .line 86
    iget v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    return v0
.end method

.method static synthetic m(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    return-object v0
.end method

.method private m()V
    .registers 3

    .prologue
    .line 657
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-nez v0, :cond_9

    .line 665
    :cond_8
    :goto_8
    return-void

    .line 660
    :cond_9
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->e()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 661
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    const v1, 0x1080023

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_8

    .line 663
    :cond_1a
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    const v1, 0x1080024

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_8
.end method

.method static synthetic n(Lcom/dropbox/android/widget/DbxMediaController;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->n:Landroid/widget/TextView;

    return-object v0
.end method

.method private n()V
    .registers 3

    .prologue
    .line 668
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->e()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 669
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 670
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->b()V

    .line 671
    sget-object v0, Lcom/dropbox/android/widget/DbxMediaController;->c:Ljava/lang/String;

    const-string v1, "PAUSED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->M:Lcom/dropbox/android/widget/x;

    if-eqz v0, :cond_23

    .line 673
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->M:Lcom/dropbox/android/widget/x;

    invoke-interface {v0}, Lcom/dropbox/android/widget/x;->b()V

    .line 682
    :cond_23
    :goto_23
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 683
    return-void

    .line 676
    :cond_27
    sget-object v0, Lcom/dropbox/android/widget/DbxMediaController;->c:Ljava/lang/String;

    const-string v1, "RESUMED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v0}, Lcom/dropbox/android/widget/v;->a()V

    .line 678
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->M:Lcom/dropbox/android/widget/x;

    if-eqz v0, :cond_23

    .line 679
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->M:Lcom/dropbox/android/widget/x;

    invoke-interface {v0}, Lcom/dropbox/android/widget/x;->a()V

    goto :goto_23
.end method

.method static synthetic o(Lcom/dropbox/android/widget/DbxMediaController;)Lcom/dropbox/android/widget/y;
    .registers 2
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->C:Lcom/dropbox/android/widget/y;

    return-object v0
.end method

.method private o()Z
    .registers 3

    .prologue
    .line 698
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->D:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->E:I

    const/16 v1, 0x3e8

    if-ge v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method private p()V
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 836
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz v0, :cond_17

    .line 837
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 838
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->t:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2c

    move v0, v1

    :goto_14
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 841
    :cond_17
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2b

    .line 842
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 843
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->u:Landroid/view/View$OnClickListener;

    if-eqz v3, :cond_2e

    :goto_28
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 845
    :cond_2b
    return-void

    :cond_2c
    move v0, v2

    .line 838
    goto :goto_14

    :cond_2e
    move v1, v2

    .line 843
    goto :goto_28
.end method

.method static synthetic p(Lcom/dropbox/android/widget/DbxMediaController;)V
    .registers 1
    .parameter

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    return-void
.end method


# virtual methods
.method protected final a()Landroid/view/View;
    .registers 4

    .prologue
    .line 315
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->e:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 316
    const v1, 0x7f030047

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    .line 318
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(Landroid/view/View;)V

    .line 320
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    return-object v0
.end method

.method public final a(I)V
    .registers 6
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 425
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->f()V

    .line 427
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    if-nez v0, :cond_29

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    if-eqz v0, :cond_29

    .line 428
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->l()I

    .line 429
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_18

    .line 430
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 432
    :cond_18
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->k()V

    .line 433
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->j()V

    .line 434
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    iget-object v2, p0, Lcom/dropbox/android/widget/DbxMediaController;->k:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 435
    iput-boolean v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    .line 437
    :cond_29
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 442
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 444
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 445
    if-eqz p1, :cond_4d

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v1}, Lcom/dropbox/android/widget/v;->e()Z

    move-result v1

    if-eqz v1, :cond_4d

    .line 446
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 447
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 449
    :cond_4d
    return-void
.end method

.method public final b()V
    .registers 2

    .prologue
    .line 387
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 388
    return-void
.end method

.method public final c()Z
    .registers 2

    .prologue
    .line 452
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    return v0
.end method

.method public final d()V
    .registers 3

    .prologue
    .line 459
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    if-nez v0, :cond_5

    .line 474
    :cond_4
    :goto_4
    return-void

    .line 462
    :cond_5
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    if-eqz v0, :cond_4

    .line 464
    :try_start_9
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 465
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->h:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->j:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_16
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_16} :catch_23

    .line 469
    :goto_16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->o:Z

    .line 470
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->A:Lcom/dropbox/android/widget/w;

    if-eqz v0, :cond_4

    .line 471
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->A:Lcom/dropbox/android/widget/w;

    invoke-interface {v0}, Lcom/dropbox/android/widget/w;->a()V

    goto :goto_4

    .line 466
    :catch_23
    move-exception v0

    .line 467
    const-string v0, "MediaController"

    const-string v1, "already removed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_16
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .registers 7
    .parameter

    .prologue
    const/16 v4, 0xbb8

    const/4 v0, 0x1

    .line 583
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    .line 585
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_32

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_32

    move v1, v0

    .line 587
    :goto_14
    const/16 v3, 0x4f

    if-eq v2, v3, :cond_20

    const/16 v3, 0x55

    if-eq v2, v3, :cond_20

    const/16 v3, 0x3e

    if-ne v2, v3, :cond_34

    .line 590
    :cond_20
    if-eqz v1, :cond_31

    .line 591
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->n()V

    .line 592
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 593
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v1, :cond_31

    .line 594
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 645
    :cond_31
    :goto_31
    return v0

    .line 585
    :cond_32
    const/4 v1, 0x0

    goto :goto_14

    .line 598
    :cond_34
    const/16 v3, 0x7e

    if-ne v2, v3, :cond_4e

    .line 599
    if-eqz v1, :cond_31

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v1}, Lcom/dropbox/android/widget/v;->e()Z

    move-result v1

    if-nez v1, :cond_31

    .line 600
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v1}, Lcom/dropbox/android/widget/v;->a()V

    .line 601
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 602
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    goto :goto_31

    .line 605
    :cond_4e
    const/16 v3, 0x56

    if-eq v2, v3, :cond_56

    const/16 v3, 0x7f

    if-ne v2, v3, :cond_6c

    .line 607
    :cond_56
    if-eqz v1, :cond_31

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v1}, Lcom/dropbox/android/widget/v;->e()Z

    move-result v1

    if-eqz v1, :cond_31

    .line 608
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    invoke-interface {v1}, Lcom/dropbox/android/widget/v;->b()V

    .line 609
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 610
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    goto :goto_31

    .line 613
    :cond_6c
    const/16 v3, 0x19

    if-eq v2, v3, :cond_78

    const/16 v3, 0x18

    if-eq v2, v3, :cond_78

    const/16 v3, 0xa4

    if-ne v2, v3, :cond_7d

    .line 617
    :cond_78
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_31

    .line 618
    :cond_7d
    const/16 v3, 0x52

    if-ne v2, v3, :cond_87

    .line 619
    if-eqz v1, :cond_31

    .line 620
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    goto :goto_31

    .line 625
    :cond_87
    const/4 v1, 0x4

    if-ne v2, v1, :cond_bc

    .line 626
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_9a

    .line 627
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 628
    if-eqz v1, :cond_31

    .line 629
    invoke-virtual {v1, p1, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    goto :goto_31

    .line 632
    :cond_9a
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_bc

    .line 633
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    .line 634
    if-eqz v1, :cond_a9

    .line 635
    invoke-virtual {v1, p1}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    .line 637
    :cond_a9
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_bc

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_bc

    .line 638
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->B:Lcom/dropbox/android/widget/u;

    invoke-interface {v1}, Lcom/dropbox/android/widget/u;->onBackPressed()V

    goto/16 :goto_31

    .line 644
    :cond_bc
    invoke-virtual {p0, v4}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 645
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_31
.end method

.method public final e()V
    .registers 3

    .prologue
    .line 508
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->H:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 509
    return-void
.end method

.method public final f()V
    .registers 2

    .prologue
    .line 906
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->o()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 907
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->D:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/dropbox/android/widget/z;->a(Lcom/dropbox/android/widget/DbxMediaController;Ljava/lang/String;)V

    .line 909
    :cond_b
    return-void
.end method

.method public onFinishInflate()V
    .registers 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 171
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(Landroid/view/View;)V

    .line 172
    :cond_9
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 571
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 572
    const/4 v0, 0x1

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 577
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxMediaController;->a(I)V

    .line 578
    const/4 v0, 0x0

    return v0
.end method

.method public setAnchorView(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 290
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    if-eqz v0, :cond_c

    .line 291
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->F:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 293
    :cond_c
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    .line 294
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    if-eqz v0, :cond_19

    .line 295
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxMediaController;->F:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 298
    :cond_19
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 303
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->removeAllViews()V

    .line 304
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxMediaController;->a()Landroid/view/View;

    move-result-object v1

    .line 305
    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/widget/DbxMediaController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 306
    return-void
.end method

.method public setEnabled(Z)V
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 787
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_b

    .line 788
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 790
    :cond_b
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    if-eqz v0, :cond_14

    .line 791
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->w:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 793
    :cond_14
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1d

    .line 794
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->x:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 796
    :cond_1d
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2d

    .line 797
    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz p1, :cond_4c

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->t:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_4c

    move v0, v1

    :goto_2a
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 799
    :cond_2d
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3c

    .line 800
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz p1, :cond_4e

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxMediaController;->u:Landroid/view/View$OnClickListener;

    if-eqz v3, :cond_4e

    :goto_39
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 802
    :cond_3c
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_45

    .line 803
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setEnabled(Z)V

    .line 805
    :cond_45
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->k()V

    .line 806
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 807
    return-void

    :cond_4c
    move v0, v2

    .line 797
    goto :goto_2a

    :cond_4e
    move v1, v2

    .line 800
    goto :goto_39
.end method

.method public setMediaPlayer(Lcom/dropbox/android/widget/v;)V
    .registers 2
    .parameter

    .prologue
    .line 280
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->d:Lcom/dropbox/android/widget/v;

    .line 281
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->m()V

    .line 282
    return-void
.end method

.method public setOnHideListener(Lcom/dropbox/android/widget/w;)V
    .registers 2
    .parameter

    .prologue
    .line 865
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->A:Lcom/dropbox/android/widget/w;

    .line 866
    return-void
.end method

.method public setOnPlayPauseListener(Lcom/dropbox/android/widget/x;)V
    .registers 2
    .parameter

    .prologue
    .line 873
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->M:Lcom/dropbox/android/widget/x;

    .line 874
    return-void
.end method

.method public setOnUserSeekListener(Lcom/dropbox/android/widget/y;)V
    .registers 2
    .parameter

    .prologue
    .line 869
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->C:Lcom/dropbox/android/widget/y;

    .line 870
    return-void
.end method

.method public setPrevNextListeners(Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 848
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxMediaController;->t:Landroid/view/View$OnClickListener;

    .line 849
    iput-object p2, p0, Lcom/dropbox/android/widget/DbxMediaController;->u:Landroid/view/View$OnClickListener;

    .line 850
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->s:Z

    .line 852
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->g:Landroid/view/View;

    if-eqz v0, :cond_29

    .line 853
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxMediaController;->p()V

    .line 855
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1c

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_1c

    .line 856
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->y:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 858
    :cond_1c
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    if-eqz v0, :cond_29

    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->r:Z

    if-nez v0, :cond_29

    .line 859
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxMediaController;->z:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 862
    :cond_29
    return-void
.end method
