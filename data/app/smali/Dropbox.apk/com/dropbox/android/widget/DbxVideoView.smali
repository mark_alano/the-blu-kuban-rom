.class public Lcom/dropbox/android/widget/DbxVideoView;
.super Landroid/view/SurfaceView;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/v;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:I

.field private C:I

.field private final D:Ljava/lang/Object;

.field private E:Landroid/media/MediaPlayer$OnCompletionListener;

.field private final F:Landroid/media/MediaPlayer$OnInfoListener;

.field private G:Landroid/media/MediaPlayer$OnErrorListener;

.field private H:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field a:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field b:Landroid/media/MediaPlayer$OnPreparedListener;

.field c:Landroid/view/SurfaceHolder$Callback;

.field private e:Landroid/net/Uri;

.field private f:Ljava/util/Map;

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/view/SurfaceHolder;

.field private k:Landroid/media/MediaPlayer;

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Lcom/dropbox/android/widget/DbxMediaController;

.field private q:Landroid/media/MediaPlayer$OnCompletionListener;

.field private r:Landroid/media/MediaPlayer$OnPreparedListener;

.field private s:Landroid/media/MediaPlayer$OnInfoListener;

.field private t:I

.field private u:Landroid/media/MediaPlayer$OnErrorListener;

.field private v:I

.field private w:Z

.field private x:Z

.field private y:Z

.field private volatile z:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 51
    const-class v0, Lcom/dropbox/android/widget/DbxVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 94
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 55
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 71
    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 72
    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 75
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    .line 76
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 84
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->s:Landroid/media/MediaPlayer$OnInfoListener;

    .line 179
    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->z:Z

    .line 180
    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->A:Z

    .line 285
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    .line 286
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    .line 287
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    .line 329
    new-instance v0, Lcom/dropbox/android/widget/A;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/A;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->a:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 354
    new-instance v0, Lcom/dropbox/android/widget/B;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/B;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->b:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 406
    new-instance v0, Lcom/dropbox/android/widget/C;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/C;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->E:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 424
    new-instance v0, Lcom/dropbox/android/widget/D;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/D;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->F:Landroid/media/MediaPlayer$OnInfoListener;

    .line 436
    new-instance v0, Lcom/dropbox/android/widget/E;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/E;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    .line 490
    new-instance v0, Lcom/dropbox/android/widget/G;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/G;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->H:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 538
    new-instance v0, Lcom/dropbox/android/widget/H;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/H;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->c:Landroid/view/SurfaceHolder$Callback;

    .line 95
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->p()V

    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/dropbox/android/widget/DbxVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 100
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->p()V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 104
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 71
    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 72
    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 75
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    .line 76
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 84
    iput-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->s:Landroid/media/MediaPlayer$OnInfoListener;

    .line 179
    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->z:Z

    .line 180
    iput-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->A:Z

    .line 285
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    .line 286
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    .line 287
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    .line 329
    new-instance v0, Lcom/dropbox/android/widget/A;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/A;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->a:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 354
    new-instance v0, Lcom/dropbox/android/widget/B;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/B;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->b:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 406
    new-instance v0, Lcom/dropbox/android/widget/C;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/C;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->E:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 424
    new-instance v0, Lcom/dropbox/android/widget/D;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/D;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->F:Landroid/media/MediaPlayer$OnInfoListener;

    .line 436
    new-instance v0, Lcom/dropbox/android/widget/E;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/E;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    .line 490
    new-instance v0, Lcom/dropbox/android/widget/G;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/G;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->H:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 538
    new-instance v0, Lcom/dropbox/android/widget/H;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/H;-><init>(Lcom/dropbox/android/widget/DbxVideoView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->c:Landroid/view/SurfaceHolder$Callback;

    .line 105
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->p()V

    .line 106
    return-void
.end method

.method private a(Landroid/media/MediaPlayer;)I
    .registers 5
    .parameter

    .prologue
    .line 307
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    monitor-enter v1

    .line 308
    :try_start_3
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_c

    .line 309
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    monitor-exit v1

    .line 312
    :goto_b
    return v0

    .line 311
    :cond_c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_12

    .line 312
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    goto :goto_b

    .line 311
    :catchall_12
    move-exception v0

    :try_start_13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_13 .. :try_end_14} :catchall_12

    throw v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    return p1
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;Landroid/media/MediaPlayer;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Landroid/media/MediaPlayer;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method private a(Z)V
    .registers 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 577
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_13

    .line 578
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 579
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 580
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 581
    if-eqz p1, :cond_13

    .line 582
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 585
    :cond_13
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DbxVideoView;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->w:Z

    return p1
.end method

.method private b(Landroid/media/MediaPlayer;)I
    .registers 5
    .parameter

    .prologue
    .line 321
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    monitor-enter v1

    .line 322
    :try_start_3
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_c

    .line 323
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    monitor-exit v1

    .line 326
    :goto_b
    return v0

    .line 325
    :cond_c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_3 .. :try_end_d} :catchall_12

    .line 326
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    goto :goto_b

    .line 325
    :catchall_12
    move-exception v0

    :try_start_13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_13 .. :try_end_14} :catchall_12

    throw v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxVideoView;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    return p1
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxVideoView;Landroid/media/MediaPlayer;)I
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->b(Landroid/media/MediaPlayer;)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DbxVideoView;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->x:Z

    return p1
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    return p1
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DbxVideoView;)Ljava/lang/Object;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DbxVideoView;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->y:Z

    return p1
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DbxVideoView;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    return v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    return p1
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DbxVideoView;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->z:Z

    return p1
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DbxVideoView;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    return v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->t:I

    return p1
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DbxVideoView;Z)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->a(Z)V

    return-void
.end method

.method static synthetic f(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->n:I

    return p1
.end method

.method static synthetic f(Lcom/dropbox/android/widget/DbxVideoView;)Z
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->A:Z

    return v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/DbxVideoView;I)I
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 50
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->o:I

    return p1
.end method

.method static synthetic g(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->r:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/widget/DbxVideoView;)Lcom/dropbox/android/widget/DbxMediaController;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/widget/DbxVideoView;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->v:I

    return v0
.end method

.method static synthetic k(Lcom/dropbox/android/widget/DbxVideoView;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->n:I

    return v0
.end method

.method static synthetic l(Lcom/dropbox/android/widget/DbxVideoView;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->o:I

    return v0
.end method

.method static synthetic m(Lcom/dropbox/android/widget/DbxVideoView;)I
    .registers 2
    .parameter

    .prologue
    .line 50
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    return v0
.end method

.method static synthetic n(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnCompletionListener;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->q:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic o(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnInfoListener;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->s:Landroid/media/MediaPlayer$OnInfoListener;

    return-object v0
.end method

.method static synthetic o()Ljava/lang/String;
    .registers 1

    .prologue
    .line 50
    sget-object v0, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic p(Lcom/dropbox/android/widget/DbxVideoView;)Landroid/media/MediaPlayer$OnErrorListener;
    .registers 2
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->u:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method private p()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 160
    iput v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    .line 161
    iput v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    .line 162
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->c:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 163
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 164
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/DbxVideoView;->setFocusable(Z)V

    .line 165
    invoke-virtual {p0, v3}, Lcom/dropbox/android/widget/DbxVideoView;->setFocusableInTouchMode(Z)V

    .line 166
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->requestFocus()Z

    .line 167
    iput v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 168
    iput v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 169
    return-void
.end method

.method private q()V
    .registers 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 217
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_c

    .line 265
    :cond_b
    :goto_b
    return-void

    .line 223
    :cond_c
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 224
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 229
    invoke-direct {p0, v5}, Lcom/dropbox/android/widget/DbxVideoView;->a(Z)V

    .line 231
    :try_start_24
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->b:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 233
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->F:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 234
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->a:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 237
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->E:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 238
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 239
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->H:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 240
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->t:I

    .line 243
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 244
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->j:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 245
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 246
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 247
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 250
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 251
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->r()V
    :try_end_81
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_81} :catch_82
    .catch Ljava/lang/IllegalArgumentException; {:try_start_24 .. :try_end_81} :catch_aa

    goto :goto_b

    .line 252
    :catch_82
    move-exception v0

    .line 253
    sget-object v1, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 254
    iput v4, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 255
    iput v4, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 256
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1, v6, v5}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_b

    .line 258
    :catch_aa
    move-exception v0

    .line 259
    sget-object v1, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 260
    iput v4, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 261
    iput v4, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 262
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->G:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-interface {v0, v1, v6, v5}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_b
.end method

.method static synthetic q(Lcom/dropbox/android/widget/DbxVideoView;)V
    .registers 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->q()V

    return-void
.end method

.method private r()V
    .registers 3

    .prologue
    .line 276
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_29

    .line 277
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0, p0}, Lcom/dropbox/android/widget/DbxMediaController;->setMediaPlayer(Lcom/dropbox/android/widget/v;)V

    .line 278
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_2a

    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 280
    :goto_1b
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/DbxMediaController;->setAnchorView(Landroid/view/View;)V

    .line 281
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->setEnabled(Z)V

    .line 283
    :cond_29
    return-void

    :cond_2a
    move-object v0, p0

    .line 278
    goto :goto_1b
.end method

.method private s()V
    .registers 2

    .prologue
    .line 646
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->c()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 647
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    .line 651
    :goto_d
    return-void

    .line 649
    :cond_e
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->b()V

    goto :goto_d
.end method


# virtual methods
.method public final a()V
    .registers 3

    .prologue
    const/4 v1, 0x3

    .line 655
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 656
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 657
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 659
    :cond_e
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 660
    return-void
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 725
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 726
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 727
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->v:I

    .line 731
    :goto_e
    return-void

    .line 729
    :cond_f
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->v:I

    goto :goto_e
.end method

.method public final a(II)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 290
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->D:Ljava/lang/Object;

    monitor-enter v1

    .line 291
    :try_start_3
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->B:I

    .line 292
    iput p2, p0, Lcom/dropbox/android/widget/DbxVideoView;->C:I

    .line 293
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_3a

    .line 294
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    .line 295
    iput p2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    .line 296
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 297
    sget-object v0, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Forced video size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    return-void

    .line 293
    :catchall_3a
    move-exception v0

    :try_start_3b
    monitor-exit v1
    :try_end_3c
    .catchall {:try_start_3b .. :try_end_3c} :catchall_3a

    throw v0
.end method

.method public final b()V
    .registers 3

    .prologue
    const/4 v1, 0x4

    .line 664
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 665
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 666
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 667
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 670
    :cond_16
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 671
    return-void
.end method

.method public final c()I
    .registers 4

    .prologue
    .line 704
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    if-gtz v0, :cond_2f

    .line 705
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 706
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 707
    sget-object v0, Lcom/dropbox/android/widget/DbxVideoView;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "got duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 712
    :goto_2e
    return v0

    :cond_2f
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    goto :goto_2e
.end method

.method public final d()I
    .registers 2

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 718
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 720
    :goto_c
    return v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public final e()Z
    .registers 2

    .prologue
    .line 735
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final f()I
    .registers 2

    .prologue
    .line 740
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_7

    .line 741
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->t:I

    .line 743
    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final g()Z
    .registers 2

    .prologue
    .line 756
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->w:Z

    return v0
.end method

.method public final h()Z
    .registers 2

    .prologue
    .line 761
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->x:Z

    return v0
.end method

.method public final i()Z
    .registers 2

    .prologue
    .line 766
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->y:Z

    return v0
.end method

.method public final j()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 207
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_16

    .line 208
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 210
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    .line 211
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    .line 212
    iput v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    .line 214
    :cond_16
    return-void
.end method

.method public final k()Z
    .registers 3

    .prologue
    .line 678
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final l()V
    .registers 2

    .prologue
    .line 682
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/DbxVideoView;->a(Z)V

    .line 683
    return-void
.end method

.method public final m()V
    .registers 1

    .prologue
    .line 686
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->q()V

    .line 687
    return-void
.end method

.method public final n()Z
    .registers 4

    .prologue
    const/4 v0, 0x1

    .line 748
    iget-object v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_13

    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_13

    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    if-eqz v1, :cond_13

    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->h:I

    if-eq v1, v0, :cond_13

    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 606
    const/4 v0, 0x4

    if-eq p1, v0, :cond_40

    const/16 v0, 0x18

    if-eq p1, v0, :cond_40

    const/16 v0, 0x19

    if-eq p1, v0, :cond_40

    const/16 v0, 0xa4

    if-eq p1, v0, :cond_40

    const/16 v0, 0x52

    if-eq p1, v0, :cond_40

    const/4 v0, 0x5

    if-eq p1, v0, :cond_40

    const/4 v0, 0x6

    if-eq p1, v0, :cond_40

    move v0, v1

    .line 613
    :goto_1b
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v2

    if-eqz v2, :cond_7c

    if-eqz v0, :cond_7c

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_7c

    .line 614
    const/16 v0, 0x4f

    if-eq p1, v0, :cond_2f

    const/16 v0, 0x55

    if-ne p1, v0, :cond_4b

    .line 616
    :cond_2f
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 617
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->b()V

    .line 618
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->b()V

    .line 642
    :cond_3f
    :goto_3f
    return v1

    .line 606
    :cond_40
    const/4 v0, 0x0

    goto :goto_1b

    .line 620
    :cond_42
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->a()V

    .line 621
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    goto :goto_3f

    .line 624
    :cond_4b
    const/16 v0, 0x7e

    if-ne p1, v0, :cond_60

    .line 625
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_3f

    .line 626
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->a()V

    .line 627
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    goto :goto_3f

    .line 630
    :cond_60
    const/16 v0, 0x56

    if-eq p1, v0, :cond_68

    const/16 v0, 0x7f

    if-ne p1, v0, :cond_79

    .line 632
    :cond_68
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->k:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 633
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->b()V

    .line 634
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->b()V

    goto :goto_3f

    .line 638
    :cond_79
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->s()V

    .line 642
    :cond_7c
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_3f
.end method

.method protected onMeasure(II)V
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 111
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/DbxVideoView;->getDefaultSize(II)I

    move-result v1

    .line 112
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    invoke-static {v0, p2}, Lcom/dropbox/android/widget/DbxVideoView;->getDefaultSize(II)I

    move-result v0

    .line 113
    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    if-lez v2, :cond_22

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    if-lez v2, :cond_22

    .line 114
    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    mul-int/2addr v3, v1

    if-le v2, v3, :cond_26

    .line 116
    iget v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    mul-int/2addr v0, v1

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    div-int/2addr v0, v2

    .line 127
    :cond_22
    :goto_22
    invoke-virtual {p0, v1, v0}, Lcom/dropbox/android/widget/DbxVideoView;->setMeasuredDimension(II)V

    .line 128
    return-void

    .line 117
    :cond_26
    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    mul-int/2addr v3, v1

    if-ge v2, v3, :cond_22

    .line 119
    iget v1, p0, Lcom/dropbox/android/widget/DbxVideoView;->l:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/dropbox/android/widget/DbxVideoView;->m:I

    div-int/2addr v1, v2

    goto :goto_22
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 589
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_d

    .line 590
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->s()V

    .line 592
    :cond_d
    const/4 v0, 0x0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .parameter

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->n()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_d

    .line 598
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->s()V

    .line 600
    :cond_d
    const/4 v0, 0x0

    return v0
.end method

.method public setAllowSeek(Z)V
    .registers 4
    .parameter

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->z:Z

    if-eqz v0, :cond_c

    .line 189
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setAllowSeek() called too late; video already prepared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_c
    iput-boolean p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->A:Z

    .line 192
    return-void
.end method

.method public setDuration(I)V
    .registers 2
    .parameter

    .prologue
    .line 693
    iput p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->g:I

    .line 694
    return-void
.end method

.method public setMediaController(Lcom/dropbox/android/widget/DbxMediaController;)V
    .registers 3
    .parameter

    .prologue
    .line 268
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    if-eqz v0, :cond_9

    .line 269
    iget-object v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/DbxMediaController;->d()V

    .line 271
    :cond_9
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->p:Lcom/dropbox/android/widget/DbxMediaController;

    .line 272
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->r()V

    .line 273
    return-void
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .registers 2
    .parameter

    .prologue
    .line 517
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->q:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 518
    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .registers 2
    .parameter

    .prologue
    .line 530
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->u:Landroid/media/MediaPlayer$OnErrorListener;

    .line 531
    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .registers 2
    .parameter

    .prologue
    .line 535
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->s:Landroid/media/MediaPlayer$OnInfoListener;

    .line 536
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .registers 2
    .parameter

    .prologue
    .line 506
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->r:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 507
    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .registers 3
    .parameter

    .prologue
    .line 172
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DbxVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 173
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .registers 3
    .parameter

    .prologue
    .line 176
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dropbox/android/widget/DbxVideoView;->setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V

    .line 177
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;Ljava/util/Map;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 198
    iput-object p1, p0, Lcom/dropbox/android/widget/DbxVideoView;->e:Landroid/net/Uri;

    .line 199
    iput-object p2, p0, Lcom/dropbox/android/widget/DbxVideoView;->f:Ljava/util/Map;

    .line 200
    const/4 v0, 0x0

    iput v0, p0, Lcom/dropbox/android/widget/DbxVideoView;->v:I

    .line 201
    invoke-direct {p0}, Lcom/dropbox/android/widget/DbxVideoView;->q()V

    .line 202
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->requestLayout()V

    .line 203
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DbxVideoView;->invalidate()V

    .line 204
    return-void
.end method
