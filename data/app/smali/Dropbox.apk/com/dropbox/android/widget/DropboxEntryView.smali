.class public Lcom/dropbox/android/widget/DropboxEntryView;
.super Lcom/dropbox/android/widget/QuickActionView;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/taskqueue/E;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/CheckBox;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/FrameLayout;

.field private i:Landroid/widget/ImageView;

.field private j:Ljava/util/concurrent/ThreadPoolExecutor;

.field private k:Landroid/widget/ProgressBar;

.field private l:Ldbxyzptlk/k/f;

.field private m:Ljava/lang/String;

.field private n:Z

.field private final o:Lcom/dropbox/android/widget/X;

.field private final p:Landroid/os/Handler;

.field private final q:Lcom/dropbox/android/widget/P;

.field private final r:Lcom/dropbox/android/filemanager/x;

.field private final s:Lcom/dropbox/android/taskqueue/q;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/widget/X;Lcom/dropbox/android/util/F;)V
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/QuickActionView;-><init>(Landroid/content/Context;)V

    .line 74
    iput-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->n:Z

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->p:Landroid/os/Handler;

    .line 78
    new-instance v0, Lcom/dropbox/android/widget/P;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->p:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/P;-><init>(Lcom/dropbox/android/widget/DropboxEntryView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->q:Lcom/dropbox/android/widget/P;

    .line 79
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->r:Lcom/dropbox/android/filemanager/x;

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->r:Lcom/dropbox/android/filemanager/x;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/taskqueue/q;

    .line 84
    const v0, 0x7f03003c

    invoke-static {p1, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 85
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/DropboxEntryView;->addView(Landroid/view/View;)V

    .line 87
    iput-object p2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/X;

    .line 88
    const v0, 0x7f06009a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->a:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f06009c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->c:Landroid/widget/CheckBox;

    .line 90
    const v0, 0x7f06009d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f06009b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    .line 93
    const v0, 0x7f06009e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->e:Landroid/widget/ImageView;

    .line 94
    const v0, 0x7f0600a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    .line 95
    const v0, 0x7f06009f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->h:Landroid/widget/FrameLayout;

    .line 96
    const v0, 0x7f060042

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->i:Landroid/widget/ImageView;

    .line 97
    const v0, 0x7f0600a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    .line 98
    const v0, 0x7f0600a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/DropboxEntryView;)Lcom/dropbox/android/taskqueue/q;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/taskqueue/q;

    return-object v0
.end method

.method private a(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 9
    .parameter

    .prologue
    const/16 v1, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 184
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    if-eqz v0, :cond_c

    .line 185
    invoke-direct {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->c()V

    .line 218
    :goto_b
    return-void

    .line 188
    :cond_c
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_1b

    .line 190
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 191
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_b

    .line 193
    :cond_1b
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 197
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-wide v1, p1, Lcom/dropbox/android/filemanager/LocalEntry;->h:J

    invoke-static {v0, v1, v2, v6}, Lcom/dropbox/android/util/aj;->a(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    .line 200
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/X;

    sget-object v2, Lcom/dropbox/android/widget/X;->c:Lcom/dropbox/android/widget/X;

    if-ne v0, v2, :cond_63

    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Z

    move-result v0

    if-nez v0, :cond_63

    .line 201
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0b0057

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 212
    :goto_47
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0b0058

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v5

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b

    .line 204
    :cond_63
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->l:Ljava/lang/String;

    invoke-static {v0}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bd;->a(Ljava/util/Date;)J

    move-result-wide v2

    .line 206
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0b005b

    invoke-static {v0, v4, v2, v3}, Lcom/dropbox/android/util/bd;->a(Landroid/content/Context;IJ)Ljava/lang/String;

    move-result-object v0

    goto :goto_47
.end method

.method private a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/support/v4/app/Fragment;ZLcom/dropbox/android/widget/quickactions/QuickActionBar;)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v8, 0xb3

    const/16 v7, 0xff

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/X;

    sget-object v3, Lcom/dropbox/android/widget/X;->b:Lcom/dropbox/android/widget/X;

    if-ne v0, v3, :cond_7f

    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-nez v0, :cond_7f

    move v0, v1

    .line 147
    :goto_11
    iget-object v3, p0, Lcom/dropbox/android/widget/DropboxEntryView;->r:Lcom/dropbox/android/filemanager/x;

    invoke-virtual {v3, p1}, Lcom/dropbox/android/filemanager/x;->c(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v3

    if-nez v3, :cond_81

    move v3, v1

    .line 148
    :goto_1a
    iget-object v4, p0, Lcom/dropbox/android/widget/DropboxEntryView;->r:Lcom/dropbox/android/filemanager/x;

    invoke-virtual {v4, p1}, Lcom/dropbox/android/filemanager/x;->d(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v4

    if-nez v4, :cond_83

    move v4, v1

    .line 150
    :goto_23
    iget-object v5, p0, Lcom/dropbox/android/widget/DropboxEntryView;->a:Landroid/widget/TextView;

    iget-object v6, p1, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DropboxEntryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 154
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DropboxEntryView;->b(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 156
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/DropboxEntryView;->c(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 158
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/dropbox/android/filemanager/x;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/k/f;

    move-result-object v5

    instance-of v5, v5, Ldbxyzptlk/k/d;

    .line 159
    iget-object v6, p0, Lcom/dropbox/android/widget/DropboxEntryView;->c:Landroid/widget/CheckBox;

    if-nez v0, :cond_85

    if-eqz v4, :cond_47

    if-eqz v3, :cond_47

    if-eqz v5, :cond_85

    :cond_47
    move v4, v1

    :goto_48
    invoke-virtual {v6, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 160
    iget-object v4, p0, Lcom/dropbox/android/widget/DropboxEntryView;->c:Landroid/widget/CheckBox;

    new-instance v5, Lcom/dropbox/android/widget/O;

    iget-object v6, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/X;

    invoke-direct {v5, p1, v6}, Lcom/dropbox/android/widget/O;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/widget/X;)V

    invoke-virtual {p0, p4, v4, p2, v5}, Lcom/dropbox/android/widget/DropboxEntryView;->a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/widget/CheckBox;Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/aq;)V

    .line 163
    if-nez v0, :cond_5b

    if-eqz v3, :cond_87

    :cond_5b
    move v0, v1

    .line 164
    :goto_5c
    if-nez v0, :cond_89

    :goto_5e
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/DropboxEntryView;->setEnabled(Z)V

    .line 165
    if-eqz v0, :cond_79

    .line 166
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    invoke-static {v8, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 167
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    invoke-static {v8, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 170
    :cond_79
    if-eqz p3, :cond_7e

    .line 171
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->a()V

    .line 173
    :cond_7e
    return-void

    :cond_7f
    move v0, v2

    .line 146
    goto :goto_11

    :cond_81
    move v3, v2

    .line 147
    goto :goto_1a

    :cond_83
    move v4, v2

    .line 148
    goto :goto_23

    :cond_85
    move v4, v2

    .line 159
    goto :goto_48

    :cond_87
    move v0, v2

    .line 163
    goto :goto_5c

    :cond_89
    move v1, v2

    .line 164
    goto :goto_5e
.end method

.method static synthetic b(Lcom/dropbox/android/widget/DropboxEntryView;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Ljava/lang/String;

    return-object v0
.end method

.method private b()V
    .registers 4

    .prologue
    const/16 v2, 0x8

    .line 130
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DropboxEntryView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 131
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 132
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    .line 133
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    .line 134
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 140
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 141
    return-void
.end method

.method private b(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 6
    .parameter

    .prologue
    .line 221
    iget-boolean v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->s:Z

    if-eqz v0, :cond_25

    .line 222
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 223
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Ljava/lang/String;

    .line 224
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/taskqueue/q;

    iget-object v1, p1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/taskqueue/q;->a(Ljava/lang/String;Ljava/lang/ref/WeakReference;)V

    .line 226
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->j:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/dropbox/android/widget/K;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/widget/K;-><init>(Lcom/dropbox/android/widget/DropboxEntryView;Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 264
    :goto_24
    return-void

    .line 257
    :cond_25
    iget-object v0, p1, Lcom/dropbox/android/filemanager/LocalEntry;->j:Ljava/lang/String;

    .line 258
    if-nez v0, :cond_2b

    .line 259
    const-string v0, ""

    .line 262
    :cond_2b
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->f:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    invoke-static {v1, v2, v3, v0}, Lcom/dropbox/android/widget/al;->a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_24
.end method

.method private c()V
    .registers 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 178
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    instance-of v0, v0, Ldbxyzptlk/k/k;

    if-eqz v0, :cond_1f

    move v2, v3

    .line 179
    :goto_d
    invoke-virtual {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    if-nez v2, :cond_21

    move v2, v3

    :goto_16
    iget-object v3, p0, Lcom/dropbox/android/widget/DropboxEntryView;->k:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/dropbox/android/widget/DropboxEntryView;->b:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/widget/al;->a(Landroid/content/Context;Ldbxyzptlk/k/f;ZLandroid/widget/ProgressBar;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    .line 181
    return-void

    :cond_1f
    move v2, v4

    .line 178
    goto :goto_d

    :cond_21
    move v2, v4

    .line 179
    goto :goto_16
.end method

.method private c(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 6
    .parameter

    .prologue
    const v0, 0x7f02015e

    const/4 v1, 0x0

    .line 267
    .line 268
    iget-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->o:Lcom/dropbox/android/widget/X;

    sget-object v3, Lcom/dropbox/android/widget/X;->c:Lcom/dropbox/android/widget/X;

    if-ne v2, v3, :cond_21

    .line 269
    invoke-virtual {p1}, Lcom/dropbox/android/filemanager/LocalEntry;->a()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 279
    :cond_10
    :goto_10
    if-lez v0, :cond_1c

    .line 280
    iget-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 281
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 283
    :cond_1c
    return-void

    .line 272
    :cond_1d
    const v0, 0x7f02015f

    goto :goto_10

    .line 275
    :cond_21
    iget-boolean v2, p1, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    if-nez v2, :cond_10

    move v0, v1

    goto :goto_10
.end method

.method static synthetic c(Lcom/dropbox/android/widget/DropboxEntryView;)Z
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->n:Z

    return v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->h:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/DropboxEntryView;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->i:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/DropboxEntryView;)V
    .registers 1
    .parameter

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->c()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;ZLjava/util/concurrent/ThreadPoolExecutor;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 104
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    if-eqz v0, :cond_e

    .line 105
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->q:Lcom/dropbox/android/widget/P;

    invoke-virtual {v0, v1}, Ldbxyzptlk/k/f;->unregisterObserver(Ljava/lang/Object;)V

    .line 106
    iput-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    .line 108
    :cond_e
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 109
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->s:Lcom/dropbox/android/taskqueue/q;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/dropbox/android/taskqueue/q;->a(Ljava/lang/String;Lcom/dropbox/android/taskqueue/E;)V

    .line 110
    iput-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Ljava/lang/String;

    .line 113
    :cond_1b
    invoke-direct {p0}, Lcom/dropbox/android/widget/DropboxEntryView;->b()V

    .line 115
    iput-object p5, p0, Lcom/dropbox/android/widget/DropboxEntryView;->j:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 117
    invoke-static {p1}, Lcom/dropbox/android/provider/K;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v0

    .line 118
    iget-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {v1}, Lcom/dropbox/android/util/ae;->i(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->n:Z

    .line 119
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->r:Lcom/dropbox/android/filemanager/x;

    invoke-virtual {v1}, Lcom/dropbox/android/filemanager/x;->b()Ldbxyzptlk/k/g;

    move-result-object v1

    new-instance v2, Ldbxyzptlk/k/i;

    new-instance v3, Lcom/dropbox/android/util/DropboxPath;

    iget-object v4, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ldbxyzptlk/k/i;-><init>(Lcom/dropbox/android/util/DropboxPath;)V

    invoke-virtual {v1, v2}, Ldbxyzptlk/k/g;->a(Ldbxyzptlk/k/i;)Ldbxyzptlk/k/f;

    move-result-object v1

    iput-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    .line 121
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    if-eqz v1, :cond_4f

    .line 122
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxEntryView;->l:Ldbxyzptlk/k/f;

    iget-object v2, p0, Lcom/dropbox/android/widget/DropboxEntryView;->q:Lcom/dropbox/android/widget/P;

    invoke-virtual {v1, v2}, Ldbxyzptlk/k/f;->registerObserver(Landroid/database/ContentObserver;)V

    .line 126
    :cond_4f
    invoke-direct {p0, v0, p3, p4, p2}, Lcom/dropbox/android/widget/DropboxEntryView;->a(Lcom/dropbox/android/filemanager/LocalEntry;Landroid/support/v4/app/Fragment;ZLcom/dropbox/android/widget/quickactions/QuickActionBar;)V

    .line 127
    return-void
.end method

.method public final a(Ljava/lang/String;Ldbxyzptlk/n/o;Lcom/dropbox/android/taskqueue/m;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 323
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 289
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-static {}, Lcom/dropbox/android/util/bc;->e()Ldbxyzptlk/n/o;

    move-result-object v0

    invoke-virtual {p3, v0}, Ldbxyzptlk/n/o;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 290
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxEntryView;->j:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v1, Lcom/dropbox/android/widget/M;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/dropbox/android/widget/M;-><init>(Lcom/dropbox/android/widget/DropboxEntryView;Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/o;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    .line 320
    :cond_1c
    return-void
.end method
