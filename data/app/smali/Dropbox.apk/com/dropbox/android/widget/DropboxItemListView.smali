.class public Lcom/dropbox/android/widget/DropboxItemListView;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# static fields
.field public static final a:Ldbxyzptlk/n/o;

.field private static c:Ljava/lang/String;


# instance fields
.field protected b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

.field private d:Lcom/dropbox/android/taskqueue/q;

.field private e:Lcom/dropbox/android/widget/T;

.field private f:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 33
    const-class v0, Lcom/dropbox/android/widget/DropboxItemListView;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/DropboxItemListView;->c:Ljava/lang/String;

    .line 35
    sget-object v0, Ldbxyzptlk/n/o;->c:Ldbxyzptlk/n/o;

    sput-object v0, Lcom/dropbox/android/widget/DropboxItemListView;->a:Ldbxyzptlk/n/o;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/DropboxItemListView;->a(Landroid/content/Context;)V

    .line 60
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/database/Cursor;
    .registers 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 113
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/T;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/T;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_33

    .line 114
    sget-object v0, Lcom/dropbox/android/widget/DropboxItemListView;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Trying to get item at position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but length is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/T;

    invoke-virtual {v3}, Lcom/dropbox/android/widget/T;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 122
    :goto_32
    return-object v0

    .line 118
    :cond_33
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/T;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/T;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 119
    instance-of v2, v0, Landroid/database/Cursor;

    if-eqz v2, :cond_40

    .line 120
    check-cast v0, Landroid/database/Cursor;

    goto :goto_32

    :cond_40
    move-object v0, v1

    .line 122
    goto :goto_32
.end method

.method public final a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 3
    .parameter

    .prologue
    .line 108
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/T;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/T;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;)V
    .registers 4
    .parameter

    .prologue
    .line 77
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030024

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 79
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/DropboxItemListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    .line 82
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->h()Lcom/dropbox/android/taskqueue/q;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->d:Lcom/dropbox/android/taskqueue/q;

    .line 83
    return-void
.end method

.method public final a(ZLandroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/c;Lcom/dropbox/android/widget/quickactions/f;Lcom/dropbox/android/widget/X;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 92
    if-eqz p1, :cond_13

    .line 93
    new-instance v0, Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-direct {v0, p2}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;-><init>(Landroid/support/v4/app/Fragment;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    .line 94
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0, p3}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/c;)V

    .line 95
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0, p4}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/quickactions/f;)V

    .line 99
    :cond_13
    new-instance v0, Lcom/dropbox/android/widget/T;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-direct {v0, v1, p2, p5}, Lcom/dropbox/android/widget/T;-><init>(Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/X;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/T;

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/T;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 101
    return-void
.end method

.method public final a()Z
    .registers 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 69
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->b:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c()V

    .line 70
    const/4 v0, 0x1

    .line 72
    :goto_12
    return v0

    :cond_13
    const/4 v0, 0x0

    goto :goto_12
.end method

.method public final b()Landroid/widget/ListView;
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    return-object v0
.end method

.method public final c()I
    .registers 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    return v0
.end method

.method public final d()Landroid/widget/ListAdapter;
    .registers 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public setInForeground(Z)V
    .registers 3
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->e:Lcom/dropbox/android/widget/T;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/widget/T;->a(Z)V

    .line 105
    return-void
.end method

.method public setItemClickListener(Lcom/dropbox/android/widget/S;)V
    .registers 4
    .parameter

    .prologue
    .line 135
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/dropbox/android/widget/Q;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/widget/Q;-><init>(Lcom/dropbox/android/widget/DropboxItemListView;Lcom/dropbox/android/widget/S;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 145
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    new-instance v1, Lcom/dropbox/android/widget/R;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/widget/R;-><init>(Lcom/dropbox/android/widget/DropboxItemListView;Lcom/dropbox/android/widget/S;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 151
    return-void
.end method

.method public setListViewScrollState(II)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 168
    if-ne v0, p1, :cond_14

    .line 169
    :goto_e
    iget-object v1, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v1, v0, p2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 170
    return-void

    .line 168
    :cond_14
    const/4 p2, 0x0

    goto :goto_e
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .registers 3
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 128
    return-void
.end method

.method public setSelection(I)V
    .registers 3
    .parameter

    .prologue
    .line 163
    iget-object v0, p0, Lcom/dropbox/android/widget/DropboxItemListView;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 164
    return-void
.end method
