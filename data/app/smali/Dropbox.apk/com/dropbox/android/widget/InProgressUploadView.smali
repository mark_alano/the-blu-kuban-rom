.class public Lcom/dropbox/android/widget/InProgressUploadView;
.super Lcom/dropbox/android/widget/QuickActionView;
.source "panda.py"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/CheckBox;

.field private d:Lcom/dropbox/android/widget/an;

.field private e:Landroid/widget/ProgressBar;

.field private f:Ldbxyzptlk/k/f;

.field private final g:Landroid/os/Handler;

.field private final h:Lcom/dropbox/android/widget/af;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/util/F;)V
    .registers 12
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/QuickActionView;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->g:Landroid/os/Handler;

    .line 40
    new-instance v0, Lcom/dropbox/android/widget/af;

    iget-object v1, p0, Lcom/dropbox/android/widget/InProgressUploadView;->g:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/af;-><init>(Lcom/dropbox/android/widget/InProgressUploadView;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->h:Lcom/dropbox/android/widget/af;

    .line 44
    const v0, 0x7f03003d

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/InProgressUploadView;->addView(Landroid/view/View;)V

    .line 47
    const v0, 0x7f06009a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->a:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f06009c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->c:Landroid/widget/CheckBox;

    .line 49
    const v0, 0x7f06009d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f06009b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->e:Landroid/widget/ProgressBar;

    .line 52
    const v0, 0x7f06009e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 53
    const v0, 0x7f0600a0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 54
    const v0, 0x7f06009f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    .line 55
    const v0, 0x7f060042

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 56
    const v0, 0x7f0600a1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 58
    new-instance v0, Lcom/dropbox/android/widget/an;

    iget-object v3, p0, Lcom/dropbox/android/widget/InProgressUploadView;->g:Landroid/os/Handler;

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lcom/dropbox/android/widget/an;-><init>(Lcom/dropbox/android/util/F;Landroid/content/Context;Landroid/os/Handler;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/FrameLayout;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->d:Lcom/dropbox/android/widget/an;

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/InProgressUploadView;)Ldbxyzptlk/k/f;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/InProgressUploadView;)Landroid/widget/ProgressBar;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->e:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private b()V
    .registers 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->e:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 125
    return-void
.end method

.method static synthetic c(Lcom/dropbox/android/widget/InProgressUploadView;)Landroid/widget/TextView;
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;Z)V
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 71
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    if-eqz v0, :cond_10

    .line 72
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    iget-object v3, p0, Lcom/dropbox/android/widget/InProgressUploadView;->h:Lcom/dropbox/android/widget/af;

    invoke-virtual {v0, v3}, Ldbxyzptlk/k/f;->unregisterObserver(Ljava/lang/Object;)V

    .line 73
    iput-object v5, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    .line 76
    :cond_10
    invoke-direct {p0}, Lcom/dropbox/android/widget/InProgressUploadView;->b()V

    .line 78
    const-string v0, "id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 79
    new-instance v0, Ldbxyzptlk/k/i;

    const-string v3, "id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-direct {v0, v3, v4}, Ldbxyzptlk/k/i;-><init>(J)V

    .line 80
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/filemanager/x;->b()Ldbxyzptlk/k/g;

    move-result-object v3

    invoke-virtual {v3, v0}, Ldbxyzptlk/k/g;->a(Ldbxyzptlk/k/i;)Ldbxyzptlk/k/f;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    .line 81
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    if-eqz v0, :cond_45

    .line 82
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    iget-object v3, p0, Lcom/dropbox/android/widget/InProgressUploadView;->h:Lcom/dropbox/android/widget/af;

    invoke-virtual {v0, v3}, Ldbxyzptlk/k/f;->registerObserver(Landroid/database/ContentObserver;)V

    .line 85
    :cond_45
    const-string v0, "local_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 86
    invoke-static {v0}, Lcom/dropbox/android/util/ae;->c(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    .line 87
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    if-nez v0, :cond_c9

    .line 93
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    instance-of v0, v0, Ldbxyzptlk/k/k;

    if-eqz v0, :cond_c9

    .line 94
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    check-cast v0, Ldbxyzptlk/k/k;

    invoke-virtual {v0}, Ldbxyzptlk/k/k;->c()F

    move-result v0

    const/4 v3, 0x0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_c9

    .line 95
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/u;->b()Lcom/dropbox/android/service/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/service/x;->a()Z

    move-result v0

    if-nez v0, :cond_c9

    .line 97
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    const v3, 0x7f0b0051

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    move v0, v1

    .line 104
    :goto_91
    if-eqz v0, :cond_a0

    .line 105
    invoke-virtual {p0}, Lcom/dropbox/android/widget/InProgressUploadView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/InProgressUploadView;->f:Ldbxyzptlk/k/f;

    iget-object v3, p0, Lcom/dropbox/android/widget/InProgressUploadView;->e:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/dropbox/android/widget/InProgressUploadView;->b:Landroid/widget/TextView;

    invoke-static/range {v0 .. v5}, Lcom/dropbox/android/widget/al;->a(Landroid/content/Context;Ldbxyzptlk/k/f;ZLandroid/widget/ProgressBar;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    .line 109
    :cond_a0
    const-string v0, "local_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    const-string v1, "mime_type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 113
    iget-object v2, p0, Lcom/dropbox/android/widget/InProgressUploadView;->d:Lcom/dropbox/android/widget/an;

    invoke-virtual {v2, v0, v8, v1}, Lcom/dropbox/android/widget/an;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/dropbox/android/widget/InProgressUploadView;->c:Landroid/widget/CheckBox;

    new-instance v1, Lcom/dropbox/android/widget/ae;

    invoke-direct {v1, v6, v7}, Lcom/dropbox/android/widget/ae;-><init>(J)V

    invoke-virtual {p0, p2, v0, p3, v1}, Lcom/dropbox/android/widget/InProgressUploadView;->a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/widget/CheckBox;Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/aq;)V

    .line 118
    if-eqz p4, :cond_c8

    .line 119
    invoke-virtual {p0}, Lcom/dropbox/android/widget/InProgressUploadView;->a()V

    .line 121
    :cond_c8
    return-void

    :cond_c9
    move v0, v2

    goto :goto_91
.end method
