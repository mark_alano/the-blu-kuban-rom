.class public final Lcom/dropbox/android/widget/O;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/widget/aq;


# instance fields
.field final a:Lcom/dropbox/android/filemanager/LocalEntry;

.field final b:Lcom/dropbox/android/widget/X;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;Lcom/dropbox/android/widget/X;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    iput-object p1, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 331
    iput-object p2, p0, Lcom/dropbox/android/widget/O;->b:Lcom/dropbox/android/widget/X;

    .line 332
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)[Lcom/dropbox/android/widget/quickactions/a;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 336
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v2, v3}, Lcom/dropbox/android/filemanager/x;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/k/f;

    move-result-object v2

    instance-of v2, v2, Ldbxyzptlk/k/d;

    .line 338
    if-eqz v2, :cond_1c

    .line 339
    new-instance v2, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;

    iget-object v3, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v2, v3}, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 340
    new-array v0, v0, [Lcom/dropbox/android/widget/quickactions/a;

    aput-object v2, v0, v1

    .line 366
    :goto_1b
    return-object v0

    .line 342
    :cond_1c
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 343
    new-instance v3, Lcom/dropbox/android/widget/quickactions/ButtonShare;

    iget-object v4, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v3, v4}, Lcom/dropbox/android/widget/quickactions/ButtonShare;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    iget-object v3, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v3, v3, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-nez v3, :cond_3b

    .line 345
    new-instance v3, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;

    iget-object v4, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v3, v4}, Lcom/dropbox/android/widget/quickactions/ButtonFavorite;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_3b
    iget-object v3, p0, Lcom/dropbox/android/widget/O;->b:Lcom/dropbox/android/widget/X;

    sget-object v4, Lcom/dropbox/android/widget/X;->c:Lcom/dropbox/android/widget/X;

    if-eq v3, v4, :cond_47

    iget-object v3, p0, Lcom/dropbox/android/widget/O;->b:Lcom/dropbox/android/widget/X;

    sget-object v4, Lcom/dropbox/android/widget/X;->e:Lcom/dropbox/android/widget/X;

    if-ne v3, v4, :cond_a7

    .line 350
    :cond_47
    :goto_47
    if-nez v0, :cond_87

    .line 351
    iget-object v1, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->f:Z

    if-eqz v1, :cond_5d

    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v1

    iget-object v3, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-virtual {v1, v3}, Lcom/dropbox/android/filemanager/x;->b(Lcom/dropbox/android/filemanager/LocalEntry;)Ldbxyzptlk/k/f;

    move-result-object v1

    instance-of v1, v1, Ldbxyzptlk/k/a;

    if-nez v1, :cond_87

    .line 352
    :cond_5d
    new-instance v1, Lcom/dropbox/android/widget/quickactions/ButtonDelete;

    iget-object v3, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v1, v3}, Lcom/dropbox/android/widget/quickactions/ButtonDelete;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    iget-object v1, p0, Lcom/dropbox/android/widget/O;->b:Lcom/dropbox/android/widget/X;

    sget-object v3, Lcom/dropbox/android/widget/X;->d:Lcom/dropbox/android/widget/X;

    if-eq v1, v3, :cond_87

    .line 354
    new-instance v1, Lcom/dropbox/android/widget/quickactions/ButtonRename;

    iget-object v3, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v1, v3}, Lcom/dropbox/android/widget/quickactions/ButtonRename;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    iget-object v1, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-nez v1, :cond_87

    .line 357
    new-instance v1, Lcom/dropbox/android/widget/quickactions/ButtonExport;

    iget-object v3, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v1, v3}, Lcom/dropbox/android/widget/quickactions/ButtonExport;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    :cond_87
    if-nez v0, :cond_8f

    iget-object v0, p0, Lcom/dropbox/android/widget/O;->b:Lcom/dropbox/android/widget/X;

    sget-object v1, Lcom/dropbox/android/widget/X;->d:Lcom/dropbox/android/widget/X;

    if-ne v0, v1, :cond_99

    .line 363
    :cond_8f
    new-instance v0, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;

    iget-object v1, p0, Lcom/dropbox/android/widget/O;->a:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/quickactions/ButtonViewInFolder;-><init>(Lcom/dropbox/android/filemanager/LocalEntry;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    :cond_99
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/dropbox/android/widget/quickactions/a;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dropbox/android/widget/quickactions/a;

    goto/16 :goto_1b

    :cond_a7
    move v0, v1

    .line 348
    goto :goto_47
.end method
