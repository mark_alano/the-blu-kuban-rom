.class public abstract Lcom/dropbox/android/widget/QuickActionView;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# instance fields
.field private a:Lcom/dropbox/android/widget/aq;

.field private b:Landroid/support/v4/app/Fragment;

.field private c:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

.field private d:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/QuickActionView;)Lcom/dropbox/android/widget/aq;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->a:Lcom/dropbox/android/widget/aq;

    return-object v0
.end method

.method static synthetic b(Lcom/dropbox/android/widget/QuickActionView;)Landroid/support/v4/app/Fragment;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->b:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/QuickActionView;)Landroid/widget/CheckBox;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->d:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/QuickActionView;)Lcom/dropbox/android/widget/quickactions/QuickActionBar;
    .registers 2
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->c:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .registers 4

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/dropbox/android/widget/QuickActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 88
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/QuickActionView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 89
    const/16 v1, 0x320

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 90
    new-instance v0, Lcom/dropbox/android/widget/ax;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/ax;-><init>(Lcom/dropbox/android/widget/QuickActionView;)V

    const-wide/16 v1, 0x384

    invoke-virtual {p0, v0, v1, v2}, Lcom/dropbox/android/widget/QuickActionView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 96
    return-void
.end method

.method protected final a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/widget/CheckBox;Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/aq;)V
    .registers 8
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 35
    iput-object p1, p0, Lcom/dropbox/android/widget/QuickActionView;->c:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    .line 36
    iput-object p2, p0, Lcom/dropbox/android/widget/QuickActionView;->d:Landroid/widget/CheckBox;

    .line 37
    iput-object p3, p0, Lcom/dropbox/android/widget/QuickActionView;->b:Landroid/support/v4/app/Fragment;

    .line 38
    if-eqz p1, :cond_1f

    .line 39
    iput-object p4, p0, Lcom/dropbox/android/widget/QuickActionView;->a:Lcom/dropbox/android/widget/aq;

    .line 40
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 41
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->d:Landroid/widget/CheckBox;

    new-instance v1, Lcom/dropbox/android/widget/av;

    invoke-direct {v1, p0}, Lcom/dropbox/android/widget/av;-><init>(Lcom/dropbox/android/widget/QuickActionView;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 54
    :cond_1f
    return-void
.end method

.method protected onCreateContextMenu(Landroid/view/ContextMenu;)V
    .registers 8
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->a:Lcom/dropbox/android/widget/aq;

    if-eqz v0, :cond_2e

    .line 64
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->c:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->e()[Lcom/dropbox/android/widget/quickactions/a;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_25

    .line 72
    :goto_c
    array-length v2, v0

    const/4 v1, 0x0

    :goto_e
    if-ge v1, v2, :cond_2e

    aget-object v3, v0, v1

    .line 73
    invoke-virtual {v3}, Lcom/dropbox/android/widget/quickactions/a;->b()I

    move-result v4

    invoke-interface {p1, v4}, Landroid/view/ContextMenu;->add(I)Landroid/view/MenuItem;

    move-result-object v4

    new-instance v5, Lcom/dropbox/android/widget/aw;

    invoke-direct {v5, p0, v3}, Lcom/dropbox/android/widget/aw;-><init>(Lcom/dropbox/android/widget/QuickActionView;Lcom/dropbox/android/widget/quickactions/a;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 68
    :cond_25
    iget-object v0, p0, Lcom/dropbox/android/widget/QuickActionView;->a:Lcom/dropbox/android/widget/aq;

    iget-object v1, p0, Lcom/dropbox/android/widget/QuickActionView;->b:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v1}, Lcom/dropbox/android/widget/aq;->a(Landroid/support/v4/app/Fragment;)[Lcom/dropbox/android/widget/quickactions/a;

    move-result-object v0

    goto :goto_c

    .line 82
    :cond_2e
    return-void
.end method
