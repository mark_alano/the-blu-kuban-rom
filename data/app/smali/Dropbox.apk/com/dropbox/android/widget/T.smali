.class public Lcom/dropbox/android/widget/T;
.super Ldbxyzptlk/e/a;
.source "panda.py"


# static fields
.field private static final k:Ljava/lang/String;


# instance fields
.field protected final j:Lcom/dropbox/android/util/F;

.field private l:Lcom/dropbox/android/widget/V;

.field private m:Z

.field private final n:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

.field private final o:Landroid/support/v4/app/Fragment;

.field private final p:Lcom/dropbox/android/widget/X;

.field private q:Ljava/util/concurrent/BlockingQueue;

.field private r:Ljava/util/concurrent/ThreadPoolExecutor;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 26
    const-class v0, Lcom/dropbox/android/widget/T;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/T;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/X;)V
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    .line 66
    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v8, v2}, Ldbxyzptlk/e/a;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 55
    new-instance v0, Lcom/dropbox/android/widget/V;

    invoke-direct {v0}, Lcom/dropbox/android/widget/V;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/T;->l:Lcom/dropbox/android/widget/V;

    .line 56
    iput-boolean v1, p0, Lcom/dropbox/android/widget/T;->m:Z

    .line 224
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/T;->q:Ljava/util/concurrent/BlockingQueue;

    .line 226
    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, Lcom/dropbox/android/widget/T;->q:Ljava/util/concurrent/BlockingQueue;

    new-instance v7, Lcom/dropbox/android/widget/W;

    invoke-direct {v7, v8}, Lcom/dropbox/android/widget/W;-><init>(Lcom/dropbox/android/widget/U;)V

    move v2, v1

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/T;->r:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 67
    iput-object p1, p0, Lcom/dropbox/android/widget/T;->n:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    .line 68
    iput-object p2, p0, Lcom/dropbox/android/widget/T;->o:Landroid/support/v4/app/Fragment;

    .line 69
    iput-object p3, p0, Lcom/dropbox/android/widget/T;->p:Lcom/dropbox/android/widget/X;

    .line 70
    new-instance v0, Lcom/dropbox/android/util/F;

    invoke-direct {v0}, Lcom/dropbox/android/util/F;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/T;->j:Lcom/dropbox/android/util/F;

    .line 71
    return-void
.end method

.method private c()V
    .registers 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/dropbox/android/widget/T;->n:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    if-eqz v0, :cond_9

    .line 185
    iget-object v0, p0, Lcom/dropbox/android/widget/T;->n:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c()V

    .line 187
    :cond_9
    return-void
.end method

.method public static d(Landroid/database/Cursor;)Z
    .registers 3
    .parameter

    .prologue
    .line 201
    if-eqz p0, :cond_16

    .line 202
    const/4 v0, -0x1

    invoke-interface {p0, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 203
    :cond_6
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 204
    invoke-static {p0}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v0

    .line 205
    sget-object v1, Lcom/dropbox/android/provider/P;->b:Lcom/dropbox/android/provider/P;

    if-eq v0, v1, :cond_6

    .line 206
    const/4 v0, 0x1

    .line 211
    :goto_15
    return v0

    :cond_16
    const/4 v0, 0x0

    goto :goto_15
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 129
    invoke-static {p2}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v0

    .line 130
    sget-object v1, Lcom/dropbox/android/widget/U;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/P;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_30

    .line 138
    :pswitch_f
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown item type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :pswitch_17
    new-instance v0, Lcom/dropbox/android/widget/UpFolderView;

    invoke-direct {v0, p1}, Lcom/dropbox/android/widget/UpFolderView;-><init>(Landroid/content/Context;)V

    .line 136
    :goto_1c
    return-object v0

    .line 134
    :pswitch_1d
    new-instance v0, Lcom/dropbox/android/widget/InProgressUploadView;

    iget-object v1, p0, Lcom/dropbox/android/widget/T;->j:Lcom/dropbox/android/util/F;

    invoke-direct {v0, p1, v1}, Lcom/dropbox/android/widget/InProgressUploadView;-><init>(Landroid/content/Context;Lcom/dropbox/android/util/F;)V

    goto :goto_1c

    .line 136
    :pswitch_25
    new-instance v0, Lcom/dropbox/android/widget/DropboxEntryView;

    iget-object v1, p0, Lcom/dropbox/android/widget/T;->p:Lcom/dropbox/android/widget/X;

    iget-object v2, p0, Lcom/dropbox/android/widget/T;->j:Lcom/dropbox/android/util/F;

    invoke-direct {v0, p1, v1, v2}, Lcom/dropbox/android/widget/DropboxEntryView;-><init>(Landroid/content/Context;Lcom/dropbox/android/widget/X;Lcom/dropbox/android/util/F;)V

    goto :goto_1c

    .line 130
    nop

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_25
        :pswitch_f
        :pswitch_17
        :pswitch_1d
    .end packed-switch
.end method

.method public final a(I)V
    .registers 3
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/dropbox/android/widget/T;->l:Lcom/dropbox/android/widget/V;

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/V;->a(Lcom/dropbox/android/widget/V;I)V

    .line 75
    return-void
.end method

.method public final a(Landroid/database/Cursor;)V
    .registers 2
    .parameter

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/dropbox/android/widget/T;->c()V

    .line 166
    invoke-super {p0, p1}, Ldbxyzptlk/e/a;->a(Landroid/database/Cursor;)V

    .line 167
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 144
    invoke-static {p3}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v0

    .line 145
    iget-object v1, p0, Lcom/dropbox/android/widget/T;->l:Lcom/dropbox/android/widget/V;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/dropbox/android/widget/V;->a(I)Z

    move-result v4

    .line 146
    sget-object v1, Lcom/dropbox/android/widget/U;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/P;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_40

    .line 159
    :pswitch_19
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid item type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :pswitch_21
    check-cast p1, Lcom/dropbox/android/widget/UpFolderView;

    invoke-virtual {p1, p3}, Lcom/dropbox/android/widget/UpFolderView;->a(Landroid/database/Cursor;)V

    .line 161
    :goto_26
    return-void

    .line 151
    :pswitch_27
    check-cast p1, Lcom/dropbox/android/widget/InProgressUploadView;

    iget-object v0, p0, Lcom/dropbox/android/widget/T;->n:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    iget-object v1, p0, Lcom/dropbox/android/widget/T;->o:Landroid/support/v4/app/Fragment;

    invoke-virtual {p1, p3, v0, v1, v4}, Lcom/dropbox/android/widget/InProgressUploadView;->a(Landroid/database/Cursor;Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;Z)V

    goto :goto_26

    :pswitch_31
    move-object v0, p1

    .line 155
    check-cast v0, Lcom/dropbox/android/widget/DropboxEntryView;

    iget-object v2, p0, Lcom/dropbox/android/widget/T;->n:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    iget-object v3, p0, Lcom/dropbox/android/widget/T;->o:Landroid/support/v4/app/Fragment;

    iget-object v5, p0, Lcom/dropbox/android/widget/T;->r:Ljava/util/concurrent/ThreadPoolExecutor;

    move-object v1, p3

    invoke-virtual/range {v0 .. v5}, Lcom/dropbox/android/widget/DropboxEntryView;->a(Landroid/database/Cursor;Lcom/dropbox/android/widget/quickactions/QuickActionBar;Landroid/support/v4/app/Fragment;ZLjava/util/concurrent/ThreadPoolExecutor;)V

    goto :goto_26

    .line 146
    nop

    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_31
        :pswitch_19
        :pswitch_21
        :pswitch_27
    .end packed-switch
.end method

.method public final a(Z)V
    .registers 2
    .parameter

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/dropbox/android/widget/T;->m:Z

    .line 79
    return-void
.end method

.method public final b(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 3
    .parameter

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/dropbox/android/widget/T;->c()V

    .line 172
    invoke-super {p0, p1}, Ldbxyzptlk/e/a;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final b()V
    .registers 3

    .prologue
    .line 177
    sget-object v0, Lcom/dropbox/android/widget/T;->k:Ljava/lang/String;

    const-string v1, "onContentChanged"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-direct {p0}, Lcom/dropbox/android/widget/T;->c()V

    .line 179
    invoke-super {p0}, Ldbxyzptlk/e/a;->b()V

    .line 181
    return-void
.end method

.method protected finalize()V
    .registers 2

    .prologue
    .line 235
    :try_start_0
    iget-object v0, p0, Lcom/dropbox/android/widget/T;->r:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V
    :try_end_5
    .catchall {:try_start_0 .. :try_end_5} :catchall_9

    .line 237
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 239
    return-void

    .line 237
    :catchall_9
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getItemViewType(I)I
    .registers 4
    .parameter

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/dropbox/android/widget/T;->a()Landroid/database/Cursor;

    move-result-object v0

    .line 109
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 110
    invoke-static {v0}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v0

    .line 111
    sget-object v1, Lcom/dropbox/android/widget/U;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/provider/P;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_26

    .line 121
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown item type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :pswitch_1e
    const/4 v0, 0x0

    .line 119
    :goto_1f
    return v0

    .line 115
    :pswitch_20
    const/4 v0, 0x1

    goto :goto_1f

    .line 117
    :pswitch_22
    const/4 v0, 0x2

    goto :goto_1f

    .line 119
    :pswitch_24
    const/4 v0, 0x3

    goto :goto_1f

    .line 111
    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_24
        :pswitch_20
        :pswitch_1e
        :pswitch_22
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .registers 2

    .prologue
    .line 103
    const/4 v0, 0x6

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/dropbox/android/widget/T;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/dropbox/android/widget/T;->a:Z

    if-nez v0, :cond_a

    .line 194
    :cond_8
    const/4 v0, 0x0

    .line 197
    :goto_9
    return v0

    :cond_a
    invoke-super {p0}, Ldbxyzptlk/e/a;->isEmpty()Z

    move-result v0

    goto :goto_9
.end method

.method public isEnabled(I)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    invoke-virtual {p0}, Lcom/dropbox/android/widget/T;->a()Landroid/database/Cursor;

    move-result-object v2

    .line 84
    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 86
    invoke-static {v2}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v3

    .line 87
    sget-object v4, Lcom/dropbox/android/widget/U;->a:[I

    invoke-virtual {v3}, Lcom/dropbox/android/provider/P;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_3a

    move v0, v1

    .line 97
    :cond_19
    :goto_19
    :pswitch_19
    return v0

    .line 89
    :pswitch_1a
    invoke-static {v2}, Lcom/dropbox/android/provider/K;->a(Landroid/database/Cursor;)Lcom/dropbox/android/filemanager/LocalEntry;

    move-result-object v2

    .line 90
    iget-object v3, p0, Lcom/dropbox/android/widget/T;->p:Lcom/dropbox/android/widget/X;

    sget-object v4, Lcom/dropbox/android/widget/X;->b:Lcom/dropbox/android/widget/X;

    if-ne v3, v4, :cond_28

    iget-boolean v3, v2, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v3, :cond_32

    :cond_28
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/dropbox/android/filemanager/x;->c(Lcom/dropbox/android/filemanager/LocalEntry;)Z

    move-result v2

    if-nez v2, :cond_37

    :cond_32
    move v2, v0

    .line 92
    :goto_33
    if-eqz v2, :cond_19

    move v0, v1

    goto :goto_19

    :cond_37
    move v2, v1

    .line 90
    goto :goto_33

    .line 87
    nop

    :pswitch_data_3a
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_19
        :pswitch_19
    .end packed-switch
.end method
