.class public Lcom/dropbox/android/widget/TurnOnCameraUploadsStatusBar;
.super Landroid/widget/FrameLayout;
.source "panda.py"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 20
    const v0, 0x7f030020

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 21
    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/TurnOnCameraUploadsStatusBar;->addView(Landroid/view/View;)V

    .line 22
    const v1, 0x7f060059

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/dropbox/android/widget/ViewButton;

    .line 23
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ViewButton;->setClickable(Z)V

    .line 24
    new-instance v1, Lcom/dropbox/android/widget/aH;

    invoke-direct {v1, p0, p1}, Lcom/dropbox/android/widget/aH;-><init>(Lcom/dropbox/android/widget/TurnOnCameraUploadsStatusBar;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/ViewButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    invoke-virtual {p2, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 35
    return-void
.end method
