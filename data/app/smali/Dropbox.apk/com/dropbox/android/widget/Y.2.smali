.class public final Lcom/dropbox/android/widget/Y;
.super Lcom/dropbox/android/widget/az;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/f;


# instance fields
.field a:Lcom/dropbox/android/filemanager/d;

.field protected final b:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/Set;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/az;-><init>(Landroid/content/Context;)V

    .line 31
    iput-object p3, p0, Lcom/dropbox/android/widget/Y;->b:Ljava/util/Set;

    .line 32
    invoke-virtual {p0, p2}, Lcom/dropbox/android/widget/Y;->a(Landroid/database/Cursor;)V

    .line 33
    return-void
.end method

.method private a(Landroid/database/Cursor;Landroid/view/View;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 71
    const/4 v0, 0x0

    .line 73
    const-string v1, "_cursor_type_tag"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 74
    const-string v2, "_separator"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_60

    .line 75
    const-string v0, "_separator"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v1, "_sep_camera_roll"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_45

    .line 77
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->c:Landroid/content/Context;

    const v1, 0x7f0b0139

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 85
    :goto_2f
    const v0, 0x7f0600a5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->a:Lcom/dropbox/android/filemanager/d;

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/d;->a(I)V

    .line 90
    return-void

    .line 78
    :cond_45
    const-string v1, "_sep_other_media"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->c:Landroid/content/Context;

    const v1, 0x7f0b013a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2f

    .line 81
    :cond_58
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected seperator used in GalleryPickerListAdapter"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_60
    move-object v1, v0

    goto :goto_2f
.end method

.method private a(Lcom/dropbox/android/widget/SweetListView;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 93
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_44

    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_44

    .line 96
    invoke-virtual {p1}, Lcom/dropbox/android/widget/SweetListView;->getFirstVisiblePosition()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 97
    invoke-virtual {p1}, Lcom/dropbox/android/widget/SweetListView;->getLastVisiblePosition()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 100
    invoke-virtual {p1}, Lcom/dropbox/android/widget/SweetListView;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_44

    .line 101
    iget-object v2, p0, Lcom/dropbox/android/widget/Y;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 102
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/Y;->h:I

    add-int/2addr v0, v1

    .line 103
    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->a:Lcom/dropbox/android/filemanager/d;

    sub-int/2addr v0, v2

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/filemanager/d;->a(II)V

    .line 106
    :cond_44
    return-void
.end method

.method private b(Landroid/database/Cursor;Landroid/view/View;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 152
    const-string v0, "content_uri"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 153
    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 155
    check-cast p2, Lcom/dropbox/android/widget/GalleryItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->a:Lcom/dropbox/android/filemanager/d;

    invoke-virtual {p2, p1, v0, v1}, Lcom/dropbox/android/widget/GalleryItemView;->a(Landroid/database/Cursor;ZLcom/dropbox/android/filemanager/d;)V

    .line 156
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 121
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 126
    packed-switch p2, :pswitch_data_1e

    .line 132
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :pswitch_b
    new-instance v0, Lcom/dropbox/android/widget/GalleryItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/dropbox/android/widget/GalleryItemView;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 130
    :goto_12
    return-object v0

    :pswitch_13
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->c:Landroid/content/Context;

    const v1, 0x7f03003e

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_12

    .line 126
    nop

    :pswitch_data_1e
    .packed-switch -0x1
        :pswitch_b
        :pswitch_13
    .end packed-switch
.end method

.method public final a(I)Lcom/dropbox/android/filemanager/g;
    .registers 8
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 169
    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 170
    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/Y;->d(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_49

    .line 173
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    const-string v2, "thumb_path"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 174
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    const-string v3, "content_uri"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 175
    const-string v0, "_tag_video"

    iget-object v3, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    iget-object v4, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    const-string v5, "_cursor_type_tag"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 176
    new-instance v0, Lcom/dropbox/android/filemanager/g;

    invoke-direct {v0, v1, v2, v3}, Lcom/dropbox/android/filemanager/g;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 180
    :goto_48
    return-object v0

    .line 178
    :cond_49
    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 180
    const/4 v0, 0x0

    goto :goto_48
.end method

.method public final a(Landroid/database/Cursor;)V
    .registers 5
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->a:Lcom/dropbox/android/filemanager/d;

    if-eqz v0, :cond_9

    .line 38
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->a:Lcom/dropbox/android/filemanager/d;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/d;->a()V

    .line 41
    :cond_9
    if-eqz p1, :cond_27

    .line 42
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/bc;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_2b

    const/4 v0, 0x1

    .line 45
    :goto_18
    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->a:Lcom/dropbox/android/filemanager/d;

    if-nez v1, :cond_2d

    .line 46
    new-instance v1, Lcom/dropbox/android/filemanager/d;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v1, v2, p0, v0}, Lcom/dropbox/android/filemanager/d;-><init>(ILcom/dropbox/android/filemanager/f;I)V

    iput-object v1, p0, Lcom/dropbox/android/widget/Y;->a:Lcom/dropbox/android/filemanager/d;

    .line 52
    :cond_27
    :goto_27
    invoke-super {p0, p1}, Lcom/dropbox/android/widget/az;->a(Landroid/database/Cursor;)V

    .line 53
    return-void

    .line 42
    :cond_2b
    const/4 v0, 0x3

    goto :goto_18

    .line 48
    :cond_2d
    new-instance v0, Lcom/dropbox/android/filemanager/d;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/Y;->a:Lcom/dropbox/android/filemanager/d;

    invoke-direct {v0, v1, p0, v2}, Lcom/dropbox/android/filemanager/d;-><init>(ILcom/dropbox/android/filemanager/f;Lcom/dropbox/android/filemanager/d;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/Y;->a:Lcom/dropbox/android/filemanager/d;

    goto :goto_27
.end method

.method public final a(Landroid/database/Cursor;Landroid/view/View;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 138
    packed-switch p3, :pswitch_data_14

    .line 146
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :pswitch_b
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/Y;->b(Landroid/database/Cursor;Landroid/view/View;)V

    .line 148
    :goto_e
    return-void

    .line 143
    :pswitch_f
    invoke-direct {p0, p1, p2}, Lcom/dropbox/android/widget/Y;->a(Landroid/database/Cursor;Landroid/view/View;)V

    goto :goto_e

    .line 138
    nop

    :pswitch_data_14
    .packed-switch -0x1
        :pswitch_b
        :pswitch_f
    .end packed-switch
.end method

.method public final b()I
    .registers 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 161
    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public final b(Landroid/database/Cursor;)Z
    .registers 3
    .parameter

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/Y;->d(Landroid/database/Cursor;)Z

    move-result v0

    return v0
.end method

.method public final c(Landroid/database/Cursor;)I
    .registers 4
    .parameter

    .prologue
    .line 110
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    const-string v1, "_cursor_type_tag"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 111
    iget-object v1, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 112
    const-string v1, "_tag_photo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    const-string v1, "_tag_video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 113
    :cond_1e
    const/4 v0, -0x1

    .line 115
    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    move-object v0, p3

    check-cast v0, Lcom/dropbox/android/widget/SweetListView;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/Y;->a(Lcom/dropbox/android/widget/SweetListView;)V

    .line 67
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/widget/az;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/dropbox/android/widget/Y;->d:Landroid/database/Cursor;

    if-nez v0, :cond_6

    .line 188
    const/4 v0, 0x0

    .line 191
    :goto_5
    return v0

    :cond_6
    invoke-super {p0}, Lcom/dropbox/android/widget/az;->isEmpty()Z

    move-result v0

    goto :goto_5
.end method
