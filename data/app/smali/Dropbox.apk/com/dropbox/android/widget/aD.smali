.class public final Lcom/dropbox/android/widget/aD;
.super Lcom/dropbox/android/widget/az;
.source "panda.py"

# interfaces
.implements Lcom/dropbox/android/filemanager/o;


# static fields
.field private static final a:Ljava/util/Map;


# instance fields
.field private b:Lcom/dropbox/android/filemanager/i;

.field private k:Lcom/dropbox/android/widget/aF;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/dropbox/android/widget/aD;->a:Ljava/util/Map;

    .line 42
    sget-object v0, Lcom/dropbox/android/widget/aD;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/provider/P;->e:Lcom/dropbox/android/provider/P;

    sget-object v2, Lcom/dropbox/android/widget/aG;->a:Lcom/dropbox/android/widget/aG;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/aG;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/dropbox/android/widget/aD;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/provider/P;->d:Lcom/dropbox/android/provider/P;

    sget-object v2, Lcom/dropbox/android/widget/aG;->b:Lcom/dropbox/android/widget/aG;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/aG;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/dropbox/android/widget/aD;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/provider/P;->f:Lcom/dropbox/android/provider/P;

    sget-object v2, Lcom/dropbox/android/widget/aG;->c:Lcom/dropbox/android/widget/aG;

    invoke-virtual {v2}, Lcom/dropbox/android/widget/aG;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/dropbox/android/widget/aD;->a:Ljava/util/Map;

    sget-object v1, Lcom/dropbox/android/provider/P;->a:Lcom/dropbox/android/provider/P;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/az;-><init>(Landroid/content/Context;)V

    .line 49
    new-instance v0, Lcom/dropbox/android/widget/aF;

    invoke-direct {v0, p0}, Lcom/dropbox/android/widget/aF;-><init>(Lcom/dropbox/android/widget/aD;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/aD;->k:Lcom/dropbox/android/widget/aF;

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dropbox/android/widget/aD;->l:Z

    .line 53
    invoke-virtual {p0, p2}, Lcom/dropbox/android/widget/aD;->a(Landroid/database/Cursor;)V

    .line 54
    return-void
.end method

.method private a(Lcom/dropbox/android/widget/SweetListView;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 96
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_44

    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_44

    .line 99
    invoke-virtual {p1}, Lcom/dropbox/android/widget/SweetListView;->getFirstVisiblePosition()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 100
    invoke-virtual {p1}, Lcom/dropbox/android/widget/SweetListView;->getLastVisiblePosition()I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 103
    invoke-virtual {p1}, Lcom/dropbox/android/widget/SweetListView;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_44

    .line 104
    iget-object v2, p0, Lcom/dropbox/android/widget/aD;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 105
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/aD;->h:I

    add-int/2addr v0, v1

    .line 106
    iget-object v1, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    sub-int/2addr v0, v2

    invoke-virtual {v1, v2, v0}, Lcom/dropbox/android/filemanager/i;->a(II)V

    .line 109
    :cond_44
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/aD;)Z
    .registers 2
    .parameter

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/dropbox/android/widget/aD;->l:Z

    return v0
.end method

.method private e(Landroid/database/Cursor;)V
    .registers 5
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 206
    if-eqz p1, :cond_1c

    .line 207
    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 208
    :cond_6
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 209
    invoke-static {p1}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/P;->d:Lcom/dropbox/android/provider/P;

    if-ne v0, v1, :cond_6

    .line 210
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->k:Lcom/dropbox/android/widget/aF;

    invoke-static {v0, p1}, Lcom/dropbox/android/widget/aF;->a(Lcom/dropbox/android/widget/aF;Landroid/database/Cursor;)V

    .line 214
    :cond_19
    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 216
    :cond_1c
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 118
    invoke-static {}, Lcom/dropbox/android/widget/aG;->values()[Lcom/dropbox/android/widget/aG;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/view/View;
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 147
    const/4 v0, -0x1

    if-ne p2, v0, :cond_b

    .line 148
    new-instance v0, Lcom/dropbox/android/widget/ThumbGridItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/dropbox/android/widget/ThumbGridItemView;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 159
    :goto_a
    return-object v0

    .line 150
    :cond_b
    invoke-static {}, Lcom/dropbox/android/widget/aG;->values()[Lcom/dropbox/android/widget/aG;

    move-result-object v0

    aget-object v0, v0, p2

    .line 151
    sget-object v1, Lcom/dropbox/android/widget/aE;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aG;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_42

    .line 161
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :pswitch_24
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/content/Context;

    const v1, 0x7f03003f

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_a

    .line 155
    :pswitch_2e
    new-instance v0, Lcom/dropbox/android/widget/CameraUploadItemView;

    iget-object v1, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/dropbox/android/widget/CameraUploadItemView;-><init>(Landroid/content/Context;)V

    .line 156
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_a

    .line 159
    :pswitch_39
    new-instance v0, Lcom/dropbox/android/widget/TurnOnCameraUploadsStatusBar;

    iget-object v1, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/dropbox/android/widget/TurnOnCameraUploadsStatusBar;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    goto :goto_a

    .line 151
    nop

    :pswitch_data_42
    .packed-switch 0x1
        :pswitch_24
        :pswitch_2e
        :pswitch_39
    .end packed-switch
.end method

.method public final a(I)Lcom/dropbox/android/filemanager/p;
    .registers 7
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 179
    iget-object v1, p0, Lcom/dropbox/android/widget/aD;->d:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 180
    iget-object v1, p0, Lcom/dropbox/android/widget/aD;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/aD;->d(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_43

    .line 183
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->d:Landroid/database/Cursor;

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 184
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->d:Landroid/database/Cursor;

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 185
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->d:Landroid/database/Cursor;

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_41

    const-string v1, "video/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    const/4 v0, 0x1

    .line 187
    :goto_34
    new-instance v1, Lcom/dropbox/android/filemanager/p;

    iget-object v4, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/dropbox/android/filemanager/p;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;Z)V

    move-object v0, v1

    .line 191
    :goto_40
    return-object v0

    .line 186
    :cond_41
    const/4 v0, 0x0

    goto :goto_34

    .line 189
    :cond_43
    iget-object v1, p0, Lcom/dropbox/android/widget/aD;->d:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 191
    const/4 v0, 0x0

    goto :goto_40
.end method

.method public final a(Landroid/database/Cursor;)V
    .registers 5
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    if-eqz v0, :cond_9

    .line 59
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/i;->a()V

    .line 62
    :cond_9
    if-eqz p1, :cond_1e

    .line 63
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    if-nez v0, :cond_25

    .line 64
    new-instance v0, Lcom/dropbox/android/filemanager/i;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-static {}, Lcom/dropbox/android/util/bc;->d()Ldbxyzptlk/n/o;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/dropbox/android/filemanager/i;-><init>(ILcom/dropbox/android/filemanager/o;Ldbxyzptlk/n/o;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    .line 69
    :cond_1e
    :goto_1e
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/aD;->e(Landroid/database/Cursor;)V

    .line 71
    invoke-super {p0, p1}, Lcom/dropbox/android/widget/az;->a(Landroid/database/Cursor;)V

    .line 72
    return-void

    .line 66
    :cond_25
    new-instance v0, Lcom/dropbox/android/filemanager/i;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iget-object v2, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    invoke-direct {v0, v1, p0, v2}, Lcom/dropbox/android/filemanager/i;-><init>(ILcom/dropbox/android/filemanager/o;Lcom/dropbox/android/filemanager/i;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    goto :goto_1e
.end method

.method public final a(Landroid/database/Cursor;Landroid/view/View;I)V
    .registers 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 123
    const/4 v0, -0x1

    if-ne p3, v0, :cond_b

    .line 124
    check-cast p2, Lcom/dropbox/android/widget/ThumbGridItemView;

    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    invoke-virtual {p2, p1, v0}, Lcom/dropbox/android/widget/ThumbGridItemView;->a(Landroid/database/Cursor;Lcom/dropbox/android/filemanager/i;)V

    .line 143
    :goto_a
    :pswitch_a
    return-void

    .line 126
    :cond_b
    invoke-static {}, Lcom/dropbox/android/widget/aG;->values()[Lcom/dropbox/android/widget/aG;

    move-result-object v0

    aget-object v0, v0, p3

    .line 127
    sget-object v1, Lcom/dropbox/android/widget/aE;->a:[I

    invoke-virtual {v0}, Lcom/dropbox/android/widget/aG;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_4c

    .line 140
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :pswitch_24
    const-string v0, "HEADER_LABEL"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 130
    const v0, 0x7f0600a5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/i;->a(I)V

    goto :goto_a

    .line 134
    :pswitch_44
    check-cast p2, Lcom/dropbox/android/widget/CameraUploadItemView;

    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->k:Lcom/dropbox/android/widget/aF;

    invoke-virtual {p2, p1, v0}, Lcom/dropbox/android/widget/CameraUploadItemView;->a(Landroid/database/Cursor;Lcom/dropbox/android/widget/h;)V

    goto :goto_a

    .line 127
    :pswitch_data_4c
    .packed-switch 0x1
        :pswitch_24
        :pswitch_44
        :pswitch_a
    .end packed-switch
.end method

.method public final a(Z)V
    .registers 3
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    if-eqz v0, :cond_9

    .line 76
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->b:Lcom/dropbox/android/filemanager/i;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/filemanager/i;->a(Z)V

    .line 78
    :cond_9
    return-void
.end method

.method public final b()I
    .registers 3

    .prologue
    .line 170
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 171
    invoke-virtual {p0}, Lcom/dropbox/android/widget/aD;->d()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public final b(Landroid/database/Cursor;)Z
    .registers 4
    .parameter

    .prologue
    .line 83
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/aD;->d(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_12

    invoke-static {p1}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/provider/P;->d:Lcom/dropbox/android/provider/P;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/provider/P;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_12
    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public final c(Landroid/database/Cursor;)I
    .registers 4
    .parameter

    .prologue
    .line 113
    sget-object v0, Lcom/dropbox/android/widget/aD;->a:Ljava/util/Map;

    invoke-static {p1}, Lcom/dropbox/android/provider/P;->a(Landroid/database/Cursor;)Lcom/dropbox/android/provider/P;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 90
    move-object v0, p3

    check-cast v0, Lcom/dropbox/android/widget/SweetListView;

    invoke-direct {p0, v0}, Lcom/dropbox/android/widget/aD;->a(Lcom/dropbox/android/widget/SweetListView;)V

    .line 92
    invoke-super {p0, p1, p2, p3}, Lcom/dropbox/android/widget/az;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/dropbox/android/widget/aD;->d:Landroid/database/Cursor;

    if-nez v0, :cond_6

    .line 199
    const/4 v0, 0x0

    .line 202
    :goto_5
    return v0

    :cond_6
    invoke-super {p0}, Lcom/dropbox/android/widget/az;->isEmpty()Z

    move-result v0

    goto :goto_5
.end method
