.class public final Lcom/dropbox/android/widget/al;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 46
    const-class v0, Lcom/dropbox/android/widget/al;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/al;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 232
    if-eqz p2, :cond_81

    .line 233
    invoke-virtual {p5}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v2

    if-nez v2, :cond_11

    invoke-virtual {p4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-nez v2, :cond_3f

    .line 234
    :cond_11
    invoke-static {p5}, Lcom/dropbox/android/widget/al;->a(Landroid/view/View;)V

    .line 235
    if-eqz p3, :cond_17

    move v0, v1

    :cond_17
    invoke-virtual {p7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 257
    :goto_1a
    if-eqz p1, :cond_88

    invoke-static {p1}, Lcom/dropbox/android/util/bd;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_88

    .line 258
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 259
    const v0, 0x106000c

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 265
    :goto_2d
    invoke-virtual {p4, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 266
    invoke-virtual {p4, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 267
    invoke-virtual {p5, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 268
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p6, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 269
    invoke-virtual {p6, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 270
    return-void

    .line 237
    :cond_3f
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200dd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .line 239
    const v2, 0x7f060100

    invoke-virtual {p4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/TransitionDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 240
    const v2, 0x7f060101

    invoke-virtual {v0, v2, p1}, Landroid/graphics/drawable/TransitionDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 241
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 242
    const/16 v2, 0x96

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 246
    if-nez p3, :cond_72

    invoke-virtual {p7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_72

    .line 247
    invoke-static {p7}, Lcom/dropbox/android/widget/al;->b(Landroid/view/View;)V

    :cond_70
    :goto_70
    move-object p1, v0

    .line 252
    goto :goto_1a

    .line 248
    :cond_72
    if-eqz p3, :cond_70

    invoke-virtual {p7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_70

    .line 249
    invoke-static {p7}, Lcom/dropbox/android/widget/al;->a(Landroid/view/View;)V

    .line 250
    invoke-virtual {p7, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_70

    .line 254
    :cond_81
    if-eqz p3, :cond_84

    move v0, v1

    :cond_84
    invoke-virtual {p7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1a

    .line 261
    :cond_88
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 262
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2d
.end method

.method public static a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 274
    .line 275
    if-nez p3, :cond_28

    .line 276
    const-string v0, "page_white"

    invoke-static {p0, v0}, Lcom/dropbox/android/util/ar;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 289
    :cond_8
    :goto_8
    if-eqz v0, :cond_11

    .line 290
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 291
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 294
    :cond_11
    if-eqz p2, :cond_27

    .line 295
    invoke-virtual {p2}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v1, v0

    .line 296
    invoke-virtual {p2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 297
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 299
    :cond_27
    return-void

    .line 278
    :cond_28
    invoke-static {p0, p3}, Lcom/dropbox/android/util/ar;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 279
    if-nez v0, :cond_8

    .line 280
    sget-object v0, Lcom/dropbox/android/widget/al;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to load media icon type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    if-eqz p3, :cond_57

    const-string v0, "folder"

    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_57

    .line 282
    const-string v0, "folder"

    invoke-static {p0, v0}, Lcom/dropbox/android/util/ar;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_8

    .line 284
    :cond_57
    const-string v0, "page_white"

    invoke-static {p0, v0}, Lcom/dropbox/android/util/ar;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_8
.end method

.method public static a(Landroid/content/Context;Ldbxyzptlk/k/f;ZLandroid/widget/ProgressBar;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0xff

    const/4 v2, 0x4

    .line 173
    if-eqz p1, :cond_7a

    .line 174
    if-eqz p2, :cond_29

    instance-of v0, p1, Ldbxyzptlk/k/j;

    if-eqz v0, :cond_29

    move-object v0, p1

    check-cast v0, Ldbxyzptlk/k/j;

    invoke-virtual {v0}, Ldbxyzptlk/k/j;->c()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_29

    .line 176
    check-cast p1, Ldbxyzptlk/k/j;

    invoke-virtual {p1}, Ldbxyzptlk/k/j;->c()F

    move-result v0

    .line 177
    float-to-int v0, v0

    invoke-virtual {p3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 178
    invoke-virtual {p3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 179
    invoke-virtual {p4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    :cond_28
    :goto_28
    return-void

    .line 181
    :cond_29
    invoke-virtual {p1, p0}, Ldbxyzptlk/k/f;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    invoke-virtual {p3, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 184
    invoke-virtual {p4, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    instance-of v0, p1, Ldbxyzptlk/k/k;

    if-eqz v0, :cond_6b

    move-object v0, p1

    check-cast v0, Ldbxyzptlk/k/k;

    invoke-virtual {v0}, Ldbxyzptlk/k/k;->e()Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->k:Lcom/dropbox/android/taskqueue/m;

    if-eq v0, v1, :cond_4f

    check-cast p1, Ldbxyzptlk/k/k;

    invoke-virtual {p1}, Ldbxyzptlk/k/k;->e()Lcom/dropbox/android/taskqueue/m;

    move-result-object v0

    sget-object v1, Lcom/dropbox/android/taskqueue/m;->l:Lcom/dropbox/android/taskqueue/m;

    if-ne v0, v1, :cond_6b

    .line 189
    :cond_4f
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 190
    if-eqz p5, :cond_28

    .line 191
    const/16 v0, 0xb3

    invoke-static {v0, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p5, v0, v1}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_28

    .line 195
    :cond_6b
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_28

    .line 199
    :cond_7a
    invoke-virtual {p3, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 200
    invoke-virtual {p4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_28
.end method

.method public static a(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 206
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f80

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 207
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 208
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 209
    return-void
.end method

.method public static b(Landroid/view/View;)V
    .registers 4
    .parameter

    .prologue
    .line 212
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f80

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 213
    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 214
    new-instance v1, Lcom/dropbox/android/widget/am;

    invoke-direct {v1, p0}, Lcom/dropbox/android/widget/am;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 226
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 227
    return-void
.end method
