.class public final Lcom/dropbox/android/widget/an;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/FrameLayout;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Lcom/dropbox/android/util/F;

.field private final g:Landroid/content/Context;

.field private final h:Landroid/os/Handler;

.field private final i:Lcom/dropbox/android/widget/ao;

.field private j:I

.field private k:Lcom/dropbox/android/util/E;

.field private l:Ljava/lang/String;

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/dropbox/android/util/F;Landroid/content/Context;Landroid/os/Handler;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/FrameLayout;Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Lcom/dropbox/android/widget/ao;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/ao;-><init>(Lcom/dropbox/android/widget/an;Lcom/dropbox/android/widget/am;)V

    iput-object v0, p0, Lcom/dropbox/android/widget/an;->i:Lcom/dropbox/android/widget/ao;

    .line 66
    iput v2, p0, Lcom/dropbox/android/widget/an;->j:I

    .line 67
    iput-object v1, p0, Lcom/dropbox/android/widget/an;->k:Lcom/dropbox/android/util/E;

    .line 68
    iput-object v1, p0, Lcom/dropbox/android/widget/an;->l:Ljava/lang/String;

    .line 69
    iput-boolean v2, p0, Lcom/dropbox/android/widget/an;->m:Z

    .line 75
    iput-object p1, p0, Lcom/dropbox/android/widget/an;->f:Lcom/dropbox/android/util/F;

    .line 76
    iput-object p2, p0, Lcom/dropbox/android/widget/an;->g:Landroid/content/Context;

    .line 77
    iput-object p3, p0, Lcom/dropbox/android/widget/an;->h:Landroid/os/Handler;

    .line 78
    iput-object p4, p0, Lcom/dropbox/android/widget/an;->a:Landroid/widget/ImageView;

    .line 79
    iput-object p5, p0, Lcom/dropbox/android/widget/an;->b:Landroid/widget/ImageView;

    .line 80
    iput-object p6, p0, Lcom/dropbox/android/widget/an;->c:Landroid/widget/FrameLayout;

    .line 81
    iput-object p7, p0, Lcom/dropbox/android/widget/an;->d:Landroid/widget/ImageView;

    .line 82
    iput-object p8, p0, Lcom/dropbox/android/widget/an;->e:Landroid/widget/ImageView;

    .line 83
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/an;)I
    .registers 2
    .parameter

    .prologue
    .line 54
    iget v0, p0, Lcom/dropbox/android/widget/an;->j:I

    return v0
.end method

.method static synthetic a(Lcom/dropbox/android/widget/an;Lcom/dropbox/android/util/E;)Lcom/dropbox/android/util/E;
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lcom/dropbox/android/widget/an;->k:Lcom/dropbox/android/util/E;

    return-object p1
.end method

.method private a()V
    .registers 4

    .prologue
    const/16 v2, 0x8

    .line 136
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 137
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->b:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 140
    return-void
.end method

.method static synthetic b(Lcom/dropbox/android/widget/an;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/dropbox/android/widget/an;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d(Lcom/dropbox/android/widget/an;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->g:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/dropbox/android/widget/an;)Z
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/dropbox/android/widget/an;->m:Z

    return v0
.end method

.method static synthetic f(Lcom/dropbox/android/widget/an;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->b:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/dropbox/android/widget/an;)Landroid/widget/FrameLayout;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->c:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic h(Lcom/dropbox/android/widget/an;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->a:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic i(Lcom/dropbox/android/widget/an;)Landroid/widget/ImageView;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic j(Lcom/dropbox/android/widget/an;)Lcom/dropbox/android/util/F;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->f:Lcom/dropbox/android/util/F;

    return-object v0
.end method

.method static synthetic k(Lcom/dropbox/android/widget/an;)Landroid/os/Handler;
    .registers 2
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->h:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 88
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->k:Lcom/dropbox/android/util/E;

    if-eqz v0, :cond_d

    .line 89
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->k:Lcom/dropbox/android/util/E;

    invoke-virtual {v0}, Lcom/dropbox/android/util/E;->b()V

    .line 90
    iput-object v3, p0, Lcom/dropbox/android/widget/an;->k:Lcom/dropbox/android/util/E;

    .line 93
    :cond_d
    if-nez p3, :cond_13

    .line 94
    invoke-static {p2}, Lcom/dropbox/android/util/ae;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 97
    :cond_13
    invoke-static {p3}, Lcom/dropbox/android/util/ae;->i(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dropbox/android/widget/an;->m:Z

    .line 99
    invoke-static {p3}, Lcom/dropbox/android/util/ae;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6e

    .line 100
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->f:Lcom/dropbox/android/util/F;

    invoke-virtual {v0, p1}, Lcom/dropbox/android/util/F;->a(Ljava/lang/String;)Lcom/dropbox/android/util/E;

    move-result-object v3

    .line 102
    if-eqz v3, :cond_54

    .line 103
    iput-object v3, p0, Lcom/dropbox/android/widget/an;->k:Lcom/dropbox/android/util/E;

    .line 104
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 105
    iget-object v0, p0, Lcom/dropbox/android/widget/an;->g:Landroid/content/Context;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/dropbox/android/widget/an;->g:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v3}, Lcom/dropbox/android/util/E;->c()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v1, v4, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-boolean v3, p0, Lcom/dropbox/android/widget/an;->m:Z

    iget-object v4, p0, Lcom/dropbox/android/widget/an;->b:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/dropbox/android/widget/an;->c:Landroid/widget/FrameLayout;

    iget-object v6, p0, Lcom/dropbox/android/widget/an;->a:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/dropbox/android/widget/an;->d:Landroid/widget/ImageView;

    invoke-static/range {v0 .. v7}, Lcom/dropbox/android/widget/al;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;ZZLandroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    .line 133
    :goto_53
    return-void

    .line 114
    :cond_54
    invoke-direct {p0}, Lcom/dropbox/android/widget/an;->a()V

    .line 115
    iput-object p1, p0, Lcom/dropbox/android/widget/an;->l:Ljava/lang/String;

    .line 116
    iget v0, p0, Lcom/dropbox/android/widget/an;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/an;->j:I

    .line 117
    invoke-static {}, Lcom/dropbox/android/filemanager/I;->a()Lcom/dropbox/android/filemanager/I;

    move-result-object v0

    .line 118
    iget-object v1, p0, Lcom/dropbox/android/widget/an;->l:Ljava/lang/String;

    iget v2, p0, Lcom/dropbox/android/widget/an;->j:I

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/dropbox/android/widget/an;->i:Lcom/dropbox/android/widget/ao;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/dropbox/android/filemanager/I;->b(Ljava/lang/String;IILcom/dropbox/android/filemanager/R;)Lcom/dropbox/android/filemanager/S;

    goto :goto_53

    .line 123
    :cond_6e
    invoke-static {p2}, Lcom/dropbox/android/util/ae;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    if-nez v0, :cond_80

    if-eqz p3, :cond_80

    .line 125
    invoke-static {p3}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 126
    if-eqz v1, :cond_80

    .line 127
    invoke-static {v1}, Lcom/dropbox/android/util/ae;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 130
    :cond_80
    invoke-direct {p0}, Lcom/dropbox/android/widget/an;->a()V

    .line 131
    iget-object v1, p0, Lcom/dropbox/android/widget/an;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/dropbox/android/widget/an;->e:Landroid/widget/ImageView;

    invoke-static {v1, v2, v3, v0}, Lcom/dropbox/android/widget/al;->a(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_53
.end method
