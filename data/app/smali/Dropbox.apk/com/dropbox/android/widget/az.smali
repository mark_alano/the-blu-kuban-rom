.class public abstract Lcom/dropbox/android/widget/az;
.super Landroid/widget/BaseAdapter;
.source "panda.py"

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field protected final c:Landroid/content/Context;

.field protected d:Landroid/database/Cursor;

.field protected e:Ljava/util/ArrayList;

.field protected f:Landroid/widget/AdapterView$OnItemClickListener;

.field protected g:Landroid/widget/AdapterView$OnItemLongClickListener;

.field protected h:I

.field i:Landroid/database/DataSetObservable;

.field j:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/dropbox/android/widget/az;->h:I

    .line 60
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/az;->i:Landroid/database/DataSetObservable;

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    .line 23
    iput-object p1, p0, Lcom/dropbox/android/widget/az;->c:Landroid/content/Context;

    .line 24
    return-void
.end method

.method private a(I)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 280
    if-nez p1, :cond_2f

    move v0, v1

    .line 281
    :goto_5
    if-nez v0, :cond_2e

    .line 282
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 283
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 284
    iget-object v4, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 285
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/az;->d(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_31

    move v0, v1

    .line 286
    :goto_29
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 288
    :cond_2e
    return v0

    :cond_2f
    move v0, v2

    .line 280
    goto :goto_5

    :cond_31
    move v0, v2

    .line 285
    goto :goto_29
.end method

.method private e()V
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    if-nez v0, :cond_d

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    .line 127
    :goto_c
    return-void

    .line 104
    :cond_d
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    .line 107
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move v0, v1

    .line 108
    :goto_20
    iget-object v2, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_5f

    .line 109
    iget-object v2, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/az;->d(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_46

    .line 112
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 121
    :goto_40
    iget-object v2, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_20

    .line 114
    :cond_46
    iget v2, p0, Lcom/dropbox/android/widget/az;->h:I

    rem-int v2, v0, v2

    if-nez v2, :cond_5c

    .line 116
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 118
    :cond_5c
    add-int/lit8 v0, v0, 0x1

    goto :goto_40

    .line 125
    :cond_5f
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    goto :goto_c
.end method

.method private f(I)Z
    .registers 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 292
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_37

    move v0, v1

    .line 293
    :goto_d
    if-nez v0, :cond_36

    .line 294
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 295
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    add-int/lit8 v4, p1, 0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 296
    iget-object v4, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 297
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/az;->d(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_39

    move v0, v1

    .line 298
    :goto_31
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 300
    :cond_36
    return v0

    :cond_37
    move v0, v2

    .line 292
    goto :goto_d

    :cond_39
    move v0, v2

    .line 297
    goto :goto_31
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Landroid/view/ViewGroup;I)Landroid/view/View;
.end method

.method public final a(ILandroid/view/View;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 272
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 273
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 274
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v2}, Lcom/dropbox/android/widget/az;->c(Landroid/database/Cursor;)I

    move-result v2

    invoke-virtual {p0, v1, p2, v2}, Lcom/dropbox/android/widget/az;->a(Landroid/database/Cursor;Landroid/view/View;I)V

    .line 275
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 276
    return-void
.end method

.method public a(Landroid/database/Cursor;)V
    .registers 4
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    if-ne p1, v0, :cond_5

    .line 86
    :cond_4
    :goto_4
    return-void

    .line 69
    :cond_5
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_14

    .line 70
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_14

    .line 71
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 74
    :cond_14
    iput-object p1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    .line 75
    invoke-direct {p0}, Lcom/dropbox/android/widget/az;->e()V

    .line 77
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_4

    .line 78
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_2e

    .line 79
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 80
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    goto :goto_4

    .line 82
    :cond_2e
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onInvalidated()V

    goto :goto_4
.end method

.method public abstract a(Landroid/database/Cursor;Landroid/view/View;I)V
.end method

.method protected final a(Landroid/view/View;Landroid/view/View;Landroid/database/Cursor;)V
    .registers 11
    .parameter
    .parameter
    .parameter

    .prologue
    .line 247
    .line 248
    invoke-virtual {p0, p3}, Lcom/dropbox/android/widget/az;->b(Landroid/database/Cursor;)Z

    move-result v2

    .line 249
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    .line 250
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    const-string v1, "_id"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 251
    new-instance v0, Lcom/dropbox/android/widget/aA;

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/widget/aA;-><init>(Lcom/dropbox/android/widget/az;ZLandroid/view/View;IJ)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    new-instance v0, Lcom/dropbox/android/widget/aB;

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v6}, Lcom/dropbox/android/widget/aB;-><init>(Lcom/dropbox/android/widget/az;ZLandroid/view/View;IJ)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 269
    return-void
.end method

.method public final a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 213
    iput-object p1, p0, Lcom/dropbox/android/widget/az;->f:Landroid/widget/AdapterView$OnItemClickListener;

    .line 214
    return-void
.end method

.method public final a(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .registers 2
    .parameter

    .prologue
    .line 217
    iput-object p1, p0, Lcom/dropbox/android/widget/az;->g:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 218
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .registers 2

    .prologue
    .line 196
    const/4 v0, 0x1

    return v0
.end method

.method public abstract b()I
.end method

.method protected final b(I)Z
    .registers 3
    .parameter

    .prologue
    .line 97
    const/4 v0, -0x1

    if-ne p1, v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public b(Landroid/database/Cursor;)Z
    .registers 3
    .parameter

    .prologue
    .line 209
    const/4 v0, 0x1

    return v0
.end method

.method public abstract c(Landroid/database/Cursor;)I
.end method

.method public final c()Landroid/database/Cursor;
    .registers 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    return-object v0
.end method

.method public final c(I)V
    .registers 7
    .parameter

    .prologue
    .line 419
    const/4 v0, 0x1

    int-to-double v1, p1

    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->b()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 420
    iget v1, p0, Lcom/dropbox/android/widget/az;->h:I

    if-eq v1, v0, :cond_1f

    .line 421
    iput v0, p0, Lcom/dropbox/android/widget/az;->h:I

    .line 422
    invoke-direct {p0}, Lcom/dropbox/android/widget/az;->e()V

    .line 423
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->i:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 425
    :cond_1f
    return-void
.end method

.method public final d()I
    .registers 4

    .prologue
    .line 415
    const/4 v0, 0x2

    const/high16 v1, 0x3fc0

    iget-object v2, p0, Lcom/dropbox/android/widget/az;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final d(I)I
    .registers 3
    .parameter

    .prologue
    .line 431
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_3
    return v0

    :cond_4
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3
.end method

.method protected final d(Landroid/database/Cursor;)Z
    .registers 3
    .parameter

    .prologue
    .line 93
    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/az;->c(Landroid/database/Cursor;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/az;->b(I)Z

    move-result v0

    return v0
.end method

.method public final e(I)I
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 437
    move v1, v0

    move v2, v0

    .line 438
    :goto_3
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_19

    .line 439
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v0, p1, :cond_1a

    .line 445
    :cond_19
    return v2

    .line 438
    :cond_1a
    add-int/lit8 v0, v1, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3
.end method

.method public getCount()I
    .registers 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 4
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_18

    .line 141
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 142
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    .line 144
    :goto_17
    return-object v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_17
.end method

.method public getItemId(I)J
    .registers 5
    .parameter

    .prologue
    .line 154
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_24

    .line 155
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 156
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 158
    :goto_23
    return-wide v0

    :cond_24
    const-wide/16 v0, 0x0

    goto :goto_23
.end method

.method public final getItemViewType(I)I
    .registers 5
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 228
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_24

    .line 229
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 230
    iget-object v2, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 232
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/az;->c(Landroid/database/Cursor;)I

    move-result v0

    .line 233
    if-eq v0, v1, :cond_1f

    .line 235
    :goto_1e
    return v0

    .line 233
    :cond_1f
    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->a()I

    move-result v0

    goto :goto_1e

    :cond_24
    move v0, v1

    .line 235
    goto :goto_1e
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 306
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 307
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 309
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/az;->c(Landroid/database/Cursor;)I

    move-result v4

    .line 310
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/az;->d(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_52

    .line 313
    if-eqz p2, :cond_47

    move-object v1, p2

    .line 321
    :goto_23
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    move-object v0, v1

    .line 322
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v5, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v1, v0, v5}, Lcom/dropbox/android/widget/az;->a(Landroid/view/View;Landroid/view/View;Landroid/database/Cursor;)V

    .line 323
    iget-object v5, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    move-object v0, v1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v5, v0, v4}, Lcom/dropbox/android/widget/az;->a(Landroid/database/Cursor;Landroid/view/View;I)V

    .line 324
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 386
    :goto_46
    return-object v1

    .line 316
    :cond_47
    new-instance v1, Lcom/dropbox/android/widget/ViewButton;

    iget-object v0, p0, Lcom/dropbox/android/widget/az;->c:Landroid/content/Context;

    invoke-direct {v1, v0, v2}, Lcom/dropbox/android/widget/ViewButton;-><init>(Landroid/content/Context;Z)V

    .line 317
    invoke-virtual {p0, v1, v4}, Lcom/dropbox/android/widget/az;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    goto :goto_23

    .line 330
    :cond_52
    if-eqz p2, :cond_c3

    move-object v0, p2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    iget v1, p0, Lcom/dropbox/android/widget/az;->h:I

    if-ne v0, v1, :cond_c3

    move-object v0, p2

    .line 354
    :goto_60
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/az;->a(I)Z

    move-result v1

    .line 355
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/az;->f(I)Z

    move-result v3

    .line 358
    check-cast v0, Landroid/view/ViewGroup;

    .line 360
    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->d()I

    move-result v5

    if-eqz v1, :cond_109

    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->d()I

    move-result v1

    :goto_74
    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->d()I

    move-result v6

    if-eqz v3, :cond_10c

    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->d()I

    move-result v3

    :goto_7e
    invoke-virtual {v0, v5, v1, v6, v3}, Landroid/view/ViewGroup;->setPadding(IIII)V

    move v3, v2

    .line 366
    :goto_82
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_10f

    iget v1, p0, Lcom/dropbox/android/widget/az;->h:I

    if-ge v3, v1, :cond_10f

    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v1}, Lcom/dropbox/android/widget/az;->d(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_10f

    .line 367
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    .line 368
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    check-cast v1, Landroid/view/ViewGroup;

    .line 369
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 370
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 371
    iget-object v7, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v7, v6, v4}, Lcom/dropbox/android/widget/az;->a(Landroid/database/Cursor;Landroid/view/View;I)V

    .line 372
    iget-object v7, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-virtual {p0, v1, v6, v7}, Lcom/dropbox/android/widget/az;->a(Landroid/view/View;Landroid/view/View;Landroid/database/Cursor;)V

    .line 373
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v1, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 374
    iget-object v1, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 375
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    .line 376
    goto :goto_82

    .line 336
    :cond_c3
    new-instance p2, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/dropbox/android/widget/az;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 337
    const/high16 v0, -0x4080

    invoke-virtual {p2, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    .line 338
    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    move v0, v2

    .line 340
    :goto_d3
    iget v1, p0, Lcom/dropbox/android/widget/az;->h:I

    if-ge v0, v1, :cond_106

    .line 341
    new-instance v1, Lcom/dropbox/android/widget/ViewButton;

    iget-object v3, p0, Lcom/dropbox/android/widget/az;->c:Landroid/content/Context;

    invoke-direct {v1, v3, v2}, Lcom/dropbox/android/widget/ViewButton;-><init>(Landroid/content/Context;Z)V

    .line 342
    invoke-virtual {p0, v1, v4}, Lcom/dropbox/android/widget/az;->a(Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 343
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    const/high16 v7, 0x3f80

    invoke-direct {v3, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 344
    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->d()I

    move-result v5

    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->d()I

    move-result v6

    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->d()I

    move-result v7

    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->d()I

    move-result v8

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 345
    invoke-virtual {v1, v3}, Lcom/dropbox/android/widget/ViewButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 346
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 340
    add-int/lit8 v0, v0, 0x1

    goto :goto_d3

    :cond_106
    move-object v0, p2

    .line 348
    goto/16 :goto_60

    :cond_109
    move v1, v2

    .line 360
    goto/16 :goto_74

    :cond_10c
    move v3, v2

    goto/16 :goto_7e

    .line 380
    :cond_10f
    :goto_10f
    iget v1, p0, Lcom/dropbox/android/widget/az;->h:I

    if-ge v3, v1, :cond_126

    .line 381
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    check-cast v1, Landroid/view/ViewGroup;

    .line 382
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 383
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 384
    add-int/lit8 v3, v3, 0x1

    .line 385
    goto :goto_10f

    :cond_126
    move-object v1, v0

    .line 386
    goto/16 :goto_46
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/dropbox/android/widget/az;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .registers 2

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .registers 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final isEnabled(I)Z
    .registers 3
    .parameter

    .prologue
    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 174
    iput-object p1, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    .line 175
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->i:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 176
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_10

    .line 177
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 179
    :cond_10
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .registers 3
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_d

    .line 186
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->d:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 188
    :cond_d
    iget-object v0, p0, Lcom/dropbox/android/widget/az;->i:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dropbox/android/widget/az;->j:Landroid/database/DataSetObserver;

    .line 190
    return-void
.end method
