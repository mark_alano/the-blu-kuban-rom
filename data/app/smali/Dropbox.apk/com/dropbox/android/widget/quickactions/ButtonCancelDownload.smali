.class public Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field protected final e:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 2
    .parameter

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 17
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 28
    const v0, 0x7f03004f

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .registers 6
    .parameter

    .prologue
    .line 21
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/x;->i()Lcom/dropbox/android/taskqueue/p;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/dropbox/android/taskqueue/DownloadTask;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/dropbox/android/widget/quickactions/ButtonCancelDownload;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-direct {v1, v2, v3}, Lcom/dropbox/android/taskqueue/DownloadTask;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 23
    invoke-virtual {v1}, Lcom/dropbox/android/taskqueue/DownloadTask;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/taskqueue/p;->b(Ljava/lang/String;)Z

    .line 24
    return-void
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 33
    const v0, 0x7f0b0175

    return v0
.end method
