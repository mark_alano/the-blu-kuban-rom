.class public Lcom/dropbox/android/widget/quickactions/ButtonDelete;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field private final e:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 2
    .parameter

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 22
    return-void
.end method

.method static synthetic a(Lcom/dropbox/android/widget/quickactions/ButtonDelete;)Lcom/dropbox/android/filemanager/LocalEntry;
    .registers 2
    .parameter

    .prologue
    .line 17
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    return-object v0
.end method

.method private b(Landroid/support/v4/app/Fragment;)V
    .registers 6
    .parameter

    .prologue
    .line 41
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 42
    new-instance v0, Lcom/dropbox/android/widget/quickactions/c;

    invoke-direct {v0, p0, v1}, Lcom/dropbox/android/widget/quickactions/c;-><init>(Lcom/dropbox/android/widget/quickactions/ButtonDelete;Landroid/content/Context;)V

    .line 60
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 61
    const v3, 0x7f0b0095

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 62
    const v0, 0x7f0b0018

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 64
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-boolean v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->k:Z

    if-eqz v0, :cond_36

    const v0, 0x7f0b0098

    .line 65
    :goto_24
    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 67
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->b:Ljava/lang/String;

    .line 68
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 70
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 71
    return-void

    .line 64
    :cond_36
    const v0, 0x7f0b0094

    goto :goto_24
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 27
    const v0, 0x7f030051

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .registers 2
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/dropbox/android/widget/quickactions/ButtonDelete;->b(Landroid/support/v4/app/Fragment;)V

    .line 38
    return-void
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 32
    const v0, 0x7f0b0172

    return v0
.end method
