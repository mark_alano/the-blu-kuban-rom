.class public Lcom/dropbox/android/widget/quickactions/ButtonRename;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field private final e:Lcom/dropbox/android/filemanager/LocalEntry;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/filemanager/LocalEntry;)V
    .registers 2
    .parameter

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/ButtonRename;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 20
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 24
    const v0, 0x7f030055

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/ButtonRename;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;

    move-result-object v0

    .line 30
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 31
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/RenameFolderDialogFrag;->a(Landroid/support/v4/app/j;)V

    .line 32
    return-void
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 36
    const v0, 0x7f0b0171

    return v0
.end method
