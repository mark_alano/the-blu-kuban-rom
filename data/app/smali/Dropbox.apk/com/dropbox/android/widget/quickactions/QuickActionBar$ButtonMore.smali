.class public Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;
.super Lcom/dropbox/android/widget/quickactions/a;
.source "panda.py"


# instance fields
.field protected final e:[Lcom/dropbox/android/widget/quickactions/a;

.field protected final f:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

.field final synthetic g:Lcom/dropbox/android/widget/quickactions/QuickActionBar;


# direct methods
.method public constructor <init>(Lcom/dropbox/android/widget/quickactions/QuickActionBar;[Lcom/dropbox/android/widget/quickactions/a;Lcom/dropbox/android/widget/quickactions/QuickActionBar;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 189
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->g:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-direct {p0}, Lcom/dropbox/android/widget/quickactions/a;-><init>()V

    .line 190
    iput-object p2, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->e:[Lcom/dropbox/android/widget/quickactions/a;

    .line 191
    iput-object p3, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->f:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    .line 192
    return-void
.end method


# virtual methods
.method public final a()I
    .registers 2

    .prologue
    .line 215
    const v0, 0x7f030054

    return v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->f:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    iget-object v1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->e:[Lcom/dropbox/android/widget/quickactions/a;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a([Lcom/dropbox/android/widget/quickactions/a;)V

    .line 206
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->g:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-static {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 209
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->g:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-static {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->b(Lcom/dropbox/android/widget/quickactions/QuickActionBar;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->g:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-static {v1}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->a(Lcom/dropbox/android/widget/quickactions/QuickActionBar;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->openContextMenu(Landroid/view/View;)V

    .line 211
    :cond_22
    return-void
.end method

.method public final b()I
    .registers 2

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 196
    invoke-static {}, Lcom/dropbox/android/util/i;->d()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "which"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 199
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->a(Landroid/support/v4/app/Fragment;)V

    .line 200
    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar$ButtonMore;->d()V

    .line 201
    return-void
.end method
