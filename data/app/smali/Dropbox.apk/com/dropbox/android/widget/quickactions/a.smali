.class public abstract Lcom/dropbox/android/widget/quickactions/a;
.super Ljava/lang/Object;
.source "panda.py"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field protected static final a:Ljava/lang/String;


# instance fields
.field protected b:Landroid/view/View;

.field protected c:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

.field protected d:Landroid/support/v4/app/Fragment;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 11
    const-class v0, Lcom/dropbox/android/widget/quickactions/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dropbox/android/widget/quickactions/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Landroid/support/v4/app/Fragment;)V
.end method

.method public final a(Landroid/support/v4/app/Fragment;Lcom/dropbox/android/widget/quickactions/QuickActionBar;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/a;->b:Landroid/view/View;

    if-eqz v0, :cond_c

    .line 36
    sget-object v0, Lcom/dropbox/android/widget/quickactions/a;->a:Ljava/lang/String;

    const-string v1, "Trying to inflate an already-inflated item"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :goto_b
    return-void

    .line 40
    :cond_c
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/a;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/dropbox/android/widget/quickactions/a;->b:Landroid/view/View;

    .line 41
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/a;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    iput-object p1, p0, Lcom/dropbox/android/widget/quickactions/a;->d:Landroid/support/v4/app/Fragment;

    .line 43
    iput-object p2, p0, Lcom/dropbox/android/widget/quickactions/a;->c:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    .line 45
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/a;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/quickactions/a;->a(Landroid/view/View;)V

    goto :goto_b
.end method

.method protected a(Landroid/view/View;)V
    .registers 2
    .parameter

    .prologue
    .line 48
    return-void
.end method

.method public abstract b()I
.end method

.method protected final c()Landroid/view/View;
    .registers 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/a;->b:Landroid/view/View;

    return-object v0
.end method

.method protected final d()V
    .registers 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/a;->c:Lcom/dropbox/android/widget/quickactions/QuickActionBar;

    invoke-virtual {v0}, Lcom/dropbox/android/widget/quickactions/QuickActionBar;->c()V

    .line 71
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/dropbox/android/widget/quickactions/a;->d()V

    .line 65
    invoke-static {}, Lcom/dropbox/android/util/i;->d()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "which"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 66
    iget-object v0, p0, Lcom/dropbox/android/widget/quickactions/a;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {p0, v0}, Lcom/dropbox/android/widget/quickactions/a;->a(Landroid/support/v4/app/Fragment;)V

    .line 67
    return-void
.end method
