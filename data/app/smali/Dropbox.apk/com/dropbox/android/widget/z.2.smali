.class final Lcom/dropbox/android/widget/z;
.super Landroid/os/AsyncTask;
.source "panda.py"


# static fields
.field private static a:Z

.field private static b:J


# instance fields
.field private final c:Ljava/lang/String;

.field private d:Lcom/dropbox/android/widget/DbxMediaController;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 918
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/widget/z;->a:Z

    .line 919
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/dropbox/android/widget/z;->b:J

    return-void
.end method

.method constructor <init>(Lcom/dropbox/android/widget/DbxMediaController;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 925
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 926
    iput-object p2, p0, Lcom/dropbox/android/widget/z;->c:Ljava/lang/String;

    .line 927
    iput-object p1, p0, Lcom/dropbox/android/widget/z;->d:Lcom/dropbox/android/widget/DbxMediaController;

    .line 928
    return-void
.end method

.method public static a(Lcom/dropbox/android/widget/DbxMediaController;Ljava/lang/String;)V
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 961
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 962
    sget-boolean v2, Lcom/dropbox/android/widget/z;->a:Z

    if-nez v2, :cond_22

    sget-wide v2, Lcom/dropbox/android/widget/z;->b:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_22

    .line 963
    new-instance v2, Lcom/dropbox/android/widget/z;

    invoke-direct {v2, p0, p1}, Lcom/dropbox/android/widget/z;-><init>(Lcom/dropbox/android/widget/DbxMediaController;Ljava/lang/String;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/dropbox/android/widget/z;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 964
    const/4 v2, 0x1

    sput-boolean v2, Lcom/dropbox/android/widget/z;->a:Z

    .line 965
    sput-wide v0, Lcom/dropbox/android/widget/z;->b:J

    .line 968
    :cond_22
    return-void
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Void;)Ljava/lang/Integer;
    .registers 5
    .parameter

    .prologue
    .line 933
    :try_start_0
    invoke-static {}, Lcom/dropbox/android/widget/DbxMediaController;->g()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fetching progress: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dropbox/android/widget/z;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    iget-object v0, v0, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    iget-object v1, p0, Lcom/dropbox/android/widget/z;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/r/i;->e(Ljava/lang/String;)I

    move-result v0

    .line 935
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3a

    .line 936
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_2e
    .catch Ldbxyzptlk/o/a; {:try_start_0 .. :try_end_2e} :catch_30

    move-result-object v0

    .line 941
    :goto_2f
    return-object v0

    .line 938
    :catch_30
    move-exception v0

    .line 939
    invoke-static {}, Lcom/dropbox/android/widget/DbxMediaController;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "TranscodeProgressAsyncTask"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 941
    :cond_3a
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method protected final a(Ljava/lang/Integer;)V
    .registers 6
    .parameter

    .prologue
    .line 946
    if-eqz p1, :cond_27

    .line 947
    iget-object v0, p0, Lcom/dropbox/android/widget/z;->d:Lcom/dropbox/android/widget/DbxMediaController;

    .line 948
    if-eqz v0, :cond_27

    .line 949
    invoke-static {}, Lcom/dropbox/android/widget/DbxMediaController;->g()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Got progress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 950
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/dropbox/android/widget/DbxMediaController;->a(Lcom/dropbox/android/widget/DbxMediaController;I)V

    .line 953
    :cond_27
    const/4 v0, 0x0

    sput-boolean v0, Lcom/dropbox/android/widget/z;->a:Z

    .line 954
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter

    .prologue
    .line 916
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/z;->a([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 2
    .parameter

    .prologue
    .line 916
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/dropbox/android/widget/z;->a(Ljava/lang/Integer;)V

    return-void
.end method
