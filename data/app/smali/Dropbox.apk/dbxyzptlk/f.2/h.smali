.class public final Ldbxyzptlk/f/h;
.super Ldbxyzptlk/f/i;
.source "panda.py"


# instance fields
.field private b:I

.field private c:J

.field private d:D

.field private e:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 94
    invoke-direct {p0}, Ldbxyzptlk/f/i;-><init>()V

    .line 96
    :try_start_3
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 97
    iput-wide v0, p0, Ldbxyzptlk/f/h;->c:J

    long-to-double v0, v0

    iput-wide v0, p0, Ldbxyzptlk/f/h;->d:D

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Ldbxyzptlk/f/h;->b:I
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_f} :catch_10

    .line 114
    :goto_f
    return-void

    .line 99
    :catch_10
    move-exception v0

    .line 101
    :try_start_11
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 102
    iput-wide v0, p0, Ldbxyzptlk/f/h;->d:D

    double-to-long v0, v0

    iput-wide v0, p0, Ldbxyzptlk/f/h;->c:J

    .line 103
    const/4 v0, 0x1

    iput v0, p0, Ldbxyzptlk/f/h;->b:I
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_1d} :catch_1e

    goto :goto_f

    .line 104
    :catch_1e
    move-exception v0

    .line 106
    :try_start_1f
    invoke-static {p1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/f/h;->e:Z

    .line 107
    const/4 v0, 0x2

    iput v0, p0, Ldbxyzptlk/f/h;->b:I

    .line 108
    iget-boolean v0, p0, Ldbxyzptlk/f/h;->e:Z

    if-eqz v0, :cond_3d

    const-wide/16 v0, 0x1

    :goto_2e
    iput-wide v0, p0, Ldbxyzptlk/f/h;->c:J

    long-to-double v0, v0

    iput-wide v0, p0, Ldbxyzptlk/f/h;->d:D
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_33} :catch_34

    goto :goto_f

    .line 109
    :catch_34
    move-exception v0

    .line 110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given text neither represents a double, int nor boolean value."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_3d
    const-wide/16 v0, 0x0

    goto :goto_2e
.end method

.method public constructor <init>(Z)V
    .registers 4
    .parameter

    .prologue
    .line 138
    invoke-direct {p0}, Ldbxyzptlk/f/i;-><init>()V

    .line 139
    iput-boolean p1, p0, Ldbxyzptlk/f/h;->e:Z

    .line 140
    if-eqz p1, :cond_12

    const-wide/16 v0, 0x1

    :goto_9
    iput-wide v0, p0, Ldbxyzptlk/f/h;->c:J

    long-to-double v0, v0

    iput-wide v0, p0, Ldbxyzptlk/f/h;->d:D

    .line 141
    const/4 v0, 0x2

    iput v0, p0, Ldbxyzptlk/f/h;->b:I

    .line 142
    return-void

    .line 140
    :cond_12
    const-wide/16 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>([BI)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Ldbxyzptlk/f/i;-><init>()V

    .line 69
    packed-switch p2, :pswitch_data_26

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Type argument is not valid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :pswitch_e
    invoke-static {p1}, Ldbxyzptlk/f/c;->c([B)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/f/h;->c:J

    long-to-double v0, v0

    iput-wide v0, p0, Ldbxyzptlk/f/h;->d:D

    .line 83
    :goto_17
    iput p2, p0, Ldbxyzptlk/f/h;->b:I

    .line 84
    return-void

    .line 75
    :pswitch_1a
    invoke-static {p1}, Ldbxyzptlk/f/c;->d([B)D

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/f/h;->d:D

    .line 76
    iget-wide v0, p0, Ldbxyzptlk/f/h;->d:D

    double-to-long v0, v0

    iput-wide v0, p0, Ldbxyzptlk/f/h;->c:J

    goto :goto_17

    .line 69
    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_e
        :pswitch_1a
    .end packed-switch
.end method


# virtual methods
.method public final a()Z
    .registers 5

    .prologue
    .line 160
    iget v0, p0, Ldbxyzptlk/f/h;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 161
    iget-boolean v0, p0, Ldbxyzptlk/f/h;->e:Z

    .line 163
    :goto_7
    return v0

    :cond_8
    iget-wide v0, p0, Ldbxyzptlk/f/h;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    goto :goto_7

    :cond_12
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final b()J
    .registers 3

    .prologue
    .line 171
    iget-wide v0, p0, Ldbxyzptlk/f/h;->c:J

    return-wide v0
.end method

.method public final c()D
    .registers 3

    .prologue
    .line 190
    iget-wide v0, p0, Ldbxyzptlk/f/h;->d:D

    return-wide v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 209
    instance-of v1, p1, Ldbxyzptlk/f/h;

    if-nez v1, :cond_6

    .line 211
    :cond_5
    :goto_5
    return v0

    .line 210
    :cond_6
    check-cast p1, Ldbxyzptlk/f/h;

    .line 211
    iget v1, p0, Ldbxyzptlk/f/h;->b:I

    iget v2, p1, Ldbxyzptlk/f/h;->b:I

    if-ne v1, v2, :cond_5

    iget-wide v1, p0, Ldbxyzptlk/f/h;->c:J

    iget-wide v3, p1, Ldbxyzptlk/f/h;->c:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_5

    iget-wide v1, p0, Ldbxyzptlk/f/h;->d:D

    iget-wide v3, p1, Ldbxyzptlk/f/h;->d:D

    cmpl-double v1, v1, v3

    if-nez v1, :cond_5

    iget-boolean v1, p0, Ldbxyzptlk/f/h;->e:Z

    iget-boolean v2, p1, Ldbxyzptlk/f/h;->e:Z

    if-ne v1, v2, :cond_5

    const/4 v0, 0x1

    goto :goto_5
.end method

.method public final hashCode()I
    .registers 7

    .prologue
    const/16 v5, 0x20

    .line 216
    iget v0, p0, Ldbxyzptlk/f/h;->b:I

    .line 217
    mul-int/lit8 v0, v0, 0x25

    iget-wide v1, p0, Ldbxyzptlk/f/h;->c:J

    iget-wide v3, p0, Ldbxyzptlk/f/h;->c:J

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 218
    mul-int/lit8 v0, v0, 0x25

    iget-wide v1, p0, Ldbxyzptlk/f/h;->d:D

    invoke-static {v1, v2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v1

    iget-wide v3, p0, Ldbxyzptlk/f/h;->d:D

    invoke-static {v3, v4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v3

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    .line 219
    mul-int/lit8 v1, v0, 0x25

    invoke-virtual {p0}, Ldbxyzptlk/f/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2b

    const/4 v0, 0x1

    :goto_29
    add-int/2addr v0, v1

    .line 220
    return v0

    .line 219
    :cond_2b
    const/4 v0, 0x0

    goto :goto_29
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 226
    iget v0, p0, Ldbxyzptlk/f/h;->b:I

    packed-switch v0, :pswitch_data_26

    .line 237
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_9
    return-object v0

    .line 228
    :pswitch_a
    invoke-virtual {p0}, Ldbxyzptlk/f/h;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 231
    :pswitch_13
    invoke-virtual {p0}, Ldbxyzptlk/f/h;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 234
    :pswitch_1c
    invoke-virtual {p0}, Ldbxyzptlk/f/h;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_9

    .line 226
    nop

    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_a
        :pswitch_13
        :pswitch_1c
    .end packed-switch
.end method
