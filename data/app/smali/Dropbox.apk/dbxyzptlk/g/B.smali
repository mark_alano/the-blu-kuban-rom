.class public final Ldbxyzptlk/g/B;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ldbxyzptlk/g/B;


# instance fields
.field private c:Z

.field private d:Z

.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private volatile g:Ldbxyzptlk/g/F;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 36
    const-class v0, Ldbxyzptlk/g/B;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/g/B;->a:Ljava/lang/String;

    .line 46
    const/4 v0, 0x0

    sput-object v0, Ldbxyzptlk/g/B;->b:Ldbxyzptlk/g/B;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-boolean v0, p0, Ldbxyzptlk/g/B;->c:Z

    .line 56
    iput-boolean v0, p0, Ldbxyzptlk/g/B;->d:Z

    .line 58
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/g/B;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 59
    sget-object v0, Ldbxyzptlk/g/F;->b:Ldbxyzptlk/g/F;

    iput-object v0, p0, Ldbxyzptlk/g/B;->g:Ldbxyzptlk/g/F;

    .line 62
    iput-object p1, p0, Ldbxyzptlk/g/B;->e:Landroid/content/Context;

    .line 63
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.dropbox.android.taskqueue.CameraUploadTask.ACTION_CAMERA_UPLOAD_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 64
    iget-object v1, p0, Ldbxyzptlk/g/B;->e:Landroid/content/Context;

    invoke-static {v1}, Ldbxyzptlk/a/g;->a(Landroid/content/Context;)Ldbxyzptlk/a/g;

    move-result-object v1

    new-instance v2, Ldbxyzptlk/g/C;

    invoke-direct {v2, p0}, Ldbxyzptlk/g/C;-><init>(Ldbxyzptlk/g/B;)V

    invoke-virtual {v1, v2, v0}, Ldbxyzptlk/a/g;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 72
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Ldbxyzptlk/g/B;
    .registers 3
    .parameter

    .prologue
    .line 49
    const-class v1, Ldbxyzptlk/g/B;

    monitor-enter v1

    :try_start_3
    sget-object v0, Ldbxyzptlk/g/B;->b:Ldbxyzptlk/g/B;

    if-nez v0, :cond_e

    .line 50
    new-instance v0, Ldbxyzptlk/g/B;

    invoke-direct {v0, p0}, Ldbxyzptlk/g/B;-><init>(Landroid/content/Context;)V

    sput-object v0, Ldbxyzptlk/g/B;->b:Ldbxyzptlk/g/B;

    .line 52
    :cond_e
    sget-object v0, Ldbxyzptlk/g/B;->b:Ldbxyzptlk/g/B;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 49
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Ldbxyzptlk/g/B;)Ldbxyzptlk/g/F;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Ldbxyzptlk/g/B;->g:Ldbxyzptlk/g/F;

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/g/B;Ldbxyzptlk/g/F;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1}, Ldbxyzptlk/g/B;->a(Ldbxyzptlk/g/F;)V

    return-void
.end method

.method private a(Ldbxyzptlk/g/F;)V
    .registers 5
    .parameter

    .prologue
    .line 101
    iput-object p1, p0, Ldbxyzptlk/g/B;->g:Ldbxyzptlk/g/F;

    .line 102
    iget-object v0, p0, Ldbxyzptlk/g/B;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/g/D;

    .line 103
    iget-object v2, p0, Ldbxyzptlk/g/B;->g:Ldbxyzptlk/g/F;

    invoke-interface {v0, v2}, Ldbxyzptlk/g/D;->a(Ldbxyzptlk/g/F;)V

    goto :goto_8

    .line 106
    :cond_1a
    return-void
.end method

.method static synthetic a(Ldbxyzptlk/g/B;Z)Z
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 34
    iput-boolean p1, p0, Ldbxyzptlk/g/B;->c:Z

    return p1
.end method

.method static synthetic b()Ljava/lang/String;
    .registers 1

    .prologue
    .line 34
    sget-object v0, Ldbxyzptlk/g/B;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Ldbxyzptlk/g/B;)Z
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-boolean v0, p0, Ldbxyzptlk/g/B;->d:Z

    return v0
.end method

.method static synthetic c(Ldbxyzptlk/g/B;)Landroid/content/Context;
    .registers 2
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Ldbxyzptlk/g/B;->e:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c()Ldbxyzptlk/g/B;
    .registers 1

    .prologue
    .line 34
    sget-object v0, Ldbxyzptlk/g/B;->b:Ldbxyzptlk/g/B;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .registers 4

    .prologue
    .line 75
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Ldbxyzptlk/g/B;->c:Z

    if-nez v0, :cond_21

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbxyzptlk/g/B;->c:Z

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/g/B;->d:Z

    .line 78
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Ldbxyzptlk/g/E;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Ldbxyzptlk/g/E;-><init>(Ldbxyzptlk/g/B;Ldbxyzptlk/g/C;)V

    const-string v2, "PhotoGalleryMetadataThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 79
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 80
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1f
    .catchall {:try_start_1 .. :try_end_1f} :catchall_25

    .line 84
    :goto_1f
    monitor-exit p0

    return-void

    .line 82
    :cond_21
    const/4 v0, 0x1

    :try_start_22
    iput-boolean v0, p0, Ldbxyzptlk/g/B;->d:Z
    :try_end_24
    .catchall {:try_start_22 .. :try_end_24} :catchall_25

    goto :goto_1f

    .line 75
    :catchall_25
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ldbxyzptlk/g/D;)V
    .registers 3
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Ldbxyzptlk/g/B;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v0, p0, Ldbxyzptlk/g/B;->g:Ldbxyzptlk/g/F;

    invoke-interface {p1, v0}, Ldbxyzptlk/g/D;->a(Ldbxyzptlk/g/F;)V

    .line 94
    return-void
.end method

.method public final b(Ldbxyzptlk/g/D;)V
    .registers 3
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Ldbxyzptlk/g/B;->f:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 98
    return-void
.end method
