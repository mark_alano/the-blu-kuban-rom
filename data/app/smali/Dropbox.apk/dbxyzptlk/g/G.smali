.class public final Ldbxyzptlk/g/G;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# instance fields
.field protected a:Ljava/util/Collection;

.field protected b:Lcom/dropbox/android/util/DropboxPath;

.field protected c:Z

.field protected d:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLjava/util/Collection;Lcom/dropbox/android/util/DropboxPath;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 33
    iput-object p3, p0, Ldbxyzptlk/g/G;->a:Ljava/util/Collection;

    .line 34
    iput-object p4, p0, Ldbxyzptlk/g/G;->b:Lcom/dropbox/android/util/DropboxPath;

    .line 35
    iput-boolean p2, p0, Ldbxyzptlk/g/G;->c:Z

    .line 36
    instance-of v0, p1, Ldbxyzptlk/g/I;

    if-nez v0, :cond_15

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "QueueUserUploadsAsyncTask context must implment QueueUserUploadsAsyncTask"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_15
    invoke-static {}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a()Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;

    move-result-object v0

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->a(Landroid/support/v4/app/j;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 18
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/G;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/Void;
    .registers 8
    .parameter
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Ldbxyzptlk/g/G;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Ldbxyzptlk/g/G;->a:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 49
    iget-object v1, p0, Ldbxyzptlk/g/G;->a:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 50
    new-instance v1, Ldbxyzptlk/g/H;

    invoke-direct {v1, p0}, Ldbxyzptlk/g/H;-><init>(Ldbxyzptlk/g/G;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 56
    invoke-static {}, Lcom/dropbox/android/filemanager/x;->a()Lcom/dropbox/android/filemanager/x;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/g/G;->b:Lcom/dropbox/android/util/DropboxPath;

    iget-boolean v3, p0, Ldbxyzptlk/g/G;->c:Z

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/dropbox/android/filemanager/x;->a(Lcom/dropbox/android/util/DropboxPath;Ljava/util/Collection;ZZ)V

    .line 61
    iput-object v0, p0, Ldbxyzptlk/g/G;->d:Ljava/util/List;

    .line 63
    :cond_2e
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 77
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 18
    check-cast p2, Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/G;->a(Landroid/content/Context;Ljava/lang/Void;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Void;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 69
    move-object v0, p1

    check-cast v0, Ldbxyzptlk/g/I;

    .line 70
    iget-object v1, p0, Ldbxyzptlk/g/G;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ldbxyzptlk/g/I;->a(Ljava/util/List;)V

    .line 71
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/SimpleProgressDialogFrag;->b(Landroid/support/v4/app/j;)V

    .line 72
    return-void
.end method
