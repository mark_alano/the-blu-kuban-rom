.class public Ldbxyzptlk/g/K;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 20
    const-class v0, Ldbxyzptlk/g/K;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/g/K;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/g/a;
    .registers 9
    .parameter
    .parameter

    .prologue
    const v5, 0x7f0b0046

    const/4 v4, 0x0

    .line 49
    :try_start_4
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->b()Ljava/lang/String;

    .line 51
    new-instance v0, Ldbxyzptlk/g/N;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/g/N;-><init>(Ldbxyzptlk/g/L;)V
    :try_end_11
    .catch Ldbxyzptlk/o/j; {:try_start_4 .. :try_end_11} :catch_12
    .catch Ldbxyzptlk/o/i; {:try_start_4 .. :try_end_11} :catch_2c
    .catch Ldbxyzptlk/o/d; {:try_start_4 .. :try_end_11} :catch_5c
    .catch Ldbxyzptlk/o/a; {:try_start_4 .. :try_end_11} :catch_6a

    .line 68
    :goto_11
    return-object v0

    .line 52
    :catch_12
    move-exception v0

    .line 54
    sget-object v1, Ldbxyzptlk/g/K;->a:Ljava/lang/String;

    const-string v2, "Error resending twofactor code"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const v1, 0x7f0b01b1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/o/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    new-instance v0, Ldbxyzptlk/g/t;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/g/t;-><init>(Ljava/lang/String;Z)V

    goto :goto_11

    .line 57
    :catch_2c
    move-exception v0

    .line 58
    sget-object v1, Ldbxyzptlk/g/K;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error resending twofactor code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 60
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/o/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    new-instance v0, Ldbxyzptlk/g/t;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/g/t;-><init>(Ljava/lang/String;Z)V

    goto :goto_11

    .line 62
    :catch_5c
    move-exception v0

    .line 63
    const v0, 0x7f0b0045

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 64
    new-instance v0, Ldbxyzptlk/g/t;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/g/t;-><init>(Ljava/lang/String;Z)V

    goto :goto_11

    .line 65
    :catch_6a
    move-exception v0

    .line 66
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 67
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    new-instance v0, Ldbxyzptlk/g/t;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/g/t;-><init>(Ljava/lang/String;Z)V

    goto :goto_11
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 18
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/K;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/g/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-interface {p2, p1}, Ldbxyzptlk/g/a;->a(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 42
    sget-object v0, Ldbxyzptlk/g/K;->a:Ljava/lang/String;

    const-string v1, "Error resending twofactor code."

    invoke-static {v0, v1, p2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 43
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v0, p2, v1}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 44
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 18
    check-cast p2, Ldbxyzptlk/g/a;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/K;->a(Landroid/content/Context;Ldbxyzptlk/g/a;)V

    return-void
.end method
