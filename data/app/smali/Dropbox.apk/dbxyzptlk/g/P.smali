.class public Ldbxyzptlk/g/P;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/dropbox/android/filemanager/LocalEntry;

.field private c:Landroid/content/Intent;

.field private final d:Ldbxyzptlk/g/S;

.field private e:Ldbxyzptlk/g/R;

.field private f:Ldbxyzptlk/r/u;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 37
    const-class v0, Ldbxyzptlk/g/P;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/g/P;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;Ldbxyzptlk/g/S;)V
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 47
    iput-object v0, p0, Ldbxyzptlk/g/P;->e:Ldbxyzptlk/g/R;

    .line 99
    iput-object v0, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    .line 56
    iput-object p2, p0, Ldbxyzptlk/g/P;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 57
    iput-object p3, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    .line 58
    iput-object p4, p0, Ldbxyzptlk/g/P;->d:Ldbxyzptlk/g/S;

    .line 59
    invoke-virtual {p0}, Ldbxyzptlk/g/P;->f()V

    .line 60
    invoke-static {}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a()Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;

    move-result-object v0

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->a(Landroid/support/v4/app/j;)V

    .line 61
    return-void
.end method

.method private static a(Lcom/dropbox/android/service/x;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/dropbox/android/service/x;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 85
    const-string v0, "wifi"

    .line 91
    :goto_8
    return-object v0

    .line 86
    :cond_9
    invoke-virtual {p0}, Lcom/dropbox/android/service/x;->c()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 87
    const-string v0, "3g"

    goto :goto_8

    .line 88
    :cond_12
    invoke-virtual {p0}, Lcom/dropbox/android/service/x;->a()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 89
    const-string v0, "2g"

    goto :goto_8

    .line 91
    :cond_1b
    const-string v0, "none"

    goto :goto_8
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 35
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/P;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v2

    .line 105
    iget-object v0, p0, Ldbxyzptlk/g/P;->d:Ldbxyzptlk/g/S;

    sget-object v3, Ldbxyzptlk/g/S;->b:Ldbxyzptlk/g/S;

    if-ne v0, v3, :cond_a9

    .line 106
    iget-object v0, p0, Ldbxyzptlk/g/P;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->n(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_96

    .line 108
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 109
    if-eqz v0, :cond_b6

    .line 110
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 111
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "x"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 115
    :goto_46
    invoke-static {}, Lcom/dropbox/android/service/u;->a()Lcom/dropbox/android/service/u;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dropbox/android/service/u;->b()Lcom/dropbox/android/service/x;

    move-result-object v3

    invoke-static {v3}, Ldbxyzptlk/g/P;->a(Lcom/dropbox/android/service/x;)Ljava/lang/String;

    move-result-object v3

    .line 116
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v4

    .line 118
    :try_start_5a
    iget-object v5, v2, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    iget-object v6, p0, Ldbxyzptlk/g/P;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v6, v6, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    iget-object v4, v4, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    invoke-virtual {v5, v6, v0, v3, v4}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/r/u;

    move-result-object v0

    .line 120
    iput-object v0, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    .line 121
    iget-object v0, v0, Ldbxyzptlk/r/u;->a:Ljava/lang/String;

    .line 122
    sget-object v3, Ldbxyzptlk/g/P;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Stream URL: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_82
    .catch Ldbxyzptlk/r/B; {:try_start_5a .. :try_end_82} :catch_83

    .line 139
    :goto_82
    return-object v0

    .line 124
    :catch_83
    move-exception v0

    .line 125
    sget-object v0, Ldbxyzptlk/g/P;->a:Ljava/lang/String;

    const-string v3, "Transcoding failed on the server, falling back."

    invoke-static {v0, v3}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iput-object v1, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    .line 132
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    .line 136
    :cond_96
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/dropbox/android/util/bj;->a(I)Z

    move-result v0

    .line 137
    iget-object v1, v2, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    iget-object v2, p0, Ldbxyzptlk/g/P;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ldbxyzptlk/r/i;->a(Ljava/lang/String;Z)Ldbxyzptlk/n/j;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/n/j;->a:Ljava/lang/String;

    goto :goto_82

    .line 139
    :cond_a9
    iget-object v0, v2, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    iget-object v1, p0, Ldbxyzptlk/g/P;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldbxyzptlk/r/i;->c(Ljava/lang/String;)Ldbxyzptlk/n/j;

    move-result-object v0

    iget-object v0, v0, Ldbxyzptlk/n/j;->a:Ljava/lang/String;

    goto :goto_82

    :cond_b6
    move-object v0, v1

    goto :goto_46
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Ldbxyzptlk/g/P;->d:Ldbxyzptlk/g/S;

    sget-object v1, Ldbxyzptlk/g/S;->b:Ldbxyzptlk/g/S;

    if-ne v0, v1, :cond_22

    const v0, 0x7f0b00aa

    .line 71
    :goto_9
    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 75
    sget-object v0, Ldbxyzptlk/g/P;->a:Ljava/lang/String;

    const-string v1, "Error in SharingFileAsyncTask"

    invoke-static {v0, v1, p2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 76
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v0, p2, v1}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 77
    return-void

    .line 69
    :cond_22
    const v0, 0x7f0b0090

    goto :goto_9
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 35
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/P;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 145
    move-object v0, p1

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/j;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/activity/dialog/TextProgressDialogFrag;->b(Landroid/support/v4/app/j;)V

    .line 146
    if-eqz p2, :cond_19

    .line 147
    sget-object v0, Ldbxyzptlk/g/Q;->a:[I

    iget-object v1, p0, Ldbxyzptlk/g/P;->d:Ldbxyzptlk/g/S;

    invoke-virtual {v1}, Ldbxyzptlk/g/S;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_e8

    .line 193
    :cond_19
    :goto_19
    return-void

    .line 149
    :pswitch_1a
    iget-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    iget-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 151
    invoke-static {}, Lcom/dropbox/android/util/i;->X()Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v1, "component.shared.to"

    iget-object v2, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    goto :goto_19

    .line 156
    :pswitch_3e
    iget-object v0, p0, Ldbxyzptlk/g/P;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->n(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ae

    .line 157
    iget-object v0, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    if-eqz v0, :cond_ae

    .line 158
    sget-object v0, Ldbxyzptlk/g/P;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Container="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    iget-object v2, v2, Ldbxyzptlk/r/u;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", canSeek="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    iget-boolean v2, v2, Ldbxyzptlk/r/u;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    const-string v1, "EXTRA_CONTAINER"

    iget-object v2, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    iget-object v2, v2, Ldbxyzptlk/r/u;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    iget-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    const-string v1, "EXTRA_CAN_SEEK"

    iget-object v2, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    iget-boolean v2, v2, Ldbxyzptlk/r/u;->e:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 161
    iget-object v0, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    iget-object v0, v0, Ldbxyzptlk/r/u;->b:Ljava/lang/String;

    if-eqz v0, :cond_d7

    .line 162
    iget-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    const-string v1, "EXTRA_METADATA_URL"

    iget-object v2, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    iget-object v2, v2, Ldbxyzptlk/r/u;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    :goto_9d
    iget-object v0, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    iget-object v0, v0, Ldbxyzptlk/r/u;->f:Ljava/lang/String;

    if-eqz v0, :cond_df

    .line 167
    iget-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    const-string v1, "EXTRA_PROGRESS_URL"

    iget-object v2, p0, Ldbxyzptlk/g/P;->f:Ldbxyzptlk/r/u;

    iget-object v2, v2, Ldbxyzptlk/r/u;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 177
    :cond_ae
    :goto_ae
    iget-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/g/P;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 179
    iget-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    iget-object v1, p0, Ldbxyzptlk/g/P;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/a;->a(Landroid/content/Intent;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 182
    :try_start_c2
    iget-object v0, p0, Ldbxyzptlk/g/P;->c:Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_c7
    .catch Landroid/content/ActivityNotFoundException; {:try_start_c2 .. :try_end_c7} :catch_c9

    goto/16 :goto_19

    .line 183
    :catch_c9
    move-exception v0

    .line 184
    iget-object v1, p0, Ldbxyzptlk/g/P;->e:Ldbxyzptlk/g/R;

    if-eqz v1, :cond_e7

    .line 185
    iget-object v0, p0, Ldbxyzptlk/g/P;->e:Ldbxyzptlk/g/R;

    iget-object v1, p0, Ldbxyzptlk/g/P;->b:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-interface {v0, v1}, Ldbxyzptlk/g/R;->a(Lcom/dropbox/android/filemanager/LocalEntry;)V

    goto/16 :goto_19

    .line 164
    :cond_d7
    sget-object v0, Ldbxyzptlk/g/P;->a:Ljava/lang/String;

    const-string v1, "No Metadata URL."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9d

    .line 169
    :cond_df
    sget-object v0, Ldbxyzptlk/g/P;->a:Ljava/lang/String;

    const-string v1, "No progress URL."

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_ae

    .line 187
    :cond_e7
    throw v0

    .line 147
    :pswitch_data_e8
    .packed-switch 0x1
        :pswitch_1a
        :pswitch_3e
    .end packed-switch
.end method

.method public final a(Ldbxyzptlk/g/R;)V
    .registers 2
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Ldbxyzptlk/g/P;->e:Ldbxyzptlk/g/R;

    .line 65
    return-void
.end method
