.class public Ldbxyzptlk/g/T;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 18
    const-class v0, Ldbxyzptlk/g/T;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/g/T;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dropbox/android/activity/DropboxActionBarActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 23
    invoke-virtual {p0}, Ldbxyzptlk/g/T;->f()V

    .line 24
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 16
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/T;->a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ljava/lang/String;
    .registers 8
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 35
    :try_start_1
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v1

    .line 36
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v2

    .line 38
    invoke-virtual {v1}, Ldbxyzptlk/l/m;->c()Ldbxyzptlk/r/w;
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_c} :catch_31

    move-result-object v1

    if-nez v1, :cond_12

    .line 40
    :try_start_f
    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/a;->d()Ldbxyzptlk/r/w;
    :try_end_12
    .catch Ldbxyzptlk/o/j; {:try_start_f .. :try_end_12} :catch_25
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_12} :catch_31

    .line 49
    :cond_12
    :try_start_12
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v1

    .line 50
    iget-object v2, v2, Lcom/dropbox/android/filemanager/a;->a:Ldbxyzptlk/r/i;

    invoke-static {p1, v1}, Ldbxyzptlk/r/h;->a(Landroid/content/Context;Ldbxyzptlk/j/g;)Ldbxyzptlk/r/h;

    move-result-object v1

    invoke-virtual {v2, v1}, Ldbxyzptlk/r/i;->a(Ldbxyzptlk/r/h;)Ljava/lang/String;

    move-result-object v0

    .line 56
    :goto_24
    return-object v0

    .line 41
    :catch_25
    move-exception v1

    .line 43
    sget-object v1, Ldbxyzptlk/g/T;->a:Ljava/lang/String;

    const-string v3, "Unauthorized token, unlinking account."

    invoke-static {v1, v3}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-virtual {v2}, Lcom/dropbox/android/filemanager/a;->e()Z
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_30} :catch_31

    goto :goto_24

    .line 53
    :catch_31
    move-exception v1

    .line 54
    sget-object v2, Ldbxyzptlk/g/T;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception in checkForUpgrade: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_24
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 28
    sget-object v0, Ldbxyzptlk/g/T;->a:Ljava/lang/String;

    const-string v1, "Error in checking for upgrade."

    invoke-static {v0, v1, p2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 29
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v0, p2, v1}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 30
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 16
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/T;->a(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 61
    check-cast p1, Lcom/dropbox/android/activity/DropboxActionBarActivity;

    .line 62
    if-eqz p2, :cond_1f

    const-string v0, "show_update"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "force_update"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 63
    :cond_14
    const-string v0, "force_update"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 64
    sput-boolean v0, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a:Z

    .line 65
    invoke-virtual {p1, v0}, Lcom/dropbox/android/activity/DropboxActionBarActivity;->a(Z)V

    .line 67
    :cond_1f
    return-void
.end method
