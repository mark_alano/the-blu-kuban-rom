.class public Ldbxyzptlk/g/d;
.super Ldbxyzptlk/g/h;
.source "panda.py"

# interfaces
.implements Ldbxyzptlk/g/i;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private final e:Lcom/dropbox/android/filemanager/LocalEntry;

.field private f:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 43
    const-class v0, Ldbxyzptlk/g/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/g/d;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;Landroid/content/Intent;)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ldbxyzptlk/g/h;-><init>(Landroid/content/Context;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 50
    iput-object p2, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    .line 51
    iput-object p3, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    .line 52
    invoke-virtual {p0, p0}, Ldbxyzptlk/g/d;->a(Ldbxyzptlk/g/i;)V

    .line 53
    return-void
.end method

.method private static a(Landroid/content/Intent;)Landroid/content/Intent;
    .registers 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 120
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 121
    if-nez v1, :cond_8

    .line 130
    :cond_7
    :goto_7
    return-object v0

    .line 124
    :cond_8
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ldbxyzptlk/g/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 125
    if-eqz v1, :cond_7

    .line 128
    invoke-virtual {p0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 129
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_7
.end method

.method private static a(Landroid/net/Uri;)Landroid/net/Uri;
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 318
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    const-string v2, "file"

    invoke-static {v1, v2}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4a

    .line 319
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 321
    :try_start_16
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 322
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 323
    new-instance v2, Ljava/io/BufferedReader;

    const/16 v3, 0x2000

    invoke-direct {v2, v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    .line 326
    :cond_27
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4a

    .line 327
    const-string v3, "URL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 328
    const-string v3, "="

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 329
    const/4 v4, -0x1

    if-le v3, v4, :cond_27

    .line 330
    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 331
    if-eqz v1, :cond_4a

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_49
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_49} :catch_4b

    move-result-object v0

    .line 339
    :cond_4a
    :goto_4a
    return-object v0

    .line 335
    :catch_4b
    move-exception v1

    .line 336
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v2

    sget-object v3, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v2, v1, v3}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    goto :goto_4a
.end method

.method static synthetic a()Ljava/lang/String;
    .registers 1

    .prologue
    .line 41
    sget-object v0, Ldbxyzptlk/g/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Landroid/content/pm/ResolveInfo;)Ljava/lang/String;
    .registers 2
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v5, -0x1

    const/16 v4, 0x2e

    .line 86
    invoke-virtual {p0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 87
    if-ne v2, v5, :cond_c

    .line 105
    :cond_b
    :goto_b
    return-object v0

    .line 90
    :cond_c
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v5, :cond_b

    .line 93
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v0, v1

    .line 94
    :goto_1c
    if-ge v0, v2, :cond_2c

    .line 95
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v1

    if-ne v1, v4, :cond_29

    .line 102
    const/16 v1, 0x5f

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 94
    :cond_29
    add-int/lit8 v0, v0, 0x1

    goto :goto_1c

    .line 105
    :cond_2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_b
.end method

.method private static a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Intent;)Ljava/util/Set;
    .registers 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v0, 0x1

    .line 66
    invoke-virtual {p0, p2, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 68
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 70
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 71
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_13
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 72
    invoke-static {v0}, Ldbxyzptlk/g/d;->a(Landroid/content/pm/ResolveInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 74
    :cond_27
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 75
    invoke-static {v0}, Ldbxyzptlk/g/d;->a(Landroid/content/pm/ResolveInfo;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_2b

    .line 77
    :cond_3f
    return-object v2
.end method

.method static synthetic a(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-static {p0, p1}, Ldbxyzptlk/g/d;->d(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private static b(Landroid/content/Intent;)Landroid/content/Intent;
    .registers 3
    .parameter

    .prologue
    .line 144
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_30

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-static {v0, v1}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 148
    invoke-virtual {p0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 149
    const-string v1, "http://www.example.com/example"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 152
    :goto_2f
    return-object v0

    :cond_30
    const/4 v0, 0x0

    goto :goto_2f
.end method

.method private static b(Landroid/net/Uri;)Landroid/net/Uri;
    .registers 4
    .parameter

    .prologue
    .line 344
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 345
    invoke-static {v0}, Ldbxyzptlk/f/l;->a(Ljava/io/File;)Ldbxyzptlk/f/i;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/f/g;

    .line 346
    const-string v1, "URL"

    invoke-virtual {v0, v1}, Ldbxyzptlk/f/g;->a(Ljava/lang/String;)Ldbxyzptlk/f/i;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/f/k;

    .line 347
    invoke-virtual {v0}, Ldbxyzptlk/f/k;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_1e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1e} :catch_20

    move-result-object v0

    .line 352
    :goto_1f
    return-object v0

    .line 348
    :catch_20
    move-exception v0

    .line 349
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 352
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method static synthetic b(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 2
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-static {p0, p1}, Ldbxyzptlk/g/d;->c(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method private static c(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 156
    new-instance v0, Ldbxyzptlk/g/e;

    invoke-direct {v0, p1}, Ldbxyzptlk/g/e;-><init>(Landroid/content/Intent;)V

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Ljava/lang/Runnable;)V

    .line 162
    return-void
.end method

.method private static d(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 166
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_4} :catch_5
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_4} :catch_11

    .line 174
    :goto_4
    return-void

    .line 167
    :catch_5
    move-exception v0

    .line 168
    const v0, 0x7f0b0049

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4

    .line 170
    :catch_11
    move-exception v0

    .line 171
    const v0, 0x7f0b0077

    invoke-static {p0, v0, v1}, Lcom/dropbox/android/util/aZ;->b(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_4
.end method

.method private static e(Landroid/content/Context;Landroid/content/Intent;)Z
    .registers 10
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 203
    invoke-static {p1}, Ldbxyzptlk/g/d;->b(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v3

    .line 207
    invoke-static {p1}, Ldbxyzptlk/g/d;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v4

    .line 208
    if-eqz v4, :cond_99

    .line 210
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, v4, p1}, Ldbxyzptlk/g/d;->a(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Intent;)Ljava/util/Set;

    move-result-object v0

    .line 212
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v5

    if-lez v5, :cond_99

    .line 214
    invoke-static {}, Lcom/dropbox/android/util/i;->g()Lcom/dropbox/android/util/s;

    move-result-object v5

    const-string v6, "mime"

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v5

    invoke-virtual {v5}, Lcom/dropbox/android/util/s;->c()V

    .line 215
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2f
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_65

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 218
    invoke-static {}, Lcom/dropbox/android/util/i;->D()Lcom/dropbox/android/util/s;

    move-result-object v6

    const-string v7, "package.name"

    invoke-virtual {v6, v7, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    const-string v6, "mime"

    invoke-virtual {p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v6

    const-string v7, "extension"

    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->t(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v7, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    goto :goto_2f

    :cond_65
    move v0, v2

    .line 226
    :goto_66
    if-nez v0, :cond_6c

    if-nez v3, :cond_6c

    move v0, v1

    .line 268
    :goto_6b
    return v0

    .line 231
    :cond_6c
    if-eqz v3, :cond_94

    .line 237
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    aput-object v3, v0, v1

    aput-object v4, v0, v2

    .line 242
    :goto_75
    new-instance v1, Lcom/dropbox/android/widget/ag;

    const v3, 0x7f0b008a

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, p0, v3, v0, v4}, Lcom/dropbox/android/widget/ag;-><init>(Landroid/content/Context;Ljava/lang/String;[Landroid/content/Intent;[Landroid/content/Intent;)V

    .line 244
    new-instance v0, Ldbxyzptlk/g/f;

    invoke-direct {v0, p1, p0}, Ldbxyzptlk/g/f;-><init>(Landroid/content/Intent;Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lcom/dropbox/android/widget/ag;->a(Lcom/dropbox/android/widget/aj;)V

    .line 261
    new-instance v0, Ldbxyzptlk/g/g;

    invoke-direct {v0, v1}, Ldbxyzptlk/g/g;-><init>(Lcom/dropbox/android/widget/ag;)V

    invoke-static {v0}, Lcom/dropbox/android/activity/lock/LockReceiver;->a(Ljava/lang/Runnable;)V

    move v0, v2

    .line 268
    goto :goto_6b

    .line 239
    :cond_94
    new-array v0, v2, [Landroid/content/Intent;

    aput-object v4, v0, v1

    goto :goto_75

    :cond_99
    move v0, v1

    goto :goto_66
.end method


# virtual methods
.method public final a(Ljava/io/File;Landroid/content/Context;)V
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 273
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    if-nez v0, :cond_40

    .line 274
    iget-object v0, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v1, v1, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    iget-object v2, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/dropbox/android/util/ae;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    .line 275
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    .line 277
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    const v1, 0x10000003

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 280
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TITLE"

    const v2, 0x7f0b008a

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 282
    :cond_40
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    iget-object v1, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/a;->a(Landroid/content/Intent;Lcom/dropbox/android/filemanager/LocalEntry;)V

    .line 284
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->d(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 285
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/g/d;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 286
    if-eqz v0, :cond_75

    .line 287
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 288
    invoke-static {p2, v1}, Ldbxyzptlk/g/d;->c(Landroid/content/Context;Landroid/content/Intent;)V

    .line 289
    const-string v0, "view.success"

    iget-object v1, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    .line 314
    :goto_74
    return-void

    .line 291
    :cond_75
    sget-object v0, Ldbxyzptlk/g/d;->d:Ljava/lang/String;

    const-string v1, "Url file didn\'t have url"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_74

    .line 295
    :cond_7d
    iget-object v0, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->n:Ljava/lang/String;

    invoke-static {v0}, Lcom/dropbox/android/util/ae;->e(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b1

    .line 296
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Ldbxyzptlk/g/d;->b(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    .line 297
    if-eqz v0, :cond_a9

    .line 298
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 299
    invoke-static {p2, v1}, Ldbxyzptlk/g/d;->c(Landroid/content/Context;Landroid/content/Intent;)V

    .line 300
    const-string v0, "view.success"

    iget-object v1, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    goto :goto_74

    .line 302
    :cond_a9
    sget-object v0, Ldbxyzptlk/g/d;->d:Ljava/lang/String;

    const-string v1, "Webloc file didn\'t have url"

    invoke-static {v0, v1}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_74

    .line 305
    :cond_b1
    iget-object v0, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v0, v0, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    if-eqz v0, :cond_c2

    .line 306
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    const-string v1, "CHARACTER_SET"

    iget-object v2, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    iget-object v2, v2, Lcom/dropbox/android/filemanager/LocalEntry;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    :cond_c2
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    invoke-static {p2, v0}, Ldbxyzptlk/g/d;->e(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_cf

    .line 310
    iget-object v0, p0, Ldbxyzptlk/g/d;->f:Landroid/content/Intent;

    invoke-static {p2, v0}, Ldbxyzptlk/g/d;->c(Landroid/content/Context;Landroid/content/Intent;)V

    .line 312
    :cond_cf
    const-string v0, "view.success"

    iget-object v1, p0, Ldbxyzptlk/g/d;->e:Lcom/dropbox/android/filemanager/LocalEntry;

    invoke-static {v0, v1}, Lcom/dropbox/android/util/i;->a(Ljava/lang/String;Lcom/dropbox/android/filemanager/LocalEntry;)Lcom/dropbox/android/util/s;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dropbox/android/util/s;->c()V

    goto :goto_74
.end method
