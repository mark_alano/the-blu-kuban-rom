.class public Ldbxyzptlk/g/r;
.super Ldbxyzptlk/t/a;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 22
    const-class v0, Ldbxyzptlk/g/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/g/r;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .registers 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-direct {p0, p1}, Ldbxyzptlk/t/a;-><init>(Landroid/content/Context;)V

    .line 71
    iput-object p2, p0, Ldbxyzptlk/g/r;->b:Ljava/lang/String;

    .line 72
    iput-boolean p3, p0, Ldbxyzptlk/g/r;->c:Z

    .line 73
    return-void
.end method


# virtual methods
.method public final varargs a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/g/a;
    .registers 9
    .parameter
    .parameter

    .prologue
    const v5, 0x7f0b0046

    const/4 v4, 0x0

    .line 84
    :try_start_4
    invoke-static {}, Lcom/dropbox/android/filemanager/a;->a()Lcom/dropbox/android/filemanager/a;

    move-result-object v0

    .line 85
    iget-object v1, p0, Ldbxyzptlk/g/r;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/dropbox/android/filemanager/a;->a(Ljava/lang/String;)V
    :try_end_d
    .catch Ldbxyzptlk/o/j; {:try_start_4 .. :try_end_d} :catch_1e
    .catch Ldbxyzptlk/o/i; {:try_start_4 .. :try_end_d} :catch_38
    .catch Ldbxyzptlk/o/d; {:try_start_4 .. :try_end_d} :catch_74
    .catch Ldbxyzptlk/o/a; {:try_start_4 .. :try_end_d} :catch_82

    .line 88
    :try_start_d
    invoke-virtual {v0}, Lcom/dropbox/android/filemanager/a;->d()Ldbxyzptlk/r/w;
    :try_end_10
    .catch Ldbxyzptlk/o/a; {:try_start_d .. :try_end_10} :catch_96
    .catch Ldbxyzptlk/o/j; {:try_start_d .. :try_end_10} :catch_1e
    .catch Ldbxyzptlk/o/i; {:try_start_d .. :try_end_10} :catch_38
    .catch Ldbxyzptlk/o/d; {:try_start_d .. :try_end_10} :catch_74

    .line 91
    :goto_10
    :try_start_10
    iget-boolean v0, p0, Ldbxyzptlk/g/r;->c:Z

    if-eqz v0, :cond_17

    .line 94
    invoke-virtual {p0}, Ldbxyzptlk/g/r;->f()V

    .line 97
    :cond_17
    new-instance v0, Ldbxyzptlk/g/u;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/g/u;-><init>(Ldbxyzptlk/g/s;)V
    :try_end_1d
    .catch Ldbxyzptlk/o/j; {:try_start_10 .. :try_end_1d} :catch_1e
    .catch Ldbxyzptlk/o/i; {:try_start_10 .. :try_end_1d} :catch_38
    .catch Ldbxyzptlk/o/d; {:try_start_10 .. :try_end_1d} :catch_74
    .catch Ldbxyzptlk/o/a; {:try_start_10 .. :try_end_1d} :catch_82

    .line 117
    :goto_1d
    return-object v0

    .line 98
    :catch_1e
    move-exception v0

    .line 100
    sget-object v1, Ldbxyzptlk/g/r;->a:Ljava/lang/String;

    const-string v2, "Error logging in"

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const v1, 0x7f0b01b1

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/o/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 102
    new-instance v0, Ldbxyzptlk/g/t;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ldbxyzptlk/g/t;-><init>(Ljava/lang/String;Z)V

    goto :goto_1d

    .line 103
    :catch_38
    move-exception v0

    .line 104
    iget v1, v0, Ldbxyzptlk/o/i;->b:I

    const/16 v2, 0x190

    if-eq v1, v2, :cond_66

    iget v1, v0, Ldbxyzptlk/o/i;->b:I

    const/16 v2, 0x193

    if-eq v1, v2, :cond_66

    .line 106
    sget-object v1, Ldbxyzptlk/g/r;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error verifying twofactor: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ldbxyzptlk/j/f;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 109
    :cond_66
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldbxyzptlk/o/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 110
    new-instance v0, Ldbxyzptlk/g/t;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/g/t;-><init>(Ljava/lang/String;Z)V

    goto :goto_1d

    .line 111
    :catch_74
    move-exception v0

    .line 112
    const v0, 0x7f0b0045

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 113
    new-instance v0, Ldbxyzptlk/g/t;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/g/t;-><init>(Ljava/lang/String;Z)V

    goto :goto_1d

    .line 114
    :catch_82
    move-exception v0

    .line 115
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    sget-object v2, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v1, v0, v2}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 116
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 117
    new-instance v0, Ldbxyzptlk/g/t;

    invoke-direct {v0, v1, v4}, Ldbxyzptlk/g/t;-><init>(Ljava/lang/String;Z)V

    goto :goto_1d

    .line 89
    :catch_96
    move-exception v0

    goto/16 :goto_10
.end method

.method public final bridge synthetic a(Landroid/content/Context;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 20
    check-cast p2, [Ljava/lang/Void;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/r;->a(Landroid/content/Context;[Ljava/lang/Void;)Ldbxyzptlk/g/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/content/Context;Ldbxyzptlk/g/a;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 123
    invoke-interface {p2, p1}, Ldbxyzptlk/g/a;->a(Landroid/content/Context;)V

    .line 124
    return-void
.end method

.method protected final a(Landroid/content/Context;Ljava/lang/Exception;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 77
    sget-object v0, Ldbxyzptlk/g/r;->a:Ljava/lang/String;

    const-string v1, "Error in Logging in."

    invoke-static {v0, v1, p2}, Ldbxyzptlk/j/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 78
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    sget-object v1, Ldbxyzptlk/j/e;->b:Ldbxyzptlk/j/e;

    invoke-virtual {v0, p2, v1}, Ldbxyzptlk/j/b;->a(Ljava/lang/Throwable;Ldbxyzptlk/j/e;)V

    .line 79
    return-void
.end method

.method protected final bridge synthetic a(Landroid/content/Context;Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 20
    check-cast p2, Ldbxyzptlk/g/a;

    invoke-virtual {p0, p1, p2}, Ldbxyzptlk/g/r;->a(Landroid/content/Context;Ldbxyzptlk/g/a;)V

    return-void
.end method
