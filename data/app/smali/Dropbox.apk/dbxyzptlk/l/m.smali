.class public Ldbxyzptlk/l/m;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field private static final a:Ljava/lang/String;

.field private static g:Ldbxyzptlk/l/m;


# instance fields
.field private final b:Landroid/content/Context;

.field private c:Ldbxyzptlk/r/w;

.field private final d:Ljava/lang/Object;

.field private volatile e:Landroid/content/SharedPreferences;

.field private volatile f:Landroid/content/SharedPreferences;

.field private final h:Ljava/util/concurrent/CopyOnWriteArrayList;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 40
    const-class v0, Ldbxyzptlk/l/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/l/m;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/l/m;->d:Ljava/lang/Object;

    .line 392
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Ldbxyzptlk/l/m;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 254
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/l/m;->b:Landroid/content/Context;

    .line 255
    new-instance v0, Ldbxyzptlk/l/n;

    invoke-direct {v0, p0}, Ldbxyzptlk/l/n;-><init>(Ldbxyzptlk/l/m;)V

    invoke-virtual {v0}, Ldbxyzptlk/l/n;->start()V

    .line 262
    return-void
.end method

.method public static E()V
    .registers 7

    .prologue
    .line 681
    invoke-static {}, Ldbxyzptlk/l/m;->a()Ldbxyzptlk/l/m;

    move-result-object v0

    .line 682
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/j/b;->a()Ldbxyzptlk/j/g;

    move-result-object v1

    iget-object v1, v1, Ldbxyzptlk/j/g;->b:Ljava/lang/String;

    .line 684
    invoke-static {}, Lcom/dropbox/android/util/i;->b()Lcom/dropbox/android/util/s;

    move-result-object v2

    .line 686
    invoke-virtual {v0}, Ldbxyzptlk/l/m;->c()Ldbxyzptlk/r/w;

    move-result-object v3

    .line 687
    if-eqz v3, :cond_34

    .line 688
    const-string v4, "COUNTRY"

    iget-object v5, v3, Ldbxyzptlk/r/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 689
    const-string v4, "QUOTA_QUOTA"

    iget-wide v5, v3, Ldbxyzptlk/r/w;->c:J

    invoke-virtual {v2, v4, v5, v6}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 690
    const-string v4, "QUOTA_NORMAL"

    iget-wide v5, v3, Ldbxyzptlk/r/w;->d:J

    invoke-virtual {v2, v4, v5, v6}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 691
    const-string v4, "QUOTA_SHARED"

    iget-wide v5, v3, Ldbxyzptlk/r/w;->e:J

    invoke-virtual {v2, v4, v5, v6}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 694
    :cond_34
    const-string v3, "CAMERA_UPLOAD_ENABLED"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->g()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 695
    const-string v3, "CAMERA_UPLOAD_INITIAL_SCAN"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->i()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 696
    const-string v3, "CAMERA_UPLOAD_HAS_UPLOADED_ONCE"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->h()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 697
    const-string v3, "CAMERA_UPLOAD_HASH_UPDATE"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->j()Ldbxyzptlk/l/q;

    move-result-object v4

    invoke-virtual {v4}, Ldbxyzptlk/l/q;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 698
    const-string v3, "CAMERA_UPLOAD_FIRST_MEDIA_SCAN"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->p()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 699
    const-string v3, "CAMERA_UPLOAD_USE_3G"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->q()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 700
    const-string v3, "CAMERA_UPLOAD_3G_LIMIT"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->r()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 701
    const-string v3, "CAMERA_UPLOAD_IGNORE_EXISTING"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->s()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 702
    const-string v3, "CAMERA_UPLOAD_SEEN_INTRO"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->t()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 703
    const-string v3, "CAMERA_UPLOAD_BROMO_COUNT"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->v()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 704
    const-string v3, "CAMERA_UPLOAD_SEEN_BROMO"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->w()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 705
    const-string v3, "CAMERA_UPLOADS_ALBUM_CURSOR"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->x()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 707
    const-string v3, "NOTIFICATIONS_TO_MUTE"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 708
    const-string v3, "NOTIFICATIONS_MUTED"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->B()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/android/util/s;

    .line 709
    const-string v3, "SEEN_TOUR"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->C()Z

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 710
    const-string v3, "UPDATE_NAG_TIMES"

    invoke-virtual {v0, v1}, Ldbxyzptlk/l/m;->f(Ljava/lang/String;)I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v2, v3, v4, v5}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;J)Lcom/dropbox/android/util/s;

    .line 711
    const-string v1, "DID_REPORT_HOST_SPECIAL"

    invoke-virtual {v0}, Ldbxyzptlk/l/m;->H()Z

    move-result v0

    invoke-virtual {v2, v1, v0}, Lcom/dropbox/android/util/s;->a(Ljava/lang/String;Z)Lcom/dropbox/android/util/s;

    .line 713
    invoke-virtual {v2}, Lcom/dropbox/android/util/s;->c()V

    .line 714
    return-void
.end method

.method static synthetic K()Ljava/lang/String;
    .registers 1

    .prologue
    .line 38
    sget-object v0, Ldbxyzptlk/l/m;->a:Ljava/lang/String;

    return-object v0
.end method

.method private L()Landroid/content/SharedPreferences;
    .registers 5

    .prologue
    .line 265
    iget-object v0, p0, Ldbxyzptlk/l/m;->e:Landroid/content/SharedPreferences;

    if-nez v0, :cond_17

    .line 266
    iget-object v1, p0, Ldbxyzptlk/l/m;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 267
    :try_start_7
    iget-object v0, p0, Ldbxyzptlk/l/m;->e:Landroid/content/SharedPreferences;

    if-nez v0, :cond_16

    .line 268
    new-instance v0, Ldbxyzptlk/l/c;

    iget-object v2, p0, Ldbxyzptlk/l/m;->b:Landroid/content/Context;

    const-string v3, "DropboxAccountPrefs"

    invoke-direct {v0, v2, v3}, Ldbxyzptlk/l/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/l/m;->e:Landroid/content/SharedPreferences;

    .line 270
    :cond_16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_7 .. :try_end_17} :catchall_1a

    .line 272
    :cond_17
    iget-object v0, p0, Ldbxyzptlk/l/m;->e:Landroid/content/SharedPreferences;

    return-object v0

    .line 270
    :catchall_1a
    move-exception v0

    :try_start_1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method private M()Landroid/content/SharedPreferences;
    .registers 5

    .prologue
    .line 276
    iget-object v0, p0, Ldbxyzptlk/l/m;->f:Landroid/content/SharedPreferences;

    if-nez v0, :cond_17

    .line 277
    iget-object v1, p0, Ldbxyzptlk/l/m;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 278
    :try_start_7
    iget-object v0, p0, Ldbxyzptlk/l/m;->f:Landroid/content/SharedPreferences;

    if-nez v0, :cond_16

    .line 279
    new-instance v0, Ldbxyzptlk/l/c;

    iget-object v2, p0, Ldbxyzptlk/l/m;->b:Landroid/content/Context;

    const-string v3, "DropboxPersistentPrefs"

    invoke-direct {v0, v2, v3}, Ldbxyzptlk/l/c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/l/m;->f:Landroid/content/SharedPreferences;

    .line 281
    :cond_16
    monitor-exit v1
    :try_end_17
    .catchall {:try_start_7 .. :try_end_17} :catchall_1a

    .line 283
    :cond_17
    iget-object v0, p0, Ldbxyzptlk/l/m;->f:Landroid/content/SharedPreferences;

    return-object v0

    .line 281
    :catchall_1a
    move-exception v0

    :try_start_1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    throw v0
.end method

.method private N()Landroid/content/SharedPreferences$Editor;
    .registers 2

    .prologue
    .line 803
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method private O()Landroid/content/SharedPreferences$Editor;
    .registers 2

    .prologue
    .line 807
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ldbxyzptlk/l/m;)Landroid/content/SharedPreferences;
    .registers 2
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static a()Ldbxyzptlk/l/m;
    .registers 1

    .prologue
    .line 239
    sget-object v0, Ldbxyzptlk/l/m;->g:Ldbxyzptlk/l/m;

    if-nez v0, :cond_a

    .line 240
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 242
    :cond_a
    sget-object v0, Ldbxyzptlk/l/m;->g:Ldbxyzptlk/l/m;

    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .registers 3
    .parameter

    .prologue
    .line 231
    const-class v1, Ldbxyzptlk/l/m;

    monitor-enter v1

    :try_start_3
    sget-object v0, Ldbxyzptlk/l/m;->g:Ldbxyzptlk/l/m;

    if-nez v0, :cond_10

    .line 232
    new-instance v0, Ldbxyzptlk/l/m;

    invoke-direct {v0, p0}, Ldbxyzptlk/l/m;-><init>(Landroid/content/Context;)V

    sput-object v0, Ldbxyzptlk/l/m;->g:Ldbxyzptlk/l/m;
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_16

    .line 236
    monitor-exit v1

    return-void

    .line 234
    :cond_10
    :try_start_10
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_16
    .catchall {:try_start_10 .. :try_end_16} :catchall_16

    .line 231
    :catchall_16
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Ldbxyzptlk/l/m;)Landroid/content/SharedPreferences;
    .registers 2
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;)Ldbxyzptlk/l/m;
    .registers 2
    .parameter

    .prologue
    .line 246
    sget-object v0, Ldbxyzptlk/l/m;->g:Ldbxyzptlk/l/m;

    if-nez v0, :cond_a

    .line 248
    new-instance v0, Ldbxyzptlk/l/m;

    invoke-direct {v0, p0}, Ldbxyzptlk/l/m;-><init>(Landroid/content/Context;)V

    .line 250
    :goto_9
    return-object v0

    :cond_a
    sget-object v0, Ldbxyzptlk/l/m;->g:Ldbxyzptlk/l/m;

    goto :goto_9
.end method

.method private static c(Landroid/content/Context;)Ljava/lang/String;
    .registers 4
    .parameter

    .prologue
    .line 600
    const-string v0, ""

    .line 601
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_a

    .line 604
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    .line 607
    :cond_a
    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_55

    .line 610
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "android_id"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 611
    const-string v2, "9774d56d682e549c"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_55

    .line 616
    :goto_22
    invoke-static {v1}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 619
    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 620
    if-eqz v0, :cond_46

    .line 621
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 622
    if-eqz v0, :cond_46

    .line 623
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 624
    if-eqz v0, :cond_46

    .line 626
    const-string v1, "\\W"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 632
    :cond_46
    invoke-static {v1}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 634
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 637
    :cond_54
    return-object v1

    :cond_55
    move-object v1, v0

    goto :goto_22
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .registers 4

    .prologue
    .line 574
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "NOTIFICATIONS_TO_MUTE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .registers 4

    .prologue
    .line 582
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "NOTIFICATIONS_MUTED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final C()Z
    .registers 4

    .prologue
    .line 591
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "SEEN_TOUR"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final D()Ljava/lang/String;
    .registers 4

    .prologue
    .line 641
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DEVICE_UDID"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 643
    invoke-static {v0}, Lcom/dropbox/android/util/aT;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 653
    :goto_12
    return-object v0

    .line 649
    :cond_13
    iget-object v0, p0, Ldbxyzptlk/l/m;->b:Landroid/content/Context;

    invoke-static {v0}, Ldbxyzptlk/l/m;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 651
    invoke-direct {p0}, Ldbxyzptlk/l/m;->O()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "DEVICE_UDID"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_12
.end method

.method public final F()J
    .registers 5

    .prologue
    .line 721
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ANAL_NEXT_ROTATION"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final G()Ljava/lang/String;
    .registers 4

    .prologue
    .line 736
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "ANAL_LAST_USER_INFO"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final H()Z
    .registers 4

    .prologue
    .line 749
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DID_REPORT_HOST_SPECIAL"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final I()Ldbxyzptlk/l/t;
    .registers 9

    .prologue
    const/4 v0, 0x0

    .line 757
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "TWOFACTOR_CHECKPOINT_TOKEN"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 758
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "TWOFACTOR_CHECKPOINT_EXPIRE_TIME_MS"

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 759
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "TWOFACTOR_DESCRIPTION"

    invoke-interface {v4, v5, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 760
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v6, "TWOFACTOR_DELIVERY_MODE"

    invoke-interface {v5, v6, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ldbxyzptlk/l/s;->a(Ljava/lang/String;)Ldbxyzptlk/l/s;

    move-result-object v5

    .line 761
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "TWOFACTOR_TEMP_USERNAME"

    invoke-interface {v6, v7, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 762
    if-eqz v1, :cond_48

    if-eqz v4, :cond_48

    if-eqz v5, :cond_48

    if-eqz v6, :cond_48

    if-eqz v5, :cond_48

    .line 763
    new-instance v0, Ldbxyzptlk/l/t;

    invoke-direct/range {v0 .. v6}, Ldbxyzptlk/l/t;-><init>(Ljava/lang/String;JLjava/lang/String;Ldbxyzptlk/l/s;Ljava/lang/String;)V

    .line 770
    :cond_48
    return-object v0
.end method

.method public final J()Ljava/lang/String;
    .registers 4

    .prologue
    .line 798
    const-string v0, "{}"

    .line 799
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "GANDALF_FEATURES_AND_VARIANTS"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .registers 4
    .parameter

    .prologue
    .line 536
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_BROMO_COUNT"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 537
    return-void
.end method

.method public final a(J)V
    .registers 5
    .parameter

    .prologue
    .line 570
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_TURNED_ON_TIME"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 571
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .registers 5
    .parameter

    .prologue
    .line 354
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LAST_EXPORT_URI_V2"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 355
    return-void
.end method

.method public final a(Ldbxyzptlk/l/o;)V
    .registers 5
    .parameter

    .prologue
    .line 441
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_NOTIFICATION"

    invoke-virtual {p1}, Ldbxyzptlk/l/o;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 442
    return-void
.end method

.method public final a(Ldbxyzptlk/l/p;)V
    .registers 3
    .parameter

    .prologue
    .line 395
    iget-object v0, p0, Ldbxyzptlk/l/m;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 396
    return-void
.end method

.method public final a(Ldbxyzptlk/l/q;)V
    .registers 5
    .parameter

    .prologue
    .line 429
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_HASH_UPDATE"

    invoke-virtual {p1}, Ldbxyzptlk/l/q;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 430
    return-void
.end method

.method public final a(Ldbxyzptlk/l/t;)V
    .registers 6
    .parameter

    .prologue
    .line 775
    if-eqz p1, :cond_40

    .line 776
    invoke-direct {p0}, Ldbxyzptlk/l/m;->O()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_CHECKPOINT_TOKEN"

    invoke-virtual {p1}, Ldbxyzptlk/l/t;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_CHECKPOINT_EXPIRE_TIME_MS"

    invoke-virtual {p1}, Ldbxyzptlk/l/t;->c()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_DESCRIPTION"

    invoke-virtual {p1}, Ldbxyzptlk/l/t;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_DELIVERY_MODE"

    invoke-virtual {p1}, Ldbxyzptlk/l/t;->e()Ldbxyzptlk/l/s;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/l/s;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_TEMP_USERNAME"

    invoke-virtual {p1}, Ldbxyzptlk/l/t;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 790
    :goto_3f
    return-void

    .line 783
    :cond_40
    invoke-direct {p0}, Ldbxyzptlk/l/m;->O()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_CHECKPOINT_TOKEN"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_CHECKPOINT_EXPIRE_TIME_MS"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_DESCRIPTION"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_DELIVERY_MODE"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "TWOFACTOR_TEMP_USERNAME"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_3f
.end method

.method public final a(Ldbxyzptlk/r/w;)V
    .registers 6
    .parameter

    .prologue
    .line 313
    if-eqz p1, :cond_51

    .line 314
    iput-object p1, p0, Ldbxyzptlk/l/m;->c:Ldbxyzptlk/r/w;

    .line 315
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 316
    const-string v1, "COUNTRY"

    iget-object v2, p1, Ldbxyzptlk/r/w;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 317
    const-string v1, "DISPLAY_NAME"

    iget-object v2, p1, Ldbxyzptlk/r/w;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 318
    const-string v1, "QUOTA_QUOTA"

    iget-wide v2, p1, Ldbxyzptlk/r/w;->c:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 319
    const-string v1, "QUOTA_NORMAL"

    iget-wide v2, p1, Ldbxyzptlk/r/w;->d:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 320
    const-string v1, "QUOTA_SHARED"

    iget-wide v2, p1, Ldbxyzptlk/r/w;->e:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 321
    const-string v1, "UID"

    iget-wide v2, p1, Ldbxyzptlk/r/w;->f:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 322
    const-string v1, "REFERRAL_LINK"

    iget-object v2, p1, Ldbxyzptlk/r/w;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 323
    const-string v1, "EMAIL"

    iget-object v2, p1, Ldbxyzptlk/r/w;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 324
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 325
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    iget-wide v1, p1, Ldbxyzptlk/r/w;->f:J

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/j/b;->a(J)V

    .line 329
    :goto_50
    return-void

    .line 327
    :cond_51
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/j/b;->a(J)V

    goto :goto_50
.end method

.method public final a(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 341
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LAST_URI"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 342
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .registers 5
    .parameter

    .prologue
    .line 527
    invoke-static {p1}, Ldbxyzptlk/D/c;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 528
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "CAMERA_UPLOAD_BROMO_MAX_IDS"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 529
    return-void
.end method

.method public final declared-synchronized a(Z)V
    .registers 5
    .parameter

    .prologue
    .line 372
    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_ENABLED"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 373
    if-nez p1, :cond_37

    .line 374
    const-string v1, "CAMERA_UPLOAD_INITIAL_SCAN"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_HAS_UPLOADED_ONCE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 378
    :goto_1b
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 379
    iget-object v0, p0, Ldbxyzptlk/l/m;->h:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbxyzptlk/l/p;

    .line 380
    invoke-interface {v0}, Ldbxyzptlk/l/p;->a()V
    :try_end_33
    .catchall {:try_start_1 .. :try_end_33} :catchall_34

    goto :goto_24

    .line 372
    :catchall_34
    move-exception v0

    monitor-exit p0

    throw v0

    .line 376
    :cond_37
    :try_start_37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0, v1, v2}, Ldbxyzptlk/l/m;->a(J)V
    :try_end_3e
    .catchall {:try_start_37 .. :try_end_3e} :catchall_34

    goto :goto_1b

    .line 383
    :cond_3f
    monitor-exit p0

    return-void
.end method

.method public final b()V
    .registers 4

    .prologue
    .line 287
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/j/b;->a(J)V

    .line 288
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 289
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/l/m;->c:Ldbxyzptlk/r/w;

    .line 290
    return-void
.end method

.method public final b(J)V
    .registers 5
    .parameter

    .prologue
    .line 728
    invoke-direct {p0}, Ldbxyzptlk/l/m;->O()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ANAL_NEXT_ROTATION"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 729
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 363
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LAST_GET_CONTENT_URI"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 364
    return-void
.end method

.method public final b(Z)V
    .registers 4
    .parameter

    .prologue
    .line 403
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_HAS_UPLOADED_ONCE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 404
    return-void
.end method

.method public final c()Ldbxyzptlk/r/w;
    .registers 15

    .prologue
    const-wide/16 v4, 0x0

    .line 294
    iget-object v0, p0, Ldbxyzptlk/l/m;->c:Ldbxyzptlk/r/w;

    if-nez v0, :cond_5c

    .line 295
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 296
    const-string v1, "UID"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5c

    .line 297
    const-string v1, "COUNTRY"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 298
    const-string v2, "DISPLAY_NAME"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 299
    const-string v3, "QUOTA_QUOTA"

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 300
    const-string v3, "QUOTA_NORMAL"

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 301
    const-string v3, "QUOTA_SHARED"

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    .line 302
    const-string v3, "UID"

    invoke-interface {v0, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    .line 303
    const-string v5, "REFERRAL_LINK"

    const-string v12, ""

    invoke-interface {v0, v5, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 304
    const-string v12, "EMAIL"

    const-string v13, ""

    invoke-interface {v0, v12, v13}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 305
    new-instance v0, Ldbxyzptlk/r/w;

    invoke-direct/range {v0 .. v12}, Ldbxyzptlk/r/w;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JJJLjava/lang/String;)V

    iput-object v0, p0, Ldbxyzptlk/l/m;->c:Ldbxyzptlk/r/w;

    .line 306
    invoke-static {}, Ldbxyzptlk/j/b;->b()Ldbxyzptlk/j/b;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/l/m;->c:Ldbxyzptlk/r/w;

    iget-wide v1, v1, Ldbxyzptlk/r/w;->f:J

    invoke-virtual {v0, v1, v2}, Ldbxyzptlk/j/b;->a(J)V

    .line 309
    :cond_5c
    iget-object v0, p0, Ldbxyzptlk/l/m;->c:Ldbxyzptlk/r/w;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 548
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOADS_ALBUM_CURSOR"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 549
    return-void
.end method

.method public final c(Z)V
    .registers 4
    .parameter

    .prologue
    .line 417
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_INITIAL_SCAN"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 418
    return-void
.end method

.method public final d()Ljava/lang/String;
    .registers 4

    .prologue
    .line 336
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    const-string v1, "/"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 337
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "LAST_URI"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 578
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "NOTIFICATIONS_TO_MUTE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 579
    return-void
.end method

.method public final d(Z)V
    .registers 4
    .parameter

    .prologue
    .line 461
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_HAD_BACKLOG"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 462
    return-void
.end method

.method public final e()Landroid/net/Uri;
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 345
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "LAST_EXPORT_URI_V2"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 346
    if-eqz v1, :cond_11

    .line 347
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 349
    :cond_11
    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 587
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "NOTIFICATIONS_MUTED"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 588
    return-void
.end method

.method public final e(Z)V
    .registers 4
    .parameter

    .prologue
    .line 475
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_FIRST_MEDIA_SCAN"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 476
    return-void
.end method

.method public final f(Ljava/lang/String;)I
    .registers 6
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 657
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "UPDATE_NAG_VERSION"

    const-string v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 658
    if-eqz p1, :cond_1f

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 659
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "UPDATE_NAG_TIMES"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 661
    :cond_1f
    return v0
.end method

.method public final f()Ljava/lang/String;
    .registers 4

    .prologue
    .line 358
    new-instance v0, Lcom/dropbox/android/util/DropboxPath;

    const-string v1, "/"

    invoke-direct {v0, v1}, Lcom/dropbox/android/util/DropboxPath;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/dropbox/android/util/DropboxPath;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 359
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "LAST_GET_CONTENT_URI"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f(Z)V
    .registers 4
    .parameter

    .prologue
    .line 483
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_USE_3G"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 484
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .registers 7
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 665
    invoke-direct {p0}, Ldbxyzptlk/l/m;->M()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 666
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 668
    const-string v3, "UPDATE_NAG_VERSION"

    const-string v4, ""

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 670
    if-eqz p1, :cond_2b

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 671
    const-string v3, "UPDATE_NAG_TIMES"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 676
    :goto_1f
    add-int/lit8 v0, v0, 0x1

    .line 677
    const-string v1, "UPDATE_NAG_TIMES"

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 678
    return-void

    .line 673
    :cond_2b
    const-string v1, "UPDATE_NAG_VERSION"

    invoke-interface {v2, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1f
.end method

.method public final g(Z)V
    .registers 4
    .parameter

    .prologue
    .line 491
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_3G_LIMIT"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 492
    return-void
.end method

.method public final g()Z
    .registers 4

    .prologue
    .line 367
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_ENABLED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final h(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 745
    invoke-direct {p0}, Ldbxyzptlk/l/m;->O()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ANAL_LAST_USER_INFO"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 746
    return-void
.end method

.method public final h(Z)V
    .registers 4
    .parameter

    .prologue
    .line 499
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_IGNORE_EXISTING"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 500
    return-void
.end method

.method public final h()Z
    .registers 4

    .prologue
    .line 399
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_HAS_UPLOADED_ONCE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final i(Ljava/lang/String;)V
    .registers 4
    .parameter

    .prologue
    .line 793
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "GANDALF_FEATURES_AND_VARIANTS"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 794
    return-void
.end method

.method public final i(Z)V
    .registers 4
    .parameter

    .prologue
    .line 507
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_SEEN_INTRO"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 508
    return-void
.end method

.method public final i()Z
    .registers 4

    .prologue
    .line 413
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_INITIAL_SCAN"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final j()Ldbxyzptlk/l/q;
    .registers 4

    .prologue
    .line 421
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_HASH_UPDATE"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 422
    if-eqz v0, :cond_12

    .line 423
    invoke-static {v0}, Ldbxyzptlk/l/q;->valueOf(Ljava/lang/String;)Ldbxyzptlk/l/q;

    move-result-object v0

    .line 425
    :goto_11
    return-object v0

    :cond_12
    sget-object v0, Ldbxyzptlk/l/q;->b:Ldbxyzptlk/l/q;

    goto :goto_11
.end method

.method public final j(Z)V
    .registers 4
    .parameter

    .prologue
    .line 544
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_SEEN_BROMO"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 545
    return-void
.end method

.method public final k()Ldbxyzptlk/l/o;
    .registers 4

    .prologue
    .line 433
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_NOTIFICATION"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 434
    if-eqz v0, :cond_12

    .line 435
    invoke-static {v0}, Ldbxyzptlk/l/o;->valueOf(Ljava/lang/String;)Ldbxyzptlk/l/o;

    move-result-object v0

    .line 437
    :goto_11
    return-object v0

    :cond_12
    sget-object v0, Ldbxyzptlk/l/o;->a:Ldbxyzptlk/l/o;

    goto :goto_11
.end method

.method public final k(Z)V
    .registers 4
    .parameter

    .prologue
    .line 595
    invoke-direct {p0}, Ldbxyzptlk/l/m;->O()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SEEN_TOUR"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 596
    return-void
.end method

.method public final l()J
    .registers 5

    .prologue
    .line 445
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_NUM_UPLOADS"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final l(Z)V
    .registers 4
    .parameter

    .prologue
    .line 753
    invoke-direct {p0}, Ldbxyzptlk/l/m;->O()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "DID_REPORT_HOST_SPECIAL"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 754
    return-void
.end method

.method public final m()V
    .registers 7

    .prologue
    .line 449
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_NUM_UPLOADS"

    invoke-virtual {p0}, Ldbxyzptlk/l/m;->l()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 450
    return-void
.end method

.method public final n()V
    .registers 5

    .prologue
    .line 453
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_NUM_UPLOADS"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 454
    return-void
.end method

.method public final o()Z
    .registers 4

    .prologue
    .line 457
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_HAD_BACKLOG"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .registers 4

    .prologue
    .line 471
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_FIRST_MEDIA_SCAN"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .registers 4

    .prologue
    .line 479
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_USE_3G"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .registers 4

    .prologue
    .line 487
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_3G_LIMIT"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final s()Z
    .registers 4

    .prologue
    .line 495
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_IGNORE_EXISTING"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final t()Z
    .registers 4

    .prologue
    .line 503
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_SEEN_INTRO"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final u()Ljava/util/Map;
    .registers 4

    .prologue
    .line 511
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_BROMO_MAX_IDS"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 512
    if-nez v0, :cond_13

    .line 513
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 522
    :goto_12
    return-object v0

    .line 517
    :cond_13
    :try_start_13
    new-instance v1, Ldbxyzptlk/E/b;

    invoke-direct {v1}, Ldbxyzptlk/E/b;-><init>()V

    .line 519
    invoke-virtual {v1, v0}, Ldbxyzptlk/E/b;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;
    :try_end_1e
    .catch Ldbxyzptlk/E/c; {:try_start_13 .. :try_end_1e} :catch_1f

    goto :goto_12

    .line 521
    :catch_1f
    move-exception v0

    .line 522
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    goto :goto_12
.end method

.method public final v()I
    .registers 4

    .prologue
    .line 532
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_BROMO_COUNT"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final w()Z
    .registers 4

    .prologue
    .line 540
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_SEEN_BROMO"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final x()Ljava/lang/String;
    .registers 4

    .prologue
    .line 552
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOADS_ALBUM_CURSOR"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final y()V
    .registers 3

    .prologue
    .line 556
    invoke-direct {p0}, Ldbxyzptlk/l/m;->N()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "CAMERA_UPLOADS_ALBUM_CURSOR"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 557
    return-void
.end method

.method public final z()J
    .registers 5

    .prologue
    const-wide/16 v2, -0x1

    .line 560
    invoke-direct {p0}, Ldbxyzptlk/l/m;->L()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CAMERA_UPLOAD_TURNED_ON_TIME"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 561
    cmp-long v2, v0, v2

    if-nez v2, :cond_17

    .line 563
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 564
    invoke-virtual {p0, v0, v1}, Ldbxyzptlk/l/m;->a(J)V

    .line 566
    :cond_17
    return-wide v0
.end method
