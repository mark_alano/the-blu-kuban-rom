.class public Ldbxyzptlk/n/a;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field protected final b:Ldbxyzptlk/q/m;

.field protected final c:Ldbxyzptlk/n/s;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 90
    invoke-static {}, Ldbxyzptlk/n/y;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldbxyzptlk/n/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ldbxyzptlk/q/m;Ldbxyzptlk/n/s;)V
    .registers 5
    .parameter
    .parameter

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    if-nez p1, :cond_d

    .line 107
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Session must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_d
    iput-object p1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    .line 110
    iput-object p2, p0, Ldbxyzptlk/n/a;->c:Ldbxyzptlk/n/s;

    .line 111
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/r;)Ldbxyzptlk/n/i;
    .registers 9
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 1397
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 1399
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 1400
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1403
    :cond_1f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/files/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1404
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "rev"

    aput-object v3, v1, v2

    aput-object p2, v1, v4

    const/4 v2, 0x2

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v3}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1408
    iget-object v2, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v2}, Ldbxyzptlk/q/m;->h()Ljava/lang/String;

    move-result-object v2

    const-string v3, "r2"

    invoke-static {v2, v3, v0, v1}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1410
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v1, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1411
    iget-object v0, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v0, v1}, Ldbxyzptlk/q/m;->a(Lorg/apache/http/HttpRequest;)V

    .line 1413
    invoke-static {p3, v1}, Ldbxyzptlk/n/q;->a(Ldbxyzptlk/n/r;Lorg/apache/http/client/methods/HttpUriRequest;)V

    .line 1414
    invoke-static {p3}, Ldbxyzptlk/n/q;->a(Ldbxyzptlk/n/r;)V

    .line 1415
    iget-object v0, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-static {v0, v1}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/q/m;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 1416
    invoke-static {p3, v0, v4}, Ldbxyzptlk/n/q;->a(Ldbxyzptlk/n/r;Lorg/apache/http/HttpResponse;Z)V

    .line 1418
    new-instance v2, Ldbxyzptlk/n/i;

    invoke-direct {v2, v1, v0}, Ldbxyzptlk/n/i;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v2
.end method

.method private a(Ljava/lang/String;Ljava/io/InputStream;JZLjava/lang/String;Ldbxyzptlk/n/t;)Ldbxyzptlk/n/p;
    .registers 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 2163
    if-eqz p1, :cond_b

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 2164
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "path is null or empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2167
    :cond_13
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 2169
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_31

    .line 2170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2173
    :cond_31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/files_put/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2175
    if-nez p6, :cond_52

    .line 2176
    const-string p6, ""

    .line 2179
    :cond_52
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "overwrite"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "parent_rev"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    aput-object p6, v1, v2

    const/4 v2, 0x4

    const-string v3, "locale"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v3}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 2185
    iget-object v2, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v2}, Ldbxyzptlk/q/m;->h()Ljava/lang/String;

    move-result-object v2

    const-string v3, "r2"

    invoke-static {v2, v3, v0, v1}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2188
    new-instance v2, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v2, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 2189
    iget-object v0, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v0, v2}, Ldbxyzptlk/q/m;->a(Lorg/apache/http/HttpRequest;)V

    .line 2191
    new-instance v1, Lorg/apache/http/entity/InputStreamEntity;

    invoke-direct {v1, p2, p3, p4}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    .line 2192
    const-string v0, "application/octet-stream"

    invoke-virtual {v1, v0}, Lorg/apache/http/entity/InputStreamEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 2193
    invoke-virtual {v1, v4}, Lorg/apache/http/entity/InputStreamEntity;->setChunked(Z)V

    .line 2197
    if-eqz p7, :cond_af

    .line 2198
    new-instance v0, Ldbxyzptlk/n/u;

    invoke-direct {v0, v1, p7}, Ldbxyzptlk/n/u;-><init>(Lorg/apache/http/HttpEntity;Ldbxyzptlk/n/t;)V

    .line 2201
    :goto_a4
    invoke-virtual {v2, v0}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2203
    new-instance v0, Ldbxyzptlk/n/d;

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-direct {v0, v2, v1}, Ldbxyzptlk/n/d;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Ldbxyzptlk/q/m;)V

    return-object v0

    :cond_af
    move-object v0, v1

    goto :goto_a4
.end method

.method protected static a(Ljava/util/Map;Ljava/lang/String;)Z
    .registers 4
    .parameter
    .parameter

    .prologue
    .line 2114
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2115
    if-eqz v0, :cond_11

    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    .line 2116
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2118
    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method protected static b(Ljava/util/Map;Ljava/lang/String;)J
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 2529
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 2530
    const-wide/16 v1, 0x0

    .line 2531
    if-eqz v0, :cond_20

    .line 2532
    instance-of v3, v0, Ljava/lang/Number;

    if-eqz v3, :cond_13

    .line 2533
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    .line 2540
    :goto_12
    return-wide v0

    .line 2534
    :cond_13
    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_20

    .line 2537
    check-cast v0, Ljava/lang/String;

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    goto :goto_12

    :cond_20
    move-wide v0, v1

    goto :goto_12
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Ldbxyzptlk/n/t;)Ldbxyzptlk/n/h;
    .registers 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1354
    iget-object v0, p0, Ldbxyzptlk/n/a;->c:Ldbxyzptlk/n/s;

    invoke-interface {v0}, Ldbxyzptlk/n/s;->a()Ldbxyzptlk/n/r;

    move-result-object v0

    .line 1355
    invoke-direct {p0, p1, p2, v0}, Ldbxyzptlk/n/a;->a(Ljava/lang/String;Ljava/lang/String;Ldbxyzptlk/n/r;)Ldbxyzptlk/n/i;

    move-result-object v1

    .line 1356
    invoke-static {v1, p3, p4, v0}, Ldbxyzptlk/n/i;->a(Ldbxyzptlk/n/i;Ljava/io/OutputStream;Ldbxyzptlk/n/t;Ldbxyzptlk/n/r;)V

    .line 1357
    invoke-static {v0}, Ldbxyzptlk/n/q;->b(Ldbxyzptlk/n/r;)V

    .line 1358
    invoke-virtual {v1}, Ldbxyzptlk/n/i;->a()Ldbxyzptlk/n/h;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ldbxyzptlk/n/o;Ldbxyzptlk/n/n;)Ldbxyzptlk/n/i;
    .registers 10
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1651
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 1653
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/thumbnails/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1654
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "size"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-virtual {p2}, Ldbxyzptlk/n/o;->a()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "format"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-virtual {p3}, Ldbxyzptlk/n/n;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1657
    sget-object v0, Ldbxyzptlk/n/x;->a:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->h()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->b(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ldbxyzptlk/n/m;

    move-result-object v0

    .line 1660
    new-instance v1, Ldbxyzptlk/n/i;

    iget-object v2, v0, Ldbxyzptlk/n/m;->a:Lorg/apache/http/client/methods/HttpUriRequest;

    iget-object v0, v0, Ldbxyzptlk/n/m;->b:Lorg/apache/http/HttpResponse;

    invoke-direct {v1, v2, v0}, Ldbxyzptlk/n/i;-><init>(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/HttpResponse;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Z)Ldbxyzptlk/n/j;
    .registers 10
    .parameter
    .parameter

    .prologue
    .line 2049
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 2050
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/media/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2053
    sget-object v0, Ldbxyzptlk/n/x;->a:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "locale"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v6}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2059
    new-instance v1, Ldbxyzptlk/n/j;

    const/4 v2, 0x0

    invoke-direct {v1, v0, p2, v2}, Ldbxyzptlk/n/j;-><init>(Ljava/util/Map;ZLdbxyzptlk/n/b;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;)Ldbxyzptlk/n/k;
    .registers 8
    .parameter

    .prologue
    .line 1938
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 1940
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "root"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/q/n;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "path"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1945
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/fileops/create_folder"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1949
    new-instance v1, Ldbxyzptlk/n/k;

    invoke-direct {v1, v0}, Ldbxyzptlk/n/k;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Ldbxyzptlk/n/k;
    .registers 12
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1703
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 1705
    if-gtz p2, :cond_7

    .line 1706
    const/16 p2, 0x61a8

    .line 1709
    :cond_7
    const/16 v0, 0xa

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "file_limit"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "hash"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p3, v4, v0

    const/4 v0, 0x4

    const-string v1, "list"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    invoke-static {p4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x6

    const-string v1, "rev"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    aput-object p5, v4, v0

    const/16 v0, 0x8

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/16 v0, 0x9

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1717
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/metadata/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1720
    sget-object v0, Ldbxyzptlk/n/x;->a:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1724
    new-instance v1, Ldbxyzptlk/n/k;

    invoke-direct {v1, v0}, Ldbxyzptlk/n/k;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ldbxyzptlk/n/k;
    .registers 9
    .parameter
    .parameter

    .prologue
    .line 1861
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 1863
    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "root"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/q/n;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "from_path"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p1, v4, v0

    const/4 v0, 0x4

    const-string v1, "to_path"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    aput-object p2, v4, v0

    const/4 v0, 0x6

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1869
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/fileops/move"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1874
    new-instance v1, Ldbxyzptlk/n/k;

    invoke-direct {v1, v0}, Ldbxyzptlk/n/k;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;Ljava/io/InputStream;JLdbxyzptlk/n/t;)Ldbxyzptlk/n/p;
    .registers 14
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1575
    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Ldbxyzptlk/n/a;->a(Ljava/lang/String;Ljava/io/InputStream;JZLjava/lang/String;Ldbxyzptlk/n/t;)Ldbxyzptlk/n/p;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;Ldbxyzptlk/n/t;)Ldbxyzptlk/n/p;
    .registers 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1502
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Ldbxyzptlk/n/a;->a(Ljava/lang/String;Ljava/io/InputStream;JZLjava/lang/String;Ldbxyzptlk/n/t;)Ldbxyzptlk/n/p;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ldbxyzptlk/q/m;
    .registers 2

    .prologue
    .line 1292
    iget-object v0, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;IZ)Ljava/util/List;
    .registers 11
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1804
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 1806
    if-gtz p3, :cond_7

    .line 1807
    const/16 p3, 0x2710

    .line 1810
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/search/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1812
    const/16 v0, 0x8

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "query"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    aput-object p2, v4, v0

    const/4 v0, 0x2

    const-string v1, "file_limit"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x4

    const-string v1, "include_deleted"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    invoke-static {p4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x6

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x7

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1819
    sget-object v0, Ldbxyzptlk/n/x;->a:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    .line 1822
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1823
    instance-of v2, v0, Ldbxyzptlk/D/a;

    if-eqz v2, :cond_92

    .line 1824
    check-cast v0, Ldbxyzptlk/D/a;

    .line 1825
    invoke-virtual {v0}, Ldbxyzptlk/D/a;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_79
    :goto_79
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_92

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 1826
    instance-of v3, v0, Ljava/util/Map;

    if-eqz v3, :cond_79

    .line 1828
    new-instance v3, Ldbxyzptlk/n/k;

    check-cast v0, Ljava/util/Map;

    invoke-direct {v3, v0}, Ldbxyzptlk/n/k;-><init>(Ljava/util/Map;)V

    .line 1829
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_79

    .line 1834
    :cond_92
    return-object v1
.end method

.method protected final b()V
    .registers 3

    .prologue
    .line 2514
    iget-object v0, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v0}, Ldbxyzptlk/q/m;->c()Z

    move-result v0

    if-nez v0, :cond_f

    .line 2515
    new-instance v0, Ldbxyzptlk/o/j;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ldbxyzptlk/o/j;-><init>(Lorg/apache/http/HttpResponse;)V

    throw v0

    .line 2517
    :cond_f
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .registers 8
    .parameter

    .prologue
    .line 1973
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 1975
    const/4 v0, 0x6

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "root"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/q/n;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "path"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    aput-object p1, v4, v0

    const/4 v0, 0x4

    const-string v1, "locale"

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    .line 1979
    sget-object v0, Ldbxyzptlk/n/x;->b:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/fileops/delete"

    const-string v3, "r2"

    iget-object v5, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    .line 1981
    return-void
.end method

.method public final c(Ljava/lang/String;)Ldbxyzptlk/n/j;
    .registers 9
    .parameter

    .prologue
    .line 2083
    invoke-virtual {p0}, Ldbxyzptlk/n/a;->b()V

    .line 2085
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/shares/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->a()Ldbxyzptlk/q/n;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2088
    sget-object v0, Ldbxyzptlk/n/x;->a:Ldbxyzptlk/n/x;

    iget-object v1, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v1}, Ldbxyzptlk/q/m;->g()Ljava/lang/String;

    move-result-object v1

    const-string v3, "r2"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "locale"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-interface {v6}, Ldbxyzptlk/q/m;->b()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, Ldbxyzptlk/n/a;->b:Ldbxyzptlk/q/m;

    invoke-static/range {v0 .. v5}, Ldbxyzptlk/n/w;->a(Ldbxyzptlk/n/x;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ldbxyzptlk/q/m;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 2094
    const-string v1, "url"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2095
    const-string v2, "expires"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 2097
    if-eqz v1, :cond_5f

    if-nez v2, :cond_67

    .line 2098
    :cond_5f
    new-instance v0, Ldbxyzptlk/o/f;

    const-string v1, "Could not parse share response."

    invoke-direct {v0, v1}, Ldbxyzptlk/o/f;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2101
    :cond_67
    new-instance v1, Ldbxyzptlk/n/j;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Ldbxyzptlk/n/j;-><init>(Ljava/util/Map;Ldbxyzptlk/n/b;)V

    return-object v1
.end method
