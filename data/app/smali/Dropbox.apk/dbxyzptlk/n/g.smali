.class public final Ldbxyzptlk/n/g;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/List;

.field public final d:Z


# direct methods
.method public constructor <init>(ZLjava/util/List;Ljava/lang/String;Z)V
    .registers 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2312
    iput-boolean p1, p0, Ldbxyzptlk/n/g;->a:Z

    .line 2313
    iput-object p2, p0, Ldbxyzptlk/n/g;->c:Ljava/util/List;

    .line 2314
    iput-object p3, p0, Ldbxyzptlk/n/g;->b:Ljava/lang/String;

    .line 2315
    iput-boolean p4, p0, Ldbxyzptlk/n/g;->d:Z

    .line 2316
    return-void
.end method

.method public static a(Ldbxyzptlk/p/k;Ldbxyzptlk/p/c;)Ldbxyzptlk/n/g;
    .registers 7
    .parameter
    .parameter

    .prologue
    .line 2319
    invoke-virtual {p0}, Ldbxyzptlk/p/k;->b()Ldbxyzptlk/p/g;

    move-result-object v0

    .line 2320
    const-string v1, "reset"

    invoke-virtual {v0, v1}, Ldbxyzptlk/p/g;->b(Ljava/lang/String;)Ldbxyzptlk/p/k;

    move-result-object v1

    invoke-virtual {v1}, Ldbxyzptlk/p/k;->f()Z

    move-result v1

    .line 2321
    const-string v2, "cursor"

    invoke-virtual {v0, v2}, Ldbxyzptlk/p/g;->b(Ljava/lang/String;)Ldbxyzptlk/p/k;

    move-result-object v2

    invoke-virtual {v2}, Ldbxyzptlk/p/k;->e()Ljava/lang/String;

    move-result-object v2

    .line 2322
    const-string v3, "has_more"

    invoke-virtual {v0, v3}, Ldbxyzptlk/p/g;->b(Ljava/lang/String;)Ldbxyzptlk/p/k;

    move-result-object v3

    invoke-virtual {v3}, Ldbxyzptlk/p/k;->f()Z

    move-result v3

    .line 2323
    const-string v4, "entries"

    invoke-virtual {v0, v4}, Ldbxyzptlk/p/g;->b(Ljava/lang/String;)Ldbxyzptlk/p/k;

    move-result-object v0

    invoke-virtual {v0}, Ldbxyzptlk/p/k;->c()Ldbxyzptlk/p/d;

    move-result-object v0

    new-instance v4, Ldbxyzptlk/n/f;

    invoke-direct {v4, p1}, Ldbxyzptlk/n/f;-><init>(Ldbxyzptlk/p/c;)V

    invoke-virtual {v0, v4}, Ldbxyzptlk/p/d;->a(Ldbxyzptlk/p/c;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2325
    new-instance v4, Ldbxyzptlk/n/g;

    invoke-direct {v4, v1, v0, v2, v3}, Ldbxyzptlk/n/g;-><init>(ZLjava/util/List;Ljava/lang/String;Z)V

    return-object v4
.end method
