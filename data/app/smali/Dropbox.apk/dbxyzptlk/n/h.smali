.class public final Ldbxyzptlk/n/h;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private a:Ljava/lang/String;

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Ldbxyzptlk/n/k;


# direct methods
.method private constructor <init>(Lorg/apache/http/HttpResponse;)V
    .registers 7
    .parameter

    .prologue
    const-wide/16 v3, -0x1

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 490
    iput-object v0, p0, Ldbxyzptlk/n/h;->a:Ljava/lang/String;

    .line 491
    iput-wide v3, p0, Ldbxyzptlk/n/h;->b:J

    .line 492
    iput-object v0, p0, Ldbxyzptlk/n/h;->c:Ljava/lang/String;

    .line 493
    iput-object v0, p0, Ldbxyzptlk/n/h;->d:Ldbxyzptlk/n/k;

    .line 498
    invoke-static {p1}, Ldbxyzptlk/n/h;->a(Lorg/apache/http/HttpResponse;)Ldbxyzptlk/n/k;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/n/h;->d:Ldbxyzptlk/n/k;

    .line 499
    iget-object v0, p0, Ldbxyzptlk/n/h;->d:Ldbxyzptlk/n/k;

    if-nez v0, :cond_21

    .line 500
    new-instance v0, Ldbxyzptlk/o/f;

    const-string v1, "Error parsing metadata."

    invoke-direct {v0, v1}, Ldbxyzptlk/o/f;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503
    :cond_21
    iget-object v0, p0, Ldbxyzptlk/n/h;->d:Ldbxyzptlk/n/k;

    invoke-static {p1, v0}, Ldbxyzptlk/n/h;->a(Lorg/apache/http/HttpResponse;Ldbxyzptlk/n/k;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/n/h;->b:J

    .line 504
    iget-wide v0, p0, Ldbxyzptlk/n/h;->b:J

    cmp-long v0, v0, v3

    if-nez v0, :cond_37

    .line 505
    new-instance v0, Ldbxyzptlk/o/f;

    const-string v1, "Error determining file size."

    invoke-direct {v0, v1}, Ldbxyzptlk/o/f;-><init>(Ljava/lang/String;)V

    throw v0

    .line 509
    :cond_37
    const-string v0, "Content-Type"

    invoke-interface {p1, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 510
    if-eqz v0, :cond_6d

    .line 511
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 512
    if-eqz v0, :cond_6d

    .line 513
    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 514
    array-length v1, v0

    if-lez v1, :cond_57

    .line 515
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldbxyzptlk/n/h;->a:Ljava/lang/String;

    .line 517
    :cond_57
    array-length v1, v0

    if-le v1, v2, :cond_6d

    .line 518
    aget-object v0, v0, v2

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 519
    array-length v1, v0

    if-le v1, v2, :cond_6d

    .line 520
    aget-object v0, v0, v2

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/n/h;->c:Ljava/lang/String;

    .line 525
    :cond_6d
    return-void
.end method

.method synthetic constructor <init>(Lorg/apache/http/HttpResponse;Ldbxyzptlk/n/b;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 488
    invoke-direct {p0, p1}, Ldbxyzptlk/n/h;-><init>(Lorg/apache/http/HttpResponse;)V

    return-void
.end method

.method private static a(Lorg/apache/http/HttpResponse;Ldbxyzptlk/n/k;)J
    .registers 6
    .parameter
    .parameter

    .prologue
    .line 571
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v0

    .line 572
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_f

    .line 581
    :goto_e
    return-wide v0

    .line 577
    :cond_f
    if-eqz p1, :cond_14

    .line 578
    iget-wide v0, p1, Ldbxyzptlk/n/k;->h:J

    goto :goto_e

    .line 581
    :cond_14
    const-wide/16 v0, -0x1

    goto :goto_e
.end method

.method private static a(Lorg/apache/http/HttpResponse;)Ldbxyzptlk/n/k;
    .registers 3
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 536
    if-nez p0, :cond_5

    move-object v0, v1

    .line 554
    :goto_4
    return-object v0

    .line 540
    :cond_5
    const-string v0, "X-Dropbox-Metadata"

    invoke-interface {p0, v0}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v0

    .line 542
    if-nez v0, :cond_f

    move-object v0, v1

    .line 543
    goto :goto_4

    .line 547
    :cond_f
    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 548
    invoke-static {v0}, Ldbxyzptlk/D/d;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 549
    if-nez v0, :cond_1b

    move-object v0, v1

    .line 550
    goto :goto_4

    .line 553
    :cond_1b
    check-cast v0, Ljava/util/Map;

    .line 554
    new-instance v1, Ldbxyzptlk/n/k;

    invoke-direct {v1, v0}, Ldbxyzptlk/n/k;-><init>(Ljava/util/Map;)V

    move-object v0, v1

    goto :goto_4
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .registers 2

    .prologue
    .line 589
    iget-object v0, p0, Ldbxyzptlk/n/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()J
    .registers 3

    .prologue
    .line 604
    iget-wide v0, p0, Ldbxyzptlk/n/h;->b:J

    return-wide v0
.end method

.method public final c()Ljava/lang/String;
    .registers 2

    .prologue
    .line 612
    iget-object v0, p0, Ldbxyzptlk/n/h;->c:Ljava/lang/String;

    return-object v0
.end method
