.class public Ldbxyzptlk/n/k;
.super Ljava/lang/Object;
.source "panda.py"


# static fields
.field public static final v:Ldbxyzptlk/p/c;


# instance fields
.field public h:J

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Z

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;

.field public s:Z

.field public t:Z

.field public u:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 362
    new-instance v0, Ldbxyzptlk/n/l;

    invoke-direct {v0}, Ldbxyzptlk/n/l;-><init>()V

    sput-object v0, Ldbxyzptlk/n/k;->v:Ldbxyzptlk/p/c;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 339
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .registers 6
    .parameter

    .prologue
    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    const-string v0, "bytes"

    invoke-static {p1, v0}, Ldbxyzptlk/n/a;->b(Ljava/util/Map;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/n/k;->h:J

    .line 309
    const-string v0, "hash"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    .line 310
    const-string v0, "icon"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/n/k;->j:Ljava/lang/String;

    .line 311
    const-string v0, "is_dir"

    invoke-static {p1, v0}, Ldbxyzptlk/n/a;->a(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/n/k;->k:Z

    .line 312
    const-string v0, "modified"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/n/k;->l:Ljava/lang/String;

    .line 313
    const-string v0, "client_mtime"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/n/k;->m:Ljava/lang/String;

    .line 314
    const-string v0, "path"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    .line 315
    const-string v0, "root"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/n/k;->o:Ljava/lang/String;

    .line 316
    const-string v0, "size"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/n/k;->p:Ljava/lang/String;

    .line 317
    const-string v0, "mime_type"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    .line 318
    const-string v0, "rev"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    .line 319
    const-string v0, "thumb_exists"

    invoke-static {p1, v0}, Ldbxyzptlk/n/a;->a(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/n/k;->s:Z

    .line 320
    const-string v0, "is_deleted"

    invoke-static {p1, v0}, Ldbxyzptlk/n/a;->a(Ljava/util/Map;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/n/k;->t:Z

    .line 322
    const-string v0, "contents"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 323
    if-eqz v0, :cond_b1

    instance-of v1, v0, Ldbxyzptlk/D/a;

    if-eqz v1, :cond_b1

    .line 324
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Ldbxyzptlk/n/k;->u:Ljava/util/List;

    .line 326
    check-cast v0, Ldbxyzptlk/D/a;

    invoke-virtual {v0}, Ldbxyzptlk/D/a;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 327
    :cond_96
    :goto_96
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b4

    .line 328
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 329
    instance-of v2, v0, Ljava/util/Map;

    if-eqz v2, :cond_96

    .line 330
    iget-object v2, p0, Ldbxyzptlk/n/k;->u:Ljava/util/List;

    new-instance v3, Ldbxyzptlk/n/k;

    check-cast v0, Ljava/util/Map;

    invoke-direct {v3, v0}, Ldbxyzptlk/n/k;-><init>(Ljava/util/Map;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_96

    .line 334
    :cond_b1
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/n/k;->u:Ljava/util/List;

    .line 336
    :cond_b4
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .registers 4

    .prologue
    .line 346
    iget-object v0, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 347
    iget-object v1, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .registers 4

    .prologue
    .line 354
    iget-object v0, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 355
    const-string v0, ""

    .line 358
    :goto_c
    return-object v0

    .line 357
    :cond_d
    iget-object v0, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 358
    iget-object v1, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    const/4 v2, 0x0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_c
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 8
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 394
    if-ne p0, p1, :cond_5

    .line 454
    :cond_4
    :goto_4
    return v0

    .line 396
    :cond_5
    if-nez p1, :cond_9

    move v0, v1

    .line 397
    goto :goto_4

    .line 398
    :cond_9
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 399
    goto :goto_4

    .line 400
    :cond_15
    check-cast p1, Ldbxyzptlk/n/k;

    .line 401
    iget-wide v2, p0, Ldbxyzptlk/n/k;->h:J

    iget-wide v4, p1, Ldbxyzptlk/n/k;->h:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_21

    move v0, v1

    .line 402
    goto :goto_4

    .line 403
    :cond_21
    iget-object v2, p0, Ldbxyzptlk/n/k;->m:Ljava/lang/String;

    if-nez v2, :cond_2b

    .line 404
    iget-object v2, p1, Ldbxyzptlk/n/k;->m:Ljava/lang/String;

    if-eqz v2, :cond_37

    move v0, v1

    .line 405
    goto :goto_4

    .line 406
    :cond_2b
    iget-object v2, p0, Ldbxyzptlk/n/k;->m:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/n/k;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_37

    move v0, v1

    .line 407
    goto :goto_4

    .line 408
    :cond_37
    iget-object v2, p0, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    if-nez v2, :cond_41

    .line 409
    iget-object v2, p1, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    if-eqz v2, :cond_4d

    move v0, v1

    .line 410
    goto :goto_4

    .line 411
    :cond_41
    iget-object v2, p0, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4d

    move v0, v1

    .line 412
    goto :goto_4

    .line 413
    :cond_4d
    iget-object v2, p0, Ldbxyzptlk/n/k;->j:Ljava/lang/String;

    if-nez v2, :cond_57

    .line 414
    iget-object v2, p1, Ldbxyzptlk/n/k;->j:Ljava/lang/String;

    if-eqz v2, :cond_63

    move v0, v1

    .line 415
    goto :goto_4

    .line 416
    :cond_57
    iget-object v2, p0, Ldbxyzptlk/n/k;->j:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/n/k;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_63

    move v0, v1

    .line 417
    goto :goto_4

    .line 418
    :cond_63
    iget-boolean v2, p0, Ldbxyzptlk/n/k;->t:Z

    iget-boolean v3, p1, Ldbxyzptlk/n/k;->t:Z

    if-eq v2, v3, :cond_6b

    move v0, v1

    .line 419
    goto :goto_4

    .line 420
    :cond_6b
    iget-boolean v2, p0, Ldbxyzptlk/n/k;->k:Z

    iget-boolean v3, p1, Ldbxyzptlk/n/k;->k:Z

    if-eq v2, v3, :cond_73

    move v0, v1

    .line 421
    goto :goto_4

    .line 422
    :cond_73
    iget-object v2, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    if-nez v2, :cond_7d

    .line 423
    iget-object v2, p1, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    if-eqz v2, :cond_8a

    move v0, v1

    .line 424
    goto :goto_4

    .line 425
    :cond_7d
    iget-object v2, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8a

    move v0, v1

    .line 426
    goto/16 :goto_4

    .line 427
    :cond_8a
    iget-object v2, p0, Ldbxyzptlk/n/k;->l:Ljava/lang/String;

    if-nez v2, :cond_95

    .line 428
    iget-object v2, p1, Ldbxyzptlk/n/k;->l:Ljava/lang/String;

    if-eqz v2, :cond_a2

    move v0, v1

    .line 429
    goto/16 :goto_4

    .line 430
    :cond_95
    iget-object v2, p0, Ldbxyzptlk/n/k;->l:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/n/k;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a2

    move v0, v1

    .line 431
    goto/16 :goto_4

    .line 432
    :cond_a2
    iget-object v2, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    if-nez v2, :cond_ad

    .line 433
    iget-object v2, p1, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    if-eqz v2, :cond_ba

    move v0, v1

    .line 434
    goto/16 :goto_4

    .line 435
    :cond_ad
    iget-object v2, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_ba

    move v0, v1

    .line 436
    goto/16 :goto_4

    .line 437
    :cond_ba
    iget-object v2, p0, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    if-nez v2, :cond_c5

    .line 438
    iget-object v2, p1, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    if-eqz v2, :cond_d2

    move v0, v1

    .line 439
    goto/16 :goto_4

    .line 440
    :cond_c5
    iget-object v2, p0, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d2

    move v0, v1

    .line 441
    goto/16 :goto_4

    .line 442
    :cond_d2
    iget-object v2, p0, Ldbxyzptlk/n/k;->o:Ljava/lang/String;

    if-nez v2, :cond_dd

    .line 443
    iget-object v2, p1, Ldbxyzptlk/n/k;->o:Ljava/lang/String;

    if-eqz v2, :cond_ea

    move v0, v1

    .line 444
    goto/16 :goto_4

    .line 445
    :cond_dd
    iget-object v2, p0, Ldbxyzptlk/n/k;->o:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/n/k;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_ea

    move v0, v1

    .line 446
    goto/16 :goto_4

    .line 447
    :cond_ea
    iget-object v2, p0, Ldbxyzptlk/n/k;->p:Ljava/lang/String;

    if-nez v2, :cond_f5

    .line 448
    iget-object v2, p1, Ldbxyzptlk/n/k;->p:Ljava/lang/String;

    if-eqz v2, :cond_102

    move v0, v1

    .line 449
    goto/16 :goto_4

    .line 450
    :cond_f5
    iget-object v2, p0, Ldbxyzptlk/n/k;->p:Ljava/lang/String;

    iget-object v3, p1, Ldbxyzptlk/n/k;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_102

    move v0, v1

    .line 451
    goto/16 :goto_4

    .line 452
    :cond_102
    iget-boolean v2, p0, Ldbxyzptlk/n/k;->s:Z

    iget-boolean v3, p1, Ldbxyzptlk/n/k;->s:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 453
    goto/16 :goto_4
.end method

.method public hashCode()I
    .registers 9

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 371
    .line 373
    iget-wide v4, p0, Ldbxyzptlk/n/k;->h:J

    iget-wide v6, p0, Ldbxyzptlk/n/k;->h:J

    const/16 v0, 0x20

    ushr-long/2addr v6, v0

    xor-long/2addr v4, v6

    long-to-int v0, v4

    add-int/lit8 v0, v0, 0x1f

    .line 374
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/n/k;->m:Ljava/lang/String;

    if-nez v0, :cond_6f

    move v0, v1

    :goto_17
    add-int/2addr v0, v4

    .line 376
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    if-nez v0, :cond_76

    move v0, v1

    :goto_1f
    add-int/2addr v0, v4

    .line 377
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/n/k;->j:Ljava/lang/String;

    if-nez v0, :cond_7d

    move v0, v1

    :goto_27
    add-int/2addr v0, v4

    .line 378
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Ldbxyzptlk/n/k;->t:Z

    if-eqz v0, :cond_84

    move v0, v2

    :goto_2f
    add-int/2addr v0, v4

    .line 379
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Ldbxyzptlk/n/k;->k:Z

    if-eqz v0, :cond_86

    move v0, v2

    :goto_37
    add-int/2addr v0, v4

    .line 380
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    if-nez v0, :cond_88

    move v0, v1

    :goto_3f
    add-int/2addr v0, v4

    .line 382
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/n/k;->l:Ljava/lang/String;

    if-nez v0, :cond_8f

    move v0, v1

    :goto_47
    add-int/2addr v0, v4

    .line 384
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    if-nez v0, :cond_96

    move v0, v1

    :goto_4f
    add-int/2addr v0, v4

    .line 385
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    if-nez v0, :cond_9d

    move v0, v1

    :goto_57
    add-int/2addr v0, v4

    .line 386
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Ldbxyzptlk/n/k;->o:Ljava/lang/String;

    if-nez v0, :cond_a4

    move v0, v1

    :goto_5f
    add-int/2addr v0, v4

    .line 387
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Ldbxyzptlk/n/k;->p:Ljava/lang/String;

    if-nez v4, :cond_ab

    :goto_66
    add-int/2addr v0, v1

    .line 388
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Ldbxyzptlk/n/k;->s:Z

    if-eqz v1, :cond_b2

    :goto_6d
    add-int/2addr v0, v2

    .line 389
    return v0

    .line 374
    :cond_6f
    iget-object v0, p0, Ldbxyzptlk/n/k;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_17

    .line 376
    :cond_76
    iget-object v0, p0, Ldbxyzptlk/n/k;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1f

    .line 377
    :cond_7d
    iget-object v0, p0, Ldbxyzptlk/n/k;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_27

    :cond_84
    move v0, v3

    .line 378
    goto :goto_2f

    :cond_86
    move v0, v3

    .line 379
    goto :goto_37

    .line 380
    :cond_88
    iget-object v0, p0, Ldbxyzptlk/n/k;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3f

    .line 382
    :cond_8f
    iget-object v0, p0, Ldbxyzptlk/n/k;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_47

    .line 384
    :cond_96
    iget-object v0, p0, Ldbxyzptlk/n/k;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4f

    .line 385
    :cond_9d
    iget-object v0, p0, Ldbxyzptlk/n/k;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_57

    .line 386
    :cond_a4
    iget-object v0, p0, Ldbxyzptlk/n/k;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5f

    .line 387
    :cond_ab
    iget-object v1, p0, Ldbxyzptlk/n/k;->p:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_66

    :cond_b2
    move v2, v3

    .line 388
    goto :goto_6d
.end method
