.class public final Ldbxyzptlk/r/p;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field private final a:J

.field private final b:Ldbxyzptlk/q/k;

.field private final c:Ljava/lang/String;

.field private final d:Ldbxyzptlk/l/r;


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .registers 8
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    const-string v0, "uid"

    invoke-static {p1, v0}, Ldbxyzptlk/r/i;->d(Ljava/util/Map;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Ldbxyzptlk/r/p;->a:J

    .line 203
    const/4 v0, 0x0

    .line 204
    const-string v1, "requires_twofactor"

    invoke-interface {p1, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 205
    const-string v0, "requires_twofactor"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 208
    :cond_21
    if-eqz v0, :cond_67

    .line 209
    const-string v0, "checkpoint_token_ttl"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 210
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, v0

    .line 212
    iput-object v4, p0, Ldbxyzptlk/r/p;->b:Ldbxyzptlk/q/k;

    .line 213
    new-instance v0, Ldbxyzptlk/l/r;

    const-string v1, "checkpoint_token"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v4, "twofactor_desc"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v5, "twofactor_delivery_mode"

    invoke-interface {p1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Ldbxyzptlk/l/s;->a(Ljava/lang/String;)Ldbxyzptlk/l/s;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Ldbxyzptlk/l/r;-><init>(Ljava/lang/String;JLjava/lang/String;Ldbxyzptlk/l/s;)V

    iput-object v0, p0, Ldbxyzptlk/r/p;->d:Ldbxyzptlk/l/r;

    .line 224
    :goto_5c
    const-string v0, "email"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/r/p;->c:Ljava/lang/String;

    .line 225
    return-void

    .line 219
    :cond_67
    new-instance v2, Ldbxyzptlk/q/k;

    const-string v0, "token"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "secret"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Ldbxyzptlk/q/k;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Ldbxyzptlk/r/p;->b:Ldbxyzptlk/q/k;

    .line 222
    iput-object v4, p0, Ldbxyzptlk/r/p;->d:Ldbxyzptlk/l/r;

    goto :goto_5c
.end method

.method synthetic constructor <init>(Ljava/util/Map;Ldbxyzptlk/r/j;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 182
    invoke-direct {p0, p1}, Ldbxyzptlk/r/p;-><init>(Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .registers 2

    .prologue
    .line 228
    iget-object v0, p0, Ldbxyzptlk/r/p;->d:Ldbxyzptlk/l/r;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final b()Ldbxyzptlk/l/r;
    .registers 2

    .prologue
    .line 232
    iget-object v0, p0, Ldbxyzptlk/r/p;->d:Ldbxyzptlk/l/r;

    return-object v0
.end method

.method public final c()Ldbxyzptlk/q/k;
    .registers 2

    .prologue
    .line 236
    iget-object v0, p0, Ldbxyzptlk/r/p;->b:Ldbxyzptlk/q/k;

    return-object v0
.end method

.method public final d()J
    .registers 3

    .prologue
    .line 240
    iget-wide v0, p0, Ldbxyzptlk/r/p;->a:J

    return-wide v0
.end method

.method public final e()Ljava/lang/String;
    .registers 2

    .prologue
    .line 248
    iget-object v0, p0, Ldbxyzptlk/r/p;->c:Ljava/lang/String;

    return-object v0
.end method
