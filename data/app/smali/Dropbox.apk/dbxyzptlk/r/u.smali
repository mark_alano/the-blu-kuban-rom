.class public final Ldbxyzptlk/r/u;
.super Ljava/lang/Object;
.source "panda.py"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/Date;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .registers 3
    .parameter

    .prologue
    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 296
    const-string v0, "url"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/r/u;->a:Ljava/lang/String;

    .line 297
    const-string v0, "container"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/r/u;->d:Ljava/lang/String;

    .line 298
    const-string v0, "metadata_url"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/r/u;->b:Ljava/lang/String;

    .line 299
    const-string v0, "progress_url"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldbxyzptlk/r/u;->f:Ljava/lang/String;

    .line 301
    const-string v0, "expires"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 302
    if-eqz v0, :cond_4c

    .line 303
    invoke-static {v0}, Ldbxyzptlk/n/w;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Ldbxyzptlk/r/u;->c:Ljava/util/Date;

    .line 308
    :goto_3b
    const-string v0, "can_seek"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 309
    if-eqz v0, :cond_50

    .line 310
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Ldbxyzptlk/r/u;->e:Z

    .line 314
    :goto_4b
    return-void

    .line 305
    :cond_4c
    const/4 v0, 0x0

    iput-object v0, p0, Ldbxyzptlk/r/u;->c:Ljava/util/Date;

    goto :goto_3b

    .line 312
    :cond_50
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbxyzptlk/r/u;->e:Z

    goto :goto_4b
.end method

.method synthetic constructor <init>(Ljava/util/Map;Ldbxyzptlk/r/j;)V
    .registers 3
    .parameter
    .parameter

    .prologue
    .line 258
    invoke-direct {p0, p1}, Ldbxyzptlk/r/u;-><init>(Ljava/util/Map;)V

    return-void
.end method
