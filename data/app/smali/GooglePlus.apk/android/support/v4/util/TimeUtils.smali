.class public final Landroid/support/v4/util/TimeUtils;
.super Ljava/lang/Object;
.source "TimeUtils.java"


# static fields
.field private static sFormatStr:[C

.field private static final sFormatSync:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/support/v4/util/TimeUtils;->sFormatSync:Ljava/lang/Object;

    .line 36
    const/16 v0, 0x18

    new-array v0, v0, [C

    sput-object v0, Landroid/support/v4/util/TimeUtils;->sFormatStr:[C

    return-void
.end method

.method private static accumField(IIZI)I
    .registers 5
    .parameter "amt"
    .parameter "suffix"
    .parameter "always"
    .parameter "zeropad"

    .prologue
    .line 39
    const/16 v0, 0x63

    if-gt p0, v0, :cond_9

    if-eqz p2, :cond_c

    const/4 v0, 0x3

    if-lt p3, v0, :cond_c

    .line 40
    :cond_9
    add-int/lit8 v0, p1, 0x3

    .line 48
    :goto_b
    return v0

    .line 42
    :cond_c
    const/16 v0, 0x9

    if-gt p0, v0, :cond_15

    if-eqz p2, :cond_18

    const/4 v0, 0x2

    if-lt p3, v0, :cond_18

    .line 43
    :cond_15
    add-int/lit8 v0, p1, 0x2

    goto :goto_b

    .line 45
    :cond_18
    if-nez p2, :cond_1c

    if-lez p0, :cond_1f

    .line 46
    :cond_1c
    add-int/lit8 v0, p1, 0x1

    goto :goto_b

    .line 48
    :cond_1f
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static formatDuration(JJLjava/io/PrintWriter;)V
    .registers 8
    .parameter "time"
    .parameter "now"
    .parameter "pw"

    .prologue
    .line 169
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-nez v0, :cond_c

    .line 170
    const-string v0, "--"

    invoke-virtual {p4, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 174
    :goto_b
    return-void

    .line 173
    :cond_c
    sub-long v0, p0, p2

    const/4 v2, 0x0

    invoke-static {v0, v1, p4, v2}, Landroid/support/v4/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;I)V

    goto :goto_b
.end method

.method public static formatDuration(JLjava/io/PrintWriter;)V
    .registers 4
    .parameter "duration"
    .parameter "pw"

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Landroid/support/v4/util/TimeUtils;->formatDuration(JLjava/io/PrintWriter;I)V

    .line 165
    return-void
.end method

.method private static formatDuration(JLjava/io/PrintWriter;I)V
    .registers 22
    .parameter "duration"
    .parameter "pw"
    .parameter "fieldLen"

    .prologue
    .line 156
    sget-object v16, Landroid/support/v4/util/TimeUtils;->sFormatSync:Ljava/lang/Object;

    monitor-enter v16

    .line 157
    const/4 v2, 0x0

    :try_start_4
    sget-object v3, Landroid/support/v4/util/TimeUtils;->sFormatStr:[C

    array-length v3, v3

    if-ge v3, v2, :cond_d

    new-array v2, v2, [C

    sput-object v2, Landroid/support/v4/util/TimeUtils;->sFormatStr:[C

    :cond_d
    sget-object v2, Landroid/support/v4/util/TimeUtils;->sFormatStr:[C

    const-wide/16 v3, 0x0

    cmp-long v3, p0, v3

    if-nez v3, :cond_2a

    const/4 v3, 0x0

    const/16 v4, 0x30

    aput-char v4, v2, v3

    const/4 v12, 0x1

    .line 158
    .end local p0
    .local v12, len:I
    :goto_1b
    new-instance v2, Ljava/lang/String;

    sget-object v3, Landroid/support/v4/util/TimeUtils;->sFormatStr:[C

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v12}, Ljava/lang/String;-><init>([CII)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 159
    monitor-exit v16

    return-void

    .line 157
    .end local v12           #len:I
    .restart local p0
    :cond_2a
    const-wide/16 v3, 0x0

    cmp-long v3, p0, v3

    if-lez v3, :cond_ae

    const/16 v3, 0x2b

    move v7, v3

    .end local p0
    :goto_33
    const-wide/16 v3, 0x3e8

    rem-long v3, p0, v3

    long-to-int v0, v3

    move/from16 v17, v0

    const-wide/16 v3, 0x3e8

    div-long v3, p0, v3

    long-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-int v6, v3

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const v8, 0x15180

    if-le v6, v8, :cond_56

    const v3, 0x15180

    div-int v3, v6, v3

    const v8, 0x15180

    mul-int/2addr v8, v3

    sub-int/2addr v6, v8

    :cond_56
    const/16 v8, 0xe10

    if-le v6, v8, :cond_c4

    div-int/lit16 v5, v6, 0xe10

    mul-int/lit16 v8, v5, 0xe10

    sub-int/2addr v6, v8

    move v15, v5

    move v5, v6

    :goto_61
    const/16 v6, 0x3c

    if-le v5, v6, :cond_c1

    div-int/lit8 v4, v5, 0x3c

    mul-int/lit8 v6, v4, 0x3c

    sub-int/2addr v5, v6

    move v13, v4

    move v14, v5

    :goto_6c
    const/4 v4, 0x0

    aput-char v7, v2, v4

    const/4 v5, 0x1

    const/16 v4, 0x64

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/support/v4/util/TimeUtils;->printField([CICIZI)I

    move-result v9

    const/16 v8, 0x68

    if-eq v9, v5, :cond_b8

    const/4 v10, 0x1

    :goto_7d
    const/4 v11, 0x0

    move-object v6, v2

    move v7, v15

    invoke-static/range {v6 .. v11}, Landroid/support/v4/util/TimeUtils;->printField([CICIZI)I

    move-result v9

    const/16 v8, 0x6d

    if-eq v9, v5, :cond_ba

    const/4 v10, 0x1

    :goto_89
    const/4 v11, 0x0

    move-object v6, v2

    move v7, v13

    invoke-static/range {v6 .. v11}, Landroid/support/v4/util/TimeUtils;->printField([CICIZI)I

    move-result v8

    const/16 v4, 0x73

    if-eq v8, v5, :cond_bc

    const/4 v6, 0x1

    :goto_95
    const/4 v7, 0x0

    move v3, v14

    move v5, v8

    invoke-static/range {v2 .. v7}, Landroid/support/v4/util/TimeUtils;->printField([CICIZI)I

    move-result v5

    const/16 v4, 0x6d

    const/4 v6, 0x1

    const/4 v7, 0x0

    move/from16 v3, v17

    invoke-static/range {v2 .. v7}, Landroid/support/v4/util/TimeUtils;->printField([CICIZI)I

    move-result v3

    const/16 v4, 0x73

    aput-char v4, v2, v3
    :try_end_aa
    .catchall {:try_start_4 .. :try_end_aa} :catchall_be

    add-int/lit8 v12, v3, 0x1

    goto/16 :goto_1b

    .restart local p0
    :cond_ae
    const/16 v3, 0x2d

    move-wide/from16 v0, p0

    neg-long v0, v0

    move-wide/from16 p0, v0

    move v7, v3

    goto/16 :goto_33

    .end local p0
    :cond_b8
    const/4 v10, 0x0

    goto :goto_7d

    :cond_ba
    const/4 v10, 0x0

    goto :goto_89

    :cond_bc
    const/4 v6, 0x0

    goto :goto_95

    .line 159
    :catchall_be
    move-exception v2

    monitor-exit v16

    throw v2

    :cond_c1
    move v13, v4

    move v14, v5

    goto :goto_6c

    :cond_c4
    move v15, v5

    move v5, v6

    goto :goto_61
.end method

.method private static printField([CICIZI)I
    .registers 9
    .parameter "formatStr"
    .parameter "amt"
    .parameter "suffix"
    .parameter "pos"
    .parameter "always"
    .parameter "zeropad"

    .prologue
    .line 53
    if-nez p4, :cond_4

    if-lez p1, :cond_3c

    .line 54
    :cond_4
    move v1, p3

    .line 55
    .local v1, startPos:I
    if-eqz p4, :cond_a

    const/4 v2, 0x3

    if-ge p5, v2, :cond_e

    :cond_a
    const/16 v2, 0x63

    if-le p1, v2, :cond_1a

    .line 56
    :cond_e
    div-int/lit8 v0, p1, 0x64

    .line 57
    .local v0, dig:I
    add-int/lit8 v2, v0, 0x30

    int-to-char v2, v2

    aput-char v2, p0, p3

    .line 58
    add-int/lit8 p3, p3, 0x1

    .line 59
    mul-int/lit8 v2, v0, 0x64

    sub-int/2addr p1, v2

    .line 61
    .end local v0           #dig:I
    :cond_1a
    if-eqz p4, :cond_1f

    const/4 v2, 0x2

    if-ge p5, v2, :cond_25

    :cond_1f
    const/16 v2, 0x9

    if-gt p1, v2, :cond_25

    if-eq v1, p3, :cond_31

    .line 62
    :cond_25
    div-int/lit8 v0, p1, 0xa

    .line 63
    .restart local v0       #dig:I
    add-int/lit8 v2, v0, 0x30

    int-to-char v2, v2

    aput-char v2, p0, p3

    .line 64
    add-int/lit8 p3, p3, 0x1

    .line 65
    mul-int/lit8 v2, v0, 0xa

    sub-int/2addr p1, v2

    .line 67
    .end local v0           #dig:I
    :cond_31
    add-int/lit8 v2, p1, 0x30

    int-to-char v2, v2

    aput-char v2, p0, p3

    .line 68
    add-int/lit8 p3, p3, 0x1

    .line 69
    aput-char p2, p0, p3

    .line 70
    add-int/lit8 p3, p3, 0x1

    .line 72
    .end local v1           #startPos:I
    :cond_3c
    return p3
.end method
