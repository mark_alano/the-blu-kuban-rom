.class Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoIcsImpl;
.super Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoStubImpl;
.source "AccessibilityNodeInfoCompat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AccessibilityNodeInfoIcsImpl"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 399
    invoke-direct {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat$AccessibilityNodeInfoStubImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public final addAction(Ljava/lang/Object;I)V
    .registers 3
    .parameter "info"
    .parameter "action"

    .prologue
    .line 417
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 418
    return-void
.end method

.method public final getBoundsInParent(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "outBounds"

    .prologue
    .line 437
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInParent(Landroid/graphics/Rect;)V

    .line 438
    return-void
.end method

.method public final getBoundsInScreen(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "outBounds"

    .prologue
    .line 442
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->getBoundsInScreen(Landroid/graphics/Rect;)V

    .line 443
    return-void
.end method

.method public final getContentDescription(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "info"

    .prologue
    .line 462
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getText(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 3
    .parameter "info"

    .prologue
    .line 477
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final obtain()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 402
    invoke-static {}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain()Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    return-object v0
.end method

.method public final obtain(Landroid/view/View;)Ljava/lang/Object;
    .registers 3
    .parameter "source"

    .prologue
    .line 407
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    return-object v0
.end method

.method public final obtain(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .parameter "info"

    .prologue
    .line 412
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-static {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/accessibility/AccessibilityNodeInfo;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    return-object v0
.end method

.method public final recycle(Ljava/lang/Object;)V
    .registers 2
    .parameter "info"

    .prologue
    .line 632
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->recycle()V

    .line 633
    return-void
.end method

.method public final setBoundsInParent(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "bounds"

    .prologue
    .line 542
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    .line 543
    return-void
.end method

.method public final setBoundsInScreen(Ljava/lang/Object;Landroid/graphics/Rect;)V
    .registers 3
    .parameter "info"
    .parameter "bounds"

    .prologue
    .line 547
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInScreen(Landroid/graphics/Rect;)V

    .line 548
    return-void
.end method

.method public final setClassName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "className"

    .prologue
    .line 562
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 563
    return-void
.end method

.method public final setEnabled(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "enabled"

    .prologue
    .line 577
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 578
    return-void
.end method

.method public final setPackageName(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "packageName"

    .prologue
    .line 597
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPackageName(Ljava/lang/CharSequence;)V

    .line 598
    return-void
.end method

.method public final setParent(Ljava/lang/Object;Landroid/view/View;)V
    .registers 3
    .parameter "info"
    .parameter "parent"

    .prologue
    .line 602
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;)V

    .line 603
    return-void
.end method

.method public final setScrollable(Ljava/lang/Object;Z)V
    .registers 3
    .parameter "info"
    .parameter "scrollable"

    .prologue
    .line 612
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 613
    return-void
.end method

.method public final setText(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .registers 3
    .parameter "info"
    .parameter "text"

    .prologue
    .line 627
    check-cast p1, Landroid/view/accessibility/AccessibilityNodeInfo;

    .end local p1
    invoke-virtual {p1, p2}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 628
    return-void
.end method
