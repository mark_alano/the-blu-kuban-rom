.class public final Lcom/google/android/apps/plus/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AudienceView:[I = null

.field public static final AudienceView_android_maxLines:I = 0x0

.field public static final CircleButton:[I = null

.field public static final CircleButton_android_drawableLeft:I = 0x4

.field public static final CircleButton_android_paddingLeft:I = 0x1

.field public static final CircleButton_android_paddingRight:I = 0x2

.field public static final CircleButton_android_text:I = 0x3

.field public static final CircleButton_android_textColor:I = 0x0

.field public static final CircleButton_circle_button_icon_spacing:I = 0x5

.field public static final CircleButton_circle_button_label_spacing:I = 0x6

.field public static final CircleListItemView:[I = null

.field public static final CircleListItemView_circle_list_item_avatar_size:I = 0x8

.field public static final CircleListItemView_circle_list_item_avatar_spacing:I = 0x9

.field public static final CircleListItemView_circle_list_item_circle_icon_size_large:I = 0xb

.field public static final CircleListItemView_circle_list_item_circle_icon_size_small:I = 0xa

.field public static final CircleListItemView_circle_list_item_gap_between_count_and_checkbox:I = 0xd

.field public static final CircleListItemView_circle_list_item_gap_between_icon_and_text:I = 0x5

.field public static final CircleListItemView_circle_list_item_gap_between_name_and_count:I = 0xc

.field public static final CircleListItemView_circle_list_item_height:I = 0x0

.field public static final CircleListItemView_circle_list_item_member_count_text_color:I = 0xe

.field public static final CircleListItemView_circle_list_item_name_text_bold:I = 0x7

.field public static final CircleListItemView_circle_list_item_name_text_size:I = 0x6

.field public static final CircleListItemView_circle_list_item_padding_bottom:I = 0x2

.field public static final CircleListItemView_circle_list_item_padding_left:I = 0x3

.field public static final CircleListItemView_circle_list_item_padding_right:I = 0x4

.field public static final CircleListItemView_circle_list_item_padding_top:I = 0x1

.field public static final ColumnGridView_Layout:[I = null

.field public static final ColumnGridView_Layout_layout_majorSpan:I = 0x2

.field public static final ColumnGridView_Layout_layout_minorSpan:I = 0x1

.field public static final ColumnGridView_Layout_layout_orientation:I = 0x0

.field public static final ConstrainedTextView:[I = null

.field public static final ConstrainedTextView_android_maxLines:I = 0x3

.field public static final ConstrainedTextView_android_textColor:I = 0x2

.field public static final ConstrainedTextView_android_textSize:I = 0x0

.field public static final ConstrainedTextView_android_textStyle:I = 0x1

.field public static final ContactListItemView:[I = null

.field public static final ContactListItemView_contact_list_item_action_button_background:I = 0xe

.field public static final ContactListItemView_contact_list_item_action_button_width:I = 0xf

.field public static final ContactListItemView_contact_list_item_circle_icon_drawable:I = 0x7

.field public static final ContactListItemView_contact_list_item_circle_icon_size:I = 0x8

.field public static final ContactListItemView_contact_list_item_circles_text_color:I = 0xa

.field public static final ContactListItemView_contact_list_item_circles_text_size:I = 0x9

.field public static final ContactListItemView_contact_list_item_email_icon_padding_left:I = 0x13

.field public static final ContactListItemView_contact_list_item_email_icon_padding_top:I = 0x12

.field public static final ContactListItemView_contact_list_item_gap_between_icon_and_circles:I = 0xc

.field public static final ContactListItemView_contact_list_item_gap_between_image_and_text:I = 0x5

.field public static final ContactListItemView_contact_list_item_gap_between_name_and_circles:I = 0xb

.field public static final ContactListItemView_contact_list_item_gap_between_text_and_button:I = 0xd

.field public static final ContactListItemView_contact_list_item_height:I = 0x0

.field public static final ContactListItemView_contact_list_item_name_text_size:I = 0x6

.field public static final ContactListItemView_contact_list_item_padding_bottom:I = 0x2

.field public static final ContactListItemView_contact_list_item_padding_left:I = 0x3

.field public static final ContactListItemView_contact_list_item_padding_right:I = 0x4

.field public static final ContactListItemView_contact_list_item_padding_top:I = 0x1

.field public static final ContactListItemView_contact_list_item_vertical_divider_padding:I = 0x11

.field public static final ContactListItemView_contact_list_item_vertical_divider_width:I = 0x10

.field public static final ExactLayout_Layout:[I = null

.field public static final ExactLayout_Layout_layout_center_horizontal_bound:I = 0x2

.field public static final ExactLayout_Layout_layout_center_vertical_bound:I = 0x3

.field public static final ExactLayout_Layout_layout_x:I = 0x0

.field public static final ExactLayout_Layout_layout_y:I = 0x1

.field public static final ExpandingScrollView:[I = null

.field public static final ExpandingScrollView_minExposureLand:I = 0x0

.field public static final ExpandingScrollView_minExposurePort:I = 0x1

.field public static final MultiWaveView:[I = null

.field public static final MultiWaveView_bottomChevronDrawable:I = 0x7

.field public static final MultiWaveView_directionDescriptions:I = 0x2

.field public static final MultiWaveView_feedbackCount:I = 0xc

.field public static final MultiWaveView_handleDrawable:I = 0x3

.field public static final MultiWaveView_hitRadius:I = 0x9

.field public static final MultiWaveView_horizontalOffset:I = 0xe

.field public static final MultiWaveView_leftChevronDrawable:I = 0x4

.field public static final MultiWaveView_rightChevronDrawable:I = 0x5

.field public static final MultiWaveView_snapMargin:I = 0xb

.field public static final MultiWaveView_targetDescriptions:I = 0x1

.field public static final MultiWaveView_targetDrawables:I = 0x0

.field public static final MultiWaveView_topChevronDrawable:I = 0x6

.field public static final MultiWaveView_verticalOffset:I = 0xd

.field public static final MultiWaveView_vibrationDuration:I = 0xa

.field public static final MultiWaveView_waveDrawable:I = 0x8

.field public static final ParticipantsGalleryFragment:[I = null

.field public static final ParticipantsGalleryFragment_backgroundColor:I = 0x0

.field public static final ParticipantsGalleryFragment_emptyMessage:I = 0x1

.field public static final ScaledLayout:[I = null

.field public static final ScaledLayout_scale:I = 0x0

.field public static final ScaledLayout_scaleHeight:I = 0x1

.field public static final ScaledLayout_scaleMargin:I = 0x3

.field public static final ScaledLayout_scaleMarginBottom:I = 0x5

.field public static final ScaledLayout_scaleMarginLeft:I = 0x6

.field public static final ScaledLayout_scaleMarginMode:I = 0x8

.field public static final ScaledLayout_scaleMarginRight:I = 0x7

.field public static final ScaledLayout_scaleMarginTop:I = 0x4

.field public static final ScaledLayout_scaleWidth:I = 0x2

.field public static final TabButton:[I = null

.field public static final TabButton_android_icon:I = 0x0

.field public static final TabButton_android_text:I = 0x1

.field public static final Theme:[I = null

.field public static final Theme_actionDropDownStyle:I = 0x6

.field public static final Theme_buttonBarButtonStyle:I = 0x1

.field public static final Theme_buttonBarStyle:I = 0x0

.field public static final Theme_buttonSelectableBackground:I = 0x5

.field public static final Theme_editAudienceBackground:I = 0x3

.field public static final Theme_editLocationBackground:I = 0x4

.field public static final Theme_listSelector:I = 0x2

.field public static final Theme_navigationItemHeight:I = 0x7

.field public static final Thermometer:[I = null

.field public static final Thermometer_background:I = 0x0

.field public static final Thermometer_foreground:I = 0x1

.field public static final Thermometer_orientation:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    const/16 v6, 0xf

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 6704
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010153

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->AudienceView:[I

    .line 6735
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_74

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->CircleButton:[I

    .line 6839
    new-array v0, v6, [I

    fill-array-data v0, :array_86

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->CircleListItemView:[I

    .line 7094
    new-array v0, v4, [I

    fill-array-data v0, :array_a8

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ColumnGridView_Layout:[I

    .line 7158
    new-array v0, v5, [I

    fill-array-data v0, :array_b2

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ConstrainedTextView:[I

    .line 7233
    const/16 v0, 0x14

    new-array v0, v0, [I

    fill-array-data v0, :array_be

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ContactListItemView:[I

    .line 7559
    new-array v0, v5, [I

    fill-array-data v0, :array_ea

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ExactLayout_Layout:[I

    .line 7630
    new-array v0, v3, [I

    fill-array-data v0, :array_f6

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ExpandingScrollView:[I

    .line 7707
    new-array v0, v6, [I

    fill-array-data v0, :array_fe

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->MultiWaveView:[I

    .line 7937
    new-array v0, v3, [I

    fill-array-data v0, :array_120

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ParticipantsGalleryFragment:[I

    .line 7986
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_128

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->ScaledLayout:[I

    .line 8134
    new-array v0, v3, [I

    fill-array-data v0, :array_13e

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->TabButton:[I

    .line 8173
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_146

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->Theme:[I

    .line 8277
    new-array v0, v4, [I

    fill-array-data v0, :array_15a

    sput-object v0, Lcom/google/android/apps/plus/R$styleable;->Thermometer:[I

    return-void

    .line 6735
    nop

    :array_74
    .array-data 0x4
        0x98t 0x0t 0x1t 0x1t
        0xd6t 0x0t 0x1t 0x1t
        0xd8t 0x0t 0x1t 0x1t
        0x4ft 0x1t 0x1t 0x1t
        0x6ft 0x1t 0x1t 0x1t
        0x4dt 0x0t 0x1t 0x7ft
        0x4et 0x0t 0x1t 0x7ft
    .end array-data

    .line 6839
    :array_86
    .array-data 0x4
        0x2at 0x0t 0x1t 0x7ft
        0x2bt 0x0t 0x1t 0x7ft
        0x2ct 0x0t 0x1t 0x7ft
        0x2dt 0x0t 0x1t 0x7ft
        0x2et 0x0t 0x1t 0x7ft
        0x2ft 0x0t 0x1t 0x7ft
        0x30t 0x0t 0x1t 0x7ft
        0x31t 0x0t 0x1t 0x7ft
        0x32t 0x0t 0x1t 0x7ft
        0x33t 0x0t 0x1t 0x7ft
        0x34t 0x0t 0x1t 0x7ft
        0x35t 0x0t 0x1t 0x7ft
        0x36t 0x0t 0x1t 0x7ft
        0x37t 0x0t 0x1t 0x7ft
        0x38t 0x0t 0x1t 0x7ft
    .end array-data

    .line 7094
    :array_a8
    .array-data 0x4
        0x48t 0x0t 0x1t 0x7ft
        0x49t 0x0t 0x1t 0x7ft
        0x4at 0x0t 0x1t 0x7ft
    .end array-data

    .line 7158
    :array_b2
    .array-data 0x4
        0x95t 0x0t 0x1t 0x1t
        0x97t 0x0t 0x1t 0x1t
        0x98t 0x0t 0x1t 0x1t
        0x53t 0x1t 0x1t 0x1t
    .end array-data

    .line 7233
    :array_be
    .array-data 0x4
        0x16t 0x0t 0x1t 0x7ft
        0x17t 0x0t 0x1t 0x7ft
        0x18t 0x0t 0x1t 0x7ft
        0x19t 0x0t 0x1t 0x7ft
        0x1at 0x0t 0x1t 0x7ft
        0x1bt 0x0t 0x1t 0x7ft
        0x1ct 0x0t 0x1t 0x7ft
        0x1dt 0x0t 0x1t 0x7ft
        0x1et 0x0t 0x1t 0x7ft
        0x1ft 0x0t 0x1t 0x7ft
        0x20t 0x0t 0x1t 0x7ft
        0x21t 0x0t 0x1t 0x7ft
        0x22t 0x0t 0x1t 0x7ft
        0x23t 0x0t 0x1t 0x7ft
        0x24t 0x0t 0x1t 0x7ft
        0x25t 0x0t 0x1t 0x7ft
        0x26t 0x0t 0x1t 0x7ft
        0x27t 0x0t 0x1t 0x7ft
        0x28t 0x0t 0x1t 0x7ft
        0x29t 0x0t 0x1t 0x7ft
    .end array-data

    .line 7559
    :array_ea
    .array-data 0x4
        0x4ft 0x0t 0x1t 0x7ft
        0x50t 0x0t 0x1t 0x7ft
        0x51t 0x0t 0x1t 0x7ft
        0x52t 0x0t 0x1t 0x7ft
    .end array-data

    .line 7630
    :array_f6
    .array-data 0x4
        0x4bt 0x0t 0x1t 0x7ft
        0x4ct 0x0t 0x1t 0x7ft
    .end array-data

    .line 7707
    :array_fe
    .array-data 0x4
        0x39t 0x0t 0x1t 0x7ft
        0x3at 0x0t 0x1t 0x7ft
        0x3bt 0x0t 0x1t 0x7ft
        0x3ct 0x0t 0x1t 0x7ft
        0x3dt 0x0t 0x1t 0x7ft
        0x3et 0x0t 0x1t 0x7ft
        0x3ft 0x0t 0x1t 0x7ft
        0x40t 0x0t 0x1t 0x7ft
        0x41t 0x0t 0x1t 0x7ft
        0x42t 0x0t 0x1t 0x7ft
        0x43t 0x0t 0x1t 0x7ft
        0x44t 0x0t 0x1t 0x7ft
        0x45t 0x0t 0x1t 0x7ft
        0x46t 0x0t 0x1t 0x7ft
        0x47t 0x0t 0x1t 0x7ft
    .end array-data

    .line 7937
    :array_120
    .array-data 0x4
        0x8t 0x0t 0x1t 0x7ft
        0x9t 0x0t 0x1t 0x7ft
    .end array-data

    .line 7986
    :array_128
    .array-data 0x4
        0xat 0x0t 0x1t 0x7ft
        0xbt 0x0t 0x1t 0x7ft
        0xct 0x0t 0x1t 0x7ft
        0xdt 0x0t 0x1t 0x7ft
        0xet 0x0t 0x1t 0x7ft
        0xft 0x0t 0x1t 0x7ft
        0x10t 0x0t 0x1t 0x7ft
        0x11t 0x0t 0x1t 0x7ft
        0x12t 0x0t 0x1t 0x7ft
    .end array-data

    .line 8134
    :array_13e
    .array-data 0x4
        0x2t 0x0t 0x1t 0x1t
        0x4ft 0x1t 0x1t 0x1t
    .end array-data

    .line 8173
    :array_146
    .array-data 0x4
        0x0t 0x0t 0x1t 0x7ft
        0x1t 0x0t 0x1t 0x7ft
        0x2t 0x0t 0x1t 0x7ft
        0x3t 0x0t 0x1t 0x7ft
        0x4t 0x0t 0x1t 0x7ft
        0x5t 0x0t 0x1t 0x7ft
        0x6t 0x0t 0x1t 0x7ft
        0x7t 0x0t 0x1t 0x7ft
    .end array-data

    .line 8277
    :array_15a
    .array-data 0x4
        0x13t 0x0t 0x1t 0x7ft
        0x14t 0x0t 0x1t 0x7ft
        0x15t 0x0t 0x1t 0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .prologue
    .line 6693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
