.class final Lcom/google/android/apps/plus/analytics/EsAnalytics$1;
.super Ljava/lang/Object;
.source "EsAnalytics.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$action:Lcom/google/android/apps/plus/analytics/OzActions;

.field final synthetic val$analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$extras:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V
    .registers 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    iput-object p4, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$action:Lcom/google/android/apps/plus/analytics/OzActions;

    iput-object p5, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$extras:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .registers 6

    .prologue
    .line 246
    iget-object v1, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    if-nez v0, :cond_15

    new-instance v0, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    invoke-direct {v0}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>()V

    :goto_d
    iget-object v3, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$action:Lcom/google/android/apps/plus/analytics/OzActions;

    iget-object v4, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$extras:Landroid/os/Bundle;

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)J

    .line 248
    return-void

    .line 246
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/EsAnalytics$1;->val$analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    goto :goto_d
.end method
