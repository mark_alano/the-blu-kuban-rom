.class public final enum Lcom/google/android/apps/plus/analytics/OzViews;
.super Ljava/lang/Enum;
.source "OzViews.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/plus/analytics/OzViews;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ADD_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ADD_PERSON_TO_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ADD_TO_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum ALBUMS_OF_USER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CIRCLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CIRCLE_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum COMMENT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONTACTS_CIRCLELIST:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONTACTS_SYNC_CONFIG:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_GROUP:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_INVITE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_ONE_ON_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_PARTICIPANT_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CONVERSATION_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum EVENT_THEMES:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum HANGOUT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum HANGOUT_PARTICIPANTS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum HANGOUT_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum HOME:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum INSTANT_UPLOAD_GALLERY:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_EVERYONE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_MANAGE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_NEARBY:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum LOOP_WHATS_HOT:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum MY_EVENTS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum NOTIFICATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum NOTIFICATIONS_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PEOPLE_BLOCKED:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PEOPLE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PHOTOS_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PHOTO_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PLATFORM_PLUS_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PLUSONE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum REMOVE_FROM_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum RESHARE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum VIDEO:Lcom/google/android/apps/plus/analytics/OzViews;

.field public static final enum WW_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzViews;


# instance fields
.field private final mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

.field private final mViewData:Lcom/google/api/services/plusi/model/OutputData;


# direct methods
.method static constructor <clinit>()V
    .registers 15

    .prologue
    const/4 v2, 0x4

    const/16 v14, 0xa

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    .line 21
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "UNKNOWN"

    const/4 v3, 0x0

    const-string v4, "str"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 22
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "HOME"

    const-string v3, "str"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v11, v3, v4}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 23
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "NOTIFICATIONS"

    const-string v3, "str"

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v12, v3, v4}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 24
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "GENERAL_SETTINGS"

    const-string v3, "Settings"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v13, v3, v4}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 26
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LOOP_EVERYONE"

    const-string v3, "str"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_EVERYONE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 27
    new-instance v3, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v4, "LOOP_CIRCLES"

    const/4 v5, 0x5

    const-string v6, "str"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 28
    new-instance v3, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v4, "LOOP_NEARBY"

    const/4 v5, 0x6

    const-string v6, "str"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_NEARBY:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 29
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LOOP_MANAGE"

    const/4 v3, 0x7

    const-string v4, "str"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_MANAGE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 30
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LOOP_WHATS_HOT"

    const/16 v3, 0x8

    const-string v4, "xplr"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_WHATS_HOT:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 31
    new-instance v3, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v4, "LOOP_USER"

    const/16 v5, 0x9

    const-string v6, "pr"

    const/4 v7, 0x0

    const-string v8, "pr"

    const/4 v9, 0x0

    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 33
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "COMPOSE"

    const-string v3, "ttn"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v0, v1, v14, v3, v4}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 34
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "LOCATION_PICKER"

    const/16 v3, 0xb

    const-string v4, "ttn"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 35
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CIRCLE_PICKER"

    const/16 v3, 0xc

    const-string v4, "ttn"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CIRCLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 36
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PEOPLE_PICKER"

    const/16 v3, 0xd

    const-string v4, "ttn"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 37
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "COMMENT"

    const/16 v3, 0xe

    const-string v4, "ttn"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMMENT:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 38
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "SHARE"

    const/16 v3, 0xf

    const-string v4, "ttn"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 39
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "RESHARE"

    const/16 v3, 0x10

    const-string v4, "ttn"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->RESHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 41
    new-instance v3, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v4, "ACTIVITY"

    const/16 v5, 0x11

    const-string v6, "pr"

    const/4 v7, 0x0

    const-string v8, "plu"

    const/4 v9, 0x0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V

    sput-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 42
    new-instance v3, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v4, "PROFILE"

    const/16 v5, 0x12

    const-string v6, "pr"

    const/4 v7, 0x0

    const-string v8, "pr"

    const/4 v9, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    sput-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 43
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CIRCLE_SETTINGS"

    const/16 v3, 0x13

    const-string v4, "Settings"

    const/16 v5, 0xb

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CIRCLE_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 44
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PEOPLE_IN_CIRCLES"

    const/16 v3, 0x14

    const-string v4, "sg"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 45
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "ADD_CIRCLE"

    const/16 v3, 0x15

    const-string v4, "sg"

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 46
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "ADD_TO_CIRCLE"

    const/16 v3, 0x16

    const-string v4, "sg"

    const/16 v5, 0xe

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_TO_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 47
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PEOPLE_SEARCH"

    const/16 v3, 0x17

    const-string v4, "pr"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 48
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "SEARCH"

    const/16 v3, 0x18

    const-string v4, "se"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 49
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PLUSONE"

    const/16 v3, 0x19

    const-string v4, "plusone"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLUSONE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 50
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "REMOVE_FROM_CIRCLE"

    const/16 v3, 0x1a

    const-string v4, "sg"

    const/16 v5, 0xb

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->REMOVE_FROM_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 51
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "ADD_PERSON_TO_CIRCLES"

    const/16 v3, 0x1b

    const-string v4, "sg"

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_PERSON_TO_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 52
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PEOPLE_BLOCKED"

    const/16 v3, 0x1c

    const-string v4, "sg"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_BLOCKED:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 54
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "WW_SUGGESTIONS"

    const/16 v3, 0x1d

    const-string v4, "getstarted"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->WW_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 56
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PHOTO"

    const/16 v3, 0x1e

    const-string v4, "phst"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 57
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PHOTOS_HOME"

    const/16 v3, 0x1f

    const-string v4, "phst"

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 58
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PHOTOS_LIST"

    const/16 v3, 0x20

    const-string v4, "phst"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 59
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PHOTO_PICKER"

    const/16 v3, 0x21

    const-string v4, "ttn"

    const/16 v5, 0x1d

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 60
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "VIDEO"

    const/16 v3, 0x22

    const-string v4, "lightbox2"

    const/16 v5, 0x1b

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->VIDEO:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 61
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "ALBUMS_OF_USER"

    const/16 v3, 0x23

    const-string v4, "pr"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ALBUMS_OF_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 62
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "INSTANT_UPLOAD_GALLERY"

    const/16 v3, 0x24

    const-string v4, "phst"

    const/16 v5, 0x1e

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->INSTANT_UPLOAD_GALLERY:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 64
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATIONS"

    const/16 v3, 0x25

    const-string v4, "messenger"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 65
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_GROUP"

    const/16 v3, 0x26

    const-string v4, "messenger"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_GROUP:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 66
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_ONE_ON_ONE"

    const/16 v3, 0x27

    const-string v4, "messenger"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_ONE_ON_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 67
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_START_NEW"

    const/16 v3, 0x28

    const-string v4, "messenger"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 68
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_PARTICIPANT_LIST"

    const/16 v3, 0x29

    const-string v4, "messenger"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_PARTICIPANT_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 69
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONVERSATION_INVITE"

    const/16 v3, 0x2a

    const-string v4, "messenger"

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_INVITE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 71
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "HANGOUT"

    const/16 v3, 0x2b

    const-string v4, "h"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 72
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "HANGOUT_START_NEW"

    const/16 v3, 0x2c

    const-string v4, "h"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 73
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "HANGOUT_PARTICIPANTS"

    const/16 v3, 0x2d

    const-string v4, "h"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_PARTICIPANTS:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 75
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "NOTIFICATIONS_WIDGET"

    const/16 v3, 0x2e

    const-string v4, "nots"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 76
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "NOTIFICATIONS_CIRCLE"

    const/16 v3, 0x2f

    const-string v4, "nots"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 77
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "NOTIFICATIONS_SYSTEM"

    const/16 v3, 0x30

    const-string v4, "nots"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 79
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONTACTS_CIRCLELIST"

    const/16 v3, 0x31

    const-string v4, "sg"

    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_CIRCLELIST:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 80
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CONTACTS_SYNC_CONFIG"

    const/16 v3, 0x32

    const-string v4, "settings"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_SYNC_CONFIG:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 82
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PLATFORM_PLUS_ONE"

    const/16 v3, 0x33

    const-string v4, "plusone"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_PLUS_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 83
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "PLATFORM_THIRD_PARTY_APP"

    const/16 v3, 0x34

    const-string v4, "plusone"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 85
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "EVENT"

    const/16 v3, 0x35

    const-string v4, "oevt"

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 86
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "CREATE_EVENT"

    const/16 v3, 0x36

    const-string v4, "oevt"

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 87
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "MY_EVENTS"

    const/16 v3, 0x37

    const-string v4, "oevt"

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->MY_EVENTS:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 88
    new-instance v0, Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v1, "EVENT_THEMES"

    const/16 v3, 0x38

    const-string v4, "oevt"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT_THEMES:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 18
    const/16 v0, 0x39

    new-array v0, v0, [Lcom/google/android/apps/plus/analytics/OzViews;

    const/4 v1, 0x0

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v3, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v12

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v13

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_EVERYONE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v2

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_NEARBY:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_MANAGE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_WHATS_HOT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v1, v0, v14

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CIRCLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->COMMENT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->RESHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CIRCLE_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_TO_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLUSONE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->REMOVE_FROM_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ADD_PERSON_TO_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_BLOCKED:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->WW_SUGGESTIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->VIDEO:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->ALBUMS_OF_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->INSTANT_UPLOAD_GALLERY:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATIONS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_GROUP:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_ONE_ON_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_PARTICIPANT_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_INVITE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_PARTICIPANTS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_CIRCLE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_CIRCLELIST:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_SYNC_CONFIG:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_PLUS_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->MY_EVENTS:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT_THEMES:Lcom/google/android/apps/plus/analytics/OzViews;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->$VALUES:[Lcom/google/android/apps/plus/analytics/OzViews;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;)V
    .registers 12
    .parameter
    .parameter
    .parameter "namespace"
    .parameter "typeNum"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 98
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 99
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .registers 13
    .parameter
    .parameter
    .parameter "namespace"
    .parameter "typeNum"
    .parameter "filter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 102
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 103
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V
    .registers 15
    .parameter
    .parameter
    .parameter "namespace"
    .parameter "typeNum"
    .parameter "typeStr"
    .parameter "filter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 106
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/OzViews;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 107
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .registers 9
    .parameter
    .parameter
    .parameter "namespace"
    .parameter "typeNum"
    .parameter "typeStr"
    .parameter "filter"
    .parameter "tab"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 111
    new-instance v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iput-object p3, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->namespace:Ljava/lang/String;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iput-object p4, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeNum:Ljava/lang/Integer;

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    iput-object p5, v0, Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;->typeStr:Ljava/lang/String;

    .line 116
    if-nez p6, :cond_1a

    if-eqz p7, :cond_2e

    .line 117
    :cond_1a
    new-instance v0, Lcom/google/api/services/plusi/model/OutputData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutputData;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mViewData:Lcom/google/api/services/plusi/model/OutputData;

    .line 119
    if-eqz p6, :cond_27

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mViewData:Lcom/google/api/services/plusi/model/OutputData;

    iput-object p6, v0, Lcom/google/api/services/plusi/model/OutputData;->filterType:Ljava/lang/Integer;

    .line 123
    :cond_27
    if-eqz p7, :cond_2d

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mViewData:Lcom/google/api/services/plusi/model/OutputData;

    iput-object p7, v0, Lcom/google/api/services/plusi/model/OutputData;->tab:Ljava/lang/Integer;

    .line 129
    :cond_2d
    :goto_2d
    return-void

    .line 127
    :cond_2e
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mViewData:Lcom/google/api/services/plusi/model/OutputData;

    goto :goto_2d
.end method

.method public static getName(Lcom/google/android/apps/plus/analytics/OzViews;)Ljava/lang/String;
    .registers 2
    .parameter "view"

    .prologue
    .line 159
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_3
    return-object v0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/OzViews;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2
    .parameter "context"

    .prologue
    .line 163
    if-eqz p0, :cond_d

    instance-of v0, p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    if-eqz v0, :cond_d

    .line 164
    check-cast p0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    .end local p0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    .line 167
    :goto_c
    return-object v0

    .restart local p0
    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public static valueOf(I)Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 3
    .parameter "number"

    .prologue
    .line 140
    invoke-static {}, Lcom/google/android/apps/plus/analytics/OzViews;->values()[Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    .line 141
    .local v0, values:[Lcom/google/android/apps/plus/analytics/OzViews;
    if-eqz v0, :cond_e

    if-ltz p0, :cond_e

    array-length v1, v0

    if-ge p0, v1, :cond_e

    aget-object v1, v0, p0

    :goto_d
    return-object v1

    :cond_e
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_d
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2
    .parameter "name"

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->$VALUES:[Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual {v0}, [Lcom/google/android/apps/plus/analytics/OzViews;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method


# virtual methods
.method public final getFavaDiagnosticsNamespacedType()Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;
    .registers 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mFavaDiagnosticsNamespacedType:Lcom/google/api/services/plusi/model/FavaDiagnosticsNamespacedType;

    return-object v0
.end method

.method public final getViewData()Lcom/google/api/services/plusi/model/OutputData;
    .registers 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/plus/analytics/OzViews;->mViewData:Lcom/google/api/services/plusi/model/OutputData;

    return-object v0
.end method
