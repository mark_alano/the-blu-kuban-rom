.class public final Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;
.super Lcom/google/android/apps/plus/api/PlusOneOperation;
.source "CommentOptimisticPlusOneOperation.java"


# instance fields
.field private mActivityId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activityId"
    .parameter "commentId"
    .parameter "isPlusOne"

    .prologue
    .line 34
    const-string v5, "TACO_COMMENT"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p6

    move v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 35
    iput-object p5, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method protected final onFailure()V
    .registers 6

    .prologue
    .line 56
    iget-object v1, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mIsPlusOne:Z

    if-nez v0, :cond_11

    const/4 v0, 0x1

    :goto_d
    invoke-static {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/content/EsPostsData;->plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 57
    return-void

    .line 56
    :cond_11
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected final onPopulateRequest()V
    .registers 6

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mIsPlusOne:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 43
    return-void
.end method

.method protected final onSuccess(Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .registers 7
    .parameter "plusOneData"

    .prologue
    .line 47
    if-eqz p1, :cond_f

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mActivityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->updateCommentPlusOneId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_f
    return-void
.end method
