.class public final Lcom/google/android/apps/plus/api/DeletePhotosOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "DeletePhotosOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/DeletePhotosRequest;",
        "Lcom/google/api/services/plusi/model/DeletePhotosResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mPhotoIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter "intent"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    .local p3, photoIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    const-string v3, "deletephotos"

    invoke-static {}, Lcom/google/api/services/plusi/model/DeletePhotosRequestJson;->getInstance()Lcom/google/api/services/plusi/model/DeletePhotosRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/DeletePhotosResponseJson;->getInstance()Lcom/google/api/services/plusi/model/DeletePhotosResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 33
    iput-object p3, p0, Lcom/google/android/apps/plus/api/DeletePhotosOperation;->mPhotoIds:Ljava/util/List;

    .line 34
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 5
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/DeletePhotosResponse;

    .end local p1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/DeletePhotosOperation;->onStartResultProcessing()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/DeletePhotosResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/apps/plus/api/DeletePhotosOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/DeletePhotosOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/DeletePhotosOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/DeletePhotosOperation;->mPhotoIds:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->deletePhotos$43585934(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    :cond_1b
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/DeletePhotosRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/DeletePhotosOperation;->mPhotoIds:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/DeletePhotosRequest;->photoId:Ljava/util/List;

    return-void
.end method
