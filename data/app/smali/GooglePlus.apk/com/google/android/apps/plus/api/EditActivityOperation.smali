.class public final Lcom/google/android/apps/plus/api/EditActivityOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "EditActivityOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EditActivityRequest;",
        "Lcom/google/api/services/plusi/model/EditActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mContent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activityId"
    .parameter "content"

    .prologue
    .line 40
    const-string v3, "editactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/EditActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EditActivityRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EditActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EditActivityResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 47
    iput-object p5, p0, Lcom/google/android/apps/plus/api/EditActivityOperation;->mActivityId:Ljava/lang/String;

    .line 48
    iput-object p6, p0, Lcom/google/android/apps/plus/api/EditActivityOperation;->mContent:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 9
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/EditActivityResponse;

    .end local p1
    if-eqz p1, :cond_23

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EditActivityResponse;->update:Lcom/google/api/services/plusi/model/Update;

    if-eqz v0, :cond_23

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditActivityOperation;->mActivityId:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    new-instance v0, Lcom/google/android/apps/plus/api/GetActivityOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/EditActivityOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/EditActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/EditActivityOperation;->mActivityId:Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/GetActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->start()V

    :cond_23
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/EditActivityRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditActivityOperation;->mActivityId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->externalId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/EditActivityOperation;->mContent:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->updateText:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EditActivityRequest;->preserveExistingAttachment:Ljava/lang/Boolean;

    return-void
.end method
