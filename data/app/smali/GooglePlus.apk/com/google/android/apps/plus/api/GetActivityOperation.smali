.class public final Lcom/google/android/apps/plus/api/GetActivityOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetActivityOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetActivityRequest;",
        "Lcom/google/api/services/plusi/model/GetActivityResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mOwnerGaiaId:Ljava/lang/String;

.field private mResponseUpdateId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "activityId"
    .parameter "ownerGaiaId"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 34
    const-string v3, "getactivity"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivityRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivityRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetActivityResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetActivityResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 36
    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mActivityId:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mOwnerGaiaId:Ljava/lang/String;

    .line 38
    return-void
.end method


# virtual methods
.method public final getResponseUpdateId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mResponseUpdateId:Ljava/lang/String;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 7
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    check-cast p1, Lcom/google/api/services/plusi/model/GetActivityResponse;

    .end local p1
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetActivityResponse;->update:Lcom/google/api/services/plusi/model/Update;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    const-string v4, "DEFAULT"

    invoke-static {v1, v2, v3, v0, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->insertActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/Update;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Update;->updateId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mResponseUpdateId:Ljava/lang/String;

    :cond_2b
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 26
    check-cast p1, Lcom/google/api/services/plusi/model/GetActivityRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mActivityId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityRequest;->activityId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mOwnerGaiaId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetActivityOperation;->mOwnerGaiaId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityRequest;->ownerId:Ljava/lang/String;

    :cond_12
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetActivityRequest;->fetchReadState:Ljava/lang/Boolean;

    return-void
.end method
