.class public final Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetCelebritySuggestionsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequest;",
        "Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 36
    const-string v3, "getcelebritysuggestions"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 5
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;->category:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->insertCelebritySuggestions(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 21
    check-cast p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequest;

    .end local p1
    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetCelebritySuggestionsRequest;->maxPerCategory:Ljava/lang/Integer;

    return-void
.end method
