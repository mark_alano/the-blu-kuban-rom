.class public final Lcom/google/android/apps/plus/api/GetNotificationsOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetNotificationsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetNotificationsRequest;",
        "Lcom/google/api/services/plusi/model/GetNotificationsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mContinuationToken:Ljava/lang/String;

.field private mLastNotificationTime:D

.field private mLastReadTime:Ljava/lang/Double;

.field private mNotifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCoalescedItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 51
    const-string v3, "getnotifications"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetNotificationsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetNotificationsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetNotificationsResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 56
    return-void
.end method

.method public static getMaxNotifications()J
    .registers 2

    .prologue
    .line 151
    const-wide/16 v0, 0xf

    return-wide v0
.end method


# virtual methods
.method public final getContinuationToken()Ljava/lang/String;
    .registers 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getLastReadTime()Ljava/lang/Double;
    .registers 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastReadTime:Ljava/lang/Double;

    return-object v0
.end method

.method public final getNotifications()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCoalescedItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mNotifications:Ljava/util/List;

    return-object v0
.end method

.method public final getNotifications(DLjava/lang/String;)V
    .registers 4
    .parameter "lastNotificationTime"
    .parameter "continuationToken"

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastNotificationTime:D

    .line 67
    iput-object p3, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    .line 68
    return-void
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/google/api/services/plusi/model/GetNotificationsResponse;

    .end local p1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->onStartResultProcessing()V

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsResponse;->notificationsData:Lcom/google/api/services/plusi/model/DataNotificationsData;

    if-eqz v0, :cond_15

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationsData;->continuationToken:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationsData;->lastReadTime:Ljava/lang/Double;

    iput-object v1, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastReadTime:Ljava/lang/Double;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationsData;->coalescedItem:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mNotifications:Ljava/util/List;

    :cond_15
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 8
    .parameter "x0"

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 28
    check-cast p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;

    .end local p1
    const-wide/16 v0, 0xf

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->maxResults:Ljava/lang/Long;

    const-string v0, "NOTIFICATION_MOBILE"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->renderContextLocation:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->continuationToken:Ljava/lang/String;

    :cond_18
    iget-wide v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastNotificationTime:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2c

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->mLastNotificationTime:D

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toBigInteger()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->oldestNotificationTimeUsec:Ljava/math/BigInteger;

    :cond_2c
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "PLAIN"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->summarySnippets:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->setPushEnabled:Ljava/lang/Boolean;

    new-instance v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;-><init>()V

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;->includeFullActorDetails:Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;->includeFullEntityDetails:Ljava/lang/Boolean;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;->includeFullRootDetails:Ljava/lang/Boolean;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/NotificationsResponseOptions;->numPhotoEntities:Ljava/lang/Integer;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetNotificationsRequest;->notificationsResponseOptions:Lcom/google/api/services/plusi/model/NotificationsResponseOptions;

    return-void
.end method
