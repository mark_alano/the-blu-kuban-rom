.class public final Lcom/google/android/apps/plus/api/GetPhotoOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetPhotoOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetPhotoRequest;",
        "Lcom/google/api/services/plusi/model/GetPhotoResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mPhotoId:J

.field private final mUserId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;JLjava/lang/String;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "photoId"
    .parameter "userId"

    .prologue
    .line 32
    const-string v3, "getphoto"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetPhotoRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetPhotoRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetPhotoResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetPhotoResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 35
    iput-wide p5, p0, Lcom/google/android/apps/plus/api/GetPhotoOperation;->mPhotoId:J

    .line 36
    iput-object p7, p0, Lcom/google/android/apps/plus/api/GetPhotoOperation;->mUserId:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 7
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/GetPhotoResponse;

    .end local p1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/GetPhotoOperation;->onStartResultProcessing()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPhotoOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetPhotoOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/google/api/services/plusi/model/GetPhotoResponse;->photo:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/GetPhotoResponse;->isDownloadable:Ljava/lang/Boolean;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsPhotosDataApiary;->insertPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/api/services/plusi/model/DataPhoto;Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 5
    .parameter "x0"

    .prologue
    const/4 v2, 0x1

    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/GetPhotoRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetPhotoOperation;->mUserId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetPhotoRequest;->ownerId:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;-><init>()V

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnAlbumInfo:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnComments:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnDownloadability:Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnOwnerInfo:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPhotos:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnPlusOnes:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnShapes:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/RequestsPhotoOptions;->returnVideoUrls:Ljava/lang/Boolean;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetPhotoRequest;->photoOptions:Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    iget-wide v0, p0, Lcom/google/android/apps/plus/api/GetPhotoOperation;->mPhotoId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/GetPhotoRequest;->photoId:Ljava/lang/String;

    return-void
.end method
