.class public final Lcom/google/android/apps/plus/api/GetSettingsOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "GetSettingsOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/GetMobileSettingsRequest;",
        "Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

.field private mSetupAccount:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "setupAccount"
    .parameter "intent"
    .parameter "listener"

    .prologue
    const/4 v6, 0x0

    .line 48
    const-string v3, "getmobilesettings"

    invoke-static {}, Lcom/google/api/services/plusi/model/GetMobileSettingsRequestJson;->getInstance()Lcom/google/api/services/plusi/model/GetMobileSettingsRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;->getInstance()Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 51
    iput-boolean p3, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSetupAccount:Z

    .line 52
    return-void
.end method


# virtual methods
.method public final getAccountSettings()Lcom/google/android/apps/plus/content/AccountSettingsData;
    .registers 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 10
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 31
    check-cast p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;

    .end local p1
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    if-eqz v1, :cond_1a

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    if-eqz v1, :cond_1a

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_29

    :cond_1a
    const-string v0, "HttpTransaction"

    const-string v1, "Settings response missing gaid ID"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Settings response missing gaid ID"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_29
    iget-object v5, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    iget-object v6, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->info:Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;

    iget-object v1, v6, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->displayName:Ljava/lang/String;

    iget-object v4, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isChild:Ljava/lang/Boolean;

    if-eqz v4, :cond_5e

    iget-object v4, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isChild:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_5e

    move v4, v0

    :goto_3e
    iget-boolean v7, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSetupAccount:Z

    if-eqz v7, :cond_95

    iget-object v7, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v7

    if-nez v7, :cond_60

    iget-object v7, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->plusPageInfo:Ljava/util/List;

    if-eqz v7, :cond_60

    iget-object v7, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->plusPageInfo:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_60

    new-instance v0, Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/content/AccountSettingsData;-><init>(Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    :goto_5d
    return-void

    :cond_5e
    move v4, v2

    goto :goto_3e

    :cond_60
    iget-object v7, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v7

    if-nez v7, :cond_74

    iget-object v7, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isPlusPage:Ljava/lang/Boolean;

    if-eqz v7, :cond_93

    iget-object v5, v5, Lcom/google/api/services/plusi/model/MobileSettingsUser;->isPlusPage:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_93

    :cond_74
    move v5, v0

    :goto_75
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->insertAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mContext:Landroid/content/Context;

    iget-object v2, v6, Lcom/google/api/services/plusi/model/MobileSettingsUserInfo;->photoUrl:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->activateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :goto_88
    new-instance v1, Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/content/AccountSettingsData;-><init>(Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mContext:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveServerSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V

    goto :goto_5d

    :cond_93
    move v5, v2

    goto :goto_75

    :cond_95
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v5, v1, v3, v4}, Lcom/google/android/apps/plus/content/EsAccountsData;->updateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_88
.end method

.method public final hasPlusPages()Z
    .registers 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/plus/api/GetSettingsOperation;->mSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 2
    .parameter "x0"

    .prologue
    .line 31
    return-void
.end method
