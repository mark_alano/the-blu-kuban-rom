.class public final Lcom/google/android/apps/plus/api/MuteUserOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "MuteUserOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/MuteUserRequest;",
        "Lcom/google/api/services/plusi/model/MuteUserResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mGaiaId:Ljava/lang/String;

.field private final mIsMute:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "gaiaId"
    .parameter "isMute"

    .prologue
    .line 37
    const-string v3, "muteuser"

    invoke-static {}, Lcom/google/api/services/plusi/model/MuteUserRequestJson;->getInstance()Lcom/google/api/services/plusi/model/MuteUserRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/MuteUserResponseJson;->getInstance()Lcom/google/api/services/plusi/model/MuteUserResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 44
    iput-object p5, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mGaiaId:Ljava/lang/String;

    .line 45
    iput-boolean p6, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mIsMute:Z

    .line 46
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    check-cast p1, Lcom/google/api/services/plusi/model/MuteUserResponse;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mGaiaId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/MuteUserResponse;->isMuted:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->changeMuteState(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Z

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 20
    check-cast p1, Lcom/google/api/services/plusi/model/MuteUserRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mGaiaId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MuteUserRequest;->obfuscatedGaiaId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/MuteUserOperation;->mIsMute:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/MuteUserRequest;->shouldMute:Ljava/lang/Boolean;

    return-void
.end method
