.class public final Lcom/google/android/apps/plus/api/OzServerException;
.super Lcom/google/android/apps/plus/api/ProtocolException;
.source "OzServerException.java"


# static fields
.field private static final serialVersionUID:J = -0x59eca412221c9248L


# instance fields
.field private final mError:Lcom/google/android/apps/plus/api/ApiaryErrorResponse;

.field private final mErrorCode:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/api/ApiaryErrorResponse;)V
    .registers 5
    .parameter "error"

    .prologue
    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getErrorType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/api/ProtocolException;-><init>(Ljava/lang/String;)V

    .line 47
    iput-object p1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mError:Lcom/google/android/apps/plus/api/ApiaryErrorResponse;

    .line 48
    invoke-virtual {p1}, Lcom/google/android/apps/plus/api/ApiaryErrorResponse;->getErrorType()Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, errorType:Ljava/lang/String;
    const-string v1, "INVALID_CREDENTIALS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 51
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    .line 99
    :goto_33
    return-void

    .line 53
    :cond_34
    const-string v1, "FORBIDDEN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_40

    .line 54
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 56
    :cond_40
    const-string v1, "NOT_FOUND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4c

    .line 57
    const/4 v1, 0x3

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 59
    :cond_4c
    const-string v1, "INVALID_VALUE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_58

    .line 60
    const/4 v1, 0x4

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 62
    :cond_58
    const-string v1, "SERVICE_UNAVAILABLE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_64

    .line 63
    const/4 v1, 0x5

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 65
    :cond_64
    const-string v1, "INVALID_ACTION_TOKEN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_70

    .line 66
    const/4 v1, 0x6

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 68
    :cond_70
    const-string v1, "PERMISSION_ERROR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7c

    .line 69
    const/4 v1, 0x7

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 71
    :cond_7c
    const-string v1, "NETWORK_ERROR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_89

    .line 72
    const/16 v1, 0x8

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 74
    :cond_89
    const-string v1, "OUT_OF_BOX_REQUIRED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_96

    .line 75
    const/16 v1, 0x9

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 77
    :cond_96
    const-string v1, "APP_UPGRADE_REQUIRED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a3

    .line 78
    const/16 v1, 0xa

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 80
    :cond_a3
    const-string v1, "HAS_PLUSONE_OPT_IN_REQUIRED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b0

    .line 81
    const/16 v1, 0xb

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto :goto_33

    .line 83
    :cond_b0
    const-string v1, "BAD_PROFILE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_be

    .line 84
    const/16 v1, 0xc

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto/16 :goto_33

    .line 86
    :cond_be
    const-string v1, "AGE_RESTRICTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_cc

    .line 87
    const/16 v1, 0xd

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto/16 :goto_33

    .line 89
    :cond_cc
    const-string v1, "ES_STREAM_POST_RESTRICTIONS_NOT_SUPPORTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_da

    .line 91
    const/16 v1, 0xe

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto/16 :goto_33

    .line 93
    :cond_da
    const-string v1, "ES_BLOCKED_FOR_DOMAIN_BY_ADMIN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e8

    .line 94
    const/16 v1, 0xf

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto/16 :goto_33

    .line 97
    :cond_e8
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    goto/16 :goto_33
.end method


# virtual methods
.method public final getErrorCode()I
    .registers 2

    .prologue
    .line 105
    iget v0, p0, Lcom/google/android/apps/plus/api/OzServerException;->mErrorCode:I

    return v0
.end method
