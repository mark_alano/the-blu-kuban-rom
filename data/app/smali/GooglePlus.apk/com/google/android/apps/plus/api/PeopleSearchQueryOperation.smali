.class public final Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "PeopleSearchQueryOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SearchQueryRequest;",
        "Lcom/google/api/services/plusi/model/SearchQueryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContinuationToken:Ljava/lang/String;

.field private mInludePlusPages:Z

.field private mNewContinuationToken:Ljava/lang/String;

.field private mPeopleResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleResult;",
            ">;"
        }
    .end annotation
.end field

.field private final mQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "query"
    .parameter "continuationToken"
    .parameter "includePlusPages"
    .parameter "intent"
    .parameter "listener"

    .prologue
    const/4 v6, 0x0

    .line 48
    const-string v3, "searchquery"

    invoke-static {}, Lcom/google/api/services/plusi/model/SearchQueryRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SearchQueryRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SearchQueryResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SearchQueryResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 50
    iput-object p3, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mQuery:Ljava/lang/String;

    .line 51
    iput-object p4, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mContinuationToken:Ljava/lang/String;

    .line 52
    iput-boolean p5, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mInludePlusPages:Z

    .line 53
    return-void
.end method


# virtual methods
.method public final getContinuationToken()Ljava/lang/String;
    .registers 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mNewContinuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getPeopleSearchResults()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mPeopleResults:Ljava/util/List;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;

    .end local p1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    if-eqz v0, :cond_1c

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SearchResults;->peopleResults:Lcom/google/api/services/plusi/model/PeopleResults;

    if-eqz v0, :cond_1c

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SearchResults;->peopleResults:Lcom/google/api/services/plusi/model/PeopleResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PeopleResults;->shownPeopleBlob:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mNewContinuationToken:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryResponse;->results:Lcom/google/api/services/plusi/model/SearchResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SearchResults;->peopleResults:Lcom/google/api/services/plusi/model/PeopleResults;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PeopleResults;->result:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mPeopleResults:Ljava/util/List;

    :cond_1c
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 25
    check-cast p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;

    .end local p1
    new-instance v0, Lcom/google/api/services/plusi/model/SearchQuery;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SearchQuery;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mQuery:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/SearchQuery;->queryText:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->searchQuery:Lcom/google/api/services/plusi/model/SearchQuery;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mInludePlusPages:Z

    if-eqz v0, :cond_2b

    const-string v0, "PEOPLE"

    :goto_17
    iput-object v0, v1, Lcom/google/api/services/plusi/model/SearchQuery;->filter:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mContinuationToken:Ljava/lang/String;

    if-eqz v0, :cond_2a

    new-instance v0, Lcom/google/api/services/plusi/model/PeopleRequestData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PeopleRequestData;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->peopleRequestData:Lcom/google/api/services/plusi/model/PeopleRequestData;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/SearchQueryRequest;->peopleRequestData:Lcom/google/api/services/plusi/model/PeopleRequestData;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PeopleSearchQueryOperation;->mContinuationToken:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PeopleRequestData;->shownPeopleBlob:Ljava/lang/String;

    :cond_2a
    return-void

    :cond_2b
    const-string v0, "PEOPLE_ONLY"

    goto :goto_17
.end method
