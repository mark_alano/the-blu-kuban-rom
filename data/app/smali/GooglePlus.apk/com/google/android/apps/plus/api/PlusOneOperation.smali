.class public Lcom/google/android/apps/plus/api/PlusOneOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "PlusOneOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PlusOneRequest;",
        "Lcom/google/api/services/plusi/model/PlusOneResponse;",
        ">;"
    }
.end annotation


# instance fields
.field protected final mIsPlusOne:Z

.field protected final mItemId:Ljava/lang/String;

.field protected final mItemType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "itemType"
    .parameter "itemId"
    .parameter "isPlusOne"

    .prologue
    .line 56
    const-string v3, "plusone"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusOneRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PlusOneRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusOneResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PlusOneResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 63
    iput-object p5, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mItemType:Ljava/lang/String;

    .line 64
    iput-object p6, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mItemId:Ljava/lang/String;

    .line 65
    iput-boolean p7, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mIsPlusOne:Z

    .line 66
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/PlusOneResponse;

    .end local p1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneResponse;->success:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneResponse;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/api/PlusOneOperation;->onSuccess(Lcom/google/api/services/plusi/model/DataPlusOne;)V

    :goto_f
    return-void

    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PlusOneOperation;->onFailure()V

    goto :goto_f
.end method

.method protected onFailure()V
    .registers 1

    .prologue
    .line 84
    return-void
.end method

.method protected final onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .registers 5
    .parameter "errorCode"
    .parameter "reasonPhrase"
    .parameter "ex"

    .prologue
    .line 96
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_6

    if-eqz p3, :cond_9

    .line 97
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PlusOneOperation;->onFailure()V

    .line 99
    :cond_9
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/network/PlusiOperation;->onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V

    .line 100
    return-void
.end method

.method protected onPopulateRequest()V
    .registers 1

    .prologue
    .line 72
    return-void
.end method

.method protected onSuccess(Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .registers 2
    .parameter "plusOneData"

    .prologue
    .line 78
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 22
    check-cast p1, Lcom/google/api/services/plusi/model/PlusOneRequest;

    .end local p1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/PlusOneOperation;->onPopulateRequest()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mItemType:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneRequest;->type:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mItemId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneRequest;->itemId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PlusOneOperation;->mIsPlusOne:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PlusOneRequest;->isPlusone:Ljava/lang/Boolean;

    return-void
.end method
