.class public final Lcom/google/android/apps/plus/api/PostEventCommentOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "PostEventCommentOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/PostCommentRequest;",
        "Lcom/google/api/services/plusi/model/PostCommentResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final sRandom:Ljava/util/Random;


# instance fields
.field private final mActivityId:Ljava/lang/String;

.field private final mContent:Ljava/lang/String;

.field private final mEventId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 30
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->sRandom:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activityId"
    .parameter "eventId"
    .parameter "content"

    .prologue
    .line 49
    const-string v3, "postcomment"

    invoke-static {}, Lcom/google/api/services/plusi/model/PostCommentRequestJson;->getInstance()Lcom/google/api/services/plusi/model/PostCommentRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/PostCommentResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PostCommentResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 56
    iput-object p5, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mActivityId:Ljava/lang/String;

    .line 57
    iput-object p7, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mContent:Ljava/lang/String;

    .line 58
    iput-object p6, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mEventId:Ljava/lang/String;

    .line 59
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 8
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    check-cast p1, Lcom/google/api/services/plusi/model/PostCommentResponse;

    .end local p1
    if-eqz p1, :cond_83

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PostCommentResponse;->comment:Lcom/google/api/services/plusi/model/Comment;

    if-eqz v0, :cond_83

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mActivityId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->updateId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_83

    new-instance v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;-><init>()V

    const/4 v2, 0x5

    iput v2, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->activityType:I

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->authorName:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerName:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->obfuscatedId:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/Comment;->timestamp:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->timestamp:J

    new-instance v2, Lcom/google/android/apps/plus/content/EsEventData$EventComment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/content/EsEventData$EventComment;-><init>()V

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->commentId:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->commentId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->text:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->text:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->isOwnedByViewer:Ljava/lang/Boolean;

    if-eqz v3, :cond_47

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->isOwnedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v2, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->ownedByViewer:Z

    :cond_47
    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v3, :cond_5c

    iget-object v3, v0, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    if-eqz v3, :cond_5c

    iget-object v0, v0, Lcom/google/api/services/plusi/model/Comment;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v3, v0

    iput-wide v3, v2, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->totalPlusOnes:J

    :cond_5c
    iget-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->ownerGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_83

    iget-object v0, v2, Lcom/google/android/apps/plus/content/EsEventData$EventComment;->text:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_83

    sget-object v0, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COMMENT_JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/json/EsJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/plus/content/EsEventData$EventActivity;->data:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mEventId:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsEventData;->insertEventActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    :cond_83
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"

    .prologue
    .line 28
    check-cast p1, Lcom/google/api/services/plusi/model/PostCommentRequest;

    .end local p1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->sRandom:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->clientId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mActivityId:Ljava/lang/String;

    iput-object v2, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->activityId:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->creationTimeMs:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->mContent:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/PostCommentRequest;->commentText:Ljava/lang/String;

    return-void
.end method
