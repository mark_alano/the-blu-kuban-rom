.class public final Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;
.super Lcom/google/android/apps/plus/api/PlusOneOperation;
.source "PostOptimisticPlusOneOperation.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "activityId"
    .parameter "isPlusOne"

    .prologue
    .line 31
    const-string v5, "TACO"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 32
    return-void
.end method


# virtual methods
.method protected final onFailure()V
    .registers 5

    .prologue
    .line 50
    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mIsPlusOne:Z

    if-nez v0, :cond_f

    const/4 v0, 0x1

    :goto_b
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsPostsData;->plusOnePost(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 51
    return-void

    .line 50
    :cond_f
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected final onPopulateRequest()V
    .registers 5

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mIsPlusOne:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->plusOnePost(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/DbPlusOneData;

    .line 38
    return-void
.end method

.method protected final onSuccess(Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .registers 6
    .parameter "plusOneData"

    .prologue
    .line 42
    if-eqz p1, :cond_d

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->mItemId:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->updatePostPlusOneId(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    :cond_d
    return-void
.end method
