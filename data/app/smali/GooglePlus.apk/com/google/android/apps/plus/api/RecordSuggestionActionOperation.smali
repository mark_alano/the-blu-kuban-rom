.class public final Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "RecordSuggestionActionOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/RecordSuggestionRequest;",
        "Lcom/google/api/services/plusi/model/RecordSuggestionResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mPersonIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mSuggestionsUi:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "suggestionsUi"
    .parameter
    .parameter "intent"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p4, personIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 53
    const-string v3, "recordsuggestion"

    invoke-static {}, Lcom/google/api/services/plusi/model/RecordSuggestionRequestJson;->getInstance()Lcom/google/api/services/plusi/model/RecordSuggestionRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/RecordSuggestionResponseJson;->getInstance()Lcom/google/api/services/plusi/model/RecordSuggestionResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mSuggestionsUi:Ljava/lang/String;

    .line 56
    iput-object p4, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mPersonIds:Ljava/util/List;

    .line 57
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 8
    .parameter "x0"

    .prologue
    const/4 v5, 0x2

    .line 27
    check-cast p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;

    .end local p1
    new-instance v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataSuggestionAction;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->accepted:Ljava/lang/Boolean;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    const-string v1, "REJECT"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->actionType:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mSuggestionsUi:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionUi:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestedEntityId:Ljava/util/List;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionId:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->mPersonIds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_37
    :goto_37
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_89

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Lcom/google/api/services/plusi/model/DataSuggestedEntityId;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/DataSuggestedEntityId;-><init>()V

    new-instance v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/DataCircleMemberId;-><init>()V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_66

    iput-object v4, v2, Lcom/google/api/services/plusi/model/DataSuggestedEntityId;->obfuscatedGaiaId:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestedEntityId:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionId:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_37

    :cond_66
    const-string v4, "e:"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_37

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/api/services/plusi/model/DataSuggestedEntityId;->email:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestedEntityId:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/google/api/services/plusi/model/RecordSuggestionRequest;->suggestionAction:Lcom/google/api/services/plusi/model/DataSuggestionAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSuggestionAction;->suggestionId:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_37

    :cond_89
    return-void
.end method
