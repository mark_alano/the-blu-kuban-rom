.class public final Lcom/google/android/apps/plus/api/SendEventRsvpOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "SendEventRsvpOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/api/SendEventRsvpOperation$Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/EventRespondRequest;",
        "Lcom/google/api/services/plusi/model/EventRespondResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final mEventId:Ljava/lang/String;

.field private final mOwnerId:Ljava/lang/String;

.field private final mRollbackRsvpType:Ljava/lang/String;

.field private final mRsvpType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "eventId"
    .parameter "ownerId"
    .parameter "rsvpType"
    .parameter "rollbackRsvpType"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 54
    const-string v3, "eventrespond"

    invoke-static {}, Lcom/google/api/services/plusi/model/EventRespondRequestJson;->getInstance()Lcom/google/api/services/plusi/model/EventRespondRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/EventRespondResponseJson;->getInstance()Lcom/google/api/services/plusi/model/EventRespondResponseJson;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 56
    iput-object p3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    .line 57
    iput-object p4, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mOwnerId:Ljava/lang/String;

    .line 58
    iput-object p5, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRsvpType:Ljava/lang/String;

    .line 59
    iput-object p6, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRollbackRsvpType:Ljava/lang/String;

    .line 60
    return-void
.end method

.method private rollback()V
    .registers 6

    .prologue
    .line 88
    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->getPlusEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v0

    .line 92
    .local v0, event:Lcom/google/api/services/plusi/model/PlusEvent;
    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRsvpType:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 93
    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRollbackRsvpType:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsEventData;->setRsvpType(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 98
    :cond_21
    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mOwnerId:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsEventData;->refreshEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 6
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p1, Lcom/google/api/services/plusi/model/EventRespondResponse;

    .end local p1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/EventRespondResponse;->result:Ljava/lang/String;

    if-eqz v0, :cond_18

    iget-object v0, p1, Lcom/google/api/services/plusi/model/EventRespondResponse;->result:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/api/SendEventRsvpOperation$Status;->SUCCESS:Lcom/google/android/apps/plus/api/SendEventRsvpOperation$Status;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation$Status;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    invoke-direct {p0}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->rollback()V

    :goto_17
    return-void

    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRsvpType:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsEventData;->setRsvpType(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_17
.end method

.method protected final onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .registers 5
    .parameter "errorCode"
    .parameter "reasonPhrase"
    .parameter "ex"

    .prologue
    .line 82
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_6

    if-eqz p3, :cond_9

    .line 83
    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->rollback()V

    .line 85
    :cond_9
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 24
    check-cast p1, Lcom/google/api/services/plusi/model/EventRespondRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mEventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->eventId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SendEventRsvpOperation;->mRsvpType:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->response:Ljava/lang/String;

    return-void
.end method
