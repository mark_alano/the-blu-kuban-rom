.class public final Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "SharePhotosToEventOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SharePhotosToEventRequest;",
        "Lcom/google/api/services/plusi/model/SharePhotosToEventResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final mEventId:Ljava/lang/String;

.field final mPhotoIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter
    .parameter "eventId"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Landroid/content/Intent;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    .local p4, photoIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Long;>;"
    const-string v3, "sharephotostoevent"

    invoke-static {}, Lcom/google/api/services/plusi/model/SharePhotosToEventRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SharePhotosToEventRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SharePhotosToEventResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SharePhotosToEventResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 43
    iput-object p5, p0, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->mEventId:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->mPhotoIds:Ljava/util/List;

    .line 45
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 24
    check-cast p1, Lcom/google/api/services/plusi/model/SharePhotosToEventRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->mEventId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SharePhotosToEventRequest;->eventId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->mPhotoIds:Ljava/util/List;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SharePhotosToEventRequest;->photoId:Ljava/util/List;

    return-void
.end method
