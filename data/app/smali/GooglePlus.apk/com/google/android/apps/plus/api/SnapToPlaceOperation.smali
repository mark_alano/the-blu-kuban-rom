.class public final Lcom/google/android/apps/plus/api/SnapToPlaceOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "SnapToPlaceOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/SnapToPlaceRequest;",
        "Lcom/google/api/services/plusi/model/SnapToPlaceResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private mFirstPlace:Lcom/google/android/apps/plus/content/DbLocation;

.field private final mIsPlaceSearch:Z

.field private final mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

.field private mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private final mStoreResult:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/LocationQuery;Z)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "locationQuery"
    .parameter "storeResult"

    .prologue
    .line 51
    const-string v3, "snaptoplace"

    invoke-static {}, Lcom/google/api/services/plusi/model/SnapToPlaceRequestJson;->getInstance()Lcom/google/api/services/plusi/model/SnapToPlaceRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/SnapToPlaceResponseJson;->getInstance()Lcom/google/api/services/plusi/model/SnapToPlaceResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 57
    iput-object p5, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    .line 58
    iput-boolean p6, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mStoreResult:Z

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->hasQueryString()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mIsPlaceSearch:Z

    .line 60
    return-void
.end method


# virtual methods
.method public final getCoarseLocation()Lcom/google/android/apps/plus/content/DbLocation;
    .registers 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method public final getPlaceLocation()Lcom/google/android/apps/plus/content/DbLocation;
    .registers 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mFirstPlace:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method public final getPreciseLocation()Lcom/google/android/apps/plus/content/DbLocation;
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 10
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 29
    check-cast p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;

    .end local p1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->preciseLocation:Lcom/google/api/services/plusi/model/LocationResult;

    if-eqz v0, :cond_15

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v1, 0x1

    iget-object v3, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->preciseLocation:Lcom/google/api/services/plusi/model/LocationResult;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/LocationResult;->location:Lcom/google/api/services/plusi/model/Location;

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILcom/google/api/services/plusi/model/Location;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    :cond_15
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->cityLocation:Lcom/google/api/services/plusi/model/LocationResult;

    if-eqz v0, :cond_25

    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v1, 0x2

    iget-object v3, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->cityLocation:Lcom/google/api/services/plusi/model/LocationResult;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/LocationResult;->location:Lcom/google/api/services/plusi/model/Location;

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILcom/google/api/services/plusi/model/Location;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    :cond_25
    iget-object v3, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->localPlace:Ljava/util/List;

    if-nez v3, :cond_63

    move v1, v2

    :goto_2a
    iget-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceResponse;->userIsAtFirstPlace:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_43

    if-lez v1, :cond_43

    new-instance v5, Lcom/google/android/apps/plus/content/DbLocation;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/LocationResult;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocationResult;->location:Lcom/google/api/services/plusi/model/Location;

    invoke-direct {v5, v7, v0}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILcom/google/api/services/plusi/model/Location;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mFirstPlace:Lcom/google/android/apps/plus/content/DbLocation;

    :cond_43
    iget-boolean v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mStoreResult:Z

    if-eqz v0, :cond_7c

    if-lez v1, :cond_69

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_4e
    if-ge v2, v1, :cond_6a

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/LocationResult;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/LocationResult;->location:Lcom/google/api/services/plusi/model/Location;

    new-instance v6, Lcom/google/android/apps/plus/content/DbLocation;

    invoke-direct {v6, v7, v0}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILcom/google/api/services/plusi/model/Location;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_4e

    :cond_63
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_2a

    :cond_69
    move-object v5, v4

    :cond_6a
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mIsPlaceSearch:Z

    if-eqz v3, :cond_7d

    move-object v3, v4

    :goto_75
    iget-boolean v6, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mIsPlaceSearch:Z

    if-eqz v6, :cond_80

    :goto_79
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsPostsData;->insertLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;Lcom/google/android/apps/plus/content/DbLocation;Ljava/util/ArrayList;)V

    :cond_7c
    return-void

    :cond_7d
    iget-object v3, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    goto :goto_75

    :cond_80
    iget-object v4, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    goto :goto_79
.end method

.method public final hasCoarseLocation()Z
    .registers 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mCoarseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final hasPlaceLocation()Z
    .registers 2

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mFirstPlace:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final hasPreciseLocation()Z
    .registers 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mPreciseLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 7
    .parameter "x0"

    .prologue
    const-wide v3, 0x416312d000000000L

    .line 29
    check-cast p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->getLocation()Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;->latitudeE7:Ljava/lang/Integer;

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v1

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;->longitudeE7:Ljava/lang/Integer;

    invoke-virtual {v0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_36

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;->precisionMeters:Ljava/lang/Double;

    :cond_36
    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->hasQueryString()Z

    move-result v0

    if-eqz v0, :cond_46

    iget-object v0, p0, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LocationQuery;->getQueryString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/SnapToPlaceRequest;->searchQuery:Ljava/lang/String;

    :cond_46
    return-void
.end method
