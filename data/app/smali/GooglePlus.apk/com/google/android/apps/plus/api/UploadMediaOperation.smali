.class public final Lcom/google/android/apps/plus/api/UploadMediaOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "UploadMediaOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/UploadMediaRequest;",
        "Lcom/google/api/services/plusi/model/UploadMediaResponse;",
        ">;"
    }
.end annotation


# static fields
.field private static final QUERY_PARAMS:Landroid/os/Bundle;


# instance fields
.field private final mAlbumId:Ljava/lang/String;

.field private final mOwnerId:Ljava/lang/String;

.field private final mPayloadData:[B


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 28
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "uploadType"

    const-string v2, "multipart"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->QUERY_PARAMS:Landroid/os/Bundle;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;[B)V
    .registers 20
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "userId"
    .parameter "albumId"
    .parameter "payloadData"

    .prologue
    .line 48
    const-string v4, "uploadmedia"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/upload"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/api/UploadMediaOperation;->QUERY_PARAMS:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/api/services/plusi/model/UploadMediaRequestJson;->getInstance()Lcom/google/api/services/plusi/model/UploadMediaRequestJson;

    move-result-object v7

    invoke-static {}, Lcom/google/api/services/plusi/model/UploadMediaResponseJson;->getInstance()Lcom/google/api/services/plusi/model/UploadMediaResponseJson;

    move-result-object v8

    new-instance v11, Lcom/google/android/apps/plus/network/UploadMultipartHttpRequestConfiguration;

    const-string v1, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite"

    const/4 v2, 0x0

    invoke-direct {v11, p1, p2, v1, v2}, Lcom/google/android/apps/plus/network/UploadMultipartHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v9, p3

    move-object/from16 v10, p4

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    .line 54
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mOwnerId:Ljava/lang/String;

    .line 55
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumId:Ljava/lang/String;

    .line 57
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mPayloadData:[B

    .line 58
    return-void
.end method


# virtual methods
.method protected final bridge synthetic createHttpEntity(Lcom/google/android/apps/plus/json/GenericJson;)Lcom/google/android/apps/plus/network/JsonHttpEntity;
    .registers 5
    .parameter "x0"

    .prologue
    .line 24
    check-cast p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;

    .end local p1
    new-instance v0, Lcom/google/android/apps/plus/network/JsonHttpEntity;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mRequestJson:Lcom/google/android/apps/plus/json/EsJson;

    iget-object v2, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mPayloadData:[B

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/apps/plus/network/JsonHttpEntity;-><init>(Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/GenericJson;[B)V

    return-object v0
.end method

.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/UploadMediaOperation;->onStartResultProcessing()V

    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 3
    .parameter "x0"

    .prologue
    .line 24
    check-cast p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mOwnerId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->ownerId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/UploadMediaOperation;->mAlbumId:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->albumId:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/UploadMediaRequest;->autoResize:Ljava/lang/Boolean;

    return-void
.end method
