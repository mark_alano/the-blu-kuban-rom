.class public final Lcom/google/android/apps/plus/api/WriteReviewOperation;
.super Lcom/google/android/apps/plus/network/PlusiOperation;
.source "WriteReviewOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/network/PlusiOperation",
        "<",
        "Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;",
        "Lcom/google/api/services/plusi/model/WritePlaceReviewResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private cid:Ljava/lang/String;

.field private review:Lcom/google/android/apps/plus/content/GooglePlaceReview;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/GooglePlaceReview;Ljava/lang/String;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "intent"
    .parameter "listener"
    .parameter "review"
    .parameter "cid"

    .prologue
    .line 47
    const-string v3, "writeplacereview"

    invoke-static {}, Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;->getInstance()Lcom/google/api/services/plusi/model/WritePlaceReviewRequestJson;

    move-result-object v4

    invoke-static {}, Lcom/google/api/services/plusi/model/WritePlaceReviewResponseJson;->getInstance()Lcom/google/api/services/plusi/model/WritePlaceReviewResponseJson;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/network/PlusiOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 51
    iput-object p5, p0, Lcom/google/android/apps/plus/api/WriteReviewOperation;->review:Lcom/google/android/apps/plus/content/GooglePlaceReview;

    .line 52
    iput-object p6, p0, Lcom/google/android/apps/plus/api/WriteReviewOperation;->cid:Ljava/lang/String;

    .line 53
    return-void
.end method


# virtual methods
.method protected final bridge synthetic handleResponse(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 2
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    return-void
.end method

.method protected final bridge synthetic populateRequest(Lcom/google/android/apps/plus/json/GenericJson;)V
    .registers 7
    .parameter "x0"

    .prologue
    .line 28
    check-cast p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/api/WriteReviewOperation;->cid:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->cid:Ljava/lang/String;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/api/WriteReviewOperation;->review:Lcom/google/android/apps/plus/content/GooglePlaceReview;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/GooglePlaceReview;->getZagatAspects()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_19
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_36

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    invoke-direct {v4}, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;-><init>()V

    iput-object v0, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->labelId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_19

    :cond_36
    iput-object v1, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->zagatAspectRatings:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/apps/plus/api/WriteReviewOperation;->review:Lcom/google/android/apps/plus/content/GooglePlaceReview;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/GooglePlaceReview;->getPriceValue()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_57

    new-instance v0, Lcom/google/api/services/plusi/model/PriceProto;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PriceProto;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/api/WriteReviewOperation;->review:Lcom/google/android/apps/plus/content/GooglePlaceReview;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/GooglePlaceReview;->getPriceValue()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PriceProto;->valueDisplay:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/api/WriteReviewOperation;->review:Lcom/google/android/apps/plus/content/GooglePlaceReview;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/GooglePlaceReview;->getPriceCurrencyCode()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/PriceProto;->currencyCode:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->price:Lcom/google/api/services/plusi/model/PriceProto;

    :cond_57
    iget-object v0, p0, Lcom/google/android/apps/plus/api/WriteReviewOperation;->review:Lcom/google/android/apps/plus/content/GooglePlaceReview;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/GooglePlaceReview;->getPriceLevelId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_74

    new-instance v2, Lcom/google/api/services/plusi/model/PriceLevelsProto;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/PriceLevelsProto;-><init>()V

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/api/services/plusi/model/PriceLevelsProto;->ratedValueId:Ljava/lang/Long;

    iput-object v2, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

    :cond_74
    iget-object v0, p0, Lcom/google/android/apps/plus/api/WriteReviewOperation;->review:Lcom/google/android/apps/plus/content/GooglePlaceReview;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/GooglePlaceReview;->getReviewText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->reviewText:Ljava/lang/String;

    const-string v0, "hotpot-android-gplus"

    iput-object v0, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->source:Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/AbuseSignals;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/AbuseSignals;-><init>()V

    iput-object v0, p1, Lcom/google/api/services/plusi/model/WritePlaceReviewRequest;->abuseSignals:Lcom/google/api/services/plusi/model/AbuseSignals;

    return-void
.end method
