.class public Lcom/google/android/apps/plus/content/AudienceData;
.super Ljava/lang/Object;
.source "AudienceData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCircles:[Lcom/google/android/apps/plus/content/CircleData;

.field private mTotalPersonCount:I

.field private mUsers:[Lcom/google/android/apps/plus/content/PersonData;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/AudienceData$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/AudienceData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "parcel"

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    sget-object v0, Lcom/google/android/apps/plus/content/PersonData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    .line 84
    sget-object v0, Lcom/google/android/apps/plus/content/CircleData;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    .line 86
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/CircleData;)V
    .registers 4
    .parameter "circle"

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-array v0, v1, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    .line 78
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    aput-object p1, v0, v1

    .line 80
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 5
    .parameter "person"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-array v0, v2, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    .line 66
    new-array v0, v1, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    aput-object p1, v0, v1

    .line 68
    iput v2, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .registers 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, personList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/PersonData;>;"
    .local p2, circleList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/CircleData;>;"
    if-eqz p1, :cond_a

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    :goto_6
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;I)V

    .line 31
    return-void

    .line 30
    :cond_a
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;I)V
    .registers 6
    .parameter
    .parameter
    .parameter "totalPersonCount"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, personList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/PersonData;>;"
    .local p2, circleList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/CircleData;>;"
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    if-eqz p1, :cond_25

    .line 43
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 49
    :goto_13
    if-eqz p2, :cond_2a

    .line 50
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 56
    :goto_22
    iput p3, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    .line 57
    return-void

    .line 46
    :cond_25
    new-array v0, v1, [Lcom/google/android/apps/plus/content/PersonData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    goto :goto_13

    .line 53
    :cond_2a
    new-array v0, v1, [Lcom/google/android/apps/plus/content/CircleData;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    goto :goto_22
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 155
    const/4 v0, 0x0

    return v0
.end method

.method public final getCircle(I)Lcom/google/android/apps/plus/content/CircleData;
    .registers 4
    .parameter "index"

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final getCircleCount()I
    .registers 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    array-length v0, v0

    return v0
.end method

.method public final getCircles()[Lcom/google/android/apps/plus/content/CircleData;
    .registers 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    return-object v0
.end method

.method public final getHiddenUserCount()I
    .registers 4

    .prologue
    .line 113
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    iget-object v2, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    array-length v2, v2

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public final getUser(I)Lcom/google/android/apps/plus/content/PersonData;
    .registers 4
    .parameter "index"

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final getUserCount()I
    .registers 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    array-length v0, v0

    return v0
.end method

.method public final getUsers()[Lcom/google/android/apps/plus/content/PersonData;
    .registers 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Audience circles: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", users: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hidden users: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getHiddenUserCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    .prologue
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mUsers:[Lcom/google/android/apps/plus/content/PersonData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mCircles:[Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 92
    iget v0, p0, Lcom/google/android/apps/plus/content/AudienceData;->mTotalPersonCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    return-void
.end method
