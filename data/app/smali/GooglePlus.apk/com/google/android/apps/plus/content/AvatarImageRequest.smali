.class public final Lcom/google/android/apps/plus/content/AvatarImageRequest;
.super Lcom/google/android/apps/plus/content/CachedImageRequest;
.source "AvatarImageRequest.java"


# instance fields
.field private mDownloadUrl:Ljava/lang/String;

.field private final mGaiaId:Ljava/lang/String;

.field private mHashCode:I

.field private final mSize:I

.field private final mSizeInPx:I

.field private final mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .registers 5
    .parameter "gaiaId"
    .parameter "url"
    .parameter "size"
    .parameter "sizeInPx"

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/CachedImageRequest;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mGaiaId:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mUrl:Ljava/lang/String;

    .line 35
    iput p3, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mSize:I

    .line 36
    iput p4, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mSizeInPx:I

    .line 37
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    .prologue
    const/4 v1, 0x0

    .line 92
    instance-of v2, p1, Lcom/google/android/apps/plus/content/AvatarImageRequest;

    if-nez v2, :cond_6

    .line 97
    :cond_5
    :goto_5
    return v1

    :cond_6
    move-object v0, p1

    .line 96
    check-cast v0, Lcom/google/android/apps/plus/content/AvatarImageRequest;

    .line 97
    .local v0, other:Lcom/google/android/apps/plus/content/AvatarImageRequest;
    iget v2, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mSize:I

    iget v3, v0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mSize:I

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mUrl:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mUrl:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v1, 0x1

    goto :goto_5
.end method

.method protected final getCacheFilePrefix()Ljava/lang/String;
    .registers 2

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mSize:I

    packed-switch v0, :pswitch_data_10

    .line 74
    const/4 v0, 0x0

    :goto_6
    return-object v0

    .line 69
    :pswitch_7
    const-string v0, "AT"

    goto :goto_6

    .line 70
    :pswitch_a
    const-string v0, "AS"

    goto :goto_6

    .line 71
    :pswitch_d
    const-string v0, "AM"

    goto :goto_6

    .line 68
    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_7
        :pswitch_a
        :pswitch_d
    .end packed-switch
.end method

.method public final getCanonicalDownloadUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AvatarImageRequest;->getDownloadUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDownloadUrl()Ljava/lang/String;
    .registers 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mDownloadUrl:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 42
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mSizeInPx:I

    iget-object v1, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mUrl:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->getCroppedAndResizedUrl(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mDownloadUrl:Ljava/lang/String;

    .line 44
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mDownloadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getGaiaId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mHashCode:I

    if-nez v0, :cond_19

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_1c

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mHashCode:I

    .line 85
    :goto_10
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mHashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mSize:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mHashCode:I

    .line 87
    :cond_19
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mHashCode:I

    return v0

    .line 83
    :cond_1c
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mHashCode:I

    goto :goto_10
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 102
    const/4 v0, 0x0

    .line 103
    .local v0, size:Ljava/lang/String;
    iget v1, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mSize:I

    packed-switch v1, :pswitch_data_32

    .line 109
    :goto_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AvatarImageRequest: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/content/AvatarImageRequest;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 104
    :pswitch_28
    const-string v0, "tiny"

    goto :goto_6

    .line 105
    :pswitch_2b
    const-string v0, "small"

    goto :goto_6

    .line 106
    :pswitch_2e
    const-string v0, "medium"

    goto :goto_6

    .line 103
    nop

    :pswitch_data_32
    .packed-switch 0x0
        :pswitch_28
        :pswitch_2b
        :pswitch_2e
    .end packed-switch
.end method
