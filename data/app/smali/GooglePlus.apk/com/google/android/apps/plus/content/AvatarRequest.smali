.class public final Lcom/google/android/apps/plus/content/AvatarRequest;
.super Lcom/google/android/apps/plus/content/ImageRequest;
.source "AvatarRequest.java"


# instance fields
.field private final mAvatarUrl:Ljava/lang/String;

.field private final mContactLookupKey:Ljava/lang/String;

.field private final mGaiaId:Ljava/lang/String;

.field private mHashCode:I

.field private final mIdType:I

.field private final mRounded:Z

.field private final mSize:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .registers 4
    .parameter "gaiaId"
    .parameter "size"

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;IZ)V

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .registers 5
    .parameter "gaiaId"
    .parameter "size"
    .parameter "rounded"

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .registers 5
    .parameter "gaiaId"
    .parameter "avatarUrl"
    .parameter "size"

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZ)V
    .registers 6
    .parameter "gaiaId"
    .parameter "avatarUrl"
    .parameter "size"
    .parameter "rounded"

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/ImageRequest;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mIdType:I

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mGaiaId:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mAvatarUrl:Ljava/lang/String;

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mContactLookupKey:Ljava/lang/String;

    .line 51
    iput p3, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mSize:I

    .line 52
    iput-boolean p4, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mRounded:Z

    .line 53
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    .prologue
    const/4 v1, 0x0

    .line 163
    if-ne p1, p0, :cond_5

    .line 164
    const/4 v1, 0x1

    .line 176
    :cond_4
    :goto_4
    return v1

    .line 167
    :cond_5
    instance-of v2, p1, Lcom/google/android/apps/plus/content/AvatarRequest;

    if-eqz v2, :cond_4

    move-object v0, p1

    .line 171
    check-cast v0, Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 172
    .local v0, k:Lcom/google/android/apps/plus/content/AvatarRequest;
    iget v2, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mSize:I

    iget v3, v0, Lcom/google/android/apps/plus/content/AvatarRequest;->mSize:I

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mRounded:Z

    iget-boolean v3, v0, Lcom/google/android/apps/plus/content/AvatarRequest;->mRounded:Z

    if-ne v2, v3, :cond_4

    .line 176
    iget-object v1, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mGaiaId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/content/AvatarRequest;->mGaiaId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    goto :goto_4
.end method

.method public final getAvatarUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mAvatarUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getGaiaId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mGaiaId:Ljava/lang/String;

    return-object v0
.end method

.method public final getSize()I
    .registers 2

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mSize:I

    return v0
.end method

.method public final hashCode()I
    .registers 3

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mHashCode:I

    if-nez v0, :cond_1d

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_20

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mSize:I

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mHashCode:I

    .line 151
    :goto_13
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mRounded:Z

    if-eqz v0, :cond_1d

    .line 152
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mHashCode:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mHashCode:I

    .line 155
    :cond_1d
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mHashCode:I

    return v0

    .line 149
    :cond_20
    iget v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mSize:I

    iput v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mHashCode:I

    goto :goto_13
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mGaiaId:Ljava/lang/String;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final isRounded()Z
    .registers 2

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mRounded:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 191
    const/4 v1, 0x0

    .line 192
    .local v1, size:Ljava/lang/String;
    iget v2, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mSize:I

    packed-switch v2, :pswitch_data_3e

    .line 197
    :goto_6
    iget-boolean v2, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mRounded:Z

    if-eqz v2, :cond_3b

    const-string v0, "(rounded)"

    .line 198
    .local v0, rounded:Ljava/lang/String;
    :goto_c
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AvatarRequest: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/content/AvatarRequest;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 193
    .end local v0           #rounded:Ljava/lang/String;
    :pswitch_32
    const-string v1, "tiny"

    goto :goto_6

    .line 194
    :pswitch_35
    const-string v1, "small"

    goto :goto_6

    .line 195
    :pswitch_38
    const-string v1, "medium"

    goto :goto_6

    .line 197
    :cond_3b
    const-string v0, ""

    goto :goto_c

    .line 192
    :pswitch_data_3e
    .packed-switch 0x0
        :pswitch_32
        :pswitch_35
        :pswitch_38
    .end packed-switch
.end method
