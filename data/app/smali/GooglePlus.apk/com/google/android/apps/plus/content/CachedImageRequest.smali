.class public abstract Lcom/google/android/apps/plus/content/CachedImageRequest;
.super Lcom/google/android/apps/plus/content/ImageRequest;
.source "CachedImageRequest.java"


# instance fields
.field private mCacheDir:Ljava/lang/String;

.field private mCacheFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/ImageRequest;-><init>()V

    return-void
.end method

.method private buildCacheFilePath()V
    .registers 11

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/CachedImageRequest;->getCanonicalDownloadUrl()Ljava/lang/String;

    move-result-object v4

    .line 67
    .local v4, url:Ljava/lang/String;
    const-wide v0, 0x3ffffffffffe5L

    .line 68
    .local v0, code:J
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    .line 69
    .local v3, len:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_e
    if-ge v2, v3, :cond_1d

    .line 70
    const-wide/16 v5, 0x1f

    mul-long/2addr v5, v0

    invoke-virtual {v4, v2}, Ljava/lang/String;->charAt(I)C

    move-result v7

    int-to-long v7, v7

    add-long v0, v5, v7

    .line 69
    add-int/lit8 v2, v2, 0x1

    goto :goto_e

    .line 73
    :cond_1d
    rem-int/lit8 v5, v3, 0x10

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/content/CachedImageRequest;->mCacheDir:Ljava/lang/String;

    .line 74
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/CachedImageRequest;->getCacheFilePrefix()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x4

    shr-long v6, v0, v6

    const-wide v8, 0xfffffffffffffffL

    and-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/content/CachedImageRequest;->mCacheFileName:Ljava/lang/String;

    .line 75
    return-void
.end method


# virtual methods
.method public final getCacheDir()Ljava/lang/String;
    .registers 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/content/CachedImageRequest;->mCacheDir:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/CachedImageRequest;->buildCacheFilePath()V

    .line 42
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/content/CachedImageRequest;->mCacheDir:Ljava/lang/String;

    return-object v0
.end method

.method public final getCacheFileName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/plus/content/CachedImageRequest;->mCacheFileName:Ljava/lang/String;

    if-nez v0, :cond_7

    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/CachedImageRequest;->buildCacheFilePath()V

    .line 52
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/content/CachedImageRequest;->mCacheFileName:Ljava/lang/String;

    return-object v0
.end method

.method protected abstract getCacheFilePrefix()Ljava/lang/String;
.end method

.method public abstract getCanonicalDownloadUrl()Ljava/lang/String;
.end method

.method public abstract getDownloadUrl()Ljava/lang/String;
.end method
