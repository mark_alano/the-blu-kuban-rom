.class public final Lcom/google/android/apps/plus/content/DbAnalyticsEvents;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbAnalyticsEvents.java"


# direct methods
.method public static deserializeClientOzEventList([B)Ljava/util/List;
    .registers 10
    .parameter "array"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 44
    if-nez p0, :cond_5

    move-object v5, v6

    .line 60
    :cond_4
    :goto_4
    return-object v5

    .line 48
    :cond_5
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 49
    .local v0, bb:Ljava/nio/ByteBuffer;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 50
    .local v5, returnList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v2

    .line 52
    .local v2, eventJson:Lcom/google/api/services/plusi/model/ClientOzEventJson;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 53
    .local v4, items:I
    const/4 v3, 0x0

    .local v3, i:I
    :goto_17
    if-ge v3, v4, :cond_4

    .line 54
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v7

    new-array v1, v7, [B

    const/4 v8, 0x0

    invoke-virtual {v0, v1, v8, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 55
    .local v1, byteEvent:[B
    if-eqz v1, :cond_28

    array-length v7, v1

    if-nez v7, :cond_2a

    :cond_28
    move-object v5, v6

    .line 56
    goto :goto_4

    .line 58
    :cond_2a
    invoke-virtual {v2, v1}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    add-int/lit8 v3, v3, 0x1

    goto :goto_17
.end method

.method public static serializeClientOzEventList(Ljava/util/List;)[B
    .registers 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;)[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    .local p0, eventList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/ClientOzEvent;>;"
    if-nez p0, :cond_4

    .line 25
    const/4 v5, 0x0

    .line 39
    :goto_3
    return-object v5

    .line 28
    :cond_4
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 29
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 30
    .local v1, dos:Ljava/io/DataOutputStream;
    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v3

    .line 32
    .local v3, eventJson:Lcom/google/api/services/plusi/model/ClientOzEventJson;
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 33
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_1d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/ClientOzEvent;

    .line 34
    .local v2, event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    invoke-virtual {v3, v2}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v6

    if-nez v6, :cond_34

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_1d

    :cond_34
    array-length v7, v6

    invoke-virtual {v1, v7}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v1, v6}, Ljava/io/DataOutputStream;->write([B)V

    goto :goto_1d

    .line 37
    .end local v2           #event:Lcom/google/api/services/plusi/model/ClientOzEvent;
    :cond_3c
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    .line 38
    .local v5, result:[B
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    goto :goto_3
.end method
