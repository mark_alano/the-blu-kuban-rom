.class public Lcom/google/android/apps/plus/content/DbLocation;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbLocation.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mBestAddress:Ljava/lang/String;

.field private final mClusterId:Ljava/lang/String;

.field private final mHasCoordinates:Z

.field private final mLatitudeE7:I

.field private final mLongitudeE7:I

.field private final mName:Ljava/lang/String;

.field private final mPrecisionMeters:D

.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 383
    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/DbLocation$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/DbLocation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/location/Location;)V
    .registers 7
    .parameter "type"
    .parameter "location"

    .prologue
    const-wide v2, 0x416312d000000000L

    .line 109
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    .line 110
    if-nez p2, :cond_10

    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 116
    :cond_10
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    .line 118
    invoke-virtual {p2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    .line 119
    invoke-virtual {p2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    .line 121
    invoke-virtual {p2}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_3b

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v0, v0

    :goto_38
    iput-wide v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    .line 122
    return-void

    .line 121
    :cond_3b
    const-wide/high16 v0, -0x4010

    goto :goto_38
.end method

.method public constructor <init>(ILcom/google/api/services/plusi/model/Location;)V
    .registers 11
    .parameter "type"
    .parameter "location"

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide v6, 0x416312d000000000L

    .line 79
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    .line 80
    if-ltz p1, :cond_f

    const/4 v4, 0x3

    if-le p1, v4, :cond_15

    .line 81
    :cond_f
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 83
    :cond_15
    if-nez p2, :cond_1d

    .line 84
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 86
    :cond_1d
    iput p1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    .line 87
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->locationTag:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    .line 88
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->bestAddress:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    .line 89
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->clusterId:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    .line 90
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    if-eqz v4, :cond_57

    iget-object v0, p2, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    .line 91
    .local v0, latitude:Ljava/lang/Integer;
    :goto_31
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    if-eqz v4, :cond_6b

    iget-object v1, p2, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    .line 92
    .local v1, longitude:Ljava/lang/Integer;
    :goto_37
    if-eqz v0, :cond_7f

    if-eqz v1, :cond_7f

    const/4 v2, 0x1

    :goto_3c
    iput-boolean v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    .line 93
    iget-boolean v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v2, :cond_81

    .line 94
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    .line 95
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    .line 99
    :goto_4e
    iget-object v2, p2, Lcom/google/api/services/plusi/model/Location;->precisionMeters:Ljava/lang/Double;

    if-nez v2, :cond_86

    const-wide/high16 v2, -0x4010

    :goto_54
    iput-wide v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    .line 101
    return-void

    .line 90
    .end local v0           #latitude:Ljava/lang/Integer;
    .end local v1           #longitude:Ljava/lang/Integer;
    :cond_57
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->latitude:Ljava/lang/Float;

    if-eqz v4, :cond_69

    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->latitude:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    float-to-double v4, v4

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_31

    :cond_69
    move-object v0, v2

    goto :goto_31

    .line 91
    .restart local v0       #latitude:Ljava/lang/Integer;
    :cond_6b
    iget-object v4, p2, Lcom/google/api/services/plusi/model/Location;->longitude:Ljava/lang/Float;

    if-eqz v4, :cond_7d

    iget-object v2, p2, Lcom/google/api/services/plusi/model/Location;->longitude:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v4, v2

    mul-double/2addr v4, v6

    double-to-int v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_37

    :cond_7d
    move-object v1, v2

    goto :goto_37

    .restart local v1       #longitude:Ljava/lang/Integer;
    :cond_7f
    move v2, v3

    .line 92
    goto :goto_3c

    .line 97
    :cond_81
    iput v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    iput v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    goto :goto_4e

    .line 99
    :cond_86
    iget-object v2, p2, Lcom/google/api/services/plusi/model/Location;->precisionMeters:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    goto :goto_54
.end method

.method public constructor <init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V
    .registers 11
    .parameter "type"
    .parameter "latitudeE7"
    .parameter "longitudeE7"
    .parameter "name"
    .parameter "bestAddress"
    .parameter "clusterId"
    .parameter "precisionMeters"

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    .line 56
    if-ltz p1, :cond_9

    const/4 v0, 0x3

    if-le p1, v0, :cond_f

    .line 57
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 59
    :cond_f
    iput p1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    .line 60
    iput-object p4, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    .line 61
    iput-object p5, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    .line 62
    iput-object p6, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    .line 63
    if-eqz p2, :cond_31

    if-eqz p3, :cond_31

    const/4 v0, 0x1

    :goto_1c
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    .line 64
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v0, :cond_33

    .line 65
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    .line 66
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    .line 70
    :goto_2e
    iput-wide p7, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    .line 71
    return-void

    :cond_31
    move v0, v1

    .line 63
    goto :goto_1c

    .line 68
    :cond_33
    iput v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    iput v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    goto :goto_2e
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "parcel"

    .prologue
    .line 401
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    .line 402
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    .line 403
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    .line 404
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    .line 405
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_37

    const/4 v0, 0x1

    :goto_1c
    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    .line 406
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    .line 407
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    .line 408
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    .line 409
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    .line 410
    return-void

    .line 405
    :cond_37
    const/4 v0, 0x0

    goto :goto_1c
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;
    .registers 14
    .parameter "data"

    .prologue
    const/4 v3, 0x0

    .line 361
    if-nez p0, :cond_4

    .line 376
    :goto_3
    return-object v3

    .line 365
    :cond_4
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 367
    .local v9, bb:Ljava/nio/ByteBuffer;
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 368
    .local v1, type:I
    invoke-static {v9}, Lcom/google/android/apps/plus/content/DbLocation;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v4

    .line 369
    .local v4, name:Ljava/lang/String;
    invoke-static {v9}, Lcom/google/android/apps/plus/content/DbLocation;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v5

    .line 370
    .local v5, bestAddress:Ljava/lang/String;
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    if-eqz v0, :cond_3e

    const/4 v10, 0x1

    .line 371
    .local v10, hasCoordinates:Z
    :goto_1b
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v11

    .line 372
    .local v11, latitudeE7:I
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    .line 373
    .local v12, longitudeE7:I
    invoke-virtual {v9}, Ljava/nio/ByteBuffer;->getDouble()D

    move-result-wide v7

    .line 374
    .local v7, precision:D
    invoke-static {v9}, Lcom/google/android/apps/plus/content/DbLocation;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v6

    .line 376
    .local v6, clusterId:Ljava/lang/String;
    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v10, :cond_40

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_33
    if-eqz v10, :cond_39

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :cond_39
    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    move-object v3, v0

    goto :goto_3

    .line 370
    .end local v6           #clusterId:Ljava/lang/String;
    .end local v7           #precision:D
    .end local v10           #hasCoordinates:Z
    .end local v11           #latitudeE7:I
    .end local v12           #longitudeE7:I
    :cond_3e
    const/4 v10, 0x0

    goto :goto_1b

    .restart local v6       #clusterId:Ljava/lang/String;
    .restart local v7       #precision:D
    .restart local v10       #hasCoordinates:Z
    .restart local v11       #latitudeE7:I
    .restart local v12       #longitudeE7:I
    :cond_40
    move-object v2, v3

    .line 376
    goto :goto_33
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbLocation;)[B
    .registers 6
    .parameter "location"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 338
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 339
    .local v2, stream:Ljava/io/ByteArrayOutputStream;
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 341
    .local v0, os:Ljava/io/DataOutputStream;
    iget v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 342
    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbLocation;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 343
    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbLocation;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 344
    iget-boolean v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v3, :cond_3f

    const/4 v3, 0x1

    :goto_20
    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 345
    iget v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 346
    iget v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 347
    iget-wide v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    invoke-virtual {v0, v3, v4}, Ljava/io/DataOutputStream;->writeDouble(D)V

    .line 348
    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbLocation;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 350
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 351
    .local v1, result:[B
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 352
    return-object v1

    .line 344
    .end local v1           #result:[B
    :cond_3f
    const/4 v3, 0x0

    goto :goto_20
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 398
    const/4 v0, 0x0

    return v0
.end method

.method public final getAndroidLocation()Landroid/location/Location;
    .registers 6

    .prologue
    const-wide v3, 0x416312d000000000L

    .line 236
    new-instance v0, Landroid/location/Location;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 237
    .local v0, location:Landroid/location/Location;
    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v1, :cond_1d

    .line 238
    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    .line 239
    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    int-to-double v1, v1

    div-double/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    .line 242
    :cond_1d
    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-ltz v1, :cond_2b

    .line 243
    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 245
    :cond_2b
    return-object v0
.end method

.method public final getBestAddress()Ljava/lang/String;
    .registers 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    return-object v0
.end method

.method public final getClusterId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    return-object v0
.end method

.method public final getLatitudeE7()I
    .registers 2

    .prologue
    .line 203
    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    return v0
.end method

.method public final getLocationName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    .line 188
    :goto_a
    return-object v0

    .line 185
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_16

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    goto :goto_a

    .line 188
    :cond_16
    const-string v0, ""

    goto :goto_a
.end method

.method public final getLongitudeE7()I
    .registers 2

    .prologue
    .line 211
    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrecisionMeters()D
    .registers 3

    .prologue
    .line 219
    iget-wide v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    return-wide v0
.end method

.method public final hasCoordinates()Z
    .registers 2

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    return v0
.end method

.method public final isCoarse()Z
    .registers 3

    .prologue
    .line 148
    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final isPrecise()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 139
    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    if-ne v1, v0, :cond_6

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final toProtocolObject()Lcom/google/api/services/plusi/model/Location;
    .registers 6

    .prologue
    .line 286
    new-instance v0, Lcom/google/api/services/plusi/model/Location;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Location;-><init>()V

    .line 287
    .local v0, protoLocation:Lcom/google/api/services/plusi/model/Location;
    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->locationTag:Ljava/lang/String;

    .line 288
    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->bestAddress:Ljava/lang/String;

    .line 289
    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->clusterId:Ljava/lang/String;

    .line 290
    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v1, :cond_25

    .line 291
    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    .line 292
    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    .line 294
    :cond_25
    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    const-wide/16 v3, 0x0

    cmpl-double v1, v1, v3

    if-ltz v1, :cond_35

    .line 295
    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/Location;->precisionMeters:Ljava/lang/Double;

    .line 298
    :cond_35
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    .prologue
    .line 269
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "LocationValue type: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    packed-switch v0, :pswitch_data_8a

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "unknown("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_23
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", addr: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hasCoord: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", latE7: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", lngE7: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cluster: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", precision: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_80
    const-string v0, "precise"

    goto :goto_23

    :pswitch_83
    const-string v0, "coarse"

    goto :goto_23

    :pswitch_86
    const-string v0, "place"

    goto :goto_23

    nop

    :pswitch_data_8a
    .packed-switch 0x1
        :pswitch_80
        :pswitch_83
        :pswitch_86
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    .prologue
    .line 414
    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mBestAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 417
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mHasCoordinates:Z

    if-eqz v0, :cond_2c

    const/4 v0, 0x1

    :goto_14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 418
    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLatitudeE7:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 419
    iget v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mLongitudeE7:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 420
    iget-wide v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mPrecisionMeters:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 421
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbLocation;->mClusterId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 422
    return-void

    .line 417
    :cond_2c
    const/4 v0, 0x0

    goto :goto_14
.end method
