.class public final Lcom/google/android/apps/plus/content/DbPlusOneData;
.super Lcom/google/android/apps/plus/content/DbSerializer;
.source "DbPlusOneData.java"


# instance fields
.field private mCount:I

.field private mId:Ljava/lang/String;

.field private mPlusOnedByMe:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    .line 38
    return-void
.end method

.method private constructor <init>(Lcom/google/api/services/plusi/model/DataPlusOne;)V
    .registers 3
    .parameter "dataPlusOne"

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    .line 23
    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->id:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    .line 24
    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    .line 25
    iget-object v0, p1, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .registers 4
    .parameter "id"
    .parameter "count"
    .parameter "plusOnedByMe"

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/DbSerializer;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    .line 30
    iput p2, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    .line 31
    iput-boolean p3, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    .line 32
    return-void
.end method

.method public static deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;
    .registers 6
    .parameter "data"

    .prologue
    const/4 v3, 0x1

    .line 81
    if-nez p0, :cond_5

    .line 82
    const/4 v4, 0x0

    .line 91
    :goto_4
    return-object v4

    .line 85
    :cond_5
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 87
    .local v0, bb:Ljava/nio/ByteBuffer;
    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getShortString(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, id:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 89
    .local v1, count:I
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    if-ne v4, v3, :cond_1d

    .line 91
    .local v3, isPlusOnedByMe:Z
    :goto_17
    new-instance v4, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v4, v2, v1, v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>(Ljava/lang/String;IZ)V

    goto :goto_4

    .line 89
    .end local v3           #isPlusOnedByMe:Z
    :cond_1d
    const/4 v3, 0x0

    goto :goto_17
.end method

.method public static serialize(Lcom/google/android/apps/plus/content/DbPlusOneData;)[B
    .registers 5
    .parameter "plusOneData"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 69
    .local v2, stream:Ljava/io/ByteArrayOutputStream;
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 71
    .local v0, os:Ljava/io/DataOutputStream;
    iget-object v3, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/content/DbPlusOneData;->putShortString(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    .line 72
    iget v3, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 73
    iget-boolean v3, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    if-eqz v3, :cond_26

    const/4 v3, 0x1

    :goto_1b
    invoke-virtual {v0, v3}, Ljava/io/DataOutputStream;->write(I)V

    .line 75
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 76
    .local v1, result:[B
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->close()V

    .line 77
    return-object v1

    .line 73
    .end local v1           #result:[B
    :cond_26
    const/4 v3, 0x0

    goto :goto_1b
.end method

.method public static serialize(Lcom/google/api/services/plusi/model/DataPlusOne;)[B
    .registers 2
    .parameter "dataPlusOne"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>(Lcom/google/api/services/plusi/model/DataPlusOne;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/android/apps/plus/content/DbPlusOneData;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    return v0
.end method

.method public final getId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    return-object v0
.end method

.method public final isPlusOnedByMe()Z
    .registers 2

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    return v0
.end method

.method public final setId(Ljava/lang/String;)V
    .registers 2
    .parameter "id"

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mId:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public final updatePlusOnedByMe(Z)V
    .registers 4
    .parameter "plusOnedByMe"

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    if-eq v0, p1, :cond_e

    .line 58
    iput-boolean p1, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mPlusOnedByMe:Z

    .line 59
    iget v1, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    if-eqz p1, :cond_f

    const/4 v0, 0x1

    :goto_b
    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/content/DbPlusOneData;->mCount:I

    .line 61
    :cond_e
    return-void

    .line 59
    :cond_f
    const/4 v0, -0x1

    goto :goto_b
.end method
