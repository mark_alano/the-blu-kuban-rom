.class public final Lcom/google/android/apps/plus/content/EsAvatarData;
.super Ljava/lang/Object;
.source "EsAvatarData.java"


# static fields
.field private static final AVATAR_URL_PROJECTION:[Ljava/lang/String;

.field private static sBackgroundColor:I

.field private static sDefaultAvatarMedium:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarMediumRound:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarSmall:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarSmallRound:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarTiny:Landroid/graphics/Bitmap;

.field private static sDefaultAvatarTinyRound:Landroid/graphics/Bitmap;

.field private static final sFifeAbbrs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sFifeHosts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sMediumAvatarSize:I

.field public static sRoundAvatarsEnabled:Z

.field private static sSmallAvatarSize:I

.field private static sTinyAvatarSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    .line 37
    sput-boolean v3, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    .line 45
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const-string v1, "avatar"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    .line 70
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    const-string v1, "lh3.googleusercontent.com"

    const-string v2, "~3"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    const-string v1, "~3"

    const-string v2, "lh3.googleusercontent.com"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    const-string v1, "lh4.googleusercontent.com"

    const-string v2, "~4"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    const-string v1, "~4"

    const-string v2, "lh4.googleusercontent.com"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    const-string v1, "lh5.googleusercontent.com"

    const-string v2, "~5"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    const-string v1, "~5"

    const-string v2, "lh5.googleusercontent.com"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    const-string v1, "lh6.googleusercontent.com"

    const-string v2, "~6"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    const-string v1, "~6"

    const-string v2, "lh6.googleusercontent.com"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    return-void
.end method

.method public static compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "url"

    .prologue
    .line 475
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 476
    const/4 v4, 0x0

    .line 506
    :goto_7
    return-object v4

    .line 480
    :cond_8
    const/4 v3, 0x0

    .line 481
    .local v3, start:I
    const-string v4, "https://"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_43

    .line 482
    const/16 v3, 0x8

    .line 490
    :cond_13
    :goto_13
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 491
    .local v0, end:I
    const-string v4, "/photo.jpg"

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 492
    add-int/lit8 v0, v0, -0x9

    .line 496
    :cond_21
    const/16 v4, 0x2f

    invoke-virtual {p0, v4, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 497
    .local v2, slash:I
    const/4 v4, -0x1

    if-ne v2, v4, :cond_57

    .line 498
    const/4 v1, 0x0

    .line 503
    .local v1, hostAbbreviation:Ljava/lang/String;
    :goto_2b
    if-eqz v1, :cond_64

    .line 504
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_7

    .line 483
    .end local v0           #end:I
    .end local v1           #hostAbbreviation:Ljava/lang/String;
    .end local v2           #slash:I
    :cond_43
    const-string v4, "http://"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4d

    .line 484
    const/4 v3, 0x7

    goto :goto_13

    .line 485
    :cond_4d
    const-string v4, "//"

    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 486
    const/4 v3, 0x2

    goto :goto_13

    .line 500
    .restart local v0       #end:I
    .restart local v2       #slash:I
    :cond_57
    sget-object v4, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeHosts:Ljava/util/HashMap;

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .restart local v1       #hostAbbreviation:Ljava/lang/String;
    goto :goto_2b

    .line 506
    :cond_64
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_7
.end method

.method private static getAvatarBackgroundColor(Landroid/content/Context;)I
    .registers 3
    .parameter "context"

    .prologue
    .line 240
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sBackgroundColor:I

    if-nez v0, :cond_11

    .line 241
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sBackgroundColor:I

    .line 243
    :cond_11
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sBackgroundColor:I

    return v0
.end method

.method private static getAvatarSizeInPx(Landroid/content/Context;I)I
    .registers 3
    .parameter "context"
    .parameter "size"

    .prologue
    .line 84
    packed-switch p1, :pswitch_data_14

    .line 90
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 85
    :pswitch_5
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v0

    goto :goto_4

    .line 86
    :pswitch_a
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v0

    goto :goto_4

    .line 87
    :pswitch_f
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    move-result v0

    goto :goto_4

    .line 84
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_5
        :pswitch_a
        :pswitch_f
    .end packed-switch
.end method

.method public static getAvatarUrlSignature(Ljava/lang/String;)I
    .registers 3
    .parameter "avatarUrl"

    .prologue
    const/4 v1, 0x1

    .line 536
    if-nez p0, :cond_5

    move v0, v1

    .line 545
    :cond_4
    :goto_4
    return v0

    .line 540
    :cond_5
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 541
    .local v0, signature:I
    if-eqz v0, :cond_d

    if-ne v0, v1, :cond_4

    .line 542
    :cond_d
    const/4 v0, 0x2

    goto :goto_4
.end method

.method public static getMediumAvatarSize(Landroid/content/Context;)I
    .registers 3
    .parameter "context"

    .prologue
    .line 185
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sMediumAvatarSize:I

    if-nez v0, :cond_15

    .line 186
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sMediumAvatarSize:I

    .line 189
    :cond_15
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sMediumAvatarSize:I

    return v0
.end method

.method public static getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "context"

    .prologue
    .line 198
    sget-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMedium:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1b

    .line 199
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 200
    .local v1, resources:Landroid/content/res/Resources;
    const v2, 0x7f02009b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 201
    .local v0, drawable:Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0           #drawable:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMedium:Landroid/graphics/Bitmap;

    .line 203
    .end local v1           #resources:Landroid/content/res/Resources;
    :cond_1b
    sget-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMedium:Landroid/graphics/Bitmap;

    return-object v2
.end method

.method public static getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "context"
    .parameter "round"

    .prologue
    .line 212
    if-eqz p1, :cond_17

    sget-boolean v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    if-eqz v0, :cond_17

    .line 213
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMediumRound:Landroid/graphics/Bitmap;

    if-nez v0, :cond_14

    .line 214
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->getRoundedBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMediumRound:Landroid/graphics/Bitmap;

    .line 217
    :cond_14
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarMediumRound:Landroid/graphics/Bitmap;

    .line 219
    :goto_16
    return-object v0

    :cond_17
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_16
.end method

.method public static getSmallAvatarSize(Landroid/content/Context;)I
    .registers 3
    .parameter "context"

    .prologue
    .line 142
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sSmallAvatarSize:I

    if-nez v0, :cond_15

    .line 143
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sSmallAvatarSize:I

    .line 146
    :cond_15
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sSmallAvatarSize:I

    return v0
.end method

.method public static getSmallDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "context"

    .prologue
    .line 155
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmall:Landroid/graphics/Bitmap;

    if-nez v0, :cond_13

    .line 156
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmall:Landroid/graphics/Bitmap;

    .line 159
    :cond_13
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmall:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static getSmallDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "context"
    .parameter "round"

    .prologue
    .line 168
    if-eqz p1, :cond_17

    sget-boolean v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    if-eqz v0, :cond_17

    .line 169
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmallRound:Landroid/graphics/Bitmap;

    if-nez v0, :cond_14

    .line 170
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->getRoundedBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmallRound:Landroid/graphics/Bitmap;

    .line 173
    :cond_14
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarSmallRound:Landroid/graphics/Bitmap;

    .line 175
    :goto_16
    return-object v0

    :cond_17
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_16
.end method

.method public static getTinyAvatarSize(Landroid/content/Context;)I
    .registers 3
    .parameter "context"

    .prologue
    .line 99
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sTinyAvatarSize:I

    if-nez v0, :cond_15

    .line 100
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sTinyAvatarSize:I

    .line 103
    :cond_15
    sget v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sTinyAvatarSize:I

    return v0
.end method

.method public static getTinyDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .registers 4
    .parameter "context"

    .prologue
    .line 112
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTiny:Landroid/graphics/Bitmap;

    if-nez v0, :cond_13

    .line 113
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTiny:Landroid/graphics/Bitmap;

    .line 116
    :cond_13
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTiny:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static getTinyDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;
    .registers 3
    .parameter "context"
    .parameter "round"

    .prologue
    .line 125
    if-eqz p1, :cond_17

    sget-boolean v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    if-eqz v0, :cond_17

    .line 126
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTinyRound:Landroid/graphics/Bitmap;

    if-nez v0, :cond_14

    .line 127
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->getRoundedBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTinyRound:Landroid/graphics/Bitmap;

    .line 130
    :cond_14
    sget-object v0, Lcom/google/android/apps/plus/content/EsAvatarData;->sDefaultAvatarTinyRound:Landroid/graphics/Bitmap;

    .line 132
    :goto_16
    return-object v0

    :cond_17
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_16
.end method

.method private static loadAndroidContactAvatars(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;)V
    .registers 24
    .parameter "context"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/AvatarRequest;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/plus/content/AvatarRequest;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .line 412
    .local p1, requests:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    .local p2, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Lcom/google/android/apps/plus/content/AvatarRequest;[B>;"
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 413
    .local v2, resolver:Landroid/content/ContentResolver;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v10

    .line 414
    .local v10, count:I
    const/4 v13, 0x0

    .local v13, i:I
    :goto_9
    if-ge v13, v10, :cond_c2

    .line 415
    move-object/from16 v0, p1

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 416
    .local v19, request:Lcom/google/android/apps/plus/content/AvatarRequest;
    const/4 v14, 0x0

    .line 417
    .local v14, lookupKey:Ljava/lang/String;
    if-eqz v14, :cond_9c

    .line 418
    const/16 v18, 0x0

    .line 422
    .local v18, photoUri:Landroid/net/Uri;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xe

    if-ge v4, v6, :cond_a5

    .line 423
    const-wide/16 v15, 0x0

    .line 424
    .local v15, photoId:J
    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v4, v14}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 425
    .local v3, contactUri:Landroid/net/Uri;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "photo_id"

    aput-object v7, v4, v6

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 429
    .local v17, photoIdCursor:Landroid/database/Cursor;
    :try_start_35
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_42

    .line 430
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_41
    .catchall {:try_start_35 .. :try_end_41} :catchall_a0

    move-result-wide v15

    .line 433
    :cond_42
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 436
    const-wide/16 v6, 0x0

    cmp-long v4, v15, v6

    if-eqz v4, :cond_c3

    .line 437
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    move-wide v0, v15

    invoke-static {v4, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    .line 444
    .end local v15           #photoId:J
    .end local v17           #photoIdCursor:Landroid/database/Cursor;
    .end local v18           #photoUri:Landroid/net/Uri;
    .local v5, photoUri:Landroid/net/Uri;
    :goto_52
    const/4 v12, 0x0

    .line 445
    .local v12, data:[B
    if-eqz v5, :cond_73

    .line 446
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "data15"

    aput-object v7, v6, v4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v2

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 449
    .local v11, cursor:Landroid/database/Cursor;
    :try_start_65
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_70

    .line 450
    const/4 v4, 0x0

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_6f
    .catchall {:try_start_65 .. :try_end_6f} :catchall_b2

    move-result-object v12

    .line 453
    :cond_70
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 457
    .end local v11           #cursor:Landroid/database/Cursor;
    :cond_73
    if-eqz v12, :cond_95

    .line 458
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/content/AvatarRequest;->getSize()I

    move-result v4

    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->getAvatarSizeInPx(Landroid/content/Context;I)I

    move-result v20

    .line 459
    .local v20, size:I
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/apps/plus/content/AvatarRequest;->isRounded()Z

    move-result v4

    if-eqz v4, :cond_b7

    sget-boolean v4, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    if-eqz v4, :cond_b7

    .line 460
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getAvatarBackgroundColor(Landroid/content/Context;)I

    move-result v4

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v12, v1, v4}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToRoundBitmap(Landroid/content/Context;[BII)[B

    move-result-object v12

    .line 467
    .end local v20           #size:I
    :cond_95
    :goto_95
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    .end local v3           #contactUri:Landroid/net/Uri;
    .end local v5           #photoUri:Landroid/net/Uri;
    .end local v12           #data:[B
    :cond_9c
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_9

    .line 433
    .restart local v3       #contactUri:Landroid/net/Uri;
    .restart local v15       #photoId:J
    .restart local v17       #photoIdCursor:Landroid/database/Cursor;
    .restart local v18       #photoUri:Landroid/net/Uri;
    :catchall_a0
    move-exception v4

    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    throw v4

    .line 440
    .end local v3           #contactUri:Landroid/net/Uri;
    .end local v15           #photoId:J
    .end local v17           #photoIdCursor:Landroid/database/Cursor;
    :cond_a5
    sget-object v4, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v4, v14}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 441
    .restart local v3       #contactUri:Landroid/net/Uri;
    const-string v4, "photo"

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .end local v18           #photoUri:Landroid/net/Uri;
    .restart local v5       #photoUri:Landroid/net/Uri;
    goto :goto_52

    .line 453
    .restart local v11       #cursor:Landroid/database/Cursor;
    .restart local v12       #data:[B
    :catchall_b2
    move-exception v4

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v4

    .line 463
    .end local v11           #cursor:Landroid/database/Cursor;
    .restart local v20       #size:I
    :cond_b7
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getAvatarBackgroundColor(Landroid/content/Context;)I

    move-result v4

    move/from16 v0, v20

    invoke-static {v12, v0, v4}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap([BII)[B

    move-result-object v12

    goto :goto_95

    .line 469
    .end local v3           #contactUri:Landroid/net/Uri;
    .end local v5           #photoUri:Landroid/net/Uri;
    .end local v12           #data:[B
    .end local v14           #lookupKey:Ljava/lang/String;
    .end local v19           #request:Lcom/google/android/apps/plus/content/AvatarRequest;
    .end local v20           #size:I
    :cond_c2
    return-void

    .restart local v3       #contactUri:Landroid/net/Uri;
    .restart local v14       #lookupKey:Ljava/lang/String;
    .restart local v15       #photoId:J
    .restart local v17       #photoIdCursor:Landroid/database/Cursor;
    .restart local v18       #photoUri:Landroid/net/Uri;
    .restart local v19       #request:Lcom/google/android/apps/plus/content/AvatarRequest;
    :cond_c3
    move-object/from16 v5, v18

    .end local v18           #photoUri:Landroid/net/Uri;
    .restart local v5       #photoUri:Landroid/net/Uri;
    goto :goto_52
.end method

.method private static loadAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)[B
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "avatarUrl"
    .parameter "size"

    .prologue
    const/4 v4, 0x2

    .line 363
    new-instance v2, Lcom/google/android/apps/plus/content/AvatarImageRequest;

    invoke-static {p0, p4}, Lcom/google/android/apps/plus/content/EsAvatarData;->getAvatarSizeInPx(Landroid/content/Context;I)I

    move-result v3

    invoke-direct {v2, p2, p3, p4, v3}, Lcom/google/android/apps/plus/content/AvatarImageRequest;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 365
    .local v2, request:Lcom/google/android/apps/plus/content/AvatarImageRequest;
    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMedia(Landroid/content/Context;Lcom/google/android/apps/plus/content/CachedImageRequest;)[B

    move-result-object v0

    .line 366
    .local v0, image:[B
    if-eqz v0, :cond_12

    move-object v1, v0

    .line 404
    .end local v0           #image:[B
    .local v1, image:[B
    :goto_11
    return-object v1

    .line 370
    .end local v1           #image:[B
    .restart local v0       #image:[B
    :cond_12
    packed-switch p4, :pswitch_data_4c

    .line 400
    :cond_15
    :goto_15
    if-nez v0, :cond_1a

    .line 401
    invoke-static {p0, p1, v2}, Lcom/google/android/apps/plus/service/ImageDownloader;->downloadImage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/CachedImageRequest;)V

    :cond_1a
    move-object v1, v0

    .line 404
    .end local v0           #image:[B
    .restart local v1       #image:[B
    goto :goto_11

    .line 372
    .end local v1           #image:[B
    .restart local v0       #image:[B
    :pswitch_1c
    invoke-static {p0, p1, p2, p3, v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v0

    .line 373
    if-nez v0, :cond_27

    .line 374
    const/4 v3, 0x1

    invoke-static {p0, p1, p2, p3, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v0

    .line 377
    :cond_27
    if-eqz v0, :cond_15

    .line 378
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v3

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap([BI)[B

    move-result-object v0

    .line 380
    if-eqz v0, :cond_15

    .line 381
    invoke-static {p0, v2, v0}, Lcom/google/android/apps/plus/content/EsMediaCache;->insertMedia(Landroid/content/Context;Lcom/google/android/apps/plus/content/CachedImageRequest;[B)V

    goto :goto_15

    .line 388
    :pswitch_37
    invoke-static {p0, p1, p2, p3, v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v0

    .line 389
    if-eqz v0, :cond_15

    .line 390
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v3

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap([BI)[B

    move-result-object v0

    .line 392
    if-eqz v0, :cond_15

    .line 393
    invoke-static {p0, v2, v0}, Lcom/google/android/apps/plus/content/EsMediaCache;->insertMedia(Landroid/content/Context;Lcom/google/android/apps/plus/content/CachedImageRequest;[B)V

    goto :goto_15

    .line 370
    nop

    :pswitch_data_4c
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_37
    .end packed-switch
.end method

.method public static loadAvatarUrl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 553
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 556
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "contacts"

    sget-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    const-string v3, "gaia_id=?"

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 559
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_1b
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_34

    .line 560
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 561
    .local v8, compressedUrl:Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_34

    .line 562
    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2f
    .catchall {:try_start_1b .. :try_end_2f} :catchall_38

    move-result-object v5

    .line 566
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 568
    .end local v8           #compressedUrl:Ljava/lang/String;
    :goto_33
    return-object v5

    .line 566
    :cond_34
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_33

    :catchall_38
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static loadAvatars(Landroid/content/Context;Ljava/util/List;)Ljava/util/Map;
    .registers 11
    .parameter "context"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/AvatarRequest;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/plus/content/AvatarRequest;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 255
    .local p1, requests:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 256
    .local v6, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Lcom/google/android/apps/plus/content/AvatarRequest;[B>;"
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 257
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-nez v0, :cond_c

    .line 288
    :cond_b
    :goto_b
    return-object v6

    .line 261
    :cond_c
    const/4 v4, 0x0

    .line 262
    .local v4, hasAndroidContacts:Z
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 263
    .local v2, count:I
    const/4 v5, 0x0

    .local v5, i:I
    :goto_12
    if-ge v5, v2, :cond_1d

    .line 264
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 265
    add-int/lit8 v5, v5, 0x1

    goto :goto_12

    .line 270
    :cond_1d
    if-eqz v4, :cond_45

    .line 271
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 272
    .local v1, androidContactRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 273
    .local v3, googlePlusRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    const/4 v5, 0x0

    :goto_2a
    if-ge v5, v2, :cond_38

    .line 274
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 276
    .local v7, request:Lcom/google/android/apps/plus/content/AvatarRequest;
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    add-int/lit8 v5, v5, 0x1

    goto :goto_2a

    .line 281
    .end local v7           #request:Lcom/google/android/apps/plus/content/AvatarRequest;
    :cond_38
    invoke-static {p0, v1, v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadAndroidContactAvatars(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;)V

    .line 282
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_b

    .line 283
    invoke-static {p0, v0, v3, v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadGooglePlusAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Ljava/util/HashMap;)V

    goto :goto_b

    .line 286
    .end local v1           #androidContactRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    .end local v3           #googlePlusRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    :cond_45
    invoke-static {p0, v0, p1, v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadGooglePlusAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Ljava/util/HashMap;)V

    goto :goto_b
.end method

.method private static loadGooglePlusAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Ljava/util/HashMap;)V
    .registers 26
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/AvatarRequest;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/plus/content/AvatarRequest;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .line 296
    .local p2, requests:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    .local p3, map:Ljava/util/HashMap;,"Ljava/util/HashMap<Lcom/google/android/apps/plus/content/AvatarRequest;[B>;"
    const/4 v12, 0x0

    .line 297
    .local v12, avatarUrls:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/16 v18, 0x0

    .line 298
    .local v18, noUrlRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v19

    .line 299
    .local v19, reqCount:I
    const/4 v15, 0x0

    .local v15, i:I
    :goto_8
    move/from16 v0, v19

    if-ge v15, v0, :cond_2b

    .line 300
    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 301
    .local v20, request:Lcom/google/android/apps/plus/content/AvatarRequest;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AvatarRequest;->getAvatarUrl()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_28

    .line 302
    if-nez v18, :cond_21

    .line 303
    new-instance v18, Ljava/util/ArrayList;

    .end local v18           #noUrlRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 305
    .restart local v18       #noUrlRequests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    :cond_21
    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    :cond_28
    add-int/lit8 v15, v15, 0x1

    goto :goto_8

    .line 309
    .end local v20           #request:Lcom/google/android/apps/plus/content/AvatarRequest;
    :cond_2b
    if-eqz v18, :cond_a8

    .line 310
    new-instance v12, Ljava/util/HashMap;

    .end local v12           #avatarUrls:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 311
    .restart local v12       #avatarUrls:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 314
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 315
    .local v13, count:I
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    .line 316
    .local v21, sb:Ljava/lang/StringBuilder;
    new-array v7, v13, [Ljava/lang/String;

    .line 317
    .local v7, args:[Ljava/lang/String;
    const-string v4, "gaia_id IN ("

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    const/4 v15, 0x0

    :goto_4d
    if-ge v15, v13, :cond_67

    .line 319
    const-string v4, "?,"

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/AvatarRequest;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v15

    .line 318
    add-int/lit8 v15, v15, 0x1

    goto :goto_4d

    .line 322
    :cond_67
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 323
    const/16 v4, 0x29

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 325
    const-string v4, "contacts"

    sget-object v5, Lcom/google/android/apps/plus/content/EsAvatarData;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 328
    .local v14, cursor:Landroid/database/Cursor;
    :goto_88
    :try_start_88
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_a5

    .line 329
    const/4 v4, 0x0

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_9f
    .catchall {:try_start_88 .. :try_end_9f} :catchall_a0

    goto :goto_88

    .line 333
    :catchall_a0
    move-exception v4

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_a5
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 337
    .end local v3           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v7           #args:[Ljava/lang/String;
    .end local v13           #count:I
    .end local v14           #cursor:Landroid/database/Cursor;
    .end local v21           #sb:Ljava/lang/StringBuilder;
    :cond_a8
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, i$:Ljava/util/Iterator;
    :cond_ac
    :goto_ac
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_103

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/plus/content/AvatarRequest;

    .line 338
    .restart local v20       #request:Lcom/google/android/apps/plus/content/AvatarRequest;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AvatarRequest;->getAvatarUrl()Ljava/lang/String;

    move-result-object v11

    .line 339
    .local v11, avatarUrl:Ljava/lang/String;
    if-nez v11, :cond_ca

    if-eqz v12, :cond_ca

    .line 340
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AvatarRequest;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    .end local v11           #avatarUrl:Ljava/lang/String;
    check-cast v11, Ljava/lang/String;

    .line 342
    .restart local v11       #avatarUrl:Ljava/lang/String;
    :cond_ca
    if-nez v11, :cond_d5

    .line 343
    const/4 v4, 0x0

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_ac

    .line 345
    :cond_d5
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AvatarRequest;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AvatarRequest;->getSize()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v11, v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v17

    .line 347
    .local v17, image:[B
    if-eqz v17, :cond_ac

    .line 348
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AvatarRequest;->isRounded()Z

    move-result v4

    if-eqz v4, :cond_f9

    sget-boolean v4, Lcom/google/android/apps/plus/content/EsAvatarData;->sRoundAvatarsEnabled:Z

    if-eqz v4, :cond_f9

    .line 349
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->getRoundedBitmap(Landroid/content/Context;[B)[B

    move-result-object v17

    .line 351
    :cond_f9
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_ac

    .line 355
    .end local v11           #avatarUrl:Ljava/lang/String;
    .end local v17           #image:[B
    .end local v20           #request:Lcom/google/android/apps/plus/content/AvatarRequest;
    :cond_103
    return-void
.end method

.method public static uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "url"

    .prologue
    const/4 v4, 0x0

    .line 514
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 515
    const/4 v2, 0x0

    .line 532
    :goto_8
    return-object v2

    .line 518
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 519
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v2, "http://"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x7e

    if-ne v2, v3, :cond_49

    .line 521
    const/16 v2, 0x2f

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 522
    .local v1, slash:I
    sget-object v2, Lcom/google/android/apps/plus/content/EsAvatarData;->sFifeAbbrs:Ljava/util/HashMap;

    invoke-virtual {p0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 528
    .end local v1           #slash:I
    :goto_37
    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_44

    .line 529
    const-string v2, "photo.jpg"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 532
    :cond_44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_8

    .line 525
    :cond_49
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_37
.end method
