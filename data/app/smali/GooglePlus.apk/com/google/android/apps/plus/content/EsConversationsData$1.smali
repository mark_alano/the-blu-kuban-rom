.class final Lcom/google/android/apps/plus/content/EsConversationsData$1;
.super Ljava/lang/Object;
.source "EsConversationsData.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsConversationsData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .prologue
    .line 420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 8
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 420
    check-cast p1, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    .end local p1
    check-cast p2, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    .end local p2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getTimestamp()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getTimestamp()J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-nez v4, :cond_1a

    const/4 v0, 0x0

    :goto_19
    return v0

    :cond_1a
    cmp-long v0, v0, v2

    if-gez v0, :cond_20

    const/4 v0, 0x1

    goto :goto_19

    :cond_20
    const/4 v0, -0x1

    goto :goto_19
.end method
