.class public final Lcom/google/android/apps/plus/content/EsDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "EsDatabaseHelper.java"


# static fields
.field private static final MASTER_COLUMNS:[Ljava/lang/String;

.field private static sAlarmsInitialized:Z

.field private static sHelpers:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/plus/content/EsDatabaseHelper;",
            ">;"
        }
    .end annotation
.end field

.field private static sLastDatabaseDeletionTimestamp:J


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDeleted:Z

.field private mIndex:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 63
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->MASTER_COLUMNS:[Ljava/lang/String;

    .line 68
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->sHelpers:Landroid/util/SparseArray;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .registers 6
    .parameter "context"
    .parameter "index"

    .prologue
    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "es"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x477

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 129
    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mContext:Landroid/content/Context;

    .line 130
    iput p2, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mIndex:I

    .line 131
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/content/EsDatabaseHelper;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->doDeleteDatabase()V

    return-void
.end method

.method private declared-synchronized doDeleteDatabase()V
    .registers 6

    .prologue
    .line 365
    monitor-enter p0

    :try_start_1
    iget-boolean v3, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mDeleted:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_2e

    if-eqz v3, :cond_7

    .line 389
    :goto_5
    monitor-exit p0

    return-void

    .line 369
    :cond_7
    :try_start_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_a
    .catchall {:try_start_7 .. :try_end_a} :catchall_2e

    move-result-object v0

    .line 373
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_c
    const/4 v3, 0x3

    if-ge v2, v3, :cond_21

    .line 375
    :try_start_f
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 377
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mDeleted:Z

    .line 378
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sput-wide v3, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->sLastDatabaseDeletionTimestamp:J

    .line 380
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 381
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_21
    .catchall {:try_start_f .. :try_end_21} :catchall_2e
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_21} :catch_31

    .line 388
    :cond_21
    :try_start_21
    new-instance v3, Ljava/io/File;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z
    :try_end_2d
    .catchall {:try_start_21 .. :try_end_2d} :catchall_2e

    goto :goto_5

    .line 365
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v2           #i:I
    :catchall_2e
    move-exception v3

    monitor-exit p0

    throw v3

    .line 383
    .restart local v0       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v2       #i:I
    :catch_31
    move-exception v1

    .line 384
    .local v1, e:Ljava/lang/Throwable;
    :try_start_32
    const-string v3, "EsDatabaseHelper"

    const-string v4, "Cannot close database"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_39
    .catchall {:try_start_32 .. :try_end_39} :catchall_2e

    .line 373
    add-int/lit8 v2, v2, 0x1

    goto :goto_c
.end method

.method private static dropAllViews(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 11
    .parameter "db"

    .prologue
    const/4 v4, 0x0

    .line 310
    const-string v1, "sqlite_master"

    sget-object v2, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->MASTER_COLUMNS:[Ljava/lang/String;

    const-string v3, "type=\'view\'"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 312
    .local v8, viewCursor:Landroid/database/Cursor;
    if-eqz v8, :cond_37

    .line 314
    :goto_11
    :try_start_11
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 316
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 317
    .local v9, viewName:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DROP VIEW IF EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_2e
    .catchall {:try_start_11 .. :try_end_2e} :catchall_2f

    goto :goto_11

    .line 320
    .end local v9           #viewName:Ljava/lang/String;
    :catchall_2f
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_34
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 323
    :cond_37
    return-void
.end method

.method public static declared-synchronized getDatabaseHelper(Landroid/content/Context;I)Lcom/google/android/apps/plus/content/EsDatabaseHelper;
    .registers 7
    .parameter "context"
    .parameter "index"

    .prologue
    .line 99
    const-class v2, Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    monitor-enter v2

    if-nez p0, :cond_10

    .line 100
    :try_start_5
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v3, "Context is null"

    invoke-direct {v1, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_d
    .catchall {:try_start_5 .. :try_end_d} :catchall_d

    .line 99
    :catchall_d
    move-exception v1

    monitor-exit v2

    throw v1

    .line 102
    :cond_10
    if-gez p1, :cond_27

    .line 103
    :try_start_12
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid account index: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 105
    :cond_27
    sget-object v1, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->sHelpers:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    .line 106
    .local v0, helper:Lcom/google/android/apps/plus/content/EsDatabaseHelper;
    if-nez v0, :cond_3b

    .line 107
    new-instance v0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    .end local v0           #helper:Lcom/google/android/apps/plus/content/EsDatabaseHelper;
    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;-><init>(Landroid/content/Context;I)V

    .line 108
    .restart local v0       #helper:Lcom/google/android/apps/plus/content/EsDatabaseHelper;
    sget-object v1, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->sHelpers:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 111
    :cond_3b
    sget-boolean v1, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->sAlarmsInitialized:Z

    if-nez v1, :cond_48

    .line 112
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->scheduleDatabaseCleanupAlarm(Landroid/content/Context;)V

    .line 113
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->scheduleSyncAlarm(Landroid/content/Context;)V

    .line 114
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->sAlarmsInitialized:Z
    :try_end_48
    .catchall {:try_start_12 .. :try_end_48} :catchall_d

    .line 117
    :cond_48
    monitor-exit v2

    return-object v0
.end method

.method public static getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;
    .registers 4
    .parameter "context"
    .parameter "account"

    .prologue
    .line 85
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v0

    .line 86
    .local v0, index:I
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;I)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    return-object v1
.end method

.method static getRowsCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J
    .registers 15
    .parameter "db"
    .parameter "table"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 423
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "COUNT(*)"

    aput-object v0, v2, v1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 428
    .local v8, cc:Landroid/database/Cursor;
    :try_start_13
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 429
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1d
    .catchall {:try_start_13 .. :try_end_1d} :catchall_25

    move-result-wide v9

    .line 434
    .local v9, count:J
    :goto_1e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 437
    return-wide v9

    .line 431
    .end local v9           #count:J
    :cond_22
    const-wide/16 v9, 0x0

    .restart local v9       #count:J
    goto :goto_1e

    .line 434
    .end local v9           #count:J
    :catchall_25
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static isDatabaseRecentlyDeleted()Z
    .registers 5

    .prologue
    const/4 v0, 0x0

    .line 403
    sget-wide v1, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->sLastDatabaseDeletionTimestamp:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_a

    .line 407
    :cond_9
    :goto_9
    return v0

    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sget-wide v3, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->sLastDatabaseDeletionTimestamp:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0xea60

    cmp-long v1, v1, v3

    if-gez v1, :cond_9

    const/4 v0, 0x1

    goto :goto_9
.end method

.method private rebuildTables(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 4
    .parameter "db"

    .prologue
    .line 259
    iget-object v1, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 261
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->rebuildTables(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 262
    return-void
.end method

.method private static upgradeViews(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 6
    .parameter "db"

    .prologue
    .line 238
    const-string v3, "EsDatabaseHelper"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 239
    const-string v3, "EsDatabaseHelper"

    const-string v4, "Upgrade database views"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_10
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->getViewNames()[Ljava/lang/String;

    move-result-object v1

    .line 244
    .local v1, viewName:[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_15
    array-length v3, v1

    if-ge v0, v3, :cond_2f

    .line 245
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "DROP VIEW IF EXISTS "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 244
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 248
    :cond_2f
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->getViewSQLs()[Ljava/lang/String;

    move-result-object v2

    .line 249
    .local v2, viewSQL:[Ljava/lang/String;
    const/4 v0, 0x0

    :goto_34
    array-length v3, v2

    if-ge v0, v3, :cond_3f

    .line 250
    aget-object v3, v2, v0

    invoke-virtual {p0, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 249
    add-int/lit8 v0, v0, 0x1

    goto :goto_34

    .line 252
    :cond_3f
    return-void
.end method


# virtual methods
.method public final createNewDatabase()V
    .registers 2

    .prologue
    .line 395
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mDeleted:Z

    .line 396
    return-void
.end method

.method public final deleteDatabase()V
    .registers 5

    .prologue
    .line 351
    new-instance v0, Lcom/google/android/apps/plus/content/EsDatabaseHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/content/EsDatabaseHelper$1;-><init>(Lcom/google/android/apps/plus/content/EsDatabaseHelper;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 359
    return-void
.end method

.method public final declared-synchronized getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 3

    .prologue
    .line 330
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mDeleted:Z

    if-eqz v0, :cond_10

    .line 331
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    const-string v1, "Database deleted"

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_d

    .line 330
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 333
    :cond_10
    :try_start_10
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_d

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 3

    .prologue
    .line 341
    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mDeleted:Z

    if-eqz v0, :cond_10

    .line 342
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    const-string v1, "Database deleted"

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_d

    .line 341
    :catchall_d
    move-exception v0

    monitor-exit p0

    throw v0

    .line 344
    :cond_10
    :try_start_10
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_d

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 5
    .parameter "db"

    .prologue
    .line 148
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->getTableSQLs()[Ljava/lang/String;

    move-result-object v1

    .line 149
    .local v1, sqls:[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_5
    array-length v2, v1

    if-ge v0, v2, :cond_10

    .line 150
    aget-object v2, v1, v0

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 149
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 153
    :cond_10
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->getIndexSQLs()[Ljava/lang/String;

    move-result-object v1

    .line 154
    const/4 v0, 0x0

    :goto_15
    array-length v2, v1

    if-ge v0, v2, :cond_20

    .line 155
    aget-object v2, v1, v0

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_15

    .line 158
    :cond_20
    invoke-static {}, Lcom/google/android/apps/plus/content/EsProvider;->getViewSQLs()[Ljava/lang/String;

    move-result-object v1

    .line 159
    const/4 v0, 0x0

    :goto_25
    array-length v2, v1

    if-ge v0, v2, :cond_30

    .line 160
    aget-object v2, v1, v0

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_25

    .line 163
    :cond_30
    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsProvider;->insertVirtualCircles(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 165
    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/RingtoneUtils;->registerHangoutRingtoneIfNecessary(Landroid/content/Context;)V

    .line 166
    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 4
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->rebuildTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 174
    return-void
.end method

.method public final onOpen(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 3
    .parameter "db"

    .prologue
    .line 138
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z

    move-result v0

    if-nez v0, :cond_b

    .line 139
    const-string v0, "PRAGMA foreign_keys=ON;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 141
    :cond_b
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .registers 9
    .parameter "db"
    .parameter "oldVersion"
    .parameter "newVersion"

    .prologue
    .line 181
    const-string v2, "EsDatabaseHelper"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 182
    const-string v2, "EsDatabaseHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Upgrade database: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " --> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_27
    if-ge p3, p2, :cond_2d

    .line 188
    :try_start_29
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->rebuildTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 230
    :cond_2c
    :goto_2c
    return-void

    .line 192
    :cond_2d
    const/16 v2, 0x2f4

    if-ge p2, v2, :cond_82

    .line 195
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->rebuildTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 196
    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mIndex:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->onAccountUpgradeRequired(Landroid/content/Context;I)V

    .line 198
    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccountUnsafe(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 199
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_2c

    .line 200
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    const-string v3, "com.google.android.apps.plus.content.EsProvider"

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    invoke-static {v2, v3, v4}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_55
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_29 .. :try_end_55} :catch_56

    goto :goto_2c

    .line 223
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    :catch_56
    move-exception v1

    .line 224
    .local v1, e:Landroid/database/sqlite/SQLiteException;
    const-string v2, "EsDatabaseHelper"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7e

    .line 225
    const-string v2, "EsDatabaseHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to upgrade database: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " --> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 228
    :cond_7e
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->rebuildTables(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_2c

    .line 207
    .end local v1           #e:Landroid/database/sqlite/SQLiteException;
    :cond_82
    const/16 v2, 0x38f

    if-ge p2, v2, :cond_8d

    .line 208
    :try_start_86
    iget-object v2, p0, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/RingtoneUtils;->registerHangoutRingtoneIfNecessary(Landroid/content/Context;)V

    .line 209
    const/16 p2, 0x38f

    .line 212
    :cond_8d
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->rebuildTables(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 222
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->upgradeViews(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_93
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_86 .. :try_end_93} :catch_56

    goto :goto_2c
.end method

.method public final rebuildTables(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 12
    .parameter "db"
    .parameter "account"

    .prologue
    const/4 v4, 0x0

    .line 269
    const-string v1, "sqlite_master"

    sget-object v2, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->MASTER_COLUMNS:[Ljava/lang/String;

    const-string v3, "type=\'table\'"

    move-object v0, p1

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_47

    :cond_11
    :goto_11
    :try_start_11
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_44

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "android_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    const-string v2, "sqlite_"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DROP TABLE IF EXISTS "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_3e
    .catchall {:try_start_11 .. :try_end_3e} :catchall_3f

    goto :goto_11

    :catchall_3f
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_44
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 270
    :cond_47
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->dropAllViews(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 272
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 274
    if-eqz p2, :cond_71

    .line 275
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v8

    .line 276
    .local v8, gaiaId:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "UPDATE account_status SET user_id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "user_id IS NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 280
    .end local v8           #gaiaId:Ljava/lang/String;
    :cond_71
    return-void
.end method
