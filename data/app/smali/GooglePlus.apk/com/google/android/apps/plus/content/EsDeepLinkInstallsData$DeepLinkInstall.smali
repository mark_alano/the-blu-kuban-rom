.class public final Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;
.super Ljava/lang/Object;
.source "EsDeepLinkInstallsData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeepLinkInstall"
.end annotation


# instance fields
.field public final authorName:Ljava/lang/String;

.field public final creationSource:Ljava/lang/String;

.field public final data:Ljava/lang/String;

.field public final packageName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "authorName"
    .parameter "creationSource"
    .parameter "packageName"
    .parameter "data"

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->authorName:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->creationSource:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->packageName:Ljava/lang/String;

    .line 81
    iput-object p4, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->data:Ljava/lang/String;

    .line 82
    return-void
.end method

.method static synthetic access$000(Landroid/database/Cursor;)Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;
    .registers 6
    .parameter "x0"

    .prologue
    .line 44
    const-string v1, ""

    const-string v0, "embed_json"

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_42

    invoke-static {}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->getInstance()Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/EmbedClientItemJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v0, :cond_42

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EmbedClientItem;->deepLinkData:Lcom/google/api/services/plusi/model/DeepLinkData;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DeepLinkData;->deepLinkId:Ljava/lang/String;

    :goto_1e
    new-instance v1, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;

    const-string v2, "name"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "source_name"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "package_name"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_42
    move-object v0, v1

    goto :goto_1e
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 5

    .prologue
    .line 86
    const-string v0, "DeepLinkInstall: authorName=%s, appName=%s, packageName=%s data=%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->authorName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->creationSource:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->packageName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->data:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
