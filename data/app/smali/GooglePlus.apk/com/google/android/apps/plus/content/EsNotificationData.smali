.class public final Lcom/google/android/apps/plus/content/EsNotificationData;
.super Ljava/lang/Object;
.source "EsNotificationData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsNotificationData$NotificationQuery;,
        Lcom/google/android/apps/plus/content/EsNotificationData$NotificationTimestampsQuery;,
        Lcom/google/android/apps/plus/content/EsNotificationData$NotificationIdsQuery;,
        Lcom/google/android/apps/plus/content/EsNotificationData$IdAndTimestampQuery;
    }
.end annotation


# static fields
.field private static final MAP_CATEGORY:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAP_ENTITY_TYPE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final MAP_NOTIFICATION_TYPE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final mSyncLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .prologue
    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 269
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    .line 270
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    .line 271
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    .line 275
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "CIRCLE"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "EVENTS"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "GAMES"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "GENERIC_CATEGORY"

    const v2, 0xffff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "HANGOUT"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 281
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "MOBILE"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 282
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "PHOTOS"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "QUESTIONS"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "SQUARE"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "STREAM"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "SYSTEM"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    const-string v1, "TARGET"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "ACTIVITY"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "ALBUM"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_SHARE"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "DEPRECATED_SYSTEM_TACO"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "EVENT"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "MATERIALIZED_TORTILLA"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "PHOTO"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 298
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "QUESTION"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "RESHARED"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    const-string v1, "UNKNOWN_ENTITY_TYPE"

    const v2, 0xffff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "ASPEN_INVITE"

    const/16 v2, 0x4a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "BIRTHDAY_WISH"

    const/16 v2, 0x3f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_CONTACT_JOINED"

    const/16 v2, 0x45

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_DIGESTED_ADD"

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_EXPLICIT_INVITE"

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_INVITE_REQUEST"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_INVITEE_JOINED_ES"

    const/16 v2, 0x26

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_MEMBER_JOINED_ES"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_PERSONAL_ADD"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_RECIPROCATING_ADD"

    const/16 v2, 0x27

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_RECOMMEND_PEOPLE"

    const/16 v2, 0x42

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "CIRCLE_STATUS_CHANGE"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "DIGEST_SWEEP"

    const/16 v2, 0x46

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE_ADD_ADMIN"

    const/16 v2, 0x22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 316
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE_REMOVE_ADMIN"

    const/16 v2, 0x23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 318
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "ENTITYPROFILE_TRANSFER_OWNERSHIP"

    const/16 v2, 0x24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_BEFORE_REMINDER"

    const/16 v2, 0x3b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_CHANGE"

    const/16 v2, 0x35

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_CHECKIN"

    const/16 v2, 0x3a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_INVITE"

    const/16 v2, 0x2f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_INVITEE_CHANGE"

    const/16 v2, 0x39

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_PHOTOS_ADDED"

    const/16 v2, 0x3e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_PHOTOS_COLLECTION"

    const/16 v2, 0x38

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_PHOTOS_REMINDER"

    const/16 v2, 0x37

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_RSVP_CONFIRMATION"

    const/16 v2, 0x43

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "EVENTS_STARTING"

    const/16 v2, 0x36

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "GAMES_APPLICATION_MESSAGE"

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "GAMES_INVITE_REQUEST"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "GAMES_ONEUP_NOTIFICATION"

    const/16 v2, 0x49

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 334
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "GAMES_PERSONAL_MESSAGE"

    const/16 v2, 0x11

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "HANGOUT_INVITE"

    const/16 v2, 0x21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "MOBILE_NEW_CONVERSATION"

    const/16 v2, 0x1d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_CAMERASYNC_UPLOADED"

    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_FACE_SUGGESTED"

    const/16 v2, 0x29

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 340
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_PROFILE_PHOTO_SUGGESTED"

    const/16 v2, 0x44

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_PROFILE_PHOTO_SUGGESTION_ACCEPTED"

    const/16 v2, 0x47

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_TAG_ADDED_ON_PHOTO"

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "PHOTOS_TAGGED_IN_PHOTO"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_ANSWERER_FOLLOWUP"

    const/16 v2, 0x1e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_ASKER_FOLLOWUP"

    const/16 v2, 0x1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_DASHER_WELCOME"

    const/16 v2, 0x1b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_REFERRAL"

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_REPLY"

    const/16 v2, 0x16

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "QUESTIONS_UNANSWERED_QUESTION"

    const/16 v2, 0x1c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_INVITE"

    const/16 v2, 0x30

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_MEMBERSHIP_APPROVED"

    const/16 v2, 0x33

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_MEMBERSHIP_REQUEST"

    const/16 v2, 0x34

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_NAME_CHANGE"

    const/16 v2, 0x48

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_NEW_MODERATOR"

    const/16 v2, 0x41

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SQUARE_SUBSCRIPTION"

    const/16 v2, 0x31

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_AT_REPLY"

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_FOLLOWUP"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_FOR_PHOTO_TAGGED"

    const/16 v2, 0x19

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_FOR_PHOTO_TAGGER"

    const/16 v2, 0x1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_NEW"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_COMMENT_ON_MENTION"

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_LIKE"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_PLUSONE_COMMENT"

    const/16 v2, 0x15

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_PLUSONE_POST"

    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST_AT_REPLY"

    const/16 v2, 0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST_FROM_UNCIRCLED"

    const/16 v2, 0x3d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 380
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST_SHARED"

    const/16 v2, 0x18

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 381
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 382
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_POST_SUBSCRIBED"

    const/16 v2, 0x40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "STREAM_RESHARE"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_CELEBRITY_SUGGESTIONS"

    const/16 v2, 0x2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_CONNECTED_SITES"

    const/16 v2, 0x2e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_DO_NOT_USE"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_FRIEND_SUGGESTIONS"

    const/16 v2, 0x2c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_INVITE"

    const/16 v2, 0x25

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_TOOLTIP"

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "SYSTEM_WELCOME"

    const/16 v2, 0x2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "TARGET_SHARED"

    const/16 v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    sget-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    const-string v1, "UNKNOWN_NOTIFICATION_TYPE"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsNotificationData;->mSyncLock:Ljava/lang/Object;

    return-void
.end method

.method static cleanupData$3105fef4(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 13
    .parameter "db"

    .prologue
    const/4 v9, 0x0

    const-wide/16 v10, 0xc8

    const/4 v3, 0x0

    .line 1229
    const-string v0, "notifications"

    invoke-static {p0, v0, v3, v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getRowsCount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    const-string v0, "EsNotificationData"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_31

    const-string v0, "EsNotificationData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deleteOldNotifications, keep count: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", have: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_31
    sub-long v0, v4, v10

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-lez v0, :cond_93

    const-string v1, "notifications"

    sget-object v2, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationIdsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "timestamp ASC"

    sub-long/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_93

    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v0, 0x100

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    :try_start_55
    const-string v0, "notif_id IN("

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v0, 0x1

    :goto_5b
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_82

    if-eqz v0, :cond_7c

    move v0, v9

    :goto_64
    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v4, 0x27

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_76
    .catchall {:try_start_55 .. :try_end_76} :catchall_77

    goto :goto_5b

    :catchall_77
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_7c
    const/16 v4, 0x2c

    :try_start_7e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_64

    :cond_82
    const/16 v0, 0x29

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;
    :try_end_87
    .catchall {:try_start_7e .. :try_end_87} :catchall_77

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const-string v0, "notifications"

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1230
    :cond_93
    return-void
.end method

.method public static deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 2
    .parameter "context"
    .parameter "account"

    .prologue
    .line 1239
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 1240
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatNotifications;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 1241
    return-void
.end method

.method public static getLatestNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D
    .registers 14
    .parameter "context"
    .parameter "account"

    .prologue
    const-wide/high16 v10, -0x4010

    const/4 v3, 0x0

    .line 1040
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1043
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "notifications"

    sget-object v2, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationTimestampsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v7, "timestamp DESC"

    const-string v8, "1"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1047
    .local v9, cursor:Landroid/database/Cursor;
    if-nez v9, :cond_1e

    move-wide v1, v10

    .line 1059
    :goto_1d
    return-wide v1

    .line 1052
    :cond_1e
    :try_start_1e
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 1053
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getDouble(I)D
    :try_end_28
    .catchall {:try_start_1e .. :try_end_28} :catchall_32

    move-result-wide v1

    .line 1056
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1d

    :cond_2d
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-wide v1, v10

    .line 1059
    goto :goto_1d

    .line 1056
    :catchall_32
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static getNotificationsToDisplay(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/database/Cursor;
    .registers 12
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v9, 0x4

    const/4 v4, 0x0

    .line 1102
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1105
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "notifications"

    sget-object v2, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "read=0 AND seen=0 AND enabled=1"

    const-string v7, "timestamp DESC"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1110
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_72

    .line 1111
    const-string v1, "EsNotificationData"

    invoke-static {v1, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_72

    .line 1112
    :goto_22
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6e

    .line 1113
    const-string v1, "EsNotificationData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getNotificationsToDisplay: unread notification id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x1

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", coalescingCode: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", timestamp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x5

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_22

    .line 1123
    :cond_6e
    const/4 v1, -0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1127
    :cond_72
    return-object v8
.end method

.method private static getOldestUnreadNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D
    .registers 14
    .parameter "context"
    .parameter "account"

    .prologue
    const-wide/high16 v10, -0x4010

    const/4 v4, 0x0

    .line 1071
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1074
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "notifications"

    sget-object v2, Lcom/google/android/apps/plus/content/EsNotificationData$NotificationTimestampsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "read=0"

    const-string v7, "timestamp ASC"

    const-string v8, "1"

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1078
    .local v9, cursor:Landroid/database/Cursor;
    if-nez v9, :cond_1f

    move-wide v1, v10

    .line 1090
    :goto_1e
    return-wide v1

    .line 1083
    :cond_1f
    :try_start_1f
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2e

    .line 1084
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getDouble(I)D
    :try_end_29
    .catchall {:try_start_1f .. :try_end_29} :catchall_33

    move-result-wide v1

    .line 1087
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1e

    :cond_2e
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    move-wide v1, v10

    .line 1090
    goto :goto_1e

    .line 1087
    :catchall_33
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private static getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;)J
    .registers 6
    .parameter "db"

    .prologue
    .line 814
    const-string v1, "SELECT COUNT(*) FROM %s WHERE %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "notifications"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "read=0 AND seen=0 AND enabled=1"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    .line 818
    .local v0, statementForUnreadCount:Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    move-result-wide v1

    return-wide v1
.end method

.method private static insertNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;DDZ)V
    .registers 62
    .parameter "context"
    .parameter "account"
    .parameter
    .parameter "earliestNotificationTime"
    .parameter "lastReadTime"
    .parameter "fromTickle"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCoalescedItem;",
            ">;DDZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 492
    .local p2, notifications:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataCoalescedItem;>;"
    if-eqz p7, :cond_63

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_TICKLE:Lcom/google/android/apps/plus/analytics/OzActions;

    .line 495
    .local v12, actionForLogging:Lcom/google/android/apps/plus/analytics/OzActions;
    :goto_4
    if-eqz p7, :cond_66

    sget-object v46, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 498
    .local v46, startViewForLogging:Lcom/google/android/apps/plus/analytics/OzViews;
    :goto_8
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 502
    .local v3, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_10
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 503
    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsNotificationData;->getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v44

    .line 505
    .local v44, prevUnreadCount:J
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_69

    .line 506
    const-wide/16 v4, 0x0

    cmpg-double v4, p3, v4

    if-gtz v4, :cond_33

    .line 509
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 512
    :cond_33
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 515
    new-instance v25, Landroid/os/Bundle;

    invoke-direct/range {v25 .. v25}, Landroid/os/Bundle;-><init>()V

    .line 516
    .local v25, extras:Landroid/os/Bundle;
    const-string v4, "extra_num_unread_notifi"

    move-wide/from16 v0, v44

    long-to-int v5, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 518
    const-string v4, "extra_prev_num_unread_noti"

    move-wide/from16 v0, v44

    long-to-int v5, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 520
    new-instance v4, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-object/from16 v0, v46

    invoke-direct {v4, v0}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-static {v0, v1, v4, v12, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V
    :try_end_5f
    .catchall {:try_start_10 .. :try_end_5f} :catchall_9e

    .line 804
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 811
    :goto_62
    return-void

    .line 492
    .end local v3           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v12           #actionForLogging:Lcom/google/android/apps/plus/analytics/OzActions;
    .end local v25           #extras:Landroid/os/Bundle;
    .end local v44           #prevUnreadCount:J
    .end local v46           #startViewForLogging:Lcom/google/android/apps/plus/analytics/OzViews;
    :cond_63
    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->NOTIFICATION_FETCHED_FROM_USER_REFRESH:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_4

    .line 495
    .restart local v12       #actionForLogging:Lcom/google/android/apps/plus/analytics/OzActions;
    :cond_66
    sget-object v46, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_WIDGET:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_8

    .line 525
    .restart local v3       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v44       #prevUnreadCount:J
    .restart local v46       #startViewForLogging:Lcom/google/android/apps/plus/analytics/OzViews;
    :cond_69
    :try_start_69
    new-instance v50, Ljava/util/HashMap;

    invoke-direct/range {v50 .. v50}, Ljava/util/HashMap;-><init>()V

    .line 527
    .local v50, timestamps:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Double;>;"
    const-string v4, "notifications"

    sget-object v5, Lcom/google/android/apps/plus/content/EsNotificationData$IdAndTimestampQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_7a
    .catchall {:try_start_69 .. :try_end_7a} :catchall_9e

    move-result-object v19

    .line 530
    .local v19, cursor:Landroid/database/Cursor;
    :goto_7b
    :try_start_7b
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_a3

    .line 531
    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, v19

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    move-object/from16 v0, v50

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_98
    .catchall {:try_start_7b .. :try_end_98} :catchall_99

    goto :goto_7b

    .line 536
    :catchall_99
    move-exception v4

    :try_start_9a
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    throw v4
    :try_end_9e
    .catchall {:try_start_9a .. :try_end_9e} :catchall_9e

    .line 804
    .end local v19           #cursor:Landroid/database/Cursor;
    .end local v44           #prevUnreadCount:J
    .end local v50           #timestamps:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Double;>;"
    :catchall_9e
    move-exception v4

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4

    .line 536
    .restart local v19       #cursor:Landroid/database/Cursor;
    .restart local v44       #prevUnreadCount:J
    .restart local v50       #timestamps:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Double;>;"
    :cond_a3
    :try_start_a3
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 539
    const-string v4, "SELECT MAX(%s) FROM %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "timestamp"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string v7, "notifications"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v47

    .line 542
    .local v47, statementForLastTimestamp:Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual/range {v47 .. v47}, Landroid/database/sqlite/SQLiteStatement;->simpleQueryForLong()J

    move-result-wide v32

    .line 544
    .local v32, lastTimestamp:J
    new-instance v37, Ljava/util/ArrayList;

    invoke-direct/range {v37 .. v37}, Ljava/util/ArrayList;-><init>()V

    .line 545
    .local v37, notificationTypes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 548
    .local v18, coalescingCodes:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v53, Landroid/content/ContentValues;

    invoke-direct/range {v53 .. v53}, Landroid/content/ContentValues;-><init>()V

    .line 549
    .local v53, values:Landroid/content/ContentValues;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v26

    :cond_d4
    :goto_d4
    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5f8

    invoke-interface/range {v26 .. v26}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    .line 550
    .local v35, notif:Lcom/google/api/services/plusi/model/DataCoalescedItem;
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->id:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d4

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->coalescingCode:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d4

    .line 551
    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->id:Ljava/lang/String;

    move-object/from16 v28, v0

    .line 555
    .local v28, id:Ljava/lang/String;
    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->coalescingCode:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 556
    .local v17, coalescingCode:Ljava/lang/String;
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeDouble(Ljava/lang/Double;)D

    move-result-wide v48

    .line 557
    .local v48, timestamp:D
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->isEntityDeleted:Ljava/lang/Boolean;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v29

    .line 558
    .local v29, isEntityDeleted:Z
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->category:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_23d

    sget-object v5, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_23d

    sget-object v5, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_CATEGORY:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 559
    .local v16, category:I
    :goto_12e
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityReferenceType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_242

    sget-object v5, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_242

    sget-object v5, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_ENTITY_TYPE:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v23

    .line 563
    .local v23, entityType:I
    :goto_14c
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->isRead:Ljava/lang/Boolean;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v4

    if-nez v4, :cond_15a

    cmpg-double v4, v48, p5

    if-gtz v4, :cond_247

    :cond_15a
    const/16 v30, 0x1

    .line 566
    .local v30, isRead:Z
    :goto_15c
    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_206

    .line 567
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Notification id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", coalescingCode: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", category: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v35

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->category:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", filterType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v35

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->filterType:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", timestamp: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v48

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", read: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", entityDeleted: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", pushEnabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v35

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->pushEnabled:Ljava/lang/Boolean;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 575
    .local v34, logText:Ljava/lang/String;
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    if-eqz v4, :cond_1ff

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

    if-eqz v4, :cond_1ff

    .line 576
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v34

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", snippet: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v35

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntityEntityData;->summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EntitySummaryData;->summaryPlaintext:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v34

    .line 579
    :cond_1ff
    const-string v4, "EsNotificationData"

    move-object/from16 v0, v34

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    .end local v34           #logText:Ljava/lang/String;
    :cond_206
    move-object/from16 v0, v50

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v38

    check-cast v38, Ljava/lang/Double;

    .line 585
    .local v38, oldTimestamp:Ljava/lang/Double;
    if-eqz v38, :cond_24b

    invoke-virtual/range {v38 .. v38}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    cmpl-double v4, v4, v48

    if-nez v4, :cond_24b

    if-nez v30, :cond_24b

    .line 586
    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_d4

    .line 587
    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Ignore notification with same timestamp and not read. Id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_d4

    .line 558
    .end local v16           #category:I
    .end local v23           #entityType:I
    .end local v30           #isRead:Z
    .end local v38           #oldTimestamp:Ljava/lang/Double;
    :cond_23d
    const v16, 0xffff

    goto/16 :goto_12e

    .line 559
    .restart local v16       #category:I
    :cond_242
    const v23, 0xffff

    goto/16 :goto_14c

    .line 563
    .restart local v23       #entityType:I
    :cond_247
    const/16 v30, 0x0

    goto/16 :goto_15c

    .line 593
    .restart local v30       #isRead:Z
    .restart local v38       #oldTimestamp:Ljava/lang/Double;
    :cond_24b
    invoke-virtual/range {v53 .. v53}, Landroid/content/ContentValues;->clear()V

    .line 595
    const-string v4, "notif_id"

    move-object/from16 v0, v53

    move-object/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    const-string v4, "coalescing_code"

    move-object/from16 v0, v53

    move-object/from16 v1, v17

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const-string v4, "timestamp"

    invoke-static/range {v48 .. v49}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 598
    const-string v4, "category"

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 599
    const-string v4, "entity_type"

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 604
    const/4 v4, 0x1

    move/from16 v0, v16

    if-ne v0, v4, :cond_297

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityReference:Ljava/lang/String;

    if-eqz v4, :cond_297

    .line 605
    const-string v4, "activity_id"

    move-object/from16 v0, v35

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityReference:Ljava/lang/String;

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    :cond_297
    const/16 v36, 0x0

    .line 610
    .local v36, notificationType:I
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->action:Ljava/util/List;

    if-eqz v4, :cond_35c

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->action:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_35c

    .line 611
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->action:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/DataAction;

    .line 612
    .local v11, action:Lcom/google/api/services/plusi/model/DataAction;
    if-eqz v11, :cond_35c

    iget-object v4, v11, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    if-eqz v4, :cond_35c

    iget-object v4, v11, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_35c

    .line 616
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 617
    .local v14, actors:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    iget-object v4, v11, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    .local v27, i$:Ljava/util/Iterator;
    :cond_2cd
    :goto_2cd
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_326

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Lcom/google/api/services/plusi/model/DataItem;

    .line 618
    .local v31, item:Lcom/google/api/services/plusi/model/DataItem;
    move-object/from16 v0, v31

    iget-object v13, v0, Lcom/google/api/services/plusi/model/DataItem;->actor:Lcom/google/api/services/plusi/model/DataActor;

    .line 619
    .local v13, actor:Lcom/google/api/services/plusi/model/DataActor;
    if-eqz v13, :cond_2cd

    .line 620
    iget-object v4, v13, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    if-eqz v4, :cond_2eb

    .line 621
    iget-object v4, v13, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v13, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    .line 623
    :cond_2eb
    invoke-interface {v14, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 624
    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2cd

    .line 625
    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "- Actor name: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v13, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " gaiaId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v13, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " photoUrl: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v13, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2cd

    .line 631
    .end local v13           #actor:Lcom/google/api/services/plusi/model/DataActor;
    .end local v31           #item:Lcom/google/api/services/plusi/model/DataItem;
    :cond_326
    invoke-interface {v14}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_337

    .line 632
    const-string v4, "circle_data"

    invoke-static {v14}, Lcom/google/android/apps/plus/content/DbDataActor;->serializeDataActorList(Ljava/util/List;)[B

    move-result-object v5

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 642
    :cond_337
    iget-object v4, v11, Lcom/google/api/services/plusi/model/DataAction;->item:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataItem;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataItem;->notificationType:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_581

    sget-object v5, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_581

    sget-object v5, Lcom/google/android/apps/plus/content/EsNotificationData;->MAP_NOTIFICATION_TYPE:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v36

    .line 646
    .end local v11           #action:Lcom/google/api/services/plusi/model/DataAction;
    .end local v14           #actors:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    .end local v27           #i$:Ljava/util/Iterator;
    :cond_35c
    :goto_35c
    const-string v4, "notification_type"

    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 647
    const-string v4, "read"

    invoke-static/range {v30 .. v30}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 648
    const-string v4, "seen"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 653
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->pushEnabled:Ljava/lang/Boolean;

    if-eqz v4, :cond_585

    .line 654
    const-string v4, "enabled"

    move-object/from16 v0, v35

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->pushEnabled:Ljava/lang/Boolean;

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 660
    :goto_38f
    const/16 v4, 0xa

    move/from16 v0, v16

    if-eq v0, v4, :cond_39b

    invoke-static/range {v36 .. v36}, Lcom/google/android/apps/plus/content/EsNotificationData;->isEventNotificationType(I)Z

    move-result v4

    if-eqz v4, :cond_592

    :cond_39b
    const/16 v24, 0x1

    .line 663
    .local v24, eventNotification:Z
    :goto_39d
    const/16 v20, 0x0

    .line 664
    .local v20, displayMessage:Ljava/lang/String;
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    if-eqz v4, :cond_534

    .line 665
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

    if-eqz v4, :cond_3b7

    .line 666
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->summarySnippet:Lcom/google/api/services/plusi/model/EntitySummaryData;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/EntitySummaryData;->summaryPlaintext:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 672
    :cond_3b7
    const/4 v15, 0x0

    .line 673
    .local v15, albumId:Ljava/lang/String;
    const/16 v39, 0x0

    .line 674
    .local v39, ownerId:Ljava/lang/String;
    const/16 v41, 0x0

    .line 677
    .local v41, photoId:Ljava/lang/Long;
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    if-eqz v4, :cond_416

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    if-eqz v4, :cond_416

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v4, :cond_416

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;

    if-eqz v4, :cond_416

    .line 681
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;

    move-object/from16 v43, v0

    .line 682
    .local v43, plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;
    move-object/from16 v0, v43

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusPhoto;->ownerObfuscatedId:Ljava/lang/String;

    move-object/from16 v39, v0

    .line 683
    move-object/from16 v0, v43

    iget-object v15, v0, Lcom/google/api/services/plusi/model/PlusPhoto;->albumId:Ljava/lang/String;
    :try_end_400
    .catchall {:try_start_a3 .. :try_end_400} :catchall_9e

    .line 685
    :try_start_400
    move-object/from16 v0, v43

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusPhoto;->photoId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_416

    .line 686
    move-object/from16 v0, v43

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PlusPhoto;->photoId:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_415
    .catchall {:try_start_400 .. :try_end_415} :catchall_9e
    .catch Ljava/lang/NumberFormatException; {:try_start_400 .. :try_end_415} :catch_596

    move-result-object v41

    .line 696
    .end local v43           #plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;
    :cond_416
    :goto_416
    const/4 v4, 0x3

    move/from16 v0, v16

    if-ne v0, v4, :cond_478

    :try_start_41b
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->photos:Lcom/google/api/services/plusi/model/EntityPhotosData;

    if-eqz v4, :cond_478

    .line 699
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->photos:Lcom/google/api/services/plusi/model/EntityPhotosData;

    move-object/from16 v22, v0

    .line 700
    .local v22, entityPhotosData:Lcom/google/api/services/plusi/model/EntityPhotosData;
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    if-eqz v4, :cond_478

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_478

    .line 702
    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/api/services/plusi/model/EntityPhotosData;->photo:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/google/api/services/plusi/model/DataPhoto;
    :try_end_446
    .catchall {:try_start_41b .. :try_end_446} :catchall_9e

    .line 703
    .local v40, photo:Lcom/google/api/services/plusi/model/DataPhoto;
    if-eqz v40, :cond_478

    .line 705
    :try_start_448
    move-object/from16 v0, v40

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_45e

    .line 706
    move-object/from16 v0, v40

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->id:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_45d
    .catchall {:try_start_448 .. :try_end_45d} :catchall_9e
    .catch Ljava/lang/NumberFormatException; {:try_start_448 .. :try_end_45d} :catch_5b8

    move-result-object v41

    .line 713
    :cond_45e
    :goto_45e
    :try_start_45e
    move-object/from16 v0, v40

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    if-eqz v4, :cond_46a

    .line 714
    move-object/from16 v0, v40

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->album:Lcom/google/api/services/plusi/model/DataAlbum;

    iget-object v15, v4, Lcom/google/api/services/plusi/model/DataAlbum;->id:Ljava/lang/String;

    .line 716
    :cond_46a
    move-object/from16 v0, v40

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    if-eqz v4, :cond_478

    .line 717
    move-object/from16 v0, v40

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataPhoto;->owner:Lcom/google/api/services/plusi/model/DataUser;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    move-object/from16 v39, v0

    .line 723
    .end local v22           #entityPhotosData:Lcom/google/api/services/plusi/model/EntityPhotosData;
    .end local v40           #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    :cond_478
    const-string v4, "EsNotificationData"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4bb

    invoke-static/range {v39 .. v39}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_48f

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_48f

    if-eqz v41, :cond_4bb

    .line 726
    :cond_48f
    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "- Photo ownerId: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v39

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " albumId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " photoId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    :cond_4bb
    invoke-static/range {v39 .. v39}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4ca

    .line 731
    const-string v4, "pd_gaia_id"

    move-object/from16 v0, v53

    move-object/from16 v1, v39

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    :cond_4ca
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4d7

    .line 734
    const-string v4, "pd_album_id"

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    :cond_4d7
    if-eqz v41, :cond_4e2

    .line 737
    const-string v4, "pd_photo_id"

    move-object/from16 v0, v53

    move-object/from16 v1, v41

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 743
    :cond_4e2
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    if-eqz v4, :cond_534

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    if-eqz v4, :cond_534

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    if-eqz v4, :cond_534

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v4, :cond_534

    .line 747
    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->entityData:Lcom/google/api/services/plusi/model/EntityEntityData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityEntityData;->update:Lcom/google/api/services/plusi/model/EntityUpdateData;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/EntityUpdateData;->activity:Lcom/google/api/services/plusi/model/Update;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Update;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/EmbedClientItem;->plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v42, v0

    .line 749
    .local v42, plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;
    const-string v4, "ed_event_id"

    move-object/from16 v0, v42

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    const-string v4, "ed_creator_id"

    move-object/from16 v0, v42

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    const/16 v24, 0x1

    .line 758
    .end local v15           #albumId:Ljava/lang/String;
    .end local v39           #ownerId:Ljava/lang/String;
    .end local v41           #photoId:Ljava/lang/Long;
    .end local v42           #plusEvent:Lcom/google/api/services/plusi/model/PlusEvent;
    :cond_534
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_53c

    if-eqz v29, :cond_547

    .line 759
    :cond_53c
    if-eqz v24, :cond_5da

    .line 760
    const v4, 0x7f0801e1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 772
    :cond_547
    :goto_547
    const-string v4, "message"

    move-object/from16 v0, v53

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    const-string v5, "ed_event"

    if-eqz v24, :cond_5f5

    const/4 v4, 0x1

    :goto_555
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v53

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 775
    const-string v4, "notifications"

    const-string v5, "coalescing_code"

    const/4 v6, 0x5

    move-object/from16 v0, v53

    invoke-virtual {v3, v4, v5, v0, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 780
    move-wide/from16 v0, v32

    long-to-double v4, v0

    cmpl-double v4, v48, v4

    if-lez v4, :cond_d4

    .line 781
    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 782
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_d4

    .line 642
    .end local v20           #displayMessage:Ljava/lang/String;
    .end local v24           #eventNotification:Z
    .restart local v11       #action:Lcom/google/api/services/plusi/model/DataAction;
    .restart local v14       #actors:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    .restart local v27       #i$:Ljava/util/Iterator;
    :cond_581
    const/16 v36, 0x0

    goto/16 :goto_35c

    .line 657
    .end local v11           #action:Lcom/google/api/services/plusi/model/DataAction;
    .end local v14           #actors:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    .end local v27           #i$:Ljava/util/Iterator;
    :cond_585
    const-string v4, "enabled"

    invoke-static/range {v30 .. v30}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v53

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_38f

    .line 660
    :cond_592
    const/16 v24, 0x0

    goto/16 :goto_39d

    .line 688
    .restart local v15       #albumId:Ljava/lang/String;
    .restart local v20       #displayMessage:Ljava/lang/String;
    .restart local v24       #eventNotification:Z
    .restart local v39       #ownerId:Ljava/lang/String;
    .restart local v41       #photoId:Ljava/lang/Long;
    .restart local v43       #plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;
    :catch_596
    move-exception v21

    .line 689
    .local v21, e:Ljava/lang/NumberFormatException;
    const-string v4, "EsNotificationData"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_416

    .line 690
    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid photoId "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_416

    .line 708
    .end local v21           #e:Ljava/lang/NumberFormatException;
    .end local v43           #plusPhoto:Lcom/google/api/services/plusi/model/PlusPhoto;
    .restart local v22       #entityPhotosData:Lcom/google/api/services/plusi/model/EntityPhotosData;
    .restart local v40       #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    :catch_5b8
    move-exception v21

    .line 709
    .restart local v21       #e:Ljava/lang/NumberFormatException;
    const-string v4, "EsNotificationData"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_45e

    .line 710
    const-string v4, "EsNotificationData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid photoId "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_45e

    .line 762
    .end local v15           #albumId:Ljava/lang/String;
    .end local v21           #e:Ljava/lang/NumberFormatException;
    .end local v22           #entityPhotosData:Lcom/google/api/services/plusi/model/EntityPhotosData;
    .end local v39           #ownerId:Ljava/lang/String;
    .end local v40           #photo:Lcom/google/api/services/plusi/model/DataPhoto;
    .end local v41           #photoId:Ljava/lang/Long;
    :cond_5da
    const/4 v4, 0x3

    move/from16 v0, v16

    if-ne v0, v4, :cond_5ea

    .line 763
    const v4, 0x7f0801e2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_547

    .line 767
    :cond_5ea
    const v4, 0x7f0801e0

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    goto/16 :goto_547

    .line 773
    :cond_5f5
    const/4 v4, 0x0

    goto/16 :goto_555

    .line 787
    .end local v16           #category:I
    .end local v17           #coalescingCode:Ljava/lang/String;
    .end local v20           #displayMessage:Ljava/lang/String;
    .end local v23           #entityType:I
    .end local v24           #eventNotification:Z
    .end local v28           #id:Ljava/lang/String;
    .end local v29           #isEntityDeleted:Z
    .end local v30           #isRead:Z
    .end local v35           #notif:Lcom/google/api/services/plusi/model/DataCoalescedItem;
    .end local v36           #notificationType:I
    .end local v38           #oldTimestamp:Ljava/lang/Double;
    .end local v48           #timestamp:D
    :cond_5f8
    new-instance v25, Landroid/os/Bundle;

    invoke-direct/range {v25 .. v25}, Landroid/os/Bundle;-><init>()V

    .line 788
    .restart local v25       #extras:Landroid/os/Bundle;
    invoke-virtual/range {v37 .. v37}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_61f

    invoke-virtual/range {v37 .. v37}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v4, v5, :cond_61f

    .line 790
    const-string v4, "extra_notification_types"

    move-object/from16 v0, v25

    move-object/from16 v1, v37

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 792
    const-string v4, "extra_coalescing_codes"

    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 795
    :cond_61f
    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsNotificationData;->getUnreadCount(Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v51

    .line 796
    .local v51, unreadCount:J
    const-string v4, "extra_num_unread_notifi"

    move-wide/from16 v0, v51

    long-to-int v5, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 797
    const-string v4, "extra_prev_num_unread_noti"

    move-wide/from16 v0, v44

    long-to-int v5, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 799
    new-instance v4, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-object/from16 v0, v46

    invoke-direct {v4, v0}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-static {v0, v1, v4, v12, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 802
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_64a
    .catchall {:try_start_45e .. :try_end_64a} :catchall_9e

    .line 804
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 808
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v5, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_62
.end method

.method public static isCommentNotificationType(I)Z
    .registers 2
    .parameter "type"

    .prologue
    .line 1307
    sparse-switch p0, :sswitch_data_8

    .line 1317
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 1314
    :sswitch_5
    const/4 v0, 0x1

    goto :goto_4

    .line 1307
    nop

    :sswitch_data_8
    .sparse-switch
        0x2 -> :sswitch_5
        0x3 -> :sswitch_5
        0xe -> :sswitch_5
        0xf -> :sswitch_5
        0x19 -> :sswitch_5
        0x1a -> :sswitch_5
    .end sparse-switch
.end method

.method public static isEventNotificationType(I)Z
    .registers 2
    .parameter "type"

    .prologue
    .line 1283
    packed-switch p0, :pswitch_data_8

    .line 1297
    :pswitch_3
    const/4 v0, 0x0

    :goto_4
    return v0

    .line 1294
    :pswitch_5
    const/4 v0, 0x1

    goto :goto_4

    .line 1283
    nop

    :pswitch_data_8
    .packed-switch 0x2f
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public static markAllNotificationsAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 8
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 995
    const-string v2, "EsNotificationData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 996
    const-string v2, "EsNotificationData"

    const-string v3, "markAllNotificationsAsRead"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    :cond_12
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1002
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 1003
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "read"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1005
    const-string v2, "notifications"

    invoke-virtual {v0, v2, v1, v4, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1007
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    invoke-static {v3, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1009
    return-void
.end method

.method public static markAllNotificationsAsSeen(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 8
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1018
    const-string v2, "EsNotificationData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 1019
    const-string v2, "EsNotificationData"

    const-string v3, "markAllNotificationsAsSeen"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    :cond_12
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1025
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 1026
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "seen"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1028
    const-string v2, "notifications"

    invoke-virtual {v0, v2, v1, v5, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1029
    return-void
.end method

.method public static markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "notificationId"

    .prologue
    const/4 v5, 0x1

    .line 971
    const-string v2, "EsNotificationData"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 972
    const-string v2, "EsNotificationData"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "markNotificationAsRead: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    :cond_1e
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 978
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 979
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "read"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 981
    const-string v2, "notifications"

    const-string v3, "notif_id=?"

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 984
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->NOTIFICATIONS_URI:Landroid/net/Uri;

    invoke-static {v3, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 986
    return-void
.end method

.method public static syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V
    .registers 29
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "listener"
    .parameter "fromTickle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 832
    sget-object v21, Lcom/google/android/apps/plus/content/EsNotificationData;->mSyncLock:Ljava/lang/Object;

    monitor-enter v21

    .line 833
    :try_start_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 834
    monitor-exit v21

    .line 959
    :goto_a
    return-void

    .line 837
    :cond_b
    const-string v3, "EsNotificationData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1b

    .line 838
    const-string v3, "EsNotificationData"

    const-string v4, "syncNotifications starting sync stream"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    :cond_1b
    const-string v3, "Notifications"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 844
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsNotificationData;->getOldestUnreadNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D

    move-result-wide v6

    .line 845
    .local v6, timestamp:D
    const-wide/16 v3, 0x0

    cmpg-double v3, v6, v3

    if-gez v3, :cond_10d

    .line 847
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsNotificationData;->getLatestNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D

    move-result-wide v6

    .line 848
    const-string v3, "EsNotificationData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4d

    .line 849
    const-string v3, "EsNotificationData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "syncNotifications latest notification: "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    :cond_4d
    const-wide/16 v3, 0x0

    cmpl-double v3, v6, v3

    if-lez v3, :cond_56

    .line 853
    const-wide/high16 v3, 0x3ff0

    add-double/2addr v6, v3

    .line 865
    :cond_56
    :goto_56
    const/4 v12, 0x0

    .line 866
    .local v12, continuationToken:Ljava/lang/String;
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    .line 868
    .local v20, notificationsMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCoalescedItem;>;"
    const/4 v13, 0x0

    .line 869
    .local v13, error:Z
    const-wide/16 v8, 0x0

    .line 870
    .local v8, lastReadTime:D
    const/16 v16, 0x0

    .local v16, i:I
    :goto_61
    const/4 v3, 0x3

    move/from16 v0, v16

    if-ge v0, v3, :cond_df

    .line 871
    const-string v3, "EsNotificationData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8f

    .line 872
    const-string v3, "EsNotificationData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "syncNotifications continuation token: "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, ", chunk: "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    :cond_8f
    new-instance v15, Lcom/google/android/apps/plus/api/GetNotificationsOperation;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-direct {v15, v0, v1, v3, v2}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 878
    .local v15, gno:Lcom/google/android/apps/plus/api/GetNotificationsOperation;
    if-nez v16, :cond_12c

    move-wide v3, v6

    :goto_9e
    invoke-virtual {v15, v3, v4, v12}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getNotifications(DLjava/lang/String;)V

    .line 879
    new-instance v3, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v3}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v15, v0, v3}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    .line 880
    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->hasError()Z

    move-result v3

    if-eqz v3, :cond_130

    .line 882
    const-string v3, "EsNotificationData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_de

    .line 883
    const-string v3, "EsNotificationData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "syncNotifications error in chunk: "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, " with error code "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getErrorCode()I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    :cond_de
    const/4 v13, 0x1

    .line 940
    .end local v15           #gno:Lcom/google/android/apps/plus/api/GetNotificationsOperation;
    :cond_df
    if-nez v13, :cond_fe

    .line 941
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface/range {v20 .. v20}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 944
    .local v5, notifications:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataCoalescedItem;>;"
    new-instance v3, Lcom/google/android/apps/plus/content/EsNotificationData$1;

    invoke-direct {v3}, Lcom/google/android/apps/plus/content/EsNotificationData$1;-><init>()V

    invoke-static {v5, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v10, p4

    .line 951
    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/plus/content/EsNotificationData;->insertNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;DDZ)V

    .line 955
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/service/AndroidNotification;->update(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 958
    .end local v5           #notifications:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataCoalescedItem;>;"
    :cond_fe
    invoke-interface/range {v20 .. v20}, Ljava/util/Map;->size()I

    move-result v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    .line 959
    monitor-exit v21
    :try_end_108
    .catchall {:try_start_3 .. :try_end_108} :catchall_10a

    goto/16 :goto_a

    .end local v6           #timestamp:D
    .end local v8           #lastReadTime:D
    .end local v12           #continuationToken:Ljava/lang/String;
    .end local v13           #error:Z
    .end local v16           #i:I
    .end local v20           #notificationsMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCoalescedItem;>;"
    :catchall_10a
    move-exception v3

    monitor-exit v21

    throw v3

    .line 858
    .restart local v6       #timestamp:D
    :cond_10d
    :try_start_10d
    const-string v3, "EsNotificationData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_56

    .line 859
    const-string v3, "EsNotificationData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "syncNotifications oldest unread time: "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_56

    .line 878
    .restart local v8       #lastReadTime:D
    .restart local v12       #continuationToken:Ljava/lang/String;
    .restart local v13       #error:Z
    .restart local v15       #gno:Lcom/google/android/apps/plus/api/GetNotificationsOperation;
    .restart local v16       #i:I
    .restart local v20       #notificationsMap:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCoalescedItem;>;"
    :cond_12c
    const-wide/16 v3, 0x0

    goto/16 :goto_9e

    .line 890
    :cond_130
    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getLastReadTime()Ljava/lang/Double;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeDouble(Ljava/lang/Double;)D

    move-result-wide v8

    .line 892
    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getNotifications()Ljava/util/List;

    move-result-object v11

    .line 893
    .local v11, chunkNotifications:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataCoalescedItem;>;"
    if-eqz v11, :cond_df

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_df

    .line 897
    const-string v3, "EsNotificationData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_165

    .line 901
    const-string v3, "EsNotificationData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "syncNotifications retrieved: "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 906
    :cond_165
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, i$:Ljava/util/Iterator;
    :cond_169
    :goto_169
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_214

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    .line 907
    .local v19, notif:Lcom/google/api/services/plusi/model/DataCoalescedItem;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->id:Ljava/lang/String;

    move-object/from16 v18, v0

    .line 908
    .local v18, id:Ljava/lang/String;
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/api/services/plusi/model/DataCoalescedItem;

    .line 909
    .local v14, existingNotif:Lcom/google/api/services/plusi/model/DataCoalescedItem;
    if-eqz v14, :cond_209

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    if-eqz v3, :cond_209

    iget-object v3, v14, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    if-eqz v3, :cond_209

    .line 913
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    iget-object v10, v14, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v22

    cmpl-double v3, v3, v22

    if-lez v3, :cond_1da

    .line 915
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 917
    const-string v3, "EsNotificationData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_169

    .line 918
    const-string v3, "EsNotificationData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "syncNotifications replacing notification: "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, " is newer: "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_169

    .line 922
    :cond_1da
    const-string v3, "EsNotificationData"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_169

    .line 923
    const-string v3, "EsNotificationData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "syncNotifications ignoring notification: "

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v10, " is older: "

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataCoalescedItem;->timestamp:Ljava/lang/Double;

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_169

    .line 928
    :cond_209
    move-object/from16 v0, v20

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_169

    .line 932
    .end local v14           #existingNotif:Lcom/google/api/services/plusi/model/DataCoalescedItem;
    .end local v18           #id:Ljava/lang/String;
    .end local v19           #notif:Lcom/google/api/services/plusi/model/DataCoalescedItem;
    :cond_214
    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getContinuationToken()Ljava/lang/String;

    move-result-object v12

    .line 933
    if-eqz v12, :cond_df

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v3

    int-to-long v3, v3

    invoke-static {}, Lcom/google/android/apps/plus/api/GetNotificationsOperation;->getMaxNotifications()J
    :try_end_222
    .catchall {:try_start_10d .. :try_end_222} :catchall_10a

    move-result-wide v22

    cmp-long v3, v3, v22

    if-ltz v3, :cond_df

    .line 936
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_61
.end method
