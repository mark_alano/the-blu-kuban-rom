.class public final Lcom/google/android/apps/plus/content/EsPeopleData;
.super Ljava/lang/Object;
.source "EsPeopleData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;,
        Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;,
        Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;,
        Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    }
.end annotation


# static fields
.field private static final CIRCLES_PROJECTION:[Ljava/lang/String;

.field public static final CONTACT_INFO_JSON:Lcom/google/android/apps/plus/json/EsJson;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/plus/json/EsJson",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final PROFILE_COLUMNS:[Ljava/lang/String;

.field private static final SUGGESTED_PEOPLE_COLUMNS:[Ljava/lang/String;

.field private static final USERS_PROJECTION:[Ljava/lang/String;

.field private static final sCircleSyncLock:Ljava/lang/Object;

.field public static sHandler:Landroid/os/Handler;

.field private static volatile sInitialSyncLatch:Ljava/util/concurrent/CountDownLatch;

.field private static final sMyProfileSyncLock:Ljava/lang/Object;

.field private static final sPeopleSyncLock:Ljava/lang/Object;

.field private static sProfileFetchLocks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final sSuggestedCelebritiesSyncLock:Ljava/lang/Object;

.field private static final sSuggestedPeopleSyncLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 251
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "circle_id"

    aput-object v1, v0, v5

    const-string v1, "circle_name"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v6

    const-string v1, "contact_count"

    aput-object v1, v0, v7

    const-string v1, "semantic_hints"

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->CIRCLES_PROJECTION:[Ljava/lang/String;

    .line 265
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "profile_state"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "profile_type"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->CONTACT_PROJECTION:[Ljava/lang/String;

    .line 275
    new-array v0, v8, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "avatar"

    aput-object v1, v0, v6

    const-string v1, "in_my_circles"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->USERS_PROJECTION:[Ljava/lang/String;

    .line 384
    const-class v0, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Lcom/google/api/services/plusi/model/DataEmailJson;->getInstance()Lcom/google/api/services/plusi/model/DataEmailJson;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "emails"

    aput-object v2, v1, v4

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPhoneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPhoneJson;

    move-result-object v2

    aput-object v2, v1, v6

    const-string v2, "phones"

    aput-object v2, v1, v7

    invoke-static {}, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddressJson;->getInstance()Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddressJson;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "addresses"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->CONTACT_INFO_JSON:Lcom/google/android/apps/plus/json/EsJson;

    .line 424
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "profile_state"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "group_concat(link_circle_id, \'|\') AS packed_circle_ids"

    aput-object v1, v0, v6

    const-string v1, "blocked"

    aput-object v1, v0, v7

    const-string v1, "last_updated_time"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string v2, "contact_update_time"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "contact_proto"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "profile_update_time"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "profile_proto"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->PROFILE_COLUMNS:[Ljava/lang/String;

    .line 447
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sCircleSyncLock:Ljava/lang/Object;

    .line 448
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sPeopleSyncLock:Ljava/lang/Object;

    .line 449
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sMyProfileSyncLock:Ljava/lang/Object;

    .line 450
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sSuggestedPeopleSyncLock:Ljava/lang/Object;

    .line 451
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sSuggestedCelebritiesSyncLock:Ljava/lang/Object;

    .line 458
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sInitialSyncLatch:Ljava/util/concurrent/CountDownLatch;

    .line 2005
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "suggested_person_id"

    aput-object v1, v0, v5

    const-string v1, "dismissed"

    aput-object v1, v0, v4

    const-string v1, "sort_order"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->SUGGESTED_PEOPLE_COLUMNS:[Ljava/lang/String;

    .line 2990
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sProfileFetchLocks:Ljava/util/HashMap;

    return-void
.end method

.method public static activateAccount$1f9c1b47()V
    .registers 2

    .prologue
    .line 466
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sInitialSyncLatch:Ljava/util/concurrent/CountDownLatch;

    .line 467
    return-void
.end method

.method public static buildCircleId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleId;
    .registers 3
    .parameter "circleId"

    .prologue
    .line 3663
    new-instance v0, Lcom/google/api/services/plusi/model/DataCircleId;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataCircleId;-><init>()V

    .line 3664
    .local v0, id:Lcom/google/api/services/plusi/model/DataCircleId;
    const-string v1, "f."

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 3665
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataCircleId;->focusId:Ljava/lang/String;

    .line 3669
    :goto_14
    return-object v0

    .line 3667
    :cond_15
    iput-object p0, v0, Lcom/google/api/services/plusi/model/DataCircleId;->focusId:Ljava/lang/String;

    goto :goto_14
.end method

.method public static buildPersonFromPersonIdAndName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/content/PersonData;
    .registers 6
    .parameter "personId"
    .parameter "name"

    .prologue
    const/4 v3, 0x2

    .line 3678
    const/4 v1, 0x0

    .line 3679
    .local v1, gaiaId:Ljava/lang/String;
    const/4 v0, 0x0

    .line 3680
    .local v0, email:Ljava/lang/String;
    const-string v2, "g:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 3681
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 3687
    :cond_f
    :goto_f
    new-instance v2, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v2, v1, p1, v0}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 3682
    :cond_15
    const-string v2, "e:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 3683
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_f

    .line 3684
    :cond_22
    const-string v2, "p:"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 3685
    move-object v0, p0

    goto :goto_f
.end method

.method private static buildSearchKeysForEmailAddresses$154ba4cc(Lcom/google/api/services/plusi/model/DataCirclePerson;Ljava/util/ArrayList;)V
    .registers 11
    .parameter "person"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/plusi/model/DataCirclePerson;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, searchKeys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;>;"
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2637
    const/4 v3, 0x0

    .line 2638
    .local v3, emails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_77

    move v2, v6

    .line 2639
    .local v2, emailBasedId:Z
    :goto_e
    iget-object v8, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->email:Ljava/util/List;

    if-eqz v8, :cond_79

    iget-object v8, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->email:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_79

    move v4, v6

    .line 2641
    .local v4, hasEmailAddresses:Z
    :goto_1f
    if-nez v2, :cond_23

    if-eqz v4, :cond_7b

    .line 2642
    :cond_23
    new-instance v3, Ljava/util/ArrayList;

    .end local v3           #emails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2643
    .restart local v3       #emails:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_3f

    .line 2644
    iget-object v7, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->normalizeEmailAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2645
    .local v0, address:Ljava/lang/String;
    if-eqz v0, :cond_3f

    .line 2646
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2647
    new-instance v7, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;

    invoke-direct {v7, v6, v0}, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2652
    .end local v0           #address:Ljava/lang/String;
    :cond_3f
    if-eqz v4, :cond_7b

    .line 2653
    iget-object v7, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->email:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, i$:Ljava/util/Iterator;
    :cond_49
    :goto_49
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataEmail;

    .line 2654
    .local v1, email:Lcom/google/api/services/plusi/model/DataEmail;
    iget-object v7, v1, Lcom/google/api/services/plusi/model/DataEmail;->value:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_49

    .line 2655
    iget-object v7, v1, Lcom/google/api/services/plusi/model/DataEmail;->value:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->normalizeEmailAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2656
    .restart local v0       #address:Ljava/lang/String;
    if-eqz v0, :cond_49

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_49

    .line 2657
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2658
    new-instance v7, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;

    invoke-direct {v7, v6, v0}, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;-><init>(ILjava/lang/String;)V

    invoke-virtual {p1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_49

    .end local v0           #address:Ljava/lang/String;
    .end local v1           #email:Lcom/google/api/services/plusi/model/DataEmail;
    .end local v2           #emailBasedId:Z
    .end local v4           #hasEmailAddresses:Z
    .end local v5           #i$:Ljava/util/Iterator;
    :cond_77
    move v2, v7

    .line 2638
    goto :goto_e

    .restart local v2       #emailBasedId:Z
    :cond_79
    move v4, v7

    .line 2639
    goto :goto_1f

    .line 2665
    .restart local v4       #hasEmailAddresses:Z
    :cond_7b
    return-void
.end method

.method public static changeMuteState(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Z
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "isMuted"

    .prologue
    .line 2958
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "g:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2959
    .local v2, personId:Ljava/lang/String;
    const/4 v0, 0x0

    .line 2961
    .local v0, changed:Z
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2963
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2965
    :try_start_1b
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getProfileInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/api/services/plusi/model/SimpleProfile;

    move-result-object v3

    .line 2966
    .local v3, profile:Lcom/google/api/services/plusi/model/SimpleProfile;
    if-eqz v3, :cond_45

    iget-object v5, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    if-eqz v5, :cond_45

    iget-object v5, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->socialGraphData:Lcom/google/api/services/plusi/model/SocialGraphData;

    if-eqz v5, :cond_45

    .line 2969
    iget-object v5, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->socialGraphData:Lcom/google/api/services/plusi/model/SocialGraphData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SocialGraphData;->muted:Ljava/lang/Boolean;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v5

    if-eq v5, p3, :cond_45

    .line 2970
    iget-object v5, v3, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->socialGraphData:Lcom/google/api/services/plusi/model/SocialGraphData;

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v5, Lcom/google/api/services/plusi/model/SocialGraphData;->muted:Ljava/lang/Boolean;

    .line 2971
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceProfileInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)V

    .line 2972
    const/4 v0, 0x1

    .line 2975
    :cond_45
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_48
    .catchall {:try_start_1b .. :try_end_48} :catchall_5c

    .line 2977
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2980
    if-eqz v0, :cond_5b

    .line 2981
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 2982
    .local v4, resolver:Landroid/content/ContentResolver;
    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

    invoke-static {v5, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2986
    .end local v4           #resolver:Landroid/content/ContentResolver;
    :cond_5b
    return v0

    .line 2977
    .end local v3           #profile:Lcom/google/api/services/plusi/model/SimpleProfile;
    :catchall_5c
    move-exception v5

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5
.end method

.method public static changePlusOneData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Z
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "plusOnedBySelf"

    .prologue
    .line 2917
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "g:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2918
    .local v2, personId:Ljava/lang/String;
    const/4 v0, 0x0

    .line 2920
    .local v0, changed:Z
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2922
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2924
    :try_start_1b
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getProfileInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/api/services/plusi/model/SimpleProfile;

    move-result-object v4

    .line 2925
    .local v4, profile:Lcom/google/api/services/plusi/model/SimpleProfile;
    if-eqz v4, :cond_51

    iget-object v6, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    if-eqz v6, :cond_51

    iget-object v6, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/Page;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v6, :cond_51

    .line 2926
    iget-object v6, v4, Lcom/google/api/services/plusi/model/SimpleProfile;->page:Lcom/google/api/services/plusi/model/Page;

    iget-object v3, v6, Lcom/google/api/services/plusi/model/Page;->plusone:Lcom/google/api/services/plusi/model/DataPlusOne;

    .line 2927
    .local v3, plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;
    iget-object v6, v3, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eq v6, p3, :cond_51

    .line 2928
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    iput-object v6, v3, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    .line 2929
    iget-object v6, v3, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-eqz p3, :cond_68

    const/4 v6, 0x1

    :goto_46
    add-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v3, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    .line 2930
    invoke-static {v1, v2, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceProfileInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)V

    .line 2931
    const/4 v0, 0x1

    .line 2934
    .end local v3           #plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;
    :cond_51
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_54
    .catchall {:try_start_1b .. :try_end_54} :catchall_6a

    .line 2936
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2939
    if-eqz v0, :cond_67

    .line 2940
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 2941
    .local v5, resolver:Landroid/content/ContentResolver;
    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

    invoke-static {v6, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2945
    .end local v5           #resolver:Landroid/content/ContentResolver;
    :cond_67
    return v0

    .line 2929
    .restart local v3       #plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;
    :cond_68
    const/4 v6, -0x1

    goto :goto_46

    .line 2936
    .end local v3           #plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;
    .end local v4           #profile:Lcom/google/api/services/plusi/model/SimpleProfile;
    :catchall_6a
    move-exception v6

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v6
.end method

.method static cleanupData(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 6
    .parameter "db"
    .parameter "account"

    .prologue
    const/16 v3, 0x29

    .line 3580
    const-string v0, "contacts"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "in_my_circles=0  AND blocked=0 AND gaia_id!="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND gaia_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN (SELECT DISTINCT author_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM activities"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND gaia_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN (SELECT DISTINCT author_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM activity_comments"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND person_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN (SELECT DISTINCT suggested_person_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM suggested_people"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND gaia_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN (SELECT DISTINCT author_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM photo_comment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND gaia_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN (SELECT DISTINCT creator_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM photo_shape"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND gaia_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN (SELECT DISTINCT subject_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM photo_shape"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND gaia_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN (SELECT DISTINCT gaia_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM circle_action"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND gaia_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN (SELECT DISTINCT gaia_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM event_people"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3608
    return-void
.end method

.method private static collectSearchKeysForName(Ljava/util/ArrayList;Ljava/lang/String;)V
    .registers 10
    .parameter
    .parameter "name"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, searchKeys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;>;"
    const/4 v7, 0x0

    .line 2562
    if-eqz p0, :cond_9

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2584
    :cond_9
    :goto_9
    return-void

    .line 2566
    :cond_a
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 2567
    .local v4, string:Ljava/lang/String;
    const/4 v3, 0x0

    .line 2568
    .local v3, offset:I
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    .line 2569
    .local v2, length:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_14
    if-ge v1, v2, :cond_33

    .line 2570
    invoke-virtual {v4, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 2571
    .local v0, c:C
    invoke-static {v0}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v5

    if-nez v5, :cond_30

    .line 2572
    if-le v1, v3, :cond_2e

    .line 2573
    new-instance v5, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;

    invoke-virtual {v4, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2576
    :cond_2e
    add-int/lit8 v3, v1, 0x1

    .line 2569
    :cond_30
    add-int/lit8 v1, v1, 0x1

    goto :goto_14

    .line 2580
    .end local v0           #c:C
    :cond_33
    if-le v2, v3, :cond_9

    .line 2581
    new-instance v5, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;

    invoke-virtual {v4, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v7, v6}, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;-><init>(ILjava/lang/String;)V

    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9
.end method

.method public static convertAudienceToSharingRoster(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/api/services/plusi/model/SharingRoster;
    .registers 15
    .parameter "audience"

    .prologue
    .line 3694
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 3696
    .local v8, sharingTargetList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/SharingTargetId;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_b
    if-ge v3, v4, :cond_56

    aget-object v1, v0, v3

    .line 3697
    .local v1, circle:Lcom/google/android/apps/plus/content/CircleData;
    new-instance v6, Lcom/google/api/services/plusi/model/SharingTargetId;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/SharingTargetId;-><init>()V

    .line 3699
    .local v6, shareTarget:Lcom/google/api/services/plusi/model/SharingTargetId;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v9

    .line 3700
    .local v9, type:I
    const/4 v11, 0x5

    if-ne v9, v11, :cond_25

    .line 3701
    const-string v11, "YOUR_CIRCLES"

    iput-object v11, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    .line 3716
    :goto_1f
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3696
    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    .line 3702
    :cond_25
    const/4 v11, 0x7

    if-ne v9, v11, :cond_2d

    .line 3703
    const-string v11, "EXTENDED_CIRCLES"

    iput-object v11, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    goto :goto_1f

    .line 3704
    :cond_2d
    const/16 v11, 0x8

    if-ne v9, v11, :cond_36

    .line 3705
    const-string v11, "DASHER_DOMAIN"

    iput-object v11, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    goto :goto_1f

    .line 3706
    :cond_36
    const/16 v11, 0x9

    if-ne v9, v11, :cond_3f

    .line 3707
    const-string v11, "PUBLIC"

    iput-object v11, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    goto :goto_1f

    .line 3709
    :cond_3f
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v2

    .line 3710
    .local v2, circleId:Ljava/lang/String;
    const-string v11, "f."

    invoke-virtual {v2, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_53

    .line 3711
    const/4 v11, 0x2

    invoke-virtual {v2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->circleId:Ljava/lang/String;

    goto :goto_1f

    .line 3713
    :cond_53
    iput-object v2, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->circleId:Ljava/lang/String;

    goto :goto_1f

    .line 3719
    .end local v1           #circle:Lcom/google/android/apps/plus/content/CircleData;
    .end local v2           #circleId:Ljava/lang/String;
    .end local v6           #shareTarget:Lcom/google/api/services/plusi/model/SharingTargetId;
    .end local v9           #type:I
    :cond_56
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v4, v0

    const/4 v3, 0x0

    :goto_5c
    if-ge v3, v4, :cond_b5

    aget-object v10, v0, v3

    .line 3720
    .local v10, user:Lcom/google/android/apps/plus/content/PersonData;
    new-instance v6, Lcom/google/api/services/plusi/model/SharingTargetId;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/SharingTargetId;-><init>()V

    .line 3721
    .restart local v6       #shareTarget:Lcom/google/api/services/plusi/model/SharingTargetId;
    new-instance v5, Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/DataCircleMemberId;-><init>()V

    .line 3723
    .local v5, personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;
    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_82

    .line 3724
    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v5, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    .line 3735
    :goto_7a
    iput-object v5, v6, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    .line 3736
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3719
    :cond_7f
    :goto_7f
    add-int/lit8 v3, v3, 0x1

    goto :goto_5c

    .line 3725
    :cond_82
    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_93

    .line 3726
    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v5, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    goto :goto_7a

    .line 3729
    :cond_93
    const-string v11, "EsPeopleData"

    const/4 v12, 0x6

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_7f

    .line 3730
    const-string v11, "EsPeopleData"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Invalid user: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7f

    .line 3739
    .end local v5           #personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;
    .end local v6           #shareTarget:Lcom/google/api/services/plusi/model/SharingTargetId;
    .end local v10           #user:Lcom/google/android/apps/plus/content/PersonData;
    :cond_b5
    new-instance v7, Lcom/google/api/services/plusi/model/SharingRoster;

    invoke-direct {v7}, Lcom/google/api/services/plusi/model/SharingRoster;-><init>()V

    .line 3740
    .local v7, sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;
    iput-object v8, v7, Lcom/google/api/services/plusi/model/SharingRoster;->sharingTargetId:Ljava/util/List;

    .line 3742
    return-object v7
.end method

.method private static convertCirclePersonToContactInfo(Lcom/google/api/services/plusi/model/DataCirclePerson;)Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;
    .registers 11
    .parameter "person"

    .prologue
    .line 3844
    new-instance v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;

    invoke-direct {v6}, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;-><init>()V

    .line 3846
    .local v6, info:Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    if-nez v9, :cond_a

    .line 3912
    :cond_9
    return-object v6

    .line 3850
    :cond_a
    iget-object v5, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    .line 3851
    .local v5, id:Lcom/google/api/services/plusi/model/DataCircleMemberId;
    iget-object v9, v5, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_29

    .line 3852
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->emails:Ljava/util/List;

    .line 3854
    new-instance v3, Lcom/google/api/services/plusi/model/DataEmail;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/DataEmail;-><init>()V

    .line 3855
    .local v3, emailData:Lcom/google/api/services/plusi/model/DataEmail;
    iget-object v9, v5, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    iput-object v9, v3, Lcom/google/api/services/plusi/model/DataEmail;->value:Ljava/lang/String;

    .line 3857
    iget-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->emails:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3860
    .end local v3           #emailData:Lcom/google/api/services/plusi/model/DataEmail;
    :cond_29
    iget-object v9, v5, Lcom/google/api/services/plusi/model/DataCircleMemberId;->phone:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_46

    .line 3861
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->phones:Ljava/util/List;

    .line 3863
    new-instance v8, Lcom/google/api/services/plusi/model/DataPhone;

    invoke-direct {v8}, Lcom/google/api/services/plusi/model/DataPhone;-><init>()V

    .line 3864
    .local v8, phoneData:Lcom/google/api/services/plusi/model/DataPhone;
    iget-object v9, v5, Lcom/google/api/services/plusi/model/DataCircleMemberId;->phone:Ljava/lang/String;

    iput-object v9, v8, Lcom/google/api/services/plusi/model/DataPhone;->value:Ljava/lang/String;

    .line 3866
    iget-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->phones:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3869
    .end local v8           #phoneData:Lcom/google/api/services/plusi/model/DataPhone;
    :cond_46
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->email:Ljava/util/List;

    if-eqz v9, :cond_82

    .line 3870
    iget-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->emails:Ljava/util/List;

    if-nez v9, :cond_57

    .line 3871
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->emails:Ljava/util/List;

    .line 3873
    :cond_57
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->email:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_5f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_82

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataEmail;

    .line 3874
    .local v2, email:Lcom/google/api/services/plusi/model/DataEmail;
    new-instance v3, Lcom/google/api/services/plusi/model/DataEmail;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/DataEmail;-><init>()V

    .line 3875
    .restart local v3       #emailData:Lcom/google/api/services/plusi/model/DataEmail;
    iget-object v9, v2, Lcom/google/api/services/plusi/model/DataEmail;->standardTag:Ljava/lang/Integer;

    iput-object v9, v3, Lcom/google/api/services/plusi/model/DataEmail;->standardTag:Ljava/lang/Integer;

    .line 3876
    iget-object v9, v2, Lcom/google/api/services/plusi/model/DataEmail;->customTag:Ljava/lang/String;

    iput-object v9, v3, Lcom/google/api/services/plusi/model/DataEmail;->customTag:Ljava/lang/String;

    .line 3877
    iget-object v9, v2, Lcom/google/api/services/plusi/model/DataEmail;->value:Ljava/lang/String;

    iput-object v9, v3, Lcom/google/api/services/plusi/model/DataEmail;->value:Ljava/lang/String;

    .line 3879
    iget-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->emails:Ljava/util/List;

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5f

    .line 3883
    .end local v2           #email:Lcom/google/api/services/plusi/model/DataEmail;
    .end local v3           #emailData:Lcom/google/api/services/plusi/model/DataEmail;
    .end local v4           #i$:Ljava/util/Iterator;
    :cond_82
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->phone:Ljava/util/List;

    if-eqz v9, :cond_be

    .line 3884
    iget-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->phones:Ljava/util/List;

    if-nez v9, :cond_93

    .line 3885
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->phones:Ljava/util/List;

    .line 3887
    :cond_93
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->phone:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4       #i$:Ljava/util/Iterator;
    :goto_9b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_be

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/services/plusi/model/DataPhone;

    .line 3888
    .local v7, phone:Lcom/google/api/services/plusi/model/DataPhone;
    new-instance v8, Lcom/google/api/services/plusi/model/DataPhone;

    invoke-direct {v8}, Lcom/google/api/services/plusi/model/DataPhone;-><init>()V

    .line 3889
    .restart local v8       #phoneData:Lcom/google/api/services/plusi/model/DataPhone;
    iget-object v9, v7, Lcom/google/api/services/plusi/model/DataPhone;->standardTag:Ljava/lang/Integer;

    iput-object v9, v8, Lcom/google/api/services/plusi/model/DataPhone;->standardTag:Ljava/lang/Integer;

    .line 3890
    iget-object v9, v7, Lcom/google/api/services/plusi/model/DataPhone;->customTag:Ljava/lang/String;

    iput-object v9, v8, Lcom/google/api/services/plusi/model/DataPhone;->customTag:Ljava/lang/String;

    .line 3891
    iget-object v9, v7, Lcom/google/api/services/plusi/model/DataPhone;->value:Ljava/lang/String;

    iput-object v9, v8, Lcom/google/api/services/plusi/model/DataPhone;->value:Ljava/lang/String;

    .line 3893
    iget-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->phones:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9b

    .line 3897
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v7           #phone:Lcom/google/api/services/plusi/model/DataPhone;
    .end local v8           #phoneData:Lcom/google/api/services/plusi/model/DataPhone;
    :cond_be
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->address:Ljava/util/List;

    if-eqz v9, :cond_9

    .line 3898
    iget-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->addresses:Ljava/util/List;

    if-nez v9, :cond_cf

    .line 3899
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->addresses:Ljava/util/List;

    .line 3901
    :cond_cf
    iget-object v9, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->address:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4       #i$:Ljava/util/Iterator;
    :goto_d7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;

    .line 3902
    .local v0, address:Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;
    new-instance v1, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;-><init>()V

    .line 3904
    .local v1, addressData:Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;
    iget-object v9, v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->standardTag:Ljava/lang/Integer;

    iput-object v9, v1, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->standardTag:Ljava/lang/Integer;

    .line 3905
    iget-object v9, v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->customTag:Ljava/lang/String;

    iput-object v9, v1, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->customTag:Ljava/lang/String;

    .line 3906
    iget-object v9, v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->value:Ljava/lang/String;

    iput-object v9, v1, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->value:Ljava/lang/String;

    .line 3908
    iget-object v9, v6, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->addresses:Ljava/util/List;

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d7
.end method

.method public static convertSharingRosterToAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/RenderedSharingRosters;)Lcom/google/android/apps/plus/content/AudienceData;
    .registers 23
    .parameter "context"
    .parameter "account"
    .parameter "sharingRoster"

    .prologue
    .line 3756
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 3757
    .local v16, personList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/PersonData;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 3759
    .local v8, circleList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/CircleData;>;"
    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "circle_id"

    aput-object v2, v4, v1

    const/4 v1, 0x1

    const-string v2, "type"

    aput-object v2, v4, v1

    const/4 v1, 0x2

    const-string v2, "contact_count"

    aput-object v2, v4, v1

    .line 3761
    .local v4, projection:[Ljava/lang/String;
    const/4 v3, 0x5

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v10

    .line 3762
    .local v10, cursor:Landroid/database/Cursor;
    if-nez v10, :cond_3b

    .line 3763
    const-string v1, "EsPeopleData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_39

    .line 3764
    const-string v1, "EsPeopleData"

    const-string v2, "Error converting sharing roster to audience"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3766
    :cond_39
    const/4 v1, 0x0

    .line 3839
    :goto_3a
    return-object v1

    .line 3769
    :cond_3b
    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    .line 3770
    .local v7, circleIdMap:Landroid/util/SparseArray;,"Landroid/util/SparseArray<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 3771
    .local v9, circleSizeMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v1, -0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 3772
    :goto_49
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_70

    .line 3773
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 3774
    .local v12, id:Ljava/lang/String;
    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 3775
    .local v19, type:I
    const/4 v1, 0x2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 3777
    .local v13, memberCount:I
    const/4 v1, 0x1

    move/from16 v0, v19

    if-eq v0, v1, :cond_68

    .line 3778
    move/from16 v0, v19

    invoke-virtual {v7, v0, v12}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 3780
    :cond_68
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v12, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_49

    .line 3782
    .end local v12           #id:Ljava/lang/String;
    .end local v13           #memberCount:I
    .end local v19           #type:I
    :cond_70
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 3784
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/RenderedSharingRosters;->targets:Ljava/util/List;

    if-eqz v1, :cond_168

    .line 3785
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/api/services/plusi/model/RenderedSharingRosters;->targets:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i$:Ljava/util/Iterator;
    :cond_81
    :goto_81
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_168

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/google/api/services/plusi/model/SharingTarget;

    .line 3786
    .local v18, target:Lcom/google/api/services/plusi/model/SharingTarget;
    move-object/from16 v0, v18

    iget-object v14, v0, Lcom/google/api/services/plusi/model/SharingTarget;->displayName:Ljava/lang/String;

    .line 3787
    .local v14, name:Ljava/lang/String;
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SharingTarget;->id:Lcom/google/api/services/plusi/model/SharingTargetId;

    move-object/from16 v17, v0

    .line 3789
    .local v17, sharingTargetId:Lcom/google/api/services/plusi/model/SharingTargetId;
    if-nez v17, :cond_aa

    .line 3790
    const-string v1, "EsPeopleData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_81

    .line 3791
    const-string v1, "EsPeopleData"

    const-string v2, "null SharingTargetId"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_81

    .line 3796
    :cond_aa
    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    if-nez v1, :cond_b6

    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/google/api/services/plusi/model/SharingTargetId;->circleId:Ljava/lang/String;

    if-eqz v1, :cond_115

    .line 3800
    :cond_b6
    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    if-eqz v1, :cond_ee

    .line 3801
    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/google/api/services/plusi/model/SharingTargetId;->groupType:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleType(Ljava/lang/String;)I

    move-result v19

    .line 3802
    .restart local v19       #type:I
    move/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 3803
    .restart local v12       #id:Ljava/lang/String;
    if-nez v12, :cond_f8

    .line 3805
    const-string v1, "EsPeopleData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_81

    .line 3806
    const-string v1, "EsPeopleData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Circle ID not found for type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_81

    .line 3811
    .end local v12           #id:Ljava/lang/String;
    .end local v19           #type:I
    :cond_ee
    const/16 v19, 0x1

    .line 3812
    .restart local v19       #type:I
    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/google/api/services/plusi/model/SharingTargetId;->circleId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 3814
    .restart local v12       #id:Ljava/lang/String;
    :cond_f8
    invoke-virtual {v9, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    .line 3815
    .local v13, memberCount:Ljava/lang/Integer;
    if-nez v13, :cond_105

    .line 3816
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 3818
    :cond_105
    new-instance v1, Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move/from16 v0, v19

    invoke-direct {v1, v12, v0, v14, v2}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_81

    .line 3819
    .end local v12           #id:Ljava/lang/String;
    .end local v13           #memberCount:Ljava/lang/Integer;
    .end local v19           #type:I
    :cond_115
    move-object/from16 v0, v17

    iget-object v1, v0, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    if-eqz v1, :cond_156

    .line 3821
    move-object/from16 v0, v17

    iget-object v15, v0, Lcom/google/api/services/plusi/model/SharingTargetId;->personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    .line 3822
    .local v15, personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;
    iget-object v1, v15, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    if-nez v1, :cond_127

    iget-object v1, v15, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    if-eqz v1, :cond_137

    .line 3823
    :cond_127
    new-instance v1, Lcom/google/android/apps/plus/content/PersonData;

    iget-object v2, v15, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    invoke-direct {v1, v2, v14, v3}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_81

    .line 3827
    :cond_137
    const-string v1, "EsPeopleData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_81

    .line 3828
    const-string v1, "EsPeopleData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid user from roster: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_81

    .line 3832
    .end local v15           #personId:Lcom/google/api/services/plusi/model/DataCircleMemberId;
    :cond_156
    const-string v1, "EsPeopleData"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_81

    .line 3833
    const-string v1, "EsPeopleData"

    const-string v2, "Invalid SharingTargetId"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_81

    .line 3839
    .end local v11           #i$:Ljava/util/Iterator;
    .end local v14           #name:Ljava/lang/String;
    .end local v17           #sharingTargetId:Lcom/google/api/services/plusi/model/SharingTargetId;
    .end local v18           #target:Lcom/google/api/services/plusi/model/SharingTarget;
    :cond_168
    new-instance v1, Lcom/google/android/apps/plus/content/AudienceData;

    move-object/from16 v0, v16

    invoke-direct {v1, v0, v8}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    goto/16 :goto_3a
.end method

.method public static deleteFromSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2219
    .local p2, personIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2222
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2224
    :try_start_b
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2225
    .local v2, personId:Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "dismissed"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "suggested_people"

    const-string v5, "suggested_person_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {v0, v4, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_37
    .catchall {:try_start_b .. :try_end_37} :catchall_38

    goto :goto_f

    .line 2229
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #personId:Ljava/lang/String;
    :catchall_38
    move-exception v3

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 2227
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_3d
    :try_start_3d
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_40
    .catchall {:try_start_3d .. :try_end_40} :catchall_38

    .line 2229
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2232
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2233
    return-void
.end method

.method public static deserializeContactInfo([B)Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;
    .registers 2
    .parameter "contactBytes"

    .prologue
    .line 2877
    if-nez p0, :cond_4

    .line 2878
    const/4 v0, 0x0

    .line 2881
    :goto_3
    return-object v0

    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->CONTACT_INFO_JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/json/EsJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;

    goto :goto_3
.end method

.method private static deserializeProfile([B)Lcom/google/api/services/plusi/model/SimpleProfile;
    .registers 2
    .parameter "profileBytes"

    .prologue
    .line 2855
    if-nez p0, :cond_4

    .line 2856
    const/4 v0, 0x0

    .line 2859
    :goto_3
    return-object v0

    :cond_4
    invoke-static {}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->getInstance()Lcom/google/api/services/plusi/model/SimpleProfileJson;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/SimpleProfile;

    goto :goto_3
.end method

.method private static doCirclesSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)Z
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1237
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1258
    :goto_9
    return v4

    .line 1241
    :cond_a
    const-string v1, "Circles"

    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 1243
    new-instance v0, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;

    move-object v1, p0

    move-object v2, p1

    move v5, v4

    move-object v7, v6

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZZILjava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 1251
    .local v0, gco:Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;
    new-instance v1, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    invoke-virtual {v0, p2, v1}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    .line 1252
    const-string v1, "EsPeopleData"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    .line 1254
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->getCircleList()Lcom/google/api/services/plusi/model/DataViewerCircles;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->getSystemGroups()Lcom/google/api/services/plusi/model/DataSystemGroups;

    move-result-object v2

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->insertCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataViewerCircles;Lcom/google/api/services/plusi/model/DataSystemGroups;)I

    move-result v9

    .line 1256
    .local v9, count:I
    invoke-virtual {p2, v9}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    move v4, v3

    .line 1258
    goto :goto_9
.end method

.method public static ensurePeopleSynced(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .registers 10
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v3, 0x1

    .line 739
    sget-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sInitialSyncLatch:Ljava/util/concurrent/CountDownLatch;

    .line 740
    .local v0, latch:Ljava/util/concurrent/CountDownLatch;
    if-nez v0, :cond_6

    .line 764
    :cond_5
    :goto_5
    return v3

    .line 744
    :cond_6
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->queryPeopleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v1

    .line 745
    .local v1, syncTimestamp:J
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_5

    .line 751
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->postSyncPeopleRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 754
    const-wide/32 v4, 0x15f90

    :try_start_16
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v6}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1b
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_1b} :catch_27

    .line 759
    :goto_1b
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_5

    .line 761
    const/4 v3, 0x0

    goto :goto_5

    :catch_27
    move-exception v4

    goto :goto_1b
.end method

.method private static ensureSuggestedCelebritiesSynced(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)Z
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "timeToLive"
    .parameter "refreshIfStale"

    .prologue
    .line 833
    sget-object v6, Lcom/google/android/apps/plus/content/EsPeopleData;->sSuggestedCelebritiesSyncLock:Ljava/lang/Object;

    monitor-enter v6

    .line 834
    :try_start_3
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->querySuggestedCelebritiesSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v2

    .line 835
    .local v2, timestamp:J
    const-wide/16 v7, -0x1

    cmp-long v5, v2, v7

    if-eqz v5, :cond_18

    if-eqz p4, :cond_62

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v2

    cmp-long v5, v7, p2

    if-lez v5, :cond_62

    .line 837
    :cond_18
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 840
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 841
    .local v4, values:Landroid/content/ContentValues;
    const-string v5, "suggested_celeb_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 843
    const-string v5, "account_status"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v1, v5, v4, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 845
    new-instance v0, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-direct {v0, p0, p1, v5, v7}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 847
    .local v0, csop:Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->start()V

    .line 848
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->hasError()Z

    move-result v5

    if-eqz v5, :cond_62

    .line 849
    const-string v5, "EsPeopleData"

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;->logError(Ljava/lang/String;)V

    .line 851
    const-string v5, "suggested_celeb_sync_time"

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 852
    const-string v5, "account_status"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v1, v5, v4, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 853
    const/4 v5, 0x0

    monitor-exit v6
    :try_end_61
    .catchall {:try_start_3 .. :try_end_61} :catchall_65

    .line 856
    .end local v0           #csop:Lcom/google/android/apps/plus/api/GetCelebritySuggestionsOperation;
    .end local v1           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v4           #values:Landroid/content/ContentValues;
    :goto_61
    return v5

    :cond_62
    const/4 v5, 0x1

    monitor-exit v6

    goto :goto_61

    .line 857
    .end local v2           #timestamp:J
    :catchall_65
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method private static ensureSuggestedPeopleSynced(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)Z
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "timeToLive"
    .parameter "refreshIfStale"

    .prologue
    .line 804
    sget-object v6, Lcom/google/android/apps/plus/content/EsPeopleData;->sSuggestedPeopleSyncLock:Ljava/lang/Object;

    monitor-enter v6

    .line 805
    :try_start_3
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->querySuggestedPeopleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v2

    .line 806
    .local v2, timestamp:J
    const-wide/16 v7, -0x1

    cmp-long v5, v2, v7

    if-eqz v5, :cond_18

    if-eqz p4, :cond_62

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v2

    cmp-long v5, v7, p2

    if-lez v5, :cond_62

    .line 808
    :cond_18
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 811
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 812
    .local v4, values:Landroid/content/ContentValues;
    const-string v5, "suggested_people_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 814
    const-string v5, "account_status"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0, v5, v4, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 816
    new-instance v1, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-direct {v1, p0, p1, v5, v7}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 818
    .local v1, op:Lcom/google/android/apps/plus/api/FindMorePeopleOperation;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;->start()V

    .line 819
    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;->hasError()Z

    move-result v5

    if-eqz v5, :cond_62

    .line 820
    const-string v5, "EsPeopleData"

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/api/FindMorePeopleOperation;->logError(Ljava/lang/String;)V

    .line 822
    const-string v5, "suggested_people_sync_time"

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 823
    const-string v5, "account_status"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v0, v5, v4, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 824
    const/4 v5, 0x0

    monitor-exit v6
    :try_end_61
    .catchall {:try_start_3 .. :try_end_61} :catchall_65

    .line 827
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v1           #op:Lcom/google/android/apps/plus/api/FindMorePeopleOperation;
    .end local v4           #values:Landroid/content/ContentValues;
    :goto_61
    return v5

    :cond_62
    const/4 v5, 0x1

    monitor-exit v6

    goto :goto_61

    .line 828
    .end local v2           #timestamp:J
    :catchall_65
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method public static extractGaiaId(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "personId"

    .prologue
    .line 4009
    if-eqz p0, :cond_10

    const-string v0, "g:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 4010
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 4012
    :goto_f
    return-object v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public static getBlockedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/util/ArrayList;)Landroid/database/Cursor;
    .registers 22
    .parameter "context"
    .parameter "account"
    .parameter "projection"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 912
    .local p3, includedPersonIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    .line 915
    .local v9, db:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    .line 916
    .local v3, uri:Landroid/net/Uri;
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    .line 917
    .local v6, args:[Ljava/lang/String;
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 918
    .local v12, sb:Ljava/lang/StringBuilder;
    const-string v2, "blocked=1"

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 919
    array-length v2, v6

    if-lez v2, :cond_4a

    .line 920
    const-string v2, " OR person_id IN ("

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 921
    const/4 v10, 0x0

    .local v10, i:I
    :goto_31
    array-length v2, v6

    if-ge v10, v2, :cond_3c

    .line 922
    const-string v2, "?,"

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 921
    add-int/lit8 v10, v10, 0x1

    goto :goto_31

    .line 924
    :cond_3c
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 925
    const-string v2, ")"

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 928
    .end local v10           #i:I
    :cond_4a
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->queryBlockedPeopleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v13

    .line 929
    .local v13, timestamp:J
    const-wide/16 v4, -0x1

    cmp-long v2, v13, v4

    if-nez v2, :cond_a9

    .line 930
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 931
    .local v15, values:Landroid/content/ContentValues;
    const-string v2, "blocked_people_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v15, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 933
    const-string v2, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v9, v2, v15, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 935
    new-instance v11, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v11, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 937
    .local v11, op:Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;
    invoke-virtual {v11}, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;->start()V

    .line 938
    invoke-virtual {v11}, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_99

    .line 939
    const-string v2, "EsPeopleData"

    invoke-virtual {v11, v2}, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;->logError(Ljava/lang/String;)V

    .line 941
    const-string v2, "blocked_people_sync_time"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v15, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 942
    const-string v2, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v9, v2, v15, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 943
    const/4 v8, 0x0

    .line 957
    .end local v11           #op:Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;
    .end local v15           #values:Landroid/content/ContentValues;
    :cond_98
    :goto_98
    return-object v8

    .line 946
    .restart local v11       #op:Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;
    .restart local v15       #values:Landroid/content/ContentValues;
    :cond_99
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    goto :goto_98

    .line 948
    .end local v11           #op:Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;
    .end local v15           #values:Landroid/content/ContentValues;
    :cond_a9
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    move-object/from16 v4, p2

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 950
    .local v8, cursor:Landroid/database/Cursor;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v13

    const-wide/16 v16, 0x2710

    cmp-long v2, v4, v16

    if-lez v2, :cond_98

    .line 951
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 952
    .restart local v15       #values:Landroid/content/ContentValues;
    const-string v2, "blocked_people_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v15, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 954
    const-string v2, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v9, v2, v15, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 955
    new-instance v2, Lcom/google/android/apps/plus/content/EsPeopleData$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/plus/content/EsPeopleData$2;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->postOnServiceThread(Ljava/lang/Runnable;)V

    goto :goto_98
.end method

.method private static getCircleCount(Lcom/google/api/services/plusi/model/DataCirclePerson;)I
    .registers 6
    .parameter "person"

    .prologue
    .line 4039
    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    if-nez v4, :cond_6

    .line 4040
    const/4 v0, 0x0

    .line 4051
    :cond_5
    return v0

    .line 4043
    :cond_6
    const/4 v0, 0x0

    .line 4044
    .local v0, count:I
    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 4045
    .local v3, size:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_e
    if-ge v1, v3, :cond_5

    .line 4046
    iget-object v4, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataMembership;

    .line 4047
    .local v2, membership:Lcom/google/api/services/plusi/model/DataMembership;
    iget-object v4, v2, Lcom/google/api/services/plusi/model/DataMembership;->deleted:Ljava/lang/Boolean;

    if-eqz v4, :cond_24

    iget-object v4, v2, Lcom/google/api/services/plusi/model/DataMembership;->deleted:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_26

    .line 4048
    :cond_24
    add-int/lit8 v0, v0, 0x1

    .line 4045
    :cond_26
    add-int/lit8 v1, v1, 0x1

    goto :goto_e
.end method

.method private static getCircleId(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "circleId"

    .prologue
    .line 3655
    const-string v0, "f."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3658
    .end local p0
    :goto_8
    return-object p0

    .restart local p0
    :cond_9
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "f."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_8
.end method

.method public static getCircleMemberId(Ljava/lang/String;)Lcom/google/api/services/plusi/model/DataCircleMemberId;
    .registers 4
    .parameter "personId"

    .prologue
    const/4 v2, 0x2

    .line 3992
    new-instance v0, Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataCircleMemberId;-><init>()V

    .line 3994
    .local v0, memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;
    const-string v1, "g:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 3995
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    .line 4001
    :cond_14
    :goto_14
    return-object v0

    .line 3996
    :cond_15
    const-string v1, "e:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    .line 3997
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    goto :goto_14

    .line 3998
    :cond_24
    const-string v1, "p:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 3999
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->phone:Ljava/lang/String;

    goto :goto_14
.end method

.method private static getCircleType(Ljava/lang/String;)I
    .registers 2
    .parameter "type"

    .prologue
    .line 4072
    const-string v0, "PUBLIC"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 4073
    const/16 v0, 0x9

    .line 4087
    :goto_a
    return v0

    .line 4074
    :cond_b
    const-string v0, "DASHER_DOMAIN"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 4075
    const/16 v0, 0x8

    goto :goto_a

    .line 4076
    :cond_16
    const-string v0, "YOUR_CIRCLES"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 4077
    const/4 v0, 0x5

    goto :goto_a

    .line 4078
    :cond_20
    const-string v0, "EXTENDED_CIRCLES"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 4079
    const/4 v0, 0x7

    goto :goto_a

    .line 4080
    :cond_2a
    const-string v0, "BLOCKED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 4081
    const/16 v0, 0xa

    goto :goto_a

    .line 4082
    :cond_35
    const-string v0, "IGNORED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 4083
    const/16 v0, 0x64

    goto :goto_a

    .line 4084
    :cond_40
    const-string v0, "ALL_CIRCLE_MEMBERS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 4085
    const/16 v0, 0xb

    goto :goto_a

    .line 4087
    :cond_4b
    const/4 v0, 0x0

    goto :goto_a
.end method

.method public static getCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .registers 17
    .parameter "context"
    .parameter "account"
    .parameter "usageType"
    .parameter "projection"
    .parameter "query"
    .parameter "maxResults"

    .prologue
    .line 494
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->queryCircleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v9

    .line 495
    .local v9, syncTimestamp:J
    const-wide/16 v0, -0x1

    cmp-long v0, v9, v0

    if-nez v0, :cond_e

    .line 498
    const/4 v0, 0x1

    :try_start_b
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_e} :catch_68

    .line 507
    :cond_e
    packed-switch p2, :pswitch_data_e8

    .line 583
    :pswitch_11
    const/4 v3, 0x0

    .line 587
    .local v3, selection:Ljava/lang/String;
    :goto_12
    if-eqz p4, :cond_43

    .line 588
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "(circle_name LIKE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x25

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 590
    .local v8, querySql:Ljava/lang/String;
    if-nez v3, :cond_ce

    .line 591
    move-object v3, v8

    .line 598
    .end local v8           #querySql:Ljava/lang/String;
    :cond_43
    :goto_43
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 599
    .local v6, builder:Landroid/net/Uri$Builder;
    invoke-static {v6, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    .line 600
    if-eqz p5, :cond_57

    .line 601
    const-string v0, "limit"

    invoke-static/range {p5 .. p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 605
    :cond_57
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    const/4 v4, 0x0

    const-string v5, "show_order ASC, sort_key"

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .end local v3           #selection:Ljava/lang/String;
    .end local v6           #builder:Landroid/net/Uri$Builder;
    :goto_67
    return-object v0

    .line 499
    :catch_68
    move-exception v7

    .line 500
    .local v7, e:Ljava/io/IOException;
    const-string v0, "EsPeopleData"

    const-string v1, "Error syncing circles"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 501
    const/4 v0, 0x0

    goto :goto_67

    .line 509
    .end local v7           #e:Ljava/io/IOException;
    :pswitch_72
    const-string v3, "0"

    .line 510
    .restart local v3       #selection:Ljava/lang/String;
    goto :goto_12

    .line 513
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_75
    const-string v3, "semantic_hints&1=0 AND (type!=10 OR contact_count>0)"

    .line 517
    .restart local v3       #selection:Ljava/lang/String;
    goto :goto_12

    .line 520
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_78
    const-string v3, "semantic_hints&2=0 AND type!=10 AND type!=100"

    .line 524
    .restart local v3       #selection:Ljava/lang/String;
    goto :goto_12

    .line 527
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_7b
    const-string v3, "type=1 OR circle_id=\'v.whatshot\'"

    .line 530
    .restart local v3       #selection:Ljava/lang/String;
    goto :goto_12

    .line 533
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_7e
    const-string v3, "type IN (1,-1)"

    .line 535
    .restart local v3       #selection:Ljava/lang/String;
    goto :goto_12

    .line 538
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_81
    const-string v3, "semantic_hints&8=0"

    .line 540
    .restart local v3       #selection:Ljava/lang/String;
    goto :goto_12

    .line 543
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_84
    const-string v3, "semantic_hints&8=0 AND type NOT IN (9,8)"

    .line 548
    .restart local v3       #selection:Ljava/lang/String;
    goto :goto_12

    .line 551
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_87
    const-string v3, "semantic_hints&8=0 AND type=1"

    .line 554
    .restart local v3       #selection:Ljava/lang/String;
    goto :goto_12

    .line 557
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_8a
    const-string v3, "semantic_hints&64!=0 AND type=1"

    .line 560
    .restart local v3       #selection:Ljava/lang/String;
    goto :goto_12

    .line 563
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_8d
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getStreamViewList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "circle_id IN ("

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_a1
    if-ge v1, v5, :cond_b7

    if-eqz v1, :cond_aa

    const/16 v0, 0x2c

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_aa
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v4, v0}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a1

    :cond_b7
    const/16 v0, 0x29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 564
    .restart local v3       #selection:Ljava/lang/String;
    goto/16 :goto_12

    .line 567
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_c2
    const-string v3, "semantic_hints&8=0"

    .line 569
    .restart local v3       #selection:Ljava/lang/String;
    goto/16 :goto_12

    .line 572
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_c6
    const-string v3, "semantic_hints&8=0"

    .line 574
    .restart local v3       #selection:Ljava/lang/String;
    goto/16 :goto_12

    .line 577
    .end local v3           #selection:Ljava/lang/String;
    :pswitch_ca
    const-string v3, "semantic_hints&1=0"

    .line 579
    .restart local v3       #selection:Ljava/lang/String;
    goto/16 :goto_12

    .line 593
    .restart local v8       #querySql:Ljava/lang/String;
    :cond_ce
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_43

    .line 507
    nop

    :pswitch_data_e8
    .packed-switch -0x1
        :pswitch_72
        :pswitch_11
        :pswitch_75
        :pswitch_78
        :pswitch_7e
        :pswitch_7b
        :pswitch_81
        :pswitch_87
        :pswitch_8a
        :pswitch_8d
        :pswitch_84
        :pswitch_c2
        :pswitch_c6
        :pswitch_ca
    .end packed-switch
.end method

.method public static getDefaultCircleId(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;
    .registers 11
    .parameter "context"
    .parameter "circlesCursor"
    .parameter "forSharing"

    .prologue
    const/4 v7, 0x0

    .line 4119
    if-nez p1, :cond_5

    move-object v6, v7

    .line 4142
    :goto_4
    return-object v6

    .line 4123
    :cond_5
    const-string v6, "semantic_hints"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 4124
    .local v5, semanticHintsColumn:I
    const-string v6, "circle_name"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 4125
    .local v4, nameColumn:I
    const-string v6, "circle_id"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 4126
    .local v1, idColumn:I
    if-eqz p2, :cond_40

    const v6, 0x7f0803fe

    :goto_1c
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 4128
    .local v0, defaultName:Ljava/lang/String;
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_4c

    .line 4130
    :cond_26
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    and-int/lit8 v6, v6, 0x40

    if-eqz v6, :cond_44

    const/4 v2, 0x1

    .line 4133
    .local v2, isCircleForSharing:Z
    :goto_2f
    if-ne p2, v2, :cond_46

    .line 4134
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4135
    .local v3, name:Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_46

    .line 4136
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 4126
    .end local v0           #defaultName:Ljava/lang/String;
    .end local v2           #isCircleForSharing:Z
    .end local v3           #name:Ljava/lang/String;
    :cond_40
    const v6, 0x7f0803ff

    goto :goto_1c

    .line 4130
    .restart local v0       #defaultName:Ljava/lang/String;
    :cond_44
    const/4 v2, 0x0

    goto :goto_2f

    .line 4139
    .restart local v2       #isCircleForSharing:Z
    :cond_46
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_26

    .end local v2           #isCircleForSharing:Z
    :cond_4c
    move-object v6, v7

    .line 4142
    goto :goto_4
.end method

.method private static getDefaultShowOrder(I)I
    .registers 2
    .parameter "type"

    .prologue
    .line 1707
    packed-switch p0, :pswitch_data_16

    .line 1724
    :pswitch_3
    const/16 v0, 0x3c

    .line 1726
    .local v0, showOrder:I
    :goto_5
    return v0

    .line 1709
    .end local v0           #showOrder:I
    :pswitch_6
    const/16 v0, 0x32

    .line 1710
    .restart local v0       #showOrder:I
    goto :goto_5

    .line 1712
    .end local v0           #showOrder:I
    :pswitch_9
    const/16 v0, 0x28

    .line 1713
    .restart local v0       #showOrder:I
    goto :goto_5

    .line 1715
    .end local v0           #showOrder:I
    :pswitch_c
    const/16 v0, 0x14

    .line 1716
    .restart local v0       #showOrder:I
    goto :goto_5

    .line 1718
    .end local v0           #showOrder:I
    :pswitch_f
    const/16 v0, 0x1e

    .line 1719
    .restart local v0       #showOrder:I
    goto :goto_5

    .line 1721
    .end local v0           #showOrder:I
    :pswitch_12
    const/16 v0, 0x3e8

    .line 1722
    .restart local v0       #showOrder:I
    goto :goto_5

    .line 1707
    nop

    :pswitch_data_16
    .packed-switch 0x5
        :pswitch_c
        :pswitch_3
        :pswitch_f
        :pswitch_9
        :pswitch_6
        :pswitch_12
    .end packed-switch
.end method

.method private static getLastUpdatedTime(Lcom/google/api/services/plusi/model/DataCirclePerson;)J
    .registers 5
    .parameter "person"

    .prologue
    .line 4059
    iget-object v2, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->lastUpdateTime:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 4061
    iget-object v2, p0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->lastUpdateTime:Ljava/lang/String;

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    .line 4065
    .local v0, lastUpdatedTime:J
    :goto_14
    return-wide v0

    .line 4063
    .end local v0           #lastUpdatedTime:J
    :cond_15
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .restart local v0       #lastUpdatedTime:J
    goto :goto_14
.end method

.method public static getMembershipChangeMessageId(Ljava/util/List;Ljava/util/List;)I
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .local p0, circlesToAdd:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p1, circlesToRemove:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3226
    if-eqz p0, :cond_1b

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1b

    move v0, v1

    .line 3227
    .local v0, adding:Z
    :goto_b
    if-eqz p1, :cond_1d

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1d

    .line 3228
    .local v1, removing:Z
    :goto_13
    if-eqz v0, :cond_1f

    if-nez v1, :cond_1f

    .line 3229
    const v2, 0x7f080408

    .line 3233
    :goto_1a
    return v2

    .end local v0           #adding:Z
    .end local v1           #removing:Z
    :cond_1b
    move v0, v2

    .line 3226
    goto :goto_b

    .restart local v0       #adding:Z
    :cond_1d
    move v1, v2

    .line 3227
    goto :goto_13

    .line 3230
    .restart local v1       #removing:Z
    :cond_1f
    if-eqz v1, :cond_27

    if-nez v0, :cond_27

    .line 3231
    const v2, 0x7f080409

    goto :goto_1a

    .line 3233
    :cond_27
    const v2, 0x7f08040a

    goto :goto_1a
.end method

.method public static getPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter "circleId"
    .parameter "excludedCircleId"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 700
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->ensurePeopleSynced(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 727
    :goto_8
    return-object v5

    .line 705
    :cond_9
    if-nez p2, :cond_39

    .line 706
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    .line 710
    .local v1, uri:Landroid/net/Uri;
    :goto_d
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    .line 711
    if-eqz p3, :cond_2d

    .line 712
    if-nez p6, :cond_48

    .line 713
    const/4 v0, 0x1

    new-array p6, v0, [Ljava/lang/String;

    .end local p6
    aput-object p3, p6, v2

    .line 721
    .restart local p6
    :goto_1a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND person_id NOT IN (SELECT link_person_id FROM circle_contact WHERE link_circle_id=?)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 727
    :cond_2d
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_8

    .line 708
    .end local v1           #uri:Landroid/net/Uri;
    :cond_39
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_BY_CIRCLE_ID_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .restart local v1       #uri:Landroid/net/Uri;
    goto :goto_d

    .line 716
    :cond_48
    move-object v6, p6

    .local v6, args:[Ljava/lang/String;
    array-length v0, p6

    add-int/lit8 v0, v0, 0x1

    new-array p6, v0, [Ljava/lang/String;

    .line 717
    array-length v0, v6

    invoke-static {v6, v2, p6, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 718
    array-length v0, p6

    add-int/lit8 v0, v0, -0x1

    aput-object p3, p6, v0

    goto :goto_1a
.end method

.method private static getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;
    .registers 3
    .parameter "memberId"

    .prologue
    const/4 v0, 0x0

    .line 3954
    if-nez p0, :cond_4

    .line 3970
    :cond_3
    :goto_3
    return-object v0

    .line 3958
    :cond_4
    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 3959
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "g:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3962
    :cond_1e
    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_38

    .line 3963
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "e:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3966
    :cond_38
    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->phone:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3967
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "p:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataCircleMemberId;->phone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method public static getProfileAndContactData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "fullProfileNeeded"

    .prologue
    .line 3005
    new-instance v0, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;-><init>()V

    .line 3008
    .local v0, data:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;
    sget-object v4, Lcom/google/android/apps/plus/content/EsPeopleData;->sProfileFetchLocks:Ljava/util/HashMap;

    monitor-enter v4

    .line 3009
    :try_start_8
    sget-object v3, Lcom/google/android/apps/plus/content/EsPeopleData;->sProfileFetchLocks:Ljava/util/HashMap;

    invoke-virtual {v3, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 3010
    .local v2, lock:Ljava/lang/Object;
    if-nez v2, :cond_1a

    .line 3011
    new-instance v2, Ljava/lang/Object;

    .end local v2           #lock:Ljava/lang/Object;
    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 3012
    .restart local v2       #lock:Ljava/lang/Object;
    sget-object v3, Lcom/google/android/apps/plus/content/EsPeopleData;->sProfileFetchLocks:Ljava/util/HashMap;

    invoke-virtual {v3, p2, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3014
    :cond_1a
    monitor-exit v4
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_40

    .line 3016
    monitor-enter v2

    .line 3017
    :try_start_1c
    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->loadProfileAndContactDataFromDatabase(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V

    .line 3018
    iget v3, v0, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    if-eqz v3, :cond_29

    if-eqz p3, :cond_35

    iget-object v3, v0, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-nez v3, :cond_35

    .line 3020
    :cond_29
    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2c
    .catchall {:try_start_1c .. :try_end_2c} :catchall_43

    move-result-object v1

    .line 3021
    .local v1, gaiaId:Ljava/lang/String;
    if-eqz v1, :cond_35

    .line 3023
    :try_start_2f
    invoke-static {p0, p1, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->loadProfileFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 3024
    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->loadProfileAndContactDataFromDatabase(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V
    :try_end_35
    .catchall {:try_start_2f .. :try_end_35} :catchall_43
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_35} :catch_49

    .line 3030
    .end local v1           #gaiaId:Ljava/lang/String;
    :cond_35
    :goto_35
    :try_start_35
    monitor-exit v2
    :try_end_36
    .catchall {:try_start_35 .. :try_end_36} :catchall_43

    .line 3032
    sget-object v4, Lcom/google/android/apps/plus/content/EsPeopleData;->sProfileFetchLocks:Ljava/util/HashMap;

    monitor-enter v4

    .line 3033
    :try_start_39
    sget-object v3, Lcom/google/android/apps/plus/content/EsPeopleData;->sProfileFetchLocks:Ljava/util/HashMap;

    invoke-virtual {v3, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3034
    monitor-exit v4
    :try_end_3f
    .catchall {:try_start_39 .. :try_end_3f} :catchall_46

    .line 3036
    return-object v0

    .line 3014
    .end local v2           #lock:Ljava/lang/Object;
    :catchall_40
    move-exception v3

    monitor-exit v4

    throw v3

    .line 3030
    .restart local v2       #lock:Ljava/lang/Object;
    :catchall_43
    move-exception v3

    monitor-exit v2

    throw v3

    .line 3034
    :catchall_46
    move-exception v3

    monitor-exit v4

    throw v3

    .restart local v1       #gaiaId:Ljava/lang/String;
    :catch_49
    move-exception v3

    goto :goto_35
.end method

.method private static getProfileInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Lcom/google/api/services/plusi/model/SimpleProfile;
    .registers 13
    .parameter "db"
    .parameter "personId"

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 2885
    const/4 v9, 0x0

    .line 2886
    .local v9, profileProto:[B
    const-string v1, "profiles"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "profile_proto"

    aput-object v0, v2, v10

    const-string v3, "profile_person_id=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v10

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2889
    .local v8, cursor:Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 2890
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    .line 2892
    :cond_23
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2893
    if-nez v9, :cond_29

    .line 2896
    :goto_28
    return-object v5

    :cond_29
    invoke-static {v9}, Lcom/google/android/apps/plus/content/EsPeopleData;->deserializeProfile([B)Lcom/google/api/services/plusi/model/SimpleProfile;

    move-result-object v5

    goto :goto_28
.end method

.method private static getSemanticHints(Lcom/google/api/services/plusi/model/DataSystemGroup;)I
    .registers 7
    .parameter "group"

    .prologue
    .line 4095
    const/4 v0, 0x0

    .line 4096
    .local v0, flags:I
    iget-object v2, p0, Lcom/google/api/services/plusi/model/DataSystemGroup;->clientPolicy:Lcom/google/api/services/plusi/model/DataClientPolicies;

    .line 4097
    .local v2, policy:Lcom/google/api/services/plusi/model/DataClientPolicies;
    if-eqz v2, :cond_48

    iget-object v5, v2, Lcom/google/api/services/plusi/model/DataClientPolicies;->policy:Ljava/util/List;

    if-eqz v5, :cond_48

    .line 4098
    const/4 v1, 0x0

    .local v1, i:I
    iget-object v5, v2, Lcom/google/api/services/plusi/model/DataClientPolicies;->policy:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .local v3, size:I
    :goto_10
    if-ge v1, v3, :cond_48

    .line 4099
    iget-object v5, v2, Lcom/google/api/services/plusi/model/DataClientPolicies;->policy:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 4100
    .local v4, value:Ljava/lang/String;
    const-string v5, "CANNOT_VIEW_MEMBERSHIP"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_27

    .line 4101
    or-int/lit8 v0, v0, 0x1

    .line 4098
    :cond_24
    :goto_24
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 4102
    :cond_27
    const-string v5, "CANNOT_MODIFY_MEMBERSHIP"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_32

    .line 4103
    or-int/lit8 v0, v0, 0x2

    goto :goto_24

    .line 4104
    :cond_32
    const-string v5, "CANNOT_ACL_TO"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3d

    .line 4105
    or-int/lit8 v0, v0, 0x8

    goto :goto_24

    .line 4106
    :cond_3d
    const-string v5, "VISIBLE_ONLY_WHEN_POPULATED"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 4107
    or-int/lit8 v0, v0, 0x10

    goto :goto_24

    .line 4111
    .end local v1           #i:I
    .end local v3           #size:I
    .end local v4           #value:Ljava/lang/String;
    :cond_48
    return v0
.end method

.method public static getStringForAddress(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;
    .registers 5
    .parameter "context"
    .parameter "tag"

    .prologue
    const/4 v0, 0x0

    .line 3550
    if-nez p1, :cond_4

    .line 3563
    :cond_3
    :goto_3
    return-object v0

    .line 3554
    :cond_4
    const-string v1, "HOME"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 3555
    const v0, 0x7f08024d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3556
    :cond_16
    const-string v1, "WORK"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 3557
    const v0, 0x7f08024e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3558
    :cond_28
    const-string v1, "OTHER"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3560
    const-string v1, "CUSTOM"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3561
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    goto :goto_3
.end method

.method public static getStringForEmailType(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;
    .registers 5
    .parameter "context"
    .parameter "tag"

    .prologue
    const/4 v0, 0x0

    .line 3391
    if-nez p1, :cond_4

    .line 3404
    :cond_3
    :goto_3
    return-object v0

    .line 3395
    :cond_4
    const-string v1, "HOME"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 3396
    const v0, 0x7f080241

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3397
    :cond_16
    const-string v1, "WORK"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 3398
    const v0, 0x7f080242

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3399
    :cond_28
    const-string v1, "OTHER"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3401
    const-string v1, "CUSTOM"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3402
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    goto :goto_3
.end method

.method public static getStringForPhoneType(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;
    .registers 5
    .parameter "context"
    .parameter "tag"

    .prologue
    const/4 v0, 0x0

    .line 3466
    if-nez p1, :cond_4

    .line 3513
    :cond_3
    :goto_3
    return-object v0

    .line 3470
    :cond_4
    const-string v1, "HOME"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 3471
    const v0, 0x7f08022d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3472
    :cond_16
    const-string v1, "WORK"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 3473
    const v0, 0x7f08022f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3474
    :cond_28
    const-string v1, "OTHER"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3476
    const-string v1, "HOME_FAX"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_44

    .line 3477
    const v0, 0x7f080232

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3478
    :cond_44
    const-string v1, "WORK_FAX"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_56

    .line 3479
    const v0, 0x7f080233

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3480
    :cond_56
    const-string v1, "MOBILE"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_68

    .line 3481
    const v0, 0x7f08022e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3482
    :cond_68
    const-string v1, "PAGER"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7a

    .line 3483
    const v0, 0x7f080240

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3484
    :cond_7a
    const-string v1, "OTHER_FAX"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8d

    .line 3485
    const v0, 0x7f080234

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3486
    :cond_8d
    const-string v1, "COMPANY_MAIN"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a0

    .line 3487
    const v0, 0x7f080235

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3488
    :cond_a0
    const-string v1, "ASSISTANT"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b3

    .line 3489
    const v0, 0x7f080236

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3490
    :cond_b3
    const-string v1, "CAR"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c6

    .line 3491
    const v0, 0x7f080237

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3492
    :cond_c6
    const-string v1, "RADIO"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d9

    .line 3493
    const v0, 0x7f080238

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3494
    :cond_d9
    const-string v1, "ISDN"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_ec

    .line 3495
    const v0, 0x7f080239

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3496
    :cond_ec
    const-string v1, "CALLBACK"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_ff

    .line 3497
    const v0, 0x7f08023a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3498
    :cond_ff
    const-string v1, "TELEX"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_112

    .line 3499
    const v0, 0x7f08023b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3500
    :cond_112
    const-string v1, "TTY_TDD"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_125

    .line 3501
    const v0, 0x7f08023c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3502
    :cond_125
    const-string v1, "WORK_MOBILE"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_138

    .line 3503
    const v0, 0x7f08023d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3504
    :cond_138
    const-string v1, "WORK_PAGER"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14b

    .line 3505
    const v0, 0x7f08023e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3506
    :cond_14b
    const-string v1, "MAIN"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15e

    .line 3507
    const v0, 0x7f080230

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3508
    :cond_15e
    const-string v1, "GRAND_CENTRAL"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_171

    .line 3509
    const v0, 0x7f08023f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 3510
    :cond_171
    const-string v1, "CUSTOM"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3511
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    goto/16 :goto_3
.end method

.method public static getStringForPhoneType(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "context"
    .parameter "type"

    .prologue
    const/4 v0, 0x0

    .line 3413
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_9

    move-object p1, v0

    .line 3457
    .end local p1
    :cond_8
    :goto_8
    return-object p1

    .line 3415
    .restart local p1
    :cond_9
    const-string v1, "1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 3416
    const v0, 0x7f08022d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_8

    .line 3417
    :cond_19
    const-string v1, "2"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_29

    .line 3418
    const v0, 0x7f08022f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_8

    .line 3419
    :cond_29
    const-string v1, "3"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_33

    move-object p1, v0

    .line 3421
    goto :goto_8

    .line 3422
    :cond_33
    const-string v0, "4"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 3423
    const v0, 0x7f080232

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_8

    .line 3424
    :cond_43
    const-string v0, "5"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 3425
    const v0, 0x7f080233

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_8

    .line 3426
    :cond_53
    const-string v0, "6"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 3427
    const v0, 0x7f08022e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_8

    .line 3428
    :cond_63
    const-string v0, "7"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 3429
    const v0, 0x7f080240

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_8

    .line 3430
    :cond_73
    const-string v0, "8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_83

    .line 3431
    const v0, 0x7f080234

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_8

    .line 3432
    :cond_83
    const-string v0, "9"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_94

    .line 3433
    const v0, 0x7f080235

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3434
    :cond_94
    const-string v0, "10"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a5

    .line 3435
    const v0, 0x7f080236

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3436
    :cond_a5
    const-string v0, "11"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 3437
    const v0, 0x7f080237

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3438
    :cond_b6
    const-string v0, "12"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c7

    .line 3439
    const v0, 0x7f080238

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3440
    :cond_c7
    const-string v0, "13"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d8

    .line 3441
    const v0, 0x7f080239

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3442
    :cond_d8
    const-string v0, "14"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e9

    .line 3443
    const v0, 0x7f08023a

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3444
    :cond_e9
    const-string v0, "15"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_fa

    .line 3445
    const v0, 0x7f08023b

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3446
    :cond_fa
    const-string v0, "16"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10b

    .line 3447
    const v0, 0x7f08023c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3448
    :cond_10b
    const-string v0, "17"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11c

    .line 3449
    const v0, 0x7f08023d

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3450
    :cond_11c
    const-string v0, "18"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12d

    .line 3451
    const v0, 0x7f08023e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3452
    :cond_12d
    const-string v0, "19"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13e

    .line 3453
    const v0, 0x7f080230

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8

    .line 3454
    :cond_13e
    const-string v0, "20"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 3455
    const v0, 0x7f08023f

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_8
.end method

.method public static getStringForPlusPagePhoneType(Landroid/content/Context;Lcom/google/api/services/plusi/model/ContactTag;)Ljava/lang/String;
    .registers 5
    .parameter "context"
    .parameter "tag"

    .prologue
    const/4 v0, 0x0

    .line 3522
    if-nez p1, :cond_4

    .line 3541
    :cond_3
    :goto_3
    return-object v0

    .line 3526
    :cond_4
    const-string v1, "HOME"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_22

    const-string v1, "WORK"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_22

    const-string v1, "OTHER"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2a

    .line 3529
    :cond_22
    const v0, 0x7f08022c

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3530
    :cond_2a
    const-string v1, "HOME_FAX"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_48

    const-string v1, "WORK_FAX"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_48

    const-string v1, "OTHER_FAX"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_50

    .line 3533
    :cond_48
    const v0, 0x7f080231

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3534
    :cond_50
    const-string v1, "MOBILE"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_62

    .line 3535
    const v0, 0x7f08022e

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3536
    :cond_62
    const-string v1, "PAGER"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_74

    .line 3537
    const v0, 0x7f080240

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 3538
    :cond_74
    const-string v1, "CUSTOM"

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3539
    iget-object v0, p1, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    goto :goto_3
.end method

.method public static getSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;ZZ)Landroid/database/Cursor;
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "projection"
    .parameter "refreshIfStale"
    .parameter "longTimeToLive"

    .prologue
    const-wide/32 v4, 0x2bf20

    const/4 v3, 0x0

    .line 786
    invoke-static {p0, p1, v4, v5, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->ensureSuggestedPeopleSynced(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)Z

    move-result v0

    if-nez v0, :cond_b

    .line 799
    :cond_a
    :goto_a
    return-object v3

    .line 794
    :cond_b
    invoke-static {p0, p1, v4, v5, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->ensureSuggestedCelebritiesSynced(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 798
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    .line 799
    .local v1, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    goto :goto_a
.end method

.method public static getUserName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"

    .prologue
    .line 4149
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4152
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_8
    const-string v1, "SELECT name  FROM contacts  WHERE gaia_id = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_13
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_8 .. :try_end_13} :catch_15

    move-result-object v1

    .line 4157
    :goto_14
    return-object v1

    :catch_15
    move-exception v1

    const/4 v1, 0x0

    goto :goto_14
.end method

.method public static hasCircleActionData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Z
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "notificationId"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1953
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1956
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v5, "SELECT count(*) FROM circle_action WHERE notification_id=?"

    new-array v6, v3, [Ljava/lang/String;

    aput-object p2, v6, v4

    invoke-static {v0, v5, v6}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v1

    .line 1961
    .local v1, storedCount:J
    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-eqz v5, :cond_1b

    :goto_1a
    return v3

    :cond_1b
    move v3, v4

    goto :goto_1a
.end method

.method public static hasPublicCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .registers 10
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 3358
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3360
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v6, "circles"

    const-string v7, "type=9"

    invoke-static {v0, v6, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_15

    .line 3377
    :cond_14
    :goto_14
    return v4

    .line 3366
    :cond_15
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->queryCircleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v2

    .line 3367
    .local v2, syncTimestamp:J
    const-wide/16 v6, -0x1

    cmp-long v6, v2, v6

    if-nez v6, :cond_23

    .line 3370
    const/4 v6, 0x0

    :try_start_20
    invoke-static {p0, p1, v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_23} :catch_2f

    .line 3377
    :cond_23
    const-string v6, "circles"

    const-string v7, "type=9"

    invoke-static {v0, v6, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_14

    move v4, v5

    goto :goto_14

    .line 3371
    :catch_2f
    move-exception v1

    .line 3372
    .local v1, e:Ljava/io/IOException;
    const-string v4, "EsPeopleData"

    const-string v6, "Error syncing circles"

    invoke-static {v4, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v4, v5

    .line 3373
    goto :goto_14
.end method

.method public static insertBlockedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .registers 23
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCirclePerson;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 987
    .local p2, people:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataCirclePerson;>;"
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 989
    .local v18, unblockedPeople:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 991
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 993
    :try_start_10
    new-instance v19, Landroid/content/ContentValues;

    invoke-direct/range {v19 .. v19}, Landroid/content/ContentValues;-><init>()V

    .line 994
    .local v19, values:Landroid/content/ContentValues;
    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    .line 997
    .local v9, arg:[Ljava/lang/String;
    const-string v2, "contacts"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "person_id"

    aput-object v5, v3, v4

    const-string v4, "blocked=1"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2b
    .catchall {:try_start_10 .. :try_end_2b} :catchall_42

    move-result-object v12

    .line 1001
    .local v12, cursor:Landroid/database/Cursor;
    :goto_2c
    :try_start_2c
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_47

    .line 1002
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_3c
    .catchall {:try_start_2c .. :try_end_3c} :catchall_3d

    goto :goto_2c

    .line 1005
    :catchall_3d
    move-exception v2

    :try_start_3e
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_42
    .catchall {:try_start_3e .. :try_end_42} :catchall_42

    .line 1054
    .end local v9           #arg:[Ljava/lang/String;
    .end local v12           #cursor:Landroid/database/Cursor;
    .end local v19           #values:Landroid/content/ContentValues;
    :catchall_42
    move-exception v2

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2

    .line 1005
    .restart local v9       #arg:[Ljava/lang/String;
    .restart local v12       #cursor:Landroid/database/Cursor;
    .restart local v19       #values:Landroid/content/ContentValues;
    :cond_47
    :try_start_47
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 1008
    if-nez p2, :cond_66

    const/4 v11, 0x0

    .line 1009
    .local v11, count:I
    :goto_4d
    const/4 v13, 0x0

    .local v13, i:I
    :goto_4e
    if-ge v13, v11, :cond_6b

    .line 1010
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1009
    add-int/lit8 v13, v13, 0x1

    goto :goto_4e

    .line 1008
    .end local v11           #count:I
    .end local v13           #i:I
    :cond_66
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v11

    goto :goto_4d

    .line 1013
    .restart local v11       #count:I
    .restart local v13       #i:I
    :cond_6b
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a6

    .line 1014
    const-string v2, "blocked"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1015
    const-string v2, "last_updated_time"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1016
    invoke-virtual/range {v18 .. v18}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, i$:Ljava/util/Iterator;
    :goto_8d
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a6

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 1017
    .local v16, personId:Ljava/lang/String;
    const/4 v2, 0x0

    aput-object v16, v9, v2

    .line 1018
    const-string v2, "contacts"

    const-string v3, "person_id=?"

    move-object/from16 v0, v19

    invoke-virtual {v1, v2, v0, v3, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_8d

    .line 1024
    .end local v14           #i$:Ljava/util/Iterator;
    .end local v16           #personId:Ljava/lang/String;
    :cond_a6
    const/4 v13, 0x0

    :goto_a7
    if-ge v13, v11, :cond_18f

    .line 1025
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/api/services/plusi/model/DataCirclePerson;

    .line 1026
    .local v15, person:Lcom/google/api/services/plusi/model/DataCirclePerson;
    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v16

    .line 1028
    .restart local v16       #personId:Ljava/lang/String;
    const-string v2, "PLUSPAGE"

    iget-object v3, v15, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->profileType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18b

    const/16 v17, 0x2

    .line 1030
    .local v17, profileType:I
    :goto_cd
    iget-object v2, v15, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->displayName:Ljava/lang/String;

    invoke-static {v15}, Lcom/google/android/apps/plus/content/EsPeopleData;->getLastUpdatedTime(Lcom/google/api/services/plusi/model/DataCirclePerson;)J

    move-result-wide v3

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "name"

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "last_updated_time"

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "in_my_circles"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "blocked"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "profile_type"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "avatar"

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {v16 .. v16}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "profile_state"

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "contacts"

    const-string v4, "person_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v16, v6, v7

    invoke-virtual {v1, v3, v5, v4, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_13f

    const-string v3, "person_id"

    move-object/from16 v0, v16

    invoke-virtual {v5, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "gaia_id"

    invoke-virtual {v5, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "contacts"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_13f
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    const-string v2, "contact_update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "contact_proto"

    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->serializeContactInfo(Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;)[B

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v2, "profiles"

    const-string v3, "profile_person_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v16, v4, v6

    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_176

    const-string v2, "profile_person_id"

    move-object/from16 v0, v16

    invoke-virtual {v5, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "profiles"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1035
    :cond_176
    const/4 v2, 0x0

    aput-object v16, v9, v2

    .line 1036
    const-string v2, "circle_contact"

    const-string v3, "link_person_id=?"

    invoke-virtual {v1, v2, v3, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1038
    const-string v2, "contact_search"

    const-string v3, "search_person_id=?"

    invoke-virtual {v1, v2, v3, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1024
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_a7

    .line 1028
    .end local v17           #profileType:I
    :cond_18b
    const/16 v17, 0x1

    goto/16 :goto_cd

    .line 1042
    .end local v15           #person:Lcom/google/api/services/plusi/model/DataCirclePerson;
    .end local v16           #personId:Ljava/lang/String;
    :cond_18f
    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentValues;->clear()V

    .line 1043
    const-string v2, "contact_count"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1044
    const/4 v2, 0x0

    const-string v3, "15"

    aput-object v3, v9, v2

    .line 1045
    const-string v2, "circles"

    const-string v3, "circle_id=?"

    move-object/from16 v0, v19

    invoke-virtual {v1, v2, v0, v3, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1047
    invoke-virtual/range {v19 .. v19}, Landroid/content/ContentValues;->clear()V

    .line 1048
    const-string v2, "blocked_people_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1050
    const-string v2, "account_status"

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1052
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1c9
    .catchall {:try_start_47 .. :try_end_1c9} :catchall_42

    .line 1054
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1057
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    .line 1058
    .local v10, contentResolver:Landroid/content/ContentResolver;
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1059
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v10, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1060
    return-void
.end method

.method public static insertCelebritySuggestions(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, categories:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;>;"
    const/4 v11, 0x0

    .line 2017
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2019
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2021
    :try_start_c
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    .line 2024
    .local v1, count:I
    const/4 v5, 0x0

    .line 2025
    .local v5, selection:Ljava/lang/String;
    if-lez v1, :cond_41

    .line 2026
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2027
    .local v4, sb:Ljava/lang/StringBuilder;
    const-string v7, "category NOT IN ("

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2028
    const-string v7, "#"

    invoke-static {v4, v7}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 2029
    const/4 v3, 0x0

    .local v3, i:I
    :goto_23
    if-ge v3, v1, :cond_38

    .line 2030
    const-string v7, ","

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2031
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->category:Ljava/lang/String;

    invoke-static {v4, v7}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 2029
    add-int/lit8 v3, v3, 0x1

    goto :goto_23

    .line 2033
    :cond_38
    const-string v7, ")"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2034
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2037
    .end local v3           #i:I
    .end local v4           #sb:Ljava/lang/StringBuilder;
    :cond_41
    const-string v7, "suggested_people"

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2039
    const/4 v3, 0x0

    .restart local v3       #i:I
    :goto_48
    if-ge v3, v1, :cond_62

    .line 2040
    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;

    .line 2041
    .local v0, category:Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;
    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->category:Ljava/lang/String;

    iget-object v8, v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->categoryName:Ljava/lang/String;

    add-int/lit8 v9, v3, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;->celebrity:Ljava/util/List;

    invoke-static {v2, v7, v8, v9, v10}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceSuggestionsInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 2039
    add-int/lit8 v3, v3, 0x1

    goto :goto_48

    .line 2045
    .end local v0           #category:Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;
    :cond_62
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 2046
    .local v6, values:Landroid/content/ContentValues;
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 2047
    const-string v7, "suggested_celeb_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2049
    const-string v7, "account_status"

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v7, v6, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2051
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_81
    .catchall {:try_start_c .. :try_end_81} :catchall_8e

    .line 2053
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2056
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v11}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2057
    return-void

    .line 2053
    .end local v1           #count:I
    .end local v3           #i:I
    .end local v5           #selection:Ljava/lang/String;
    .end local v6           #values:Landroid/content/ContentValues;
    :catchall_8e
    move-exception v7

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v7
.end method

.method public static insertCircleActionData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "notificationId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataActor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1975
    .local p3, actors:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataActor;>;"
    if-eqz p3, :cond_8

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2002
    :cond_8
    :goto_8
    return-void

    .line 1981
    :cond_9
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->ensurePeopleSynced(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    .line 1983
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1986
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_14
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1989
    new-instance v3, Landroid/content/ContentValues;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 1990
    .local v3, values:Landroid/content/ContentValues;
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_21
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_51

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataActor;

    .line 1991
    .local v0, actor:Lcom/google/api/services/plusi/model/DataActor;
    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->name:Ljava/lang/String;

    iget-object v6, v0, Lcom/google/api/services/plusi/model/DataActor;->photoUrl:Ljava/lang/String;

    invoke-static {v1, v4, v5, v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1993
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    .line 1994
    const-string v4, "notification_id"

    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1995
    const-string v4, "gaia_id"

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataActor;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1996
    const-string v4, "circle_action"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_4b
    .catchall {:try_start_14 .. :try_end_4b} :catchall_4c

    goto :goto_21

    .line 2001
    .end local v0           #actor:Lcom/google/api/services/plusi/model/DataActor;
    .end local v2           #i$:Ljava/util/Iterator;
    .end local v3           #values:Landroid/content/ContentValues;
    :catchall_4c
    move-exception v4

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4

    .line 1999
    .restart local v2       #i$:Ljava/util/Iterator;
    .restart local v3       #values:Landroid/content/ContentValues;
    :cond_51
    :try_start_51
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_54
    .catchall {:try_start_51 .. :try_end_54} :catchall_4c

    .line 2001
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_8
.end method

.method private static insertCirclePersons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Z
    .registers 14
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCirclePerson;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2255
    .local p2, contacts:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataCirclePerson;>;"
    const/4 v1, 0x0

    .line 2256
    .local v1, contactsUpdated:Z
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v5

    .line 2258
    .local v5, numContacts:I
    const-string v8, "EsPeopleData"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_36

    .line 2259
    const/4 v4, 0x0

    .local v4, i:I
    :goto_f
    if-ge v4, v5, :cond_36

    .line 2260
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataCirclePerson;

    .line 2261
    .local v0, contact:Lcom/google/api/services/plusi/model/DataCirclePerson;
    const-string v8, "EsPeopleData"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, ">>>>> Contact id: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/api/services/plusi/model/DataCirclePersonJson;->getInstance()Lcom/google/api/services/plusi/model/DataCirclePersonJson;

    move-result-object v10

    invoke-virtual {v10, v0}, Lcom/google/api/services/plusi/model/DataCirclePersonJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2259
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 2266
    .end local v0           #contact:Lcom/google/api/services/plusi/model/DataCirclePerson;
    .end local v4           #i:I
    :cond_36
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2269
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .line 2270
    .local v3, fromIndex:I
    :goto_3f
    if-ge v3, v5, :cond_4f

    .line 2271
    add-int/lit8 v6, v3, 0x4b

    .line 2272
    .local v6, toIndex:I
    if-le v6, v5, :cond_46

    .line 2273
    move v6, v5

    .line 2275
    :cond_46
    invoke-static {p0, v2, p2, v3, v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceCirclePersons$7c3ddbf6(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;II)I

    move-result v7

    .line 2277
    .local v7, updateCount:I
    if-lez v7, :cond_4d

    .line 2278
    const/4 v1, 0x1

    .line 2280
    :cond_4d
    move v3, v6

    .line 2281
    goto :goto_3f

    .line 2283
    .end local v6           #toIndex:I
    .end local v7           #updateCount:I
    :cond_4f
    return v1
.end method

.method private static insertCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/DataViewerCircles;Lcom/google/api/services/plusi/model/DataSystemGroups;)I
    .registers 40
    .parameter "context"
    .parameter "account"
    .parameter "circleList"
    .parameter "systemGroups"

    .prologue
    .line 1455
    if-eqz p2, :cond_8

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataViewerCircles;->circle:Ljava/util/List;

    if-nez v3, :cond_a

    .line 1456
    :cond_8
    const/4 v15, 0x0

    .line 1640
    :cond_9
    :goto_9
    return v15

    .line 1459
    :cond_a
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 1460
    .local v13, circleMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCircleData;>;"
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataViewerCircles;->circle:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2f

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/DataCircleData;

    .line 1461
    .local v11, circle:Lcom/google/api/services/plusi/model/DataCircleData;
    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleId;->focusId:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v13, v3, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_17

    .line 1464
    .end local v11           #circle:Lcom/google/api/services/plusi/model/DataCircleData;
    :cond_2f
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    .line 1465
    .local v20, groupMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/api/services/plusi/model/DataSystemGroup;>;"
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSystemGroups;->systemGroup:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_3c
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5d

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/api/services/plusi/model/DataSystemGroup;

    .line 1466
    .local v19, group:Lcom/google/api/services/plusi/model/DataSystemGroup;
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->id:Ljava/lang/String;

    if-nez v3, :cond_58

    const-string v3, "0"

    :goto_50
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3c

    :cond_58
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->id:Ljava/lang/String;

    goto :goto_50

    .line 1469
    .end local v19           #group:Lcom/google/api/services/plusi/model/DataSystemGroup;
    :cond_5d
    const/4 v15, 0x0

    .line 1470
    .local v15, count:I
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1472
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1474
    :try_start_69
    new-instance v34, Ljava/util/ArrayList;

    invoke-direct/range {v34 .. v34}, Ljava/util/ArrayList;-><init>()V

    .line 1475
    .local v34, updates:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 1476
    .local v17, deletedCircles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "circles"

    sget-object v4, Lcom/google/android/apps/plus/content/EsPeopleData;->CIRCLES_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_7f
    .catchall {:try_start_69 .. :try_end_7f} :catchall_b9

    move-result-object v16

    .line 1479
    .local v16, cursor:Landroid/database/Cursor;
    const/16 v21, 0x0

    .line 1481
    .local v21, haveVirtualCirles:Z
    :cond_82
    :goto_82
    :try_start_82
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1b9

    .line 1482
    const/4 v3, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 1485
    .local v14, circleType:I
    const/4 v3, -0x1

    if-ne v14, v3, :cond_95

    .line 1486
    const/16 v21, 0x1

    .line 1487
    goto :goto_82

    .line 1490
    :cond_95
    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 1491
    .local v12, circleId:Ljava/lang/String;
    invoke-virtual {v13, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/DataCircleData;

    .line 1492
    .restart local v11       #circle:Lcom/google/api/services/plusi/model/DataCircleData;
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/api/services/plusi/model/DataSystemGroup;

    .line 1493
    .restart local v19       #group:Lcom/google/api/services/plusi/model/DataSystemGroup;
    if-nez v11, :cond_be

    if-nez v19, :cond_be

    .line 1494
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_b3
    .catchall {:try_start_82 .. :try_end_b3} :catchall_b4

    goto :goto_82

    .line 1547
    .end local v11           #circle:Lcom/google/api/services/plusi/model/DataCircleData;
    .end local v12           #circleId:Ljava/lang/String;
    .end local v14           #circleType:I
    .end local v19           #group:Lcom/google/api/services/plusi/model/DataSystemGroup;
    :catchall_b4
    move-exception v3

    :try_start_b5
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_b9
    .catchall {:try_start_b5 .. :try_end_b9} :catchall_b9

    .line 1632
    .end local v16           #cursor:Landroid/database/Cursor;
    .end local v17           #deletedCircles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v21           #haveVirtualCirles:Z
    .end local v34           #updates:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :catchall_b9
    move-exception v3

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 1496
    .restart local v11       #circle:Lcom/google/api/services/plusi/model/DataCircleData;
    .restart local v12       #circleId:Ljava/lang/String;
    .restart local v14       #circleType:I
    .restart local v16       #cursor:Landroid/database/Cursor;
    .restart local v17       #deletedCircles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v19       #group:Lcom/google/api/services/plusi/model/DataSystemGroup;
    .restart local v21       #haveVirtualCirles:Z
    .restart local v34       #updates:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_be
    const/4 v3, 0x1

    :try_start_bf
    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 1497
    .local v25, name:Ljava/lang/String;
    const/4 v3, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v24

    .line 1498
    .local v24, memberCount:I
    const/4 v3, 0x4

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v31

    .line 1506
    .local v31, semanticHints:I
    if-eqz v11, :cond_177

    .line 1507
    invoke-virtual {v13, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1509
    const/16 v29, 0x1

    .line 1510
    .local v29, newType:I
    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataCircleProperties;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    .line 1511
    .local v27, newName:Ljava/lang/String;
    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v0, v3, Lcom/google/api/services/plusi/model/DataCircleProperties;->nameSortKey:Ljava/lang/String;

    move-object/from16 v32, v0

    .line 1512
    .local v32, sortKey:Ljava/lang/String;
    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleProperties;->memberCount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 1513
    .local v26, newMemberCount:I
    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleProperties;->forSharing:Ljava/lang/Boolean;

    if-eqz v3, :cond_171

    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleProperties;->forSharing:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_171

    const/16 v18, 0x1

    .line 1515
    .local v18, forSharing:Z
    :goto_100
    if-eqz v18, :cond_174

    const/16 v28, 0x40

    .line 1530
    .end local v18           #forSharing:Z
    .local v28, newSemanticHints:I
    :goto_104
    move/from16 v0, v29

    if-ne v0, v14, :cond_11e

    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_11e

    move/from16 v0, v26

    move/from16 v1, v24

    if-ne v0, v1, :cond_11e

    move/from16 v0, v28

    move/from16 v1, v31

    if-eq v0, v1, :cond_82

    .line 1534
    :cond_11e
    new-instance v35, Landroid/content/ContentValues;

    invoke-direct/range {v35 .. v35}, Landroid/content/ContentValues;-><init>()V

    .line 1535
    .local v35, values:Landroid/content/ContentValues;
    const-string v3, "circle_id"

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1536
    const-string v3, "circle_name"

    move-object/from16 v0, v35

    move-object/from16 v1, v27

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1537
    const-string v3, "sort_key"

    move-object/from16 v0, v35

    move-object/from16 v1, v32

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1538
    const-string v3, "type"

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1539
    const-string v3, "contact_count"

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1540
    const-string v3, "semantic_hints"

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1541
    const-string v3, "show_order"

    invoke-static/range {v29 .. v29}, Lcom/google/android/apps/plus/content/EsPeopleData;->getDefaultShowOrder(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1542
    invoke-virtual/range {v34 .. v35}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_82

    .line 1513
    .end local v28           #newSemanticHints:I
    .end local v35           #values:Landroid/content/ContentValues;
    :cond_171
    const/16 v18, 0x0

    goto :goto_100

    .line 1515
    .restart local v18       #forSharing:Z
    :cond_174
    const/16 v28, 0x0

    goto :goto_104

    .line 1518
    .end local v18           #forSharing:Z
    .end local v26           #newMemberCount:I
    .end local v27           #newName:Ljava/lang/String;
    .end local v29           #newType:I
    .end local v32           #sortKey:Ljava/lang/String;
    :cond_177
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->type:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleType(Ljava/lang/String;)I

    move-result v29

    .line 1521
    .restart local v29       #newType:I
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->name:Ljava/lang/String;

    move-object/from16 v27, v0

    .line 1522
    .restart local v27       #newName:Ljava/lang/String;
    const/16 v3, 0x9

    move/from16 v0, v29

    if-ne v0, v3, :cond_199

    .line 1523
    const v3, 0x7f08021a

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 1525
    :cond_199
    if-nez v27, :cond_1b1

    const/16 v32, 0x0

    .line 1526
    .restart local v32       #sortKey:Ljava/lang/String;
    :goto_19d
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->memberCount:Ljava/lang/Integer;

    if-eqz v3, :cond_1b6

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->memberCount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 1527
    .restart local v26       #newMemberCount:I
    :goto_1ab
    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/content/EsPeopleData;->getSemanticHints(Lcom/google/api/services/plusi/model/DataSystemGroup;)I

    move-result v28

    .restart local v28       #newSemanticHints:I
    goto/16 :goto_104

    .line 1525
    .end local v26           #newMemberCount:I
    .end local v28           #newSemanticHints:I
    .end local v32           #sortKey:Ljava/lang/String;
    :cond_1b1
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;
    :try_end_1b4
    .catchall {:try_start_bf .. :try_end_1b4} :catchall_b4

    move-result-object v32

    goto :goto_19d

    .line 1526
    .restart local v32       #sortKey:Ljava/lang/String;
    :cond_1b6
    const/16 v26, 0x0

    goto :goto_1ab

    .line 1547
    .end local v11           #circle:Lcom/google/api/services/plusi/model/DataCircleData;
    .end local v12           #circleId:Ljava/lang/String;
    .end local v14           #circleType:I
    .end local v19           #group:Lcom/google/api/services/plusi/model/DataSystemGroup;
    .end local v24           #memberCount:I
    .end local v25           #name:Ljava/lang/String;
    .end local v27           #newName:Ljava/lang/String;
    .end local v29           #newType:I
    .end local v31           #semanticHints:I
    .end local v32           #sortKey:Ljava/lang/String;
    :cond_1b9
    :try_start_1b9
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 1551
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_216

    .line 1552
    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    .line 1553
    .local v30, sb:Ljava/lang/StringBuilder;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1554
    .local v10, args:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "circle_id IN ("

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1555
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_1d7
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1f0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 1556
    .local v23, id:Ljava/lang/String;
    const-string v3, "?,"

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1557
    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1d7

    .line 1559
    .end local v23           #id:Ljava/lang/String;
    :cond_1f0
    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1560
    const-string v3, ")"

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1561
    const-string v4, "circles"

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1562
    add-int/lit8 v15, v15, 0x1

    .line 1566
    .end local v10           #args:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v30           #sb:Ljava/lang/StringBuilder;
    :cond_216
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_246

    .line 1567
    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_220
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_246

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Landroid/content/ContentValues;

    .line 1568
    .restart local v35       #values:Landroid/content/ContentValues;
    const-string v3, "circles"

    const-string v4, "circle_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "circle_id"

    move-object/from16 v0, v35

    invoke-virtual {v0, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, v35

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1571
    add-int/lit8 v15, v15, 0x1

    goto :goto_220

    .line 1576
    .end local v35           #values:Landroid/content/ContentValues;
    :cond_246
    invoke-virtual {v13}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2f6

    .line 1577
    new-instance v35, Landroid/content/ContentValues;

    invoke-direct/range {v35 .. v35}, Landroid/content/ContentValues;-><init>()V

    .line 1578
    .restart local v35       #values:Landroid/content/ContentValues;
    invoke-virtual {v13}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :goto_259
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2f6

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/DataCircleData;

    .line 1579
    .restart local v11       #circle:Lcom/google/api/services/plusi/model/DataCircleData;
    invoke-virtual/range {v35 .. v35}, Landroid/content/ContentValues;->clear()V

    .line 1580
    const-string v3, "circle_id"

    iget-object v4, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataCircleId;->focusId:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    const-string v3, "type"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1582
    const-string v3, "circle_name"

    iget-object v4, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataCircleProperties;->name:Ljava/lang/String;

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1583
    const-string v3, "sort_key"

    iget-object v4, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataCircleProperties;->nameSortKey:Ljava/lang/String;

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1584
    const-string v4, "contact_count"

    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleProperties;->memberCount:Ljava/lang/Integer;

    if-eqz v3, :cond_2ef

    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleProperties;->memberCount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :goto_2a9
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1587
    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleProperties;->forSharing:Ljava/lang/Boolean;

    if-eqz v3, :cond_2f1

    iget-object v3, v11, Lcom/google/api/services/plusi/model/DataCircleData;->circleProperties:Lcom/google/api/services/plusi/model/DataCircleProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleProperties;->forSharing:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2f1

    const/16 v18, 0x1

    .line 1589
    .restart local v18       #forSharing:Z
    :goto_2c4
    const-string v4, "semantic_hints"

    if-eqz v18, :cond_2f4

    const/16 v3, 0x40

    :goto_2ca
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1591
    const-string v3, "show_order"

    const/4 v4, 0x1

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->getDefaultShowOrder(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1593
    const-string v3, "circles"

    const/4 v4, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1594
    add-int/lit8 v15, v15, 0x1

    .line 1595
    goto/16 :goto_259

    .line 1584
    .end local v18           #forSharing:Z
    :cond_2ef
    const/4 v3, 0x0

    goto :goto_2a9

    .line 1587
    :cond_2f1
    const/16 v18, 0x0

    goto :goto_2c4

    .line 1589
    .restart local v18       #forSharing:Z
    :cond_2f4
    const/4 v3, 0x0

    goto :goto_2ca

    .line 1598
    .end local v11           #circle:Lcom/google/api/services/plusi/model/DataCircleData;
    .end local v18           #forSharing:Z
    .end local v35           #values:Landroid/content/ContentValues;
    :cond_2f6
    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_390

    .line 1599
    new-instance v35, Landroid/content/ContentValues;

    invoke-direct/range {v35 .. v35}, Landroid/content/ContentValues;-><init>()V

    .line 1600
    .restart local v35       #values:Landroid/content/ContentValues;
    invoke-virtual/range {v20 .. v20}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_309
    :goto_309
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_390

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/api/services/plusi/model/DataSystemGroup;

    .line 1601
    .restart local v19       #group:Lcom/google/api/services/plusi/model/DataSystemGroup;
    invoke-virtual/range {v35 .. v35}, Landroid/content/ContentValues;->clear()V

    .line 1602
    const-string v4, "circle_id"

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->id:Ljava/lang/String;

    if-nez v3, :cond_37f

    const-string v3, "0"

    :goto_322
    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1603
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->type:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleType(Ljava/lang/String;)I

    move-result v33

    .line 1606
    .local v33, type:I
    const/16 v3, 0x64

    move/from16 v0, v33

    if-eq v0, v3, :cond_309

    .line 1607
    const-string v3, "type"

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1611
    const/16 v3, 0x9

    move/from16 v0, v33

    if-ne v0, v3, :cond_384

    .line 1612
    const-string v3, "circle_name"

    const v4, 0x7f08021a

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1618
    :goto_356
    const-string v3, "semantic_hints"

    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/content/EsPeopleData;->getSemanticHints(Lcom/google/api/services/plusi/model/DataSystemGroup;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1619
    const-string v3, "show_order"

    invoke-static/range {v33 .. v33}, Lcom/google/android/apps/plus/content/EsPeopleData;->getDefaultShowOrder(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1620
    const-string v3, "circles"

    const/4 v4, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1621
    add-int/lit8 v15, v15, 0x1

    .line 1622
    goto :goto_309

    .line 1602
    .end local v33           #type:I
    :cond_37f
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->id:Ljava/lang/String;

    goto :goto_322

    .line 1615
    .restart local v33       #type:I
    :cond_384
    const-string v3, "circle_name"

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataSystemGroup;->name:Ljava/lang/String;

    move-object/from16 v0, v35

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_356

    .line 1626
    .end local v19           #group:Lcom/google/api/services/plusi/model/DataSystemGroup;
    .end local v33           #type:I
    .end local v35           #values:Landroid/content/ContentValues;
    :cond_390
    if-nez v21, :cond_397

    .line 1627
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsProvider;->insertVirtualCircles(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1630
    :cond_397
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_39a
    .catchall {:try_start_1b9 .. :try_end_39a} :catchall_b9

    .line 1632
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1635
    if-eqz v15, :cond_9

    .line 1636
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1637
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    goto/16 :goto_9
.end method

.method public static insertNewCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "circleId"
    .parameter "circleName"
    .parameter "sortKey"
    .parameter "justFollowing"

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1656
    sget-object v3, Lcom/google/android/apps/plus/content/EsPeopleData;->sCircleSyncLock:Ljava/lang/Object;

    monitor-enter v3

    .line 1657
    :try_start_5
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1659
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1660
    .local v1, values:Landroid/content/ContentValues;
    const-string v4, "circle_id"

    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1661
    const-string v4, "circle_name"

    invoke-virtual {v1, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1662
    const-string v4, "sort_key"

    invoke-virtual {v1, v4, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1663
    const-string v4, "type"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1664
    const-string v4, "contact_count"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1665
    const-string v4, "semantic_hints"

    if-eqz p5, :cond_67

    :goto_3d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1667
    const-string v2, "show_order"

    const/4 v4, 0x1

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->getDefaultShowOrder(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1669
    const-string v2, "circles"

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-virtual {v0, v2, v4, v1, v5}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1671
    monitor-exit v3
    :try_end_5a
    .catchall {:try_start_5 .. :try_end_5a} :catchall_6a

    .line 1673
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1674
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    .line 1675
    return-void

    .line 1665
    :cond_67
    const/16 v2, 0x40

    goto :goto_3d

    .line 1671
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v1           #values:Landroid/content/ContentValues;
    :catchall_6a
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static insertProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)V
    .registers 13
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .parameter "profile"

    .prologue
    const/4 v6, 0x0

    .line 2713
    const-string v5, "EsPeopleData"

    const/4 v7, 0x3

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2e

    .line 2714
    const-string v7, "EsPeopleData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Profile for "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    if-eqz p3, :cond_87

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_23
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v7, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2718
    :cond_2e
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "g:"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2719
    .local v3, personId:Ljava/lang/String;
    const/4 v1, 0x0

    .line 2720
    .local v1, changed:Z
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2723
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2725
    :try_start_49
    invoke-static {v2, v3, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceProfileProtoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    move-result v1

    .line 2726
    if-eqz v1, :cond_5e

    .line 2727
    iget-object v5, p3, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonConfig;->socialGraphData:Lcom/google/api/services/plusi/model/SocialGraphData;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/SocialGraphData;->blocked:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2728
    .local v0, blocked:Z
    iget-object v5, p3, Lcom/google/api/services/plusi/model/SimpleProfile;->displayName:Ljava/lang/String;

    invoke-static {v2, v3, v5, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->setPersonBlockedInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2735
    .end local v0           #blocked:Z
    :cond_5e
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_61
    .catchall {:try_start_49 .. :try_end_61} :catchall_89

    .line 2737
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2740
    if-eqz v1, :cond_86

    .line 2741
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 2742
    .local v4, resolver:Landroid/content/ContentResolver;
    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

    invoke-static {v5, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2744
    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2746
    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v5

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/service/ImageCache;->notifyAvatarChange(Ljava/lang/String;)V

    .line 2748
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    .line 2750
    .end local v4           #resolver:Landroid/content/ContentResolver;
    :cond_86
    return-void

    .end local v1           #changed:Z
    .end local v2           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v3           #personId:Ljava/lang/String;
    :cond_87
    move-object v5, v6

    .line 2714
    goto :goto_23

    .line 2737
    .restart local v1       #changed:Z
    .restart local v2       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v3       #personId:Ljava/lang/String;
    :catchall_89
    move-exception v5

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5
.end method

.method public static insertSelf(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 7
    .parameter "context"
    .parameter "account"
    .parameter "photoUrl"

    .prologue
    .line 1847
    const-string v1, "EsPeopleData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_21

    .line 1848
    const-string v1, "EsPeopleData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>> insertSelf: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1851
    :cond_21
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1854
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_30

    .line 1855
    const/4 p2, 0x0

    .line 1858
    :cond_30
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1860
    :try_start_33
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2, p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1863
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_41
    .catchall {:try_start_33 .. :try_end_41} :catchall_45

    .line 1865
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1866
    return-void

    .line 1865
    :catchall_45
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public static insertSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedPerson;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, suggestions:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataSuggestedPerson;>;"
    const/4 v5, 0x0

    .line 2067
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2069
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2071
    :try_start_c
    const-string v2, "#"

    const/4 v3, 0x0

    const-string v4, "0"

    invoke-static {v0, v2, v3, v4, p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceSuggestionsInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 2074
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2075
    .local v1, values:Landroid/content/ContentValues;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 2076
    const-string v2, "suggested_people_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2078
    const-string v2, "account_status"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2080
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_33
    .catchall {:try_start_c .. :try_end_33} :catchall_40

    .line 2082
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2085
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->SUGGESTED_PEOPLE_URI:Landroid/net/Uri;

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 2086
    return-void

    .line 2082
    .end local v1           #values:Landroid/content/ContentValues;
    :catchall_40
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method private static isContactModified(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z
    .registers 15
    .parameter "db"
    .parameter "personId"
    .parameter "lastUpdatedTime"

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2427
    const-string v1, "contacts"

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "last_updated_time"

    aput-object v0, v2, v10

    const-string v3, "person_id=?"

    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v10

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2432
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_18
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 2433
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_22
    .catchall {:try_start_18 .. :try_end_22} :catchall_33

    move-result-wide v0

    cmp-long v0, v0, p2

    if-eqz v0, :cond_2c

    move v0, v9

    .line 2436
    :goto_28
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2439
    :goto_2b
    return v0

    :cond_2c
    move v0, v10

    .line 2433
    goto :goto_28

    .line 2436
    :cond_2e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 2439
    goto :goto_2b

    .line 2436
    :catchall_33
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static isInMyCircles(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .registers 8
    .parameter "db"
    .parameter "personId"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3243
    :try_start_2
    const-string v2, "SELECT in_my_circles FROM contacts WHERE person_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {p0, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_d
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_2 .. :try_end_d} :catch_17

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_15

    .line 3249
    :goto_14
    return v0

    :cond_15
    move v0, v1

    .line 3243
    goto :goto_14

    .line 3249
    :catch_17
    move-exception v0

    move v0, v1

    goto :goto_14
.end method

.method public static isPersonInList(Lcom/google/android/apps/plus/content/PersonData;Ljava/util/List;)Z
    .registers 5
    .parameter "person"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/content/PersonData;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3921
    .local p1, personList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/PersonData;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/PersonData;

    .line 3922
    .local v1, iter:Lcom/google/android/apps/plus/content/PersonData;
    invoke-static {v1, p0}, Lcom/google/android/apps/plus/content/EsPeopleData;->isSamePerson(Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 3923
    const/4 v2, 0x1

    .line 3926
    .end local v1           #iter:Lcom/google/android/apps/plus/content/PersonData;
    :goto_17
    return v2

    :cond_18
    const/4 v2, 0x0

    goto :goto_17
.end method

.method public static isSamePerson(Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z
    .registers 5
    .parameter "person1"
    .parameter "person2"

    .prologue
    const/4 v0, 0x1

    .line 3935
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_20

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_20

    .line 3936
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 3947
    :cond_1f
    :goto_1f
    return v0

    .line 3941
    :cond_20
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3e

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3e

    .line 3942
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1f

    .line 3947
    :cond_3e
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method private static loadProfileAndContactDataFromDatabase(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V
    .registers 15
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "data"

    .prologue
    .line 3069
    const/4 v1, 0x0

    iput v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    .line 3071
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3074
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "contacts LEFT OUTER JOIN profiles ON (contacts.person_id=profiles.profile_person_id) LEFT OUTER JOIN circle_contact ON ( contacts.person_id = circle_contact.link_person_id)"

    sget-object v2, Lcom/google/android/apps/plus/content/EsPeopleData;->PROFILE_COLUMNS:[Ljava/lang/String;

    const-string v3, "profiles.profile_person_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 3078
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_1e
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_79

    .line 3079
    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->gaiaId:Ljava/lang/String;

    .line 3080
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    .line 3081
    const/4 v1, 0x3

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_7d

    const/4 v1, 0x1

    :goto_39
    iput-boolean v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->blocked:Z

    .line 3082
    const/4 v1, 0x2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->packedCircleIds:Ljava/lang/String;

    .line 3083
    const/4 v1, 0x4

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->lastUpdatedTime:J

    .line 3084
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->displayName:Ljava/lang/String;

    .line 3085
    const/4 v1, 0x5

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->contactUpdateTime:J

    .line 3086
    const/4 v1, 0x6

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    .line 3087
    .local v8, contactProto:[B
    if-eqz v8, :cond_64

    .line 3088
    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsPeopleData;->deserializeContactInfo([B)Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;

    move-result-object v1

    iput-object v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->contact:Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;

    .line 3090
    :cond_64
    const/4 v1, 0x7

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileUpdateTime:J

    .line 3091
    const/16 v1, 0x8

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    .line 3092
    .local v10, profileProto:[B
    if-eqz v10, :cond_79

    .line 3093
    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsPeopleData;->deserializeProfile([B)Lcom/google/api/services/plusi/model/SimpleProfile;

    move-result-object v1

    iput-object v1, p3, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;
    :try_end_79
    .catchall {:try_start_1e .. :try_end_79} :catchall_7f

    .line 3097
    .end local v8           #contactProto:[B
    .end local v10           #profileProto:[B
    :cond_79
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 3098
    return-void

    .line 3081
    :cond_7d
    const/4 v1, 0x0

    goto :goto_39

    .line 3097
    :catchall_7f
    move-exception v1

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private static loadProfileFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter "gaiaId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 3110
    new-instance v0, Lcom/google/android/apps/plus/api/GetProfileOperation;

    const/4 v4, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/GetProfileOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 3112
    .local v0, gpo:Lcom/google/android/apps/plus/api/GetProfileOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetProfileOperation;->start()V

    .line 3113
    const-string v1, "EsPeopleData"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/GetProfileOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    .line 3114
    return-void
.end method

.method public static modifyCircleProperties(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 12
    .parameter "context"
    .parameter "account"
    .parameter "circleId"
    .parameter "circleName"
    .parameter "justFollowing"

    .prologue
    const/4 v3, 0x0

    .line 1688
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1690
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1691
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "circle_name"

    invoke-virtual {v1, v2, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1692
    const-string v4, "semantic_hints"

    if-eqz p4, :cond_41

    move v2, v3

    :goto_18
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1695
    const-string v2, "circles"

    const-string v4, "circle_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v0, v2, v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1698
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1699
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    .line 1700
    return-void

    .line 1692
    :cond_41
    const/16 v2, 0x40

    goto :goto_18
.end method

.method private static normalizeEmailAddress(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "email"

    .prologue
    const/4 v2, 0x0

    .line 2690
    invoke-static {p0}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v1

    .line 2691
    .local v1, parsed:[Landroid/text/util/Rfc822Token;
    if-eqz v1, :cond_a

    array-length v3, v1

    if-nez v3, :cond_b

    .line 2700
    :cond_a
    :goto_a
    return-object v2

    .line 2695
    :cond_b
    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    .line 2696
    .local v0, address:Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 2700
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    goto :goto_a
.end method

.method private static postSyncPeopleRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 3
    .parameter "context"
    .parameter "account"

    .prologue
    .line 3617
    new-instance v0, Lcom/google/android/apps/plus/content/EsPeopleData$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData$1;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->postOnServiceThread(Ljava/lang/Runnable;)V

    .line 3627
    return-void
.end method

.method private static queryAllPersonIds(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashSet;
    .registers 11
    .parameter "db"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1427
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 1428
    .local v9, personIds:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v1, "contacts"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "person_id"

    aput-object v0, v2, v3

    const-string v3, "in_my_circles!=0"

    move-object v0, p0

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1433
    .local v8, cursor:Landroid/database/Cursor;
    :goto_1a
    :try_start_1a
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 1434
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_28
    .catchall {:try_start_1a .. :try_end_28} :catchall_29

    goto :goto_1a

    .line 1437
    :catchall_29
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1440
    return-object v9
.end method

.method private static queryBlockedPeopleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 968
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 971
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_8
    const-string v1, "SELECT blocked_people_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_8 .. :try_end_e} :catch_10

    move-result-wide v1

    .line 975
    :goto_f
    return-wide v1

    :catch_10
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_f
.end method

.method private static queryCircleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 639
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 642
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_8
    const-string v1, "SELECT circle_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_8 .. :try_end_e} :catch_10

    move-result-wide v1

    .line 646
    :goto_f
    return-wide v1

    :catch_10
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_f
.end method

.method private static queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)I
    .registers 5
    .parameter "db"
    .parameter "table"
    .parameter "where"

    .prologue
    .line 3382
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT COUNT(*) FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method private static queryPeopleLastUpdateToken(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;
    .registers 6
    .parameter "context"
    .parameter "account"

    .prologue
    const/4 v1, 0x0

    .line 1214
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1217
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_9
    const-string v2, "SELECT people_last_update_token  FROM account_status"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_f
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_9 .. :try_end_f} :catch_11

    move-result-object v1

    .line 1221
    :goto_10
    return-object v1

    :catch_11
    move-exception v2

    goto :goto_10
.end method

.method private static queryPeopleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 1196
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1199
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_8
    const-string v1, "SELECT people_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_8 .. :try_end_e} :catch_10

    move-result-wide v1

    .line 1203
    :goto_f
    return-wide v1

    :catch_10
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_f
.end method

.method private static querySuggestedCelebritiesSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 885
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 888
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_8
    const-string v1, "SELECT suggested_celeb_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_8 .. :try_end_e} :catch_10

    move-result-wide v1

    .line 892
    :goto_f
    return-wide v1

    :catch_10
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_f
.end method

.method private static querySuggestedPeopleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 867
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 870
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_8
    const-string v1, "SELECT suggested_people_sync_time  FROM account_status"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_e
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_8 .. :try_end_e} :catch_10

    move-result-wide v1

    .line 874
    :goto_f
    return-wide v1

    :catch_10
    move-exception v1

    const-wide/16 v1, -0x1

    goto :goto_f
.end method

.method public static refreshProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3048
    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3049
    .local v0, gaiaId:Ljava/lang/String;
    if-nez v0, :cond_7

    .line 3058
    :goto_6
    return-void

    .line 3053
    :cond_7
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 3054
    new-instance v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncMyProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    goto :goto_6

    .line 3056
    :cond_17
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->loadProfileFromServer(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_6
.end method

.method private static removeContactInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z
    .registers 9
    .parameter "db"
    .parameter "personId"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2383
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2384
    .local v2, values:Landroid/content/ContentValues;
    const-string v5, "in_my_circles"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2385
    new-array v0, v3, [Ljava/lang/String;

    aput-object p1, v0, v4

    .line 2386
    .local v0, args:[Ljava/lang/String;
    const-string v5, "contacts"

    const-string v6, "person_id=?"

    invoke-virtual {p0, v5, v2, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 2388
    .local v1, count:I
    if-eqz v1, :cond_25

    .line 2389
    const-string v5, "circle_contact"

    const-string v6, "link_person_id=?"

    invoke-virtual {p0, v5, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2391
    :cond_25
    if-lez v1, :cond_28

    :goto_27
    return v3

    :cond_28
    move v3, v4

    goto :goto_27
.end method

.method private static removeContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Z
    .registers 8
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2365
    .local p2, personIds:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 2366
    .local v3, removed:Z
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2369
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2371
    :try_start_c
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2372
    .local v2, personId:Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->removeContactInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v4

    or-int/2addr v3, v4

    goto :goto_10

    .line 2374
    .end local v2           #personId:Ljava/lang/String;
    :cond_22
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_25
    .catchall {:try_start_c .. :try_end_25} :catchall_29

    .line 2376
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2379
    return v3

    .line 2376
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_29
    move-exception v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method public static removeDeletedCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)V
    .registers 10
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1738
    .local p2, circleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_9

    .line 1759
    :cond_8
    :goto_8
    return-void

    .line 1742
    :cond_9
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1744
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1745
    .local v3, sb:Ljava/lang/StringBuilder;
    const-string v4, "circle_id IN ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1746
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1747
    .local v2, numCircles:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_20
    if-ge v1, v2, :cond_31

    .line 1748
    if-lez v1, :cond_29

    .line 1749
    const/16 v4, 0x2c

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1751
    :cond_29
    const/16 v4, 0x3f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1747
    add-int/lit8 v1, v1, 0x1

    goto :goto_20

    .line 1753
    :cond_31
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1755
    const-string v5, "circles"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v0, v5, v6, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1757
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1758
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    goto :goto_8
.end method

.method private static replaceCircleMembershipInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCirclePerson;Z)V
    .registers 16
    .parameter "db"
    .parameter "personId"
    .parameter "person"
    .parameter "newContact"

    .prologue
    const/4 v11, 0x0

    .line 2591
    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleCount(Lcom/google/api/services/plusi/model/DataCirclePerson;)I

    move-result v1

    .line 2593
    .local v1, circleIdCount:I
    if-eqz v1, :cond_8f

    .line 2594
    add-int/lit8 v8, v1, 0x1

    new-array v0, v8, [Ljava/lang/String;

    .line 2595
    .local v0, args:[Ljava/lang/String;
    aput-object p1, v0, v11

    .line 2596
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2597
    .local v6, sb:Ljava/lang/StringBuilder;
    iget-object v8, p2, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v7

    .line 2598
    .local v7, size:I
    const/4 v3, 0x1

    .line 2599
    .local v3, index:I
    const/4 v2, 0x0

    .local v2, i:I
    move v4, v3

    .end local v3           #index:I
    .local v4, index:I
    :goto_1b
    if-ge v2, v7, :cond_46

    .line 2600
    iget-object v8, p2, Lcom/google/api/services/plusi/model/DataCirclePerson;->membership:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/api/services/plusi/model/DataMembership;

    .line 2601
    .local v5, membership:Lcom/google/api/services/plusi/model/DataMembership;
    iget-object v8, v5, Lcom/google/api/services/plusi/model/DataMembership;->deleted:Ljava/lang/Boolean;

    if-eqz v8, :cond_31

    iget-object v8, v5, Lcom/google/api/services/plusi/model/DataMembership;->deleted:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_9e

    .line 2602
    :cond_31
    const-string v8, "?,"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2603
    add-int/lit8 v3, v4, 0x1

    .end local v4           #index:I
    .restart local v3       #index:I
    iget-object v8, v5, Lcom/google/api/services/plusi/model/DataMembership;->circleId:Lcom/google/api/services/plusi/model/DataCircleId;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataCircleId;->focusId:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v0, v4

    .line 2599
    :goto_42
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    .end local v3           #index:I
    .restart local v4       #index:I
    goto :goto_1b

    .line 2606
    .end local v5           #membership:Lcom/google/api/services/plusi/model/DataMembership;
    :cond_46
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2608
    if-nez p3, :cond_6d

    .line 2610
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "DELETE FROM circle_contact WHERE link_person_id=? AND link_circle_id NOT IN ("

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2618
    :cond_6d
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "INSERT OR IGNORE INTO circle_contact(link_person_id,link_circle_id) SELECT ?, circle_id FROM circles WHERE circle_id IN ("

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2629
    .end local v0           #args:[Ljava/lang/String;
    .end local v2           #i:I
    .end local v4           #index:I
    .end local v6           #sb:Ljava/lang/StringBuilder;
    .end local v7           #size:I
    :cond_89
    :goto_89
    const-string v8, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=circle_id) WHERE type=1"

    invoke-virtual {p0, v8}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2630
    return-void

    .line 2624
    :cond_8f
    if-nez p3, :cond_89

    .line 2625
    const-string v8, "circle_contact"

    const-string v9, "link_person_id=?"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    aput-object p1, v10, v11

    invoke-virtual {p0, v8, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_89

    .restart local v0       #args:[Ljava/lang/String;
    .restart local v2       #i:I
    .restart local v4       #index:I
    .restart local v5       #membership:Lcom/google/api/services/plusi/model/DataMembership;
    .restart local v6       #sb:Ljava/lang/StringBuilder;
    .restart local v7       #size:I
    :cond_9e
    move v3, v4

    .end local v4           #index:I
    .restart local v3       #index:I
    goto :goto_42
.end method

.method private static replaceCirclePersonInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCirclePerson;)V
    .registers 14
    .parameter "db"
    .parameter "personId"
    .parameter "person"

    .prologue
    const/4 v10, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2413
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2414
    .local v1, searchKeys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;>;"
    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleCount(Lcom/google/api/services/plusi/model/DataCirclePerson;)I

    move-result v2

    if-eqz v2, :cond_d5

    move v2, v3

    :goto_f
    iget-object v5, p2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->displayName:Ljava/lang/String;

    if-eqz v2, :cond_23

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->collectSearchKeysForName(Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_23
    const-string v8, "name"

    invoke-virtual {v6, v8, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "sort_key"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->firstNameSortKey:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "avatar"

    iget-object v8, p2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->photoUrl:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "last_updated_time"

    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getLastUpdatedTime(Lcom/google/api/services/plusi/model/DataCirclePerson;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "in_my_circles"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "ENTITY"

    iget-object v7, p2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->profileType:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d8

    const-string v2, "profile_type"

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :goto_6a
    const-string v2, "profile_state"

    const/4 v7, 0x5

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "contacts"

    const-string v7, "person_id=?"

    new-array v8, v3, [Ljava/lang/String;

    aput-object v5, v8, v4

    invoke-virtual {p0, v2, v6, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_e2

    const-string v2, "person_id"

    invoke-virtual {v6, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "gaia_id"

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "contacts"

    invoke-virtual {p0, v2, v10, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move v0, v3

    :goto_96
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    const-string v2, "contact_update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "contact_proto"

    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->convertCirclePersonToContactInfo(Lcom/google/api/services/plusi/model/DataCirclePerson;)Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->serializeContactInfo(Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;)[B

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v2, "profiles"

    const-string v7, "profile_person_id=?"

    new-array v3, v3, [Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v6, v7, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_cb

    const-string v2, "profile_person_id"

    invoke-virtual {v6, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "profiles"

    invoke-virtual {p0, v2, v10, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2417
    .local v0, newContact:Z
    :cond_cb
    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceCircleMembershipInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCirclePerson;Z)V

    .line 2418
    invoke-static {p2, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->buildSearchKeysForEmailAddresses$154ba4cc(Lcom/google/api/services/plusi/model/DataCirclePerson;Ljava/util/ArrayList;)V

    .line 2419
    invoke-static {p0, p1, v1, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceSearchKeysInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/ArrayList;Z)V

    .line 2420
    return-void

    .end local v0           #newContact:Z
    :cond_d5
    move v2, v4

    .line 2414
    goto/16 :goto_f

    :cond_d8
    const-string v2, "profile_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v2, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_6a

    :cond_e2
    move v0, v4

    goto :goto_96
.end method

.method private static replaceCirclePersons$7c3ddbf6(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;II)I
    .registers 33
    .parameter "context"
    .parameter "db"
    .parameter
    .parameter "fromIndex"
    .parameter "toIndex"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCirclePerson;",
            ">;II)I"
        }
    .end annotation

    .prologue
    .line 2293
    .local p2, persons:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataCirclePerson;>;"
    const/16 v27, 0x0

    .line 2294
    .local v27, updateCount:I
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 2295
    .local v12, avatarsToRefresh:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2297
    :try_start_a
    new-instance v23, Ljava/util/HashMap;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V

    .line 2301
    .local v23, personIdToTimestampMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    .line 2302
    .local v24, sb:Ljava/lang/StringBuilder;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2303
    .local v11, args:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "person_id IN ("

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2304
    move/from16 v16, p3

    .local v16, i:I
    :goto_22
    move/from16 v0, v16

    move/from16 v1, p4

    if-ge v0, v1, :cond_45

    .line 2305
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/api/services/plusi/model/DataCirclePerson;

    .line 2306
    .local v13, contact:Lcom/google/api/services/plusi/model/DataCirclePerson;
    const-string v3, "?,"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2307
    iget-object v3, v13, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2304
    add-int/lit8 v16, v16, 0x1

    goto :goto_22

    .line 2309
    .end local v13           #contact:Lcom/google/api/services/plusi/model/DataCirclePerson;
    :cond_45
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2310
    const-string v3, ")"

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2312
    const-string v4, "contacts"

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "person_id"

    aput-object v6, v5, v3

    const/4 v3, 0x1

    const-string v6, "last_updated_time"

    aput-object v6, v5, v3

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_7b
    .catchall {:try_start_a .. :try_end_7b} :catchall_9d

    move-result-object v14

    .line 2317
    .local v14, cursor:Landroid/database/Cursor;
    :goto_7c
    :try_start_7c
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_a2

    .line 2318
    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 2319
    .local v22, personId:Ljava/lang/String;
    const/4 v3, 0x1

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v25

    .line 2320
    .local v25, timestamp:J
    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_97
    .catchall {:try_start_7c .. :try_end_97} :catchall_98

    goto :goto_7c

    .line 2323
    .end local v22           #personId:Ljava/lang/String;
    .end local v25           #timestamp:J
    :catchall_98
    move-exception v3

    :try_start_99
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_9d
    .catchall {:try_start_99 .. :try_end_9d} :catchall_9d

    .line 2346
    .end local v11           #args:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v14           #cursor:Landroid/database/Cursor;
    .end local v16           #i:I
    .end local v23           #personIdToTimestampMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v24           #sb:Ljava/lang/StringBuilder;
    :catchall_9d
    move-exception v3

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3

    .line 2323
    .restart local v11       #args:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v14       #cursor:Landroid/database/Cursor;
    .restart local v16       #i:I
    .restart local v23       #personIdToTimestampMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Long;>;"
    .restart local v24       #sb:Ljava/lang/StringBuilder;
    :cond_a2
    :try_start_a2
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 2327
    move/from16 v16, p3

    :goto_a7
    move/from16 v0, v16

    move/from16 v1, p4

    if-ge v0, v1, :cond_fc

    .line 2328
    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/api/services/plusi/model/DataCirclePerson;

    .line 2329
    .local v21, person:Lcom/google/api/services/plusi/model/DataCirclePerson;
    invoke-static/range {v21 .. v21}, Lcom/google/android/apps/plus/content/EsPeopleData;->getLastUpdatedTime(Lcom/google/api/services/plusi/model/DataCirclePerson;)J

    move-result-wide v19

    .line 2330
    .local v19, lastUpdatedTime:J
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v22

    .line 2331
    .restart local v22       #personId:Ljava/lang/String;
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    .line 2332
    .local v18, lastSyncedTime:Ljava/lang/Long;
    if-eqz v18, :cond_d7

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v19

    if-gez v3, :cond_f7

    .line 2333
    :cond_d7
    add-int/lit8 v27, v27, 0x1

    .line 2334
    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move-object/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceCirclePersonInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCirclePerson;)V

    .line 2335
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    if-eqz v3, :cond_fa

    iget-object v4, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_fa

    iget-object v15, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    .line 2336
    .local v15, gaiaId:Ljava/lang/String;
    :goto_f2
    if-eqz v15, :cond_f7

    .line 2337
    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2327
    .end local v15           #gaiaId:Ljava/lang/String;
    :cond_f7
    add-int/lit8 v16, v16, 0x1

    goto :goto_a7

    .line 2335
    :cond_fa
    const/4 v15, 0x0

    goto :goto_f2

    .line 2342
    .end local v18           #lastSyncedTime:Ljava/lang/Long;
    .end local v19           #lastUpdatedTime:J
    .end local v21           #person:Lcom/google/api/services/plusi/model/DataCirclePerson;
    .end local v22           #personId:Ljava/lang/String;
    :cond_fc
    if-lez v27, :cond_101

    .line 2343
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_101
    .catchall {:try_start_a2 .. :try_end_101} :catchall_9d

    .line 2346
    :cond_101
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2349
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, i$:Ljava/util/Iterator;
    :goto_108
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11c

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 2350
    .restart local v15       #gaiaId:Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v3

    invoke-virtual {v3, v15}, Lcom/google/android/apps/plus/service/ImageCache;->notifyAvatarChange(Ljava/lang/String;)V

    goto :goto_108

    .line 2353
    .end local v15           #gaiaId:Ljava/lang/String;
    :cond_11c
    return v27
.end method

.method private static replaceProfileInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)V
    .registers 8
    .parameter "db"
    .parameter "personId"
    .parameter "profile"

    .prologue
    .line 2901
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2902
    .local v0, values:Landroid/content/ContentValues;
    const-string v1, "profile_proto"

    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->serializeProfile(Lcom/google/api/services/plusi/model/SimpleProfile;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2903
    const-string v1, "profiles"

    const-string v2, "profile_person_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {p0, v1, v0, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2905
    return-void
.end method

.method private static replaceProfileProtoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)Z
    .registers 26
    .parameter "db"
    .parameter "personId"
    .parameter "profile"

    .prologue
    .line 2761
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    if-eqz v2, :cond_144

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/CommonConfig;->profileState:Lcom/google/api/services/plusi/model/ProfileState;

    if-eqz v2, :cond_144

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->config:Lcom/google/api/services/plusi/model/CommonConfig;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/CommonConfig;->profileState:Lcom/google/api/services/plusi/model/ProfileState;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/ProfileState;->value:Ljava/lang/String;

    const-string v3, "ENABLED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_120

    const/16 v20, 0x2

    .line 2762
    .local v20, profileState:I
    :goto_20
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->displayName:Ljava/lang/String;

    .line 2763
    .local v12, name:Ljava/lang/String;
    const-string v2, "PLUSPAGE"

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->profileType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_148

    const/16 v21, 0x2

    .line 2766
    .local v21, profileType:I
    :goto_32
    const/4 v13, 0x1

    .line 2767
    .local v13, newContact:Z
    const/16 v18, -0x1

    .line 2768
    .local v18, oldProfileState:I
    const/16 v19, -0x1

    .line 2769
    .local v19, oldProfileType:I
    const/16 v16, 0x0

    .line 2771
    .local v16, oldName:Ljava/lang/String;
    const-string v3, "contacts"

    sget-object v4, Lcom/google/android/apps/plus/content/EsPeopleData;->CONTACT_PROJECTION:[Ljava/lang/String;

    const-string v5, "person_id=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 2774
    .local v11, cursor:Landroid/database/Cursor;
    :try_start_4e
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_64

    .line 2775
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 2776
    const/4 v2, 0x1

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 2777
    const/4 v2, 0x2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_62
    .catchall {:try_start_4e .. :try_end_62} :catchall_14c

    move-result v19

    .line 2778
    const/4 v13, 0x0

    .line 2781
    :cond_64
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2784
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 2786
    .local v22, values:Landroid/content/ContentValues;
    const/4 v10, 0x0

    .line 2787
    .local v10, changed:Z
    move/from16 v0, v18

    move/from16 v1, v20

    if-ne v0, v1, :cond_81

    move-object/from16 v0, v16

    invoke-static {v0, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_81

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_bf

    .line 2789
    :cond_81
    const/4 v10, 0x1

    .line 2790
    const-string v2, "name"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2791
    const-string v2, "profile_state"

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2792
    const-string v2, "profile_type"

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2794
    if-eqz v13, :cond_151

    .line 2795
    const-string v2, "person_id"

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2796
    const-string v2, "gaia_id"

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2798
    const-string v2, "contacts"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2805
    :cond_bf
    :goto_bf
    const/4 v14, 0x1

    .line 2806
    .local v14, newProfile:Z
    const/16 v17, 0x0

    .line 2807
    .local v17, oldProfileProto:[B
    const-string v3, "profiles"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "profile_proto"

    aput-object v5, v4, v2

    const-string v5, "profile_person_id=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 2811
    :try_start_dd
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_e9

    .line 2812
    const/4 v14, 0x0

    .line 2813
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_e8
    .catchall {:try_start_dd .. :try_end_e8} :catchall_164

    move-result-object v17

    .line 2816
    :cond_e9
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2819
    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->serializeProfile(Lcom/google/api/services/plusi/model/SimpleProfile;)[B

    move-result-object v15

    .line 2821
    .local v15, newProfileProto:[B
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    .line 2822
    const-string v2, "profile_update_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2823
    if-eqz v14, :cond_169

    .line 2824
    const-string v2, "profile_person_id"

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2825
    const-string v2, "profile_proto"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2826
    const-string v2, "profiles"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2827
    const/4 v10, 0x1

    .line 2837
    :goto_11f
    return v10

    .line 2761
    .end local v10           #changed:Z
    .end local v11           #cursor:Landroid/database/Cursor;
    .end local v12           #name:Ljava/lang/String;
    .end local v13           #newContact:Z
    .end local v14           #newProfile:Z
    .end local v15           #newProfileProto:[B
    .end local v16           #oldName:Ljava/lang/String;
    .end local v17           #oldProfileProto:[B
    .end local v18           #oldProfileState:I
    .end local v19           #oldProfileType:I
    .end local v20           #profileState:I
    .end local v21           #profileType:I
    .end local v22           #values:Landroid/content/ContentValues;
    :cond_120
    const-string v3, "DISABLED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12c

    const/16 v20, 0x3

    goto/16 :goto_20

    :cond_12c
    const-string v3, "BLOCKED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_138

    const/16 v20, 0x5

    goto/16 :goto_20

    :cond_138
    const-string v3, "PRIVATE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_144

    const/16 v20, 0x4

    goto/16 :goto_20

    :cond_144
    const/16 v20, 0x0

    goto/16 :goto_20

    .line 2763
    .restart local v12       #name:Ljava/lang/String;
    .restart local v20       #profileState:I
    :cond_148
    const/16 v21, 0x1

    goto/16 :goto_32

    .line 2781
    .restart local v11       #cursor:Landroid/database/Cursor;
    .restart local v13       #newContact:Z
    .restart local v16       #oldName:Ljava/lang/String;
    .restart local v18       #oldProfileState:I
    .restart local v19       #oldProfileType:I
    .restart local v21       #profileType:I
    :catchall_14c
    move-exception v2

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v2

    .line 2800
    .restart local v10       #changed:Z
    .restart local v22       #values:Landroid/content/ContentValues;
    :cond_151
    const-string v2, "contacts"

    const-string v3, "person_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_bf

    .line 2816
    .restart local v14       #newProfile:Z
    .restart local v17       #oldProfileProto:[B
    :catchall_164
    move-exception v2

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v2

    .line 2829
    .restart local v15       #newProfileProto:[B
    :cond_169
    move-object/from16 v0, v17

    invoke-static {v0, v15}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_179

    .line 2830
    const-string v2, "profile_proto"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2831
    const/4 v10, 0x1

    .line 2833
    :cond_179
    const-string v2, "profiles"

    const-string v3, "profile_person_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_11f
.end method

.method private static replaceSearchKeysInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .registers 11
    .parameter "db"
    .parameter "personId"
    .parameter
    .parameter "newContact"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2672
    .local p2, searchKeys:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;>;"
    if-nez p3, :cond_f

    .line 2673
    const-string v3, "contact_search"

    const-string v4, "search_person_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {p0, v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2677
    :cond_f
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2678
    .local v2, values:Landroid/content/ContentValues;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_18
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_42

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;

    .line 2679
    .local v1, searchKey:Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;
    const-string v3, "search_person_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2680
    const-string v3, "search_key_type"

    iget v4, v1, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;->keyType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2681
    const-string v3, "search_key"

    iget-object v4, v1, Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;->key:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2682
    const-string v3, "contact_search"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_18

    .line 2684
    .end local v1           #searchKey:Lcom/google/android/apps/plus/content/EsPeopleData$SearchKey;
    :cond_42
    return-void
.end method

.method private static replaceSuggestionsInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .registers 35
    .parameter "db"
    .parameter "category"
    .parameter "categoryLabel"
    .parameter "categorySortKey"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedPerson;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2091
    .local p4, suggestions:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataSuggestedPerson;>;"
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 2092
    .local v11, currentSuggestedPeople:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 2094
    .local v18, markedAsDeleted:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const-string v3, "suggested_people"

    sget-object v4, Lcom/google/android/apps/plus/content/EsPeopleData;->SUGGESTED_PEOPLE_COLUMNS:[Ljava/lang/String;

    const-string v5, "category=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 2098
    .local v12, cursor:Landroid/database/Cursor;
    :goto_1f
    :try_start_1f
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4e

    .line 2099
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 2100
    .local v19, personId:Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_42

    const/4 v13, 0x1

    .line 2101
    .local v13, deleted:Z
    :goto_32
    const/4 v2, 0x2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    .line 2102
    .local v25, sortOrder:I
    if-eqz v13, :cond_44

    .line 2108
    invoke-virtual/range {v18 .. v19}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_3c
    .catchall {:try_start_1f .. :try_end_3c} :catchall_3d

    goto :goto_1f

    .line 2114
    .end local v13           #deleted:Z
    .end local v19           #personId:Ljava/lang/String;
    .end local v25           #sortOrder:I
    :catchall_3d
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2

    .line 2100
    .restart local v19       #personId:Ljava/lang/String;
    :cond_42
    const/4 v13, 0x0

    goto :goto_32

    .line 2110
    .restart local v13       #deleted:Z
    .restart local v25       #sortOrder:I
    :cond_44
    :try_start_44
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4d
    .catchall {:try_start_44 .. :try_end_4d} :catchall_3d

    goto :goto_1f

    .line 2114
    .end local v13           #deleted:Z
    .end local v19           #personId:Ljava/lang/String;
    .end local v25           #sortOrder:I
    :cond_4e
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 2117
    if-nez p4, :cond_a7

    const/4 v10, 0x0

    .line 2118
    .local v10, count:I
    :goto_54
    invoke-virtual {v11}, Ljava/util/HashMap;->size()I

    move-result v2

    if-eq v2, v10, :cond_ac

    const/16 v28, 0x1

    .line 2119
    .local v28, updateNeeded:Z
    :goto_5c
    if-nez v28, :cond_85

    .line 2120
    const/4 v15, 0x0

    .local v15, i:I
    :goto_5f
    if-ge v15, v10, :cond_85

    .line 2121
    move-object/from16 v0, p4

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    .line 2122
    .local v26, suggestion:Lcom/google/api/services/plusi/model/DataSuggestedPerson;
    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v19

    .line 2123
    .restart local v19       #personId:Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    .line 2124
    .local v20, position:Ljava/lang/Integer;
    if-eqz v20, :cond_83

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v15, :cond_af

    .line 2125
    :cond_83
    const/16 v28, 0x1

    .line 2131
    .end local v15           #i:I
    .end local v19           #personId:Ljava/lang/String;
    .end local v20           #position:Ljava/lang/Integer;
    .end local v26           #suggestion:Lcom/google/api/services/plusi/model/DataSuggestedPerson;
    :cond_85
    new-instance v29, Landroid/content/ContentValues;

    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    .line 2132
    .local v29, values:Landroid/content/ContentValues;
    if-nez v28, :cond_b2

    .line 2133
    const-string v2, "category_sort_key"

    move-object/from16 v0, v29

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2134
    const-string v2, "suggested_people"

    const-string v3, "category=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2212
    :goto_a6
    return-void

    .line 2117
    .end local v10           #count:I
    .end local v28           #updateNeeded:Z
    .end local v29           #values:Landroid/content/ContentValues;
    :cond_a7
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v10

    goto :goto_54

    .line 2118
    .restart local v10       #count:I
    :cond_ac
    const/16 v28, 0x0

    goto :goto_5c

    .line 2120
    .restart local v15       #i:I
    .restart local v19       #personId:Ljava/lang/String;
    .restart local v20       #position:Ljava/lang/Integer;
    .restart local v26       #suggestion:Lcom/google/api/services/plusi/model/DataSuggestedPerson;
    .restart local v28       #updateNeeded:Z
    :cond_af
    add-int/lit8 v15, v15, 0x1

    goto :goto_5f

    .line 2139
    .end local v15           #i:I
    .end local v19           #personId:Ljava/lang/String;
    .end local v20           #position:Ljava/lang/Integer;
    .end local v26           #suggestion:Lcom/google/api/services/plusi/model/DataSuggestedPerson;
    .restart local v29       #values:Landroid/content/ContentValues;
    :cond_b2
    const-string v2, "suggested_people"

    const-string v3, "dismissed=0 AND category=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2143
    new-instance v17, Ljava/util/HashSet;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashSet;-><init>()V

    .line 2144
    .local v17, keepDeleted:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v15, 0x0

    .restart local v15       #i:I
    :goto_c7
    if-ge v15, v10, :cond_205

    .line 2145
    move-object/from16 v0, p4

    invoke-interface {v0, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/google/api/services/plusi/model/DataSuggestedPerson;

    .line 2146
    .restart local v26       #suggestion:Lcom/google/api/services/plusi/model/DataSuggestedPerson;
    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v0, v2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    move-object/from16 v22, v0

    .line 2147
    .local v22, properties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;
    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v19

    .line 2148
    .restart local v19       #personId:Ljava/lang/String;
    invoke-virtual/range {v18 .. v19}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f3

    .line 2152
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2144
    :goto_f0
    add-int/lit8 v15, v15, 0x1

    goto :goto_c7

    .line 2156
    :cond_f3
    invoke-virtual/range {v29 .. v29}, Landroid/content/ContentValues;->clear()V

    .line 2157
    const-string v2, "person_id"

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2158
    const-string v2, "gaia_id"

    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2159
    const-string v2, "name"

    move-object/from16 v0, v22

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->displayName:Ljava/lang/String;

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2160
    const-string v2, "avatar"

    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberProperties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->photoUrl:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2162
    const-string v2, "PLUSPAGE"

    move-object/from16 v0, v22

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->profileType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_201

    const/16 v21, 0x2

    .line 2164
    .local v21, profileType:I
    :goto_136
    const-string v2, "profile_type"

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2165
    const-string v2, "contacts"

    const/4 v3, 0x0

    const/4 v4, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 2168
    invoke-virtual/range {v29 .. v29}, Landroid/content/ContentValues;->clear()V

    .line 2169
    const-string v2, "suggested_person_id"

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2170
    const-string v2, "sort_order"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2171
    const-string v2, "category"

    move-object/from16 v0, v29

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2172
    const-string v2, "category_label"

    move-object/from16 v0, v29

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2173
    const-string v2, "category_sort_key"

    move-object/from16 v0, v29

    move-object/from16 v1, p3

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2174
    move-object/from16 v0, v26

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->explanation:Lcom/google/api/services/plusi/model/DataSugggestionExplanation;

    if-eqz v2, :cond_197

    .line 2175
    invoke-static {}, Lcom/google/api/services/plusi/model/DataSugggestionExplanationJson;->getInstance()Lcom/google/api/services/plusi/model/DataSugggestionExplanationJson;

    move-result-object v2

    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataSuggestedPerson;->explanation:Lcom/google/api/services/plusi/model/DataSugggestionExplanation;

    invoke-virtual {v2, v3}, Lcom/google/api/services/plusi/model/DataSugggestionExplanationJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v14

    .line 2177
    .local v14, explanation:[B
    const-string v2, "explanation"

    move-object/from16 v0, v29

    invoke-virtual {v0, v2, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2180
    .end local v14           #explanation:[B
    :cond_197
    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->tagLine:Ljava/lang/String;

    if-nez v2, :cond_1b5

    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    if-nez v2, :cond_1b5

    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    if-nez v2, :cond_1b5

    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->location:Ljava/lang/String;

    if-nez v2, :cond_1b5

    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->school:Ljava/lang/String;

    if-eqz v2, :cond_1f5

    .line 2183
    :cond_1b5
    new-instance v27, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    invoke-direct/range {v27 .. v27}, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;-><init>()V

    .line 2184
    .local v27, trimmedProps:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;
    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->tagLine:Ljava/lang/String;

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->tagLine:Ljava/lang/String;

    .line 2185
    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    .line 2186
    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    .line 2187
    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->location:Ljava/lang/String;

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->location:Ljava/lang/String;

    .line 2188
    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->school:Ljava/lang/String;

    move-object/from16 v0, v27

    iput-object v2, v0, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->school:Ljava/lang/String;

    .line 2190
    invoke-static {}, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesJson;->getInstance()Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesJson;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v23

    .line 2192
    .local v23, propertyBytes:[B
    const-string v2, "properties"

    move-object/from16 v0, v29

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2195
    .end local v23           #propertyBytes:[B
    .end local v27           #trimmedProps:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;
    :cond_1f5
    const-string v2, "suggested_people"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/16 :goto_f0

    .line 2162
    .end local v21           #profileType:I
    :cond_201
    const/16 v21, 0x1

    goto/16 :goto_136

    .line 2199
    .end local v19           #personId:Ljava/lang/String;
    .end local v22           #properties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;
    .end local v26           #suggestion:Lcom/google/api/services/plusi/model/DataSuggestedPerson;
    :cond_205
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    .line 2200
    .local v24, sb:Ljava/lang/StringBuilder;
    const-string v2, "dismissed!=0 AND category=?"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2202
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_24f

    .line 2203
    const-string v2, " AND suggested_person_id NOT IN ("

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2204
    invoke-virtual/range {v17 .. v17}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, i$:Ljava/util/Iterator;
    :goto_222
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_23d

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    .line 2205
    .restart local v19       #personId:Ljava/lang/String;
    move-object/from16 v0, v24

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->appendEscapedSQLString(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 2206
    const/16 v2, 0x2c

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_222

    .line 2208
    .end local v19           #personId:Ljava/lang/String;
    :cond_23d
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 2209
    const/16 v2, 0x29

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2211
    .end local v16           #i$:Ljava/util/Iterator;
    :cond_24f
    const-string v2, "suggested_people"

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_a6
.end method

.method public static replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 15
    .parameter "db"
    .parameter "gaiaId"
    .parameter "userName"
    .parameter "uncompressedPhotoUrl"

    .prologue
    .line 1879
    if-eqz p2, :cond_c

    if-eqz p1, :cond_c

    const-string v0, "0"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    .line 1880
    :cond_c
    const-string v0, "EsPeopleData"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 1881
    const-string v0, "EsPeopleData"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ">>>>> Person id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; *** Skip. No gaia id or name"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1885
    :cond_39
    const/4 v0, 0x0

    .line 1902
    :goto_3a
    return v0

    .line 1888
    :cond_3b
    const/4 v9, 0x0

    .line 1889
    .local v9, info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    const-string v1, "contacts"

    sget-object v2, Lcom/google/android/apps/plus/content/EsPeopleData;->USERS_PROJECTION:[Ljava/lang/String;

    const-string v3, "gaia_id = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1892
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_50
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_75

    .line 1893
    new-instance v10, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;

    const/4 v0, 0x0

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;-><init>(B)V
    :try_end_5c
    .catchall {:try_start_50 .. :try_end_5c} :catchall_7f

    .line 1894
    .end local v9           #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    .local v10, info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    const/4 v0, 0x1

    :try_start_5d
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->name:Ljava/lang/String;

    .line 1895
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v10, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->avatarUrl:Ljava/lang/String;

    .line 1896
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_7d

    const/4 v0, 0x1

    :goto_72
    iput-boolean v0, v10, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->inMyCircles:Z
    :try_end_74
    .catchall {:try_start_5d .. :try_end_74} :catchall_84

    move-object v9, v10

    .line 1899
    .end local v10           #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    .restart local v9       #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    :cond_75
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1902
    invoke-static {p0, p1, p2, p3, v9}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;)Z

    move-result v0

    goto :goto_3a

    .line 1896
    .end local v9           #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    .restart local v10       #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    :cond_7d
    const/4 v0, 0x0

    goto :goto_72

    .line 1899
    .end local v10           #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    .restart local v9       #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    :catchall_7f
    move-exception v0

    :goto_80
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .end local v9           #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    .restart local v10       #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    :catchall_84
    move-exception v0

    move-object v9, v10

    .end local v10           #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    .restart local v9       #info:Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;
    goto :goto_80
.end method

.method private static replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;)Z
    .registers 12
    .parameter "db"
    .parameter "gaiaId"
    .parameter "userName"
    .parameter "uncompressedPhotoUrl"
    .parameter "info"

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1907
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v6}, Landroid/content/ContentValues;-><init>(I)V

    .line 1908
    .local v1, values:Landroid/content/ContentValues;
    invoke-static {p3}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1909
    .local v0, compressedAvatarUrl:Ljava/lang/String;
    if-nez p4, :cond_5f

    .line 1910
    const-string v3, "EsPeopleData"

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_35

    .line 1911
    const-string v3, "EsPeopleData"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, ">>>>> Inserting person id: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", name: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1914
    :cond_35
    const-string v3, "person_id"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "g:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1915
    const-string v3, "gaia_id"

    invoke-virtual {v1, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1916
    const-string v3, "name"

    invoke-virtual {v1, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1917
    const-string v3, "avatar"

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1918
    const-string v3, "contacts"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1940
    :goto_5e
    return v2

    .line 1921
    :cond_5f
    iget-object v4, p4, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->name:Ljava/lang/String;

    invoke-static {p2, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_73

    iget-boolean v4, p4, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->inMyCircles:Z

    if-nez v4, :cond_b3

    iget-object v4, p4, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->avatarUrl:Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b3

    .line 1923
    :cond_73
    const-string v4, "EsPeopleData"

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_99

    .line 1924
    const-string v4, "EsPeopleData"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, ">>>>> Updating person id: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", name: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1927
    :cond_99
    const-string v4, "name"

    invoke-virtual {v1, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1933
    iget-boolean v4, p4, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->inMyCircles:Z

    if-nez v4, :cond_a7

    .line 1934
    const-string v4, "avatar"

    invoke-virtual {v1, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1936
    :cond_a7
    const-string v4, "contacts"

    const-string v5, "gaia_id = ?"

    new-array v6, v2, [Ljava/lang/String;

    aput-object p1, v6, v3

    invoke-virtual {p0, v4, v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_5e

    :cond_b3
    move v2, v3

    .line 1940
    goto :goto_5e
.end method

.method public static replaceUsersInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V
    .registers 14
    .parameter "db"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataUser;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1765
    .local p1, users:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/DataUser;>;"
    const/4 v8, 0x0

    .line 1766
    .local v8, fromIndex:I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    .line 1767
    .local v9, numUsers:I
    :goto_5
    if-ge v8, v9, :cond_118

    .line 1768
    add-int/lit8 v10, v8, 0x4b

    .line 1769
    .local v10, toIndex:I
    if-le v10, v9, :cond_c

    .line 1770
    move v10, v9

    .line 1772
    :cond_c
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const-string v0, "gaia_id IN("

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v8

    :goto_21
    if-ge v1, v10, :cond_51

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataUser;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4d

    const-string v2, "0"

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-static {v2, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4d

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataUser;->displayName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4d

    const-string v2, "?,"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_21

    :cond_51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    const-string v0, ")"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_115

    const-string v1, "contacts"

    sget-object v2, Lcom/google/android/apps/plus/content/EsPeopleData;->USERS_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :goto_7e
    :try_start_7e
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_b2

    new-instance v2, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;

    const/4 v0, 0x0

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;-><init>(B)V

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->name:Ljava/lang/String;

    const/4 v0, 0x2

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->avatarUrl:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_b0

    const/4 v0, 0x1

    :goto_a5
    iput-boolean v0, v2, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;->inMyCircles:Z

    invoke-virtual {v11, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_aa
    .catchall {:try_start_7e .. :try_end_aa} :catchall_ab

    goto :goto_7e

    :catchall_ab
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_b0
    const/4 v0, 0x0

    goto :goto_a5

    :cond_b2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .end local v8           #fromIndex:I
    :goto_b5
    if-ge v8, v10, :cond_115

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataUser;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataUser;->displayName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d5

    const-string v3, "0"

    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_109

    :cond_d5
    const-string v1, "EsPeopleData"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_106

    const-string v1, "EsPeopleData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ">>>>> Person id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataUser;->id:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataUser;->displayName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; *** Skip. No gaia id or name"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_106
    :goto_106
    add-int/lit8 v8, v8, 0x1

    goto :goto_b5

    :cond_109
    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataUser;->profilePhotoUrl:Ljava/lang/String;

    invoke-virtual {v11, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;

    invoke-static {p0, v1, v2, v3, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceUserInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsPeopleData$UserInfo;)Z

    goto :goto_106

    .line 1773
    :cond_115
    move v8, v10

    .line 1774
    .restart local v8       #fromIndex:I
    goto/16 :goto_5

    .line 1775
    .end local v10           #toIndex:I
    :cond_118
    return-void
.end method

.method private static serializeContactInfo(Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;)[B
    .registers 2
    .parameter "contactInfo"

    .prologue
    .line 2866
    if-nez p0, :cond_4

    .line 2867
    const/4 v0, 0x0

    .line 2870
    :goto_3
    return-object v0

    :cond_4
    sget-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->CONTACT_INFO_JSON:Lcom/google/android/apps/plus/json/EsJson;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/json/EsJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v0

    goto :goto_3
.end method

.method private static serializeProfile(Lcom/google/api/services/plusi/model/SimpleProfile;)[B
    .registers 2
    .parameter "profile"

    .prologue
    .line 2844
    if-nez p0, :cond_4

    .line 2845
    const/4 v0, 0x0

    .line 2848
    :goto_3
    return-object v0

    :cond_4
    invoke-static {}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->getInstance()Lcom/google/api/services/plusi/model/SimpleProfileJson;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v0

    goto :goto_3
.end method

.method public static setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCirclePerson;[Ljava/lang/String;[Ljava/lang/String;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "oldPersonId"
    .parameter "person"
    .parameter "circlesToAdd"
    .parameter "circlesToRemove"

    .prologue
    const/4 v9, 0x0

    .line 3144
    const/4 v3, 0x0

    .line 3145
    .local v3, oldInCircles:Z
    const/4 v2, 0x0

    .line 3147
    .local v2, newInCircles:Z
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3149
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3151
    if-eqz p3, :cond_6e

    :try_start_10
    iget-object v7, p3, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v4

    .line 3152
    .local v4, personId:Ljava/lang/String;
    :goto_16
    invoke-static {v4, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1f

    .line 3153
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->removeContactInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    .line 3156
    :cond_1f
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->isInMyCircles(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v3

    .line 3157
    if-nez v3, :cond_2a

    if-eqz p3, :cond_2a

    .line 3158
    invoke-static {v0, v4, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceCirclePersonInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/DataCirclePerson;)V

    .line 3161
    :cond_2a
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 3162
    .local v6, sb:Ljava/lang/StringBuilder;
    if-eqz p4, :cond_8e

    array-length v7, p4

    if-lez v7, :cond_8e

    .line 3163
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "INSERT OR IGNORE INTO circle_contact(link_person_id,link_circle_id) SELECT "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ",circle_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " FROM circles"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " WHERE circle_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " IN("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3170
    const/4 v1, 0x0

    .local v1, i:I
    :goto_63
    array-length v7, p4

    if-ge v1, v7, :cond_70

    .line 3171
    const-string v7, "?,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3170
    add-int/lit8 v1, v1, 0x1

    goto :goto_63

    .end local v1           #i:I
    .end local v4           #personId:Ljava/lang/String;
    .end local v6           #sb:Ljava/lang/StringBuilder;
    :cond_6e
    move-object v4, p2

    .line 3151
    goto :goto_16

    .line 3173
    .restart local v1       #i:I
    .restart local v4       #personId:Ljava/lang/String;
    .restart local v6       #sb:Ljava/lang/StringBuilder;
    :cond_70
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3174
    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3175
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7, p4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3176
    invoke-static {v0, p4}, Lcom/google/android/apps/plus/content/EsPeopleData;->updateMemberCountsInTransaction(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)V

    .line 3178
    if-nez v3, :cond_8d

    .line 3179
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->updateInMyCirclesFlagAndTimestampInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 3182
    :cond_8d
    const/4 v2, 0x1

    .line 3185
    .end local v1           #i:I
    :cond_8e
    if-eqz p5, :cond_e6

    array-length v7, p5

    if-lez v7, :cond_e6

    .line 3186
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3187
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "DELETE FROM circle_contact WHERE link_person_id="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AND link_circle_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " IN  ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3192
    const/4 v1, 0x0

    .restart local v1       #i:I
    :goto_ba
    array-length v7, p5

    if-ge v1, v7, :cond_c5

    .line 3193
    const-string v7, "?,"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3192
    add-int/lit8 v1, v1, 0x1

    goto :goto_ba

    .line 3195
    :cond_c5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3196
    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3197
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7, p5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3198
    invoke-static {v0, p5}, Lcom/google/android/apps/plus/content/EsPeopleData;->updateMemberCountsInTransaction(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)V

    .line 3200
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->updateInMyCirclesFlagAndTimestampInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 3202
    if-nez v2, :cond_e6

    .line 3203
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->isInMyCircles(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v2

    .line 3206
    .end local v1           #i:I
    :cond_e6
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_e9
    .catchall {:try_start_10 .. :try_end_e9} :catchall_101

    .line 3208
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3211
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    .line 3212
    .local v5, resolver:Landroid/content/ContentResolver;
    sget-object v7, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v5, v7, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3213
    sget-object v7, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    invoke-virtual {v5, v7, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3215
    if-eq v3, v2, :cond_100

    .line 3216
    const/4 v7, 0x1

    invoke-static {p0, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;Z)V

    .line 3218
    :cond_100
    return-void

    .line 3208
    .end local v4           #personId:Ljava/lang/String;
    .end local v5           #resolver:Landroid/content/ContentResolver;
    .end local v6           #sb:Ljava/lang/StringBuilder;
    :catchall_101
    move-exception v7

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v7
.end method

.method public static setPersonBlocked(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 9
    .parameter "context"
    .parameter "account"
    .parameter "personId"
    .parameter "name"
    .parameter "blocked"

    .prologue
    const/4 v3, 0x0

    .line 3309
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 3312
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 3314
    :try_start_c
    invoke-static {v0, p2, p3, p4}, Lcom/google/android/apps/plus/content/EsPeopleData;->setPersonBlockedInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 3315
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_29

    .line 3317
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 3320
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 3321
    .local v1, resolver:Landroid/content/ContentResolver;
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3323
    if-eqz p4, :cond_28

    .line 3324
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 3325
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    .line 3327
    :cond_28
    return-void

    .line 3317
    .end local v1           #resolver:Landroid/content/ContentResolver;
    :catchall_29
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method private static setPersonBlockedInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 11
    .parameter "db"
    .parameter "personId"
    .parameter "name"
    .parameter "blocked"

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 3331
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 3332
    .local v1, values:Landroid/content/ContentValues;
    const-string v2, "blocked"

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 3333
    if-eqz p3, :cond_1b

    .line 3334
    const-string v2, "in_my_circles"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 3337
    :cond_1b
    const-string v2, "contacts"

    const-string v3, "person_id=?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-virtual {p0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 3339
    .local v0, count:I
    if-nez v0, :cond_44

    if-eqz p3, :cond_44

    .line 3340
    const-string v2, "person_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3341
    const-string v2, "gaia_id"

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3342
    const-string v2, "name"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 3343
    const-string v2, "contacts"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 3346
    :cond_44
    if-eqz p3, :cond_56

    .line 3347
    const-string v2, "circle_contact"

    const-string v3, "link_person_id=?"

    new-array v4, v6, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-virtual {p0, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 3350
    const-string v2, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=circle_id) WHERE type=1"

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 3352
    :cond_56
    return-void
.end method

.method private static syncCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V
    .registers 11
    .parameter "context"
    .parameter "account"
    .parameter "requestPeopleSync"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 658
    sget-object v5, Lcom/google/android/apps/plus/content/EsPeopleData;->sCircleSyncLock:Ljava/lang/Object;

    monitor-enter v5

    .line 659
    :try_start_3
    new-instance v2, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    .line 660
    .local v2, syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    const-string v4, "Circle sync"

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    .line 661
    const/4 v4, 0x0

    invoke-static {p0, p1, v2, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->doCirclesSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)Z

    move-result v1

    .line 662
    .local v1, success:Z
    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    .line 663
    if-eqz v1, :cond_47

    .line 664
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 665
    .local v3, values:Landroid/content/ContentValues;
    const-string v4, "circle_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 666
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 668
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v4, "account_status"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v4, v3, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 670
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 672
    if-eqz p2, :cond_47

    .line 674
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->postSyncPeopleRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 677
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v3           #values:Landroid/content/ContentValues;
    :cond_47
    monitor-exit v5
    :try_end_48
    .catchall {:try_start_3 .. :try_end_48} :catchall_49

    return-void

    .end local v1           #success:Z
    .end local v2           #syncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    :catchall_49
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method public static syncMyProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 28
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1072
    sget-object v23, Lcom/google/android/apps/plus/content/EsPeopleData;->sMyProfileSyncLock:Ljava/lang/Object;

    monitor-enter v23

    .line 1075
    :try_start_3
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_67

    move-result-object v14

    .line 1079
    .local v14, db:Landroid/database/sqlite/SQLiteDatabase;
    :try_start_b
    const-string v4, "SELECT contact_update_time  FROM profiles  WHERE profile_person_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v8

    invoke-static {v14, v4, v5}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_1a
    .catchall {:try_start_b .. :try_end_1a} :catchall_67
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_b .. :try_end_1a} :catch_29

    move-result-wide v15

    .line 1088
    .local v15, lastSyncTime:J
    :goto_1b
    :try_start_1b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v15

    const-wide/32 v8, 0xea60

    cmp-long v4, v4, v8

    if-gez v4, :cond_2d

    .line 1089
    monitor-exit v23

    .line 1139
    :goto_28
    return-void

    .line 1085
    .end local v15           #lastSyncTime:J
    :catch_29
    move-exception v4

    const-wide/16 v15, 0x0

    .restart local v15       #lastSyncTime:J
    goto :goto_1b

    .line 1092
    :cond_2d
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v6

    .line 1094
    .local v6, gaiaId:Ljava/lang/String;
    const-string v4, "MyProfile"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    .line 1095
    new-instance v3, Lcom/google/android/apps/plus/api/GetContactInfoOperation;

    const/4 v7, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v8, p3

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/plus/api/GetContactInfoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 1097
    .local v3, gco:Lcom/google/android/apps/plus/api/GetContactInfoOperation;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetContactInfoOperation;->start()V

    .line 1098
    const-string v4, "EsPeopleData"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/api/GetContactInfoOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    .line 1100
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v20

    .line 1101
    .local v20, personId:Ljava/lang/String;
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/GetContactInfoOperation;->getPerson()Lcom/google/api/services/plusi/model/DataCirclePerson;

    move-result-object v19

    .line 1103
    .local v19, person:Lcom/google/api/services/plusi/model/DataCirclePerson;
    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/content/EsPeopleData;->getLastUpdatedTime(Lcom/google/api/services/plusi/model/DataCirclePerson;)J

    move-result-wide v17

    .line 1105
    .local v17, lastUpdatedTime:J
    move-object/from16 v0, v20

    move-wide/from16 v1, v17

    invoke-static {v14, v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->isContactModified(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z

    move-result v4

    if-nez v4, :cond_6a

    .line 1106
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 1107
    monitor-exit v23
    :try_end_66
    .catchall {:try_start_1b .. :try_end_66} :catchall_67

    goto :goto_28

    .line 1139
    .end local v3           #gco:Lcom/google/android/apps/plus/api/GetContactInfoOperation;
    .end local v6           #gaiaId:Ljava/lang/String;
    .end local v14           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v15           #lastSyncTime:J
    .end local v17           #lastUpdatedTime:J
    .end local v19           #person:Lcom/google/api/services/plusi/model/DataCirclePerson;
    .end local v20           #personId:Ljava/lang/String;
    :catchall_67
    move-exception v4

    monitor-exit v23

    throw v4

    .line 1110
    .restart local v3       #gco:Lcom/google/android/apps/plus/api/GetContactInfoOperation;
    .restart local v6       #gaiaId:Ljava/lang/String;
    .restart local v14       #db:Landroid/database/sqlite/SQLiteDatabase;
    .restart local v15       #lastSyncTime:J
    .restart local v17       #lastUpdatedTime:J
    .restart local v19       #person:Lcom/google/api/services/plusi/model/DataCirclePerson;
    .restart local v20       #personId:Ljava/lang/String;
    :cond_6a
    :try_start_6a
    new-instance v7, Lcom/google/android/apps/plus/api/GetProfileOperation;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object v10, v6

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/plus/api/GetProfileOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 1112
    .local v7, gpo:Lcom/google/android/apps/plus/api/GetProfileOperation;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/GetProfileOperation;->start()V

    .line 1113
    const-string v4, "EsPeopleData"

    invoke-virtual {v7, v4}, Lcom/google/android/apps/plus/api/GetProfileOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    .line 1115
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_82
    .catchall {:try_start_6a .. :try_end_82} :catchall_67

    .line 1117
    :try_start_82
    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/GetProfileOperation;->getProfile()Lcom/google/api/services/plusi/model/SimpleProfile;

    move-result-object v4

    move-object/from16 v0, v20

    invoke-static {v14, v0, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->replaceProfileProtoInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/api/services/plusi/model/SimpleProfile;)Z

    .line 1119
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 1120
    .local v22, values:Landroid/content/ContentValues;
    const-string v4, "last_updated_time"

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1122
    const-string v4, "contacts"

    const-string v5, "person_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v20, v8, v9

    move-object/from16 v0, v22

    invoke-virtual {v14, v4, v0, v5, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1124
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_ad
    .catchall {:try_start_82 .. :try_end_ad} :catchall_d2

    .line 1126
    :try_start_ad
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1129
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    .line 1131
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    .line 1132
    .local v21, resolver:Landroid/content/ContentResolver;
    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-static {v4, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1136
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/service/ImageCache;->notifyAvatarChange(Ljava/lang/String;)V

    .line 1138
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    .line 1139
    monitor-exit v23

    goto/16 :goto_28

    .line 1126
    .end local v21           #resolver:Landroid/content/ContentResolver;
    .end local v22           #values:Landroid/content/ContentValues;
    :catchall_d2
    move-exception v4

    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
    :try_end_d7
    .catchall {:try_start_ad .. :try_end_d7} :catchall_67
.end method

.method public static syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V
    .registers 32
    .parameter "context"
    .parameter "account"
    .parameter "syncState"
    .parameter "listener"
    .parameter "forceSync"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1159
    sget-object v23, Lcom/google/android/apps/plus/content/EsPeopleData;->sPeopleSyncLock:Ljava/lang/Object;

    monitor-enter v23

    .line 1160
    if-nez p4, :cond_17

    .line 1161
    :try_start_5
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->queryPeopleSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v12

    .line 1162
    .local v12, circleSyncTimestamp:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v3, v12

    const-wide/32 v5, 0xea60

    cmp-long v3, v3, v5

    if-gez v3, :cond_17

    .line 1165
    monitor-exit v23

    .line 1186
    .end local v12           #circleSyncTimestamp:J
    :goto_16
    return-void

    .line 1170
    :cond_17
    sget-object v4, Lcom/google/android/apps/plus/content/EsPeopleData;->sCircleSyncLock:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1a
    .catchall {:try_start_5 .. :try_end_1a} :catchall_66

    .line 1171
    :try_start_1a
    invoke-static/range {p0 .. p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->doCirclesSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)Z

    move-result v17

    .line 1172
    .local v17, success:Z
    monitor-exit v4
    :try_end_1f
    .catchall {:try_start_1a .. :try_end_1f} :catchall_69

    .line 1173
    :try_start_1f
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v3

    if-eqz v3, :cond_6c

    const/4 v3, 0x0

    :goto_26
    and-int v17, v17, v3

    .line 1175
    if-eqz v17, :cond_64

    .line 1176
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 1177
    .local v18, values:Landroid/content/ContentValues;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    .line 1178
    .local v15, now:J
    const-string v3, "circle_sync_time"

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1179
    const-string v3, "people_sync_time"

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1180
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v14

    .line 1182
    .local v14, db:Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "account_status"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v14, v3, v0, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1184
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1186
    .end local v14           #db:Landroid/database/sqlite/SQLiteDatabase;
    .end local v15           #now:J
    .end local v18           #values:Landroid/content/ContentValues;
    :cond_64
    monitor-exit v23
    :try_end_65
    .catchall {:try_start_1f .. :try_end_65} :catchall_66

    goto :goto_16

    .end local v17           #success:Z
    :catchall_66
    move-exception v3

    monitor-exit v23

    throw v3

    .line 1172
    :catchall_69
    move-exception v3

    :try_start_6a
    monitor-exit v4

    throw v3

    .line 1173
    .restart local v17       #success:Z
    :cond_6c
    const-string v3, "SocialNetwork"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    const/16 v20, 0x0

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v24

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->queryPeopleLastUpdateToken(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v9

    const/4 v4, 0x0

    const/16 v19, 0x0

    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    move/from16 v21, v3

    move-object/from16 v22, v4

    :goto_93
    if-nez v21, :cond_1fe

    new-instance v3, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/16 v8, 0x12c

    const/4 v10, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v11, p3

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZZILjava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    new-instance v4, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v4}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0, v4}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->hasError()Z

    move-result v4

    if-eqz v4, :cond_bd

    const-string v4, "EsPeopleData"

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->logError(Ljava/lang/String;)V

    const/4 v3, 0x0

    goto/16 :goto_26

    :cond_bd
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/LoadSocialNetworkOperation;->getPersonList()Lcom/google/api/services/plusi/model/DataPersonList;

    move-result-object v5

    iget-object v3, v5, Lcom/google/api/services/plusi/model/DataPersonList;->syncClientInfo:Ljava/util/List;

    if-eqz v3, :cond_f1

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move/from16 v4, v21

    move-object/from16 v7, v22

    :goto_cd
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2a4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v8, "INVALID_SYNC_STATE"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e6

    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/content/EsPeopleData;->queryAllPersonIds(Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/HashSet;

    move-result-object v7

    goto :goto_cd

    :cond_e6
    const-string v8, "SYNC_SESSION_END"

    invoke-virtual {v8, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2a1

    const/4 v3, 0x1

    :goto_ef
    move v4, v3

    goto :goto_cd

    :cond_f1
    move/from16 v6, v21

    move-object/from16 v7, v22

    :goto_f5
    iget-object v3, v5, Lcom/google/api/services/plusi/model/DataPersonList;->syncStateToken:Lcom/google/api/services/plusi/model/DataSyncStateToken;

    if-eqz v3, :cond_103

    invoke-static {}, Lcom/google/api/services/plusi/model/DataSyncStateTokenJson;->getInstance()Lcom/google/api/services/plusi/model/DataSyncStateTokenJson;

    move-result-object v3

    iget-object v4, v5, Lcom/google/api/services/plusi/model/DataPersonList;->syncStateToken:Lcom/google/api/services/plusi/model/DataSyncStateToken;

    invoke-virtual {v3, v4}, Lcom/google/api/services/plusi/model/DataSyncStateTokenJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    :cond_103
    if-nez v9, :cond_106

    const/4 v6, 0x1

    :cond_106
    iget-object v3, v5, Lcom/google/api/services/plusi/model/DataPersonList;->invalidMemberId:Ljava/util/List;

    if-eqz v3, :cond_29d

    iget-object v3, v5, Lcom/google/api/services/plusi/model/DataPersonList;->invalidMemberId:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_29d

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v5, Lcom/google/api/services/plusi/model/DataPersonList;->invalidMemberId:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_11d
    :goto_11d
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_17e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;

    iget-object v10, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_145

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "g:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->obfuscatedGaiaId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_145
    iget-object v10, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_161

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "e:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->email:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_161
    iget-object v10, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->phone:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_11d

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "p:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataCircleMemberId;->phone:Ljava/lang/String;

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_11d

    :cond_17e
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_29d

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->removeContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Z

    move-result v3

    or-int v19, v19, v3

    move/from16 v4, v19

    :goto_190
    iget-object v3, v5, Lcom/google/api/services/plusi/model/DataPersonList;->person:Ljava/util/List;

    if-eqz v3, :cond_295

    iget-object v3, v5, Lcom/google/api/services/plusi/model/DataPersonList;->person:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move/from16 v5, v20

    :cond_19c
    :goto_19c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1ca

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/plusi/model/DataCirclePerson;

    iget-object v10, v3, Lcom/google/api/services/plusi/model/DataCirclePerson;->memberId:Lcom/google/api/services/plusi/model/DataCircleMemberId;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsPeopleData;->getPersonId(Lcom/google/api/services/plusi/model/DataCircleMemberId;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_19c

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getCircleCount(Lcom/google/api/services/plusi/model/DataCirclePerson;)I

    move-result v11

    if-eqz v11, :cond_1c4

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1bb
    if-eqz v7, :cond_1c0

    invoke-virtual {v7, v10}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    :cond_1c0
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_19c

    :cond_1c4
    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1bb

    :cond_1ca
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v8, 0x7d0

    if-le v3, v8, :cond_292

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->insertCirclePersons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Z

    move-result v3

    or-int/2addr v3, v4

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->clear()V

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1f4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->removeContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Z

    move-result v4

    or-int/2addr v3, v4

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->clear()V

    :cond_1f4
    :goto_1f4
    move/from16 v21, v6

    move/from16 v19, v3

    move-object/from16 v22, v7

    move/from16 v20, v5

    goto/16 :goto_93

    :cond_1fe
    if-eqz v22, :cond_20d

    invoke-virtual/range {v22 .. v22}, Ljava/util/HashSet;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_20d

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_20d
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_28f

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->removeContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Z

    move-result v3

    or-int v3, v3, v19

    :goto_21f
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->insertCirclePersons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)Z

    move-result v4

    or-int/2addr v3, v4

    const-string v4, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=circle_id) WHERE type=1"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    if-eqz v3, :cond_243

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_243
    sget-object v4, Lcom/google/android/apps/plus/content/EsPeopleData;->sInitialSyncLatch:Ljava/util/concurrent/CountDownLatch;

    if-eqz v4, :cond_24d

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    const/4 v4, 0x0

    sput-object v4, Lcom/google/android/apps/plus/content/EsPeopleData;->sInitialSyncLatch:Ljava/util/concurrent/CountDownLatch;

    :cond_24d
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "people_last_update_token"

    invoke-virtual {v4, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "avatars_downloaded"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "account_status"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v5, v4, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_280

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    sget-object v5, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_280
    move-object/from16 v0, p2

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    if-eqz v3, :cond_28c

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V
    :try_end_28c
    .catchall {:try_start_6a .. :try_end_28c} :catchall_66

    :cond_28c
    const/4 v3, 0x1

    goto/16 :goto_26

    :cond_28f
    move/from16 v3, v19

    goto :goto_21f

    :cond_292
    move v3, v4

    goto/16 :goto_1f4

    :cond_295
    move/from16 v21, v6

    move/from16 v19, v4

    move-object/from16 v22, v7

    goto/16 :goto_93

    :cond_29d
    move/from16 v4, v19

    goto/16 :goto_190

    :cond_2a1
    move v3, v4

    goto/16 :goto_ef

    :cond_2a4
    move v6, v4

    goto/16 :goto_f5
.end method

.method private static updateInMyCirclesFlagAndTimestampInTransaction(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .registers 5
    .parameter "db"
    .parameter "personId"

    .prologue
    .line 3264
    const-string v0, "UPDATE contacts SET in_my_circles=(EXISTS (SELECT 1 FROM circle_contact WHERE link_person_id=?)),last_updated_time=last_updated_time + 1 WHERE person_id=?"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3271
    return-void
.end method

.method private static updateMemberCountsInTransaction(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;)V
    .registers 9
    .parameter "db"
    .parameter "circleIds"

    .prologue
    .line 3277
    if-eqz p1, :cond_1d

    array-length v5, p1

    if-eqz v5, :cond_1d

    .line 3278
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    .line 3279
    .local v0, args:[Ljava/lang/String;
    move-object v1, p1

    .local v1, arr$:[Ljava/lang/String;
    array-length v4, p1

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_b
    if-ge v3, v4, :cond_1d

    aget-object v2, v1, v3

    .line 3280
    .local v2, circleId:Ljava/lang/String;
    const/4 v5, 0x0

    const/4 v6, 0x1

    aput-object v2, v0, v6

    aput-object v2, v0, v5

    .line 3281
    const-string v5, "UPDATE circles SET contact_count=(SELECT count(*) FROM circle_contact WHERE link_circle_id=?) WHERE circle_id=?"

    invoke-virtual {p0, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 3279
    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    .line 3288
    .end local v0           #args:[Ljava/lang/String;
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #circleId:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_1d
    return-void
.end method
