.class public Lcom/google/android/apps/plus/content/GooglePlaceReview;
.super Ljava/lang/Object;
.source "GooglePlaceReview.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/GooglePlaceReview;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private priceCurrencyCode:Ljava/lang/String;

.field private priceLevelValueId:J

.field private priceValue:Ljava/lang/String;

.field private reviewText:Ljava/lang/String;

.field private zagatAspects:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/apps/plus/content/GooglePlaceReview$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/GooglePlaceReview$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "in"

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->zagatAspects:Landroid/os/Bundle;

    .line 58
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->reviewText:Ljava/lang/String;

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceValue:Ljava/lang/String;

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceCurrencyCode:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceLevelValueId:J

    .line 62
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/GooglePlaceReview;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V
    .registers 13
    .parameter "reviewProto"

    .prologue
    const/4 v10, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    iput-object v9, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->zagatAspects:Landroid/os/Bundle;

    .line 32
    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    .line 33
    .local v2, aspects:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;
    iget-object v9, v2, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_13
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_29

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    .line 34
    .local v0, aspect:Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;
    iget-object v1, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->labelId:Ljava/lang/String;

    .line 35
    .local v1, aspectId:Ljava/lang/String;
    iget-object v8, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    .line 36
    .local v8, value:Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->zagatAspects:Landroid/os/Bundle;

    invoke-virtual {v9, v1, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_13

    .line 40
    .end local v0           #aspect:Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;
    .end local v1           #aspectId:Ljava/lang/String;
    .end local v8           #value:Ljava/lang/String;
    :cond_29
    iget-object v3, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->fullText:Ljava/lang/String;

    .line 41
    .local v3, fullText:Ljava/lang/String;
    iget-object v7, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->snippet:Ljava/lang/String;

    .line 42
    .local v7, snippet:Ljava/lang/String;
    if-eqz v3, :cond_35

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_36

    :cond_35
    move-object v3, v7

    .end local v3           #fullText:Ljava/lang/String;
    :cond_36
    iput-object v3, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->reviewText:Ljava/lang/String;

    .line 44
    iget-object v5, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->price:Lcom/google/api/services/plusi/model/PriceProto;

    .line 45
    .local v5, price:Lcom/google/api/services/plusi/model/PriceProto;
    if-nez v5, :cond_50

    move-object v9, v10

    :goto_3d
    iput-object v9, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceValue:Ljava/lang/String;

    .line 46
    if-nez v5, :cond_53

    :goto_41
    iput-object v10, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceCurrencyCode:Ljava/lang/String;

    .line 48
    iget-object v6, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;

    .line 49
    .local v6, priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;
    if-eqz v6, :cond_4b

    iget-object v9, v6, Lcom/google/api/services/plusi/model/PriceLevelsProto;->ratedValueId:Ljava/lang/Long;

    if-nez v9, :cond_56

    :cond_4b
    const-wide/16 v9, 0x0

    :goto_4d
    iput-wide v9, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceLevelValueId:J

    .line 51
    return-void

    .line 45
    .end local v6           #priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;
    :cond_50
    iget-object v9, v5, Lcom/google/api/services/plusi/model/PriceProto;->valueDisplay:Ljava/lang/String;

    goto :goto_3d

    .line 46
    :cond_53
    iget-object v10, v5, Lcom/google/api/services/plusi/model/PriceProto;->currencyCode:Ljava/lang/String;

    goto :goto_41

    .line 49
    .restart local v6       #priceLevel:Lcom/google/api/services/plusi/model/PriceLevelsProto;
    :cond_56
    iget-object v9, v6, Lcom/google/api/services/plusi/model/PriceLevelsProto;->ratedValueId:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    goto :goto_4d
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public final getPriceCurrencyCode()Ljava/lang/String;
    .registers 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceCurrencyCode:Ljava/lang/String;

    return-object v0
.end method

.method public final getPriceLevelId()Ljava/lang/Long;
    .registers 3

    .prologue
    .line 112
    iget-wide v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceLevelValueId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final getPriceValue()Ljava/lang/String;
    .registers 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceValue:Ljava/lang/String;

    return-object v0
.end method

.method public final getReviewText()Ljava/lang/String;
    .registers 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->reviewText:Ljava/lang/String;

    return-object v0
.end method

.method public final getZagatAspects()Landroid/os/Bundle;
    .registers 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->zagatAspects:Landroid/os/Bundle;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "parcel"
    .parameter "flags"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->zagatAspects:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->reviewText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceCurrencyCode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget-wide v0, p0, Lcom/google/android/apps/plus/content/GooglePlaceReview;->priceLevelValueId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 76
    return-void
.end method
