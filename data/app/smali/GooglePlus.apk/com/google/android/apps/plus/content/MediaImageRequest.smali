.class public Lcom/google/android/apps/plus/content/MediaImageRequest;
.super Lcom/google/android/apps/plus/content/CachedImageRequest;
.source "MediaImageRequest.java"


# instance fields
.field private mCanonicalUrl:Ljava/lang/String;

.field private final mCropAndResize:Z

.field private mDownloadUrl:Ljava/lang/String;

.field private mHashCode:I

.field private final mHeight:I

.field private final mMediaType:I

.field private final mUrl:Ljava/lang/String;

.field private final mWidth:I


# direct methods
.method public constructor <init>()V
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 32
    const/4 v1, 0x0

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .registers 10
    .parameter "url"
    .parameter "mediaType"
    .parameter "size"

    .prologue
    .line 36
    const/4 v2, 0x3

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v3, p3

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIIZ)V
    .registers 7
    .parameter "url"
    .parameter "mediaType"
    .parameter "width"
    .parameter "height"
    .parameter "cropAndResize"

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/CachedImageRequest;-><init>()V

    .line 41
    if-nez p1, :cond_b

    .line 42
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 45
    :cond_b
    iput-object p1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mUrl:Ljava/lang/String;

    .line 46
    iput p2, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mMediaType:I

    .line 47
    iput p3, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mWidth:I

    .line 48
    iput p4, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHeight:I

    .line 49
    iput-boolean p5, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mCropAndResize:Z

    .line 50
    return-void
.end method

.method public static areCanonicallyEqual(Lcom/google/android/apps/plus/content/MediaImageRequest;Lcom/google/android/apps/plus/content/MediaImageRequest;)Z
    .registers 4
    .parameter "first"
    .parameter "second"

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static areCanonicallyEqual(Lcom/google/android/apps/plus/content/MediaImageRequest;Ljava/lang/String;)Z
    .registers 4
    .parameter "request"
    .parameter "url"

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->canonicalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static areCanonicallyEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "first"
    .parameter "second"

    .prologue
    .line 109
    invoke-static {p0}, Lcom/google/android/apps/plus/content/MediaImageRequest;->canonicalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/content/MediaImageRequest;->canonicalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private static canonicalize(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "url"

    .prologue
    .line 174
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 178
    .end local p0
    :goto_6
    return-object p0

    .restart local p0
    :cond_7
    const-string v0, "http://images\\d+-focus-opensocial.googleusercontent.com/gadgets/proxy"

    const-string v1, "http://images1-focus-opensocial.googleusercontent.com/gadgets/proxy"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_6
.end method

.method private getCanonicalUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mCanonicalUrl:Ljava/lang/String;

    if-nez v0, :cond_c

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/MediaImageRequest;->canonicalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mCanonicalUrl:Ljava/lang/String;

    .line 64
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mCanonicalUrl:Ljava/lang/String;

    return-object v0
.end method

.method private getDownloadUrl(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "url"

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    if-nez v0, :cond_3f

    .line 82
    iget-boolean v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mCropAndResize:Z

    if-eqz v0, :cond_c

    iget v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mWidth:I

    if-nez v0, :cond_42

    .line 83
    :cond_c
    iput-object p1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    .line 90
    :goto_e
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    .line 94
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    const-string v1, "&google_plus:card_type=nonsquare"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&google_plus:widget"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    .line 97
    :cond_3f
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    return-object v0

    .line 84
    :cond_42
    iget v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHeight:I

    if-ne v0, v1, :cond_51

    .line 85
    iget v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mWidth:I

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/ImageUtils;->getCroppedAndResizedUrl(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    goto :goto_e

    .line 87
    :cond_51
    iget v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHeight:I

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/util/ImageUtils;->getCenterCroppedAndResizedUrl(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mDownloadUrl:Ljava/lang/String;

    goto :goto_e
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .parameter "o"

    .prologue
    const/4 v1, 0x0

    .line 153
    instance-of v2, p1, Lcom/google/android/apps/plus/content/MediaImageRequest;

    if-nez v2, :cond_6

    .line 158
    :cond_5
    :goto_5
    return v1

    :cond_6
    move-object v0, p1

    .line 157
    check-cast v0, Lcom/google/android/apps/plus/content/MediaImageRequest;

    .line 158
    .local v0, other:Lcom/google/android/apps/plus/content/MediaImageRequest;
    iget v2, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mWidth:I

    iget v3, v0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mWidth:I

    if-ne v2, v3, :cond_5

    iget v2, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHeight:I

    iget v3, v0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHeight:I

    if-ne v2, v3, :cond_5

    iget v2, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mMediaType:I

    iget v3, v0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mMediaType:I

    if-ne v2, v3, :cond_5

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/MediaImageRequest;->areCanonicallyEqual(Lcom/google/android/apps/plus/content/MediaImageRequest;Lcom/google/android/apps/plus/content/MediaImageRequest;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v1, 0x1

    goto :goto_5
.end method

.method protected final getCacheFilePrefix()Ljava/lang/String;
    .registers 2

    .prologue
    .line 170
    const-string v0, "M"

    return-object v0
.end method

.method public final getCanonicalDownloadUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getDownloadUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getDownloadUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getHeight()I
    .registers 2

    .prologue
    .line 130
    iget v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHeight:I

    return v0
.end method

.method public final getMediaType()I
    .registers 2

    .prologue
    .line 116
    iget v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mMediaType:I

    return v0
.end method

.method public final getUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getWidth()I
    .registers 2

    .prologue
    .line 123
    iget v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mWidth:I

    return v0
.end method

.method public hashCode()I
    .registers 3

    .prologue
    .line 140
    iget v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHashCode:I

    if-nez v1, :cond_10

    .line 141
    invoke-direct {p0}, Lcom/google/android/apps/plus/content/MediaImageRequest;->getCanonicalUrl()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, url:Ljava/lang/String;
    if-eqz v0, :cond_13

    .line 143
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHashCode:I

    .line 148
    .end local v0           #url:Ljava/lang/String;
    :cond_10
    :goto_10
    iget v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHashCode:I

    return v1

    .line 145
    .restart local v0       #url:Ljava/lang/String;
    :cond_13
    const/4 v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHashCode:I

    goto :goto_10
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mUrl:Ljava/lang/String;

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MediaImageRequest: type="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mMediaType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/content/MediaImageRequest;->mHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
