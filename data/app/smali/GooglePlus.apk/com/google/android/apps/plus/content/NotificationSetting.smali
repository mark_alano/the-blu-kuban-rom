.class public Lcom/google/android/apps/plus/content/NotificationSetting;
.super Ljava/lang/Object;
.source "NotificationSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/NotificationSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 127
    new-instance v0, Lcom/google/android/apps/plus/content/NotificationSetting$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/NotificationSetting$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/NotificationSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "parcel"

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/NotificationSetting;->readBoolean(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForEmail:Ljava/lang/Boolean;

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/NotificationSetting;->readBoolean(Landroid/os/Parcel;)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    .line 50
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/NotificationSetting;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/NotificationSetting;)V
    .registers 4
    .parameter "setting"

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForEmail:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForEmail:Ljava/lang/Boolean;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, p1, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    .line 40
    return-void
.end method

.method public constructor <init>(Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;)V
    .registers 2
    .parameter "setting"

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    .line 25
    return-void
.end method

.method private static readBoolean(Landroid/os/Parcel;)Ljava/lang/Boolean;
    .registers 3
    .parameter "in"

    .prologue
    .line 99
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 100
    .local v0, val:I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_a

    .line 101
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 105
    :goto_9
    return-object v1

    .line 102
    :cond_a
    if-nez v0, :cond_f

    .line 103
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_9

    .line 105
    :cond_f
    const/4 v1, 0x0

    goto :goto_9
.end method

.method private static writeBoolean(Landroid/os/Parcel;Ljava/lang/Boolean;)V
    .registers 3
    .parameter "dest"
    .parameter "b"

    .prologue
    .line 91
    if-nez p1, :cond_7

    .line 92
    const/4 v0, -0x1

    .line 94
    :goto_3
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    return-void

    .line 94
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    goto :goto_3

    :cond_f
    const/4 v0, 0x0

    goto :goto_3
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method public final getDeliveryOption()Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;
    .registers 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    return-object v0
.end method

.method public final getDescription()Ljava/lang/String;
    .registers 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final isEnabled()Z
    .registers 2

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final setEnabled(Z)V
    .registers 4
    .parameter "enabled"

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    .line 81
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{Setting "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " offNetId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->bucketId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->offnetworkBucketId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->category:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForEmail:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/NotificationSetting;->writeBoolean(Landroid/os/Parcel;Ljava/lang/Boolean;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSetting;->mSetting:Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataNotificationSettingsDeliveryOption;->enabledForPhone:Ljava/lang/Boolean;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/NotificationSetting;->writeBoolean(Landroid/os/Parcel;Ljava/lang/Boolean;)V

    .line 60
    return-void
.end method
