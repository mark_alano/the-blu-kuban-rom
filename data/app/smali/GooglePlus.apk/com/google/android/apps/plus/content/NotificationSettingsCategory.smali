.class public Lcom/google/android/apps/plus/content/NotificationSettingsCategory;
.super Ljava/lang/Object;
.source "NotificationSettingsCategory.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/content/NotificationSettingsCategory;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDescription:Ljava/lang/String;

.field private final mSettings:[Lcom/google/android/apps/plus/content/NotificationSetting;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mDescription:Ljava/lang/String;

    .line 40
    sget-object v0, Lcom/google/android/apps/plus/content/NotificationSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/content/NotificationSetting;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mSettings:[Lcom/google/android/apps/plus/content/NotificationSetting;

    .line 41
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .registers 4
    .parameter "description"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/NotificationSetting;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p2, settingsList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/NotificationSetting;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mDescription:Ljava/lang/String;

    .line 30
    if-eqz p2, :cond_15

    .line 31
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/plus/content/NotificationSetting;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mSettings:[Lcom/google/android/apps/plus/content/NotificationSetting;

    .line 32
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mSettings:[Lcom/google/android/apps/plus/content/NotificationSetting;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 36
    :goto_14
    return-void

    .line 34
    :cond_15
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/apps/plus/content/NotificationSetting;

    iput-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mSettings:[Lcom/google/android/apps/plus/content/NotificationSetting;

    goto :goto_14
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public final getDescription()Ljava/lang/String;
    .registers 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public final getSetting(I)Lcom/google/android/apps/plus/content/NotificationSetting;
    .registers 3
    .parameter "index"

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mSettings:[Lcom/google/android/apps/plus/content/NotificationSetting;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getSettingsCount()I
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mSettings:[Lcom/google/android/apps/plus/content/NotificationSetting;

    array-length v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Category: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Settings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mSettings:[Lcom/google/android/apps/plus/content/NotificationSetting;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->mSettings:[Lcom/google/android/apps/plus/content/NotificationSetting;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 47
    return-void
.end method
