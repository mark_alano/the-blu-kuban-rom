.class public final Lcom/google/android/apps/plus/external/PlatformContractUtils;
.super Ljava/lang/Object;
.source "PlatformContractUtils.java"


# direct methods
.method public static getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;
    .registers 4
    .parameter "info"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/network/ApiaryApiInfo;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 94
    .local v0, result:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz p0, :cond_16

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v1

    if-eqz v1, :cond_16

    .line 95
    const-string v1, "CONTAINER_URL"

    invoke-static {p0}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getContainerUrl(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    :cond_16
    return-object v0
.end method

.method public static getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;
    .registers 7
    .parameter "packageName"
    .parameter "packageManager"

    .prologue
    const/4 v2, 0x0

    .line 157
    const/4 v0, 0x0

    .line 159
    .local v0, cert:Ljava/lang/String;
    const/16 v3, 0x40

    :try_start_4
    invoke-virtual {p1, p0, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 161
    .local v1, info:Landroid/content/pm/PackageInfo;
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v3, :cond_23

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v3, v3
    :try_end_f
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_f} :catch_48

    if-lez v3, :cond_23

    .line 163
    :try_start_11
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    const-string v4, "SHA1"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1f
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_11 .. :try_end_1f} :catch_36
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_11 .. :try_end_1f} :catch_48

    move-result-object v4

    if-nez v4, :cond_28

    move-object v0, v2

    .line 175
    .end local v1           #info:Landroid/content/pm/PackageInfo;
    :cond_23
    :goto_23
    if-nez v0, :cond_27

    .line 176
    const-string v0, "0"

    .line 178
    :cond_27
    return-object v0

    .line 163
    .restart local v1       #info:Landroid/content/pm/PackageInfo;
    :cond_28
    :try_start_28
    invoke-virtual {v4, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v3

    if-nez v3, :cond_30

    move-object v0, v2

    goto :goto_23

    :cond_30
    const/4 v2, 0x2

    invoke-static {v3, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_34
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_28 .. :try_end_34} :catch_36
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_28 .. :try_end_34} :catch_48

    move-result-object v0

    goto :goto_23

    .line 165
    :catch_36
    move-exception v2

    :try_start_37
    const-string v2, "PlusOneContract"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_23

    .line 166
    const-string v2, "PlusOneContract"

    const-string v3, "Unable to compute digest, returning zeros"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_47
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_37 .. :try_end_47} :catch_48

    goto :goto_23

    .line 171
    .end local v1           #info:Landroid/content/pm/PackageInfo;
    :catch_48
    move-exception v2

    goto :goto_23
.end method

.method public static getContainerUrl(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/lang/String;
    .registers 8
    .parameter "info"

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v5

    if-eqz v5, :cond_a

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object p0

    .line 65
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getCertificate()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getCertificate()Ljava/lang/String;

    move-result-object v2

    .line 67
    .local v2, certHash:Ljava/lang/String;
    :goto_14
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getClientId()Ljava/lang/String;

    move-result-object v3

    .line 68
    .local v3, clientId:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, apiKey:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 71
    .local v4, pkg:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "http://"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".apps.googleusercontent.com/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 73
    .local v1, builder:Landroid/net/Uri$Builder;
    if-eqz v3, :cond_48

    .line 74
    const-string v5, "client_id"

    invoke-virtual {v1, v5, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 76
    :cond_48
    if-eqz v0, :cond_4f

    .line 77
    const-string v5, "api_key"

    invoke-virtual {v1, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 79
    :cond_4f
    if-eqz v4, :cond_56

    .line 80
    const-string v5, "pkg"

    invoke-virtual {v1, v5, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 82
    :cond_56
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 65
    .end local v0           #apiKey:Ljava/lang/String;
    .end local v1           #builder:Landroid/net/Uri$Builder;
    .end local v2           #certHash:Ljava/lang/String;
    .end local v3           #clientId:Ljava/lang/String;
    .end local v4           #pkg:Ljava/lang/String;
    :cond_5f
    const-string v2, "0"

    goto :goto_14
.end method
