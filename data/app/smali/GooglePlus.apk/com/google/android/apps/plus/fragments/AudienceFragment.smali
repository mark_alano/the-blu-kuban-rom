.class public Lcom/google/android/apps/plus/fragments/AudienceFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "AudienceFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;,
        Lcom/google/android/apps/plus/fragments/AudienceFragment$RTCListener;,
        Lcom/google/android/apps/plus/fragments/AudienceFragment$HangoutSuggestionsQuery;,
        Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;"
    }
.end annotation


# instance fields
.field private displayedSuggestedParticipants:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAudienceChangedCallback:Ljava/lang/Runnable;

.field protected mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

.field protected mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

.field private mCacheSuggestionsResponse:Z

.field private mCircleUsageType:I

.field private mFilterNullGaiaIds:Z

.field private mGridView:Landroid/widget/GridView;

.field private mIncludePhoneOnlyContacts:Z

.field private mIncludePlusPages:Z

.field private mListHeader:Landroid/widget/TextView;

.field private mListParent:Landroid/view/View;

.field private mPublicProfileSearchEnabled:Z

.field private mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/AudienceFragment$RTCListener;

.field private mRequestId:Ljava/lang/Integer;

.field protected mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

.field private mShowSuggestedPeople:Z

.field private mSuggestedPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

.field private mSuggestedPeopleScrollView:Landroid/widget/ScrollView;

.field private mSuggestedPeopleSize:I


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePhoneOnlyContacts:Z

    .line 88
    new-instance v0, Lcom/google/android/apps/plus/fragments/AudienceFragment$RTCListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment$RTCListener;-><init>(Lcom/google/android/apps/plus/fragments/AudienceFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/AudienceFragment$RTCListener;

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    .line 99
    iput v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleSize:I

    .line 150
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/AudienceFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/AudienceFragment;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/AudienceFragment;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/AudienceFragment;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->isInAudience(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/AudienceFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mShowSuggestedPeople:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/AudienceFragment;)Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/AudienceFragment;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListHeader:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/AudienceFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->updateSuggestedPeopleDisplay()V

    return-void
.end method

.method private cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .registers 4
    .parameter "response"

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mCacheSuggestionsResponse:Z

    if-eqz v0, :cond_10

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/content/EsAudienceData;->processSuggestionsResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    .line 233
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mCacheSuggestionsResponse:Z

    .line 235
    :cond_10
    return-void
.end method

.method private isInAudience(Ljava/lang/String;)Z
    .registers 9
    .parameter "participantId"

    .prologue
    .line 289
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    .line 290
    .local v1, audience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_c
    if-ge v4, v5, :cond_21

    aget-object v3, v0, v4

    .line 291
    .local v3, audiencePerson:Lcom/google/android/apps/plus/content/PersonData;
    invoke-static {v3}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantIdFromPerson(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;

    move-result-object v2

    .line 293
    .local v2, audienceParticipantId:Ljava/lang/String;
    if-eqz v2, :cond_1e

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 295
    const/4 v6, 0x1

    .line 299
    .end local v2           #audienceParticipantId:Ljava/lang/String;
    .end local v3           #audiencePerson:Lcom/google/android/apps/plus/content/PersonData;
    :goto_1d
    return v6

    .line 290
    .restart local v2       #audienceParticipantId:Ljava/lang/String;
    .restart local v3       #audiencePerson:Lcom/google/android/apps/plus/content/PersonData;
    :cond_1e
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 299
    .end local v2           #audienceParticipantId:Ljava/lang/String;
    .end local v3           #audiencePerson:Lcom/google/android/apps/plus/content/PersonData;
    :cond_21
    const/4 v6, 0x0

    goto :goto_1d
.end method

.method private loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .registers 8
    .parameter "response"

    .prologue
    .line 217
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getSuggestionList()Ljava/util/List;

    move-result-object v4

    .line 219
    .local v4, suggestions:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Client$Suggestion;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    .line 220
    .local v3, suggestion:Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->getSuggestedUserList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 221
    .local v2, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1c

    .line 224
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v3           #suggestion:Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    :cond_2e
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->updateSuggestedPeopleDisplay()V

    .line 225
    return-void
.end method

.method private updateSuggestedPeopleDisplay()V
    .registers 16

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 249
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 251
    .local v9, suggestedParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    const/4 v2, 0x0

    .line 252
    .local v2, found:Z
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->displayedSuggestedParticipants:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :cond_1b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_36

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 253
    .local v0, audiencePerson:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v8

    .line 254
    .local v8, participantId:Ljava/lang/String;
    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1b

    .line 255
    const/4 v2, 0x1

    .line 260
    .end local v0           #audiencePerson:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v8           #participantId:Ljava/lang/String;
    :cond_36
    if-nez v2, :cond_8

    .line 261
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->displayedSuggestedParticipants:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListHeader:Landroid/widget/TextView;

    if-eqz v10, :cond_8

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListHeader:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getVisibility()I

    move-result v10

    if-eqz v10, :cond_8

    .line 264
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListHeader:Landroid/widget/TextView;

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    .line 269
    .end local v2           #found:Z
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v9           #suggestedParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_4f
    const/4 v5, 0x0

    .line 270
    .local v5, id:I
    new-instance v7, Landroid/database/MatrixCursor;

    sget-object v10, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleQuery;->columnNames:[Ljava/lang/String;

    invoke-direct {v7, v10}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 271
    .local v7, matrixCursor:Landroid/database/MatrixCursor;
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->displayedSuggestedParticipants:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_5d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 272
    .local v1, displayedSuggestedParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    const/4 v10, 0x4

    new-array v13, v10, [Ljava/lang/Object;

    add-int/lit8 v6, v5, 0x1

    .end local v5           #id:I
    .local v6, id:I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v13, v12

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v13, v11

    const/4 v10, 0x2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v10

    const/4 v14, 0x3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->isInAudience(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_98

    move v10, v11

    :goto_8d
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v13, v14

    invoke-virtual {v7, v13}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v5, v6

    .end local v6           #id:I
    .restart local v5       #id:I
    goto :goto_5d

    .end local v5           #id:I
    .restart local v6       #id:I
    :cond_98
    move v10, v12

    goto :goto_8d

    .line 278
    .end local v1           #displayedSuggestedParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v6           #id:I
    .restart local v5       #id:I
    :cond_9a
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    invoke-virtual {v10, v7}, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 281
    iget v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleSize:I

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->getCount()I

    move-result v11

    if-eq v10, v11, :cond_c4

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->getCount()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v11}, Landroid/widget/GridView;->getChildCount()I

    move-result v11

    if-ne v10, v11, :cond_c4

    .line 283
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v10, v12, v12}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 284
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->getCount()I

    move-result v10

    iput v10, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleSize:I

    .line 286
    :cond_c4
    return-void
.end method


# virtual methods
.method public final getAudience()Lcom/google/android/apps/plus/content/AudienceData;
    .registers 2

    .prologue
    .line 741
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    return-object v0
.end method

.method protected final getSuggestedPeople()V
    .registers 6

    .prologue
    .line 489
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    .line 490
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->isAudienceEmpty()Z

    move-result v1

    .line 492
    .local v1, emptyAudience:Z
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mCacheSuggestionsResponse:Z

    .line 493
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->HANGOUT:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    invoke-static {v2, v3, v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->requestSuggestedParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    .line 495
    if-eqz v1, :cond_29

    .line 498
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 500
    :cond_29
    return-void
.end method

.method public final isAudienceEmpty()Z
    .registers 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 575
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    .line 576
    .local v1, audience:Lcom/google/android/apps/plus/content/AudienceData;
    if-nez v1, :cond_b

    .line 589
    :cond_a
    :goto_a
    return v5

    .line 578
    :cond_b
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v7

    if-lez v7, :cond_13

    move v5, v6

    .line 579
    goto :goto_a

    .line 581
    :cond_13
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_19
    if-ge v3, v4, :cond_a

    aget-object v2, v0, v3

    .line 582
    .local v2, circle:Lcom/google/android/apps/plus/content/CircleData;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v7

    if-gtz v7, :cond_32

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    const/16 v8, 0x9

    if-eq v7, v8, :cond_32

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    const/4 v8, 0x7

    if-ne v7, v8, :cond_34

    :cond_32
    move v5, v6

    .line 585
    goto :goto_a

    .line 581
    :cond_34
    add-int/lit8 v3, v3, 0x1

    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 529
    const/4 v0, 0x0

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 313
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 314
    if-nez p1, :cond_1c

    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 316
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "audience"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    .line 318
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    if-eqz v0, :cond_1c

    .line 319
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/AudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 322
    .end local v0           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v1           #intent:Landroid/content/Intent;
    :cond_1c
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 564
    const/4 v0, 0x1

    if-ne p1, v0, :cond_12

    .line 565
    const/4 v0, -0x1

    if-ne p2, v0, :cond_12

    if-eqz p3, :cond_12

    .line 566
    const-string v0, "audience"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    .line 569
    :cond_12
    return-void
.end method

.method public final onAddPersonToCirclesAction(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 4
    .parameter "personId"
    .parameter "name"
    .parameter "forSharing"

    .prologue
    .line 668
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    .prologue
    .line 307
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onAttach(Landroid/app/Activity;)V

    .line 308
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 309
    return-void
.end method

.method protected final onAudienceChanged()V
    .registers 2

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceChangedCallback:Ljava/lang/Runnable;

    if-eqz v0, :cond_9

    .line 504
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceChangedCallback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 506
    :cond_9
    return-void
.end method

.method public final onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "personId"
    .parameter "name"

    .prologue
    .line 677
    return-void
.end method

.method public final onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .registers 4
    .parameter "circleId"
    .parameter "circle"

    .prologue
    .line 650
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/AudienceView;->addCircle(Lcom/google/android/apps/plus/content/CircleData;)V

    .line 652
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    instance-of v0, v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    if-eqz v0, :cond_12

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    check-cast v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    .line 655
    :cond_12
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 11
    .parameter "v"

    .prologue
    .line 546
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_2e

    .line 557
    :goto_7
    return-void

    .line 549
    :sswitch_8
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    .line 550
    .local v3, audience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const v2, 0x7f080283

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v4, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mCircleUsageType:I

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePhoneOnlyContacts:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePlusPages:Z

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mPublicProfileSearchEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mFilterNullGaiaIds:Z

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_7

    .line 546
    :sswitch_data_2e
    .sparse-switch
        0x7f090052 -> :sswitch_8
        0x7f090057 -> :sswitch_8
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 390
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 391
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeople:Ljava/util/List;

    .line 392
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->displayedSuggestedParticipants:Ljava/util/List;

    .line 393
    if-eqz p1, :cond_4f

    .line 394
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 395
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    .line 396
    const-string v0, "cache_suggestions_response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mCacheSuggestionsResponse:Z

    .line 402
    :goto_2f
    const-string v0, "show_suggested_people"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mShowSuggestedPeople:Z

    .line 404
    const-string v0, "public_profile_search"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mPublicProfileSearchEnabled:Z

    .line 406
    const-string v0, "phone_only_contacts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePhoneOnlyContacts:Z

    .line 408
    const-string v0, "plus_pages"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePlusPages:Z

    .line 411
    :cond_4f
    return-void

    .line 399
    :cond_50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    .line 400
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mCacheSuggestionsResponse:Z

    goto :goto_2f
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 11
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 689
    const-string v0, "Audience"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 690
    const-string v0, "Audience"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "onCreateLoader "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    :cond_1e
    const/4 v0, 0x1

    if-ne p1, v0, :cond_39

    .line 693
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 695
    .local v2, suggestionsUri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/AudienceFragment$HangoutSuggestionsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v6, "sequence ASC"

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 700
    .end local v2           #suggestionsUri:Landroid/net/Uri;
    :goto_38
    return-object v0

    :cond_39
    move-object v0, v4

    goto :goto_38
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 419
    const v1, 0x7f030009

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 420
    .local v0, view:Landroid/view/View;
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mGridView:Landroid/widget/GridView;

    .line 421
    const v1, 0x7f090053

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleScrollView:Landroid/widget/ScrollView;

    .line 424
    const v1, 0x7f090054

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListParent:Landroid/view/View;

    .line 425
    const v1, 0x7f090055

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListHeader:Landroid/widget/TextView;

    .line 426
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 427
    new-instance v1, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;-><init>(Lcom/google/android/apps/plus/fragments/AudienceFragment;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    .line 428
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mGridView:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 430
    return-object v0
.end method

.method public final onDismissSuggestionAction(Ljava/lang/String;)V
    .registers 2
    .parameter "personId"

    .prologue
    .line 685
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 538
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->onItemClick(I)V

    .line 539
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 9
    .parameter
    .parameter "data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 706
    const-string v1, "Audience"

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_22

    .line 707
    const-string v1, "Audience"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onLoadFinished "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    :cond_22
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    if-ne v1, v4, :cond_5f

    .line 711
    if-eqz p2, :cond_5f

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5f

    .line 713
    :cond_30
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    .line 719
    .local v0, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 720
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_30

    .line 722
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->updateSuggestedPeopleDisplay()V

    .line 725
    .end local v0           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_5f
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 730
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 379
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onPause()V

    .line 380
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mShowSuggestedPeople:Z

    if-eqz v0, :cond_c

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/AudienceFragment$RTCListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 383
    :cond_c
    return-void
.end method

.method public final onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 5
    .parameter "personId"
    .parameter "contactLookupKey"
    .parameter "person"

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/views/AudienceView;->addPerson(Lcom/google/android/apps/plus/content/PersonData;)V

    .line 661
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    instance-of v0, v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    if-eqz v0, :cond_12

    .line 662
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    check-cast v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    .line 664
    :cond_12
    return-void
.end method

.method public onResume()V
    .registers 4

    .prologue
    .line 351
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    .line 352
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_11

    .line 353
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/AudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 354
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    .line 356
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mShowSuggestedPeople:Z

    if-eqz v1, :cond_1a

    .line 357
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/AudienceFragment$RTCListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 359
    :cond_1a
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_63

    .line 360
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_63

    .line 363
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->removeResult(I)Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    move-result-object v0

    .line 364
    .local v0, result:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    if-eqz v0, :cond_63

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getErrorCode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_63

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    if-eqz v1, :cond_63

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSuggestionsResponse()Z

    move-result v1

    if-eqz v1, :cond_63

    .line 367
    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    .line 368
    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    .line 372
    .end local v0           #result:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    :cond_63
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 510
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 511
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_c

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 514
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_22

    .line 515
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 516
    const-string v0, "cache_suggestions_response"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mCacheSuggestionsResponse:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 518
    :cond_22
    const-string v0, "show_suggested_people"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mShowSuggestedPeople:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 519
    const-string v0, "public_profile_search"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mPublicProfileSearchEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 520
    const-string v0, "phone_only_contacts"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePhoneOnlyContacts:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 521
    const-string v0, "plus_pages"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePlusPages:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 522
    return-void
.end method

.method public final onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .registers 4
    .parameter "adapter"

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListParent:Landroid/view/View;

    if-eqz v0, :cond_10

    .line 637
    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListParent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 643
    :cond_10
    :goto_10
    return-void

    .line 640
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListParent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_10
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 329
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onStart()V

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_c

    .line 331
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStart()V

    .line 333
    :cond_c
    return-void
.end method

.method public final onStop()V
    .registers 2

    .prologue
    .line 340
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onStart()V

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_c

    .line 342
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStop()V

    .line 344
    :cond_c
    return-void
.end method

.method public final onUnblockPersonAction(Ljava/lang/String;Z)V
    .registers 3
    .parameter "personId"
    .parameter "isPlusPage"

    .prologue
    .line 681
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 9
    .parameter "view"
    .parameter "savedInstanceState"

    .prologue
    .line 435
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 436
    const v2, 0x7f090052

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/AudienceView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    .line 438
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0f003d

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 440
    .local v1, themeContext:Landroid/content/Context;
    new-instance v2, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v2, v1, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    .line 442
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePhoneOnlyContacts:Z

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePhoneNumberContacts(Z)V

    .line 443
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePlusPages:Z

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePlusPages(Z)V

    .line 444
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mPublicProfileSearchEnabled:Z

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setPublicProfileSearchEnabled(Z)V

    .line 445
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mCircleUsageType:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setCircleUsageType(I)V

    .line 446
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mFilterNullGaiaIds:Z

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setFilterNullGaiaIds(Z)V

    .line 447
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setListener(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;)V

    .line 448
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onCreate(Landroid/os/Bundle;)V

    .line 449
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    instance-of v2, v2, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    if-eqz v2, :cond_6d

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    check-cast v0, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    .line 451
    .local v0, audienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;
    const v2, 0x7f0801f9

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setEmptyAudienceHint(I)V

    .line 452
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAutoCompleteAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;)V

    .line 454
    .end local v0           #audienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;
    :cond_6d
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/AudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 455
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/AudienceView;->initLoaders(Landroid/support/v4/app/LoaderManager;)V

    .line 457
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setupAudienceClickListener()V

    .line 458
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    new-instance v3, Lcom/google/android/apps/plus/fragments/AudienceFragment$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/AudienceFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/AudienceView;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    .line 472
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mShowSuggestedPeople:Z

    if-eqz v2, :cond_a4

    .line 473
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment$SuggestedPeopleAdpater;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListHeader:Landroid/widget/TextView;

    if-eqz v2, :cond_a1

    .line 474
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mListHeader:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 476
    :cond_a1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getSuggestedPeople()V

    .line 478
    :cond_a4
    return-void
.end method

.method public final setAudienceChangedCallback(Ljava/lang/Runnable;)V
    .registers 2
    .parameter "callback"

    .prologue
    .line 628
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mAudienceChangedCallback:Ljava/lang/Runnable;

    .line 629
    return-void
.end method

.method public final setCirclesUsageType(I)V
    .registers 2
    .parameter "circleUsageType"

    .prologue
    .line 757
    iput p1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mCircleUsageType:I

    .line 758
    return-void
.end method

.method public final setFilterNullGaiaIds(Z)V
    .registers 3
    .parameter "filterNullGaiaIds"

    .prologue
    .line 778
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mFilterNullGaiaIds:Z

    .line 779
    return-void
.end method

.method public final setIncludePhoneOnlyContacts(Z)V
    .registers 2
    .parameter "includePhoneContactsOnly"

    .prologue
    .line 764
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePhoneOnlyContacts:Z

    .line 765
    return-void
.end method

.method public final setIncludePlusPages(Z)V
    .registers 2
    .parameter "includePlusPages"

    .prologue
    .line 771
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mIncludePlusPages:Z

    .line 772
    return-void
.end method

.method public final setPublicProfileSearchEnabled(Z)V
    .registers 3
    .parameter "publicProfileSearchEnabled"

    .prologue
    .line 785
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mPublicProfileSearchEnabled:Z

    .line 786
    return-void
.end method

.method public final setShowSuggestedPeople(Z)V
    .registers 3
    .parameter "show"

    .prologue
    .line 793
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/AudienceFragment;->mShowSuggestedPeople:Z

    .line 794
    return-void
.end method

.method protected setupAudienceClickListener()V
    .registers 3

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 485
    .local v0, view:Landroid/view/View;
    const v1, 0x7f090057

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 486
    return-void
.end method
