.class final Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;
.super Ljava/lang/Object;
.source "CheckinListFragment.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/CheckinListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckinLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/CheckinListFragment;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;-><init>(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .registers 7
    .parameter "location"

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->access$200(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)Lcom/google/android/apps/plus/phone/LocationController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled()Z

    move-result v0

    .line 113
    .local v0, isLocationEnabled:Z
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    #calls: Lcom/google/android/apps/plus/fragments/CheckinListFragment;->removeLocationListener()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->access$300(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)V

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mQuery:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->access$400(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5e

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    new-instance v2, Lcom/google/android/apps/plus/api/LocationQuery;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mQuery:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->access$400(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p1, v3}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    #setter for: Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->access$502(Lcom/google/android/apps/plus/fragments/CheckinListFragment;Lcom/google/android/apps/plus/api/LocationQuery;)Lcom/google/android/apps/plus/api/LocationQuery;

    .line 123
    :goto_2b
    if-eqz v0, :cond_5d

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getView()Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    const v4, 0x7f080096

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    #calls: Lcom/google/android/apps/plus/fragments/CheckinListFragment;->showProgress(Landroid/view/View;Ljava/lang/String;)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->access$600(Lcom/google/android/apps/plus/fragments/CheckinListFragment;Landroid/view/View;Ljava/lang/String;)V

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->access$500(Lcom/google/android/apps/plus/fragments/CheckinListFragment;)Lcom/google/android/apps/plus/api/LocationQuery;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 127
    :cond_5d
    return-void

    .line 119
    :cond_5e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/CheckinListFragment$CheckinLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    new-instance v2, Lcom/google/android/apps/plus/api/LocationQuery;

    const/4 v3, 0x0

    invoke-direct {v2, p1, v3}, Lcom/google/android/apps/plus/api/LocationQuery;-><init>(Landroid/location/Location;Ljava/lang/String;)V

    #setter for: Lcom/google/android/apps/plus/fragments/CheckinListFragment;->mLocationQuery:Lcom/google/android/apps/plus/api/LocationQuery;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->access$502(Lcom/google/android/apps/plus/fragments/CheckinListFragment;Lcom/google/android/apps/plus/api/LocationQuery;)Lcom/google/android/apps/plus/api/LocationQuery;

    goto :goto_2b
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    .prologue
    .line 134
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    .prologue
    .line 141
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    .prologue
    .line 148
    return-void
.end method
