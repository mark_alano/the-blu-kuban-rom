.class public final Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
.super Ljava/lang/Object;
.source "CircleSpinnerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CircleSpinnerInfo"
.end annotation


# instance fields
.field public final circleType:I

.field public final count:I

.field public final iconResId:I

.field public final id:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;III)V
    .registers 6
    .parameter "id"
    .parameter "title"
    .parameter "circleType"
    .parameter "count"
    .parameter "iconResId"

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->id:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->title:Ljava/lang/String;

    .line 36
    iput p4, p0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->count:I

    .line 37
    iput p3, p0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->circleType:I

    .line 38
    iput p5, p0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->iconResId:I

    .line 39
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .registers 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->title:Ljava/lang/String;

    return-object v0
.end method
