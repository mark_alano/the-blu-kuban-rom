.class public final Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CircleSpinnerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    .prologue
    .line 48
    const v0, 0x7f0300bf

    const v1, 0x1020014

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 49
    const v0, 0x7f030013

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->setDropDownViewResource(I)V

    .line 50
    return-void
.end method

.method private bindView(Landroid/view/View;I)V
    .registers 8
    .parameter "view"
    .parameter "position"

    .prologue
    .line 67
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;

    .line 68
    .local v1, item:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
    const v3, 0x1020015

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 69
    .local v2, memberCountTextView:Landroid/widget/TextView;
    if-eqz v2, :cond_35

    .line 70
    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->id:Ljava/lang/String;

    if-eqz v3, :cond_4a

    iget v3, v1, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->circleType:I

    const/16 v4, 0xa

    if-eq v3, v4, :cond_4a

    .line 71
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v1, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->count:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    :cond_35
    :goto_35
    const v3, 0x1020006

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 78
    .local v0, iconView:Landroid/widget/ImageView;
    if-eqz v0, :cond_49

    .line 79
    iget v3, v1, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->iconResId:I

    if-nez v3, :cond_4f

    .line 80
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    :cond_49
    :goto_49
    return-void

    .line 73
    .end local v0           #iconView:Landroid/widget/ImageView;
    :cond_4a
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_35

    .line 82
    .restart local v0       #iconView:Landroid/widget/ImageView;
    :cond_4f
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 83
    iget v3, v1, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->iconResId:I

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_49
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 61
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 62
    .local v0, view:Landroid/view/View;
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->bindView(Landroid/view/View;I)V

    .line 63
    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 55
    .local v0, view:Landroid/view/View;
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->bindView(Landroid/view/View;I)V

    .line 56
    return-object v0
.end method
