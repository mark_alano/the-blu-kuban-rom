.class public final Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "DesktopActivityIdLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mDesktopActivityId:Ljava/lang/String;

.field private final mOwnerGaiaId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter "desktopActivityId"
    .parameter "ownerGaiaId"

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 40
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    .line 41
    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mOwnerGaiaId:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .registers 13

    .prologue
    const/4 v11, 0x6

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 46
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 47
    .local v7, desktopIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance v0, Lcom/google/android/apps/plus/api/GetActivityOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mOwnerGaiaId:Ljava/lang/String;

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/GetActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 51
    .local v0, op:Lcom/google/android/apps/plus/api/GetActivityOperation;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->start()V

    .line 52
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v1, :cond_4b

    .line 53
    const-string v1, "DesktopActivityIdLoader"

    invoke-static {v1, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_49

    .line 54
    const-string v1, "DesktopActivityIdLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot resolve desktop activity ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getException()Ljava/lang/Exception;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_49
    :goto_49
    move-object v8, v5

    .line 69
    :goto_4a
    return-object v8

    .line 57
    :cond_4b
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_7e

    .line 58
    const-string v1, "DesktopActivityIdLoader"

    invoke-static {v1, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_49

    .line 59
    const-string v1, "DesktopActivityIdLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot resolve  desktop activity ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/DesktopActivityIdLoader;->mDesktopActivityId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getErrorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_49

    .line 63
    :cond_7e
    new-instance v8, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v10, [Ljava/lang/String;

    const-string v2, "activity_id"

    aput-object v2, v1, v9

    invoke-direct {v8, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    .line 65
    .local v8, matrixCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    new-array v1, v10, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetActivityOperation;->getResponseUpdateId()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {v8, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_4a
.end method
