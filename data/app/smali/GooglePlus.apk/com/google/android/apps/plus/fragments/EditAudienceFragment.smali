.class public Lcom/google/android/apps/plus/fragments/EditAudienceFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "EditAudienceFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;,
        Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AbsListView$OnScrollListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/views/CheckableListItemView$OnItemCheckedChangeListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

.field private mAudienceChangedCallback:Ljava/lang/Runnable;

.field private mAudienceSet:Z

.field private mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

.field private mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private final mCircleContentObserver:Landroid/database/DataSetObserver;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mCircleSelectionEnabled:Z

.field private mCircleUsageType:I

.field private mFilterNullGaiaIds:Z

.field private mIncludePlusPages:Z

.field private mIncomingAudienceIsReadOnly:Z

.field private mListView:Landroid/widget/ListView;

.field private mListener:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;

.field private mLoaderError:Z

.field private mLoadersInitialized:Z

.field private final mSelectedCircles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/CircleData;",
            ">;"
        }
    .end annotation
.end field

.field private final mSelectedPeople:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    .line 304
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    .line 311
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceChangedCallback:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/fragments/CircleNameResolver;
    .registers 2
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncomingAudienceIsReadOnly:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/content/AudienceData;
    .registers 2
    .parameter "x0"

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;)Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;
    .registers 2
    .parameter "x0"

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Lcom/google/android/apps/plus/views/CircleListItemView;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->addToSelectedCircles(Lcom/google/android/apps/plus/views/CircleListItemView;)V

    return-void
.end method

.method private addToSelectedCircles(Lcom/google/android/apps/plus/views/CircleListItemView;)V
    .registers 8
    .parameter "item"

    .prologue
    .line 628
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleId()Ljava/lang/String;

    move-result-object v1

    .line 629
    .local v1, circleId:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleType()I

    move-result v4

    .line 630
    .local v4, type:I
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleName()Ljava/lang/String;

    move-result-object v2

    .line 631
    .local v2, name:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/CircleListItemView;->getMemberCount()I

    move-result v3

    .line 632
    .local v3, size:I
    new-instance v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    .line 633
    .local v0, circle:Lcom/google/android/apps/plus/content/CircleData;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v5, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 634
    return-void
.end method

.method private getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 558
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method private getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;
    .registers 7

    .prologue
    .line 729
    new-instance v1, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 730
    .local v1, circleList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/CircleData;>;"
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, i$:Ljava/util/Iterator;
    :goto_15
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    .line 731
    .local v0, circle:Lcom/google/android/apps/plus/content/CircleData;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_15

    .line 733
    .end local v0           #circle:Lcom/google/android/apps/plus/content/CircleData;
    :cond_25
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 734
    .local v4, userList:Ljava/util/List;,"Ljava/util/List<Lcom/google/android/apps/plus/content/PersonData;>;"
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/PersonData;

    .line 735
    .local v3, person:Lcom/google/android/apps/plus/content/PersonData;
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3a

    .line 737
    .end local v3           #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_4a
    new-instance v5, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v5, v4, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v5
.end method

.method private isLoading()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 439
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    if-eqz v2, :cond_2b

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleSelectionEnabled:Z

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_2b

    :cond_12
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_2b

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_2b

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v2

    if-nez v2, :cond_2c

    :cond_2b
    move v0, v1

    :cond_2c
    return v0
.end method

.method private updateSelectionCount()V
    .registers 10

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;

    if-eqz v0, :cond_13

    .line 491
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isLoading()Z

    move-result v0

    if-eqz v0, :cond_21

    const-string v0, ""

    :goto_10
    invoke-interface {v5, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;->onAudienceChanged(Ljava/lang/String;)V

    .line 495
    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    if-eqz v0, :cond_20

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 498
    :cond_20
    return-void

    .line 491
    :cond_21
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleSelectionEnabled:Z

    if-eqz v0, :cond_a9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_a9

    invoke-interface {v6}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_a9

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a9

    move v0, v1

    move v2, v1

    move v3, v1

    :cond_3c
    const/4 v7, 0x2

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v8, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_53

    const/4 v7, 0x3

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    packed-switch v7, :pswitch_data_ae

    :pswitch_51
    add-int/lit8 v3, v3, 0x1

    :cond_53
    :goto_53
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_3c

    :goto_59
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    add-int/2addr v3, v6

    if-nez v3, :cond_87

    if-eqz v2, :cond_75

    const v0, 0x7f080284

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    :pswitch_6c
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    :pswitch_71
    move v2, v4

    goto :goto_53

    :pswitch_73
    move v0, v4

    goto :goto_53

    :cond_75
    if-eqz v0, :cond_7f

    const v0, 0x7f080285

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    :cond_7f
    const v0, 0x7f080287

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    :cond_87
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0014

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_10

    :cond_a9
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_59

    nop

    :pswitch_data_ae
    .packed-switch 0x5
        :pswitch_73
        :pswitch_51
        :pswitch_71
        :pswitch_6c
        :pswitch_6c
    .end packed-switch
.end method


# virtual methods
.method public final addSelectedCircle(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .registers 4
    .parameter "circleId"
    .parameter "circle"

    .prologue
    .line 755
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 756
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    .line 757
    return-void
.end method

.method public final addSelectedPerson(Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 6
    .parameter "personId"
    .parameter "person"

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 745
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mLoadersInitialized:Z

    if-eqz v0, :cond_12

    .line 746
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 748
    :cond_12
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    .line 749
    return-void
.end method

.method public final getAudience()Lcom/google/android/apps/plus/content/AudienceData;
    .registers 2

    .prologue
    .line 722
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    return-object v0
.end method

.method public final hasAudience()Z
    .registers 2

    .prologue
    .line 680
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceSet:Z

    return v0
.end method

.method protected final isEmpty()Z
    .registers 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 451
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isLoading()Z

    move-result v2

    if-nez v2, :cond_21

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->isPartitionEmpty(I)Z

    move-result v2

    if-eqz v2, :cond_22

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->isPartitionEmpty(I)Z

    move-result v2

    if-eqz v2, :cond_22

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->isPartitionEmpty(I)Z

    move-result v2

    if-eqz v2, :cond_22

    :cond_21
    move v0, v1

    :cond_22
    return v0
.end method

.method public final isSelectionValid()Z
    .registers 2

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .registers 5
    .parameter "activity"

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 371
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onAttach(Landroid/app/Activity;)V

    .line 372
    new-instance v0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    .line 373
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->addPartition(ZZ)V

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->addPartition(ZZ)V

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->addPartition(ZZ)V

    .line 376
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 377
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 379
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 386
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 387
    if-eqz p1, :cond_12

    .line 388
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 390
    :cond_12
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 391
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleSelectionEnabled:Z

    if-eqz v0, :cond_25

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 394
    :cond_25
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 395
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    .line 396
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mLoadersInitialized:Z

    .line 397
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 14
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 505
    packed-switch p1, :pswitch_data_80

    .line 525
    const/4 v0, 0x0

    :goto_9
    return-object v0

    .line 507
    :pswitch_a
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleUsageType:I

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v4, v6

    const-string v5, "circle_name"

    aput-object v5, v4, v7

    const-string v5, "circle_id"

    aput-object v5, v4, v8

    const-string v5, "type"

    aput-object v5, v4, v9

    const-string v5, "contact_count"

    aput-object v5, v4, v10

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_9

    .line 513
    :pswitch_31
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v3, v6

    const-string v4, "name"

    aput-object v4, v3, v7

    const-string v4, "person_id"

    aput-object v4, v3, v8

    const-string v4, "gaia_id"

    aput-object v4, v3, v9

    const-string v4, "packed_circle_ids"

    aput-object v4, v3, v10

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncludePlusPages:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mFilterNullGaiaIds:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_9

    .line 519
    :pswitch_5b
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleNotInCirclesLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    new-array v3, v10, [Ljava/lang/String;

    const-string v4, "_id"

    aput-object v4, v3, v6

    const-string v4, "name"

    aput-object v4, v3, v7

    const-string v4, "person_id"

    aput-object v4, v3, v8

    const-string v4, "gaia_id"

    aput-object v4, v3, v9

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mFilterNullGaiaIds:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PeopleNotInCirclesLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/util/HashMap;Z)V

    goto :goto_9

    .line 505
    nop

    :pswitch_data_80
    .packed-switch 0x0
        :pswitch_5b
        :pswitch_a
        :pswitch_31
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 11
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 414
    const v1, 0x7f030021

    invoke-virtual {p1, v1, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 415
    .local v0, view:Landroid/view/View;
    new-instance v1, Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v5, v4}, Lcom/google/android/apps/plus/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    .line 416
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 417
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceChangedCallback:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    .line 418
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    const v2, 0x7f090058

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 419
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    const v2, 0x7f090057

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 420
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListView:Landroid/widget/ListView;

    .line 421
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 422
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 423
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 424
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 425
    return-object v0
.end method

.method public final onItemCheckedChanged(Lcom/google/android/apps/plus/views/CheckableListItemView;Z)V
    .registers 14
    .parameter "view"
    .parameter "checked"

    .prologue
    .line 566
    instance-of v9, p1, Lcom/google/android/apps/plus/views/CircleListItemView;

    if-eqz v9, :cond_62

    move-object v6, p1

    .line 567
    check-cast v6, Lcom/google/android/apps/plus/views/CircleListItemView;

    .line 568
    .local v6, item:Lcom/google/android/apps/plus/views/CircleListItemView;
    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleId()Ljava/lang/String;

    move-result-object v3

    .line 569
    .local v3, circleId:Ljava/lang/String;
    if-eqz p2, :cond_5c

    .line 570
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 571
    .local v1, activity:Landroid/app/Activity;
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 572
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleType()I

    move-result v9

    invoke-static {v0, v9}, Lcom/google/android/apps/plus/util/AccountsUtil;->isRestrictedCircleForAccount(Lcom/google/android/apps/plus/content/EsAccount;I)Z

    move-result v9

    if-eqz v9, :cond_58

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenMinorPublicExtendedDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v9

    if-nez v9, :cond_58

    .line 574
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v9

    invoke-direct {v2, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 575
    .local v2, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/CircleListItemView;->getCircleName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 576
    const v9, 0x7f080342

    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 578
    const v9, 0x7f0801c4

    new-instance v10, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$3;

    invoke-direct {v10, p0, v6, v1, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Lcom/google/android/apps/plus/views/CircleListItemView;Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v2, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 587
    const v9, 0x7f0801c5

    new-instance v10, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$4;

    invoke-direct {v10, p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/EditAudienceFragment;Lcom/google/android/apps/plus/views/CheckableListItemView;)V

    invoke-virtual {v2, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 595
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 619
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #activity:Landroid/app/Activity;
    .end local v2           #builder:Landroid/app/AlertDialog$Builder;
    .end local v3           #circleId:Ljava/lang/String;
    .end local v6           #item:Lcom/google/android/apps/plus/views/CircleListItemView;
    :cond_54
    :goto_54
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    .line 620
    return-void

    .line 597
    .restart local v0       #account:Lcom/google/android/apps/plus/content/EsAccount;
    .restart local v1       #activity:Landroid/app/Activity;
    .restart local v3       #circleId:Ljava/lang/String;
    .restart local v6       #item:Lcom/google/android/apps/plus/views/CircleListItemView;
    :cond_58
    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->addToSelectedCircles(Lcom/google/android/apps/plus/views/CircleListItemView;)V

    goto :goto_54

    .line 600
    .end local v0           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v1           #activity:Landroid/app/Activity;
    :cond_5c
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_54

    .line 602
    .end local v3           #circleId:Ljava/lang/String;
    .end local v6           #item:Lcom/google/android/apps/plus/views/CircleListItemView;
    :cond_62
    instance-of v9, p1, Lcom/google/android/apps/plus/views/PeopleListItemView;

    if-eqz v9, :cond_54

    move-object v6, p1

    .line 603
    check-cast v6, Lcom/google/android/apps/plus/views/PeopleListItemView;

    .line 604
    .local v6, item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getPersonId()Ljava/lang/String;

    move-result-object v8

    .line 605
    .local v8, personId:Ljava/lang/String;
    if-eqz p2, :cond_9a

    .line 606
    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getGaiaId()Ljava/lang/String;

    move-result-object v5

    .line 607
    .local v5, gaiaId:Ljava/lang/String;
    const/4 v4, 0x0

    .line 608
    .local v4, email:Ljava/lang/String;
    const-string v9, "e:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_90

    .line 609
    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 613
    :cond_81
    :goto_81
    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getContactName()Ljava/lang/String;

    move-result-object v7

    .line 614
    .local v7, name:Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    new-instance v10, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v10, v5, v7, v4}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v8, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_54

    .line 610
    .end local v7           #name:Ljava/lang/String;
    :cond_90
    const-string v9, "p:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_81

    .line 611
    move-object v4, v8

    goto :goto_81

    .line 616
    .end local v4           #email:Ljava/lang/String;
    .end local v5           #gaiaId:Ljava/lang/String;
    :cond_9a
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v9, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_54
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 641
    .local p1, adapter:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    instance-of v0, p2, Landroid/widget/Checkable;

    if-eqz v0, :cond_9

    .line 642
    check-cast p2, Landroid/widget/Checkable;

    .end local p2
    invoke-interface {p2}, Landroid/widget/Checkable;->toggle()V

    .line 644
    :cond_9
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 57
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    if-nez p2, :cond_11

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mLoaderError:Z

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_3c

    :goto_10
    return-void

    :cond_11
    move v0, v2

    goto :goto_7

    :pswitch_13
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->changeCursor(ILandroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateView(Landroid/view/View;)V

    goto :goto_10

    :pswitch_20
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->changeCursor(ILandroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateView(Landroid/view/View;)V

    goto :goto_10

    :pswitch_2e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;

    invoke-virtual {v0, v2, p2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment$EditAudienceAdapter;->changeCursor(ILandroid/database/Cursor;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateView(Landroid/view/View;)V

    goto :goto_10

    nop

    :pswitch_data_3c
    .packed-switch 0x0
        :pswitch_2e
        :pswitch_13
        :pswitch_20
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 555
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onResume()V
    .registers 2

    .prologue
    .line 434
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    .line 435
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateView(Landroid/view/View;)V

    .line 436
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 404
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 405
    const-string v0, "audience"

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudienceFromList()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 406
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .registers 5
    .parameter "view"
    .parameter "firstVisibleItem"
    .parameter "visibleItemCount"
    .parameter "totalItemCount"

    .prologue
    .line 665
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .registers 4
    .parameter "view"
    .parameter "scrollState"

    .prologue
    .line 651
    const/4 v0, 0x2

    if-ne p2, v0, :cond_9

    .line 652
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageCache;->pause()V

    .line 656
    :goto_8
    return-void

    .line 654
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageCache;->resume()V

    goto :goto_8
.end method

.method public final setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 12
    .parameter "audience"

    .prologue
    .line 687
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mAudienceSet:Z

    .line 688
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    .line 689
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    .line 690
    if-eqz p1, :cond_7b

    .line 691
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_15
    if-ge v4, v5, :cond_25

    aget-object v1, v0, v4

    .line 692
    .local v1, circle:Lcom/google/android/apps/plus/content/CircleData;
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedCircles:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 691
    add-int/lit8 v4, v4, 0x1

    goto :goto_15

    .line 695
    .end local v1           #circle:Lcom/google/android/apps/plus/content/CircleData;
    :cond_25
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v5, v0

    const/4 v4, 0x0

    :goto_2b
    if-ge v4, v5, :cond_7b

    aget-object v6, v0, v4

    .line 696
    .local v6, person:Lcom/google/android/apps/plus/content/PersonData;
    const/4 v7, 0x0

    .line 697
    .local v7, personId:Ljava/lang/String;
    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v3

    .line 698
    .local v3, gaiaId:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_53

    .line 699
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "g:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 710
    :cond_49
    :goto_49
    if-eqz v7, :cond_50

    .line 711
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mSelectedPeople:Ljava/util/HashMap;

    invoke-virtual {v8, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    :cond_50
    add-int/lit8 v4, v4, 0x1

    goto :goto_2b

    .line 700
    :cond_53
    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_49

    .line 702
    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v2

    .line 703
    .local v2, email:Ljava/lang/String;
    const-string v8, "p:"

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6b

    .line 704
    move-object v7, v2

    goto :goto_49

    .line 706
    :cond_6b
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "e:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_49

    .line 715
    .end local v0           #arr$:[Lcom/google/android/apps/plus/content/PersonData;
    .end local v2           #email:Ljava/lang/String;
    .end local v3           #gaiaId:Ljava/lang/String;
    .end local v4           #i$:I
    .end local v5           #len$:I
    .end local v6           #person:Lcom/google/android/apps/plus/content/PersonData;
    .end local v7           #personId:Ljava/lang/String;
    :cond_7b
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    .line 716
    return-void
.end method

.method public final setCircleSelectionEnabled(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 343
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleSelectionEnabled:Z

    .line 344
    return-void
.end method

.method public final setCircleUsageType(I)V
    .registers 2
    .parameter "circleUsageType"

    .prologue
    .line 329
    iput p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mCircleUsageType:I

    .line 330
    return-void
.end method

.method public final setFilterNullGaiaIds(Z)V
    .registers 2
    .parameter "filterNullGaiaIds"

    .prologue
    .line 354
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mFilterNullGaiaIds:Z

    .line 355
    return-void
.end method

.method public final setIncludePlusPages(Z)V
    .registers 2
    .parameter "flag"

    .prologue
    .line 336
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncludePlusPages:Z

    .line 337
    return-void
.end method

.method public final setIncomingAudienceIsReadOnly(Z)V
    .registers 2
    .parameter "isReadOnly"

    .prologue
    .line 362
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mIncomingAudienceIsReadOnly:Z

    .line 363
    return-void
.end method

.method public final setOnSelectionChangeListener(Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mListener:Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;

    .line 323
    return-void
.end method

.method protected final updateView(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 462
    const v2, 0x102000a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 463
    .local v0, list:Landroid/view/View;
    const v2, 0x7f090072

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 464
    .local v1, serverError:Landroid/view/View;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->mLoaderError:Z

    if-eqz v2, :cond_22

    .line 465
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 466
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 467
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->showContent(Landroid/view/View;)V

    .line 483
    :goto_1e
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->updateSelectionCount()V

    .line 484
    return-void

    .line 468
    :cond_22
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isLoading()Z

    move-result v2

    if-eqz v2, :cond_32

    .line 469
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 470
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 471
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_1e

    .line 472
    :cond_32
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_48

    .line 473
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 474
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 475
    const v2, 0x7f0801a5

    invoke-static {p1, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 476
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_1e

    .line 478
    :cond_48
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 479
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 480
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->showContent(Landroid/view/View;)V

    goto :goto_1e
.end method
