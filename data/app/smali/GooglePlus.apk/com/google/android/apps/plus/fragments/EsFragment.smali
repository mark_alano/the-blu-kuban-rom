.class public abstract Lcom/google/android/apps/plus/fragments/EsFragment;
.super Landroid/support/v4/app/Fragment;
.source "EsFragment.java"


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field protected mNewerReqId:Ljava/lang/Integer;

.field protected mOlderReqId:Ljava/lang/Integer;

.field private mPaused:Z

.field private mRestoredFragment:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/apps/plus/fragments/EsFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EsFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private doShowEmptyViewProgress(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    .prologue
    const/4 v3, 0x0

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 215
    const v1, 0x1020004

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 216
    .local v0, emptyView:Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 217
    const v1, 0x7f090073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 218
    const v1, 0x7f090066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 220
    .end local v0           #emptyView:Landroid/view/View;
    :cond_27
    return-void
.end method

.method private removeProgressViewMessages()V
    .registers 3

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 297
    return-void
.end method

.method protected static setupEmptyView(Landroid/view/View;I)V
    .registers 4
    .parameter "view"
    .parameter "emptyViewText"

    .prologue
    .line 274
    const v1, 0x7f090073

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 275
    .local v0, etv:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 276
    return-void
.end method


# virtual methods
.method protected final doShowEmptyViewProgressDelayed()V
    .registers 3

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_13

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mPaused:Z

    if-nez v1, :cond_13

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 204
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_13

    .line 205
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/EsFragment;->doShowEmptyViewProgress(Landroid/view/View;)V

    .line 208
    .end local v0           #view:Landroid/view/View;
    :cond_13
    return-void
.end method

.method protected abstract isEmpty()Z
.end method

.method protected final onAsyncData()V
    .registers 3

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 314
    .local v0, activity:Landroid/app/Activity;
    instance-of v1, v0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    if-eqz v1, :cond_d

    .line 315
    check-cast v0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    .end local v0           #activity:Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onAsyncData()V

    .line 317
    :cond_d
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    if-eqz p1, :cond_30

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mRestoredFragment:Z

    .line 77
    const-string v0, "n_pending_req"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 78
    const-string v0, "n_pending_req"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 81
    :cond_1c
    const-string v0, "o_pending_req"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 82
    const-string v0, "o_pending_req"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mOlderReqId:Ljava/lang/Integer;

    .line 85
    :cond_30
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .registers 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"
    .parameter "layoutResId"

    .prologue
    .line 99
    const/4 v1, 0x0

    invoke-virtual {p1, p4, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 101
    .local v0, view:Landroid/view/View;
    return-object v0
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 150
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mPaused:Z

    .line 153
    return-void
.end method

.method public onResume()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 111
    const/4 v0, 0x0

    .line 112
    .local v0, hadPending:Z
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_22

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-eqz v1, :cond_5d

    .line 114
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 123
    :cond_22
    :goto_22
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_3f

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-eqz v1, :cond_61

    .line 125
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3f

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 134
    :cond_3f
    :goto_3f
    if-eqz v0, :cond_59

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v1, :cond_59

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mOlderReqId:Ljava/lang/Integer;

    if-nez v1, :cond_59

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getView()Landroid/view/View;

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_59

    .line 138
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragment;->showEmptyView(Landroid/view/View;)V

    .line 142
    :cond_59
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mPaused:Z

    .line 143
    return-void

    .line 118
    :cond_5d
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 119
    const/4 v0, 0x1

    goto :goto_22

    .line 129
    :cond_61
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mOlderReqId:Ljava/lang/Integer;

    .line 130
    const/4 v0, 0x1

    goto :goto_3f
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 167
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 169
    const-string v0, "n_pending_req"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 172
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_21

    .line 173
    const-string v0, "o_pending_req"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 175
    :cond_21
    return-void
.end method

.method protected final showContent(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->removeProgressViewMessages()V

    .line 264
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 265
    return-void
.end method

.method protected final showEmptyView(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    .prologue
    const/4 v2, 0x0

    .line 253
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->removeProgressViewMessages()V

    .line 254
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2a

    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f090073

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f090066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 255
    :cond_2a
    return-void
.end method

.method protected final showEmptyViewProgress(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    .prologue
    const/4 v3, 0x0

    .line 188
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mRestoredFragment:Z

    if-eqz v0, :cond_1b

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1a

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x320

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 196
    :cond_1a
    :goto_1a
    return-void

    .line 194
    :cond_1b
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->doShowEmptyViewProgress(Landroid/view/View;)V

    goto :goto_1a
.end method

.method protected final showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V
    .registers 4
    .parameter "view"
    .parameter "progressText"

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 242
    const v0, 0x7f090068

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 245
    :cond_15
    return-void
.end method

.method protected updateSpinner(Landroid/widget/ProgressBar;)V
    .registers 3
    .parameter "progressView"

    .prologue
    .line 284
    if-nez p1, :cond_3

    .line 290
    :goto_2
    return-void

    .line 288
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragment;->mOlderReqId:Ljava/lang/Integer;

    if-nez v0, :cond_11

    const/16 v0, 0x8

    :goto_d
    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_2

    :cond_11
    const/4 v0, 0x0

    goto :goto_d
.end method
