.class public abstract Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.super Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.source "EsFragmentActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;,
        Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenuItem;,
        Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenu;
    }
.end annotation


# instance fields
.field private mHideTitleBar:Z

.field private final mMenuItems:[Landroid/view/MenuItem;

.field private final mTitleClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;-><init>()V

    .line 49
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/MenuItem;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    .line 451
    new-instance v0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleClickListener;-><init>(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    .line 495
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)[Landroid/view/MenuItem;
    .registers 2
    .parameter "x0"

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mHideTitleBar:Z

    return v0
.end method

.method private static getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;
    .registers 5
    .parameter "menu"
    .parameter "index"

    .prologue
    .line 871
    const/4 v1, 0x0

    .line 872
    .local v1, visibleItemIndex:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_2
    invoke-interface {p0}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_1e

    .line 873
    invoke-interface {p0, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 874
    if-ne v1, p1, :cond_19

    .line 875
    invoke-interface {p0, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 882
    :goto_18
    return-object v2

    .line 878
    :cond_19
    add-int/lit8 v1, v1, 0x1

    .line 872
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 882
    :cond_1e
    const/4 v2, 0x0

    goto :goto_18
.end method

.method private setupTitleButton1(Landroid/view/MenuItem;)V
    .registers 5
    .parameter "menuItem"

    .prologue
    const/4 v2, 0x0

    .line 773
    const v1, 0x7f09023a

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 775
    .local v0, button:Landroid/widget/ImageButton;
    if-eqz p1, :cond_2e

    .line 776
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 777
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 778
    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 779
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 780
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 785
    :goto_29
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    aput-object p1, v1, v2

    .line 787
    return-void

    .line 782
    :cond_2e
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_29
.end method

.method private setupTitleButton2(Landroid/view/MenuItem;)V
    .registers 5
    .parameter "menuItem"

    .prologue
    .line 795
    const v1, 0x7f09023e

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 797
    .local v0, button:Landroid/widget/ImageButton;
    if-eqz p1, :cond_2f

    .line 798
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 799
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 800
    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 801
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 802
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 807
    :goto_29
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    const/4 v2, 0x1

    aput-object p1, v1, v2

    .line 808
    return-void

    .line 804
    :cond_2f
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_29
.end method

.method private setupTitleButton3(Landroid/view/MenuItem;)V
    .registers 9
    .parameter "menuItem"

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 816
    const v3, 0x7f09023f

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 818
    .local v0, button:Landroid/widget/Button;
    if-eqz p1, :cond_4c

    .line 819
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3, v5, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 820
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 821
    invoke-interface {p1}, Landroid/view/MenuItem;->isEnabled()Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 822
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 823
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 825
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;

    move-result-object v2

    .line 826
    .local v2, text:Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_46

    .line 827
    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 828
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    const/4 v3, -0x2

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 829
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 830
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 831
    invoke-virtual {v0, v6, v4, v6, v4}, Landroid/widget/Button;->setPadding(IIII)V

    .line 837
    .end local v1           #params:Landroid/view/ViewGroup$LayoutParams;
    .end local v2           #text:Ljava/lang/CharSequence;
    :cond_46
    :goto_46
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mMenuItems:[Landroid/view/MenuItem;

    const/4 v4, 0x2

    aput-object p1, v3, v4

    .line 838
    return-void

    .line 834
    :cond_4c
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_46
.end method


# virtual methods
.method public final createTitlebarButtons(I)V
    .registers 8
    .parameter "menuResId"

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 675
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton1(Landroid/view/MenuItem;)V

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton2(Landroid/view/MenuItem;)V

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton3(Landroid/view/MenuItem;)V

    .line 677
    new-instance v1, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenu;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$TitleMenu;-><init>(Landroid/content/Context;)V

    .line 678
    .local v1, menu:Landroid/view/Menu;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 682
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPrepareTitlebarButtons(Landroid/view/Menu;)V

    .line 684
    const/4 v2, 0x0

    .line 685
    .local v2, visibleMenuCount:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1d
    invoke-interface {v1}, Landroid/view/Menu;->size()I

    move-result v3

    if-ge v0, v3, :cond_32

    .line 686
    invoke-interface {v1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/MenuItem;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 687
    add-int/lit8 v2, v2, 0x1

    .line 685
    :cond_2f
    add-int/lit8 v0, v0, 0x1

    goto :goto_1d

    .line 691
    :cond_32
    packed-switch v2, :pswitch_data_7e

    .line 715
    const-string v3, "EsFragmentActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Maximum title buttons is 3. You have "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " visible menu items"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    :goto_4f
    :pswitch_4f
    return-void

    .line 697
    :pswitch_50
    invoke-static {v1, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton3(Landroid/view/MenuItem;)V

    goto :goto_4f

    .line 702
    :pswitch_58
    invoke-static {v1, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton2(Landroid/view/MenuItem;)V

    .line 703
    invoke-static {v1, v5}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton3(Landroid/view/MenuItem;)V

    goto :goto_4f

    .line 708
    :pswitch_67
    invoke-static {v1, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton1(Landroid/view/MenuItem;)V

    .line 709
    invoke-static {v1, v5}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton2(Landroid/view/MenuItem;)V

    .line 710
    const/4 v3, 0x2

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getVisibleItem(Landroid/view/Menu;I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->setupTitleButton3(Landroid/view/MenuItem;)V

    goto :goto_4f

    .line 691
    :pswitch_data_7e
    .packed-switch 0x0
        :pswitch_4f
        :pswitch_50
        :pswitch_58
        :pswitch_67
    .end packed-switch
.end method

.method protected getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;
    .registers 2

    .prologue
    .line 850
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final goHome(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 6
    .parameter "account"

    .prologue
    .line 750
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 752
    .local v0, activityIntent:Landroid/content/Intent;
    if-eqz v0, :cond_2c

    .line 753
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 754
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_2c

    const-string v3, "notif_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2c

    const-string v3, "notif_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2c

    .line 756
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/Intents;->getHostNavigationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    .line 757
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 758
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 759
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->finish()V

    .line 765
    .end local v1           #extras:Landroid/os/Bundle;
    .end local v2           #intent:Landroid/content/Intent;
    :goto_2b
    return-void

    .line 764
    :cond_2c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onBackPressed()V

    goto :goto_2b
.end method

.method protected final hideTitlebar(Z)V
    .registers 7
    .parameter "showAnimation"

    .prologue
    const/4 v4, 0x1

    .line 601
    const v3, 0x7f090236

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 603
    .local v2, titleLayout:Landroid/view/View;
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mHideTitleBar:Z

    if-ne v3, v4, :cond_d

    .line 636
    :goto_c
    return-void

    .line 606
    :cond_d
    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mHideTitleBar:Z

    .line 608
    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 609
    .local v0, currentAnimation:Landroid/view/animation/Animation;
    if-eqz v0, :cond_18

    .line 610
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 613
    :cond_18
    if-eqz p1, :cond_2d

    .line 614
    const v3, 0x7f040003

    invoke-static {p0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 616
    .local v1, titleAnimation:Landroid/view/animation/Animation;
    new-instance v3, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$1;

    invoke-direct {v3, p0, v2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity$1;-><init>(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;Landroid/view/View;)V

    invoke-virtual {v1, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 632
    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_c

    .line 634
    .end local v1           #titleAnimation:Landroid/view/animation/Animation;
    :cond_2d
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_c
.end method

.method protected final isIntentAccountActive()Z
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 505
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 506
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    if-eqz v0, :cond_3e

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3f

    const-string v2, "EsFragmentActivity"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3e

    const-string v2, "EsFragmentActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Activity finished because it is associated with a signed-out account: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3e
    :goto_3e
    return v1

    :cond_3f
    const/4 v1, 0x1

    goto :goto_3e
.end method

.method protected onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 2
    .parameter "menu"

    .prologue
    .line 736
    return-void
.end method

.method protected onTitlebarLabelClick()V
    .registers 1

    .prologue
    .line 742
    return-void
.end method

.method protected final setTitlebarSubtitle(Ljava/lang/String;)V
    .registers 4
    .parameter "subtitle"

    .prologue
    .line 657
    const v1, 0x7f09023c

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 659
    .local v0, textView:Landroid/widget/TextView;
    if-nez p1, :cond_11

    .line 660
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 667
    :goto_10
    return-void

    .line 662
    :cond_11
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 663
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 664
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 665
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_10
.end method

.method protected final setTitlebarTitle(Ljava/lang/String;)V
    .registers 4
    .parameter "title"

    .prologue
    .line 644
    const v1, 0x7f09023b

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 646
    .local v0, textView:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 647
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 648
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 649
    return-void
.end method

.method protected final showTitlebar(Z)V
    .registers 3
    .parameter "enableUp"

    .prologue
    .line 541
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->showTitlebar(ZZ)V

    .line 542
    return-void
.end method

.method protected showTitlebar(ZZ)V
    .registers 10
    .parameter "showAnimation"
    .parameter "enableUp"

    .prologue
    const/4 v5, 0x0

    .line 551
    const v4, 0x7f090236

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 553
    .local v2, titleLayout:Landroid/view/View;
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mHideTitleBar:Z

    if-nez v4, :cond_13

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_13

    .line 586
    :goto_12
    return-void

    .line 556
    :cond_13
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mHideTitleBar:Z

    .line 558
    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 559
    .local v0, currentAnimation:Landroid/view/animation/Animation;
    if-eqz v0, :cond_1e

    .line 560
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 563
    :cond_1e
    if-eqz p1, :cond_2a

    .line 564
    const v4, 0x7f040002

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 566
    .local v1, titleAnimation:Landroid/view/animation/Animation;
    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 569
    .end local v1           #titleAnimation:Landroid/view/animation/Animation;
    :cond_2a
    const v4, 0x7f090238

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz p2, :cond_53

    move v4, v5

    :goto_34
    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    .line 572
    const v4, 0x7f090237

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 573
    .local v3, touchView:Landroid/view/View;
    if-eqz p2, :cond_56

    .line 574
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->mTitleClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 575
    const v4, 0x7f0803e3

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 585
    :goto_4f
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_12

    .line 569
    .end local v3           #touchView:Landroid/view/View;
    :cond_53
    const/16 v4, 0x8

    goto :goto_34

    .line 582
    .restart local v3       #touchView:Landroid/view/View;
    :cond_56
    invoke-virtual {v3, v5}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_4f
.end method
