.class final Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "EsPeopleListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAddPeopleToCirclesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 96
    return-void
.end method

.method public final onCircleSyncComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 78
    return-void
.end method

.method public final onDismissSuggestedPeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 114
    return-void
.end method

.method public final onEventManageGuestComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 122
    return-void
.end method

.method public final onRemovePeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 105
    return-void
.end method

.method public final onSetCircleMemebershipComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 87
    return-void
.end method
