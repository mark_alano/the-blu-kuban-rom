.class public abstract Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "EsPeopleListFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field protected mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

.field private mAvatarsPreloaded:Z

.field private final mCircleContentObserver:Landroid/database/DataSetObserver;

.field protected mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private final mHandler:Landroid/os/Handler;

.field protected mListView:Landroid/widget/ListView;

.field protected mPendingRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    .line 57
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mHandler:Landroid/os/Handler;

    .line 60
    new-instance v0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    .line 70
    new-instance v0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method


# virtual methods
.method protected final addCircleMembership(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .registers 10
    .parameter "personId"
    .parameter "name"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 362
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 365
    invoke-static {p3, v5}, Lcom/google/android/apps/plus/content/EsPeopleData;->getMembershipChangeMessageId(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->showProgressDialog(I)V

    .line 366
    return-void
.end method

.method protected getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method protected abstract getAdapter()Landroid/widget/ListAdapter;
.end method

.method protected abstract getEmptyText()I
.end method

.method protected final handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 385
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_d

    .line 401
    :cond_c
    :goto_c
    return-void

    .line 389
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 391
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1e

    .line 392
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 395
    :cond_1e
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 397
    if-eqz p2, :cond_c

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 398
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0801a8

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_c
.end method

.method protected abstract inflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected abstract isError()Z
.end method

.method protected abstract isLoaded()Z
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 9
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 340
    const/4 v3, -0x1

    if-ne p2, v3, :cond_21

    if-nez p1, :cond_21

    .line 341
    const-string v3, "person_id"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 342
    .local v0, personId:Ljava/lang/String;
    const-string v3, "display_name"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 343
    .local v1, personName:Ljava/lang/String;
    const-string v3, "selected_circle_ids"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 345
    .local v2, selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mHandler:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$3;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 354
    .end local v0           #personId:Ljava/lang/String;
    .end local v1           #personName:Ljava/lang/String;
    .end local v2           #selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_21
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 355
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .registers 5
    .parameter "activity"

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onAttach(Landroid/app/Activity;)V

    .line 135
    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    .line 137
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 139
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 146
    if-eqz p1, :cond_16

    .line 147
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 148
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 151
    :cond_16
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    .line 153
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onInitLoaders(Landroid/os/Bundle;)V

    .line 154
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 188
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->inflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 189
    .local v0, view:Landroid/view/View;
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mListView:Landroid/widget/ListView;

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 193
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 194
    return-object v0
.end method

.method protected abstract onInitLoaders(Landroid/os/Bundle;)V
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .registers 3
    .parameter "view"

    .prologue
    .line 199
    instance-of v0, p1, Lcom/google/android/apps/plus/views/Recyclable;

    if-eqz v0, :cond_9

    .line 200
    check-cast p1, Lcom/google/android/apps/plus/views/Recyclable;

    .end local p1
    invoke-interface {p1}, Lcom/google/android/apps/plus/views/Recyclable;->onRecycle()V

    .line 202
    :cond_9
    return-void
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 300
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onPause()V

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 302
    return-void
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 282
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 285
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2e

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2e

    .line 287
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 288
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 289
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 292
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_2e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->updateView(Landroid/view/View;)V

    .line 293
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 161
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 163
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 165
    :cond_12
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .registers 5
    .parameter "view"
    .parameter "firstVisibleItem"
    .parameter "visibleItemCount"
    .parameter "totalItemCount"

    .prologue
    .line 323
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .registers 4
    .parameter "view"
    .parameter "scrollState"

    .prologue
    .line 309
    const/4 v0, 0x2

    if-ne p2, v0, :cond_9

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageCache;->pause()V

    .line 314
    :goto_8
    return-void

    .line 312
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageCache;->resume()V

    goto :goto_8
.end method

.method final preloadAvatars(Landroid/database/Cursor;I)V
    .registers 7
    .parameter "cursor"
    .parameter "gaiaIdColumnIndex"

    .prologue
    .line 216
    if-eqz p1, :cond_6

    .line 217
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mAvatarsPreloaded:Z

    if-eqz v2, :cond_7

    .line 234
    :cond_6
    :goto_6
    return-void

    .line 220
    :cond_7
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 221
    .local v1, requests:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/content/AvatarRequest;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_28

    .line 223
    :cond_12
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, gaiaId:Ljava/lang/String;
    if-eqz v0, :cond_22

    .line 225
    new-instance v2, Lcom/google/android/apps/plus/content/AvatarRequest;

    const/4 v3, 0x2

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    :cond_22
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_12

    .line 229
    .end local v0           #gaiaId:Ljava/lang/String;
    :cond_28
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_33

    .line 230
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mAvatarCache:Lcom/google/android/apps/plus/service/ImageCache;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/service/ImageCache;->preloadAvatarsInBackground(Ljava/util/List;)V

    .line 232
    :cond_33
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mAvatarsPreloaded:Z

    goto :goto_6
.end method

.method protected final showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "personId"
    .parameter "name"

    .prologue
    const/4 v2, 0x0

    .line 329
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {v0, v1, p1, p2, v2}, Lcom/google/android/apps/plus/phone/Intents;->getCircleMembershipActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 333
    return-void
.end method

.method protected final showProgressDialog(I)V
    .registers 6
    .parameter "message"

    .prologue
    .line 372
    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    .line 375
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 376
    return-void
.end method

.method protected updateView(Landroid/view/View;)V
    .registers 6
    .parameter "view"

    .prologue
    const v3, 0x7f090072

    const/4 v2, 0x0

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->isLoaded()Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v1

    if-nez v1, :cond_19

    :cond_12
    const/4 v1, 0x1

    :goto_13
    if-eqz v1, :cond_1b

    .line 259
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 275
    :goto_18
    return-void

    :cond_19
    move v1, v2

    .line 258
    goto :goto_13

    .line 261
    :cond_1b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->isError()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 262
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 271
    :cond_28
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->showContent(Landroid/view/View;)V

    goto :goto_18

    .line 265
    :cond_2c
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_28

    .line 267
    const v1, 0x7f090073

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 268
    .local v0, emptyText:Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->getEmptyText()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 269
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_18
.end method
