.class final Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedAlbumsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGetAlbumListComplete$6a63df5(I)V
    .registers 4
    .parameter "requestId"

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_20

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getView()Landroid/view/View;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->updateView(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;Landroid/view/View;)V

    .line 82
    :cond_20
    return-void
.end method

.method public final onPhotoImageLoaded$b81653(Lcom/google/android/apps/plus/api/MediaRef;Landroid/graphics/Bitmap;I)V
    .registers 4
    .parameter "ref"
    .parameter "bitmap"
    .parameter "cropType"

    .prologue
    .line 66
    return-void
.end method

.method public final onPhotosHomeComplete$6a63df5(I)V
    .registers 4
    .parameter "requestId"

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_20

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->mOlderReqId:Ljava/lang/Integer;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->getView()Landroid/view/View;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->updateView(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;->access$000(Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;Landroid/view/View;)V

    .line 74
    :cond_20
    return-void
.end method
