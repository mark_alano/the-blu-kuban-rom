.class final Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;
.super Ljava/lang/Object;
.source "HostedEventFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedEventFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SettingsLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 1347
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 1347
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 5
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1350
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks$1;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;Landroid/content/Context;)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 7
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 1347
    check-cast p2, Ljava/lang/String;

    .end local p2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_45

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$1600(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/android/apps/plus/fragments/EventActiveState;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$1600(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/android/apps/plus/fragments/EventActiveState;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$1100(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    iput-boolean v3, v2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$1600(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/android/apps/plus/fragments/EventActiveState;

    move-result-object v2

    iget-boolean v2, v2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    if-eq v1, v2, :cond_45

    const v1, 0x7f090042

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;

    if-eqz v0, :cond_45

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedEventFragment$SettingsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedEventFragment;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;->access$1600(Lcom/google/android/apps/plus/fragments/HostedEventFragment;)Lcom/google/android/apps/plus/fragments/EventActiveState;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->bind(Lcom/google/android/apps/plus/fragments/EventActiveState;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->invalidate()V

    :cond_45
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1389
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Ljava/lang/String;>;"
    return-void
.end method
