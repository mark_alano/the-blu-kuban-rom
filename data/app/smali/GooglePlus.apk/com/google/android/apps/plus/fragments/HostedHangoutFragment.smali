.class public Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedHangoutFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$5;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$HangoutSuggestionsQuery;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleQuery;,
        Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;"
    }
.end annotation


# static fields
.field private static final ACTIVE_HANGOUT_MODE_DEFAULT:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;


# instance fields
.field private displayedSuggestedParticipants:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

.field private mAudienceOverlay:Landroid/view/View;

.field protected mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

.field protected mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

.field private mCacheSuggestionsResponse:Z

.field private mCircleUsageType:I

.field private mFilterNullGaiaIds:Z

.field private mGridView:Landroid/widget/GridView;

.field private mIncludePhoneOnlyContacts:Z

.field private mIncludePlusPages:Z

.field private mListHeader:Landroid/widget/TextView;

.field private mListParent:Landroid/view/View;

.field private mPreviouslyAudienceEmpty:Z

.field private mPreviouslyOvercapacity:Z

.field private mPublicProfileSearchEnabled:Z

.field private mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

.field private mRequestId:Ljava/lang/Integer;

.field private mResumeHangoutButton:Landroid/widget/Button;

.field private mRingBeforeDisable:Z

.field protected mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

.field private mShakeDetectorWasRunning:Z

.field private mShowSuggestedPeople:Z

.field private mStartHangoutButton:Landroid/widget/Button;

.field private mSuggestedPeople:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

.field private mSuggestedPeopleScrollView:Landroid/widget/ScrollView;

.field private mSuggestedPeopleSize:I

.field private mToggleHangoutRingButton:Landroid/widget/ImageButton;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->MODE_DISABLE:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->ACTIVE_HANGOUT_MODE_DEFAULT:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    return-void
.end method

.method public constructor <init>()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    .line 104
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    .line 108
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    .line 123
    sget-object v0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->ACTIVE_HANGOUT_MODE_DEFAULT:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    .line 126
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleSize:I

    .line 570
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyAudienceEmpty:Z

    .line 571
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyOvercapacity:Z

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->updateSuggestedPeopleDisplay()V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/ImageButton;
    .registers 2
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;ZZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0, v0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->disableHangoutRing(ZZ)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyAudienceEmpty:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyAudienceEmpty:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyOvercapacity:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPreviouslyOvercapacity:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->enableHangoutRing(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/Button;
    .registers 2
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isInAudience(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V
    .registers 3
    .parameter "x0"

    .prologue
    const/4 v1, 0x1

    .line 71
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRingBeforeDisable:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->disableHangoutRing(ZZ)V

    :cond_9
    :goto_9
    return-void

    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->audienceSizeIsGreaterThan(I)Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-direct {p0, v1, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->disableHangoutRing(ZZ)V

    goto :goto_9

    :cond_16
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isAudienceEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->enableHangoutRing(Z)V

    goto :goto_9
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRingBeforeDisable:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;
    .registers 2
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    return-object v0
.end method

.method private cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .registers 4
    .parameter "response"

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    if-eqz v0, :cond_10

    .line 261
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/content/EsAudienceData;->processSuggestionsResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    .line 262
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    .line 264
    :cond_10
    return-void
.end method

.method private disableHangoutRing(ZZ)V
    .registers 5
    .parameter "overcapacity"
    .parameter "toast"

    .prologue
    .line 656
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRingBeforeDisable:Z

    .line 657
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    const v1, 0x7f02016b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 658
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    const v1, 0x7f0803eb

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 660
    if-eqz p2, :cond_21

    .line 661
    if-eqz p1, :cond_22

    const v0, 0x7f0800d8

    :goto_1e
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->toast(I)V

    .line 664
    :cond_21
    return-void

    .line 661
    :cond_22
    const v0, 0x7f0800d7

    goto :goto_1e
.end method

.method private enableHangoutRing(Z)V
    .registers 4
    .parameter "toast"

    .prologue
    .line 646
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRingBeforeDisable:Z

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    const v1, 0x7f02016c

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 648
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    const v1, 0x7f0803ec

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 650
    if-eqz p1, :cond_1f

    .line 651
    const v0, 0x7f0800d6

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->toast(I)V

    .line 653
    :cond_1f
    return-void
.end method

.method private isInAudience(Ljava/lang/String;)Z
    .registers 9
    .parameter "participantId"

    .prologue
    .line 318
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    .line 319
    .local v1, audience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_c
    if-ge v4, v5, :cond_21

    aget-object v3, v0, v4

    .line 320
    .local v3, audiencePerson:Lcom/google/android/apps/plus/content/PersonData;
    invoke-static {v3}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantIdFromPerson(Lcom/google/android/apps/plus/content/PersonData;)Ljava/lang/String;

    move-result-object v2

    .line 322
    .local v2, audienceParticipantId:Ljava/lang/String;
    if-eqz v2, :cond_1e

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 324
    const/4 v6, 0x1

    .line 328
    .end local v2           #audienceParticipantId:Ljava/lang/String;
    .end local v3           #audiencePerson:Lcom/google/android/apps/plus/content/PersonData;
    :goto_1d
    return v6

    .line 319
    .restart local v2       #audienceParticipantId:Ljava/lang/String;
    .restart local v3       #audiencePerson:Lcom/google/android/apps/plus/content/PersonData;
    :cond_1e
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 328
    .end local v2           #audienceParticipantId:Ljava/lang/String;
    .end local v3           #audiencePerson:Lcom/google/android/apps/plus/content/PersonData;
    :cond_21
    const/4 v6, 0x0

    goto :goto_1d
.end method

.method private loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V
    .registers 8
    .parameter "response"

    .prologue
    .line 245
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getSuggestionList()Ljava/util/List;

    move-result-object v4

    .line 247
    .local v4, suggestions:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Client$Suggestion;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;

    .line 248
    .local v3, suggestion:Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Client$Suggestion;->getSuggestedUserList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_1c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 249
    .local v2, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1c

    .line 253
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v3           #suggestion:Lcom/google/wireless/realtimechat/proto/Client$Suggestion;
    :cond_2e
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->updateSuggestedPeopleDisplay()V

    .line 254
    return-void
.end method

.method private toast(I)V
    .registers 5
    .parameter "resId"

    .prologue
    .line 667
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 668
    .local v0, toastText:Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 669
    return-void
.end method

.method private updateSuggestedPeopleDisplay()V
    .registers 16

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 278
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_8
    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 280
    .local v9, suggestedParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    const/4 v2, 0x0

    .line 281
    .local v2, found:Z
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->displayedSuggestedParticipants:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :cond_1b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_36

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 282
    .local v0, audiencePerson:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v8

    .line 283
    .local v8, participantId:Ljava/lang/String;
    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1b

    .line 284
    const/4 v2, 0x1

    .line 289
    .end local v0           #audiencePerson:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v8           #participantId:Ljava/lang/String;
    :cond_36
    if-nez v2, :cond_8

    .line 290
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->displayedSuggestedParticipants:Ljava/util/List;

    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    if-eqz v10, :cond_8

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    invoke-virtual {v10}, Landroid/widget/TextView;->getVisibility()I

    move-result v10

    if-eqz v10, :cond_8

    .line 293
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    .line 298
    .end local v2           #found:Z
    .end local v4           #i$:Ljava/util/Iterator;
    .end local v9           #suggestedParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_4f
    const/4 v5, 0x0

    .line 299
    .local v5, id:I
    new-instance v7, Landroid/database/MatrixCursor;

    sget-object v10, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleQuery;->columnNames:[Ljava/lang/String;

    invoke-direct {v7, v10}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 300
    .local v7, matrixCursor:Landroid/database/MatrixCursor;
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->displayedSuggestedParticipants:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, i$:Ljava/util/Iterator;
    :goto_5d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_9a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 301
    .local v1, displayedSuggestedParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    const/4 v10, 0x4

    new-array v13, v10, [Ljava/lang/Object;

    add-int/lit8 v6, v5, 0x1

    .end local v5           #id:I
    .local v6, id:I
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v13, v12

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v13, v11

    const/4 v10, 0x2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v10

    const/4 v14, 0x3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isInAudience(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_98

    move v10, v11

    :goto_8d
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v13, v14

    invoke-virtual {v7, v13}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v5, v6

    .end local v6           #id:I
    .restart local v5       #id:I
    goto :goto_5d

    .end local v5           #id:I
    .restart local v6       #id:I
    :cond_98
    move v10, v12

    goto :goto_8d

    .line 307
    .end local v1           #displayedSuggestedParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .end local v6           #id:I
    .restart local v5       #id:I
    :cond_9a
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v10, v7}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 310
    iget v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleSize:I

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->getCount()I

    move-result v11

    if-eq v10, v11, :cond_c4

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->getCount()I

    move-result v10

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v11}, Landroid/widget/GridView;->getChildCount()I

    move-result v11

    if-ne v10, v11, :cond_c4

    .line 312
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v10, v12, v12}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 313
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->getCount()I

    move-result v10

    iput v10, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleSize:I

    .line 315
    :cond_c4
    return-void
.end method


# virtual methods
.method public final audienceSizeIsGreaterThan(I)Z
    .registers 12
    .parameter "size"

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 781
    const/4 v3, 0x0

    .line 783
    .local v3, count:I
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    .line 784
    .local v1, audience:Lcom/google/android/apps/plus/content/AudienceData;
    if-nez v1, :cond_c

    .line 804
    :cond_b
    :goto_b
    return v6

    .line 788
    :cond_c
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v8

    if-lez v8, :cond_18

    .line 789
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v8

    add-int/lit8 v3, v8, 0x0

    .line 792
    :cond_18
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_1e
    if-ge v4, v5, :cond_49

    aget-object v2, v0, v4

    .line 793
    .local v2, circle:Lcom/google/android/apps/plus/content/CircleData;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v8

    const/16 v9, 0x9

    if-eq v8, v9, :cond_39

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v8

    const/4 v9, 0x7

    if-eq v8, v9, :cond_39

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_3b

    :cond_39
    move v6, v7

    .line 796
    goto :goto_b

    .line 799
    :cond_3b
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v8

    if-lez v8, :cond_46

    .line 800
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v8

    add-int/2addr v3, v8

    .line 792
    :cond_46
    add-int/lit8 v4, v4, 0x1

    goto :goto_1e

    .line 804
    .end local v2           #circle:Lcom/google/android/apps/plus/content/CircleData;
    :cond_49
    const/16 v8, 0xa

    if-le v3, v8, :cond_b

    move v6, v7

    goto :goto_b
.end method

.method public final getAudience()Lcom/google/android/apps/plus/content/AudienceData;
    .registers 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    return-object v0
.end method

.method protected final getSuggestedPeople()V
    .registers 6

    .prologue
    .line 672
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    .line 673
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->isAudienceEmpty()Z

    move-result v1

    .line 675
    .local v1, emptyAudience:Z
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    .line 676
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;->HANGOUT:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;

    invoke-static {v2, v3, v0, v4}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->requestSuggestedParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest$SuggestionsType;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    .line 678
    if-eqz v1, :cond_29

    .line 681
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 683
    :cond_29
    return-void
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 995
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final isAudienceEmpty()Z
    .registers 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 755
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    .line 756
    .local v1, audience:Lcom/google/android/apps/plus/content/AudienceData;
    if-nez v1, :cond_b

    .line 769
    :cond_a
    :goto_a
    return v5

    .line 758
    :cond_b
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v7

    if-lez v7, :cond_13

    move v5, v6

    .line 759
    goto :goto_a

    .line 761
    :cond_13
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    .local v0, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_19
    if-ge v3, v4, :cond_a

    aget-object v2, v0, v3

    .line 762
    .local v2, circle:Lcom/google/android/apps/plus/content/CircleData;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v7

    if-gtz v7, :cond_32

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    const/16 v8, 0x9

    if-eq v7, v8, :cond_32

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    const/4 v8, 0x7

    if-ne v7, v8, :cond_34

    :cond_32
    move v5, v6

    .line 765
    goto :goto_a

    .line 761
    :cond_34
    add-int/lit8 v3, v3, 0x1

    goto :goto_19
.end method

.method public final isEmpty()Z
    .registers 2

    .prologue
    .line 712
    const/4 v0, 0x0

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 333
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 334
    if-nez p1, :cond_1c

    .line 335
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 336
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "audience"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    .line 338
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    if-eqz v0, :cond_1c

    .line 339
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 342
    .end local v0           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v1           #intent:Landroid/content/Intent;
    :cond_1c
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 744
    const/4 v0, 0x1

    if-ne p1, v0, :cond_12

    .line 745
    const/4 v0, -0x1

    if-ne p2, v0, :cond_12

    if-eqz p3, :cond_12

    .line 746
    const-string v0, "audience"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    .line 749
    :cond_12
    return-void
.end method

.method public final onAddPersonToCirclesAction(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 4
    .parameter "personId"
    .parameter "name"
    .parameter "forSharing"

    .prologue
    .line 836
    return-void
.end method

.method public final onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "personId"
    .parameter "name"

    .prologue
    .line 845
    return-void
.end method

.method public final onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .registers 4
    .parameter "circleId"
    .parameter "circle"

    .prologue
    .line 824
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addCircle(Lcom/google/android/apps/plus/content/CircleData;)V

    .line 825
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    .line 826
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 11
    .parameter "v"

    .prologue
    .line 729
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090057

    if-ne v0, v1, :cond_2e

    .line 730
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    .line 731
    .local v3, audience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const v2, 0x7f080283

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCircleUsageType:I

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mFilterNullGaiaIds:Z

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 737
    .end local v3           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    :cond_2e
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 466
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 467
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeople:Ljava/util/List;

    .line 468
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->displayedSuggestedParticipants:Ljava/util/List;

    .line 469
    if-eqz p1, :cond_51

    .line 470
    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_72

    .line 471
    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    .line 472
    const-string v1, "cache_suggestions_response"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    .line 478
    :goto_31
    const-string v1, "show_suggested_people"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    .line 480
    const-string v1, "public_profile_search"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    .line 482
    const-string v1, "phone_only_contacts"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    .line 484
    const-string v1, "plus_pages"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    .line 488
    :cond_51
    const/16 v1, 0xa

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCircleUsageType:I

    .line 489
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    .line 490
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    .line 491
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    .line 492
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    .line 493
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mFilterNullGaiaIds:Z

    .line 495
    sget-object v1, Lcom/google/android/apps/plus/util/Property;->ACTIVE_HANGOUT_MODE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v0

    .line 496
    .local v0, activeHangoutMode:Ljava/lang/String;
    const-string v1, "disable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_78

    .line 497
    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->MODE_DISABLE:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    .line 505
    :goto_71
    return-void

    .line 475
    .end local v0           #activeHangoutMode:Ljava/lang/String;
    :cond_72
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    .line 476
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    goto :goto_31

    .line 498
    .restart local v0       #activeHangoutMode:Ljava/lang/String;
    :cond_78
    const-string v1, "hide"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_85

    .line 499
    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->MODE_HIDE:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    goto :goto_71

    .line 500
    :cond_85
    const-string v1, "none"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_92

    .line 501
    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->MODE_NONE:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    goto :goto_71

    .line 503
    :cond_92
    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->ACTIVE_HANGOUT_MODE_DEFAULT:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    goto :goto_71
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 11
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 857
    const-string v0, "HangoutFrag"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 858
    const-string v0, "HangoutFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "onCreateLoader "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    :cond_1e
    const/4 v0, 0x1

    if-ne p1, v0, :cond_39

    .line 861
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->HANGOUT_SUGGESTIONS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 863
    .local v2, suggestionsUri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$HangoutSuggestionsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v6, "sequence ASC"

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 868
    .end local v2           #suggestionsUri:Landroid/net/Uri;
    :goto_38
    return-object v0

    :cond_39
    move-object v0, v4

    goto :goto_38
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 9
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 513
    const v1, 0x7f030050

    invoke-virtual {p1, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 514
    .local v0, view:Landroid/view/View;
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mGridView:Landroid/widget/GridView;

    .line 515
    const v1, 0x7f090053

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleScrollView:Landroid/widget/ScrollView;

    .line 517
    const v1, 0x7f090054

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    .line 518
    const v1, 0x7f090055

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    .line 519
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1, p0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 520
    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    .line 521
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mGridView:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 523
    const v1, 0x7f09010f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    .line 524
    invoke-direct {p0, v4, v4}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->disableHangoutRing(ZZ)V

    .line 525
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 532
    const v1, 0x7f090110

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    .line 533
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 534
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 544
    const v1, 0x7f090111

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mResumeHangoutButton:Landroid/widget/Button;

    .line 545
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mResumeHangoutButton:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 558
    const v1, 0x7f09010e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    .line 559
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 560
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 561
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xc

    if-lt v1, v2, :cond_af

    .line 562
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 564
    :cond_af
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 565
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 567
    return-object v0
.end method

.method public final onDismissSuggestionAction(Ljava/lang/String;)V
    .registers 2
    .parameter "personId"

    .prologue
    .line 853
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 721
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->onItemClick(I)V

    .line 722
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 8
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 71
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    const-string v0, "HangoutFrag"

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_24

    const-string v0, "HangoutFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadFinished "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_24
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v3, :cond_61

    if-eqz p2, :cond_61

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_61

    :cond_32
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeople:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_32

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->updateSuggestedPeopleDisplay()V

    :cond_61
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 898
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 6
    .parameter "item"

    .prologue
    .line 976
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_2c

    .line 984
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_b
    return v1

    .line 978
    :pswitch_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080411

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 980
    .local v0, helpUrlParam:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->startExternalActivity(Landroid/content/Intent;)V

    .line 981
    const/4 v1, 0x1

    goto :goto_b

    .line 976
    nop

    :pswitch_data_2c
    .packed-switch 0x7f090290
        :pswitch_c
    .end packed-switch
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 455
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    .line 456
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    if-eqz v0, :cond_c

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 459
    :cond_c
    return-void
.end method

.method public final onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 5
    .parameter "personId"
    .parameter "contactLookupKey"
    .parameter "person"

    .prologue
    .line 830
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->addPerson(Lcom/google/android/apps/plus/content/PersonData;)V

    .line 831
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->clearText()V

    .line 832
    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 3
    .parameter "actionBar"

    .prologue
    .line 968
    const v0, 0x7f08005a

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    .line 969
    return-void
.end method

.method public final onResume()V
    .registers 7

    .prologue
    const/4 v5, 0x4

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 400
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    .line 403
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mResumeHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    const v2, 0x7f0801fa

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 404
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v1

    if-eqz v1, :cond_57

    .line 406
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mStartHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 407
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mToggleHangoutRingButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 408
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mResumeHangoutButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 411
    sget-object v1, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$5;->$SwitchMap$com$google$android$apps$plus$fragments$HostedHangoutFragment$ActiveHangoutMode:[I

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mActiveViewMode:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$ActiveHangoutMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_ca

    .line 419
    :cond_57
    :goto_57
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_65

    .line 429
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 430
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceResult:Lcom/google/android/apps/plus/content/AudienceData;

    .line 432
    :cond_65
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    if-eqz v1, :cond_6e

    .line 433
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRealTimeChatListener:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$RTCListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 435
    :cond_6e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_b7

    .line 436
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_b7

    .line 439
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->removeResult(I)Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    move-result-object v0

    .line 440
    .local v0, result:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    if-eqz v0, :cond_b7

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getErrorCode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_b7

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    if-eqz v1, :cond_b7

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSuggestionsResponse()Z

    move-result v1

    if-eqz v1, :cond_b7

    .line 443
    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->loadSuggestedPeople(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    .line 444
    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->cacheSuggestedResponse(Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)V

    .line 448
    .end local v0           #result:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    :cond_b7
    return-void

    .line 413
    :pswitch_b8
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setVisibility(I)V

    .line 414
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_57

    .line 418
    :pswitch_c3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceOverlay:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_57

    .line 411
    nop

    :pswitch_data_ca
    .packed-switch 0x1
        :pswitch_b8
        :pswitch_c3
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 693
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 694
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_c

    .line 695
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 697
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_22

    .line 698
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 699
    const-string v0, "cache_suggestions_response"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCacheSuggestionsResponse:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 701
    :cond_22
    const-string v0, "show_suggested_people"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 702
    const-string v0, "public_profile_search"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 703
    const-string v0, "phone_only_contacts"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 704
    const-string v0, "plus_pages"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 705
    return-void
.end method

.method public final onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .registers 4
    .parameter "adapter"

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    if-eqz v0, :cond_10

    .line 814
    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 815
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 820
    :cond_10
    :goto_10
    return-void

    .line 817
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListParent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_10
.end method

.method public final onStart()V
    .registers 3

    .prologue
    .line 349
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStart()V

    .line 350
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v1, :cond_c

    .line 351
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStart()V

    .line 355
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 356
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_1c

    .line 357
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->stop()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShakeDetectorWasRunning:Z

    .line 359
    :cond_1c
    return-void
.end method

.method public final onStop()V
    .registers 3

    .prologue
    .line 366
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStart()V

    .line 367
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v1, :cond_c

    .line 368
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStop()V

    .line 372
    :cond_c
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShakeDetectorWasRunning:Z

    if-eqz v1, :cond_1d

    .line 373
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 374
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_1d

    .line 375
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->start()Z

    .line 378
    .end local v0           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_1d
    return-void
.end method

.method public final onUnblockPersonAction(Ljava/lang/String;Z)V
    .registers 3
    .parameter "personId"
    .parameter "isPlusPage"

    .prologue
    .line 849
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 8
    .parameter "view"
    .parameter "savedInstanceState"

    .prologue
    .line 575
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 576
    const v1, 0x7f090052

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    .line 577
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    const v2, 0x7f0801f9

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setEmptyAudienceHint(I)V

    .line 579
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0f003d

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 581
    .local v0, themeContext:Landroid/content/Context;
    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    .line 583
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePhoneOnlyContacts:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePhoneNumberContacts(Z)V

    .line 584
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mIncludePlusPages:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePlusPages(Z)V

    .line 585
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mPublicProfileSearchEnabled:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setPublicProfileSearchEnabled(Z)V

    .line 586
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mCircleUsageType:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setCircleUsageType(I)V

    .line 587
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mFilterNullGaiaIds:Z

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setFilterNullGaiaIds(Z)V

    .line 588
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setListener(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;)V

    .line 589
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onCreate(Landroid/os/Bundle;)V

    .line 590
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAutoCompleteAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;)V

    .line 591
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 592
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->initLoaders(Landroid/support/v4/app/LoaderManager;)V

    .line 594
    const v1, 0x7f090057

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 595
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mAudienceView:Lcom/google/android/apps/plus/views/TypeableAudienceView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    .line 627
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mShowSuggestedPeople:Z

    if-eqz v1, :cond_a5

    .line 628
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mSuggestedPeopleAdapter:Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment$SuggestedPeopleAdpater;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    if-eqz v1, :cond_a2

    .line 629
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->mListHeader:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 631
    :cond_a2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedHangoutFragment;->getSuggestedPeople()V

    .line 633
    :cond_a5
    return-void
.end method
