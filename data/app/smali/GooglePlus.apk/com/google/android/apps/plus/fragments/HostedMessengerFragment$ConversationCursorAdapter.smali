.class final Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;
.super Lcom/android/common/widget/CompositeCursorAdapter;
.source "HostedMessengerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConversationCursorAdapter"
.end annotation


# instance fields
.field private mConversationsCursor:Landroid/database/Cursor;

.field private mSuggestionsCursor:Landroid/database/Cursor;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;Landroid/content/Context;Landroid/widget/AbsListView;)V
    .registers 6
    .parameter
    .parameter "context"
    .parameter "listView"

    .prologue
    const/4 v1, 0x0

    .line 357
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    .line 358
    invoke-direct {p0, p2, v1}, Lcom/android/common/widget/CompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    .line 359
    invoke-virtual {p0, v1, v1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->addPartition(ZZ)V

    .line 360
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->addPartition(ZZ)V

    .line 362
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;)V

    invoke-virtual {p3, v0}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 375
    return-void
.end method


# virtual methods
.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;I)V
    .registers 15
    .parameter "view"
    .parameter "partition"
    .parameter "cursor"
    .parameter "position"

    .prologue
    const/4 v6, 0x7

    const/4 v9, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 446
    if-eqz p3, :cond_c

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 459
    :cond_c
    :goto_c
    return-void

    .line 449
    :cond_d
    packed-switch p2, :pswitch_data_14a

    goto :goto_c

    :pswitch_11
    move-object v0, p1

    .line 451
    check-cast v0, Lcom/google/android/apps/plus/views/ConversationListItemView;

    .line 452
    .local v0, cliv:Lcom/google/android/apps/plus/views/ConversationListItemView;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->clear()V

    const/16 v2, 0xb

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v2, 0xd

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v3, :cond_a3

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setConversationName(Ljava/lang/CharSequence;)V

    const/16 v2, 0xf

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_36

    const-string v2, ""

    :cond_36
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0802c0

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v2, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setLastMessage(Ljava/lang/CharSequence;)V

    :cond_48
    :goto_48
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v5, 0x4

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v2, v5, v6}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setTimeSince(Ljava/lang/CharSequence;)V

    const/4 v2, 0x5

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setUnreadCount(I)V

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v3, :cond_111

    move v2, v3

    :goto_6a
    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setMuted(Z)V

    const/16 v2, 0x10

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_124

    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, "|"

    invoke-direct {v3, v2, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    :cond_81
    :goto_81
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_114

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_81

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    iget-object v5, v5, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_81

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_81

    :cond_a3
    const/4 v2, 0x6

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_ae

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    :cond_ae
    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setConversationName(Ljava/lang/CharSequence;)V

    const/16 v2, 0xc

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/16 v2, 0x8

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v2, 0x9

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    if-nez v5, :cond_148

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f08020d

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_d4
    if-eqz v8, :cond_ea

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f080212

    new-array v7, v3, [Ljava/lang/Object;

    aput-object v2, v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setLastMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_48

    :cond_ea
    if-eqz v7, :cond_48

    if-ne v6, v3, :cond_104

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f080211

    new-array v8, v9, [Ljava/lang/Object;

    aput-object v2, v8, v4

    aput-object v7, v8, v3

    invoke-virtual {v5, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setLastMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_48

    :cond_104
    const-string v2, "\\<.*?\\>"

    const-string v5, ""

    invoke-virtual {v7, v2, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setLastMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_48

    :cond_111
    move v2, v4

    goto/16 :goto_6a

    :cond_114
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setParticipantsId(Ljava/util/List;Ljava/lang/String;)V

    :goto_11f
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->updateContentDescription()V

    goto/16 :goto_c

    :cond_124
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setParticipantsId(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_11f

    .end local v0           #cliv:Lcom/google/android/apps/plus/views/ConversationListItemView;
    :pswitch_131
    move-object v1, p1

    .line 455
    check-cast v1, Lcom/google/android/apps/plus/views/SuggestedParticipantView;

    .line 456
    .local v1, spv:Lcom/google/android/apps/plus/views/SuggestedParticipantView;
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->setParticipantId(Ljava/lang/String;)V

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->setParticipantName(Ljava/lang/CharSequence;)V

    goto/16 :goto_c

    .end local v1           #spv:Lcom/google/android/apps/plus/views/SuggestedParticipantView;
    .restart local v0       #cliv:Lcom/google/android/apps/plus/views/ConversationListItemView;
    :cond_148
    move-object v2, v5

    goto :goto_d4

    .line 449
    :pswitch_data_14a
    .packed-switch 0x0
        :pswitch_11
        :pswitch_131
    .end packed-switch
.end method

.method protected final getView(ILandroid/database/Cursor;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter "partition"
    .parameter "cursor"
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 422
    const/4 v0, 0x0

    .line 423
    .local v0, view:Landroid/view/View;
    if-eqz p4, :cond_6

    .line 424
    packed-switch p1, :pswitch_data_20

    .line 437
    :cond_6
    :goto_6
    if-nez v0, :cond_10

    .line 438
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, p1, p2, p5}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->newView$54126883(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 440
    :cond_10
    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->bindView(Landroid/view/View;ILandroid/database/Cursor;I)V

    .line 441
    return-object v0

    .line 426
    :pswitch_14
    instance-of v1, p4, Lcom/google/android/apps/plus/views/ConversationListItemView;

    if-eqz v1, :cond_6

    .line 427
    move-object v0, p4

    goto :goto_6

    .line 431
    :pswitch_1a
    instance-of v1, p4, Lcom/google/android/apps/plus/views/SuggestedParticipantView;

    if-eqz v1, :cond_6

    .line 432
    move-object v0, p4

    goto :goto_6

    .line 424
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_14
        :pswitch_1a
    .end packed-switch
.end method

.method public final hasStableIds()Z
    .registers 2

    .prologue
    .line 353
    const/4 v0, 0x1

    return v0
.end method

.method protected final newHeaderView$4ac0fa28(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .parameter "context"
    .parameter "partition"
    .parameter "parent"

    .prologue
    .line 399
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 400
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x7f0300b0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected final newView$54126883(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter "context"
    .parameter "partition"
    .parameter "cursor"
    .parameter "container"

    .prologue
    const/4 v1, 0x0

    .line 407
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 408
    .local v0, inflater:Landroid/view/LayoutInflater;
    packed-switch p2, :pswitch_data_1a

    .line 416
    :goto_8
    return-object v1

    .line 410
    :pswitch_9
    const v2, 0x7f03001d

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_8

    .line 413
    :pswitch_11
    const v2, 0x7f0300cb

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_8

    .line 408
    nop

    :pswitch_data_1a
    .packed-switch 0x0
        :pswitch_9
        :pswitch_11
    .end packed-switch
.end method

.method public final onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 6
    .parameter
    .parameter "data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 378
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v1, :cond_42

    .line 379
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mConversationsCursor:Landroid/database/Cursor;

    .line 380
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mSuggestionsCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_17

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mSuggestionsCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    .line 390
    :cond_17
    :goto_17
    const-string v0, "ConversationList"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 391
    const-string v0, "ConversationList"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadFinished suggestions "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mSuggestionsCursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " conversations "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mConversationsCursor:Landroid/database/Cursor;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :cond_41
    return-void

    .line 384
    :cond_42
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v2, :cond_17

    .line 385
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->mSuggestionsCursor:Landroid/database/Cursor;

    .line 386
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->this$0:Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->mConversationCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;->access$300(Lcom/google/android/apps/plus/fragments/HostedMessengerFragment;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 387
    invoke-virtual {p0, v1, p2}, Lcom/google/android/apps/plus/fragments/HostedMessengerFragment$ConversationCursorAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto :goto_17
.end method
