.class public Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedPeopleFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;
.implements Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;
.implements Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;
.implements Lcom/google/android/apps/plus/fragments/UnblockPersonDialog$PersonUnblocker;
.implements Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$DeleteCircleConfirmationDialog;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog$CirclePropertiesListener;",
        "Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;",
        "Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;",
        "Lcom/google/android/apps/plus/fragments/UnblockPersonDialog$PersonUnblocker;",
        "Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;"
    }
.end annotation


# static fields
.field private static final CIRCLES_PROJECTION:[Ljava/lang/String;

.field protected static sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

.field private mCircleMembers:Landroid/database/Cursor;

.field protected mCircleName:Ljava/lang/String;

.field private mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

.field private mCirclesCursor:Landroid/database/Cursor;

.field private mCurrentSpinnerPosition:I

.field private mDataLoaded:Z

.field protected mDeleteCircleRequestId:Ljava/lang/Integer;

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private final mHandler:Landroid/os/Handler;

.field private mIsNew:Z

.field protected mNewCircleRequestId:Ljava/lang/Integer;

.field protected mPendingRequestId:Ljava/lang/Integer;

.field private mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

.field private mRefreshSuggestedPeople:Z

.field private mScrollPosition:I

.field private mSearchMode:Z

.field private mSelectedCircleId:Ljava/lang/String;

.field private mSelectedViewType:I

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mShownPersonIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

.field private mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

.field private mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

.field private mSuggestionScrollView:Landroid/widget/ScrollView;

.field private mViewingAsPlusPage:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 96
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "circle_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "circle_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "contact_count"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "semantic_hints"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->CIRCLES_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 189
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;-><init>(Z)V

    .line 190
    return-void
.end method

.method public constructor <init>(Z)V
    .registers 4
    .parameter "isNew"

    .prologue
    const/4 v1, -0x1

    .line 192
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    .line 120
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    .line 123
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCurrentSpinnerPosition:I

    .line 129
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mHandler:Landroid/os/Handler;

    .line 130
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mScrollPosition:I

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mShownPersonIds:Ljava/util/ArrayList;

    .line 140
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 193
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mIsNew:Z

    .line 194
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 65
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_13
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->handleNewCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->handleDeleteCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;)Lcom/google/android/apps/plus/views/ColumnGridView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    return-object v0
.end method

.method private changeCircleMembers(Landroid/database/Cursor;)V
    .registers 6
    .parameter "data"

    .prologue
    const/4 v0, 0x1

    .line 393
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    if-nez v1, :cond_1f

    .line 394
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 395
    if-eqz p1, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    if-eqz v0, :cond_1e

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SuggestionGridView;->setScrollPositions(Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;)V

    .line 397
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    .line 405
    :cond_1e
    :goto_1e
    return-void

    .line 400
    :cond_1f
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    if-eq v2, v0, :cond_2a

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_30

    :cond_2a
    :goto_2a
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->changeCircleMembers$2c8bde3e(Landroid/database/Cursor;Z)V

    goto :goto_1e

    :cond_30
    const/4 v0, 0x0

    goto :goto_2a
.end method

.method private dismissProgressDialog()V
    .registers 4

    .prologue
    .line 1230
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 1232
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_11

    .line 1233
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 1235
    :cond_11
    return-void
.end method

.method private static getLoggingViewFromType(I)Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2
    .parameter "selectedViewType"

    .prologue
    .line 1246
    packed-switch p0, :pswitch_data_10

    .line 1260
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_5
    return-object v0

    .line 1248
    :pswitch_6
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_5

    .line 1252
    :pswitch_9
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_IN_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_5

    .line 1256
    :pswitch_c
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_BLOCKED:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_5

    .line 1246
    nop

    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
    .end packed-switch
.end method

.method private handleDeleteCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v2, 0x0

    .line 1213
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_e

    .line 1227
    :cond_d
    :goto_d
    return-void

    .line 1217
    :cond_e
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->dismissProgressDialog()V

    .line 1219
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    .line 1221
    if-eqz p2, :cond_1c

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_2b

    .line 1222
    :cond_1c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0803dc

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 1224
    :cond_2b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0801a8

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_d
.end method

.method private handleNewCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 8
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v4, 0x0

    .line 1189
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_e

    .line 1204
    :cond_d
    :goto_d
    return-void

    .line 1193
    :cond_e
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->dismissProgressDialog()V

    .line 1195
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    .line 1197
    if-eqz p2, :cond_1c

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_36

    .line 1198
    :cond_1c
    const v1, 0x7f0803db

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1199
    .local v0, message:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 1201
    .end local v0           #message:Ljava/lang/String;
    :cond_36
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0801a8

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d
.end method

.method private setSearchMode(Z)V
    .registers 5
    .parameter "searchMode"

    .prologue
    const/4 v2, 0x0

    .line 662
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-ne v0, p1, :cond_6

    .line 677
    :goto_5
    return-void

    .line 666
    :cond_6
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    .line 667
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setQueryString(Ljava/lang/String;)V

    .line 668
    if-eqz p1, :cond_2b

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->changeCircleMembers$2c8bde3e(Landroid/database/Cursor;Z)V

    .line 670
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryText(Ljava/lang/String;)V

    .line 675
    :goto_20
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->invalidateActionBar()V

    .line 676
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    goto :goto_5

    .line 672
    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleMembers:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->changeCircleMembers(Landroid/database/Cursor;)V

    goto :goto_20
.end method

.method private showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "personId"
    .parameter "name"

    .prologue
    .line 936
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x1

    invoke-static {v0, v1, p1, p2, v2}, Lcom/google/android/apps/plus/phone/Intents;->getCircleMembershipActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 940
    return-void
.end method

.method private showProgressDialog(I)V
    .registers 6
    .parameter "message"

    .prologue
    .line 1155
    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    .line 1158
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1159
    return-void
.end method

.method private updateView(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 589
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-eqz v0, :cond_15

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 592
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showContent(Landroid/view/View;)V

    .line 628
    :goto_14
    return-void

    .line 594
    :cond_15
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    packed-switch v0, :pswitch_data_84

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 610
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDataLoaded:Z

    if-nez v0, :cond_5a

    .line 611
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    .line 612
    const v0, 0x7f080096

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_14

    .line 596
    :pswitch_33
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDataLoaded:Z

    if-nez v0, :cond_4c

    .line 597
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    .line 598
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 599
    const v0, 0x7f080400

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_14

    .line 601
    :cond_4c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 603
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showContent(Landroid/view/View;)V

    goto :goto_14

    .line 613
    :cond_5a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleMembers:Landroid/database/Cursor;

    if-nez v0, :cond_64

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    goto :goto_14

    .line 616
    :cond_64
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleMembers:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_7b

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    .line 618
    const v0, 0x7f0801a7

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 619
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_14

    .line 621
    :cond_7b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    .line 622
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showContent(Landroid/view/View;)V

    goto :goto_14

    .line 594
    :pswitch_data_84
    .packed-switch 0x0
        :pswitch_33
    .end packed-switch
.end method


# virtual methods
.method public final doDeleteCircle()V
    .registers 4

    .prologue
    .line 1071
    const v1, 0x7f080299

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showProgressDialog(I)V

    .line 1073
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1074
    .local v0, circleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1075
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->deleteCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    .line 1076
    return-void
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 1239
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoggingViewFromType(I)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v0

    return-object v0
.end method

.method protected final handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_d

    .line 1180
    :cond_c
    :goto_c
    return-void

    .line 1172
    :cond_d
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->dismissProgressDialog()V

    .line 1174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1176
    if-eqz p2, :cond_c

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1177
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0801a8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_c
.end method

.method protected final isEmpty()Z
    .registers 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 684
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDataLoaded:Z

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 697
    :cond_b
    :goto_b
    return v0

    .line 688
    :cond_c
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-nez v2, :cond_b

    .line 692
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    packed-switch v2, :pswitch_data_26

    .line 697
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->getCount()I

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    goto :goto_b

    .line 694
    :pswitch_1f
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->isEmpty()Z

    move-result v0

    goto :goto_b

    .line 692
    :pswitch_data_26
    .packed-switch 0x0
        :pswitch_1f
    .end packed-switch
.end method

.method public final onActionButtonClicked(I)V
    .registers 3
    .parameter "actionId"

    .prologue
    .line 635
    packed-switch p1, :pswitch_data_a

    .line 641
    :goto_3
    return-void

    .line 637
    :pswitch_4
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->setSearchMode(Z)V

    goto :goto_3

    .line 635
    nop

    :pswitch_data_a
    .packed-switch 0x0
        :pswitch_4
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 11
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 947
    const/4 v0, -0x1

    if-ne p2, v0, :cond_30

    if-nez p1, :cond_30

    .line 948
    const-string v0, "person_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 949
    .local v2, personId:Ljava/lang/String;
    const-string v0, "display_name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 950
    .local v3, personName:Ljava/lang/String;
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "original_circle_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 952
    .local v4, originalCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selected_circle_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 954
    .local v5, selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 963
    .end local v2           #personId:Ljava/lang/String;
    .end local v3           #personName:Ljava/lang/String;
    .end local v4           #originalCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5           #selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_30
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 964
    return-void
.end method

.method public final onAddPersonToCirclesAction(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .parameter "personId"
    .parameter "name"
    .parameter "forSharing"

    .prologue
    .line 872
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_27

    .line 873
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-static {v1, v2, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getDefaultCircleId(Landroid/content/Context;Landroid/database/Cursor;Z)Ljava/lang/String;

    move-result-object v0

    .line 877
    .local v0, defaultCircleId:Ljava/lang/String;
    if-eqz v0, :cond_27

    .line 878
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p1, p2, v0}, Lcom/google/android/apps/plus/service/EsService;->addPersonToCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    const-string v1, "g:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_26

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->setSearchMode(Z)V

    .line 883
    .end local v0           #defaultCircleId:Ljava/lang/String;
    :cond_26
    :goto_26
    return-void

    .line 880
    :cond_27
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_26
.end method

.method public final onBackPressed()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 645
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-eqz v1, :cond_9

    .line 646
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->setSearchMode(Z)V

    .line 647
    const/4 v0, 0x1

    .line 649
    :cond_9
    return v0
.end method

.method public final onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "personId"
    .parameter "name"

    .prologue
    .line 977
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    return-void
.end method

.method public final onCirclePropertiesChange(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 11
    .parameter "circleId"
    .parameter "circleName"
    .parameter "justFollowing"

    .prologue
    .line 891
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 930
    :goto_6
    return-void

    .line 895
    :cond_7
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    .line 897
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    if-eqz v4, :cond_51

    .line 898
    const/4 v1, 0x0

    .line 899
    .local v1, duplicateName:Z
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->getCount()I

    move-result v0

    .line 900
    .local v0, count:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_17
    if-ge v2, v0, :cond_3c

    .line 901
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;

    .line 902
    .local v3, item:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
    iget-object v4, v3, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->id:Ljava/lang/String;

    if-eqz v4, :cond_4e

    iget v4, v3, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->circleType:I

    const/16 v5, 0xa

    if-eq v4, v5, :cond_4e

    iget-object v4, v3, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->title:Ljava/lang/String;

    invoke-virtual {p2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4e

    iget-object v4, v3, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->id:Ljava/lang/String;

    invoke-static {p1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4e

    .line 906
    const/4 v1, 0x1

    .line 911
    .end local v3           #item:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
    :cond_3c
    if-eqz v1, :cond_51

    .line 912
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f0802b1

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_6

    .line 900
    .restart local v3       #item:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
    :cond_4e
    add-int/lit8 v2, v2, 0x1

    goto :goto_17

    .line 918
    .end local v0           #count:I
    .end local v1           #duplicateName:Z
    .end local v2           #i:I
    .end local v3           #item:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
    :cond_51
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleName:Ljava/lang/String;

    .line 920
    if-nez p1, :cond_68

    .line 921
    const v4, 0x7f080226

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showProgressDialog(I)V

    .line 923
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5, p2, p3}, Lcom/google/android/apps/plus/service/EsService;->createCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    goto :goto_6

    .line 926
    :cond_68
    const v4, 0x7f080229

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showProgressDialog(I)V

    .line 927
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5, p1, p2, p3}, Lcom/google/android/apps/plus/service/EsService;->modifyCircleProperties(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto :goto_6
.end method

.method public final onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .registers 3
    .parameter "circleId"
    .parameter "circle"

    .prologue
    .line 849
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 10
    .parameter "savedInstanceState"

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 206
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 208
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mIsNew:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mRefreshSuggestedPeople:Z

    .line 209
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mViewingAsPlusPage:Z

    .line 210
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mViewingAsPlusPage:Z

    if-eqz v1, :cond_1c

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    if-nez v1, :cond_1c

    .line 211
    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    .line 214
    :cond_1c
    if-eqz p1, :cond_98

    .line 215
    const-string v1, "selected_circle_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    .line 216
    const-string v1, "selected_view_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    .line 217
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mIsNew:Z

    if-nez v1, :cond_3a

    .line 218
    const-string v1, "search_mode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    .line 220
    :cond_3a
    const-string v1, "shown_persons"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mShownPersonIds:Ljava/util/ArrayList;

    .line 221
    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_56

    .line 222
    const-string v1, "request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 224
    :cond_56
    const-string v1, "new_circle_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6a

    .line 225
    const-string v1, "new_circle_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    .line 227
    :cond_6a
    const-string v1, "delete_circle_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7e

    .line 228
    const-string v1, "delete_circle_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    .line 230
    :cond_7e
    const-string v1, "new_circle_name"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleName:Ljava/lang/String;

    .line 231
    const-string v1, "scrollPos"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mScrollPosition:I

    .line 232
    const-string v1, "scrollPositions"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionScrollPositions:Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    .line 236
    :cond_98
    new-instance v1, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    .line 237
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->setNotifyOnChange(Z)V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    .line 240
    .local v0, loaderManager:Landroid/support/v4/app/LoaderManager;
    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v1, v4, v5, v0, v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    const/4 v4, -0x1

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setCircleUsageType(I)V

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setPublicProfileSearchEnabled(Z)V

    .line 244
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setIncludePeopleInCircles(Z)V

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setIncludePlusPages(Z)V

    .line 246
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mViewingAsPlusPage:Z

    if-nez v1, :cond_113

    move v1, v2

    :goto_d9
    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setAddToCirclesActionEnabled(Z)V

    .line 247
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setCircleSpinnerAdapter(Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;)V

    .line 248
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setListener(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;)V

    .line 250
    new-instance v1, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/16 v5, 0x309

    invoke-direct {v1, v3, v0, v4, v5}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->setCircleSpinnerAdapter(Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;)V

    .line 253
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->setListener(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;)V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v2, v7, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 256
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v7, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 257
    return-void

    :cond_113
    move v1, v3

    .line 246
    goto :goto_d9
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 9
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    packed-switch p1, :pswitch_data_52

    .line 320
    :goto_3
    const/4 v1, 0x0

    :goto_4
    return-object v1

    .line 297
    :pswitch_5
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1a

    const/16 v0, 0xc

    .line 300
    .local v0, circleUsage:I
    :goto_c
    new-instance v1, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->CIRCLES_PROJECTION:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_4

    .line 297
    .end local v0           #circleUsage:I
    :cond_1a
    const/4 v0, 0x1

    goto :goto_c

    .line 305
    :pswitch_1c
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    packed-switch v1, :pswitch_data_5a

    goto :goto_3

    .line 307
    :pswitch_22
    new-instance v1, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->PROJECTION:[Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mRefreshSuggestedPeople:Z

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/SuggestedPeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Z)V

    goto :goto_4

    .line 311
    :pswitch_32
    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->PEOPLE_PROJECTION:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/PeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 315
    :pswitch_42
    new-instance v1, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->PEOPLE_PROJECTION:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mShownPersonIds:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/BlockedPeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_4

    .line 295
    :pswitch_data_52
    .packed-switch 0x1
        :pswitch_5
        :pswitch_1c
    .end packed-switch

    .line 305
    :pswitch_data_5a
    .packed-switch 0x0
        :pswitch_22
        :pswitch_32
        :pswitch_42
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 12
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 552
    const v3, 0x7f030051

    invoke-virtual {p1, v3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 554
    .local v2, view:Landroid/view/View;
    const v3, 0x7f090109

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 555
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 557
    const v3, 0x7f090112

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ScrollView;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionScrollView:Landroid/widget/ScrollView;

    .line 558
    const v3, 0x7f090113

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/SuggestionGridView;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    .line 559
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/views/SuggestionGridView;->setAdapter(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;)V

    .line 561
    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    if-nez v3, :cond_47

    .line 562
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    .line 565
    :cond_47
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 566
    .local v1, resources:Landroid/content/res/Resources;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v5, :cond_58

    move v0, v4

    .line 569
    .local v0, landscape:Z
    :cond_58
    if-eqz v0, :cond_93

    .line 570
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    .line 571
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    .line 572
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const v4, 0x7f0d01a6

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setMinColumnWidth(I)V

    .line 580
    :goto_71
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v4, v4, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    .line 581
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v4, v4, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v5, v5, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v6, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v6, v6, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v7, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v7, v7, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    .line 584
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    .line 585
    return-object v2

    .line 575
    :cond_93
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    .line 576
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v3, :cond_a5

    move v3, v4

    :goto_a1
    invoke-virtual {v6, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    goto :goto_71

    :cond_a5
    move v3, v5

    goto :goto_a1
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 1142
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 1149
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1135
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1128
    return-void
.end method

.method public final onDismissSuggestionAction(Ljava/lang/String;)V
    .registers 6
    .parameter "personId"

    .prologue
    .line 1114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1115
    .local v0, personIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1119
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v3, "ANDROID_PEOPLE_SUGGESTIONS_PAGE"

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/service/EsService;->dismissSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    .line 1121
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 12
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x0

    const/4 v2, -0x1

    const/4 v7, 0x1

    .line 65
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_9a

    :cond_d
    :goto_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    return-void

    :pswitch_15
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->clear()V

    if-eqz p2, :cond_45

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_45

    :cond_24
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->add(Ljava/lang/Object;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_24

    :cond_45
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->invalidateActionBar()V

    goto :goto_d

    :pswitch_4e
    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDataLoaded:Z

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleMembers:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mShownPersonIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    if-ne v0, v8, :cond_72

    if-eqz p2, :cond_72

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_72

    :cond_63
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mShownPersonIds:Ljava/util/ArrayList;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_63

    :cond_72
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-nez v0, :cond_79

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->changeCircleMembers(Landroid/database/Cursor;)V

    :cond_79
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mScrollPosition:I

    if-eq v0, v2, :cond_d

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-nez v0, :cond_85

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    if-eqz v0, :cond_d

    :cond_85
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mScrollPosition:I

    if-nez v0, :cond_92

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelectionToTop()V

    :goto_8e
    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mScrollPosition:I

    goto/16 :goto_d

    :cond_92
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mScrollPosition:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelection(I)V

    goto :goto_8e

    :pswitch_data_9a
    .packed-switch 0x1
        :pswitch_15
        :pswitch_4e
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 412
    .local p1, arg0:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 9
    .parameter "item"

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 785
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_a2

    move v3, v4

    .line 805
    :cond_b
    :goto_b
    return v3

    .line 787
    :sswitch_c
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    if-eqz v2, :cond_b

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$DeleteCircleConfirmationDialog;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$DeleteCircleConfirmationDialog;-><init>()V

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$DeleteCircleConfirmationDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "delete_circle_conf"

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment$DeleteCircleConfirmationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_b

    .line 792
    :sswitch_22
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7e

    :cond_3a
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    invoke-static {v2, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_76

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    const/4 v6, 0x4

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    :goto_57
    if-eq v2, v5, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleName:Ljava/lang/String;

    and-int/lit8 v2, v2, 0x40

    if-nez v2, :cond_80

    move v2, v3

    :goto_65
    invoke-static {v5, v6, v2}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->newInstance$50fd8769(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;

    move-result-object v2

    invoke-virtual {v2, p0, v4}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "circle_settings"

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_b

    :cond_76
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3a

    :cond_7e
    move v2, v5

    goto :goto_57

    :cond_80
    move v2, v4

    goto :goto_65

    .line 797
    :sswitch_82
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f08040e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 799
    .local v1, helpUrlParam:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 800
    .local v0, helpUrl:Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startExternalActivity(Landroid/content/Intent;)V

    goto/16 :goto_b

    .line 785
    nop

    :sswitch_data_a2
    .sparse-switch
        0x7f090290 -> :sswitch_82
        0x7f0902ba -> :sswitch_c
        0x7f0902bb -> :sswitch_22
    .end sparse-switch
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 762
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    .line 763
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 764
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->onPeopleListVisibilityChange(Z)V

    .line 765
    return-void
.end method

.method public final onPersonSelected(Ljava/lang/String;)V
    .registers 6
    .parameter "personId"

    .prologue
    .line 866
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 867
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    .line 868
    return-void
.end method

.method public final onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 8
    .parameter "personId"
    .parameter "contactLookupKey"
    .parameter "person"

    .prologue
    .line 853
    if-eqz p2, :cond_13

    .line 854
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v2, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 856
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startExternalActivity(Landroid/content/Intent;)V

    .line 862
    :goto_12
    return-void

    .line 858
    .end local v0           #intent:Landroid/content/Intent;
    :cond_13
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 860
    .restart local v0       #intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_12
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 14
    .parameter "actionBar"

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 416
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-eqz v0, :cond_12

    .line 417
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showSearchView()V

    .line 418
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V

    .line 429
    :goto_11
    return-void

    .line 420
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    if-nez v0, :cond_21

    .line 421
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    .line 424
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_a1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v0

    if-nez v0, :cond_b7

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;

    const v2, 0x7f0803c6

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->add(Ljava/lang/Object;)V

    move v0, v10

    :goto_46
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_d5

    move v2, v3

    :cond_4f
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v4, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v4, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    const/4 v7, 0x3

    invoke-interface {v4, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    const/4 v8, 0x2

    invoke-interface {v4, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    new-instance v4, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    invoke-virtual {v11, v4}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->add(Ljava/lang/Object;)V

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    packed-switch v4, :pswitch_data_d8

    :cond_79
    :goto_79
    :pswitch_79
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_4f

    move v6, v2

    :goto_82
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;

    const v2, 0x7f080297

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v5, 0x7f020096

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->add(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCurrentSpinnerPosition:I

    if-eq v0, v6, :cond_a1

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->onPrimarySpinnerSelectionChange(I)V

    :cond_a1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    .line 426
    const v0, 0x7f0200f3

    const v1, 0x7f08027e

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    goto/16 :goto_11

    :cond_b7
    move v0, v3

    .line 424
    goto :goto_46

    :pswitch_b9
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_79

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    add-int/2addr v2, v0

    goto :goto_79

    :pswitch_c9
    const/16 v4, 0xa

    if-ne v7, v4, :cond_79

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCirclesCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    add-int/2addr v2, v0

    goto :goto_79

    :cond_d5
    move v6, v3

    goto :goto_82

    nop

    :pswitch_data_d8
    .packed-switch 0x0
        :pswitch_79
        :pswitch_b9
        :pswitch_c9
    .end packed-switch
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 4
    .parameter "menu"

    .prologue
    const/4 v1, 0x1

    .line 772
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 774
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    if-ne v0, v1, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    if-eqz v0, :cond_20

    .line 775
    const v0, 0x7f0902ba

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 776
    const v0, 0x7f0902bb

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 778
    :cond_20
    return-void
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .registers 16
    .parameter "position"

    .prologue
    const/4 v13, 0x2

    const/4 v11, 0x1

    const/4 v3, 0x0

    const/4 v12, 0x0

    .line 492
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_2b

    .line 494
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->setPrimarySpinnerSelection(I)V

    .line 495
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->newInstance$47e87423()Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;

    move-result-object v7

    .line 497
    .local v7, dialog:Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;
    invoke-virtual {v7, p0, v12}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 498
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "new_circle_input"

    invoke-virtual {v7, v0, v1}, Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 547
    .end local v7           #dialog:Lcom/google/android/apps/plus/fragments/CirclePropertiesFragmentDialog;
    :cond_2a
    :goto_2a
    return-void

    .line 502
    :cond_2b
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCurrentSpinnerPosition:I

    if-eq v0, p1, :cond_2a

    .line 503
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCurrentSpinnerPosition:I

    .line 505
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    if-ne v0, v13, :cond_92

    move v9, v11

    .line 507
    .local v9, reloadCircles:Z
    :goto_36
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;

    .line 509
    .local v8, info:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
    iget-object v0, v8, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->id:Ljava/lang/String;

    if-nez v0, :cond_94

    .line 510
    const/4 v10, 0x0

    .line 517
    .local v10, viewType:I
    :goto_43
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    if-eq v0, v10, :cond_64

    .line 519
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->clearNavigationAction()V

    .line 521
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoggingViewFromType(I)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    invoke-static {v10}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoggingViewFromType(I)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 524
    iput v10, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    .line 525
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    if-ne v0, v13, :cond_9e

    move v0, v11

    :goto_61
    or-int/2addr v9, v0

    .line 526
    iput v12, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mScrollPosition:I

    .line 529
    :cond_64
    const/4 v6, 0x0

    .line 530
    .local v6, circleId:Ljava/lang/String;
    if-eqz v10, :cond_69

    .line 531
    iget-object v6, v8, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->id:Ljava/lang/String;

    .line 534
    :cond_69
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    invoke-static {v6, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_75

    .line 535
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    .line 536
    iput v12, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mScrollPosition:I

    .line 539
    :cond_75
    iput-boolean v12, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDataLoaded:Z

    .line 540
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v13, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 541
    if-eqz v9, :cond_87

    .line 542
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v11, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 544
    :cond_87
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->invalidateActionBar()V

    .line 545
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    goto :goto_2a

    .end local v6           #circleId:Ljava/lang/String;
    .end local v8           #info:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
    .end local v9           #reloadCircles:Z
    .end local v10           #viewType:I
    :cond_92
    move v9, v12

    .line 505
    goto :goto_36

    .line 511
    .restart local v8       #info:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;
    .restart local v9       #reloadCircles:Z
    :cond_94
    iget v0, v8, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter$CircleSpinnerInfo;->circleType:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_9c

    .line 512
    const/4 v10, 0x2

    .restart local v10       #viewType:I
    goto :goto_43

    .line 514
    .end local v10           #viewType:I
    :cond_9c
    const/4 v10, 0x1

    .restart local v10       #viewType:I
    goto :goto_43

    :cond_9e
    move v0, v12

    .line 525
    goto :goto_61
.end method

.method public final onQueryClose()V
    .registers 2

    .prologue
    .line 830
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->setSearchMode(Z)V

    .line 831
    return-void
.end method

.method public final onQueryTextChanged(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "query"

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-eqz v0, :cond_10

    .line 814
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    if-nez p1, :cond_11

    const/4 v0, 0x0

    :goto_d
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->setQueryString(Ljava/lang/String;)V

    .line 816
    :cond_10
    return-void

    .line 814
    :cond_11
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_d
.end method

.method public final onQueryTextSubmitted(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "query"

    .prologue
    .line 823
    return-void
.end method

.method public final onResume()V
    .registers 5

    .prologue
    const/4 v2, 0x0

    .line 725
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    .line 727
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 729
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->onPeopleListVisibilityChange(Z)V

    .line 731
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_32

    .line 732
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_32

    .line 733
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 734
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 735
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 738
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_32
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_57

    .line 739
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_57

    .line 740
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 741
    .restart local v0       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->handleNewCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 742
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    .line 745
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_57
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_7c

    .line 746
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_7c

    .line 747
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 748
    .restart local v0       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->handleDeleteCircleCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 749
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    .line 752
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_7c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    .line 754
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)Ljava/lang/Integer;

    .line 755
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 264
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    if-eqz v0, :cond_c

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 268
    :cond_c
    const-string v0, "selected_circle_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string v0, "selected_view_type"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 270
    const-string v0, "search_mode"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 271
    const-string v0, "shown_persons"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mShownPersonIds:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_37

    .line 273
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 275
    :cond_37
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_46

    .line 276
    const-string v0, "new_circle_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mNewCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 278
    :cond_46
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_55

    .line 279
    const-string v0, "delete_circle_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mDeleteCircleRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 281
    :cond_55
    const-string v0, "new_circle_name"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mCircleName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v1, "scrollPos"

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v0, :cond_7b

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getFirstVisiblePosition()I

    move-result v0

    :goto_68
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    if-eqz v0, :cond_7a

    .line 285
    const-string v0, "scrollPositions"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionGridView:Lcom/google/android/apps/plus/views/SuggestionGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/SuggestionGridView;->getScrollPositions()Lcom/google/android/apps/plus/views/SuggestionGridView$ScrollPositions;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 288
    :cond_7a
    return-void

    .line 282
    :cond_7b
    const/4 v0, -0x1

    goto :goto_68
.end method

.method public final onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .registers 3
    .parameter "adapter"

    .prologue
    .line 838
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 839
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_9

    .line 840
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->updateView(Landroid/view/View;)V

    .line 842
    :cond_9
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .registers 4
    .parameter "args"

    .prologue
    .line 198
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 199
    const-string v0, "people_view_type"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedViewType:I

    .line 201
    const-string v0, "circle_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSelectedCircleId:Ljava/lang/String;

    .line 202
    return-void
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 706
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStart()V

    .line 707
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->onStart()V

    .line 708
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSuggestionAdapter:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->onStart()V

    .line 709
    return-void
.end method

.method public final onStop()V
    .registers 2

    .prologue
    .line 716
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStart()V

    .line 717
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->onStop()V

    .line 718
    return-void
.end method

.method public final onUnblockPersonAction(Ljava/lang/String;Z)V
    .registers 6
    .parameter "personId"
    .parameter "isPlusPage"

    .prologue
    const/4 v1, 0x0

    .line 1042
    new-instance v0, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;-><init>(Ljava/lang/String;Z)V

    .line 1043
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;
    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1044
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "unblock_person"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1045
    return-void
.end method

.method public final onUpButtonClicked()Z
    .registers 3

    .prologue
    const/4 v0, 0x0

    .line 654
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mSearchMode:Z

    if-eqz v1, :cond_9

    .line 655
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->setSearchMode(Z)V

    .line 656
    const/4 v0, 0x1

    .line 658
    :cond_9
    return v0
.end method

.method protected final setCircleMembership(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 15
    .parameter "personId"
    .parameter "personName"
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 985
    .local p3, originalCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 986
    .local v7, circlesToAdd:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :cond_9
    :goto_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 987
    .local v6, circleId:Ljava/lang/String;
    invoke-virtual {p3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 988
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 992
    .end local v6           #circleId:Ljava/lang/String;
    :cond_1f
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 993
    .local v8, circlesToRemove:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_28
    :goto_28
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 994
    .restart local v6       #circleId:Ljava/lang/String;
    invoke-virtual {p4, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 995
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_28

    .line 999
    .end local v6           #circleId:Ljava/lang/String;
    :cond_3e
    invoke-static {v7, v8}, Lcom/google/android/apps/plus/content/EsPeopleData;->getMembershipChangeMessageId(Ljava/util/List;Ljava/util/List;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showProgressDialog(I)V

    .line 1002
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v7, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v8, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1006
    const-string v0, "g:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_77

    .line 1009
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->setSearchMode(Z)V

    .line 1011
    :cond_77
    return-void
.end method

.method public final unblockPerson(Ljava/lang/String;)V
    .registers 6
    .parameter "personId"

    .prologue
    .line 1049
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->setPersonBlocked(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 1051
    const v0, 0x7f080264

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPeopleFragment;->showProgressDialog(I)V

    .line 1052
    return-void
.end method
