.class final Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;
.super Ljava/lang/Object;
.source "HostedPhotosFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 507
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemDeselected(Landroid/view/View;I)V
    .registers 5
    .parameter "v"
    .parameter "position"

    .prologue
    .line 532
    const/4 v0, 0x0

    .line 533
    .local v0, mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    if-eqz p1, :cond_9

    .line 534
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    .line 537
    .restart local v0       #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_9
    if-nez v0, :cond_15

    .line 538
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    .line 541
    :cond_15
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 542
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateContextualActionBar()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    .line 543
    return-void
.end method

.method public final onItemSelected(Landroid/view/View;I)V
    .registers 5
    .parameter "v"
    .parameter "position"

    .prologue
    .line 513
    const/4 v0, 0x0

    .line 514
    .local v0, mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    if-eqz p1, :cond_9

    .line 515
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    .line 518
    .restart local v0       #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_9
    if-nez v0, :cond_1a

    .line 519
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    .line 520
    if-eqz p1, :cond_1a

    .line 521
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 525
    :cond_1a
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$500(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 526
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;->this$0:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateContextualActionBar()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->access$600(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    .line 527
    return-void
.end method
