.class public Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedPhotosFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;
.implements Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;
.implements Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$AlbumDetailsQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnLongClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;",
        "Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;",
        "Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;"
    }
.end annotation


# instance fields
.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeCallback:Landroid/view/ActionMode$Callback;

.field private mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

.field private mAlbumCount:I

.field private mAlbumId:Ljava/lang/String;

.field private mAlbumName:Ljava/lang/String;

.field private mAlbumType:Ljava/lang/String;

.field private mAlbumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

.field private mCount:I

.field private mDateFormat:Ljava/text/DateFormat;

.field private mDeleteReqId:Ljava/lang/Integer;

.field private final mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mEventId:Ljava/lang/String;

.field private mExtras:Landroid/os/Bundle;

.field private mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field private mHereFromNotification:Z

.field private mLastNotificationTime:J

.field private mLoaderActive:Z

.field private mNotificationId:Ljava/lang/String;

.field private mOwnerId:Ljava/lang/String;

.field private mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

.field private mPhotoOfUserId:Ljava/lang/String;

.field private mPickerMode:I

.field private mPickerShareWithZeroSelected:Z

.field private mPickerTitleResourceId:I

.field private mRefreshReqId:Ljava/lang/Integer;

.field private mRefreshable:Z

.field private final mSelectedPhotoMediaRefs:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mStreamId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    .line 144
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    .line 156
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    .line 167
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDateFormat:Ljava/text/DateFormat;

    .line 195
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 233
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handlePhotoDelete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Lcom/google/android/apps/plus/views/PhotoAlbumView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/text/DateFormat;
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDateFormat:Ljava/text/DateFormat;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Ljava/util/HashSet;
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateContextualActionBar()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)Landroid/view/ActionMode;
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$802(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showDeleteConfirmationDialog()V

    return-void
.end method

.method private handlePhotoDelete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 8
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v4, 0x0

    .line 269
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_e

    .line 282
    :cond_d
    :goto_d
    return-void

    .line 273
    :cond_e
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    .line 275
    if-eqz p2, :cond_2f

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 276
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 277
    .local v0, res:Landroid/content/res/Resources;
    const v2, 0x7f080161

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 278
    .local v1, toastText:Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 281
    .end local v0           #res:Landroid/content/res/Resources;
    .end local v1           #toastText:Ljava/lang/CharSequence;
    :cond_2f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "progress_dialog"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/DialogFragment;

    if-eqz v2, :cond_40

    invoke-virtual {v2}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_40
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    goto :goto_d
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 243
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_d

    .line 259
    :cond_c
    :goto_c
    return-void

    .line 247
    :cond_d
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    .line 249
    if-eqz p2, :cond_2f

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 250
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 251
    .local v0, res:Landroid/content/res/Resources;
    const v2, 0x7f080162

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 252
    .local v1, toastText:Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 256
    .end local v0           #res:Landroid/content/res/Resources;
    .end local v1           #toastText:Ljava/lang/CharSequence;
    :cond_2f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "progress_dialog"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/DialogFragment;

    if-eqz v2, :cond_40

    invoke-virtual {v2}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 258
    :cond_40
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    goto :goto_c
.end method

.method private invalidateContextualActionBar()V
    .registers 2

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_a

    .line 1094
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->invalidate()V

    .line 1098
    :goto_9
    return-void

    .line 1096
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateActionBar()V

    goto :goto_9
.end method

.method private isInstantUploadAlbum()Z
    .registers 3

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    const-string v1, "from_my_phone"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private isLocalCameraAlbum()Z
    .registers 3

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    const-string v1, "camera_photos"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private loadAlbumName()V
    .registers 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1321
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    if-eqz v6, :cond_27

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    if-eqz v6, :cond_27

    move v1, v4

    .line 1322
    .local v1, hasAlbumAndOwner:Z
    :goto_b
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    .line 1323
    .local v3, needName:Z
    iget v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_29

    move v2, v4

    .line 1324
    .local v2, needCount:Z
    :goto_17
    if-eqz v1, :cond_2b

    if-nez v3, :cond_1d

    if-eqz v2, :cond_2b

    .line 1325
    :cond_1d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 1332
    :cond_26
    :goto_26
    return-void

    .end local v1           #hasAlbumAndOwner:Z
    .end local v2           #needCount:Z
    .end local v3           #needName:Z
    :cond_27
    move v1, v5

    .line 1321
    goto :goto_b

    .restart local v1       #hasAlbumAndOwner:Z
    .restart local v3       #needName:Z
    :cond_29
    move v2, v5

    .line 1323
    goto :goto_17

    .line 1326
    .restart local v2       #needCount:Z
    :cond_2b
    if-nez v3, :cond_26

    .line 1327
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    .line 1328
    .local v0, hab:Lcom/google/android/apps/plus/views/HostActionBar;
    if-eqz v0, :cond_26

    .line 1329
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    goto :goto_26
.end method

.method private showDeleteConfirmationDialog()V
    .registers 9

    .prologue
    .line 895
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 896
    .local v3, res:Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 898
    .local v0, count:I
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v4

    if-eqz v4, :cond_3e

    .line 899
    const v2, 0x7f0e0004

    .line 903
    .local v2, message:I
    :goto_13
    const v4, 0x7f0e0002

    invoke-virtual {v3, v4, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0e0010

    invoke-virtual {v3, v6, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0801c5

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    .line 908
    .local v1, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v4, 0x0

    invoke-virtual {v1, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 909
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "delete_dialog"

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 910
    return-void

    .line 901
    .end local v1           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    .end local v2           #message:I
    :cond_3e
    const v2, 0x7f0e0003

    .restart local v2       #message:I
    goto :goto_13
.end method

.method private updatePickerMode(I)V
    .registers 4
    .parameter "mode"

    .prologue
    .line 975
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    .line 977
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    packed-switch v0, :pswitch_data_5c

    .line 1004
    :cond_7
    :goto_7
    :pswitch_7
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->invalidateContextualActionBar()V

    .line 1005
    return-void

    .line 979
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 980
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->isInSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 981
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->endSelectionMode()V

    goto :goto_7

    .line 986
    :pswitch_1e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->isInSelectionMode()Z

    move-result v0

    if-nez v0, :cond_7

    .line 987
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->startSelectionMode()V

    goto :goto_7

    .line 992
    :pswitch_2c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->isInSelectionMode()Z

    move-result v0

    if-nez v0, :cond_39

    .line 993
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->startSelectionMode()V

    .line 995
    :cond_39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_7

    .line 996
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    if-nez v0, :cond_4e

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    :cond_4e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionModeCallback:Landroid/view/ActionMode$Callback;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mActionMode:Landroid/view/ActionMode;

    goto :goto_7

    .line 977
    nop

    :pswitch_data_5c
    .packed-switch 0x0
        :pswitch_b
        :pswitch_7
        :pswitch_1e
        :pswitch_2c
    .end packed-switch
.end method

.method private updateView(Landroid/view/View;)V
    .registers 8
    .parameter "view"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1296
    if-nez p1, :cond_5

    .line 1315
    :goto_4
    return-void

    .line 1300
    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 1301
    .local v0, c:Landroid/database/Cursor;
    if-eqz v0, :cond_26

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lez v5, :cond_26

    move v1, v3

    .line 1302
    .local v1, hasResults:Z
    :goto_14
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-nez v5, :cond_1a

    if-nez v0, :cond_28

    :cond_1a
    move v2, v3

    .line 1304
    .local v2, refreshInProgress:Z
    :goto_1b
    if-eqz v2, :cond_2a

    if-nez v1, :cond_2a

    .line 1305
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 1314
    :goto_22
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateSpinner()V

    goto :goto_4

    .end local v1           #hasResults:Z
    .end local v2           #refreshInProgress:Z
    :cond_26
    move v1, v4

    .line 1301
    goto :goto_14

    .restart local v1       #hasResults:Z
    :cond_28
    move v2, v4

    .line 1302
    goto :goto_1b

    .line 1307
    .restart local v2       #refreshInProgress:Z
    :cond_2a
    if-eqz v1, :cond_30

    .line 1308
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showContent(Landroid/view/View;)V

    goto :goto_22

    .line 1310
    :cond_30
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_22
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 1266
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 739
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    if-nez v0, :cond_6

    .line 740
    const/4 v0, 0x1

    .line 742
    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->isEmpty()Z

    move-result v0

    goto :goto_5
.end method

.method protected final isProgressIndicatorVisible()Z
    .registers 2

    .prologue
    .line 754
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    if-nez v0, :cond_e

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_e
    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final onActionButtonClicked(I)V
    .registers 7
    .parameter "actionId"

    .prologue
    .line 856
    packed-switch p1, :pswitch_data_2e

    .line 873
    :goto_3
    return-void

    .line 858
    :pswitch_4
    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_d

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHereFromNotification:Z

    if-eqz v3, :cond_11

    .line 860
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->shareSelectedPhotos()V

    goto :goto_3

    .line 862
    :cond_11
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 863
    .local v0, activity:Landroid/app/Activity;
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 864
    .local v2, result:Landroid/content/Intent;
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 866
    .local v1, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    const-string v3, "mediarefs"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 867
    const/4 v3, -0x1

    invoke-virtual {v0, v3, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 868
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_3

    .line 856
    :pswitch_data_2e
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 1226
    packed-switch p1, :pswitch_data_16

    .line 1239
    :cond_3
    :goto_3
    return-void

    .line 1228
    :pswitch_4
    if-eqz p2, :cond_3

    .line 1229
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1230
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_3

    .line 1226
    nop

    :pswitch_data_16
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .registers 18
    .parameter "view"

    .prologue
    .line 1132
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "mediarefs"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "mediarefs"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, [Lcom/google/android/apps/plus/api/MediaRef;

    move-object v8, v1

    :goto_19
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "album_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "album_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v9, v1

    :goto_30
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "stream_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "stream_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v10, v1

    :goto_47
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "photos_of_user_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_ed

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "photos_of_user_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v11, v1

    :goto_5e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "photo_picker_mode"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v2, "allow_crop"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    const v1, 0x7f09003d

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v14

    invoke-interface {v14, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/16 v1, 0x8

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v1, 0x5

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v1, 0x9

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v1, 0xb

    invoke-interface {v14, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_f1

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_a8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    const-string v5, "camera_photos"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_f4

    if-nez v8, :cond_f4

    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v5, 0x0

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    const/4 v3, 0x1

    new-array v8, v3, [Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v3, 0x0

    aput-object v1, v8, v3

    move-object v3, v1

    :goto_c7
    if-eqz v12, :cond_fd

    const/4 v1, 0x7

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v4, v1, v3, v13}, Lcom/google/android/apps/plus/phone/Intents;->getPhotoPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;Z)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1133
    :goto_e0
    return-void

    .line 1132
    :cond_e1
    const/4 v1, 0x0

    move-object v8, v1

    goto/16 :goto_19

    :cond_e5
    const/4 v1, 0x0

    move-object v9, v1

    goto/16 :goto_30

    :cond_e9
    const/4 v1, 0x0

    move-object v10, v1

    goto/16 :goto_47

    :cond_ed
    const/4 v1, 0x0

    move-object v11, v1

    goto/16 :goto_5e

    :cond_f1
    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_a8

    :cond_f4
    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v6, 0x0

    move-object v5, v15

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object v3, v1

    goto :goto_c7

    :cond_fd
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v1

    instance-of v4, v1, Lcom/google/android/apps/plus/phone/Pageable;

    if-eqz v4, :cond_171

    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1}, Lcom/google/android/apps/plus/phone/Pageable;->getCurrentPage()I

    move-result v1

    :goto_110
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoViewActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setMediaRefs([Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoOfUserId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPageHint(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHereFromNotification:Z

    if-eqz v1, :cond_166

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mNotificationId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_166

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mNotificationId:Ljava/lang/String;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setNotificationId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    :cond_166
    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_e0

    :cond_171
    const/4 v1, -0x1

    goto :goto_110
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 11
    .parameter "savedInstanceState"

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 294
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 296
    const/4 v1, 0x0

    .line 297
    .local v1, extraPickerMode:Ljava/lang/Integer;
    const/4 v0, 0x0

    .line 299
    .local v0, doFirstTimeNotificationWorkflow:Z
    if-eqz p1, :cond_1b9

    .line 300
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    .line 301
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "INTENT"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 302
    const-string v4, "ALBUM_NAME"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 303
    const-string v4, "ALBUM_NAME"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    .line 305
    :cond_2b
    const-string v4, "STATE_PICKER_MODE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4d

    .line 306
    const-string v4, "STATE_PICKER_MODE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 307
    const-string v4, "STATE_PICKER_TITLE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    .line 308
    const-string v4, "STATE_PICKER_SHARE_ON_ZERO"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerShareWithZeroSelected:Z

    .line 311
    :cond_4d
    const-string v4, "SELECTED_ITEMS"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6b

    .line 312
    const-string v4, "SELECTED_ITEMS"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 314
    .local v3, mediaRefs:[Landroid/os/Parcelable;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_5c
    array-length v4, v3

    if-ge v2, v4, :cond_6b

    .line 315
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    aget-object v4, v3, v2

    check-cast v4, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v7, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 314
    add-int/lit8 v2, v2, 0x1

    goto :goto_5c

    .line 318
    .end local v2           #i:I
    .end local v3           #mediaRefs:[Landroid/os/Parcelable;
    :cond_6b
    const-string v4, "refresh_request"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7f

    .line 319
    const-string v4, "refresh_request"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    .line 321
    :cond_7f
    const-string v4, "delete_request"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_93

    .line 322
    const-string v4, "delete_request"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    .line 324
    :cond_93
    const-string v4, "loader_active"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a3

    .line 325
    const-string v4, "loader_active"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    .line 335
    :cond_a3
    :goto_a3
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "owner_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_b7

    .line 337
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "owner_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    .line 340
    :cond_b7
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "album_name"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_cf

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    if-nez v4, :cond_cf

    .line 341
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "album_name"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    .line 344
    :cond_cf
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "album_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e3

    .line 346
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "album_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    .line 349
    :cond_e3
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "album_type"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_f7

    .line 351
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "album_type"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    .line 354
    :cond_f7
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "stream_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10b

    .line 356
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "stream_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    .line 359
    :cond_10b
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "event_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11f

    .line 361
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "event_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    .line 364
    :cond_11f
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "photos_of_user_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_133

    .line 366
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "photos_of_user_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPhotoOfUserId:Ljava/lang/String;

    .line 369
    :cond_133
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "notif_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mNotificationId:Ljava/lang/String;

    .line 370
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mNotificationId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1dc

    :goto_145
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHereFromNotification:Z

    .line 372
    if-nez v1, :cond_15f

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_mode"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_15f

    .line 374
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_mode"

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 378
    :cond_15f
    if-eqz v1, :cond_167

    .line 379
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    .line 382
    :cond_167
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_17b

    .line 384
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    .line 387
    :cond_17b
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_share_on_zero"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_18f

    .line 389
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_share_on_zero"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerShareWithZeroSelected:Z

    .line 395
    :cond_18f
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1df

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_selected"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1df

    .line 397
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v5, "photo_picker_selected"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 399
    .restart local v3       #mediaRefs:[Landroid/os/Parcelable;
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_1aa
    array-length v4, v3

    if-ge v2, v4, :cond_1df

    .line 400
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    aget-object v4, v3, v2

    check-cast v4, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 399
    add-int/lit8 v2, v2, 0x1

    goto :goto_1aa

    .line 328
    .end local v2           #i:I
    .end local v3           #mediaRefs:[Landroid/os/Parcelable;
    :cond_1b9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    .line 330
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "notif_id"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1da

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    const-string v7, "do_notif_refresh"

    invoke-virtual {v4, v7, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1da

    move v0, v5

    :goto_1d8
    goto/16 :goto_a3

    :cond_1da
    move v0, v6

    goto :goto_1d8

    :cond_1dc
    move v5, v6

    .line 370
    goto/16 :goto_145

    .line 404
    :cond_1df
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->loadAlbumName()V

    .line 406
    if-eqz v0, :cond_21a

    .line 407
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryLastPhotoNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    .line 409
    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    const-wide/16 v7, 0x0

    cmp-long v4, v4, v7

    if-gez v4, :cond_202

    .line 410
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v7, 0xdbba00

    sub-long/2addr v4, v7

    iput-wide v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    .line 414
    :cond_202
    const/4 v4, 0x0

    const v5, 0x7f080095

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "progress_dialog"

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 415
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->refresh()V

    .line 417
    :cond_21a
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 12
    .parameter "id"
    .parameter "bundle"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 637
    packed-switch p1, :pswitch_data_5c

    move-object v0, v5

    .line 660
    :goto_5
    return-object v0

    .line 640
    :pswitch_6
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v1

    if-eqz v1, :cond_22

    .line 641
    new-instance v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .local v0, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    :goto_17
    move-object v1, v0

    .line 646
    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    .line 647
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/phone/Pageable;->setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V

    goto :goto_5

    .line 643
    .end local v0           #loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    :cond_22
    new-instance v0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPhotoOfUserId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/AlbumViewLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v0       #loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    goto :goto_17

    .line 652
    .end local v0           #loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    :pswitch_38
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->ALBUM_VIEW_BY_ALBUM_AND_OWNER_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 655
    .local v8, albumUri:Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v8, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    .line 656
    .local v3, loaderUri:Landroid/net/Uri;
    new-instance v1, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$AlbumDetailsQuery;->PROJECTION:[Ljava/lang/String;

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_5

    .line 637
    nop

    :pswitch_data_5c
    .packed-switch 0x1
        :pswitch_6
        :pswitch_38
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 14
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x1

    .line 443
    const v0, 0x7f03004d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoAlbumView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    const v1, 0x7f090109

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 448
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v8

    .line 449
    .local v8, sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget v1, v8, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget v1, v8, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    iget v3, v8, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    iget v4, v8, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    iget v5, v8, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    .line 453
    new-instance v0, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumType:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object v5, p0

    move-object v6, p0

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Ljava/lang/String;Lcom/google/android/apps/plus/views/ColumnGridView;Landroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter$ViewUseListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    .line 455
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->setSelectedMediaRefs(Ljava/util/HashSet;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const v1, 0x7f02017c

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelector(I)V

    .line 460
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v9, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 462
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    const v1, 0x7f080073

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v1, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->registerSelectionListener(Lcom/google/android/apps/plus/views/ColumnGridView$ItemSelectionListener;)V

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/plus/views/PhotoAlbumView;->enableDateDisplay(Z)V

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumView:Lcom/google/android/apps/plus/views/PhotoAlbumView;

    return-object v0
.end method

.method public final onDataSourceLoading(Z)V
    .registers 2
    .parameter "active"

    .prologue
    .line 748
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    .line 749
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateSpinner()V

    .line 750
    return-void
.end method

.method public final onDestroyView()V
    .registers 3

    .prologue
    .line 554
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onDestroyView()V

    .line 555
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->unregisterSelectionListener()V

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V

    .line 557
    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 925
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 929
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 921
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 9
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 914
    const-string v0, "delete_dialog"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 915
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v0

    if-eqz v0, :cond_4e

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/EsService;->deleteLocalPhotos(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    :goto_2e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0005

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 917
    :cond_4d
    :goto_4d
    return-void

    .line 915
    :cond_4e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_54
    :goto_54
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v4

    if-eqz v4, :cond_54

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_81

    const-string v0, "HostedPhotosFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4d

    const-string v0, "HostedPhotosFragment"

    const-string v1, "Found a photo from phone which is not owned by the current user."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4d

    :cond_81
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_54

    :cond_8d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v3, v2, v1}, Lcom/google/android/apps/plus/service/EsService;->deletePhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    goto :goto_2e
.end method

.method public final onDoneButtonClick()V
    .registers 2

    .prologue
    .line 851
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    .line 852
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 13
    .parameter "x0"
    .parameter "x1"

    .prologue
    const-wide/16 v8, 0x0

    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 84
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_b0

    :cond_d
    :goto_d
    return-void

    :pswitch_e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->loadAlbumName()V

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_26

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->refresh()V

    :cond_26
    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    cmp-long v1, v1, v8

    if-lez v1, :cond_d

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getTimestampForItem(I)J

    move-result-wide v3

    cmp-long v5, v3, v1

    if-lez v5, :cond_d

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v5, v6, v3, v4}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLastPhotoNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    iput-wide v8, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLastNotificationTime:J

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->clear()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    :goto_57
    if-ge v0, v3, :cond_71

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getTimestampForItem(I)J

    move-result-wide v4

    cmp-long v4, v4, v1

    if-lez v4, :cond_71

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getMediaRefForItem(I)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_57

    :cond_71
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_d

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    goto :goto_d

    :pswitch_7e
    if-eqz p2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    if-nez v2, :cond_9b

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    if-eqz v1, :cond_9b

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    :cond_9b
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_d

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_ab

    const/4 v0, -0x2

    :goto_a7
    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    goto/16 :goto_d

    :cond_ab
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_a7

    :pswitch_data_b0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_7e
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 760
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .registers 4
    .parameter "v"

    .prologue
    .line 1137
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v1

    if-nez v1, :cond_c

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isInstantUploadAlbum()Z

    move-result v1

    if-eqz v1, :cond_24

    .line 1138
    :cond_c
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    .line 1139
    const v1, 0x7f09003d

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1140
    .local v0, position:I
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->select(I)V

    .line 1141
    const/4 v1, 0x1

    .line 1143
    .end local v0           #position:I
    :goto_23
    return v1

    :cond_24
    const/4 v1, 0x0

    goto :goto_23
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    .line 877
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_14

    .line 887
    const/4 v0, 0x0

    .line 891
    :goto_8
    return v0

    .line 879
    :pswitch_9
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updatePickerMode(I)V

    .line 891
    :goto_d
    const/4 v0, 0x1

    goto :goto_8

    .line 883
    :pswitch_f
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showDeleteConfirmationDialog()V

    goto :goto_d

    .line 877
    nop

    :pswitch_data_14
    .packed-switch 0x7f0902c3
        :pswitch_9
        :pswitch_f
    .end packed-switch
.end method

.method public final onPause()V
    .registers 3

    .prologue
    .line 599
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 601
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/phone/Pageable;->setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V

    .line 602
    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 12
    .parameter "actionBar"

    .prologue
    const v9, 0x7f0e0001

    const v8, 0x7f080094

    const v7, 0x7f020093

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 764
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 766
    invoke-virtual {p1, p0}, Lcom/google/android/apps/plus/views/HostActionBar;->setOnDoneButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;)V

    .line 768
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    packed-switch v2, :pswitch_data_82

    .line 806
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->finishContextActionMode()V

    .line 807
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    .line 808
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v2

    if-nez v2, :cond_27

    .line 809
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    .line 814
    :cond_27
    :goto_27
    return-void

    .line 770
    :pswitch_28
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    .line 771
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v2

    if-nez v2, :cond_27

    .line 772
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    goto :goto_27

    .line 777
    :pswitch_37
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 778
    .local v0, count:I
    if-gtz v0, :cond_43

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerShareWithZeroSelected:Z

    if-eqz v2, :cond_5a

    .line 779
    :cond_43
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v9, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 781
    .local v1, selected:Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    .line 783
    invoke-virtual {p1, v5, v7, v8}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    goto :goto_27

    .line 786
    .end local v1           #selected:Ljava/lang/String;
    :cond_5a
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    goto :goto_27

    .line 792
    .end local v0           #count:I
    :pswitch_60
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->startContextActionMode()V

    .line 794
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 795
    .restart local v0       #count:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v2, v9, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 797
    .restart local v1       #selected:Ljava/lang/String;
    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    .line 799
    if-lez v0, :cond_27

    .line 800
    invoke-virtual {p1, v5, v7, v8}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    goto :goto_27

    .line 768
    :pswitch_data_82
    .packed-switch 0x1
        :pswitch_28
        :pswitch_37
        :pswitch_60
    .end packed-switch
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 8
    .parameter "menu"

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 818
    const v3, 0x7f0902c3

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 819
    .local v2, selectItemMenu:Landroid/view/MenuItem;
    const v3, 0x7f0902c4

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 821
    .local v1, deleteMenu:Landroid/view/MenuItem;
    iget v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    packed-switch v3, :pswitch_data_4e

    .line 844
    :cond_15
    :goto_15
    :pswitch_15
    return-void

    .line 823
    :pswitch_16
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isLocalCameraAlbum()Z

    move-result v3

    if-nez v3, :cond_22

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isInstantUploadAlbum()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 824
    :cond_22
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 825
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_15

    .line 834
    :pswitch_29
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 836
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v0

    .line 837
    .local v0, count:I
    if-lez v0, :cond_4a

    .line 838
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0010

    invoke-virtual {v3, v4, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 840
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_15

    .line 842
    :cond_4a
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_15

    .line 821
    :pswitch_data_4e
    .packed-switch 0x0
        :pswitch_16
        :pswitch_15
        :pswitch_15
        :pswitch_29
    .end packed-switch
.end method

.method public final onResume()V
    .registers 4

    .prologue
    .line 567
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    .line 569
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 571
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/Pageable;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    .line 572
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/phone/Pageable;->setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V

    .line 573
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    if-eqz v1, :cond_2a

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPageableLoader:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1}, Lcom/google/android/apps/plus/phone/Pageable;->isDataSourceLoading()Z

    move-result v1

    if-nez v1, :cond_2a

    .line 574
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->onDataSourceLoading(Z)V

    .line 577
    :cond_2a
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_47

    .line 578
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-eqz v1, :cond_73

    .line 579
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_47

    .line 580
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 588
    :cond_47
    :goto_47
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_6a

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_6a

    .line 589
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 590
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handlePhotoDelete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 593
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_6a
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->onResume()V

    .line 594
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateSpinner()V

    .line 595
    return-void

    .line 583
    :cond_73
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 584
    .restart local v0       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_47
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    .prologue
    .line 606
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 608
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    if-eqz v1, :cond_74

    .line 609
    const-string v1, "INTENT"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mExtras:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 610
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    if-eqz v1, :cond_19

    .line 611
    const-string v1, "ALBUM_NAME"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumName:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    :cond_19
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    if-eqz v1, :cond_32

    .line 614
    const-string v1, "STATE_PICKER_MODE"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerMode:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 615
    const-string v1, "STATE_PICKER_TITLE"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerTitleResourceId:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 616
    const-string v1, "STATE_PICKER_SHARE_ON_ZERO"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPickerShareWithZeroSelected:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 618
    :cond_32
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    if-lez v1, :cond_4c

    .line 619
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v0, v1, [Lcom/google/android/apps/plus/api/MediaRef;

    .line 620
    .local v0, selectedItems:[Lcom/google/android/apps/plus/api/MediaRef;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 621
    const-string v1, "SELECTED_ITEMS"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 623
    .end local v0           #selectedItems:[Lcom/google/android/apps/plus/api/MediaRef;
    :cond_4c
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_5b

    .line 624
    const-string v1, "refresh_request"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 626
    :cond_5b
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_6a

    .line 627
    const-string v1, "delete_request"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mDeleteReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 629
    :cond_6a
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mLoaderActive:Z

    if-eqz v1, :cond_74

    .line 630
    const-string v1, "loader_active"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 633
    :cond_74
    return-void
.end method

.method public final onStop()V
    .registers 2

    .prologue
    .line 561
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStop()V

    .line 562
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->onStop()V

    .line 563
    return-void
.end method

.method public final onViewUsed(I)V
    .registers 8
    .parameter "position"

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1102
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAdapter:Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/AlbumGridViewAdapter;->getCount()I

    move-result v0

    .line 1105
    .local v0, currentCount:I
    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumCount:I

    if-ne v0, v5, :cond_d

    .line 1128
    :cond_c
    :goto_c
    return-void

    .line 1110
    :cond_d
    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mCount:I

    if-eq v5, v0, :cond_15

    .line 1111
    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mCount:I

    .line 1112
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshable:Z

    .line 1115
    :cond_15
    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshable:Z

    if-eqz v5, :cond_c

    .line 1116
    const/16 v5, 0x10

    if-ge v0, v5, :cond_26

    move v1, v3

    .line 1117
    .local v1, isPartialAlbum:Z
    :goto_1e
    if-eqz v1, :cond_28

    .line 1118
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->refresh()V

    .line 1124
    :cond_23
    :goto_23
    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshable:Z

    goto :goto_c

    .end local v1           #isPartialAlbum:Z
    :cond_26
    move v1, v4

    .line 1116
    goto :goto_1e

    .line 1121
    .restart local v1       #isPartialAlbum:Z
    :cond_28
    add-int/lit8 v5, v0, -0x40

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 1122
    .local v2, threshold:I
    if-lt p1, v2, :cond_c

    .line 1123
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v3

    if-eqz v3, :cond_23

    instance-of v5, v3, Lcom/google/android/apps/plus/phone/Pageable;

    if-eqz v5, :cond_49

    check-cast v3, Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/Pageable;->hasMore()Z

    move-result v5

    if-eqz v5, :cond_49

    invoke-interface {v3}, Lcom/google/android/apps/plus/phone/Pageable;->loadMore()V

    :cond_49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    goto :goto_23
.end method

.method public final refresh()V
    .registers 7

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 1262
    :goto_4
    return-void

    .line 1248
    :cond_5
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    .line 1250
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    if-eqz v0, :cond_33

    .line 1251
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mStreamId:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/16 v5, 0x1f4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->getStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    .line 1261
    :cond_2b
    :goto_2b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->updateView(Landroid/view/View;)V

    goto :goto_4

    .line 1253
    :cond_33
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPhotoOfUserId:Ljava/lang/String;

    if-eqz v0, :cond_4a

    .line 1254
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mPhotoOfUserId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getPhotosOfUser(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    goto :goto_2b

    .line 1255
    :cond_4a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    if-eqz v0, :cond_63

    .line 1256
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAlbumId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->getAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    goto :goto_2b

    .line 1257
    :cond_63
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    if-eqz v0, :cond_2b

    .line 1258
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mOwnerId:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mRefreshReqId:Ljava/lang/Integer;

    goto :goto_2b
.end method

.method protected final shareSelectedPhotos()V
    .registers 7

    .prologue
    .line 1150
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mSelectedPhotoMediaRefs:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1152
    .local v1, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 1154
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->startActivity(Landroid/content/Intent;)V

    .line 1156
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mHereFromNotification:Z

    if-eqz v2, :cond_25

    .line 1157
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->SHARE_INSTANT_UPLOAD_FROM_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1160
    :cond_25
    return-void
.end method
