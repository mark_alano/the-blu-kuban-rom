.class final Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "HostedProfileFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlePlusOneCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 178
    return-void
.end method

.method public final onDeleteProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlePlusOneCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 184
    return-void
.end method

.method public final onGetProfileAndContactComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 156
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 157
    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onGetProfileAndContactComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleProfileServiceCallback$b5e9bbb(I)V

    .line 160
    return-void
.end method

.method public final onInsertCameraPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 212
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 213
    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onInsertCameraPhotoComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #calls: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlerInsertCameraPhoto$b5e9bbb(I)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$200(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 216
    return-void
.end method

.method public final onReportAbuseRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleReportAbuseCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 172
    return-void
.end method

.method public final onSetCircleMemebershipComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 189
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 190
    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSetCircleMemebershipComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleProfileServiceCallback$b5e9bbb(I)V

    .line 193
    return-void
.end method

.method public final onSetMutedRequestComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "isMuted"
    .parameter "result"

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleSetMutedCallback(IZLcom/google/android/apps/plus/service/ServiceResult;)V

    .line 166
    return-void
.end method

.method public final onSetProfilePhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 8
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 198
    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 199
    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSetProfilePhotoComplete(); requestId="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleProfileServiceCallback$b5e9bbb(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$100(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v1

    #setter for: Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->access$002(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    .line 207
    return-void
.end method
