.class public Lcom/google/android/apps/plus/fragments/HostedProfileFragment;
.super Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.source "HostedProfileFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;
.implements Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;
.implements Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;
.implements Lcom/google/android/apps/plus/fragments/UnblockPersonDialog$PersonUnblocker;
.implements Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedStreamFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/BlockFragment$Listener;",
        "Lcom/google/android/apps/plus/fragments/BlockPersonDialog$PersonBlocker;",
        "Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;",
        "Lcom/google/android/apps/plus/fragments/UnblockPersonDialog$PersonUnblocker;",
        "Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;"
    }
.end annotation


# instance fields
.field private mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

.field private mBlockInProgress:Z

.field private final mCircleContentObserver:Landroid/database/DataSetObserver;

.field private mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mContext:Landroid/content/Context;

.field private mControlPrimarySpinner:Z

.field private mCurrentSpinnerPosition:I

.field private final mHandler:Landroid/os/Handler;

.field private mHasGaiaId:Z

.field private mInsertCameraPhotoRequestId:Ljava/lang/Integer;

.field private mIsBlocked:Z

.field private mIsMute:Z

.field private mIsMyProfile:Z

.field private mIsPlusPage:Z

.field private mLandscape:Z

.field private mMapLoaderActive:Z

.field private mMuteRequestId:Ljava/lang/Integer;

.field private mMuteRequestIsMuted:Z

.field private mPersonId:Ljava/lang/String;

.field private final mPlacesMapLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mPlacesMapUrl:Ljava/lang/String;

.field private mPlusOneRequestId:Ljava/lang/Integer;

.field private mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

.field private final mProfileAndContactDataLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation
.end field

.field private mProfileAndContactLoaderActive:Z

.field private mProfileIsExpanded:Z

.field private mProfilePendingRequestId:Ljava/lang/Integer;

.field private final mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mReportAbuseRequestId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x1

    .line 84
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;-><init>()V

    .line 122
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    .line 128
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mControlPrimarySpinner:Z

    .line 149
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactLoaderActive:Z

    .line 150
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMapLoaderActive:Z

    .line 152
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 219
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactDataLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 293
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlacesMapLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 335
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$4;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    .line 356
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMute:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/lang/String;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlacesMapUrl:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_24

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlacesMapUrl:Ljava/lang/String;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "map_url"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlacesMapUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMapLoaderActive:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/16 v2, 0x65

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlacesMapLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_24
    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)Z
    .registers 6
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileUpdateTime:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xdbba0

    cmp-long v0, v0, v2

    if-lez v0, :cond_20

    const-string v0, "HostedProfileFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1e

    const-string v0, "HostedProfileFragment"

    const-string v1, "Refreshing because profile info is stale."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1e
    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlerInsertCameraPhoto$b5e9bbb(I)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactLoaderActive:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMapLoaderActive:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;
    .registers 2
    .parameter "x0"

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z

    return p1
.end method

.method private canShowConversationActions()Z
    .registers 2

    .prologue
    .line 690
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactLoaderActive:Z

    if-nez v0, :cond_22

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v0, :cond_22

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v0

    if-nez v0, :cond_22

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z

    if-nez v0, :cond_22

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v0, :cond_22

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mError:Z

    if-nez v0, :cond_22

    const/4 v0, 0x1

    :goto_21
    return v0

    :cond_22
    const/4 v0, 0x0

    goto :goto_21
.end method

.method private canShowRefreshInActionBar()Z
    .registers 3

    .prologue
    .line 685
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v0

    .line 686
    .local v0, metrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    iget v1, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v1, :cond_e

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mLandscape:Z

    if-eqz v1, :cond_10

    :cond_e
    const/4 v1, 0x1

    :goto_f
    return v1

    :cond_10
    const/4 v1, 0x0

    goto :goto_f
.end method

.method private handlerInsertCameraPhoto$b5e9bbb(I)V
    .registers 11
    .parameter "requestId"

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1180
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_f

    .line 1188
    :cond_e
    :goto_e
    return-void

    .line 1184
    :cond_f
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    if-nez v0, :cond_3b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f080167

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_2b
    instance-of v0, v7, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    if-eqz v0, :cond_35

    move-object v0, v7

    check-cast v0, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    invoke-interface {v0}, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;->hideInsertCameraPhotoDialog()V

    .line 1186
    :cond_35
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    .line 1187
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    goto :goto_e

    .line 1184
    :cond_3b
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const-wide/16 v2, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const v3, 0x7f0802ce

    invoke-virtual {v7, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v2, v3, v0, v8}, Lcom/google/android/apps/plus/phone/Intents;->getPhotoPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_2b
.end method

.method private safeStartActivity(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    .prologue
    .line 384
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_3} :catch_4

    .line 390
    :cond_3
    :goto_3
    return-void

    .line 385
    :catch_4
    move-exception v0

    .line 386
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v1, "HostedProfileFragment"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 387
    const-string v1, "HostedProfileFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot launch activity: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3
.end method

.method private setPersonBlocked(Z)V
    .registers 9
    .parameter "blocked"

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusPage()Z

    move-result v4

    move v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/BlockFragment;->getInstance(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/apps/plus/fragments/BlockFragment;

    move-result-object v6

    .line 560
    .local v6, dialog:Lcom/google/android/apps/plus/fragments/BlockFragment;
    const/4 v0, 0x0

    invoke-virtual {v6, p0, v0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 561
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/fragments/BlockFragment;->show(Landroid/support/v4/app/FragmentActivity;)V

    .line 562
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    .line 563
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->beginBlockInProgress()V

    .line 564
    return-void
.end method

.method private showProgressDialog(I)V
    .registers 6
    .parameter "resId"

    .prologue
    .line 1410
    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    .line 1413
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1414
    return-void
.end method


# virtual methods
.method public final blockPerson(Ljava/io/Serializable;)V
    .registers 3
    .parameter "callbackData"

    .prologue
    .line 537
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->setPersonBlocked(Z)V

    .line 538
    return-void
.end method

.method protected final createStreamAdapter(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;Landroid/view/View;)Lcom/google/android/apps/plus/phone/StreamAdapter;
    .registers 21
    .parameter "context"
    .parameter "gridView"
    .parameter "account"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewUseListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"
    .parameter "floatingComposeBarView"

    .prologue
    .line 786
    move-object/from16 v10, p5

    .line 788
    .local v10, originalListener:Lcom/google/android/apps/plus/views/ItemClickListener;
    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$5;

    invoke-direct {v5, p0, v10}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Lcom/google/android/apps/plus/views/ItemClickListener;)V

    .line 812
    .local v5, filteredItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;
    new-instance v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    const/4 v9, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;Landroid/view/View;)V

    return-object v0
.end method

.method public final doPickPhotoFromAlbums()V
    .registers 5

    .prologue
    const/4 v3, 0x1

    .line 1333
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newAlbumsActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    .line 1334
    .local v0, builder:Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPersonId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotosHome(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setShowCameraAlbum(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const v2, 0x7f08008d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerTitleResourceId(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAllowCrop(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    .line 1341
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1342
    return-void
.end method

.method protected final doShowEmptyView(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    .prologue
    .line 1522
    return-void
.end method

.method protected final doShowEmptyViewProgress(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    .prologue
    .line 1528
    return-void
.end method

.method public final doTakePhoto()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 1323
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v2, "camera-profile.jpg"

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntent$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1324
    .local v1, intent:Landroid/content/Intent;
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_e
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_e} :catch_f

    .line 1329
    .end local v1           #intent:Landroid/content/Intent;
    :goto_e
    return-void

    .line 1325
    :catch_f
    move-exception v0

    .line 1326
    .local v0, e:Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0802cd

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_e
.end method

.method protected final getComposeBarCursor()Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .registers 2

    .prologue
    .line 775
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getExtrasForLogging()Landroid/os/Bundle;
    .registers 3

    .prologue
    .line 1508
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1509
    const-string v0, "extra_gaia_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 1511
    :goto_10
    return-object v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 1502
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getViewIsExpanded()Z

    move-result v0

    if-eqz v0, :cond_f

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_e
    return-object v0

    :cond_f
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_e
.end method

.method protected final handlePlusOneCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1164
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_d

    .line 1174
    :cond_c
    :goto_c
    return-void

    .line 1168
    :cond_d
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    .line 1169
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    .line 1171
    if-eqz p2, :cond_c

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1172
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f0801a8

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_c
.end method

.method protected final handleProfileServiceCallback$b5e9bbb(I)V
    .registers 5
    .parameter "requestId"

    .prologue
    .line 1092
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_d

    .line 1104
    :cond_c
    :goto_c
    return-void

    .line 1096
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 1098
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1e

    .line 1099
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 1102
    :cond_1e
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    .line 1103
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    goto :goto_c
.end method

.method protected final handleReportAbuseCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v3, 0x0

    .line 1139
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_e

    .line 1158
    :cond_d
    :goto_d
    return-void

    .line 1143
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 1145
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1f

    .line 1146
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 1149
    :cond_1f
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    .line 1150
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    .line 1152
    if-eqz p2, :cond_3a

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 1153
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0801a8

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 1155
    :cond_3a
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f080279

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d
.end method

.method protected final handleSetMutedCallback(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 8
    .parameter "requestId"
    .parameter "isMuted"
    .parameter "result"

    .prologue
    const/4 v3, 0x0

    .line 1110
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_e

    .line 1132
    :cond_d
    :goto_d
    return-void

    .line 1114
    :cond_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 1116
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1f

    .line 1117
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 1120
    :cond_1f
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    .line 1121
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    .line 1123
    if-eqz p3, :cond_3a

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 1124
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0801a8

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 1125
    :cond_3a
    if-eqz p2, :cond_49

    .line 1126
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f080270

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 1129
    :cond_49
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f080271

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_d
.end method

.method protected final initCirclesLoader()V
    .registers 1

    .prologue
    .line 601
    return-void
.end method

.method protected final isAdapterEmpty()Z
    .registers 3

    .prologue
    const/4 v0, 0x1

    .line 1264
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;->getCount()I

    move-result v1

    if-ne v1, v0, :cond_a

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method protected final isLocalDataAvailable(Landroid/database/Cursor;)Z
    .registers 4
    .parameter "data"

    .prologue
    const/4 v0, 0x1

    .line 851
    if-eqz p1, :cond_a

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-le v1, v0, :cond_a

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method protected final isProgressIndicatorVisible()Z
    .registers 2

    .prologue
    .line 747
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-nez v0, :cond_1e

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactLoaderActive:Z

    if-nez v0, :cond_1e

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMapLoaderActive:Z

    if-nez v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_20

    :cond_1e
    const/4 v0, 0x1

    :goto_1f
    return v0

    :cond_20
    const/4 v0, 0x0

    goto :goto_1f
.end method

.method protected final needsAsyncData()Z
    .registers 2

    .prologue
    .line 1516
    const/4 v0, 0x1

    return v0
.end method

.method public final onActionButtonClicked(I)V
    .registers 11
    .parameter "actionId"

    .prologue
    const/4 v8, 0x2

    .line 971
    const/4 v4, 0x0

    .line 972
    .local v4, intent:Landroid/content/Intent;
    packed-switch p1, :pswitch_data_74

    .line 1004
    :cond_5
    :goto_5
    if-eqz v4, :cond_a

    .line 1009
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    .line 1011
    :cond_a
    return-void

    .line 974
    :pswitch_b
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v2

    .line 975
    .local v2, fullName:Ljava/lang/String;
    const/4 v3, 0x0

    .line 976
    .local v3, gaiaId:Ljava/lang/String;
    const/4 v1, 0x0

    .line 977
    .local v1, email:Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const-string v7, "e:"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_38

    .line 978
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 984
    :goto_23
    new-instance v5, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v5, v3, v2, v1}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 985
    .local v5, user:Lcom/google/android/apps/plus/content/PersonData;
    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    .line 986
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-static {v6, v7, v0}, Lcom/google/android/apps/plus/phone/Intents;->getNewConversationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v4

    .line 987
    goto :goto_5

    .line 979
    .end local v0           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v5           #user:Lcom/google/android/apps/plus/content/PersonData;
    :cond_38
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v6, :cond_5

    .line 980
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    goto :goto_23

    .line 990
    .end local v1           #email:Ljava/lang/String;
    .end local v2           #fullName:Ljava/lang/String;
    .end local v3           #gaiaId:Ljava/lang/String;
    :pswitch_3f
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v2

    .line 991
    .restart local v2       #fullName:Ljava/lang/String;
    const/4 v3, 0x0

    .line 992
    .restart local v3       #gaiaId:Ljava/lang/String;
    const/4 v1, 0x0

    .line 993
    .restart local v1       #email:Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const-string v7, "e:"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6d

    .line 994
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1000
    :goto_57
    new-instance v5, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v5, v3, v2, v1}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    .restart local v5       #user:Lcom/google/android/apps/plus/content/PersonData;
    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    .line 1002
    .restart local v0       #audience:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v6, v7, v8, v0}, Lcom/google/android/apps/plus/phone/Intents;->getNewHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLcom/google/android/apps/plus/content/AudienceData;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_5

    .line 995
    .end local v0           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v5           #user:Lcom/google/android/apps/plus/content/PersonData;
    :cond_6d
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v6, :cond_5

    .line 996
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    goto :goto_57

    .line 972
    :pswitch_data_74
    .packed-switch 0x0
        :pswitch_b
        :pswitch_3f
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 10
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 1448
    const/4 v4, -0x1

    if-eq p2, v4, :cond_4

    .line 1490
    :cond_3
    :goto_3
    return-void

    .line 1452
    :cond_4
    packed-switch p1, :pswitch_data_56

    goto :goto_3

    .line 1454
    :pswitch_8
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "original_circle_ids"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1456
    .local v2, originalCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "selected_circle_ids"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1458
    .local v3, selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$6;

    invoke-direct {v5, p0, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 1468
    .end local v2           #originalCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3           #selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :pswitch_27
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1469
    .local v0, activity:Landroid/app/Activity;
    instance-of v4, v0, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    if-eqz v4, :cond_35

    move-object v4, v0

    .line 1470
    check-cast v4, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    invoke-interface {v4}, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    .line 1473
    :cond_35
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v5, "camera-profile.jpg"

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    goto :goto_3

    .line 1479
    .end local v0           #activity:Landroid/app/Activity;
    :pswitch_40
    if-eqz p3, :cond_3

    .line 1481
    const-string v4, "data"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 1482
    .local v1, imageBytes:[B
    if-eqz v1, :cond_3

    .line 1483
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$7;

    invoke-direct {v5, p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/HostedProfileFragment;[B)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 1452
    nop

    :pswitch_data_56
    .packed-switch 0x0
        :pswitch_8
        :pswitch_27
        :pswitch_40
        :pswitch_40
    .end packed-switch
.end method

.method public final onAddressClicked(Ljava/lang/String;)V
    .registers 5
    .parameter "address"

    .prologue
    .line 447
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_24

    .line 448
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "geo:0,0?q="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 449
    .local v0, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/util/MapUtils;->launchMapsActivity(Landroid/content/Context;Landroid/net/Uri;)V

    .line 451
    .end local v0           #uri:Landroid/net/Uri;
    :cond_24
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    .prologue
    .line 767
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onAttach(Landroid/app/Activity;)V

    .line 768
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    .line 769
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_16

    const/4 v0, 0x1

    :goto_13
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mLandscape:Z

    .line 771
    return-void

    .line 769
    :cond_16
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public final onAvatarClicked()V
    .registers 5

    .prologue
    .line 369
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-eqz v1, :cond_24

    .line 370
    new-instance v1, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;

    const v2, 0x7f0802c7

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;-><init>(IZ)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/Intents;->isCameraIntentRegistered(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setIsCameraSupported(Z)V

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "change_photo"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 380
    :goto_23
    return-void

    .line 372
    :cond_24
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const v2, 0x7f0802cc

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    .line 378
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_23
.end method

.method public final onBlockCompleted(Z)V
    .registers 3
    .parameter "success"

    .prologue
    .line 1441
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    .line 1442
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->endBlockInProgress(Z)V

    .line 1443
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V

    .line 1444
    return-void
.end method

.method public final onCirclesButtonClicked()V
    .registers 6

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getCircleMembershipActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 396
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    .prologue
    .line 605
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreate(Landroid/os/Bundle;)V

    .line 607
    if-eqz p1, :cond_91

    .line 608
    const-string v1, "profile_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 609
    const-string v1, "profile_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    .line 612
    :cond_19
    const-string v1, "plusone_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2d

    .line 613
    const-string v1, "plusone_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    .line 615
    :cond_2d
    const-string v1, "abuse_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_41

    .line 616
    const-string v1, "abuse_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    .line 619
    :cond_41
    const-string v1, "mute_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5d

    .line 620
    const-string v1, "mute_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    .line 621
    const-string v1, "mute_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestIsMuted:Z

    .line 623
    :cond_5d
    const-string v1, "camera_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_71

    .line 624
    const-string v1, "camera_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    .line 627
    :cond_71
    const-string v1, "block_in_progress"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_81

    .line 628
    const-string v1, "block_in_progress"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    .line 631
    :cond_81
    const-string v1, "profile_is_expanded"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_91

    .line 632
    const-string v1, "profile_is_expanded"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileIsExpanded:Z

    .line 637
    :cond_91
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 638
    .local v0, args:Landroid/os/Bundle;
    const-string v1, "person_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/16 v2, 0x64

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAndContactDataLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 645
    new-instance v0, Landroid/os/Bundle;

    .end local v0           #args:Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 646
    .restart local v0       #args:Landroid/os/Bundle;
    const-string v1, "map_url"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlacesMapUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/16 v2, 0x65

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlacesMapLoader:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 649
    new-instance v1, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 650
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleContentObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 651
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    .line 655
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 656
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 7
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x3

    .line 836
    const-string v0, "HostedProfileFragment"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 841
    const-string v1, "HostedProfileFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "Loader<Cursor> onCreateLoader() -- "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-ne p1, v3, :cond_26

    const-string v0, "POSTS_LOADER_ID"

    :goto_16
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 844
    :cond_21
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;

    move-result-object v0

    return-object v0

    .line 841
    :cond_26
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_16
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 14
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 820
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v6

    .line 822
    .local v6, view:Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    .line 823
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.SENDTO"

    const-string v7, "sms"

    const-string v8, ""

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v4, v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/high16 v7, 0x1

    invoke-virtual {v5, v4, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_52

    const/4 v4, 0x1

    :goto_37
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->init(Ljava/lang/String;ZZZLcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    .line 825
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->setOnClickListener(Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;)V

    .line 826
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-eqz v0, :cond_4a

    .line 827
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->beginBlockInProgress()V

    .line 829
    :cond_4a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileIsExpanded:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->setViewIsExpanded(Z)V

    .line 831
    return-object v6

    .line 823
    :cond_52
    const/4 v4, 0x0

    goto :goto_37
.end method

.method public final onEmailClicked(Ljava/lang/String;)V
    .registers 8
    .parameter "email"

    .prologue
    .line 408
    const/4 v2, 0x0

    .line 410
    .local v2, parsed:[Landroid/text/util/Rfc822Token;
    if-eqz p1, :cond_7

    .line 411
    invoke-static {p1}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v2

    .line 414
    :cond_7
    if-eqz v2, :cond_c

    array-length v3, v2

    if-nez v3, :cond_d

    .line 427
    :cond_c
    :goto_c
    return-void

    .line 418
    :cond_d
    const/4 v3, 0x0

    aget-object v0, v2, v3

    .line 419
    .local v0, formattedEmail:Landroid/text/util/Rfc822Token;
    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2f

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2f

    .line 421
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/util/Rfc822Token;->setName(Ljava/lang/String;)V

    .line 424
    :cond_2f
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SENDTO"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "mailto:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/text/util/Rfc822Token;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 426
    .local v1, intent:Landroid/content/Intent;
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    goto :goto_c
.end method

.method public final onExpandClicked(Z)V
    .registers 10
    .parameter "isExpanded"

    .prologue
    const/4 v4, 0x0

    .line 514
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileIsExpanded:Z

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->setViewIsExpanded(Z)V

    .line 516
    if-eqz p1, :cond_20

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getExtrasForLogging()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    move-object v5, v4

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordNavigationEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 517
    return-void

    .line 516
    :cond_20
    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->PROFILE:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_e
.end method

.method public final onLinkClicked(Ljava/lang/String;)V
    .registers 5
    .parameter "url"

    .prologue
    .line 455
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 456
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 457
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 458
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    .line 460
    .end local v0           #intent:Landroid/content/Intent;
    :cond_19
    return-void
.end method

.method public final onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 9
    .parameter
    .parameter "data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 856
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_a6

    .line 875
    const-string v0, "HostedProfileFragment"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 876
    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loader<Cursor> onLoadFinished() -- "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    :cond_29
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 881
    :cond_2c
    :goto_2c
    return-void

    .line 858
    :pswitch_2d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    if-eqz v0, :cond_39

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->wrapsStreamCursor(Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_63

    .line 860
    :cond_39
    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-interface {p2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;I)V

    sget-object v1, Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;->PROJECTION_STREAM:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [Ljava/lang/Object;

    const/16 v2, 0xe

    const/16 v3, 0x200

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    aput-object p2, v1, v4

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;-><init>([Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    .line 862
    :cond_63
    const-string v0, "HostedProfileFragment"

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8b

    .line 863
    const-string v0, "HostedProfileFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loader<Cursor> onLoadFinished() -- POSTS_LOADER_ID, "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rows"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    :cond_8b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-super {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    .line 867
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mActiveProfileCursor:Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment$ProfileMergeCursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2c

    .line 868
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->notifyDataSetChanged()V

    .line 869
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showContent(Landroid/view/View;)V

    goto :goto_2c

    .line 856
    nop

    :pswitch_data_a6
    .packed-switch 0x3
        :pswitch_2d
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 84
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 885
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onLocalCallClicked(Ljava/lang/String;)V
    .registers 6
    .parameter "phoneNumber"

    .prologue
    .line 464
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tel:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startExternalActivity(Landroid/content/Intent;)V

    .line 466
    return-void
.end method

.method public final onLocalDirectionsClicked(Ljava/lang/String;)V
    .registers 4
    .parameter "url"

    .prologue
    .line 470
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 471
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/MapUtils;->launchMapsActivity(Landroid/content/Context;Landroid/net/Uri;)V

    .line 473
    :cond_11
    return-void
.end method

.method public final onLocalMapClicked(Ljava/lang/String;)V
    .registers 4
    .parameter "url"

    .prologue
    .line 477
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 478
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/MapUtils;->launchMapsActivity(Landroid/content/Context;Landroid/net/Uri;)V

    .line 480
    :cond_11
    return-void
.end method

.method public final onLocalReviewClicked(II)V
    .registers 8
    .parameter "reviewType"
    .parameter "reviewIndex"

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/LocalReviewActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "person_id"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "local_review_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "local_review_index"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 486
    .local v0, intent:Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    .line 487
    return-void
.end method

.method public final onLocationClicked(Ljava/lang/String;)V
    .registers 5
    .parameter "address"

    .prologue
    .line 400
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_24

    .line 401
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "geo:0,0?q="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 402
    .local v0, uri:Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/util/MapUtils;->launchMapsActivity(Landroid/content/Context;Landroid/net/Uri;)V

    .line 404
    .end local v0           #uri:Landroid/net/Uri;
    :cond_24
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 11
    .parameter "item"

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    .line 1192
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    sparse-switch v6, :sswitch_data_f8

    .line 1255
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    :goto_d
    return v5

    .line 1194
    :sswitch_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->refresh()V

    goto :goto_d

    .line 1198
    :sswitch_12
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    if-eqz v6, :cond_4b

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v4

    .line 1200
    .local v4, name:Ljava/lang/String;
    :goto_1c
    new-instance v1, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;-><init>()V

    .line 1201
    .local v1, dialog:Lcom/google/android/apps/plus/fragments/MuteProfileDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1202
    .local v0, args:Landroid/os/Bundle;
    const-string v6, "name"

    invoke-virtual {v0, v6, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1203
    const-string v6, "gender"

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getGender()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    const-string v6, "target_mute"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1205
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->setArguments(Landroid/os/Bundle;)V

    .line 1206
    invoke-virtual {v1, p0, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1207
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "mute_profile"

    invoke-virtual {v1, v6, v7}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_d

    .line 1198
    .end local v0           #args:Landroid/os/Bundle;
    .end local v1           #dialog:Lcom/google/android/apps/plus/fragments/MuteProfileDialog;
    .end local v4           #name:Ljava/lang/String;
    :cond_4b
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getGivenName()Ljava/lang/String;

    move-result-object v4

    goto :goto_1c

    .line 1211
    :sswitch_52
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    if-eqz v6, :cond_8b

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v4

    .line 1213
    .restart local v4       #name:Ljava/lang/String;
    :goto_5c
    new-instance v1, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;-><init>()V

    .line 1214
    .restart local v1       #dialog:Lcom/google/android/apps/plus/fragments/MuteProfileDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1215
    .restart local v0       #args:Landroid/os/Bundle;
    const-string v6, "name"

    invoke-virtual {v0, v6, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    const-string v6, "gender"

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getGender()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    const-string v6, "target_mute"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1218
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->setArguments(Landroid/os/Bundle;)V

    .line 1219
    invoke-virtual {v1, p0, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1220
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "unmute_profile"

    invoke-virtual {v1, v6, v7}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_d

    .line 1211
    .end local v0           #args:Landroid/os/Bundle;
    .end local v1           #dialog:Lcom/google/android/apps/plus/fragments/MuteProfileDialog;
    .end local v4           #name:Ljava/lang/String;
    :cond_8b
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getGivenName()Ljava/lang/String;

    move-result-object v4

    goto :goto_5c

    .line 1224
    :sswitch_92
    new-instance v1, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusPage()Z

    move-result v6

    invoke-direct {v1, v6}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;-><init>(Z)V

    .line 1226
    .local v1, dialog:Lcom/google/android/apps/plus/fragments/BlockPersonDialog;
    invoke-virtual {v1, p0, v8}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1227
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "block_person"

    invoke-virtual {v1, v6, v7}, Lcom/google/android/apps/plus/fragments/BlockPersonDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 1232
    .end local v1           #dialog:Lcom/google/android/apps/plus/fragments/BlockPersonDialog;
    :sswitch_ab
    new-instance v1, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusPage()Z

    move-result v7

    invoke-direct {v1, v6, v7}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;-><init>(Ljava/lang/String;Z)V

    .line 1234
    .local v1, dialog:Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;
    invoke-virtual {v1, p0, v8}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1235
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "unblock_person"

    invoke-virtual {v1, v6, v7}, Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 1240
    .end local v1           #dialog:Lcom/google/android/apps/plus/fragments/UnblockPersonDialog;
    :sswitch_c6
    new-instance v1, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;-><init>()V

    .line 1241
    .local v1, dialog:Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;
    invoke-virtual {v1, p0, v8}, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1242
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    const-string v7, "report_abuse"

    invoke-virtual {v1, v6, v7}, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 1247
    .end local v1           #dialog:Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;
    :sswitch_d9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080410

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1249
    .local v3, helpUrlParam:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6, v3}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1250
    .local v2, helpUrl:Landroid/net/Uri;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v6, v7, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startExternalActivity(Landroid/content/Intent;)V

    goto/16 :goto_d

    .line 1192
    :sswitch_data_f8
    .sparse-switch
        0x7f09028d -> :sswitch_e
        0x7f090290 -> :sswitch_d9
        0x7f0902b5 -> :sswitch_12
        0x7f0902b6 -> :sswitch_52
        0x7f0902b7 -> :sswitch_92
        0x7f0902b8 -> :sswitch_ab
        0x7f0902b9 -> :sswitch_c6
    .end sparse-switch
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 1078
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onPause()V

    .line 1079
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->onPause()V

    .line 1080
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->onPause()V

    .line 1082
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 1083
    return-void
.end method

.method public final onPhoneNumberClicked(Ljava/lang/String;)V
    .registers 6
    .parameter "number"

    .prologue
    .line 431
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_27

    .line 432
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tel:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 433
    .local v0, intent:Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    .line 435
    .end local v0           #intent:Landroid/content/Intent;
    :cond_27
    return-void
.end method

.method public final onPlusOneClicked()V
    .registers 4

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->isPlusOnedByMe()Z

    move-result v0

    if-eqz v0, :cond_23

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isProfilePlusOnePending(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->deleteProfilePlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    .line 510
    :cond_22
    :goto_22
    return-void

    .line 508
    :cond_23
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isProfilePlusOnePending(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_22

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->createProfilePlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    goto :goto_22
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 8
    .parameter "actionBar"

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 716
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mControlPrimarySpinner:Z

    if-eqz v2, :cond_13

    .line 717
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ProfileActivity;->createSpinnerAdapter(Landroid/content/Context;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 718
    .local v0, adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCurrentSpinnerPosition:I

    .line 719
    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {p1, v0, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    .line 722
    .end local v0           #adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    :cond_13
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowRefreshInActionBar()Z

    move-result v1

    .line 725
    .local v1, canShowRefresh:Z
    if-nez v1, :cond_1c

    .line 726
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    .line 729
    :cond_1c
    if-nez v1, :cond_24

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowConversationActions()Z

    move-result v2

    if-nez v2, :cond_27

    .line 730
    :cond_24
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    .line 733
    :cond_27
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->isProgressIndicatorVisible()Z

    move-result v2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowConversationActions()Z

    move-result v5

    if-nez v5, :cond_52

    move v2, v3

    :goto_32
    if-eqz v2, :cond_51

    .line 734
    const v2, 0x7f02016e

    const v5, 0x7f0802a2

    invoke-virtual {p1, v3, v2, v5}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    .line 738
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Lcom/google/android/apps/plus/service/Hangout;->isHangoutCreationSupported(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Z

    move-result v2

    if-eqz v2, :cond_51

    .line 739
    const v2, 0x7f02016d

    const v3, 0x7f0802a3

    invoke-virtual {p1, v4, v2, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    .line 743
    :cond_51
    return-void

    .line 733
    :cond_52
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v5

    iget v5, v5, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v5, :cond_64

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mLandscape:Z

    if-nez v5, :cond_64

    if-eqz v2, :cond_64

    move v2, v3

    goto :goto_32

    :cond_64
    move v2, v4

    goto :goto_32
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 13
    .parameter "menu"

    .prologue
    const v10, 0x7f0902b8

    const v9, 0x7f0902b7

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 889
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_ed

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_ed

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    if-eqz v8, :cond_ed

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_ed

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_ed

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v8, :cond_ed

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_ed

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMute:Z

    if-nez v8, :cond_ed

    move v1, v6

    .line 899
    .local v1, isMuteVisible:Z
    :goto_31
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_f0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_f0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_f0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v8, :cond_f0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_f0

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMute:Z

    if-eqz v8, :cond_f0

    move v5, v6

    .line 907
    .local v5, isUnmuteVisible:Z
    :goto_4a
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_f3

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_f3

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    if-eqz v8, :cond_f3

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileAdapter:Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/phone/ProfileStreamAdapter;->getFullName()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_f3

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v8, :cond_f3

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_f3

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z

    if-nez v8, :cond_f3

    move v0, v6

    .line 916
    .local v0, isBlockVisible:Z
    :goto_6f
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_f6

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_f6

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    if-nez v8, :cond_f6

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_f6

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsBlocked:Z

    if-eqz v8, :cond_f6

    move v4, v6

    .line 923
    .local v4, isUnblockVisible:Z
    :goto_84
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    if-eqz v8, :cond_f8

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    if-nez v8, :cond_f8

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-nez v8, :cond_f8

    move v3, v6

    .line 929
    .local v3, isReportAbuseVisible:Z
    :goto_91
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowRefreshInActionBar()Z

    move-result v8

    if-nez v8, :cond_fa

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowConversationActions()Z

    move-result v8

    if-eqz v8, :cond_fa

    move v2, v6

    .line 931
    .local v2, isRefreshVisible:Z
    :goto_9e
    const v6, 0x7f09028d

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 932
    const v6, 0x7f0902b5

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 933
    const v6, 0x7f0902b6

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 934
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 935
    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 936
    const v6, 0x7f0902b9

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 939
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsPlusPage:Z

    if-eqz v6, :cond_fc

    .line 940
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    const v7, 0x7f080255

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 941
    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    const v7, 0x7f080256

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 946
    :goto_ec
    return-void

    .end local v0           #isBlockVisible:Z
    .end local v1           #isMuteVisible:Z
    .end local v2           #isRefreshVisible:Z
    .end local v3           #isReportAbuseVisible:Z
    .end local v4           #isUnblockVisible:Z
    .end local v5           #isUnmuteVisible:Z
    :cond_ed
    move v1, v7

    .line 889
    goto/16 :goto_31

    .restart local v1       #isMuteVisible:Z
    :cond_f0
    move v5, v7

    .line 899
    goto/16 :goto_4a

    .restart local v5       #isUnmuteVisible:Z
    :cond_f3
    move v0, v7

    .line 907
    goto/16 :goto_6f

    .restart local v0       #isBlockVisible:Z
    :cond_f6
    move v4, v7

    .line 916
    goto :goto_84

    .restart local v4       #isUnblockVisible:Z
    :cond_f8
    move v3, v7

    .line 923
    goto :goto_91

    .restart local v3       #isReportAbuseVisible:Z
    :cond_fa
    move v2, v7

    .line 929
    goto :goto_9e

    .line 943
    .restart local v2       #isRefreshVisible:Z
    :cond_fc
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    const v7, 0x7f080253

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 944
    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    const v7, 0x7f080254

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_ec
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .registers 7
    .parameter "position"

    .prologue
    .line 950
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mControlPrimarySpinner:Z

    if-eqz v1, :cond_d

    .line 951
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCurrentSpinnerPosition:I

    if-eq v1, p1, :cond_d

    .line 952
    packed-switch p1, :pswitch_data_48

    .line 961
    :goto_b
    :pswitch_b
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mCurrentSpinnerPosition:I

    .line 967
    :cond_d
    return-void

    .line 958
    :pswitch_e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "destination"

    const/4 v4, 0x7

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "person_id"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "notif_id"

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v1

    const-string v2, "photos_home"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 960
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_b

    .line 952
    nop

    :pswitch_data_48
    .packed-switch 0x0
        :pswitch_b
        :pswitch_e
    .end packed-switch
.end method

.method public final onResume()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1030
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onResume()V

    .line 1031
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 1033
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2d

    .line 1034
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 1035
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    .line 1036
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleProfileServiceCallback$b5e9bbb(I)V

    .line 1037
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    .line 1041
    :cond_2d
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_52

    .line 1042
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_52

    .line 1043
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 1044
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleReportAbuseCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 1045
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    .line 1049
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_52
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_79

    .line 1050
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_79

    .line 1051
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 1052
    .restart local v0       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestIsMuted:Z

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handleSetMutedCallback(IZLcom/google/android/apps/plus/service/ServiceResult;)V

    .line 1053
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    .line 1057
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_79
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_9e

    .line 1058
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_9e

    .line 1059
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 1060
    .restart local v0       #result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlePlusOneCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 1061
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    .line 1065
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_9e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_c2

    .line 1066
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_c2

    .line 1067
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    .line 1068
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->handlerInsertCameraPhoto$b5e9bbb(I)V

    .line 1069
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    .line 1073
    :cond_c2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    .line 1074
    return-void
.end method

.method public final onReviewAuthorAvatarClicked(Ljava/lang/String;)V
    .registers 6
    .parameter "gaiaId"

    .prologue
    .line 496
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 497
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 499
    .local v0, intent:Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->safeStartActivity(Landroid/content/Intent;)V

    .line 501
    .end local v0           #intent:Landroid/content/Intent;
    :cond_14
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 660
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 661
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 662
    const-string v0, "profile_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 664
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_21

    .line 665
    const-string v0, "plusone_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPlusOneRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 667
    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_30

    .line 668
    const-string v0, "abuse_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 670
    :cond_30
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_46

    .line 671
    const-string v0, "mute_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 672
    const-string v0, "mute_state"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestIsMuted:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 674
    :cond_46
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_55

    .line 675
    const-string v0, "camera_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 678
    :cond_55
    const-string v0, "block_in_progress"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mBlockInProgress:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 679
    const-string v0, "profile_is_expanded"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfileIsExpanded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 680
    return-void
.end method

.method public final onSendTextClicked(Ljava/lang/String;)V
    .registers 6
    .parameter "phoneNumber"

    .prologue
    .line 439
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 440
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sms:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->startExternalActivity(Landroid/content/Intent;)V

    .line 443
    :cond_27
    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .registers 5
    .parameter "args"

    .prologue
    const/4 v1, 0x1

    .line 585
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 587
    const-string v0, "person_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mIsMyProfile:Z

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_37

    move v0, v1

    :goto_23
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mHasGaiaId:Z

    .line 592
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    .line 593
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    if-nez v0, :cond_36

    .line 594
    const-string v0, "show_empty_stream"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 596
    :cond_36
    return-void

    .line 589
    :cond_37
    const/4 v0, 0x0

    goto :goto_23
.end method

.method public final onZagatExplanationClicked()V
    .registers 4

    .prologue
    .line 491
    new-instance v0, Lcom/google/android/apps/plus/fragments/ProfileZagatExplanationDialog;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/ProfileZagatExplanationDialog;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "zagat_explanation"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProfileZagatExplanationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 492
    return-void
.end method

.method public final refresh()V
    .registers 1

    .prologue
    .line 1015
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->refresh()V

    .line 1016
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->refreshProfile()V

    .line 1017
    return-void
.end method

.method public final refreshProfile()V
    .registers 5

    .prologue
    .line 1023
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    .line 1025
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->updateSpinner()V

    .line 1026
    return-void
.end method

.method public final relinquishPrimarySpinner()V
    .registers 2

    .prologue
    .line 1268
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mControlPrimarySpinner:Z

    .line 1269
    return-void
.end method

.method public final reportAbuse(Ljava/lang/String;)V
    .registers 7
    .parameter "abuseType"

    .prologue
    .line 570
    const-string v1, "IMPERSONATION"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 571
    const v1, 0x7f080272

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08027c

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 575
    .local v0, warning:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog_warning"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 581
    .end local v0           #warning:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :goto_2b
    return-void

    .line 577
    :cond_2c
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    invoke-static {v1, v2, v3, p1}, Lcom/google/android/apps/plus/service/EsService;->reportProfileAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mReportAbuseRequestId:Ljava/lang/Integer;

    .line 579
    const v1, 0x7f08027b

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    goto :goto_2b
.end method

.method protected final setCircleMembership(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .registers 13
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1368
    .local p1, originalCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, selectedCircleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1369
    .local v7, circlesToAdd:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, i$:Ljava/util/Iterator;
    :cond_9
    :goto_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1370
    .local v6, circleId:Ljava/lang/String;
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1371
    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 1375
    .end local v6           #circleId:Ljava/lang/String;
    :cond_1f
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1376
    .local v8, circlesToRemove:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_28
    :goto_28
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1377
    .restart local v6       #circleId:Ljava/lang/String;
    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_28

    .line 1378
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_28

    .line 1382
    .end local v6           #circleId:Ljava/lang/String;
    :cond_3e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mPersonId:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v7, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v8, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    .line 1386
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_78

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_78

    .line 1387
    const v0, 0x7f080408

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    .line 1393
    :goto_77
    return-void

    .line 1388
    :cond_78
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8b

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8b

    .line 1389
    const v0, 0x7f080409

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    goto :goto_77

    .line 1391
    :cond_8b
    const v0, 0x7f08040a

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    goto :goto_77
.end method

.method public final setPersonMuted(Z)V
    .registers 5
    .parameter "isMuted"

    .prologue
    .line 549
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mGaiaId:Ljava/lang/String;

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/apps/plus/service/EsService;->setPersonMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestId:Ljava/lang/Integer;

    .line 550
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mMuteRequestIsMuted:Z

    .line 551
    const v0, 0x7f08026b

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    .line 552
    return-void
.end method

.method protected final setProfilePhoto([B)V
    .registers 4
    .parameter "imageBytes"

    .prologue
    .line 1399
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->setProfilePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->mProfilePendingRequestId:Ljava/lang/Integer;

    .line 1401
    const v0, 0x7f0802cf

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->showProgressDialog(I)V

    .line 1402
    return-void
.end method

.method public final unblockPerson(Ljava/lang/String;)V
    .registers 3
    .parameter "personId"

    .prologue
    .line 542
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->setPersonBlocked(Z)V

    .line 543
    return-void
.end method

.method protected final updateSpinner()V
    .registers 2

    .prologue
    .line 758
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->canShowRefreshInActionBar()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 759
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    .line 763
    :goto_9
    return-void

    .line 761
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->invalidateActionBar()V

    goto :goto_9
.end method
