.class public Lcom/google/android/apps/plus/fragments/HostedStreamFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedStreamFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;
.implements Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;
.implements Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;
.implements Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;
.implements Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;,
        Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ContinuationTokenQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog$PhotoHandler;",
        "Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;",
        "Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;",
        "Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;",
        "Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;"
    }
.end annotation


# static fields
.field private static final CIRCLES_PROJECTION:[Ljava/lang/String;

.field private static sNextPagePreloadTriggerRows:I

.field private static sUsePhotoOneUp:Z


# instance fields
.field protected mAdapter:Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;

.field protected mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

.field protected mCircleId:Ljava/lang/String;

.field protected mComposeBarCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

.field protected mContinuationToken:Ljava/lang/String;

.field private mCurrentSpinnerPosition:I

.field protected mEndOfStream:Z

.field protected mError:Z

.field protected mFirstLoad:Z

.field private mFragmentStartTime:J

.field protected mGaiaId:Ljava/lang/String;

.field protected mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field protected mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

.field private mLastDeactivationTime:J

.field protected mLoaderHash:Ljava/lang/Integer;

.field protected mLocation:Landroid/location/Location;

.field protected mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

.field protected mLocationDisabledView:Landroid/view/View;

.field protected mLocationSettingsButton:Landroid/widget/Button;

.field protected mNearby:Z

.field protected mPostsUri:Landroid/net/Uri;

.field protected mPreloadRequested:Z

.field private mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRefreshDisabled:Z

.field protected mResetAnimationState:Z

.field protected mResetPosition:Z

.field private mScrollOffset:I

.field private mScrollPos:I

.field private mServerErrorRetryButton:Landroid/view/View;

.field private mServerErrorView:Landroid/view/View;

.field protected final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mStreamLength:I

.field protected mStreamOwnerUserId:Ljava/lang/String;

.field protected mView:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x0

    .line 96
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "circle_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "circle_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->CIRCLES_PROJECTION:[Ljava/lang/String;

    .line 130
    sput-boolean v3, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sUsePhotoOneUp:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    .line 233
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    .line 246
    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 361
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 83
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 83
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateServerErrorView()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isPaused()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onShakeAnimFinished()V

    return-void
.end method

.method private addLocationListener(Landroid/location/Location;)V
    .registers 15
    .parameter "lastLocation"

    .prologue
    const v12, 0x7f09021f

    const/4 v11, 0x0

    const/16 v10, 0x8

    .line 1053
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-nez v0, :cond_20

    .line 1055
    new-instance v0, Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x1

    const-wide/16 v4, 0xbb8

    new-instance v7, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;

    invoke-direct {v7, p0, v11}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$StreamLocationListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;B)V

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/LocationController;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZJLandroid/location/Location;Landroid/location/LocationListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    .line 1059
    :cond_20
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v9

    .line 1060
    .local v9, view:Landroid/view/View;
    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 1061
    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    .line 1063
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled()Z

    move-result v8

    .line 1065
    .local v8, isProviderEnabled:Z
    if-nez v8, :cond_6c

    .line 1066
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->removeProgressViewMessages()V

    const v0, 0x1020004

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090073

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090066

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v9, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    invoke-virtual {v0, v11}, Landroid/view/View;->setVisibility(I)V

    .line 1067
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->removeLocationListener()V

    .line 1074
    :cond_6b
    :goto_6b
    return-void

    .line 1069
    :cond_6c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->init()V

    .line 1070
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-nez v0, :cond_6b

    .line 1071
    const v0, 0x7f08016c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v9, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_6b
.end method

.method private static getViewForLogging(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2
    .parameter "circleId"

    .prologue
    .line 1476
    const-string v0, "v.all.circles"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1477
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_EVERYONE:Lcom/google/android/apps/plus/analytics/OzViews;

    .line 1487
    :goto_a
    return-object v0

    .line 1478
    :cond_b
    const-string v0, "v.whatshot"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1479
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_WHATS_HOT:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_a

    .line 1480
    :cond_16
    const-string v0, "v.nearby"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 1481
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_NEARBY:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_a

    .line 1482
    :cond_21
    const-string v0, "f."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 1483
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_CIRCLES:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_a

    .line 1484
    :cond_2c
    const-string v0, "g."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 1485
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOOP_USER:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_a

    .line 1487
    :cond_37
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_a
.end method

.method private onShakeAnimFinished()V
    .registers 5

    .prologue
    .line 1323
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isPaused()Z

    move-result v2

    if-nez v2, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_d

    .line 1341
    :cond_c
    :goto_c
    return-void

    .line 1327
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 1328
    .local v1, view:Landroid/view/View;
    if-eqz v1, :cond_21

    .line 1329
    const v2, 0x7f090228

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1330
    .local v0, plusOneGlassView:Landroid/view/View;
    if-eqz v0, :cond_21

    .line 1331
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1335
    .end local v0           #plusOneGlassView:Landroid/view/View;
    :cond_21
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    if-eqz v2, :cond_c

    .line 1339
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;->activityId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;->plusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->togglePlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    .line 1340
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    goto :goto_c
.end method

.method private prefetchContent()V
    .registers 5

    .prologue
    const/4 v3, 0x4

    .line 971
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPreloadRequested:Z

    .line 972
    const-string v1, "HostedStreamFrag"

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 973
    const-string v1, "HostedStreamFrag"

    const-string v2, "prefetchContent - mPreloadRequested=true"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    :cond_13
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->post(Ljava/lang/Runnable;)Z

    move-result v0

    .line 987
    .local v0, result:Z
    if-nez v0, :cond_2f

    const-string v1, "HostedStreamFrag"

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 988
    const-string v1, "HostedStreamFrag"

    const-string v2, "prefetchContent - posting the runnable returned false!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 990
    :cond_2f
    return-void
.end method

.method private removeLocationListener()V
    .registers 2

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-eqz v0, :cond_c

    .line 1082
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->release()V

    .line 1083
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    .line 1085
    :cond_c
    return-void
.end method

.method private startStreamOneUp(Lcom/google/android/apps/plus/views/StreamCardView;Z)V
    .registers 18
    .parameter "streamCardView"
    .parameter "showKeyboard"

    .prologue
    .line 1245
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamCardView;->getAlbumId()Ljava/lang/String;

    move-result-object v2

    .line 1246
    .local v2, albumId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamCardView;->getMediaRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v12

    .line 1247
    .local v12, mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamCardView;->getMediaLinkUrl()Ljava/lang/String;

    move-result-object v11

    .line 1248
    .local v11, mediaLinkUrl:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamCardView;->getDesiredWidth()I

    move-result v5

    .line 1249
    .local v5, desiredWidth:I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamCardView;->getDesiredHeight()I

    move-result v4

    .line 1250
    .local v4, desiredHeight:I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamCardView;->getMediaCount()I

    move-result v10

    .line 1251
    .local v10, mediaCount:I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamCardView;->getLinkTitle()Ljava/lang/String;

    move-result-object v8

    .line 1252
    .local v8, linkTitle:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamCardView;->getLinkUrl()Ljava/lang/String;

    move-result-object v9

    .line 1254
    .local v9, linkUrl:Ljava/lang/String;
    if-eqz v12, :cond_31

    .line 1255
    invoke-virtual {v12}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v13

    .line 1256
    .local v13, targetGaiaId:Ljava/lang/String;
    const-string v14, "extra_gaia_id"

    invoke-static {v14, v13}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    .line 1258
    .local v6, extras:Landroid/os/Bundle;
    sget-object v14, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_SELECT_ACTIVITY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v14, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    .line 1261
    .end local v6           #extras:Landroid/os/Bundle;
    .end local v13           #targetGaiaId:Ljava/lang/String;
    :cond_31
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/views/StreamCardView;->getActivityId()Ljava/lang/String;

    move-result-object v1

    .line 1262
    .local v1, activityId:Ljava/lang/String;
    if-nez v1, :cond_38

    .line 1290
    :goto_37
    return-void

    .line 1266
    :cond_38
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 1267
    .local v3, context:Landroid/content/Context;
    iget-object v14, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v14, v1}, Lcom/google/android/apps/plus/phone/Intents;->getStreamOneUpActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 1268
    .local v7, intent:Landroid/content/Intent;
    if-lez v5, :cond_54

    if-lez v4, :cond_54

    .line 1269
    const-string v14, "photo_width"

    invoke-virtual {v7, v14, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1270
    const-string v14, "photo_height"

    invoke-virtual {v7, v14, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1272
    :cond_54
    if-eqz v12, :cond_60

    .line 1273
    const-string v14, "photo_ref"

    invoke-virtual {v7, v14, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1274
    const-string v14, "photo_count"

    invoke-virtual {v7, v14, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1276
    :cond_60
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6b

    .line 1277
    const-string v14, "photo_link_url"

    invoke-virtual {v7, v14, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1279
    :cond_6b
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_76

    .line 1280
    const-string v14, "album_id"

    invoke-virtual {v7, v14, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1282
    :cond_76
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_81

    .line 1283
    const-string v14, "link_title"

    invoke-virtual {v7, v14, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1285
    :cond_81
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_8c

    .line 1286
    const-string v14, "link_url"

    invoke-virtual {v7, v14, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1288
    :cond_8c
    const-string v14, "show_keyboard"

    move/from16 v0, p2

    invoke-virtual {v7, v14, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1289
    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_37
.end method

.method private togglePlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
    .registers 5
    .parameter "activityId"
    .parameter "plusOneData"

    .prologue
    .line 1344
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->isPostPlusOnePending(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 1345
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1346
    .local v0, activity:Landroid/app/Activity;
    if-eqz p2, :cond_18

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v1

    if-eqz v1, :cond_18

    .line 1347
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->deletePostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    .line 1352
    .end local v0           #activity:Landroid/app/Activity;
    :cond_17
    :goto_17
    return-void

    .line 1349
    .restart local v0       #activity:Landroid/app/Activity;
    :cond_18
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/service/EsService;->createPostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    goto :goto_17
.end method

.method private updateLocationHeader(Landroid/view/View;)V
    .registers 5
    .parameter "parent"

    .prologue
    .line 1093
    const v2, 0x7f090220

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1094
    .local v1, textView:Landroid/widget/TextView;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-nez v2, :cond_14

    .line 1095
    const v2, 0x7f08016c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1104
    :goto_13
    return-void

    .line 1097
    :cond_14
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/LocationController;->getFormattedAddress(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    .line 1098
    .local v0, address:Ljava/lang/String;
    if-eqz v0, :cond_20

    .line 1099
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_13

    .line 1101
    :cond_20
    const v2, 0x7f08018e

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_13
.end method

.method private updateServerErrorView()V
    .registers 3

    .prologue
    .line 1578
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorView:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    :goto_7
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1579
    return-void

    .line 1578
    :cond_b
    const/16 v0, 0x8

    goto :goto_7
.end method


# virtual methods
.method protected final checkResetAnimationState()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 1492
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    if-eqz v0, :cond_1c

    .line 1493
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetPosition:Z

    if-eqz v0, :cond_12

    .line 1494
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    .line 1495
    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    .line 1496
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelectionToTop()V

    .line 1498
    :cond_12
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetPosition:Z

    .line 1499
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->resetAnimationState()V

    .line 1500
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    .line 1502
    :cond_1c
    return-void
.end method

.method protected createStreamAdapter(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;Landroid/view/View;)Lcom/google/android/apps/plus/phone/StreamAdapter;
    .registers 20
    .parameter "context"
    .parameter "gridView"
    .parameter "account"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "viewUseListener"
    .parameter "plusBarClickListener"
    .parameter "mediaClickListener"
    .parameter "floatingComposeBarView"

    .prologue
    .line 458
    new-instance v0, Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/phone/StreamAdapter;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;Landroid/view/View;)V

    return-object v0
.end method

.method public doPickPhotoFromAlbums()V
    .registers 6

    .prologue
    .line 1572
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->COMPOSE_CHOOSE_PHOTO:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 1573
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getCameraPhotosPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1574
    .local v0, intent:Landroid/content/Intent;
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1575
    return-void
.end method

.method protected doShowEmptyViewProgress(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 1113
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->doShowEmptyViewProgress(Landroid/view/View;)V

    .line 1114
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1115
    return-void
.end method

.method public doTakePhoto()V
    .registers 4

    .prologue
    .line 1563
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 1564
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getWidgetCameraLauncherActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    .line 1566
    .local v0, cameraLauncherActivityIntent:Landroid/content/Intent;
    const-string v1, "com.google.android.apps.plus.widget.CAMERA_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1567
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    .line 1568
    return-void
.end method

.method protected fetchContent(Z)V
    .registers 10
    .parameter "newer"

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 998
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "show_empty_stream"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1045
    :cond_e
    :goto_e
    return-void

    .line 1002
    :cond_f
    if-nez p1, :cond_15

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    if-nez v0, :cond_e

    .line 1007
    :cond_15
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v0, :cond_55

    .line 1008
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-eqz v0, :cond_e

    .line 1012
    if-eqz p1, :cond_50

    .line 1013
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    .line 1019
    :cond_21
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f080096

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    .line 1022
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    new-instance v3, Lcom/google/android/apps/plus/content/DbLocation;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-direct {v3, v6, v4}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILandroid/location/Location;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getNearbyActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 1038
    .local v7, reqId:Ljava/lang/Integer;
    :goto_48
    if-eqz p1, :cond_7c

    .line 1039
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 1044
    :goto_4c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    goto :goto_e

    .line 1014
    .end local v7           #reqId:Ljava/lang/Integer;
    :cond_50
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    if-nez v0, :cond_21

    goto :goto_e

    .line 1025
    :cond_55
    if-eqz p1, :cond_77

    .line 1026
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    .line 1031
    :cond_59
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 1034
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->getActivityStream(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .restart local v7       #reqId:Ljava/lang/Integer;
    goto :goto_48

    .line 1027
    .end local v7           #reqId:Ljava/lang/Integer;
    :cond_77
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    if-nez v0, :cond_59

    goto :goto_e

    .line 1041
    .restart local v7       #reqId:Ljava/lang/Integer;
    :cond_7c
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mOlderReqId:Ljava/lang/Integer;

    goto :goto_4c
.end method

.method protected getComposeBarCursor()Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .registers 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mComposeBarCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    if-nez v0, :cond_20

    .line 441
    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v4, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v3

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mComposeBarCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    .line 442
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mComposeBarCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v4, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 444
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mComposeBarCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    return-object v0
.end method

.method public getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 1460
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected initCirclesLoader()V
    .registers 4

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 400
    return-void
.end method

.method protected isAdapterEmpty()Z
    .registers 2

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 1455
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isAdapterEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method protected isLocalDataAvailable(Landroid/database/Cursor;)Z
    .registers 3
    .parameter "data"

    .prologue
    .line 768
    if-eqz p1, :cond_a

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_a

    const/4 v0, 0x1

    :goto_9
    return v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_9
.end method

.method protected needsAsyncData()Z
    .registers 2

    .prologue
    .line 1465
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 8
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 1546
    packed-switch p1, :pswitch_data_22

    .line 1559
    :cond_3
    :goto_3
    return-void

    .line 1548
    :pswitch_4
    const/4 v2, -0x1

    if-ne p2, v2, :cond_3

    if-eqz p3, :cond_3

    .line 1549
    const-string v2, "mediarefs"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1551
    .local v0, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    .line 1553
    .local v1, postActivityIntent:Landroid/content/Intent;
    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1554
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    .line 1546
    nop

    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch
.end method

.method protected final onAsyncData()V
    .registers 6

    .prologue
    const-wide/16 v3, 0x0

    .line 1152
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onAsyncData()V

    .line 1154
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1156
    .local v0, activity:Landroid/app/Activity;
    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentStartTime:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_15

    instance-of v1, v0, Lcom/google/android/apps/plus/phone/ProfileActivity;

    if-eqz v1, :cond_15

    .line 1157
    iput-wide v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentStartTime:J

    .line 1159
    :cond_15
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 12
    .parameter "v"

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 891
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationSettingsButton:Landroid/widget/Button;

    if-ne p1, v6, :cond_e

    .line 892
    invoke-static {}, Lcom/google/android/apps/plus/phone/Intents;->getLocationSettingActivityIntent()Landroid/content/Intent;

    move-result-object v4

    .line 893
    .local v4, intent:Landroid/content/Intent;
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    .line 935
    .end local v4           #intent:Landroid/content/Intent;
    .end local p1
    :cond_d
    :goto_d
    return-void

    .line 894
    .restart local p1
    :cond_e
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorRetryButton:Landroid/view/View;

    if-ne p1, v6, :cond_2b

    .line 895
    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    .line 896
    const-string v6, "HostedStreamFrag"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_24

    .line 897
    const-string v6, "HostedStreamFrag"

    const-string v7, "onClick - mError=false"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 899
    :cond_24
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prefetchContent()V

    .line 900
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateServerErrorView()V

    goto :goto_d

    .line 901
    :cond_2b
    instance-of v6, p1, Lcom/google/android/apps/plus/views/EventStreamCardView;

    if-eqz v6, :cond_4a

    move-object v0, p1

    .line 902
    check-cast v0, Lcom/google/android/apps/plus/views/EventStreamCardView;

    .line 903
    .local v0, eventCardView:Lcom/google/android/apps/plus/views/EventStreamCardView;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventStreamCardView;->getEvent()Lcom/google/api/services/plusi/model/PlusEvent;

    move-result-object v1

    .line 905
    .local v1, eventData:Lcom/google/api/services/plusi/model/PlusEvent;
    if-eqz v1, :cond_d

    .line 906
    iget-object v2, v1, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    .line 907
    .local v2, eventId:Ljava/lang/String;
    iget-object v5, v1, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    .line 908
    .local v5, ownerId:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v7, v2, v5, v9}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_d

    .line 911
    .end local v0           #eventCardView:Lcom/google/android/apps/plus/views/EventStreamCardView;
    .end local v1           #eventData:Lcom/google/api/services/plusi/model/PlusEvent;
    .end local v2           #eventId:Ljava/lang/String;
    .end local v5           #ownerId:Ljava/lang/String;
    :cond_4a
    instance-of v6, p1, Lcom/google/android/apps/plus/views/StreamCardView;

    if-eqz v6, :cond_54

    .line 913
    check-cast p1, Lcom/google/android/apps/plus/views/StreamCardView;

    .end local p1
    invoke-direct {p0, p1, v8}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startStreamOneUp(Lcom/google/android/apps/plus/views/StreamCardView;Z)V

    goto :goto_d

    .line 914
    .restart local p1
    :cond_54
    instance-of v6, p1, Landroid/widget/ImageButton;

    if-eqz v6, :cond_d

    .line 915
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 916
    .local v3, id:I
    packed-switch v3, :pswitch_data_b6

    goto :goto_d

    .line 918
    :pswitch_60
    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_POST:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 919
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v7, v9}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_d

    .line 924
    :pswitch_73
    new-instance v6, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;

    const v7, 0x7f0802cb

    invoke-direct {v6, v7}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;-><init>(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/phone/Intents;->isCameraIntentRegistered(Landroid/content/Context;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setIsCameraSupported(Z)V

    invoke-virtual {v6, p0, v8}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    const-string v8, "take_photo"

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/plus/fragments/ChoosePhotoDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 929
    :pswitch_94
    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->LOOP_CHECKIN:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 930
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v8, Landroid/content/Intent;

    const-class v9, Lcom/google/android/apps/plus/phone/CheckinActivity;

    invoke-direct {v8, v6, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "account"

    invoke-virtual {v8, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v6, "places_only"

    const/4 v7, 0x1

    invoke-virtual {v8, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_d

    .line 916
    :pswitch_data_b6
    .packed-switch 0x7f09006a
        :pswitch_60
        :pswitch_73
        :pswitch_94
    .end packed-switch
.end method

.method public final onCommentsClicked$1b4287ec(Lcom/google/android/apps/plus/views/StreamCardView;)V
    .registers 3
    .parameter "streamCardView"

    .prologue
    .line 1214
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startStreamOneUp(Lcom/google/android/apps/plus/views/StreamCardView;Z)V

    .line 1215
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    const/4 v2, 0x0

    .line 404
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 406
    if-eqz p1, :cond_62

    .line 407
    const-string v0, "scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    .line 408
    const-string v0, "scroll_off"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    .line 410
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    .line 412
    const-string v0, "loader_hash"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 413
    const-string v0, "loader_hash"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLoaderHash:Ljava/lang/Integer;

    .line 415
    :cond_34
    const-string v0, "stream_length"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    .line 416
    const-string v0, "last_deactivation"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastDeactivationTime:J

    .line 417
    const-string v0, "error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    .line 418
    const-string v0, "reset_animation"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    .line 419
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetPosition:Z

    .line 422
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRefreshDisabled:Z

    .line 430
    :goto_5b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prepareLoaderUri()V

    .line 431
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->initCirclesLoader()V

    .line 432
    return-void

    .line 424
    :cond_62
    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    .line 425
    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    .line 427
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFragmentStartTime:J

    goto :goto_5b
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 12
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v7, 0x0

    .line 731
    packed-switch p1, :pswitch_data_68

    move-object v0, v5

    .line 762
    :goto_8
    return-object v0

    .line 733
    :pswitch_9
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x3

    sget-object v5, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->CIRCLES_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_8

    .line 739
    :pswitch_18
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "show_empty_stream"

    invoke-virtual {v1, v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 740
    iput v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    .line 743
    :cond_26
    if-ne p1, v4, :cond_60

    move v8, v0

    .line 745
    .local v8, limit:I
    :goto_29
    const-string v6, "sort_index ASC"

    .line 746
    .local v6, orderAndLimit:Ljava/lang/String;
    const/4 v1, -0x1

    if-eq v8, v1, :cond_45

    .line 747
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LIMIT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 750
    :cond_45
    if-eq p1, v4, :cond_4c

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    if-nez v1, :cond_4c

    move v7, v0

    .line 752
    .local v7, hideMuted:Z
    :cond_4c
    if-ne p1, v4, :cond_63

    sget-object v3, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$ContinuationTokenQuery;->PROJECTION:[Ljava/lang/String;

    .line 755
    .local v3, projection:[Ljava/lang/String;
    :goto_50
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPostsUri:Landroid/net/Uri;

    if-eqz v7, :cond_66

    const-string v4, "has_muted=0"

    :goto_5c
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 743
    .end local v3           #projection:[Ljava/lang/String;
    .end local v6           #orderAndLimit:Ljava/lang/String;
    .end local v7           #hideMuted:Z
    .end local v8           #limit:I
    :cond_60
    iget v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    goto :goto_29

    .line 752
    .restart local v6       #orderAndLimit:Ljava/lang/String;
    .restart local v7       #hideMuted:Z
    .restart local v8       #limit:I
    :cond_63
    sget-object v3, Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;->PROJECTION_STREAM:[Ljava/lang/String;

    goto :goto_50

    .restart local v3       #projection:[Ljava/lang/String;
    :cond_66
    move-object v4, v5

    .line 755
    goto :goto_5c

    .line 731
    :pswitch_data_68
    .packed-switch 0x1
        :pswitch_9
        :pswitch_18
        :pswitch_18
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 16
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 466
    const v0, 0x7f0300c0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    .line 468
    .local v11, view:Landroid/view/View;
    sget v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sNextPagePreloadTriggerRows:I

    if-nez v0, :cond_1c

    .line 469
    invoke-virtual {v11}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v10

    .line 470
    .local v10, screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    iget v0, v10, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v0, :cond_cf

    .line 471
    const/16 v0, 0x8

    sput v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sNextPagePreloadTriggerRows:I

    .line 477
    .end local v10           #screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    :cond_1c
    :goto_1c
    const v0, 0x7f090109

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 479
    const v0, 0x7f090221

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 480
    .local v9, floatingComposeBarView:Landroid/view/View;
    const v0, 0x7f09006a

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 481
    const v0, 0x7f09006b

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 482
    const v0, 0x7f09006c

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 485
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PostClickListener;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    move-object v0, p0

    move-object v4, p0

    move-object v6, p0

    move-object v7, p0

    move-object v8, p0

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->createStreamAdapter(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/phone/StreamAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;Landroid/view/View;)Lcom/google/android/apps/plus/phone/StreamAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getComposeBarCursor()Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/StreamAdapter;->changeComposeBarCursor(Landroid/database/Cursor;)V

    .line 488
    new-instance v0, Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;-><init>(Lcom/google/android/apps/plus/phone/TranslationAdapter$TranslationListAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 490
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const v1, 0x7f02017c

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelector(I)V

    .line 492
    const v0, 0x7f0800dd

    invoke-static {v11, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 494
    const v0, 0x7f090222

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    .line 495
    const v0, 0x7f090224

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationSettingsButton:Landroid/widget/Button;

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationSettingsButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 498
    const v0, 0x7f090225

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorView:Landroid/view/View;

    .line 499
    const v0, 0x7f090226

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorRetryButton:Landroid/view/View;

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServerErrorRetryButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 502
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "show_empty_stream"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_cb

    .line 503
    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyView(Landroid/view/View;)V

    .line 506
    :cond_cb
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateServerErrorView()V

    .line 507
    return-object v11

    .line 473
    .end local v9           #floatingComposeBarView:Landroid/view/View;
    .restart local v10       #screenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;
    :cond_cf
    const/16 v0, 0x10

    sput v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sNextPagePreloadTriggerRows:I

    goto/16 :goto_1c
.end method

.method public final onDestroyView()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 546
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onDestroyView()V

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v0, :cond_f

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOnScrollListener(Lcom/google/android/apps/plus/views/ColumnGridView$OnScrollListener;)V

    .line 549
    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    .line 551
    :cond_f
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 16
    .parameter
    .parameter "data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 773
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v6

    packed-switch v6, :pswitch_data_21c

    .line 883
    :cond_7
    :goto_7
    return-void

    .line 775
    :pswitch_8
    if-eqz p2, :cond_7

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    if-eqz v6, :cond_84

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    move-object v7, v6

    :goto_11
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v6

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v8

    if-eq v6, v8, :cond_9a

    const/4 v9, 0x1

    :goto_1e
    if-nez v9, :cond_a2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_a2

    const/4 v6, 0x0

    :goto_27
    const/4 v8, 0x0

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    add-int/lit8 v8, v6, 0x1

    invoke-virtual {v11, v6}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getRealCircleId()Ljava/lang/String;

    move-result-object v6

    invoke-static {v10, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9c

    const/4 v6, 0x1

    :goto_41
    if-eqz v6, :cond_7

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_7

    const/4 v6, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v8}, Landroid/widget/ArrayAdapter;->clear()V

    :cond_4f
    const/4 v8, 0x0

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    new-instance v10, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    const/4 v12, 0x1

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12, v8}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_71

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    :cond_71
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_4f

    const/4 v7, -0x1

    iput v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v7, v8, v6}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    goto :goto_7

    :cond_84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    const-string v7, "streams"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v7, "circle"

    const-string v8, "v.all.circles"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v7, v6

    goto/16 :goto_11

    :cond_9a
    const/4 v9, 0x0

    goto :goto_1e

    :cond_9c
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_219

    :cond_a2
    move v6, v9

    goto :goto_41

    .line 780
    :pswitch_a4
    if-eqz p2, :cond_c4

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-nez v6, :cond_c4

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_c4

    .line 781
    const/4 v6, 0x0

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    .line 786
    :goto_b9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v6

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_7

    .line 784
    :cond_c4
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    goto :goto_b9

    .line 791
    :pswitch_c8
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;->getCount()I

    move-result v3

    .line 792
    .local v3, oldCount:I
    if-eqz v3, :cond_d3

    .line 793
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->saveScrollPosition()V

    .line 796
    :cond_d3
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    iget v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    packed-switch v6, :pswitch_data_226

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    if-nez v6, :cond_16c

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    if-eqz v6, :cond_16c

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    if-eqz v6, :cond_168

    const-string v8, "com.google.android.apps.plus.search_key-"

    invoke-virtual {v6, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_168

    const/4 v6, 0x1

    :goto_ef
    if-eqz v6, :cond_16a

    const/4 v6, 0x0

    :goto_f2
    invoke-virtual {v7, v6}, Lcom/google/android/apps/plus/phone/StreamAdapter;->setMarkPostsAsRead(Z)V

    .line 797
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v6, p2}, Lcom/google/android/apps/plus/phone/StreamAdapter;->changeStreamCursor(Landroid/database/Cursor;)V

    .line 798
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->checkResetAnimationState()V

    .line 800
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v2

    .line 803
    .local v2, newCount:I
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    .line 804
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPreloadRequested:Z

    .line 805
    const-string v6, "HostedStreamFrag"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_119

    .line 806
    const-string v6, "HostedStreamFrag"

    const-string v7, "onLoadFinished - mPreloadRequested=false"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    :cond_119
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-eqz v6, :cond_16e

    if-nez v2, :cond_16e

    .line 809
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v6

    const v7, 0x7f090072

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 810
    const/4 v0, 0x0

    .line 836
    .local v0, asyncDataReceived:Z
    :goto_12f
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    .line 838
    if-eqz p2, :cond_13a

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-nez v6, :cond_1d1

    .line 839
    :cond_13a
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLoaderHash:Ljava/lang/Integer;

    .line 868
    :cond_13d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->restoreScrollPosition()V

    .line 872
    :goto_140
    const-string v6, "HostedStreamFrag"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_15f

    .line 873
    const-string v6, "HostedStreamFrag"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "onLoadFinished - mEndOfStream="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    :cond_15f
    if-eqz v0, :cond_7

    .line 878
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onAsyncData()V

    goto/16 :goto_7

    .line 796
    .end local v0           #asyncDataReceived:Z
    .end local v2           #newCount:I
    :pswitch_166
    const/4 v6, 0x0

    goto :goto_f2

    :cond_168
    const/4 v6, 0x0

    goto :goto_ef

    :cond_16a
    const/4 v6, 0x1

    goto :goto_f2

    :cond_16c
    const/4 v6, 0x1

    goto :goto_f2

    .line 812
    .restart local v2       #newCount:I
    :cond_16e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v6

    const v7, 0x7f090072

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 814
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isLocalDataAvailable(Landroid/database/Cursor;)Z

    move-result v6

    if-eqz v6, :cond_1a1

    .line 815
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showContent(Landroid/view/View;)V

    .line 816
    const/4 v0, 0x1

    .line 817
    .restart local v0       #asyncDataReceived:Z
    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    .line 818
    const/16 v6, 0x13

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_19f

    const/4 v6, 0x1

    :goto_199
    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    .line 820
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    goto :goto_12f

    .line 818
    :cond_19f
    const/4 v6, 0x0

    goto :goto_199

    .line 821
    .end local v0           #asyncDataReceived:Z
    :cond_1a1
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    if-eqz v6, :cond_1bc

    .line 822
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v7, "no_location_stream_key"

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPostsUri:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1b9

    .line 824
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->fetchContent(Z)V

    .line 826
    :cond_1b9
    const/4 v0, 0x0

    .restart local v0       #asyncDataReceived:Z
    goto/16 :goto_12f

    .line 828
    .end local v0           #asyncDataReceived:Z
    :cond_1bc
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v6, :cond_1c4

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-eqz v6, :cond_1ce

    .line 829
    :cond_1c4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyView(Landroid/view/View;)V

    .line 830
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    .line 832
    :cond_1ce
    const/4 v0, 0x1

    .restart local v0       #asyncDataReceived:Z
    goto/16 :goto_12f

    .line 845
    :cond_1d1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 846
    .local v5, stringBuilder:Ljava/lang/StringBuilder;
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_1e4

    .line 849
    const/4 v6, 0x1

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 851
    :cond_1e4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v1

    .line 855
    .local v1, loaderHash:I
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLoaderHash:Ljava/lang/Integer;

    if-eqz v6, :cond_217

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLoaderHash:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-eq v6, v1, :cond_217

    const/4 v4, 0x1

    .line 856
    .local v4, snapToTopOfStream:Z
    :goto_1f9
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLoaderHash:Ljava/lang/Integer;

    .line 859
    if-nez v4, :cond_20e

    if-ge v2, v3, :cond_20e

    .line 860
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->getLastVisiblePosition()I

    move-result v6

    add-int/lit8 v7, v2, -0x1

    if-lt v6, v7, :cond_20e

    .line 861
    const/4 v4, 0x1

    .line 865
    :cond_20e
    if-eqz v4, :cond_13d

    .line 866
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelectionToTop()V

    goto/16 :goto_140

    .line 855
    .end local v4           #snapToTopOfStream:Z
    :cond_217
    const/4 v4, 0x0

    goto :goto_1f9

    .end local v0           #asyncDataReceived:Z
    .end local v1           #loaderHash:I
    .end local v2           #newCount:I
    .end local v3           #oldCount:I
    .end local v5           #stringBuilder:Ljava/lang/StringBuilder;
    :cond_219
    move v6, v8

    goto/16 :goto_27

    .line 773
    :pswitch_data_21c
    .packed-switch 0x1
        :pswitch_8
        :pswitch_a4
        :pswitch_c8
    .end packed-switch

    .line 796
    :pswitch_data_226
    .packed-switch 0x1
        :pswitch_166
        :pswitch_166
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 83
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 887
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onMediaClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;ZLcom/google/android/apps/plus/views/StreamCardView;)V
    .registers 9
    .parameter "albumId"
    .parameter "ownerId"
    .parameter "mediaRef"
    .parameter "isVideo"
    .parameter "view"

    .prologue
    .line 1220
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1221
    .local v0, context:Landroid/content/Context;
    if-eqz p4, :cond_14

    .line 1222
    invoke-virtual {p3}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1223
    .local v1, videoUrl:Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    .line 1242
    .end local v1           #videoUrl:Ljava/lang/String;
    :goto_13
    return-void

    .line 1225
    :cond_14
    const/4 v2, 0x0

    invoke-direct {p0, p5, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startStreamOneUp(Lcom/google/android/apps/plus/views/StreamCardView;Z)V

    goto :goto_13
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 535
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    .line 536
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->onPause()V

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->onPause()V

    .line 539
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 541
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->removeLocationListener()V

    .line 542
    return-void
.end method

.method public final onPlusOneAnimFinished()V
    .registers 8

    .prologue
    .line 1294
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->isPaused()Z

    move-result v5

    if-nez v5, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-nez v5, :cond_d

    .line 1320
    :cond_c
    :goto_c
    return-void

    .line 1298
    :cond_d
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    if-eqz v5, :cond_c

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v5, :cond_c

    .line 1302
    const/4 v1, 0x0

    .line 1303
    .local v1, found:Z
    const/4 v2, 0x0

    .local v2, i:I
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v3

    .local v3, size:I
    :goto_1d
    if-ge v2, v3, :cond_43

    .line 1304
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1305
    .local v0, child:Landroid/view/View;
    instance-of v5, v0, Lcom/google/android/apps/plus/views/StreamCardView;

    if-eqz v5, :cond_49

    move-object v4, v0

    .line 1306
    check-cast v4, Lcom/google/android/apps/plus/views/StreamCardView;

    .line 1307
    .local v4, streamCardView:Lcom/google/android/apps/plus/views/StreamCardView;
    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/StreamCardView;->getActivityId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    iget-object v6, v6, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;->activityId:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_49

    .line 1309
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    iget v6, v6, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;->overrideCount:I

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/plus/views/StreamCardView;->overridePlusOnedButtonDisplay(ZI)V

    .line 1311
    const/4 v1, 0x1

    .line 1317
    .end local v0           #child:Landroid/view/View;
    .end local v4           #streamCardView:Lcom/google/android/apps/plus/views/StreamCardView;
    :cond_43
    if-nez v1, :cond_c

    .line 1318
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->onShakeAnimFinished()V

    goto :goto_c

    .line 1303
    .restart local v0       #child:Landroid/view/View;
    :cond_49
    add-int/lit8 v2, v2, 0x1

    goto :goto_1d
.end method

.method public final onPlusOneClicked(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Lcom/google/android/apps/plus/views/StreamCardView;)V
    .registers 13
    .parameter "activityId"
    .parameter "plusOneData"
    .parameter "streamCardView"

    .prologue
    const/4 v7, 0x0

    .line 1164
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    if-nez v5, :cond_19

    .line 1165
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xc

    if-lt v5, v6, :cond_13

    if-eqz p2, :cond_1a

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1168
    :cond_13
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->togglePlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V

    .line 1169
    invoke-virtual {p3, v7, v7}, Lcom/google/android/apps/plus/views/StreamCardView;->overridePlusOnedButtonDisplay(ZI)V

    .line 1195
    :cond_19
    :goto_19
    return-void

    .line 1171
    :cond_1a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v4

    .line 1172
    .local v4, view:Landroid/view/View;
    if-nez p2, :cond_5c

    const/4 v0, 0x1

    .line 1174
    .local v0, overrideCount:I
    :goto_21
    new-instance v5, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    invoke-direct {v5, p1, p2, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;I)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAnimatingPlusOneInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$PlusOneInfo;

    .line 1175
    const v5, 0x7f090227

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;

    .line 1177
    .local v1, plusOneAnimatorView:Lcom/google/android/apps/plus/views/PlusOneAnimatorView;
    invoke-virtual {p3}, Lcom/google/android/apps/plus/views/StreamCardView;->getPlusOneButtonAnimationCopies()Landroid/util/Pair;

    move-result-object v2

    .line 1179
    .local v2, plusOneButtons:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/views/ClickableButton;Lcom/google/android/apps/plus/views/ClickableButton;>;"
    iget-object v5, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v6, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1, p0, v5, v6}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->startPlusOneAnim(Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;Lcom/google/android/apps/plus/views/ClickableButton;Lcom/google/android/apps/plus/views/ClickableButton;)V

    .line 1182
    invoke-virtual {p3}, Lcom/google/android/apps/plus/views/StreamCardView;->startDelayedShakeAnimation()V

    .line 1184
    const v5, 0x7f090228

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1185
    .local v3, plusOneGlassView:Landroid/view/View;
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1187
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$2;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/HostedStreamFragment;)V

    const-wide/16 v7, 0x393

    invoke-virtual {v5, v6, v7, v8}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_19

    .line 1172
    .end local v0           #overrideCount:I
    .end local v1           #plusOneAnimatorView:Lcom/google/android/apps/plus/views/PlusOneAnimatorView;
    .end local v2           #plusOneButtons:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/views/ClickableButton;Lcom/google/android/apps/plus/views/ClickableButton;>;"
    .end local v3           #plusOneGlassView:Landroid/view/View;
    :cond_5c
    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v5

    add-int/lit8 v0, v5, 0x1

    goto :goto_21
.end method

.method protected onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .registers 5
    .parameter "actionBar"

    .prologue
    .line 615
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0300bf

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    .line 623
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    .line 624
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    .line 625
    return-void
.end method

.method public onPrimarySpinnerSelectionChange(I)V
    .registers 15
    .parameter "position"

    .prologue
    const v12, 0x7f09021f

    const/4 v11, 0x2

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 672
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    if-eq v0, p1, :cond_c9

    .line 673
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    .line 676
    .local v6, newInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_3e

    .line 678
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    if-ltz v0, :cond_ca

    .line 679
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    .line 681
    .local v7, oldInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getRealCircleId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getViewForLogging(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    .line 685
    .end local v7           #oldInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;
    .local v1, startView:Lcom/google/android/apps/plus/analytics/OzViews;
    :goto_2d
    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getRealCircleId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getViewForLogging(Ljava/lang/String;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    .line 688
    .local v2, endView:Lcom/google/android/apps/plus/analytics/OzViews;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->clearNavigationAction()V

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    .line 689
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->recordNavigationAction(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/lang/Long;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 692
    .end local v1           #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    .end local v2           #endView:Lcom/google/android/apps/plus/analytics/OzViews;
    :cond_3e
    iput p1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    .line 693
    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getCircleId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    .line 694
    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getView()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    .line 695
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    if-ne v0, v11, :cond_cd

    move v0, v9

    :goto_51
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    .line 699
    iput-boolean v9, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mFirstLoad:Z

    .line 701
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mContinuationToken:Ljava/lang/String;

    .line 703
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getView()Landroid/view/View;

    move-result-object v8

    .line 704
    .local v8, view:Landroid/view/View;
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v0, :cond_cf

    .line 705
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->addLocationListener(Landroid/location/Location;)V

    .line 706
    invoke-virtual {v8, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 707
    invoke-direct {p0, v8}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    .line 708
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-nez v0, :cond_7a

    .line 709
    const v0, 0x7f08016c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v8, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    .line 717
    :cond_7a
    :goto_7a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prepareLoaderUri()V

    .line 718
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "circle_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "view"

    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 720
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v11, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 722
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v3, "streams"

    invoke-virtual {v0, v3, v10}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "circle"

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v5, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCurrentSpinnerPosition:I

    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;->getRealCircleId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x9

    if-lt v0, v4, :cond_de

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 723
    :goto_c4
    iput-boolean v9, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    .line 725
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->refresh()V

    .line 727
    .end local v6           #newInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;
    .end local v8           #view:Landroid/view/View;
    :cond_c9
    return-void

    .line 683
    .restart local v6       #newInfo:Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSpinnerInfo;
    :cond_ca
    const/4 v1, 0x0

    .restart local v1       #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    goto/16 :goto_2d

    .end local v1           #startView:Lcom/google/android/apps/plus/analytics/OzViews;
    :cond_cd
    move v0, v10

    .line 695
    goto :goto_51

    .line 712
    .restart local v8       #view:Landroid/view/View;
    :cond_cf
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->removeLocationListener()V

    .line 713
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    .line 714
    invoke-virtual {v8, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_7a

    .line 722
    :cond_de
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_c4
.end method

.method public final onReshareClicked(Ljava/lang/String;Z)V
    .registers 8
    .parameter "activityId"
    .parameter "isLimited"

    .prologue
    .line 1199
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3, p1, p2}, Lcom/google/android/apps/plus/phone/Intents;->getReshareActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    .line 1201
    .local v1, intent:Landroid/content/Intent;
    if-eqz p2, :cond_2f

    .line 1202
    const v2, 0x7f0801a0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0801a1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0801a2

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4, v1}, Lcom/google/android/apps/plus/fragments/ConfirmIntentDialog;->newInstance(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/content/Intent;)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 1206
    .local v0, dialog:Landroid/support/v4/app/DialogFragment;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "confirm_reshare"

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1210
    .end local v0           #dialog:Landroid/support/v4/app/DialogFragment;
    :goto_2e
    return-void

    .line 1208
    :cond_2f
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_2e
.end method

.method public onResume()V
    .registers 5

    .prologue
    .line 512
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    .line 514
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 516
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v3, :cond_2c

    .line 517
    const/4 v0, 0x0

    .local v0, i:I
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v1

    .local v1, size:I
    :goto_13
    if-ge v0, v1, :cond_27

    .line 518
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 519
    .local v2, v:Landroid/view/View;
    instance-of v3, v2, Lcom/google/android/apps/plus/views/CardView;

    if-eqz v3, :cond_24

    .line 520
    check-cast v2, Lcom/google/android/apps/plus/views/CardView;

    .end local v2           #v:Landroid/view/View;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/CardView;->onStart()V

    .line 517
    :cond_24
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 523
    :cond_27
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->onResume()V

    .line 526
    .end local v0           #i:I
    .end local v1           #size:I
    :cond_2c
    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v3, :cond_34

    .line 527
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->addLocationListener(Landroid/location/Location;)V

    .line 530
    :cond_34
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    .line 531
    return-void
.end method

.method protected final onResumeContentFetched(Landroid/view/View;)V
    .registers 2
    .parameter "view"

    .prologue
    .line 1108
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    .line 1109
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    .prologue
    .line 569
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 571
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v0, :cond_22

    .line 572
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->saveScrollPosition()V

    .line 573
    const-string v0, "scroll_pos"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 574
    const-string v0, "scroll_off"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 577
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-eqz v0, :cond_2d

    .line 578
    const-string v0, "location"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 581
    :cond_2d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLoaderHash:Ljava/lang/Integer;

    if-eqz v0, :cond_3c

    .line 582
    const-string v0, "loader_hash"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLoaderHash:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 585
    :cond_3c
    const-string v0, "stream_length"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamLength:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 586
    const-string v0, "last_deactivation"

    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLastDeactivationTime:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 587
    const-string v0, "error"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 588
    const-string v0, "reset_animation"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mResetAnimationState:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 589
    return-void
.end method

.method protected onSetArguments(Landroid/os/Bundle;)V
    .registers 5
    .parameter "args"

    .prologue
    const/4 v0, 0x0

    .line 593
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSetArguments(Landroid/os/Bundle;)V

    .line 595
    const-string v1, "gaia_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    .line 596
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    if-nez v1, :cond_39

    .line 597
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamOwnerUserId:Ljava/lang/String;

    .line 602
    :goto_18
    const-string v1, "circle_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    .line 604
    const-string v1, "view"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3e

    .line 605
    const-string v1, "view"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    .line 610
    :goto_30
    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_36

    const/4 v0, 0x1

    :cond_36
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    .line 611
    return-void

    .line 599
    :cond_39
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mStreamOwnerUserId:Ljava/lang/String;

    goto :goto_18

    .line 607
    :cond_3e
    iput v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    goto :goto_30
.end method

.method public final onStop()V
    .registers 5

    .prologue
    .line 555
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onStop()V

    .line 557
    const/4 v0, 0x0

    .local v0, i:I
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildCount()I

    move-result v1

    .local v1, size:I
    :goto_a
    if-ge v0, v1, :cond_1e

    .line 558
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 559
    .local v2, v:Landroid/view/View;
    instance-of v3, v2, Lcom/google/android/apps/plus/views/CardView;

    if-eqz v3, :cond_1b

    .line 560
    check-cast v2, Lcom/google/android/apps/plus/views/CardView;

    .end local v2           #v:Landroid/view/View;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/CardView;->onStop()V

    .line 557
    :cond_1b
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 564
    :cond_1e
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->invalidateViews()V

    .line 565
    return-void
.end method

.method public final onViewUsed(I)V
    .registers 4
    .parameter "position"

    .prologue
    .line 958
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPreloadRequested:Z

    if-nez v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mEndOfStream:Z

    if-nez v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mError:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-nez v0, :cond_11

    .line 965
    :cond_10
    :goto_10
    return-void

    .line 962
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mInnerAdapter:Lcom/google/android/apps/plus/phone/StreamAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/StreamAdapter;->getCount()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->sNextPagePreloadTriggerRows:I

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_10

    .line 963
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->prefetchContent()V

    goto :goto_10
.end method

.method protected prepareLoaderUri()V
    .registers 7

    .prologue
    const/4 v5, 0x0

    .line 941
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-nez v1, :cond_14

    .line 943
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "no_location_stream_key"

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPostsUri:Landroid/net/Uri;

    .line 954
    :goto_13
    return-void

    .line 946
    :cond_14
    const/4 v0, 0x0

    .line 947
    .local v0, location:Lcom/google/android/apps/plus/content/DbLocation;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    if-eqz v1, :cond_20

    .line 948
    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    .end local v0           #location:Lcom/google/android/apps/plus/content/DbLocation;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    invoke-direct {v0, v5, v1}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILandroid/location/Location;)V

    .line 950
    .restart local v0       #location:Lcom/google/android/apps/plus/content/DbLocation;
    :cond_20
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGaiaId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mCircleId:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mView:I

    invoke-static {v2, v3, v0, v5, v4}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mPostsUri:Landroid/net/Uri;

    goto :goto_13
.end method

.method public refresh()V
    .registers 3

    .prologue
    .line 1400
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRefreshDisabled:Z

    if-eqz v1, :cond_8

    .line 1401
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mRefreshDisabled:Z

    .line 1416
    :goto_7
    return-void

    .line 1405
    :cond_8
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    .line 1407
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v1, :cond_1b

    .line 1409
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    .line 1410
    .local v0, lastLocation:Landroid/location/Location;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocation:Landroid/location/Location;

    .line 1411
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->addLocationListener(Landroid/location/Location;)V

    .line 1412
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateSpinner()V

    goto :goto_7

    .line 1414
    .end local v0           #lastLocation:Landroid/location/Location;
    :cond_1b
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->fetchContent(Z)V

    goto :goto_7
.end method

.method protected final restoreScrollPosition()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 1531
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-nez v0, :cond_6

    .line 1542
    :cond_5
    :goto_5
    return-void

    .line 1535
    :cond_6
    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    if-nez v0, :cond_e

    iget v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    if-eqz v0, :cond_5

    .line 1536
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    iget v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setSelectionFromTop(II)V

    .line 1539
    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    .line 1540
    iput v3, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    goto :goto_5
.end method

.method protected final saveScrollPosition()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 1505
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-nez v1, :cond_6

    .line 1524
    :goto_5
    return-void

    .line 1513
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollPos:I

    .line 1514
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mAdapter:Lcom/google/android/apps/plus/phone/StreamTranslationAdapter;

    if-eqz v1, :cond_24

    .line 1515
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1516
    .local v0, v:Landroid/view/View;
    if-eqz v0, :cond_21

    .line 1517
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    goto :goto_5

    .line 1519
    :cond_21
    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    goto :goto_5

    .line 1522
    .end local v0           #v:Landroid/view/View;
    :cond_24
    iput v2, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mScrollOffset:I

    goto :goto_5
.end method

.method protected final showContent(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    const/4 v1, 0x0

    .line 1141
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showContent(Landroid/view/View;)V

    .line 1142
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v0, :cond_15

    .line 1143
    const v0, 0x7f09021f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1144
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    .line 1146
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mGridView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setVisibility(I)V

    .line 1147
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1148
    return-void
.end method

.method protected final showEmptyView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 1131
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showEmptyView(Landroid/view/View;)V

    .line 1132
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mNearby:Z

    if-eqz v0, :cond_15

    .line 1133
    const v0, 0x7f09021f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1134
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->updateLocationHeader(Landroid/view/View;)V

    .line 1136
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1137
    return-void
.end method

.method protected final showEmptyViewProgress(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 1119
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 1120
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1121
    return-void
.end method

.method protected final showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V
    .registers 5
    .parameter "view"
    .parameter "progressText"

    .prologue
    .line 1125
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->showEmptyViewProgress(Landroid/view/View;Ljava/lang/String;)V

    .line 1126
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/HostedStreamFragment;->mLocationDisabledView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1127
    return-void
.end method
