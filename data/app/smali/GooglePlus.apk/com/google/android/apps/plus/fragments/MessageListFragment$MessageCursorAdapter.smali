.class final Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "MessageListFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/MessageListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MessageCursorAdapter"
.end annotation


# instance fields
.field final mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

.field final mViews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;Landroid/widget/AbsListView;Landroid/database/Cursor;)V
    .registers 6
    .parameter "fragment"
    .parameter "listView"
    .parameter "cursor"

    .prologue
    .line 494
    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 495
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    .line 496
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    .line 498
    new-instance v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;)V

    invoke-virtual {p2, v0}, Landroid/widget/AbsListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 512
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 25
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 555
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mDataValid:Z

    if-nez v2, :cond_e

    .line 556
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "this should only be called when the cursor is valid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 559
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-nez v2, :cond_31

    .line 560
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "couldn\'t move cursor to position "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 563
    :cond_31
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/LayoutInflater;

    .line 565
    .local v9, layoutInflater:Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x7

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 566
    .local v10, messageType:I
    const/4 v2, 0x1

    if-ne v10, v2, :cond_308

    .line 567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/16 v3, 0xb

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 568
    .local v11, thumbUrl:Ljava/lang/String;
    if-eqz v11, :cond_200

    .line 570
    if-eqz p2, :cond_1ca

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    if-eqz v2, :cond_1ca

    move-object/from16 v12, p2

    .line 571
    check-cast v12, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    .line 577
    .local v12, view:Lcom/google/android/apps/plus/views/MessageListItemViewImage;
    :goto_61
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_76

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_76
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->clear()V

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x3

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x5

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long v16, v3, v5

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v12, v2, v3}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setMessageRowId(J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/16 v3, 0x9

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setAuthorName(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/16 v3, 0xb

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_13c

    const-string v2, "//"

    invoke-virtual {v13, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3a4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_d6
    const/4 v3, 0x0

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageWidth()Ljava/lang/Integer;

    move-result-object v4

    if-eqz v4, :cond_1f2

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageWidth()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v19

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageHeight()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v20

    const-string v3, "MessageListFragment"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_118

    const-string v3, "MessageListFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BindUserImageMessageView image width "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " height "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_118
    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1dd

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    new-instance v3, Lcom/google/android/apps/plus/content/LocalImageRequest;

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v3, v2, v0, v1}, Lcom/google/android/apps/plus/content/LocalImageRequest;-><init>(Lcom/google/android/apps/plus/api/MediaRef;II)V

    move-object v2, v3

    :goto_139
    invoke-virtual {v12, v13, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setImage(Ljava/lang/String;Lcom/google/android/apps/plus/content/ImageRequest;)V

    :cond_13c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->isFirst()Z

    move-result v2

    if-nez v2, :cond_18a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v2

    if-eqz v2, :cond_18a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v3, 0x5

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x3

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3a1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x7

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3a1

    sub-long v2, v16, v2

    invoke-static {}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1400()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_3a1

    const/4 v2, 0x1

    :goto_182
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move v14, v2

    :cond_18a
    if-eqz v15, :cond_1fa

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1500(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1fa

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1600(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z

    move-result v2

    if-nez v2, :cond_1fa

    const/4 v2, 0x1

    :goto_1a9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x6

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v12, v3, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setMessageStatus(IZ)V

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setTimeSince(Ljava/lang/CharSequence;)V

    invoke-static {v15}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setGaiaId(Ljava/lang/String;)V

    if-eqz v14, :cond_1fc

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->hideAuthor()V

    :goto_1c6
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->updateContentDescription()V

    .line 611
    .end local v11           #thumbUrl:Ljava/lang/String;
    .end local v12           #view:Lcom/google/android/apps/plus/views/MessageListItemViewImage;
    :goto_1c9
    return-object v12

    .line 573
    .restart local v11       #thumbUrl:Ljava/lang/String;
    :cond_1ca
    const v2, 0x7f03005e

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    .line 575
    .restart local v12       #view:Lcom/google/android/apps/plus/views/MessageListItemViewImage;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setMessageClickListener(Lcom/google/android/apps/plus/views/MessageClickListener;)V

    goto/16 :goto_61

    .line 577
    :cond_1dd
    move/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v0, v1, v13}, Lcom/google/android/apps/plus/util/ImageUtils;->getCenterCroppedAndResizedUrl(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v2, Lcom/google/android/apps/plus/content/MediaImageRequest;

    const/4 v4, 0x3

    const/4 v7, 0x0

    move/from16 v5, v19

    move/from16 v6, v20

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    goto/16 :goto_139

    :cond_1f2
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setOnMeasureListener(Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;)V

    move-object v2, v3

    goto/16 :goto_139

    :cond_1fa
    const/4 v2, 0x0

    goto :goto_1a9

    :cond_1fc
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->showAuthor()V

    goto :goto_1c6

    .line 581
    .end local v12           #view:Lcom/google/android/apps/plus/views/MessageListItemViewImage;
    :cond_200
    if-eqz p2, :cond_2ef

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/plus/views/MessageListItemView;

    if-eqz v2, :cond_2ef

    move-object/from16 v12, p2

    .line 582
    check-cast v12, Lcom/google/android/apps/plus/views/MessageListItemView;

    .line 588
    .local v12, view:Lcom/google/android/apps/plus/views/MessageListItemView;
    :goto_20c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_221

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v2, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_221
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemView;->clear()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v5, 0x3

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v6, 0x5

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    invoke-static {v3, v5, v6}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x0

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    invoke-virtual {v12, v13, v14}, Lcom/google/android/apps/plus/views/MessageListItemView;->setMessageRowId(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/16 v8, 0x9

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/MessageListItemView;->setAuthorName(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x4

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/MessageListItemView;->setMessage(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isFirst()Z

    move-result v3

    if-nez v3, :cond_2af

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v3

    if-eqz v3, :cond_2af

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x5

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    const-wide/16 v15, 0x3e8

    div-long/2addr v13, v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x3

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2a8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v8, 0x7

    invoke-interface {v3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v8, 0x1

    if-ne v3, v8, :cond_2a8

    sub-long/2addr v5, v13

    invoke-static {}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1400()J

    move-result-wide v13

    cmp-long v3, v5, v13

    if-gtz v3, :cond_2a8

    const/4 v2, 0x1

    :cond_2a8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    :cond_2af
    move v3, v2

    if-eqz v4, :cond_302

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1500(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_302

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->access$1600(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z

    move-result v2

    if-nez v2, :cond_302

    const/4 v2, 0x1

    :goto_2cf
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v6, 0x6

    invoke-interface {v5, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    invoke-virtual {v12, v5, v2}, Lcom/google/android/apps/plus/views/MessageListItemView;->setMessageStatus(IZ)V

    invoke-virtual {v12, v7}, Lcom/google/android/apps/plus/views/MessageListItemView;->setTimeSince(Ljava/lang/CharSequence;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemView;->setGaiaId(Ljava/lang/String;)V

    if-eqz v3, :cond_304

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemView;->hideAuthor()V

    :goto_2ea
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemView;->updateContentDescription()V

    goto/16 :goto_1c9

    .line 584
    .end local v12           #view:Lcom/google/android/apps/plus/views/MessageListItemView;
    :cond_2ef
    const v2, 0x7f03005d

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/views/MessageListItemView;

    .line 586
    .restart local v12       #view:Lcom/google/android/apps/plus/views/MessageListItemView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/MessageListItemView;->setMessageClickListener(Lcom/google/android/apps/plus/views/MessageClickListener;)V

    goto/16 :goto_20c

    .line 588
    :cond_302
    const/4 v2, 0x0

    goto :goto_2cf

    :cond_304
    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MessageListItemView;->showAuthor()V

    goto :goto_2ea

    .line 591
    .end local v11           #thumbUrl:Ljava/lang/String;
    .end local v12           #view:Lcom/google/android/apps/plus/views/MessageListItemView;
    :cond_308
    const/4 v2, 0x6

    if-ne v10, v2, :cond_356

    .line 593
    if-eqz p2, :cond_34b

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;

    if-eqz v2, :cond_34b

    move-object/from16 v12, p2

    .line 595
    check-cast v12, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;

    .line 600
    .local v12, view:Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;
    :goto_317
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x7

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->setType(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x5

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->setTimeSince(Ljava/lang/CharSequence;)V

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->updateContentDescription()V

    goto/16 :goto_1c9

    .line 597
    .end local v12           #view:Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;
    :cond_34b
    const v2, 0x7f030044

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;

    .restart local v12       #view:Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;
    goto :goto_317

    .line 604
    .end local v12           #view:Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;
    :cond_356
    if-eqz p2, :cond_396

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/plus/views/SystemMessageListItemView;

    if-eqz v2, :cond_396

    move-object/from16 v12, p2

    .line 605
    check-cast v12, Lcom/google/android/apps/plus/views/SystemMessageListItemView;

    .line 610
    .local v12, view:Lcom/google/android/apps/plus/views/SystemMessageListItemView;
    :goto_362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x7

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/SystemMessageListItemView;->setType(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Lcom/google/android/apps/plus/views/SystemMessageListItemView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, 0x5

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/Dates;->getShortRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v12, v2}, Lcom/google/android/apps/plus/views/SystemMessageListItemView;->setTimeSince(Ljava/lang/CharSequence;)V

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/SystemMessageListItemView;->updateContentDescription()V

    goto/16 :goto_1c9

    .line 607
    .end local v12           #view:Lcom/google/android/apps/plus/views/SystemMessageListItemView;
    :cond_396
    const v2, 0x7f0300ce

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/views/SystemMessageListItemView;

    .restart local v12       #view:Lcom/google/android/apps/plus/views/SystemMessageListItemView;
    goto :goto_362

    .restart local v11       #thumbUrl:Ljava/lang/String;
    .local v12, view:Lcom/google/android/apps/plus/views/MessageListItemViewImage;
    :cond_3a1
    move v2, v14

    goto/16 :goto_182

    :cond_3a4
    move-object v2, v13

    goto/16 :goto_d6
.end method

.method public final onMeasured(Landroid/view/View;)V
    .registers 16
    .parameter "view"

    .prologue
    const/4 v1, 0x0

    .line 520
    instance-of v2, p1, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    if-eqz v2, :cond_64

    move-object v13, p1

    .line 521
    check-cast v13, Lcom/google/android/apps/plus/views/MessageListItemViewImage;

    .line 525
    .local v13, mliv:Lcom/google/android/apps/plus/views/MessageListItemViewImage;
    invoke-virtual {v13, v1}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setOnMeasureListener(Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;)V

    .line 526
    invoke-virtual {v13}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageWidth()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 527
    .local v9, width:I
    invoke-virtual {v13}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getImageHeight()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 528
    .local v10, height:I
    const-string v2, "MessageListFragment"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_42

    .line 529
    const-string v2, "MessageListFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onMeasured image width "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " height "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    :cond_42
    invoke-virtual {v13}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getFullResUrl()Ljava/lang/String;

    move-result-object v12

    .line 534
    .local v12, imageUrl:Ljava/lang/String;
    const-string v2, "content://"

    invoke-virtual {v12, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_65

    .line 536
    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 537
    .local v5, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    const-wide/16 v2, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v4, v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 539
    .local v0, ref:Lcom/google/android/apps/plus/api/MediaRef;
    new-instance v6, Lcom/google/android/apps/plus/content/LocalImageRequest;

    invoke-direct {v6, v0, v9, v10}, Lcom/google/android/apps/plus/content/LocalImageRequest;-><init>(Lcom/google/android/apps/plus/api/MediaRef;II)V

    .line 547
    .end local v0           #ref:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v5           #uri:Landroid/net/Uri;
    .local v6, imageRequest:Lcom/google/android/apps/plus/content/ImageRequest;
    :goto_61
    invoke-virtual {v13, v12, v6}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setImage(Ljava/lang/String;Lcom/google/android/apps/plus/content/ImageRequest;)V

    .line 548
    .end local v6           #imageRequest:Lcom/google/android/apps/plus/content/ImageRequest;
    .end local v9           #width:I
    .end local v10           #height:I
    .end local v12           #imageUrl:Ljava/lang/String;
    .end local v13           #mliv:Lcom/google/android/apps/plus/views/MessageListItemViewImage;
    :cond_64
    return-void

    .line 541
    .restart local v9       #width:I
    .restart local v10       #height:I
    .restart local v12       #imageUrl:Ljava/lang/String;
    .restart local v13       #mliv:Lcom/google/android/apps/plus/views/MessageListItemViewImage;
    :cond_65
    invoke-static {v9, v10, v12}, Lcom/google/android/apps/plus/util/ImageUtils;->getCenterCroppedAndResizedUrl(IILjava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 544
    .local v7, resizedUrl:Ljava/lang/String;
    new-instance v6, Lcom/google/android/apps/plus/content/MediaImageRequest;

    const/4 v8, 0x3

    const/4 v11, 0x0

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/plus/content/MediaImageRequest;-><init>(Ljava/lang/String;IIIZ)V

    .restart local v6       #imageRequest:Lcom/google/android/apps/plus/content/ImageRequest;
    goto :goto_61
.end method

.method public final onResume()V
    .registers 2

    .prologue
    .line 742
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->mViews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 743
    return-void
.end method
