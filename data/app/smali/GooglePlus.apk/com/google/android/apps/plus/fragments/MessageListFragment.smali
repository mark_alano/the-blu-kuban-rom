.class public Lcom/google/android/apps/plus/fragments/MessageListFragment;
.super Lcom/google/android/apps/plus/fragments/EsListFragment;
.source "MessageListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/views/MessageClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;,
        Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;,
        Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;,
        Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;,
        Lcom/google/android/apps/plus/fragments/MessageListFragment$MessagesQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsListFragment",
        "<",
        "Landroid/widget/ListView;",
        "Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/views/MessageClickListener;"
    }
.end annotation


# static fields
.field private static COLLAPSE_POSTS_THRESHOLD:J


# instance fields
.field private defaultListViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAnimateTypingVisibilityRunnable:Ljava/lang/Runnable;

.field private mCheckExpiredTypingRunnable:Ljava/lang/Runnable;

.field private mConversationId:Ljava/lang/String;

.field private mConversationRowId:Ljava/lang/Long;

.field private mEarliestEventTimestamp:J

.field private mFirstEventTimestamp:J

.field private mHandler:Landroid/os/Handler;

.field private mHeaderView:Landroid/view/View;

.field private mInitialLoadFinished:Z

.field private mIsGroup:Z

.field private mIsTypingVisible:Z

.field private mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

.field private mLoadingOlderEvents:Z

.field private mMessagesUri:Landroid/net/Uri;

.field private mParticipantList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private final mRTCServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

.field private mRequestId:Ljava/lang/Integer;

.field private mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

.field private mSlideInUpAnimation:Landroid/view/animation/Animation;

.field private mSlideOutDownAnimation:Landroid/view/animation/Animation;

.field private mTotalItemBeforeLoadingOlder:I

.field private mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

.field private mTypingParticipants:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTypingTextView:Landroid/widget/TextView;

.field private mTypingView:Landroid/view/View;

.field private mTypingVisibilityChanged:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 80
    const-wide/32 v0, 0xea60

    sput-wide v0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->COLLAPSE_POSTS_THRESHOLD:J

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;-><init>()V

    .line 129
    new-instance v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    .line 141
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    .line 147
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsTypingVisible:Z

    .line 148
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z

    .line 150
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHandler:Landroid/os/Handler;

    .line 157
    new-instance v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAnimateTypingVisibilityRunnable:Ljava/lang/Runnable;

    .line 164
    new-instance v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mCheckExpiredTypingRunnable:Ljava/lang/Runnable;

    .line 488
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->animateTypingVisibility()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAnimateTypingVisibilityRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/google/android/apps/plus/fragments/MessageListFragment;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->updateHeaderVisibility()V

    return-void
.end method

.method static synthetic access$1400()J
    .registers 2

    .prologue
    .line 75
    sget-wide v0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->COLLAPSE_POSTS_THRESHOLD:J

    return-wide v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/MessageListFragment;)J
    .registers 3
    .parameter "x0"

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mEarliestEventTimestamp:J

    return-wide v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/fragments/MessageListFragment;)J
    .registers 3
    .parameter "x0"

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mFirstEventTimestamp:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .registers 11
    .parameter "x0"

    .prologue
    .line 75
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_13
    :goto_13
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;

    iget-wide v6, v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;->typingStartTimeMs:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x7530

    cmp-long v1, v6, v8

    if-lez v1, :cond_13

    const-string v1, "MessageListFragment"

    const/4 v6, 0x3

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_54

    const-string v6, "MessageListFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "Typing status expired for "

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;->userName:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_54
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_13

    :cond_5c
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_60
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_60

    :cond_72
    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHeaderView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/android/apps/plus/fragments/MessageListFragment;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 75
    iput p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTotalItemBeforeLoadingOlder:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->updateTypingVisibility()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/fragments/MessageListFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Long;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mParticipantList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mCheckExpiredTypingRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/MessageListFragment;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private declared-synchronized animateTypingVisibility()V
    .registers 15

    .prologue
    const/4 v0, 0x0

    .line 1012
    monitor-enter p0

    :try_start_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    if-nez v1, :cond_1c

    .line 1013
    :cond_a
    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1014
    const-string v0, "MessageListFragment"

    const-string v1, "Ignoring animation due to null views"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a
    .catchall {:try_start_2 .. :try_end_1a} :catchall_bd

    .line 1047
    :cond_1a
    :goto_1a
    monitor-exit p0

    return-void

    .line 1018
    :cond_1c
    const/4 v1, 0x0

    :try_start_1d
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z

    .line 1019
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-lez v1, :cond_c0

    const/4 v12, 0x1

    .line 1021
    .local v12, showTyping:Z
    :goto_28
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsTypingVisible:Z

    if-eq v1, v12, :cond_b9

    .line 1022
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    if-eqz v12, :cond_c3

    :goto_30
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1023
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->defaultListViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1024
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v13

    .line 1025
    .local v13, translationYDelta:I
    if-eqz v12, :cond_c7

    .line 1026
    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->defaultListViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    .line 1027
    .local v9, lp:Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v9, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1028
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1029
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    int-to-float v6, v13

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    .line 1037
    .end local v9           #lp:Landroid/widget/RelativeLayout$LayoutParams;
    :goto_6b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v10

    .line 1038
    .local v10, parentHeight:I
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v11

    .line 1039
    .local v11, parentWidth:I
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getWidth()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeight()I

    move-result v0

    invoke-virtual {v1, v2, v0, v11, v10}, Landroid/view/animation/TranslateAnimation;->initialize(IIII)V

    .line 1042
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 1043
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    if-eqz v12, :cond_d8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    :goto_ad
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1044
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1046
    .end local v10           #parentHeight:I
    .end local v11           #parentWidth:I
    .end local v13           #translationYDelta:I
    :cond_b9
    iput-boolean v12, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsTypingVisible:Z
    :try_end_bb
    .catchall {:try_start_1d .. :try_end_bb} :catchall_bd

    goto/16 :goto_1a

    .line 1012
    .end local v12           #showTyping:Z
    :catchall_bd
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_c0
    move v12, v0

    .line 1019
    goto/16 :goto_28

    .line 1022
    .restart local v12       #showTyping:Z
    :cond_c3
    const/16 v0, 0x8

    goto/16 :goto_30

    .line 1033
    .restart local v13       #translationYDelta:I
    :cond_c7
    :try_start_c7
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    neg-int v6, v13

    int-to-float v6, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    goto :goto_6b

    .line 1043
    .restart local v10       #parentHeight:I
    .restart local v11       #parentWidth:I
    :cond_d8
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideOutDownAnimation:Landroid/view/animation/Animation;
    :try_end_da
    .catchall {:try_start_c7 .. :try_end_da} :catchall_bd

    goto :goto_ad
.end method

.method private declared-synchronized isTypingAnimationPlaying()Z
    .registers 2

    .prologue
    .line 1007
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0}, Landroid/view/animation/TranslateAnimation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTranslateListAnimation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0}, Landroid/view/animation/TranslateAnimation;->hasEnded()Z
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_1a

    move-result v0

    if-nez v0, :cond_18

    const/4 v0, 0x1

    :goto_16
    monitor-exit p0

    return v0

    :cond_18
    const/4 v0, 0x0

    goto :goto_16

    :catchall_1a
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 5
    .parameter "action"

    .prologue
    .line 1148
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_11

    .line 1149
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1150
    .local v0, activity:Landroid/support/v4/app/FragmentActivity;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1153
    .end local v0           #activity:Landroid/support/v4/app/FragmentActivity;
    :cond_11
    return-void
.end method

.method private updateHeaderVisibility()V
    .registers 4

    .prologue
    .line 943
    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 944
    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateHeaderVisibility "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 947
    :cond_2b
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    if-eqz v0, :cond_3a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    if-eqz v0, :cond_3a

    .line 948
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHeaderView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 952
    :goto_39
    return-void

    .line 950
    :cond_3a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHeaderView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_39
.end method

.method private declared-synchronized updateTypingVisibility()V
    .registers 11

    .prologue
    const/4 v8, 0x3

    .line 958
    monitor-enter p0

    :try_start_2
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingTextView:Landroid/widget/TextView;
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_85

    if-nez v6, :cond_8

    .line 1004
    :goto_6
    monitor-exit p0

    return-void

    .line 961
    :cond_8
    const/4 v6, 0x3

    :try_start_9
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    new-array v5, v6, [Ljava/lang/String;

    .line 962
    .local v5, userNames:[Ljava/lang/String;
    const/4 v1, 0x0

    .line 963
    .local v1, total:I
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    move v2, v1

    .end local v1           #total:I
    .local v2, total:I
    :goto_21
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_37

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;

    .line 964
    .local v4, userInfo:Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;
    add-int/lit8 v1, v2, 0x1

    .end local v2           #total:I
    .restart local v1       #total:I
    iget-object v6, v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;->userName:Ljava/lang/String;

    aput-object v6, v5, v2

    .line 965
    if-eq v1, v8, :cond_38

    move v2, v1

    .line 966
    .end local v1           #total:I
    .restart local v2       #total:I
    goto :goto_21

    .end local v4           #userInfo:Lcom/google/android/apps/plus/fragments/MessageListFragment$UserTypingInfo;
    :cond_37
    move v1, v2

    .line 970
    .end local v2           #total:I
    .restart local v1       #total:I
    :cond_38
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    packed-switch v6, :pswitch_data_de

    .line 989
    const v6, 0x7f080219

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x1

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 993
    .local v3, typingMessage:Ljava/lang/String;
    :goto_66
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 996
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->isTypingAnimationPlaying()Z

    move-result v6

    if-eqz v6, :cond_d8

    .line 997
    const-string v6, "MessageListFragment"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_81

    .line 998
    const-string v6, "MessageListFragment"

    const-string v7, "Animation already playing. Setting typing visibility changed"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1000
    :cond_81
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z
    :try_end_84
    .catchall {:try_start_9 .. :try_end_84} :catchall_85

    goto :goto_6

    .line 958
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #total:I
    .end local v3           #typingMessage:Ljava/lang/String;
    .end local v5           #userNames:[Ljava/lang/String;
    :catchall_85
    move-exception v6

    monitor-exit p0

    throw v6

    .line 974
    .restart local v0       #i$:Ljava/util/Iterator;
    .restart local v1       #total:I
    .restart local v5       #userNames:[Ljava/lang/String;
    :pswitch_88
    :try_start_88
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 975
    .restart local v3       #typingMessage:Ljava/lang/String;
    goto :goto_66

    .line 977
    .end local v3           #typingMessage:Ljava/lang/String;
    :pswitch_93
    const v6, 0x7f080216

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 979
    .restart local v3       #typingMessage:Ljava/lang/String;
    goto :goto_66

    .line 981
    .end local v3           #typingMessage:Ljava/lang/String;
    :pswitch_a4
    const v6, 0x7f080217

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x1

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 983
    .restart local v3       #typingMessage:Ljava/lang/String;
    goto :goto_66

    .line 985
    .end local v3           #typingMessage:Ljava/lang/String;
    :pswitch_bb
    const v6, 0x7f080218

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x0

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const/4 v9, 0x1

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const/4 v9, 0x2

    aget-object v9, v5, v9

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 987
    .restart local v3       #typingMessage:Ljava/lang/String;
    goto :goto_66

    .line 1002
    :cond_d8
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->animateTypingVisibility()V
    :try_end_db
    .catchall {:try_start_88 .. :try_end_db} :catchall_85

    goto/16 :goto_6

    .line 970
    nop

    :pswitch_data_de
    .packed-switch 0x0
        :pswitch_88
        :pswitch_93
        :pswitch_a4
        :pswitch_bb
    .end packed-switch
.end method


# virtual methods
.method public final displayLeaveConversationDialog()V
    .registers 6

    .prologue
    .line 1138
    const v1, 0x7f0800cb

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0800cc

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0801e6

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0801c5

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 1143
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1144
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "leave_conversation"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1145
    return-void
.end method

.method public final handleFatalError(I)V
    .registers 9
    .parameter "fatalErrorType"

    .prologue
    .line 1064
    packed-switch p1, :pswitch_data_38

    .line 1076
    const v2, 0x7f080200

    .line 1080
    .local v2, textResource:I
    :goto_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1081
    .local v1, resources:Landroid/content/res/Resources;
    const v3, 0x7f0801ff

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f080203

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 1086
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v3, 0x0

    invoke-virtual {v0, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1087
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "conversation_error"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1088
    return-void

    .line 1067
    .end local v0           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    .end local v1           #resources:Landroid/content/res/Resources;
    .end local v2           #textResource:I
    :pswitch_2f
    const v2, 0x7f080201

    .line 1069
    .restart local v2       #textResource:I
    goto :goto_6

    .line 1071
    .end local v2           #textResource:I
    :pswitch_33
    const v2, 0x7f080202

    .line 1073
    .restart local v2       #textResource:I
    goto :goto_6

    .line 1064
    nop

    :pswitch_data_38
    .packed-switch 0x3
        :pswitch_2f
        :pswitch_33
    .end packed-switch
.end method

.method public final messageLoadFailed()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 433
    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 434
    const-string v0, "MessageListFragment"

    const-string v1, "messageLoadFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    :cond_11
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    .line 437
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->updateHeaderVisibility()V

    .line 438
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0800cf

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 440
    return-void
.end method

.method public final messageLoadSucceeded()V
    .registers 5

    .prologue
    .line 446
    const-string v1, "MessageListFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 447
    const-string v1, "MessageListFragment"

    const-string v2, "messageLoadSucceeded"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    :cond_10
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 457
    .local v0, handler:Landroid/os/Handler;
    new-instance v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 464
    return-void
.end method

.method public final onCancelButtonClicked(J)V
    .registers 5
    .parameter "messageRowId"

    .prologue
    .line 776
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->removeMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    .line 777
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 13
    .parameter "savedInstanceState"

    .prologue
    const-wide/16 v9, 0x15e

    const-wide/16 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 285
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 286
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 288
    .local v0, activity:Landroid/app/Activity;
    if-eqz p1, :cond_c6

    .line 289
    const-string v3, "request_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c2

    .line 290
    const-string v3, "request_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    .line 294
    :goto_23
    const-string v3, "loading_older_events"

    invoke-virtual {p1, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    .line 295
    const-string v3, "initial_load_finished"

    invoke-virtual {p1, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    .line 301
    :goto_33
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "participant"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "account"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "is_group"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z

    .line 305
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "conversation_row_id"

    invoke-virtual {v3, v4, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    .line 306
    .local v1, rowId:J
    cmp-long v3, v1, v7

    if-eqz v3, :cond_ce

    .line 307
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    .line 308
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/content/EsProvider;->buildMessagesUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mMessagesUri:Landroid/net/Uri;

    .line 309
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v6, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 314
    :goto_91
    const v3, 0x7f04000a

    invoke-static {v0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideOutDownAnimation:Landroid/view/animation/Animation;

    .line 316
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideOutDownAnimation:Landroid/view/animation/Animation;

    new-instance v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$3;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 331
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideOutDownAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 332
    const v3, 0x7f040009

    invoke-static {v0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    .line 333
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    new-instance v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$4;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 348
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSlideInUpAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v3, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 349
    return-void

    .line 292
    .end local v1           #rowId:J
    :cond_c2
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    goto/16 :goto_23

    .line 297
    :cond_c6
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    .line 298
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    .line 299
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    goto/16 :goto_33

    .line 311
    .restart local v1       #rowId:J
    :cond_ce
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    goto :goto_91
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 10
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 895
    const/4 v0, 0x1

    if-ne p1, v0, :cond_15

    .line 896
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mMessagesUri:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessagesQuery;->PROJECTION:[Ljava/lang/String;

    const-string v6, "timestamp"

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    :goto_14
    return-object v0

    :cond_15
    move-object v0, v4

    goto :goto_14
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 13
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v8, 0x0

    .line 800
    const v4, 0x7f03005b

    invoke-virtual {p1, v4, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 801
    .local v3, view:Landroid/view/View;
    const v4, 0x102000a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    .line 802
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->defaultListViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 803
    const v4, 0x7f090126

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    .line 804
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingView:Landroid/view/View;

    const v5, 0x7f090127

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingTextView:Landroid/widget/TextView;

    .line 805
    const v4, 0x7f03005c

    invoke-virtual {p1, v4, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 806
    .local v1, header:Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    invoke-virtual {v4, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 807
    const v4, 0x7f09012a

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHeaderView:Landroid/view/View;

    .line 808
    new-instance v4, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    const/4 v6, 0x0

    invoke-direct {v4, p0, v5, v6}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;Landroid/widget/AbsListView;Landroid/database/Cursor;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    .line 809
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 810
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_6c

    .line 811
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    invoke-virtual {v4, v8}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 814
    :cond_6c
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    new-instance v5, Lcom/google/android/apps/plus/fragments/MessageListFragment$6;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 841
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v4, Landroid/widget/ListView;

    new-instance v5, Lcom/google/android/apps/plus/fragments/MessageListFragment$7;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/MessageListFragment;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 866
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-eqz v4, :cond_b4

    .line 867
    const v4, 0x7f090128

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 868
    .local v0, emptyConversationView:Landroid/view/View;
    const v4, 0x7f090129

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 869
    .local v2, text:Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f0802c3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 871
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 873
    .end local v0           #emptyConversationView:Landroid/view/View;
    .end local v2           #text:Landroid/widget/TextView;
    :cond_b4
    return-object v3
.end method

.method public final bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 75
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic onDestroyView()V
    .registers 1

    .prologue
    .line 75
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onDestroyView()V

    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 6
    .parameter "tag"

    .prologue
    .line 1123
    const-string v0, "conversation_error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 1124
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->leaveConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    .line 1125
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;->leaveConversation()V

    .line 1126
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1128
    :cond_23
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 1135
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1116
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 7
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1095
    const-string v0, "leave_conversation"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 1096
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 1097
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->leaveConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    .line 1098
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;->leaveConversation()V

    .line 1099
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1109
    :cond_28
    :goto_28
    return-void

    .line 1100
    :cond_29
    const-string v0, "conversation_error"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 1101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->leaveConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    .line 1102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;->leaveConversation()V

    .line 1103
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_28

    .line 1105
    :cond_4d
    const-string v0, "MessageListFragment"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 1106
    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalidate dialog "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_28
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 7
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v3, 0x1

    .line 75
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_22

    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onLoadFinished "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_22
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v3, :cond_5d

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->restoreScrollPosition()V

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_5e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->showContent(Landroid/view/View;)V

    :goto_3f
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    if-eqz v0, :cond_55

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTotalItemBeforeLoadingOlder:I

    sub-int/2addr v0, v1

    if-gez v0, :cond_66

    const/4 v0, 0x0

    move v1, v0

    :goto_4e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    :cond_55
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->updateHeaderVisibility()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->onAsyncData()V

    :cond_5d
    return-void

    :cond_5e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_3f

    :cond_66
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_74

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    goto :goto_4e

    :cond_74
    move v1, v0

    goto :goto_4e
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1054
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 1055
    return-void
.end method

.method public final onMediaImageClick(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "url"
    .parameter "imageOwnerGaiaId"

    .prologue
    .line 784
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoViewActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v0

    .line 786
    .local v0, builder:Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoOnly(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setPhotoUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    move-result-object v1

    const v2, 0x7f080080

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;

    .line 791
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoViewIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->startActivity(Landroid/content/Intent;)V

    .line 792
    return-void
.end method

.method public final onPause()V
    .registers 3

    .prologue
    .line 471
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onPause()V

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAnimateTypingVisibilityRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 474
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mCheckExpiredTypingRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 475
    return-void
.end method

.method public final onResume()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 389
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onResume()V

    .line 390
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 427
    :goto_e
    return-void

    .line 395
    :cond_f
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsTypingVisible:Z

    .line 396
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingVisibilityChanged:Z

    .line 397
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 398
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRTCServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 400
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    if-eqz v1, :cond_32

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_32

    .line 402
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 404
    :cond_32
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_5b

    .line 407
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_5b

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->removeResult(I)Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    move-result-object v0

    .line 411
    .local v0, result:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    if-eqz v0, :cond_58

    .line 413
    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getErrorCode()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_63

    .line 415
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->messageLoadSucceeded()V

    .line 423
    :cond_58
    :goto_58
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    .line 426
    .end local v0           #result:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    :cond_5b
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v1, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment$MessageCursorAdapter;->onResume()V

    goto :goto_e

    .line 418
    .restart local v0       #result:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
    :cond_63
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->messageLoadFailed()V

    goto :goto_58
.end method

.method public final onRetryButtonClicked(J)V
    .registers 5
    .parameter "messageRowId"

    .prologue
    .line 765
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_RETRY_SEND:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 766
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->retrySendMessage(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    .line 768
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "bundle"

    .prologue
    .line 881
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 882
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 883
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 885
    :cond_12
    const-string v0, "loading_older_events"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLoadingOlderEvents:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 886
    const-string v0, "initial_load_finished"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 887
    return-void
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 75
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 75
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method

.method public final onUserImageClicked(Ljava/lang/String;)V
    .registers 7
    .parameter "gaiaId"

    .prologue
    .line 752
    if-eqz p1, :cond_18

    .line 753
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 755
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 757
    .end local v0           #intent:Landroid/content/Intent;
    :cond_18
    return-void
.end method

.method public final reinitialize()V
    .registers 13

    .prologue
    const-wide/16 v10, -0x1

    const/4 v9, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 355
    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mInitialLoadFinished:Z

    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "account"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 357
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "is_group"

    invoke-virtual {v4, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mIsGroup:Z

    .line 358
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "conversation_row_id"

    invoke-virtual {v4, v5, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    .line 359
    .local v1, rowId:J
    cmp-long v4, v1, v10

    if-eqz v4, :cond_b2

    .line 360
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    .line 361
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/content/EsProvider;->buildMessagesUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mMessagesUri:Landroid/net/Uri;

    .line 362
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    invoke-virtual {v4, v7, v9, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 366
    :goto_5a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "participant"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 368
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_a9

    .line 369
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f090128

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 370
    .local v0, emptyConversationView:Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-eqz v4, :cond_b5

    .line 371
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f090129

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 372
    .local v3, text:Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f0802c3

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 380
    .end local v0           #emptyConversationView:Landroid/view/View;
    .end local v3           #text:Landroid/widget/TextView;
    :cond_a9
    :goto_a9
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mTypingParticipants:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 381
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->animateTypingVisibility()V

    .line 382
    return-void

    .line 364
    :cond_b2
    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationRowId:Ljava/lang/Long;

    goto :goto_5a

    .line 376
    .restart local v0       #emptyConversationView:Landroid/view/View;
    :cond_b5
    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_a9
.end method

.method public final setConversationInfo(Ljava/lang/String;JJ)V
    .registers 9
    .parameter "conversationId"
    .parameter "firstEventTimestamp"
    .parameter "earliestEventTimestamp"

    .prologue
    .line 479
    const-string v0, "MessageListFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 480
    const-string v0, "MessageListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setConversationInfo first "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " earliest local "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    :cond_27
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mConversationId:Ljava/lang/String;

    .line 484
    iput-wide p2, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mFirstEventTimestamp:J

    .line 485
    iput-wide p4, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mEarliestEventTimestamp:J

    .line 486
    return-void
.end method

.method public final setLeaveConversationListener(Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 277
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mLeaveConversationListener:Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;

    .line 278
    return-void
.end method

.method public final setParticipantList(Ljava/util/HashMap;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 185
    .local p1, participantList:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/MessageListFragment;->mParticipantList:Ljava/util/HashMap;

    .line 186
    return-void
.end method
