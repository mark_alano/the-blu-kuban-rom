.class public Lcom/google/android/apps/plus/fragments/MuteProfileDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "MuteProfileDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mGender:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mTargetMuteState:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 5
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 81
    packed-switch p2, :pswitch_data_14

    .line 91
    :goto_3
    return-void

    .line 83
    :pswitch_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->setPersonMuted(Z)V

    goto :goto_3

    .line 87
    :pswitch_10
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_3

    .line 81
    :pswitch_data_14
    .packed-switch -0x2
        :pswitch_10
        :pswitch_4
    .end packed-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 15
    .parameter "savedInstanceState"

    .prologue
    const/4 v12, 0x1

    .line 40
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 41
    .local v1, args:Landroid/os/Bundle;
    const-string v8, "name"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mName:Ljava/lang/String;

    .line 42
    const-string v8, "gender"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mGender:Ljava/lang/String;

    .line 43
    const-string v8, "target_mute"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 46
    .local v0, activity:Landroid/support/v4/app/FragmentActivity;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 47
    .local v2, builder:Landroid/app/AlertDialog$Builder;
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    if-eqz v8, :cond_8c

    const v8, 0x7f080267

    :goto_2d
    new-array v9, v12, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mName:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 49
    .local v6, title:Ljava/lang/String;
    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 51
    const v8, 0x104000a

    invoke-virtual {v2, v8, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 52
    const/high16 v8, 0x104

    invoke-virtual {v2, v8, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 53
    invoke-virtual {v2, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 56
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f03000b

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 58
    .local v7, view:Landroid/view/View;
    const v8, 0x7f09005a

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 60
    .local v5, message:Landroid/widget/TextView;
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mGender:Ljava/lang/String;

    const-string v9, "MALE"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_94

    .line 61
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    if-eqz v8, :cond_90

    const v8, 0x7f080268

    :goto_6f
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 70
    .local v3, content:Ljava/lang/String;
    :goto_73
    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const v8, 0x7f09005b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 73
    .local v4, explanation:Landroid/widget/TextView;
    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 76
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    return-object v8

    .line 47
    .end local v3           #content:Ljava/lang/String;
    .end local v4           #explanation:Landroid/widget/TextView;
    .end local v5           #message:Landroid/widget/TextView;
    .end local v6           #title:Ljava/lang/String;
    .end local v7           #view:Landroid/view/View;
    :cond_8c
    const v8, 0x7f08026c

    goto :goto_2d

    .line 61
    .restart local v5       #message:Landroid/widget/TextView;
    .restart local v6       #title:Ljava/lang/String;
    .restart local v7       #view:Landroid/view/View;
    :cond_90
    const v8, 0x7f08026d

    goto :goto_6f

    .line 63
    :cond_94
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mGender:Ljava/lang/String;

    const-string v9, "FEMALE"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_ae

    .line 64
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    if-eqz v8, :cond_aa

    const v8, 0x7f080269

    :goto_a5
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    .restart local v3       #content:Ljava/lang/String;
    goto :goto_73

    .end local v3           #content:Ljava/lang/String;
    :cond_aa
    const v8, 0x7f08026e

    goto :goto_a5

    .line 67
    :cond_ae
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->mTargetMuteState:Z

    if-eqz v8, :cond_ba

    const v8, 0x7f08026a

    :goto_b5
    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/MuteProfileDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    .restart local v3       #content:Ljava/lang/String;
    goto :goto_73

    .end local v3           #content:Ljava/lang/String;
    :cond_ba
    const v8, 0x7f08026f

    goto :goto_b5
.end method
