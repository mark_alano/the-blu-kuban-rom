.class public Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;
.super Landroid/support/v4/app/Fragment;
.source "OobContactsSyncFragment.java"


# instance fields
.field private mArrowContactsStatsSync:Landroid/widget/ImageView;

.field private mArrowContactsSync:Landroid/widget/ImageView;

.field private mCiclesImage:Landroid/widget/ImageView;

.field private mContactsImage:Landroid/widget/ImageView;

.field private mContactsStatsSyncChoice:Landroid/widget/CheckBox;

.field private mContactsSyncChoice:Landroid/widget/CheckBox;

.field private statsSyncOnly:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)Landroid/widget/ImageView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mArrowContactsStatsSync:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)Landroid/widget/ImageView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mArrowContactsSync:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)Landroid/widget/CheckBox;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsSyncChoice:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)Landroid/widget/CheckBox;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method public final commit()Z
    .registers 7

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 156
    .local v1, activity:Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "account"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    .line 158
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->statsSyncOnly:Z

    if-nez v4, :cond_1d

    .line 159
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsSyncChoice:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    .line 160
    .local v3, syncOn:Z
    invoke-static {v1, v0, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsSyncPreference(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    .line 163
    .end local v3           #syncOn:Z
    :cond_1d
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    .line 164
    .local v2, statsSyncOn:Z
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsSyncPreference(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    .line 166
    if-eqz v2, :cond_2d

    .line 168
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/EsService;->disableWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    .line 173
    :goto_2b
    const/4 v4, 0x1

    return v4

    .line 171
    :cond_2d
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/service/EsService;->enableAndPerformWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    goto :goto_2b
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 17
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 61
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0xe

    if-ge v11, v12, :cond_e1

    const/4 v11, 0x1

    :goto_7
    iput-boolean v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->statsSyncOnly:Z

    .line 63
    const v11, 0x7f030069

    const/4 v12, 0x0

    invoke-virtual {p1, v11, p2, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 66
    .local v10, view:Landroid/view/View;
    const v11, 0x7f0900b9

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mCiclesImage:Landroid/widget/ImageView;

    .line 67
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mCiclesImage:Landroid/widget/ImageView;

    const v12, 0x7f020086

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 69
    const v11, 0x7f090141

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsImage:Landroid/widget/ImageView;

    .line 70
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsImage:Landroid/widget/ImageView;

    const v12, 0x7f02018c

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 73
    const v11, 0x7f090142

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mArrowContactsStatsSync:Landroid/widget/ImageView;

    .line 76
    const v11, 0x7f090143

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/CheckBox;

    iput-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    .line 77
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    new-instance v12, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$1;

    invoke-direct {v12, p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)V

    invoke-virtual {v11, v12}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 83
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 85
    iget-boolean v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->statsSyncOnly:Z

    if-eqz v11, :cond_e4

    .line 86
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mArrowContactsStatsSync:Landroid/widget/ImageView;

    const v12, 0x7f02018b

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 126
    :goto_69
    iget-boolean v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->statsSyncOnly:Z

    if-eqz v11, :cond_14e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    .line 128
    .local v0, checkBoxDescription:Landroid/widget/TextView;
    :goto_6f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 129
    .local v7, resources:Landroid/content/res/Resources;
    const v11, 0x7f0802bd

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 131
    .local v4, hyperlinkMessage:Ljava/lang/String;
    new-instance v8, Landroid/text/SpannableString;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/apps/plus/util/AndroidUtils;->hasTelephony(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_15a

    const v11, 0x7f0802ba

    :goto_8e
    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const v12, 0x7f0802bc

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v11}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 139
    .local v8, s:Landroid/text/SpannableString;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f080413

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 141
    .local v3, helpUrlParam:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    invoke-static {v11, v3}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 142
    .local v5, link:Landroid/net/Uri;
    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 143
    .local v6, pattern:Ljava/util/regex/Pattern;
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 144
    .local v9, scheme:Ljava/lang/String;
    invoke-static {v8, v6, v9}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    .line 145
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v11

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 148
    return-object v10

    .line 61
    .end local v0           #checkBoxDescription:Landroid/widget/TextView;
    .end local v3           #helpUrlParam:Ljava/lang/String;
    .end local v4           #hyperlinkMessage:Ljava/lang/String;
    .end local v5           #link:Landroid/net/Uri;
    .end local v6           #pattern:Ljava/util/regex/Pattern;
    .end local v7           #resources:Landroid/content/res/Resources;
    .end local v8           #s:Landroid/text/SpannableString;
    .end local v9           #scheme:Ljava/lang/String;
    .end local v10           #view:Landroid/view/View;
    :cond_e1
    const/4 v11, 0x0

    goto/16 :goto_7

    .line 88
    .restart local v10       #view:Landroid/view/View;
    :cond_e4
    const v11, 0x7f090144

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mArrowContactsSync:Landroid/widget/ImageView;

    .line 89
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mArrowContactsSync:Landroid/widget/ImageView;

    const v12, 0x7f02018b

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 91
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mArrowContactsStatsSync:Landroid/widget/ImageView;

    const v12, 0x7f02018a

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 93
    const v11, 0x7f090145

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/CheckBox;

    iput-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsSyncChoice:Landroid/widget/CheckBox;

    .line 94
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsSyncChoice:Landroid/widget/CheckBox;

    new-instance v12, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$2;

    invoke-direct {v12, p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)V

    invoke-virtual {v11, v12}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 100
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->mContactsSyncChoice:Landroid/widget/CheckBox;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 102
    new-instance v2, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)V

    .line 108
    .local v2, contactsSyncToggler:Landroid/view/View$OnClickListener;
    const v11, 0x7f090146

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    const v11, 0x7f090147

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    new-instance v1, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;)V

    .line 119
    .local v1, contactsStatsSyncToggler:Landroid/view/View$OnClickListener;
    const v11, 0x7f090148

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    const v11, 0x7f090149

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_69

    .line 126
    .end local v1           #contactsStatsSyncToggler:Landroid/view/View$OnClickListener;
    .end local v2           #contactsSyncToggler:Landroid/view/View$OnClickListener;
    :cond_14e
    const v11, 0x7f090149

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    move-object v0, v11

    goto/16 :goto_6f

    .line 131
    .restart local v0       #checkBoxDescription:Landroid/widget/TextView;
    .restart local v4       #hyperlinkMessage:Ljava/lang/String;
    .restart local v7       #resources:Landroid/content/res/Resources;
    :cond_15a
    const v11, 0x7f0802bb

    goto/16 :goto_8e
.end method
