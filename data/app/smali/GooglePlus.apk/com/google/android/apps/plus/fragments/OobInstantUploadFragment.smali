.class public Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;
.super Landroid/support/v4/app/Fragment;
.source "OobInstantUploadFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# instance fields
.field private mUploadChoice:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private doNextStep()V
    .registers 9

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mUploadChoice:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v7

    .line 178
    .local v7, option:I
    packed-switch v7, :pswitch_data_2e

    .line 193
    const/4 v5, 0x0

    .line 194
    .local v5, uploadEnabled:Z
    const/4 v4, 0x1

    .line 199
    .local v4, wifiOnly:Z
    :goto_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    .line 200
    .local v2, activity:Lcom/google/android/apps/plus/phone/OobDeviceActivity;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 201
    .local v6, myIntent:Landroid/content/Intent;
    const-string v0, "account"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    .line 204
    .local v3, account:Lcom/google/android/apps/plus/content/EsAccount;
    new-instance v0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZZ)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 205
    return-void

    .line 180
    .end local v2           #activity:Lcom/google/android/apps/plus/phone/OobDeviceActivity;
    .end local v3           #account:Lcom/google/android/apps/plus/content/EsAccount;
    .end local v4           #wifiOnly:Z
    .end local v5           #uploadEnabled:Z
    .end local v6           #myIntent:Landroid/content/Intent;
    :pswitch_28
    const/4 v5, 0x1

    .line 181
    .restart local v5       #uploadEnabled:Z
    const/4 v4, 0x1

    .line 182
    .restart local v4       #wifiOnly:Z
    goto :goto_b

    .line 186
    .end local v4           #wifiOnly:Z
    .end local v5           #uploadEnabled:Z
    :pswitch_2b
    const/4 v5, 0x1

    .line 187
    .restart local v5       #uploadEnabled:Z
    const/4 v4, 0x0

    .line 188
    .restart local v4       #wifiOnly:Z
    goto :goto_b

    .line 178
    :pswitch_data_2e
    .packed-switch 0x7f09014e
        :pswitch_2b
        :pswitch_28
    .end packed-switch
.end method


# virtual methods
.method public final commit()Z
    .registers 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 71
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mUploadChoice:Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    const v6, 0x7f090150

    if-ne v3, v6, :cond_30

    move v0, v4

    .line 72
    .local v0, iuDisabled:Z
    :goto_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v6, "account"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/InstantUpload;->isSyncEnabled$1f9c1b43(Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    .line 73
    .local v1, iuSyncEnabled:Z
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    .line 75
    .local v2, masterSyncEnabled:Z
    if-eqz v2, :cond_2a

    if-nez v1, :cond_2c

    :cond_2a
    if-eqz v0, :cond_32

    .line 77
    :cond_2c
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->doNextStep()V

    .line 88
    :goto_2f
    return v4

    .end local v0           #iuDisabled:Z
    .end local v1           #iuSyncEnabled:Z
    .end local v2           #masterSyncEnabled:Z
    :cond_30
    move v0, v5

    .line 71
    goto :goto_e

    .line 81
    .restart local v0       #iuDisabled:Z
    .restart local v1       #iuSyncEnabled:Z
    .restart local v2       #masterSyncEnabled:Z
    :cond_32
    if-nez v2, :cond_64

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "photo_master_dialog"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_62

    const v4, 0x7f080042

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f080043

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0801c4

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v4, v6, v7, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v4

    invoke-virtual {v4, p0, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    const-string v6, "photo_master_dialog"

    invoke-virtual {v4, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_62
    :goto_62
    move v4, v5

    .line 88
    goto :goto_2f

    .line 86
    :cond_64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v6, "photo_sync_dialog"

    invoke-virtual {v3, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v6

    if-nez v6, :cond_62

    const v6, 0x7f08002d

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f080045

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v6, v4, v5

    invoke-virtual {p0, v7, v4}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const v6, 0x7f080044

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0801c6

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0801c8

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v4, v7, v8}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v4

    invoke-virtual {v4, p0, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    const-string v6, "photo_sync_dialog"

    invoke-virtual {v4, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_62
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const v5, 0x7f09014e

    .line 45
    const v3, 0x7f03006d

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 47
    .local v1, view:Landroid/view/View;
    const v3, 0x7f09014d

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioGroup;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mUploadChoice:Landroid/widget/RadioGroup;

    .line 48
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mUploadChoice:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v5}, Landroid/widget/RadioGroup;->check(I)V

    .line 51
    const-string v3, "ro.carrier"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    .local v0, carrier:Ljava/lang/String;
    const-string v3, "wifi-only"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3f

    .line 54
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mUploadChoice:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v5}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 57
    .local v2, wifiMobileUploadOption:Landroid/view/View;
    if-eqz v2, :cond_37

    .line 58
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mUploadChoice:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v2}, Landroid/widget/RadioGroup;->removeView(Landroid/view/View;)V

    .line 61
    :cond_37
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->mUploadChoice:Landroid/widget/RadioGroup;

    const v4, 0x7f09014f

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->check(I)V

    .line 64
    .end local v2           #wifiMobileUploadOption:Landroid/view/View;
    :cond_3f
    return-object v1
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 150
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 157
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 143
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 4
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->doNextStep()V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinue()V

    .line 136
    return-void
.end method
