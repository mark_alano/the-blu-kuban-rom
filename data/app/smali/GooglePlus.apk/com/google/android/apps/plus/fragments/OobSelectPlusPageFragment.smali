.class public Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;
.super Landroid/support/v4/app/ListFragment;
.source "OobSelectPlusPageFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$ServiceListener;
    }
.end annotation


# static fields
.field private static final DIALOG_IDS:[Ljava/lang/String;


# instance fields
.field private mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

.field private final mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mSelectedAccountPosition:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "activation_progress"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "net_failure"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "server_error"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->DIALOG_IDS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 53
    new-instance v0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$ServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    .line 61
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private createAccountNameArray()[Ljava/lang/String;
    .registers 10

    .prologue
    const/4 v8, 0x0

    .line 320
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 321
    .local v4, res:Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getNumPlusPages()I

    move-result v2

    .line 322
    .local v2, numPages:I
    add-int/lit8 v5, v2, 0x1

    new-array v0, v5, [Ljava/lang/String;

    .line 324
    .local v0, accountNames:[Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getUserDisplayName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v8

    .line 325
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1c
    if-ge v1, v2, :cond_37

    .line 326
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getPlusPageName(I)Ljava/lang/String;

    move-result-object v3

    .line 327
    .local v3, pageName:Ljava/lang/String;
    add-int/lit8 v5, v1, 0x1

    const v6, 0x7f080047

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v3, v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v5

    .line 325
    add-int/lit8 v1, v1, 0x1

    goto :goto_1c

    .line 329
    .end local v3           #pageName:Ljava/lang/String;
    :cond_37
    return-object v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 13
    .parameter "requestId"
    .parameter "result"

    .prologue
    const v9, 0x7f08003d

    const v8, 0x7f08003a

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_15

    .line 157
    :cond_14
    :goto_14
    return-void

    .line 143
    :cond_15
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 145
    sget-object v5, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->DIALOG_IDS:[Ljava/lang/String;

    array-length v6, v5

    move v2, v4

    :goto_1b
    if-ge v2, v6, :cond_32

    aget-object v1, v5, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    if-eqz v1, :cond_2e

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2e
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1b

    .line 147
    :cond_32
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_c1

    .line 148
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v0

    .line 149
    .local v0, exception:Ljava/lang/Exception;
    instance-of v1, v0, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v1, :cond_96

    .line 150
    check-cast v0, Lcom/google/android/apps/plus/api/OzServerException;

    .end local v0           #exception:Ljava/lang/Exception;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_cc

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_51
    const v5, 0x7f0801c4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v1, v5, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {v1, p0, v4}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "server_error"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_14

    :sswitch_6c
    const v1, 0x7f08003b

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v3

    goto :goto_51

    :sswitch_75
    const v1, 0x7f080038

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v3

    goto :goto_51

    :sswitch_7e
    const v1, 0x7f080039

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v3

    goto :goto_51

    :sswitch_87
    const v1, 0x7f08003e

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v1, 0x7f08003f

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_51

    .line 152
    .restart local v0       #exception:Ljava/lang/Exception;
    :cond_96
    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f080040

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v5, 0x7f0801c5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v3, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/support/v4/app/DialogFragment;->setCancelable(Z)V

    invoke-virtual {v1, p0, v4}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "net_failure"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 155
    .end local v0           #exception:Ljava/lang/Exception;
    :cond_c1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/OobDeviceActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinue()V

    goto/16 :goto_14

    .line 150
    :sswitch_data_cc
    .sparse-switch
        0x1 -> :sswitch_75
        0xa -> :sswitch_6c
        0xc -> :sswitch_7e
        0xf -> :sswitch_87
    .end sparse-switch
.end method


# virtual methods
.method public final activateAccount()V
    .registers 12

    .prologue
    const/4 v6, 0x0

    .line 284
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->isAccountSelected()Z

    move-result v9

    if-nez v9, :cond_8

    .line 313
    :goto_7
    return-void

    .line 289
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 290
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 291
    .local v8, myIntent:Landroid/content/Intent;
    const-string v9, "account"

    invoke-virtual {v8, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    .line 296
    .local v1, account:Lcom/google/android/apps/plus/content/EsAccount;
    iget v9, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    if-lez v9, :cond_57

    const/4 v5, 0x1

    .line 298
    .local v5, isPlusPage:Z
    :goto_1d
    if-eqz v5, :cond_59

    .line 300
    iget v9, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    add-int/lit8 v7, v9, -0x1

    .line 301
    .local v7, index:I
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v9, v7}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getPlusPageId(I)Ljava/lang/String;

    move-result-object v2

    .line 302
    .local v2, gaiaId:Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v9, v7}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getPlusPageName(I)Ljava/lang/String;

    move-result-object v3

    .line 303
    .local v3, displayName:Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v9, v7}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getPlusPagePhotoUrl(I)Ljava/lang/String;

    move-result-object v4

    .line 310
    .end local v7           #index:I
    .local v4, photoUrl:Ljava/lang/String;
    :goto_35
    const/4 v9, 0x0

    const v10, 0x7f080037

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10, v6}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v9

    const-string v10, "activation_progress"

    invoke-virtual {v6, v9, v10}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 311
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->activateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/plus/content/AccountSettingsData;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto :goto_7

    .end local v2           #gaiaId:Ljava/lang/String;
    .end local v3           #displayName:Ljava/lang/String;
    .end local v4           #photoUrl:Ljava/lang/String;
    .end local v5           #isPlusPage:Z
    :cond_57
    move v5, v6

    .line 296
    goto :goto_1d

    .line 305
    .restart local v5       #isPlusPage:Z
    :cond_59
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getUserGaiaId()Ljava/lang/String;

    move-result-object v2

    .line 306
    .restart local v2       #gaiaId:Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getUserDisplayName()Ljava/lang/String;

    move-result-object v3

    .line 307
    .restart local v3       #displayName:Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/AccountSettingsData;->getUserPhotoUrl()Ljava/lang/String;

    move-result-object v4

    .restart local v4       #photoUrl:Ljava/lang/String;
    goto :goto_35
.end method

.method public final isAccountSelected()Z
    .registers 3

    .prologue
    .line 277
    iget v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v0, 0x1

    :goto_6
    return v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 75
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "plus_pages"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/AccountSettingsData;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mAccountSettings:Lcom/google/android/apps/plus/content/AccountSettingsData;

    .line 77
    const v3, 0x7f03006f

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 80
    .local v2, view:Landroid/view/View;
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x109000f

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->createAccountNameArray()[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v3, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 82
    .local v0, adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 84
    if-eqz p3, :cond_4c

    .line 85
    const-string v3, "selected_account"

    const/4 v4, -0x1

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    .line 87
    const-string v3, "reqid"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4c

    .line 88
    const-string v3, "reqid"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 92
    :cond_4c
    return-object v2
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 4
    .parameter "tag"

    .prologue
    .line 256
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "OOB dialog not cancelable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 261
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 252
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 4
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 245
    const-string v0, "net_failure"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 246
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->activateAccount()V

    .line 248
    :cond_b
    return-void
.end method

.method public final onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .registers 9
    .parameter "l"
    .parameter "v"
    .parameter "position"
    .parameter "id"

    .prologue
    .line 265
    invoke-super/range {p0 .. p5}, Landroid/support/v4/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 266
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->isAccountSelected()Z

    move-result v0

    .line 267
    .local v0, wasAccountSelected:Z
    iput p3, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    .line 268
    if-nez v0, :cond_15

    .line 269
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->setContinueButtonEnabled(Z)V

    .line 271
    :cond_15
    return-void
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 134
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 136
    return-void
.end method

.method public final onResume()V
    .registers 3

    .prologue
    .line 116
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mEsServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2d

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2d

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 122
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    if-eqz v0, :cond_2d

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 127
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_2d
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 105
    const-string v0, "selected_account"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mSelectedAccountPosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_19

    .line 107
    const-string v0, "reqid"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 109
    :cond_19
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 5
    .parameter "view"
    .parameter "savedInstanceState"

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 100
    return-void
.end method
