.class public Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;
.super Landroid/support/v4/app/Fragment;
.source "PeopleSearchAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddEmailDialogListener"
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 316
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 346
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 353
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 339
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 4
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    if-eqz v0, :cond_9

    .line 330
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 332
    :cond_9
    return-void
.end method

.method public final setAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .registers 2
    .parameter "adapter"

    .prologue
    .line 321
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    .line 322
    return-void
.end method
