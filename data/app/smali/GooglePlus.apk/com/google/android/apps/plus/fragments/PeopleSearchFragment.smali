.class public Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;
.super Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;
.source "PeopleSearchFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;
.implements Lcom/google/android/apps/plus/fragments/Refreshable;
.implements Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;
.implements Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

.field private mAddToCirclesActionEnabled:Z

.field private mCircleUsageType:I

.field private mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;

.field private mPeopleInCirclesEnabled:Z

.field private mPhoneOnlyContactsEnabled:Z

.field private mPlusPagesEnabled:Z

.field private mProgressView:Landroid/widget/ProgressBar;

.field private mPublicProfileSearchEnabled:Z

.field private mQuery:Ljava/lang/String;

.field private mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;-><init>()V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mCircleUsageType:I

    .line 54
    return-void
.end method


# virtual methods
.method protected final getAdapter()Landroid/widget/ListAdapter;
    .registers 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    return-object v0
.end method

.method protected final getEmptyText()I
    .registers 2

    .prologue
    .line 300
    const/4 v0, 0x0

    return v0
.end method

.method protected final inflateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "inflater"
    .parameter "container"

    .prologue
    .line 196
    const v0, 0x7f03007c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final isEmpty()Z
    .registers 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->isSearchingForFirstResult()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method protected final isError()Z
    .registers 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->isError()Z

    move-result v0

    return v0
.end method

.method protected final isLoaded()Z
    .registers 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->isLoaded()Z

    move-result v0

    return v0
.end method

.method public final onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V
    .registers 3
    .parameter "view"
    .parameter "action"

    .prologue
    .line 328
    return-void
.end method

.method public final onAddPersonToCirclesAction(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 4
    .parameter "personId"
    .parameter "name"
    .parameter "forSharing"

    .prologue
    .line 342
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .registers 7
    .parameter "activity"

    .prologue
    const/4 v4, 0x0

    .line 147
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onAttach(Landroid/app/Activity;)V

    .line 148
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAddToCirclesActionEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setAddToCirclesActionEnabled(Z)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mPublicProfileSearchEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setPublicProfileSearchEnabled(Z)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mPhoneOnlyContactsEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePhoneNumberContacts(Z)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mCircleUsageType:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setCircleUsageType(I)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mPlusPagesEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePlusPages(Z)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mPeopleInCirclesEnabled:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePeopleInCircles(Z)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setShowProgressWhenEmpty(Z)V

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "filter_null_gaia_ids"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setFilterNullGaiaIds(Z)V

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setListener(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setQueryString(Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method public final onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "personId"
    .parameter "name"

    .prologue
    .line 352
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->showCircleMembershipDialog(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    return-void
.end method

.method public final onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .registers 4
    .parameter "circleId"
    .parameter "circle"

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;->onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V

    .line 333
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 165
    if-eqz p1, :cond_a

    .line 166
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mQuery:Ljava/lang/String;

    .line 168
    :cond_a
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 169
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 183
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 185
    .local v0, view:Landroid/view/View;
    const v1, 0x7f09004c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->createInstance(Landroid/view/View;)Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    const v2, 0x7f0801cd

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryHint(I)V

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V

    .line 190
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->updateView(Landroid/view/View;)V

    .line 191
    return-object v0
.end method

.method public final onDismissSuggestionAction(Ljava/lang/String;)V
    .registers 2
    .parameter "personId"

    .prologue
    .line 361
    return-void
.end method

.method protected final onInitLoaders(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onCreate(Landroid/os/Bundle;)V

    .line 259
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 315
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onItemClick(I)V

    .line 316
    return-void
.end method

.method public final onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .registers 5
    .parameter "personId"
    .parameter "contactLookupKey"
    .parameter "person"

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    .line 338
    return-void
.end method

.method public final onQueryClose()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryText(Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setQueryString(Ljava/lang/String;)V

    .line 286
    return-void
.end method

.method public final onQueryTextChanged(Ljava/lang/CharSequence;)V
    .registers 4
    .parameter "query"

    .prologue
    .line 213
    if-nez p1, :cond_2a

    const/4 v0, 0x0

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mQuery:Ljava/lang/String;

    .line 215
    sget-boolean v0, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v0, :cond_1e

    if-eqz p1, :cond_1e

    .line 216
    const-string v0, "*#*#dumpdb*#*#"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 217
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/DumpDatabase;->dumpNow(Landroid/content/Context;)V

    .line 223
    :cond_1e
    :goto_1e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_29

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setQueryString(Ljava/lang/String;)V

    .line 226
    :cond_29
    return-void

    .line 213
    :cond_2a
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 218
    :cond_33
    const-string v0, "*#*#cleandb*#*#"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 219
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/DumpDatabase;->cleanNow(Landroid/content/Context;)V

    goto :goto_1e
.end method

.method public final onQueryTextSubmitted(Ljava/lang/CharSequence;)V
    .registers 2
    .parameter "query"

    .prologue
    .line 230
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 173
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_c

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 177
    :cond_c
    const-string v0, "query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public final onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .registers 3
    .parameter "adapter"

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 321
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_9

    .line 322
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->updateView(Landroid/view/View;)V

    .line 324
    :cond_9
    return-void
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 201
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onStart()V

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStart()V

    .line 203
    return-void
.end method

.method public final onStop()V
    .registers 2

    .prologue
    .line 207
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsPeopleListFragment;->onStart()V

    .line 208
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStop()V

    .line 209
    return-void
.end method

.method public final onUnblockPersonAction(Ljava/lang/String;Z)V
    .registers 3
    .parameter "personId"
    .parameter "isPlusPage"

    .prologue
    .line 357
    return-void
.end method

.method public final setAddToCirclesActionEnabled(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAddToCirclesActionEnabled:Z

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_b

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setAddToCirclesActionEnabled(Z)V

    .line 93
    :cond_b
    return-void
.end method

.method public final setCircleUsageType(I)V
    .registers 3
    .parameter "usageType"

    .prologue
    .line 78
    iput p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mCircleUsageType:I

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_b

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setCircleUsageType(I)V

    .line 82
    :cond_b
    return-void
.end method

.method public final setInitialQueryString(Ljava/lang/String;)V
    .registers 3
    .parameter "query"

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mQuery:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 132
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mQuery:Ljava/lang/String;

    .line 134
    :cond_6
    return-void
.end method

.method public final setOnSelectionChangeListener(Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;

    .line 72
    return-void
.end method

.method public final setPeopleInCirclesEnabled(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mPeopleInCirclesEnabled:Z

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_b

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePeopleInCircles(Z)V

    .line 125
    :cond_b
    return-void
.end method

.method public final setPhoneOnlyContactsEnabled(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 107
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mPhoneOnlyContactsEnabled:Z

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_b

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePhoneNumberContacts(Z)V

    .line 111
    :cond_b
    return-void
.end method

.method public final setPlusPagesEnabled(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mPlusPagesEnabled:Z

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_b

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePlusPages(Z)V

    .line 118
    :cond_b
    return-void
.end method

.method public final setProgressBar(Landroid/widget/ProgressBar;)V
    .registers 3
    .parameter "progressView"

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mProgressView:Landroid/widget/ProgressBar;

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    .line 241
    return-void
.end method

.method public final setPublicProfileSearchEnabled(Z)V
    .registers 3
    .parameter "flag"

    .prologue
    .line 100
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mPublicProfileSearchEnabled:Z

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_b

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setPublicProfileSearchEnabled(Z)V

    .line 104
    :cond_b
    return-void
.end method

.method public final startSearch()V
    .registers 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    if-eqz v0, :cond_b

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setQueryText(Ljava/lang/String;)V

    .line 143
    :cond_b
    return-void
.end method

.method protected final updateView(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    .prologue
    const v4, 0x7f090163

    const v3, 0x102000a

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-nez v0, :cond_e

    .line 280
    :goto_d
    return-void

    .line 267
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->isSearchingForFirstResult()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 268
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 269
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 270
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_d

    .line 271
    :cond_28
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_42

    .line 272
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 273
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 274
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->showContent(Landroid/view/View;)V

    goto :goto_d

    .line 276
    :cond_42
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 277
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 278
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->showContent(Landroid/view/View;)V

    goto :goto_d
.end method
