.class public final Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;
.super Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
.source "PeopleSearchListAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "fragmentManager"
    .parameter "loaderManager"
    .parameter "account"

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .registers 12
    .parameter "context"
    .parameter "fragmentManager"
    .parameter "loaderManager"
    .parameter "account"
    .parameter "instanceId"

    .prologue
    .line 56
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    .line 57
    return-void
.end method


# virtual methods
.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;I)V
    .registers 27
    .parameter "view"
    .parameter "partition"
    .parameter "cursor"
    .parameter "position"

    .prologue
    .line 88
    if-eqz p3, :cond_8

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 203
    :cond_8
    :goto_8
    return-void

    .line 92
    :cond_9
    packed-switch p2, :pswitch_data_1c4

    goto :goto_8

    :pswitch_d
    move-object/from16 v2, p1

    .line 94
    check-cast v2, Lcom/google/android/apps/plus/views/CircleListItemView;

    .line 95
    .local v2, item:Lcom/google/android/apps/plus/views/CircleListItemView;
    const/4 v3, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 96
    .local v4, type:I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/CircleListItemView;->setHighlightedText(Ljava/lang/String;)V

    .line 97
    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v8, v4}, Lcom/google/android/apps/plus/util/AccountsUtil;->isRestrictedCircleForAccount(Lcom/google/android/apps/plus/content/EsAccount;I)Z

    move-result v7

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/plus/views/CircleListItemView;->setCircle(Ljava/lang/String;ILjava/lang/String;IZ)V

    goto :goto_8

    .end local v2           #item:Lcom/google/android/apps/plus/views/CircleListItemView;
    .end local v4           #type:I
    :pswitch_40
    move-object/from16 v2, p1

    .line 106
    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListItemView;

    .line 107
    .local v2, item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setHighlightedText(Ljava/lang/String;)V

    .line 108
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    .line 109
    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 110
    .local v20, personId:Ljava/lang/String;
    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPersonId(Ljava/lang/String;)V

    .line 111
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 112
    .local v15, gaiaId:Ljava/lang/String;
    const/4 v3, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 113
    .local v18, lookupKey:Ljava/lang/String;
    const/4 v3, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 114
    .local v12, avatarUrl:Ljava/lang/String;
    invoke-static {v12}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v2, v15, v0, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContactIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const/4 v3, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContactName(Ljava/lang/String;)V

    .line 117
    const/16 v3, 0xc

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 118
    .local v11, snippet:Ljava/lang/String;
    const/4 v3, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 120
    .local v6, circleIds:Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_110

    const/16 v16, 0x1

    .line 121
    .local v16, inCircles:Z
    :goto_9d
    const/16 v3, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 122
    .local v7, email:Ljava/lang/String;
    const/4 v9, 0x0

    .line 123
    .local v9, phoneNumber:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mIncludePhoneNumberContacts:Z

    if-eqz v3, :cond_b4

    .line 124
    const/16 v3, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 126
    :cond_b4
    const/16 v3, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v3, 0xb

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object v5, v2

    invoke-virtual/range {v5 .. v11}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPackedCircleIdsEmailAddressPhoneNumberAndSnippet(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mAddToCirclesActionEnabled:Z

    if-eqz v3, :cond_113

    if-nez v16, :cond_113

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_113

    const/4 v3, 0x1

    :goto_e1
    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setAddButtonVisible(Z)V

    .line 135
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mAddToCirclesActionEnabled:Z

    if-eqz v3, :cond_f5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v3, :cond_f5

    .line 136
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V

    .line 139
    :cond_f5
    const/4 v14, 0x1

    .line 140
    .local v14, firstRow:Z
    if-eqz v18, :cond_fb

    .line 143
    if-nez p4, :cond_115

    .line 144
    const/4 v14, 0x1

    .line 156
    :cond_fb
    :goto_fb
    invoke-virtual {v2, v14}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setFirstRow(Z)V

    .line 158
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, p4

    if-ne v0, v3, :cond_10b

    .line 159
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->continueLoadingPublicProfiles()V

    .line 162
    :cond_10b
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateContentDescription()V

    goto/16 :goto_8

    .line 120
    .end local v7           #email:Ljava/lang/String;
    .end local v9           #phoneNumber:Ljava/lang/String;
    .end local v14           #firstRow:Z
    .end local v16           #inCircles:Z
    :cond_110
    const/16 v16, 0x0

    goto :goto_9d

    .line 133
    .restart local v7       #email:Ljava/lang/String;
    .restart local v9       #phoneNumber:Ljava/lang/String;
    .restart local v16       #inCircles:Z
    :cond_113
    const/4 v3, 0x0

    goto :goto_e1

    .line 146
    .restart local v14       #firstRow:Z
    :cond_115
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v3

    if-eqz v3, :cond_fb

    .line 147
    const/4 v3, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 149
    .local v21, prevLookupKey:Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_12d

    .line 150
    const/4 v14, 0x0

    .line 152
    :cond_12d
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_fb

    .end local v2           #item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    .end local v6           #circleIds:Ljava/lang/String;
    .end local v7           #email:Ljava/lang/String;
    .end local v9           #phoneNumber:Ljava/lang/String;
    .end local v11           #snippet:Ljava/lang/String;
    .end local v12           #avatarUrl:Ljava/lang/String;
    .end local v14           #firstRow:Z
    .end local v15           #gaiaId:Ljava/lang/String;
    .end local v16           #inCircles:Z
    .end local v18           #lookupKey:Ljava/lang/String;
    .end local v20           #personId:Ljava/lang/String;
    .end local v21           #prevLookupKey:Ljava/lang/String;
    :pswitch_131
    move-object/from16 v2, p1

    .line 166
    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListItemView;

    .line 167
    .restart local v2       #item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setWellFormedEmail(Ljava/lang/String;)V

    .line 168
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mAddToCirclesActionEnabled:Z

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setAddButtonVisible(Z)V

    .line 169
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mAddToCirclesActionEnabled:Z

    if-eqz v3, :cond_154

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v3, :cond_154

    .line 170
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V

    .line 172
    :cond_154
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateContentDescription()V

    goto/16 :goto_8

    .end local v2           #item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    :pswitch_159
    move-object/from16 v2, p1

    .line 176
    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListItemView;

    .line 177
    .restart local v2       #item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setWellFormedSms(Ljava/lang/String;)V

    .line 178
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mAddToCirclesActionEnabled:Z

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setAddButtonVisible(Z)V

    .line 179
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mAddToCirclesActionEnabled:Z

    if-eqz v3, :cond_17c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v3, :cond_17c

    .line 180
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V

    .line 182
    :cond_17c
    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateContentDescription()V

    goto/16 :goto_8

    .line 186
    .end local v2           #item:Lcom/google/android/apps/plus/views/PeopleListItemView;
    :pswitch_181
    const/16 v17, 0x8

    .line 187
    .local v17, loadingVisibility:I
    const/16 v19, 0x8

    .line 188
    .local v19, notFoundVisibility:I
    const/16 v13, 0x8

    .line 189
    .local v13, errorVisibility:I
    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_1d2

    .line 200
    :goto_191
    const v3, 0x7f090164

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 201
    const v3, 0x7f090165

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 202
    const v3, 0x7f090166

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v13}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_8

    .line 191
    :pswitch_1bb
    const/16 v17, 0x0

    .line 192
    goto :goto_191

    .line 194
    :pswitch_1be
    const/16 v19, 0x0

    .line 195
    goto :goto_191

    .line 197
    :pswitch_1c1
    const/4 v13, 0x0

    goto :goto_191

    .line 92
    nop

    :pswitch_data_1c4
    .packed-switch 0x1
        :pswitch_d
        :pswitch_131
        :pswitch_159
        :pswitch_40
        :pswitch_181
    .end packed-switch

    .line 189
    :pswitch_data_1d2
    .packed-switch 0x1
        :pswitch_1bb
        :pswitch_1be
        :pswitch_1c1
    .end packed-switch
.end method

.method protected final newView$54126883(Landroid/content/Context;ILandroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter "context"
    .parameter "partition"
    .parameter "cursor"
    .parameter "container"

    .prologue
    .line 65
    packed-switch p2, :pswitch_data_1e

    .line 80
    const/4 v1, 0x0

    :goto_4
    return-object v1

    .line 67
    :pswitch_5
    new-instance v1, Lcom/google/android/apps/plus/views/CircleListItemView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/CircleListItemView;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 72
    :pswitch_b
    invoke-static {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->createInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/views/PeopleListItemView;

    move-result-object v1

    goto :goto_4

    .line 75
    :pswitch_10
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 76
    .local v0, inflater:Landroid/view/LayoutInflater;
    const v1, 0x7f03007d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_4

    .line 65
    nop

    :pswitch_data_1e
    .packed-switch 0x1
        :pswitch_5
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_10
    .end packed-switch
.end method

.method public final onActionButtonClick(Lcom/google/android/apps/plus/views/PeopleListItemView;I)V
    .registers 7
    .parameter "view"
    .parameter "action"

    .prologue
    .line 213
    if-nez p2, :cond_11

    .line 214
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getWellFormedEmail()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 215
    const-string v0, "add_email_dialog"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->showPersonNameDialog(Ljava/lang/String;)V

    .line 223
    :cond_11
    :goto_11
    return-void

    .line 216
    :cond_12
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getWellFormedSms()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 217
    const-string v0, "add_sms_dialog"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->showPersonNameDialog(Ljava/lang/String;)V

    goto :goto_11

    .line 219
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onAddPersonToCirclesAction(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_11
.end method
