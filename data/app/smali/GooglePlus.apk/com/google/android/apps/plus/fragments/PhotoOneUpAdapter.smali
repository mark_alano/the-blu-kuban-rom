.class public final Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "PhotoOneUpAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/SettableItemAdapter;


# instance fields
.field private mContainerHeight:I

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHeights:Landroid/util/SparseIntArray;

.field private mLeftoverPosition:I

.field private mLoading:Z

.field private final mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

.field private final mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

.field private mPhotoPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/OneUpListener;Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V
    .registers 7
    .parameter "context"
    .parameter "cursor"
    .parameter "oneUpListener"
    .parameter "onMeasuredListener"

    .prologue
    const/4 v1, -0x1

    .line 59
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 42
    iput v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mPhotoPosition:I

    .line 44
    iput v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    .line 61
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    .line 62
    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    .line 63
    return-void
.end method


# virtual methods
.method public final addFlaggedComment(Ljava/lang/String;)V
    .registers 3
    .parameter "commentId"

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 236
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->notifyDataSetChanged()V

    .line 237
    return-void
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 15
    .parameter "view"
    .parameter "context"
    .parameter "c"

    .prologue
    .line 102
    const/4 v9, 0x1

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 103
    .local v7, type:I
    packed-switch v7, :pswitch_data_108

    .line 179
    :goto_8
    :pswitch_8
    return-void

    :pswitch_9
    move-object v8, p1

    .line 105
    check-cast v8, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;

    .line 106
    .local v8, v:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V

    .line 107
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    .line 108
    const/4 v9, 0x3

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x4

    invoke-interface {p3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setOwner(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const/16 v9, 0x12

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setCaption(Ljava/lang/String;)V

    .line 111
    const/16 v9, 0xa

    invoke-interface {p3, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_3d

    .line 112
    const/16 v9, 0xa

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setDate(J)V

    .line 114
    :cond_3d
    const/16 v9, 0x13

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setPlusOne([B)V

    .line 115
    const/4 v9, 0x5

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->setAlbum(Ljava/lang/String;)V

    .line 116
    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->invalidate()V

    .line 117
    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;->requestLayout()V

    goto :goto_8

    .end local v8           #v:Lcom/google/android/apps/plus/views/PhotoOneUpInfoView;
    :pswitch_55
    move-object v8, p1

    .line 122
    check-cast v8, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;

    .line 123
    .local v8, v:Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    .line 124
    const/4 v9, 0x2

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->setCount(I)V

    .line 125
    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->invalidate()V

    .line 126
    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->requestLayout()V

    goto :goto_8

    .line 131
    .end local v8           #v:Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;
    :pswitch_6c
    const/4 v9, 0x2

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, commentId:Ljava/lang/String;
    const/16 v9, 0x8

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 133
    .local v1, dbBytes:[B
    const/4 v6, 0x0

    .line 134
    .local v6, plusOneBytes:[B
    if-eqz v1, :cond_88

    .line 136
    :try_start_7a
    invoke-static {}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPlusOneJson;

    move-result-object v9

    invoke-virtual {v9, v1}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/api/services/plusi/model/DataPlusOne;

    invoke-static {v9}, Lcom/google/android/apps/plus/content/DbPlusOneData;->serialize(Lcom/google/api/services/plusi/model/DataPlusOne;)[B
    :try_end_87
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_87} :catch_106

    move-result-object v6

    .line 142
    :cond_88
    :goto_88
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v9, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    .local v3, isFlagged:Z
    move-object v8, p1

    .line 144
    check-cast v8, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    .line 145
    .local v8, v:Lcom/google/android/apps/plus/views/StreamOneUpCommentView;
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V

    .line 146
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    .line 147
    const/4 v9, 0x3

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x4

    invoke-interface {p3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setAuthor(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const/4 v9, 0x7

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v0, v9, v3}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setComment(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 150
    invoke-virtual {v8, v6}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setPlusOne([B)V

    .line 151
    const/4 v9, 0x5

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setDate(J)V

    .line 152
    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->invalidate()V

    .line 153
    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->requestLayout()V

    goto/16 :goto_8

    .line 158
    .end local v0           #commentId:Ljava/lang/String;
    .end local v1           #dbBytes:[B
    .end local v3           #isFlagged:Z
    .end local v6           #plusOneBytes:[B
    .end local v8           #v:Lcom/google/android/apps/plus/views/StreamOneUpCommentView;
    :pswitch_c3
    const v9, 0x7f09022c

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    .line 159
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 160
    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    goto/16 :goto_8

    :pswitch_d6
    move-object v8, p1

    .line 165
    check-cast v8, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;

    .line 166
    .local v8, v:Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;
    iget v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mContainerHeight:I

    .line 167
    .local v5, leftover:I
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    if-eqz v9, :cond_fb

    .line 168
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v9}, Landroid/util/SparseIntArray;->size()I

    move-result v9

    add-int/lit8 v2, v9, -0x1

    .local v2, i:I
    :goto_e7
    if-lez v5, :cond_fb

    if-ltz v2, :cond_fb

    .line 169
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v9, v2}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v4

    .line 170
    .local v4, key:I
    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v9, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v9

    sub-int/2addr v5, v9

    .line 168
    add-int/lit8 v2, v2, -0x1

    goto :goto_e7

    .line 173
    .end local v2           #i:I
    .end local v4           #key:I
    :cond_fb
    invoke-virtual {v8, v5}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->bind(I)V

    .line 174
    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->invalidate()V

    .line 175
    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->requestLayout()V

    goto/16 :goto_8

    .end local v5           #leftover:I
    .end local v8           #v:Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;
    .restart local v0       #commentId:Ljava/lang/String;
    .restart local v1       #dbBytes:[B
    .restart local v6       #plusOneBytes:[B
    :catch_106
    move-exception v9

    goto :goto_88

    .line 103
    :pswitch_data_108
    .packed-switch 0x0
        :pswitch_9
        :pswitch_6c
        :pswitch_8
        :pswitch_c3
        :pswitch_55
        :pswitch_d6
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .registers 4
    .parameter "position"

    .prologue
    .line 72
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .registers 2

    .prologue
    .line 67
    const/4 v0, 0x6

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .parameter "context"
    .parameter "c"
    .parameter "parent"

    .prologue
    const/4 v3, 0x0

    .line 183
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 187
    .local v0, layoutInflater:Landroid/view/LayoutInflater;
    const/4 v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_42

    .line 216
    :pswitch_11
    const/4 v1, 0x0

    .line 220
    .local v1, v:Landroid/view/View;
    :goto_12
    return-object v1

    .line 189
    .end local v1           #v:Landroid/view/View;
    :pswitch_13
    const v2, 0x7f030088

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 191
    .restart local v1       #v:Landroid/view/View;
    goto :goto_12

    .line 194
    .end local v1           #v:Landroid/view/View;
    :pswitch_1b
    const v2, 0x7f0300c2

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 197
    .restart local v1       #v:Landroid/view/View;
    goto :goto_12

    .line 200
    .end local v1           #v:Landroid/view/View;
    :pswitch_23
    const v2, 0x7f0300c3

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 202
    .restart local v1       #v:Landroid/view/View;
    goto :goto_12

    .line 205
    .end local v1           #v:Landroid/view/View;
    :pswitch_2b
    const v2, 0x7f0300c6

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 207
    .restart local v1       #v:Landroid/view/View;
    goto :goto_12

    .line 210
    .end local v1           #v:Landroid/view/View;
    :pswitch_33
    const v2, 0x7f0300c5

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 211
    .restart local v1       #v:Landroid/view/View;
    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    goto :goto_12

    .line 187
    nop

    :pswitch_data_42
    .packed-switch 0x0
        :pswitch_13
        :pswitch_23
        :pswitch_11
        :pswitch_2b
        :pswitch_1b
        :pswitch_33
    .end packed-switch
.end method

.method public final removeFlaggedComment(Ljava/lang/String;)V
    .registers 3
    .parameter "commentId"

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 244
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->notifyDataSetChanged()V

    .line 245
    return-void
.end method

.method public final setContainerHeight(I)V
    .registers 2
    .parameter "height"

    .prologue
    .line 251
    iput p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mContainerHeight:I

    .line 252
    return-void
.end method

.method public final setFlaggedComments(Ljava/util/HashSet;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228
    .local p1, flaggedComments:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    .line 229
    return-void
.end method

.method public final setItemHeight(II)V
    .registers 4
    .parameter "position"
    .parameter "height"

    .prologue
    .line 256
    if-ltz p1, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    if-ne p1, v0, :cond_b

    .line 260
    :cond_a
    :goto_a
    return-void

    .line 259
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_a
.end method

.method public final setLoading(Z)V
    .registers 3
    .parameter "loading"

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLoading:Z

    if-eq p1, v0, :cond_9

    .line 274
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLoading:Z

    .line 275
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->notifyDataSetChanged()V

    .line 277
    :cond_9
    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 6
    .parameter "newCursor"

    .prologue
    const/4 v3, -0x1

    .line 77
    iput v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mPhotoPosition:I

    .line 78
    if-eqz p1, :cond_36

    .line 79
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 80
    .local v0, count:I
    new-instance v2, Landroid/util/SparseIntArray;

    invoke-direct {v2, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    .line 81
    add-int/lit8 v2, v0, -0x1

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    .line 82
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 84
    :cond_1a
    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 85
    .local v1, rowType:I
    if-nez v1, :cond_2f

    .line 86
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mPhotoPosition:I

    .line 90
    :goto_27
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 97
    .end local v0           #count:I
    .end local v1           #rowType:I
    :cond_2a
    :goto_2a
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    .line 89
    .restart local v0       #count:I
    .restart local v1       #rowType:I
    :cond_2f
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1a

    goto :goto_27

    .line 93
    .end local v0           #count:I
    .end local v1           #rowType:I
    :cond_36
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    .line 94
    iput v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->mLeftoverPosition:I

    goto :goto_2a
.end method
