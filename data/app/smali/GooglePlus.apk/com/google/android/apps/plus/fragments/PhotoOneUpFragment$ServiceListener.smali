.class final Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "PhotoOneUpFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 1748
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 1748
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 1748
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    return v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v1, 0x0

    .line 1926
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_16

    .line 2009
    :cond_15
    :goto_15
    return v1

    .line 1930
    :cond_16
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v3, 0x0

    #setter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1502(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1932
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    .line 1934
    if-eqz p2, :cond_6f

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_6f

    .line 1936
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)I

    move-result v2

    sparse-switch v2, :sswitch_data_84

    .line 1993
    const v0, 0x7f080163

    .line 1998
    .local v0, resId:I
    :goto_35
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_15

    .line 1938
    .end local v0           #resId:I
    :sswitch_43
    const v0, 0x7f080155

    .line 1939
    .restart local v0       #resId:I
    goto :goto_35

    .line 1943
    .end local v0           #resId:I
    :sswitch_47
    const v0, 0x7f080161

    .line 1944
    .restart local v0       #resId:I
    goto :goto_35

    .line 1948
    .end local v0           #resId:I
    :sswitch_4b
    const v0, 0x7f08015e

    .line 1949
    .restart local v0       #resId:I
    goto :goto_35

    .line 1953
    .end local v0           #resId:I
    :sswitch_4f
    const v0, 0x7f08014f

    .line 1954
    .restart local v0       #resId:I
    goto :goto_35

    .line 1958
    .end local v0           #resId:I
    :sswitch_53
    const v0, 0x7f080151

    .line 1959
    .restart local v0       #resId:I
    goto :goto_35

    .line 1963
    .end local v0           #resId:I
    :sswitch_57
    const v0, 0x7f080152

    .line 1964
    .restart local v0       #resId:I
    goto :goto_35

    .line 1968
    .end local v0           #resId:I
    :sswitch_5b
    const v0, 0x7f080150

    .line 1969
    .restart local v0       #resId:I
    goto :goto_35

    .line 1973
    .end local v0           #resId:I
    :sswitch_5f
    const v0, 0x7f08008a

    .line 1974
    .restart local v0       #resId:I
    goto :goto_35

    .line 1978
    .end local v0           #resId:I
    :sswitch_63
    const v0, 0x7f080093

    .line 1979
    .restart local v0       #resId:I
    goto :goto_35

    .line 1983
    .end local v0           #resId:I
    :sswitch_67
    const v0, 0x7f080093

    .line 1984
    .restart local v0       #resId:I
    goto :goto_35

    .line 1988
    .end local v0           #resId:I
    :sswitch_6b
    const v0, 0x7f080089

    .line 1989
    .restart local v0       #resId:I
    goto :goto_35

    .line 2002
    .end local v0           #resId:I
    :cond_6f
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)I

    move-result v1

    packed-switch v1, :pswitch_data_b2

    .line 2009
    :goto_78
    const/4 v1, 0x1

    goto :goto_15

    .line 2005
    :pswitch_7a
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_78

    .line 1936
    :sswitch_data_84
    .sparse-switch
        0x10 -> :sswitch_47
        0x11 -> :sswitch_4b
        0x12 -> :sswitch_43
        0x15 -> :sswitch_6b
        0x20 -> :sswitch_4f
        0x21 -> :sswitch_53
        0x22 -> :sswitch_57
        0x25 -> :sswitch_5b
        0x30 -> :sswitch_67
        0x31 -> :sswitch_5f
        0x32 -> :sswitch_63
    .end sparse-switch

    .line 2002
    :pswitch_data_b2
    .packed-switch 0x10
        :pswitch_7a
        :pswitch_7a
    .end packed-switch
.end method


# virtual methods
.method public final onCreatePhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1777
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 1779
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1781
    :cond_18
    return-void
.end method

.method public final onDeletePhotoCommentsComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1792
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1793
    return-void
.end method

.method public final onDeletePhotosComplete$5d3076b3(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "result"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1837
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_16

    .line 1855
    :cond_15
    :goto_15
    return-void

    .line 1840
    :cond_16
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v2, 0x0

    #setter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1502(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1843
    if-eqz p2, :cond_3a

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3a

    .line 1844
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    .line 1846
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1700(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080161

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_15

    .line 1850
    :cond_3a
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 1851
    .local v0, refList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1853
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/service/EsService;->deleteLocalPhotos(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    #setter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1502(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    goto :goto_15
.end method

.method public final onEditPhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1786
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1787
    return-void
.end method

.method public final onGetPhoto$4894d499(IJ)V
    .registers 6
    .parameter "requestId"
    .parameter "photoId"

    .prologue
    .line 1752
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_15

    .line 1759
    :cond_14
    :goto_14
    return-void

    .line 1755
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$902(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1757
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1000(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 1758
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    goto :goto_14
.end method

.method public final onGetPhotoSettings$6e3d3b8d(IZ)V
    .registers 5
    .parameter "requestId"
    .parameter "downloadAllowed"

    .prologue
    .line 1764
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_15

    .line 1772
    :cond_14
    :goto_14
    return-void

    .line 1767
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$902(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1769
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    #setter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1202(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 1770
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1000(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Lcom/google/android/apps/plus/views/HostActionBar;)V

    .line 1771
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    goto :goto_14
.end method

.method public final onLocalPhotoDelete(ILjava/util/ArrayList;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter
    .parameter "result"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1860
    .local p2, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v2

    if-nez v2, :cond_23

    .line 1861
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/MediaRef;

    .line 1862
    .local v1, ref:Lcom/google/android/apps/plus/api/MediaRef;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2000(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    invoke-interface {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->onPhotoRemoved$1349ef()V

    goto :goto_a

    .line 1865
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #ref:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_23
    return-void
.end method

.method public final onNameTagApprovalComplete$4894d499(IJLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "photoId"
    .parameter "result"

    .prologue
    .line 1870
    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1871
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2000(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$PhotoOneUpCallbacks;->onPhotoRemoved$1349ef()V

    .line 1873
    :cond_f
    return-void
.end method

.method public final onPhotoPlusOneComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "plusOned"
    .parameter "result"

    .prologue
    .line 1822
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_15

    .line 1832
    :cond_14
    :goto_14
    return-void

    .line 1825
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v1, 0x0

    #setter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1502(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1827
    if-eqz p3, :cond_14

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1828
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    if-eqz p2, :cond_37

    const v0, 0x7f08015b

    :goto_2e
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_14

    :cond_37
    const v0, 0x7f08015c

    goto :goto_2e
.end method

.method public final onReportPhotoCommentsComplete$141714ed(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "commentId"
    .parameter "isUndo"
    .parameter "result"

    .prologue
    .line 1798
    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1799
    if-eqz p3, :cond_12

    .line 1800
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->removeFlaggedComment(Ljava/lang/String;)V

    .line 1805
    :cond_11
    :goto_11
    return-void

    .line 1802
    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->addFlaggedComment(Ljava/lang/String;)V

    goto :goto_11
.end method

.method public final onReportPhotoComplete$4894d499(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1810
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1811
    return-void
.end method

.method public final onSavePhoto(ILjava/io/File;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 13
    .parameter "requestId"
    .parameter "saveToFile"
    .parameter "isFullRes"
    .parameter "description"
    .parameter "mimeType"
    .parameter "result"

    .prologue
    .line 1879
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, p1, :cond_15

    .line 1916
    :cond_14
    :goto_14
    return-void

    .line 1882
    :cond_15
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v4, 0x0

    #setter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1502(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1884
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    .line 1888
    if-eqz p6, :cond_7e

    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_7e

    .line 1889
    const-string v3, "StreamOneUp"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_42

    .line 1890
    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v3

    if-eqz v3, :cond_61

    .line 1891
    const-string v3, "StreamOneUp"

    const-string v4, "Could not download image"

    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1899
    :cond_42
    :goto_42
    if-eqz p3, :cond_7a

    .line 1900
    const v2, 0x7f090030

    .line 1905
    .local v2, dialogId:I
    :goto_47
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1906
    .local v1, args:Landroid/os/Bundle;
    const-string v3, "tag"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1907
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Landroid/support/v4/app/FragmentActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_14

    .line 1893
    .end local v1           #args:Landroid/os/Bundle;
    .end local v2           #dialogId:I
    :cond_61
    const-string v3, "StreamOneUp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not download image: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_42

    .line 1902
    :cond_7a
    const v2, 0x7f09002f

    .restart local v2       #dialogId:I
    goto :goto_47

    .line 1909
    .end local v2           #dialogId:I
    :cond_7e
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1910
    .local v0, activity:Landroid/app/Activity;
    if-eqz p2, :cond_91

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_91

    .line 1911
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v3, v0, p2, p4, p5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 1914
    :cond_91
    const v3, 0x7f080087

    const/4 v4, 0x1

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_14
.end method

.method public final onSetProfilePhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 1816
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 1817
    return-void
.end method
