.class final Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "PhotoViewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PhotoViewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 305
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreatePhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 5
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 309
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$000(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$000(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    :cond_18
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 314
    return-void
.end method

.method public final onDeletePhotoCommentsComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 320
    return-void
.end method

.method public final onDeletePhotosComplete$5d3076b3(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 15
    .parameter "requestId"
    .parameter "result"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 408
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_12

    .line 432
    :cond_11
    :goto_11
    return-void

    .line 412
    :cond_12
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/phone/PhotoViewActivity;

    .line 413
    .local v7, activity:Lcom/google/android/apps/plus/phone/PhotoViewActivity;
    if-eqz p2, :cond_22

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 414
    :cond_22
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->hideProgressDialog()V
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$500(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)V

    .line 416
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 417
    .local v10, res:Landroid/content/res/Resources;
    const v1, 0x7f080161

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 418
    .local v11, toastText:Ljava/lang/CharSequence;
    const/4 v1, 0x0

    invoke-static {v7, v11, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_11

    .line 421
    .end local v10           #res:Landroid/content/res/Resources;
    .end local v11           #toastText:Ljava/lang/CharSequence;
    :cond_3d
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$600(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_82

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$600(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 422
    .local v8, photoUri:Landroid/net/Uri;
    :goto_4f
    invoke-static {v8}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_84

    move-object v5, v8

    .line 424
    .local v5, localUri:Landroid/net/Uri;
    :goto_56
    if-eqz v5, :cond_86

    move-object v4, v1

    .line 425
    .local v4, remoteUrl:Ljava/lang/String;
    :goto_59
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$700(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$800(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)J

    move-result-wide v2

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 427
    .local v0, mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    new-instance v9, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 428
    .local v9, refList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v7, v9}, Lcom/google/android/apps/plus/service/EsService;->deleteLocalPhotos(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    goto :goto_11

    .end local v0           #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v4           #remoteUrl:Ljava/lang/String;
    .end local v5           #localUri:Landroid/net/Uri;
    .end local v8           #photoUri:Landroid/net/Uri;
    .end local v9           #refList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    :cond_82
    move-object v8, v1

    .line 421
    goto :goto_4f

    .restart local v8       #photoUri:Landroid/net/Uri;
    :cond_84
    move-object v5, v1

    .line 422
    goto :goto_56

    .line 424
    .restart local v5       #localUri:Landroid/net/Uri;
    :cond_86
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$600(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Ljava/lang/String;

    move-result-object v4

    goto :goto_59
.end method

.method public final onEditPhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 338
    return-void
.end method

.method public final onGetPhoto$4894d499(IJ)V
    .registers 7
    .parameter "requestId"
    .parameter "photoId"

    .prologue
    .line 365
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_11

    .line 375
    :cond_10
    :goto_10
    return-void

    .line 369
    :cond_11
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    .line 371
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 372
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_10

    .line 373
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateView(Landroid/view/View;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$200(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Landroid/view/View;)V

    goto :goto_10
.end method

.method public final onGetPhotoSettings$6e3d3b8d(IZ)V
    .registers 5
    .parameter "requestId"
    .parameter "downloadAllowed"

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_11

    .line 388
    :cond_10
    :goto_10
    return-void

    .line 384
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    .line 385
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    #setter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$302(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 387
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateMenuItems()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$400(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)V

    goto :goto_10
.end method

.method public final onLocalImageLoaded(Lcom/google/android/apps/plus/api/MediaRef;Landroid/graphics/Bitmap;II)Z
    .registers 10
    .parameter "ref"
    .parameter "bitmap"
    .parameter "width"
    .parameter "height"

    .prologue
    const/4 v4, 0x0

    .line 516
    invoke-static {}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getPhotoSize()Ljava/lang/Integer;

    move-result-object v1

    .line 517
    .local v1, size:Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne p3, v3, :cond_11

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq p4, v3, :cond_13

    :cond_11
    move v3, v4

    .line 530
    :goto_12
    return v3

    .line 521
    :cond_13
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getPhotoRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    .line 522
    .local v0, photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    if-eqz v0, :cond_3d

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3d

    .line 523
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getView()Landroid/view/View;

    move-result-object v2

    .line 524
    .local v2, view:Landroid/view/View;
    if-eqz v2, :cond_3d

    .line 525
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v3, v3, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v3, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v3, p2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->bindPhoto(Landroid/graphics/Bitmap;)V

    .line 526
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateView(Landroid/view/View;)V
    invoke-static {v3, v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$200(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Landroid/view/View;)V

    .line 527
    const/4 v3, 0x1

    goto :goto_12

    .end local v2           #view:Landroid/view/View;
    :cond_3d
    move v3, v4

    .line 530
    goto :goto_12
.end method

.method public final onLocalPhotoDelete(ILjava/util/ArrayList;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter
    .parameter "result"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ")V"
        }
    .end annotation

    .prologue
    .line 437
    .local p2, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_11

    .line 446
    :cond_10
    return-void

    .line 441
    :cond_11
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v2, p1, p3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 442
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1d
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/MediaRef;

    .line 443
    .local v1, ref:Lcom/google/android/apps/plus/api/MediaRef;
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$900(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    invoke-interface {v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->onPhotoRemoved$1349ef()V

    goto :goto_1d
.end method

.method public final onNameTagApprovalComplete$4894d499(IJLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "photoId"
    .parameter "result"

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_11

    .line 458
    :cond_10
    :goto_10
    return-void

    .line 455
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v0, p1, p4}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$900(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->onPhotoRemoved$1349ef()V

    goto :goto_10
.end method

.method public final onPhotoImageLoaded$b81653(Lcom/google/android/apps/plus/api/MediaRef;Landroid/graphics/Bitmap;I)V
    .registers 7
    .parameter "ref"
    .parameter "bitmap"
    .parameter "cropType"

    .prologue
    .line 503
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v2, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getPhotoRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v0

    .line 504
    .local v0, photoRef:Lcom/google/android/apps/plus/api/MediaRef;
    if-eqz v0, :cond_28

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 505
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 506
    .local v1, view:Landroid/view/View;
    if-eqz v1, :cond_28

    .line 507
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v2, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->bindPhoto(Landroid/graphics/Bitmap;)V

    .line 508
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateView(Landroid/view/View;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$200(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Landroid/view/View;)V

    .line 511
    .end local v1           #view:Landroid/view/View;
    :cond_28
    return-void
.end method

.method public final onPhotoPlusOneComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "plusOned"
    .parameter "result"

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_11

    .line 403
    :cond_10
    :goto_10
    return-void

    .line 398
    :cond_11
    if-eqz p3, :cond_10

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 399
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz p2, :cond_2d

    const v0, 0x7f08015b

    :goto_24
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_10

    :cond_2d
    const v0, 0x7f08015c

    goto :goto_24
.end method

.method public final onPlusOneComment$56b78e3(ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "plusOne"
    .parameter "result"

    .prologue
    .line 343
    if-eqz p2, :cond_1b

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz p1, :cond_1c

    const v0, 0x7f08015b

    :goto_13
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 348
    :cond_1b
    return-void

    .line 344
    :cond_1c
    const v0, 0x7f08015c

    goto :goto_13
.end method

.method public final onReportPhotoCommentsComplete$141714ed(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 6
    .parameter "requestId"
    .parameter "commentId"
    .parameter "isUndo"
    .parameter "result"

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v0, p1, p4}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 326
    if-eqz p3, :cond_14

    .line 327
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->removeFlaggedComment(Ljava/lang/String;)V

    .line 332
    :cond_13
    :goto_13
    return-void

    .line 329
    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->addFlaggedComment(Ljava/lang/String;)V

    goto :goto_13
.end method

.method public final onReportPhotoComplete$4894d499(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 354
    return-void
.end method

.method public final onSavePhoto(ILjava/io/File;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 14
    .parameter "requestId"
    .parameter "saveToFile"
    .parameter "isFullRes"
    .parameter "description"
    .parameter "mimeType"
    .parameter "result"

    .prologue
    .line 464
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v4, v4, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    iget-object v4, v4, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-eq v4, p1, :cond_11

    .line 499
    :cond_10
    :goto_10
    return-void

    .line 468
    :cond_11
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 470
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->hideProgressDialog()V
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$500(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)V

    .line 472
    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v4

    if-nez v4, :cond_4a

    .line 473
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 474
    .local v0, activity:Landroid/app/Activity;
    if-eqz p2, :cond_34

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_34

    .line 475
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v4, v0, p2, p4, p5}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$1000(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    :cond_34
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080087

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 479
    .local v3, toastText:Ljava/lang/String;
    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_10

    .line 481
    .end local v0           #activity:Landroid/app/Activity;
    .end local v3           #toastText:Ljava/lang/String;
    :cond_4a
    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v4

    if-eqz v4, :cond_7a

    .line 482
    const-string v4, "PhotoViewFragment"

    const-string v5, "Could not download image"

    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 489
    :goto_5b
    if-eqz p3, :cond_93

    .line 490
    const v2, 0x7f090030

    .line 495
    .local v2, dialogId:I
    :goto_60
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 496
    .local v1, args:Landroid/os/Bundle;
    const-string v4, "tag"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getTag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4, v2, v1}, Landroid/support/v4/app/FragmentActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_10

    .line 484
    .end local v1           #args:Landroid/os/Bundle;
    .end local v2           #dialogId:I
    :cond_7a
    const-string v4, "PhotoViewFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not download image: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5b

    .line 492
    :cond_93
    const v2, 0x7f09002f

    .restart local v2       #dialogId:I
    goto :goto_60
.end method

.method public final onSetProfilePhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PhotoViewFragment;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    .line 360
    return-void
.end method
