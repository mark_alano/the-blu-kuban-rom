.class public Lcom/google/android/apps/plus/fragments/PhotoViewFragment;
.super Lcom/google/android/apps/plus/fragments/EsListFragment;
.source "PhotoViewFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;
.implements Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PhotoViewFragment$MyTextWatcher;,
        Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoClickListener;,
        Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PlusOneQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoViewFragment$HorizontallySrollable;,
        Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsListFragment",
        "<",
        "Lcom/google/android/apps/plus/views/PhotoListView;",
        "Lcom/google/android/apps/plus/phone/PhotoViewAdapter;",
        ">;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;",
        "Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAlbumId:Ljava/lang/String;

.field private mAllowPlusOne:Z

.field private mAutoRefreshDone:Z

.field private mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

.field private mCommentButton:Landroid/view/View;

.field private mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mCommentView:Landroid/view/View;

.field private mDefaultAlbumName:Ljava/lang/String;

.field private mDisplayName:Ljava/lang/String;

.field private mDownloadable:Ljava/lang/Boolean;

.field private final mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mEventId:Ljava/lang/String;

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFooterView:Landroid/view/View;

.field private mForceLoadId:Ljava/lang/Long;

.field private mIsPlaceHolder:Z

.field private mOperationType:I

.field private mOwnerId:Ljava/lang/String;

.field private mPendingBytes:[B

.field private mPhotoId:J

.field private mPhotoUrl:Ljava/lang/String;

.field private mPlusOneByMe:Ljava/lang/Boolean;

.field private mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

.field private mProgressBarView:Landroid/widget/ProgressBar;

.field private mTextWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 565
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;-><init>()V

    .line 301
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mIsPlaceHolder:Z

    .line 305
    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 566
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    const v3, 0x7f080093

    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_12

    :cond_10
    move v0, v1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->hideProgressDialog()V

    if-eqz p2, :cond_87

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_87

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOperationType:I

    sparse-switch v2, :sswitch_data_8a

    const v2, 0x7f080163

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_30
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x1

    goto :goto_11

    :sswitch_3d
    const v2, 0x7f08015e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :sswitch_45
    const v2, 0x7f080161

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :sswitch_4d
    const v2, 0x7f08014f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :sswitch_55
    const v2, 0x7f080152

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :sswitch_5d
    const v2, 0x7f080151

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :sswitch_65
    const v2, 0x7f080150

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :sswitch_6d
    const v2, 0x7f08008a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :sswitch_75
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :sswitch_7a
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :sswitch_7f
    const v2, 0x7f080089

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_30

    :cond_87
    move v0, v1

    goto :goto_11

    nop

    :sswitch_data_8a
    .sparse-switch
        0x1 -> :sswitch_3d
        0x2 -> :sswitch_45
        0x14 -> :sswitch_4d
        0x15 -> :sswitch_55
        0x18 -> :sswitch_5d
        0x19 -> :sswitch_65
        0x1e -> :sswitch_6d
        0x1f -> :sswitch_75
        0x20 -> :sswitch_7a
        0x28 -> :sswitch_7f
    .end sparse-switch
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    .prologue
    .line 101
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_21

    :try_start_6
    const-string v0, "download"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object v2, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v8}, Landroid/app/DownloadManager;->addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J
    :try_end_21
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_21} :catch_3b

    :cond_21
    :goto_21
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :catch_3b
    move-exception v0

    const-string v1, "PhotoViewFragment"

    const-string v2, "Could not add photo to the Downloads application"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_21
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateMenuItems()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->hideProgressDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)J
    .registers 3
    .parameter "x0"

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    return-wide v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;
    .registers 2
    .parameter "x0"

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    return-object v0
.end method

.method private canDownload()Z
    .registers 2

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method private canTogglePlusOne()Z
    .registers 6

    .prologue
    const/4 v0, 0x0

    .line 1082
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneByMe:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 1090
    :cond_5
    :goto_5
    return v0

    .line 1085
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAlbumId:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->isPhotoPlusOnePending(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1090
    const/4 v0, 0x1

    goto :goto_5
.end method

.method private doReportComment(Ljava/lang/String;ZZ)V
    .registers 9
    .parameter "commentId"
    .parameter "remove"
    .parameter "isUndo"

    .prologue
    .line 1708
    if-eqz p3, :cond_4d

    const v3, 0x7f0803d3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1711
    .local v2, title:Ljava/lang/String;
    :goto_9
    if-eqz p3, :cond_55

    const v3, 0x7f0803d4

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1714
    .local v1, question:Ljava/lang/String;
    :goto_12
    const v3, 0x7f0801c4

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0801c5

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v1, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 1716
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "comment_id"

    invoke-virtual {v3, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1717
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "delete"

    invoke-virtual {v3, v4, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1718
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "is_undo"

    invoke-virtual {v3, v4, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1719
    const/4 v3, 0x0

    invoke-virtual {v0, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1720
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "report_comment"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1721
    return-void

    .line 1708
    .end local v0           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    .end local v1           #question:Ljava/lang/String;
    .end local v2           #title:Ljava/lang/String;
    :cond_4d
    const v3, 0x7f0803d0

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_9

    .line 1711
    .restart local v2       #title:Ljava/lang/String;
    :cond_55
    const v3, 0x7f0803d1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_12
.end method

.method private hasPlusOned()Z
    .registers 2

    .prologue
    .line 1074
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneByMe:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    :goto_5
    return v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneByMe:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_5
.end method

.method private hideProgressDialog()V
    .registers 3

    .prologue
    .line 1814
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1815
    .local v0, activity:Landroid/app/Activity;
    if-eqz v0, :cond_c

    .line 1816
    const v1, 0x7f09002e

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dismissDialog(I)V

    .line 1818
    :cond_c
    return-void
.end method

.method private setViewVisibility(Z)V
    .registers 14
    .parameter "animate"

    .prologue
    const/16 v11, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1545
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v4, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentFullScreen(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    .line 1546
    .local v0, fullScreen:Z
    if-nez v0, :cond_14

    iget-wide v7, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    const-wide/16 v9, 0x0

    cmp-long v4, v7, v9

    if-nez v4, :cond_48

    :cond_14
    move v1, v6

    .line 1547
    .local v1, hide:Z
    :goto_15
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v4}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isShowPhotoOnly()Z

    move-result v3

    .line 1548
    .local v3, photoOnly:Z
    if-nez v1, :cond_29

    if-nez v3, :cond_29

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v4, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->allowComments()Z

    move-result v4

    if-nez v4, :cond_4a

    :cond_29
    move v2, v6

    .line 1550
    .local v2, hideFooter:Z
    :goto_2a
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v4, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setFullScreen$25decb5(Z)V

    .line 1551
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v4, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setShowPhotoOnly(Z)V

    .line 1552
    if-eqz v2, :cond_4c

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eq v4, v11, :cond_47

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentView:Landroid/view/View;

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    .line 1553
    :cond_47
    :goto_47
    return-void

    .end local v1           #hide:Z
    .end local v2           #hideFooter:Z
    .end local v3           #photoOnly:Z
    :cond_48
    move v1, v5

    .line 1546
    goto :goto_15

    .restart local v1       #hide:Z
    .restart local v3       #photoOnly:Z
    :cond_4a
    move v2, v5

    .line 1548
    goto :goto_2a

    .line 1552
    .restart local v2       #hideFooter:Z
    :cond_4c
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_47

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentView:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_47
.end method

.method private static setVisible(Landroid/view/Menu;IZ)V
    .registers 4
    .parameter "menu"
    .parameter "menuItemId"
    .parameter "visible"

    .prologue
    .line 1735
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1736
    .local v0, item:Landroid/view/MenuItem;
    if-eqz v0, :cond_9

    .line 1737
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1739
    :cond_9
    return-void
.end method

.method private showProgressDialog(I)V
    .registers 3
    .parameter "operationType"

    .prologue
    .line 1788
    const v0, 0x7f080164

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(ILjava/lang/String;)V

    .line 1789
    return-void
.end method

.method private showProgressDialog(ILjava/lang/String;)V
    .registers 6
    .parameter "operationType"
    .parameter "progressText"

    .prologue
    .line 1798
    iput p1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOperationType:I

    .line 1800
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1801
    .local v1, args:Landroid/os/Bundle;
    const-string v2, "dialog_message"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1803
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1804
    .local v0, activity:Landroid/app/Activity;
    if-eqz v0, :cond_18

    .line 1805
    const v2, 0x7f09002e

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    .line 1807
    :cond_18
    return-void
.end method

.method private updateMenuItems()V
    .registers 2

    .prologue
    .line 1743
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    if-eqz v0, :cond_9

    .line 1744
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->updateMenuItems()V

    .line 1746
    :cond_9
    return-void
.end method

.method private updateView(Landroid/view/View;)V
    .registers 5
    .parameter "view"

    .prologue
    .line 1753
    if-eqz p1, :cond_b

    const v2, 0x1020004

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_c

    .line 1774
    :cond_b
    :goto_b
    return-void

    .line 1757
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v2, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->isPhotoBound()Z

    move-result v0

    .line 1758
    .local v0, hasImage:Z
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v2, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->isPhotoLoading()Z

    move-result v1

    .line 1760
    .local v1, imageLoading:Z
    if-eqz v1, :cond_27

    .line 1761
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showEmptyViewProgress(Landroid/view/View;)V

    .line 1773
    :goto_21
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mProgressBarView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    goto :goto_b

    .line 1763
    :cond_27
    if-eqz v0, :cond_2d

    .line 1764
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showContent(Landroid/view/View;)V

    goto :goto_21

    .line 1765
    :cond_2d
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mIsPlaceHolder:Z

    if-eqz v2, :cond_3b

    .line 1766
    const v2, 0x7f08007d

    invoke-static {p1, v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 1767
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_21

    .line 1769
    :cond_3b
    const v2, 0x7f080071

    invoke-static {p1, v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    .line 1770
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_21
.end method


# virtual methods
.method public final downloadPhoto(Landroid/content/Context;Z)V
    .registers 14
    .parameter "context"
    .parameter "fullRes"

    .prologue
    const/16 v6, 0x800

    .line 1403
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    if-nez v5, :cond_7

    .line 1443
    :goto_6
    return-void

    .line 1407
    :cond_7
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v5, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getPhotoRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v3

    .line 1408
    .local v3, mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v5, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getAlbumName()Ljava/lang/String;

    move-result-object v0

    .line 1411
    .local v0, albumName:Ljava/lang/String;
    iget-wide v7, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    const-wide/16 v9, 0x0

    cmp-long v5, v7, v9

    if-nez v5, :cond_6b

    .line 1412
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    .line 1421
    .local v2, imageUrl:Ljava/lang/String;
    :goto_21
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7a

    .line 1422
    if-eqz p2, :cond_74

    .line 1423
    const-string v5, "d"

    invoke-static {v5, v2}, Lcom/google/android/apps/plus/phone/FIFEUtil;->setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1432
    .local v1, downloadUrl:Ljava/lang/String;
    :goto_33
    if-eqz v1, :cond_84

    .line 1433
    const-string v5, "PhotoViewFragment"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_52

    .line 1434
    const-string v5, "PhotoViewFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Downloading image from: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1437
    :cond_52
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p1, v5, v1, p2, v0}, Lcom/google/android/apps/plus/service/EsService;->savePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 1438
    const/16 v5, 0x2a

    const v6, 0x7f080084

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(ILjava/lang/String;)V

    goto :goto_6

    .line 1414
    .end local v1           #downloadUrl:Ljava/lang/String;
    .end local v2           #imageUrl:Ljava/lang/String;
    :cond_6b
    if-nez v3, :cond_6f

    const/4 v2, 0x0

    goto :goto_21

    :cond_6f
    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v2

    goto :goto_21

    .line 1425
    .restart local v2       #imageUrl:Ljava/lang/String;
    :cond_74
    const/4 v5, 0x0

    invoke-static {v6, v2, v5}, Lcom/google/android/apps/plus/phone/FIFEUtil;->setImageUrlSize(ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .restart local v1       #downloadUrl:Ljava/lang/String;
    goto :goto_33

    .line 1428
    .end local v1           #downloadUrl:Ljava/lang/String;
    :cond_7a
    if-eqz p2, :cond_82

    const/4 v5, -0x1

    :goto_7d
    invoke-static {v5, v2}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->setImageUrlSize(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .restart local v1       #downloadUrl:Ljava/lang/String;
    goto :goto_33

    .end local v1           #downloadUrl:Ljava/lang/String;
    :cond_82
    move v5, v6

    goto :goto_7d

    .line 1440
    .restart local v1       #downloadUrl:Ljava/lang/String;
    :cond_84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080086

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1441
    .local v4, toastText:Ljava/lang/String;
    const/4 v5, 0x1

    invoke-static {p1, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_6
.end method

.method protected final isEmpty()Z
    .registers 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1063
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 1064
    .local v1, view:Landroid/view/View;
    if-eqz v1, :cond_20

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_20

    move v0, v3

    .line 1067
    .local v0, isViewAvailable:Z
    :goto_12
    if-eqz v0, :cond_22

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v2, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->isPhotoBound()Z

    move-result v2

    if-nez v2, :cond_22

    move v2, v3

    :goto_1f
    return v2

    .end local v0           #isViewAvailable:Z
    :cond_20
    move v0, v4

    .line 1064
    goto :goto_12

    .restart local v0       #isViewAvailable:Z
    :cond_22
    move v2, v4

    .line 1067
    goto :goto_1f
.end method

.method public final onActionBarHeightCalculated(I)V
    .registers 3
    .parameter "actionBarHeight"

    .prologue
    .line 1058
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setActionBarHeight(I)V

    .line 1059
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 1475
    packed-switch p1, :pswitch_data_10

    .line 1487
    :cond_3
    :goto_3
    return-void

    .line 1477
    :pswitch_4
    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 1478
    const-string v0, "data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPendingBytes:[B

    goto :goto_3

    .line 1475
    :pswitch_data_10
    .packed-switch 0x1
        :pswitch_4
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .registers 4
    .parameter "activity"

    .prologue
    .line 570
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onAttach(Landroid/app/Activity;)V

    .line 571
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    if-eqz v0, :cond_c

    .line 572
    check-cast p1, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    return-void

    .line 574
    .restart local p1
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity must implement PhotoViewCallbacks"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 18
    .parameter "v"

    .prologue
    .line 911
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_156

    .line 951
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->isPhotoBound()Z

    move-result v1

    if-nez v1, :cond_f6

    .line 954
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    move-object/from16 v0, p0

    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentFullScreen(Landroid/support/v4/app/Fragment;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 970
    :cond_1f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->toggleFullScreen()V

    .line 975
    :cond_26
    :goto_26
    return-void

    .line 913
    :sswitch_27
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v12

    .line 914
    .local v12, comment:Landroid/text/Editable;
    invoke-interface {v12}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_79

    .line 915
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v12}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v13

    .line 917
    .local v13, commentText:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;->newBuilder()Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;->setFocusObfuscatedOwnerId(Ljava/lang/String;)Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-virtual {v3, v6, v7}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;->setPhotoId(J)Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;

    invoke-virtual {v3}, Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId$Builder;->build()Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;

    move-result-object v3

    invoke-static {v1, v2, v3, v13}, Lcom/google/android/apps/plus/service/EsService;->createPhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/tacotruck/proto/Network$MediaReference$PicasaPhotoId;Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 920
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentView:Landroid/view/View;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 922
    const/16 v1, 0x14

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(I)V

    goto :goto_26

    .line 924
    .end local v13           #commentText:Ljava/lang/String;
    :cond_79
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentButton:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_26

    .line 930
    .end local v12           #comment:Landroid/text/Editable;
    :sswitch_82
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->toggleTags()V

    goto :goto_26

    .line 935
    :sswitch_8c
    const v1, 0x7f09003c

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 936
    .local v5, shapeId:Ljava/lang/Long;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v6, 0x1

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/service/EsService;->nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 938
    const/16 v1, 0x1e

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(I)V

    goto/16 :goto_26

    .line 943
    .end local v5           #shapeId:Ljava/lang/Long;
    :sswitch_c1
    const v1, 0x7f09003c

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 944
    .restart local v5       #shapeId:Ljava/lang/Long;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/service/EsService;->nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 946
    const/16 v1, 0x1f

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(I)V

    goto/16 :goto_26

    .line 960
    .end local v5           #shapeId:Ljava/lang/Long;
    :cond_f6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->isVideo()Z

    move-result v1

    if-eqz v1, :cond_1f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    move-object/from16 v0, p0

    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentFullScreen(Landroid/support/v4/app/Fragment;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 961
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->isVideoReady()Z

    move-result v1

    if-eqz v1, :cond_13f

    .line 962
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v1, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getVideoData()[B

    move-result-object v11

    invoke-static/range {v6 .. v11}, Lcom/google/android/apps/plus/phone/Intents;->getVideoViewActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J[B)Landroid/content/Intent;

    move-result-object v14

    .line 964
    .local v14, startIntent:Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_26

    .line 966
    .end local v14           #startIntent:Landroid/content/Intent;
    :cond_13f
    const v1, 0x7f08007c

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 967
    .local v15, toastText:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v15, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_26

    .line 911
    :sswitch_data_156
    .sparse-switch
        0x7f0900aa -> :sswitch_27
        0x7f090174 -> :sswitch_82
        0x7f09018f -> :sswitch_8c
        0x7f090190 -> :sswitch_c1
    .end sparse-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "savedInstanceState"

    .prologue
    const-wide/16 v5, 0x0

    const/4 v4, 0x1

    .line 586
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 588
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mFlaggedComments:Ljava/util/HashSet;

    .line 590
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 591
    .local v0, args:Landroid/os/Bundle;
    if-eqz p1, :cond_92

    .line 592
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.FORCE_LOAD"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 593
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.FORCE_LOAD"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mForceLoadId:Ljava/lang/Long;

    .line 595
    :cond_27
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.DOWNLOADABLE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 596
    const-string v2, "com.google.android.apps.plus.PhotoViewFragment.DOWNLOADABLE"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;

    .line 598
    :cond_3b
    const-string v2, "flagged_comments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 599
    .local v1, ids:[Ljava/lang/String;
    if-eqz v1, :cond_4c

    .line 600
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 608
    .end local v1           #ids:[Ljava/lang/String;
    :cond_4c
    :goto_4c
    const-string v2, "account"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 609
    const-string v2, "photo_id"

    invoke-virtual {v0, v2, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    .line 610
    const-string v2, "owner_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    .line 611
    const-string v2, "event_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mEventId:Ljava/lang/String;

    .line 612
    const-string v2, "photo_url"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    .line 613
    const-string v2, "display_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDisplayName:Ljava/lang/String;

    .line 614
    const-string v2, "album_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDefaultAlbumName:Ljava/lang/String;

    .line 615
    const-string v2, "allow_plusone"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAllowPlusOne:Z

    .line 617
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setHasOptionsMenu(Z)V

    .line 618
    return-void

    .line 603
    :cond_92
    const-string v2, "force_load_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4c

    .line 604
    const-string v2, "force_load_id"

    invoke-virtual {v0, v2, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mForceLoadId:Ljava/lang/Long;

    goto :goto_4c
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 11
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 764
    packed-switch p1, :pswitch_data_54

    move-object v0, v4

    .line 787
    :goto_5
    return-object v0

    .line 766
    :pswitch_6
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoViewLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mForceLoadId:Ljava/lang/Long;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/PhotoViewLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;Ljava/lang/Long;)V

    goto :goto_5

    .line 771
    :pswitch_1a
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-wide v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v0, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 774
    .local v2, queryUri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoTagScroller$PhotoShapeQuery;->PROJECTION:[Ljava/lang/String;

    const-string v6, "shape_id"

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 779
    .end local v2           #queryUri:Landroid/net/Uri;
    :pswitch_37
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-wide v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v0, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 782
    .restart local v2       #queryUri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PlusOneQuery;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 764
    nop

    :pswitch_data_54
    .packed-switch 0x7f09002b
        :pswitch_6
        :pswitch_37
        :pswitch_1a
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 7
    .parameter "menu"
    .parameter "inflater"

    .prologue
    .line 1144
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 1159
    :cond_8
    :goto_8
    return-void

    .line 1148
    :cond_9
    const v2, 0x7f100017

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1149
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_8

    .line 1153
    const v2, 0x7f090291

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 1155
    .local v0, barLayout:Landroid/view/View;
    const v2, 0x7f09004a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 1157
    .local v1, progressBarView:Landroid/widget/ProgressBar;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->onUpdateProgressView(Landroid/widget/ProgressBar;)V

    goto :goto_8
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 16
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 623
    const v0, 0x7f030081

    invoke-super {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v11

    .line 627
    .local v11, view:Landroid/view/View;
    const v0, 0x7f030017

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mFooterView:Landroid/view/View;

    .line 629
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    new-instance v6, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoClickListener;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v6, v5, v7}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoClickListener;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDefaultAlbumName:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mEventId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mFooterView:Landroid/view/View;

    move-object v5, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    .line 632
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setFlaggedComments(Ljava/util/HashSet;)V

    .line 635
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 636
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 637
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 640
    const v0, 0x7f09011b

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentView:Landroid/view/View;

    .line 642
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentView:Landroid/view/View;

    const v1, 0x7f0900aa

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentButton:Landroid/view/View;

    .line 643
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentView:Landroid/view/View;

    const v1, 0x7f0900a9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 647
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    .line 648
    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$MyTextWatcher;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentButton:Landroid/view/View;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$MyTextWatcher;-><init>(Landroid/view/View;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mTextWatcher:Landroid/text/TextWatcher;

    .line 649
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 650
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v1, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 664
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v0

    if-lez v0, :cond_df

    const/4 v0, 0x1

    :goto_b0
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 666
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mListView:Landroid/widget/AbsListView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ViewUtils;->getListenerForSwitchToList(Landroid/widget/AbsListView;)Landroid/view/View$OnFocusChangeListener;

    move-result-object v10

    .line 668
    .local v10, switchToListListener:Landroid/view/View$OnFocusChangeListener;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 672
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setViewVisibility(Z)V

    .line 674
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_de

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    if-eqz v0, :cond_de

    .line 675
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->getActionBarHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->setActionBarHeight(I)V

    .line 678
    :cond_de
    return-object v11

    .line 664
    .end local v10           #switchToListListener:Landroid/view/View$OnFocusChangeListener;
    :cond_df
    const/4 v0, 0x0

    goto :goto_b0
.end method

.method public final bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 101
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onDestroyView()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 730
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mProgressBarView:Landroid/widget/ProgressBar;

    .line 731
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentView:Landroid/view/View;

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 733
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 734
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->destroy()V

    .line 735
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 736
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 737
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCommentButton:Landroid/view/View;

    .line 738
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mIsPlaceHolder:Z

    .line 739
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 740
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->clear()V

    .line 742
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onDestroyView()V

    .line 743
    return-void
.end method

.method public final onDetach()V
    .registers 2

    .prologue
    .line 580
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    .line 581
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onDetach()V

    .line 582
    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 1654
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 11
    .parameter "which"
    .parameter "args"

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1658
    const-string v0, "comment_action"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 1659
    .local v7, mCommentAction:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-nez v7, :cond_12

    .line 1660
    const-string v0, "PhotoViewFragment"

    const-string v1, "No actions for comment option dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1688
    :goto_11
    return-void

    .line 1663
    :cond_12
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_20

    .line 1664
    const-string v0, "PhotoViewFragment"

    const-string v1, "Option selected outside the action list"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_11

    .line 1668
    :cond_20
    const-string v0, "comment_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1669
    .local v3, commentId:Ljava/lang/String;
    const-string v0, "comment_content"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1671
    .local v4, commentContent:Ljava/lang/String;
    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_94

    goto :goto_11

    .line 1676
    :pswitch_3a
    invoke-direct {p0, v3, v6, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->doReportComment(Ljava/lang/String;ZZ)V

    goto :goto_11

    .line 1673
    :pswitch_3e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    iget-wide v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/Intents;->getEditCommentActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_11

    .line 1679
    :pswitch_55
    invoke-direct {p0, v3, v1, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->doReportComment(Ljava/lang/String;ZZ)V

    goto :goto_11

    .line 1682
    :pswitch_59
    invoke-direct {p0, v3, v6, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->doReportComment(Ljava/lang/String;ZZ)V

    goto :goto_11

    .line 1685
    :pswitch_5d
    const v0, 0x7f08012b

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f080134

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0801c4

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v5, 0x7f0801c5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1, v2, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_id"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p0, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "remove_comment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 1671
    :pswitch_data_94
    .packed-switch 0x15
        :pswitch_3a
        :pswitch_55
        :pswitch_59
        :pswitch_5d
        :pswitch_3e
    .end packed-switch
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1650
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 13
    .parameter "args"
    .parameter "tag"

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 1615
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 1616
    .local v7, photoIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Long;>;"
    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1617
    const-string v0, "remove_dialog"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1618
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    .line 1619
    .local v6, context:Landroid/content/Context;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    invoke-static {v6, v0, v1, v7}, Lcom/google/android/apps/plus/service/EsService;->deletePhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 1620
    const/4 v0, 0x2

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0005

    invoke-virtual {v1, v2, v8}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(ILjava/lang/String;)V

    .line 1646
    .end local v6           #context:Landroid/content/Context;
    :cond_39
    :goto_39
    return-void

    .line 1623
    :cond_3a
    const-string v0, "report_dialog"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 1624
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v5}, Lcom/google/android/apps/plus/service/EsService;->reportPhotoAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 1625
    invoke-direct {p0, v8}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(I)V

    goto :goto_39

    .line 1627
    :cond_5a
    const-string v0, "remove_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_84

    .line 1628
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "comment_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 1630
    const/16 v0, 0x18

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(I)V

    goto :goto_39

    .line 1632
    :cond_84
    const-string v0, "report_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ba

    .line 1633
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "comment_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "delete"

    invoke-virtual {p1, v8, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string v8, "is_undo"

    invoke-virtual {p1, v8, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->reportPhotoComment$3486cdbb(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;ZZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 1636
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(I)V

    goto :goto_39

    .line 1638
    :cond_ba
    const-string v0, "remove_tag"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_39

    .line 1639
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getMyApprovedShapeId()Ljava/lang/Long;

    move-result-object v4

    .line 1640
    .local v4, shapeId:Ljava/lang/Long;
    if-eqz v4, :cond_39

    .line 1641
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    iget-wide v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 1643
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(I)V

    goto/16 :goto_39
.end method

.method public final onFullScreenChanged$25decb5()V
    .registers 2

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    .line 1016
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setViewVisibility(Z)V

    .line 1019
    return-void
.end method

.method public onGlobalLayout()V
    .registers 3

    .prologue
    .line 1005
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 1006
    .local v0, view:Landroid/view/View;
    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1b

    .line 1007
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateView(Landroid/view/View;)V

    .line 1008
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v1, Lcom/google/android/apps/plus/views/PhotoListView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1010
    :cond_1b
    return-void
.end method

.method public final onInterceptMoveLeft(FF)Z
    .registers 4
    .parameter "origX"
    .parameter "origY"

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1040
    const/4 v0, 0x0

    .line 1043
    :goto_9
    return v0

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->interceptMoveLeft(FF)Z

    move-result v0

    goto :goto_9
.end method

.method public final onInterceptMoveRight(FF)Z
    .registers 4
    .parameter "origX"
    .parameter "origY"

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1050
    const/4 v0, 0x0

    .line 1053
    :goto_9
    return v0

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->interceptMoveRight(FF)Z

    move-result v0

    goto :goto_9
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x5

    .line 1557
    instance-of v0, p2, Lcom/google/android/apps/plus/views/CommentRowView;

    if-eqz v0, :cond_dc

    .line 1558
    check-cast p2, Lcom/google/android/apps/plus/views/CommentRowView;

    .end local p2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/CommentRowView;->getAuthorId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/CommentRowView;->isFlagged()Z

    move-result v5

    if-eqz v1, :cond_a4

    const v5, 0x7f0803d6

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v5, 0x19

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_48
    if-nez v2, :cond_4c

    if-eqz v1, :cond_5f

    :cond_4c
    const v1, 0x7f0803d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v0, 0x18

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5f
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const v1, 0x7f0803cc

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_action"

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_id"

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/CommentRowView;->getCommentId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "comment_content"

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/CommentRowView;->getCommentContent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "remove_comment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1564
    :cond_a3
    :goto_a3
    return-void

    .line 1558
    :cond_a4
    if-eqz v5, :cond_ba

    const v5, 0x7f0803d2

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v5, 0x17

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_48

    :cond_ba
    const v5, 0x7f0803cf

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v2, :cond_d1

    const/16 v5, 0x16

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_48

    :cond_d1
    const/16 v5, 0x15

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_48

    .line 1560
    .restart local p2
    :cond_dc
    const-string v0, "PhotoViewFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a3

    .line 1561
    const-string v0, "PhotoViewFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "HostedStreamOneUpFragment.onItemClick: Some other view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a3
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 11
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/16 v3, 0xd

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_13

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_178

    :cond_13
    :goto_13
    return-void

    :pswitch_14
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-eqz p2, :cond_20

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2b

    :cond_20
    const v0, 0x7f080081

    invoke-static {v5, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_13

    :cond_2b
    if-eqz p2, :cond_a5

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_a5

    invoke-interface {p2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_d3

    move-object v0, v4

    :goto_3a
    if-eqz v0, :cond_174

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataVideo;

    const-string v3, "FINAL"

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d9

    move v0, v1

    :goto_51
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;

    if-nez v3, :cond_60

    const/16 v3, 0x11

    invoke-interface {p2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_dc

    move-object v3, v4

    :goto_5e
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;

    :cond_60
    const/16 v3, 0x10

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x7

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "PLACEHOLDER"

    invoke-static {v7, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_79

    if-nez v0, :cond_79

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;

    if-nez v0, :cond_ed

    :cond_79
    const-string v0, "profile"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_ed

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAutoRefreshDone:Z

    if-nez v0, :cond_ed

    move v0, v1

    :goto_86
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAutoRefreshDone:Z

    if-eqz v0, :cond_ff

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    if-nez v0, :cond_ff

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-nez v0, :cond_ef

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    invoke-static {v5, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->getPhotoSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    move v1, v3

    :cond_a5
    :goto_a5
    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mForceLoadId:Ljava/lang/Long;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->notifyDataSetChanged()V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mIsPlaceHolder:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateMenuItems()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f09002d

    invoke-virtual {v0, v1, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f09002c

    invoke-virtual {v0, v1, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setViewVisibility(Z)V

    goto/16 :goto_13

    :cond_d3
    invoke-interface {p2, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    goto/16 :goto_3a

    :cond_d9
    move v0, v2

    goto/16 :goto_51

    :cond_dc
    const/16 v3, 0x11

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v1, :cond_eb

    move v3, v1

    :goto_e5
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto/16 :goto_5e

    :cond_eb
    move v3, v2

    goto :goto_e5

    :cond_ed
    move v0, v2

    goto :goto_86

    :cond_ef
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v5, v0, v1, v6, v7}, Lcom/google/android/apps/plus/service/EsService;->getPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    :cond_ff
    move v1, v3

    goto :goto_a5

    :pswitch_101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->bindScroller(Landroid/content/Context;Landroid/database/Cursor;)V

    goto/16 :goto_13

    :pswitch_10e
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneByMe:Ljava/lang/Boolean;

    if-eqz p2, :cond_151

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_151

    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_161

    :goto_11e
    invoke-interface {p2, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_166

    move v0, v2

    :goto_125
    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAlbumId:Ljava/lang/String;

    if-lez v0, :cond_16d

    if-eqz v4, :cond_16d

    invoke-static {}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->getInstance()Lcom/google/api/services/plusi/model/DataPlusOneJson;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/api/services/plusi/model/DataPlusOneJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataPlusOne;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    if-eqz v0, :cond_16b

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneData:Lcom/google/api/services/plusi/model/DataPlusOne;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_16b

    :goto_14b
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneByMe:Ljava/lang/Boolean;

    :cond_151
    :goto_151
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneByMe:Ljava/lang/Boolean;

    if-eq v3, v0, :cond_13

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateMenuItems()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateView(Landroid/view/View;)V

    goto/16 :goto_13

    :cond_161
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    goto :goto_11e

    :cond_166
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_125

    :cond_16b
    move v1, v2

    goto :goto_14b

    :cond_16d
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPlusOneByMe:Ljava/lang/Boolean;

    goto :goto_151

    :cond_174
    move v0, v2

    goto/16 :goto_51

    nop

    :pswitch_data_178
    .packed-switch 0x7f09002b
        :pswitch_14
        :pswitch_10e
        :pswitch_101
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 907
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 31
    .parameter "item"

    .prologue
    .line 1208
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    move-object/from16 v0, p0

    invoke-interface {v5, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v5

    if-nez v5, :cond_e

    .line 1209
    const/4 v5, 0x0

    .line 1375
    :goto_d
    return v5

    .line 1212
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v17

    .line 1214
    .local v17, activity:Landroid/app/Activity;
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    sparse-switch v5, :sswitch_data_2f2

    .line 1375
    const/4 v5, 0x0

    goto :goto_d

    .line 1216
    :sswitch_1b
    check-cast v17, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    .end local v17           #activity:Landroid/app/Activity;
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onTitlebarLabelClick()V

    .line 1217
    const/4 v5, 0x1

    goto :goto_d

    .line 1221
    .restart local v17       #activity:Landroid/app/Activity;
    :sswitch_22
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->canTogglePlusOne()Z

    move-result v5

    if-eqz v5, :cond_40

    .line 1222
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAlbumId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    const/4 v10, 0x1

    invoke-static/range {v4 .. v10}, Lcom/google/android/apps/plus/service/EsService;->photoPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JZ)I

    .line 1225
    :cond_40
    const/4 v5, 0x1

    goto :goto_d

    .line 1229
    :sswitch_42
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->canTogglePlusOne()Z

    move-result v5

    if-eqz v5, :cond_60

    .line 1230
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAlbumId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    const/4 v10, 0x0

    invoke-static/range {v4 .. v10}, Lcom/google/android/apps/plus/service/EsService;->photoPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JZ)I

    .line 1233
    :cond_60
    const/4 v5, 0x1

    goto :goto_d

    .line 1237
    :sswitch_62
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    if-eqz v5, :cond_d8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    .line 1238
    .local v24, photoUri:Landroid/net/Uri;
    :goto_70
    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_db

    move-object/from16 v9, v24

    .line 1239
    .local v9, localUri:Landroid/net/Uri;
    :goto_78
    if-eqz v9, :cond_dd

    const/4 v8, 0x0

    .line 1240
    .local v8, remoteUrl:Ljava/lang/String;
    :goto_7b
    new-instance v4, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    sget-object v10, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v4 .. v10}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 1242
    .local v4, ref:Lcom/google/android/apps/plus/api/MediaRef;
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 1243
    .local v25, refList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1245
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v19

    .line 1246
    .local v19, context:Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v19

    move-object/from16 v1, v25

    invoke-static {v0, v5, v1}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v22

    .line 1247
    .local v22, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->startActivity(Landroid/content/Intent;)V

    .line 1249
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-eqz v5, :cond_e4

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    if-eqz v5, :cond_e4

    const-string v6, "notif_id"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e2

    const/4 v5, 0x1

    :goto_c4
    if-eqz v5, :cond_d5

    .line 1250
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->SHARE_INSTANT_UPLOAD_FROM_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v7

    move-object/from16 v0, v19

    invoke-static {v0, v5, v6, v7}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1254
    :cond_d5
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1237
    .end local v4           #ref:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v8           #remoteUrl:Ljava/lang/String;
    .end local v9           #localUri:Landroid/net/Uri;
    .end local v19           #context:Landroid/content/Context;
    .end local v22           #intent:Landroid/content/Intent;
    .end local v24           #photoUri:Landroid/net/Uri;
    .end local v25           #refList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    :cond_d8
    const/16 v24, 0x0

    goto :goto_70

    .line 1238
    .restart local v24       #photoUri:Landroid/net/Uri;
    :cond_db
    const/4 v9, 0x0

    goto :goto_78

    .line 1239
    .restart local v9       #localUri:Landroid/net/Uri;
    :cond_dd
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    goto :goto_7b

    .line 1249
    .restart local v4       #ref:Lcom/google/android/apps/plus/api/MediaRef;
    .restart local v8       #remoteUrl:Ljava/lang/String;
    .restart local v19       #context:Landroid/content/Context;
    .restart local v22       #intent:Landroid/content/Intent;
    .restart local v25       #refList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    :cond_e2
    const/4 v5, 0x0

    goto :goto_c4

    :cond_e4
    const/4 v5, 0x0

    goto :goto_c4

    .line 1258
    .end local v4           #ref:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v8           #remoteUrl:Ljava/lang/String;
    .end local v9           #localUri:Landroid/net/Uri;
    .end local v19           #context:Landroid/content/Context;
    .end local v22           #intent:Landroid/content/Intent;
    .end local v24           #photoUri:Landroid/net/Uri;
    .end local v25           #refList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    :sswitch_e6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    if-eqz v5, :cond_12a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    .line 1259
    .restart local v24       #photoUri:Landroid/net/Uri;
    :goto_f4
    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_12d

    move-object/from16 v9, v24

    .line 1260
    .restart local v9       #localUri:Landroid/net/Uri;
    :goto_fc
    if-eqz v9, :cond_12f

    const/4 v8, 0x0

    .line 1261
    .restart local v8       #remoteUrl:Ljava/lang/String;
    :goto_ff
    new-instance v10, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    sget-object v16, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object v14, v8

    move-object v15, v9

    invoke-direct/range {v10 .. v16}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 1263
    .local v10, mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDisplayName:Ljava/lang/String;

    const/4 v11, 0x1

    invoke-static {v5, v6, v7, v10, v11}, Lcom/google/android/apps/plus/phone/Intents;->getPhotoPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;Z)Landroid/content/Intent;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1266
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1258
    .end local v8           #remoteUrl:Ljava/lang/String;
    .end local v9           #localUri:Landroid/net/Uri;
    .end local v10           #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v24           #photoUri:Landroid/net/Uri;
    :cond_12a
    const/16 v24, 0x0

    goto :goto_f4

    .line 1259
    .restart local v24       #photoUri:Landroid/net/Uri;
    :cond_12d
    const/4 v9, 0x0

    goto :goto_fc

    .line 1260
    .restart local v9       #localUri:Landroid/net/Uri;
    :cond_12f
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    goto :goto_ff

    .line 1270
    .end local v9           #localUri:Landroid/net/Uri;
    .end local v24           #photoUri:Landroid/net/Uri;
    :sswitch_134
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->downloadPhoto(Landroid/content/Context;Z)V

    .line 1271
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1275
    :sswitch_13f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v5, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getPhotoImage()Landroid/graphics/Bitmap;

    move-result-object v18

    .line 1276
    .local v18, bitmap:Landroid/graphics/Bitmap;
    if-eqz v18, :cond_187

    .line 1277
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    .line 1278
    .local v26, res:Landroid/content/res/Resources;
    const v5, 0x7f080160

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 1280
    .local v28, toastSuccess:Ljava/lang/String;
    const v5, 0x7f08015f

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v27

    .line 1281
    .local v27, toastError:Ljava/lang/String;
    const/16 v5, 0x29

    const v6, 0x7f080083

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(ILjava/lang/String;)V

    .line 1283
    new-instance v5, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move-object/from16 v2, v27

    move-object/from16 v3, v17

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/PhotoViewFragment;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)V

    const/4 v6, 0x1

    new-array v6, v6, [Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    aput-object v18, v6, v7

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$3;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1307
    .end local v26           #res:Landroid/content/res/Resources;
    .end local v27           #toastError:Ljava/lang/String;
    .end local v28           #toastSuccess:Ljava/lang/String;
    :cond_187
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1311
    .end local v18           #bitmap:Landroid/graphics/Bitmap;
    :sswitch_18a
    const v5, 0x7f080068

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f080069

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0801c4

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v11, 0x7f0801c5

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v6, v7, v11}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v20

    .line 1316
    .local v20, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v5, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1317
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "remove_tag"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1318
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1322
    .end local v20           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :sswitch_1c8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v5, v6, v7, v11, v12}, Lcom/google/android/apps/plus/service/EsService;->getPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getView()Landroid/view/View;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateView(Landroid/view/View;)V

    .line 1323
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1327
    :sswitch_1f0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    .line 1328
    .restart local v26       #res:Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    if-eqz v5, :cond_24f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v24

    .line 1329
    .restart local v24       #photoUri:Landroid/net/Uri;
    :goto_202
    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_252

    move-object/from16 v9, v24

    .line 1331
    .restart local v9       #localUri:Landroid/net/Uri;
    :goto_20a
    if-nez v9, :cond_254

    const v23, 0x7f0e0003

    .line 1334
    .local v23, messageId:I
    :goto_20f
    const v5, 0x7f0e0002

    const/4 v6, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0e0010

    const/4 v11, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v7, v11}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v7

    const v11, 0x7f0801c5

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v6, v7, v11}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v20

    .line 1339
    .restart local v20       #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v5, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1340
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "remove_dialog"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1341
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1328
    .end local v9           #localUri:Landroid/net/Uri;
    .end local v20           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    .end local v23           #messageId:I
    .end local v24           #photoUri:Landroid/net/Uri;
    :cond_24f
    const/16 v24, 0x0

    goto :goto_202

    .line 1329
    .restart local v24       #photoUri:Landroid/net/Uri;
    :cond_252
    const/4 v9, 0x0

    goto :goto_20a

    .line 1331
    .restart local v9       #localUri:Landroid/net/Uri;
    :cond_254
    const v23, 0x7f0e0004

    goto :goto_20f

    .line 1345
    .end local v9           #localUri:Landroid/net/Uri;
    .end local v24           #photoUri:Landroid/net/Uri;
    .end local v26           #res:Landroid/content/res/Resources;
    :sswitch_258
    const v5, 0x7f08006a

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f08006b

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0801c4

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v11, 0x7f0801c5

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v5, v6, v7, v11}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v20

    .line 1350
    .restart local v20       #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    const/4 v5, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1351
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "report_dialog"

    move-object/from16 v0, v20

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1352
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1356
    .end local v20           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :sswitch_296
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v17

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/phone/Intents;->getSettingsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v22

    .line 1357
    .restart local v22       #intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->startActivity(Landroid/content/Intent;)V

    .line 1358
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1362
    .end local v22           #intent:Landroid/content/Intent;
    :sswitch_2aa
    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v6, :cond_2c1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v11

    invoke-static {v6, v7, v5, v11}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    .line 1363
    :cond_2c1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    .line 1364
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1368
    :sswitch_2cb
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080412

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 1370
    .local v21, helpUrlParam:Ljava/lang/String;
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.VIEW"

    move-object/from16 v0, v17

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/fragments/EsFragment;->startActivity(Landroid/content/Intent;)V

    .line 1372
    const/4 v5, 0x1

    goto/16 :goto_d

    .line 1214
    :sswitch_data_2f2
    .sparse-switch
        0x102002c -> :sswitch_1b
        0x7f090199 -> :sswitch_62
        0x7f09028d -> :sswitch_1c8
        0x7f09028e -> :sswitch_296
        0x7f09028f -> :sswitch_2aa
        0x7f090290 -> :sswitch_2cb
        0x7f0902b9 -> :sswitch_258
        0x7f0902ca -> :sswitch_22
        0x7f0902cb -> :sswitch_42
        0x7f0902cc -> :sswitch_e6
        0x7f0902cd -> :sswitch_13f
        0x7f0902ce -> :sswitch_18a
        0x7f0902cf -> :sswitch_1f0
        0x7f0902d0 -> :sswitch_134
    .end sparse-switch
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 719
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onPause()V

    .line 721
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->removeScreenListener(Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;)V

    .line 723
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->removeMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;)V

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->resetPhotoView()V

    .line 725
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 18
    .parameter "menu"

    .prologue
    .line 1163
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    move-object/from16 v0, p0

    invoke-interface {v11, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v11

    if-nez v11, :cond_d

    .line 1204
    .end local p1
    :goto_c
    return-void

    .line 1167
    .restart local p1
    :cond_d
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    if-nez v11, :cond_ec

    const/4 v9, 0x0

    .line 1169
    .local v9, shapeId:Ljava/lang/Long;
    :goto_14
    if-eqz v9, :cond_f8

    const/4 v10, 0x1

    .line 1170
    .local v10, taggedAsMe:Z
    :goto_17
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    if-eqz v11, :cond_fb

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 1171
    .local v8, photoUri:Landroid/net/Uri;
    :goto_25
    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-eqz v11, :cond_fe

    invoke-static {v8}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v11

    if-nez v11, :cond_fe

    const/4 v3, 0x1

    .line 1173
    .local v3, isRemotePhoto:Z
    :goto_36
    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    const-wide/16 v13, 0x0

    cmp-long v11, v11, v13

    if-nez v11, :cond_101

    if-eqz v8, :cond_101

    const/4 v6, 0x1

    .line 1174
    .local v6, onlyPhotoUrl:Z
    :goto_43
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_5d

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    if-nez v11, :cond_104

    invoke-static {v8}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v11

    if-eqz v11, :cond_104

    :cond_5d
    const/4 v5, 0x1

    .line 1176
    .local v5, myPhoto:Z
    :goto_5e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    const-string v12, "stream_id"

    invoke-virtual {v11, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1177
    .local v7, photoStream:Ljava/lang/String;
    const-string v11, "camerasync"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1178
    .local v2, isInstantUpload:Z
    if-nez v6, :cond_7a

    if-eqz v3, :cond_107

    if-nez v5, :cond_7a

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->canDownload()Z

    move-result v11

    if-eqz v11, :cond_107

    :cond_7a
    const/4 v1, 0x1

    .line 1180
    .local v1, allowDownload:Z
    :goto_7b
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0xb

    if-ge v11, v12, :cond_10a

    .line 1181
    const v11, 0x7f0902cb

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1182
    const v11, 0x7f0902ca

    move-object/from16 v4, p1

    .line 1188
    .end local p1
    .local v4, menu:Landroid/view/Menu;
    :goto_8f
    const/4 v12, 0x0

    move v15, v12

    move v12, v11

    move v11, v15

    .end local v4           #menu:Landroid/view/Menu;
    :goto_93
    invoke-static {v4, v12, v11}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1192
    const v11, 0x7f090199

    move-object/from16 v0, p1

    invoke-static {v0, v11, v2}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1195
    const v12, 0x7f0902cc

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v11

    if-nez v11, :cond_13c

    if-nez v5, :cond_af

    if-eqz v10, :cond_13c

    :cond_af
    const/4 v11, 0x1

    :goto_b0
    move-object/from16 v0, p1

    invoke-static {v0, v12, v11}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1198
    const v11, 0x7f0902cd

    move-object/from16 v0, p1

    invoke-static {v0, v11, v5}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1199
    const v11, 0x7f0902cf

    move-object/from16 v0, p1

    invoke-static {v0, v11, v5}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1200
    const v11, 0x7f0902d0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1201
    const v12, 0x7f0902b9

    if-nez v5, :cond_13f

    if-eqz v3, :cond_13f

    const/4 v11, 0x1

    :goto_d5
    move-object/from16 v0, p1

    invoke-static {v0, v12, v11}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1202
    const v11, 0x7f09028d

    move-object/from16 v0, p1

    invoke-static {v0, v11, v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1203
    const v11, 0x7f0902ce

    move-object/from16 v0, p1

    invoke-static {v0, v11, v10}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    goto/16 :goto_c

    .line 1167
    .end local v1           #allowDownload:Z
    .end local v2           #isInstantUpload:Z
    .end local v3           #isRemotePhoto:Z
    .end local v5           #myPhoto:Z
    .end local v6           #onlyPhotoUrl:Z
    .end local v7           #photoStream:Ljava/lang/String;
    .end local v8           #photoUri:Landroid/net/Uri;
    .end local v9           #shapeId:Ljava/lang/Long;
    .end local v10           #taggedAsMe:Z
    .restart local p1
    :cond_ec
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v11, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->getMyApprovedShapeId()Ljava/lang/Long;

    move-result-object v9

    goto/16 :goto_14

    .line 1169
    .restart local v9       #shapeId:Ljava/lang/Long;
    :cond_f8
    const/4 v10, 0x0

    goto/16 :goto_17

    .line 1170
    .restart local v10       #taggedAsMe:Z
    :cond_fb
    const/4 v8, 0x0

    goto/16 :goto_25

    .line 1171
    .restart local v8       #photoUri:Landroid/net/Uri;
    :cond_fe
    const/4 v3, 0x0

    goto/16 :goto_36

    .line 1173
    .restart local v3       #isRemotePhoto:Z
    :cond_101
    const/4 v6, 0x0

    goto/16 :goto_43

    .line 1174
    .restart local v6       #onlyPhotoUrl:Z
    :cond_104
    const/4 v5, 0x0

    goto/16 :goto_5e

    .line 1178
    .restart local v2       #isInstantUpload:Z
    .restart local v5       #myPhoto:Z
    .restart local v7       #photoStream:Ljava/lang/String;
    :cond_107
    const/4 v1, 0x0

    goto/16 :goto_7b

    .line 1183
    .restart local v1       #allowDownload:Z
    :cond_10a
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->hasPlusOned()Z

    move-result v11

    if-eqz v11, :cond_120

    .line 1184
    const v11, 0x7f0902cb

    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1185
    const v11, 0x7f0902ca

    move-object/from16 v4, p1

    .end local p1
    .restart local v4       #menu:Landroid/view/Menu;
    goto/16 :goto_8f

    .line 1187
    .end local v4           #menu:Landroid/view/Menu;
    .restart local p1
    :cond_120
    const v11, 0x7f0902cb

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v11, v12}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1188
    const v11, 0x7f0902ca

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAllowPlusOne:Z

    if-eqz v12, :cond_141

    if-eqz v3, :cond_141

    const/4 v12, 0x1

    move-object/from16 v4, p1

    move v15, v11

    move v11, v12

    move v12, v15

    goto/16 :goto_93

    .line 1195
    .end local p1
    :cond_13c
    const/4 v11, 0x0

    goto/16 :goto_b0

    .line 1201
    :cond_13f
    const/4 v11, 0x0

    goto :goto_d5

    .restart local p1
    :cond_141
    move-object/from16 v4, p1

    .end local p1
    .restart local v4       #menu:Landroid/view/Menu;
    goto/16 :goto_8f
.end method

.method public final onPrepareTitlebarButtons(Landroid/view/Menu;)Z
    .registers 16
    .parameter "menu"

    .prologue
    const-wide/16 v12, 0x0

    const v11, 0x7f0902cb

    const v10, 0x7f0902ca

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1118
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v8, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v8

    if-nez v8, :cond_13

    .line 1139
    .end local p1
    :goto_12
    return v7

    .line 1122
    .restart local p1
    :cond_13
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    if-eqz v8, :cond_6a

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1123
    .local v5, photoUri:Landroid/net/Uri;
    :goto_1d
    iget-wide v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    cmp-long v8, v8, v12

    if-eqz v8, :cond_6c

    invoke-static {v5}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v8

    if-nez v8, :cond_6c

    move v1, v6

    .line 1125
    .local v1, isRemotePhoto:Z
    :goto_2a
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3e

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    if-nez v8, :cond_6e

    invoke-static {v5}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v8

    if-eqz v8, :cond_6e

    :cond_3e
    move v3, v6

    .line 1127
    .local v3, myPhoto:Z
    :goto_3f
    iget-wide v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    cmp-long v8, v8, v12

    if-nez v8, :cond_70

    if-eqz v5, :cond_70

    move v4, v6

    .line 1128
    .local v4, onlyPhotoUrl:Z
    :goto_48
    if-nez v4, :cond_54

    if-eqz v1, :cond_72

    if-nez v3, :cond_54

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->canDownload()Z

    move-result v8

    if-eqz v8, :cond_72

    :cond_54
    move v0, v6

    .line 1130
    .local v0, allowDownload:Z
    :goto_55
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->hasPlusOned()Z

    move-result v8

    if-eqz v8, :cond_74

    .line 1131
    invoke-static {p1, v11, v6}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    move-object v2, p1

    .line 1135
    .end local p1
    :goto_5f
    invoke-static {v2, v10, v7}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1137
    const v7, 0x7f0902d0

    invoke-static {p1, v7, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    move v7, v6

    .line 1139
    goto :goto_12

    .line 1122
    .end local v0           #allowDownload:Z
    .end local v1           #isRemotePhoto:Z
    .end local v3           #myPhoto:Z
    .end local v4           #onlyPhotoUrl:Z
    .end local v5           #photoUri:Landroid/net/Uri;
    .restart local p1
    :cond_6a
    const/4 v5, 0x0

    goto :goto_1d

    .restart local v5       #photoUri:Landroid/net/Uri;
    :cond_6c
    move v1, v7

    .line 1123
    goto :goto_2a

    .restart local v1       #isRemotePhoto:Z
    :cond_6e
    move v3, v7

    .line 1125
    goto :goto_3f

    .restart local v3       #myPhoto:Z
    :cond_70
    move v4, v7

    .line 1127
    goto :goto_48

    .restart local v4       #onlyPhotoUrl:Z
    :cond_72
    move v0, v7

    .line 1128
    goto :goto_55

    .line 1134
    .restart local v0       #allowDownload:Z
    :cond_74
    invoke-static {p1, v11, v7}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->setVisible(Landroid/view/Menu;IZ)V

    .line 1135
    iget-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAllowPlusOne:Z

    if-eqz v8, :cond_80

    if-eqz v1, :cond_80

    move v7, v6

    move-object v2, p1

    goto :goto_5f

    :cond_80
    move-object v2, p1

    .end local p1
    .local v2, menu:Landroid/view/Menu;
    goto :goto_5f
.end method

.method public final onResume()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 683
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_46

    const/4 v0, 0x1

    .line 685
    .local v0, hadProgressDialog:Z
    :goto_6
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onResume()V

    .line 687
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 688
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->addScreenListener(Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnScreenListener;)V

    .line 689
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->addMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoViewActivity$OnMenuItemListener;)V

    .line 691
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const v3, 0x7f09002b

    invoke-virtual {v2, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 694
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPendingBytes:[B

    if-eqz v2, :cond_3c

    .line 695
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v2, :cond_48

    .line 698
    const-string v2, "PhotoViewFragment"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3a

    .line 699
    const-string v2, "PhotoViewFragment"

    const-string v3, "Both a pending profile image and an existing request"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    :cond_3a
    :goto_3a
    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPendingBytes:[B

    .line 711
    :cond_3c
    if-eqz v0, :cond_45

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v2, :cond_45

    .line 713
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->hideProgressDialog()V

    .line 715
    :cond_45
    return-void

    .line 683
    .end local v0           #hadProgressDialog:Z
    :cond_46
    const/4 v0, 0x0

    goto :goto_6

    .line 702
    .restart local v0       #hadProgressDialog:Z
    :cond_48
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPendingBytes:[B

    .line 704
    .local v1, profileBytes:[B
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/service/EsService;->setProfilePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mNewerReqId:Ljava/lang/Integer;

    .line 705
    const/16 v2, 0x28

    const v3, 0x7f080088

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->showProgressDialog(ILjava/lang/String;)V

    goto :goto_3a
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 6
    .parameter "outState"

    .prologue
    .line 747
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 749
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mForceLoadId:Ljava/lang/Long;

    if-eqz v1, :cond_12

    .line 750
    const-string v1, "com.google.android.apps.plus.PhotoViewFragment.FORCE_LOAD"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mForceLoadId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 752
    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    .line 753
    const-string v1, "com.google.android.apps.plus.PhotoViewFragment.DOWNLOADABLE"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mDownloadable:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 755
    :cond_21
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3b

    .line 756
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    .line 757
    .local v0, ids:[Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 758
    const-string v1, "flagged_comments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 760
    .end local v0           #ids:[Ljava/lang/String;
    :cond_3b
    return-void
.end method

.method public bridge synthetic onScroll(Landroid/widget/AbsListView;III)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 101
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScroll(Landroid/widget/AbsListView;III)V

    return-void
.end method

.method protected final onScrollDown()V
    .registers 7

    .prologue
    .line 979
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mFooterView:Landroid/view/View;

    const v2, 0x7f090066

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 980
    .local v0, progressView:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_29

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    if-nez v1, :cond_29

    .line 982
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOwnerId:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mPhotoId:J

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->getPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mOlderReqId:Ljava/lang/Integer;

    .line 984
    :cond_29
    return-void
.end method

.method public bridge synthetic onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 101
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsListFragment;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    return-void
.end method

.method public final onUpdateProgressView(Landroid/widget/ProgressBar;)V
    .registers 4
    .parameter "progressBarView"

    .prologue
    .line 1107
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mProgressBarView:Landroid/widget/ProgressBar;

    .line 1108
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mProgressBarView:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateSpinner(Landroid/widget/ProgressBar;)V

    .line 1110
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 1111
    .local v0, myView:Landroid/view/View;
    if-eqz v0, :cond_10

    .line 1112
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateView(Landroid/view/View;)V

    .line 1114
    :cond_10
    return-void
.end method

.method public final onViewActivated()V
    .registers 3

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 1025
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mAdapter:Lcom/google/android/apps/plus/phone/EsCursorAdapter;

    check-cast v0, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoViewAdapter;->resetViews()V

    .line 1034
    :cond_f
    :goto_f
    return-void

    .line 1027
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment$PhotoViewCallbacks;->onFragmentVisible(Landroid/support/v4/app/Fragment;)V

    .line 1030
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_f

    .line 1031
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoViewFragment;->updateMenuItems()V

    goto :goto_f
.end method
