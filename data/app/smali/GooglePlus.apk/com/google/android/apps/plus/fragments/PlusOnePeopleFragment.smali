.class public Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "PlusOnePeopleFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment$PeopleSetQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/DialogFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 35
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 2
    .parameter "v"

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->dismiss()V

    .line 119
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 8
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 82
    packed-switch p1, :pswitch_data_20

    .line 94
    :cond_4
    :goto_4
    return-object v2

    .line 84
    :pswitch_5
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v3, :cond_4

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 89
    .local v0, extras:Landroid/os/Bundle;
    const-string v2, "plus_one_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 90
    .local v1, plusOneId:Ljava/lang/String;
    new-instance v2, Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_4

    .line 82
    nop

    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_5
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 11
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 59
    const v3, 0x7f030057

    invoke-virtual {p1, v3, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 61
    .local v2, view:Landroid/view/View;
    new-instance v3, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, v6}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    .line 62
    const v3, 0x102000a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 63
    .local v1, listView:Landroid/widget/ListView;
    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 64
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 66
    const v3, 0x7f09005c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    const v3, 0x7f09005d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v3

    const v4, 0x7f080183

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 72
    .local v0, extras:Landroid/os/Bundle;
    const-string v3, "account"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v3, v5, v6, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 75
    const v3, 0x7f090066

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 77
    return-object v2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {v3, p3}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->isExtraPeopleViewIndex(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 136
    :cond_8
    :goto_8
    return-void

    .line 127
    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {v3, p3}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 128
    .local v0, cursor:Landroid/database/Cursor;
    if-eqz v0, :cond_8

    .line 132
    const/4 v3, 0x1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 133
    .local v2, personId:Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    invoke-static {v3, v4, v2, v5}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 135
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_8
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 28
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_3a

    :goto_9
    return-void

    :pswitch_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "total_plus_ones"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-nez p2, :cond_34

    const/4 v0, 0x0

    :goto_27
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    sub-int v0, v1, v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->setExtraPeopleCount(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment;->mAdapter:Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_9

    :cond_34
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_27

    nop

    :pswitch_data_3a
    .packed-switch 0x0
        :pswitch_a
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method
