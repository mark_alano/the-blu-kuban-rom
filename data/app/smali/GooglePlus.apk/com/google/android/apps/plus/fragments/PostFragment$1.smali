.class final Lcom/google/android/apps/plus/fragments/PostFragment$1;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 286
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .registers 15
    .parameter "v"

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 289
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_12e

    .line 335
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_15

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1000(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V

    .line 344
    :cond_15
    :goto_15
    return-void

    .line 291
    :sswitch_16
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_ACL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 293
    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const v3, 0x7f080282

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$600(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    const/4 v4, 0x5

    move v6, v5

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v12, v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_15

    .line 305
    :sswitch_5a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_CAMERA:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    const-string v0, "camera-post.jpg"

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/Intents;->getCameraIntent$3a35108a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    .line 308
    .local v9, intent:Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v9, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_15

    .line 313
    .end local v9           #intent:Landroid/content/Intent;
    :sswitch_83
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_GALLERY:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 315
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_b8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;

    move-result-object v0

    :cond_b8
    invoke-static {v1, v2, v0, v7}, Lcom/google/android/apps/plus/phone/Intents;->getCameraPhotosPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Z)Landroid/content/Intent;

    move-result-object v9

    .line 317
    .restart local v9       #intent:Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0, v9, v7}, Lcom/google/android/apps/plus/fragments/PostFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_15

    .line 322
    .end local v9           #intent:Landroid/content/Intent;
    :sswitch_c3
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 325
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$800(Lcom/google/android/apps/plus/fragments/PostFragment;)Z

    move-result v11

    .line 326
    .local v11, placesOnly:Z
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$900(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v1

    if-eqz v1, :cond_12b

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$900(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbLocation;->hasCoordinates()Z

    move-result v1

    if-eqz v1, :cond_12b

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$900(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v10

    .line 328
    .local v10, location:Lcom/google/android/apps/plus/content/DbLocation;
    :goto_fa
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    new-instance v9, Landroid/content/Intent;

    const-class v2, Lcom/google/android/apps/plus/phone/CheckinActivity;

    invoke-direct {v9, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "android.intent.action.PICK"

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "account"

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "places_only"

    invoke-virtual {v9, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz v10, :cond_123

    const-string v0, "location"

    invoke-virtual {v9, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 330
    .restart local v9       #intent:Landroid/content/Intent;
    :cond_123
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v1, 0x4

    invoke-virtual {v0, v9, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_15

    .end local v9           #intent:Landroid/content/Intent;
    .end local v10           #location:Lcom/google/android/apps/plus/content/DbLocation;
    :cond_12b
    move-object v10, v0

    .line 326
    goto :goto_fa

    .line 289
    nop

    :sswitch_data_12e
    .sparse-switch
        0x7f090052 -> :sswitch_16
        0x7f0901ab -> :sswitch_c3
        0x7f0901af -> :sswitch_5a
        0x7f0901b0 -> :sswitch_83
    .end sparse-switch
.end method
