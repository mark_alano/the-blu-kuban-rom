.class final Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub$MultiChoiceCallbackStub;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaActionModeCallback"
.end annotation


# instance fields
.field private final mActionModeStub:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .registers 4
    .parameter

    .prologue
    .line 1824
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1825
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_13

    .line 1826
    new-instance v0, Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;-><init>(Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub$MultiChoiceCallbackStub;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->mActionModeStub:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    .line 1830
    :goto_12
    return-void

    .line 1828
    :cond_13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->mActionModeStub:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    goto :goto_12
.end method


# virtual methods
.method public final getCallback()Landroid/widget/AbsListView$MultiChoiceModeListener;
    .registers 2

    .prologue
    .line 1836
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->mActionModeStub:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;->getCallback()Landroid/widget/AbsListView$MultiChoiceModeListener;

    move-result-object v0

    return-object v0
.end method

.method public final onActionItemClicked(Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;Landroid/view/MenuItem;)Z
    .registers 5
    .parameter "actionMode"
    .parameter "menuItem"

    .prologue
    .line 1860
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_1e

    .line 1865
    :goto_7
    const/4 v0, 0x1

    return v0

    .line 1862
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mSelectedPhotos:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3200(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3300(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/util/List;)V

    .line 1863
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    .line 1864
    invoke-virtual {p1}, Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;->finish()V

    goto :goto_7

    .line 1860
    nop

    :pswitch_data_1e
    .packed-switch 0x7f0902d2
        :pswitch_9
    .end packed-switch
.end method

.method public final onCreateActionMode(Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;Landroid/view/Menu;)Z
    .registers 6
    .parameter "actionMode"
    .parameter "menu"

    .prologue
    .line 1841
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #setter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3102(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;)Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    .line 1843
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 1844
    .local v0, inflater:Landroid/view/MenuInflater;
    const v1, 0x7f10001a

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1846
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const v2, 0x7f080198

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, -0x100

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;->setTitle(Ljava/lang/CharSequence;I)V

    .line 1848
    const/4 v1, 0x1

    return v1
.end method

.method public final onDestroyActionMode$6caf4747()V
    .registers 3

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v1, 0x0

    #setter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3102(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;)Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    .line 1879
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->access$3500(Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;)V

    .line 1880
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mSelectedPhotos:Ljava/util/List;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3200(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1881
    return-void
.end method
