.class final Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;
.super Ljava/lang/Object;
.source "PostFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaGallery"
.end annotation


# instance fields
.field private mGalleryView:Landroid/view/ViewGroup;

.field private mPhotoAdapter:Lcom/google/android/apps/plus/fragments/PhotoAdapter;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/ViewGroup;)V
    .registers 10
    .parameter
    .parameter "context"
    .parameter
    .parameter "galleryView"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, images:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    const/4 v4, 0x0

    .line 1978
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1979
    new-instance v2, Lcom/google/android/apps/plus/fragments/PhotoAdapter;

    invoke-direct {v2, p2, p3}, Lcom/google/android/apps/plus/fragments/PhotoAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mPhotoAdapter:Lcom/google/android/apps/plus/fragments/PhotoAdapter;

    .line 1980
    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    .line 1982
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mPhotoAdapter:Lcom/google/android/apps/plus/fragments/PhotoAdapter;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PhotoAdapter;->getCount()I

    move-result v1

    .line 1983
    .local v1, photoCount:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_16
    if-ge v0, v1, :cond_26

    .line 1984
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mPhotoAdapter:Lcom/google/android/apps/plus/fragments/PhotoAdapter;

    invoke-virtual {v3, v0, v4, v4}, Lcom/google/android/apps/plus/fragments/PhotoAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1983
    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    .line 1986
    :cond_26
    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;Lcom/google/android/apps/plus/api/MediaRef;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v2, 0x0

    .line 1974
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mPhotoAdapter:Lcom/google/android/apps/plus/fragments/PhotoAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PhotoAdapter;->add(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mPhotoAdapter:Lcom/google/android/apps/plus/fragments/PhotoAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mPhotoAdapter:Lcom/google/android/apps/plus/fragments/PhotoAdapter;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PhotoAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/android/apps/plus/fragments/PhotoAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3600(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;Lcom/google/android/apps/plus/api/MediaRef;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mPhotoAdapter:Lcom/google/android/apps/plus/fragments/PhotoAdapter;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/PhotoAdapter;->remove(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_d
    if-ge v1, v2, :cond_2a

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2a
    return-void

    :cond_2b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_d
.end method

.method static synthetic access$3500(Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_8
    if-ltz v0, :cond_17

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->mGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    :cond_17
    return-void
.end method
