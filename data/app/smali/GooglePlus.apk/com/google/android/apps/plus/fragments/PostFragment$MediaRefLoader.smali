.class final Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;
.super Landroid/support/v4/content/AsyncTaskLoader;
.source "PostFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaRefLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/AsyncTaskLoader",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/google/android/apps/plus/api/MediaRef;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mLoadedList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private final mMediaRefList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)V
    .registers 5
    .parameter "context"
    .parameter "account"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1901
    .local p3, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    invoke-direct {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 1898
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mLoadedList:Ljava/util/ArrayList;

    .line 1902
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mMediaRefList:Ljava/util/ArrayList;

    .line 1903
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 1904
    return-void
.end method


# virtual methods
.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .registers 16

    .prologue
    .line 1895
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mMediaRefList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1a
    :goto_1a
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_df

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_80

    const/4 v0, 0x0

    :goto_2e
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GalleryUtils;->isGalleryContentUri(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_8a

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/util/GalleryUtils;->getPhotoId(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v13, 0x0

    cmp-long v3, v5, v13

    if-eqz v3, :cond_1a

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/util/GalleryUtils;->getAccountName(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1a

    invoke-virtual {v3, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1a

    invoke-static {v11, v0}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->safeGetMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_85

    const-string v3, "video/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_85

    const/4 v0, 0x1

    :goto_69
    if-eqz v0, :cond_87

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_6d
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mLoadedList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1a

    :cond_80
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_2e

    :cond_85
    const/4 v0, 0x0

    goto :goto_69

    :cond_87
    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_6d

    :cond_8a
    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v0

    if-eqz v0, :cond_cc

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v11, v0}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->safeGetMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c7

    const-string v2, "video/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c7

    const/4 v0, 0x1

    :goto_a7
    if-eqz v0, :cond_c9

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_ab
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v7

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mLoadedList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1a

    :cond_c7
    const/4 v0, 0x0

    goto :goto_a7

    :cond_c9
    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_ab

    :cond_cc
    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->hasUrl()Z

    move-result v0

    if-nez v0, :cond_d8

    invoke-virtual {v7}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_d8
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mLoadedList:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1a

    :cond_df
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mLoadedList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected final onStartLoading()V
    .registers 2

    .prologue
    .line 1908
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->mLoadedList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_b

    .line 1909
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;->forceLoad()V

    .line 1911
    :cond_b
    return-void
.end method
