.class final Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PostLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 406
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 406
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .registers 4
    .parameter "location"

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->removeLocationListener()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$900(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 428
    :goto_d
    return-void

    .line 417
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #setter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1602(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/location/Location;)Landroid/location/Location;

    .line 419
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->getCityLevelLocationPreference()Z
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1700(Lcom/google/android/apps/plus/fragments/PostFragment;)Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/LocationController;->getCityLevelLocation(Landroid/location/Location;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    :goto_21
    #setter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$902(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/content/DbLocation;)Lcom/google/android/apps/plus/content/DbLocation;

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1800(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V

    goto :goto_d

    .line 419
    :cond_35
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/LocationController;->getStreetLevelLocation(Landroid/location/Location;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    goto :goto_21
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    .prologue
    .line 432
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .registers 2
    .parameter "provider"

    .prologue
    .line 436
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .registers 4
    .parameter "provider"
    .parameter "status"
    .parameter "extras"

    .prologue
    .line 440
    return-void
.end method
