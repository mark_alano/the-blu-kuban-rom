.class public Lcom/google/android/apps/plus/fragments/PostFragment;
.super Landroid/support/v4/app/Fragment;
.source "PostFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;,
        Lcom/google/android/apps/plus/fragments/PostFragment$MediaRefLoader;,
        Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;,
        Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;,
        Lcom/google/android/apps/plus/fragments/PostFragment$PreviewCursorLoader;,
        Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;,
        Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;,
        Lcom/google/android/apps/plus/fragments/PostFragment$AccountStatusQuery;,
        Lcom/google/android/apps/plus/fragments/PostFragment$PlatformAudienceQuery;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

.field private mActivityId:Ljava/lang/String;

.field private mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

.field private mAttachmentRefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mAttachments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

.field private mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mDeepLinkId:Ljava/lang/String;

.field private mDeepLinkMetadata:Landroid/os/Bundle;

.field private mFooterMessage:Ljava/lang/String;

.field private mInsertCameraPhotoRequestId:Ljava/lang/Integer;

.field private mIsFromPlusOne:Z

.field private mLoadingMediaAttachments:Z

.field private mLoadingUrlPreview:Z

.field private mLoadingView:Landroid/view/View;

.field private mLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private mLocationChecked:Z

.field private mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

.field private mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

.field private mMediaGalleryView:Landroid/view/ViewGroup;

.field private final mMediaRefLoaderCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

.field private mOriginalText:Ljava/lang/String;

.field private mPendingPostId:Ljava/lang/Integer;

.field private mPreviewContainerView:Landroid/view/ViewGroup;

.field private mPreviewResult:Lcom/google/android/apps/plus/network/ApiaryActivity;

.field private mProviderLocation:Landroid/location/Location;

.field private mRemoveLocation:Z

.field private mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

.field private mResultMediaItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSelectedPhotos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private final mTextWatcher:Landroid/text/TextWatcher;

.field private mUrl:Ljava/lang/String;

.field private onClickListener:Landroid/view/View$OnClickListener;

.field private onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 116
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/network/ApiaryActivity;

    .line 253
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSelectedPhotos:Ljava/util/List;

    .line 255
    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 257
    new-instance v0, Lcom/google/android/apps/plus/util/MentionTokenizer;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    .line 286
    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    .line 347
    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTextWatcher:Landroid/text/TextWatcher;

    .line 388
    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 1785
    new-instance v0, Lcom/google/android/apps/plus/fragments/PostFragment$8;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$8;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaRefLoaderCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    .line 1974
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/Integer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V
    .registers 7
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 116
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_4a

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSelectedPhotos:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4b

    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSelectedPhotos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;->finish()V

    :cond_2a
    :goto_2a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    if-eqz v0, :cond_4a

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    const v1, 0x7f080199

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSelectedPhotos:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/high16 v2, -0x100

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;->setTitle(Ljava/lang/CharSequence;I)V

    :cond_4a
    return-void

    :cond_4b
    invoke-virtual {p1, v2}, Landroid/view/View;->setSelected(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSelectedPhotos:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSelectedPhotos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_2a

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    if-eqz v0, :cond_64

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;->finish()V

    :cond_64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaActionModeCallback;->getCallback()Landroid/widget/AbsListView$MultiChoiceModeListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    goto :goto_2a
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/util/MentionTokenizer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/widget/ScrollView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeLocationListener()V

    return-void
.end method

.method static synthetic access$1602(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/location/Location;)Landroid/location/Location;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/fragments/PostFragment;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getCityLevelLocationPreference()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->insertCameraPhoto(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/android/apps/plus/fragments/PostFragment;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/network/ApiaryActivity;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PostFragment;->handlePreviewResult(Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/network/ApiaryActivity;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateViews(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$2900(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachments:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachments:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/PostFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 116
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PostFragment;->handlePostResult(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/api/MediaRef;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->addToMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V

    return-void
.end method

.method static synthetic access$3102(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;)Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActionMode:Lcom/google/android/apps/plus/platform/MultiChoiceActionModeStub;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mSelectedPhotos:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/util/List;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeFromMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V

    goto :goto_4

    :cond_14
    return-void
.end method

.method static synthetic access$3400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View$OnClickListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AudienceView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/PostFragment;)Z
    .registers 3
    .parameter "x0"

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_11
    return v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_11
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;
    .registers 2
    .parameter "x0"

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/content/DbLocation;)Lcom/google/android/apps/plus/content/DbLocation;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    return-object p1
.end method

.method private addLocationListener()V
    .registers 10

    .prologue
    .line 1618
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-nez v0, :cond_26

    .line 1619
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 1620
    new-instance v0, Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x1

    const-wide/16 v4, 0xbb8

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    new-instance v7, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/google/android/apps/plus/fragments/PostFragment$PostLocationListener;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;B)V

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/LocationController;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZJLandroid/location/Location;Landroid/location/LocationListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    .line 1624
    :cond_26
    return-void
.end method

.method private addToMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V
    .registers 3
    .parameter "mediaRef"

    .prologue
    .line 1682
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1683
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->access$2700(Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;Lcom/google/android/apps/plus/api/MediaRef;)V

    .line 1684
    return-void
.end method

.method private clearMediaGallery()V
    .registers 3

    .prologue
    .line 1711
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_15

    .line 1712
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeFromMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V

    goto :goto_0

    .line 1714
    :cond_15
    return-void
.end method

.method private getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    .registers 2

    .prologue
    .line 1777
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v0}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    .line 1779
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v0

    return-object v0
.end method

.method private getCityLevelLocationPreference()Z
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 977
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string v2, "streams"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 979
    .local v0, sharedPreferences:Landroid/content/SharedPreferences;
    const-string v1, "city_level_sharebox_location"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 980
    const-string v1, "city_level_sharebox_location"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 983
    :goto_19
    return v1

    :cond_1a
    const-string v1, "city_level_location"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_19
.end method

.method private static getLocationFromExtras(Landroid/os/Bundle;)Lcom/google/android/apps/plus/content/DbLocation;
    .registers 16
    .parameter "args"

    .prologue
    .line 648
    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/content/DbLocation;

    .line 651
    .local v12, location:Lcom/google/android/apps/plus/content/DbLocation;
    if-eqz v12, :cond_b

    .line 712
    .end local v12           #location:Lcom/google/android/apps/plus/content/DbLocation;
    :goto_a
    return-object v12

    .line 656
    .restart local v12       #location:Lcom/google/android/apps/plus/content/DbLocation;
    :cond_b
    const-string v0, "location_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1b

    const-string v0, "cid"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_124

    .line 658
    :cond_1b
    const/4 v9, 0x0

    .line 659
    .local v9, addedFields:Z
    const/4 v2, 0x0

    .line 660
    .local v2, latitudeE7:Ljava/lang/Integer;
    const/4 v3, 0x0

    .line 661
    .local v3, longitudeE7:Ljava/lang/Integer;
    const/4 v6, 0x0

    .line 662
    .local v6, clusterId:Ljava/lang/String;
    const/4 v4, 0x0

    .line 663
    .local v4, name:Ljava/lang/String;
    const/4 v5, 0x0

    .line 666
    .local v5, bestAddress:Ljava/lang/String;
    const-string v0, "latitude"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_80

    const-string v0, "longitude"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 668
    :try_start_31
    const-string v0, "latitude"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    .line 669
    .local v10, latitude:D
    const-string v0, "longitude"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v13

    .line 671
    .local v13, longitude:D
    const-wide v0, -0x3fa9800000000000L

    cmpl-double v0, v10, v0

    if-ltz v0, :cond_ba

    const-wide v0, 0x4056800000000000L

    cmpg-double v0, v10, v0

    if-gtz v0, :cond_ba

    const-wide v0, -0x3f99800000000000L

    cmpl-double v0, v13, v0

    if-ltz v0, :cond_ba

    const-wide v0, 0x4066800000000000L

    cmpg-double v0, v13, v0

    if-gtz v0, :cond_ba

    .line 673
    const-wide v0, 0x416312d000000000L

    mul-double/2addr v0, v10

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 674
    const-wide v0, 0x416312d000000000L

    mul-double/2addr v0, v13

    double-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_7e
    .catch Ljava/lang/NumberFormatException; {:try_start_31 .. :try_end_7e} :catch_ee

    move-result-object v3

    .line 675
    const/4 v9, 0x1

    .line 692
    .end local v10           #latitude:D
    .end local v13           #longitude:D
    :cond_80
    :goto_80
    const-string v0, "cid"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 693
    const-string v0, "cid"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 694
    const/4 v9, 0x1

    .line 697
    :cond_8f
    const-string v0, "location_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9e

    .line 698
    const-string v0, "location_name"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 699
    const/4 v9, 0x1

    .line 702
    :cond_9e
    const-string v0, "address"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ad

    .line 703
    const-string v0, "address"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 704
    const/4 v9, 0x1

    .line 707
    :cond_ad
    if-eqz v9, :cond_124

    .line 708
    new-instance v0, Lcom/google/android/apps/plus/content/DbLocation;

    const/4 v1, 0x3

    const-wide/16 v7, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/content/DbLocation;-><init>(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    move-object v12, v0

    goto/16 :goto_a

    .line 677
    .restart local v10       #latitude:D
    .restart local v13       #longitude:D
    :cond_ba
    :try_start_ba
    const-string v0, "PostFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 678
    const-string v0, "PostFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "Provided latitude/longitude are out of range. latitude: "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "latitude"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", longitude: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "longitude"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_ed
    .catch Ljava/lang/NumberFormatException; {:try_start_ba .. :try_end_ed} :catch_ee

    goto :goto_80

    .line 684
    .end local v10           #latitude:D
    .end local v13           #longitude:D
    :catch_ee
    move-exception v0

    const-string v0, "PostFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 685
    const-string v0, "PostFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v7, "Can\'t parse latitude/longitude extras. latitude: "

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "latitude"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", longitude: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "longitude"

    invoke-virtual {p0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_80

    .line 712
    .end local v2           #latitudeE7:Ljava/lang/Integer;
    .end local v3           #longitudeE7:Ljava/lang/Integer;
    .end local v4           #name:Ljava/lang/String;
    .end local v5           #bestAddress:Ljava/lang/String;
    .end local v6           #clusterId:Ljava/lang/String;
    .end local v9           #addedFields:Z
    :cond_124
    const/4 v12, 0x0

    goto/16 :goto_a
.end method

.method private handlePostResult(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 8
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v4, 0x0

    .line 1590
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_e

    .line 1613
    :cond_d
    :goto_d
    return-void

    .line 1595
    :cond_e
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    .line 1597
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1598
    .local v0, activity:Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "req_pending"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 1600
    .local v1, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v1, :cond_26

    .line 1601
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 1605
    :cond_26
    if-eqz p2, :cond_3d

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 1606
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f080196

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_d

    .line 1609
    :cond_3d
    const v2, 0x7f08017f

    invoke-static {v0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1610
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string v3, "streams"

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "want_sharebox_locations"

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v3, "city_level_sharebox_location"

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getCityLevelLocationPreference()Z

    move-result v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1611
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setResult(I)V

    .line 1612
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_d
.end method

.method private handlePreviewResult(Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/network/ApiaryActivity;)V
    .registers 7
    .parameter "result"
    .parameter "activity"

    .prologue
    .line 837
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_30

    .line 838
    const-string v1, "PostFragment"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_27

    .line 839
    const-string v1, "PostFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not retrieve preview: errorCode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    :cond_27
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/16 v2, 0x6e27

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->showDialog(I)V

    .line 844
    :cond_30
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/network/ApiaryActivity;

    .line 845
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 846
    .local v0, mainHandler:Landroid/os/Handler;
    new-instance v1, Lcom/google/android/apps/plus/fragments/PostFragment$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 852
    return-void
.end method

.method private hasDeepLinkMetadata()Z
    .registers 3

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkMetadata:Landroid/os/Bundle;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkMetadata:Landroid/os/Bundle;

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    const/4 v0, 0x1

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    goto :goto_13
.end method

.method private insertCameraPhoto(Ljava/lang/String;)V
    .registers 10
    .parameter "mediaLocation"

    .prologue
    .line 1385
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    .line 1387
    .local v7, activity:Landroid/app/Activity;
    if-eqz p1, :cond_33

    .line 1388
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1389
    .local v5, photoUri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    sget-object v6, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    .line 1391
    .local v0, mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    .line 1392
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1393
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateResultMediaItems()V

    .line 1399
    .end local v0           #mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    .end local v5           #photoUri:Landroid/net/Uri;
    :goto_29
    instance-of v1, v7, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    if-eqz v1, :cond_32

    .line 1400
    check-cast v7, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    .end local v7           #activity:Landroid/app/Activity;
    invoke-interface {v7}, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;->hideInsertCameraPhotoDialog()V

    .line 1402
    :cond_32
    return-void

    .line 1395
    .restart local v7       #activity:Landroid/app/Activity;
    :cond_33
    const v1, 0x7f080167

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v7, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_29
.end method

.method private removeFromMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V
    .registers 3
    .parameter "mediaRef"

    .prologue
    .line 1692
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1693
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->access$2800(Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;Lcom/google/android/apps/plus/api/MediaRef;)V

    .line 1694
    return-void
.end method

.method private removeLocationListener()V
    .registers 2

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    if-eqz v0, :cond_c

    .line 1628
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/LocationController;->release()V

    .line 1629
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationController:Lcom/google/android/apps/plus/phone/LocationController;

    .line 1631
    :cond_c
    return-void
.end method

.method private static setLocationText(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "view"
    .parameter "titleText"
    .parameter "hintText"
    .parameter "centerText"

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 1239
    const v3, 0x1020016

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1240
    .local v2, title:Landroid/widget/TextView;
    if-nez p1, :cond_2f

    .line 1241
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1246
    :goto_10
    const v3, 0x1020005

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1247
    .local v1, hint:Landroid/widget/TextView;
    if-nez p2, :cond_36

    .line 1248
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1253
    :goto_1e
    const v3, 0x7f0901ac

    invoke-virtual {p0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1254
    .local v0, centered:Landroid/widget/TextView;
    if-nez p3, :cond_3d

    .line 1255
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1260
    :goto_2e
    return-void

    .line 1243
    .end local v0           #centered:Landroid/widget/TextView;
    .end local v1           #hint:Landroid/widget/TextView;
    :cond_2f
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1244
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_10

    .line 1250
    .restart local v1       #hint:Landroid/widget/TextView;
    :cond_36
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1251
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1e

    .line 1257
    .restart local v0       #centered:Landroid/widget/TextView;
    :cond_3d
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1258
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2e
.end method

.method private updateLocation(Landroid/view/View;)V
    .registers 10
    .parameter "view"

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1201
    const v4, 0x7f0901ad

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1202
    .local v3, progress:Landroid/view/View;
    const v4, 0x7f090094

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1203
    .local v1, marker:Landroid/widget/ImageView;
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    if-eqz v4, :cond_5e

    .line 1204
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v4, :cond_4d

    .line 1205
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1206
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1207
    const v4, 0x7f0200ca

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1209
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbLocation;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1210
    .local v2, name:Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbLocation;->getBestAddress()Ljava/lang/String;

    move-result-object v0

    .line 1211
    .local v0, bestAddress:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_43

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_43

    .line 1212
    invoke-static {p1, v2, v0, v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationText(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    .end local v0           #bestAddress:Ljava/lang/String;
    .end local v2           #name:Ljava/lang/String;
    :goto_42
    return-void

    .line 1214
    .restart local v0       #bestAddress:Ljava/lang/String;
    .restart local v2       #name:Ljava/lang/String;
    :cond_43
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v5, v5, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationText(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_42

    .line 1217
    .end local v0           #bestAddress:Ljava/lang/String;
    .end local v2           #name:Ljava/lang/String;
    :cond_4d
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1218
    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1219
    const v4, 0x7f08016c

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v5, v5, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationText(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_42

    .line 1222
    :cond_5e
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1223
    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1224
    const v4, 0x7f020098

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1225
    const v4, 0x7f08016d

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v5, v5, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationText(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_42
.end method

.method private updatePostUI()V
    .registers 3

    .prologue
    .line 1179
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1180
    .local v0, activity:Landroid/app/Activity;
    instance-of v1, v0, Lcom/google/android/apps/plus/phone/PostActivity;

    if-eqz v1, :cond_e

    .line 1181
    check-cast v0, Lcom/google/android/apps/plus/phone/PostActivity;

    .end local v0           #activity:Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PostActivity;->invalidateMenu()V

    .line 1185
    :cond_d
    :goto_d
    return-void

    .line 1182
    .restart local v0       #activity:Landroid/app/Activity;
    :cond_e
    instance-of v1, v0, Lcom/google/android/apps/plus/phone/ShareActivity;

    if-eqz v1, :cond_d

    .line 1183
    check-cast v0, Lcom/google/android/apps/plus/phone/ShareActivity;

    .end local v0           #activity:Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShareActivity;->invalidateMenu()V

    goto :goto_d
.end method

.method private updatePreviewContainer$3c7ec8c3()V
    .registers 5

    .prologue
    const/4 v1, 0x0

    .line 1576
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1578
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/network/ApiaryActivity;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/views/ActivityPreviewViewFactory;->createViewFromActivity(Landroid/content/Context;Lcom/google/android/apps/plus/network/ApiaryActivity;)Lcom/google/android/apps/plus/views/ActivityPreviewView;

    move-result-object v0

    .line 1580
    .local v0, preview:Landroid/view/View;
    if-eqz v0, :cond_17

    .line 1581
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1584
    :cond_17
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1585
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1586
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    if-eqz v3, :cond_2b

    :goto_27
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1587
    return-void

    .line 1586
    :cond_2b
    const/16 v1, 0x8

    goto :goto_27
.end method

.method private updateResultMediaItems()V
    .registers 9

    .prologue
    const/16 v6, 0xfa

    const/4 v7, 0x1

    .line 1760
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    if-eqz v2, :cond_44

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_44

    .line 1761
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v2, v3

    if-le v2, v6, :cond_45

    .line 1762
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f080139

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1770
    :cond_39
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1771
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    .line 1772
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    .line 1774
    :cond_44
    return-void

    .line 1765
    :cond_45
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_4b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_39

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/MediaRef;

    .line 1766
    .local v1, mediaRef:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->addToMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V

    goto :goto_4b
.end method

.method private updateViews(Landroid/view/View;)V
    .registers 20
    .parameter "view"

    .prologue
    .line 1520
    if-eqz p1, :cond_fb

    .line 1521
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v13

    const-string v14, "is_internal"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    if-nez v13, :cond_fc

    .line 1522
    const v13, 0x7f09019e

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 1523
    const v13, 0x7f0901a0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1524
    .local v8, name:Landroid/widget/TextView;
    const v13, 0x7f09019f

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/AvatarView;

    .line 1525
    .local v5, avatar:Lcom/google/android/apps/plus/views/AvatarView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v5, v13}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    .line 1526
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f080181

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v13

    invoke-virtual {v8, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1531
    .end local v5           #avatar:Lcom/google/android/apps/plus/views/AvatarView;
    .end local v8           #name:Landroid/widget/TextView;
    :goto_64
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v11

    .line 1533
    .local v11, sourceInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    if-eqz v11, :cond_ce

    .line 1534
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v10

    .line 1535
    .local v10, pkgName:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-virtual {v13}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 1536
    .local v9, pkgManager:Landroid/content/pm/PackageManager;
    const v13, 0x7f0901a1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1537
    .local v4, appView:Landroid/view/View;
    const v13, 0x7f0901a2

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1538
    .local v1, appIcon:Landroid/widget/ImageView;
    const v13, 0x7f0901a3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1542
    .local v3, appText:Landroid/widget/TextView;
    :try_start_a1
    invoke-virtual {v9, v10}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    invoke-virtual {v1, v13}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1544
    const/4 v13, 0x0

    invoke-virtual {v9, v10, v13}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1546
    .local v2, appName:Ljava/lang/CharSequence;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f080182

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v2, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1547
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Landroid/view/View;->setVisibility(I)V
    :try_end_ce
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a1 .. :try_end_ce} :catch_10c

    .line 1555
    .end local v1           #appIcon:Landroid/widget/ImageView;
    .end local v2           #appName:Ljava/lang/CharSequence;
    .end local v3           #appText:Landroid/widget/TextView;
    .end local v4           #appView:Landroid/view/View;
    .end local v9           #pkgManager:Landroid/content/pm/PackageManager;
    .end local v10           #pkgName:Ljava/lang/String;
    :cond_ce
    :goto_ce
    const v13, 0x7f0901a7

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1556
    .local v7, footerSep:Landroid/view/View;
    const v13, 0x7f0901a8

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1558
    .local v6, footer:Landroid/widget/TextView;
    const/16 v12, 0x8

    .line 1559
    .local v12, visibility:I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    if-eqz v13, :cond_f2

    .line 1560
    const/4 v12, 0x0

    .line 1561
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1563
    :cond_f2
    invoke-virtual {v7, v12}, Landroid/view/View;->setVisibility(I)V

    .line 1564
    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1566
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer$3c7ec8c3()V

    .line 1568
    .end local v6           #footer:Landroid/widget/TextView;
    .end local v7           #footerSep:Landroid/view/View;
    .end local v11           #sourceInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    .end local v12           #visibility:I
    :cond_fb
    return-void

    .line 1528
    :cond_fc
    const v13, 0x7f09019e

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_64

    .line 1551
    .restart local v1       #appIcon:Landroid/widget/ImageView;
    .restart local v3       #appText:Landroid/widget/TextView;
    .restart local v4       #appView:Landroid/view/View;
    .restart local v9       #pkgManager:Landroid/content/pm/PackageManager;
    .restart local v10       #pkgName:Ljava/lang/String;
    .restart local v11       #sourceInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;
    :catch_10c
    move-exception v13

    const/16 v13, 0x8

    invoke-virtual {v4, v13}, Landroid/view/View;->setVisibility(I)V

    goto :goto_ce
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 950
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 951
    if-nez p1, :cond_35

    .line 952
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v3, :cond_36

    move v0, v1

    .line 953
    .local v0, hasLocation:Z
    :goto_c
    if-nez v0, :cond_28

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-string v4, "streams"

    invoke-virtual {v3, v4, v2}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v4, "want_sharebox_locations"

    invoke-interface {v3, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_38

    const-string v4, "want_sharebox_locations"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    :goto_26
    if-eqz v3, :cond_3f

    :cond_28
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3f

    :goto_32
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationChecked(Z)V

    .line 956
    .end local v0           #hasLocation:Z
    :cond_35
    return-void

    :cond_36
    move v0, v2

    .line 952
    goto :goto_c

    .line 953
    .restart local v0       #hasLocation:Z
    :cond_38
    const-string v4, "want_locations"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    goto :goto_26

    :cond_3f
    move v1, v2

    goto :goto_32
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 15
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 1325
    const/4 v8, -0x1

    if-eq p2, v8, :cond_4

    .line 1378
    :cond_3
    :goto_3
    return-void

    .line 1328
    :cond_4
    packed-switch p1, :pswitch_data_9e

    goto :goto_3

    .line 1356
    :pswitch_8
    if-eqz p3, :cond_3

    .line 1357
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    .line 1358
    const-string v8, "mediarefs"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 1360
    .local v6, mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1362
    .local v7, mediaRefsCount:I
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->clearMediaGallery()V

    .line 1363
    const/4 v3, 0x0

    .local v3, i:I
    :goto_1f
    if-ge v3, v7, :cond_3

    .line 1364
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultMediaItems:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1363
    add-int/lit8 v3, v3, 0x1

    goto :goto_1f

    .line 1330
    .end local v3           #i:I
    .end local v6           #mediaRefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/api/MediaRef;>;"
    .end local v7           #mediaRefsCount:I
    :pswitch_2d
    if-eqz p3, :cond_3

    .line 1331
    const-string v8, "audience"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 1333
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v8, :cond_3

    .line 1334
    const-string v8, "PostFragment"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1335
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v1

    .local v1, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v5, v1

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_4e
    if-ge v4, v5, :cond_3

    aget-object v2, v1, v4

    .line 1336
    .local v2, circle:Lcom/google/android/apps/plus/content/CircleData;
    const-string v8, "PostFragment"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Out circle id: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1335
    add-int/lit8 v4, v4, 0x1

    goto :goto_4e

    .line 1345
    .end local v1           #arr$:[Lcom/google/android/apps/plus/content/CircleData;
    .end local v2           #circle:Lcom/google/android/apps/plus/content/CircleData;
    .end local v4           #i$:I
    .end local v5           #len$:I
    :pswitch_6d
    if-eqz p3, :cond_3

    .line 1346
    const-string v8, "location"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    .line 1350
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-nez v8, :cond_81

    const/4 v8, 0x1

    :goto_7e
    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    goto :goto_3

    :cond_81
    const/4 v8, 0x0

    goto :goto_7e

    .line 1371
    :pswitch_83
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 1372
    .local v0, activity:Landroid/app/Activity;
    instance-of v8, v0, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    if-eqz v8, :cond_91

    move-object v8, v0

    .line 1373
    check-cast v8, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;

    invoke-interface {v8}, Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;->showInsertCameraPhotoDialog()V

    .line 1376
    :cond_91
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v9, "camera-post.jpg"

    invoke-static {v0, v8, v9}, Lcom/google/android/apps/plus/service/EsService;->insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    goto/16 :goto_3

    .line 1328
    nop

    :pswitch_data_9e
    .packed-switch 0x1
        :pswitch_8
        :pswitch_83
        :pswitch_2d
        :pswitch_6d
    .end packed-switch
.end method

.method public final onContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter "item"

    .prologue
    .line 1159
    invoke-interface {p1}, Landroid/view/MenuItem;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "viewId"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 1160
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    .line 1172
    :goto_10
    return v2

    .line 1163
    :cond_11
    invoke-interface {p1}, Landroid/view/MenuItem;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "viewId"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1165
    .local v1, viewId:I
    const v2, 0x7f0901a5

    if-ne v1, v2, :cond_32

    .line 1166
    invoke-interface {p1}, Landroid/view/MenuItem;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    .line 1167
    .local v0, ref:Lcom/google/android/apps/plus/api/MediaRef;
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeFromMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V

    .line 1172
    .end local v0           #ref:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_30
    :goto_30
    const/4 v2, 0x1

    goto :goto_10

    .line 1168
    :cond_32
    const v2, 0x7f0901a6

    if-ne v1, v2, :cond_30

    .line 1169
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/network/ApiaryActivity;

    .line 1170
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer$3c7ec8c3()V

    goto :goto_30
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 15
    .parameter "savedInstanceState"

    .prologue
    .line 476
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 478
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v10

    .line 479
    .local v10, args:Landroid/os/Bundle;
    const-string v0, "account"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 481
    if-nez p1, :cond_1ad

    .line 484
    const-string v0, "external_id"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 485
    const-string v0, "external_id"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    .line 487
    :cond_23
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    if-nez v0, :cond_4a

    .line 496
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->randomString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    .line 499
    :cond_4a
    invoke-static {v10}, Lcom/google/android/apps/plus/fragments/PostFragment;->getLocationFromExtras(Landroid/os/Bundle;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    .line 501
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    .line 502
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_77

    .line 503
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachments:Ljava/util/ArrayList;

    .line 504
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x7f090028

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaRefLoaderCallbacks:Landroid/support/v4/app/LoaderManager$LoaderCallbacks;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 506
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    .line 509
    :cond_77
    const-string v0, "url"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_87

    .line 510
    const-string v0, "url"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    .line 513
    :cond_87
    const-string v0, "deep_link_id"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_97

    .line 514
    const-string v0, "deep_link_id"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkId:Ljava/lang/String;

    .line 517
    :cond_97
    const-string v0, "deep_link_metadata"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a7

    .line 518
    const-string v0, "deep_link_metadata"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkMetadata:Landroid/os/Bundle;

    .line 521
    :cond_a7
    const-string v0, "footer"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b7

    .line 522
    const-string v0, "footer"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    .line 525
    :cond_b7
    const-string v0, "api_info"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_165

    .line 526
    const-string v0, "api_info"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    .line 531
    :goto_c9
    const-string v0, "audience"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 532
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-nez v0, :cond_e6

    .line 533
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;B)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 537
    :cond_e6
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_137

    .line 538
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v10, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    .line 540
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    if-nez v0, :cond_137

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    if-eqz v0, :cond_137

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-nez v0, :cond_137

    .line 541
    new-instance v12, Landroid/text/SpannableString;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    invoke-direct {v12, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 542
    .local v12, string:Landroid/text/SpannableString;
    const/4 v0, 0x1

    invoke-static {v12, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    .line 543
    const/4 v0, 0x0

    invoke-virtual {v12}, Landroid/text/SpannableString;->length()I

    move-result v1

    const-class v2, Landroid/text/style/URLSpan;

    invoke-virtual {v12, v0, v1, v2}, Landroid/text/SpannableString;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Landroid/text/style/URLSpan;

    .line 544
    .local v11, spans:[Landroid/text/style/URLSpan;
    array-length v0, v11

    if-lez v0, :cond_137

    .line 545
    const/4 v0, 0x0

    aget-object v0, v11, v0

    invoke-virtual {v0}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    .line 548
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_137

    .line 549
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    .line 555
    .end local v11           #spans:[Landroid/text/style/URLSpan;
    .end local v12           #string:Landroid/text/SpannableString;
    :cond_137
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkId:Ljava/lang/String;

    if-eqz v0, :cond_1a3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-nez v0, :cond_1a3

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->hasDeepLinkMetadata()Z

    move-result v0

    if-nez v0, :cond_1a3

    .line 556
    const-string v0, "PostFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_155

    .line 557
    const-string v0, "PostFragment"

    const-string v1, "Mobile deep-link IDs must specify metadata."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :cond_155
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 560
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 623
    :goto_164
    return-void

    .line 528
    :cond_165
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    const/4 v1, 0x0

    const-string v3, "com.google.android.apps.social"

    const-string v4, "com.google.android.apps.social"

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    new-instance v3, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    const/4 v4, 0x0

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    move-object v5, v2

    move-object v9, v0

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/network/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/ApiaryApiInfo;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    goto/16 :goto_c9

    .line 564
    :cond_1a3
    const-string v0, "is_from_plusone"

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    goto :goto_164

    .line 567
    :cond_1ad
    const-string v0, "activity_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    .line 569
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1ca

    .line 570
    const-string v0, "location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    .line 571
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    .line 574
    :cond_1ca
    const-string v0, "prov_location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1dc

    .line 575
    const-string v0, "prov_location"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    .line 579
    :cond_1dc
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f0

    .line 580
    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    .line 583
    :cond_1f0
    const-string v0, "insert_camera_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_204

    .line 584
    const-string v0, "insert_camera_photo_req_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    .line 588
    :cond_204
    const-string v0, "preview_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_216

    .line 589
    const-string v0, "preview_result"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/network/ApiaryActivity;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/network/ApiaryActivity;

    .line 592
    :cond_216
    const-string v0, "api_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_228

    .line 593
    const-string v0, "api_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    .line 596
    :cond_228
    const-string v0, "footer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_238

    .line 597
    const-string v0, "footer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    .line 600
    :cond_238
    const-string v0, "l_attachments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    .line 602
    const-string v0, "loading_attachments"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    .line 605
    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_259

    .line 606
    const-string v0, "url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    .line 609
    :cond_259
    const-string v0, "deep_link_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_269

    .line 610
    const-string v0, "deep_link_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkId:Ljava/lang/String;

    .line 613
    :cond_269
    const-string v0, "deep_link_metadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_279

    .line 614
    const-string v0, "deep_link_metadata"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkMetadata:Landroid/os/Bundle;

    .line 617
    :cond_279
    const-string v0, "text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_289

    .line 618
    const-string v0, "text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    .line 621
    :cond_289
    const-string v0, "is_from_plusone"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    goto/16 :goto_164
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 11
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    .prologue
    .line 1133
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    if-ne p2, v5, :cond_53

    .line 1134
    const/4 v4, 0x0

    .line 1135
    .local v4, ref:Lcom/google/android/apps/plus/api/MediaRef;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_6
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_20

    .line 1136
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1137
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isPressed()Z

    move-result v5

    if-eqz v5, :cond_54

    .line 1138
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    .end local v4           #ref:Lcom/google/android/apps/plus/api/MediaRef;
    check-cast v4, Lcom/google/android/apps/plus/api/MediaRef;

    .line 1143
    .end local v0           #child:Landroid/view/View;
    .restart local v4       #ref:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_20
    if-eqz v4, :cond_53

    .line 1144
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 1145
    .local v2, inflater:Landroid/view/MenuInflater;
    const v5, 0x7f10001a

    invoke-virtual {v2, v5, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1147
    const v5, 0x7f080198

    invoke-interface {p1, v5}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 1148
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1150
    .local v3, intent:Landroid/content/Intent;
    const-string v5, "viewId"

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1151
    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1152
    const v5, 0x7f0902d2

    invoke-interface {p1, v5}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v3}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    .line 1155
    .end local v1           #i:I
    .end local v2           #inflater:Landroid/view/MenuInflater;
    .end local v3           #intent:Landroid/content/Intent;
    .end local v4           #ref:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_53
    return-void

    .line 1135
    .restart local v0       #child:Landroid/view/View;
    .restart local v1       #i:I
    .restart local v4       #ref:Lcom/google/android/apps/plus/api/MediaRef;
    :cond_54
    add-int/lit8 v1, v1, 0x1

    goto :goto_6
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 13
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    const v8, 0x7f0901a6

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 857
    const v2, 0x7f030093

    invoke-virtual {p1, v2, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 859
    .local v1, view:Landroid/view/View;
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 861
    const v2, 0x7f090066

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;

    .line 862
    const v2, 0x7f0901a5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    .line 864
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_33

    .line 865
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 868
    :cond_33
    new-instance v2, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGalleryView:Landroid/view/ViewGroup;

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/ViewGroup;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mMediaGallery:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    .line 870
    const v2, 0x7f090052

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/AudienceView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    .line 871
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    new-instance v3, Lcom/google/android/apps/plus/fragments/PostFragment$5;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/AudienceView;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    .line 879
    const v2, 0x7f09019d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 880
    const v2, 0x7f0901a4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 881
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/apps/plus/phone/ShareActivity;

    if-nez v2, :cond_89

    .line 882
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0005

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setMinLines(I)V

    .line 885
    :cond_89
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "start_editing"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_ac

    if-nez p3, :cond_ac

    .line 887
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 889
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->requestFocus()Z

    .line 891
    :cond_ac
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2, p0, v3, v4, v5}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    .line 892
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 893
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 894
    if-nez p3, :cond_cd

    .line 896
    :try_start_c6
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_cd
    .catch Ljava/lang/Exception; {:try_start_c6 .. :try_end_cd} :catch_16b

    .line 901
    :cond_cd
    :goto_cd
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v3, Lcom/google/android/apps/plus/fragments/PostFragment$6;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/PostFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 916
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/AudienceView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 918
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 919
    const v2, 0x7f0901ab

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 920
    const v2, 0x7f0901af

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 921
    const v2, 0x7f0901b0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 923
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewContainerView:Landroid/view/ViewGroup;

    .line 926
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_133

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    if-nez v2, :cond_133

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-eqz v2, :cond_133

    .line 927
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x3

    sget-object v4, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    new-instance v5, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;

    invoke-direct {v5, p0, v6}, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;B)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 929
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    .line 932
    :cond_133
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkId:Ljava/lang/String;

    if-eqz v2, :cond_14f

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-nez v2, :cond_14f

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->hasDeepLinkMetadata()Z

    move-result v2

    if-eqz v2, :cond_14f

    .line 933
    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v2}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkMetadata:Landroid/os/Bundle;

    invoke-static {v3}, Lcom/google/android/apps/plus/network/ApiaryActivityFactory;->getApiaryActivity(Landroid/os/Bundle;)Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->handlePreviewResult(Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/network/ApiaryActivity;)V

    .line 937
    :cond_14f
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-nez v2, :cond_157

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkId:Ljava/lang/String;

    if-eqz v2, :cond_161

    .line 938
    :cond_157
    const v2, 0x7f0901ae

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 941
    :cond_161
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V

    .line 942
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    .line 943
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateViews(Landroid/view/View;)V

    .line 945
    return-object v1

    .line 897
    :catch_16b
    move-exception v0

    .line 898
    .local v0, ex:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_cd
.end method

.method public final onDestroyView()V
    .registers 2

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->destroy()V

    .line 1001
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 1002
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 1003
    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 1317
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 1321
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1313
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 7
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 1303
    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 1304
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    if-eqz v0, :cond_23

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_18
    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 1307
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1309
    :cond_22
    return-void

    .line 1304
    :cond_23
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_18
.end method

.method public final onDiscard()V
    .registers 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1284
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 1285
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    if-eqz v1, :cond_4d

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    :goto_17
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_50

    move v1, v2

    :goto_1e
    if-eqz v1, :cond_5c

    .line 1286
    const v1, 0x7f080029

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f08019a

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v4, 0x7f0801c6

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801c8

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 1290
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {v0, p0, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 1291
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1299
    .end local v0           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :goto_4c
    return-void

    .line 1285
    :cond_4d
    const-string v1, ""

    goto :goto_17

    :cond_50
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5a

    move v1, v2

    goto :goto_1e

    :cond_5a
    move v1, v3

    goto :goto_1e

    .line 1293
    :cond_5c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    if-eqz v1, :cond_77

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_6c
    invoke-static {v2, v3, v4, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 1297
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_4c

    .line 1293
    :cond_77
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CANCEL_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_6c
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 1065
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 1066
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 1067
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeLocationListener()V

    .line 1068
    return-void
.end method

.method public final onResume()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 1007
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 1009
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 1011
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_1d

    .line 1012
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/AudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    .line 1013
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    .line 1014
    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 1017
    :cond_1d
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    if-eqz v1, :cond_40

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_40

    .line 1018
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 1019
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->handlePostResult(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 1022
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_40
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_62

    .line 1023
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_62

    .line 1024
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    .line 1025
    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->getLastCameraMediaLocation()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->insertCameraPhoto(Ljava/lang/String;)V

    .line 1026
    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    .line 1031
    :cond_62
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_b9

    .line 1032
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    if-eqz v1, :cond_86

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenLocationDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-nez v1, :cond_86

    .line 1034
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v3, 0x1d71d84

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentActivity;->showDialog(I)V

    .line 1037
    :cond_86
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    if-eqz v1, :cond_91

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-nez v1, :cond_91

    .line 1038
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->addLocationListener()V

    .line 1042
    :cond_91
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-nez v1, :cond_99

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    if-eqz v1, :cond_a9

    .line 1043
    :cond_99
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    .line 1044
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    if-nez v1, :cond_b7

    const/4 v1, 0x1

    :goto_a2
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationChecked(Z)V

    .line 1045
    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    .line 1046
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    .line 1057
    :cond_a9
    :goto_a9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V

    .line 1058
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    .line 1060
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateResultMediaItems()V

    .line 1061
    return-void

    :cond_b7
    move v1, v2

    .line 1044
    goto :goto_a2

    .line 1050
    :cond_b9
    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mResultLocation:Lcom/google/android/apps/plus/content/DbLocation;

    .line 1051
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mRemoveLocation:Z

    .line 1054
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationChecked(Z)V

    goto :goto_a9
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 1072
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1074
    const-string v0, "activity_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1076
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v0, :cond_15

    .line 1077
    const-string v0, "location"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1080
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    if-eqz v0, :cond_20

    .line 1081
    const-string v0, "prov_location"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1084
    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    if-eqz v0, :cond_2f

    .line 1085
    const-string v0, "pending_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1088
    :cond_2f
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/network/ApiaryActivity;

    if-eqz v0, :cond_3a

    .line 1089
    const-string v0, "preview_result"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/network/ApiaryActivity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1092
    :cond_3a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    if-eqz v0, :cond_45

    .line 1093
    const-string v0, "api_info"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1096
    :cond_45
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    if-eqz v0, :cond_50

    .line 1097
    const-string v0, "footer"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mFooterMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1100
    :cond_50
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    if-eqz v0, :cond_5b

    .line 1101
    const-string v0, "l_attachments"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1104
    :cond_5b
    const-string v0, "loading_attachments"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingMediaAttachments:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1106
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_6d

    .line 1107
    const-string v0, "url"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    :cond_6d
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkId:Ljava/lang/String;

    if-eqz v0, :cond_78

    .line 1111
    const-string v0, "deep_link_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1114
    :cond_78
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkMetadata:Landroid/os/Bundle;

    if-eqz v0, :cond_83

    .line 1115
    const-string v0, "deep_link_metadata"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkMetadata:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 1118
    :cond_83
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    if-eqz v0, :cond_8e

    .line 1119
    const-string v0, "text"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mOriginalText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    :cond_8e
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_9d

    .line 1123
    const-string v0, "insert_camera_photo_req_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mInsertCameraPhotoRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1126
    :cond_9d
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    if-eqz v0, :cond_a7

    .line 1127
    const-string v0, "is_from_plusone"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1129
    :cond_a7
    return-void
.end method

.method public final post()Z
    .registers 21

    .prologue
    .line 1410
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    if-nez v2, :cond_c

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingUrlPreview:Z

    if-eqz v2, :cond_e

    .line 1411
    :cond_c
    const/4 v2, 0x0

    .line 1489
    :goto_d
    return v2

    .line 1414
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mIsFromPlusOne:Z

    if-eqz v2, :cond_4f

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE_FROM_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_22
    invoke-static {v3, v4, v5, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 1419
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 1421
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    .line 1422
    .local v13, activity:Landroid/app/Activity;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v7

    .line 1423
    .local v7, audience:Lcom/google/android/apps/plus/content/AudienceData;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mCommentsView:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v19

    .line 1426
    .local v19, spannable:Landroid/text/Spannable;
    invoke-static {v7}, Lcom/google/android/apps/plus/util/PeopleUtils;->isEmpty(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v2

    if-eqz v2, :cond_52

    .line 1427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/AudienceView;->performClick()Z

    .line 1428
    const/4 v2, 0x0

    goto :goto_d

    .line 1414
    .end local v7           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v13           #activity:Landroid/app/Activity;
    .end local v19           #spannable:Landroid/text/Spannable;
    :cond_4f
    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CONFIRM_SHARE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_22

    .line 1432
    .restart local v7       #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .restart local v13       #activity:Landroid/app/Activity;
    .restart local v19       #spannable:Landroid/text/Spannable;
    :cond_52
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;

    if-eqz v2, :cond_9c

    const/16 v17, 0x1

    .line 1433
    .local v17, hasLink:Z
    :goto_5a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkId:Ljava/lang/String;

    if-eqz v2, :cond_9f

    const/16 v16, 0x1

    .line 1434
    .local v16, hasDeepLink:Z
    :goto_62
    invoke-interface/range {v19 .. v19}, Landroid/text/Spannable;->length()I

    move-result v2

    if-lez v2, :cond_a2

    const/4 v15, 0x1

    .line 1435
    .local v15, hasComment:Z
    :goto_69
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    if-eqz v2, :cond_a4

    const/16 v18, 0x1

    .line 1436
    .local v18, hasLocation:Z
    :goto_71
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a7

    const/4 v14, 0x1

    .line 1437
    .local v14, hasAttachmentRefs:Z
    :goto_7c
    if-nez v17, :cond_a9

    if-nez v16, :cond_a9

    if-nez v15, :cond_a9

    if-nez v18, :cond_a9

    if-nez v14, :cond_a9

    .line 1438
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080180

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v13, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1440
    const/4 v2, 0x0

    goto/16 :goto_d

    .line 1432
    .end local v14           #hasAttachmentRefs:Z
    .end local v15           #hasComment:Z
    .end local v16           #hasDeepLink:Z
    .end local v17           #hasLink:Z
    .end local v18           #hasLocation:Z
    :cond_9c
    const/16 v17, 0x0

    goto :goto_5a

    .line 1433
    .restart local v17       #hasLink:Z
    :cond_9f
    const/16 v16, 0x0

    goto :goto_62

    .line 1434
    .restart local v16       #hasDeepLink:Z
    :cond_a2
    const/4 v15, 0x0

    goto :goto_69

    .line 1435
    .restart local v15       #hasComment:Z
    :cond_a4
    const/16 v18, 0x0

    goto :goto_71

    .line 1436
    .restart local v18       #hasLocation:Z
    :cond_a7
    const/4 v14, 0x0

    goto :goto_7c

    .line 1443
    .restart local v14       #hasAttachmentRefs:Z
    :cond_a9
    const/4 v2, 0x0

    const v3, 0x7f080164

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "req_pending"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 1445
    if-eqz v14, :cond_d4

    .line 1446
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_ATTACHMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 1450
    :cond_d4
    if-eqz v15, :cond_e7

    .line 1451
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 1455
    :cond_e7
    if-eqz v18, :cond_fa

    .line 1456
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 1460
    :cond_fa
    if-eqz v17, :cond_10d

    .line 1461
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_URL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 1465
    :cond_10d
    if-eqz v16, :cond_120

    .line 1466
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_WITH_DEEP_LINK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 1470
    :cond_120
    new-instance v1, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzViews;->SHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->PLATFORM_THIRD_PARTY_APP:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    invoke-static {v6}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/network/ApiaryApiInfo;)Ljava/util/Map;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;Lcom/google/android/apps/plus/analytics/OzViews;JLjava/util/Map;)V

    .line 1473
    .local v1, analytics:Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v9

    .line 1474
    .local v9, comment:Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_154

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_COMMENT_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    :cond_154
    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v2

    if-lez v2, :cond_16b

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CIRCLES_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    :cond_16b
    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v2

    if-lez v2, :cond_182

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getAnalyticsInfo()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_PEOPLE_SHARE_ACL_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    .line 1475
    :cond_182
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/network/ApiaryApiInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPreviewResult:Lcom/google/android/apps/plus/network/ApiaryActivity;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mActivityId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mDeepLinkId:Ljava/lang/String;

    move-object v4, v1

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/service/EsService;->postActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/network/ApiaryApiInfo;Lcom/google/android/apps/plus/network/ApiaryActivity;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/PostFragment;->mPendingPostId:Ljava/lang/Integer;

    .line 1479
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/google/android/apps/plus/fragments/PostFragment$7;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v7}, Lcom/google/android/apps/plus/fragments/PostFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 1489
    const/4 v2, 0x1

    goto/16 :goto_d
.end method

.method public final setLocationChecked(Z)V
    .registers 5
    .parameter "checked"

    .prologue
    const/4 v2, 0x0

    .line 1644
    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    .line 1645
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocationChecked:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v0, :cond_3a

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->isProviderEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_22

    const v0, 0x1bfb7a8

    invoke-virtual {v1, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_17
    :goto_17
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    .line 1646
    return-void

    .line 1645
    :cond_22
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->addLocationListener()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenLocationDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_17

    const v0, 0x1d71d84

    invoke-virtual {v1, v0}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_17

    :cond_3a
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment;->removeLocationListener()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;

    goto :goto_17
.end method
