.class public Lcom/google/android/apps/plus/fragments/ReshareFragment;
.super Lcom/google/android/apps/plus/fragments/AudienceFragment;
.source "ReshareFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/ReshareFragment$ServiceListener;,
        Lcom/google/android/apps/plus/fragments/ReshareFragment$PlatformAudienceQuery;,
        Lcom/google/android/apps/plus/fragments/ReshareFragment$AccountStatusQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/AudienceFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;"
    }
.end annotation


# static fields
.field private static sAuthorBitmap:Landroid/graphics/Bitmap;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActivityId:Ljava/lang/String;

.field private mAvatarView:Landroid/widget/ImageView;

.field private mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mLimited:Z

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mReshareInfo:Landroid/widget/TextView;

.field private mScrollView:Landroid/widget/ScrollView;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private final mTextWatcher:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;-><init>()V

    .line 97
    new-instance v0, Lcom/google/android/apps/plus/fragments/ReshareFragment$ServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/ReshareFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    .line 120
    new-instance v0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/ReshareFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mTextWatcher:Landroid/text/TextWatcher;

    .line 160
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Landroid/widget/ScrollView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/ReshareFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 11
    .parameter "requestId"
    .parameter "result"

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 284
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, p1, :cond_f

    .line 314
    :cond_e
    :goto_e
    return-void

    .line 288
    :cond_f
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 290
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "req_pending"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/DialogFragment;

    .line 292
    .local v1, dialog:Landroid/support/v4/app/DialogFragment;
    if-eqz v1, :cond_22

    .line 293
    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 296
    :cond_22
    if-eqz p2, :cond_75

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_75

    .line 297
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v2

    .line 298
    .local v2, exception:Ljava/lang/Exception;
    instance-of v3, v2, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v3, :cond_66

    check-cast v2, Lcom/google/android/apps/plus/api/OzServerException;

    .end local v2           #exception:Ljava/lang/Exception;
    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v3

    const/16 v4, 0xe

    if-ne v3, v4, :cond_66

    .line 301
    const v3, 0x7f080158

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f080157

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801c4

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 305
    .local v0, alertDialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v3

    invoke-virtual {v0, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "StreamPostRestrictionsNotSupported"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_e

    .line 309
    .end local v0           #alertDialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :cond_66
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f080155

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_e

    .line 312
    :cond_75
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_e
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 170
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 173
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 174
    const-string v1, "activity_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mActivityId:Ljava/lang/String;

    .line 175
    const-string v1, "limited"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mLimited:Z

    .line 176
    sget-object v1, Lcom/google/android/apps/plus/fragments/ReshareFragment;->sAuthorBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_35

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/fragments/ReshareFragment;->sAuthorBitmap:Landroid/graphics/Bitmap;

    .line 180
    :cond_35
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mLimited:Z

    if-eqz v1, :cond_5b

    const/16 v1, 0x9

    :goto_3b
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->setCirclesUsageType(I)V

    .line 182
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->setIncludePhoneOnlyContacts(Z)V

    .line 183
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->setIncludePlusPages(Z)V

    .line 185
    if-eqz p1, :cond_5a

    .line 186
    const-string v1, "reshare_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5a

    .line 187
    const-string v1, "reshare_request_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 190
    :cond_5a
    return-void

    .line 180
    :cond_5b
    const/4 v1, 0x5

    goto :goto_3b
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 15
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 334
    packed-switch p1, :pswitch_data_5c

    move-object v5, v4

    .line 355
    :goto_5
    return-object v5

    .line 336
    :pswitch_6
    const-string v0, "com.google.android.apps.plus"

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsProvider;->buildPlatformAudienceUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    .line 338
    .local v2, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/ReshareFragment$PlatformAudienceQuery;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v5, v0

    goto :goto_5

    .line 342
    .end local v2           #uri:Landroid/net/Uri;
    :pswitch_21
    new-instance v5, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/fragments/ReshareFragment$AccountStatusQuery;->PROJECTION:[Ljava/lang/String;

    move-object v9, v4

    move-object v10, v4

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 347
    :pswitch_38
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 348
    .local v2, uri:Landroid/net/Uri$Builder;
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mActivityId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    .line 351
    new-instance v5, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;->PROJECTION_ACTIVITY:[Ljava/lang/String;

    move-object v9, v4

    move-object v10, v4

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 334
    nop

    :pswitch_data_5c
    .packed-switch 0x1
        :pswitch_6
        :pswitch_21
        :pswitch_38
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .parameter "inflater"
    .parameter "container"
    .parameter "savedInstanceState"

    .prologue
    .line 195
    const v1, 0x7f0300ad

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 197
    .local v0, view:Landroid/view/View;
    const v1, 0x7f09020b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAvatarView:Landroid/widget/ImageView;

    .line 198
    const v1, 0x7f09020c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mReshareInfo:Landroid/widget/TextView;

    .line 200
    return-object v0
.end method

.method public final onDestroyView()V
    .registers 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->destroy()V

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 232
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onDestroyView()V

    .line 233
    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .registers 2
    .parameter "tag"

    .prologue
    .line 457
    return-void
.end method

.method public final onDialogListClick$12e92030(ILandroid/os/Bundle;)V
    .registers 3
    .parameter "which"
    .parameter "args"

    .prologue
    .line 461
    return-void
.end method

.method public final onDialogNegativeClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 3
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 453
    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .registers 4
    .parameter "args"
    .parameter "tag"

    .prologue
    .line 446
    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 447
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 449
    :cond_f
    return-void
.end method

.method public final onDiscard()V
    .registers 7

    .prologue
    const/4 v2, 0x0

    .line 430
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 432
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_42

    const/4 v1, 0x1

    :goto_13
    if-eqz v1, :cond_44

    .line 433
    const v1, 0x7f08019f

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v3, 0x7f08019a

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0801c6

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0801c8

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    .line 437
    .local v0, dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    invoke-virtual {v0, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 438
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 442
    .end local v0           #dialog:Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
    :goto_41
    return-void

    :cond_42
    move v1, v2

    .line 432
    goto :goto_13

    .line 440
    :cond_44
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_41
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 5
    .parameter "v"
    .parameter "actionId"
    .parameter "event"

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    if-ne p1, v0, :cond_7

    .line 323
    packed-switch p2, :pswitch_data_e

    .line 329
    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0

    .line 325
    :pswitch_9
    invoke-static {p1}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 326
    const/4 v0, 0x1

    goto :goto_8

    .line 323
    :pswitch_data_e
    .packed-switch 0x6
        :pswitch_9
    .end packed-switch
.end method

.method public final onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .registers 15
    .parameter
    .parameter "cursor"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    const/4 v9, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 362
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v7

    packed-switch v7, :pswitch_data_94

    .line 404
    :cond_a
    :goto_a
    return-void

    .line 364
    :pswitch_b
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/AudienceView;->isEdited()Z

    move-result v7

    if-nez v7, :cond_2b

    if-eqz p2, :cond_2b

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_2b

    .line 365
    invoke-interface {p2, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 367
    .local v1, audienceBytes:[B
    if-eqz v1, :cond_2b

    .line 368
    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbAudienceData;->deserialize([B)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    .line 369
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/views/AudienceView;->setDefaultAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    goto :goto_a

    .line 374
    .end local v0           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v1           #audienceBytes:[B
    :cond_2b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v9, v8, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_a

    .line 378
    :pswitch_34
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/AudienceView;->isEdited()Z

    move-result v7

    if-nez v7, :cond_a

    if-eqz p2, :cond_a

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 379
    invoke-interface {p2, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    .line 381
    .restart local v1       #audienceBytes:[B
    if-eqz v1, :cond_a

    .line 382
    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbAudienceData;->deserialize([B)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    .line 383
    .restart local v0       #audience:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/views/AudienceView;->setDefaultAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    goto :goto_a

    .line 389
    .end local v0           #audience:Lcom/google/android/apps/plus/content/AudienceData;
    .end local v1           #audienceBytes:[B
    :pswitch_54
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    .line 390
    .local v6, context:Landroid/content/Context;
    if-eqz p2, :cond_8a

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_8a

    .line 391
    invoke-interface {p2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 392
    .local v2, authorId:Ljava/lang/String;
    const/4 v7, 0x3

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 393
    .local v3, authorName:Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mReshareInfo:Landroid/widget/TextView;

    const v8, 0x7f08018a

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v3, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    invoke-static {v6}, Lcom/google/android/apps/plus/service/ImageCache;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageCache;

    move-result-object v5

    .line 397
    .local v5, cache:Lcom/google/android/apps/plus/service/ImageCache;
    new-instance v4, Lcom/google/android/apps/plus/content/AvatarRequest;

    invoke-direct {v4, v2, v10, v11}, Lcom/google/android/apps/plus/content/AvatarRequest;-><init>(Ljava/lang/String;IZ)V

    .line 399
    .local v4, avatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;
    invoke-virtual {v5, p0, v4}, Lcom/google/android/apps/plus/service/ImageCache;->loadImage(Lcom/google/android/apps/plus/service/ImageCache$ImageConsumer;Lcom/google/android/apps/plus/content/ImageRequest;)V

    goto :goto_a

    .line 402
    .end local v2           #authorId:Ljava/lang/String;
    .end local v3           #authorName:Ljava/lang/String;
    .end local v4           #avatarRequest:Lcom/google/android/apps/plus/content/AvatarRequest;
    .end local v5           #cache:Lcom/google/android/apps/plus/service/ImageCache;
    :cond_8a
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v6, v7, v8}, Lcom/google/android/apps/plus/service/EsService;->getActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    goto/16 :goto_a

    .line 362
    nop

    :pswitch_data_94
    .packed-switch 0x1
        :pswitch_b
        :pswitch_34
        :pswitch_54
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 56
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 423
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onPause()V
    .registers 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 252
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onPause()V

    .line 253
    return-void
.end method

.method public final onResume()V
    .registers 3

    .prologue
    .line 237
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onResume()V

    .line 238
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 240
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2b

    .line 241
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2b

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 244
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 247
    .end local v0           #result:Lcom/google/android/apps/plus/service/ServiceResult;
    :cond_2b
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 257
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 259
    const-string v0, "reshare_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 261
    :cond_12
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 7
    .parameter "view"
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x0

    .line 205
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 207
    const v0, 0x7f09019d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 209
    const v0, 0x7f09020a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0, p0, v1, v3, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mSearchListAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setShowPersonNameDialog(Z)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 218
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mLimited:Z

    if-nez v0, :cond_49

    .line 223
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 225
    :cond_49
    return-void
.end method

.method public final reshare()Z
    .registers 7

    .prologue
    const/4 v3, 0x0

    .line 464
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v4}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    .line 466
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    .line 467
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    invoke-static {v0}, Lcom/google/android/apps/plus/util/PeopleUtils;->isEmpty(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 468
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/AudienceView;->performClick()Z

    .line 478
    :goto_15
    return v3

    .line 472
    :cond_16
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 473
    .local v2, context:Landroid/content/Context;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v1

    .line 475
    .local v1, content:Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mActivityId:Ljava/lang/String;

    invoke-static {v2, v4, v5, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->reshareActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mPendingRequestId:Ljava/lang/Integer;

    .line 477
    const/4 v4, 0x0

    const v5, 0x7f080164

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "req_pending"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 478
    const/4 v3, 0x1

    goto :goto_15
.end method

.method public setBitmap(Landroid/graphics/Bitmap;Z)V
    .registers 5
    .parameter "bitmap"
    .parameter "loading"

    .prologue
    .line 414
    if-nez p1, :cond_a

    .line 415
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAvatarView:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/apps/plus/fragments/ReshareFragment;->sAuthorBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 419
    :goto_9
    return-void

    .line 417
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAvatarView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_9
.end method

.method protected final setupAudienceClickListener()V
    .registers 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    return-void
.end method
