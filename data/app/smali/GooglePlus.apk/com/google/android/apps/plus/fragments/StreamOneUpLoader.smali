.class public final Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;
.super Landroid/support/v4/content/CursorLoader;
.source "StreamOneUpLoader.java"


# instance fields
.field private final mActivityUri:Landroid/net/Uri;

.field private final mCommentsUri:Landroid/net/Uri;

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 6
    .parameter "context"
    .parameter "account"
    .parameter "activityId"

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    .line 33
    new-instance v1, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v1, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    .line 43
    invoke-static {p2, p3}, Lcom/google/android/apps/plus/content/EsProvider;->buildActivityViewUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mActivityUri:Landroid/net/Uri;

    .line 45
    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 46
    .local v0, builder:Landroid/net/Uri$Builder;
    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 47
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    .line 48
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mCommentsUri:Landroid/net/Uri;

    .line 49
    return-void
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .registers 16

    .prologue
    const/4 v14, 0x0

    .line 91
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 92
    .local v0, resolver:Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mActivityUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$ActivityQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 95
    .local v6, activityCursor:Landroid/database/Cursor;
    if-eqz v6, :cond_1c

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1e

    :cond_1c
    move-object v12, v6

    .line 137
    .end local v0           #resolver:Landroid/content/ContentResolver;
    .end local v6           #activityCursor:Landroid/database/Cursor;
    :goto_1d
    return-object v12

    .line 102
    .restart local v0       #resolver:Landroid/content/ContentResolver;
    .restart local v6       #activityCursor:Landroid/database/Cursor;
    :cond_1e
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mCommentsUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$CommentCountQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 104
    .local v7, c:Landroid/database/Cursor;
    if-eqz v7, :cond_99

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_99

    const/4 v1, 0x2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-lez v1, :cond_99

    .line 105
    move-object v8, v7

    .line 113
    .local v8, commentCountCursor:Landroid/database/Cursor;
    :goto_39
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mCommentsUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$CommentQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "created ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 117
    .local v9, commentCursor:Landroid/database/Cursor;
    new-instance v11, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$LoadingQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v11, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 118
    .local v11, loadingCursor:Landroid/database/MatrixCursor;
    invoke-virtual {v11}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    const v2, 0x7ffffffc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 121
    new-instance v10, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$LeftoverQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v10, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 122
    .local v10, leftoverCursor:Landroid/database/MatrixCursor;
    invoke-virtual {v10}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    const v2, 0x7ffffffd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 125
    new-instance v12, Landroid/database/MergeCursor;

    const/4 v1, 0x5

    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object v6, v1, v2

    const/4 v2, 0x1

    aput-object v8, v1, v2

    const/4 v2, 0x2

    aput-object v9, v1, v2

    const/4 v2, 0x3

    aput-object v11, v1, v2

    const/4 v2, 0x4

    aput-object v10, v1, v2

    invoke-direct {v12, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 133
    .local v12, merge:Landroid/database/MergeCursor;
    goto :goto_1d

    .line 107
    .end local v8           #commentCountCursor:Landroid/database/Cursor;
    .end local v9           #commentCursor:Landroid/database/Cursor;
    .end local v10           #leftoverCursor:Landroid/database/MatrixCursor;
    .end local v11           #loadingCursor:Landroid/database/MatrixCursor;
    .end local v12           #merge:Landroid/database/MergeCursor;
    :cond_99
    if-eqz v7, :cond_9e

    .line 108
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_9e
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_9e} :catch_a0

    .line 110
    :cond_9e
    const/4 v8, 0x0

    .restart local v8       #commentCountCursor:Landroid/database/Cursor;
    goto :goto_39

    .line 134
    .end local v0           #resolver:Landroid/content/ContentResolver;
    .end local v6           #activityCursor:Landroid/database/Cursor;
    .end local v7           #c:Landroid/database/Cursor;
    .end local v8           #commentCountCursor:Landroid/database/Cursor;
    :catch_a0
    move-exception v13

    .line 136
    .local v13, t:Ljava/lang/Throwable;
    const-string v1, "StreamOnUpLoader"

    const-string v2, "loadInBackground failed"

    invoke-static {v1, v2, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v12, v14

    .line 137
    goto/16 :goto_1d
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .registers 2

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final onAbandon()V
    .registers 3

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_14

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserverRegistered:Z

    .line 78
    :cond_14
    return-void
.end method

.method protected final onReset()V
    .registers 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->cancelLoad()Z

    .line 83
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onReset()V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->onAbandon()V

    .line 85
    return-void
.end method

.method protected final onStartLoading()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 53
    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onStartLoading()V

    .line 54
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserverRegistered:Z

    if-nez v1, :cond_21

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 56
    .local v0, resolver:Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mActivityUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mCommentsUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 58
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserverRegistered:Z

    .line 60
    .end local v0           #resolver:Landroid/content/ContentResolver;
    :cond_21
    return-void
.end method

.method protected final onStopLoading()V
    .registers 1

    .prologue
    .line 70
    return-void
.end method
