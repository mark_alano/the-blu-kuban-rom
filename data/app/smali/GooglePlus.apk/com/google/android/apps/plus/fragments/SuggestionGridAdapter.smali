.class public final Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "SuggestionGridAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;,
        Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;
    }
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mCategories:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final mCircleContentObserver:Landroid/database/DataSetObserver;

.field private final mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

.field private mListener:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;

.field private mTooltipPersonId:Ljava/lang/String;

.field private mValid:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 118
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "category"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "category_label"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "explanation"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "properties"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .registers 7
    .parameter "context"
    .parameter "loaderManager"
    .parameter "account"
    .parameter "instanceId"

    .prologue
    .line 165
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    .line 154
    new-instance v0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleContentObserver:Landroid/database/DataSetObserver;

    .line 166
    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 167
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleContentObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    .line 169
    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;->setNotifyOnChange(Z)V

    .line 171
    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 23
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 248
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 249
    .local v13, personId:Ljava/lang/String;
    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 250
    .local v12, gaiaId:Ljava/lang/String;
    const/4 v2, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .local v7, avatarUrl:Ljava/lang/String;
    move-object/from16 v1, p1

    .line 252
    check-cast v1, Lcom/google/android/apps/plus/views/PersonCardView;

    .line 253
    .local v1, card:Lcom/google/android/apps/plus/views/PersonCardView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    .line 254
    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/PersonCardView;->setOnPersonCardClickListener(Lcom/google/android/apps/plus/views/PersonCardView$OnPersonCardClickListener;)V

    .line 255
    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setContactName(Ljava/lang/String;)V

    .line 256
    invoke-virtual {v1, v13}, Lcom/google/android/apps/plus/views/PersonCardView;->setPersonId(Ljava/lang/String;)V

    .line 257
    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v12, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    .line 260
    .local v16, resources:Landroid/content/res/Resources;
    const/4 v4, 0x0

    .line 261
    .local v4, description:Ljava/lang/String;
    const/16 v2, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 264
    .local v11, explanationBytes:[B
    if-eqz v11, :cond_bf

    .line 265
    invoke-static {}, Lcom/google/api/services/plusi/model/DataSugggestionExplanationJson;->getInstance()Lcom/google/api/services/plusi/model/DataSugggestionExplanationJson;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/google/api/services/plusi/model/DataSugggestionExplanationJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/api/services/plusi/model/DataSugggestionExplanation;

    .line 267
    .local v10, explanation:Lcom/google/api/services/plusi/model/DataSugggestionExplanation;
    iget-object v2, v10, Lcom/google/api/services/plusi/model/DataSugggestionExplanation;->numberOfCommonFriends:Ljava/lang/Integer;

    if-eqz v2, :cond_77

    iget-object v2, v10, Lcom/google/api/services/plusi/model/DataSugggestionExplanation;->numberOfCommonFriends:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_77

    .line 269
    const v2, 0x7f0e0028

    iget-object v3, v10, Lcom/google/api/services/plusi/model/DataSugggestionExplanation;->numberOfCommonFriends:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v0, v10, Lcom/google/api/services/plusi/model/DataSugggestionExplanation;->numberOfCommonFriends:Ljava/lang/Integer;

    move-object/from16 v18, v0

    aput-object v18, v5, v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 297
    .end local v10           #explanation:Lcom/google/api/services/plusi/model/DataSugggestionExplanation;
    :cond_77
    :goto_77
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/plus/views/PersonCardView;->setPackedCircleIdsEmailAndDescription(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 299
    const/4 v2, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 300
    .local v9, circleIds:Ljava/lang/String;
    const/4 v2, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 302
    .local v8, category:Ljava/lang/String;
    invoke-static {v13}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->isCircleMembershipRequestPending(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_116

    .line 303
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setShowCircleChangePending(Z)V

    .line 310
    :goto_96
    const/16 v17, 0x0

    .line 311
    .local v17, showTooltip:Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mTooltipPersonId:Ljava/lang/String;

    invoke-static {v2, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_af

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_af

    .line 312
    const/16 v17, 0x1

    .line 313
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mTooltipPersonId:Ljava/lang/String;

    .line 316
    :cond_af
    const v2, 0x7f080404

    move/from16 v0, v17

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setShowTooltip(ZI)V

    .line 318
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setDismissActionButtonVisible(Z)V

    .line 319
    return-void

    .line 273
    .end local v8           #category:Ljava/lang/String;
    .end local v9           #circleIds:Ljava/lang/String;
    .end local v17           #showTooltip:Z
    :cond_bf
    const/16 v2, 0xa

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v15

    .line 274
    .local v15, propertiesBytes:[B
    if-eqz v15, :cond_77

    .line 275
    invoke-static {}, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesJson;->getInstance()Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesJson;

    move-result-object v2

    invoke-virtual {v2, v15}, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;

    .line 277
    .local v14, properties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;
    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->tagLine:Ljava/lang/String;

    if-eqz v2, :cond_da

    .line 278
    iget-object v4, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->tagLine:Ljava/lang/String;

    goto :goto_77

    .line 279
    :cond_da
    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    if-eqz v2, :cond_fe

    .line 280
    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    if-eqz v2, :cond_fa

    .line 281
    const v2, 0x7f0803fc

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    aput-object v6, v3, v5

    const/4 v5, 0x1

    iget-object v6, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    aput-object v6, v3, v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_77

    .line 285
    :cond_fa
    iget-object v4, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->company:Ljava/lang/String;

    goto/16 :goto_77

    .line 287
    :cond_fe
    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    if-eqz v2, :cond_106

    .line 288
    iget-object v4, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->occupation:Ljava/lang/String;

    goto/16 :goto_77

    .line 289
    :cond_106
    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->school:Ljava/lang/String;

    if-eqz v2, :cond_10e

    .line 290
    iget-object v4, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->school:Ljava/lang/String;

    goto/16 :goto_77

    .line 291
    :cond_10e
    iget-object v2, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->location:Ljava/lang/String;

    if-eqz v2, :cond_77

    .line 292
    iget-object v4, v14, Lcom/google/api/services/plusi/model/DataCircleMemberProperties;->location:Ljava/lang/String;

    goto/16 :goto_77

    .line 305
    .end local v14           #properties:Lcom/google/api/services/plusi/model/DataCircleMemberProperties;
    .end local v15           #propertiesBytes:[B
    .restart local v8       #category:Ljava/lang/String;
    .restart local v9       #circleIds:Ljava/lang/String;
    :cond_116
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PersonCardView;->setShowCircleChangePending(Z)V

    .line 306
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    const-string v3, "#"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v1, v9, v2, v3}, Lcom/google/android/apps/plus/views/PersonCardView;->setOneClickCircles(Ljava/lang/String;Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;Z)V

    goto/16 :goto_96
.end method

.method public final getCategories()Ljava/util/ArrayList;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 192
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mValid:Z

    if-eqz v4, :cond_8

    .line 193
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    .line 236
    :goto_7
    return-object v4

    .line 196
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 197
    .local v2, cursor:Landroid/database/Cursor;
    if-nez v2, :cond_18

    .line 198
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 199
    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mValid:Z

    .line 200
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    goto :goto_7

    .line 203
    :cond_18
    const/4 v3, 0x0

    .line 204
    .local v3, index:I
    const/4 v0, 0x0

    .line 205
    .local v0, category:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_6a

    .line 207
    :cond_20
    const/4 v4, 0x7

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 208
    .local v1, categoryName:Ljava/lang/String;
    if-eqz v0, :cond_31

    #getter for: Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCategory:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->access$000(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_64

    .line 209
    :cond_31
    if-eqz v0, :cond_3f

    .line 210
    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    #getter for: Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mOffset:I
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->access$200(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;)I

    move-result v5

    sub-int/2addr v4, v5

    #setter for: Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCount:I
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->access$102(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;I)I

    .line 213
    :cond_3f
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_8e

    .line 214
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0           #category:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;
    check-cast v0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;

    .line 219
    .restart local v0       #category:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;
    :goto_4f
    #setter for: Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCategory:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->access$002(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 220
    const/16 v4, 0x8

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    #setter for: Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCategoryLabel:Ljava/lang/String;
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->access$302(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;Ljava/lang/String;)Ljava/lang/String;

    .line 221
    invoke-interface {v2}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    #setter for: Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mOffset:I
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->access$202(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;I)I

    .line 222
    add-int/lit8 v3, v3, 0x1

    .line 224
    :cond_64
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_20

    .line 227
    .end local v1           #categoryName:Ljava/lang/String;
    :cond_6a
    if-eqz v0, :cond_78

    .line 228
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    #getter for: Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mOffset:I
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->access$200(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;)I

    move-result v5

    sub-int/2addr v4, v5

    #setter for: Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->mCount:I
    invoke-static {v0, v4}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;->access$102(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;I)I

    .line 231
    :cond_78
    :goto_78
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v3, :cond_99

    .line 232
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_78

    .line 216
    .restart local v1       #categoryName:Ljava/lang/String;
    :cond_8e
    new-instance v0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;

    .end local v0           #category:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;
    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;-><init>(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;)V

    .line 217
    .restart local v0       #category:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionCategoryAdapter;
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4f

    .line 235
    .end local v1           #categoryName:Ljava/lang/String;
    :cond_99
    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mValid:Z

    .line 236
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCategories:Ljava/util/ArrayList;

    goto/16 :goto_7
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    .line 241
    new-instance v0, Lcom/google/android/apps/plus/views/PersonCardView;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/views/PersonCardView;-><init>(Landroid/content/Context;)V

    .line 242
    .local v0, personCardView:Lcom/google/android/apps/plus/views/PersonCardView;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PersonCardView;->setAutoWidthForHorizontalScrolling()V

    .line 243
    return-object v0
.end method

.method public final onActionButtonClick(Lcom/google/android/apps/plus/views/PersonCardView;I)V
    .registers 3
    .parameter "view"
    .parameter "action"

    .prologue
    .line 333
    return-void
.end method

.method public final onChangeCircles(Lcom/google/android/apps/plus/views/PersonCardView;)V
    .registers 6
    .parameter "view"

    .prologue
    .line 337
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->isOneClickAdd()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getContactName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->isForSharing()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;->onAddPersonToCirclesAction(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasOneClickAddTooltipBeenShown(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_2e

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->setOneClickAddTooltipShown(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 342
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPersonId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mTooltipPersonId:Ljava/lang/String;

    .line 347
    :cond_2e
    :goto_2e
    return-void

    .line 345
    :cond_2f
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getContactName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;->onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2e
.end method

.method public final onDismissButtonClick(Lcom/google/android/apps/plus/views/PersonCardView;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;->onDismissSuggestionAction(Ljava/lang/String;)V

    .line 352
    return-void
.end method

.method public final onItemClick(Lcom/google/android/apps/plus/views/PersonCardView;)V
    .registers 4
    .parameter "view"

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PersonCardView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;->onPersonSelected(Ljava/lang/String;)V

    .line 324
    return-void
.end method

.method public final onStart()V
    .registers 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    .line 183
    return-void
.end method

.method public final setCircleSpinnerAdapter(Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;)V
    .registers 2
    .parameter "circleSpinnerAdapter"

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mCircleSpinnerAdapter:Lcom/google/android/apps/plus/fragments/CircleSpinnerAdapter;

    .line 175
    return-void
.end method

.method public final setListener(Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 178
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mListener:Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter$SuggestionGridAdapterListener;

    .line 179
    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .registers 3
    .parameter "newCursor"

    .prologue
    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SuggestionGridAdapter;->mValid:Z

    .line 188
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
