.class final Lcom/google/android/apps/plus/fragments/WriteReviewFragment$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "WriteReviewFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/WriteReviewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/WriteReviewFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)V
    .registers 2
    .parameter

    .prologue
    .line 450
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/WriteReviewFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 450
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;)V

    return-void
.end method


# virtual methods
.method public final onDeleteReviewComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/WriteReviewFragment;

    #calls: Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->handleDeleteReviewCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->access$500(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 460
    return-void
.end method

.method public final onWriteReviewComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 4
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/WriteReviewFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/WriteReviewFragment;

    #calls: Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->handleWriteReviewCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/fragments/WriteReviewFragment;->access$400(Lcom/google/android/apps/plus/fragments/WriteReviewFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 455
    return-void
.end method
