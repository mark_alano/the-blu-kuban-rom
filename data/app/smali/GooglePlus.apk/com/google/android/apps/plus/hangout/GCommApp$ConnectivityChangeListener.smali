.class final Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;
.super Landroid/content/BroadcastReceiver;
.source "GCommApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/GCommApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectivityChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/GCommApp;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V
    .registers 2
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/GCommApp;B)V
    .registers 3
    .parameter "x0"
    .parameter

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 12
    .parameter "context"
    .parameter "intent"

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 117
    const-string v3, "networkInfo"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 119
    .local v0, info:Landroid/net/NetworkInfo;
    const-string v3, "Connectivity change: network %s in state %s"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    const-string v3, "noConnectivity"

    invoke-virtual {p2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 125
    .local v1, noConnectivity:Z
    if-eqz v1, :cond_2b

    .line 126
    const-string v3, "No connectivity"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 129
    :cond_2b
    const-string v3, "otherNetwork"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/NetworkInfo;

    .line 131
    .local v2, otherNetwork:Landroid/net/NetworkInfo;
    if-eqz v2, :cond_4f

    .line 132
    const-string v3, "Other network is %s in state %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    :cond_4f
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    #getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$000(Lcom/google/android/apps/plus/hangout/GCommApp;)I

    move-result v4

    if-ne v3, v4, :cond_72

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    #getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$100(Lcom/google/android/apps/plus/hangout/GCommApp;)I

    move-result v4

    if-ne v3, v4, :cond_72

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$200(Landroid/net/NetworkInfo;)Z

    move-result v3

    if-nez v3, :cond_72

    .line 140
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->raiseNetworkError()V

    .line 143
    :cond_72
    return-void
.end method
