.class public Lcom/google/android/apps/plus/hangout/GCommApp;
.super Ljava/lang/Object;
.source "GCommApp.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Lcom/google/android/apps/plus/service/Hangout$ApplicationEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/GCommApp$5;,
        Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;,
        Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;,
        Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;,
        Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;,
        Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static crashReported:Z

.field private static gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;


# instance fields
.field private account:Lcom/google/android/apps/plus/content/EsAccount;

.field private final appEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

.field private audioFocus:Z

.field private audioManager:Landroid/media/AudioManager;

.field private final connectivityChangeListener:Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

.field private connectivityManager:Landroid/net/ConnectivityManager;

.field private cpuWakeLock:Landroid/os/PowerManager$WakeLock;

.field private currentNetworkSubtype:I

.field private currentNetworkType:I

.field private currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

.field private eventHandlers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/GCommEventHandler;",
            ">;"
        }
    .end annotation
.end field

.field private exitMeetingCleanupDone:Z

.field private gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

.field private volatile gcommService:Lcom/google/android/apps/plus/hangout/GCommService;

.field private final gcommServiceConnection:Landroid/content/ServiceConnection;

.field private greenRoomParticipantIds:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

.field private hangoutSigninRefCount:I

.field private hangoutStartTime:J

.field private headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

.field private incomingAudioLevelBeforeAudioFocusLoss:I

.field private isBound:Z

.field private isExitingHangout:Z

.field private lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

.field private mSavedAudioMode:I

.field private muteMicOnAudioFocusGain:Z

.field private outgoingVideoMute:Z

.field private final plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

.field private screenWakeLock:Landroid/os/PowerManager$WakeLock;

.field screenoffBroadcastListener:Landroid/content/BroadcastReceiver;

.field private selectedVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

.field private signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

.field private wifiLock:Landroid/net/wifi/WifiManager$WifiLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method private constructor <init>(Lcom/google/android/apps/plus/phone/EsApplication;)V
    .registers 13
    .parameter "plusOneApplication"

    .prologue
    const/4 v2, -0x1

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/GCommApp$1;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenoffBroadcastListener:Landroid/content/BroadcastReceiver;

    .line 319
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    .line 335
    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;

    invoke-direct {v0, p0, v9}, Lcom/google/android/apps/plus/hangout/GCommApp$AppEventHandler;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->appEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 337
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    .line 339
    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    .line 340
    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    .line 344
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    .line 348
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->handler:Landroid/os/Handler;

    .line 441
    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/GCommApp$3;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommServiceConnection:Landroid/content/ServiceConnection;

    .line 352
    const-string v0, "Constructing GCommApp"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 353
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    .line 355
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->resetSelfMediaState()V

    .line 357
    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

    invoke-direct {v0, p0, v9}, Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityChangeListener:Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

    .line 359
    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/Utils;->initialize(Landroid/content/Context;)V

    .line 362
    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/EsApplication;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 365
    .local v1, privateStoragePath:Ljava/lang/String;
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->NATIVE_HANGOUT_LOG:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v4

    .line 366
    .local v4, canLogPII:Z
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->NATIVE_WRAPPER_HANGOUT_LOG_LEVEL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v6

    .line 367
    .local v6, logLevel:Ljava/lang/String;
    const-string v2, "Google_Plus_Android"

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Utils;->getVersion()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/plus/network/ClientVersion;->from(Landroid/content/Context;)I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->initialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 371
    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$2;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/GCommApp$2;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    new-array v2, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/hangout/GCommApp$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 386
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    if-nez v0, :cond_9c

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    const-class v3, Lcom/google/android/apps/plus/hangout/GCommService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v0, v3, v10}, Lcom/google/android/apps/plus/phone/EsApplication;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    :cond_9c
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    if-nez v0, :cond_a6

    .line 387
    const-string v0, "Unable to bind to GCommService"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 411
    :goto_a5
    return-void

    .line 391
    :cond_a6
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    .line 392
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 395
    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/EsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    .line 397
    const-string v0, "power"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/PowerManager;

    .line 399
    .local v7, powerManager:Landroid/os/PowerManager;
    const-string v0, "gcomm"

    invoke-virtual {v7, v10, v0}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 400
    const/16 v0, 0xa

    const-string v2, "gcomm"

    invoke-virtual {v7, v0, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 402
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/wifi/WifiManager;

    .line 404
    .local v8, wifiManager:Landroid/net/wifi/WifiManager;
    const-string v0, "gcomm"

    invoke-virtual {v8, v10, v0}, Landroid/net/wifi/WifiManager;->createWifiLock(ILjava/lang/String;)Landroid/net/wifi/WifiManager$WifiLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    .line 406
    iput v9, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    .line 407
    sput-object p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    .line 409
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenoffBroadcastListener:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v2}, Lcom/google/android/apps/plus/phone/EsApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_a5
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/GCommApp;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/GCommApp;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/phone/EsApplication;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/hangout/GCommApp;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanupDone:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/google/android/apps/plus/hangout/GCommApp;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanupDone:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/hangout/GCommApp;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;)Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/hangout/GCommApp;)V
    .registers 6
    .parameter "x0"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 61
    iget v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    const/4 v3, -0x2

    if-ne v2, v3, :cond_f

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getMode()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    :cond_f
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setMode(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    const/4 v3, 0x2

    invoke-virtual {v2, p0, v1, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "requestAudioFocus returned "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    if-ne v2, v0, :cond_56

    :goto_30
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isAudioMute()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->muteMicOnAudioFocusGain:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/plus/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "INCOMING_AUDIO_VOLUME"

    const/16 v2, 0xff

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->setupAudio()V

    return-void

    :cond_56
    move v0, v1

    goto :goto_30
.end method

.method static synthetic access$1602(Lcom/google/android/apps/plus/hangout/GCommApp;J)J
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    iput-wide p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    return-wide p1
.end method

.method static synthetic access$1702(Lcom/google/android/apps/plus/hangout/GCommApp;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    return v0
.end method

.method static synthetic access$200(Landroid/net/NetworkInfo;)Z
    .registers 3
    .parameter "x0"

    .prologue
    .line 61
    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    if-eq v0, v1, :cond_1a

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v0, v1, :cond_1a

    invoke-virtual {p0}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    if-eq v0, v1, :cond_1a

    const/4 v0, 0x1

    :goto_19
    return v0

    :cond_1a
    const/4 v0, 0x0

    goto :goto_19
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommApp;->getCaptureSessionType()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2102(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/hangout/GCommService;)Lcom/google/android/apps/plus/hangout/GCommService;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommService:Lcom/google/android/apps/plus/hangout/GCommService;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/GCommApp;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingAndDisconnect()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/GCommApp;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/GCommApp;)Landroid/media/AudioManager;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;)Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/service/Hangout$Info;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/service/Hangout$Info;)Lcom/google/android/apps/plus/service/Hangout$Info;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    return-object v0
.end method

.method public static deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 5
    .parameter "context"
    .parameter "account"

    .prologue
    .line 1021
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    if-eqz v0, :cond_19

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1022
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingAndDisconnect()V

    .line 1023
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    const/16 v1, 0x36

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->dispatchMessage(ILjava/lang/Object;)V

    .line 1025
    :cond_19
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutRingingActivity;->deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 1026
    return-void
.end method

.method private exitMeetingAndDisconnect()V
    .registers 2

    .prologue
    .line 1000
    const-string v0, "GCommApp.exitMeetingAndDisconnect"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 1002
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    .line 1003
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeeting()V

    .line 1004
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    .line 1005
    return-void
.end method

.method private getAllEventHandlers()Ljava/util/ArrayList;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/GCommEventHandler;",
            ">;"
        }
    .end annotation

    .prologue
    .line 697
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 702
    .local v0, handlers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/hangout/GCommEventHandler;>;"
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->appEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    if-eqz v1, :cond_16

    .line 703
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->appEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 705
    :cond_16
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    if-eqz v1, :cond_1f

    .line 706
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 710
    :cond_1f
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 711
    return-object v0
.end method

.method private static getCaptureSessionType()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;
    .registers 16

    .prologue
    const/4 v15, 0x1

    .line 229
    const-string v12, ""

    .line 232
    .local v12, result:Ljava/lang/String;
    const/4 v13, 0x2

    :try_start_4
    new-array v0, v13, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "/system/bin/cat"

    aput-object v14, v0, v13

    const/4 v13, 0x1

    const-string v14, "/proc/cpuinfo"

    aput-object v14, v0, v13

    .line 233
    .local v0, args:[Ljava/lang/String;
    new-instance v3, Ljava/lang/ProcessBuilder;

    invoke-direct {v3, v0}, Ljava/lang/ProcessBuilder;-><init>([Ljava/lang/String;)V

    .line 234
    .local v3, cmd:Ljava/lang/ProcessBuilder;
    invoke-virtual {v3}, Ljava/lang/ProcessBuilder;->start()Ljava/lang/Process;

    move-result-object v10

    .line 235
    .local v10, process:Ljava/lang/Process;
    invoke-virtual {v10}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 236
    .local v6, in:Ljava/io/InputStream;
    const/16 v13, 0x400

    new-array v11, v13, [B

    .line 237
    .local v11, re:[B
    :goto_21
    invoke-virtual {v6, v11}, Ljava/io/InputStream;->read([B)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_3f

    .line 238
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v11}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto :goto_21

    .line 240
    :cond_3f
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_42
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_42} :catch_6a

    .line 245
    .end local v0           #args:[Ljava/lang/String;
    .end local v3           #cmd:Ljava/lang/ProcessBuilder;
    .end local v6           #in:Ljava/io/InputStream;
    .end local v10           #process:Ljava/lang/Process;
    .end local v11           #re:[B
    :goto_42
    const/4 v2, 0x0

    .line 246
    .local v2, bogoMips:F
    const/4 v9, 0x0

    .line 247
    .local v9, numProcessors:I
    const-string v13, "\n"

    invoke-virtual {v12, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .local v1, arr$:[Ljava/lang/String;
    array-length v7, v1

    .local v7, len$:I
    const/4 v5, 0x0

    .local v5, i$:I
    :goto_4c
    if-ge v5, v7, :cond_73

    aget-object v8, v1, v5

    .line 248
    .local v8, line:Ljava/lang/String;
    const-string v13, "BogoMIPS.*"

    invoke-virtual {v8, v13}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_67

    .line 249
    const-string v13, "[^.0-9]"

    const-string v14, ""

    invoke-virtual {v8, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    add-float/2addr v2, v13

    .line 250
    add-int/lit8 v9, v9, 0x1

    .line 247
    :cond_67
    add-int/lit8 v5, v5, 0x1

    goto :goto_4c

    .line 241
    .end local v1           #arr$:[Ljava/lang/String;
    .end local v2           #bogoMips:F
    .end local v5           #i$:I
    .end local v7           #len$:I
    .end local v8           #line:Ljava/lang/String;
    .end local v9           #numProcessors:I
    :catch_6a
    move-exception v4

    .line 242
    .local v4, ex:Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_42

    .line 256
    .end local v4           #ex:Ljava/io/IOException;
    .restart local v1       #arr$:[Ljava/lang/String;
    .restart local v2       #bogoMips:F
    .restart local v5       #i$:I
    .restart local v7       #len$:I
    .restart local v9       #numProcessors:I
    :cond_73
    const/high16 v13, 0x4120

    cmpl-float v13, v2, v13

    if-lez v13, :cond_7f

    const/high16 v13, 0x4348

    cmpg-float v13, v2, v13

    if-ltz v13, :cond_87

    :cond_7f
    const/high16 v13, 0x447a

    cmpl-float v13, v2, v13

    if-gtz v13, :cond_87

    if-le v9, v15, :cond_8a

    :cond_87
    sget-object v13, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;->MEDIUM_RESOLUTION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;

    :goto_89
    return-object v13

    :cond_8a
    sget-object v13, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;->LOW_RESOLUTION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;

    goto :goto_89
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;
    .registers 3
    .parameter "context"

    .prologue
    .line 419
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    if-nez v0, :cond_11

    .line 420
    new-instance v1, Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;-><init>(Lcom/google/android/apps/plus/phone/EsApplication;)V

    sput-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    .line 422
    :cond_11
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    return-object v0
.end method

.method public static isDebuggable(Landroid/content/Context;)Z
    .registers 2
    .parameter "context"

    .prologue
    .line 438
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_b
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_b
.end method

.method public static isInstantiated()Z
    .registers 1

    .prologue
    .line 415
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private static reportCrash(Ljava/lang/String;Z)V
    .registers 6
    .parameter "javaCrashSignature"
    .parameter "exitProcess"

    .prologue
    .line 1273
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 1276
    sget-boolean v1, Lcom/google/android/apps/plus/hangout/GCommApp;->crashReported:Z

    if-nez v1, :cond_c

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    if-nez v1, :cond_d

    .line 1301
    :cond_c
    :goto_c
    return-void

    .line 1279
    :cond_d
    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/hangout/GCommApp;->crashReported:Z

    .line 1283
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    const-class v2, Lcom/google/android/apps/plus/hangout/crash/CrashTriggerActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1284
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1285
    if-eqz p0, :cond_27

    .line 1286
    const-string v1, "com.google.android.apps.plus.hangout.java_crash_signature"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1288
    :cond_27
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/EsApplication;->startActivity(Landroid/content/Intent;)V

    .line 1293
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    if-eqz v1, :cond_3b

    .line 1294
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->finish()V

    .line 1299
    :cond_3b
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;

    iget-boolean v2, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    if-eqz v2, :cond_c

    iget-object v2, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v3, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/phone/EsApplication;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/plus/hangout/GCommApp;->isBound:Z

    goto :goto_c
.end method

.method static reportJavaCrashFromNativeCode(Ljava/lang/Throwable;)V
    .registers 3
    .parameter "ex"

    .prologue
    .line 1245
    invoke-static {p0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 1246
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->computeJavaCrashSignature(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->reportCrash(Ljava/lang/String;Z)V

    .line 1247
    return-void
.end method

.method static reportNativeCrash()V
    .registers 2

    .prologue
    .line 1250
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->reportCrash(Ljava/lang/String;Z)V

    .line 1251
    return-void
.end method

.method private resetSelfMediaState()V
    .registers 2

    .prologue
    .line 955
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->outgoingVideoMute:Z

    .line 957
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 958
    sget-object v0, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    .line 962
    :cond_d
    :goto_d
    return-void

    .line 959
    :cond_e
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isRearFacingCameraAvailable()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 960
    sget-object v0, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->RearFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    goto :goto_d
.end method

.method public static sendEmptyMessage(Landroid/content/Context;I)V
    .registers 3
    .parameter "context"
    .parameter "messageId"

    .prologue
    .line 915
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 916
    return-void
.end method

.method public static sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V
    .registers 6
    .parameter "context"
    .parameter "messageId"
    .parameter "obj"

    .prologue
    .line 919
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    .line 920
    .local v0, gcommApp:Lcom/google/android/apps/plus/hangout/GCommApp;
    iget-object v1, v0, Lcom/google/android/apps/plus/hangout/GCommApp;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/hangout/GCommApp$4;

    invoke-direct {v2, v0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommApp$4;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;ILjava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 926
    return-void
.end method

.method private setupAudio()V
    .registers 4

    .prologue
    const/4 v2, 0x1

    .line 1215
    iput-boolean v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    .line 1217
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->muteMicOnAudioFocusGain:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->setAudioMute(Z)V

    .line 1218
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingAudioVolume(I)V

    .line 1219
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->isHeadsetPluggedIn()Z

    move-result v0

    if-nez v0, :cond_20

    .line 1221
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 1223
    :cond_20
    return-void
.end method

.method private updateWakeLockState(Z)V
    .registers 5
    .parameter "startingOrInMeeting"

    .prologue
    .line 545
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;->NONE:Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;

    .line 546
    if-eqz p1, :cond_12

    .line 547
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;->SCREEN:Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;

    .line 557
    .local v0, neededLockType:Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;
    :goto_6
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommApp$5;->$SwitchMap$com$google$android$apps$plus$hangout$GCommApp$WakeLockType:[I

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_84

    .line 589
    :cond_11
    :goto_11
    return-void

    .line 549
    .end local v0           #neededLockType:Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;
    :cond_12
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;->NONE:Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;

    .restart local v0       #neededLockType:Lcom/google/android/apps/plus/hangout/GCommApp$WakeLockType;
    goto :goto_6

    .line 559
    :pswitch_15
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 560
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 561
    const-string v1, "Released CPU wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 563
    :cond_27
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 564
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 565
    const-string v1, "Released screen wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_11

    .line 569
    :pswitch_3a
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_4c

    .line 570
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 571
    const-string v1, "Acquired CPU wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 573
    :cond_4c
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 574
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 575
    const-string v1, "Released screen wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_11

    .line 579
    :pswitch_5f
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_71

    .line 580
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->screenWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 581
    const-string v1, "Acquired screen wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 583
    :cond_71
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_11

    .line 584
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->cpuWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 585
    const-string v1, "Released CPU wake lock"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_11

    .line 557
    :pswitch_data_84
    .packed-switch 0x1
        :pswitch_15
        :pswitch_3a
        :pswitch_5f
    .end packed-switch
.end method


# virtual methods
.method public final createHangout(Z)V
    .registers 3
    .parameter "ringInvitees"

    .prologue
    .line 965
    const-string v0, "GCommApp.createHangout"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 967
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->updateWakeLockState(Z)V

    .line 968
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->createHangout(Z)V

    .line 969
    return-void
.end method

.method public final disconnect()V
    .registers 4

    .prologue
    const/4 v2, -0x1

    .line 942
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GCommApp.disconnect: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 943
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    const-string v0, "Released wifi lock"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    :cond_27
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    if-eq v0, v2, :cond_36

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityChangeListener:Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 945
    :cond_36
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->signoutAndDisconnect()V

    .line 948
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->resetSelfMediaState()V

    .line 949
    return-void
.end method

.method public final dispatchMessage(ILjava/lang/Object;)V
    .registers 16
    .parameter "messageId"
    .parameter "obj"

    .prologue
    const/4 v11, 0x1

    .line 716
    sparse-switch p1, :sswitch_data_3c6

    .line 910
    new-instance v10, Ljava/lang/IllegalStateException;

    invoke-direct {v10}, Ljava/lang/IllegalStateException;-><init>()V

    throw v10

    .line 718
    :sswitch_a
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .local v7, handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 719
    check-cast v10, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V

    goto :goto_12

    .line 723
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_25
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_2d
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 724
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSigninTimeOutError()V

    goto :goto_2d

    .line 728
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_3d
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_45
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 729
    check-cast v10, Ljava/lang/String;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedIn(Ljava/lang/String;)V

    goto :goto_45

    .line 733
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_58
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_60
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 734
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedOut()V

    goto :goto_60

    .line 738
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_70
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_78
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 739
    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVCardResponse(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_78

    .line 743
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_8b
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_93
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_93

    .line 748
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_9d
    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/hangout/GCommApp;->createHangout(Z)V

    .line 907
    :cond_a0
    return-void

    .line 751
    :sswitch_a1
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_a9
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 752
    check-cast v10, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingEnterError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;)V

    goto :goto_a9

    .line 757
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_bc
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_c4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 758
    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMucEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_c4

    .line 762
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_d7
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_df
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 763
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMediaStarted()V

    goto :goto_df

    .line 767
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_ef
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_f7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 768
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    if-eqz p2, :cond_10a

    move v10, v11

    :goto_106
    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingExited(Z)V

    goto :goto_f7

    :cond_10a
    const/4 v10, 0x0

    goto :goto_106

    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_10c
    move-object v4, p2

    .line 772
    check-cast v4, Landroid/util/Pair;

    .line 773
    .local v4, arg:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_117
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 774
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    iget-object v10, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    iget-object v10, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onCallgrokLogUploadCompleted$4f708078()V

    goto :goto_117

    .line 779
    .end local v4           #arg:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_130
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_138
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 780
    check-cast v10, Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onHangoutCreated(Lcom/google/android/apps/plus/service/Hangout$Info;)V

    goto :goto_138

    .line 784
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_14b
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_153
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 785
    check-cast v10, Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onHangoutWaitTimeout(Lcom/google/android/apps/plus/service/Hangout$Info;)V

    goto :goto_153

    .line 789
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_166
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_16e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 790
    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_16e

    .line 794
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_181
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_189
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 795
    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberPresenceConnectionStatusChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_189

    .line 800
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_19c
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_1a4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 801
    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberExited(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_1a4

    .line 805
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_1b7
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_1bf
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1bf

    .line 810
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_1c9
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_1d1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 811
    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onIncomingVideoFrameReceived(I)V

    goto :goto_1d1

    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_1e8
    move-object v6, p2

    .line 815
    check-cast v6, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;

    .line 817
    .local v6, frameDimensionsChangedParams:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_1f3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 818
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->getRequestID()I

    move-result v10

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->getDimensions()Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    move-result-object v11

    iget v11, v11, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->width:I

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->getDimensions()Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    move-result-object v12

    iget v12, v12, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->height:I

    invoke-virtual {v7, v10, v11, v12}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onIncomingVideoFrameDimensionsChanged(III)V

    goto :goto_1f3

    .end local v6           #frameDimensionsChangedParams:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_213
    move-object v2, p2

    .line 826
    check-cast v2, Landroid/util/Pair;

    .line 827
    .local v2, arg:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Ljava/lang/Boolean;>;"
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_21e
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 828
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    iget-object v10, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onAudioMuteStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_21e

    .end local v2           #arg:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Ljava/lang/Boolean;>;"
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_23a
    move-object v2, p2

    .line 833
    check-cast v2, Landroid/util/Pair;

    .line 834
    .restart local v2       #arg:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Ljava/lang/Boolean;>;"
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_245
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 835
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    iget-object v10, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoPauseStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_245

    .end local v2           #arg:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Ljava/lang/Boolean;>;"
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_261
    move-object v3, p2

    .line 840
    check-cast v3, Landroid/util/Pair;

    .line 841
    .local v3, arg:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Ljava/lang/Integer;>;"
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_26c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 842
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    iget-object v10, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVolumeChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;I)V

    goto :goto_26c

    .line 847
    .end local v3           #arg:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Ljava/lang/Integer;>;"
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_288
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_290
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 848
    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onCurrentSpeakerChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_290

    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_2a3
    move-object v9, p2

    .line 852
    check-cast v9, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;

    .line 854
    .local v9, params:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_2ae
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 855
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    invoke-virtual {v9}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->getRequestID()I

    move-result v10

    invoke-virtual {v9}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->getSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v11

    invoke-virtual {v9}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->isVideoAvailable()Z

    move-result v12

    invoke-virtual {v7, v10, v11, v12}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoSourceChanged(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_2ae

    .line 861
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    .end local v9           #params:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;
    :sswitch_2ca
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_2d2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 862
    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onIncomingVideoStarted(I)V

    goto :goto_2d2

    .line 866
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_2e9
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_2f1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 867
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onOutgoingVideoStarted()V

    goto :goto_2f1

    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_301
    move-object v1, p2

    .line 871
    check-cast v1, Landroid/util/Pair;

    .line 873
    .local v1, arg:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;>;"
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_30c
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 874
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    iget-object v10, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onRemoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_30c

    .end local v1           #arg:Landroid/util/Pair;,"Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;>;"
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_324
    move-object v0, p2

    .line 879
    check-cast v0, Landroid/util/Pair;

    .line 881
    .local v0, arg:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;>;Ljava/lang/Boolean;>;"
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_32f
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 882
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    iget-object v10, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Landroid/util/Pair;

    iget-object v10, v10, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v10, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v11, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v11, Landroid/util/Pair;

    iget-object v11, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v12, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v12, Ljava/lang/Boolean;

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    invoke-virtual {v7, v10, v11, v12}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_32f

    .line 887
    .end local v0           #arg:Landroid/util/Pair;,"Landroid/util/Pair<Landroid/util/Pair<Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;>;Ljava/lang/Boolean;>;"
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_357
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_35f
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 888
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onCameraSwitchRequested()V

    goto :goto_35f

    .line 892
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_36f
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_377
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 893
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoMuteToggleRequested()V

    goto :goto_377

    .line 897
    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_387
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_38f
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    move-object v10, p2

    .line 898
    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    invoke-virtual {v7, v10}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoMuteChanged(Z)V

    goto :goto_38f

    .end local v7           #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    .end local v8           #i$:Ljava/util/Iterator;
    :sswitch_3a6
    move-object v5, p2

    .line 902
    check-cast v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    .line 903
    .local v5, dimensions:Lcom/google/android/apps/plus/hangout/RectangleDimensions;
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getAllEventHandlers()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8       #i$:Ljava/util/Iterator;
    :goto_3b1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 904
    .restart local v7       #handler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;
    iget v10, v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->width:I

    iget v11, v5, Lcom/google/android/apps/plus/hangout/RectangleDimensions;->height:I

    invoke-virtual {v7, v10, v11}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onCameraPreviewFrameDimensionsChanged(II)V

    goto :goto_3b1

    .line 716
    nop

    :sswitch_data_3c6
    .sparse-switch
        -0x3 -> :sswitch_a1
        -0x2 -> :sswitch_25
        -0x1 -> :sswitch_a
        0x1 -> :sswitch_3d
        0x2 -> :sswitch_58
        0x3 -> :sswitch_70
        0x5 -> :sswitch_8b
        0x6 -> :sswitch_9d
        0x32 -> :sswitch_130
        0x33 -> :sswitch_14b
        0x34 -> :sswitch_bc
        0x35 -> :sswitch_d7
        0x36 -> :sswitch_ef
        0x37 -> :sswitch_166
        0x38 -> :sswitch_181
        0x39 -> :sswitch_19c
        0x3b -> :sswitch_1b7
        0x3c -> :sswitch_10c
        0x65 -> :sswitch_213
        0x66 -> :sswitch_288
        0x67 -> :sswitch_2a3
        0x68 -> :sswitch_2ca
        0x69 -> :sswitch_2e9
        0x6a -> :sswitch_1c9
        0x6b -> :sswitch_1e8
        0x6d -> :sswitch_301
        0x6e -> :sswitch_324
        0x6f -> :sswitch_23a
        0x70 -> :sswitch_261
        0xc9 -> :sswitch_357
        0xca -> :sswitch_36f
        0xcb -> :sswitch_387
        0xcc -> :sswitch_3a6
    .end sparse-switch
.end method

.method public final enterHangout(Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/List;Z)V
    .registers 12
    .parameter "hangoutInfo"
    .parameter "forceConfig"
    .parameter
    .parameter "hoaConsented"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/service/Hangout$Info;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p3, participantList:Ljava/util/List;,"Ljava/util/List<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 973
    const-string v2, "GCommApp.enterHangout: %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 974
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->updateWakeLockState(Z)V

    .line 976
    iput-object v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->selectedVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 977
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v2, p1, p2, p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->enterMeeting(Lcom/google/android/apps/plus/service/Hangout$Info;ZZ)V

    .line 979
    if-nez p3, :cond_1f

    .line 980
    iput-object v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    .line 995
    :cond_1a
    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    .line 996
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    .line 997
    return-void

    .line 982
    :cond_1f
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    if-nez v2, :cond_44

    .line 983
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    .line 988
    :goto_2a
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_2e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 989
    .local v1, participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2e

    .line 985
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #participant:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    :cond_44
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    goto :goto_2a
.end method

.method public final exitMeeting()V
    .registers 2

    .prologue
    .line 1008
    const-string v0, "GCommApp.exitMeeting"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 1009
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanup()V

    .line 1010
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1011
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->exitMeeting()V

    .line 1012
    return-void
.end method

.method public final exitMeetingCleanup()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 1029
    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    .line 1032
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->updateWakeLockState(Z)V

    .line 1033
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_16
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    if-eqz v0, :cond_6d

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIncomingAudioVolume()I

    move-result v0

    :goto_20
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/phone/EsApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "INCOMING_AUDIO_VOLUME"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->onAudioFocusChange(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_70

    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    if-eq v0, v4, :cond_70

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    :goto_4f
    iput v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->mSavedAudioMode:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    .line 1034
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    if-eqz v0, :cond_66

    .line 1035
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsApplication;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1036
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->headsetBroadcastReceiver:Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;

    .line 1038
    :cond_66
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    .line 1040
    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeetingCleanupDone:Z

    .line 1041
    return-void

    .line 1033
    :cond_6d
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    goto :goto_20

    :cond_70
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setMode(I)V

    goto :goto_4f
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getApp()Lcom/google/android/apps/plus/phone/EsApplication;
    .registers 2

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    return-object v0
.end method

.method public final getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .registers 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    return-object v0
.end method

.method public final getGCommService()Lcom/google/android/apps/plus/hangout/GCommService;
    .registers 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommService:Lcom/google/android/apps/plus/hangout/GCommService;

    return-object v0
.end method

.method public final getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;
    .registers 2

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    return-object v0
.end method

.method public final getSelectedVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;
    .registers 2

    .prologue
    .line 1110
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->selectedVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public final hasAudioFocus()Z
    .registers 2

    .prologue
    .line 1118
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    return v0
.end method

.method public final isAudioMute()Z
    .registers 2

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isAudioMute()Z

    move-result v0

    return v0
.end method

.method public final isExitingHangout()Z
    .registers 2

    .prologue
    .line 533
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    return v0
.end method

.method public final isInAHangout()Z
    .registers 4

    .prologue
    const/4 v1, 0x0

    .line 519
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    if-nez v2, :cond_6

    .line 523
    :cond_5
    :goto_5
    return v1

    .line 522
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    .line 523
    .local v0, appState:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITHOUT_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-eq v0, v2, :cond_14

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITH_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v2, :cond_5

    :cond_14
    const/4 v1, 0x1

    goto :goto_5
.end method

.method public final isInAHangoutWithMedia()Z
    .registers 3

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITH_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v1, :cond_10

    const/4 v0, 0x1

    :goto_f
    return v0

    :cond_10
    const/4 v0, 0x0

    goto :goto_f
.end method

.method public final isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z
    .registers 3
    .parameter "hangoutInfo"

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v0

    if-nez v0, :cond_8

    .line 504
    const/4 v0, 0x0

    .line 507
    :goto_7
    return v0

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v0

    goto :goto_7
.end method

.method public final isOutgoingVideoMute()Z
    .registers 2

    .prologue
    .line 1099
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->outgoingVideoMute:Z

    return v0
.end method

.method public onAudioFocusChange(I)V
    .registers 9
    .parameter "focusChange"

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1170
    const-string v1, "onAudioFocusChange: %d (meeting=%s)"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1171
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-nez v1, :cond_21

    .line 1209
    :goto_20
    return-void

    .line 1175
    :cond_21
    packed-switch p1, :pswitch_data_a0

    :pswitch_24
    goto :goto_20

    .line 1201
    :pswitch_25
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIncomingAudioVolume()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    .line 1203
    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    add-int/lit8 v1, v1, 0x0

    div-int/lit8 v1, v1, 0x5

    add-int/lit8 v0, v1, 0x0

    .line 1206
    .local v0, duckLevel:I
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingAudioVolume(I)V

    .line 1207
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isAudioMute()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->muteMicOnAudioFocusGain:Z

    .line 1208
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->setAudioMute(Z)V

    goto :goto_20

    .line 1177
    .end local v0           #duckLevel:I
    :pswitch_46
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->setupAudio()V

    .line 1178
    const-string v1, "AUDIOFOCUS_GAIN: speakerphone=%s volume=%d"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIncomingAudioVolume()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_20

    .line 1186
    :pswitch_69
    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z

    .line 1187
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->isExitingHangout:Z

    if-nez v1, :cond_87

    .line 1188
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIncomingAudioVolume()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->incomingAudioLevelBeforeAudioFocusLoss:I

    .line 1190
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingAudioVolume(I)V

    .line 1192
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isAudioMute()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->muteMicOnAudioFocusGain:Z

    .line 1193
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->setAudioMute(Z)V

    .line 1195
    :cond_87
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v1, v4}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 1196
    const-string v1, "AUDIOFOCUS_LOSS: speakerphone=%s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;

    invoke-virtual {v3}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_20

    .line 1175
    :pswitch_data_a0
    .packed-switch -0x3
        :pswitch_25
        :pswitch_69
        :pswitch_69
        :pswitch_24
        :pswitch_46
    .end packed-switch
.end method

.method final raiseNetworkError()V
    .registers 4

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    const/4 v1, -0x1

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->NETWORK:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 627
    return-void
.end method

.method public final registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V
    .registers 7
    .parameter "context"
    .parameter "eventHandler"
    .parameter "isService"

    .prologue
    .line 669
    const-string v0, "Registering for events: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 671
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_1b

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/Utils;->isOnMainThread(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 672
    :cond_1b
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_27

    if-nez p2, :cond_27

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 673
    :cond_27
    if-eqz p3, :cond_3a

    .line 674
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_37

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    if-eqz v0, :cond_37

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 675
    :cond_37
    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 679
    :goto_39
    return-void

    .line 677
    :cond_3a
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_39
.end method

.method public final setAudioMute(Z)V
    .registers 3
    .parameter "mute"

    .prologue
    .line 1077
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setAudioMute(Z)V

    .line 1078
    return-void
.end method

.method public final setLastUsedCameraType(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    .registers 3
    .parameter "type"

    .prologue
    .line 1094
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_c

    if-nez p1, :cond_c

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1095
    :cond_c
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->lastUsedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    .line 1096
    return-void
.end method

.method public final setOutgoingVideoMute(Z)V
    .registers 2
    .parameter "mute"

    .prologue
    .line 1103
    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->outgoingVideoMute:Z

    .line 1104
    return-void
.end method

.method public final setSelectedVideoSource(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 2
    .parameter "member"

    .prologue
    .line 1114
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->selectedVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 1115
    return-void
.end method

.method public final shouldShowToastForMember(Lcom/google/android/apps/plus/hangout/MeetingMember;)Z
    .registers 12
    .parameter "meetingMember"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1051
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1063
    :cond_8
    :goto_8
    return v5

    .line 1055
    :cond_9
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 1058
    .local v2, now:J
    iget-wide v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_3c

    iget-wide v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutStartTime:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x1388

    cmp-long v6, v6, v8

    if-lez v6, :cond_3c

    move v0, v4

    .line 1060
    .local v0, isDefinitelyAfterStartupFilterTime:Z
    :goto_25
    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    if-eqz v6, :cond_3e

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->greenRoomParticipantIds:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3e

    move v1, v4

    .line 1063
    .local v1, isDefinitelyNewParticipant:Z
    :goto_36
    if-nez v1, :cond_3a

    if-eqz v0, :cond_8

    :cond_3a
    move v5, v4

    goto :goto_8

    .end local v0           #isDefinitelyAfterStartupFilterTime:Z
    .end local v1           #isDefinitelyNewParticipant:Z
    :cond_3c
    move v0, v5

    .line 1058
    goto :goto_25

    .restart local v0       #isDefinitelyAfterStartupFilterTime:Z
    :cond_3e
    move v1, v5

    .line 1060
    goto :goto_36
.end method

.method public final signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V
    .registers 8
    .parameter "account"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 929
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GCommApp.signinUser: signinTask="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 932
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    if-nez v2, :cond_43

    .line 933
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    if-nez v2, :cond_44

    const-string v0, "startUsingNetwork: info is null"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->raiseNetworkError()V

    move v0, v1

    :cond_2b
    :goto_2b
    if-eqz v0, :cond_43

    .line 934
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;

    .line 935
    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/phone/EsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    .line 936
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->signinTask:Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 939
    :cond_43
    return-void

    .line 933
    :cond_44
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    const-string v2, "Current network type: %d subtype: %d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    iget v4, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkSubtype:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->plusOneApplication:Lcom/google/android/apps/plus/phone/EsApplication;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->connectivityChangeListener:Lcom/google/android/apps/plus/hangout/GCommApp$ConnectivityChangeListener;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/phone/EsApplication;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentNetworkType:I

    if-ne v2, v0, :cond_2b

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v2

    if-nez v2, :cond_2b

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->wifiLock:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$WifiLock;->acquire()V

    const-string v2, "Acquired wifi lock"

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    goto :goto_2b
.end method

.method public final startingHangoutActivity(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V
    .registers 4
    .parameter "hangoutActivity"

    .prologue
    .line 630
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    .line 631
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    .line 633
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Starting HangoutActivity: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 634
    return-void
.end method

.method public final stoppingHangoutActivity()V
    .registers 3

    .prologue
    .line 637
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_e

    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    if-gtz v0, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 638
    :cond_e
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    .line 639
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->currentStartedHangoutActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    .line 641
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Stopping HangoutActivity: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 643
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    if-nez v0, :cond_30

    .line 664
    :cond_2f
    :goto_2f
    return-void

    .line 646
    :cond_30
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->hangoutSigninRefCount:I

    if-nez v0, :cond_2f

    .line 648
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 649
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isOutgoingVideoStarted()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 654
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    goto :goto_2f

    .line 661
    :cond_48
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    goto :goto_2f
.end method

.method public final unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V
    .registers 7
    .parameter "context"
    .parameter "eventHandler"
    .parameter "isService"

    .prologue
    .line 683
    const-string v0, "Unregistering for events: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 685
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_1b

    invoke-static {p1}, Lcom/google/android/apps/plus/hangout/Utils;->isOnMainThread(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1b

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 686
    :cond_1b
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_27

    if-nez p2, :cond_27

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 687
    :cond_27
    if-eqz p3, :cond_3b

    .line 688
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_37

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    if-eq p2, v0, :cond_37

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 689
    :cond_37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->serviceEventHandler:Lcom/google/android/apps/plus/hangout/GCommEventHandler;

    .line 694
    :goto_3a
    return-void

    .line 691
    :cond_3b
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp;->$assertionsDisabled:Z

    if-nez v0, :cond_4d

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4d

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 692
    :cond_4d
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp;->eventHandlers:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_3a
.end method
