.class public Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;
.super Ljava/lang/Object;
.source "GCommNativeWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VideoSourceChangedMessageParams"
.end annotation


# instance fields
.field private final requestID:I

.field private final source:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private final videoAvailable:Z


# direct methods
.method public constructor <init>(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .registers 4
    .parameter "requestID"
    .parameter "source"
    .parameter "videoAvailable"

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    iput p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->requestID:I

    .line 151
    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->source:Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 152
    iput-boolean p3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->videoAvailable:Z

    .line 153
    return-void
.end method


# virtual methods
.method public final getRequestID()I
    .registers 2

    .prologue
    .line 156
    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->requestID:I

    return v0
.end method

.method public final getSource()Lcom/google/android/apps/plus/hangout/MeetingMember;
    .registers 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->source:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public final isVideoAvailable()Z
    .registers 2

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->videoAvailable:Z

    return v0
.end method
