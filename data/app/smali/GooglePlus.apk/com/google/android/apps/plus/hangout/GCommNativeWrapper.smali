.class public Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
.super Ljava/lang/Object;
.source "GCommNativeWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$DeviceCaptureType;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;,
        Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final GCOMM_NATIVE_LIB_API_LEVEL:I = 0x5

.field public static final HANGOUT:Ljava/lang/String; = "HANGOUT"

.field public static final HANGOUT_SYNC:Ljava/lang/String; = "HANGOUT_SYNC"

.field public static final INVALID_INCOMING_VIDEO_REQUEST_ID:I = 0x0

.field static final MAX_INCOMING_AUDIO_LEVEL:I = 0xff

.field static final MIN_INCOMING_AUDIO_LEVEL:I = 0x0

.field private static final SELF_MUC_JID_BEFORE_ENTERING_HANGOUT:Ljava/lang/String; = ""

.field public static final TRANSFER:Ljava/lang/String; = "TRANSFER"


# instance fields
.field private volatile account:Lcom/google/android/apps/plus/content/EsAccount;

.field private volatile clientInitiatedExit:Z

.field private final context:Landroid/content/Context;

.field private volatile hadSomeConnectedParticipant:Z

.field private volatile hangoutCreated:Z

.field private volatile hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

.field private volatile isHangoutLite:Z

.field private volatile memberMucJidToMeetingMember:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            ">;"
        }
    .end annotation
.end field

.field private volatile membersCount:I

.field private volatile nativePeerObject:J

.field private volatile retrySignin:Z

.field private volatile ringInvitees:Z

.field private volatile roomHistory:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;",
            ">;"
        }
    .end annotation
.end field

.field private volatile selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private volatile userJid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_1b

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->$assertionsDisabled:Z

    .line 47
    const-string v0, "GCommNativeWrapper loading gcomm_ini"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 52
    const-string v0, "gcomm_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 54
    const-string v0, "GCommNativeWrapper done loading gcomm_ini"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 55
    return-void

    .line 41
    :cond_1b
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->retrySignin:Z

    .line 357
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    .line 358
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->roomHistory:Ljava/util/List;

    .line 359
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    .line 361
    return-void
.end method

.method private static ToArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)",
            "Ljava/util/ArrayList",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 515
    .local p0, array:[Ljava/lang/Object;,"[TT;"
    new-instance v3, Ljava/util/ArrayList;

    array-length v5, p0

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 516
    .local v3, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<TT;>;"
    move-object v0, p0

    .local v0, arr$:[Ljava/lang/Object;
    array-length v2, p0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_9
    if-ge v1, v2, :cond_13

    aget-object v4, v0, v1

    .line 517
    .local v4, t:Ljava/lang/Object;,"TT;"
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 516
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 519
    .end local v4           #t:Ljava/lang/Object;,"TT;"
    :cond_13
    return-object v3
.end method

.method public static initialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "context"
    .parameter "privateStoragePath"
    .parameter "crashReportProduct"
    .parameter "crashReportVersion"
    .parameter "canLogPII"
    .parameter "clientVersion"
    .parameter "logLevel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/LinkageError;
        }
    .end annotation

    .prologue
    .line 311
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStaticGetVersion()I

    move-result v0

    .line 312
    .local v0, nativeVersion:I
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GComm native lib API version:     "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 313
    const-string v1, "GComm native wrapper API version: 5"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 314
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GComm native lib logging: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at level "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 315
    const/4 v1, 0x5

    if-eq v0, v1, :cond_45

    .line 317
    const-string v1, "GComm native lib version mismatch"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 318
    new-instance v1, Ljava/lang/UnsupportedClassVersionError;

    invoke-direct {v1}, Ljava/lang/UnsupportedClassVersionError;-><init>()V

    throw v1

    .line 322
    :cond_45
    invoke-static/range {p0 .. p6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStaticInitialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_56

    .line 325
    const-string v1, "GComm native lib initialization failed"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 326
    new-instance v1, Ljava/lang/ExceptionInInitializerError;

    invoke-direct {v1}, Ljava/lang/ExceptionInInitializerError;-><init>()V

    throw v1

    .line 328
    :cond_56
    return-void
.end method

.method private native nativeBlockMedia(Ljava/lang/String;)V
.end method

.method private native nativeConnectAndSignin(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeCreateHangout()V
.end method

.method private native nativeEnterMeeting(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
.end method

.method private native nativeEnterMeetingWithCachedGreenRoomInfo(Z)V
.end method

.method private native nativeExitMeeting()V
.end method

.method private native nativeGetIncomingAudioVolume()I
.end method

.method private native nativeInitializeIncomingVideoRenderer(I)Z
.end method

.method private native nativeInviteToMeeting([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)V
.end method

.method private native nativeIsAudioMute()Z
.end method

.method private native nativeIsOutgoingVideoStarted()Z
.end method

.method private native nativeKickMeetingMember(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativePeerCreate()J
.end method

.method private native nativePeerDestroy(J)V
.end method

.method private native nativeProvideOutgoingVideoFrame([BJI)V
.end method

.method private native nativeRemoteMute(Ljava/lang/String;)V
.end method

.method private native nativeRenderIncomingVideoFrame(I)Z
.end method

.method private native nativeRequestVCard(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeSendInstantMessage(Ljava/lang/String;)V
.end method

.method private native nativeSendInstantMessageToUser(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeSendRingStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private native nativeSetAudioMute(Z)V
.end method

.method private native nativeSetIncomingAudioVolume(I)V
.end method

.method private native nativeSetIncomingVideoParameters(IIIII)V
.end method

.method private native nativeSetIncomingVideoRendererSurfaceSize(III)Z
.end method

.method private native nativeSetIncomingVideoSourceToSpeakerIndex(II)V
.end method

.method private native nativeSetIncomingVideoSourceToUser(ILjava/lang/String;)V
.end method

.method private native nativeSetPresenceConnectionStatus(I)V
.end method

.method private native nativeSignoutAndDisconnect()V
.end method

.method public static native nativeSimulateCrash()V
.end method

.method private native nativeStartIncomingVideoForSpeakerIndex(IIII)I
.end method

.method private native nativeStartIncomingVideoForUser(Ljava/lang/String;III)I
.end method

.method private native nativeStartOutgoingVideo(II)V
.end method

.method public static native nativeStaticCleanup()V
.end method

.method private static native nativeStaticGetVersion()I
.end method

.method private static native nativeStaticInitialize(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Z
.end method

.method public static native nativeStaticSetDeviceCaptureType(I)V
.end method

.method private native nativeStopIncomingVideo(I)V
.end method

.method private native nativeStopOutgoingVideo()V
.end method

.method private native nativeUploadCallgrokLog()V
.end method

.method private onAudioMuteStateChanged(Ljava/lang/String;Z)V
    .registers 7
    .parameter "memberMucJid"
    .parameter "isMuted"

    .prologue
    .line 1083
    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1085
    const/4 v0, 0x0

    .line 1094
    .local v0, member:Lcom/google/android/apps/plus/hangout/MeetingMember;
    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x65

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1096
    :goto_18
    return-void

    .line 1087
    .end local v0           #member:Lcom/google/android/apps/plus/hangout/MeetingMember;
    :cond_19
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 1089
    .restart local v0       #member:Lcom/google/android/apps/plus/hangout/MeetingMember;
    if-nez v0, :cond_9

    goto :goto_18
.end method

.method private onCallgrokLogUploadCompleted(ILjava/lang/String;)V
    .registers 6
    .parameter "error"
    .parameter "message"

    .prologue
    .line 743
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x3c

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 745
    return-void
.end method

.method private onCurrentSpeakerChanged(Ljava/lang/String;)V
    .registers 5
    .parameter "memberMucJid"

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x66

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1131
    return-void
.end method

.method private onError(I)V
    .registers 7
    .parameter "err"

    .prologue
    const/4 v4, 0x0

    .line 471
    const-string v1, "GCommNativeWrapper.onError: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 472
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    move-result-object v1

    aget-object v0, v1, p1

    .line 473
    .local v0, error:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->AUTHENTICATION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    if-ne v0, v1, :cond_3f

    .line 474
    const-string v1, "Invalidating auth token..."

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 477
    :try_start_1e
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "webupdates"

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/network/AuthData;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_1e .. :try_end_2b} :catch_46

    .line 482
    :goto_2b
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->retrySignin:Z

    if-eqz v1, :cond_3f

    .line 483
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 484
    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->retrySignin:Z

    .line 489
    :goto_3e
    return-void

    .line 488
    :cond_3f
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v2, -0x1

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_3e

    :catch_46
    move-exception v1

    goto :goto_2b
.end method

.method private onHangoutCreated(Ljava/lang/String;)V
    .registers 10
    .parameter "hangoutId"

    .prologue
    const/4 v2, 0x0

    .line 757
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutCreated:Z

    .line 758
    new-instance v0, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Creation:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    iget-boolean v7, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->ringInvitees:Z

    move-object v3, v2

    move-object v4, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    .line 763
    .local v0, hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x32

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 764
    return-void
.end method

.method private onIncomingVideoFrameDimensionsChanged(III)V
    .registers 8
    .parameter "requestID"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x6b

    new-instance v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;

    new-instance v3, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    invoke-direct {v3, p2, p3}, Lcom/google/android/apps/plus/hangout/RectangleDimensions;-><init>(II)V

    invoke-direct {v2, p1, v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;-><init>(ILcom/google/android/apps/plus/hangout/RectangleDimensions;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1154
    return-void
.end method

.method private onIncomingVideoFrameReceived(I)V
    .registers 5
    .parameter "requestID"

    .prologue
    .line 1145
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x6a

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1147
    return-void
.end method

.method private onIncomingVideoStarted(I)V
    .registers 5
    .parameter "requestID"

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x68

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1115
    return-void
.end method

.method private onInstantMessageReceived(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "fromMucJid"
    .parameter "message"

    .prologue
    .line 848
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 849
    .local v1, meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    if-nez v1, :cond_1c

    .line 850
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onInstantMessageReceived missing fromMucJid: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 853
    :cond_1c
    new-instance v0, Lcom/google/android/apps/plus/hangout/InstantMessage;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/apps/plus/hangout/InstantMessage;-><init>(Lcom/google/android/apps/plus/hangout/MeetingMember;Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    .local v0, instantMessage:Lcom/google/android/apps/plus/hangout/InstantMessage;
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v3, 0x3b

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 857
    return-void
.end method

.method private onMediaBlock(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 10
    .parameter "blockeeMucJid"
    .parameter "blockerMucJid"
    .parameter "isRecording"

    .prologue
    const/4 v3, 0x1

    .line 1171
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 1172
    .local v0, blockee:Lcom/google/android/apps/plus/hangout/MeetingMember;
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 1173
    .local v1, blocker:Lcom/google/android/apps/plus/hangout/MeetingMember;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 1174
    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setMediaBlocked(Z)V

    .line 1181
    :cond_1a
    :goto_1a
    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-nez v2, :cond_23

    .line 1182
    const/4 p3, 0x0

    .line 1186
    :cond_23
    if-nez p3, :cond_29

    if-eqz v1, :cond_3c

    if-eqz v0, :cond_3c

    .line 1187
    :cond_29
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v3, 0x6e

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1190
    :cond_3c
    return-void

    .line 1175
    :cond_3d
    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 1176
    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setMediaBlocked(Z)V

    goto :goto_1a
.end method

.method private onMeetingEnterError(I)V
    .registers 5
    .parameter "errorCode"

    .prologue
    .line 695
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    move-result-object v1

    aget-object v0, v1, p1

    .line 696
    .local v0, error:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->HANGOUT_OVER:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    if-ne v0, v1, :cond_22

    .line 699
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v1, :cond_22

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->MissedCall:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v1, v2, :cond_22

    .line 701
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    .line 702
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    .line 712
    :goto_21
    return-void

    .line 708
    :cond_22
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clearMeetingState()V

    .line 709
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v2, -0x3

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_21
.end method

.method private onMeetingExited()V
    .registers 5

    .prologue
    .line 736
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clientInitiatedExit:Z

    .line 737
    .local v0, clientInitiatedExit:Z
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clearMeetingState()V

    .line 738
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v3, 0x36

    if-eqz v0, :cond_14

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    :goto_10
    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 740
    return-void

    .line 738
    :cond_14
    const/4 v1, 0x0

    goto :goto_10
.end method

.method private onMeetingMediaStarted()V
    .registers 3

    .prologue
    .line 732
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x35

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    .line 733
    return-void
.end method

.method private onMeetingMemberEntered(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 13
    .parameter "memberMucJid"
    .parameter "nickName"
    .parameter "obfuscatedGaiaId"
    .parameter "presenceConnectionStatus"

    .prologue
    .line 768
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isHangoutLite:Z

    if-nez v1, :cond_35

    if-eqz p3, :cond_e

    const-string v1, ""

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_35

    .line 770
    :cond_e
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring invalid user: JID="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nickname="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ID=<empty> status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 800
    :goto_34
    return-void

    .line 775
    :cond_35
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v6

    .line 776
    .local v6, isSelfProfile:Z
    new-instance v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget v4, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    add-int/lit8 v1, v4, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/hangout/MeetingMember;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 783
    .local v0, meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    move-result-object v1

    aget-object v7, v1, p4

    .line 785
    .local v7, connectionStatus:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;
    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->CONNECTING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    if-eq v7, v1, :cond_58

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->JOINING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    if-ne v7, v1, :cond_7e

    .line 787
    :cond_58
    sget-object v1, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTING:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setCurrentStatus(Lcom/google/android/apps/plus/hangout/MeetingMember$Status;)V

    .line 791
    :goto_5d
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 793
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    if-nez v1, :cond_71

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    if-ne v1, v2, :cond_71

    .line 795
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    .line 797
    :cond_71
    const-string v1, ""

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeRequestVCard(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x37

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_34

    .line 789
    :cond_7e
    sget-object v1, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setCurrentStatus(Lcom/google/android/apps/plus/hangout/MeetingMember$Status;)V

    goto :goto_5d
.end method

.method private onMeetingMemberExited(Ljava/lang/String;)V
    .registers 5
    .parameter "memberMucJid"

    .prologue
    .line 838
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 839
    .local v0, meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    if-nez v0, :cond_1d

    .line 840
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onMeetingMemberExited missing memberMucJid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 845
    :goto_1c
    return-void

    .line 843
    :cond_1d
    sget-object v1, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->DISCONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setCurrentStatus(Lcom/google/android/apps/plus/hangout/MeetingMember$Status;)V

    .line 844
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x39

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_1c
.end method

.method private onMeetingMemberPresenceConnectionStateChanged(Ljava/lang/String;I)V
    .registers 8
    .parameter "memberMucJid"
    .parameter "presenceConnectionStatus"

    .prologue
    .line 804
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 805
    .local v1, meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    if-nez v1, :cond_1d

    .line 806
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onMeetingMemberPresenceConnectionStateChanged missing memberMucJid: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 835
    :cond_1c
    :goto_1c
    return-void

    .line 811
    :cond_1d
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    move-result-object v3

    aget-object v0, v3, p2

    .line 814
    .local v0, connectionStatus:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;
    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->CONNECTING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    if-eq v0, v3, :cond_2b

    sget-object v3, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->JOINING:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;

    if-ne v0, v3, :cond_4d

    .line 816
    :cond_2b
    sget-object v2, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTING:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    .line 821
    .local v2, newStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;
    :goto_2d
    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v3

    if-eq v2, v3, :cond_1c

    .line 825
    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setCurrentStatus(Lcom/google/android/apps/plus/hangout/MeetingMember$Status;)V

    .line 826
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    if-nez v3, :cond_45

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    if-ne v3, v4, :cond_45

    .line 828
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    .line 831
    :cond_45
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v4, 0x38

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_1c

    .line 818
    .end local v2           #newStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;
    :cond_4d
    sget-object v2, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    .restart local v2       #newStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;
    goto :goto_2d
.end method

.method private onMucEntered(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 11
    .parameter "selfMucJid"
    .parameter "selfNickName"
    .parameter "isHangoutLite"

    .prologue
    const/4 v5, 0x1

    .line 715
    new-instance v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    add-int/lit8 v1, v4, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    move-object v1, p1

    move-object v2, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/hangout/MeetingMember;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeRequestVCard(Ljava/lang/String;Ljava/lang/String;)V

    .line 727
    iput-boolean p3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isHangoutLite:Z

    .line 728
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x34

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 729
    return-void
.end method

.method public static onNativeCrash()V
    .registers 1

    .prologue
    .line 466
    const-string v0, "GCommNativeWrapper.onNativeCrash - Crash from native code!!!"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 467
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommApp;->reportNativeCrash()V

    .line 468
    return-void
.end method

.method private onOutgoingVideoStarted()V
    .registers 3

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v1, 0x69

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    .line 1158
    return-void
.end method

.method private onReceivedRoomHistory([Ljava/lang/String;[Ljava/lang/String;)V
    .registers 16
    .parameter "roomNames"
    .parameter "lastEnterTimes"

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 523
    sget-boolean v8, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->$assertionsDisabled:Z

    if-nez v8, :cond_11

    array-length v8, p1

    array-length v9, p2

    if-eq v8, v9, :cond_11

    new-instance v8, Ljava/lang/AssertionError;

    invoke-direct {v8}, Ljava/lang/AssertionError;-><init>()V

    throw v8

    .line 524
    :cond_11
    new-instance v5, Ljava/util/ArrayList;

    array-length v8, p1

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 525
    .local v5, roomHistory:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;>;"
    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "@"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    aget-object v7, v8, v10

    .line 526
    .local v7, userDomain:Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_26
    array-length v8, p1

    if-ge v2, v8, :cond_6b

    .line 527
    aget-object v8, p1, v2

    const-string v9, "@"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 528
    .local v6, roomNameParts:[Ljava/lang/String;
    array-length v8, v6

    if-eq v8, v12, :cond_4b

    .line 529
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Bad format for room history: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v9, p1, v2

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    .line 526
    :goto_48
    add-int/lit8 v2, v2, 0x1

    goto :goto_26

    .line 532
    :cond_4b
    aget-object v0, v6, v11

    .line 533
    .local v0, bareRoomName:Ljava/lang/String;
    aget-object v3, v6, v10

    .line 534
    .local v3, roomDomainName:Ljava/lang/String;
    move-object v1, v0

    .line 535
    .local v1, displayRoomName:Ljava/lang/String;
    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_62

    .line 536
    const-string v8, "%s (%s)"

    new-array v9, v12, [Ljava/lang/Object;

    aput-object v0, v9, v11

    aput-object v3, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 538
    :cond_62
    new-instance v4, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;

    invoke-direct {v4, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;-><init>(Ljava/lang/String;)V

    .line 539
    .local v4, roomEntry:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_48

    .line 544
    .end local v0           #bareRoomName:Ljava/lang/String;
    .end local v1           #displayRoomName:Ljava/lang/String;
    .end local v3           #roomDomainName:Ljava/lang/String;
    .end local v4           #roomEntry:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;
    .end local v6           #roomNameParts:[Ljava/lang/String;
    :cond_6b
    iput-object v5, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->roomHistory:Ljava/util/List;

    .line 545
    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v9, 0x5

    invoke-static {v8, v9, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 546
    return-void
.end method

.method private onRemoteMute(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "muteeMucJid"
    .parameter "muterMucJid"

    .prologue
    .line 1161
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 1162
    .local v0, mutee:Lcom/google/android/apps/plus/hangout/MeetingMember;
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 1164
    .local v1, muter:Lcom/google/android/apps/plus/hangout/MeetingMember;
    if-eqz v1, :cond_1f

    if-eqz v0, :cond_1f

    .line 1165
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v3, 0x6d

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1168
    :cond_1f
    return-void
.end method

.method private onSignedIn(Ljava/lang/String;)V
    .registers 4
    .parameter "userJid"

    .prologue
    .line 496
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->userJid:Ljava/lang/String;

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 498
    return-void
.end method

.method private onSignedOut()V
    .registers 3

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    .line 502
    return-void
.end method

.method private onSigninTimeOutError()V
    .registers 3

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v1, -0x2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    .line 493
    return-void
.end method

.method public static onUnhandledJavaException(Ljava/lang/Throwable;)V
    .registers 1
    .parameter "ex"

    .prologue
    .line 462
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->reportJavaCrashFromNativeCode(Ljava/lang/Throwable;)V

    .line 463
    return-void
.end method

.method private onVCardResponse(Ljava/lang/String;Lcom/google/android/apps/plus/hangout/VCard;)V
    .registers 6
    .parameter "memberMucJid"
    .parameter "vCard"

    .prologue
    .line 505
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 506
    .local v0, meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    if-nez v0, :cond_1d

    .line 507
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onVCardResponse missing memberMucJid: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->warn(Ljava/lang/String;)V

    .line 512
    :goto_1c
    return-void

    .line 510
    :cond_1d
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setVCard(Lcom/google/android/apps/plus/hangout/VCard;)V

    .line 511
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_1c
.end method

.method private onVideoPauseStateChanged(Ljava/lang/String;Z)V
    .registers 7
    .parameter "memberMucJid"
    .parameter "isPaused"

    .prologue
    .line 1099
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 1101
    .local v0, member:Lcom/google/android/apps/plus/hangout/MeetingMember;
    if-nez v0, :cond_b

    .line 1109
    :goto_a
    return-void

    .line 1104
    :cond_b
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->setVideoPaused(Z)V

    .line 1107
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x6f

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_a
.end method

.method private onVideoSourceChanged(ILjava/lang/String;Z)V
    .registers 7
    .parameter "requestID"
    .parameter "memberMucJid"
    .parameter "videoAvailable"

    .prologue
    .line 1137
    new-instance v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {v0, p1, v1, p3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;-><init>(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    .line 1139
    .local v0, params:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x67

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    .line 1140
    return-void
.end method

.method private onVolumeChanged(Ljava/lang/String;I)V
    .registers 7
    .parameter "memberMucJid"
    .parameter "volume"

    .prologue
    .line 1118
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 1120
    .local v0, member:Lcom/google/android/apps/plus/hangout/MeetingMember;
    if-nez v0, :cond_b

    .line 1126
    :goto_a
    return-void

    .line 1124
    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->context:Landroid/content/Context;

    const/16 v2, 0x70

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto :goto_a
.end method


# virtual methods
.method public blockMedia(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 6
    .parameter "member"

    .prologue
    .line 444
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 448
    :goto_8
    return-void

    .line 447
    :cond_9
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getMucJid()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeBlockMedia(Ljava/lang/String;)V

    goto :goto_8
.end method

.method clearMeetingState()V
    .registers 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 639
    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    .line 640
    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 642
    iput v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->membersCount:I

    .line 643
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    .line 644
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutCreated:Z

    .line 645
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->ringInvitees:Z

    .line 646
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clientInitiatedExit:Z

    .line 647
    return-void
.end method

.method public connectAndSignin(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .registers 9
    .parameter "account"
    .parameter "talkAuthToken"

    .prologue
    const/4 v1, 0x1

    .line 391
    iget-wide v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_12

    move v0, v1

    :goto_a
    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_12
    const/4 v0, 0x0

    goto :goto_a

    .line 393
    :cond_14
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->retrySignin:Z

    .line 394
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerCreate()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    .line 395
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Created native peer: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 397
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeConnectAndSignin(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    return-void
.end method

.method public createHangout(Z)V
    .registers 6
    .parameter "ringInvitees"

    .prologue
    .line 569
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 575
    :goto_8
    return-void

    .line 573
    :cond_9
    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->ringInvitees:Z

    .line 574
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeCreateHangout()V

    goto :goto_8
.end method

.method public enterMeeting(Lcom/google/android/apps/plus/service/Hangout$Info;ZZ)V
    .registers 12
    .parameter "hangoutInfo"
    .parameter "forceConfig"
    .parameter "hoaConsented"

    .prologue
    .line 578
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_9

    .line 590
    :goto_8
    return-void

    .line 582
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clearMeetingState()V

    .line 583
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    .line 584
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getDomain()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_39

    const-string v2, ""

    .line 585
    .local v2, domain:Ljava/lang/String;
    :goto_16
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$RoomType;->ordinal()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getServiceId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3e

    const-string v3, ""

    :goto_26
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getNick()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_43

    const-string v5, ""

    :goto_32
    move-object v0, p0

    move v6, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeEnterMeeting(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_8

    .line 584
    .end local v2           #domain:Ljava/lang/String;
    :cond_39
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getDomain()Ljava/lang/String;

    move-result-object v2

    goto :goto_16

    .line 585
    .restart local v2       #domain:Ljava/lang/String;
    :cond_3e
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getServiceId()Ljava/lang/String;

    move-result-object v3

    goto :goto_26

    :cond_43
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getNick()Ljava/lang/String;

    move-result-object v5

    goto :goto_32
.end method

.method public exitMeeting()V
    .registers 5

    .prologue
    .line 593
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 598
    :goto_8
    return-void

    .line 596
    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->clientInitiatedExit:Z

    .line 597
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeExitMeeting()V

    goto :goto_8
.end method

.method public getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->account:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    .registers 5

    .prologue
    .line 366
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    .line 367
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->NONE:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    .line 369
    :goto_a
    return-object v0

    :cond_b
    invoke-static {}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->values()[Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeGetCurrentState()I

    move-result v1

    aget-object v0, v0, v1

    goto :goto_a
.end method

.method public getHadSomeConnectedParticipantInPast()Z
    .registers 2

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hadSomeConnectedParticipant:Z

    return v0
.end method

.method public getHangoutCreated()Z
    .registers 2

    .prologue
    .line 631
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutCreated:Z

    return v0
.end method

.method public getHangoutDomain()Ljava/lang/String;
    .registers 2

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->getDomain()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHangoutId()Ljava/lang/String;
    .registers 2

    .prologue
    .line 627
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->getId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHangoutInfo()Lcom/google/android/apps/plus/service/Hangout$Info;
    .registers 2

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    return-object v0
.end method

.method public getHangoutRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;
    .registers 2

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v0

    return-object v0
.end method

.method public getHasSomeConnectedParticipant()Z
    .registers 5

    .prologue
    .line 279
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 280
    .local v1, meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->CONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    if-ne v2, v3, :cond_a

    .line 281
    const/4 v2, 0x1

    .line 284
    .end local v1           #meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    :goto_1f
    return v2

    :cond_20
    const/4 v2, 0x0

    goto :goto_1f
.end method

.method public getIncomingAudioVolume()I
    .registers 5

    .prologue
    .line 1055
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a

    .line 1056
    const/4 v0, 0x0

    .line 1059
    :goto_9
    return v0

    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeGetIncomingAudioVolume()I

    move-result v0

    goto :goto_9
.end method

.method public getIsHangoutLite()Z
    .registers 2

    .prologue
    .line 635
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->isHangoutLite:Z

    return v0
.end method

.method public getMeetingMember(Ljava/lang/String;)Lcom/google/android/apps/plus/hangout/MeetingMember;
    .registers 3
    .parameter "memberMucJid"

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public getMeetingMemberCount()I
    .registers 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public getMeetingMembersOrderedByEntry()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 260
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 261
    .local v0, members:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/google/android/apps/plus/hangout/MeetingMember;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 262
    .local v1, membersList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/hangout/MeetingMember;>;"
    new-instance v2, Lcom/google/android/apps/plus/hangout/MeetingMember$SortByEntryOrder;

    invoke-direct {v2}, Lcom/google/android/apps/plus/hangout/MeetingMember$SortByEntryOrder;-><init>()V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 263
    return-object v1
.end method

.method public getRoomHistory()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$RoomEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->roomHistory:Ljava/util/List;

    return-object v0
.end method

.method public getSelfMeetingMember()Lcom/google/android/apps/plus/hangout/MeetingMember;
    .registers 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->selfMeetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public getUserJid()Ljava/lang/String;
    .registers 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->userJid:Ljava/lang/String;

    return-object v0
.end method

.method public initializeIncomingVideoRenderer(I)Z
    .registers 6
    .parameter "requestID"

    .prologue
    .line 1008
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a

    .line 1009
    const/4 v0, 0x0

    .line 1011
    :goto_9
    return v0

    :cond_a
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeInitializeIncomingVideoRenderer(I)Z

    move-result v0

    goto :goto_9
.end method

.method inviteToMeeting(Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;ZZ)V
    .registers 25
    .parameter "inviteAudience"
    .parameter "inviteType"
    .parameter "ringParticipants"
    .parameter "createActivity"

    .prologue
    .line 651
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v18, 0x0

    cmp-long v1, v4, v18

    if-nez v1, :cond_b

    .line 692
    :goto_a
    return-void

    .line 654
    :cond_b
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/api/ApiUtils;->removeCircleIdNamespaces(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object p1

    .line 656
    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    .line 657
    .local v14, memberGaiaIdSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->memberMucJidToMeetingMember:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, i$:Ljava/util/Iterator;
    :cond_20
    :goto_20
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3a

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 658
    .local v13, meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    invoke-virtual {v13}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 659
    .local v10, gaiaId:Ljava/lang/String;
    if-eqz v10, :cond_20

    .line 660
    invoke-virtual {v14, v10}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_20

    .line 664
    .end local v10           #gaiaId:Ljava/lang/String;
    .end local v13           #meetingMember:Lcom/google/android/apps/plus/hangout/MeetingMember;
    :cond_3a
    new-instance v17, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v1

    move-object/from16 v0, v17

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 665
    .local v17, userIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v7

    .local v7, arr$:[Lcom/google/android/apps/plus/content/PersonData;
    array-length v12, v7

    .local v12, len$:I
    const/4 v11, 0x0

    .local v11, i$:I
    :goto_4b
    if-ge v11, v12, :cond_8f

    aget-object v16, v7, v11

    .line 666
    .local v16, person:Lcom/google/android/apps/plus/content/PersonData;
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v15

    .line 667
    .local v15, obfuscatedId:Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_70

    .line 668
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Person object with no id: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 665
    :goto_6d
    add-int/lit8 v11, v11, 0x1

    goto :goto_4b

    .line 671
    :cond_70
    invoke-virtual {v14, v15}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_89

    .line 672
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Skip adding: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_6d

    .line 675
    :cond_89
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6d

    .line 677
    .end local v15           #obfuscatedId:Ljava/lang/String;
    .end local v16           #person:Lcom/google/android/apps/plus/content/PersonData;
    :cond_8f
    new-instance v9, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v1

    invoke-direct {v9, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 678
    .local v9, circleIds:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v7

    .local v7, arr$:[Lcom/google/android/apps/plus/content/CircleData;
    array-length v12, v7

    const/4 v11, 0x0

    :goto_9e
    if-ge v11, v12, :cond_ac

    aget-object v8, v7, v11

    .line 679
    .local v8, circle:Lcom/google/android/apps/plus/content/CircleData;
    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 678
    add-int/lit8 v11, v11, 0x1

    goto :goto_9e

    .line 682
    .end local v8           #circle:Lcom/google/android/apps/plus/content/CircleData;
    :cond_ac
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_c5

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_c5

    const-string v1, "TRANSFER"

    move-object/from16 v0, p2

    if-eq v0, v1, :cond_c5

    .line 683
    const-string v1, "Skipping invite since no one to invite"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 686
    :cond_c5
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v2, v1, [Ljava/lang/String;

    .line 687
    .local v2, userIdArray:[Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 688
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v3, v1, [Ljava/lang/String;

    .line 689
    .local v3, circleIdArray:[Ljava/lang/String;
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-object/from16 v1, p0

    move-object/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    .line 690
    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeInviteToMeeting([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto/16 :goto_a
.end method

.method public isAudioMute()Z
    .registers 5

    .prologue
    .line 913
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a

    .line 914
    const/4 v0, 0x0

    .line 916
    :goto_9
    return v0

    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeIsAudioMute()Z

    move-result v0

    goto :goto_9
.end method

.method public isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z
    .registers 3
    .parameter "hangoutInfo"

    .prologue
    .line 608
    if-nez p1, :cond_4

    .line 609
    const/4 v0, 0x0

    .line 611
    :goto_3
    return v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/Hangout$Info;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_3
.end method

.method public isOutgoingVideoStarted()Z
    .registers 5

    .prologue
    .line 998
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a

    .line 999
    const/4 v0, 0x0

    .line 1001
    :goto_9
    return v0

    :cond_a
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeIsOutgoingVideoStarted()Z

    move-result v0

    goto :goto_9
.end method

.method public kickMeetingMember(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "userMucJid"
    .parameter "reason"

    .prologue
    .line 423
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 427
    :goto_8
    return-void

    .line 426
    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeKickMeetingMember(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method public native nativeGetCurrentState()I
.end method

.method public provideOutgoingVideoFrame([BJI)V
    .registers 9
    .parameter "data"
    .parameter "captureTime"
    .parameter "frameRotation"

    .prologue
    .line 1046
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 1052
    :cond_8
    :goto_8
    return-void

    .line 1049
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->IN_MEETING_WITH_MEDIA:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v0, v1, :cond_8

    .line 1050
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeProvideOutgoingVideoFrame([BJI)V

    goto :goto_8
.end method

.method public remoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 6
    .parameter "member"

    .prologue
    .line 437
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 441
    :goto_8
    return-void

    .line 440
    :cond_9
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getMucJid()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeRemoteMute(Ljava/lang/String;)V

    goto :goto_8
.end method

.method public renderIncomingVideoFrame(I)Z
    .registers 6
    .parameter "requestID"

    .prologue
    .line 1028
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a

    .line 1029
    const/4 v0, 0x0

    .line 1031
    :goto_9
    return v0

    :cond_a
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeRenderIncomingVideoFrame(I)Z

    move-result v0

    goto :goto_9
.end method

.method public sendInstantMessage(Ljava/lang/String;)V
    .registers 6
    .parameter "message"

    .prologue
    .line 750
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 754
    :goto_8
    return-void

    .line 753
    :cond_9
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSendInstantMessage(Ljava/lang/String;)V

    goto :goto_8
.end method

.method public sendInstantMessageToUser(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "userJid"
    .parameter "message"

    .prologue
    .line 416
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 420
    :goto_8
    return-void

    .line 419
    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSendInstantMessageToUser(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method public sendRingStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "inviteId"
    .parameter "hangoutId"
    .parameter "status"

    .prologue
    .line 451
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 455
    :goto_8
    return-void

    .line 454
    :cond_9
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSendRingStatus(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method public setAudioMute(Z)V
    .registers 6
    .parameter "mute"

    .prologue
    .line 927
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 931
    :goto_8
    return-void

    .line 930
    :cond_9
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetAudioMute(Z)V

    goto :goto_8
.end method

.method public setIncomingAudioVolume(I)V
    .registers 6
    .parameter "level"

    .prologue
    .line 1071
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 1079
    :goto_8
    return-void

    .line 1075
    :cond_9
    if-ltz p1, :cond_f

    const/16 v0, 0xff

    if-le p1, v0, :cond_24

    .line 1076
    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "level is "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1078
    :cond_24
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingAudioVolume(I)V

    goto :goto_8
.end method

.method public setIncomingVideoParameters(IIILcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;I)V
    .registers 12
    .parameter "requestID"
    .parameter "width"
    .parameter "height"
    .parameter "scalingMode"
    .parameter "framerate"

    .prologue
    .line 969
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 974
    :goto_8
    return-void

    .line 972
    :cond_9
    invoke-virtual {p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;->ordinal()I

    move-result v4

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingVideoParameters(IIIII)V

    goto :goto_8
.end method

.method public setIncomingVideoRendererSurfaceSize(III)Z
    .registers 8
    .parameter "requestID"
    .parameter "width"
    .parameter "height"

    .prologue
    .line 1018
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a

    .line 1019
    const/4 v0, 0x0

    .line 1021
    :goto_9
    return v0

    :cond_a
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingVideoRendererSurfaceSize(III)Z

    move-result v0

    goto :goto_9
.end method

.method public setIncomingVideoSourceToSpeakerIndex(II)V
    .registers 7
    .parameter "requestID"
    .parameter "index"

    .prologue
    .line 954
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 958
    :goto_8
    return-void

    .line 957
    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingVideoSourceToSpeakerIndex(II)V

    goto :goto_8
.end method

.method public setIncomingVideoSourceToUser(ILjava/lang/String;)V
    .registers 7
    .parameter "requestID"
    .parameter "memberMucJid"

    .prologue
    .line 961
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 965
    :goto_8
    return-void

    .line 964
    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetIncomingVideoSourceToUser(ILjava/lang/String;)V

    goto :goto_8
.end method

.method public setPresenceConnectionStatus(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;)V
    .registers 6
    .parameter "status"

    .prologue
    .line 430
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 434
    :goto_8
    return-void

    .line 433
    :cond_9
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$PresenceConnectionStatus;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSetPresenceConnectionStatus(I)V

    goto :goto_8
.end method

.method public signoutAndDisconnect()V
    .registers 5

    .prologue
    const-wide/16 v2, 0x0

    .line 405
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 413
    :cond_8
    :goto_8
    return-void

    .line 408
    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeSignoutAndDisconnect()V

    .line 409
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_8

    .line 410
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerDestroy(J)V

    .line 411
    iput-wide v2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    goto :goto_8
.end method

.method public startIncomingVideoForSpeakerIndex(IIII)I
    .registers 9
    .parameter "index"
    .parameter "width"
    .parameter "height"
    .parameter "framerate"

    .prologue
    .line 939
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a

    .line 940
    const/4 v0, 0x0

    .line 942
    :goto_9
    return v0

    :cond_a
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStartIncomingVideoForSpeakerIndex(IIII)I

    move-result v0

    goto :goto_9
.end method

.method public startIncomingVideoForUser(Ljava/lang/String;III)I
    .registers 9
    .parameter "memberMucJid"
    .parameter "width"
    .parameter "height"
    .parameter "framerate"

    .prologue
    .line 947
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a

    .line 948
    const/4 v0, 0x0

    .line 950
    :goto_9
    return v0

    :cond_a
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStartIncomingVideoForUser(Ljava/lang/String;III)I

    move-result v0

    goto :goto_9
.end method

.method public startOutgoingVideo(II)V
    .registers 7
    .parameter "width"
    .parameter "height"

    .prologue
    .line 984
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 988
    :goto_8
    return-void

    .line 987
    :cond_9
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStartOutgoingVideo(II)V

    goto :goto_8
.end method

.method public stopIncomingVideo(I)V
    .registers 6
    .parameter "requestID"

    .prologue
    .line 977
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 981
    :goto_8
    return-void

    .line 980
    :cond_9
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStopIncomingVideo(I)V

    goto :goto_8
.end method

.method public stopOutgoingVideo()V
    .registers 5

    .prologue
    .line 991
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 995
    :goto_8
    return-void

    .line 994
    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeStopOutgoingVideo()V

    goto :goto_8
.end method

.method public uploadCallgrokLog()V
    .registers 5

    .prologue
    .line 601
    iget-wide v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativePeerObject:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 605
    :goto_8
    return-void

    .line 604
    :cond_9
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->nativeUploadCallgrokLog()V

    goto :goto_8
.end method
