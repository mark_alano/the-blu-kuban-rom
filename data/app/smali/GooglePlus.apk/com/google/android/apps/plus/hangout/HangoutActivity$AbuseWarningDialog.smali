.class final Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;
.super Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
.source "HangoutActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AbuseWarningDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutActivity;)V
    .registers 2
    .parameter

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;->this$0:Lcom/google/android/apps/plus/hangout/HangoutActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;-><init>()V

    .line 191
    return-void
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 14
    .parameter "savedInstanceState"

    .prologue
    const/4 v11, 0x0

    .line 196
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;->getDialogContext()Landroid/content/Context;

    move-result-object v1

    .line 197
    .local v1, context:Landroid/content/Context;
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 198
    .local v3, inflater:Landroid/view/LayoutInflater;
    const v8, 0x7f03003a

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 201
    .local v2, dialogView:Landroid/view/View;
    const v8, 0x7f0900ac

    invoke-virtual {v2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 202
    .local v4, linkView:Landroid/widget/TextView;
    const v8, 0x7f0802fe

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 203
    .local v6, text:Ljava/lang/String;
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 204
    .local v5, span:Landroid/text/Spannable;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    const-string v9, "plusone_promo_abuse"

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 205
    .local v7, url:Landroid/net/Uri;
    new-instance v8, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog$1;

    invoke-direct {v8, p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog$1;-><init>(Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;Landroid/net/Uri;)V

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    const/16 v10, 0x21

    invoke-interface {v5, v8, v11, v9, v10}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 214
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 216
    const/4 v8, 0x1

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setClickable(Z)V

    .line 218
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 219
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v11}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x1080027

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f0802fc

    invoke-virtual {v8, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f0802fb

    new-instance v10, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog$2;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog$2;-><init>(Lcom/google/android/apps/plus/hangout/HangoutActivity$AbuseWarningDialog;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 234
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    return-object v8
.end method
