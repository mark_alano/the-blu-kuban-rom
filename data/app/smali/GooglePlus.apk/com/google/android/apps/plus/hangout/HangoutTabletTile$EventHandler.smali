.class final Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "HangoutTabletTile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EventHandler"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 62
    const-class v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 2
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method

.method private notifyListeners()V
    .registers 4

    .prologue
    .line 86
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->listeners:Ljava/util/List;

    if-nez v2, :cond_7

    .line 93
    :cond_6
    return-void

    .line 90
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_f
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    .line 91
    .local v1, listener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;
    invoke-interface {v1}, Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;->onParticipantPresenceChanged()V

    goto :goto_f
.end method


# virtual methods
.method public final onAudioMuteStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .registers 5
    .parameter "member"
    .parameter "muted"

    .prologue
    .line 361
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onAudioMuteStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    .line 362
    if-eqz p1, :cond_b

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 363
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Ljava/lang/Boolean;)V

    .line 367
    :goto_14
    return-void

    .line 365
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_14
.end method

.method public final onCallgrokLogUploadCompleted$4f708078()V
    .registers 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->checkAndDismissCallgrokLogUploadProgressDialog()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getHangoutTileActivity()Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    .line 354
    return-void
.end method

.method public final onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V
    .registers 6
    .parameter "error"

    .prologue
    const/4 v3, 0x1

    .line 168
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V

    .line 170
    const-string v0, "HangoutTabletTile$EventHandler.onError(%s) %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    aput-object p0, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171
    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->AUTHENTICATION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    if-ne p1, v0, :cond_4a

    .line 172
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_31

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isSigningIn()Z

    move-result v0

    if-nez v0, :cond_31

    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 173
    :cond_31
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$500(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    .line 177
    :goto_36
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v0, :cond_49

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v0, v1, p1, v3}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordErrorExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    .line 180
    :cond_49
    return-void

    .line 175
    :cond_4a
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v0, p1, v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$600(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    goto :goto_36
.end method

.method public final onHangoutCreated(Lcom/google/android/apps/plus/service/Hangout$Info;)V
    .registers 6
    .parameter "hangoutInfo"

    .prologue
    .line 251
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onHangoutCreated(Lcom/google/android/apps/plus/service/Hangout$Info;)V

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iput-object p1, v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HangoutTabletTile.onHangoutCreated: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateHangoutViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->greenRoomParticipants:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHoaConsented:Z

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->enterHangout(Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/List;Z)V

    .line 259
    return-void
.end method

.method public final onHangoutWaitTimeout(Lcom/google/android/apps/plus/service/Hangout$Info;)V
    .registers 5
    .parameter "hangoutInfo"

    .prologue
    .line 263
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onHangoutWaitTimeout(Lcom/google/android/apps/plus/service/Hangout$Info;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$900(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08031d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    return-void
.end method

.method public final onMeetingEnterError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;)V
    .registers 6
    .parameter "error"

    .prologue
    .line 187
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingEnterError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;)V

    .line 190
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 247
    :goto_b
    return-void

    .line 193
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->READY_TO_LAUNCH_MEETING:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    .line 196
    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->HANGOUT_ON_AIR:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    if-ne p1, v2, :cond_23

    .line 197
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showHoaNotification(Landroid/widget/Button;)V

    goto :goto_b

    .line 201
    :cond_23
    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->OUTDATED_CLIENT:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    if-ne p1, v2, :cond_2d

    .line 202
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    goto :goto_b

    .line 208
    :cond_2d
    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$12;->$SwitchMap$com$google$android$apps$plus$hangout$GCommNativeWrapper$MeetingEnterError:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_78

    .line 212
    const/4 v0, 0x0

    .line 213
    .local v0, finishOnOk:Z
    const v1, 0x7f0802ec

    .line 245
    .local v1, messageId:I
    :goto_3c
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HangoutTabletTile.onMeetingEnterError: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 246
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_b

    .line 217
    .end local v0           #finishOnOk:Z
    .end local v1           #messageId:I
    :pswitch_54
    const/4 v0, 0x0

    .line 218
    .restart local v0       #finishOnOk:Z
    const v1, 0x7f0802ed

    .line 219
    .restart local v1       #messageId:I
    goto :goto_3c

    .line 221
    .end local v0           #finishOnOk:Z
    .end local v1           #messageId:I
    :pswitch_59
    const/4 v0, 0x0

    .line 222
    .restart local v0       #finishOnOk:Z
    const v1, 0x7f0802ee

    .line 223
    .restart local v1       #messageId:I
    goto :goto_3c

    .line 225
    .end local v0           #finishOnOk:Z
    .end local v1           #messageId:I
    :pswitch_5e
    const/4 v0, 0x0

    .line 226
    .restart local v0       #finishOnOk:Z
    const v1, 0x7f0802ef

    .line 227
    .restart local v1       #messageId:I
    goto :goto_3c

    .line 229
    .end local v0           #finishOnOk:Z
    .end local v1           #messageId:I
    :pswitch_63
    const/4 v0, 0x0

    .line 230
    .restart local v0       #finishOnOk:Z
    const v1, 0x7f0802f0

    .line 231
    .restart local v1       #messageId:I
    goto :goto_3c

    .line 233
    .end local v0           #finishOnOk:Z
    .end local v1           #messageId:I
    :pswitch_68
    const/4 v0, 0x0

    .line 234
    .restart local v0       #finishOnOk:Z
    const v1, 0x7f0802f1

    .line 235
    .restart local v1       #messageId:I
    goto :goto_3c

    .line 237
    .end local v0           #finishOnOk:Z
    .end local v1           #messageId:I
    :pswitch_6d
    const/4 v0, 0x0

    .line 238
    .restart local v0       #finishOnOk:Z
    const v1, 0x7f0802f2

    .line 239
    .restart local v1       #messageId:I
    goto :goto_3c

    .line 241
    .end local v0           #finishOnOk:Z
    .end local v1           #messageId:I
    :pswitch_72
    const/4 v0, 0x1

    .line 242
    .restart local v0       #finishOnOk:Z
    const v1, 0x7f0802f3

    .restart local v1       #messageId:I
    goto :goto_3c

    .line 208
    nop

    :pswitch_data_78
    .packed-switch 0x3
        :pswitch_54
        :pswitch_54
        :pswitch_59
        :pswitch_5e
        :pswitch_63
        :pswitch_68
        :pswitch_6d
        :pswitch_72
    .end packed-switch
.end method

.method public final onMeetingExited(Z)V
    .registers 8
    .parameter "clientInitiated"

    .prologue
    const/4 v5, 0x1

    .line 305
    const-string v1, "HangoutTabletTile$EventHandler.onMeetingExited: this=%s, tile=%s clientInitiated=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingExited(Z)V

    .line 315
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Z

    move-result v1

    if-eqz v1, :cond_2a

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v1

    if-nez v1, :cond_2b

    .line 345
    :cond_2a
    :goto_2a
    return-void

    .line 319
    :cond_2b
    if-eqz p1, :cond_8f

    .line 320
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->getDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "google.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_85

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->uploadCallgrokLog()V

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080324

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080325

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    .line 335
    .local v0, callgrokLogUploadProgressDialog:Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "log_upload"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 344
    .end local v0           #callgrokLogUploadProgressDialog:Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;
    :goto_77
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2, v5}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordNormalExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;Z)V

    goto :goto_2a

    .line 339
    :cond_85
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getHangoutTileActivity()Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    goto :goto_77

    .line 342
    :cond_8f
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    const v2, 0x7f080305

    invoke-virtual {v1, v2, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_77
.end method

.method public final onMeetingMediaStarted()V
    .registers 3

    .prologue
    .line 281
    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMediaStarted()V

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 298
    :goto_b
    return-void

    .line 289
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateHangoutViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    const/4 v1, 0x0

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Ljava/lang/Boolean;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showFilmStrip()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1100(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommService()Lcom/google/android/apps/plus/hangout/GCommService;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getHangoutTileActivity()Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->getHangoutNotificationIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommService;->showHangoutNotification(Landroid/content/Intent;)V

    .line 297
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getHangoutTileActivity()Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->onMeetingMediaStarted()V

    goto :goto_b
.end method

.method public final onMeetingMemberEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 3
    .parameter "meetingMember"

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateHangoutViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    .line 67
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->notifyListeners()V

    .line 68
    return-void
.end method

.method public final onMeetingMemberExited(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 3
    .parameter "meetingMember"

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberExited(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateHangoutViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->notifyListeners()V

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$100(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    .line 83
    return-void
.end method

.method public final onMeetingMemberPresenceConnectionStatusChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 3
    .parameter "meetingMember"

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberPresenceConnectionStatusChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateHangoutViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->notifyListeners()V

    .line 75
    return-void
.end method

.method public final onMucEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 3
    .parameter "selfMeetingMember"

    .prologue
    .line 271
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMucEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->sendInvites()V

    .line 274
    return-void
.end method

.method public final onRemoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 5
    .parameter "mutee"
    .parameter "muter"

    .prologue
    const/4 v1, 0x1

    .line 374
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Ljava/lang/Boolean;)V

    .line 379
    :goto_10
    return-void

    .line 377
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_10
.end method

.method public final onSignedIn(Ljava/lang/String;)V
    .registers 9
    .parameter "userJid"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 100
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedIn(Ljava/lang/String;)V

    .line 101
    const-string v3, "HangoutTabletTile$EventHandler.onSignedIn: this=%s, tile=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v2

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 104
    sget-boolean v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->$assertionsDisabled:Z

    if-nez v3, :cond_2f

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isSigningIn()Z

    move-result v3

    if-nez v3, :cond_2f

    new-instance v1, Ljava/lang/AssertionError;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 107
    :cond_2f
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Z

    move-result v3

    if-nez v3, :cond_38

    .line 127
    :goto_37
    return-void

    .line 111
    :cond_38
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->skipGreenRoom:Z

    if-eqz v3, :cond_93

    .line 112
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    sget-object v4, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->ENTERING_MEETING:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    .line 114
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v3, v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-nez v3, :cond_6b

    .line 115
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 116
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    const-string v3, "hangout_ring_invitees"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->createHangout(Z)V

    goto :goto_37

    .line 119
    .end local v0           #intent:Landroid/content/Intent;
    :cond_6b
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v4, v4, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v5, v5, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->MissedCall:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v5, v6, :cond_91

    :goto_85
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->greenRoomParticipants:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHoaConsented:Z

    invoke-virtual {v3, v4, v1, v2, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->enterHangout(Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/List;Z)V

    goto :goto_37

    :cond_91
    move v1, v2

    goto :goto_85

    .line 125
    :cond_93
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->READY_TO_LAUNCH_MEETING:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    goto :goto_37
.end method

.method public final onSignedOut()V
    .registers 4

    .prologue
    .line 134
    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedOut()V

    .line 136
    const-string v0, "HangoutTabletTile$EventHandler.onSignedOut"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 144
    :goto_10
    return-void

    .line 142
    :cond_11
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    const v1, 0x7f0802eb

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNIN_ERROR:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    goto :goto_10
.end method

.method public final onSigninTimeOutError()V
    .registers 4

    .prologue
    .line 151
    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSigninTimeOutError()V

    .line 153
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HangoutTabletTile$EventHandler.onSigninTimeOutError: this="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    #getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 161
    :goto_1d
    return-void

    .line 159
    :cond_1e
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    const v1, 0x7f0802eb

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNIN_ERROR:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    goto :goto_1d
.end method

.method public final onVideoMuteChanged(Z)V
    .registers 4
    .parameter "muted"

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1500(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Ljava/lang/Boolean;)V

    .line 400
    return-void
.end method

.method public final onVolumeChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;I)V
    .registers 5
    .parameter "member"
    .parameter "volume"

    .prologue
    const/4 v1, 0x0

    .line 387
    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 388
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    #calls: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Ljava/lang/Boolean;)V

    .line 392
    :goto_12
    return-void

    .line 390
    :cond_13
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    goto :goto_12
.end method
