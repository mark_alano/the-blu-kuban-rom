.class public Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
.super Lcom/google/android/apps/plus/hangout/HangoutTile;
.source "HangoutTabletTile.java"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$12;,
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;,
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;,
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;,
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bradyLayoutContainer:Landroid/widget/RelativeLayout;

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

.field private hangoutLaunchJoinPanel:Landroid/view/ViewGroup;

.field private instructionsView:Landroid/view/View;

.field private instructionsViewFadeOutRunnable:Ljava/lang/Runnable;

.field private isRegistered:Z

.field private isTileStarted:Z

.field private mActionBar:Landroid/app/ActionBar;

.field private mActionBarDismissalTimer:Landroid/os/CountDownTimer;

.field private mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

.field private mCenterStageContainer:Landroid/widget/RelativeLayout;

.field private mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

.field private mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

.field private mEnableStageIcons:Z

.field private mFilmStripAnimOut:Landroid/view/animation/Animation;

.field private mFilmStripContainer:Landroid/view/View;

.field private mFilmStripIsPaused:Z

.field private mFilmStripPauseTimer:Landroid/os/CountDownTimer;

.field private mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

.field private final mHandler:Landroid/os/Handler;

.field private mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

.field private mInset:Lcom/google/android/apps/plus/views/ScaledLayout;

.field private mInsetVideo:Landroid/widget/FrameLayout;

.field private mInviteesContainer:Landroid/view/View;

.field private mInviteesMessageView:Landroid/widget/TextView;

.field private mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

.field private mIsAudioEnabled:Z

.field private mIsAudioMuted:Z

.field private mIsHangoutLite:Z

.field private mIsVideoMuted:Z

.field private mJoinButton:Landroid/widget/Button;

.field private mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

.field private mMessageContainer:Landroid/view/View;

.field private mMessageView:Landroid/widget/TextView;

.field private mNeedToToastForInvite:Z

.field private mSlideInUp:Landroid/view/animation/Animation;

.field private mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

.field private mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

.field private progressBar:Landroid/widget/ProgressBar;

.field private progressText:Landroid/widget/TextView;

.field private stageLayoutContainer:Landroid/widget/RelativeLayout;

.field private state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

.field private stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 583
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 584
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .parameter "context"
    .parameter "attr"

    .prologue
    .line 587
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 588
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 8
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    const/4 v3, 0x1

    .line 591
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/HangoutTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 555
    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioEnabled:Z

    .line 559
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHandler:Landroid/os/Handler;

    .line 566
    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    .line 571
    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsHangoutLite:Z

    .line 578
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    .line 592
    const-string v0, "HangoutTabletTile(): this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    aput-object p1, v1, v3

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 594
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateHangoutViews()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_20

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    if-eqz v0, :cond_1f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getSelectedVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v1

    if-ne v1, p1, :cond_1f

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->setSelectedVideoSource(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateMainVideoStreaming()V

    :cond_1f
    :goto_1f
    return-void

    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_1f
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Ljava/lang/Boolean;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showFilmStrip()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->checkAndDismissCallgrokLogUploadProgressDialog()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 61
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    :goto_8
    return-void

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_29

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mEnableStageIcons:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->getCurrentVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v0

    if-ne v0, p1, :cond_8

    if-eqz p2, :cond_23

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->showAudioMutedStatus()V

    goto :goto_8

    :cond_23
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->hideAudioMutedStatus()V

    goto :goto_8

    :cond_29
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_8
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Ljava/lang/Boolean;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    :cond_f
    :goto_f
    return-void

    :cond_10
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showActionBar()V

    goto :goto_f
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showActionBar()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->fadeOutInstructionsView()V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->pauseFilmStrip()V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/os/CountDownTimer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/os/CountDownTimer;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripPauseTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->resumeFilmStrip()V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_2a

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mEnableStageIcons:Z

    if-eqz v0, :cond_2a

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getSelectedVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v0

    if-nez v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->hidePinnedStatus()V

    :goto_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->isAudioMuted(Lcom/google/android/apps/plus/hangout/MeetingMember;)Z

    move-result v0

    if-eqz v0, :cond_31

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->showAudioMutedStatus()V

    :cond_2a
    :goto_2a
    return-void

    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->showPinnedStatus()V

    goto :goto_1d

    :cond_31
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->hideAudioMutedStatus()V

    goto :goto_2a
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 6
    .parameter "x0"

    .prologue
    const/4 v4, 0x1

    .line 61
    const-string v0, "HangoutLaunchActivity#handleAuthenticationError: state=%s appState=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    aput-object v3, v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->$assertionsDisabled:Z

    if-nez v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isSigningIn()Z

    move-result v0

    if-nez v0, :cond_2c

    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_2c
    const v0, 0x7f0802ea

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNIN_ERROR:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 61
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/widget/Button;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .registers 6
    .parameter "x0"

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0802f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0801c5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x1080027

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$11;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$11;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    return-object v0
.end method

.method private addVideoToCenterStage(Lcom/google/android/apps/plus/hangout/HangoutVideoView;)V
    .registers 4
    .parameter "video"

    .prologue
    .line 1394
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1395
    .local v0, currentVideoContainer:Landroid/view/ViewGroup;
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    if-ne v0, v1, :cond_b

    .line 1409
    :goto_a
    return-void

    .line 1400
    :cond_b
    if-eqz v0, :cond_10

    .line 1401
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1404
    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1406
    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;)V

    .line 1407
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 1408
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->requestLayout()V

    goto :goto_a
.end method

.method private checkAndDismissCallgrokLogUploadProgressDialog()V
    .registers 4

    .prologue
    .line 1791
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "log_upload"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 1793
    .local v0, callgrokLogUploadProgressDialog:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_13

    .line 1794
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 1796
    :cond_13
    return-void
.end method

.method private fadeOutInstructionsView()V
    .registers 4

    .prologue
    .line 1350
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    if-eqz v1, :cond_28

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_28

    .line 1351
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040003

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1352
    .local v0, fadeOut:Landroid/view/animation/Animation;
    new-instance v1, Lcom/google/android/apps/plus/hangout/HideViewAnimationListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/hangout/HideViewAnimationListener;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1353
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1355
    .end local v0           #fadeOut:Landroid/view/animation/Animation;
    :cond_28
    return-void
.end method

.method private hideActionBar()V
    .registers 2

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_5

    .line 1073
    :cond_4
    :goto_4
    return-void

    .line 1061
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1064
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1069
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1071
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    goto :goto_4
.end method

.method private hideFilmStrip()V
    .registers 3

    .prologue
    .line 1828
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_f

    .line 1829
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripAnimOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1831
    :cond_f
    return-void
.end method

.method private pauseFilmStrip()V
    .registers 2

    .prologue
    .line 1835
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripPauseTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1836
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    if-nez v0, :cond_11

    .line 1837
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    .line 1838
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->onPause()V

    .line 1840
    :cond_11
    return-void
.end method

.method private resumeFilmStrip()V
    .registers 2

    .prologue
    .line 1844
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripPauseTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1845
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    if-eqz v0, :cond_11

    .line 1846
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    .line 1847
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->onResume()V

    .line 1849
    :cond_11
    return-void
.end method

.method private setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V
    .registers 6
    .parameter "newMode"

    .prologue
    const/4 v3, 0x0

    .line 1499
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    if-ne p1, v0, :cond_c

    .line 1524
    :cond_b
    :goto_b
    return-void

    .line 1503
    :cond_c
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$12;->$SwitchMap$com$google$android$apps$plus$hangout$HangoutTabletTile$StageViewMode:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_82

    .line 1520
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown stage layout mode: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    goto :goto_b

    .line 1505
    :pswitch_2a
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1506
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->addVideoToCenterStage(Lcom/google/android/apps/plus/hangout/HangoutVideoView;)V

    .line 1507
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInset:Lcom/google/android/apps/plus/views/ScaledLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ScaledLayout;->setVisibility(I)V

    .line 1508
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1523
    :goto_40
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    goto :goto_b

    .line 1512
    :pswitch_43
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 1513
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->addVideoToCenterStage(Lcom/google/android/apps/plus/hangout/HangoutVideoView;)V

    .line 1514
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    if-eq v0, v2, :cond_77

    if-eqz v0, :cond_5e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_5e
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 1515
    :cond_77
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInset:Lcom/google/android/apps/plus/views/ScaledLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/ScaledLayout;->setVisibility(I)V

    .line 1516
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_40

    .line 1503
    :pswitch_data_82
    .packed-switch 0x1
        :pswitch_2a
        :pswitch_43
    .end packed-switch
.end method

.method private setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    .registers 9
    .parameter "state"

    .prologue
    const/4 v1, 0x0

    const/16 v6, 0x8

    .line 1426
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting state to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 1427
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    .line 1428
    .local v0, previousState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    .line 1429
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v2

    if-nez v2, :cond_ba

    .line 1430
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v2, v6}, Lcom/google/android/apps/plus/hangout/ToastsView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInset:Lcom/google/android/apps/plus/views/ScaledLayout;

    invoke-virtual {v2, v6}, Lcom/google/android/apps/plus/views/ScaledLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->bradyLayoutContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1431
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v2, v3, :cond_54

    .line 1432
    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_LOCAL_ONLY:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V

    .line 1436
    :goto_39
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1437
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1438
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1439
    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$12;->$SwitchMap$com$google$android$apps$plus$hangout$HangoutTile$State:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_154

    .line 1495
    :cond_53
    :goto_53
    return-void

    .line 1433
    :cond_54
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_39

    .line 1442
    :pswitch_59
    iget-boolean v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->skipGreenRoom:Z

    if-nez v2, :cond_6b

    .line 1443
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsViewFadeOutRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1450
    :cond_6b
    :pswitch_6b
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 1451
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1452
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1453
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    const v2, 0x7f08030a

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_53

    .line 1456
    :pswitch_83
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1457
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-static {}, Lcom/google/android/apps/plus/hangout/StressMode;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_91

    const/4 v1, 0x1

    :cond_91
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1458
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1459
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_53

    .line 1462
    :pswitch_9f
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->fadeOutInstructionsView()V

    .line 1463
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 1464
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1465
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1466
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    const v2, 0x7f08030b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_53

    .line 1470
    :cond_ba
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1471
    sget-boolean v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->$assertionsDisabled:Z

    if-nez v2, :cond_e5

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v2

    if-nez v2, :cond_e5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_e5
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v2, v3, :cond_11f

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stageLayoutContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->bradyLayoutContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showFilmStrip()V

    :cond_102
    :goto_102
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateHangoutViews()V

    .line 1473
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isTileStarted:Z

    if-eqz v1, :cond_53

    .line 1474
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v1

    if-eqz v1, :cond_133

    .line 1476
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v1, v2, :cond_12b

    .line 1477
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->onResume()V

    .line 1478
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->resumeFilmStrip()V

    goto/16 :goto_53

    .line 1471
    :cond_11f
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v1, v2, :cond_102

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stageLayoutContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_102

    .line 1479
    :cond_12b
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v1, v2, :cond_53

    goto/16 :goto_53

    .line 1484
    :cond_133
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v1, v2, :cond_14d

    .line 1485
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->onResume()V

    .line 1486
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->onResume()V

    .line 1487
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->resumeFilmStrip()V

    .line 1488
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->onResume()V

    goto/16 :goto_53

    .line 1489
    :cond_14d
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto/16 :goto_53

    .line 1439
    nop

    :pswitch_data_154
    .packed-switch 0x1
        :pswitch_59
        :pswitch_6b
        :pswitch_6b
        :pswitch_83
        :pswitch_9f
    .end packed-switch
.end method

.method private showActionBar()V
    .registers 2

    .prologue
    .line 1046
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_5

    .line 1053
    :goto_4
    return-void

    .line 1051
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    .line 1052
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_4
.end method

.method private showError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V
    .registers 6
    .parameter "error"
    .parameter "finishOnOk"

    .prologue
    const v2, 0x7f0802e8

    .line 1641
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$12;->$SwitchMap$com$google$android$apps$plus$hangout$GCommNativeWrapper$Error:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_34

    .line 1661
    :goto_e
    return-void

    .line 1643
    :pswitch_f
    const v0, 0x7f0802ea

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_e

    .line 1646
    :pswitch_16
    invoke-virtual {p0, v2, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_e

    .line 1649
    :pswitch_1a
    invoke-virtual {p0, v2, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_e

    .line 1652
    :pswitch_1e
    const v0, 0x7f0802e9

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_e

    .line 1655
    :pswitch_25
    const v0, 0x7f080307

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_e

    .line 1658
    :pswitch_2c
    const v0, 0x7f080306

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_e

    .line 1641
    nop

    :pswitch_data_34
    .packed-switch 0x1
        :pswitch_f
        :pswitch_16
        :pswitch_1a
        :pswitch_1e
        :pswitch_25
        :pswitch_2c
    .end packed-switch
.end method

.method private showFilmStrip()V
    .registers 4

    .prologue
    .line 1818
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMemberCount()I

    move-result v0

    .line 1820
    .local v0, memberCount:I
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsHangoutLite:Z

    if-nez v1, :cond_26

    const/4 v1, 0x1

    if-le v0, v1, :cond_26

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_26

    .line 1822
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mSlideInUp:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1824
    :cond_26
    return-void
.end method

.method private updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V
    .registers 7
    .parameter "muted"

    .prologue
    .line 1704
    if-nez p1, :cond_13

    .line 1705
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->isAudioMute()Z

    move-result v1

    .line 1712
    .local v1, isAudioMuted:Z
    :goto_e
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    if-nez v3, :cond_18

    .line 1745
    :goto_12
    return-void

    .line 1707
    .end local v1           #isAudioMuted:Z
    :cond_13
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .restart local v1       #isAudioMuted:Z
    goto :goto_e

    .line 1719
    :cond_18
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v3

    if-eqz v3, :cond_34

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->hasAudioFocus()Z

    move-result v3

    if-eqz v3, :cond_5b

    :cond_34
    const/4 v0, 0x1

    .line 1722
    .local v0, isAudioEnabled:Z
    :goto_35
    const/4 v2, 0x0

    .line 1723
    .local v2, needInvalidate:Z
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    if-eq v3, v1, :cond_3d

    .line 1724
    const/4 v2, 0x1

    .line 1725
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    .line 1727
    :cond_3d
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioEnabled:Z

    if-eq v3, v0, :cond_44

    .line 1728
    const/4 v2, 0x1

    .line 1729
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioEnabled:Z

    .line 1731
    :cond_44
    if-eqz v2, :cond_4b

    .line 1732
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->invalidateOptionsMenu()V

    .line 1736
    :cond_4b
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v4, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v3, v4, :cond_63

    .line 1737
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    if-eqz v3, :cond_5d

    .line 1738
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->showAudioMutedStatus()V

    goto :goto_12

    .line 1719
    .end local v0           #isAudioEnabled:Z
    .end local v2           #needInvalidate:Z
    :cond_5b
    const/4 v0, 0x0

    goto :goto_35

    .line 1740
    .restart local v0       #isAudioEnabled:Z
    .restart local v2       #needInvalidate:Z
    :cond_5d
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->hideAudioMutedStatus()V

    goto :goto_12

    .line 1742
    :cond_63
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_12
.end method

.method private updateHangoutViews()V
    .registers 13

    .prologue
    const/16 v6, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1527
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v4

    .line 1532
    .local v4, nativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIsHangoutLite()Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsHangoutLite:Z

    .line 1534
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->invalidateOptionsMenu()V

    .line 1536
    if-eqz v4, :cond_2d

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-nez v7, :cond_33

    .line 1537
    :cond_2d
    sget-object v7, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_LOCAL_ONLY:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V

    .line 1602
    :cond_32
    :goto_32
    return-void

    .line 1541
    :cond_33
    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMemberCount()I

    move-result v2

    .line 1542
    .local v2, memberCount:I
    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v0

    .line 1543
    .local v0, hadParticipants:Z
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v7

    sget-object v10, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Creation:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v7, v10, :cond_7d

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRingInvitees()Z

    move-result v7

    if-eqz v7, :cond_7d

    move v5, v8

    .line 1546
    .local v5, showInvitees:Z
    :goto_4e
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v7, :cond_7f

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v7

    if-eqz v7, :cond_7f

    move v1, v8

    .line 1549
    .local v1, inMeeting:Z
    :goto_5b
    if-eqz v0, :cond_81

    .line 1550
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1563
    :cond_62
    :goto_62
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v7}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 1565
    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHasSomeConnectedParticipant()Z

    move-result v7

    if-eqz v7, :cond_c6

    .line 1566
    sget-object v7, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_LOCAL_AND_REMOTE:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V

    .line 1567
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1568
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_32

    .end local v1           #inMeeting:Z
    .end local v5           #showInvitees:Z
    :cond_7d
    move v5, v9

    .line 1543
    goto :goto_4e

    .restart local v5       #showInvitees:Z
    :cond_7f
    move v1, v9

    .line 1546
    goto :goto_5b

    .line 1551
    .restart local v1       #inMeeting:Z
    :cond_81
    if-eqz v5, :cond_62

    if-eqz v1, :cond_62

    .line 1552
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v10, "audience"

    invoke-virtual {v7, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_c4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v10

    if-nez v10, :cond_c4

    const-string v10, "audience"

    invoke-virtual {v7, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v11

    invoke-virtual {v10, v7, v11}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->setInvitees(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getAvatarCount()I

    move-result v7

    if-lez v7, :cond_c4

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {v7, v9}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->setVisibility(I)V

    move v5, v8

    .line 1553
    :goto_bc
    if-eqz v5, :cond_62

    .line 1554
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_62

    :cond_c4
    move v5, v9

    .line 1552
    goto :goto_bc

    .line 1571
    :cond_c6
    sget-object v7, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_LOCAL_ONLY:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V

    .line 1573
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v7

    sget-object v10, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ring:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v7, v10, :cond_f0

    .line 1574
    if-ne v2, v8, :cond_32

    if-nez v0, :cond_32

    .line 1575
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x7f08031e

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1576
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_32

    .line 1581
    :cond_f0
    if-eqz v0, :cond_11f

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v7

    sget-object v10, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ring:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-eq v7, v10, :cond_104

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRingInvitees()Z

    move-result v7

    if-eqz v7, :cond_11f

    .line 1585
    :cond_104
    if-ne v2, v8, :cond_32

    if-nez v0, :cond_32

    .line 1586
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v10, 0x7f08031d

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1587
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_32

    .line 1592
    :cond_11f
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getWaitingMessage(Z)Ljava/lang/String;

    move-result-object v3

    .line 1593
    .local v3, message:Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v7, :cond_130

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v7

    if-eqz v7, :cond_130

    move v6, v9

    .line 1594
    .local v6, visibility:I
    :cond_130
    if-eqz v1, :cond_32

    .line 1595
    if-eqz v5, :cond_13b

    .line 1596
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesMessageView:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_32

    .line 1598
    :cond_13b
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1599
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_32
.end method

.method private updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V
    .registers 4
    .parameter "muted"

    .prologue
    .line 1749
    if-nez p1, :cond_13

    .line 1750
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v0

    .line 1757
    .local v0, isOutgoingVideoMute:Z
    :goto_e
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    if-nez v1, :cond_18

    .line 1765
    :cond_12
    :goto_12
    return-void

    .line 1752
    .end local v0           #isOutgoingVideoMute:Z
    :cond_13
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .restart local v0       #isOutgoingVideoMute:Z
    goto :goto_e

    .line 1761
    :cond_18
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsVideoMuted:Z

    if-eq v1, v0, :cond_12

    .line 1762
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsVideoMuted:Z

    .line 1763
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->invalidateOptionsMenu()V

    goto :goto_12
.end method


# virtual methods
.method public final hideChild(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    .prologue
    .line 1857
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    if-ne p1, v0, :cond_7

    .line 1858
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideFilmStrip()V

    .line 1860
    :cond_7
    return-void
.end method

.method public final isTileStarted()Z
    .registers 2

    .prologue
    .line 1292
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isTileStarted:Z

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 1028
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onActivityResult(IILandroid/content/Intent;)V

    .line 1029
    if-nez p1, :cond_d

    const/4 v0, -0x1

    if-ne p2, v0, :cond_d

    if-eqz p3, :cond_d

    .line 1031
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mNeedToToastForInvite:Z

    .line 1033
    :cond_d
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter "parent"
    .parameter "child"

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    if-ne p1, v0, :cond_7

    .line 1880
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showFilmStrip()V

    .line 1882
    :cond_7
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .parameter "parent"
    .parameter "child"

    .prologue
    .line 1889
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    if-ne p1, v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_f

    .line 1891
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideFilmStrip()V

    .line 1893
    :cond_f
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .registers 15
    .parameter "savedInstanceState"

    .prologue
    const-wide/16 v11, 0x7530

    const-wide/16 v2, 0x1388

    const-wide/16 v9, 0x1f4

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 601
    const-string v0, "HangoutTabletTile.onCreate: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v1, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v4

    aput-object v4, v1, v8

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v5, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 603
    if-eqz p1, :cond_2e

    .line 604
    invoke-static {}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->values()[Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v0

    const-string v1, "HangoutTile_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    .line 607
    :cond_2e
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_STAGE_STATUS:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mEnableStageIcons:Z

    .line 609
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    .line 612
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 614
    .local v6, inflater:Landroid/view/LayoutInflater;
    const v0, 0x7f030042

    invoke-virtual {v6, v0, p0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 617
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    .line 618
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_INVALID:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    .line 621
    const v0, 0x7f0900cc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stageLayoutContainer:Landroid/widget/RelativeLayout;

    .line 622
    const v0, 0x7f0900d5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->bradyLayoutContainer:Landroid/widget/RelativeLayout;

    .line 623
    const v0, 0x7f0900c7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ScaledLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInset:Lcom/google/android/apps/plus/views/ScaledLayout;

    .line 624
    const v0, 0x7f0900cf

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    .line 625
    new-instance v0, Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/plus/hangout/LocalVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    .line 627
    const v0, 0x7f0900d1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    .line 629
    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V

    .line 630
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isAnyCameraAvailable()Z

    move-result v0

    if-eqz v0, :cond_b1

    .line 631
    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V

    .line 635
    :cond_b1
    const v0, 0x7f0900d6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    .line 636
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$1;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsViewFadeOutRunnable:Ljava/lang/Runnable;

    .line 642
    const v0, 0x7f0900d8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    .line 643
    const v0, 0x7f0900d9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$2;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 664
    const v0, 0x7f0900da

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    .line 665
    const v0, 0x7f0900db

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    .line 668
    const v0, 0x7f0900e1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/ToastsView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    .line 669
    const v0, 0x7f0900cd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    .line 670
    new-instance v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    .line 671
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    .line 672
    const v0, 0x7f0900d3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    .line 673
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    .line 674
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 676
    const v0, 0x7f0900e0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    .line 677
    const v0, 0x7f09005a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    .line 678
    const v0, 0x7f0900dc

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    .line 679
    const v0, 0x7f0900df

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesMessageView:Landroid/widget/TextView;

    .line 680
    const v0, 0x7f0900dd

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/HangoutInviteesView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    .line 682
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040009

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mSlideInUp:Landroid/view/animation/Animation;

    .line 683
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04000a

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripAnimOut:Landroid/view/animation/Animation;

    .line 686
    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$3;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$3;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;JJ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$4;

    move-object v1, p0

    move-wide v2, v11

    move-wide v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$4;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;JJ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripPauseTimer:Landroid/os/CountDownTimer;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_1a0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$5;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->addOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V

    :cond_1a0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripAnimOut:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$6;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mSlideInUp:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$7;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$8;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040003

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040002

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v1, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setVideoChangeListener(Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;)V

    .line 687
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 6
    .parameter "menu"
    .parameter "inflater"

    .prologue
    .line 907
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 910
    const v2, 0x7f10000c

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 911
    const v2, 0x7f10000a

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 912
    const v2, 0x7f100008

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 913
    const v2, 0x7f100012

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 914
    const v2, 0x7f10000d

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 917
    const v2, 0x7f0902b2

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 918
    .local v1, videoItem:Landroid/view/MenuItem;
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isAnyCameraAvailable()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 921
    const v2, 0x7f0902a4

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 922
    .local v0, cameraItem:Landroid/view/MenuItem;
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v2

    if-eqz v2, :cond_47

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isRearFacingCameraAvailable()Z

    move-result v2

    if-eqz v2, :cond_47

    const/4 v2, 0x1

    :goto_43
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 924
    return-void

    .line 922
    :cond_47
    const/4 v2, 0x0

    goto :goto_43
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter "item"

    .prologue
    const/4 v3, 0x1

    .line 984
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 987
    .local v1, itemId:I
    const v2, 0x7f0902a8

    if-ne v1, v2, :cond_4d

    .line 988
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    .line 989
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "HangoutTabletTile onExit with state:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v2, :cond_3d

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v2

    if-eqz v2, :cond_3e

    const-string v2, "Setting userRequestedMeetingExit to true"

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeeting()V

    .line 1023
    :cond_3d
    :goto_3d
    return v3

    .line 989
    :cond_3e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    invoke-interface {v2}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    const-string v2, "Did not set userRequestedMeetingExit"

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_3d

    .line 994
    :cond_4d
    const v2, 0x7f0902a4

    if-ne v1, v2, :cond_5f

    .line 996
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v4, 0xc9

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    .line 997
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    goto :goto_3d

    .line 1002
    :cond_5f
    const v2, 0x7f09029e

    if-ne v1, v2, :cond_7c

    .line 1003
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    .line 1004
    .local v0, app:Lcom/google/android/apps/plus/hangout/GCommApp;
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isAudioMute()Z

    move-result v2

    if-nez v2, :cond_7a

    move v2, v3

    :goto_73
    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->setAudioMute(Z)V

    .line 1005
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    goto :goto_3d

    .line 1004
    :cond_7a
    const/4 v2, 0x0

    goto :goto_73

    .line 1010
    .end local v0           #app:Lcom/google/android/apps/plus/hangout/GCommApp;
    :cond_7c
    const v2, 0x7f0902b2

    if-ne v1, v2, :cond_8e

    .line 1011
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v4, 0xca

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    .line 1012
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    goto :goto_3d

    .line 1017
    :cond_8e
    const v2, 0x7f0902a9

    if-ne v1, v2, :cond_9a

    .line 1018
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    .line 1019
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->inviteMoreParticipants()V

    goto :goto_3d

    .line 1023
    :cond_9a
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    goto :goto_3d
.end method

.method public final onPause()V
    .registers 5

    .prologue
    .line 1252
    const-string v0, "HangoutTabletTile.onPause: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1255
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    .line 1256
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    .line 1257
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .registers 8
    .parameter "menu"

    .prologue
    .line 931
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 934
    const v5, 0x7f0902b2

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 935
    .local v4, videoItem:Landroid/view/MenuItem;
    invoke-interface {v4}, Landroid/view/MenuItem;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_2e

    .line 938
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsVideoMuted:Z

    .line 939
    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsVideoMuted:Z

    if-eqz v5, :cond_6e

    .line 941
    const v1, 0x7f02007c

    .line 942
    .local v1, iconId:I
    const v3, 0x7f0802d9

    .line 948
    .local v3, titleId:I
    :goto_28
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 949
    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 953
    .end local v1           #iconId:I
    .end local v3           #titleId:I
    :cond_2e
    const v5, 0x7f09029e

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 954
    .local v0, audioItem:Landroid/view/MenuItem;
    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_5e

    .line 957
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->isAudioMute()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    .line 959
    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    if-eqz v5, :cond_75

    .line 961
    const v1, 0x7f020076

    .line 962
    .restart local v1       #iconId:I
    const v3, 0x7f0802d6

    .line 968
    .restart local v3       #titleId:I
    :goto_53
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 969
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 971
    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioEnabled:Z

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 975
    .end local v1           #iconId:I
    .end local v3           #titleId:I
    :cond_5e
    const v5, 0x7f0902a9

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 976
    .local v2, inviteItem:Landroid/view/MenuItem;
    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsHangoutLite:Z

    if-nez v5, :cond_7c

    const/4 v5, 0x1

    :goto_6a
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 977
    return-void

    .line 945
    .end local v0           #audioItem:Landroid/view/MenuItem;
    .end local v2           #inviteItem:Landroid/view/MenuItem;
    :cond_6e
    const v1, 0x7f02007b

    .line 946
    .restart local v1       #iconId:I
    const v3, 0x7f0802d8

    .restart local v3       #titleId:I
    goto :goto_28

    .line 965
    .end local v1           #iconId:I
    .end local v3           #titleId:I
    .restart local v0       #audioItem:Landroid/view/MenuItem;
    :cond_75
    const v1, 0x7f020075

    .line 966
    .restart local v1       #iconId:I
    const v3, 0x7f0802d5

    .restart local v3       #titleId:I
    goto :goto_53

    .line 976
    .end local v1           #iconId:I
    .end local v3           #titleId:I
    .restart local v2       #inviteItem:Landroid/view/MenuItem;
    :cond_7c
    const/4 v5, 0x0

    goto :goto_6a
.end method

.method public final onResume()V
    .registers 5

    .prologue
    .line 1108
    const-string v0, "HangoutTabletTile.onResume: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1110
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 7
    .parameter "outState"

    .prologue
    .line 1204
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-nez v1, :cond_2a

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    .line 1205
    .local v0, stateToSave:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    :goto_6
    const-string v1, "HangoutTabletTile.onSaveInstanceState: this=%s context=%s eventHandler=%s stateToSave:%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1208
    const-string v1, "HangoutTile_state"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->ordinal()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1209
    return-void

    .line 1204
    .end local v0           #stateToSave:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    :cond_2a
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    goto :goto_6
.end method

.method public final onStart()V
    .registers 5

    .prologue
    .line 1080
    const-string v0, "HangoutTabletTile.onStart: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1083
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->startingHangoutActivity(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V

    .line 1084
    return-void
.end method

.method public final onStop()V
    .registers 5

    .prologue
    .line 1277
    const-string v0, "HangoutTabletTile.onStop: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1280
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->stoppingHangoutActivity()V

    .line 1281
    return-void
.end method

.method public final onTilePause()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 1213
    const-string v0, "HangoutTabletTile.onTilePause: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1215
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v0, v1, :cond_1e

    .line 1245
    :goto_1d
    return-void

    .line 1219
    :cond_1e
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v0, :cond_5e

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 1220
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_57

    .line 1221
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->onPause()V

    .line 1222
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->onPause()V

    .line 1223
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->pauseFilmStrip()V

    .line 1231
    :cond_3d
    :goto_3d
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_66

    .line 1232
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->onPause()V

    .line 1233
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->dismissParticipantMenuDialog()V

    .line 1234
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_INVALID:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    .line 1242
    :goto_51
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->checkAndDismissCallgrokLogUploadProgressDialog()V

    .line 1244
    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isTileStarted:Z

    goto :goto_1d

    .line 1224
    :cond_57
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_3d

    goto :goto_3d

    .line 1228
    :cond_5e
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsViewFadeOutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_3d

    .line 1235
    :cond_66
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_51
.end method

.method public final onTileResume()V
    .registers 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1114
    const-string v1, "HangoutTabletTile.onTileResume: this=%s context=%s eventHandler=%s hangoutInfo=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1118
    sget-boolean v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->$assertionsDisabled:Z

    if-nez v1, :cond_2b

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v1, :cond_2b

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1121
    :cond_2b
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showActionBar()V

    .line 1123
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, v7}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    .line 1124
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    sget-object v2, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v1, v2, :cond_4e

    .line 1125
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->getErrorMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(Ljava/lang/String;Z)V

    .line 1196
    :goto_4d
    return-void

    .line 1129
    :cond_4e
    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isTileStarted:Z

    .line 1131
    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->START:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    .line 1133
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v1, :cond_6a

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/Hangout$RoomType;->UNKNOWN:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    if-ne v1, v2, :cond_6a

    .line 1134
    const v1, 0x7f0802de

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_4d

    .line 1138
    :cond_6a
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mNeedToToastForInvite:Z

    if-eqz v1, :cond_78

    .line 1139
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    const v2, 0x7f080327

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    .line 1140
    iput-boolean v6, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mNeedToToastForInvite:Z

    .line 1143
    :cond_78
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->hasAudioFocus()Z

    move-result v1

    if-eqz v1, :cond_a5

    .line 1147
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1, v6}, Landroid/app/Activity;->setVolumeControlStream(I)V

    .line 1154
    :cond_8f
    :goto_8f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v1

    if-eqz v1, :cond_bc

    .line 1156
    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    goto :goto_4d

    .line 1148
    :cond_a5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-eqz v1, :cond_8f

    .line 1151
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    const v2, 0x7f08031f

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    goto :goto_8f

    .line 1158
    :cond_bc
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v1, :cond_11a

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v1

    if-eqz v1, :cond_11a

    .line 1159
    iput-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    .line 1162
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->exitReported(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v1

    if-eqz v1, :cond_e6

    .line 1163
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    invoke-interface {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    .line 1164
    const-string v1, "Stopping hangout tile. Exit from hangout already reported."

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_4d

    .line 1169
    :cond_e6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->exitedNormally(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v1

    if-eqz v1, :cond_103

    .line 1170
    const v1, 0x7f080305

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    .line 1181
    :goto_f8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordExitReported(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)V

    goto/16 :goto_4d

    .line 1172
    :cond_103
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->getError(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    move-result-object v0

    .line 1173
    .local v0, error:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;
    if-eqz v0, :cond_113

    .line 1175
    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    goto :goto_f8

    .line 1178
    :cond_113
    const v1, 0x7f080304

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_f8

    .line 1183
    .end local v0           #error:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;
    :cond_11a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v1

    if-eqz v1, :cond_130

    .line 1184
    const v1, 0x7f0802e7

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto/16 :goto_4d

    .line 1188
    :cond_130
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNED_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v1, v2, :cond_153

    .line 1189
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getUserJid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->onSignedIn(Ljava/lang/String;)V

    goto/16 :goto_4d

    .line 1192
    :cond_153
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    .line 1193
    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNING_IN:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    .line 1194
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_4d
.end method

.method public final onTileStart()V
    .registers 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1091
    const-string v0, "HangoutTabletTile.onTileStart: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1093
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v0, :cond_23

    .line 1094
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "setHangoutInfo must be called before switching to HangoutTabletTile"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1099
    :cond_23
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 1100
    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    .line 1101
    return-void
.end method

.method public final onTileStop()V
    .registers 6

    .prologue
    const/4 v4, 0x0

    .line 1265
    const-string v0, "HangoutTabletTile.onTileStop: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1267
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 1268
    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    .line 1269
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 1270
    return-void
.end method

.method public setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1771
    .local p1, participantList:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    .local p2, activeParticipantInAnyTile:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    return-void
.end method

.method public final showChild(Landroid/view/View;)V
    .registers 3
    .parameter "child"

    .prologue
    .line 1868
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    if-ne p1, v0, :cond_7

    .line 1869
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showFilmStrip()V

    .line 1871
    :cond_7
    return-void
.end method

.method public final transfer()V
    .registers 6

    .prologue
    const/4 v1, 0x0

    .line 1775
    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    const-string v1, "Transfer hangout"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    const-string v2, "TRANSFER"

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->inviteToMeeting(Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    const v1, 0x7f080329

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    .line 1776
    return-void
.end method

.method public final updateMainVideoStreaming()V
    .registers 3

    .prologue
    .line 1306
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_17

    .line 1307
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1308
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->updateVideoStreaming()V

    .line 1311
    :cond_17
    return-void
.end method
