.class public Lcom/google/android/apps/plus/hangout/NewHangoutActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "NewHangoutActivity.java"


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

.field private mHangoutButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/fragments/AudienceFragment;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Landroid/widget/Button;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 233
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 6
    .parameter "fragment"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 147
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/AudienceFragment;

    if-eqz v0, :cond_34

    .line 148
    check-cast p1, Lcom/google/android/apps/plus/fragments/AudienceFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setCirclesUsageType(I)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setIncludePhoneOnlyContacts(Z)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setIncludePlusPages(Z)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setPublicProfileSearchEnabled(Z)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setShowSuggestedPeople(Z)V

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setFilterNullGaiaIds(Z)V

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    new-instance v1, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;-><init>(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    .line 177
    :cond_34
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_ABORT_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 51
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onBackPressed()V

    .line 52
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const v0, 0x7f030065

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->setContentView(I)V

    .line 61
    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 68
    const v0, 0x7f090110

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$1;-><init>(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_3e

    .line 82
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->showTitlebar(Z)V

    .line 84
    const v0, 0x7f0800d2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 86
    :cond_3e
    return-void
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 4
    .parameter "dialogId"
    .parameter "bundle"

    .prologue
    .line 128
    const v0, 0x7f09003e

    if-ne p1, v0, :cond_a

    .line 129
    invoke-static {p0}, Lcom/google/android/apps/plus/util/ImageUtils;->createInsertCameraPhotoDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    .line 131
    :goto_9
    return-object v0

    :cond_a
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_9
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 184
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 185
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f10000e

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 186
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    .line 194
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_36

    .line 215
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_c
    return v1

    .line 196
    :sswitch_d
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_c

    .line 201
    :sswitch_13
    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 202
    invoke-static {p0}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    goto :goto_c

    .line 207
    :sswitch_1c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080411

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 209
    .local v0, helpUrlParam:Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->startExternalActivity(Landroid/content/Intent;)V

    goto :goto_c

    .line 194
    :sswitch_data_36
    .sparse-switch
        0x102002c -> :sswitch_d
        0x7f09028f -> :sswitch_13
        0x7f090290 -> :sswitch_1c
    .end sparse-switch
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->finish()V

    .line 113
    :cond_c
    return-void
.end method

.method protected onStart()V
    .registers 6

    .prologue
    const/4 v4, 0x1

    .line 93
    const-string v1, "NewHangoutActivity.onStart: this=%s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStart()V

    .line 96
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_1b

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 98
    .local v0, actionBar:Landroid/app/ActionBar;
    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 100
    .end local v0           #actionBar:Landroid/app/ActionBar;
    :cond_1b
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 101
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->startingHangoutActivity(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V

    .line 102
    return-void
.end method

.method protected onStop()V
    .registers 4

    .prologue
    .line 120
    const-string v0, "NewHangoutActivity.onStop: this=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStop()V

    .line 123
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->stoppingHangoutActivity()V

    .line 124
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 140
    return-void
.end method
