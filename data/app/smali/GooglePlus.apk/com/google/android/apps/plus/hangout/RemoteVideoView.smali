.class public abstract Lcom/google/android/apps/plus/hangout/RemoteVideoView;
.super Lcom/google/android/apps/plus/hangout/HangoutVideoView;
.source "RemoteVideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;,
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;,
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;,
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;,
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;
    }
.end annotation


# instance fields
.field private currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

.field protected incomingVideoContainerHeight:I

.field protected incomingVideoContainerWidth:I

.field protected incomingVideoFrameHeight:I

.field protected incomingVideoFrameWidth:I

.field private isRegistered:Z

.field protected mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field protected mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

.field private final mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

.field protected requestID:I

.field private showingUnknownAvatar:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v1, 0x0

    .line 293
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 277
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameWidth:I

    .line 278
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameHeight:I

    .line 283
    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->NONE:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    .line 284
    iput v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    .line 288
    new-instance v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/RemoteVideoView;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

    .line 295
    new-instance v0, Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/plus/hangout/VideoTextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setVideoSurface(Landroid/view/View;)V

    .line 299
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;)V

    .line 300
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Lcom/google/android/apps/plus/hangout/VideoTextureView;
    .registers 2
    .parameter "x0"

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showingUnknownAvatar:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/hangout/RemoteVideoView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showingUnknownAvatar:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;
    .registers 2
    .parameter "x0"

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    return-object v0
.end method


# virtual methods
.method public final getBitmap()Landroid/graphics/Bitmap;
    .registers 2

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final getCurrentVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;
    .registers 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public final isVideoShowing()Z
    .registers 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->isDecoding()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_d
    return v0

    :cond_e
    const/4 v0, 0x0

    goto :goto_d
.end method

.method public final onMeasure$3b4dfe4b(II)V
    .registers 10
    .parameter "contentWidth"
    .parameter "contentHeight"

    .prologue
    .line 425
    iget v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameHeight:I

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->layoutVideo(IIII)V

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 427
    .local v6, videoLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    iget v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerWidth:I

    iget v1, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-ne v0, v1, :cond_1b

    iget v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerHeight:I

    iget v1, v6, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-eq v0, v1, :cond_40

    .line 429
    :cond_1b
    iget v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerWidth:I

    .line 430
    iget v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iput v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerHeight:I

    .line 432
    iget v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    if-eqz v0, :cond_40

    .line 435
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    iget v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerHeight:I

    sget-object v4, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;->AUTO_ZOOM:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;

    const/16 v5, 0xf

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingVideoParameters(IIILcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;I)V

    .line 441
    :cond_40
    return-void
.end method

.method public final onPause()V
    .registers 5

    .prologue
    const/4 v3, 0x0

    .line 325
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    .line 326
    .local v0, appInstance:Lcom/google/android/apps/plus/hangout/GCommApp;
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->isRegistered:Z

    if-eqz v1, :cond_18

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 328
    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->isRegistered:Z

    .line 332
    :cond_18
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->onPause()V

    .line 333
    iget v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    if-eqz v1, :cond_2c

    .line 334
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopIncomingVideo(I)V

    .line 335
    iput v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    .line 337
    :cond_2c
    return-void
.end method

.method public final onResume()V
    .registers 5

    .prologue
    .line 312
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->isRegistered:Z

    if-nez v0, :cond_19

    .line 313
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    .line 314
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->isRegistered:Z

    .line 316
    :cond_19
    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    .line 320
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->startVideo()V

    .line 321
    return-void
.end method

.method public setAlpha(F)V
    .registers 3
    .parameter "alpha"

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    if-eqz v0, :cond_a

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->setAlpha(F)V

    .line 453
    :goto_9
    return-void

    .line 451
    :cond_a
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->setAlpha(F)V

    goto :goto_9
.end method

.method protected final setIncomingContent(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .registers 3
    .parameter "member"

    .prologue
    .line 409
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 410
    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    .line 417
    :goto_b
    return-void

    .line 411
    :cond_c
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isVideoPaused()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 412
    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    goto :goto_b

    .line 415
    :cond_18
    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    goto :goto_b
.end method

.method protected final setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V
    .registers 7
    .parameter "newContent"

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 358
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_8

    .line 406
    :goto_7
    return-void

    .line 362
    :cond_8
    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_52

    .line 363
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showVideoSurface()V

    .line 368
    :goto_f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getSnapshotView()Landroid/widget/ImageView;

    move-result-object v1

    .line 369
    .local v1, snapShotView:Landroid/widget/ImageView;
    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->AVATAR:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-eq p1, v2, :cond_1f

    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-eq p1, v2, :cond_1f

    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_5a

    .line 372
    :cond_1f
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 373
    .local v0, snapShot:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_56

    .line 374
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 375
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 383
    .end local v0           #snapShot:Landroid/graphics/Bitmap;
    :goto_2d
    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->AVATAR:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-eq p1, v2, :cond_39

    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-eq p1, v2, :cond_39

    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_5e

    .line 386
    :cond_39
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showAvatar()V

    .line 391
    :goto_3c
    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_62

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showBlocked()V

    .line 397
    :goto_43
    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_66

    .line 398
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showPaused()V

    .line 403
    :goto_4a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hideLogo()V

    .line 404
    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showingUnknownAvatar:Z

    .line 405
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    goto :goto_7

    .line 365
    .end local v1           #snapShotView:Landroid/widget/ImageView;
    :cond_52
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hideVideoSurface()V

    goto :goto_f

    .line 377
    .restart local v0       #snapShot:Landroid/graphics/Bitmap;
    .restart local v1       #snapShotView:Landroid/widget/ImageView;
    :cond_56
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2d

    .line 380
    .end local v0           #snapShot:Landroid/graphics/Bitmap;
    :cond_5a
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2d

    .line 388
    :cond_5e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hideAvatar()V

    goto :goto_3c

    .line 394
    :cond_62
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hideBlocked()V

    goto :goto_43

    .line 400
    :cond_66
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hidePaused()V

    goto :goto_4a
.end method

.method public setVideoChangeListener(Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 307
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

    .line 308
    return-void
.end method

.method protected abstract startVideo()V
.end method
