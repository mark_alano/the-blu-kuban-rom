.class final Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;
.super Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;
.source "ToastsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/ToastsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaBlockToast"
.end annotation


# instance fields
.field private final mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private final mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private final mIsRecording:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/ToastsView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/ToastsView;Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .registers 6
    .parameter
    .parameter "blockee"
    .parameter "blocker"
    .parameter "isRecording"

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;B)V

    .line 176
    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 177
    iput-object p3, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    .line 179
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->FORCE_HANGOUT_RECORD_ABUSE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 180
    const/4 p4, 0x1

    .line 182
    :cond_13
    iput-boolean p4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mIsRecording:Z

    .line 183
    return-void
.end method


# virtual methods
.method final populateView(Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .registers 11
    .parameter "imageView"
    .parameter "textView"

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 188
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/ToastsView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 190
    .local v3, res:Landroid/content/res/Resources;
    iget-boolean v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mIsRecording:Z

    if-eqz v4, :cond_1c

    .line 195
    const v4, 0x7f080316

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 196
    .local v2, message:Ljava/lang/String;
    const/16 v4, 0x8

    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 213
    :goto_18
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    return-void

    .line 197
    .end local v2           #message:Ljava/lang/String;
    :cond_1c
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v4, :cond_7e

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v4, :cond_7e

    .line 198
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/plus/hangout/Avatars;->renderAvatar(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/MeetingMember;Landroid/widget/ImageView;)V

    .line 200
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 201
    .local v0, blockeeName:Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/MeetingMember;->getName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, blockerName:Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlocker:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v4

    if-eqz v4, :cond_5b

    .line 203
    const v4, 0x7f080313

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2       #message:Ljava/lang/String;
    goto :goto_18

    .line 204
    .end local v2           #message:Ljava/lang/String;
    :cond_5b
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;->mBlockee:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v4

    if-eqz v4, :cond_6f

    .line 205
    const v4, 0x7f080314

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .restart local v2       #message:Ljava/lang/String;
    goto :goto_18

    .line 207
    .end local v2           #message:Ljava/lang/String;
    :cond_6f
    const v4, 0x7f080315

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    aput-object v0, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 209
    .restart local v2       #message:Ljava/lang/String;
    goto :goto_18

    .line 210
    .end local v0           #blockeeName:Ljava/lang/String;
    .end local v1           #blockerName:Ljava/lang/String;
    .end local v2           #message:Ljava/lang/String;
    :cond_7e
    const/4 v2, 0x0

    .restart local v2       #message:Ljava/lang/String;
    goto :goto_18
.end method
