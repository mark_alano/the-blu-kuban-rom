.class public Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;
.super Landroid/view/View;
.source "MultiWaveView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;
    }
.end annotation


# instance fields
.field private mActiveTarget:I

.field private mAnimatingTargets:Z

.field private mChevronAnimationInterpolator:Landroid/animation/TimeInterpolator;

.field private mChevronAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;",
            ">;"
        }
    .end annotation
.end field

.field private mChevronDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mDirectionDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDirectionDescriptionsResourceId:I

.field private mDragging:Z

.field private mFeedbackCount:I

.field private mGrabbedState:I

.field private mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

.field private mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

.field private mHitRadius:F

.field private mHorizontalOffset:F

.field private mNewTargetResources:I

.field private mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

.field private mOuterRadius:F

.field private mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

.field private mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private mResetListener:Landroid/animation/Animator$AnimatorListener;

.field private mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSnapMargin:F

.field private mTapRadius:F

.field private mTargetAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetDescriptionsResourceId:I

.field private mTargetDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetResourceId:I

.field private mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

.field private mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private mVerticalOffset:F

.field private mVibrationDuration:I

.field private mVibrator:Landroid/os/Vibrator;

.field private mWaveCenterX:F

.field private mWaveCenterY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 21
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 119
    invoke-direct/range {p0 .. p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    .line 75
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    .line 87
    const/4 v15, 0x3

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    .line 88
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    .line 90
    const/4 v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    .line 96
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    .line 97
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    .line 98
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    .line 121
    sget v15, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v16, 0xb

    move/from16 v0, v16

    if-lt v15, v0, :cond_7f

    .line 122
    sget-object v15, Lcom/google/android/apps/plus/hangout/multiwaveview/Ease$Quad;->easeOut:Landroid/animation/TimeInterpolator;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimationInterpolator:Landroid/animation/TimeInterpolator;

    .line 123
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    .line 124
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    .line 125
    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$1;-><init>(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    .line 132
    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$2;-><init>(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

    .line 140
    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$3;-><init>(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 148
    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$4;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$4;-><init>(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    .line 161
    :cond_7f
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 163
    sget-object v15, Lcom/google/android/apps/plus/R$styleable;->MultiWaveView:[I

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v15}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 164
    .local v2, a:Landroid/content/res/TypedArray;
    const/16 v15, 0xe

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHorizontalOffset:F

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHorizontalOffset:F

    .line 166
    const/16 v15, 0xd

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVerticalOffset:F

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVerticalOffset:F

    .line 167
    const/16 v15, 0x9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    .line 168
    const/16 v15, 0xb

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    .line 169
    const/16 v15, 0xa

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    .line 171
    const/16 v15, 0xc

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    .line 172
    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const/16 v16, 0x3

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 174
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    int-to-float v15, v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTapRadius:F

    .line 176
    const-string v15, "window"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/WindowManager;

    .line 178
    .local v14, windowManager:Landroid/view/WindowManager;
    new-instance v11, Landroid/util/DisplayMetrics;

    invoke-direct {v11}, Landroid/util/DisplayMetrics;-><init>()V

    .line 179
    .local v11, metrics:Landroid/util/DisplayMetrics;
    invoke-interface {v14}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v15

    invoke-virtual {v15, v11}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 180
    iget v15, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mScreenWidth:I

    .line 181
    iget v15, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mScreenHeight:I

    .line 182
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mScreenHeight:I

    move/from16 v16, v0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    move-result v15

    int-to-float v15, v15

    const v16, 0x3f666666

    mul-float v15, v15, v16

    const/high16 v16, 0x4000

    div-float v16, v15, v16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-lez v15, :cond_1d5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    :goto_170
    int-to-float v15, v15

    sub-float v15, v16, v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    .line 184
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    const/high16 v16, 0x4000

    mul-float v15, v15, v16

    float-to-int v7, v15

    .line 186
    .local v7, diameter:I
    const/16 v15, 0x8

    invoke-virtual {v2, v15}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    check-cast v15, Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    .line 188
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v15}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v15

    check-cast v15, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v15, v7, v7}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    .line 189
    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 192
    const/4 v15, 0x4

    new-array v6, v15, [I

    fill-array-data v6, :array_24a

    .line 197
    .local v6, chevrons:[I
    move-object v3, v6

    .local v3, arr$:[I
    array-length v10, v6

    .local v10, len$:I
    const/4 v9, 0x0

    .local v9, i$:I
    :goto_1b1
    if-ge v9, v10, :cond_1dc

    aget v4, v3, v9

    .line 198
    .local v4, chevron:I
    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 199
    .local v5, chevronDrawable:Landroid/graphics/drawable/Drawable;
    const/4 v8, 0x0

    .local v8, i:I
    :goto_1ba
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    if-ge v8, v15, :cond_1d9

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    if-eqz v5, :cond_1d7

    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-direct {v15, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    :goto_1cd
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    add-int/lit8 v8, v8, 0x1

    goto :goto_1ba

    .line 182
    .end local v3           #arr$:[I
    .end local v4           #chevron:I
    .end local v5           #chevronDrawable:Landroid/graphics/drawable/Drawable;
    .end local v6           #chevrons:[I
    .end local v7           #diameter:I
    .end local v8           #i:I
    .end local v9           #i$:I
    .end local v10           #len$:I
    :cond_1d5
    const/4 v15, 0x0

    goto :goto_170

    .line 200
    .restart local v3       #arr$:[I
    .restart local v4       #chevron:I
    .restart local v5       #chevronDrawable:Landroid/graphics/drawable/Drawable;
    .restart local v6       #chevrons:[I
    .restart local v7       #diameter:I
    .restart local v8       #i:I
    .restart local v9       #i$:I
    .restart local v10       #len$:I
    :cond_1d7
    const/4 v15, 0x0

    goto :goto_1cd

    .line 197
    :cond_1d9
    add-int/lit8 v9, v9, 0x1

    goto :goto_1b1

    .line 206
    .end local v4           #chevron:I
    .end local v5           #chevronDrawable:Landroid/graphics/drawable/Drawable;
    .end local v8           #i:I
    :cond_1dc
    new-instance v12, Landroid/util/TypedValue;

    invoke-direct {v12}, Landroid/util/TypedValue;-><init>()V

    .line 207
    .local v12, outValue:Landroid/util/TypedValue;
    const/4 v15, 0x0

    invoke-virtual {v2, v15, v12}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    move-result v15

    if-eqz v15, :cond_1ef

    .line 208
    iget v15, v12, Landroid/util/TypedValue;->resourceId:I

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->internalSetTargetResources(I)V

    .line 210
    :cond_1ef
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    if-eqz v15, :cond_1ff

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-nez v15, :cond_207

    .line 211
    :cond_1ff
    new-instance v15, Ljava/lang/IllegalStateException;

    const-string v16, "Must specify at least one target drawable"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 215
    :cond_207
    const/4 v15, 0x1

    invoke-virtual {v2, v15, v12}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    move-result v15

    if-eqz v15, :cond_21f

    .line 216
    iget v13, v12, Landroid/util/TypedValue;->resourceId:I

    .line 217
    .local v13, resourceId:I
    if-nez v13, :cond_21a

    .line 218
    new-instance v15, Ljava/lang/IllegalStateException;

    const-string v16, "Must specify target descriptions"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 220
    :cond_21a
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setTargetDescriptionsResourceId(I)V

    .line 224
    .end local v13           #resourceId:I
    :cond_21f
    const/4 v15, 0x2

    invoke-virtual {v2, v15, v12}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    move-result v15

    if-eqz v15, :cond_237

    .line 225
    iget v13, v12, Landroid/util/TypedValue;->resourceId:I

    .line 226
    .restart local v13       #resourceId:I
    if-nez v13, :cond_232

    .line 227
    new-instance v15, Ljava/lang/IllegalStateException;

    const-string v16, "Must specify direction descriptions"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 229
    :cond_232
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setDirectionDescriptionsResourceId(I)V

    .line 232
    .end local v13           #resourceId:I
    :cond_237
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 233
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    if-lez v15, :cond_247

    const/4 v15, 0x1

    :goto_241
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setVibrateEnabled(Z)V

    .line 234
    return-void

    .line 233
    :cond_247
    const/4 v15, 0x0

    goto :goto_241

    .line 192
    nop

    :array_24a
    .array-data 0x4
        0x4t 0x0t 0x0t 0x0t
        0x5t 0x0t 0x0t 0x0t
        0x6t 0x0t 0x0t 0x0t
        0x7t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)F
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)F
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;IFF)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mNewTargetResources:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mNewTargetResources:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->internalSetTargetResources(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideTargets(Z)V

    return-void
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    return v0
.end method

.method private announceText(Ljava/lang/String;)V
    .registers 3
    .parameter "text"

    .prologue
    .line 1001
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1002
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->sendAccessibilityEvent(I)V

    .line 1003
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1004
    return-void
.end method

.method private deactivateTargets()V
    .registers 4

    .prologue
    .line 395
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 396
    .local v1, target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    sget-object v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    goto :goto_6

    .line 398
    .end local v1           #target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    :cond_18
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    .line 399
    return-void
.end method

.method private getTargetDescription(I)Ljava/lang/String;
    .registers 4
    .parameter "index"

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1008
    :cond_c
    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptionsResourceId:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->loadDescriptions(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    .line 1009
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_2b

    .line 1011
    const-string v0, "MultiWaveView"

    const-string v1, "The number of target drawables must be equal to the number of target descriptions."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    const/4 v0, 0x0

    .line 1017
    :goto_2a
    return-object v0

    :cond_2b
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_2a
.end method

.method private handleMove(Landroid/view/MotionEvent;)V
    .registers 32
    .parameter "event"

    .prologue
    .line 753
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    move/from16 v28, v0

    if-nez v28, :cond_c

    .line 754
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->trySwitchToFirstTouchState(Landroid/view/MotionEvent;)Z

    .line 828
    :goto_b
    return-void

    .line 758
    :cond_c
    const/4 v4, -0x1

    .line 759
    .local v4, activeTarget:I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v10

    .line 760
    .local v10, historySize:I
    const/4 v13, 0x0

    .local v13, k:I
    :goto_12
    add-int/lit8 v28, v10, 0x1

    move/from16 v0, v28

    if-ge v13, v0, :cond_189

    .line 761
    if-ge v13, v10, :cond_d6

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v26

    .line 762
    .local v26, x:F
    :goto_20
    if-ge v13, v10, :cond_dc

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v27

    .line 763
    .local v27, y:F
    :goto_28
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    move/from16 v28, v0

    sub-float v24, v26, v28

    .line 764
    .local v24, tx:F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    move/from16 v28, v0

    sub-float v25, v27, v28

    .line 765
    .local v25, ty:F
    mul-float v28, v24, v24

    mul-float v29, v25, v25

    add-float v28, v28, v29

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v23, v0

    .line 766
    .local v23, touchRadius:F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    move/from16 v28, v0

    cmpl-float v28, v23, v28

    if-lez v28, :cond_e2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    move/from16 v28, v0

    div-float v18, v28, v23

    .line 767
    .local v18, scale:F
    :goto_5e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    move/from16 v28, v0

    mul-float v29, v24, v18

    add-float v14, v28, v29

    .line 768
    .local v14, limitX:F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    move/from16 v28, v0

    mul-float v29, v25, v18

    add-float v15, v28, v29

    .line 770
    .local v15, limitY:F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_e6

    const/16 v19, 0x1

    .line 771
    .local v19, singleTarget:Z
    :goto_86
    if-eqz v19, :cond_e9

    .line 773
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    move/from16 v29, v0

    sub-float v20, v28, v29

    .line 774
    .local v20, snapRadius:F
    cmpl-float v28, v23, v20

    if-lez v28, :cond_9f

    .line 775
    const/4 v4, 0x0

    .line 776
    move/from16 v26, v14

    .line 777
    move/from16 v27, v15

    .line 798
    .end local v20           #snapRadius:F
    :cond_9f
    :goto_9f
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v4, v0, :cond_16a

    .line 799
    const/16 v28, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    .line 800
    if-eqz v19, :cond_142

    move/from16 v16, v14

    .line 801
    .local v16, newX:F
    :goto_b2
    if-eqz v19, :cond_156

    move/from16 v17, v15

    .line 802
    .local v17, newY:F
    :goto_b6
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->moveHandleTo$483d6f3f(FF)V

    .line 803
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 804
    .local v6, currentTarget:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    sget-object v28, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->hasState$25e2147()Z

    .line 760
    .end local v6           #currentTarget:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    .end local v16           #newX:F
    .end local v17           #newY:F
    :goto_d2
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_12

    .line 761
    .end local v14           #limitX:F
    .end local v15           #limitY:F
    .end local v18           #scale:F
    .end local v19           #singleTarget:Z
    .end local v23           #touchRadius:F
    .end local v24           #tx:F
    .end local v25           #ty:F
    .end local v26           #x:F
    .end local v27           #y:F
    :cond_d6
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v26

    goto/16 :goto_20

    .line 762
    .restart local v26       #x:F
    :cond_dc
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v27

    goto/16 :goto_28

    .line 766
    .restart local v23       #touchRadius:F
    .restart local v24       #tx:F
    .restart local v25       #ty:F
    .restart local v27       #y:F
    :cond_e2
    const/high16 v18, 0x3f80

    goto/16 :goto_5e

    .line 770
    .restart local v14       #limitX:F
    .restart local v15       #limitY:F
    .restart local v18       #scale:F
    :cond_e6
    const/16 v19, 0x0

    goto :goto_86

    .line 782
    .restart local v19       #singleTarget:Z
    :cond_e9
    const v5, 0x7f7fffff

    .line 783
    .local v5, best:F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    move/from16 v29, v0

    mul-float v11, v28, v29

    .line 784
    .local v11, hitRadius2:F
    const/4 v12, 0x0

    .local v12, i:I
    :goto_fb
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v0, v28

    if-ge v12, v0, :cond_13c

    .line 786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 787
    .local v21, target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getX()F

    move-result v28

    sub-float v8, v14, v28

    .line 788
    .local v8, dx:F
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getY()F

    move-result v28

    sub-float v9, v15, v28

    .line 789
    .local v9, dy:F
    mul-float v28, v8, v8

    mul-float v29, v9, v9

    add-float v7, v28, v29

    .line 790
    .local v7, dist2:F
    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->isValid()Z

    move-result v28

    if-eqz v28, :cond_139

    cmpg-float v28, v7, v11

    if-gez v28, :cond_139

    cmpg-float v28, v7, v5

    if-gez v28, :cond_139

    .line 791
    move v4, v12

    .line 792
    move v5, v7

    .line 784
    :cond_139
    add-int/lit8 v12, v12, 0x1

    goto :goto_fb

    .line 795
    .end local v7           #dist2:F
    .end local v8           #dx:F
    .end local v9           #dy:F
    .end local v21           #target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    :cond_13c
    move/from16 v26, v14

    .line 796
    move/from16 v27, v15

    goto/16 :goto_9f

    .line 800
    .end local v5           #best:F
    .end local v11           #hitRadius2:F
    .end local v12           #i:I
    :cond_142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getX()F

    move-result v16

    goto/16 :goto_b2

    .line 801
    .restart local v16       #newX:F
    :cond_156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getY()F

    move-result v17

    goto/16 :goto_b6

    .line 809
    .end local v16           #newX:F
    :cond_16a
    const/16 v28, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    .line 810
    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->moveHandleTo$483d6f3f(FF)V

    .line 811
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    move-object/from16 v28, v0

    const/high16 v29, 0x3f80

    invoke-virtual/range {v28 .. v29}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto/16 :goto_d2

    .line 816
    .end local v14           #limitX:F
    .end local v15           #limitY:F
    .end local v18           #scale:F
    .end local v19           #singleTarget:Z
    .end local v23           #touchRadius:F
    .end local v24           #tx:F
    .end local v25           #ty:F
    .end local v26           #x:F
    .end local v27           #y:F
    :cond_189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->invalidateGlobalRegion(Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;)V

    .line 818
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-eq v0, v4, :cond_1d6

    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v4, v0, :cond_1d6

    .line 819
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->vibrate()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    move-object/from16 v28, v0

    if-eqz v28, :cond_1b7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    move-object/from16 v28, v0

    .line 820
    :cond_1b7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v28

    const-string v29, "accessibility"

    invoke-virtual/range {v28 .. v29}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/accessibility/AccessibilityManager;

    .line 822
    .local v3, accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v28

    if-eqz v28, :cond_1d6

    .line 823
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getTargetDescription(I)Ljava/lang/String;

    move-result-object v22

    .line 824
    .local v22, targetContentDescription:Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->announceText(Ljava/lang/String;)V

    .line 827
    .end local v3           #accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    .end local v22           #targetContentDescription:Ljava/lang/String;
    :cond_1d6
    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    goto/16 :goto_b
.end method

.method private hideChevrons()V
    .registers 4

    .prologue
    .line 936
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 937
    .local v0, chevron:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    if-eqz v0, :cond_6

    .line 938
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto :goto_6

    .line 941
    .end local v0           #chevron:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    :cond_19
    return-void
.end method

.method private hideTargets(Z)V
    .registers 16
    .parameter "animate"

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v3, 0x0

    const/4 v10, 0x0

    .line 501
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    if-eqz v4, :cond_14

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_14

    .line 502
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopTargetAnimation()V

    .line 506
    :cond_14
    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    .line 507
    if-eqz p1, :cond_a4

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_a4

    .line 508
    if-eqz p1, :cond_67

    const/16 v0, 0x4b0

    .line 509
    .local v0, duration:I
    :goto_22
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_28
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_69

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 510
    .local v2, target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    sget-object v4, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    .line 511
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    int-to-long v5, v0

    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    const-string v8, "alpha"

    aput-object v8, v7, v3

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v7, v11

    const-string v8, "delay"

    aput-object v8, v7, v12

    const/16 v8, 0xc8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v13

    const/4 v8, 0x4

    const-string v9, "onUpdate"

    aput-object v9, v7, v8

    const/4 v8, 0x5

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v9, v7, v8

    invoke-static {v2, v5, v6, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_28

    .end local v0           #duration:I
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    :cond_67
    move v0, v3

    .line 508
    goto :goto_22

    .line 516
    .restart local v0       #duration:I
    .restart local v1       #i$:Ljava/util/Iterator;
    :cond_69
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    int-to-long v6, v0

    const/16 v8, 0x8

    new-array v8, v8, [Ljava/lang/Object;

    const-string v9, "alpha"

    aput-object v9, v8, v3

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v8, v11

    const-string v3, "delay"

    aput-object v3, v8, v12

    const/16 v3, 0xc8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v13

    const/4 v3, 0x4

    const-string v9, "onUpdate"

    aput-object v9, v8, v3

    const/4 v3, 0x5

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v9, v8, v3

    const/4 v3, 0x6

    const-string v9, "onComplete"

    aput-object v9, v8, v3

    const/4 v3, 0x7

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    aput-object v9, v8, v3

    invoke-static {v5, v6, v7, v8}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 528
    .end local v0           #duration:I
    :goto_a3
    return-void

    .line 522
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_a4
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1       #i$:Ljava/util/Iterator;
    :goto_aa
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_bf

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 523
    .restart local v2       #target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    sget-object v3, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    .line 524
    invoke-virtual {v2, v10}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto :goto_aa

    .line 526
    .end local v2           #target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    :cond_bf
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v3, v10}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto :goto_a3
.end method

.method private hideUnselected(I)V
    .registers 5
    .parameter "active"

    .prologue
    const/4 v2, 0x0

    .line 492
    const/4 v0, 0x0

    .local v0, i:I
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1a

    .line 493
    if-eq v0, p1, :cond_17

    .line 494
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    .line 492
    :cond_17
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 497
    :cond_1a
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    .line 498
    return-void
.end method

.method private internalSetTargetResources(I)V
    .registers 12
    .parameter "resourceId"

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 574
    .local v6, res:Landroid/content/res/Resources;
    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 575
    .local v0, array:Landroid/content/res/TypedArray;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v1

    .line 576
    .local v1, count:I
    const/4 v5, 0x0

    .line 577
    .local v5, maxWidth:I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 578
    .local v7, targetDrawables:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;>;"
    const/4 v4, 0x0

    .local v4, i:I
    :goto_17
    if-ge v4, v1, :cond_34

    .line 579
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 580
    .local v3, drawable:Landroid/graphics/drawable/Drawable;
    new-instance v8, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-direct {v8, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 581
    if-eqz v3, :cond_31

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    if-le v8, v5, :cond_31

    .line 582
    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 578
    :cond_31
    add-int/lit8 v4, v4, 0x1

    goto :goto_17

    .line 585
    .end local v3           #drawable:Landroid/graphics/drawable/Drawable;
    :cond_34
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getHeight()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x3f666666

    mul-float/2addr v8, v9

    const/high16 v9, 0x4000

    div-float/2addr v8, v9

    iput v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    .line 586
    iget v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    float-to-int v8, v8

    mul-int/lit8 v2, v8, 0x2

    .line 587
    .local v2, diameter:I
    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    check-cast v8, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v8, v2, v2}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    .line 588
    new-instance v8, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 590
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 591
    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetResourceId:I

    .line 592
    iput-object v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    .line 593
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->updateTargetPositions()V

    .line 594
    return-void
.end method

.method private loadDescriptions(I)Ljava/util/ArrayList;
    .registers 8
    .parameter "resourceId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1035
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1036
    .local v0, array:Landroid/content/res/TypedArray;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v2

    .line 1037
    .local v2, count:I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1038
    .local v4, targetContentDescriptions:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, i:I
    :goto_16
    if-ge v3, v2, :cond_22

    .line 1039
    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1040
    .local v1, contentDescription:Ljava/lang/String;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1038
    add-int/lit8 v3, v3, 0x1

    goto :goto_16

    .line 1042
    .end local v1           #contentDescription:Ljava/lang/String;
    :cond_22
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1043
    return-object v4
.end method

.method private moveHandleTo$483d6f3f(FF)V
    .registers 4
    .parameter "x"
    .parameter "y"

    .prologue
    .line 735
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    .line 736
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    .line 737
    return-void
.end method

.method private static resolveMeasured(II)I
    .registers 5
    .parameter "measureSpec"
    .parameter "desired"

    .prologue
    .line 265
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 267
    .local v1, specSize:I
    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_14

    .line 276
    move v0, v1

    .line 278
    .local v0, result:I
    :goto_c
    return v0

    .line 269
    .end local v0           #result:I
    :sswitch_d
    move v0, p1

    .line 270
    .restart local v0       #result:I
    goto :goto_c

    .line 272
    .end local v0           #result:I
    :sswitch_f
    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 273
    .restart local v0       #result:I
    goto :goto_c

    .line 267
    :sswitch_data_14
    .sparse-switch
        -0x80000000 -> :sswitch_f
        0x0 -> :sswitch_d
    .end sparse-switch
.end method

.method private setGrabbedState(I)V
    .registers 3
    .parameter "newState"

    .prologue
    .line 858
    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mGrabbedState:I

    if-eq p1, v0, :cond_13

    .line 859
    if-eqz p1, :cond_9

    .line 860
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->vibrate()V

    .line 862
    :cond_9
    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mGrabbedState:I

    .line 863
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    if-eqz v0, :cond_13

    .line 864
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mGrabbedState:I

    .line 867
    :cond_13
    return-void
.end method

.method private stopChevronAnimation()V
    .registers 5

    .prologue
    .line 375
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_7

    .line 382
    :goto_6
    return-void

    .line 378
    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_d
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    .line 379
    .local v0, anim:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;
    iget-object v2, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->end()V

    goto :goto_d

    .line 381
    .end local v0           #anim:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;
    :cond_1f
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_6
.end method

.method private stopHandleAnimation()V
    .registers 3

    .prologue
    .line 385
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 392
    :cond_6
    :goto_6
    return-void

    .line 388
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    if-eqz v0, :cond_6

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 390
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    goto :goto_6
.end method

.method private stopTargetAnimation()V
    .registers 5

    .prologue
    .line 558
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_23

    .line 559
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    .line 560
    .local v0, anim:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;
    iget-object v2, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->end()V

    goto :goto_c

    .line 562
    .end local v0           #anim:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;
    :cond_1e
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 564
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_23
    return-void
.end method

.method private switchToState$48676aae(I)V
    .registers 12
    .parameter "state"

    .prologue
    const/4 v5, 0x0

    const/16 v9, 0xb

    const/high16 v6, 0x3f80

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 298
    packed-switch p1, :pswitch_data_234

    .line 327
    :cond_a
    :goto_a
    :pswitch_a
    return-void

    .line 300
    :pswitch_b
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->deactivateTargets()V

    .line 301
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    goto :goto_a

    .line 305
    :pswitch_16
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopHandleAnimation()V

    .line 306
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->deactivateTargets()V

    .line 307
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    if-eqz v1, :cond_2b

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2b

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopTargetAnimation()V

    :cond_2b
    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_33
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_48

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v5, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto :goto_33

    :cond_48
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    .line 308
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v4, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_ACTIVE:[I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    .line 309
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setGrabbedState(I)V

    .line 310
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v4, "accessibility"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 312
    .local v0, accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 313
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    :goto_75
    if-ge v4, v6, :cond_a

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getTargetDescription(I)Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    if-eqz v1, :cond_87

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_cd

    :cond_87
    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptionsResourceId:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->loadDescriptions(I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-eq v1, v8, :cond_cd

    const-string v1, "MultiWaveView"

    const-string v8, "The number of target drawables must be euqal to the number of direction descriptions."

    invoke-static {v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_a5
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_bc

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_bc

    new-array v8, v2, [Ljava/lang/Object;

    aput-object v7, v8, v3

    invoke-static {v1, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_bc
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_c9

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->announceText(Ljava/lang/String;)V

    :cond_c9
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_75

    :cond_cd
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_a5

    .line 324
    .end local v0           #accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    :pswitch_d6
    iget v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    const/4 v1, -0x1

    if-eq v7, v1, :cond_188

    move v1, v2

    :goto_dc
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideTargets(Z)V

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    if-eqz v1, :cond_18b

    move v4, v5

    :goto_e4
    invoke-virtual {v8, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    if-eqz v1, :cond_1a9

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v4, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_ACTIVE:[I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideUnselected(I)V

    const-string v1, "MultiWaveView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Finish with target hit = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->vibrate()V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    if-eqz v4, :cond_11b

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    invoke-interface {v4, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;->onTrigger$5359dc9a(I)V

    :cond_11b
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v9, :cond_18e

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const-wide/16 v4, 0x0

    const/16 v7, 0xe

    new-array v7, v7, [Ljava/lang/Object;

    const-string v8, "ease"

    aput-object v8, v7, v3

    sget-object v8, Lcom/google/android/apps/plus/hangout/multiwaveview/Ease$Quart;->easeOut:Landroid/animation/TimeInterpolator;

    aput-object v8, v7, v2

    const/4 v2, 0x2

    const-string v8, "delay"

    aput-object v8, v7, v2

    const/4 v2, 0x3

    const/16 v8, 0x4b0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x4

    const-string v8, "alpha"

    aput-object v8, v7, v2

    const/4 v2, 0x5

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v7, v2

    const/4 v2, 0x6

    const-string v6, "x"

    aput-object v6, v7, v2

    const/4 v2, 0x7

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v7, v2

    const/16 v2, 0x8

    const-string v6, "y"

    aput-object v6, v7, v2

    const/16 v2, 0x9

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v7, v2

    const/16 v2, 0xa

    const-string v6, "onUpdate"

    aput-object v6, v7, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v2, v7, v9

    const/16 v2, 0xc

    const-string v6, "onComplete"

    aput-object v6, v7, v2

    const/16 v2, 0xd

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    aput-object v6, v7, v2

    invoke-static {v1, v4, v5, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    :goto_183
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setGrabbedState(I)V

    goto/16 :goto_a

    :cond_188
    move v1, v3

    goto/16 :goto_dc

    :cond_18b
    move v4, v6

    goto/16 :goto_e4

    :cond_18e
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    goto :goto_183

    :cond_1a9
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v9, :cond_218

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const-wide/16 v7, 0x12c

    const/16 v1, 0xe

    new-array v5, v1, [Ljava/lang/Object;

    const-string v1, "ease"

    aput-object v1, v5, v3

    sget-object v1, Lcom/google/android/apps/plus/hangout/multiwaveview/Ease$Quart;->easeOut:Landroid/animation/TimeInterpolator;

    aput-object v1, v5, v2

    const/4 v1, 0x2

    const-string v2, "delay"

    aput-object v2, v5, v1

    const/4 v1, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x4

    const-string v2, "alpha"

    aput-object v2, v5, v1

    const/4 v1, 0x5

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x6

    const-string v2, "x"

    aput-object v2, v5, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v1

    const/16 v1, 0x8

    const-string v2, "y"

    aput-object v2, v5, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v1

    const/16 v1, 0xa

    const-string v2, "onUpdate"

    aput-object v2, v5, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v1, v5, v9

    const/16 v1, 0xc

    const-string v2, "onComplete"

    aput-object v2, v5, v1

    const/16 v2, 0xd

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    if-eqz v1, :cond_215

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

    :goto_20b
    aput-object v1, v5, v2

    invoke-static {v4, v7, v8, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    goto/16 :goto_183

    :cond_215
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    goto :goto_20b

    :cond_218
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    goto/16 :goto_183

    .line 298
    :pswitch_data_234
    .packed-switch 0x0
        :pswitch_b
        :pswitch_16
        :pswitch_a
        :pswitch_a
        :pswitch_d6
    .end packed-switch
.end method

.method private trySwitchToFirstTouchState(Landroid/view/MotionEvent;)Z
    .registers 10
    .parameter "event"

    .prologue
    const/4 v5, 0x1

    .line 870
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 871
    .local v2, x:F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 872
    .local v3, y:F
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    sub-float v0, v2, v4

    .line 873
    .local v0, dx:F
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    sub-float v1, v3, v4

    .line 874
    .local v1, dy:F
    mul-float v4, v0, v0

    mul-float v6, v1, v1

    add-float/2addr v6, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v7, "accessibility"

    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_44

    const v4, 0x3fa66666

    iget v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTapRadius:F

    mul-float/2addr v4, v7

    :goto_2e
    mul-float/2addr v4, v4

    cmpg-float v4, v6, v4

    if-gtz v4, :cond_47

    .line 875
    const-string v4, "MultiWaveView"

    const-string v6, "** Handle HIT"

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    .line 877
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->moveHandleTo$483d6f3f(FF)V

    .line 878
    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    move v4, v5

    .line 881
    :goto_43
    return v4

    .line 874
    :cond_44
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTapRadius:F

    goto :goto_2e

    .line 881
    :cond_47
    const/4 v4, 0x0

    goto :goto_43
.end method

.method private updateTargetPositions()V
    .registers 11

    .prologue
    .line 925
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_42

    .line 926
    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 927
    .local v3, targetIcon:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    const-wide v6, -0x3fe6de04abbbd2e8L

    int-to-double v8, v2

    mul-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    int-to-double v8, v8

    div-double v0, v6, v8

    .line 928
    .local v0, angle:D
    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    iget v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float/2addr v7, v8

    add-float v4, v6, v7

    .line 929
    .local v4, xPosition:F
    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    iget v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float/2addr v7, v8

    add-float v5, v6, v7

    .line 930
    .local v5, yPosition:F
    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    .line 931
    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    .line 925
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 933
    .end local v0           #angle:D
    .end local v3           #targetIcon:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    .end local v4           #xPosition:F
    .end local v5           #yPosition:F
    :cond_42
    return-void
.end method

.method private vibrate()V
    .registers 4

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_c

    .line 568
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    .line 570
    :cond_c
    return-void
.end method


# virtual methods
.method protected getSuggestedMinimumHeight()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 259
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1d

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    :cond_1d
    add-int/2addr v0, v1

    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1d

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    :cond_1d
    add-int/2addr v0, v1

    return v0
.end method

.method final invalidateGlobalRegion(Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;)V
    .registers 11
    .parameter "drawable"

    .prologue
    const/4 v6, 0x0

    .line 402
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v3

    .line 403
    .local v3, width:I
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getHeight()I

    move-result v1

    .line 404
    .local v1, height:I
    new-instance v0, Landroid/graphics/RectF;

    int-to-float v4, v3

    int-to-float v5, v1

    invoke-direct {v0, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 405
    .local v0, childBounds:Landroid/graphics/RectF;
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getX()F

    move-result v4

    div-int/lit8 v5, v3, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getY()F

    move-result v5

    div-int/lit8 v6, v1, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v0, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 406
    move-object v2, p0

    .line 407
    .local v2, view:Landroid/view/View;
    :goto_24
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_69

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    instance-of v4, v4, Landroid/view/View;

    if-eqz v4, :cond_69

    .line 408
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .end local v2           #view:Landroid/view/View;
    check-cast v2, Landroid/view/View;

    .line 409
    .restart local v2       #view:Landroid/view/View;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_45

    .line 410
    invoke-virtual {v2}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 412
    :cond_45
    iget v4, v0, Landroid/graphics/RectF;->left:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    iget v5, v0, Landroid/graphics/RectF;->top:F

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v5, v5

    iget v6, v0, Landroid/graphics/RectF;->right:F

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v7

    double-to-int v7, v7

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_24

    .line 417
    :cond_69
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 5
    .parameter "canvas"

    .prologue
    .line 945
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 946
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_b
    :goto_b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 947
    .local v1, target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    if-eqz v1, :cond_b

    .line 948
    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_b

    .line 951
    .end local v1           #target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    :cond_1d
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_23
    :goto_23
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_35

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 952
    .restart local v1       #target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    if-eqz v1, :cond_23

    .line 953
    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_23

    .line 956
    .end local v1           #target:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    :cond_35
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 957
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "event"

    .prologue
    .line 832
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "accessibility"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 834
    .local v0, accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 835
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 836
    .local v1, action:I
    packed-switch v1, :pswitch_data_34

    .line 847
    :goto_19
    :pswitch_19
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 848
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 850
    .end local v1           #action:I
    :cond_1f
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2

    .line 838
    .restart local v1       #action:I
    :pswitch_24
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_19

    .line 841
    :pswitch_29
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_19

    .line 844
    :pswitch_2e
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_19

    .line 836
    nop

    :pswitch_data_34
    .packed-switch 0x7
        :pswitch_29
        :pswitch_19
        :pswitch_24
        :pswitch_2e
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .registers 15
    .parameter "changed"
    .parameter "left"
    .parameter "top"
    .parameter "right"
    .parameter "bottom"

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 903
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 904
    sub-int v3, p4, p2

    .line 905
    .local v3, width:I
    sub-int v0, p5, p3

    .line 906
    .local v0, height:I
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHorizontalOffset:F

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float v1, v4, v5

    .line 907
    .local v1, newWaveCenterX:F
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVerticalOffset:F

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float v2, v4, v5

    .line 908
    .local v2, newWaveCenterY:F
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    cmpl-float v4, v1, v4

    if-nez v4, :cond_37

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_aa

    .line 909
    :cond_37
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_95

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_95

    .line 910
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_59

    const/high16 v4, 0x3f00

    mul-float v5, v1, v1

    mul-float v6, v2, v2

    add-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    double-to-float v5, v5

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    :cond_59
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_71

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x4000

    div-float/2addr v4, v5

    iput v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    :cond_71
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_8c

    const/4 v4, 0x1

    const/high16 v5, 0x41a0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    :cond_8c
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideChevrons()V

    invoke-direct {p0, v8}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideTargets(Z)V

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->moveHandleTo$483d6f3f(FF)V

    .line 912
    :cond_95
    iput v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    .line 913
    iput v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    .line 915
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    .line 916
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    .line 918
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->updateTargetPositions()V

    .line 920
    :cond_aa
    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Outer Radius = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "HitRadius = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SnapMargin = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FeedbackCount = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "VibrationDuration = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TapRadius = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTapRadius:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WaveCenterX = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WaveCenterY = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "HorizontalOffset = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHorizontalOffset:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "VerticalOffset = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVerticalOffset:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    return-void
.end method

.method protected onMeasure(II)V
    .registers 12
    .parameter "widthMeasureSpec"
    .parameter "heightMeasureSpec"

    .prologue
    const/high16 v8, 0x4000

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getSuggestedMinimumWidth()I

    move-result v2

    .line 284
    .local v2, minimumWidth:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getSuggestedMinimumHeight()I

    move-result v1

    .line 285
    .local v1, minimumHeight:I
    invoke-static {p1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->resolveMeasured(II)I

    move-result v4

    .line 286
    .local v4, viewWidth:I
    invoke-static {p2, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->resolveMeasured(II)I

    move-result v3

    .line 287
    .local v3, viewHeight:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getPaddingLeft()I

    move-result v5

    sub-int v5, v4, v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getPaddingTop()I

    move-result v6

    sub-int v6, v3, v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    int-to-float v5, v5

    const v6, 0x3f666666

    mul-float/2addr v5, v6

    div-float/2addr v5, v8

    iput v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    .line 289
    iget v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    mul-float/2addr v5, v8

    float-to-int v0, v5

    .line 290
    .local v0, diameter:I
    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v5, v0, v0}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    .line 291
    new-instance v5, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getResources()Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    .line 292
    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    .line 293
    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    .line 294
    invoke-virtual {p0, v4, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setMeasuredDimension(II)V

    .line 295
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 6
    .parameter "event"

    .prologue
    .line 705
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 707
    .local v0, action:I
    const/4 v1, 0x0

    .line 708
    .local v1, handled:Z
    packed-switch v0, :pswitch_data_4a

    .line 730
    :goto_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->invalidate()V

    .line 731
    if-eqz v1, :cond_44

    const/4 v2, 0x1

    :goto_e
    return v2

    .line 710
    :pswitch_f
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->trySwitchToFirstTouchState(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_1e

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopTargetAnimation()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->ping()V

    .line 711
    :cond_1e
    const/4 v1, 0x1

    .line 712
    goto :goto_8

    .line 715
    :pswitch_20
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->handleMove(Landroid/view/MotionEvent;)V

    .line 716
    const/4 v1, 0x1

    .line 717
    goto :goto_8

    .line 720
    :pswitch_25
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->handleMove(Landroid/view/MotionEvent;)V

    .line 721
    iget-boolean v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    if-eqz v2, :cond_33

    const-string v2, "MultiWaveView"

    const-string v3, "** Handle RELEASE"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_33
    const/4 v2, 0x4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    .line 722
    const/4 v1, 0x1

    .line 723
    goto :goto_8

    .line 726
    :pswitch_3f
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->handleMove(Landroid/view/MotionEvent;)V

    .line 727
    const/4 v1, 0x1

    goto :goto_8

    .line 731
    :cond_44
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_e

    .line 708
    nop

    :pswitch_data_4a
    .packed-switch 0x0
        :pswitch_f
        :pswitch_25
        :pswitch_20
        :pswitch_3f
    .end packed-switch
.end method

.method public final ping()V
    .registers 15

    .prologue
    .line 678
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopChevronAnimation()V

    .line 679
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_169

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3ecccccd

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    const v2, 0x3f666666

    mul-float/2addr v1, v2

    const/4 v2, 0x4

    new-array v3, v2, [[F

    const/4 v2, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    sub-float/2addr v6, v0

    aput v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    aput v6, v4, v5

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    add-float/2addr v6, v0

    aput v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    aput v6, v4, v5

    aput-object v4, v3, v2

    const/4 v2, 0x2

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    aput v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    sub-float/2addr v6, v0

    aput v6, v4, v5

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    aput v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    add-float/2addr v0, v6

    aput v0, v4, v5

    aput-object v4, v3, v2

    const/4 v0, 0x4

    new-array v4, v0, [[F

    const/4 v0, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    sub-float/2addr v6, v1

    aput v6, v2, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    aput v6, v2, v5

    aput-object v2, v4, v0

    const/4 v0, 0x1

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    add-float/2addr v6, v1

    aput v6, v2, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    aput v6, v2, v5

    aput-object v2, v4, v0

    const/4 v0, 0x2

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    aput v6, v2, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    sub-float/2addr v6, v1

    aput v6, v2, v5

    aput-object v2, v4, v0

    const/4 v0, 0x3

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    aput v6, v2, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    add-float/2addr v1, v6

    aput v1, v2, v5

    aput-object v2, v4, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    move v2, v0

    :goto_af
    const/4 v0, 0x4

    if-ge v2, v0, :cond_169

    const/4 v0, 0x0

    move v1, v0

    :goto_b4
    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    if-ge v1, v0, :cond_164

    mul-int/lit16 v5, v1, 0xa0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    mul-int/2addr v6, v2

    add-int/2addr v6, v1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    if-eqz v0, :cond_15f

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    const-wide/16 v7, 0x352

    const/16 v9, 0x10

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "ease"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimationInterpolator:Landroid/animation/TimeInterpolator;

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const-string v11, "delay"

    aput-object v11, v9, v10

    const/4 v10, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v9, v10

    const/4 v5, 0x4

    const-string v10, "x"

    aput-object v10, v9, v5

    const/4 v5, 0x5

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    aget-object v12, v3, v2

    const/4 v13, 0x0

    aget v12, v12, v13

    aput v12, v10, v11

    const/4 v11, 0x1

    aget-object v12, v4, v2

    const/4 v13, 0x0

    aget v12, v12, v13

    aput v12, v10, v11

    aput-object v10, v9, v5

    const/4 v5, 0x6

    const-string v10, "y"

    aput-object v10, v9, v5

    const/4 v5, 0x7

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    aget-object v12, v3, v2

    const/4 v13, 0x1

    aget v12, v12, v13

    aput v12, v10, v11

    const/4 v11, 0x1

    aget-object v12, v4, v2

    const/4 v13, 0x1

    aget v12, v12, v13

    aput v12, v10, v11

    aput-object v10, v9, v5

    const/16 v5, 0x8

    const-string v10, "alpha"

    aput-object v10, v9, v5

    const/16 v5, 0x9

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_16a

    aput-object v10, v9, v5

    const/16 v5, 0xa

    const-string v10, "scaleX"

    aput-object v10, v9, v5

    const/16 v5, 0xb

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_172

    aput-object v10, v9, v5

    const/16 v5, 0xc

    const-string v10, "scaleY"

    aput-object v10, v9, v5

    const/16 v5, 0xd

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_17a

    aput-object v10, v9, v5

    const/16 v5, 0xe

    const-string v10, "onUpdate"

    aput-object v10, v9, v5

    const/16 v5, 0xf

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v10, v9, v5

    invoke-static {v0, v7, v8, v9}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_15f
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_b4

    :cond_164
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_af

    .line 680
    :cond_169
    return-void

    .line 679
    :array_16a
    .array-data 0x4
        0x0t 0x0t 0x80t 0x3ft
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    :array_172
    .array-data 0x4
        0x0t 0x0t 0x0t 0x3ft
        0x0t 0x0t 0x0t 0x40t
    .end array-data

    :array_17a
    .array-data 0x4
        0x0t 0x0t 0x0t 0x3ft
        0x0t 0x0t 0x0t 0x40t
    .end array-data
.end method

.method public final reset(Z)V
    .registers 4
    .parameter "animate"

    .prologue
    .line 689
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopChevronAnimation()V

    .line 690
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopHandleAnimation()V

    .line 691
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopTargetAnimation()V

    .line 692
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideChevrons()V

    .line 693
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideTargets(Z)V

    .line 694
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    .line 695
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    .line 696
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    .line 697
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const/high16 v1, 0x3f80

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    .line 698
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_35

    .line 699
    invoke-static {}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->reset()V

    .line 701
    :cond_35
    return-void
.end method

.method public setDirectionDescriptionsResourceId(I)V
    .registers 3
    .parameter "resourceId"

    .prologue
    .line 641
    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptionsResourceId:I

    .line 642
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    .line 643
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 645
    :cond_b
    return-void
.end method

.method public setOnTriggerListener(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;)V
    .registers 2
    .parameter "listener"

    .prologue
    .line 960
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    .line 961
    return-void
.end method

.method public setTargetDescriptionsResourceId(I)V
    .registers 3
    .parameter "resourceId"

    .prologue
    .line 620
    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptionsResourceId:I

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 624
    :cond_b
    return-void
.end method

.method public setTargetResources(I)V
    .registers 3
    .parameter "resourceId"

    .prologue
    .line 602
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    if-eqz v0, :cond_7

    .line 604
    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mNewTargetResources:I

    .line 608
    :goto_6
    return-void

    .line 606
    :cond_7
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->internalSetTargetResources(I)V

    goto :goto_6
.end method

.method public setVibrateEnabled(Z)V
    .registers 4
    .parameter "enabled"

    .prologue
    .line 662
    if-eqz p1, :cond_15

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_15

    .line 664
    :try_start_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_14} :catch_19

    .line 670
    :goto_14
    return-void

    .line 668
    :cond_15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    goto :goto_14

    .line 666
    :catch_19
    move-exception v0

    goto :goto_14
.end method
