.class final Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
.super Ljava/lang/Object;
.source "InstantUploadSyncManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;,
        Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;
    }
.end annotation


# static fields
.field private static final PROJECTION_ENABLE_ACCOUNT:[Ljava/lang/String;

.field private static sInstance:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;


# instance fields
.field private mBackgroundData:Z

.field private final mContext:Landroid/content/Context;

.field private volatile mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;

.field private mHasWifiConnectivity:Z

.field private final mInvalidAccounts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsPlugged:Z

.field private mIsRoaming:Z

.field private final mProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/iu/SyncTaskProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final mSyncHandler:Landroid/os/Handler;

.field private final mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 44
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "auto_upload_enabled"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "auto_upload_account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "instant_share_eventid"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "instant_share_endtime"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->PROJECTION_ENABLE_ACCOUNT:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 7
    .parameter "context"

    .prologue
    const/4 v4, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    .line 55
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    .line 57
    iput-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mHasWifiConnectivity:Z

    .line 58
    iput-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsRoaming:Z

    .line 59
    iput-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsPlugged:Z

    .line 60
    iput-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mBackgroundData:Z

    .line 75
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    .line 76
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    .line 78
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "picasa-sync-manager"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 80
    .local v1, thread:Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 81
    new-instance v2, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;-><init>(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    .line 83
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 84
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 86
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 88
    new-instance v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$1;-><init>(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V

    .line 97
    .local v0, accountListener:Landroid/accounts/OnAccountsUpdateListener;
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 98
    return-void
.end method

.method private acceptSyncTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z
    .registers 9
    .parameter "task"

    .prologue
    const/4 v6, 0x6

    const/4 v1, 0x0

    const/4 v5, 0x3

    .line 300
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isBackgroundSync()Z

    move-result v2

    if-eqz v2, :cond_6c

    .line 301
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    if-nez v2, :cond_35

    .line 302
    const-string v2, "InstantUploadSyncMgr"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 303
    const-string v2, "InstantUploadSyncMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because master auto sync is off"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_31
    invoke-virtual {p1, v6}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    .line 363
    :goto_34
    return v1

    .line 309
    :cond_35
    new-instance v2, Landroid/accounts/Account;

    iget-object v3, p1, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-direct {v2, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v2, v3}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 312
    .local v0, doAutoSync:Z
    if-nez v0, :cond_6c

    .line 313
    const-string v2, "InstantUploadSyncMgr"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_68

    .line 314
    const-string v2, "InstantUploadSyncMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because auto sync is off"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :cond_68
    invoke-virtual {p1, v6}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto :goto_34

    .line 321
    .end local v0           #doAutoSync:Z
    :cond_6c
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v2

    .line 322
    :try_start_6f
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    iget-object v4, p1, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_ab

    .line 323
    const-string v3, "InstantUploadSyncMgr"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_a6

    .line 324
    const-string v3, "InstantUploadSyncMgr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "reject "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for invalid account: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :cond_a6
    monitor-exit v2
    :try_end_a7
    .catchall {:try_start_6f .. :try_end_a7} :catchall_a8

    goto :goto_34

    .line 331
    :catchall_a8
    move-exception v1

    monitor-exit v2

    throw v1

    :cond_ab
    monitor-exit v2

    .line 333
    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mBackgroundData:Z

    if-nez v2, :cond_df

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isBackgroundSync()Z

    move-result v2

    if-eqz v2, :cond_df

    .line 334
    const-string v2, "InstantUploadSyncMgr"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_d8

    .line 335
    const-string v2, "InstantUploadSyncMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for disabled background data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :cond_d8
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto/16 :goto_34

    .line 340
    :cond_df
    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsPlugged:Z

    if-nez v2, :cond_111

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isSyncOnBattery()Z

    move-result v2

    if-nez v2, :cond_111

    .line 341
    const-string v2, "InstantUploadSyncMgr"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_10b

    .line 342
    const-string v2, "InstantUploadSyncMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on battery"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_10b
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto/16 :goto_34

    .line 347
    :cond_111
    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mHasWifiConnectivity:Z

    if-nez v2, :cond_174

    .line 348
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isSyncOnWifiOnly()Z

    move-result v2

    if-eqz v2, :cond_143

    .line 349
    const-string v2, "InstantUploadSyncMgr"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_13d

    .line 350
    const-string v2, "InstantUploadSyncMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for non-wifi connection"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :cond_13d
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto/16 :goto_34

    .line 354
    :cond_143
    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsRoaming:Z

    if-eqz v2, :cond_174

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isSyncOnRoaming()Z

    move-result v2

    if-nez v2, :cond_174

    .line 355
    const-string v2, "InstantUploadSyncMgr"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_16f

    .line 356
    const-string v2, "InstantUploadSyncMgr"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for roaming"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    :cond_16f
    invoke-virtual {p1, v5}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto/16 :goto_34

    .line 363
    :cond_174
    const/4 v1, 0x1

    goto/16 :goto_34
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V
    .registers 4
    .parameter "x0"

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    monitor-enter p0

    :try_start_7
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getManualPhotoUploadTaskProvider()Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getManualVideoUploadTaskProvider()Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstantSharePhotoUploadTaskProvider()Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getInstantShareVideoUploadTaskProvider()Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getNewPhotoUploadTaskProvider()Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getExistingPhotoUploadTaskProvider()Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getNewVideoUploadTaskProvider()Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->getExistingVideoUploadTaskProvider()Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_50
    .catchall {:try_start_7 .. :try_end_50} :catchall_51

    return-void

    :catchall_51
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V
    .registers 9
    .parameter "x0"

    .prologue
    const/4 v7, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    const-string v1, "InstantUploadSyncMgr"

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_33

    const-string v1, "InstantUploadSyncMgr"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "active network: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_33
    if-eqz v4, :cond_85

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_8c

    :pswitch_3c
    move v1, v2

    :goto_3d
    if-eqz v1, :cond_85

    move v1, v2

    :goto_40
    iget-boolean v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mHasWifiConnectivity:Z

    if-eq v1, v5, :cond_89

    iput-boolean v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mHasWifiConnectivity:Z

    move v1, v2

    :goto_47
    if-eqz v4, :cond_50

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v4

    if-eqz v4, :cond_50

    move v3, v2

    :cond_50
    iget-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsRoaming:Z

    if-eq v3, v4, :cond_57

    iput-boolean v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsRoaming:Z

    move v1, v2

    :cond_57
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    const-string v3, "InstantUploadSyncMgr"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_77

    const-string v3, "InstantUploadSyncMgr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "background data: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_77
    iget-boolean v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mBackgroundData:Z

    if-eq v3, v0, :cond_87

    iput-boolean v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mBackgroundData:Z

    :goto_7d
    if-eqz v2, :cond_82

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasksInternal()V

    :cond_82
    return-void

    :pswitch_83
    move v1, v3

    goto :goto_3d

    :cond_85
    move v1, v3

    goto :goto_40

    :cond_87
    move v2, v1

    goto :goto_7d

    :cond_89
    move v1, v3

    goto :goto_47

    nop

    :pswitch_data_8c
    .packed-switch 0x0
        :pswitch_83
        :pswitch_3c
        :pswitch_83
        :pswitch_83
        :pswitch_83
        :pswitch_83
        :pswitch_83
    .end packed-switch
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Ljava/lang/Boolean;)V
    .registers 7
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v4, 0x5

    const/4 v0, 0x1

    .line 35
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    if-nez p1, :cond_39

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_29

    const-string v0, "InstantUploadSyncMgr"

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_28

    const-string v0, "InstantUploadSyncMgr"

    const-string v1, "there is no battery info yet"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .end local p1
    :cond_28
    :goto_28
    return-void

    .restart local p1
    :cond_29
    const-string v2, "plugged"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v0, :cond_35

    const/4 v2, 0x2

    if-ne v1, v2, :cond_68

    :cond_35
    :goto_35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    .end local p1
    :cond_39
    const-string v0, "InstantUploadSyncMgr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_56

    const-string v0, "InstantUploadSyncMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "battery info: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_56
    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsPlugged:Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_28

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsPlugged:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasksInternal()V

    goto :goto_28

    .restart local p1
    :cond_68
    const/4 v0, 0x0

    goto :goto_35
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V
    .registers 1
    .parameter "x0"

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasksInternal()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
    .registers 2
    .parameter "x0"

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V
    .registers 11
    .parameter "x0"

    .prologue
    const/4 v8, 0x6

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->settingsUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->PROJECTION_ENABLE_ACCOUNT:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_26

    const-string v0, "InstantUploadSyncMgr"

    invoke-static {v0, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_25

    const-string v0, "InstantUploadSyncMgr"

    const-string v1, "failed to query system settings"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_25
    :goto_25
    return-void

    :cond_26
    :try_start_26
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_40

    const-string v0, "InstantUploadSyncMgr"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3c

    const-string v0, "InstantUploadSyncMgr"

    const-string v1, "failed to query system settings"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3c
    .catchall {:try_start_26 .. :try_end_3c} :catchall_ac

    :cond_3c
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_25

    :cond_40
    const/4 v0, 0x0

    :try_start_41
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_a8

    move v1, v6

    :goto_48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/4 v0, 0x2

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_aa

    const/4 v0, 0x3

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    cmp-long v0, v3, v8

    if-gtz v0, :cond_aa

    move v0, v6

    :goto_5d
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_66

    if-eqz v0, :cond_a3

    :cond_66
    if-eqz v3, :cond_a3

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, v3, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_a3

    const-string v0, "InstantUploadSyncMgr"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_96

    const-string v0, "InstantUploadSyncMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "remove sync account: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_96
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->settingsUri:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_a3
    .catchall {:try_start_41 .. :try_end_a3} :catchall_ac

    :cond_a3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_25

    :cond_a8
    move v1, v7

    goto :goto_48

    :cond_aa
    move v0, v7

    goto :goto_5d

    :catchall_ac
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/SyncTask;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v0

    return-object v0
.end method

.method public static createSession(Ljava/lang/String;Landroid/content/SyncResult;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;
    .registers 3
    .parameter "account"
    .parameter "result"

    .prologue
    .line 530
    new-instance v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;-><init>(Ljava/lang/String;Landroid/content/SyncResult;)V

    return-object v0
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
    .registers 3
    .parameter "context"

    .prologue
    .line 68
    const-class v1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->sInstance:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    if-nez v0, :cond_e

    .line 69
    new-instance v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->sInstance:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    .line 71
    :cond_e
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->sInstance:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 68
    :catchall_12
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isValidAccount(Landroid/accounts/Account;)Z
    .registers 9
    .parameter "account"

    .prologue
    .line 435
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 436
    .local v3, manager:Landroid/accounts/AccountManager;
    const-string v5, "com.google"

    invoke-virtual {v3, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .local v0, arr$:[Landroid/accounts/Account;
    array-length v2, v0

    .local v2, len$:I
    const/4 v1, 0x0

    .local v1, i$:I
    :goto_e
    if-ge v1, v2, :cond_21

    aget-object v4, v0, v1

    .line 437
    .local v4, x:Landroid/accounts/Account;
    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 438
    const/4 v5, 0x1

    .line 441
    .end local v4           #x:Landroid/accounts/Account;
    :goto_1d
    return v5

    .line 436
    .restart local v4       #x:Landroid/accounts/Account;
    :cond_1e
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 441
    .end local v4           #x:Landroid/accounts/Account;
    :cond_21
    const/4 v5, 0x0

    goto :goto_1d
.end method

.method private nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/SyncTask;
    .registers 10
    .parameter "account"

    .prologue
    .line 413
    const/4 v0, 0x0

    .local v0, i:I
    iget-object v7, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, n:I
    :goto_7
    if-ge v0, v2, :cond_44

    .line 414
    iget-object v7, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    .line 415
    .local v3, provider:Lcom/google/android/apps/plus/iu/SyncTaskProvider;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 416
    .local v6, tasks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/iu/SyncTask;>;"
    invoke-interface {v3, v6}, Lcom/google/android/apps/plus/iu/SyncTaskProvider;->collectTasks(Ljava/util/Collection;)V

    .line 417
    const/4 v4, 0x0

    .line 418
    .local v4, result:Lcom/google/android/apps/plus/iu/SyncTask;
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :cond_1e
    :goto_1e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/iu/SyncTask;

    .line 419
    .local v5, task:Lcom/google/android/apps/plus/iu/SyncTask;
    iput v0, v5, Lcom/google/android/apps/plus/iu/SyncTask;->mPriority:I

    .line 420
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->acceptSyncTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z

    move-result v7

    if-eqz v7, :cond_1e

    .line 421
    if-eqz v4, :cond_3c

    iget-object v7, v5, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1e

    .line 424
    :cond_3c
    move-object v4, v5

    goto :goto_1e

    .line 427
    .end local v5           #task:Lcom/google/android/apps/plus/iu/SyncTask;
    :cond_3e
    if-eqz v4, :cond_41

    .line 431
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #provider:Lcom/google/android/apps/plus/iu/SyncTaskProvider;
    .end local v4           #result:Lcom/google/android/apps/plus/iu/SyncTask;
    .end local v6           #tasks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/iu/SyncTask;>;"
    :goto_40
    return-object v4

    .line 413
    .restart local v1       #i$:Ljava/util/Iterator;
    .restart local v3       #provider:Lcom/google/android/apps/plus/iu/SyncTaskProvider;
    .restart local v4       #result:Lcom/google/android/apps/plus/iu/SyncTask;
    .restart local v6       #tasks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/iu/SyncTask;>;"
    :cond_41
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 431
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v3           #provider:Lcom/google/android/apps/plus/iu/SyncTaskProvider;
    .end local v4           #result:Lcom/google/android/apps/plus/iu/SyncTask;
    .end local v6           #tasks:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/iu/SyncTask;>;"
    :cond_44
    const/4 v4, 0x0

    goto :goto_40
.end method

.method private updateTasksInternal()V
    .registers 12

    .prologue
    const/4 v10, 0x3

    .line 455
    iget-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;

    .line 456
    .local v4, session:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;
    :goto_3
    if-nez v4, :cond_86

    .line 457
    iget-object v6, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 458
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v5

    .line 459
    .local v5, task:Lcom/google/android/apps/plus/iu/SyncTask;
    if-nez v5, :cond_12

    .line 502
    .end local v5           #task:Lcom/google/android/apps/plus/iu/SyncTask;
    :cond_11
    :goto_11
    return-void

    .line 463
    .restart local v5       #task:Lcom/google/android/apps/plus/iu/SyncTask;
    :cond_12
    new-instance v0, Landroid/accounts/Account;

    iget-object v6, v5, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    const-string v7, "com.google"

    invoke-direct {v0, v6, v7}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    .local v0, account:Landroid/accounts/Account;
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v6

    if-eqz v6, :cond_4e

    .line 467
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 468
    .local v2, extras:Landroid/os/Bundle;
    const-string v6, "ignore_settings"

    const/4 v7, 0x1

    invoke-virtual {v2, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 469
    const-string v6, "InstantUploadSyncMgr"

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_48

    .line 470
    const-string v6, "InstantUploadSyncMgr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "request sync for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    :cond_48
    const-string v6, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v0, v6, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_11

    .line 475
    .end local v2           #extras:Landroid/os/Bundle;
    :cond_4e
    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v7, "InstantUploadSyncMgr"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_77

    const-string v7, "InstantUploadSyncMgr"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "account: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has been removed ?!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_77
    iget-object v7, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v7

    :try_start_7a
    iget-object v8, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    invoke-virtual {v8, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    monitor-exit v7
    :try_end_80
    .catchall {:try_start_7a .. :try_end_80} :catchall_83

    .line 477
    iget-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;

    .line 478
    goto :goto_3

    .line 475
    :catchall_83
    move-exception v6

    monitor-exit v7

    throw v6

    .line 480
    .end local v0           #account:Landroid/accounts/Account;
    .end local v5           #task:Lcom/google/android/apps/plus/iu/SyncTask;
    :cond_86
    iget-object v1, v4, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    .line 481
    .local v1, current:Lcom/google/android/apps/plus/iu/SyncTask;
    if-eqz v1, :cond_11

    .line 486
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->acceptSyncTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z

    move-result v6

    if-nez v6, :cond_b7

    .line 487
    const-string v6, "InstantUploadSyncMgr"

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_b2

    .line 488
    const-string v6, "InstantUploadSyncMgr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "stop task: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " due to environment change"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    :cond_b2
    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/SyncTask;->cancelSync()V

    goto/16 :goto_11

    .line 495
    :cond_b7
    iget-object v6, v4, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->account:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v3

    .line 496
    .local v3, next:Lcom/google/android/apps/plus/iu/SyncTask;
    if-eqz v3, :cond_11

    iget v6, v3, Lcom/google/android/apps/plus/iu/SyncTask;->mPriority:I

    iget v7, v1, Lcom/google/android/apps/plus/iu/SyncTask;->mPriority:I

    if-ge v6, v7, :cond_11

    .line 497
    const-string v6, "InstantUploadSyncMgr"

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_eb

    .line 498
    const-string v6, "InstantUploadSyncMgr"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cancel task: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    :cond_eb
    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/SyncTask;->cancelSync()V

    goto/16 :goto_11
.end method


# virtual methods
.method public final declared-synchronized onAccountInitialized(Ljava/lang/String;)V
    .registers 5
    .parameter "account"

    .prologue
    .line 404
    monitor-enter p0

    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/UserEntry;

    move-result-object v1

    .line 405
    .local v1, user:Lcom/google/android/apps/plus/iu/UserEntry;
    if-nez v1, :cond_19

    .line 406
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 407
    .local v0, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Lcom/google/android/apps/plus/iu/UserEntry;

    .end local v1           #user:Lcom/google/android/apps/plus/iu/UserEntry;
    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/iu/UserEntry;-><init>(Ljava/lang/String;)V

    .line 408
    .restart local v1       #user:Lcom/google/android/apps/plus/iu/UserEntry;
    sget-object v2, Lcom/google/android/apps/plus/iu/UserEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v2, v0, v1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_1b

    .line 410
    .end local v0           #db:Landroid/database/sqlite/SQLiteDatabase;
    :cond_19
    monitor-exit p0

    return-void

    .line 404
    .end local v1           #user:Lcom/google/android/apps/plus/iu/UserEntry;
    :catchall_1b
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final onBatteryStateChanged(Z)V
    .registers 6
    .parameter "plugged"

    .prologue
    .line 210
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    if-eqz p1, :cond_11

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_7
    invoke-static {v2, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 212
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 213
    return-void

    .line 210
    .end local v0           #msg:Landroid/os/Message;
    :cond_11
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_7
.end method

.method public final onEnvironmentChanged()V
    .registers 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 207
    return-void
.end method

.method public final performSync(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;)V
    .registers 13
    .parameter "session"

    .prologue
    const-wide/16 v9, 0x0

    const/4 v8, 0x0

    .line 575
    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;

    .line 577
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v3

    .line 578
    :try_start_e
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    .line 579
    monitor-exit v3
    :try_end_14
    .catchall {:try_start_e .. :try_end_14} :catchall_2e

    .line 584
    monitor-enter p0

    .line 585
    :try_start_15
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_1b
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_31

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/iu/SyncTaskProvider;

    .line 586
    .local v1, provider:Lcom/google/android/apps/plus/iu/SyncTaskProvider;
    invoke-interface {v1}, Lcom/google/android/apps/plus/iu/SyncTaskProvider;->onSyncStart()V
    :try_end_2a
    .catchall {:try_start_15 .. :try_end_2a} :catchall_2b

    goto :goto_1b

    .line 588
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #provider:Lcom/google/android/apps/plus/iu/SyncTaskProvider;
    :catchall_2b
    move-exception v2

    monitor-exit p0

    throw v2

    .line 579
    :catchall_2e
    move-exception v2

    monitor-exit v3

    throw v2

    .line 588
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_31
    :try_start_31
    monitor-exit p0
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_2b

    .line 591
    :try_start_32
    iget-object v2, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    iget-object v3, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    :cond_36
    const-string v2, "InstantUploadSyncManager.getNextSyncTask"

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v2

    new-instance v4, Ljava/util/concurrent/FutureTask;

    new-instance v5, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;

    invoke-direct {v5, p0, p1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;-><init>(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;)V

    invoke-direct {v4, v5}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v5, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_4b
    .catchall {:try_start_32 .. :try_end_4b} :catchall_84

    :try_start_4b
    invoke-virtual {v4}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_4e
    .catchall {:try_start_4b .. :try_end_4e} :catchall_84
    .catch Ljava/lang/Throwable; {:try_start_4b .. :try_end_4e} :catch_72

    :cond_4e
    :goto_4e
    :try_start_4e
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    iget-object v2, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    if-eqz v2, :cond_61

    iget-object v2, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->account:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    iget-object v4, v4, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_88

    .line 593
    :cond_61
    :goto_61
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;

    .line 596
    monitor-enter p1
    :try_end_65
    .catchall {:try_start_4e .. :try_end_65} :catchall_84

    .line 597
    :try_start_65
    iget-boolean v2, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mSyncCancelled:Z

    if-nez v2, :cond_6e

    .line 598
    const-wide/16 v2, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasks(J)V

    .line 600
    :cond_6e
    monitor-exit p1
    :try_end_6f
    .catchall {:try_start_65 .. :try_end_6f} :catchall_121

    .line 602
    iput-object v8, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;

    .line 603
    return-void

    .line 591
    :catch_72
    move-exception v4

    :try_start_73
    const-string v5, "InstantUploadSyncMgr"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4e

    const-string v5, "InstantUploadSyncMgr"

    const-string v6, "fail to get next task"

    invoke-static {v5, v6, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_83
    .catchall {:try_start_73 .. :try_end_83} :catchall_84

    goto :goto_4e

    .line 602
    :catchall_84
    move-exception v2

    iput-object v8, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;

    throw v2

    .line 591
    :cond_88
    :try_start_88
    const-string v2, "InstantUploadSyncMgr"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a7

    const-string v2, "InstantUploadSyncMgr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "perform sync task: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a7
    iget-object v2, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    iget-object v4, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/iu/SyncTask;->performSync(Landroid/content/SyncResult;)V

    const-string v2, "InstantUploadSyncMgr"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_cd

    const-string v2, "InstantUploadSyncMgr"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sync complete: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_cd
    .catchall {:try_start_88 .. :try_end_cd} :catchall_11c
    .catch Ljava/io/IOException; {:try_start_88 .. :try_end_cd} :catch_eb
    .catch Ljava/lang/Throwable; {:try_start_88 .. :try_end_cd} :catch_107

    :cond_cd
    const/4 v2, 0x0

    :try_start_ce
    iput-object v2, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    :goto_d0
    iget-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v2, v4, v9

    if-lez v2, :cond_36

    const-string v2, "InstantUploadSyncMgr"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_e6

    const-string v2, "InstantUploadSyncMgr"

    const-string v3, "stop sync session due to io error"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e6
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->cancelSync()V
    :try_end_e9
    .catchall {:try_start_ce .. :try_end_e9} :catchall_84

    goto/16 :goto_61

    :catch_eb
    move-exception v2

    :try_start_ec
    const-string v4, "InstantUploadSyncMgr"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_fc

    const-string v4, "InstantUploadSyncMgr"

    const-string v5, "perform sync fail"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_fc
    iget-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_103
    .catchall {:try_start_ec .. :try_end_103} :catchall_11c

    const/4 v2, 0x0

    :try_start_104
    iput-object v2, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;
    :try_end_106
    .catchall {:try_start_104 .. :try_end_106} :catchall_84

    goto :goto_d0

    :catch_107
    move-exception v2

    :try_start_108
    const-string v4, "InstantUploadSyncMgr"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_118

    const-string v4, "InstantUploadSyncMgr"

    const-string v5, "perform sync fail"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_118
    .catchall {:try_start_108 .. :try_end_118} :catchall_11c

    :cond_118
    const/4 v2, 0x0

    :try_start_119
    iput-object v2, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    goto :goto_d0

    :catchall_11c
    move-exception v2

    const/4 v3, 0x0

    iput-object v3, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    throw v2

    .line 600
    :catchall_121
    move-exception v2

    monitor-exit p1

    throw v2
    :try_end_124
    .catchall {:try_start_119 .. :try_end_124} :catchall_84
.end method

.method public final requestAccountSync()V
    .registers 3

    .prologue
    .line 607
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 608
    return-void
.end method

.method public final updateTasks(J)V
    .registers 5
    .parameter "delay"

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 203
    return-void
.end method
