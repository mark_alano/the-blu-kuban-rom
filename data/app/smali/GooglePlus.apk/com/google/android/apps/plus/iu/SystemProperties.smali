.class public final Lcom/google/android/apps/plus/iu/SystemProperties;
.super Ljava/lang/Object;
.source "SystemProperties.java"


# static fields
.field private static final sGetLongMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    .prologue
    .line 20
    const/4 v1, 0x0

    .line 22
    .local v1, m:Ljava/lang/reflect/Method;
    :try_start_1
    const-string v2, "android.os.SystemProperties"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "getLong"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_19} :catch_1d

    move-result-object v1

    .line 29
    :cond_1a
    :goto_1a
    sput-object v1, Lcom/google/android/apps/plus/iu/SystemProperties;->sGetLongMethod:Ljava/lang/reflect/Method;

    .line 30
    return-void

    .line 24
    :catch_1d
    move-exception v0

    .line 25
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "SystemProperties"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 26
    const-string v2, "SystemProperties"

    const-string v3, "initialize error"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1a
.end method

.method public static getLong(Ljava/lang/String;J)J
    .registers 12
    .parameter "key"
    .parameter "defaultValue"

    .prologue
    const-wide/16 v2, 0x64

    .line 37
    :try_start_2
    sget-object v1, Lcom/google/android/apps/plus/iu/SystemProperties;->sGetLongMethod:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_34

    .line 38
    sget-object v1, Lcom/google/android/apps/plus/iu/SystemProperties;->sGetLongMethod:Ljava/lang/reflect/Method;

    const/4 v4, 0x0

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    const/4 v6, 0x1

    const-wide/16 v7, 0x64

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_21} :catch_23

    move-result-wide v1

    .line 45
    :goto_22
    return-wide v1

    .line 40
    :catch_23
    move-exception v0

    .line 41
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "SystemProperties"

    const/4 v4, 0x6

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_34

    .line 42
    const-string v1, "SystemProperties"

    const-string v4, "get error"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0           #e:Ljava/lang/Exception;
    :cond_34
    move-wide v1, v2

    .line 45
    goto :goto_22
.end method
