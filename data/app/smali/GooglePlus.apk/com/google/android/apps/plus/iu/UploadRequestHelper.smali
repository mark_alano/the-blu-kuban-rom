.class final Lcom/google/android/apps/plus/iu/UploadRequestHelper;
.super Ljava/lang/Object;
.source "UploadRequestHelper.java"


# static fields
.field private static final PICASA_BASE_UPLOAD_URL:Landroid/net/Uri;

.field private static final PROJECTION_DATA:[Ljava/lang/String;

.field private static final PROJECTION_DATE_TAKEN:[Ljava/lang/String;

.field private static final PROJECTION_SIZE:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    const-string v0, "https://picasaweb.google.com/data/upload/resumable/media/create-session/feed/api/user/default/albumid/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->PICASA_BASE_UPLOAD_URL:Landroid/net/Uri;

    .line 52
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->PROJECTION_DATA:[Ljava/lang/String;

    .line 53
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_size"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->PROJECTION_SIZE:[Ljava/lang/String;

    .line 54
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "datetaken"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->PROJECTION_DATE_TAKEN:[Ljava/lang/String;

    return-void
.end method

.method static createBackgroundUploadTask(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/ContentValues;
    .registers 6
    .parameter "account"
    .parameter "contentUri"
    .parameter "eventId"

    .prologue
    .line 58
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "account"

    invoke-virtual {v1, v2, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "content_uri"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "mime_type"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "priority"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "camera-sync"

    const-string v2, "album_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "event_id"

    invoke-virtual {v1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method static fillRequest(Landroid/content/Context;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)Z
    .registers 20
    .parameter "context"
    .parameter "task"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    .line 93
    .local v10, resolver:Landroid/content/ContentResolver;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    .line 95
    .local v3, contentUri:Landroid/net/Uri;
    const-string v13, "iu.UploadsManager"

    const/4 v14, 0x3

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_25

    .line 96
    const-string v13, "iu.UploadsManager"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "fill request for "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_25
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getEventId()Ljava/lang/String;

    move-result-object v8

    .line 100
    .local v8, eventId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, albumId:Ljava/lang/String;
    if-nez v1, :cond_36

    .line 102
    const-string v1, "camera-sync"

    .line 103
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setAlbumId(Ljava/lang/String;)V

    .line 106
    :cond_36
    const-string v13, "lh2"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setAuthTokenType(Ljava/lang/String;)V

    .line 108
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->hasPriority()Z

    move-result v13

    if-nez v13, :cond_49

    .line 109
    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setPriority(I)V

    .line 116
    :cond_49
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->hasFingerprint()Z

    move-result v13

    if-nez v13, :cond_c6

    .line 117
    const/4 v13, 0x1

    new-array v4, v13, [J

    .line 118
    .local v4, dataLength:[J
    invoke-virtual {v10, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v13

    invoke-static {v13, v4}, Lcom/android/gallery3d/common/Fingerprint;->fromInputStream(Ljava/io/InputStream;[J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v12

    .line 120
    .local v12, taskFingerPrint:Lcom/android/gallery3d/common/Fingerprint;
    const/4 v13, 0x0

    aget-wide v13, v4, v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesTotal(J)V

    .line 121
    const/4 v13, 0x0

    aget-wide v13, v4, v13

    const-wide/16 v15, 0x0

    cmp-long v13, v13, v15

    if-lez v13, :cond_b4

    .line 122
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setFingerprint(Lcom/android/gallery3d/common/Fingerprint;)V

    .line 136
    .end local v4           #dataLength:[J
    :goto_70
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMimeType()Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, contentType:Ljava/lang/String;
    if-nez v2, :cond_dc

    .line 139
    move-object/from16 v0, p1

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->setContentType(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)Ljava/lang/String;

    move-result-object v2

    .line 140
    if-eqz v2, :cond_da

    const-string v13, "image/"

    invoke-virtual {v2, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_8e

    const-string v13, "video/"

    invoke-virtual {v2, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_da

    :cond_8e
    const/4 v13, 0x1

    :goto_8f
    if-nez v13, :cond_dc

    .line 141
    new-instance v13, Ljava/lang/IllegalArgumentException;

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "invalid MIME type "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 124
    .end local v2           #contentType:Ljava/lang/String;
    .restart local v4       #dataLength:[J
    :cond_b4
    const-string v13, "iu.UploadsManager"

    const/4 v14, 0x5

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_c4

    .line 125
    const-string v13, "iu.UploadsManager"

    const-string v14, "Could not generate fingerprint; media length is \'0\'"

    invoke-static {v13, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :cond_c4
    const/4 v13, 0x0

    .line 158
    .end local v4           #dataLength:[J
    :goto_c5
    return v13

    .line 129
    .end local v12           #taskFingerPrint:Lcom/android/gallery3d/common/Fingerprint;
    :cond_c6
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v13

    const-wide/16 v15, 0x0

    cmp-long v13, v13, v15

    if-gtz v13, :cond_d5

    .line 131
    move-object/from16 v0, p1

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->setFileSize(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V

    .line 133
    :cond_d5
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v12

    .restart local v12       #taskFingerPrint:Lcom/android/gallery3d/common/Fingerprint;
    goto :goto_70

    .line 140
    .restart local v2       #contentType:Ljava/lang/String;
    :cond_da
    const/4 v13, 0x0

    goto :goto_8f

    .line 146
    :cond_dc
    sget-object v13, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->PROJECTION_DATE_TAKEN:[Ljava/lang/String;

    const-wide/16 v14, 0x0

    invoke-static {v10, v3, v13, v14, v15}, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->getOptionalLong(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;J)J

    move-result-wide v6

    .line 148
    .local v6, dateTakenMillis:J
    const-wide/16 v13, 0x0

    cmp-long v13, v6, v13

    if-lez v13, :cond_20c

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 149
    .local v5, dateTaken:Ljava/util/Date;
    :goto_ef
    sget-object v13, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->PROJECTION_DATA:[Ljava/lang/String;

    invoke-static {v10, v3, v13}, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->getOptionalString(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 150
    .local v9, filePath:Ljava/lang/String;
    if-nez v9, :cond_fb

    .line 151
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    .line 153
    :cond_fb
    invoke-virtual {v12}, Lcom/android/gallery3d/common/Fingerprint;->toStreamId()Ljava/lang/String;

    move-result-object v11

    .line 154
    .local v11, streamId:Ljava/lang/String;
    invoke-static/range {p0 .. p0}, Lcom/android/gallery3d/common/Utils;->getUserAgent(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v14

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    if-lez v16, :cond_119

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .end local v9           #filePath:Ljava/lang/String;
    :cond_119
    if-nez v5, :cond_120

    new-instance v5, Ljava/util/Date;

    .end local v5           #dateTaken:Ljava/util/Date;
    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    :cond_120
    new-instance v16, Ljava/lang/StringBuilder;

    const-string v17, "Authorization: GoogleLogin auth=%=_auth_token_=%\r\nUser-Agent: "

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v16, "\r\nGData-Version: 2.0"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v16, "\r\nSlug: "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v16, "\r\nX-Upload-Content-Type: "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v16, "\r\nX-Upload-Content-Length: "

    move-object/from16 v0, v16

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\r\nContent-Type: application/atom+xml; charset=UTF-8"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x0

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "\r\n<entry xmlns=\'http://www.w3.org/2005/Atom\' xmlns:gphoto=\'http://schemas.google.com/photos/2007\'><category scheme=\'http://schemas.google.com/g/2005#kind\' term=\'http://schemas.google.com/photos/2007#photo\'/>"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, "<title>"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v9}, Lcom/android/gallery3d/common/Utils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v16, "</title>"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_199

    const-string v16, "<summary>"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v14}, Lcom/android/gallery3d/common/Utils;->escapeXml(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "</summary>"

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_199
    const-string v14, "<gphoto:timestamp>"

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    invoke-virtual/range {v15 .. v17}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v14, "</gphoto:timestamp>"

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1bd

    const-string v14, "<gphoto:streamId>"

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "</gphoto:streamId>"

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1bd
    const-string v14, "<gphoto:streamId>mobile_uploaded</gphoto:streamId>"

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v14, "</entry>"

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setRequestTemplate(Ljava/lang/String;)V

    .line 156
    sget-object v13, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->PICASA_BASE_UPLOAD_URL:Landroid/net/Uri;

    invoke-virtual {v13}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v13

    invoke-virtual {v13, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v14

    const-string v15, "caid"

    invoke-virtual {v14, v15, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v14

    const-string v15, "xmlerrors"

    const-string v16, "1"

    invoke-virtual/range {v14 .. v16}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    if-eqz v8, :cond_1fc

    const-string v14, "evid"

    invoke-virtual {v13, v14, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1fc
    invoke-virtual {v13}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setUrl(Ljava/lang/String;)V

    .line 158
    const/4 v13, 0x1

    goto/16 :goto_c5

    .line 148
    .end local v11           #streamId:Ljava/lang/String;
    :cond_20c
    const/4 v5, 0x0

    goto/16 :goto_ef
.end method

.method private static getFileLengthFromContent(Landroid/content/ContentResolver;Landroid/net/Uri;)J
    .registers 10
    .parameter "resolver"
    .parameter "contentUri"

    .prologue
    .line 270
    const/4 v4, 0x0

    .line 272
    .local v4, in:Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v4

    .line 273
    const/16 v6, 0x400

    new-array v0, v6, [B

    .line 274
    .local v0, buffer:[B
    const-wide/16 v1, 0x0

    .line 275
    .local v1, count:J
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 276
    .local v5, n:I
    :goto_f
    if-ltz v5, :cond_18

    .line 277
    int-to-long v6, v5

    add-long/2addr v1, v6

    .line 278
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_24
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_16} :catch_1c

    move-result v5

    goto :goto_f

    .line 288
    :cond_18
    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    return-wide v1

    .line 281
    .end local v0           #buffer:[B
    .end local v1           #count:J
    .end local v5           #n:I
    :catch_1c
    move-exception v3

    .line 282
    .local v3, e:Ljava/lang/Exception;
    :try_start_1d
    instance-of v6, v3, Ljava/lang/RuntimeException;

    if-eqz v6, :cond_29

    .line 283
    check-cast v3, Ljava/lang/RuntimeException;

    .end local v3           #e:Ljava/lang/Exception;
    throw v3
    :try_end_24
    .catchall {:try_start_1d .. :try_end_24} :catchall_24

    .line 288
    :catchall_24
    move-exception v6

    invoke-static {v4}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    .line 285
    .restart local v3       #e:Ljava/lang/Exception;
    :cond_29
    :try_start_29
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6
    :try_end_2f
    .catchall {:try_start_29 .. :try_end_2f} :catchall_24
.end method

.method private static getFileLengthFromRawFdOrContent(Landroid/content/ContentResolver;Landroid/net/Uri;)J
    .registers 6
    .parameter "resolver"
    .parameter "contentUri"

    .prologue
    .line 258
    const/4 v0, 0x0

    .line 260
    .local v0, fd:Landroid/content/res/AssetFileDescriptor;
    :try_start_1
    const-string v1, "r"

    invoke-virtual {p0, p1, v1}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    .line 261
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getLength()J
    :try_end_a
    .catchall {:try_start_1 .. :try_end_a} :catchall_24
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_a} :catch_15

    move-result-wide v1

    .line 265
    if-eqz v0, :cond_14

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    :cond_14
    :goto_14
    return-wide v1

    .line 263
    :catch_15
    move-exception v1

    :try_start_16
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->getFileLengthFromContent(Landroid/content/ContentResolver;Landroid/net/Uri;)J
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_24

    move-result-wide v1

    .line 265
    if-eqz v0, :cond_14

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_14

    :catchall_24
    move-exception v1

    if-eqz v0, :cond_2e

    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->getParcelFileDescriptor()Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/os/ParcelFileDescriptor;)V

    :cond_2e
    throw v1
.end method

.method private static getOptionalLong(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;J)J
    .registers 14
    .parameter "resolver"
    .parameter "uri"
    .parameter "projection"
    .parameter "defaultValue"

    .prologue
    const-wide/16 v7, 0x0

    .line 244
    const/4 v6, 0x0

    .line 246
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    :try_start_9
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 247
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_17
    .catchall {:try_start_9 .. :try_end_17} :catchall_28
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_17} :catch_20

    move-result-wide v0

    .line 251
    :goto_18
    if-eqz v6, :cond_1d

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1d
    :goto_1d
    return-wide v0

    :cond_1e
    move-wide v0, v7

    .line 247
    goto :goto_18

    .line 249
    :catch_20
    move-exception v0

    if-eqz v6, :cond_26

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_26
    move-wide v0, v7

    goto :goto_1d

    :catchall_28
    move-exception v0

    if-eqz v6, :cond_2e

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2e
    throw v0
.end method

.method private static getOptionalString(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "resolver"
    .parameter "uri"
    .parameter "projection"

    .prologue
    const/4 v7, 0x0

    .line 231
    const/4 v6, 0x0

    .line 233
    .local v6, cursor:Landroid/database/Cursor;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    :try_start_8
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 234
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1d

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_16
    .catchall {:try_start_8 .. :try_end_16} :catchall_27
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_16} :catch_1f

    move-result-object v0

    .line 238
    :goto_17
    if-eqz v6, :cond_1c

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1c
    :goto_1c
    return-object v0

    :cond_1d
    move-object v0, v7

    .line 234
    goto :goto_17

    .line 236
    :catch_1f
    move-exception v0

    if-eqz v6, :cond_25

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_25
    move-object v0, v7

    goto :goto_1c

    :catchall_27
    move-exception v0

    if-eqz v6, :cond_2d

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2d
    throw v0
.end method

.method static setContentType(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)Ljava/lang/String;
    .registers 10
    .parameter "resolver"
    .parameter "task"

    .prologue
    const/4 v7, 0x2

    .line 171
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    .line 172
    .local v0, contentUri:Landroid/net/Uri;
    invoke-virtual {p0, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 173
    .local v3, type:Ljava/lang/String;
    const-string v4, "iu.UploadsManager"

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 174
    const-string v4, "iu.UploadsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  contentType from resolver: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_25
    if-nez v3, :cond_4b

    .line 177
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/net/URLConnection;->guessContentTypeFromName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 178
    const-string v4, "iu.UploadsManager"

    invoke-static {v4, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4b

    .line 179
    const-string v4, "iu.UploadsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  guess contentType from url: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_4b
    if-nez v3, :cond_76

    .line 183
    const/4 v2, 0x0

    .line 185
    .local v2, is:Ljava/io/InputStream;
    :try_start_4e
    invoke-virtual {p0, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v2

    .line 186
    invoke-static {v2}, Ljava/net/URLConnection;->guessContentTypeFromStream(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 187
    const-string v4, "iu.UploadsManager"

    const/4 v5, 0x2

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_73

    .line 188
    const-string v4, "iu.UploadsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  guess contentType from stream: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_73
    .catchall {:try_start_4e .. :try_end_73} :catchall_9e
    .catch Ljava/lang/Throwable; {:try_start_4e .. :try_end_73} :catch_7c

    .line 195
    :cond_73
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 198
    .end local v2           #is:Ljava/io/InputStream;
    :cond_76
    :goto_76
    if-eqz v3, :cond_a3

    .line 199
    invoke-virtual {p1, v3}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setMimeType(Ljava/lang/String;)V

    .line 202
    .end local v3           #type:Ljava/lang/String;
    :goto_7b
    return-object v3

    .line 190
    .restart local v2       #is:Ljava/io/InputStream;
    .restart local v3       #type:Ljava/lang/String;
    :catch_7c
    move-exception v1

    .line 191
    .local v1, ignored:Ljava/lang/Throwable;
    :try_start_7d
    const-string v4, "iu.UploadsManager"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_9a

    .line 192
    const-string v4, "iu.UploadsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "failed to guess content type:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9a
    .catchall {:try_start_7d .. :try_end_9a} :catchall_9e

    .line 195
    :cond_9a
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_76

    .end local v1           #ignored:Ljava/lang/Throwable;
    :catchall_9e
    move-exception v4

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v4

    .line 202
    .end local v2           #is:Ljava/io/InputStream;
    :cond_a3
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMimeType()Ljava/lang/String;

    move-result-object v3

    goto :goto_7b
.end method

.method static setFileSize(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    .registers 11
    .parameter "resolver"
    .parameter "task"

    .prologue
    const/4 v8, 0x3

    const-wide/16 v6, 0x0

    .line 207
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    .line 208
    .local v0, contentUri:Landroid/net/Uri;
    sget-object v3, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->PROJECTION_SIZE:[Ljava/lang/String;

    invoke-static {p0, v0, v3, v6, v7}, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->getOptionalLong(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;J)J

    move-result-wide v1

    .line 209
    .local v1, size:J
    cmp-long v3, v1, v6

    if-lez v3, :cond_2d

    .line 210
    const-string v3, "iu.UploadsManager"

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2d

    .line 211
    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "   media size from resolver: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    :cond_2d
    cmp-long v3, v1, v6

    if-nez v3, :cond_55

    .line 218
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->getFileLengthFromRawFdOrContent(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v1

    .line 219
    cmp-long v3, v1, v6

    if-lez v3, :cond_55

    .line 220
    const-string v3, "iu.UploadsManager"

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_55

    .line 221
    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "   media size from content: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_55
    cmp-long v3, v1, v6

    if-nez v3, :cond_6e

    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "no content to upload: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 226
    :cond_6e
    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesTotal(J)V

    .line 227
    return-void
.end method
