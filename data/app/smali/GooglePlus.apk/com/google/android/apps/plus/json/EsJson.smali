.class public abstract Lcom/google/android/apps/plus/json/EsJson;
.super Ljava/lang/Object;
.source "EsJson.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/json/EsJson$SimpleJson;,
        Lcom/google/android/apps/plus/json/EsJson$FieldConverter;,
        Lcom/google/android/apps/plus/json/EsJson$UnknownKeyHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field protected static final JSON_KEY:Ljava/lang/Object;

.field protected static final JSON_STRING:Ljava/lang/Object;

.field private static final UTF_8:Ljava/nio/charset/Charset;

.field private static sSimpleJsonMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/android/apps/plus/json/EsJson$SimpleJson",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field private mConfiguration:[Ljava/lang/Object;

.field private mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

.field private mTargetClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 49
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/json/EsJson;->JSON_STRING:Ljava/lang/Object;

    .line 55
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/json/EsJson;->JSON_KEY:Ljava/lang/Object;

    .line 65
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/json/EsJson;->UTF_8:Ljava/nio/charset/Charset;

    .line 95
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/json/EsJson;->sSimpleJsonMap:Ljava/util/HashMap;

    return-void
.end method

.method protected constructor <init>()V
    .registers 1

    .prologue
    .line 102
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    return-void
.end method

.method protected varargs constructor <init>(Ljava/lang/Class;[Ljava/lang/Object;)V
    .registers 3
    .parameter
    .parameter "configuration"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 122
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    .local p1, clazz:Ljava/lang/Class;,"Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p1, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    .line 124
    iput-object p2, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    .line 125
    return-void
.end method

.method public static varargs buildJson(Ljava/lang/Class;[Ljava/lang/Object;)Lcom/google/android/apps/plus/json/EsJson;
    .registers 3
    .parameter
    .parameter "configuration"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;[",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 153
    .local p0, clazz:Ljava/lang/Class;,"Ljava/lang/Class<TE;>;"
    new-instance v0, Lcom/google/android/apps/plus/json/EsJson$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/json/EsJson$1;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public static getSimpleJson(Ljava/lang/Class;)Lcom/google/android/apps/plus/json/EsJson;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 139
    .local p0, clazz:Ljava/lang/Class;,"Ljava/lang/Class<TE;>;"
    sget-object v1, Lcom/google/android/apps/plus/json/EsJson;->sSimpleJsonMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/json/EsJson$SimpleJson;

    .line 140
    .local v0, json:Lcom/google/android/apps/plus/json/EsJson$SimpleJson;,"Lcom/google/android/apps/plus/json/EsJson$SimpleJson<TE;>;"
    if-nez v0, :cond_14

    .line 141
    new-instance v0, Lcom/google/android/apps/plus/json/EsJson$SimpleJson;

    .end local v0           #json:Lcom/google/android/apps/plus/json/EsJson$SimpleJson;,"Lcom/google/android/apps/plus/json/EsJson$SimpleJson<TE;>;"
    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/json/EsJson$SimpleJson;-><init>(Ljava/lang/Class;)V

    .line 142
    .restart local v0       #json:Lcom/google/android/apps/plus/json/EsJson$SimpleJson;,"Lcom/google/android/apps/plus/json/EsJson$SimpleJson<TE;>;"
    sget-object v1, Lcom/google/android/apps/plus/json/EsJson;->sSimpleJsonMap:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    :cond_14
    return-object v0
.end method

.method private static initConverter(Lcom/google/android/apps/plus/json/EsJson$FieldConverter;Ljava/lang/reflect/Field;Z)V
    .registers 12
    .parameter "converter"
    .parameter "field"
    .parameter "asString"

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 239
    iput-object p1, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    .line 241
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    .line 242
    .local v2, type:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const-class v3, Ljava/lang/String;

    if-ne v2, v3, :cond_1a

    .line 243
    iput v4, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    .line 286
    :cond_11
    :goto_11
    if-eqz p2, :cond_19

    .line 287
    iget v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    .line 289
    :cond_19
    return-void

    .line 244
    :cond_1a
    const-class v3, Ljava/lang/Integer;

    if-eq v2, v3, :cond_22

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v2, v3, :cond_25

    .line 245
    :cond_22
    iput v5, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_11

    .line 246
    :cond_25
    const-class v3, Ljava/lang/Long;

    if-eq v2, v3, :cond_2d

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-ne v2, v3, :cond_30

    .line 247
    :cond_2d
    iput v6, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_11

    .line 248
    :cond_30
    const-class v3, Ljava/lang/Float;

    if-eq v2, v3, :cond_38

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-ne v2, v3, :cond_3b

    .line 249
    :cond_38
    iput v7, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_11

    .line 250
    :cond_3b
    const-class v3, Ljava/lang/Double;

    if-eq v2, v3, :cond_43

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-ne v2, v3, :cond_46

    .line 251
    :cond_43
    iput v8, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_11

    .line 252
    :cond_46
    const-class v3, Ljava/lang/Boolean;

    if-eq v2, v3, :cond_4e

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-ne v2, v3, :cond_52

    .line 253
    :cond_4e
    const/4 v3, 0x5

    iput v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_11

    .line 254
    :cond_52
    const-class v3, Ljava/math/BigInteger;

    if-ne v2, v3, :cond_5a

    .line 255
    const/4 v3, 0x6

    iput v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto :goto_11

    .line 256
    :cond_5a
    const-class v3, Ljava/util/List;

    invoke-virtual {v3, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_b4

    .line 257
    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 258
    .local v0, genericType:Ljava/lang/reflect/ParameterizedType;
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v3

    aget-object v1, v3, v4

    check-cast v1, Ljava/lang/Class;

    .line 259
    .local v1, listItemClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    const/4 v3, 0x7

    iput v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    .line 260
    const-class v3, Ljava/lang/String;

    if-ne v1, v3, :cond_83

    .line 261
    iput v4, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    .line 278
    :goto_79
    if-eqz p2, :cond_11

    .line 279
    iget v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    .line 280
    const/4 p2, 0x0

    goto :goto_11

    .line 262
    :cond_83
    const-class v3, Ljava/lang/Integer;

    if-ne v1, v3, :cond_8a

    .line 263
    iput v5, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_79

    .line 264
    :cond_8a
    const-class v3, Ljava/lang/Long;

    if-ne v1, v3, :cond_91

    .line 265
    iput v6, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_79

    .line 266
    :cond_91
    const-class v3, Ljava/lang/Float;

    if-ne v1, v3, :cond_98

    .line 267
    iput v7, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_79

    .line 268
    :cond_98
    const-class v3, Ljava/lang/Double;

    if-ne v1, v3, :cond_9f

    .line 269
    iput v8, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_79

    .line 270
    :cond_9f
    const-class v3, Ljava/lang/Boolean;

    if-ne v1, v3, :cond_a7

    .line 271
    const/4 v3, 0x5

    iput v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_79

    .line 272
    :cond_a7
    const-class v3, Ljava/math/BigInteger;

    if-ne v1, v3, :cond_af

    .line 273
    const/4 v3, 0x6

    iput v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_79

    .line 275
    :cond_af
    const/16 v3, 0x8

    iput v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    goto :goto_79

    .line 283
    .end local v0           #genericType:Ljava/lang/reflect/ParameterizedType;
    .end local v1           #listItemClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :cond_b4
    const/16 v3, 0x8

    iput v3, p0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    goto/16 :goto_11
.end method

.method private initializeFieldConverters()V
    .registers 15

    .prologue
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    const/4 v13, 0x0

    .line 161
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v9, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/android/apps/plus/json/EsJson$FieldConverter;>;"
    const/4 v6, 0x0

    .line 163
    .local v6, index:I
    :goto_7
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    array-length v10, v10

    if-ge v6, v10, :cond_c2

    .line 164
    new-instance v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    invoke-direct {v1, v13}, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;-><init>(B)V

    .line 165
    .local v1, converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    .end local v6           #index:I
    .local v7, index:I
    aget-object v3, v10, v6

    .line 166
    .local v3, element:Ljava/lang/Object;
    sget-object v10, Lcom/google/android/apps/plus/json/EsJson;->JSON_KEY:Ljava/lang/Object;

    if-ne v3, v10, :cond_d0

    .line 167
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v6, v7, 0x1

    .end local v7           #index:I
    .restart local v6       #index:I
    aget-object v10, v10, v7

    check-cast v10, Ljava/lang/String;

    iput-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    .line 168
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    .end local v6           #index:I
    .restart local v7       #index:I
    aget-object v3, v10, v6

    move v6, v7

    .line 171
    .end local v7           #index:I
    .restart local v6       #index:I
    :goto_2c
    const/4 v0, 0x0

    .line 172
    .local v0, asString:Z
    sget-object v10, Lcom/google/android/apps/plus/json/EsJson;->JSON_STRING:Ljava/lang/Object;

    if-ne v3, v10, :cond_39

    .line 173
    const/4 v0, 0x1

    .line 174
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    .end local v6           #index:I
    .restart local v7       #index:I
    aget-object v3, v10, v6

    move v6, v7

    .line 177
    .end local v7           #index:I
    .restart local v6       #index:I
    :cond_39
    instance-of v10, v3, Lcom/google/android/apps/plus/json/EsJson;

    if-eqz v10, :cond_66

    .line 178
    check-cast v3, Lcom/google/android/apps/plus/json/EsJson;

    .end local v3           #element:Ljava/lang/Object;
    iput-object v3, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    .line 179
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    .end local v6           #index:I
    .restart local v7       #index:I
    aget-object v3, v10, v6

    .restart local v3       #element:Ljava/lang/Object;
    move v6, v7

    .end local v7           #index:I
    .restart local v6       #index:I
    :cond_48
    :goto_48
    move-object v5, v3

    .line 190
    check-cast v5, Ljava/lang/String;

    .line 191
    .local v5, fieldName:Ljava/lang/String;
    iget-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    if-nez v10, :cond_51

    .line 192
    iput-object v5, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    .line 195
    :cond_51
    iget-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    invoke-virtual {v10, v13}, Ljava/lang/String;->charAt(I)C

    move-result v10

    iput-char v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->firstChar:C

    .line 199
    :try_start_59
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v10, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_5e
    .catch Ljava/lang/NoSuchFieldException; {:try_start_59 .. :try_end_5e} :catch_a0

    move-result-object v4

    .line 204
    .local v4, field:Ljava/lang/reflect/Field;
    invoke-static {v1, v4, v0}, Lcom/google/android/apps/plus/json/EsJson;->initConverter(Lcom/google/android/apps/plus/json/EsJson$FieldConverter;Ljava/lang/reflect/Field;Z)V

    .line 206
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 180
    .end local v4           #field:Ljava/lang/reflect/Field;
    .end local v5           #fieldName:Ljava/lang/String;
    :cond_66
    instance-of v10, v3, Ljava/lang/Class;

    if-eqz v10, :cond_48

    move-object v8, v3

    .line 181
    check-cast v8, Ljava/lang/Class;

    .line 183
    .local v8, jsonClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    :try_start_6d
    const-string v10, "getInstance"

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Class;

    invoke-virtual {v8, v10, v11}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-virtual {v10, v11, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/json/EsJson;

    iput-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;
    :try_end_82
    .catch Ljava/lang/Exception; {:try_start_6d .. :try_end_82} :catch_8a

    .line 187
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mConfiguration:[Ljava/lang/Object;

    add-int/lit8 v7, v6, 0x1

    .end local v6           #index:I
    .restart local v7       #index:I
    aget-object v3, v10, v6

    move v6, v7

    .end local v7           #index:I
    .restart local v6       #index:I
    goto :goto_48

    .line 184
    :catch_8a
    move-exception v2

    .line 185
    .local v2, e:Ljava/lang/Exception;
    new-instance v10, Ljava/lang/IllegalStateException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Invalid EsJson class: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v10

    .line 201
    .end local v2           #e:Ljava/lang/Exception;
    .end local v8           #jsonClass:Ljava/lang/Class;,"Ljava/lang/Class<*>;"
    .restart local v5       #fieldName:Ljava/lang/String;
    :catch_a0
    move-exception v10

    new-instance v10, Ljava/lang/IllegalStateException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "No such field: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 209
    .end local v0           #asString:Z
    .end local v1           #converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    .end local v3           #element:Ljava/lang/Object;
    .end local v5           #fieldName:Ljava/lang/String;
    :cond_c2
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    iput-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    .line 210
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 211
    return-void

    .end local v6           #index:I
    .restart local v1       #converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    .restart local v3       #element:Ljava/lang/Object;
    .restart local v7       #index:I
    :cond_d0
    move v6, v7

    .end local v7           #index:I
    .restart local v6       #index:I
    goto/16 :goto_2c
.end method

.method private read(Lcom/google/android/apps/plus/json/JsonReader;Lcom/google/android/apps/plus/json/EsJson$UnknownKeyHandler;)Ljava/lang/Object;
    .registers 16
    .parameter "reader"
    .parameter "unknownKeyHandler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/json/JsonReader;",
            "Lcom/google/android/apps/plus/json/EsJson$UnknownKeyHandler;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 421
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    if-nez v10, :cond_13

    .line 422
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    if-nez v10, :cond_10

    .line 423
    new-instance v10, Ljava/lang/UnsupportedOperationException;

    const-string v11, "A JSON class must either configure the automatic parser or override read(JsonReader)"

    invoke-direct {v10, v11}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 426
    :cond_10
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/EsJson;->initializeFieldConverters()V

    .line 429
    :cond_13
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/EsJson;->newInstance()Ljava/lang/Object;

    move-result-object v8

    .line 430
    .local v8, object:Ljava/lang/Object;,"TT;"
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->beginObject()V

    .line 431
    :cond_1a
    :goto_1a
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1c5

    .line 432
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v6

    .line 433
    .local v6, key:Ljava/lang/String;
    const/4 v1, 0x0

    .line 434
    .local v1, converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 435
    .local v3, firstChar:C
    const/4 v5, 0x0

    .local v5, i:I
    :goto_2b
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    array-length v10, v10

    if-ge v5, v10, :cond_41

    .line 436
    iget-object v10, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aget-object v0, v10, v5

    .line 438
    .local v0, aConverter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    iget-char v10, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->firstChar:C

    if-ne v10, v3, :cond_82

    iget-object v10, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_82

    .line 439
    move-object v1, v0

    .line 444
    .end local v0           #aConverter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    :cond_41
    if-eqz v1, :cond_1b7

    .line 446
    iget v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    packed-switch v10, :pswitch_data_1ca

    .line 568
    :pswitch_48
    const/4 v9, 0x0

    .line 569
    .local v9, value:Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->skipValue()V

    .line 574
    .end local v9           #value:Ljava/lang/Object;
    :goto_4c
    :try_start_4c
    iget-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v10, v8, v9}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_51
    .catch Ljava/lang/Exception; {:try_start_4c .. :try_end_51} :catch_52

    goto :goto_1a

    .line 575
    :catch_52
    move-exception v2

    .line 576
    .local v2, e:Ljava/lang/Exception;
    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Cannot assign field value: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 435
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v0       #aConverter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    :cond_82
    add-int/lit8 v5, v5, 0x1

    goto :goto_2b

    .line 449
    .end local v0           #aConverter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    :pswitch_85
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v9

    .line 450
    .local v9, value:Ljava/lang/String;
    goto :goto_4c

    .line 453
    .end local v9           #value:Ljava/lang/String;
    :pswitch_8a
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextInt()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 454
    .local v9, value:Ljava/lang/Integer;
    goto :goto_4c

    .line 457
    .end local v9           #value:Ljava/lang/Integer;
    :pswitch_93
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    .line 458
    .restart local v9       #value:Ljava/lang/Integer;
    goto :goto_4c

    .line 461
    .end local v9           #value:Ljava/lang/Integer;
    :pswitch_9c
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextLong()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 462
    .local v9, value:Ljava/lang/Long;
    goto :goto_4c

    .line 465
    .end local v9           #value:Ljava/lang/Long;
    :pswitch_a5
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    .line 466
    .restart local v9       #value:Ljava/lang/Long;
    goto :goto_4c

    .line 469
    .end local v9           #value:Ljava/lang/Long;
    :pswitch_ae
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextDouble()D

    move-result-wide v10

    double-to-float v10, v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    .line 470
    .local v9, value:Ljava/lang/Float;
    goto :goto_4c

    .line 473
    .end local v9           #value:Ljava/lang/Float;
    :pswitch_b8
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v9

    .line 474
    .restart local v9       #value:Ljava/lang/Float;
    goto :goto_4c

    .line 477
    .end local v9           #value:Ljava/lang/Float;
    :pswitch_c1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextDouble()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    .line 478
    .local v9, value:Ljava/lang/Double;
    goto :goto_4c

    .line 481
    .end local v9           #value:Ljava/lang/Double;
    :pswitch_ca
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v9

    .line 482
    .restart local v9       #value:Ljava/lang/Double;
    goto/16 :goto_4c

    .line 485
    .end local v9           #value:Ljava/lang/Double;
    :pswitch_d4
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextBoolean()Z

    move-result v10

    if-eqz v10, :cond_de

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 486
    .local v9, value:Ljava/lang/Boolean;
    :goto_dc
    goto/16 :goto_4c

    .line 485
    .end local v9           #value:Ljava/lang/Boolean;
    :cond_de
    sget-object v9, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_dc

    .line 489
    :pswitch_e1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v9

    .line 490
    .restart local v9       #value:Ljava/lang/Boolean;
    goto/16 :goto_4c

    .line 494
    .end local v9           #value:Ljava/lang/Boolean;
    :pswitch_eb
    new-instance v9, Ljava/math/BigInteger;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    .line 495
    .local v9, value:Ljava/math/BigInteger;
    goto/16 :goto_4c

    .line 498
    .end local v9           #value:Ljava/math/BigInteger;
    :pswitch_f6
    iget-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    invoke-direct {v10, p1, p2}, Lcom/google/android/apps/plus/json/EsJson;->read(Lcom/google/android/apps/plus/json/JsonReader;Lcom/google/android/apps/plus/json/EsJson$UnknownKeyHandler;)Ljava/lang/Object;

    move-result-object v9

    .line 499
    .local v9, value:Ljava/lang/Object;
    goto/16 :goto_4c

    .line 502
    .end local v9           #value:Ljava/lang/Object;
    :pswitch_fe
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 503
    .local v7, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->beginArray()V

    .line 504
    :goto_106
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1b1

    .line 505
    iget v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    sparse-switch v10, :sswitch_data_21c

    goto :goto_106

    .line 508
    :sswitch_112
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    .line 512
    :sswitch_11a
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextInt()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    .line 516
    :sswitch_126
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    .line 520
    :sswitch_132
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextLong()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    .line 524
    :sswitch_13e
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    .line 528
    :sswitch_14a
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextDouble()D

    move-result-wide v10

    double-to-float v10, v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    .line 532
    :sswitch_157
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    .line 536
    :sswitch_163
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextDouble()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    .line 540
    :sswitch_16f
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    .line 544
    :sswitch_17b
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextBoolean()Z

    move-result v10

    if-eqz v10, :cond_187

    sget-object v10, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_183
    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_106

    :cond_187
    sget-object v10, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_183

    .line 548
    :sswitch_18a
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_106

    .line 553
    :sswitch_197
    new-instance v10, Ljava/math/BigInteger;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_106

    .line 557
    :sswitch_1a5
    iget-object v10, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    const/4 v11, 0x0

    invoke-direct {v10, p1, v11}, Lcom/google/android/apps/plus/json/EsJson;->read(Lcom/google/android/apps/plus/json/JsonReader;Lcom/google/android/apps/plus/json/EsJson$UnknownKeyHandler;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_106

    .line 563
    :cond_1b1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->endArray()V

    .line 564
    move-object v9, v7

    .line 565
    .local v9, value:Ljava/util/ArrayList;
    goto/16 :goto_4c

    .line 580
    .end local v7           #list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v9           #value:Ljava/util/ArrayList;
    :cond_1b7
    const/4 v4, 0x0

    .line 583
    .local v4, handled:Z
    if-eqz p2, :cond_1be

    .line 584
    invoke-interface {p2}, Lcom/google/android/apps/plus/json/EsJson$UnknownKeyHandler;->handleUnknownKey$7f90bde0()Z

    move-result v4

    .line 587
    :cond_1be
    if-nez v4, :cond_1a

    .line 589
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->skipValue()V

    goto/16 :goto_1a

    .line 593
    .end local v1           #converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    .end local v3           #firstChar:C
    .end local v4           #handled:Z
    .end local v5           #i:I
    .end local v6           #key:Ljava/lang/String;
    :cond_1c5
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->endObject()V

    .line 594
    return-object v8

    .line 446
    nop

    :pswitch_data_1ca
    .packed-switch 0x0
        :pswitch_85
        :pswitch_8a
        :pswitch_9c
        :pswitch_ae
        :pswitch_c1
        :pswitch_d4
        :pswitch_eb
        :pswitch_fe
        :pswitch_f6
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_48
        :pswitch_85
        :pswitch_93
        :pswitch_a5
        :pswitch_b8
        :pswitch_ca
        :pswitch_e1
        :pswitch_eb
    .end packed-switch

    .line 505
    :sswitch_data_21c
    .sparse-switch
        0x0 -> :sswitch_112
        0x1 -> :sswitch_11a
        0x2 -> :sswitch_132
        0x3 -> :sswitch_14a
        0x4 -> :sswitch_163
        0x5 -> :sswitch_17b
        0x6 -> :sswitch_197
        0x8 -> :sswitch_1a5
        0x20 -> :sswitch_112
        0x21 -> :sswitch_126
        0x22 -> :sswitch_13e
        0x23 -> :sswitch_157
        0x24 -> :sswitch_16f
        0x25 -> :sswitch_18a
        0x26 -> :sswitch_197
    .end sparse-switch
.end method

.method private write(Lcom/google/android/apps/plus/json/JsonWriter;Ljava/lang/Object;)V
    .registers 3
    .parameter "writer"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/json/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 603
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    .local p2, object:Ljava/lang/Object;,"TT;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/json/EsJson;->writeObject(Lcom/google/android/apps/plus/json/JsonWriter;Ljava/lang/Object;)V

    .line 604
    return-void
.end method

.method private writeObject(Lcom/google/android/apps/plus/json/JsonWriter;Ljava/lang/Object;)V
    .registers 13
    .parameter "writer"
    .parameter "object"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 641
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    iget-object v8, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    if-nez v8, :cond_13

    .line 642
    iget-object v8, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    if-nez v8, :cond_10

    .line 643
    new-instance v8, Ljava/lang/UnsupportedOperationException;

    const-string v9, "A JSON class must either configure the automatic parser or override read(Jsonwriter)"

    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 646
    :cond_10
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/EsJson;->initializeFieldConverters()V

    .line 649
    :cond_13
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonWriter;->beginObject()Lcom/google/android/apps/plus/json/JsonWriter;

    .line 650
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/json/EsJson;->getValues(Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    .line 651
    .local v7, values:[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1b
    array-length v8, v7

    if-ge v1, v8, :cond_b2

    .line 652
    aget-object v8, v7, v1

    if-eqz v8, :cond_32

    .line 653
    aget-object v6, v7, v1

    .line 657
    .local v6, value:Ljava/lang/Object;
    iget-object v8, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aget-object v0, v8, v1

    .line 658
    .local v0, converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    iget-object v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/json/JsonWriter;->name(Ljava/lang/String;)Lcom/google/android/apps/plus/json/JsonWriter;

    .line 659
    iget v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    packed-switch v8, :pswitch_data_b6

    .line 741
    .end local v0           #converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    .end local v6           #value:Ljava/lang/Object;
    :cond_32
    :goto_32
    :pswitch_32
    add-int/lit8 v1, v1, 0x1

    goto :goto_1b

    .line 662
    .restart local v0       #converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    .restart local v6       #value:Ljava/lang/Object;
    :pswitch_35
    check-cast v6, Ljava/lang/String;

    .end local v6           #value:Ljava/lang/Object;
    invoke-virtual {p1, v6}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/String;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_32

    .line 668
    .restart local v6       #value:Ljava/lang/Object;
    :pswitch_3b
    check-cast v6, Ljava/lang/Number;

    .end local v6           #value:Ljava/lang/Object;
    invoke-virtual {p1, v6}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_32

    .line 672
    .restart local v6       #value:Ljava/lang/Object;
    :pswitch_41
    check-cast v6, Ljava/lang/Float;

    .end local v6           #value:Ljava/lang/Object;
    invoke-virtual {p1, v6}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_32

    .line 676
    .restart local v6       #value:Ljava/lang/Object;
    :pswitch_47
    check-cast v6, Ljava/lang/Double;

    .end local v6           #value:Ljava/lang/Object;
    invoke-virtual {p1, v6}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_32

    .line 680
    .restart local v6       #value:Ljava/lang/Object;
    :pswitch_4d
    check-cast v6, Ljava/lang/Boolean;

    .end local v6           #value:Ljava/lang/Object;
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Z)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_32

    .line 689
    .restart local v6       #value:Ljava/lang/Object;
    :pswitch_57
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/String;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_32

    .line 693
    :pswitch_5f
    iget-object v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    invoke-direct {v8, p1, v6}, Lcom/google/android/apps/plus/json/EsJson;->writeObject(Lcom/google/android/apps/plus/json/JsonWriter;Ljava/lang/Object;)V

    goto :goto_32

    :pswitch_65
    move-object v4, v6

    .line 697
    check-cast v4, Ljava/util/List;

    .line 698
    .local v4, list:Ljava/util/List;,"Ljava/util/List<*>;"
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonWriter;->beginArray()Lcom/google/android/apps/plus/json/JsonWriter;

    .line 699
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 700
    .local v5, size:I
    const/4 v3, 0x0

    .local v3, j:I
    :goto_70
    if-ge v3, v5, :cond_ae

    .line 701
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 702
    .local v2, item:Ljava/lang/Object;
    iget v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    sparse-switch v8, :sswitch_data_108

    .line 700
    .end local v2           #item:Ljava/lang/Object;
    :goto_7b
    add-int/lit8 v3, v3, 0x1

    goto :goto_70

    .line 705
    .restart local v2       #item:Ljava/lang/Object;
    :sswitch_7e
    check-cast v2, Ljava/lang/String;

    .end local v2           #item:Ljava/lang/Object;
    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/String;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_7b

    .line 711
    .restart local v2       #item:Ljava/lang/Object;
    :sswitch_84
    check-cast v2, Ljava/lang/Number;

    .end local v2           #item:Ljava/lang/Object;
    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_7b

    .line 715
    .restart local v2       #item:Ljava/lang/Object;
    :sswitch_8a
    check-cast v2, Ljava/lang/Float;

    .end local v2           #item:Ljava/lang/Object;
    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_7b

    .line 719
    .restart local v2       #item:Ljava/lang/Object;
    :sswitch_90
    check-cast v2, Ljava/lang/Double;

    .end local v2           #item:Ljava/lang/Object;
    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/Number;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_7b

    .line 723
    .restart local v2       #item:Ljava/lang/Object;
    :sswitch_96
    check-cast v2, Ljava/lang/Boolean;

    .end local v2           #item:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Z)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_7b

    .line 732
    .restart local v2       #item:Ljava/lang/Object;
    :sswitch_a0
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Lcom/google/android/apps/plus/json/JsonWriter;->value(Ljava/lang/String;)Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_7b

    .line 736
    :sswitch_a8
    iget-object v8, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->json:Lcom/google/android/apps/plus/json/EsJson;

    invoke-direct {v8, p1, v2}, Lcom/google/android/apps/plus/json/EsJson;->writeObject(Lcom/google/android/apps/plus/json/JsonWriter;Ljava/lang/Object;)V

    goto :goto_7b

    .line 740
    .end local v2           #item:Ljava/lang/Object;
    :cond_ae
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonWriter;->endArray()Lcom/google/android/apps/plus/json/JsonWriter;

    goto :goto_32

    .line 746
    .end local v0           #converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    .end local v3           #j:I
    .end local v4           #list:Ljava/util/List;,"Ljava/util/List<*>;"
    .end local v5           #size:I
    .end local v6           #value:Ljava/lang/Object;
    :cond_b2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonWriter;->endObject()Lcom/google/android/apps/plus/json/JsonWriter;

    .line 747
    return-void

    .line 659
    :pswitch_data_b6
    .packed-switch 0x0
        :pswitch_35
        :pswitch_3b
        :pswitch_3b
        :pswitch_41
        :pswitch_47
        :pswitch_4d
        :pswitch_3b
        :pswitch_65
        :pswitch_5f
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_35
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
        :pswitch_57
    .end packed-switch

    .line 702
    :sswitch_data_108
    .sparse-switch
        0x0 -> :sswitch_7e
        0x1 -> :sswitch_84
        0x2 -> :sswitch_84
        0x3 -> :sswitch_8a
        0x4 -> :sswitch_90
        0x5 -> :sswitch_96
        0x6 -> :sswitch_84
        0x8 -> :sswitch_a8
        0x20 -> :sswitch_7e
        0x21 -> :sswitch_a0
        0x22 -> :sswitch_a0
        0x23 -> :sswitch_a0
        0x24 -> :sswitch_a0
        0x25 -> :sswitch_a0
        0x26 -> :sswitch_a0
    .end sparse-switch
.end method


# virtual methods
.method protected final buildDefaultConfiguration()V
    .registers 8

    .prologue
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 217
    iget-object v4, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 218
    .local v2, fields:[Ljava/lang/reflect/Field;
    array-length v4, v2

    new-array v4, v4, [Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    iput-object v4, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    .line 219
    const/4 v3, 0x0

    .local v3, i:I
    :goto_f
    array-length v4, v2

    if-ge v3, v4, :cond_60

    .line 220
    aget-object v1, v2, v3

    .line 221
    .local v1, field:Ljava/lang/reflect/Field;
    new-instance v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;-><init>(B)V

    .line 222
    .local v0, converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    .line 223
    iget-object v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v4

    iput-char v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->firstChar:C

    .line 224
    invoke-static {v0, v1, v5}, Lcom/google/android/apps/plus/json/EsJson;->initConverter(Lcom/google/android/apps/plus/json/EsJson$FieldConverter;Ljava/lang/reflect/Field;Z)V

    .line 225
    iget v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->type:I

    if-eq v4, v6, :cond_32

    iget v4, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->itemType:I

    if-ne v4, v6, :cond_59

    .line 227
    :cond_32
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot use default JSON for object containing fields of non-basic types: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 231
    :cond_59
    iget-object v4, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aput-object v0, v4, v3

    .line 219
    add-int/lit8 v3, v3, 0x1

    goto :goto_f

    .line 233
    .end local v0           #converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    .end local v1           #field:Ljava/lang/reflect/Field;
    :cond_60
    return-void
.end method

.method public final fromByteArray([B)Ljava/lang/Object;
    .registers 6
    .parameter "data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)TT;"
        }
    .end annotation

    .prologue
    .line 314
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/json/EsJson;->fromInputStream(Ljava/io/InputStream;)Ljava/lang/Object;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_8} :catch_a

    move-result-object v1

    return-object v1

    .line 315
    :catch_a
    move-exception v0

    .line 316
    .local v0, e:Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot parse JSON using "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final fromInputStream(Ljava/io/InputStream;)Ljava/lang/Object;
    .registers 5
    .parameter "stream"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 295
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    new-instance v0, Lcom/google/android/apps/plus/json/JsonReader;

    new-instance v1, Ljava/io/InputStreamReader;

    sget-object v2, Lcom/google/android/apps/plus/json/EsJson;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v1, p1, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/json/JsonReader;-><init>(Ljava/io/Reader;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;->read(Lcom/google/android/apps/plus/json/JsonReader;Lcom/google/android/apps/plus/json/EsJson$UnknownKeyHandler;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/json/JsonReader;->close()V

    return-object v1
.end method

.method public final fromString(Ljava/lang/String;)Ljava/lang/Object;
    .registers 8
    .parameter "string"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 326
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    :try_start_0
    new-instance v1, Lcom/google/android/apps/plus/json/JsonReader;

    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Lcom/google/android/apps/plus/json/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 327
    .local v1, reader:Lcom/google/android/apps/plus/json/JsonReader;
    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/plus/json/EsJson;->read(Lcom/google/android/apps/plus/json/JsonReader;Lcom/google/android/apps/plus/json/EsJson$UnknownKeyHandler;)Ljava/lang/Object;

    move-result-object v2

    .line 328
    .local v2, result:Ljava/lang/Object;,"TT;"
    invoke-virtual {v1}, Lcom/google/android/apps/plus/json/JsonReader;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_12} :catch_13

    .line 329
    return-object v2

    .line 330
    .end local v1           #reader:Lcom/google/android/apps/plus/json/JsonReader;
    .end local v2           #result:Ljava/lang/Object;,"TT;"
    :catch_13
    move-exception v0

    .line 331
    .local v0, e:Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot parse JSON using "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method protected getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 754
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    .local p1, object:Ljava/lang/Object;,"TT;"
    iget-object v3, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    array-length v3, v3

    new-array v2, v3, [Ljava/lang/Object;

    .line 755
    .local v2, values:[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_6
    iget-object v3, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    array-length v3, v3

    if-ge v1, v3, :cond_36

    .line 757
    :try_start_b
    iget-object v3, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aget-object v3, v3, v1

    iget-object v3, v3, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v3, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v1
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_17} :catch_1a

    .line 755
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 758
    :catch_1a
    move-exception v0

    .line 759
    .local v0, e:Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot obtain field value: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    .line 763
    .end local v0           #e:Ljava/lang/Exception;
    :cond_36
    return-object v2
.end method

.method public newInstance()Ljava/lang/Object;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 400
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v1

    return-object v1

    .line 401
    :catch_7
    move-exception v0

    .line 402
    .local v0, e:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Cannot create new instance"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final setField(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z
    .registers 13
    .parameter
    .parameter "name"
    .parameter "value"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    .local p1, object:Ljava/lang/Object;,"TT;"
    const/4 v6, 0x0

    .line 615
    iget-object v7, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    if-nez v7, :cond_14

    .line 616
    iget-object v7, p0, Lcom/google/android/apps/plus/json/EsJson;->mTargetClass:Ljava/lang/Class;

    if-nez v7, :cond_11

    .line 617
    new-instance v6, Ljava/lang/UnsupportedOperationException;

    const-string v7, "A JSON class must configure the automatic parser to use setField"

    invoke-direct {v6, v7}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 620
    :cond_11
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/EsJson;->initializeFieldConverters()V

    .line 623
    :cond_14
    invoke-virtual {p2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 624
    .local v3, firstChar:C
    iget-object v0, p0, Lcom/google/android/apps/plus/json/EsJson;->mFieldConverters:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;

    .local v0, arr$:[Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    array-length v5, v0

    .local v5, len$:I
    const/4 v4, 0x0

    .local v4, i$:I
    :goto_1c
    if-ge v4, v5, :cond_32

    aget-object v1, v0, v4

    .line 625
    .local v1, converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    iget-char v7, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->firstChar:C

    if-ne v3, v7, :cond_63

    iget-object v7, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->key:Ljava/lang/String;

    invoke-virtual {p2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_63

    .line 627
    :try_start_2c
    iget-object v6, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v6, p1, p3}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_31} :catch_33

    .line 628
    const/4 v6, 0x1

    .line 636
    .end local v1           #converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    :cond_32
    return v6

    .line 629
    .restart local v1       #converter:Lcom/google/android/apps/plus/json/EsJson$FieldConverter;
    :catch_33
    move-exception v2

    .line 630
    .local v2, e:Ljava/lang/Exception;
    new-instance v6, Ljava/lang/RuntimeException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Cannot assign field value: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v1, Lcom/google/android/apps/plus/json/EsJson$FieldConverter;->field:Ljava/lang/reflect/Field;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 624
    .end local v2           #e:Ljava/lang/Exception;
    :cond_63
    add-int/lit8 v4, v4, 0x1

    goto :goto_1c
.end method

.method public final toByteArray(Ljava/lang/Object;)[B
    .registers 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)[B"
        }
    .end annotation

    .prologue
    .line 349
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    .local p1, object:Ljava/lang/Object;,"TT;"
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 351
    .local v1, stream:Ljava/io/ByteArrayOutputStream;
    :try_start_5
    invoke-virtual {p0, v1, p1}, Lcom/google/android/apps/plus/json/EsJson;->writeToStream(Ljava/io/OutputStream;Ljava/lang/Object;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_8} :catch_d

    .line 356
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    return-object v2

    .line 352
    :catch_d
    move-exception v0

    .line 353
    .local v0, e:Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot generate JSON using "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public toPrettyString(Ljava/lang/Object;)Ljava/lang/String;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 379
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    .local p1, object:Ljava/lang/Object;,"TT;"
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 380
    .local v1, out:Ljava/io/StringWriter;
    new-instance v2, Lcom/google/android/apps/plus/json/JsonWriter;

    invoke-direct {v2, v1}, Lcom/google/android/apps/plus/json/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 381
    .local v2, writer:Lcom/google/android/apps/plus/json/JsonWriter;
    const-string v3, " "

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/json/JsonWriter;->setIndent(Ljava/lang/String;)V

    .line 383
    :try_start_f
    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/plus/json/EsJson;->write(Lcom/google/android/apps/plus/json/JsonWriter;Ljava/lang/Object;)V

    .line 384
    invoke-virtual {v2}, Lcom/google/android/apps/plus/json/JsonWriter;->flush()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_15} :catch_1a

    .line 389
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 385
    :catch_1a
    move-exception v0

    .line 386
    .local v0, e:Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot generate JSON using "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public final toString(Ljava/lang/Object;)Ljava/lang/String;
    .registers 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 363
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    .local p1, object:Ljava/lang/Object;,"TT;"
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 364
    .local v1, out:Ljava/io/StringWriter;
    new-instance v2, Lcom/google/android/apps/plus/json/JsonWriter;

    invoke-direct {v2, v1}, Lcom/google/android/apps/plus/json/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 366
    .local v2, writer:Lcom/google/android/apps/plus/json/JsonWriter;
    :try_start_a
    invoke-direct {p0, v2, p1}, Lcom/google/android/apps/plus/json/EsJson;->write(Lcom/google/android/apps/plus/json/JsonWriter;Ljava/lang/Object;)V

    .line 367
    invoke-virtual {v2}, Lcom/google/android/apps/plus/json/JsonWriter;->flush()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_10} :catch_15

    .line 372
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 368
    :catch_15
    move-exception v0

    .line 369
    .local v0, e:Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot generate JSON using "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public final writeToStream(Ljava/io/OutputStream;Ljava/lang/Object;)V
    .registers 7
    .parameter "stream"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/OutputStream;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 339
    .local p0, this:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TT;>;"
    .local p2, object:Ljava/lang/Object;,"TT;"
    new-instance v0, Lcom/google/android/apps/plus/json/JsonWriter;

    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/OutputStreamWriter;

    sget-object v3, Lcom/google/android/apps/plus/json/EsJson;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v2, p1, v3}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    const/16 v3, 0x2000

    invoke-direct {v1, v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/json/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 341
    .local v0, writer:Lcom/google/android/apps/plus/json/JsonWriter;
    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/plus/json/EsJson;->write(Lcom/google/android/apps/plus/json/JsonWriter;Ljava/lang/Object;)V

    .line 342
    invoke-virtual {v0}, Lcom/google/android/apps/plus/json/JsonWriter;->flush()V

    .line 343
    return-void
.end method
