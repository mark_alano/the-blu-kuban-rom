.class public final Lcom/google/android/apps/plus/json/JsonReader;
.super Ljava/lang/Object;
.source "JsonReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/json/JsonReader$1;
    }
.end annotation


# instance fields
.field private final buffer:[C

.field private final in:Ljava/io/Reader;

.field private lenient:Z

.field private limit:I

.field private name:Ljava/lang/String;

.field private pos:I

.field private skipping:Z

.field private final stack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/json/JsonScope;",
            ">;"
        }
    .end annotation
.end field

.field private token:Lcom/google/android/apps/plus/json/JsonToken;

.field private value:Ljava/lang/String;

.field private valueLength:I

.field private valuePos:I


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .registers 4
    .parameter "in"

    .prologue
    const/4 v1, 0x0

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput-boolean v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->lenient:Z

    .line 188
    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    .line 189
    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    .line 190
    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    .line 192
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    .line 194
    sget-object v0, Lcom/google/android/apps/plus/json/JsonScope;->EMPTY_DOCUMENT:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->push(Lcom/google/android/apps/plus/json/JsonScope;)V

    .line 215
    iput-boolean v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    .line 221
    if-nez p1, :cond_28

    .line 222
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_28
    iput-object p1, p0, Lcom/google/android/apps/plus/json/JsonReader;->in:Ljava/io/Reader;

    .line 225
    return-void
.end method

.method private advance()Lcom/google/android/apps/plus/json/JsonToken;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 358
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    .line 361
    .local v0, result:Lcom/google/android/apps/plus/json/JsonToken;
    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    .line 362
    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    .line 363
    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->name:Ljava/lang/String;

    .line 364
    return-object v0
.end method

.method private static decodeNumber([CII)Lcom/google/android/apps/plus/json/JsonToken;
    .registers 9
    .parameter "chars"
    .parameter "offset"
    .parameter "length"

    .prologue
    const/16 v5, 0x2d

    const/16 v4, 0x39

    const/16 v3, 0x30

    .line 1058
    move v1, p1

    .line 1059
    .local v1, i:I
    aget-char v0, p0, p1

    .line 1061
    .local v0, c:I
    if-ne v0, v5, :cond_f

    .line 1062
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    .line 1065
    :cond_f
    if-ne v0, v3, :cond_26

    .line 1066
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    .line 1076
    :cond_15
    const/16 v2, 0x2e

    if-ne v0, v2, :cond_3c

    .line 1077
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    .line 1078
    :goto_1d
    if-lt v0, v3, :cond_3c

    if-gt v0, v4, :cond_3c

    .line 1079
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    goto :goto_1d

    .line 1067
    :cond_26
    const/16 v2, 0x31

    if-lt v0, v2, :cond_39

    if-gt v0, v4, :cond_39

    .line 1068
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    .line 1069
    :goto_30
    if-lt v0, v3, :cond_15

    if-gt v0, v4, :cond_15

    .line 1070
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    goto :goto_30

    .line 1073
    :cond_39
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    .line 1101
    :goto_3b
    return-object v2

    .line 1083
    :cond_3c
    const/16 v2, 0x65

    if-eq v0, v2, :cond_44

    const/16 v2, 0x45

    if-ne v0, v2, :cond_66

    .line 1084
    :cond_44
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    .line 1085
    const/16 v2, 0x2b

    if-eq v0, v2, :cond_4e

    if-ne v0, v5, :cond_52

    .line 1086
    :cond_4e
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    .line 1088
    :cond_52
    if-lt v0, v3, :cond_63

    if-gt v0, v4, :cond_63

    .line 1089
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    .line 1090
    :goto_5a
    if-lt v0, v3, :cond_66

    if-gt v0, v4, :cond_66

    .line 1091
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    goto :goto_5a

    .line 1094
    :cond_63
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_3b

    .line 1098
    :cond_66
    add-int v2, p1, p2

    if-ne v1, v2, :cond_6d

    .line 1099
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_3b

    .line 1101
    :cond_6d
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_3b
.end method

.method private expect(Lcom/google/android/apps/plus/json/JsonToken;)V
    .registers 5
    .parameter "expected"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 295
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    .line 296
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v0, p1, :cond_2a

    .line 297
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_2a
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    .line 300
    return-void
.end method

.method private fillBuffer(I)Z
    .registers 9
    .parameter "minimum"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 712
    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    if-eq v2, v3, :cond_39

    .line 713
    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    .line 714
    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    invoke-static {v2, v3, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 719
    :goto_19
    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    .line 721
    :cond_1b
    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->in:Ljava/io/Reader;

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget-object v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    array-length v5, v5

    iget v6, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    sub-int/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/Reader;->read([CII)I

    move-result v0

    .local v0, total:I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_38

    .line 722
    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    .line 723
    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-lt v2, p1, :cond_1b

    .line 724
    const/4 v1, 0x1

    .line 727
    :cond_38
    return v1

    .line 716
    .end local v0           #total:I
    :cond_39
    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    goto :goto_19
.end method

.method private getSnippet()Ljava/lang/CharSequence;
    .registers 7

    .prologue
    const/16 v5, 0x14

    .line 1114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1115
    .local v2, snippet:Ljava/lang/StringBuilder;
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1116
    .local v1, beforePos:I
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v4, v1

    invoke-virtual {v2, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1117
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v3, v4

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1118
    .local v0, afterPos:I
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-virtual {v2, v3, v4, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1119
    return-object v2
.end method

.method private nextInArray(Z)Lcom/google/android/apps/plus/json/JsonToken;
    .registers 3
    .parameter "firstElement"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 571
    if-eqz p1, :cond_19

    .line 572
    sget-object v0, Lcom/google/android/apps/plus/json/JsonScope;->NONEMPTY_ARRAY:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V

    .line 589
    :sswitch_7
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_48

    .line 601
    iget v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    .line 606
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextValue()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    :goto_18
    return-object v0

    .line 575
    :cond_19
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_56

    .line 585
    const-string v0, "Unterminated array"

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 577
    :sswitch_27
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->pop()Lcom/google/android/apps/plus/json/JsonScope;

    .line 578
    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_18

    .line 580
    :sswitch_2f
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 591
    :sswitch_36
    if-eqz p1, :cond_40

    .line 592
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->pop()Lcom/google/android/apps/plus/json/JsonScope;

    .line 593
    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_18

    .line 600
    :cond_40
    :sswitch_40
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 589
    nop

    :sswitch_data_48
    .sparse-switch
        0x2c -> :sswitch_40
        0x3b -> :sswitch_40
        0x5d -> :sswitch_36
    .end sparse-switch

    .line 575
    :sswitch_data_56
    .sparse-switch
        0x2c -> :sswitch_7
        0x3b -> :sswitch_2f
        0x5d -> :sswitch_27
    .end sparse-switch
.end method

.method private nextInObject(Z)Lcom/google/android/apps/plus/json/JsonToken;
    .registers 4
    .parameter "firstElement"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 616
    if-eqz p1, :cond_25

    .line 618
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v1

    packed-switch v1, :pswitch_data_54

    .line 623
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    .line 639
    :sswitch_f
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v0

    .line 640
    .local v0, quote:I
    sparse-switch v0, :sswitch_data_5a

    .line 648
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    .line 620
    .end local v0           #quote:I
    :pswitch_1d
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->pop()Lcom/google/android/apps/plus/json/JsonScope;

    .line 621
    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    .line 657
    :goto_24
    return-object v1

    .line 626
    :cond_25
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v1

    sparse-switch v1, :sswitch_data_64

    .line 634
    const-string v1, "Unterminated object"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    .line 628
    :sswitch_33
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->pop()Lcom/google/android/apps/plus/json/JsonScope;

    .line 629
    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_24

    .line 642
    .restart local v0       #quote:I
    :sswitch_3b
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    .line 645
    :sswitch_42
    int-to-char v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString(C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->name:Ljava/lang/String;

    .line 649
    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->DANGLING_NAME:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V

    .line 657
    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->NAME:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_24

    .line 618
    nop

    :pswitch_data_54
    .packed-switch 0x7d
        :pswitch_1d
    .end packed-switch

    .line 640
    :sswitch_data_5a
    .sparse-switch
        0x22 -> :sswitch_42
        0x27 -> :sswitch_3b
    .end sparse-switch

    .line 626
    :sswitch_data_64
    .sparse-switch
        0x2c -> :sswitch_f
        0x3b -> :sswitch_f
        0x7d -> :sswitch_33
    .end sparse-switch
.end method

.method private nextLiteral(Z)Ljava/lang/String;
    .registers 8
    .parameter "assignOffsetsOnly"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 874
    const/4 v0, 0x0

    .line 875
    .local v0, builder:Ljava/lang/StringBuilder;
    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    .line 876
    iput v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    .line 877
    const/4 v1, 0x0

    .line 881
    .local v1, i:I
    :cond_8
    :goto_8
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/2addr v3, v1

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-ge v3, v4, :cond_23

    .line 882
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/2addr v4, v1

    aget-char v3, v3, v4

    sparse-switch v3, :sswitch_data_8a

    .line 881
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 888
    :sswitch_1c
    const-string v3, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v3

    throw v3

    .line 910
    :cond_23
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    array-length v3, v3

    if-ge v1, v3, :cond_4a

    .line 911
    add-int/lit8 v3, v1, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_8

    .line 912
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    aput-char v5, v3, v4

    .line 928
    :goto_36
    :sswitch_36
    if-eqz p1, :cond_6b

    if-nez v0, :cond_6b

    .line 934
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    .line 935
    const/4 v2, 0x0

    .line 944
    .local v2, result:Ljava/lang/String;
    :goto_3f
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    .line 945
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    .line 946
    return-object v2

    .line 920
    .end local v2           #result:Ljava/lang/String;
    :cond_4a
    if-nez v0, :cond_51

    .line 921
    new-instance v0, Ljava/lang/StringBuilder;

    .end local v0           #builder:Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 923
    .restart local v0       #builder:Ljava/lang/StringBuilder;
    :cond_51
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-virtual {v0, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 924
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    .line 925
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    .line 926
    const/4 v1, 0x0

    .line 927
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_8

    goto :goto_36

    .line 936
    :cond_6b
    iget-boolean v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    if-eqz v3, :cond_72

    .line 937
    const-string v2, "skipped!"

    .restart local v2       #result:Ljava/lang/String;
    goto :goto_3f

    .line 938
    .end local v2           #result:Ljava/lang/String;
    :cond_72
    if-nez v0, :cond_7e

    .line 939
    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-direct {v2, v3, v4, v1}, Ljava/lang/String;-><init>([CII)V

    .restart local v2       #result:Ljava/lang/String;
    goto :goto_3f

    .line 941
    .end local v2           #result:Ljava/lang/String;
    :cond_7e
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-virtual {v0, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 942
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .restart local v2       #result:Ljava/lang/String;
    goto :goto_3f

    .line 882
    :sswitch_data_8a
    .sparse-switch
        0x9 -> :sswitch_36
        0xa -> :sswitch_36
        0xc -> :sswitch_36
        0xd -> :sswitch_36
        0x20 -> :sswitch_36
        0x23 -> :sswitch_1c
        0x2c -> :sswitch_36
        0x2f -> :sswitch_1c
        0x3a -> :sswitch_36
        0x3b -> :sswitch_1c
        0x3d -> :sswitch_1c
        0x5b -> :sswitch_36
        0x5c -> :sswitch_1c
        0x5d -> :sswitch_36
        0x7b -> :sswitch_36
        0x7d -> :sswitch_36
    .end sparse-switch
.end method

.method private nextNonWhitespace()I
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 731
    :sswitch_1
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-lt v1, v2, :cond_d

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-eqz v1, :cond_35

    .line 732
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    aget-char v0, v1, v2

    .line 733
    .local v0, c:I
    sparse-switch v0, :sswitch_data_3e

    .line 774
    :cond_1a
    return v0

    .line 741
    :sswitch_1b
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-ne v1, v2, :cond_27

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 745
    :cond_27
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    .line 746
    :sswitch_2e
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    .line 782
    .end local v0           #c:I
    :cond_35
    new-instance v1, Ljava/io/EOFException;

    const-string v2, "End of input"

    invoke-direct {v1, v2}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 733
    nop

    :sswitch_data_3e
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_2e
        0x2f -> :sswitch_1b
    .end sparse-switch
.end method

.method private nextString(C)Ljava/lang/String;
    .registers 10
    .parameter "quote"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x1

    .line 829
    const/4 v0, 0x0

    .line 832
    .local v0, builder:Ljava/lang/StringBuilder;
    :cond_3
    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    .line 833
    .local v2, start:I
    :cond_5
    :goto_5
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-ge v3, v4, :cond_b2

    .line 834
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    aget-char v1, v3, v4

    .line 836
    .local v1, c:I
    if-ne v1, p1, :cond_3c

    .line 837
    iget-boolean v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    if-eqz v3, :cond_1e

    .line 838
    const-string v3, "skipped!"

    .line 843
    :goto_1d
    return-object v3

    .line 839
    :cond_1e
    if-nez v0, :cond_2d

    .line 840
    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v5, v2

    add-int/lit8 v5, v5, -0x1

    invoke-direct {v3, v4, v2, v5}, Ljava/lang/String;-><init>([CII)V

    goto :goto_1d

    .line 842
    :cond_2d
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v3, v2, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 843
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1d

    .line 846
    :cond_3c
    const/16 v3, 0x5c

    if-ne v1, v3, :cond_5

    .line 847
    if-nez v0, :cond_47

    .line 848
    new-instance v0, Ljava/lang/StringBuilder;

    .end local v0           #builder:Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 850
    .restart local v0       #builder:Ljava/lang/StringBuilder;
    :cond_47
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v3, v2, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 851
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-ne v3, v4, :cond_64

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_64

    const-string v3, "Unterminated escape sequence"

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v3

    throw v3

    :cond_64
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    aget-char v3, v3, v4

    sparse-switch v3, :sswitch_data_ce

    :goto_71
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 852
    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    goto :goto_5

    .line 851
    :sswitch_77
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v3, v3, 0x4

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-le v3, v4, :cond_8c

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_8c

    const-string v3, "Unterminated escape sequence"

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v3

    throw v3

    :cond_8c
    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-direct {v3, v4, v5, v7}, Ljava/lang/String;-><init>([CII)V

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    int-to-char v3, v3

    goto :goto_71

    :sswitch_a3
    const/16 v3, 0x9

    goto :goto_71

    :sswitch_a6
    const/16 v3, 0x8

    goto :goto_71

    :sswitch_a9
    const/16 v3, 0xa

    goto :goto_71

    :sswitch_ac
    const/16 v3, 0xd

    goto :goto_71

    :sswitch_af
    const/16 v3, 0xc

    goto :goto_71

    .line 856
    .end local v1           #c:I
    :cond_b2
    if-nez v0, :cond_b9

    .line 857
    new-instance v0, Ljava/lang/StringBuilder;

    .end local v0           #builder:Ljava/lang/StringBuilder;
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 859
    .restart local v0       #builder:Ljava/lang/StringBuilder;
    :cond_b9
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v4, v2

    invoke-virtual {v0, v3, v2, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 860
    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 862
    const-string v3, "Unterminated string"

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v3

    throw v3

    .line 851
    :sswitch_data_ce
    .sparse-switch
        0x62 -> :sswitch_a6
        0x66 -> :sswitch_af
        0x6e -> :sswitch_a9
        0x72 -> :sswitch_ac
        0x74 -> :sswitch_a3
        0x75 -> :sswitch_77
    .end sparse-switch
.end method

.method private nextValue()Lcom/google/android/apps/plus/json/JsonToken;
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x55

    const/16 v7, 0x45

    const/4 v6, 0x4

    const/16 v5, 0x6c

    const/16 v4, 0x4c

    .line 683
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v0

    .line 684
    .local v0, c:I
    sparse-switch v0, :sswitch_data_1be

    .line 701
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    .line 702
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->nextLiteral(Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    if-nez v1, :cond_4f

    const-string v1, "Expected literal value"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    .line 686
    :sswitch_28
    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->EMPTY_OBJECT:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->push(Lcom/google/android/apps/plus/json/JsonScope;)V

    .line 687
    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    .line 702
    :goto_31
    return-object v1

    .line 690
    :sswitch_32
    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->EMPTY_ARRAY:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->push(Lcom/google/android/apps/plus/json/JsonScope;)V

    .line 691
    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_31

    .line 694
    :sswitch_3c
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    .line 697
    :sswitch_43
    int-to-char v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString(C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    .line 698
    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_31

    .line 702
    :cond_4f
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_65

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    :goto_56
    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-ne v1, v2, :cond_1ba

    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :cond_65
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    if-ne v1, v6, :cond_c2

    const/16 v1, 0x6e

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_7d

    const/16 v1, 0x4e

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_c2

    :cond_7d
    const/16 v1, 0x75

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_93

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x1

    aget-char v1, v1, v2

    if-ne v8, v1, :cond_c2

    :cond_93
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-eq v5, v1, :cond_a7

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-ne v4, v1, :cond_c2

    :cond_a7
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x3

    aget-char v1, v1, v2

    if-eq v5, v1, :cond_bb

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x3

    aget-char v1, v1, v2

    if-ne v4, v1, :cond_c2

    :cond_bb
    const-string v1, "null"

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->NULL:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_56

    :cond_c2
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    if-ne v1, v6, :cond_126

    const/16 v1, 0x74

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_da

    const/16 v1, 0x54

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_126

    :cond_da
    const/16 v1, 0x72

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_f2

    const/16 v1, 0x52

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_126

    :cond_f2
    const/16 v1, 0x75

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x2

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_108

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-ne v8, v1, :cond_126

    :cond_108
    const/16 v1, 0x65

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x3

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_11e

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x3

    aget-char v1, v1, v2

    if-ne v7, v1, :cond_126

    :cond_11e
    const-string v1, "true"

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->BOOLEAN:Lcom/google/android/apps/plus/json/JsonToken;

    goto/16 :goto_56

    :cond_126
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1a1

    const/16 v1, 0x66

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_13f

    const/16 v1, 0x46

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_1a1

    :cond_13f
    const/16 v1, 0x61

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_157

    const/16 v1, 0x41

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_1a1

    :cond_157
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-eq v5, v1, :cond_16b

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-ne v4, v1, :cond_1a1

    :cond_16b
    const/16 v1, 0x73

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x3

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_183

    const/16 v1, 0x53

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x3

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_1a1

    :cond_183
    const/16 v1, 0x65

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x4

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_199

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x4

    aget-char v1, v1, v2

    if-ne v7, v1, :cond_1a1

    :cond_199
    const-string v1, "false"

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->BOOLEAN:Lcom/google/android/apps/plus/json/JsonToken;

    goto/16 :goto_56

    :cond_1a1
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/json/JsonReader;->decodeNumber([CII)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v1

    goto/16 :goto_56

    :cond_1ba
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto/16 :goto_31

    .line 684
    :sswitch_data_1be
    .sparse-switch
        0x22 -> :sswitch_43
        0x27 -> :sswitch_3c
        0x5b -> :sswitch_32
        0x7b -> :sswitch_28
    .end sparse-switch
.end method

.method private pop()Lcom/google/android/apps/plus/json/JsonScope;
    .registers 3

    .prologue
    .line 556
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/json/JsonScope;

    return-object v0
.end method

.method private push(Lcom/google/android/apps/plus/json/JsonScope;)V
    .registers 3
    .parameter "newTop"

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 561
    return-void
.end method

.method private replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V
    .registers 4
    .parameter "newTop"

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 568
    return-void
.end method

.method private syntaxError(Ljava/lang/String;)Ljava/io/IOException;
    .registers 5
    .parameter "message"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1110
    new-instance v0, Lcom/google/android/apps/plus/json/MalformedJsonException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " near "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->getSnippet()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/json/MalformedJsonException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final beginArray()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 264
    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->expect(Lcom/google/android/apps/plus/json/JsonToken;)V

    .line 265
    return-void
.end method

.method public final beginObject()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 280
    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->expect(Lcom/google/android/apps/plus/json/JsonToken;)V

    .line 281
    return-void
.end method

.method public final close()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 522
    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    .line 523
    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    .line 524
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->CLOSED:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->in:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 527
    return-void
.end method

.method public final endArray()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 272
    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->expect(Lcom/google/android/apps/plus/json/JsonToken;)V

    .line 273
    return-void
.end method

.method public final endObject()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->expect(Lcom/google/android/apps/plus/json/JsonToken;)V

    .line 289
    return-void
.end method

.method public final hasNext()Z
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v0, v1, :cond_11

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v0, v1, :cond_11

    const/4 v0, 0x1

    :goto_10
    return v0

    :cond_11
    const/4 v0, 0x0

    goto :goto_10
.end method

.method public final nextBoolean()Z
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    .line 412
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BOOLEAN:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_20

    .line 413
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected a boolean but was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 416
    :cond_20
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    const-string v2, "true"

    if-ne v1, v2, :cond_2b

    const/4 v0, 0x1

    .line 417
    .local v0, result:Z
    :goto_27
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    .line 418
    return v0

    .line 416
    .end local v0           #result:Z
    :cond_2b
    const/4 v0, 0x0

    goto :goto_27
.end method

.method public final nextDouble()D
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 445
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    .line 446
    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v3, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v2, v3, :cond_26

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v3, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v2, v3, :cond_26

    .line 447
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Expected a double but was "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 450
    :cond_26
    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 451
    .local v0, result:D
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    .line 452
    return-wide v0
.end method

.method public final nextInt()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    .line 498
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v4, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v3, v4, :cond_26

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v4, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v3, v4, :cond_26

    .line 499
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Expected an int but was "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 504
    :cond_26
    :try_start_26
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2b
    .catch Ljava/lang/NumberFormatException; {:try_start_26 .. :try_end_2b} :catch_30

    move-result v2

    .line 513
    .local v2, result:I
    :cond_2c
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    .line 514
    return v2

    .line 506
    .end local v2           #result:I
    :catch_30
    move-exception v3

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 507
    .local v0, asDouble:D
    double-to-int v2, v0

    .line 508
    .restart local v2       #result:I
    int-to-double v3, v2

    cmpl-double v3, v3, v0

    if-eqz v3, :cond_2c

    .line 509
    new-instance v3, Ljava/lang/NumberFormatException;

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public final nextLong()J
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 466
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    .line 467
    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v5, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v4, v5, :cond_26

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v5, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v4, v5, :cond_26

    .line 468
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Expected a long but was "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 473
    :cond_26
    :try_start_26
    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2b
    .catch Ljava/lang/NumberFormatException; {:try_start_26 .. :try_end_2b} :catch_30

    move-result-wide v2

    .line 482
    .local v2, result:J
    :cond_2c
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    .line 483
    return-wide v2

    .line 475
    .end local v2           #result:J
    :catch_30
    move-exception v4

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 476
    .local v0, asDouble:D
    double-to-long v2, v0

    .line 477
    .restart local v2       #result:J
    long-to-double v4, v2

    cmpl-double v4, v4, v0

    if-eqz v4, :cond_2c

    .line 478
    new-instance v4, Ljava/lang/NumberFormatException;

    iget-object v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public final nextName()Ljava/lang/String;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    .line 376
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->NAME:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_22

    .line 377
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected a name but was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 379
    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->name:Ljava/lang/String;

    .line 380
    .local v0, result:Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    .line 381
    return-object v0
.end method

.method public final nextString()Ljava/lang/String;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    .line 394
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_28

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_28

    .line 395
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected a string but was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 398
    :cond_28
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    .line 399
    .local v0, result:Ljava/lang/String;
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    .line 400
    return-object v0
.end method

.method public final peek()Lcom/google/android/apps/plus/json/JsonToken;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 314
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    if-eqz v1, :cond_9

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    .line 345
    :cond_8
    :goto_8
    return-object v0

    .line 318
    :cond_9
    sget-object v2, Lcom/google/android/apps/plus/json/JsonReader$1;->$SwitchMap$com$google$android$apps$plus$json$JsonScope:[I

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/json/JsonScope;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/json/JsonScope;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_a2

    .line 350
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 320
    :pswitch_2a
    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->NONEMPTY_DOCUMENT:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V

    .line 321
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextValue()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    .line 322
    .local v0, firstToken:Lcom/google/android/apps/plus/json/JsonToken;
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_8

    .line 323
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected JSON document to start with \'[\' or \'{\' but was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 328
    .end local v0           #firstToken:Lcom/google/android/apps/plus/json/JsonToken;
    :pswitch_56
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/json/JsonReader;->nextInArray(Z)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_8

    .line 330
    :pswitch_5b
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/json/JsonReader;->nextInArray(Z)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_8

    .line 332
    :pswitch_60
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/json/JsonReader;->nextInObject(Z)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_8

    .line 334
    :pswitch_65
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v1

    packed-switch v1, :pswitch_data_b6

    :pswitch_6c
    const-string v1, "Expected \':\'"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :pswitch_73
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :pswitch_7a
    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->NONEMPTY_OBJECT:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextValue()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_8

    .line 336
    :pswitch_84
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/json/JsonReader;->nextInObject(Z)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_8

    .line 339
    :pswitch_89
    :try_start_89
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextValue()Lcom/google/android/apps/plus/json/JsonToken;

    .line 340
    const-string v1, "Expected EOF"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1
    :try_end_93
    .catch Ljava/io/EOFException; {:try_start_89 .. :try_end_93} :catch_93

    .line 345
    :catch_93
    move-exception v1

    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_DOCUMENT:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto/16 :goto_8

    .line 348
    :pswitch_9a
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "JsonReader is closed"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 318
    :pswitch_data_a2
    .packed-switch 0x1
        :pswitch_2a
        :pswitch_56
        :pswitch_5b
        :pswitch_60
        :pswitch_65
        :pswitch_84
        :pswitch_89
        :pswitch_9a
    .end packed-switch

    .line 334
    :pswitch_data_b6
    .packed-switch 0x3a
        :pswitch_7a
        :pswitch_6c
        :pswitch_6c
        :pswitch_73
    .end packed-switch
.end method

.method public final skipValue()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 535
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    .line 537
    const/4 v0, 0x0

    .line 539
    .local v0, count:I
    :cond_5
    :try_start_5
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v1

    .line 540
    .local v1, token:Lcom/google/android/apps/plus/json/JsonToken;
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_11

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;
    :try_end_f
    .catchall {:try_start_5 .. :try_end_f} :catchall_23

    if-ne v1, v2, :cond_18

    .line 541
    :cond_11
    add-int/lit8 v0, v0, 0x1

    .line 545
    :cond_13
    :goto_13
    if-nez v0, :cond_5

    .line 547
    iput-boolean v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    .line 548
    return-void

    .line 542
    :cond_18
    :try_start_18
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_20

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;
    :try_end_1e
    .catchall {:try_start_18 .. :try_end_1e} :catchall_23

    if-ne v1, v2, :cond_13

    .line 543
    :cond_20
    add-int/lit8 v0, v0, -0x1

    goto :goto_13

    .line 547
    .end local v1           #token:Lcom/google/android/apps/plus/json/JsonToken;
    :catchall_23
    move-exception v2

    iput-boolean v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    throw v2
.end method

.method public final toString()Ljava/lang/String;
    .registers 3

    .prologue
    .line 950
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " near "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->getSnippet()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
