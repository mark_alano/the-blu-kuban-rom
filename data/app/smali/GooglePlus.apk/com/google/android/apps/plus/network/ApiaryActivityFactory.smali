.class public final Lcom/google/android/apps/plus/network/ApiaryActivityFactory;
.super Ljava/lang/Object;
.source "ApiaryActivityFactory.java"


# direct methods
.method public static getApiaryActivity(Landroid/os/Bundle;)Lcom/google/android/apps/plus/network/ApiaryActivity;
    .registers 4
    .parameter "deepLinkMetadata"

    .prologue
    .line 84
    if-nez p0, :cond_a

    .line 87
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Deep-link metadata must not be null."

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_a
    const-string v1, "ApiaryActivityFactory"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 91
    const-string v1, "ApiaryActivityFactory"

    invoke-virtual {p0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_1c
    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/ApiaryActivity;-><init>()V

    .line 98
    .local v0, activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :try_start_21
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/network/ApiaryActivity;->setDeepLinkMetadata(Landroid/os/Bundle;)V
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_24} :catch_25

    .line 103
    .end local v0           #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :goto_24
    return-object v0

    .line 100
    .restart local v0       #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :catch_25
    move-exception v1

    const/4 v0, 0x0

    goto :goto_24
.end method

.method public static getApiaryActivity(Lcom/google/api/services/plusi/model/LinkPreviewResponse;)Lcom/google/android/apps/plus/network/ApiaryActivity;
    .registers 6
    .parameter "linkPreview"

    .prologue
    const/4 v2, 0x0

    .line 35
    if-nez p0, :cond_b

    .line 38
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 41
    :cond_b
    const-string v3, "ApiaryActivityFactory"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_21

    .line 42
    const-string v3, "ApiaryActivityFactory"

    invoke-static {}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->getInstance()Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/google/api/services/plusi/model/LinkPreviewResponseJson;->toPrettyString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :cond_21
    iget-object v3, p0, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->mediaLayout:Ljava/util/List;

    if-eqz v3, :cond_2d

    iget-object v3, p0, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->mediaLayout:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_35

    .line 46
    :cond_2d
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Media layout must be specified"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 49
    :cond_35
    iget-object v3, p0, Lcom/google/api/services/plusi/model/LinkPreviewResponse;->mediaLayout:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaLayout;

    .line 51
    .local v1, mediaLayout:Lcom/google/api/services/plusi/model/MediaLayout;
    const-string v3, "WEBPAGE"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_51

    .line 52
    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;-><init>()V

    .line 66
    .local v0, activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :goto_4d
    :try_start_4d
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/network/ApiaryActivity;->setLinkPreview(Lcom/google/api/services/plusi/model/LinkPreviewResponse;)V
    :try_end_50
    .catch Ljava/io/IOException; {:try_start_4d .. :try_end_50} :catch_93

    .line 71
    .end local v0           #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :goto_50
    return-object v0

    .line 53
    :cond_51
    const-string v3, "VIDEO"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_61

    .line 54
    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/ApiaryVideoActivity;-><init>()V

    .restart local v0       #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    goto :goto_4d

    .line 55
    .end local v0           #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :cond_61
    const-string v3, "SKYJAM_PREVIEW"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_71

    .line 56
    new-instance v0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;-><init>()V

    .restart local v0       #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    goto :goto_4d

    .line 57
    .end local v0           #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :cond_71
    const-string v3, "SKYJAM_PREVIEW_ALBUM"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_81

    .line 58
    new-instance v0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;-><init>()V

    .restart local v0       #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    goto :goto_4d

    .line 59
    .end local v0           #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :cond_81
    const-string v3, "IMAGE"

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaLayout;->layoutType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_91

    .line 60
    new-instance v0, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;-><init>()V

    .restart local v0       #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    goto :goto_4d

    .end local v0           #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :cond_91
    move-object v0, v2

    .line 62
    goto :goto_50

    .line 68
    .restart local v0       #activity:Lcom/google/android/apps/plus/network/ApiaryActivity;
    :catch_93
    move-exception v3

    move-object v0, v2

    goto :goto_50
.end method
