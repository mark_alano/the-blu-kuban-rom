.class public Lcom/google/android/apps/plus/network/ApiaryArticleActivity;
.super Lcom/google/android/apps/plus/network/ApiaryActivity;
.source "ApiaryArticleActivity.java"


# instance fields
.field private mContent:Ljava/lang/String;

.field private mDisplayName:Ljava/lang/String;

.field private mFavIconUrl:Ljava/lang/String;

.field private mImageList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/plus/network/ApiaryActivity;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mImageList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final getContent()Ljava/lang/String;
    .registers 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mContent:Ljava/lang/String;

    return-object v0
.end method

.method public final getDisplayName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getFavIconUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mFavIconUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getImages()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mImageList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getType()Lcom/google/android/apps/plus/network/ApiaryActivity$Type;
    .registers 2

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/apps/plus/network/ApiaryActivity$Type;->ARTICLE:Lcom/google/android/apps/plus/network/ApiaryActivity$Type;

    return-object v0
.end method

.method protected final update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .registers 7
    .parameter "mediaLayout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    .line 36
    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mDisplayName:Ljava/lang/String;

    .line 37
    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mContent:Ljava/lang/String;

    .line 38
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mImageList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 40
    iget-object v0, p1, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    .line 42
    .local v0, mediaItemList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/MediaItem;>;"
    if-eqz v0, :cond_34

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_34

    .line 43
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaItem;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mImageList:Ljava/util/List;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "https:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    :cond_34
    iget-object v1, p1, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mDisplayName:Ljava/lang/String;

    .line 47
    iget-object v1, p1, Lcom/google/api/services/plusi/model/MediaLayout;->faviconUrl:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mFavIconUrl:Ljava/lang/String;

    .line 48
    iget-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mFavIconUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5b

    .line 49
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mFavIconUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mFavIconUrl:Ljava/lang/String;

    .line 51
    :cond_5b
    iget-object v1, p1, Lcom/google/api/services/plusi/model/MediaLayout;->description:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/network/ApiaryArticleActivity;->mContent:Ljava/lang/String;

    .line 52
    return-void
.end method
