.class public Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;
.super Lcom/google/android/apps/plus/network/ApiaryActivity;
.source "ApiaryPhotoAlbumActivity.java"


# instance fields
.field private mDisplayName:Ljava/lang/String;

.field private mImageList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/plus/network/ApiaryActivity;-><init>()V

    .line 22
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;->mImageList:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public final getDisplayName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public final getImages()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;->mImageList:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getType()Lcom/google/android/apps/plus/network/ApiaryActivity$Type;
    .registers 2

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/apps/plus/network/ApiaryActivity$Type;->PHOTOALBUM:Lcom/google/android/apps/plus/network/ApiaryActivity$Type;

    return-object v0
.end method

.method protected final update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .registers 7
    .parameter "mediaLayout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    .line 33
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;->mDisplayName:Ljava/lang/String;

    .line 34
    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;->mImageList:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->clear()V

    .line 36
    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    .line 38
    .local v2, mediaItemList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/MediaItem;>;"
    if-eqz v2, :cond_15

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1d

    .line 39
    :cond_15
    new-instance v3, Ljava/io/IOException;

    const-string v4, "empty media item"

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 42
    :cond_1d
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :goto_21
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_35

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaItem;

    .line 43
    .local v1, mediaItem:Lcom/google/api/services/plusi/model/MediaItem;
    iget-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;->mImageList:Ljava/util/LinkedList;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_21

    .line 46
    .end local v1           #mediaItem:Lcom/google/api/services/plusi/model/MediaItem;
    :cond_35
    iget-object v3, p1, Lcom/google/api/services/plusi/model/MediaLayout;->title:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/network/ApiaryPhotoAlbumActivity;->mDisplayName:Ljava/lang/String;

    .line 47
    return-void
.end method
