.class public Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;
.super Lcom/google/android/apps/plus/network/ApiaryActivity;
.source "ApiarySkyjamActivity.java"


# instance fields
.field private mAlbumName:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mImage:Ljava/lang/String;

.field private mTrackName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/apps/plus/network/ApiaryActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAlbumName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method public final getArtistName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;->mArtistName:Ljava/lang/String;

    return-object v0
.end method

.method public final getImage()Ljava/lang/String;
    .registers 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;->mImage:Ljava/lang/String;

    return-object v0
.end method

.method public final getTrackName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;->mTrackName:Ljava/lang/String;

    return-object v0
.end method

.method public final getType()Lcom/google/android/apps/plus/network/ApiaryActivity$Type;
    .registers 2

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/apps/plus/network/ApiaryActivity$Type;->AUDIO:Lcom/google/android/apps/plus/network/ApiaryActivity$Type;

    return-object v0
.end method

.method protected final update(Lcom/google/api/services/plusi/model/MediaLayout;)V
    .registers 6
    .parameter "mediaLayout"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/network/ApiaryActivity;->update(Lcom/google/api/services/plusi/model/MediaLayout;)V

    .line 32
    iget-object v2, p1, Lcom/google/api/services/plusi/model/MediaLayout;->media:Ljava/util/List;

    .line 34
    .local v2, mediaItemList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/MediaItem;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, i$:Ljava/util/Iterator;
    :cond_9
    :goto_9
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_36

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/MediaItem;

    .line 35
    .local v1, mediaItem:Lcom/google/api/services/plusi/model/MediaItem;
    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->albumArtistHtml:Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 36
    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->thumbnailUrl:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;->mImage:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->caption:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;->mTrackName:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->albumHtml:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;->mAlbumName:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/MediaItem;->albumArtistHtml:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/network/ApiarySkyjamActivity;->mArtistName:Ljava/lang/String;

    goto :goto_9

    .line 39
    .end local v1           #mediaItem:Lcom/google/api/services/plusi/model/MediaItem;
    :cond_36
    return-void
.end method
