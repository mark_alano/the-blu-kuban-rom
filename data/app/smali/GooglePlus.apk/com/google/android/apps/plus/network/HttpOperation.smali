.class public Lcom/google/android/apps/plus/network/HttpOperation;
.super Ljava/lang/Object;
.source "HttpOperation.java"

# interfaces
.implements Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;,
        Lcom/google/android/apps/plus/network/HttpOperation$SimpleThreadFactory;
    }
.end annotation


# static fields
.field private static final sBufferCache:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<[B>;"
        }
    .end annotation
.end field

.field private static final sExecutorService:Ljava/util/concurrent/ExecutorService;

.field protected static final sHandler:Landroid/os/Handler;

.field private static sSimulateException:Ljava/lang/Exception;

.field private static final sThreadFactory:Ljava/util/concurrent/ThreadFactory;


# instance fields
.field private mAborted:Z

.field protected final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected final mContext:Landroid/content/Context;

.field private volatile mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

.field private mErrorCode:I

.field private mEx:Ljava/lang/Exception;

.field private final mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

.field private final mIntent:Landroid/content/Intent;

.field private final mListener:Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

.field private final mMethod:Ljava/lang/String;

.field private mOutputStream:Ljava/io/OutputStream;

.field private mReasonPhrase:Ljava/lang/String;

.field private mRetriesRemaining:I

.field private mThreaded:Z

.field private final mUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 57
    new-instance v1, Ljava/util/Vector;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/Vector;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    .line 59
    const/4 v0, 0x0

    .local v0, i:I
    :goto_9
    if-gtz v0, :cond_17

    .line 60
    sget-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    const/16 v2, 0x800

    new-array v2, v2, [B

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 65
    :cond_17
    const/4 v1, 0x0

    sput-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sSimulateException:Ljava/lang/Exception;

    .line 68
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sHandler:Landroid/os/Handler;

    .line 92
    new-instance v1, Lcom/google/android/apps/plus/network/HttpOperation$SimpleThreadFactory;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/network/HttpOperation$SimpleThreadFactory;-><init>(B)V

    .line 93
    sput-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sThreadFactory:Ljava/util/concurrent/ThreadFactory;

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/network/HttpOperation;->sExecutorService:Ljava/util/concurrent/ExecutorService;

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 17
    .parameter "context"
    .parameter "method"
    .parameter "url"
    .parameter "account"
    .parameter "outputStream"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 141
    new-instance v4, Lcom/google/android/apps/plus/network/DefaultHttpRequestConfiguration;

    invoke-direct {v4, p1, p4}, Lcom/google/android/apps/plus/network/DefaultHttpRequestConfiguration;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/network/HttpOperation;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 143
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/content/EsAccount;Ljava/io/OutputStream;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 10
    .parameter "context"
    .parameter "method"
    .parameter "url"
    .parameter "config"
    .parameter "account"
    .parameter "outputStream"
    .parameter "intent"
    .parameter "listener"

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    .line 111
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mRetriesRemaining:I

    .line 160
    iput-object p1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mContext:Landroid/content/Context;

    .line 161
    iput-object p2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mMethod:Ljava/lang/String;

    .line 162
    iput-object p3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    .line 163
    iput-object p4, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

    .line 164
    iput-object p5, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 165
    iput-object p6, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mOutputStream:Ljava/io/OutputStream;

    .line 166
    iput-object p7, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mIntent:Landroid/content/Intent;

    .line 167
    iput-object p8, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mListener:Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    .line 168
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/network/HttpOperation;ILjava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/network/HttpOperation;->completeOperation(ILjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method private static appendEncodedQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "url"
    .parameter "name"
    .parameter "value"

    .prologue
    .line 727
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 728
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_25

    const/16 v1, 0x3f

    :goto_10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 729
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 730
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 728
    :cond_25
    const/16 v1, 0x26

    goto :goto_10
.end method

.method protected static appendLocale(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "url"

    .prologue
    .line 734
    const-string v0, "hl"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/network/HttpOperation;->appendEncodedQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static appendSyncParameter(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 4
    .parameter "url"
    .parameter "sync"

    .prologue
    .line 746
    const-string v1, "sync"

    if-eqz p1, :cond_b

    const-string v0, "1"

    :goto_6
    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/network/HttpOperation;->appendEncodedQueryParameter(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_b
    const-string v0, "0"

    goto :goto_6
.end method

.method protected static captureResponse(Ljava/io/InputStream;ILjava/lang/StringBuilder;)Ljava/io/InputStream;
    .registers 6
    .parameter "inputStream"
    .parameter "contentLength"
    .parameter "output"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 698
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 699
    .local v0, baos:Ljava/io/ByteArrayOutputStream;
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/network/HttpOperation;->readFromStream(Ljava/io/InputStream;ILjava/io/OutputStream;)V

    .line 700
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    .line 701
    .local v1, bytes:[B
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 703
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v2
.end method

.method private completeOperation(ILjava/lang/String;Ljava/lang/Exception;)V
    .registers 5
    .parameter "errorCode"
    .parameter "reasonPhrase"
    .parameter "ex"

    .prologue
    .line 460
    iput p1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    .line 461
    iput-object p2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mReasonPhrase:Ljava/lang/String;

    .line 462
    iput-object p3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mListener:Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    if-eqz v0, :cond_f

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mListener:Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;->onOperationComplete(Lcom/google/android/apps/plus/network/HttpOperation;)V

    .line 466
    :cond_f
    return-void
.end method

.method private static readFromStream(Ljava/io/InputStream;ILjava/io/OutputStream;)V
    .registers 11
    .parameter "inputStream"
    .parameter "contentLength"
    .parameter "outputStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/network/NetworkException;,
            Lcom/google/android/apps/plus/network/MemoryException;
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    .line 632
    :try_start_1
    sget-object v5, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/Vector;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_a} :catch_39

    .line 633
    .local v0, buffer:[B
    const/4 v1, 0x1

    .line 641
    .local v1, bufferFromCache:Z
    :goto_b
    if-ne p1, v7, :cond_40

    .line 642
    :goto_d
    const/4 v5, 0x0

    :try_start_e
    array-length v6, v0

    invoke-virtual {p0, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .local v2, bytesRead:I
    if-eq v2, v7, :cond_84

    .line 643
    const/4 v5, 0x0

    invoke-virtual {p2, v0, v5, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_19
    .catchall {:try_start_e .. :try_end_19} :catchall_25
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_19} :catch_1a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_e .. :try_end_19} :catch_64

    goto :goto_d

    .line 664
    .end local v2           #bytesRead:I
    :catch_1a
    move-exception v4

    .line 665
    .local v4, ex:Ljava/io/IOException;
    :try_start_1b
    new-instance v5, Lcom/google/android/apps/plus/network/NetworkException;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_25
    .catchall {:try_start_1b .. :try_end_25} :catchall_25

    .line 669
    .end local v4           #ex:Ljava/io/IOException;
    :catchall_25
    move-exception v5

    if-eqz v1, :cond_2d

    .line 670
    sget-object v6, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    invoke-virtual {v6, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 674
    :cond_2d
    if-eqz p0, :cond_32

    .line 675
    :try_start_2f
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 678
    :cond_32
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    .line 679
    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_38} :catch_97

    .line 682
    :goto_38
    throw v5

    .line 635
    .end local v0           #buffer:[B
    .end local v1           #bufferFromCache:Z
    :catch_39
    move-exception v5

    const/16 v5, 0x800

    new-array v0, v5, [B

    .line 636
    .restart local v0       #buffer:[B
    const/4 v1, 0x0

    .restart local v1       #bufferFromCache:Z
    goto :goto_b

    .line 646
    :cond_40
    move v3, p1

    .line 648
    .local v3, bytesRemaining:I
    :cond_41
    :goto_41
    if-lez v3, :cond_77

    .line 649
    const/4 v5, 0x0

    :try_start_44
    array-length v6, v0

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {p0, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .line 651
    .restart local v2       #bytesRead:I
    if-ne v2, v7, :cond_6f

    .line 652
    new-instance v5, Lcom/google/android/apps/plus/network/NetworkException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid content length: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_64
    .catchall {:try_start_44 .. :try_end_64} :catchall_25
    .catch Ljava/io/IOException; {:try_start_44 .. :try_end_64} :catch_1a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_44 .. :try_end_64} :catch_64

    .line 666
    .end local v2           #bytesRead:I
    .end local v3           #bytesRemaining:I
    :catch_64
    move-exception v4

    .line 667
    .local v4, ex:Ljava/lang/OutOfMemoryError;
    :try_start_65
    new-instance v5, Lcom/google/android/apps/plus/network/MemoryException;

    invoke-virtual {v4}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/network/MemoryException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_6f
    .catchall {:try_start_65 .. :try_end_6f} :catchall_25

    .line 653
    .end local v4           #ex:Ljava/lang/OutOfMemoryError;
    .restart local v2       #bytesRead:I
    .restart local v3       #bytesRemaining:I
    :cond_6f
    if-lez v2, :cond_41

    .line 654
    sub-int/2addr v3, v2

    .line 655
    const/4 v5, 0x0

    :try_start_73
    invoke-virtual {p2, v0, v5, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_41

    .line 660
    .end local v2           #bytesRead:I
    :cond_77
    :goto_77
    const/4 v5, 0x0

    array-length v6, v0

    invoke-virtual {p0, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    .restart local v2       #bytesRead:I
    if-eq v2, v7, :cond_84

    .line 661
    const/4 v5, 0x0

    invoke-virtual {p2, v0, v5, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_83
    .catchall {:try_start_73 .. :try_end_83} :catchall_25
    .catch Ljava/io/IOException; {:try_start_73 .. :try_end_83} :catch_1a
    .catch Ljava/lang/OutOfMemoryError; {:try_start_73 .. :try_end_83} :catch_64

    goto :goto_77

    .line 669
    .end local v3           #bytesRemaining:I
    :cond_84
    if-eqz v1, :cond_8b

    .line 670
    sget-object v5, Lcom/google/android/apps/plus/network/HttpOperation;->sBufferCache:Ljava/util/Vector;

    invoke-virtual {v5, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 674
    :cond_8b
    if-eqz p0, :cond_90

    .line 675
    :try_start_8d
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 678
    :cond_90
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    .line 679
    invoke-virtual {p2}, Ljava/io/OutputStream;->close()V
    :try_end_96
    .catch Ljava/io/IOException; {:try_start_8d .. :try_end_96} :catch_99

    .line 683
    :goto_96
    return-void

    .end local v2           #bytesRead:I
    :catch_97
    move-exception v6

    goto :goto_38

    .restart local v2       #bytesRead:I
    :catch_99
    move-exception v5

    goto :goto_96
.end method


# virtual methods
.method public final abort()V
    .registers 3

    .prologue
    .line 255
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAborted:Z

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    .line 257
    .local v0, httpTx:Lcom/google/android/apps/plus/network/HttpTransaction;
    if-eqz v0, :cond_a

    .line 258
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->abort()V

    .line 260
    :cond_a
    return-void
.end method

.method protected createPostData()Lorg/apache/http/HttpEntity;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 305
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getErrorCode()I
    .registers 2

    .prologue
    .line 500
    iget v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    return v0
.end method

.method public final getException()Ljava/lang/Exception;
    .registers 2

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    return-object v0
.end method

.method public final getIntent()Landroid/content/Intent;
    .registers 2

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    .prologue
    .line 270
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .registers 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mOutputStream:Ljava/io/OutputStream;

    return-object v0
.end method

.method public final getReasonPhrase()Ljava/lang/String;
    .registers 2

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mReasonPhrase:Ljava/lang/String;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .registers 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final hasError()Z
    .registers 3

    .prologue
    .line 521
    iget v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_8

    const/4 v0, 0x1

    :goto_7
    return v0

    :cond_8
    const/4 v0, 0x0

    goto :goto_7
.end method

.method public final isAborted()Z
    .registers 2

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAborted:Z

    return v0
.end method

.method protected isAuthenticationError(Ljava/lang/Exception;)Z
    .registers 4
    .parameter "e"

    .prologue
    .line 442
    instance-of v1, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v1, :cond_e

    move-object v0, p1

    .line 443
    check-cast v0, Lorg/apache/http/client/HttpResponseException;

    .line 444
    .local v0, ex:Lorg/apache/http/client/HttpResponseException;
    invoke-virtual {v0}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v1

    packed-switch v1, :pswitch_data_12

    .line 449
    .end local v0           #ex:Lorg/apache/http/client/HttpResponseException;
    :cond_e
    const/4 v1, 0x0

    :goto_f
    return v1

    .line 446
    .restart local v0       #ex:Lorg/apache/http/client/HttpResponseException;
    :pswitch_10
    const/4 v1, 0x1

    goto :goto_f

    .line 444
    :pswitch_data_12
    .packed-switch 0x191
        :pswitch_10
    .end packed-switch
.end method

.method protected isImmediatelyRetryableError(Ljava/lang/Exception;)Z
    .registers 3
    .parameter "ex"

    .prologue
    .line 433
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->isAuthenticationError(Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method public final logAndThrowExceptionIfFailed(Ljava/lang/String;)V
    .registers 5
    .parameter "tag"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 614
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v0

    if-eqz v0, :cond_9a

    .line 615
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->logError(Ljava/lang/String;)V

    .line 616
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v0

    if-eqz v0, :cond_9a

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    if-eqz v0, :cond_5f

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-ge v0, v1, :cond_40

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " operation failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_40
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " operation failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    invoke-direct {v0, v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_5f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v0

    if-eqz v0, :cond_9a

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " operation failed, error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mReasonPhrase:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 618
    :cond_9a
    return-void
.end method

.method public final logError(Ljava/lang/String;)V
    .registers 4
    .parameter "tag"

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    if-eqz v0, :cond_29

    .line 581
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] failed due to exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mEx:Ljava/lang/Exception;

    invoke-static {p1, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 588
    :cond_28
    :goto_28
    return-void

    .line 582
    :cond_29
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 583
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 584
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] failed due to error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mErrorCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mReasonPhrase:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_28
.end method

.method public onHttpCookie(Lorg/apache/http/cookie/Cookie;)V
    .registers 2
    .parameter "cookie"

    .prologue
    .line 340
    return-void
.end method

.method protected onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V
    .registers 2
    .parameter "inputStream"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 359
    return-void
.end method

.method protected onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .registers 4
    .parameter "errorCode"
    .parameter "reasonPhrase"
    .parameter "ex"

    .prologue
    .line 426
    return-void
.end method

.method public onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V
    .registers 6
    .parameter "inputStream"
    .parameter "contentType"
    .parameter "contentLength"
    .parameter "header"
    .parameter "statusCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 333
    return-void
.end method

.method public onHttpReadFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;)V
    .registers 8
    .parameter "inputStream"
    .parameter "contentType"
    .parameter "contentLength"
    .parameter "header"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mOutputStream:Ljava/io/OutputStream;

    .line 316
    .local v0, out:Ljava/io/OutputStream;
    if-eqz v0, :cond_c

    .line 317
    invoke-static {p1, p3, v0}, Lcom/google/android/apps/plus/network/HttpOperation;->readFromStream(Ljava/io/InputStream;ILjava/io/OutputStream;)V

    .line 318
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V

    .line 324
    :goto_b
    return-void

    .line 319
    :cond_c
    if-eqz p2, :cond_12

    .line 320
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpHandleContentFromStream$6508b088(Ljava/io/InputStream;)V

    goto :goto_b

    .line 322
    :cond_12
    const-string v1, "HttpTransaction"

    const-string v2, "Content type not specified"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b
.end method

.method public final onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    .registers 8
    .parameter "errorCode"
    .parameter "reasonPhrase"
    .parameter "ex"

    .prologue
    .line 367
    invoke-virtual {p0, p3}, Lcom/google/android/apps/plus/network/HttpOperation;->isImmediatelyRetryableError(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_32

    iget v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mRetriesRemaining:I

    if-lez v2, :cond_32

    .line 369
    :try_start_a
    invoke-virtual {p0, p3}, Lcom/google/android/apps/plus/network/HttpOperation;->isAuthenticationError(Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 370
    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

    invoke-interface {v2}, Lcom/google/android/apps/plus/network/HttpRequestConfiguration;->invalidateAuthToken()V

    .line 373
    :cond_15
    const-string v2, "HttpTransaction"

    const-string v3, "====> Restarting operation..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    iget v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mRetriesRemaining:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mRetriesRemaining:I

    .line 375
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V
    :try_end_25
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_25} :catch_26

    .line 402
    :goto_25
    return-void

    .line 379
    :catch_26
    move-exception v0

    .line 380
    .local v0, aex:Ljava/lang/Exception;
    const-string v2, "HttpTransaction"

    const-string v3, "====> Retry failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 382
    move-object p3, v0

    .line 386
    .end local v0           #aex:Ljava/lang/Exception;
    :cond_32
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpOperationComplete(ILjava/lang/String;Ljava/lang/Exception;)V

    .line 388
    move-object v1, p3

    .line 389
    .local v1, fex:Ljava/lang/Exception;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mThreaded:Z

    if-eqz v2, :cond_45

    .line 390
    sget-object v2, Lcom/google/android/apps/plus/network/HttpOperation;->sHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/apps/plus/network/HttpOperation$2;

    invoke-direct {v3, p0, p1, p2, v1}, Lcom/google/android/apps/plus/network/HttpOperation$2;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_25

    .line 400
    :cond_45
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/apps/plus/network/HttpOperation;->completeOperation(ILjava/lang/String;Ljava/lang/Exception;)V

    goto :goto_25
.end method

.method public final onStartResultProcessing()V
    .registers 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    if-eqz v0, :cond_9

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->onStartResultProcessing()V

    .line 246
    :cond_9
    return-void
.end method

.method public final setOutputStream(Ljava/io/OutputStream;)V
    .registers 2
    .parameter "outputStream"

    .prologue
    .line 479
    iput-object p1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mOutputStream:Ljava/io/OutputStream;

    .line 480
    return-void
.end method

.method public final start()V
    .registers 4

    .prologue
    const/4 v2, 0x0

    .line 174
    sget-boolean v1, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v1, :cond_15

    .line 175
    new-instance v0, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v0}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    .line 176
    .local v0, metrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    .line 177
    const-string v1, "HttpTransaction"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    .end local v0           #metrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;
    :goto_14
    return-void

    .line 179
    :cond_15
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/network/HttpOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    goto :goto_14
.end method

.method public final start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V
    .registers 12
    .parameter "syncState"
    .parameter "metrics"

    .prologue
    .line 188
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAborted:Z
    :try_end_2
    .catchall {:try_start_0 .. :try_end_2} :catchall_c9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_2} :catch_95

    if-eqz v1, :cond_10

    .line 236
    if-eqz p1, :cond_f

    if-eqz p2, :cond_f

    .line 237
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->getHttpTransactionMetrics()Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->accumulateFrom(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    .line 240
    :cond_f
    :goto_f
    return-void

    .line 192
    :cond_10
    :try_start_10
    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2f

    .line 193
    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Starting op: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_2f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->createPostData()Lorg/apache/http/HttpEntity;

    move-result-object v4

    .line 198
    .local v4, postData:Lorg/apache/http/HttpEntity;
    if-eqz v4, :cond_75

    .line 199
    new-instance v0, Lcom/google/android/apps/plus/network/HttpTransaction;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mMethod:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/network/HttpTransaction;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;)V

    .line 204
    .local v0, httpOp:Lcom/google/android/apps/plus/network/HttpTransaction;
    :goto_41
    const-string v1, "HttpTransaction"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4d

    .line 205
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->printHeaders()V

    .line 208
    :cond_4d
    if-eqz p2, :cond_56

    invoke-virtual {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onBeginTransaction(Ljava/lang/String;)V

    .line 209
    :cond_56
    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/network/HttpTransaction;->setHttpTransactionMetrics(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    .line 211
    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;
    :try_end_5b
    .catchall {:try_start_10 .. :try_end_5b} :catchall_c9
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_5b} :catch_95

    .line 214
    :try_start_5b
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->execute()Ljava/lang/Exception;

    move-result-object v7

    .line 215
    .local v7, retEx:Ljava/lang/Exception;
    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p2, v7}, Lcom/google/android/apps/plus/content/EsNetworkData;->insertData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;Ljava/lang/Exception;)V
    :try_end_66
    .catchall {:try_start_5b .. :try_end_66} :catchall_90
    .catch Ljava/lang/Exception; {:try_start_5b .. :try_end_66} :catch_81

    .line 223
    const/4 v1, 0x0

    :try_start_67
    iput-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;
    :try_end_69
    .catchall {:try_start_67 .. :try_end_69} :catchall_c9
    .catch Ljava/lang/Exception; {:try_start_67 .. :try_end_69} :catch_95

    .line 226
    .end local v7           #retEx:Ljava/lang/Exception;
    :goto_69
    if-eqz p1, :cond_f

    if-eqz p2, :cond_f

    .line 237
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->getHttpTransactionMetrics()Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->accumulateFrom(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    goto :goto_f

    .line 201
    .end local v0           #httpOp:Lcom/google/android/apps/plus/network/HttpTransaction;
    :cond_75
    :try_start_75
    new-instance v0, Lcom/google/android/apps/plus/network/HttpTransaction;

    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mMethod:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mHttpRequestConfig:Lcom/google/android/apps/plus/network/HttpRequestConfiguration;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/network/HttpTransaction;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;)V
    :try_end_80
    .catchall {:try_start_75 .. :try_end_80} :catchall_c9
    .catch Ljava/lang/Exception; {:try_start_75 .. :try_end_80} :catch_95

    .restart local v0       #httpOp:Lcom/google/android/apps/plus/network/HttpTransaction;
    goto :goto_41

    .line 216
    :catch_81
    move-exception v8

    .line 217
    .local v8, t:Ljava/lang/Exception;
    :try_start_82
    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2, p2, v8}, Lcom/google/android/apps/plus/content/EsNetworkData;->insertData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;Ljava/lang/Exception;)V

    .line 219
    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->isAborted()Z

    move-result v1

    if-nez v1, :cond_c5

    .line 220
    throw v8
    :try_end_90
    .catchall {:try_start_82 .. :try_end_90} :catchall_90

    .line 223
    .end local v8           #t:Ljava/lang/Exception;
    :catchall_90
    move-exception v1

    const/4 v2, 0x0

    :try_start_92
    iput-object v2, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    throw v1
    :try_end_95
    .catchall {:try_start_92 .. :try_end_95} :catchall_c9
    .catch Ljava/lang/Exception; {:try_start_92 .. :try_end_95} :catch_95

    .line 229
    .end local v0           #httpOp:Lcom/google/android/apps/plus/network/HttpTransaction;
    .end local v4           #postData:Lorg/apache/http/HttpEntity;
    :catch_95
    move-exception v6

    .line 230
    .local v6, ex:Ljava/lang/Exception;
    :try_start_96
    const-string v1, "HttpTransaction"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_b3

    .line 231
    const-string v1, "HttpTransaction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HttpOperation failed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 234
    :cond_b3
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v6}, Lcom/google/android/apps/plus/network/HttpOperation;->onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_b8
    .catchall {:try_start_96 .. :try_end_b8} :catchall_c9

    .line 236
    if-eqz p1, :cond_f

    if-eqz p2, :cond_f

    .line 237
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->getHttpTransactionMetrics()Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->accumulateFrom(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    goto/16 :goto_f

    .line 223
    .end local v6           #ex:Ljava/lang/Exception;
    .restart local v0       #httpOp:Lcom/google/android/apps/plus/network/HttpTransaction;
    .restart local v4       #postData:Lorg/apache/http/HttpEntity;
    .restart local v8       #t:Ljava/lang/Exception;
    :cond_c5
    const/4 v1, 0x0

    :try_start_c6
    iput-object v1, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mCurrentTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;
    :try_end_c8
    .catchall {:try_start_c6 .. :try_end_c8} :catchall_c9
    .catch Ljava/lang/Exception; {:try_start_c6 .. :try_end_c8} :catch_95

    goto :goto_69

    .line 236
    .end local v0           #httpOp:Lcom/google/android/apps/plus/network/HttpTransaction;
    .end local v4           #postData:Lorg/apache/http/HttpEntity;
    .end local v8           #t:Ljava/lang/Exception;
    :catchall_c9
    move-exception v1

    if-eqz p1, :cond_d5

    if-eqz p2, :cond_d5

    .line 237
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->getHttpTransactionMetrics()Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->accumulateFrom(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    :cond_d5
    throw v1
.end method

.method public final startThreaded()V
    .registers 3

    .prologue
    .line 287
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/network/HttpOperation;->mThreaded:Z

    .line 288
    sget-object v0, Lcom/google/android/apps/plus/network/HttpOperation;->sExecutorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/apps/plus/network/HttpOperation$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/network/HttpOperation$1;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 297
    return-void
.end method
