.class final Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;
.super Ljava/io/FilterOutputStream;
.source "HttpTransaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/network/HttpTransaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CountingOutputStream"
.end annotation


# instance fields
.field private final mChunk:J

.field private final mLength:J

.field private mNext:J

.field private final mTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

.field private mTransferred:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/network/HttpTransaction;Ljava/io/OutputStream;J)V
    .registers 9
    .parameter "transaction"
    .parameter "out"
    .parameter "length"

    .prologue
    .line 192
    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 193
    iput-object p1, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    .line 194
    const-wide/16 v0, 0x2

    mul-long/2addr v0, p3

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mLength:J

    .line 195
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    .line 196
    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mLength:J

    const-wide/16 v2, 0x5

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mChunk:J

    .line 197
    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mChunk:J

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    .line 198
    return-void
.end method


# virtual methods
.method public final write(I)V
    .registers 6
    .parameter "b"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 219
    invoke-super {p0, p1}, Ljava/io/FilterOutputStream;->write(I)V

    .line 220
    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    .line 221
    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_25

    .line 222
    invoke-super {p0}, Ljava/io/FilterOutputStream;->flush()V

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    #getter for: Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->access$000(Lcom/google/android/apps/plus/network/HttpTransaction;)Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mLength:J

    .line 224
    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mChunk:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    .line 226
    :cond_25
    return-void
.end method

.method public final write([BII)V
    .registers 8
    .parameter "b"
    .parameter "off"
    .parameter "len"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 205
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterOutputStream;->write([BII)V

    .line 206
    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    .line 207
    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_24

    .line 208
    invoke-super {p0}, Ljava/io/FilterOutputStream;->flush()V

    .line 209
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    #getter for: Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->access$000(Lcom/google/android/apps/plus/network/HttpTransaction;)Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mLength:J

    .line 210
    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mChunk:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    .line 212
    :cond_24
    return-void
.end method
