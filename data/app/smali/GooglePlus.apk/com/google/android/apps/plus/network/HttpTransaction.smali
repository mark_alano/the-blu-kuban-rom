.class final Lcom/google/android/apps/plus/network/HttpTransaction;
.super Ljava/lang/Object;
.source "HttpTransaction.java"

# interfaces
.implements Lorg/apache/http/HttpRequestInterceptor;
.implements Lorg/apache/http/HttpResponseInterceptor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;,
        Lcom/google/android/apps/plus/network/HttpTransaction$MyInputStreamEntity;,
        Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    }
.end annotation


# static fields
.field private static final sHttpParams:Lorg/apache/http/params/HttpParams;

.field private static final sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;


# instance fields
.field private mAborted:Z

.field private final mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

.field private final mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

.field private mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .prologue
    .line 81
    new-instance v0, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;

    .line 82
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 86
    sput-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sHttpParams:Lorg/apache/http/params/HttpParams;

    const-string v1, "http.socket.timeout"

    const v2, 0x15f90

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 87
    sget-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sHttpParams:Lorg/apache/http/params/HttpParams;

    const-string v1, "http.connection.timeout"

    const/16 v2, 0xbb8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 92
    sget-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;

    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v3

    const/16 v4, 0x50

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 93
    sget-object v0, Lcom/google/android/apps/plus/network/HttpTransaction;->sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;

    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v3

    const/16 v4, 0x1bb

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 94
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;)V
    .registers 8
    .parameter "method"
    .parameter "url"
    .parameter "config"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    const-string v0, "GET"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 240
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 249
    :goto_12
    if-nez p4, :cond_51

    .line 250
    new-instance v0, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "The listener cannot be null"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241
    :cond_1c
    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 242
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    goto :goto_12

    .line 243
    :cond_2c
    const-string v0, "DELETE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 244
    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    goto :goto_12

    .line 246
    :cond_3c
    new-instance v0, Lcom/google/android/apps/plus/network/NetworkException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported method: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_51
    iput-object p4, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {p3, v0}, Lcom/google/android/apps/plus/network/HttpRequestConfiguration;->addHeaders(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 256
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;Lorg/apache/http/HttpEntity;Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;)V
    .registers 9
    .parameter "method"
    .parameter "url"
    .parameter "config"
    .parameter "postData"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    const-string v0, "POST"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 285
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    .line 292
    :goto_12
    if-nez p5, :cond_41

    .line 293
    new-instance v0, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v1, "The listener cannot be null"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_1c
    const-string v0, "PUT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 287
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    goto :goto_12

    .line 289
    :cond_2c
    new-instance v0, Lcom/google/android/apps/plus/network/NetworkException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported method: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 296
    :cond_41
    iput-object p5, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    .line 298
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {p3, v0}, Lcom/google/android/apps/plus/network/HttpRequestConfiguration;->addHeaders(Lorg/apache/http/client/methods/HttpRequestBase;)V

    .line 300
    if-eqz p4, :cond_56

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    check-cast v0, Lorg/apache/http/client/methods/HttpPost;

    new-instance v1, Lcom/google/android/apps/plus/network/HttpTransaction$MyInputStreamEntity;

    invoke-direct {v1, p0, p4}, Lcom/google/android/apps/plus/network/HttpTransaction$MyInputStreamEntity;-><init>(Lcom/google/android/apps/plus/network/HttpTransaction;Lorg/apache/http/HttpEntity;)V

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 303
    :cond_56
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/network/HttpTransaction;)Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    .registers 2
    .parameter "x0"

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    return-object v0
.end method

.method private processCookies(Lorg/apache/http/HeaderIterator;Lorg/apache/http/cookie/CookieSpec;Lorg/apache/http/cookie/CookieOrigin;)V
    .registers 11
    .parameter "iterator"
    .parameter "cookieSpec"
    .parameter "cookieOrigin"

    .prologue
    .line 571
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/apache/http/HeaderIterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2d

    .line 572
    invoke-interface {p1}, Lorg/apache/http/HeaderIterator;->nextHeader()Lorg/apache/http/Header;

    move-result-object v3

    .line 574
    .local v3, header:Lorg/apache/http/Header;
    :try_start_a
    invoke-interface {p2, v3, p3}, Lorg/apache/http/cookie/CookieSpec;->parse(Lorg/apache/http/Header;Lorg/apache/http/cookie/CookieOrigin;)Ljava/util/List;

    move-result-object v1

    .line 575
    .local v1, cookies:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, i$:Ljava/util/Iterator;
    :goto_12
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    .line 576
    .local v0, cookie:Lorg/apache/http/cookie/Cookie;
    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    invoke-interface {v5, v0}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpCookie(Lorg/apache/http/cookie/Cookie;)V
    :try_end_23
    .catch Lorg/apache/http/cookie/MalformedCookieException; {:try_start_a .. :try_end_23} :catch_24

    goto :goto_12

    .line 578
    .end local v0           #cookie:Lorg/apache/http/cookie/Cookie;
    .end local v1           #cookies:Ljava/util/List;,"Ljava/util/List<Lorg/apache/http/cookie/Cookie;>;"
    .end local v4           #i$:Ljava/util/Iterator;
    :catch_24
    move-exception v2

    .line 579
    .local v2, ex:Lorg/apache/http/cookie/MalformedCookieException;
    const-string v5, "HttpTransaction"

    const-string v6, "Malformed cookie"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 582
    .end local v2           #ex:Lorg/apache/http/cookie/MalformedCookieException;
    .end local v3           #header:Lorg/apache/http/Header;
    :cond_2d
    return-void
.end method

.method private readFromHttpStream(Lorg/apache/http/HttpEntity;[Lorg/apache/http/Header;I)V
    .registers 14
    .parameter "entity"
    .parameter "header"
    .parameter "statusCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 468
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v0

    if-eqz v0, :cond_7d

    .line 469
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentEncoding()Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 474
    .local v6, contentEncoding:Ljava/lang/String;
    :goto_e
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 475
    .local v2, contentType:Ljava/lang/String;
    const/16 v0, 0x3b

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    .line 476
    .local v9, semicolonIndex:I
    const/4 v0, -0x1

    if-eq v9, v0, :cond_24

    .line 477
    const/4 v0, 0x0

    invoke-virtual {v2, v0, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 480
    :cond_24
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v4

    long-to-int v3, v4

    .line 481
    .local v3, contentLength:I
    const-string v0, "HttpTransaction"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 482
    const-string v0, "HttpTransaction"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "readFromHttpStream: Encoding: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", length: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    :cond_5a
    const/4 v1, 0x0

    .line 488
    .local v1, inputStream:Ljava/io/InputStream;
    :try_start_5b
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 491
    if-eqz v6, :cond_70

    const-string v0, "gzip"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 492
    new-instance v8, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v8, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_5b .. :try_end_6e} :catch_7f

    .line 493
    .end local v1           #inputStream:Ljava/io/InputStream;
    .local v8, inputStream:Ljava/io/InputStream;
    const/4 v3, -0x1

    move-object v1, v8

    .line 505
    .end local v8           #inputStream:Ljava/io/InputStream;
    .restart local v1       #inputStream:Ljava/io/InputStream;
    :cond_70
    const/16 v0, 0xc8

    if-ne p3, v0, :cond_8d

    .line 506
    :try_start_74
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpReadFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;)V
    :try_end_79
    .catchall {:try_start_74 .. :try_end_79} :catchall_95

    .line 513
    :goto_79
    :try_start_79
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_7c
    .catch Ljava/io/IOException; {:try_start_79 .. :try_end_7c} :catch_9c

    .line 517
    :goto_7c
    return-void

    .line 471
    .end local v1           #inputStream:Ljava/io/InputStream;
    .end local v2           #contentType:Ljava/lang/String;
    .end local v3           #contentLength:I
    .end local v6           #contentEncoding:Ljava/lang/String;
    .end local v9           #semicolonIndex:I
    :cond_7d
    const/4 v6, 0x0

    .restart local v6       #contentEncoding:Ljava/lang/String;
    goto :goto_e

    .line 495
    .restart local v1       #inputStream:Ljava/io/InputStream;
    .restart local v2       #contentType:Ljava/lang/String;
    .restart local v3       #contentLength:I
    .restart local v9       #semicolonIndex:I
    :catch_7f
    move-exception v7

    .line 497
    .local v7, ex:Ljava/io/IOException;
    :try_start_80
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_83
    .catch Ljava/io/IOException; {:try_start_80 .. :try_end_83} :catch_9a

    .line 501
    :goto_83
    new-instance v0, Lcom/google/android/apps/plus/network/NetworkException;

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 508
    .end local v7           #ex:Ljava/io/IOException;
    :cond_8d
    :try_start_8d
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    move-object v4, p2

    move v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpReadErrorFromStream(Ljava/io/InputStream;Ljava/lang/String;I[Lorg/apache/http/Header;I)V
    :try_end_94
    .catchall {:try_start_8d .. :try_end_94} :catchall_95

    goto :goto_79

    .line 512
    :catchall_95
    move-exception v0

    .line 513
    :try_start_96
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_99
    .catch Ljava/io/IOException; {:try_start_96 .. :try_end_99} :catch_9e

    .line 516
    :goto_99
    throw v0

    .restart local v7       #ex:Ljava/io/IOException;
    :catch_9a
    move-exception v0

    goto :goto_83

    .line 517
    .end local v7           #ex:Ljava/io/IOException;
    :catch_9c
    move-exception v0

    goto :goto_7c

    :catch_9e
    move-exception v4

    goto :goto_99
.end method


# virtual methods
.method public final abort()V
    .registers 4

    .prologue
    .line 585
    iget-boolean v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v1, :cond_5

    .line 609
    :cond_4
    :goto_4
    return-void

    .line 589
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    .line 590
    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    if-eqz v1, :cond_4

    .line 591
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_29

    .line 598
    invoke-static {}, Landroid/os/StrictMode;->getThreadPolicy()Landroid/os/StrictMode$ThreadPolicy;

    move-result-object v0

    .line 599
    .local v0, policy:Landroid/os/StrictMode$ThreadPolicy;
    sget-object v1, Landroid/os/StrictMode$ThreadPolicy;->LAX:Landroid/os/StrictMode$ThreadPolicy;

    invoke-static {v1}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    .line 601
    :try_start_1b
    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V
    :try_end_20
    .catchall {:try_start_1b .. :try_end_20} :catchall_24

    .line 603
    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    goto :goto_4

    :catchall_24
    move-exception v1

    invoke-static {v0}, Landroid/os/StrictMode;->setThreadPolicy(Landroid/os/StrictMode$ThreadPolicy;)V

    throw v1

    .line 605
    .end local v0           #policy:Landroid/os/StrictMode$ThreadPolicy;
    :cond_29
    iget-object v1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v1}, Lorg/apache/http/client/methods/HttpRequestBase;->abort()V

    goto :goto_4
.end method

.method public final execute()Ljava/lang/Exception;
    .registers 15

    .prologue
    const/4 v10, 0x0

    .line 313
    const/4 v2, 0x0

    .line 315
    .local v2, httpClient:Lorg/apache/http/client/HttpClient;
    :try_start_2
    iget-boolean v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v11, :cond_17

    .line 316
    new-instance v1, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v10, "Canceled"

    invoke-direct {v1, v10}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_d
    .catchall {:try_start_2 .. :try_end_d} :catchall_2a4
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_2 .. :try_end_d} :catch_b3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_d} :catch_19a

    .line 424
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v10, :cond_16

    .line 425
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 432
    :cond_16
    :goto_16
    return-object v1

    .line 319
    :cond_17
    const/4 v0, 0x0

    .line 320
    .local v0, connectionException:Ljava/lang/Exception;
    const/4 v6, 0x0

    .line 321
    .local v6, resp:Lorg/apache/http/HttpResponse;
    const/4 v4, 0x0

    .local v4, i:I
    move-object v3, v2

    .end local v2           #httpClient:Lorg/apache/http/client/HttpClient;
    .local v3, httpClient:Lorg/apache/http/client/HttpClient;
    :goto_1b
    const/4 v11, 0x2

    if-ge v4, v11, :cond_2c3

    .line 322
    const/4 v0, 0x0

    .line 323
    :try_start_1f
    new-instance v11, Lorg/apache/http/impl/conn/SingleClientConnManager;

    sget-object v12, Lcom/google/android/apps/plus/network/HttpTransaction;->sHttpParams:Lorg/apache/http/params/HttpParams;

    sget-object v13, Lcom/google/android/apps/plus/network/HttpTransaction;->sSupportedSchemes:Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v11, v12, v13}, Lorg/apache/http/impl/conn/SingleClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    new-instance v2, Lorg/apache/http/impl/client/DefaultHttpClient;

    sget-object v12, Lcom/google/android/apps/plus/network/HttpTransaction;->sHttpParams:Lorg/apache/http/params/HttpParams;

    invoke-direct {v2, v11, v12}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    const-class v11, Lorg/apache/http/client/protocol/RequestAddCookies;

    invoke-virtual {v2, v11}, Lorg/apache/http/impl/client/DefaultHttpClient;->removeRequestInterceptorByClass(Ljava/lang/Class;)V

    const-class v11, Lorg/apache/http/client/protocol/ResponseProcessCookies;

    invoke-virtual {v2, v11}, Lorg/apache/http/impl/client/DefaultHttpClient;->removeResponseInterceptorByClass(Ljava/lang/Class;)V

    invoke-virtual {v2, p0}, Lorg/apache/http/impl/client/DefaultHttpClient;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    invoke-virtual {v2, p0}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V
    :try_end_3f
    .catchall {:try_start_1f .. :try_end_3f} :catchall_2b8
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_1f .. :try_end_3f} :catch_2bf
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_3f} :catch_2bb

    .line 326
    .end local v3           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v2       #httpClient:Lorg/apache/http/client/HttpClient;
    :try_start_3f
    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-interface {v2, v11}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 328
    iget-boolean v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v11, :cond_b0

    .line 329
    new-instance v1, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v11, "Canceled"

    invoke-direct {v1, v11}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_50
    .catchall {:try_start_3f .. :try_end_50} :catchall_2a4
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_3f .. :try_end_50} :catch_63
    .catch Ljavax/net/ssl/SSLException; {:try_start_3f .. :try_end_50} :catch_91
    .catch Ljava/net/UnknownHostException; {:try_start_3f .. :try_end_50} :catch_94
    .catch Ljava/net/SocketException; {:try_start_3f .. :try_end_50} :catch_97
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_50} :catch_9a
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_3f .. :try_end_50} :catch_b3

    .line 424
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v10, :cond_59

    .line 425
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 427
    :cond_59
    if-eqz v2, :cond_16

    .line 428
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto :goto_16

    .line 334
    :catch_63
    move-exception v1

    .line 335
    .local v1, ex:Lorg/apache/http/conn/ConnectTimeoutException;
    move-object v0, v1

    .line 348
    .end local v1           #ex:Lorg/apache/http/conn/ConnectTimeoutException;
    :goto_65
    :try_start_65
    const-string v11, "HttpTransaction"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_86

    .line 349
    const-string v11, "HttpTransaction"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Connection failure: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :cond_86
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 321
    add-int/lit8 v4, v4, 0x1

    move-object v3, v2

    .end local v2           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v3       #httpClient:Lorg/apache/http/client/HttpClient;
    goto :goto_1b

    .line 336
    .end local v3           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v2       #httpClient:Lorg/apache/http/client/HttpClient;
    :catch_91
    move-exception v1

    .line 337
    .local v1, ex:Ljavax/net/ssl/SSLException;
    move-object v0, v1

    .line 346
    goto :goto_65

    .line 338
    .end local v1           #ex:Ljavax/net/ssl/SSLException;
    :catch_94
    move-exception v1

    .line 339
    .local v1, ex:Ljava/net/UnknownHostException;
    move-object v0, v1

    .line 346
    goto :goto_65

    .line 340
    .end local v1           #ex:Ljava/net/UnknownHostException;
    :catch_97
    move-exception v1

    .line 341
    .local v1, ex:Ljava/net/SocketException;
    move-object v0, v1

    .line 346
    goto :goto_65

    .line 342
    .end local v1           #ex:Ljava/net/SocketException;
    :catch_9a
    move-exception v1

    .line 343
    .local v1, ex:Ljava/lang/Exception;
    move-object v0, v1

    .line 344
    const-string v11, "HttpTransaction"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Non retryable error: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    .end local v1           #ex:Ljava/lang/Exception;
    :cond_b0
    :goto_b0
    if-eqz v0, :cond_d3

    .line 357
    throw v0
    :try_end_b3
    .catchall {:try_start_65 .. :try_end_b3} :catchall_2a4
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_65 .. :try_end_b3} :catch_b3
    .catch Ljava/lang/Exception; {:try_start_65 .. :try_end_b3} :catch_19a

    .line 394
    .end local v0           #connectionException:Ljava/lang/Exception;
    .end local v4           #i:I
    .end local v6           #resp:Lorg/apache/http/HttpResponse;
    :catch_b3
    move-exception v1

    .line 395
    .local v1, ex:Lorg/apache/http/client/HttpResponseException;
    :goto_b4
    :try_start_b4
    iget-boolean v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v10, :cond_1d4

    .line 396
    new-instance v1, Lcom/google/android/apps/plus/network/NetworkException;

    .end local v1           #ex:Lorg/apache/http/client/HttpResponseException;
    const-string v10, "Canceled"

    invoke-direct {v1, v10}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_bf
    .catchall {:try_start_b4 .. :try_end_bf} :catchall_2a4

    .line 424
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v10, :cond_c8

    .line 425
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 427
    :cond_c8
    if-eqz v2, :cond_16

    .line 428
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_16

    .line 360
    .restart local v0       #connectionException:Ljava/lang/Exception;
    .restart local v4       #i:I
    .restart local v6       #resp:Lorg/apache/http/HttpResponse;
    :cond_d3
    :try_start_d3
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v8

    .line 361
    .local v8, statusCode:I
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v9

    .line 362
    .local v9, statusPhrase:Ljava/lang/String;
    iget-boolean v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v11, :cond_102

    .line 363
    new-instance v1, Lcom/google/android/apps/plus/network/NetworkException;

    const-string v10, "Canceled"

    invoke-direct {v1, v10}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_ee
    .catchall {:try_start_d3 .. :try_end_ee} :catchall_2a4
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_d3 .. :try_end_ee} :catch_b3
    .catch Ljava/lang/Exception; {:try_start_d3 .. :try_end_ee} :catch_19a

    .line 424
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v10, :cond_f7

    .line 425
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 427
    :cond_f7
    if-eqz v2, :cond_16

    .line 428
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_16

    .line 366
    :cond_102
    const/4 v1, 0x0

    .line 367
    .local v1, ex:Ljava/lang/Exception;
    const/16 v11, 0xc8

    if-ne v8, v11, :cond_131

    .line 368
    :try_start_107
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v11

    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v12

    invoke-direct {p0, v11, v12, v8}, Lcom/google/android/apps/plus/network/HttpTransaction;->readFromHttpStream(Lorg/apache/http/HttpEntity;[Lorg/apache/http/Header;I)V

    .line 389
    :cond_112
    :goto_112
    iget-boolean v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v11, :cond_1ba

    .line 390
    new-instance v1, Lcom/google/android/apps/plus/network/NetworkException;

    .end local v1           #ex:Ljava/lang/Exception;
    const-string v10, "Canceled"

    invoke-direct {v1, v10}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_11d
    .catchall {:try_start_107 .. :try_end_11d} :catchall_2a4
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_107 .. :try_end_11d} :catch_b3
    .catch Ljava/lang/Exception; {:try_start_107 .. :try_end_11d} :catch_19a

    .line 424
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v10, :cond_126

    .line 425
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 427
    :cond_126
    if-eqz v2, :cond_16

    .line 428
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_16

    .line 370
    .restart local v1       #ex:Ljava/lang/Exception;
    :cond_131
    :try_start_131
    const-string v11, "HttpTransaction"

    const/4 v12, 0x3

    invoke-static {v11, v12}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_16e

    .line 371
    const-string v11, "HttpTransaction"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Error: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v13}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_16e
    .catchall {:try_start_131 .. :try_end_16e} :catchall_2a4
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_131 .. :try_end_16e} :catch_b3
    .catch Ljava/lang/Exception; {:try_start_131 .. :try_end_16e} :catch_19a

    .line 375
    :cond_16e
    :try_start_16e
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v11

    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v12

    invoke-direct {p0, v11, v12, v8}, Lcom/google/android/apps/plus/network/HttpTransaction;->readFromHttpStream(Lorg/apache/http/HttpEntity;[Lorg/apache/http/Header;I)V
    :try_end_179
    .catchall {:try_start_16e .. :try_end_179} :catchall_2a4
    .catch Lcom/google/android/apps/plus/api/OzServerException; {:try_start_16e .. :try_end_179} :catch_181
    .catch Ljava/lang/Exception; {:try_start_16e .. :try_end_179} :catch_184
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_16e .. :try_end_179} :catch_b3

    .line 384
    :goto_179
    if-nez v1, :cond_112

    .line 385
    :try_start_17b
    new-instance v1, Lorg/apache/http/client/HttpResponseException;

    .end local v1           #ex:Ljava/lang/Exception;
    invoke-direct {v1, v8, v9}, Lorg/apache/http/client/HttpResponseException;-><init>(ILjava/lang/String;)V

    .restart local v1       #ex:Ljava/lang/Exception;
    goto :goto_112

    .line 376
    :catch_181
    move-exception v7

    .line 379
    .local v7, serverException:Lcom/google/android/apps/plus/api/OzServerException;
    move-object v1, v7

    .line 383
    goto :goto_179

    .line 380
    .end local v7           #serverException:Lcom/google/android/apps/plus/api/OzServerException;
    :catch_184
    move-exception v5

    .line 382
    .local v5, ignore:Ljava/lang/Exception;
    const-string v11, "HttpTransaction"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Failed to read error response: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_199
    .catchall {:try_start_17b .. :try_end_199} :catchall_2a4
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_17b .. :try_end_199} :catch_b3
    .catch Ljava/lang/Exception; {:try_start_17b .. :try_end_199} :catch_19a

    goto :goto_179

    .line 407
    .end local v0           #connectionException:Ljava/lang/Exception;
    .end local v1           #ex:Ljava/lang/Exception;
    .end local v4           #i:I
    .end local v5           #ignore:Ljava/lang/Exception;
    .end local v6           #resp:Lorg/apache/http/HttpResponse;
    .end local v8           #statusCode:I
    .end local v9           #statusPhrase:Ljava/lang/String;
    :catch_19a
    move-exception v1

    .line 408
    .restart local v1       #ex:Ljava/lang/Exception;
    :goto_19b
    :try_start_19b
    iget-boolean v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v10, :cond_237

    .line 409
    new-instance v1, Lcom/google/android/apps/plus/network/NetworkException;

    .end local v1           #ex:Ljava/lang/Exception;
    const-string v10, "Canceled"

    invoke-direct {v1, v10}, Lcom/google/android/apps/plus/network/NetworkException;-><init>(Ljava/lang/String;)V
    :try_end_1a6
    .catchall {:try_start_19b .. :try_end_1a6} :catchall_2a4

    .line 424
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v10, :cond_1af

    .line 425
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 427
    :cond_1af
    if-eqz v2, :cond_16

    .line 428
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_16

    .line 393
    .restart local v0       #connectionException:Ljava/lang/Exception;
    .restart local v1       #ex:Ljava/lang/Exception;
    .restart local v4       #i:I
    .restart local v6       #resp:Lorg/apache/http/HttpResponse;
    .restart local v8       #statusCode:I
    .restart local v9       #statusPhrase:Ljava/lang/String;
    :cond_1ba
    :try_start_1ba
    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    invoke-interface {v11, v8, v9, v1}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_1bf
    .catchall {:try_start_1ba .. :try_end_1bf} :catchall_2a4
    .catch Lorg/apache/http/client/HttpResponseException; {:try_start_1ba .. :try_end_1bf} :catch_b3
    .catch Ljava/lang/Exception; {:try_start_1ba .. :try_end_1bf} :catch_19a

    .line 424
    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v11, :cond_1c8

    .line 425
    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 427
    :cond_1c8
    if-eqz v2, :cond_1d1

    .line 428
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    :cond_1d1
    move-object v1, v10

    .line 432
    goto/16 :goto_16

    .line 399
    .end local v0           #connectionException:Ljava/lang/Exception;
    .end local v4           #i:I
    .end local v6           #resp:Lorg/apache/http/HttpResponse;
    .end local v8           #statusCode:I
    .end local v9           #statusPhrase:Ljava/lang/String;
    .local v1, ex:Lorg/apache/http/client/HttpResponseException;
    :cond_1d4
    :try_start_1d4
    const-string v10, "HttpTransaction"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_216

    .line 400
    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->printStackTrace()V

    .line 401
    const-string v10, "HttpTransaction"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "HttpResponseException: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v12}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_216
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getStatusCode()I

    move-result v11

    invoke-virtual {v1}, Lorg/apache/http/client/HttpResponseException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v10, v11, v12, v1}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_223
    .catchall {:try_start_1d4 .. :try_end_223} :catchall_2a4

    .line 424
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v10, :cond_22c

    .line 425
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 427
    :cond_22c
    if-eqz v2, :cond_16

    .line 428
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_16

    .line 413
    .local v1, ex:Ljava/lang/Exception;
    :cond_237
    :try_start_237
    instance-of v10, v1, Ljava/lang/RuntimeException;

    if-eqz v10, :cond_25d

    .line 414
    const-string v10, "HttpTransaction"

    const-string v11, "ERROR"

    invoke-static {v10, v11, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 421
    :cond_242
    :goto_242
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-interface {v10, v11, v12, v1}, Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;->onHttpTransactionComplete(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_249
    .catchall {:try_start_237 .. :try_end_249} :catchall_2a4

    .line 424
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v10, :cond_252

    .line 425
    iget-object v10, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 427
    :cond_252
    if-eqz v2, :cond_16

    .line 428
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    goto/16 :goto_16

    .line 415
    :cond_25d
    :try_start_25d
    const-string v10, "HttpTransaction"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_242

    .line 416
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 417
    const-string v10, "HttpTransaction"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v12}, Lorg/apache/http/client/methods/HttpRequestBase;->getURI()Ljava/net/URI;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2a3
    .catchall {:try_start_25d .. :try_end_2a3} :catchall_2a4

    goto :goto_242

    .line 424
    .end local v1           #ex:Ljava/lang/Exception;
    :catchall_2a4
    move-exception v10

    :goto_2a5
    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v11, :cond_2ae

    .line 425
    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    .line 427
    :cond_2ae
    if-eqz v2, :cond_2b7

    .line 428
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    :cond_2b7
    throw v10

    .line 424
    .end local v2           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v0       #connectionException:Ljava/lang/Exception;
    .restart local v3       #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v4       #i:I
    .restart local v6       #resp:Lorg/apache/http/HttpResponse;
    :catchall_2b8
    move-exception v10

    move-object v2, v3

    .end local v3           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v2       #httpClient:Lorg/apache/http/client/HttpClient;
    goto :goto_2a5

    .line 407
    .end local v2           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v3       #httpClient:Lorg/apache/http/client/HttpClient;
    :catch_2bb
    move-exception v1

    move-object v2, v3

    .end local v3           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v2       #httpClient:Lorg/apache/http/client/HttpClient;
    goto/16 :goto_19b

    .line 394
    .end local v2           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v3       #httpClient:Lorg/apache/http/client/HttpClient;
    :catch_2bf
    move-exception v1

    move-object v2, v3

    .end local v3           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v2       #httpClient:Lorg/apache/http/client/HttpClient;
    goto/16 :goto_b4

    .end local v2           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v3       #httpClient:Lorg/apache/http/client/HttpClient;
    :cond_2c3
    move-object v2, v3

    .end local v3           #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v2       #httpClient:Lorg/apache/http/client/HttpClient;
    goto/16 :goto_b0
.end method

.method public final isAborted()Z
    .registers 2

    .prologue
    .line 612
    iget-boolean v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mAborted:Z

    if-eqz v0, :cond_6

    .line 613
    const/4 v0, 0x1

    .line 620
    :goto_5
    return v0

    .line 616
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    if-eqz v0, :cond_11

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v0}, Lorg/apache/http/client/methods/HttpRequestBase;->isAborted()Z

    move-result v0

    goto :goto_5

    .line 620
    :cond_11
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public final onStartResultProcessing()V
    .registers 2

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v0, :cond_9

    .line 625
    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onStartResultProcessing()V

    .line 627
    :cond_9
    return-void
.end method

.method final printHeaders()V
    .registers 8

    .prologue
    .line 259
    const-string v5, "HttpTransaction"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_46

    .line 260
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "HTTP headers:\n"

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 261
    .local v1, builder:Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mHttpMethod:Lorg/apache/http/client/methods/HttpRequestBase;

    invoke-virtual {v5}, Lorg/apache/http/client/methods/HttpRequestBase;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    .local v0, arr$:[Lorg/apache/http/Header;
    array-length v4, v0

    .local v4, len$:I
    const/4 v3, 0x0

    .local v3, i$:I
    :goto_18
    if-ge v3, v4, :cond_3d

    aget-object v2, v0, v3

    .line 262
    .local v2, header:Lorg/apache/http/Header;
    const-string v5, "Authorization"

    invoke-interface {v2}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_35

    .line 263
    const-string v5, "Authorization: <removed>"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :goto_2d
    const-string v5, "\n"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    add-int/lit8 v3, v3, 0x1

    goto :goto_18

    .line 265
    :cond_35
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2d

    .line 269
    .end local v2           #header:Lorg/apache/http/Header;
    :cond_3d
    const-string v5, "HttpTransaction"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    .end local v0           #arr$:[Lorg/apache/http/Header;
    .end local v1           #builder:Ljava/lang/StringBuilder;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_46
    return-void
.end method

.method public final process(Lorg/apache/http/HttpRequest;Lorg/apache/http/protocol/HttpContext;)V
    .registers 16
    .parameter "request"
    .parameter "context"

    .prologue
    .line 532
    const-string v11, "http.cookiespec-registry"

    invoke-interface {p2, v11}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/http/cookie/CookieSpecRegistry;

    .line 533
    .local v7, registry:Lorg/apache/http/cookie/CookieSpecRegistry;
    const-string v11, "http.target_host"

    invoke-interface {p2, v11}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/http/HttpHost;

    .line 534
    .local v10, targetHost:Lorg/apache/http/HttpHost;
    const-string v11, "http.connection"

    invoke-interface {p2, v11}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/conn/ManagedClientConnection;

    .line 535
    .local v0, conn:Lorg/apache/http/conn/ManagedClientConnection;
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v11

    invoke-static {v11}, Lorg/apache/http/client/params/HttpClientParams;->getCookiePolicy(Lorg/apache/http/params/HttpParams;)Ljava/lang/String;

    move-result-object v5

    .local v5, policy:Ljava/lang/String;
    move-object v11, p1

    .line 536
    check-cast v11, Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v11}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v8

    .line 537
    .local v8, requestURI:Ljava/net/URI;
    invoke-virtual {v10}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v3

    .line 538
    .local v3, hostName:Ljava/lang/String;
    invoke-virtual {v10}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v6

    .line 539
    .local v6, port:I
    if-gez v6, :cond_35

    .line 540
    invoke-interface {v0}, Lorg/apache/http/conn/ManagedClientConnection;->getRemotePort()I

    move-result v6

    .line 542
    :cond_35
    invoke-virtual {v8}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 543
    .local v4, path:Ljava/lang/String;
    invoke-interface {v0}, Lorg/apache/http/conn/ManagedClientConnection;->isSecure()Z

    move-result v9

    .line 544
    .local v9, secure:Z
    new-instance v1, Lorg/apache/http/cookie/CookieOrigin;

    invoke-direct {v1, v3, v6, v4, v9}, Lorg/apache/http/cookie/CookieOrigin;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    .line 545
    .local v1, cookieOrigin:Lorg/apache/http/cookie/CookieOrigin;
    invoke-interface {p1}, Lorg/apache/http/HttpRequest;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v11

    invoke-virtual {v7, v5, v11}, Lorg/apache/http/cookie/CookieSpecRegistry;->getCookieSpec(Ljava/lang/String;Lorg/apache/http/params/HttpParams;)Lorg/apache/http/cookie/CookieSpec;

    move-result-object v2

    .line 546
    .local v2, cookieSpec:Lorg/apache/http/cookie/CookieSpec;
    const-string v11, "http.cookie-spec"

    invoke-interface {p2, v11, v2}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 547
    const-string v11, "http.cookie-origin"

    invoke-interface {p2, v11, v1}, Lorg/apache/http/protocol/HttpContext;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    .line 549
    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    if-eqz v11, :cond_61

    .line 550
    iget-object v11, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-interface {v0}, Lorg/apache/http/conn/ManagedClientConnection;->getMetrics()Lorg/apache/http/HttpConnectionMetrics;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->setConnectionMetrics(Lorg/apache/http/HttpConnectionMetrics;)V

    .line 552
    :cond_61
    return-void
.end method

.method public final process(Lorg/apache/http/HttpResponse;Lorg/apache/http/protocol/HttpContext;)V
    .registers 7
    .parameter "response"
    .parameter "context"

    .prologue
    .line 559
    const-string v3, "http.cookie-spec"

    invoke-interface {p2, v3}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/http/cookie/CookieSpec;

    .line 560
    .local v1, cookieSpec:Lorg/apache/http/cookie/CookieSpec;
    const-string v3, "http.cookie-origin"

    invoke-interface {p2, v3}, Lorg/apache/http/protocol/HttpContext;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/CookieOrigin;

    .line 561
    .local v0, cookieOrigin:Lorg/apache/http/cookie/CookieOrigin;
    const-string v3, "Set-Cookie"

    invoke-interface {p1, v3}, Lorg/apache/http/HttpResponse;->headerIterator(Ljava/lang/String;)Lorg/apache/http/HeaderIterator;

    move-result-object v2

    .line 562
    .local v2, it:Lorg/apache/http/HeaderIterator;
    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->processCookies(Lorg/apache/http/HeaderIterator;Lorg/apache/http/cookie/CookieSpec;Lorg/apache/http/cookie/CookieOrigin;)V

    .line 563
    invoke-interface {v1}, Lorg/apache/http/cookie/CookieSpec;->getVersion()I

    move-result v3

    if-lez v3, :cond_28

    .line 564
    const-string v3, "Set-Cookie2"

    invoke-interface {p1, v3}, Lorg/apache/http/HttpResponse;->headerIterator(Ljava/lang/String;)Lorg/apache/http/HeaderIterator;

    move-result-object v2

    .line 565
    invoke-direct {p0, v2, v1, v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->processCookies(Lorg/apache/http/HeaderIterator;Lorg/apache/http/cookie/CookieSpec;Lorg/apache/http/cookie/CookieOrigin;)V

    .line 567
    :cond_28
    return-void
.end method

.method public final setHttpTransactionMetrics(Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V
    .registers 2
    .parameter "metrics"

    .prologue
    .line 306
    iput-object p1, p0, Lcom/google/android/apps/plus/network/HttpTransaction;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    .line 307
    return-void
.end method
