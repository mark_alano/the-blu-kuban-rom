.class public abstract Lcom/google/android/apps/plus/network/PlusiOperation;
.super Lcom/google/android/apps/plus/network/ApiaryOperation;
.source "PlusiOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Request:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        "Response:",
        "Lcom/google/android/apps/plus/json/GenericJson;",
        ">",
        "Lcom/google/android/apps/plus/network/ApiaryOperation",
        "<TRequest;TResponse;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V
    .registers 18
    .parameter "context"
    .parameter "account"
    .parameter "path"
    .parameter
    .parameter
    .parameter "intent"
    .parameter "listener"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, this:Lcom/google/android/apps/plus/network/PlusiOperation;,"Lcom/google/android/apps/plus/network/PlusiOperation<TRequest;TResponse;>;"
    .local p4, requestJson:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TRequest;>;"
    .local p5, responseJson:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TResponse;>;"
    invoke-static {p3}, Lcom/google/android/apps/plus/network/PlusiOperation;->createPlusiRequestUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v4

    const-string v5, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/network/ApiaryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    .line 47
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V
    .registers 20
    .parameter "context"
    .parameter "account"
    .parameter "path"
    .parameter
    .parameter
    .parameter "intent"
    .parameter "listener"
    .parameter "requestConfiguration"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Lcom/google/android/apps/plus/network/HttpRequestConfiguration;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, this:Lcom/google/android/apps/plus/network/PlusiOperation;,"Lcom/google/android/apps/plus/network/PlusiOperation<TRequest;TResponse;>;"
    .local p4, requestJson:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TRequest;>;"
    .local p5, responseJson:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TResponse;>;"
    invoke-static {p3}, Lcom/google/android/apps/plus/network/PlusiOperation;->createPlusiRequestUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v4

    const-string v5, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/network/ApiaryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    .line 58
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V
    .registers 22
    .parameter "context"
    .parameter "account"
    .parameter "path"
    .parameter "frontendUrlPath"
    .parameter "queryParams"
    .parameter
    .parameter
    .parameter "intent"
    .parameter "listener"
    .parameter "requestConfiguration"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TRequest;>;",
            "Lcom/google/android/apps/plus/json/EsJson",
            "<TResponse;>;",
            "Landroid/content/Intent;",
            "Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;",
            "Lcom/google/android/apps/plus/network/HttpRequestConfiguration;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, this:Lcom/google/android/apps/plus/network/PlusiOperation;,"Lcom/google/android/apps/plus/network/PlusiOperation<TRequest;TResponse;>;"
    .local p6, requestJson:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TRequest;>;"
    .local p7, responseJson:Lcom/google/android/apps/plus/json/EsJson;,"Lcom/google/android/apps/plus/json/EsJson<TResponse;>;"
    invoke-static/range {p3 .. p5}, Lcom/google/android/apps/plus/network/PlusiOperation;->createPlusiRequestUrl(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->PLUS_BACKEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v4

    const-string v5, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/network/ApiaryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/json/EsJson;Lcom/google/android/apps/plus/json/EsJson;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/network/HttpRequestConfiguration;)V

    .line 70
    return-void
.end method

.method private static createPlusiRequestUrl(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "requestPath"

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-static {p0, v0, v0}, Lcom/google/android/apps/plus/network/PlusiOperation;->createPlusiRequestUrl(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static createPlusiRequestUrl(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    .registers 11
    .parameter "requestPath"
    .parameter "frontendUrlPath"
    .parameter "queryParams"

    .prologue
    .line 81
    const-string v6, "/"

    invoke-virtual {p0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 82
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 88
    :cond_d
    sget-object v6, Lcom/google/android/apps/plus/util/Property;->ENABLE_DOGFOOD_FEATURES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v6

    if-eqz v6, :cond_54

    sget-object v6, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v6

    const-string v7, "http:"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_54

    .line 91
    sget-object v6, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 98
    .local v0, builder:Landroid/net/Uri$Builder;
    :goto_31
    if-eqz p1, :cond_69

    .line 99
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 104
    :goto_36
    if-eqz p2, :cond_73

    .line 105
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, i$:Ljava/util/Iterator;
    :goto_40
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_73

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 106
    .local v2, key:Ljava/lang/String;
    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v2, v6}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_40

    .line 93
    .end local v0           #builder:Landroid/net/Uri$Builder;
    .end local v1           #i$:Ljava/util/Iterator;
    .end local v2           #key:Ljava/lang/String;
    :cond_54
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 94
    .restart local v0       #builder:Landroid/net/Uri$Builder;
    const-string v6, "https"

    invoke-virtual {v0, v6}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_URL:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_31

    .line 101
    :cond_69
    sget-object v6, Lcom/google/android/apps/plus/util/Property;->PLUS_FRONTEND_PATH:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_36

    .line 110
    :cond_73
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 113
    sget-object v6, Lcom/google/android/apps/plus/util/Property;->TRACING_TOKEN:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v4

    .line 114
    .local v4, tracingToken:Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a8

    .line 115
    sget-object v6, Lcom/google/android/apps/plus/util/Property;->TRACING_PATH:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, tracingPath:Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a8

    invoke-static {v3, p0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_a8

    .line 117
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "token:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 118
    .local v5, tracingValue:Ljava/lang/String;
    const-string v6, "trace"

    invoke-virtual {v0, v6, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 122
    .end local v3           #tracingPath:Ljava/lang/String;
    .end local v5           #tracingValue:Ljava/lang/String;
    :cond_a8
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method
