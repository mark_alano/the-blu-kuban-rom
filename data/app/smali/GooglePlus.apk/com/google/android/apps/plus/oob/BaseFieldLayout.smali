.class public abstract Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.super Landroid/widget/LinearLayout;
.source "BaseFieldLayout.java"


# instance fields
.field protected mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

.field protected mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

.field private mInputId:I

.field private mLabelId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .parameter "context"
    .parameter "attrs"

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .parameter "context"
    .parameter "attrs"
    .parameter "defStyle"

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method


# virtual methods
.method public bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .registers 9
    .parameter "field"
    .parameter "id"
    .parameter "actionCallback"

    .prologue
    .line 84
    const v2, 0x7f09011a

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 85
    .local v1, labelView:Landroid/view/View;
    if-eqz v1, :cond_12

    .line 86
    iput p2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mLabelId:I

    .line 87
    iget v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mLabelId:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 88
    add-int/lit8 p2, p2, 0x1

    .line 91
    :cond_12
    const v2, 0x7f09021e

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 92
    .local v0, inputView:Landroid/view/View;
    if-eqz v0, :cond_22

    .line 93
    iput p2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mInputId:I

    .line 94
    iget v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mInputId:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    .line 95
    :cond_22
    iput-object p1, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    .line 99
    iput-object p3, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    .line 100
    iget-object v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v2, :cond_56

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->hasError:Ljava/lang/Boolean;

    if-eqz v2, :cond_56

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->hasError:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_56

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getLabelView()Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_56

    instance-of v3, v2, Landroid/widget/TextView;

    if-eqz v3, :cond_56

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0f001d

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 103
    :cond_56
    return-void
.end method

.method public final getActionType()Ljava/lang/String;
    .registers 2

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public final getField()Lcom/google/api/services/plusi/model/OutOfBoxField;
    .registers 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    return-object v0
.end method

.method public final getInputView()Landroid/view/View;
    .registers 2

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mInputId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getLabelView()Landroid/widget/TextView;
    .registers 2

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mLabelId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public final getServerBooleanValue()Ljava/lang/Boolean;
    .registers 3

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getServerValue()Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    move-result-object v0

    .line 144
    .local v0, value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;
    if-eqz v0, :cond_9

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->boolValue:Ljava/lang/Boolean;

    :goto_8
    return-object v1

    :cond_9
    const/4 v1, 0x0

    goto :goto_8
.end method

.method public final getServerDateValue()Lcom/google/api/services/plusi/model/MobileCoarseDate;
    .registers 3

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getServerValue()Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    move-result-object v0

    .line 136
    .local v0, value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;
    if-eqz v0, :cond_9

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->dateValue:Lcom/google/api/services/plusi/model/MobileCoarseDate;

    :goto_8
    return-object v1

    :cond_9
    const/4 v1, 0x0

    goto :goto_8
.end method

.method public final getServerImageType()Ljava/lang/String;
    .registers 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->image:Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->image:Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxImageField;->type:Ljava/lang/String;

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public final getServerStringValue()Ljava/lang/String;
    .registers 3

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getServerValue()Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    move-result-object v0

    .line 128
    .local v0, value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;
    if-eqz v0, :cond_9

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->stringValue:Ljava/lang/String;

    :goto_8
    return-object v1

    :cond_9
    const/4 v1, 0x0

    goto :goto_8
.end method

.method public final getServerValue()Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;
    .registers 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    :goto_c
    return-object v0

    :cond_d
    const/4 v0, 0x0

    goto :goto_c
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
.end method

.method public setActionEnabled(Z)V
    .registers 2
    .parameter "enabled"

    .prologue
    .line 185
    return-void
.end method

.method public final shouldPreventCompletionAction()Z
    .registers 4

    .prologue
    const/4 v0, 0x0

    .line 116
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v1, :cond_15

    const-string v1, "HIDDEN"

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 120
    :cond_15
    :goto_15
    return v0

    :cond_16
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_15

    const/4 v0, 0x1

    goto :goto_15
.end method
