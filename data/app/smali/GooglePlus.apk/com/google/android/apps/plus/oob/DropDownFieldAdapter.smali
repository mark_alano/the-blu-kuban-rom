.class final Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;
.super Landroid/widget/BaseAdapter;
.source "DropDownFieldAdapter.java"


# instance fields
.field private final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 24
    .local p1, items:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->mItems:Ljava/util/List;

    .line 26
    return-void
.end method

.method private getItem(I)Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;
    .registers 3
    .parameter "position"

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;

    return-object v0
.end method

.method private getTextView(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;
    .registers 10
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "resource"

    .prologue
    .line 53
    move-object v3, p2

    check-cast v3, Landroid/widget/TextView;

    .line 54
    .local v3, view:Landroid/widget/TextView;
    if-nez v3, :cond_14

    .line 55
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 56
    .local v0, context:Landroid/content/Context;
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 57
    .local v1, inflater:Landroid/view/LayoutInflater;
    const/4 v4, 0x0

    invoke-virtual {v1, p4, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .end local v3           #view:Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 59
    .end local v0           #context:Landroid/content/Context;
    .end local v1           #inflater:Landroid/view/LayoutInflater;
    .restart local v3       #view:Landroid/widget/TextView;
    :cond_14
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getItem(I)Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;

    move-result-object v2

    .line 60
    .local v2, item:Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;
    iget-object v4, v2, Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    return-object v3
.end method


# virtual methods
.method public final getCount()I
    .registers 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 78
    const v0, 0x1090009

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getTextView(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getItem(I)Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .registers 4
    .parameter "position"

    .prologue
    .line 49
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"

    .prologue
    .line 69
    const v0, 0x7f0300bf

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getTextView(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public final indexOf(Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;)I
    .registers 12
    .parameter "value"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 87
    const/4 v2, 0x0

    .local v2, position:I
    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getCount()I

    move-result v0

    .local v0, count:I
    :goto_7
    if-ge v2, v0, :cond_7f

    .line 88
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/oob/DropDownFieldAdapter;->getItem(I)Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;

    move-result-object v1

    .line 89
    .local v1, option:Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;
    iget-object v6, v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    if-eqz v6, :cond_13

    if-nez p1, :cond_1b

    :cond_13
    if-ne v6, p1, :cond_19

    move v3, v4

    :goto_16
    if-eqz v3, :cond_7c

    .line 93
    .end local v1           #option:Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;
    .end local v2           #position:I
    :goto_18
    return v2

    .restart local v1       #option:Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;
    .restart local v2       #position:I
    :cond_19
    move v3, v5

    .line 89
    goto :goto_16

    :cond_1b
    iget-object v3, v6, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->stringValue:Ljava/lang/String;

    iget-object v7, p1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->stringValue:Ljava/lang/String;

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7a

    iget-object v3, v6, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->boolValue:Ljava/lang/Boolean;

    iget-object v7, p1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->boolValue:Ljava/lang/Boolean;

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7a

    iget-object v3, v6, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->dateValue:Lcom/google/api/services/plusi/model/MobileCoarseDate;

    iget-object v7, p1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->dateValue:Lcom/google/api/services/plusi/model/MobileCoarseDate;

    if-eqz v3, :cond_37

    if-nez v7, :cond_4d

    :cond_37
    if-ne v3, v7, :cond_4b

    move v3, v4

    :goto_3a
    if-eqz v3, :cond_7a

    iget-object v3, v6, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->gender:Lcom/google/api/services/plusi/model/MobileGender;

    iget-object v6, p1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->gender:Lcom/google/api/services/plusi/model/MobileGender;

    if-eqz v3, :cond_44

    if-nez v6, :cond_71

    :cond_44
    if-ne v3, v6, :cond_6f

    move v3, v4

    :goto_47
    if-eqz v3, :cond_7a

    move v3, v4

    goto :goto_16

    :cond_4b
    move v3, v5

    goto :goto_3a

    :cond_4d
    iget-object v8, v3, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    iget-object v9, v7, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6d

    iget-object v8, v3, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    iget-object v9, v7, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6d

    iget-object v3, v3, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    iget-object v7, v7, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6d

    move v3, v4

    goto :goto_3a

    :cond_6d
    move v3, v5

    goto :goto_3a

    :cond_6f
    move v3, v5

    goto :goto_47

    :cond_71
    iget-object v3, v3, Lcom/google/api/services/plusi/model/MobileGender;->type:Ljava/lang/String;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/MobileGender;->type:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    goto :goto_47

    :cond_7a
    move v3, v5

    goto :goto_16

    .line 87
    :cond_7c
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 93
    .end local v1           #option:Lcom/google/api/services/plusi/model/OutOfBoxFieldOption;
    :cond_7f
    const/4 v2, -0x1

    goto :goto_18
.end method
