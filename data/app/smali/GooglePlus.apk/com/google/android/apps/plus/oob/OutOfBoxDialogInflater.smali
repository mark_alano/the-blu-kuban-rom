.class public final Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;
.super Ljava/lang/Object;
.source "OutOfBoxDialogInflater.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

.field private final mActivity:Landroid/support/v4/app/FragmentActivity;

.field private final mOutOfBoxView:Lcom/google/api/services/plusi/model/OutOfBoxView;

.field private final mParent:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Landroid/view/ViewGroup;Lcom/google/api/services/plusi/model/OutOfBoxView;Lcom/google/android/apps/plus/oob/ActionCallback;)V
    .registers 5
    .parameter "activity"
    .parameter "parent"
    .parameter "outOfBoxView"
    .parameter "actionCallback"

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActivity:Landroid/support/v4/app/FragmentActivity;

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mParent:Landroid/view/ViewGroup;

    .line 53
    iput-object p3, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mOutOfBoxView:Lcom/google/api/services/plusi/model/OutOfBoxView;

    .line 54
    iput-object p4, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    .line 55
    return-void
.end method


# virtual methods
.method public final inflate()V
    .registers 10

    .prologue
    const/4 v8, 0x0

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mParent:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 63
    iget-object v3, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mParent:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mOutOfBoxView:Lcom/google/api/services/plusi/model/OutOfBoxView;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->dialog:Lcom/google/api/services/plusi/model/OutOfBoxDialog;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Landroid/app/Dialog;

    invoke-direct {v5, v4}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iget-object v0, v1, Lcom/google/api/services/plusi/model/OutOfBoxDialog;->header:Ljava/lang/String;

    if-eqz v0, :cond_8a

    iget-object v0, v1, Lcom/google/api/services/plusi/model/OutOfBoxDialog;->header:Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x1020016

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_37

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    :cond_37
    :goto_37
    const v0, 0x7f03006a

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->setContentView(I)V

    iget-object v0, v1, Lcom/google/api/services/plusi/model/OutOfBoxDialog;->text:Ljava/lang/String;

    if-eqz v0, :cond_4f

    const v0, 0x7f09005a

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/OutOfBoxDialog;->text:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4f
    const v0, 0x7f09014a

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/OutOfBoxDialog;->action:Ljava/util/List;

    if-eqz v2, :cond_8f

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxDialog;->action:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_62
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v7, 0x7f03006b

    invoke-virtual {v2, v7, v0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iget-object v7, v1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->text:Ljava/lang/String;

    invoke-virtual {v2, v7}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_62

    :cond_8a
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    goto :goto_37

    :cond_8f
    invoke-virtual {v5}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 64
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .registers 8
    .parameter "v"

    .prologue
    const/4 v3, 0x0

    .line 119
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;

    .line 120
    .local v0, action:Lcom/google/api/services/plusi/model/OutOfBoxAction;
    const-string v1, "BACK"

    iget-object v2, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_28

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    move-result v1

    if-nez v1, :cond_27

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 133
    :cond_27
    :goto_27
    return-void

    .line 125
    :cond_28
    const-string v1, "CLOSE"

    iget-object v2, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3d

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_27

    .line 128
    :cond_3d
    const-string v1, "URL"

    iget-object v2, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4f

    .line 129
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->viewUrl(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_27

    .line 131
    :cond_4f
    iget-object v2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    new-instance v3, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v3}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v3, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxDialogInflater;->mOutOfBoxView:Lcom/google/api/services/plusi/model/OutOfBoxView;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxView;->field:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_65
    :goto_65
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v5, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v5, :cond_65

    iget-object v5, v3, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_65

    :cond_7d
    new-instance v1, Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/OutOfBoxAction;-><init>()V

    iput-object v1, v3, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v1, v3, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v4, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    iput-object v4, v1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/apps/plus/oob/ActionCallback;->sendOutOfBoxRequest(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V

    goto :goto_27
.end method
