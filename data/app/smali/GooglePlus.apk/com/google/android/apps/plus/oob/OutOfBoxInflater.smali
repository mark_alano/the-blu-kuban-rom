.class public final Lcom/google/android/apps/plus/oob/OutOfBoxInflater;
.super Ljava/lang/Object;
.source "OutOfBoxInflater.java"


# instance fields
.field private final mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mOuterLayout:Landroid/view/ViewGroup;

.field private final mViewGroup:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/google/android/apps/plus/views/BottomActionBar;)V
    .registers 5
    .parameter "outerLayout"
    .parameter "viewGroup"
    .parameter "bottomActionBar"

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mOuterLayout:Landroid/view/ViewGroup;

    .line 49
    iput-object p2, p0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    .line 50
    iput-object p3, p0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    .line 51
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    .line 52
    return-void
.end method


# virtual methods
.method public final inflateFromResponse(Lcom/google/api/services/plusi/model/OutOfBoxView;Lcom/google/android/apps/plus/oob/ActionCallback;)V
    .registers 24
    .parameter "outOfBoxView"
    .parameter "actionCallback"

    .prologue
    .line 62
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 64
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->title:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_29

    .line 65
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mOuterLayout:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const v18, 0x7f090045

    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 66
    .local v16, textView:Landroid/widget/TextView;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->title:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    .end local v16           #textView:Landroid/widget/TextView;
    :cond_29
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->header:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_49

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mOuterLayout:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    const v18, 0x7f09014b

    invoke-virtual/range {v17 .. v18}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 71
    .restart local v16       #textView:Landroid/widget/TextView;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->header:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    .end local v16           #textView:Landroid/widget/TextView;
    :cond_49
    const/4 v13, 0x0

    .line 75
    .local v13, lastTextField:Landroid/widget/EditText;
    const v14, 0x7f090006

    .line 76
    .local v14, nextId:I
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/api/services/plusi/model/OutOfBoxView;->field:Ljava/util/List;

    .line 77
    .local v10, fieldList:Ljava/util/List;,"Ljava/util/List<Lcom/google/api/services/plusi/model/OutOfBoxField;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v9

    .line 81
    .local v9, fieldCount:I
    move v5, v9

    .line 82
    .local v5, bodyFieldCount:I
    add-int/lit8 v11, v9, -0x1

    .local v11, i:I
    :goto_58
    if-ltz v11, :cond_6b

    .line 83
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/api/services/plusi/model/OutOfBoxField;

    .line 84
    .local v8, field:Lcom/google/api/services/plusi/model/OutOfBoxField;
    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    move-object/from16 v17, v0

    if-eqz v17, :cond_6b

    .line 85
    add-int/lit8 v5, v5, -0x1

    .line 82
    add-int/lit8 v11, v11, -0x1

    goto :goto_58

    .line 91
    .end local v8           #field:Lcom/google/api/services/plusi/model/OutOfBoxField;
    :cond_6b
    const/4 v11, 0x0

    :goto_6c
    if-ge v11, v5, :cond_233

    .line 92
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/api/services/plusi/model/OutOfBoxField;

    .line 93
    .restart local v8       #field:Lcom/google/api/services/plusi/model/OutOfBoxField;
    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-object/from16 v17, v0

    if-eqz v17, :cond_16f

    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-object/from16 v17, v0

    const-string v18, "TEXT_INPUT"

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_fc

    const v17, 0x7f0300be

    :goto_8f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v17

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object/from16 v7, v17

    :goto_ad
    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->text:Lcom/google/api/services/plusi/model/OutOfBoxTextField;

    move-object/from16 v17, v0

    if-nez v17, :cond_b9

    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    move-object/from16 v17, v0

    if-eqz v17, :cond_200

    :cond_b9
    const/16 v17, 0x1

    :goto_bb
    if-eqz v17, :cond_22a

    move-object/from16 v0, p2

    invoke-virtual {v7, v8, v14, v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    :goto_c2
    invoke-virtual {v7}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getChildCount()I

    move-result v17

    add-int v17, v17, v14

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->setId(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 94
    .local v7, curView:Lcom/google/android/apps/plus/oob/BaseFieldLayout;
    invoke-virtual {v7}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getId()I

    move-result v17

    add-int/lit8 v14, v17, 0x1

    .line 95
    invoke-virtual {v7}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getInputView()Landroid/view/View;

    move-result-object v12

    .line 96
    .local v12, inputView:Landroid/view/View;
    const-string v17, "TEXT_INPUT"

    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_f8

    move-object v13, v12

    .line 97
    check-cast v13, Landroid/widget/EditText;

    .line 98
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 91
    :cond_f8
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_6c

    .line 93
    .end local v7           #curView:Lcom/google/android/apps/plus/oob/BaseFieldLayout;
    .end local v12           #inputView:Landroid/view/View;
    :cond_fc
    const-string v18, "DROPDOWN"

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_10e

    const v17, 0x7f0300bc

    goto :goto_8f

    :cond_10e
    const-string v18, "CHECKBOX"

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_121

    const v17, 0x7f0300b8

    goto/16 :goto_8f

    :cond_121
    const-string v18, "HIDDEN"

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_134

    const v17, 0x7f0300ba

    goto/16 :goto_8f

    :cond_134
    const-string v18, "BIRTHDAY_INPUT"

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_147

    const v17, 0x7f0300b6

    goto/16 :goto_8f

    :cond_147
    const-string v18, "OutOfBoxInflater"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Input field has unsupported type: "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0x0

    move-object/from16 v7, v17

    goto/16 :goto_ad

    :cond_16f
    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->text:Lcom/google/api/services/plusi/model/OutOfBoxTextField;

    move-object/from16 v17, v0

    if-eqz v17, :cond_190

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v17, v0

    const v18, 0x7f0300bd

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object/from16 v7, v17

    goto/16 :goto_ad

    :cond_190
    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->error:Lcom/google/api/services/plusi/model/OutOfBoxError;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1b1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v17, v0

    const v18, 0x7f0300b9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object/from16 v7, v17

    goto/16 :goto_ad

    :cond_1b1
    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1d2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v17, v0

    const v18, 0x7f0300b7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object/from16 v7, v17

    goto/16 :goto_ad

    :cond_1d2
    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->image:Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1f3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mInflater:Landroid/view/LayoutInflater;

    move-object/from16 v17, v0

    const v18, 0x7f0300bb

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mViewGroup:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/oob/BaseFieldLayout;

    move-object/from16 v7, v17

    goto/16 :goto_ad

    :cond_1f3
    const-string v17, "OutOfBoxInflater"

    const-string v18, "Field doesn\'t have content."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0x0

    move-object/from16 v7, v17

    goto/16 :goto_ad

    :cond_200
    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-object/from16 v17, v0

    if-eqz v17, :cond_226

    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    if-eqz v17, :cond_226

    iget-object v0, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    if-eqz v17, :cond_226

    const/16 v17, 0x1

    goto/16 :goto_bb

    :cond_226
    const/16 v17, 0x0

    goto/16 :goto_bb

    :cond_22a
    const/16 v17, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v7, v8, v14, v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    goto/16 :goto_c2

    .line 102
    .end local v8           #field:Lcom/google/api/services/plusi/model/OutOfBoxField;
    :cond_233
    if-eqz v13, :cond_23c

    .line 103
    const/16 v17, 0x6

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 106
    :cond_23c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/BottomActionBar;->removeAllViews()V

    .line 107
    move v11, v5

    move v15, v14

    .end local v14           #nextId:I
    .local v15, nextId:I
    :goto_247
    if-ge v11, v9, :cond_279

    .line 108
    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/api/services/plusi/model/OutOfBoxField;

    .line 109
    .restart local v8       #field:Lcom/google/api/services/plusi/model/OutOfBoxField;
    iget-object v4, v8, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    .line 110
    .local v4, action:Lcom/google/api/services/plusi/model/OutOfBoxAction;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/oob/OutOfBoxInflater;->mBottomActionBar:Lcom/google/android/apps/plus/views/BottomActionBar;

    move-object/from16 v17, v0

    add-int/lit8 v14, v15, 0x1

    .end local v15           #nextId:I
    .restart local v14       #nextId:I
    iget-object v0, v4, Lcom/google/api/services/plusi/model/OutOfBoxAction;->text:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Lcom/google/android/apps/plus/oob/OutOfBoxInflater$1;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/apps/plus/oob/OutOfBoxInflater$1;-><init>(Lcom/google/android/apps/plus/oob/OutOfBoxInflater;Lcom/google/android/apps/plus/oob/ActionCallback;Lcom/google/api/services/plusi/model/OutOfBoxAction;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v15, v1, v2}, Lcom/google/android/apps/plus/views/BottomActionBar;->addButton(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/apps/plus/views/ActionButton;

    move-result-object v6

    .line 119
    .local v6, button:Lcom/google/android/apps/plus/views/ActionButton;
    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/views/ActionButton;->setTag(Ljava/lang/Object;)V

    .line 107
    add-int/lit8 v11, v11, 0x1

    move v15, v14

    .end local v14           #nextId:I
    .restart local v15       #nextId:I
    goto :goto_247

    .line 121
    .end local v4           #action:Lcom/google/api/services/plusi/model/OutOfBoxAction;
    .end local v6           #button:Lcom/google/android/apps/plus/views/ActionButton;
    .end local v8           #field:Lcom/google/api/services/plusi/model/OutOfBoxField;
    :cond_279
    return-void
.end method
