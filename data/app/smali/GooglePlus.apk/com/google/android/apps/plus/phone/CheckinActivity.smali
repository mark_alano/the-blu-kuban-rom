.class public Lcom/google/android/apps/plus/phone/CheckinActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "CheckinActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/CheckinListFragment$OnUpdateMenuListener;


# instance fields
.field private mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method private startSearch()V
    .registers 5

    .prologue
    .line 202
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CheckinActivity;->mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    if-eqz v1, :cond_30

    .line 203
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/CheckinActivity;->mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->getLocationData()Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v2

    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/phone/CheckinSearchActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "account"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    if-eqz v2, :cond_21

    const-string v1, "location"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 209
    .local v0, intent:Landroid/content/Intent;
    :cond_21
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/phone/CheckinActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 213
    .end local v0           #intent:Landroid/content/Intent;
    :cond_30
    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 257
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 8
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    const/4 v3, -0x1

    .line 150
    packed-switch p1, :pswitch_data_22

    .line 159
    :cond_4
    :goto_4
    return-void

    .line 152
    :pswitch_5
    if-ne p2, v3, :cond_4

    if-eqz p3, :cond_4

    .line 153
    const-string v2, "location"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/DbLocation;

    .line 154
    .local v0, location:Lcom/google/android/apps/plus/content/DbLocation;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 155
    .local v1, result:Landroid/content/Intent;
    const-string v2, "location"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 156
    invoke-virtual {p0, v3, v1}, Lcom/google/android/apps/plus/phone/CheckinActivity;->setResult(ILandroid/content/Intent;)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->finish()V

    goto :goto_4

    .line 150
    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 72
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    if-eqz v0, :cond_16

    .line 73
    check-cast p1, Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/CheckinActivity;->mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CheckinActivity;->mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->setOnUpdateMenuListener(Lcom/google/android/apps/plus/fragments/CheckinListFragment$OnUpdateMenuListener;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CheckinActivity;->mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->setSearchMode(Z)V

    .line 77
    :cond_16
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 2
    .parameter "dialog"

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->finish()V

    .line 232
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 236
    packed-switch p2, :pswitch_data_14

    .line 249
    :goto_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 250
    return-void

    .line 238
    :pswitch_7
    invoke-static {}, Lcom/google/android/apps/plus/phone/Intents;->getLocationSettingActivityIntent()Landroid/content/Intent;

    move-result-object v0

    .line 239
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    .line 244
    .end local v0           #intent:Landroid/content/Intent;
    :pswitch_f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->finish()V

    goto :goto_3

    .line 236
    nop

    :pswitch_data_14
    .packed-switch -0x2
        :pswitch_f
        :pswitch_7
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 3
    .parameter "savedInstanceState"

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->setContentView(I)V

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->showTitlebar(Z)V

    .line 50
    const v0, 0x7f080194

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .parameter "dialogId"
    .parameter "args"

    .prologue
    .line 217
    packed-switch p1, :pswitch_data_26

    .line 226
    const/4 v1, 0x0

    :goto_4
    return-object v1

    .line 219
    :pswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 220
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v1, 0x7f080169

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0801c6

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0801c8

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 222
    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 223
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_4

    .line 217
    :pswitch_data_26
    .packed-switch 0x1bfb7a8
        :pswitch_5
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 128
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_16

    .line 140
    const/4 v0, 0x0

    :goto_9
    return v0

    .line 130
    :sswitch_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/CheckinActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_9

    .line 135
    :sswitch_12
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->startSearch()V

    goto :goto_9

    .line 128
    :sswitch_data_16
    .sparse-switch
        0x102002c -> :sswitch_a
        0x7f090292 -> :sswitch_12
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 94
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CheckinActivity;->mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    if-eqz v1, :cond_18

    .line 95
    const v1, 0x7f090292

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 96
    .local v0, searchItem:Landroid/view/MenuItem;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CheckinActivity;->mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->hasLocation()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 97
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 100
    .end local v0           #searchItem:Landroid/view/MenuItem;
    :cond_18
    const/4 v1, 0x1

    return v1
.end method

.method protected final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 4
    .parameter "menu"

    .prologue
    .line 117
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CheckinActivity;->mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    if-eqz v1, :cond_14

    .line 118
    const v1, 0x7f090292

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 119
    .local v0, searchItem:Landroid/view/MenuItem;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CheckinActivity;->mCheckinFragment:Lcom/google/android/apps/plus/fragments/CheckinListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/CheckinListFragment;->hasLocation()Z

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 121
    .end local v0           #searchItem:Landroid/view/MenuItem;
    :cond_14
    return-void
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 59
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 62
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->finish()V

    .line 64
    :cond_c
    return-void
.end method

.method public onSearchRequested()Z
    .registers 2

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->startSearch()V

    .line 187
    const/4 v0, 0x1

    return v0
.end method

.method protected final onTitlebarLabelClick()V
    .registers 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 109
    return-void
.end method

.method public final onUpdateMenu()V
    .registers 2

    .prologue
    .line 174
    .line 175
    const v0, 0x7f100001

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CheckinActivity;->createTitlebarButtons(I)V

    .line 179
    return-void
.end method
