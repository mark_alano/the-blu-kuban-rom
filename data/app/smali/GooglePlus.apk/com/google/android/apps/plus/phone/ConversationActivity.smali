.class public Lcom/google/android/apps/plus/phone/ConversationActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "ConversationActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;
.implements Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;
.implements Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationParticipantPresenceListener;,
        Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;,
        Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationQuery;,
        Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsQuery;,
        Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsCommandListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragmentActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;",
        "Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;",
        "Lcom/google/android/apps/plus/util/ImageUtils$InsertCameraPhotoDialogDisplayer;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static sInstanceCount:I


# instance fields
.field private final conversationParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAdvancedHangoutsEnabled:Z

.field private mCheckExtraTile:Z

.field private mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

.field private mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

.field private mConversationId:Ljava/lang/String;

.field private mConversationName:Ljava/lang/String;

.field private mConversationRowId:Ljava/lang/Long;

.field private mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

.field private mCreateConversationRequestId:I

.field private mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

.field private mEarliestEventTimestamp:J

.field private mFirstEventTimestamp:J

.field private mFirstHangoutMenuItemIndex:I

.field private mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

.field private mIsConversationLoaded:Z

.field private mIsGroup:Z

.field private mIsMuted:Z

.field private mLastHangoutMenuItemIndex:I

.field private mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

.field private mNeedToInviteParticipants:Z

.field private mParticipantCount:I

.field private mParticipantList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private final mRealTimeChatListener:Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

.field private mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mRootView:Landroid/view/View;

.field private mShakeDetectorWasRunning:Z

.field private mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

.field private mTileContainer:Landroid/widget/LinearLayout;

.field private mTileSelectorMenuItem:Landroid/view/MenuItem;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .prologue
    .line 74
    const-class v0, Lcom/google/android/apps/plus/phone/ConversationActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    :goto_9
    sput-boolean v0, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    return-void

    :cond_c
    const/4 v0, 0x0

    goto :goto_9
.end method

.method public constructor <init>()V
    .registers 3

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    .line 190
    new-instance v0, Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRealTimeChatListener:Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

    .line 227
    new-instance v0, Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationParticipantPresenceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationParticipantPresenceListener;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->conversationParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    .line 230
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/util/HashMap;
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/phone/ConversationActivity;)Lcom/google/android/apps/plus/fragments/MessageListFragment;
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/ConversationActivity;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/ConversationActivity;)I
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCreateConversationRequestId:I

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/phone/ConversationActivity;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 74
    iput p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCreateConversationRequestId:I

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/phone/ConversationActivity;I)V
    .registers 10
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v3, -0x1

    const/4 v7, 0x0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_36

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAdvancedHangoutsEnabled:Z

    if-eqz v1, :cond_52

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v1, v2, :cond_52

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/view/View;->setPadding(IIII)V

    :cond_36
    :goto_36
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-double v1, v1

    int-to-double v3, v0

    const-wide v5, 0x3fe999999999999aL

    mul-double/2addr v3, v5

    cmpg-double v0, v1, v3

    if-gez v0, :cond_60

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->getView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_51
    return-void

    :cond_52
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getHeight()I

    move-result v2

    invoke-virtual {v1, v7, v2, v7, v7}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_36

    :cond_60
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_51
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/phone/ConversationActivity;)Z
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/phone/ConversationActivity;)Ljava/lang/Long;
    .registers 2
    .parameter "x0"

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    return-object v0
.end method

.method public static hasInstance()Z
    .registers 1

    .prologue
    .line 1117
    sget v0, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    if-lez v0, :cond_6

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private initHangoutTile()V
    .registers 13

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 1042
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-nez v1, :cond_5d

    .line 1044
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/HangoutData;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v10

    .line 1046
    .local v10, hangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    const-string v3, "c:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1b

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v10, v1, :cond_1c

    .line 1103
    .end local v10           #hangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;
    :cond_1b
    :goto_1b
    return-void

    .line 1055
    .restart local v10       #hangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;
    :cond_1c
    :try_start_1c
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;
    :try_end_27
    .catch Ljava/lang/LinkageError; {:try_start_1c .. :try_end_27} :catch_9b

    .line 1062
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAdvancedHangoutsEnabled:Z

    if-eqz v1, :cond_a6

    .line 1063
    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    .line 1067
    :goto_32
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onCreate(Landroid/os/Bundle;)V

    .line 1069
    new-instance v11, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v11, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1071
    .local v11, layoutParams:Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1, v11}, Lcom/google/android/apps/plus/hangout/HangoutTile;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1072
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mTileContainer:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1073
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->conversationParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/hangout/HangoutTile;->addParticipantPresenceListener(Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;)V

    .line 1074
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onStart()V

    .line 1078
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v1, v3, :cond_b2

    .line 1079
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->invalidateOptionsMenu()V

    .line 1085
    .end local v10           #hangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;
    .end local v11           #layoutParams:Landroid/widget/LinearLayout$LayoutParams;
    :cond_5d
    :goto_5d
    new-instance v0, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$RoomType;->EXTERNAL:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const-string v3, "messenger"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Messenger:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-object v5, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    .line 1088
    .local v0, hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v6, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object v5, v0

    move v8, v7

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/apps/plus/hangout/HangoutTile;->setHangoutInfo(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$Info;Ljava/util/ArrayList;ZZ)V

    .line 1094
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCheckExtraTile:Z

    if-eqz v1, :cond_1b

    .line 1095
    iput-boolean v7, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCheckExtraTile:Z

    .line 1097
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->shouldShowHangoutTile()Z

    move-result v1

    if-eqz v1, :cond_91

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    if-eq v1, v2, :cond_97

    :cond_91
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v1, v2, :cond_1b

    .line 1100
    :cond_97
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->toggleTiles()V

    goto :goto_1b

    .line 1056
    .end local v0           #hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    .restart local v10       #hangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;
    :catch_9b
    move-exception v9

    .line 1057
    .local v9, err:Ljava/lang/LinkageError;
    const-string v1, "Could not load hangout native library"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 1058
    invoke-virtual {v9}, Ljava/lang/LinkageError;->printStackTrace()V

    goto/16 :goto_1b

    .line 1065
    .end local v9           #err:Ljava/lang/LinkageError;
    :cond_a6
    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v7}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setInnerActionBarEnabled(Z)Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    goto :goto_32

    .line 1081
    .restart local v11       #layoutParams:Landroid/widget/LinearLayout$LayoutParams;
    :cond_b2
    const v1, 0x7f100002

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->createTitlebarButtons(I)V

    goto :goto_5d
.end method

.method private initialize()V
    .registers 11

    .prologue
    const-wide/16 v8, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 304
    const-string v3, "initialize"

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 307
    iput-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    .line 308
    iput-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    .line 309
    iput-boolean v7, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCheckExtraTile:Z

    .line 310
    iput-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    .line 311
    iput-boolean v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    .line 313
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 314
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 315
    const-string v3, "is_group"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    .line 316
    sget-boolean v3, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    if-nez v3, :cond_38

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    if-nez v3, :cond_38

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 317
    :cond_38
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 319
    const-string v3, "conversation_row_id"

    invoke-virtual {v2, v3, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 320
    .local v0, conversationRowId:J
    cmp-long v3, v0, v8

    if-eqz v3, :cond_4f

    .line 321
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    .line 323
    :cond_4f
    const-string v3, "participant"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    .line 324
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-eqz v3, :cond_a0

    .line 325
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    .line 326
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->displayParticipantsInTray()V

    .line 328
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mSingleParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setConversationLabel(Ljava/lang/String;)V

    .line 329
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    if-eqz v3, :cond_86

    .line 330
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setParticipantListButtonVisibility(Z)V

    .line 332
    :cond_86
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v3, :cond_8f

    .line 333
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->allowSendingImages(Z)V

    .line 348
    :cond_8f
    :goto_8f
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    new-instance v4, Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsCommandListener;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->getParticipantsGalleryView()Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsCommandListener;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;Lcom/google/android/apps/plus/views/ParticipantsGalleryView;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setCommandListener(Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;)V

    .line 351
    return-void

    .line 336
    :cond_a0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    invoke-virtual {v3, v7, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 337
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 338
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/ConversationTile;->setConversationRowId(Ljava/lang/Long;)V

    .line 339
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    if-eqz v3, :cond_bf

    .line 340
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setParticipantListButtonVisibility(Z)V

    .line 342
    :cond_bf
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v3, :cond_8f

    .line 343
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-virtual {v3, v7}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->allowSendingImages(Z)V

    goto :goto_8f
.end method

.method private inviteMoreParticipants()V
    .registers 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 803
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    if-eqz v0, :cond_2a

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_8
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 806
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    if-eqz v0, :cond_2f

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    if-eqz v0, :cond_2f

    .line 807
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v0, v6, :cond_2d

    move v0, v1

    :goto_24
    invoke-static {p0, v3, v4, v5, v0}, Lcom/google/android/apps/plus/phone/ParticipantHelper;->inviteMoreParticipants(Landroid/app/Activity;Ljava/util/Collection;ZLcom/google/android/apps/plus/content/EsAccount;Z)V

    .line 810
    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    .line 816
    :goto_29
    return-void

    .line 803
    :cond_2a
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_ADD_PEOPLE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_8

    :cond_2d
    move v0, v2

    .line 807
    goto :goto_24

    .line 814
    :cond_2f
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    goto :goto_29
.end method

.method private prepareToggleTilesMenu(Landroid/view/Menu;)Landroid/view/MenuItem;
    .registers 10
    .parameter "menu"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 693
    const v6, 0x7f090293

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 695
    .local v3, tileSelectorMenuItem:Landroid/view/MenuItem;
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v6, :cond_3a

    .line 696
    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v6, v7, :cond_34

    move v2, v4

    .line 697
    .local v2, inHangout:Z
    :goto_14
    if-eqz v2, :cond_36

    const v0, 0x7f02013f

    .line 698
    .local v0, drawable:I
    :goto_19
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 699
    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 700
    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 701
    if-nez v2, :cond_40

    .line 702
    iget v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstHangoutMenuItemIndex:I

    .local v1, i:I
    :goto_26
    iget v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mLastHangoutMenuItemIndex:I

    if-ge v1, v4, :cond_40

    .line 703
    invoke-interface {p1, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 702
    add-int/lit8 v1, v1, 0x1

    goto :goto_26

    .end local v0           #drawable:I
    .end local v1           #i:I
    .end local v2           #inHangout:Z
    :cond_34
    move v2, v5

    .line 696
    goto :goto_14

    .line 697
    .restart local v2       #inHangout:Z
    :cond_36
    const v0, 0x7f0200de

    goto :goto_19

    .line 707
    .end local v2           #inHangout:Z
    :cond_3a
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 708
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 712
    :cond_40
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_47

    .end local v3           #tileSelectorMenuItem:Landroid/view/MenuItem;
    :goto_46
    return-object v3

    .restart local v3       #tileSelectorMenuItem:Landroid/view/MenuItem;
    :cond_47
    const/4 v3, 0x0

    goto :goto_46
.end method

.method private setConversationLabel(Ljava/lang/String;)V
    .registers 5
    .parameter "conversationName"

    .prologue
    .line 869
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_10

    .line 870
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->showTitlebar(Z)V

    .line 871
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 876
    :goto_d
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationName:Ljava/lang/String;

    .line 877
    return-void

    .line 873
    :cond_10
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 874
    .local v0, actionBar:Landroid/app/ActionBar;
    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_d
.end method

.method private shouldShowHangoutTile()Z
    .registers 4

    .prologue
    .line 1106
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "tile"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    const-class v0, Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "tile"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    const/4 v0, 0x1

    :goto_23
    return v0

    :cond_24
    const/4 v0, 0x0

    goto :goto_23
.end method

.method private updateSubtitle()V
    .registers 9

    .prologue
    const/4 v7, 0x1

    .line 892
    const/4 v1, 0x0

    .line 893
    .local v1, subtitle:Ljava/lang/String;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    if-eqz v2, :cond_20

    iget v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantCount:I

    if-lez v2, :cond_20

    .line 894
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08020c

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 896
    :cond_20
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_2d

    .line 897
    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/ConversationActivity;->showTitlebar(Z)V

    .line 898
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setTitlebarSubtitle(Ljava/lang/String;)V

    .line 903
    :goto_2c
    return-void

    .line 900
    :cond_2d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 901
    .local v0, actionBar:Landroid/app/ActionBar;
    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_2c
.end method


# virtual methods
.method public final blockPerson(Ljava/io/Serializable;)V
    .registers 3
    .parameter "callbackData"

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_9

    .line 1161
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->blockPerson(Ljava/io/Serializable;)V

    .line 1163
    :cond_9
    return-void
.end method

.method public final displayParticipantsInTray()V
    .registers 5

    .prologue
    .line 1023
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    if-nez v1, :cond_5

    .line 1039
    :goto_4
    return-void

    .line 1027
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    if-ne v1, v2, :cond_28

    .line 1028
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1029
    .local v0, hangoutTileActiveParticipants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v1, :cond_1a

    .line 1030
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getActiveParticipantIds()Ljava/util/HashSet;

    move-result-object v0

    .line 1032
    :cond_1a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ConversationTile;->getActiveParticipantIds()Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;Ljava/util/HashSet;)V

    goto :goto_4

    .line 1036
    .end local v0           #hangoutTileActiveParticipants:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_28
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ConversationTile;->getActiveParticipantIds()Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/hangout/HangoutTile;->setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;)V

    goto :goto_4
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 2

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getGreenRoomParticipantListActivityIntent(Ljava/util/ArrayList;)Landroid/content/Intent;
    .registers 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1201
    .local p1, participants:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/google/wireless/realtimechat/proto/Data$Participant;>;"
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getParticipantListActivityIntent()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final getHangoutNotificationIntent()Landroid/content/Intent;
    .registers 5

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getConversationActivityHangoutTileIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final getParticipantListActivityIntent()Landroid/content/Intent;
    .registers 9

    .prologue
    .line 1209
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationName:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v0, v6, :cond_41

    const/4 v0, 0x1

    :goto_13
    new-instance v6, Landroid/content/Intent;

    const-class v7, Lcom/google/android/apps/plus/phone/ParticipantListActivity;

    invoke-direct {v6, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v7, "account"

    invoke-virtual {v6, v7, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "conversation_row_id"

    invoke-virtual {v6, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "conversation_name"

    invoke-virtual {v6, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_group"

    invoke-virtual {v6, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    if-eqz v0, :cond_40

    const-string v0, "tile"

    const-class v1, Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_40
    return-object v6

    :cond_41
    const/4 v0, 0x0

    goto :goto_13
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 3

    .prologue
    .line 1142
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_group"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_GROUP:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_12
    return-object v0

    :cond_13
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_ONE_ON_ONE:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_12
.end method

.method public final hideInsertCameraPhotoDialog()V
    .registers 2

    .prologue
    .line 1244
    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->dismissDialog(I)V

    .line 1245
    return-void
.end method

.method public final leaveConversation()V
    .registers 10

    .prologue
    const/4 v2, 0x0

    .line 1216
    sget-boolean v1, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    if-nez v1, :cond_f

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    .line 1218
    :cond_f
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v8

    .line 1220
    .local v8, gCommApp:Lcom/google/android/apps/plus/hangout/GCommApp;
    new-instance v0, Lcom/google/android/apps/plus/service/Hangout$Info;

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$RoomType;->EXTERNAL:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const-string v3, "messenger"

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Messenger:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v7, 0x0

    move-object v5, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/service/Hangout$Info;-><init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V

    .line 1224
    .local v0, hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;
    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v1

    if-eqz v1, :cond_2b

    .line 1225
    invoke-virtual {v8}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeeting()V

    .line 1227
    :cond_2b
    return-void
.end method

.method protected final needsAsyncData()Z
    .registers 2

    .prologue
    .line 1152
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 736
    const/4 v0, 0x1

    if-ne p1, v0, :cond_13

    .line 737
    const/4 v0, -0x1

    if-ne p2, v0, :cond_12

    if-eqz p3, :cond_12

    .line 738
    const-string v0, "audience"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 743
    :cond_12
    :goto_12
    return-void

    .line 741
    :cond_13
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_12
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 8
    .parameter "fragment"

    .prologue
    .line 424
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v0, :cond_24

    .line 425
    check-cast p1, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    if-nez v0, :cond_1d

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->allowSendingImages(Z)V

    .line 431
    :goto_12
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    new-instance v1, Lcom/google/android/apps/plus/phone/ConversationActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/ConversationActivity$2;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->setListener(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;)V

    .line 496
    :cond_1c
    :goto_1c
    return-void

    .line 429
    :cond_1d
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->allowSendingImages(Z)V

    goto :goto_12

    .line 481
    .restart local p1
    :cond_24
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/MessageListFragment;

    if-eqz v0, :cond_48

    .line 482
    check-cast p1, Lcom/google/android/apps/plus/fragments/MessageListFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    .line 483
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setLeaveConversationListener(Lcom/google/android/apps/plus/fragments/MessageListFragment$LeaveConversationListener;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    if-eqz v0, :cond_40

    .line 485
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstEventTimestamp:J

    iget-wide v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mEarliestEventTimestamp:J

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setConversationInfo(Ljava/lang/String;JJ)V

    .line 490
    :cond_40
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setParticipantList(Ljava/util/HashMap;)V

    goto :goto_1c

    .line 491
    .restart local p1
    :cond_48
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    if-eqz v0, :cond_1c

    .line 492
    check-cast p1, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    .line 494
    sget-boolean v0, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_1c

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final onBlockCompleted(Z)V
    .registers 2
    .parameter "success"

    .prologue
    .line 1170
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .parameter "v"

    .prologue
    .line 823
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09023a

    if-ne v0, v1, :cond_c

    .line 824
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->toggleTiles()V

    .line 826
    :cond_c
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    .prologue
    const/4 v4, -0x1

    const/4 v5, 0x1

    .line 243
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 246
    invoke-static {p0}, Lcom/google/android/apps/plus/service/Hangout;->isAdvancedUiSupported(Landroid/content/Context;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAdvancedHangoutsEnabled:Z

    .line 248
    new-instance v2, Lcom/google/android/apps/plus/phone/ConversationActivity$1;

    invoke-direct {v2, p0, p0}, Lcom/google/android/apps/plus/phone/ConversationActivity$1;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;Landroid/content/Context;)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03001b

    invoke-virtual {v3, v4, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    .line 249
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRootView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setContentView(Landroid/view/View;)V

    .line 250
    const-string v2, "ConversationActivity.onCreate"

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 252
    const v2, 0x7f090070

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mTileContainer:Landroid/widget/LinearLayout;

    .line 254
    const v2, 0x7f090071

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/ConversationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ConversationTile;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    .line 255
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->conversationParticipantPresenceListener:Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ConversationTile;->addParticipantPresenceListener(Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;)V

    .line 257
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    .line 259
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantCount:I

    .line 260
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_91

    .line 261
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/ConversationActivity;->showTitlebar(Z)V

    .line 262
    const v2, 0x7f100002

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/ConversationActivity;->createTitlebarButtons(I)V

    .line 269
    :goto_61
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v1

    .line 270
    .local v1, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v1, :cond_71

    .line 271
    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->stop()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mShakeDetectorWasRunning:Z

    .line 274
    :cond_71
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->initialize()V

    .line 276
    sget v2, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    if-le v2, v5, :cond_90

    .line 277
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ConversationActivity instanceCount out of sync: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 279
    :cond_90
    return-void

    .line 264
    .end local v1           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_91
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 265
    .local v0, actionBar:Landroid/app/ActionBar;
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    goto :goto_61
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 4
    .parameter "dialogId"
    .parameter "bundle"

    .prologue
    .line 1231
    const v0, 0x7f09003e

    if-ne p1, v0, :cond_a

    .line 1232
    invoke-static {p0}, Lcom/google/android/apps/plus/util/ImageUtils;->createInsertCameraPhotoDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    .line 1234
    :goto_9
    return-object v0

    :cond_a
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_9
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 11
    .parameter "id"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 910
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ConversationActivity.onCreateLoader: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 912
    if-ne p1, v5, :cond_34

    .line 913
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONVERSATIONS_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/phone/ConversationActivity$ConversationQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "_id=?"

    new-array v5, v5, [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    :goto_33
    return-object v0

    .line 918
    :cond_34
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5a

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/content/EsProvider;->buildParticipantsUri(Lcom/google/android/apps/plus/content/EsAccount;J)Landroid/net/Uri;

    move-result-object v2

    .line 921
    .local v2, uri:Landroid/net/Uri;
    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    sget-object v3, Lcom/google/android/apps/plus/phone/ConversationActivity$ParticipantsQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "participant_id!=? AND active=1"

    new-array v5, v5, [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getRealTimeChatParticipantId()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v7

    const-string v6, "first_name ASC"

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_33

    .end local v2           #uri:Landroid/net/Uri;
    :cond_5a
    move-object v0, v6

    .line 928
    goto :goto_33
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 7
    .parameter "menu"

    .prologue
    const/4 v4, 0x2

    .line 619
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 620
    .local v0, inflater:Landroid/view/MenuInflater;
    const v2, 0x7f100002

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 621
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_25

    .line 622
    const v2, 0x7f090293

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 623
    .local v1, item:Landroid/view/MenuItem;
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 624
    const v2, 0x7f090294

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 625
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 628
    .end local v1           #item:Landroid/view/MenuItem;
    :cond_25
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstHangoutMenuItemIndex:I

    .line 629
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v2, :cond_34

    .line 630
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 632
    :cond_34
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mLastHangoutMenuItemIndex:I

    .line 634
    const/4 v2, 0x1

    return v2
.end method

.method protected onDestroy()V
    .registers 4

    .prologue
    .line 599
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onDestroy()V

    .line 602
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mShakeDetectorWasRunning:Z

    if-eqz v1, :cond_14

    .line 603
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 604
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_14

    .line 605
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->start()Z

    .line 609
    .end local v0           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_14
    sget v1, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    if-gez v1, :cond_30

    .line 610
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ConversationActivity instanceCount out of sync: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/google/android/apps/plus/phone/ConversationActivity;->sInstanceCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    .line 612
    :cond_30
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 11
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 74
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v2, :cond_95

    if-eqz p2, :cond_90

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_90

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1f

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1f
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_91

    move v1, v2

    :goto_26
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_93

    move v1, v2

    :goto_2f
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstEventTimestamp:J

    const/4 v1, 0x6

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mEarliestEventTimestamp:J

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setConversationLabel(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->updateSubtitle()V

    const/4 v0, 0x7

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_63

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lcom/google/android/apps/plus/phone/ConversationActivity$3;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity$3;-><init>(Lcom/google/android/apps/plus/phone/ConversationActivity;I)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_63
    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_6e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->invalidateOptionsMenu()V

    :cond_6e
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    if-eqz v0, :cond_75

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->inviteMoreParticipants()V

    :cond_75
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationHeader:Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/ParticipantsGalleryFragment;->setParticipantListButtonVisibility(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    if-eqz v0, :cond_89

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mFirstEventTimestamp:J

    iget-wide v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mEarliestEventTimestamp:J

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setConversationInfo(Ljava/lang/String;JJ)V

    :cond_89
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    if-eqz v0, :cond_90

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->initHangoutTile()V

    :cond_90
    :goto_90
    return-void

    :cond_91
    move v1, v3

    goto :goto_26

    :cond_93
    move v1, v3

    goto :goto_2f

    :cond_95
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v5, :cond_90

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantCount:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->updateSubtitle()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    :goto_ab
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_e8

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_be

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    :cond_be
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsConversationsData;->convertParticipantType(I)Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_ab

    :cond_e8
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    if-eqz v0, :cond_ef

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->initHangoutTile()V

    :cond_ef
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->displayParticipantsInTray()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    if-eqz v0, :cond_10b

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v5, v6, :cond_118

    :goto_106
    invoke-static {p0, v0, v1, v4, v2}, Lcom/google/android/apps/plus/phone/ParticipantHelper;->inviteMoreParticipants(Landroid/app/Activity;Ljava/util/Collection;ZLcom/google/android/apps/plus/content/EsAccount;Z)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mNeedToInviteParticipants:Z

    :cond_10b
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    if-eqz v0, :cond_90

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mParticipantList:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->setParticipantList(Ljava/util/HashMap;)V

    goto/16 :goto_90

    :cond_118
    move v2, v3

    goto :goto_106
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1126
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public final onMeetingMediaStarted()V
    .registers 8

    .prologue
    .line 1187
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1188
    .local v6, tileEventData:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "AUTHOR_PROFILE_ID"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1190
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationId:Ljava/lang/String;

    const-string v3, "com.google.hangouts"

    const/4 v4, 0x0

    const-string v5, "JOIN_HANGOUT"

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sendTileEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/HashMap;)I

    .line 1193
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .registers 3
    .parameter "intent"

    .prologue
    .line 294
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 295
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->setIntent(Landroid/content/Intent;)V

    .line 296
    const-string v0, "ConversationActivity.onNewIntent"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 298
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->initialize()V

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->reinitialize()V

    .line 300
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter "item"

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 750
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_8c

    .line 792
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_3a

    .line 793
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 796
    :goto_13
    return v0

    .line 752
    :sswitch_14
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    move v0, v1

    .line 753
    goto :goto_13

    .line 756
    :sswitch_1b
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    if-eqz v0, :cond_3c

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_21
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 758
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {p0, v0, v3, v4, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->setConversationMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)I

    .line 760
    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    .line 761
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "conversation_is_muted"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3a
    :goto_3a
    move v0, v2

    .line 796
    goto :goto_13

    .line 756
    :cond_3c
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_MUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_21

    .line 765
    :sswitch_3f
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    if-eqz v0, :cond_5f

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->GROUP_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_45
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    .line 767
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {p0, v0, v3, v4, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->setConversationMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JZ)I

    .line 769
    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    .line 770
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "conversation_is_muted"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_3a

    .line 765
    :cond_5f
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_ON_ONE_CONVERSATION_UNMUTE:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_45

    .line 774
    :sswitch_62
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    if-eqz v0, :cond_3a

    .line 775
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mMessageListFragment:Lcom/google/android/apps/plus/fragments/MessageListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/MessageListFragment;->displayLeaveConversationDialog()V

    goto :goto_3a

    .line 780
    :sswitch_6c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->toggleTiles()V

    goto :goto_3a

    .line 784
    :sswitch_70
    new-instance v0, Lcom/google/android/apps/plus/phone/ConversationRenameDialog;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/apps/plus/phone/ConversationRenameDialog;-><init>(Ljava/lang/String;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "rename_conversation"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/plus/phone/ConversationRenameDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_3a

    .line 788
    :sswitch_87
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->inviteMoreParticipants()V

    goto :goto_3a

    .line 750
    nop

    :sswitch_data_8c
    .sparse-switch
        0x102002c -> :sswitch_14
        0x7f090293 -> :sswitch_6c
        0x7f090294 -> :sswitch_87
        0x7f090295 -> :sswitch_70
        0x7f090296 -> :sswitch_1b
        0x7f090297 -> :sswitch_3f
        0x7f090298 -> :sswitch_62
    .end sparse-switch
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 539
    const-string v0, "ConversationActivity.onPause"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRealTimeChatListener:Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 543
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Tile;->onTilePause()V

    .line 544
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationTile;->onPause()V

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_1d

    .line 546
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onPause()V

    .line 549
    :cond_1d
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 552
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 9
    .parameter "menu"

    .prologue
    const v6, 0x7f090295

    const v5, 0x7f090293

    const v4, 0x7f090294

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 657
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsConversationLoaded:Z

    if-nez v0, :cond_10

    .line 689
    :goto_f
    return v2

    .line 660
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    if-nez v0, :cond_49

    .line 661
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 662
    const v0, 0x7f090296

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 663
    const v0, 0x7f090297

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 664
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 665
    const v0, 0x7f090298

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 666
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_47
    :goto_47
    move v2, v1

    .line 689
    goto :goto_f

    .line 668
    :cond_49
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 670
    const v0, 0x7f090296

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    if-nez v0, :cond_94

    move v0, v1

    :goto_5c
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 671
    const v0, 0x7f090297

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsMuted:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 673
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mIsGroup:Z

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 674
    const v0, 0x7f090298

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 676
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_96

    .line 677
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->prepareToggleTilesMenu(Landroid/view/Menu;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mTileSelectorMenuItem:Landroid/view/MenuItem;

    .line 684
    :goto_8a
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_47

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    goto :goto_47

    :cond_94
    move v0, v2

    .line 670
    goto :goto_5c

    .line 680
    :cond_96
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 681
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_8a
.end method

.method public final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 5
    .parameter "menu"

    .prologue
    const/4 v2, 0x0

    .line 643
    const v0, 0x7f090294

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 644
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/ConversationActivity;->prepareToggleTilesMenu(Landroid/view/Menu;)Landroid/view/MenuItem;

    .line 646
    const v0, 0x7f090296

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 647
    const v0, 0x7f090297

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 648
    const v0, 0x7f090295

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 649
    const v0, 0x7f090298

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 650
    return-void
.end method

.method public onResume()V
    .registers 5

    .prologue
    .line 503
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 504
    const-string v0, "ConversationActivity.onResume"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mRealTimeChatListener:Lcom/google/android/apps/plus/phone/ConversationActivity$RTCServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationTile;->onResume()V

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_1b

    .line 509
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onResume()V

    .line 511
    :cond_1b
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Tile;->onTileResume()V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->isIntentAccountActive()Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v0, :cond_30

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->setAllowSendMessage(Z)V

    .line 519
    :cond_30
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_44

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->inviteParticipants(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)I

    .line 523
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mResultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    .line 526
    :cond_44
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->connectAndStayConnected(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 528
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->displayParticipantsInTray()V

    .line 532
    :goto_4c
    return-void

    .line 530
    :cond_4d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->finish()V

    goto :goto_4c
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 2
    .parameter "outState"

    .prologue
    .line 286
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 287
    return-void
.end method

.method protected onStart()V
    .registers 2

    .prologue
    .line 406
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStart()V

    .line 407
    const-string v0, "ConversationActivity.onStart"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 409
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationTile;->onStart()V

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_16

    .line 411
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onStart()V

    .line 414
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Tile;->onTileStart()V

    .line 416
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->requestFocus()V

    .line 417
    return-void
.end method

.method public onStop()V
    .registers 2

    .prologue
    .line 582
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStop()V

    .line 584
    const-string v0, "ConversationActivity.onStop"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Tile;->onTileStop()V

    .line 588
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ConversationTile;->onStop()V

    .line 589
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v0, :cond_1b

    .line 590
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onStop()V

    .line 592
    :cond_1b
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 2

    .prologue
    .line 728
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    .line 729
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .registers 5
    .parameter "hasFocus"

    .prologue
    .line 559
    if-eqz p1, :cond_1d

    .line 560
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->setCurrentConversationRowId(Ljava/lang/Long;)V

    .line 566
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->shouldShowHangoutTile()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 567
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    if-eqz v0, :cond_1c

    .line 568
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationRowId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->markConversationRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I

    .line 575
    :cond_1c
    :goto_1c
    return-void

    .line 573
    :cond_1d
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->setCurrentConversationRowId(Ljava/lang/Long;)V

    goto :goto_1c
.end method

.method public final showInsertCameraPhotoDialog()V
    .registers 2

    .prologue
    .line 1239
    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->showDialog(I)V

    .line 1240
    return-void
.end method

.method public final stopHangoutTile()V
    .registers 3

    .prologue
    .line 1177
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v0, v1, :cond_9

    .line 1178
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->toggleTiles()V

    .line 1180
    :cond_9
    return-void
.end method

.method public final toggleTiles()V
    .registers 6

    .prologue
    .line 829
    sget-boolean v3, Lcom/google/android/apps/plus/phone/ConversationActivity;->$assertionsDisabled:Z

    if-nez v3, :cond_e

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-nez v3, :cond_e

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 830
    :cond_e
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-nez v3, :cond_13

    .line 862
    :cond_12
    :goto_12
    return-void

    .line 834
    :cond_13
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v3, v4, :cond_68

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    .line 836
    .local v2, newTile:Lcom/google/android/apps/plus/views/Tile;
    :goto_1b
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    const/16 v4, 0x8

    invoke-interface {v3, v4}, Lcom/google/android/apps/plus/views/Tile;->setVisibility(I)V

    .line 837
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v3}, Lcom/google/android/apps/plus/views/Tile;->onTilePause()V

    .line 838
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    invoke-interface {v3}, Lcom/google/android/apps/plus/views/Tile;->onTileStop()V

    .line 840
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/apps/plus/views/Tile;->setVisibility(I)V

    .line 841
    invoke-interface {v2}, Lcom/google/android/apps/plus/views/Tile;->onTileStart()V

    .line 842
    invoke-interface {v2}, Lcom/google/android/apps/plus/views/Tile;->onTileResume()V

    .line 844
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    .line 846
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->displayParticipantsInTray()V

    .line 848
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v3, v4, :cond_6f

    .line 849
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mCurrentTile:Lcom/google/android/apps/plus/views/Tile;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-ne v3, v4, :cond_6b

    const v1, 0x7f02013f

    .line 851
    .local v1, drawable:I
    :goto_4a
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mTileSelectorMenuItem:Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 852
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->invalidateOptionsMenu()V

    .line 853
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mConversationTile:Lcom/google/android/apps/plus/views/ConversationTile;

    if-ne v2, v3, :cond_12

    .line 854
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 855
    .local v0, actionBar:Landroid/app/ActionBar;
    if-eqz v0, :cond_12

    .line 856
    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_12

    .line 834
    .end local v0           #actionBar:Landroid/app/ActionBar;
    .end local v1           #drawable:I
    .end local v2           #newTile:Lcom/google/android/apps/plus/views/Tile;
    :cond_68
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ConversationActivity;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    goto :goto_1b

    .line 849
    .restart local v2       #newTile:Lcom/google/android/apps/plus/views/Tile;
    :cond_6b
    const v1, 0x7f0200de

    goto :goto_4a

    .line 860
    :cond_6f
    const v3, 0x7f100002

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/ConversationActivity;->createTitlebarButtons(I)V

    goto :goto_12
.end method
