.class public Lcom/google/android/apps/plus/phone/EditEventActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EditEventActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;


# instance fields
.field private mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

.field private mEventId:Ljava/lang/String;

.field private mOwnerId:Ljava/lang/String;

.field private mShakeDetectorWasRunning:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method protected final getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;
    .registers 3

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 67
    .local v0, resources:Landroid/content/res/Resources;
    const v1, 0x7f080147

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 5
    .parameter "fragment"

    .prologue
    .line 57
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_16

    .line 58
    check-cast p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEventId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mOwnerId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->editEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setOnEventChangedListener(Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;)V

    .line 62
    :cond_16
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_9

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onDiscard()V

    .line 123
    :cond_9
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "event_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEventId:Ljava/lang/String;

    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "owner_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mOwnerId:Ljava/lang/String;

    .line 36
    const v1, 0x7f030064

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditEventActivity;->setContentView(I)V

    .line 40
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditEventActivity;->showTitlebar(Z)V

    .line 44
    const v1, 0x7f080393

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditEventActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 45
    const v1, 0x7f10001b

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditEventActivity;->createTitlebarButtons(I)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 50
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_45

    .line 51
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->stop()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mShakeDetectorWasRunning:Z

    .line 53
    :cond_45
    return-void
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onDestroy()V

    .line 84
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mShakeDetectorWasRunning:Z

    if-eqz v1, :cond_14

    .line 85
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 86
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_14

    .line 87
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->start()Z

    .line 90
    .end local v0           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_14
    return-void
.end method

.method public final onEventClosed()V
    .registers 1

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->finish()V

    .line 137
    return-void
.end method

.method public final onEventSaved()V
    .registers 1

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->finish()V

    .line 132
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 94
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_18

    .line 108
    const/4 v0, 0x0

    :cond_9
    :goto_9
    return v0

    .line 96
    :sswitch_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->onBackPressed()V

    goto :goto_9

    .line 101
    :sswitch_e
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v1, :cond_9

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->save()V

    goto :goto_9

    .line 94
    :sswitch_data_18
    .sparse-switch
        0x102002c -> :sswitch_a
        0x7f09029b -> :sswitch_e
    .end sparse-switch
.end method

.method protected onResume()V
    .registers 2

    .prologue
    .line 72
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 75
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->finish()V

    .line 77
    :cond_c
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->onBackPressed()V

    .line 116
    return-void
.end method
