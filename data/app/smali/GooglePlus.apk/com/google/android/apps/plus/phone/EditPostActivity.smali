.class public Lcom/google/android/apps/plus/phone/EditPostActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EditPostActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mEditPostFragment:Lcom/google/android/apps/plus/fragments/EditPostFragment;

.field private mShakeDetectorWasRunning:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 142
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->ACTIVITY:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final invalidateMenu()V
    .registers 2

    .prologue
    .line 197
    .line 198
    const v0, 0x7f100005

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->createTitlebarButtons(I)V

    .line 202
    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 3
    .parameter "fragment"

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 77
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditPostFragment;

    if-eqz v0, :cond_b

    .line 78
    check-cast p1, Lcom/google/android/apps/plus/fragments/EditPostFragment;

    .end local p1
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EditPostActivity;->mEditPostFragment:Lcom/google/android/apps/plus/fragments/EditPostFragment;

    .line 80
    :cond_b
    return-void
.end method

.method public onBackPressed()V
    .registers 2

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditPostActivity;->mEditPostFragment:Lcom/google/android/apps/plus/fragments/EditPostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->onDiscard()V

    .line 138
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 178
    packed-switch p2, :pswitch_data_10

    .line 182
    :goto_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 191
    return-void

    .line 180
    :pswitch_7
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->setResult(I)V

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->finish()V

    goto :goto_3

    .line 178
    nop

    :pswitch_data_10
    .packed-switch -0x1
        :pswitch_7
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "savedInstanceState"

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v1, 0x7f030025

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditPostActivity;->setContentView(I)V

    .line 41
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditPostActivity;->showTitlebar(Z)V

    .line 47
    const v1, 0x7f0801ac

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditPostActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditPostActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 48
    const v1, 0x7f100006

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditPostActivity;->createTitlebarButtons(I)V

    .line 52
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 53
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_2d

    .line 54
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->stop()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/EditPostActivity;->mShakeDetectorWasRunning:Z

    .line 56
    :cond_2d
    return-void
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .registers 7
    .parameter "dialogId"
    .parameter "args"

    .prologue
    const/4 v3, 0x0

    .line 152
    sparse-switch p1, :sswitch_data_3c

    .line 171
    const/4 v1, 0x0

    :goto_5
    return-object v1

    .line 154
    :sswitch_6
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 155
    .local v0, builder:Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0801ad

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 156
    const v2, 0x7f0801c6

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 157
    const v2, 0x7f0801c8

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 158
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 159
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_5

    .line 163
    .end local v0           #builder:Landroid/app/AlertDialog$Builder;
    :sswitch_26
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 164
    .local v1, dialog:Landroid/app/ProgressDialog;
    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 165
    const v2, 0x7f080164

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditPostActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 166
    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_5

    .line 152
    :sswitch_data_3c
    .sparse-switch
        0x48ba7 -> :sswitch_26
        0xdc073 -> :sswitch_6
    .end sparse-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100006

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .registers 3

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onDestroy()V

    .line 66
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/EditPostActivity;->mShakeDetectorWasRunning:Z

    if-eqz v1, :cond_14

    .line 67
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditPostActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/ShakeDetector;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ShakeDetector;

    move-result-object v0

    .line 68
    .local v0, shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    if-eqz v0, :cond_14

    .line 69
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/ShakeDetector;->start()Z

    .line 72
    .end local v0           #shakeDetector:Lcom/google/android/apps/plus/phone/ShakeDetector;
    :cond_14
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 113
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_1c

    .line 130
    const/4 v0, 0x0

    :goto_9
    return v0

    .line 115
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditPostActivity;->mEditPostFragment:Lcom/google/android/apps/plus/fragments/EditPostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->onDiscard()V

    goto :goto_9

    .line 120
    :sswitch_10
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditPostActivity;->mEditPostFragment:Lcom/google/android/apps/plus/fragments/EditPostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->onPost()V

    goto :goto_9

    .line 125
    :sswitch_16
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditPostActivity;->mEditPostFragment:Lcom/google/android/apps/plus/fragments/EditPostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->onDiscard()V

    goto :goto_9

    .line 113
    :sswitch_data_1c
    .sparse-switch
        0x102002c -> :sswitch_a
        0x7f09029b -> :sswitch_10
        0x7f09029c -> :sswitch_16
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 91
    const v1, 0x7f09029b

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 92
    .local v0, postItem:Landroid/view/MenuItem;
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 93
    const/4 v1, 0x1

    return v1
.end method

.method protected final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 6
    .parameter "menu"

    .prologue
    .line 105
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_1d

    .line 106
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 107
    .local v1, item:Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x7f09029b

    if-ne v2, v3, :cond_1b

    const/4 v2, 0x1

    :goto_15
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 107
    :cond_1b
    const/4 v2, 0x0

    goto :goto_15

    .line 109
    .end local v1           #item:Landroid/view/MenuItem;
    :cond_1d
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditPostActivity;->mEditPostFragment:Lcom/google/android/apps/plus/fragments/EditPostFragment;

    if-eqz v0, :cond_9

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditPostActivity;->mEditPostFragment:Lcom/google/android/apps/plus/fragments/EditPostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditPostFragment;->onDiscard()V

    .line 101
    :cond_9
    return-void
.end method
