.class public final Lcom/google/android/apps/plus/phone/EventCardAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "EventCardAdapter.java"


# static fields
.field private static sInitialized:Z

.field private static sItemMargin:I

.field private static sLargeDisplayTypeSizeCutoff:D

.field private static sScreenDisplayType:I


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field private mLandscape:Z

.field protected mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .prologue
    .line 35
    const-wide v0, 0x401b99999999999aL

    sput-wide v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sLargeDisplayTypeSizeCutoff:D

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/ColumnGridView;)V
    .registers 16
    .parameter "context"
    .parameter "account"
    .parameter "cursor"
    .parameter "onClickListener"
    .parameter "itemClickListener"
    .parameter "columnGridView"

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    .line 55
    iput-object p4, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 56
    iput-object p5, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    .line 58
    sget-boolean v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sInitialized:Z

    if-nez v0, :cond_5e

    sput-boolean v1, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sInitialized:Z

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    iget v5, v4, Landroid/util/DisplayMetrics;->xdpi:F

    div-float/2addr v0, v5

    iget v5, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v5, v5

    iget v6, v4, Landroid/util/DisplayMetrics;->ydpi:F

    div-float/2addr v5, v6

    mul-float/2addr v0, v0

    mul-float/2addr v5, v5

    add-float/2addr v0, v5

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    sget-wide v7, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sLargeDisplayTypeSizeCutoff:D

    cmpl-double v0, v5, v7

    if-ltz v0, :cond_95

    move v0, v1

    :goto_44
    sput v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sScreenDisplayType:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0d019e

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v0, v4

    float-to-int v0, v0

    sput v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sItemMargin:I

    .line 60
    :cond_5e
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_6b

    move v2, v1

    :cond_6b
    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mLandscape:Z

    .line 62
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mLandscape:Z

    if-eqz v0, :cond_97

    move v0, v1

    :goto_72
    invoke-virtual {p6, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    .line 65
    sget v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sScreenDisplayType:I

    if-nez v0, :cond_99

    :goto_79
    invoke-virtual {p6, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    .line 67
    sget v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sItemMargin:I

    invoke-virtual {p6, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    .line 68
    sget v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sItemMargin:I

    sget v1, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sItemMargin:I

    sget v2, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sItemMargin:I

    sget v3, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sItemMargin:I

    invoke-virtual {p6, v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    .line 70
    new-instance v0, Lcom/google/android/apps/plus/phone/EventCardAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/EventCardAdapter$1;-><init>(Lcom/google/android/apps/plus/phone/EventCardAdapter;)V

    invoke-virtual {p6, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    .line 79
    return-void

    :cond_95
    move v0, v2

    .line 58
    goto :goto_44

    :cond_97
    move v0, v3

    .line 62
    goto :goto_72

    :cond_99
    move v1, v3

    .line 65
    goto :goto_79
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 18
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    .prologue
    .line 83
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventCardAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_4d

    move-object v1, p1

    .line 84
    check-cast v1, Lcom/google/android/apps/plus/views/EventDestinationCardView;

    .line 86
    .local v1, eventDestinationCardView:Lcom/google/android/apps/plus/views/EventDestinationCardView;
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v11

    .line 87
    .local v11, eventData:[B
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/api/services/plusi/model/PlusEvent;

    .line 89
    .local v10, event:Lcom/google/api/services/plusi/model/PlusEvent;
    sget v3, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sScreenDisplayType:I

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p3

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->init(Landroid/database/Cursor;IILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/StreamCardView$ViewedListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamPlusBarClickListener;Lcom/google/android/apps/plus/views/StreamCardView$StreamMediaClickListener;)V

    .line 92
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2, v10}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->bindData(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)V

    .line 94
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mLandscape:Z

    if-eqz v2, :cond_4e

    const/4 v13, 0x1

    .line 97
    .local v13, screenOrientation:I
    :goto_37
    new-instance v12, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v2, -0x3

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-direct {v12, v13, v2, v3, v4}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    .line 101
    .local v12, lp:Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mLandscape:Z

    if-nez v2, :cond_4a

    sget v2, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sScreenDisplayType:I

    if-nez v2, :cond_4a

    .line 102
    const/4 v2, -0x2

    iput v2, v12, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    .line 105
    :cond_4a
    invoke-virtual {v1, v12}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    .end local v1           #eventDestinationCardView:Lcom/google/android/apps/plus/views/EventDestinationCardView;
    .end local v10           #event:Lcom/google/api/services/plusi/model/PlusEvent;
    .end local v11           #eventData:[B
    .end local v12           #lp:Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;
    .end local v13           #screenOrientation:I
    :cond_4d
    return-void

    .line 94
    .restart local v1       #eventDestinationCardView:Lcom/google/android/apps/plus/views/EventDestinationCardView;
    .restart local v10       #event:Lcom/google/api/services/plusi/model/PlusEvent;
    .restart local v11       #eventData:[B
    :cond_4e
    const/4 v13, 0x2

    goto :goto_37
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 5
    .parameter "context"
    .parameter "cursor"
    .parameter "parent"

    .prologue
    .line 111
    new-instance v0, Lcom/google/android/apps/plus/views/EventDestinationCardView;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/views/EventDestinationCardView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
