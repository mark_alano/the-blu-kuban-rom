.class public Lcom/google/android/apps/plus/phone/EventInviteeListActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EventInviteeListActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragmentActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final EVENT_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mCanInvitePeople:Z

.field private mEventId:Ljava/lang/String;

.field private final mHandler:Landroid/os/Handler;

.field private mOwnerId:Ljava/lang/String;

.field private mPendingRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .prologue
    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "can_invite_people"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->EVENT_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mHandler:Landroid/os/Handler;

    .line 296
    new-instance v0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity$3;-><init>(Lcom/google/android/apps/plus/phone/EventInviteeListActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/EventInviteeListActivity;Lcom/google/android/apps/plus/content/AudienceData;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 37
    const/4 v0, 0x0

    const v1, 0x7f08038e

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mEventId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mOwnerId:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2, p1}, Lcom/google/android/apps/plus/service/EsService;->invitePeopleToEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/EventInviteeListActivity;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()[Ljava/lang/String;
    .registers 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->EVENT_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/EventInviteeListActivity;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .registers 7
    .parameter "requestId"
    .parameter "result"

    .prologue
    .line 305
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_d

    .line 325
    :cond_c
    :goto_c
    return-void

    .line 309
    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 311
    .local v0, frag:Landroid/support/v4/app/DialogFragment;
    if-eqz v0, :cond_1e

    .line 312
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 315
    :cond_1e
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    .line 317
    if-eqz p2, :cond_35

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_35

    .line 318
    const v1, 0x7f0801a8

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_c

    .line 323
    :cond_35
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mOwnerId:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 339
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 7
    .parameter "requestCode"
    .parameter "resultCode"
    .parameter "data"

    .prologue
    .line 189
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 190
    const/4 v1, -0x1

    if-ne p2, v1, :cond_8

    if-nez p3, :cond_9

    .line 205
    :cond_8
    :goto_8
    return-void

    .line 194
    :cond_9
    packed-switch p1, :pswitch_data_20

    goto :goto_8

    .line 196
    :pswitch_d
    const-string v1, "audience"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    .line 198
    .local v0, audience:Lcom/google/android/apps/plus/content/AudienceData;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/phone/EventInviteeListActivity$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity$1;-><init>(Lcom/google/android/apps/plus/phone/EventInviteeListActivity;Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_8

    .line 194
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_d
    .end packed-switch
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 107
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EventInviteeListFragment;

    if-eqz v0, :cond_d

    .line 108
    check-cast p1, Lcom/google/android/apps/plus/fragments/EventInviteeListFragment;

    .end local p1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mEventId:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mOwnerId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/fragments/EventInviteeListFragment;->setEventId(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_d
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    if-eqz p1, :cond_19

    .line 68
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 69
    const-string v0, "request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    .line 73
    :cond_19
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mEventId:Ljava/lang/String;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "owner_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mOwnerId:Ljava/lang/String;

    .line 76
    const v0, 0x7f030028

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->setContentView(I)V

    .line 80
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->showTitlebar(Z)V

    .line 84
    const v0, 0x7f080354

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 85
    const v0, 0x7f100007

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->createTitlebarButtons(I)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 89
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .registers 5
    .parameter "loaderId"
    .parameter "args"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    .line 229
    .local v0, account:Lcom/google/android/apps/plus/content/EsAccount;
    new-instance v1, Lcom/google/android/apps/plus/phone/EventInviteeListActivity$2;

    invoke-direct {v1, p0, p0, v0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity$2;-><init>(Lcom/google/android/apps/plus/phone/EventInviteeListActivity;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-object v1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f100007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 119
    const/4 v0, 0x1

    return v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 37
    check-cast p2, Landroid/database/Cursor;

    .end local p2
    if-eqz p2, :cond_1b

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->setTitlebarTitle(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_1c

    :goto_19
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mCanInvitePeople:Z

    :cond_1b
    return-void

    :cond_1c
    move v0, v1

    goto :goto_19
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .registers 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, loader:Landroid/support/v4/content/Loader;,"Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 11
    .parameter "item"

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 145
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_28

    move v7, v5

    .line 157
    :goto_a
    return v7

    .line 147
    :sswitch_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->onBackPressed()V

    goto :goto_a

    .line 152
    :sswitch_f
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const v0, 0x7f080388

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0xb

    move-object v0, p0

    move v6, v5

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_a

    .line 145
    :sswitch_data_28
    .sparse-switch
        0x102002c -> :sswitch_b
        0x7f09029d -> :sswitch_f
    .end sparse-switch
.end method

.method public onPause()V
    .registers 2

    .prologue
    .line 292
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 294
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    .prologue
    .line 127
    const v0, 0x7f09029d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mCanInvitePeople:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 128
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .registers 4
    .parameter "menu"

    .prologue
    .line 136
    const v0, 0x7f09029d

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mCanInvitePeople:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 137
    return-void
.end method

.method public onResume()V
    .registers 3

    .prologue
    .line 269
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->isIntentAccountActive()Z

    move-result v1

    if-nez v1, :cond_d

    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->finish()V

    .line 285
    :cond_c
    :goto_c
    return-void

    .line 276
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    .line 278
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 279
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_c

    .line 280
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    .line 281
    .local v0, result:Lcom/google/android/apps/plus/service/ServiceResult;
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    .line 282
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    goto :goto_c
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    .prologue
    .line 96
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 98
    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 100
    :cond_12
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 1

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventInviteeListActivity;->onBackPressed()V

    .line 168
    return-void
.end method
