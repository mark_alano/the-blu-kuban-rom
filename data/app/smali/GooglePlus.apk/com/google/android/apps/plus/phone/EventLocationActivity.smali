.class public Lcom/google/android/apps/plus/phone/EventLocationActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EventLocationActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;


# instance fields
.field private mInitialQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 62
    instance-of v1, p1, Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    if-eqz v1, :cond_13

    move-object v0, p1

    .line 63
    check-cast v0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    .line 64
    .local v0, theFragment:Lcom/google/android/apps/plus/fragments/EventLocationFragment;
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->setOnLocationSelectedListener(Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;)V

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->mInitialQuery:Ljava/lang/String;

    if-eqz v1, :cond_13

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->mInitialQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->setInitialQueryString(Ljava/lang/String;)V

    .line 69
    .end local v0           #theFragment:Lcom/google/android/apps/plus/fragments/EventLocationFragment;
    :cond_13
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    if-nez p1, :cond_1f

    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "location"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    .line 39
    .local v0, bytes:[B
    if-eqz v0, :cond_1f

    .line 40
    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceJson;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/PlaceJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/Place;

    .line 41
    .local v1, place:Lcom/google/api/services/plusi/model/Place;
    iget-object v2, v1, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->mInitialQuery:Ljava/lang/String;

    .line 45
    .end local v0           #bytes:[B
    .end local v1           #place:Lcom/google/api/services/plusi/model/Place;
    :cond_1f
    const v2, 0x7f03002b

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->setContentView(I)V

    .line 49
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->showTitlebar(Z)V

    .line 53
    const v2, 0x7f080389

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public final onLocationSelected(Lcom/google/api/services/plusi/model/Place;)V
    .registers 5
    .parameter "place"

    .prologue
    .line 114
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 115
    .local v0, data:Landroid/content/Intent;
    if-eqz p1, :cond_14

    .line 116
    const-string v1, "location"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceJson;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/api/services/plusi/model/PlaceJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 118
    :cond_14
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->setResult(ILandroid/content/Intent;)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->finish()V

    .line 120
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    .line 88
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_e

    .line 95
    const/4 v0, 0x0

    :goto_8
    return v0

    .line 90
    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->onBackPressed()V

    .line 91
    const/4 v0, 0x1

    goto :goto_8

    .line 88
    :pswitch_data_e
    .packed-switch 0x102002c
        :pswitch_9
    .end packed-switch
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->finish()V

    .line 81
    :cond_c
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->onBackPressed()V

    .line 106
    return-void
.end method
