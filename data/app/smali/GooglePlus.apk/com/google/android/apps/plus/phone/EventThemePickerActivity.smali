.class public Lcom/google/android/apps/plus/phone/EventThemePickerActivity;
.super Lcom/google/android/apps/plus/fragments/EsTabActivity;
.source "EventThemePickerActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/EventThemeListFragment$OnThemeSelectedListener;


# direct methods
.method public constructor <init>()V
    .registers 3

    .prologue
    .line 29
    const/4 v0, 0x0

    const v1, 0x7f0900a6

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/fragments/EsTabActivity;-><init>(II)V

    .line 30
    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .registers 3

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method protected final getViewForLogging$65a8335d()Lcom/google/android/apps/plus/analytics/OzViews;
    .registers 2

    .prologue
    .line 155
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT_THEMES:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .registers 4
    .parameter "fragment"

    .prologue
    .line 61
    instance-of v1, p1, Lcom/google/android/apps/plus/fragments/EventThemeListFragment;

    if-eqz v1, :cond_11

    move-object v1, p1

    .line 62
    check-cast v1, Lcom/google/android/apps/plus/fragments/EventThemeListFragment;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/EventThemeListFragment;->setOnThemeSelectedListener(Lcom/google/android/apps/plus/fragments/EventThemeListFragment$OnThemeSelectedListener;)V

    .line 64
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->getTabIndexForFragment(Landroid/support/v4/app/Fragment;)I

    move-result v0

    .line 66
    .local v0, tabIndex:I
    packed-switch v0, :pswitch_data_1c

    .line 75
    .end local v0           #tabIndex:I
    :cond_11
    :goto_11
    return-void

    .line 68
    .restart local v0       #tabIndex:I
    :pswitch_12
    const/4 v1, 0x0

    invoke-virtual {p0, v1, p1}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->onAttachFragment(ILandroid/support/v4/app/Fragment;)V

    goto :goto_11

    .line 71
    :pswitch_17
    const/4 v1, 0x1

    invoke-virtual {p0, v1, p1}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->onAttachFragment(ILandroid/support/v4/app/Fragment;)V

    goto :goto_11

    .line 66
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_12
        :pswitch_17
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    .prologue
    const/4 v3, 0x1

    .line 37
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f030036

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->setContentView(I)V

    .line 41
    const/4 v0, 0x0

    const v1, 0x7f0900a4

    const v2, 0x7f0900a7

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->addTab(III)V

    .line 43
    const v0, 0x7f0900a5

    const v1, 0x7f0900a8

    invoke-virtual {p0, v3, v0, v1}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->addTab(III)V

    .line 48
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->showTitlebar(Z)V

    .line 52
    const v0, 0x7f080384

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->setTitlebarTitle(Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method protected final onCreateTab(I)Landroid/support/v4/app/Fragment;
    .registers 4
    .parameter "tabId"

    .prologue
    .line 82
    packed-switch p1, :pswitch_data_14

    .line 90
    const/4 v0, 0x0

    :goto_4
    return-object v0

    .line 84
    :pswitch_5
    new-instance v0, Lcom/google/android/apps/plus/fragments/EventThemeListFragment;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/EventThemeListFragment;-><init>(I)V

    goto :goto_4

    .line 87
    :pswitch_c
    new-instance v0, Lcom/google/android/apps/plus/fragments/EventThemeListFragment;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/EventThemeListFragment;-><init>(I)V

    goto :goto_4

    .line 82
    nop

    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_5
        :pswitch_c
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .parameter "item"

    .prologue
    .line 110
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_e

    .line 117
    const/4 v0, 0x0

    :goto_8
    return v0

    .line 112
    :pswitch_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->onBackPressed()V

    .line 113
    const/4 v0, 0x1

    goto :goto_8

    .line 110
    :pswitch_data_e
    .packed-switch 0x102002c
        :pswitch_9
    .end packed-switch
.end method

.method public onResume()V
    .registers 2

    .prologue
    .line 98
    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsTabActivity;->onResume()V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_c

    .line 101
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->finish()V

    .line 103
    :cond_c
    return-void
.end method

.method public final onThemeSelected(ILjava/lang/String;)V
    .registers 5
    .parameter "themeId"
    .parameter "imageUrl"

    .prologue
    .line 135
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 136
    .local v0, data:Landroid/content/Intent;
    const-string v1, "theme_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 137
    const-string v1, "theme_url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->finish()V

    .line 140
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .registers 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventThemePickerActivity;->onBackPressed()V

    .line 128
    return-void
.end method
