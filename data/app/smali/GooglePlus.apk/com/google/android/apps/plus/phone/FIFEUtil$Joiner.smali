.class final Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;
.super Ljava/lang/Object;
.source "FIFEUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/FIFEUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Joiner"
.end annotation


# instance fields
.field private final separator:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .registers 2
    .parameter "separator"

    .prologue
    .line 446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;->separator:Ljava/lang/String;

    .line 448
    return-void
.end method

.method public static on(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;
    .registers 2
    .parameter "separator"

    .prologue
    .line 441
    new-instance v0, Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static toString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .registers 2
    .parameter "part"

    .prologue
    .line 475
    instance-of v0, p0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_7

    check-cast p0, Ljava/lang/CharSequence;

    .end local p0
    :goto_6
    return-object p0

    .restart local p0
    :cond_7
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_6
.end method


# virtual methods
.method public final appendTo(Ljava/lang/StringBuilder;Ljava/lang/Iterable;)Ljava/lang/StringBuilder;
    .registers 5
    .parameter "builder"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/StringBuilder;",
            "Ljava/lang/Iterable",
            "<*>;)",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    .prologue
    .line 454
    .local p2, parts:Ljava/lang/Iterable;,"Ljava/lang/Iterable<*>;"
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 455
    .local v0, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<*>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 456
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;->toString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 457
    :goto_15
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 458
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;->separator:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 459
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/FIFEUtil$Joiner;->toString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_15

    .line 462
    :cond_2c
    return-object p1
.end method
